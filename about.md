---
layout: page
title: About
permalink: /about/
---

This content now lives at [makerforums](https://forum.makerforums.info/c/herculien)
and is continuing on there. Please go log in there with the same google
account you used for Google+ and you will still own all the content you
wrote.  This site is out of date and not maintained.

This site was an initial attempt to archive Google+ data about
the [HercuLien](https://github.com/eclsnowman/HercuLien) and
[Eustathios](https://github.com/eclsnowman/Eustathios-Spider-V2)
3D Printers.
