---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

This content now lives at [makerforums](https://forum.makerforums.info/)
and is continuing on there. Please go log in there with the same google
account you used for Google+ and you will still own all the content you
wrote.

This static site is out of date and not maintained now that
the content is available at makerforums.

This site was an initial attempt to archive Google+ data about
the [HercuLien](https://github.com/eclsnowman/HercuLien) and
[Eustathios](https://github.com/eclsnowman/Eustathios-Spider-V2)
3D Printers.
