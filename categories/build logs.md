---
layout: category
category: "Build Logs"
---

This content now lives at [forum.makerforums.info](https://forum.makerforums.info/c/herculien/build-logs).
and is continuing on there. Please go log in there with the same google
account you used for Google+ and you will still own all the content you
wrote.  This site is out of date and not maintained.
