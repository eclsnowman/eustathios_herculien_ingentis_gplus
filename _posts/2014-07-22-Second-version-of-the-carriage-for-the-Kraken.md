---
layout: post
title: "Second version of the carriage for the Kraken"
date: July 22, 2014 00:46
category: "Show and Tell"
author: Brian Bland
---
Second version of the carriage for the Kraken.  Centered on the rods better and this one will let the Kraken actually touch the build surface.  These prints were sliced with CraftWare software.  CraftWare lets you modify support similar to Simplify3D.﻿



![images/f4546c9962c5bb79a7072a176055349f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f4546c9962c5bb79a7072a176055349f.jpeg)
![images/15e00dc697d67747f7b10bac53d877a6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/15e00dc697d67747f7b10bac53d877a6.jpeg)
![images/0f4ab6ece890334630ba40220ab0ec96.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0f4ab6ece890334630ba40220ab0ec96.jpeg)
![images/b6a9bb32607e3580354af69f6ea2cdb4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b6a9bb32607e3580354af69f6ea2cdb4.jpeg)

**Brian Bland**

---
---
**Joe Spanier** *July 22, 2014 00:48*

Your part came out much better then the melty tower or garbage I've done in craftware. Guess I should look more haha. 


---
**Brian Bland** *July 22, 2014 00:54*

It took a little bit of testing to get the settings where I liked them.  Support works great.


---
**Eric Lien** *July 22, 2014 02:04*

You going to see rub of the cooling lines on the smooth rod?


---
**Brian Bland** *July 22, 2014 02:06*

Yes.  I will print up something to help with that.


---
**Eric Lien** *July 22, 2014 03:03*

**+Brian Bland** you could also look at angled barb fittings. Something like this: [http://www.amazonsupply.com/anderson-metals-brass-fitting-degree/dp/B0070TV4B8](http://www.amazonsupply.com/anderson-metals-brass-fitting-degree/dp/B0070TV4B8)


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/YAcMZNNCbmn) &mdash; content and formatting may not be reliable*
