---
layout: post
title: "Limit switch wiring Does the Azteeg X5 V3 require NC or NO limit switch wiring?"
date: September 22, 2016 22:47
category: "Discussion"
author: jerryflyguy
---
Limit switch wiring



Does the Azteeg X5 V3 require NC or NO limit switch wiring?









**jerryflyguy**

---
---
**Sébastien Plante** *September 22, 2016 23:11*

Either will do, you can reverse it in the config


---
**Zane Baird** *September 22, 2016 23:36*

I prefer NC simply because it trigger a limit in the case of a disconnected wire rather than crashing into the switch


---
**jerryflyguy** *September 22, 2016 23:42*

**+Zane Baird** agreed, didn't realize it was configurable. 


---
**Sean B** *September 23, 2016 00:11*

I did normally open, just make sure you change the config.  Mine was defaulted to NC.  Potato potato.  Typing that feels stupid.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/BLU1sTwde1R) &mdash; content and formatting may not be reliable*
