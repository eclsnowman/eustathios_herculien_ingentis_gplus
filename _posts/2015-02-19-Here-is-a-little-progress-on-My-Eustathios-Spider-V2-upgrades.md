---
layout: post
title: "Here is a little progress on My Eustathios Spider V2 upgrades"
date: February 19, 2015 03:40
category: "Deviations from Norm"
author: Eric Lien
---
Here is a little progress on My Eustathios Spider V2 upgrades.



Usable Z is now just short of 300 (I will get it there) using the same length extrusions. It uses the new V4 carriage, and also a New Z belt system that uses 608ZZ bearings instead of bushing blocks and thrust bearings. I know thrust bearings are in theory better... but accessibility to 608 is higher, and the loads are so low I see no issue. I have had my HercuLien running on them for a long time, with a MUCH heavier bed and zero issue. Also this new setup eliminates alignment of independent blocks below the bed. This block does necessitate consistent prints since I removed one degree of freedom of the two block setup... Nice thing is if you printer is off, it should be off in the same amount since both the bed supports and this new bottom block print in the same orientation.



I am getting close.



![images/59eb2d9a09d54391cb45527c85205699.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/59eb2d9a09d54391cb45527c85205699.png)
![images/c8e032de42bca041d7b69c294b0e6f13.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c8e032de42bca041d7b69c294b0e6f13.png)
![images/40029a2dfeec4dde1b8075302848d54e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/40029a2dfeec4dde1b8075302848d54e.png)
![images/f8275d5c89c34dc8f24c9f1e5e34cc30.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f8275d5c89c34dc8f24c9f1e5e34cc30.png)
![images/b678e1a10b9860b1de13490eecf368ea.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b678e1a10b9860b1de13490eecf368ea.png)
![images/07fdad65ce1c5ce8b2e1cdeeb96d5de9.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/07fdad65ce1c5ce8b2e1cdeeb96d5de9.png)
![images/8e5b2fd5e362a82854ccf91c77f719d0.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8e5b2fd5e362a82854ccf91c77f719d0.png)

**Eric Lien**

---
---
**Seth Messer** *February 19, 2015 03:43*

You are a machine. Thanks **+Eric Lien**


---
**Eric Lien** *February 19, 2015 03:49*

**+Seth Messer** I try.


---
**Erik Scott** *February 19, 2015 04:17*

Nice! I may take advantage of some of these changes. I understand why you put the X and Y steppers exterior to the frame as it eliminates the compact 32-tooth pulleys, but I really like having them tucked away in the basement. I do really like the integrated mount for the leadscrew and smooth rod and moving the z-belt higher to just below the floor. 


---
**Eric Lien** *February 19, 2015 04:41*

**+Erik Scott** I like it to, but the advantages of the costs and performance shorter belts outweigh the cleanliness factor for me this time. It works really well on HercuLien, and shorter belts should in theory have less stretch.


---
**Eric Lien** *February 19, 2015 04:45*

I am wondering if I should add 2  idlers to the z motor mount to get more pulley engagement?


---
**Gus Montoya** *February 19, 2015 04:52*

Oh wow that looks really nice!


---
**Isaac Arciaga** *February 19, 2015 05:19*

**+Eric Lien** way to go! Do the lead screw still require a 56mm step? Hard for me to tell.

Keep up the good work! Thank you!


---
**Eric Lien** *February 19, 2015 05:21*

**+Isaac Arciaga** they are shorter, but not below 40mm to get away from that stupid keyway requirement.


---
**Eric Lien** *February 19, 2015 05:26*

But now that you mention it maybe I should thread the end slightly to skip the bottom 8mm collar and use a nut to pull the bearing assembly together instead?


---
**Isaac Arciaga** *February 19, 2015 06:04*

**+Eric Lien** I'm keeping the collar since I already bought them ;). The thread would be nice though. What would be cool is if there was an option to use the common 10mm 1 start lead screws with the 6.35x12mm step so people outside of the US are not limited to using custom from Misumi.



I really like what you did to the Z blocks in the basement!


---
**Eric Lien** *February 19, 2015 06:19*

**+Isaac Arciaga** just an FYI I dropped the floor on this to get the extra height. So rods and leadscrews are longer. Also extrusion holes are lower for the floor. The standard stuff from Jason's BOM will all work, but without the extra height gain.


---
**Jean-Francois Couture** *February 19, 2015 13:10*

**+Eric Lien** You're a SolidWorks master ! that looks great, can't wait to see the real thing :)


---
**Erik Scott** *February 19, 2015 14:31*

Do the longer belts have an adverse effect on print quality? Or are you talking about stretch over time? 



Will you be including electronics mounting provisions and heated bed stuff in the new BOM and in the cad models? I'm still trying to figure out all this heated bed stuff. 


---
**Eric Lien** *February 19, 2015 15:47*

**+Erik Scott** I am planning on it. I will be planning on a smaller PSU since I recommend 120v and SSR on the bed. The PSU only needs to power the steppers and electronics.



Anybody have a good 150w or similar 24v psu they recommend? Something compact.


---
**Isaac Arciaga** *February 19, 2015 19:09*

**+Eric Lien** would it be possible to design Z to allow for a 12x12 heat spreader instead of a larger custom cut one? Held in place with clips similar to the Taz4 or the corner springy brackets from Ultibots? [http://www.ultibots.com/springy-corner-bracket-kit-x4/](http://www.ultibots.com/springy-corner-bracket-kit-x4/)

My thought was 12x12 6061 sheets are easily source from Amazon, etc.


---
**Tim Rastall** *February 19, 2015 19:30*

Seen this **+Oliver Seiler**​? Eliviates  some of that long belt backlash we were discussing.  Nice work as ever **+Eric Lien**​. 


---
**Tim Rastall** *February 19, 2015 19:33*

**+Eric Lien**​ this has been working fine for me so far:

[http://www.aliexpress.com/item/400W-24V-Single-Output-Switching-power-supply-for-LED-Strip-light-AC-to-DC/621192132.html](http://www.aliexpress.com/item/400W-24V-Single-Output-Switching-power-supply-for-LED-Strip-light-AC-to-DC/621192132.html)


---
**Eric Lien** *February 19, 2015 22:30*

**+Tim Rastall** thanks. I am kind of looking for a small one, just powerful enough for the controller, steppers, display, and maybe some led lights. Since the heated bed will run 120V AC I think 150W or more is plenty. I am kinda trying to find a real compact one for space, without compromising quality.


---
**Tim Rastall** *February 20, 2015 01:49*

**+Eric Lien** Something like these?

[http://www.trcelectronics.com/View/Phihong/PSA120-240.shtml](http://www.trcelectronics.com/View/Phihong/PSA120-240.shtml)

Or

[http://www.trcelectronics.com/View/Cosel/PLA150F-24.shtml](http://www.trcelectronics.com/View/Cosel/PLA150F-24.shtml)

or better still:

[http://www.trcelectronics.com/View/Phihong/PSA120U-240V.shtml](http://www.trcelectronics.com/View/Phihong/PSA120U-240V.shtml)


---
**Eric Lien** *February 20, 2015 03:10*

Thanks **+Tim Rastall**​



What are people thoughts on linear unregulated power supplies. Seems like a lot of the Serious CNC guys like them over switching supplies.


---
**Seth Messer** *February 20, 2015 03:16*

just wanted to say that.. i have much, so much, to learn. i was thinking i'd just be putting everything together. :D <b>grabs notebook and takes more notes</b>


---
**Erik Scott** *February 20, 2015 03:25*

**+Seth Messer**  Haha, when it comes to electronics, my eyes kinda gloss over and I just want someone to tell me "put wire here, here, and here." It's the mechanics that I enjoy. But yes, It's really fun to follow everything that's happening in this community. 


---
**Tim Rastall** *February 20, 2015 03:36*

**+Eric Lien**​​ different use case I think.  Cnc machines use stand alone stepper drivers connected directly to the psu. As soon as they are enabled they pull a pretty constant current so a linear psu will serve them well.  Particularly as linears have really flat,  noise free outputs.  Linears are huge and heavy (compared to switching). Good link here: [https://www.google.co.nz/url?sa=t&source=web&rct=j&ei=iavmVIHZOpWD8gXK44CYDg&url=https://www.valuetronics.com/Manuals/Lambda_%2520linear_versus_switching.pdf&ved=0CBoQFjAA&usg=AFQjCNFZD2MSnu_t-e2z0fGCNHjnTlaDCw&sig2=GuR95Hwad1QawXBVS2_hLQ](https://www.google.co.nz/url?sa=t&source=web&rct=j&ei=iavmVIHZOpWD8gXK44CYDg&url=https://www.valuetronics.com/Manuals/Lambda_%2520linear_versus_switching.pdf&ved=0CBoQFjAA&usg=AFQjCNFZD2MSnu_t-e2z0fGCNHjnTlaDCw&sig2=GuR95Hwad1QawXBVS2_hLQ)﻿


---
**Seth Messer** *February 20, 2015 03:37*

No doubt. I love this stuff. My main passion is in software and wood working. I can't wait to get the parts going to add 3d printing to the mix. Then I imagine cnc will be next. :)


---
**Eric Lien** *February 20, 2015 04:02*

**+Tim Rastall** thanks.



P.S. how do you always know so dang much.


---
**Tim Rastall** *February 20, 2015 05:08*

**+Eric Lien**​ I'm just good at rapidly assimilating information. Plus I've been into printers since 2011. Absorbing information is one of those things that seems to get better as you get older ime. :) Anyway,  now you know what I know and in a year's time someone will be asking you how you know so darn much (if not before). ﻿


---
**William Eades** *February 20, 2015 19:57*

**+Eric Lien** I currently run a Rigidbot Regular and the default replacement 24v PWS suggested for the Rigidbrick (360w) is the Meanwell line.  Their reliability is well tested. I have 2, a 400 and a 600 (For my next build, a 24 x 24 x 24" Build Volume Dual E3D Volcano unit)

As for PWS's, Linears have a high idle current draw compared to switchers, which only output when a load demand is sensed.  So if you are concerned about power bills, go with a switcher.


---
**Oliver Seiler** *February 23, 2015 07:26*

**+Eric Lien** I really like what you did here - thanks for sharing all your work. I've just started ordering and printing parts for my Eustathios build and try to hold back on those I think are effected by your changes. No pressure, but I can't wait to see the next iteration ;)


---
**Gus Montoya** *February 23, 2015 07:32*

The first bits of my hardware just came in. Only the corner gussets and screws and E3D v6 hotend. Reusing stepper nema17 motors and control board from my 2up. I consider this being committed now. **+Eric Lien**  Whats the new ETA for the final design? 


---
**Tim Rastall** *February 23, 2015 07:36*

**+Eric Lien**​ you might want to consider some printed guards for the exposed pulleys/belts, curious little fingers and all that :) 


---
**Eric Lien** *February 23, 2015 11:55*

**+Tim Rastall** good idea.



**+Gus Montoya**​ I am working on it. Just a balancing act between work, family, and printers. 


---
**Oliver Seiler** *February 23, 2015 19:36*

What length belts are you using for the X and Y steppers? I guess there's a bit of freedom by moving the steppers closer/further away?


---
**Erik Scott** *February 23, 2015 19:42*

**+Tim Rastall**​ I was thinking about guards/coverings for the X and Y stepper belts as well, though more for aesthetic reasons. 



**+Oliver Seiler**​ I think so. It's just like the current design, just closer and external. 


---
**Eric Lien** *February 23, 2015 20:24*

**+Oliver Seiler** if you look at my Herculien BOM, that's what I had planned.


---
**Gus Montoya** *February 26, 2015 01:33*

Hi **+Eric Lien** , is it safe to order the frame based on the part numbers from your github?   Lien3D_Eustathios_Spider/Drawings/Eustathios_Frame.SLDDRW   

That has not changed from the looks of it.


---
**Eric Lien** *February 26, 2015 03:15*

**+Gus Montoya** it has changed slightly, the hole locations are different, and rod and leadscrews are longer.


---
**Gus Montoya** *February 26, 2015 04:44*

ic, ok then I'll wait on that too. 


---
**Gus Montoya** *March 07, 2015 01:18*

Hi **+Eric Lien**, hows this coming along? Anything standardized now that I can order?


---
**Eric Lien** *March 07, 2015 01:33*

I have been sick recently, so I fell behind on my timeline. I am not finalized yet, I appreciate your patience and it should be worth the wait.


---
**Gus Montoya** *March 07, 2015 02:16*

So you got the bug too? I've had a mean bug that wouldn't go away for the last week and a half. I went to the gym today and boy does it feel like I haven't been in a very very long time. Hope you feel much much better. 


---
**Seth Messer** *March 07, 2015 12:02*

**+Eric Lien** i echo everyone else and continue to say thanks for you doing this for us Eric, and hope you get to feeling better.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/WyMLjAvjTcc) &mdash; content and formatting may not be reliable*
