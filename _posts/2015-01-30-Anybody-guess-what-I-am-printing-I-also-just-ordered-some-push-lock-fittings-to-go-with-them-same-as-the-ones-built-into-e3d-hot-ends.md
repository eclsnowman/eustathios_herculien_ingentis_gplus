---
layout: post
title: "Anybody guess what I am printing? I also just ordered some push lock fittings to go with them (same as the ones built into e3d hot ends)"
date: January 30, 2015 07:39
category: "Show and Tell"
author: Eric Lien
---
Anybody guess what I am printing? I also just ordered some push lock fittings to go with them (same as the ones built into e3d hot ends). Time to convert all my Bondtech extruders to V2 :)


**Video content missing for image https://lh6.googleusercontent.com/-7uYYjnt0K2A/VMs1N6q7e8I/AAAAAAAAoX4/obsUJM5KXLc/s0/VID_20150129_223550.mp4.gif**
![images/b24414e2816cac794a2b2fbdc6d576c0.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b24414e2816cac794a2b2fbdc6d576c0.gif)



**Eric Lien**

---
---
**Isaac Arciaga** *January 30, 2015 07:59*

**+Eric Lien** are the hobb parts available for purchase yet?


---
**Olivier KUNTZ** *January 30, 2015 09:11*

I saw you print directly on aluminium? You have no wrapping problem with this?


---
**Eric Lien** *January 30, 2015 12:34*

**+Olivier KUNTZ** there is window glass there also, and ample amounts of sauve ultra hold hair spray. Zero warping on these.


---
**Eric Lien** *January 30, 2015 12:36*

**+Isaac Arciaga** I think Martin is looking for people to contact him for preorders on the V2. He had a post in the 3d printing community yesterday.


---
**Olivier KUNTZ** *January 30, 2015 13:37*

**+Eric Lien**

I didn't try hair spray. Thing to do on next print :-) Thanks for the explanation.


---
**Daniel Salinas** *January 30, 2015 15:42*

**+Eric Lien** I spoke with Martin and he added me to the next list of shipments.  Super pumped about that.  I like the design and mechanically it makes a whole lot more sense to drive from both sides.


---
**Daniel Salinas** *January 30, 2015 15:45*

**+Olivier KUNTZ** I've had great success with the purple elmers glue stick and ABS.  My printer is in a drafty area and ulta mega hold hairspray wasn't working so in case you haven't tried that, it's worth a shot too.


---
**Eric Lien** *January 30, 2015 15:50*

Thats odd. I found hairspray to hold better for ABS, and pva glue (like elmers) work better for PLA. This is what I use: [http://www.walmart.com/ip/10293438?wmlspartner=wlpa&adid=22222222227010236330&wl0=&wl1=g&wl2=c&wl3=40842560672&wl4=&wl5=pla&wl6=57784448866&veh=sem](http://www.walmart.com/ip/10293438?wmlspartner=wlpa&adid=22222222227010236330&wl0=&wl1=g&wl2=c&wl3=40842560672&wl4=&wl5=pla&wl6=57784448866&veh=sem)


---
**Daniel Salinas** *January 30, 2015 15:54*

crazy! my experience is the exact opposite.  I can't get abs to print on hairspray without warping but my pla sticks to it perfectly and the glue stick is rock solid for abs for me.


---
**Daniel Salinas** *January 30, 2015 15:56*

I've got about 40 prints total using abs and the first half of those were done with hairspray and abs juice.  All with warping.  The last half with elmers glue stick and maybe a hint of warping on one or two of them, otherwise perfect.


---
**Eric Lien** *January 30, 2015 17:08*

**+Daniel Salinas**​ must be filament formulations or something. One other thing that helped me was the switch to 120v on the bed heater and very accurately tuning the PID for the bed when running at 110C. My temp graphs are flats as a granite slab.﻿


---
**Isaac Arciaga** *January 30, 2015 18:36*

**+Eric Lien** **+Daniel Salinas**  Have you tried PEI (Ultem) sheets? I've been using the material for ABS/PLA/PETT/PETG/PET+/Wood Fill for a couple months now on my MendelMax 3 and FuseMatic printers with much success. Only video I have printing abs on it during the MM3 beta 
{% include youtubePlayer.html id=YsHL42JWx0A %}
[https://www.youtube.com/watch?v=YsHL42JWx0A](https://www.youtube.com/watch?v=YsHL42JWx0A)



Use .03" thick sheets. don't go thicker. It's an insulator.

[http://amzn.com/B00CPRDDLY](http://amzn.com/B00CPRDDLY)



Adhere to your build surface with 3M 468MP transfer tape. (a.k.a gorilla snot) laying down 2" wide strips is the easiest to use. You can purchase different lengths. [http://amzn.com/B00P7K7T84](http://amzn.com/B00P7K7T84)



The big thread where it began (I think)

[http://forum.seemecnc.com/viewtopic.php?f=36&t=4336](http://forum.seemecnc.com/viewtopic.php?f=36&t=4336)


---
**James Rivera** *January 30, 2015 18:43*

**+Eric Lien** I hadn't really thought too much about it, but it seems obvious reading it now (hindsight) that keeping heat bed temperature <b>fluctuations</b> to a minimum would help to maintain part adhesion (and thus reduce warping). That seems to explain why my printer with an aluminum bed is so much better than the one without it--the larger thermal mass and heat spreading nature of the thick aluminum plate naturally reduces those fluctuations. PID tuning will certainly improve that, too. (I think with my stock Printrboard firmware that I may be just using bang bang temp control). Anyway, this seems like a great argument for using the mains power with an SSR like you've done. Your design continues to be proven an excellent one! :)


---
**Eric Lien** *January 30, 2015 19:25*

**+Isaac Arciaga** thanks for the reminder. I have wanted to try yltem before. My big problem is sourcing a big enough sheet for my bed that is not cost prohibitive.


---
**Joe Kachmar** *January 31, 2015 17:57*

**+Eric Lien** McMaster-Carr sells 12"x12", 1/16" thick Ultem Sheet for around $30. Seems like it would be perfect for your Eustathios, if you wanted to try it out.


---
**James Rivera** *January 31, 2015 18:30*

**+Joe Kachmar** the aforementioned Amazon link from **+Isaac Arciaga** has 12"x12"x0.03" for $17.27, with free shipping via Prime.

[http://www.amazon.com/dp/B0013HKZTA/ref=biss_dp_t_asn](http://www.amazon.com/dp/B0013HKZTA/ref=biss_dp_t_asn)


---
**Eric Lien** *January 31, 2015 18:44*

Problem is HercuLien in 17" x17"


---
**Seth Messer** *February 11, 2015 22:35*

Is there a particular BOM for everything needed to go with one of the Bondtech V2 extruders chambered in 1.75mm? Putting together my shopping list and was thinking of jumping on the beta train (despite not having a printer right now) as part of my first build.. risky move?


---
**Eric Lien** *February 11, 2015 22:55*

**+Seth Messer** I would contact martin about getting in on the beta. You really can't go wrong with any of the ones he has created so far. Even the alpha unit I have is running better than any other extruder ever used.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/SowMU6o7ps7) &mdash; content and formatting may not be reliable*
