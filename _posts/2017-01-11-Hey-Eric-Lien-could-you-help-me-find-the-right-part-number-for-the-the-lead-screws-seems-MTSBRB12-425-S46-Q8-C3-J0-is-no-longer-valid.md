---
layout: post
title: "Hey Eric Lien could you help me find the right part number for the the lead screws, seems MTSBRB12-425-S46-Q8-C3-J0 is no longer valid"
date: January 11, 2017 04:47
category: "Discussion"
author: Amit Patel
---
Hey **+Eric Lien** could you help me find the right part number for the the lead screws, seems MTSBRB12-425-S46-Q8-C3-J0 is no longer valid.





**Amit Patel**

---
---
**Eric Lien** *January 11, 2017 05:13*

I will look into it. But to be honest I would likely go with the GolMart ballscrews now. The price is cheaper and Quality is very good.


---
**Amit Patel** *January 11, 2017 06:16*

**+Eric Lien** so I should just go with these 

[aliexpress.com - Aliexpress.com : Buy Best quality 1pc Ball screw SFU1204   L425mm + 1pc RM1204 Ballscrew Ballnut for 12mm screw rail from Reliable best process suppliers on golmart](https://www.aliexpress.com/store/product/Best-quality-1pc-Ball-screw-SFU1204-L425mm-1pc-RM1204-Ballscrew-Ballnut-standard-processing-for-CNC-BK10/920371_32592258963.html)


---
**Eric Lien** *January 11, 2017 06:55*

**+Amit Patel** that is the one for Eustathios correct. 


---
**Eric Lien** *January 11, 2017 07:13*

This is the one for HercuLien: RM1204 Ball Screw L480mm Ballscrew With SFU1204 Single Ballnut For CNC Processing length can be customized

 [aliexpress.com - RM1204 Ball Screw L480mm Ballscrew With SFU1204 Single Ballnut For CNC Processing length can be customized](http://s.aliexpress.com/Rza2QnYZ) 




---
**Stefano Pagani (Stef_FPV)** *January 11, 2017 13:31*

**+Amit Patel** I would also go with the ballscrews. Just make sure you print Walters ball screw clamp.


---
**Benjamin Liedblad** *January 11, 2017 16:52*

**+Stefano Pagani** I estimate I am about one month behind your build. I just finished tightening the frame assembly and installed Z-axis last night. I will be watching the live stream  **+Eric Lien** hosted to help with your alignment before I attempt to install X-Y. 



I also went with the ballscrews and Walter's clamp. I mounted the grease fitting holes pointing out as Walter suggested, but my ballscrews just have holes, no zerk fitting. 



One of my ballscrews has grease pouring out of the hole, the other is clean.



Did your ballscrews come with zerk fittings, did you add them, or just leave the holes open? The hole is M5, so I added a setscrew for the time being to keep grease in.


---
**Stefano Pagani (Stef_FPV)** *January 11, 2017 17:53*

**+Benjamin Liedblad** Mine did not have fittings. I just left them open. I took a few apart (and broke them) and the hole just leads to the threads. Wipe off any excess grease and you should be good.  I topped both off with vasealine when they are getting low, but you will be fine.


---
*Imported from [Google+](https://plus.google.com/100854251935781152705/posts/aafPs2Rnsm4) &mdash; content and formatting may not be reliable*
