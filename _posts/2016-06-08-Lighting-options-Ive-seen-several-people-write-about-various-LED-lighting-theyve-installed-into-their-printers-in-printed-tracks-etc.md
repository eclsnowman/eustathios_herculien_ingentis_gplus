---
layout: post
title: "Lighting options. I've seen several people write about various LED lighting they've installed into their printers in printed tracks etc"
date: June 08, 2016 18:15
category: "Discussion"
author: jerryflyguy
---
Lighting options. I've seen several people write about various LED lighting they've installed into their printers in printed tracks etc. just wondering if anyone here has recommendations on where to source LED's and how they connected them up etc? Is it as easy as bare LED's to which you solder wiring in parallel or can you buy small circuit boards to solder the LED's and the wiring or?  Suggestions appreciated! 





**jerryflyguy**

---
---
**Maxime Favre** *June 08, 2016 18:31*

Depends on space available, voltage, power, heat, etc. Led for what ? Carriage? Printer general lighting ? I use these ones, 2 in serial under 24V: [http://www.dx.com/p/g4-2w-90lm-4000k-1-cob-led-warm-white-light-lamp-white-silvery-grey-dc-12v-339160?utm_rid=80235630&utm_source=affiliate#.V1hjlNl94uU](http://www.dx.com/p/g4-2w-90lm-4000k-1-cob-led-warm-white-light-lamp-white-silvery-grey-dc-12v-339160?utm_rid=80235630&utm_source=affiliate#.V1hjlNl94uU)

On my other printer these under 5V or USB: [http://www.aliexpress.com/item/25CM-5W-Dimmable-25-SMD-5152-Super-Bright-Micro-USB-LED-Strip-Lights-DC-5V/32502310731.html](http://www.aliexpress.com/item/25CM-5W-Dimmable-25-SMD-5152-Super-Bright-Micro-USB-LED-Strip-Lights-DC-5V/32502310731.html)


---
**Tomek Brzezinski** *June 08, 2016 18:53*

Buy ribbon cable LEDS of white type I prefer white-white not warm-white, for industrial applications.  Ebay best source for the quantities we're talking about (imo.)  Adafruit has an excellent selection as well, at a premium.



Get it preconfigured for the voltage of your printer (if it's either 24V or 12V) and it'll be easy enough to find. Even 5V can be found. Other voltages are trickier but you could always wire in series a cheap DCDC board.


---
**jerryflyguy** *June 08, 2016 19:06*

**+Maxime Favre** just general lighting to start. My system is 24v so that's preferred if possible. I'm just looking for a bright interior, I plan on adding smoked acrylic panels to enclose the unit. I'm thinking the LED's would be placed on the three front 2020 sections (top cross bar and side vertical bars). Not sure if I'd go as far as board controlled or just a simple rocker switch, possibly two brightness levels may be a possibility as well) I can see having to add a 5 or 12v PS to run them independent of the 24v meanwell main PS.


---
**Maxime Favre** *June 08, 2016 19:27*

24V is easier if you find the leds you want.

There's not much place under the front top bar, belt is about 10mm under. Front verticals is a good idea.

You can go cheap 24V led strip. It would be nice if you can slide them in the profiles (if the light spreads enough).

Or nice casing like these ones:[http://www.aliexpress.com/item/2015-new-3w-surface-flat-aluminum-30cm-long-led-linear-bar-boat-battery-light-9-5/1998162421.html?spm=2114.01010208.3.17.IYaA1U&ws_ab_test=searchweb201556_8,searchweb201602_2_10037_10017_507_10032_401_9912,searchweb201603_9&btsid=d433a434-bb01-4eb0-8687-1e446d468bef](http://www.aliexpress.com/item/2015-new-3w-surface-flat-aluminum-30cm-long-led-linear-bar-boat-battery-light-9-5/1998162421.html?spm=2114.01010208.3.17.IYaA1U&ws_ab_test=searchweb201556_8,searchweb201602_2_10037_10017_507_10032_401_9912,searchweb201603_9&btsid=d433a434-bb01-4eb0-8687-1e446d468bef)






---
**Daniel F** *June 08, 2016 21:10*

I used cheap LEDs for cars: [http://www.aliexpress.com/item/2pcs-lot-new-2014-COB-car-light-DRL-led-Daytime-Running-Light-Waterproof-Auto-Lamp-For/1906924872.html](http://www.aliexpress.com/item/2pcs-lot-new-2014-COB-car-light-DRL-led-Daytime-Running-Light-Waterproof-Auto-Lamp-For/1906924872.html)

and fixed them with printed clips: [https://plus.google.com/111479474271942341508/photos/photo/6279489305534867538?pid=6279489305534867538&oid=111479474271942341508](https://plus.google.com/111479474271942341508/photos/photo/6279489305534867538?pid=6279489305534867538&oid=111479474271942341508)

They are rated12V, threfore I wired them serially. Connected them to a spare FET so they can be switched on and off as necessary.

Maybe 2x12W is a bit too much for a printer, half of it would have been bright enough.


---
**Jeff DeMaagd** *June 08, 2016 22:15*

I like LED tape. There is some 24V tape out there, 12V is pretty standard. 2x 12V tapes in series would work for 24V. The main down side of the tape is the adhesive can let go.


---
**jerryflyguy** *June 08, 2016 22:22*

**+Jeff DeMaagd** thanks, never heard of LED's on a tape but it's exactly what I'm looking for! eBay my best source or is there better (Domestic North America) options?


---
**Jeff DeMaagd** *June 08, 2016 22:40*

LEDWarehouse is the name of one company that stocks in the US that offers decent LED tape for a pretty good price. I think they're from New Jersey. They sell on eBay and Amazon, plus their own site, I think.


---
**jerryflyguy** *June 09, 2016 12:36*

Turns out Amazon was the cheapest option I could find, esp if you're a Prime member, ~$15 for 5m of brite white (60/m) LED strip. 


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/Yna6ouinXSu) &mdash; content and formatting may not be reliable*
