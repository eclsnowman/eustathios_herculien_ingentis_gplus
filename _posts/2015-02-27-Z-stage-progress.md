---
layout: post
title: "Z stage progress!"
date: February 27, 2015 20:28
category: "Deviations from Norm"
author: Tim Rastall
---
Z stage progress! 


**Video content missing for image https://lh3.googleusercontent.com/-gvHwC-0C19Q/VPDTU9XG5gI/AAAAAAAAWdE/l22ZxIEPYb8/s0/VID_20150228_092446.mp4.gif**
![images/1ce13bd5705946fb22417c56d7cb855c.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1ce13bd5705946fb22417c56d7cb855c.gif)



**Tim Rastall**

---
---
**Lynn Roth** *February 27, 2015 20:41*

Looking good.


---
**James Rivera** *February 27, 2015 20:58*

That looks pretty sweet! And judging from the single finger lift, a single motor should be able to drive it. Bravo!


---
**Tim Rastall** *February 28, 2015 03:57*

**+Ashley Webster** ah,  well that might happen eventually but I've been making it up as I go along so far :). Look Ma,  no plans!


---
**Tim Rastall** *February 28, 2015 04:00*

**+James Rivera** I'm planning on 2 motors as there's 2 sheets of di-bond,  a sheet of aluminum and a sheet of glass to go on there.  Have a geared nema 17 on order too. 


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/Tk9jKFGZi9C) &mdash; content and formatting may not be reliable*
