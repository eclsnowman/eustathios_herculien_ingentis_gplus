---
layout: post
title: "It's late and I'm tired. But tomorrow will likely be the day HercuLien makes its first movements"
date: August 22, 2014 08:08
category: "Show and Tell"
author: Eric Lien
---
It's late and I'm tired.  But tomorrow will likely be the day HercuLien makes its first movements. 

![images/e171f3cb417f6eb4411de70c87b9d6a1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e171f3cb417f6eb4411de70c87b9d6a1.jpeg)



**Eric Lien**

---
---
**Maxim Melcher** *August 22, 2014 08:12*

Please,  more pictures & videos tomorrow ! 


---
**Daniel Fielding** *August 22, 2014 09:24*

So neat


---
**Ricardo de Sena** *August 22, 2014 11:53*

I'm curious about your printer and beautiful work.


---
**Jean-Francois Couture** *August 22, 2014 14:10*

I like the tie-in with the Pi. What software are you going to install on it to control the board ?


---
**Øystein Krog** *August 22, 2014 14:23*

Impressive indeed.


---
**Eric Lien** *August 22, 2014 15:40*

**+Shauki Bagdadi** Found it for 2$ at a surplus store. It is a nice Hoffman box, I felt like I was stealing it at that price.


---
**Eric Lien** *August 22, 2014 18:28*

**+Jean-Francois Couture** octoprint. 


---
**James Rivera** *August 22, 2014 23:12*

**+Eric Lien** Do you plan on using standard 1/16 microstepping, or 1/8 or 1/32?  Just curious and wondering how you plan to run this magnificent beast! :)


---
**Eric Lien** *August 22, 2014 23:49*

**+James Rivera** right now it's running 1/16th. But I may up it to 1/32. These large robotdigg steppers have tons of torque to spare. 


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/SyDwCD6CGjZ) &mdash; content and formatting may not be reliable*
