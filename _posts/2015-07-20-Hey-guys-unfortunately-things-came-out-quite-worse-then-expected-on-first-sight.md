---
layout: post
title: "Hey guys, unfortunately things came out quite worse then expected on first sight"
date: July 20, 2015 15:06
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Hey guys,



unfortunately things came out quite worse then expected on first sight. While the printer ran quite well at the beginning i noticed jumps on the Y axis after some layers of printing. After investigating a bit i noticed that the friction on Y was quite high, so I invested some time to find out why and re-check geometry and everything.



Turns out a small error somehwere on the hotend carriage seems to cause this problem. Most probably you wouldn't have noticed this on your versions with the self aligning bushings but it turned out to be <i>really</i> nasty with the robotdigg graphite bushings. Problem is that they are 60mm long and even though they are covered with a thing sleeve printed of flex filament they don't forgive too much of an angular error.



Currently the whole X/Y part is unmounted again and i'm thinking about possible next steps. I still don't know if the error comes from a not-so-good print of the carriage (don't think so as this print was really quite perfect) or probably some printing errorness on the flex sleeves. I thought about glueing in the bushings with a glue that dries to a foamy structure (like gorilla glue). This should make it a perfect fit when hardened "in position" - unfortunately i won't get this stuff out again if it fails :)



I also thought about using ball bearings in the center - these are much more forgiving regarding errors and much more insensible. Another alternative of course would be to reprint the whole carriage and use self aligning bushings. Unfortunately they're hard to source in Europe. Haven't found any suppliers yet.



That's it for now - just wanted to give you an update on my frustration level :)





**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *July 20, 2015 16:45*

I would try LM8LUU on the center carriage as a next step if you plan on a change. They are easy to obtain, and should serve you well. Bearings will have a lower friction, but higher noise.



For alignment it can be tricky. I have never had an issue. But it takes some finesse. Once thing to keep in mind is tolerances. all these printed parts are imperfect dimensionally, no matter how well tuned your printer is. So you have to adjust alignment based on the reality of the printed parts, not the ideal location based on CAD. So for example it is best to have your cross rods paralleled to the side rods. But if that binds, and putting the rods just slightly out of perfect makes it move smooth, then that is what needs to be done. 



Wish you the best in getting things tuned, it take some time, but it is worth it.


---
**Frank “Helmi” Helmschrott** *July 20, 2015 16:50*

Thanks, **+Eric Lien** - sure LM8(L)UU are a thing to think about but they are thicker in OD so i would have to make bigger changes to the carriage as the whole already interfere. I just found some shorter bronze bushings - will try them with sleeves on - just to give them a try.



Wonder how things are running on **+Walter Hsiao** 's printer.


---
**Eric Lien** *July 20, 2015 17:00*

**+Frank Helmschrott** I was wondering the same thing. I would love to see some prints from Walter's machine. He is such a perfectionist, I would love to see what someone with his attention to detail can do with this platform.


---
**Frank “Helmi” Helmschrott** *July 20, 2015 18:51*

I tweaked around a bit more and found out that it works quite okay if i allow it to misalign. The failure isn't too big but i still don't know where it comes from - i'll have to spend a bit more time with it. Also i'm not sure if this makes me happy on the long run. Probably i'll end up using ball bearings in the center instead but I'd really like to know where the "problem" comes from first.



I have som suspicion that the outer carriages could be a problem. They were under the first prints i made and they were far from perfect. Also i'm not sure if **+Walter Hsiao** 's modifications for the belt tensioning are good for alignment. Would really love to hear his experiences with all that stuff.



There's still an option to cnc mill some parts if there's room for improvement.


---
**Eric Lien** *July 20, 2015 19:23*

Yeah that is one issue with bushings. If you want a precision fit they have to be tight, but tight bushing fits are sensitive to misalignment. That's why I prefer the two smaller misalignment bushings at the outer edge. They have a smaller contact area, but still hold things at the edge where all the force gets applied anyway. 



Best of luck, and once things break-in... They will get even smoother.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/5fhLcoazgLd) &mdash; content and formatting may not be reliable*
