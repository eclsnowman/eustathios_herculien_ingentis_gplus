---
layout: post
title: "Hi, First post from me here, I'm trying to get my head around a build and have gone through the STLs and the BOM, as well as a number of posts here"
date: February 13, 2015 07:49
category: "Discussion"
author: Oliver Seiler
---
Hi,



First post from me here, I'm trying to get my head around a #Eustathios build and have gone through the STLs and the BOM, as well as a number of posts here. Is there any more information available on the build or do I need to pick it all up from there?

I've got a working printer available and lots of PLA. What settings would people recommend printing the Eustathios parts (infill/permieters/layer height) ?



Cheers





**Oliver Seiler**

---
---
**Jim Squirrel** *February 13, 2015 07:55*

For all my parts ive done 3 perimeters 25% infill and .3 layer height and abs all around except where heat wouldnt be an issue. Mainly for vapor smoothing and heat resistance for enclosure 


---
**Daniel F** *February 13, 2015 09:31*

I'm also thinking about building an Eustathios. For parts sourcing it depends on your location. For some parts it makes no sense to get them shipped from far away, others are difficult to get (MISUMI) in Europe. My plan is to use lead screws/Nuts and linear shafts from Europe ([http://www.dold-mechatronik.de/](http://www.dold-mechatronik.de/)). Same for Aluminium extrusions ([http://www.motedis.com](http://www.motedis.com)). Aluminium build plate from here: [https://www.aluminium-online-shop.de](https://www.aluminium-online-shop.de)

Electronics, Steppers, belt and pulleys are much cheaper if you order them from china (robotdigg, ali or ebay).

The only two parts I'm hesitating are the closed loop belts and the bronce bushings. I coudn't find self aligning bushings in china or europe and the ones on the BOM are rather expensive (and there will be customs and shipping as well). There are some bushings with graphite (search for JDB 081230), but they are not self aligning so I'm not sure how difficult it would be to allign them.

For the belts I'm not sure if I should try to use just one size (976mm, 488 teeth, robotdigg), which is the closest lenght to the 3 different size ones on the BOM. 


---
**Tim Rastall** *February 14, 2015 18:55*

**+Oliver Seiler**​ let me know if you hit a dead end and we can work through it over a coffee during the week. 


---
**Oliver Seiler** *February 15, 2015 07:56*

**+Daniel F**  **+Tim Rastall**  Thanks. I had completely overlooked the skp file in the repository - now everything makes much more sense B-)


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/SzTRCgz6YYv) &mdash; content and formatting may not be reliable*
