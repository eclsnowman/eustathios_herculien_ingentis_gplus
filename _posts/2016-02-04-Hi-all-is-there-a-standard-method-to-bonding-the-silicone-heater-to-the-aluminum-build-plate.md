---
layout: post
title: "Hi all is there a standard method to bonding the silicone heater to the aluminum build plate?"
date: February 04, 2016 04:20
category: "Discussion"
author: Kevin Conner
---
Hi all



is there a standard method to bonding the silicone heater to the aluminum build plate? I haven't found that in either instructions. 



thanks!





**Kevin Conner**

---
---
**Eric Lien** *February 04, 2016 04:33*

If you got the one from Alirubber than it should have come with 3m 468mp tape on it if you ordered it correctly. If not you can get some of that tape, or use high temp silicone to bond it to the aluminum.


---
**Tomek Brzezinski** *February 04, 2016 04:45*

Has any tried high temp silicone or silicone based thermal glue to bond the pad to the alu? [basically asking Eric if you've tried it or are just suggesting.] I have always only used pressure (backing plate) or 3M adhesive on my printers, but am curious if the high temp silicone or silicone glues will adhere at all to the silicone heater pad. 


---
**Eric Lien** *February 04, 2016 05:20*

**+Tomek Brzezinski**  I did on my first printer where I forgot the adhesive tape option. It works fine, but 3m 468mp is better and has no cure in period that stinks like silicone.


---
**Jim Stone** *February 04, 2016 05:31*

+1 on the 3m adhesive


---
**Kevin Conner** *February 04, 2016 05:35*

looks like I'll be ordering a roll of 468mp!  Thanks everyone


---
**Eric Lien** *February 04, 2016 06:01*

I found good prices here: [http://www.zoro.com/search?q=468mp](http://www.zoro.com/search?q=468mp)


---
**Mikael Sjöberg** *February 04, 2016 07:47*

[http://m.ebay.com/itm/291519172795?_mwBanner=1](http://m.ebay.com/itm/291519172795?_mwBanner=1)


---
*Imported from [Google+](https://plus.google.com/+KevinConner/posts/a5NXCDxNMuX) &mdash; content and formatting may not be reliable*
