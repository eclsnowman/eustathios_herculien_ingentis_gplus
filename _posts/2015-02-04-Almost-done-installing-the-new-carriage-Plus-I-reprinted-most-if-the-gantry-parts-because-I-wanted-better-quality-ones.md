---
layout: post
title: "Almost done installing the new carriage. Plus I reprinted most if the gantry parts because I wanted better quality ones"
date: February 04, 2015 08:37
category: "Show and Tell"
author: Eric Lien
---
Almost done installing the new carriage. Plus I reprinted most if the gantry parts because I wanted better quality ones. The old ones were printed on my old corexy. 



Also I included at the end of the album my last print before the upgrade (fun toy for the kids.



![images/b2545a006ca5127360067d78e8b09bef.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b2545a006ca5127360067d78e8b09bef.jpeg)
![images/09fcf330eee6189f36d111ed191b40d3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/09fcf330eee6189f36d111ed191b40d3.jpeg)
![images/33b8390af1f2ee593748142691b522ef.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/33b8390af1f2ee593748142691b522ef.jpeg)
![images/e97dc4215cab2cb9865393bd2334f77e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e97dc4215cab2cb9865393bd2334f77e.jpeg)
![images/8c99ecf7d70aa7833eb04af2cdf438ad.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8c99ecf7d70aa7833eb04af2cdf438ad.jpeg)
![images/146e43f2cd4ef8748d20d196f6d781b3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/146e43f2cd4ef8748d20d196f6d781b3.jpeg)
![images/c94d93240fed1fb76bd8c7ff9447f570.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c94d93240fed1fb76bd8c7ff9447f570.jpeg)

**Eric Lien**

---
---
**Jason Smith (Birds Of Paradise FPV)** *February 04, 2015 11:24*

Man, I love the new carriage. 


---
**Dat Chu** *February 04, 2015 11:58*

Make me wish we can hot swap. 


---
**Jean-Francois Couture** *February 04, 2015 12:54*

**+Eric Lien** Can you see the first layers when printing ? I made shrould on my prusa i3 that covers all the way around and can not see it fir the first couple layers.. had to take it off..


---
**Eric Lien** *February 04, 2015 13:49*

**+Jean-Francois Couture** the shroud stops at the bottom of the heat block. I should be able to see from the side.


---
**Robert Cicetti** *February 04, 2015 19:17*

**+Eric Lien**, looking good. Love seeing continued support for a printer like this. I would love to start building one. What is the estimated the total cost for all parts?


---
**Isaac Arciaga** *February 04, 2015 20:07*

**+Eric Lien** How's the fan shroud working out in terms of air flow? Is that the 40mm blower from MTW?


---
**Eric Lien** *February 04, 2015 20:23*

**+Isaac Arciaga**​ I got a four pack from amazon for $18 shipped. It is just a 4020 blower. I use 24v. But mounting holes I believe are common across manufactures.﻿


---
**Eric Lien** *February 05, 2015 12:00*

**+Oliver Schönrock**​ I have never seen a reason to switch. The bronze is silent compared to ball bearings. Self align. Support the carriage at the outer most edges. Require no maintenance or lubrication.



I am sure bearings have a lower coefficient of friction, but the bushings have worked well for me so far.






---
**Isaac Arciaga** *February 05, 2015 20:37*

**+Eric Lien** are there other parts you have changed from the original BoM with your Eustathios variant aside from the 8mm carriage bushings? I am building your variant but followed Jason's BoM.


---
**Florian Schütte** *February 05, 2015 22:29*

**+Dat Chu** is right. Hot swapping carriage would be nice, **+Eric Lien** . Also some modifications to make the head modular (eg. install spindle or laser). My last whish is, that there will be files for linear bearings and sliding bearings (eg. igus).


---
**Isaac Arciaga** *February 05, 2015 22:38*

**+Florian Schütte** which LM8UU bearing do you have and which Igus bearing? It's a quick edit if you would like me to do it for you.


---
**Eric Lien** *February 05, 2015 22:56*

**+Isaac Arciaga** my stl's should be able to be a drop in replacement for the ones by **+Jason Smith**​. The difference would be my new carriage which also requires my new bed supports. I haven't finalized that design yet to push to github. If you want those for now see my G+ post and google drive link.


---
**Eric Lien** *February 05, 2015 22:59*

**+Florian Schütte**​ Jason made a modified version of my carriage designed for linear bearings. I am also working on modifying the duct for him so it could be used on a volcano (longer due to the vertical heat block).



But changing that blew up my solidworks mates so I haven't got that done yet.﻿


---
**Isaac Arciaga** *February 06, 2015 00:37*

Awesome. Looking forward to the V4 carriage for the linear bearings. Will it be pushed out soon?


---
**Eric Lien** *February 06, 2015 01:14*

Soon is a relative term. If you want it right now maybe hit up jason. I have a lot of irons in the fire right now between work and family.


---
**Eric Lien** *February 06, 2015 14:39*

Here is the file Jason showed me: [https://drive.google.com/file/d/0B2629YCI5h_wVlBUVmJ2cG5pUDQ/view?usp=sharing](https://drive.google.com/file/d/0B2629YCI5h_wVlBUVmJ2cG5pUDQ/view?usp=sharing)


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/Xud3VUtVUu4) &mdash; content and formatting may not be reliable*
