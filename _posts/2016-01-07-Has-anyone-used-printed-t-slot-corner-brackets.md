---
layout: post
title: "Has anyone used printed t-slot corner brackets?"
date: January 07, 2016 12:07
category: "Deviations from Norm"
author: Liam Jackson
---
Has anyone used printed t-slot corner brackets? Or know of a beefier printed corner for the extrusion? 



I want to convert my MendelMax to a Eustathios-style printer as cheaply as possible. I know I'll need some more GT2 and 6 pulleys but that should be about it. I might try printed pulleys and see if they are as bad as I think they might be. 



I plan to use printed sliders (possibly with bearings) running on extrusion for the Z rather than buying more smooth rod.





**Liam Jackson**

---
---
**Michaël Memeteau** *January 07, 2016 13:33*

I've been designing some parts that could serve as a base for printed slider on extrusion. I was thinking of using this approach myself but ended up using v wheel. The stepper motor support for my quadrap are printed and quite beefy. Check it out here if you want:

* [https://cad.onshape.com/documents/33a1a8c5a7134dea867c5369/w/2bccfb4697ae4ea7920ae601/e/1ede91b2b3ba49c2a8af61d8](https://cad.onshape.com/documents/33a1a8c5a7134dea867c5369/w/2bccfb4697ae4ea7920ae601/e/1ede91b2b3ba49c2a8af61d8)


---
**Liam Jackson** *January 07, 2016 13:41*

**+Michaël Memeteau** thanks I'll check that out! 


---
*Imported from [Google+](https://plus.google.com/+LiamJackson/posts/dbDftvykj3y) &mdash; content and formatting may not be reliable*
