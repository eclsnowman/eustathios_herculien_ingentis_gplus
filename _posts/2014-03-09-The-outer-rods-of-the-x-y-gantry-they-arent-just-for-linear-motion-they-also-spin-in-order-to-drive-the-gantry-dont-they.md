---
layout: post
title: "The outer rods of the x / y gantry, they aren't just for linear motion, they also spin in order to drive the gantry, don't they..."
date: March 09, 2014 15:32
category: "Discussion"
author: Jarred Baines
---
The outer rods of the x / y gantry, they aren't just for linear motion, they also spin in order to drive the gantry, don't they...



Is that the whole reason for bushes vs bearings? I was looking at using linear bearings, designing a mockup printhead and gantry in inventor, then I came to put the drive motor on it and thought "..... wait... I can't use linear bearings because the shafts need to spin :-/ "



What is the reason for going to a geared drive on the x and y too? I see in the real-life pic on youmagine it looks like **+Tim Rastall** has the steppers directly coupled and in the rendered pic (the "profile pic" for this community) the thing is geared drive?





**Jarred Baines**

---
---
**David Heddle** *March 09, 2014 16:02*

Yes the rods spin linear bearings won't like that. If you look at **+Tim Rastall** profile there are a good few videos of the first version in action. The geared drive was the first version released, he has moved to a direct drive design now. The gears are 45 teeth herring bone gears. I think the reason for the change was backlash and noise. 


---
**D Rob** *March 09, 2014 17:59*

Misumiusa has some linear bearings made for spin also


---
**Eric Lien** *March 09, 2014 19:21*

Also the frelon PBC linear bearings on ceramic coated rods are capable of translation plus rotation at the same time. Also they are groved to accept orings on the bearing OD making them misalignment bearings. [http://www.ebay.com/itm/NEW-BOX-PBC-LINEAR-FL08-STANDARD-BEARING-BORE-1-2-PACIFIC-BEARING-CO-/400416690092](http://www.ebay.com/itm/NEW-BOX-PBC-LINEAR-FL08-STANDARD-BEARING-BORE-1-2-PACIFIC-BEARING-CO-/400416690092)


---
**Jarred Baines** *March 09, 2014 22:48*

WTF!

An o-ring to make them self aligning? doesn't that make them "kinda-almost-linear bearings"? I guess if it's not the O-ring 'giving' it's the carriage itself though isn't it (in the case of minor misalignment)... something to think about there...



I would like to stick to chrome / ground linear rods because, it's what I know and I can modify / purchase them through work... so the bronze bushings are probably a safe bet.



Or "linear bearings made for spin also"... I don't know...



Would it benefit the design (if cost was not an issue) to have linear rods for linear movement with a separate drive rod (perhaps above or below)?



Also, what do you guys know about DCG / Driven at Centre of Gravity



I am very interested in industrial CNC machinery, and DMG / Mazak have machines that do stuff so fast and accurate NOTHING (TBH - not much) compares.



They use (among other things) "Driven at Centre of Gravity" approach as well as "box in box" design (the dual-gantry is better for our application) - These (were, haven't looked them up for a while) $500,000 machines, they put a LOT of research into them and I have learnt a lot just looking at their design:



They also commonly use 2 direct drive motors instead of 1 to move a (leadscrew) axis. No backlash, good power transmission and overall a LOT smoother than 1 direct drive or 2 belt driven / geared drives.



Highly recommend reading up on the theory of DCG if you hadn't already encountered / considered it :



This may help



[http://www.moldmakingtechnology.com/articles/what-driven-at-the-center-of-gravity-means-to-your-machine-tool](http://www.moldmakingtechnology.com/articles/what-driven-at-the-center-of-gravity-means-to-your-machine-tool)


---
**Eric Lien** *March 10, 2014 00:43*

**+Jarred Baines** oring is just beyond the bearing profile and in 2 locations per bearing. They are Very tight tolerance to the matched shaft so if you run 2 bearings per rod I could never print anything with tight enough tolerance and warp free in abs to avoid binding between the bearings. But with the Orings to allow just a bit of give they run like rock stars. 



I think with machined aluminum carriages you could ditch the orings because tolerance would be better.



I like the aluminum shafts because 1/2" x 24"L shafts would be considerably more weight on my big printer.


---
**Jarred Baines** *March 10, 2014 02:39*

It would actually dampen vibration a bit too I suppose - probably not a bad system then!


---
*Imported from [Google+](https://plus.google.com/+JarredBaines/posts/NRGJSbc4usq) &mdash; content and formatting may not be reliable*
