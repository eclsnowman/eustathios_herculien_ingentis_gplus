---
layout: post
title: "Well I guess I didn't screw up too bad then :) Tom is brutally honest, which I like"
date: March 22, 2016 21:39
category: "Discussion"
author: Eric Lien
---
Well I guess I didn't screw up too bad then :) 



Tom is brutally honest, which I like. So if it gets his seal of approval I think I will have a little celebratory beer tonight.



Thanks for the plug **+Thomas Sanladerer** to the github and our G+ community. Something tells me we will see a spike on the github traffic and community invite requests.



Here is the video for those that don't already have Toms videos notify them about new uploads... although if you don't have Tom on your youtube subscribe list already you really need to get yourself checked :)




{% include youtubePlayer.html id=E5FqrycYL40 %}
[https://www.youtube.com/watch?v=E5FqrycYL40](https://www.youtube.com/watch?v=E5FqrycYL40)

![images/1d4a268e825990b73b814a24c01ce728.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1d4a268e825990b73b814a24c01ce728.png)



**Eric Lien**

---
---
**Eric Lien** *March 22, 2016 22:25*

Btw he was only referencing the aluminum frame style as Superior. Not that my printer was Superior. But I will take the win none-the-less :)


---
**Jim Stone** *March 23, 2016 02:12*

Have a Canadian beer too. less watered down ;)


---
**Jeff DeMaagd** *March 23, 2016 13:05*

I've been a fan of the external box frame idea for some time. I think I've figured out how to simplify assembling one but I have other projects to do first.


---
**Eric Lien** *March 23, 2016 13:44*

**+Jeff DeMaagd** I think the prize for "Mechanically Superior" goes to your printer though. That frame looked like you could have parked a car on it.



For those who haven't seen it:

[https://m.flickr.com/#/photos/jabella/25812195362/in/set-72157665553307120/](https://m.flickr.com/#/photos/jabella/25812195362/in/set-72157665553307120/)

[https://m.flickr.com/#/photos/jabella/25932975335/in/set-72157665553307120/](https://m.flickr.com/#/photos/jabella/25932975335/in/set-72157665553307120/)



And this is the print quality he is achieving:

[https://m.flickr.com/#/photos/jabella/25610476260/in/set-72157665553307120/](https://m.flickr.com/#/photos/jabella/25610476260/in/set-72157665553307120/)

[https://m.flickr.com/#/photos/jabella/25300239834/in/set-72157665553307120/](https://m.flickr.com/#/photos/jabella/25300239834/in/set-72157665553307120/)



The prints were truly stunning, not just for a printer this size, but for any printer. The layer stacking was perfect. 


---
**Jeff DeMaagd** *March 23, 2016 14:05*

Wow, thanks for the compliments. I am so very happy with how the machine came out. It drew a little blood, lots of sweat and maybe some tears I'm blanking out getting there. I only started getting the good parts coming off it three weeks ago.


---
**Jeff DeMaagd** *March 23, 2016 14:06*

I don't know if I've said this before, but the Herculien was one of my inspirations, structurally and mechanically, if not for other reasons too.


---
**Eric Lien** *March 23, 2016 16:58*

Yup... Traffic saw a nice spike :)



[http://i.imgur.com/jMgnafI.png](http://i.imgur.com/jMgnafI.png)


---
**Vic Catalasan** *March 24, 2016 22:32*

I have built a CNC foam cutter 15 years ago, 3 years ago CNC Mill for cutting aluminum. I must say the Herculien design is pretty awesome and it is about the size I needed. So far no mechanical issues with the printer and I have a ton of hours printing with it.



I do not have a problem building it, however it might be a though build for most folks, a complete kit, ready to assemble would be nice.


---
**Eric Lien** *March 24, 2016 22:41*

**+Vic Catalasan**​ The design is 100% open so anyone is free to do so. It's just between my day job and having two young kids I definitely don't have time to take on another project like selling kits.



Even just somebody being a single source of components could be a large step forward. A while ago someone in Asia was looking at starting to build up kits but I never saw that going anywhere unfortunately.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/AgBSdSFsdaD) &mdash; content and formatting may not be reliable*
