---
layout: post
title: "Wow, this black PETG from ESUN is great to print with"
date: June 17, 2015 11:56
category: "Show and Tell"
author: Eric Lien
---
Wow, this black PETG from ESUN is great to print with. A few rolls showed up on Amazon and I snatched them up. Looks like its back in stock on June 20th. ﻿



Edit: I should have mentioned in the first place, this is the head for Froggy by the ever talented **+Louise Driggers**​ 



Link: [https://www.youmagine.com/designs/froggy-the-3d-printed-ball-jointed-frog-doll](https://www.youmagine.com/designs/froggy-the-3d-printed-ball-jointed-frog-doll)

![images/5ab40cab2c288edfad48c6164f4f2592.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5ab40cab2c288edfad48c6164f4f2592.jpeg)



**Eric Lien**

---
---
**Daniel Fielding** *June 17, 2015 11:59*

Wow. What is the layer height.


---
**Ricardo de Sena** *June 17, 2015 12:11*

Great!!! Which print settings?


---
**Eric Lien** *June 17, 2015 12:56*

0.1920 layer height, 255C @ 45mm/s, 0.5mm extrusion width, 7.5mm retract at 60mm/s, 0.3mm coast to end, no active cooling, 10% infill, 2 perimeters, minimum layer time disabled, 75C bed, manual support placement in simplify3d, extrusion multiplier tuned with the single perimeter cube method, 1.003 scale factor for PETG shrink via 20x20 cube calibration.


---
**Dat Chu** *June 17, 2015 13:18*

Does it come out clean like that? 


---
**Eric Lien** *June 17, 2015 13:41*

**+Dat Chu** Yes, that is 20 seconds after the print completed... still stuck to the bed.


---
**Eric Lien** *June 17, 2015 14:36*

**+Eric LeFort** yes. PETG is essentially T-glass which is known for the wet look. Also like I said in an earlier post the interlayer adhesion is crazy. I cannot tear a single wall 20x20 cube without incredible effort by hand. This stuff is strong.


---
**Isaac Arciaga** *June 17, 2015 17:24*

**+Eric Lien**​ thanks for the heads up on the preorders. I grabbed myself 6 spools lol. Don't you just love how it can look like a lower layer height than you're actually printing at?


---
**Daniel Fielding** *June 17, 2015 19:21*

This really makes me want to import this stuff to Australia. How much does this stuff retail for?


---
**Eric Lien** *June 17, 2015 19:27*

**+Daniel fielding** $29.99 via amazon prime. 


---
**Daniel Fielding** *June 17, 2015 19:28*

And is shipping included?


---
**Eric Lien** *June 17, 2015 19:33*

On Amazon Prime, Yes that includes shipping inside the US.


---
**Daniel Fielding** *June 17, 2015 19:35*

Wow. Thats cheap. I couldn't even make profit at that cost. The power of bulk ordering.


---
**Gus Montoya** *June 18, 2015 00:41*

F-me....Black is sold out :(  

I am trying to get temperature specs on the filament. Anyone? A quick google search didn't yield results.


---
**Daniel Fielding** *June 18, 2015 01:47*

Can you explain the tuning process you used for the single perimeter cube you did with simplify3d. 


---
**Eric Lien** *June 18, 2015 02:46*

**+Daniel fielding** Tell your software the filament is exactly 1.75mm and print a single wall cube with no top fill and a known extrusion width (I use 0.50mm). Once printed measure the wall thickness, is it what you set it at? If not take desired/actual-measured and put that in for your extrusion multiplier (example you measure 0.52mm so 0.5/0.52 = 0.961 extrusion multiplier) . Then reprint and check. Is it correct now? 


---
**Eric Lien** *June 18, 2015 02:55*

**+Daniel fielding** see this video: 
{% include youtubePlayer.html id=cnjE5udkNEA %}
[https://youtu.be/cnjE5udkNEA](https://youtu.be/cnjE5udkNEA)


---
**Daniel Fielding** *June 18, 2015 09:30*

Unfortunately Simplify3D is not producing any thin wall models :(


---
**Eric Lien** *June 18, 2015 10:53*

**+Daniel fielding** Don't use a model with a thin wall. Take a solid 20x20 cube STL, set perimeters to 1, bottoms to 1 or 2, tops to zero, infill to zero. 


---
**Jason Smith (Birds Of Paradise FPV)** *June 18, 2015 11:43*

Man, I thought that little guy was going to save me 15% or more on car insurance...

It's still cool now that I know it's a frog and not a Gecko though.


---
**Daniel Fielding** *July 13, 2015 11:04*

I bit the bullet and got some of this Black PETg direct from esun to trial it, wow. I did find though that if the bed was too hot I would get bubbles in the top surface. I lowered my bed temp to 60 degrees and it was all good. Now I am facing problems with the parts popping off the bed. Was thinking about getting some PET Polyester film instead of the Kapton, has anybody tried it with PETg? Would be interesting to hear some thoughts.


---
**Eric Lien** *July 13, 2015 11:54*

**+Daniel fielding** I have the best luck with a glass bed coated in hairspray (3 wet coats that have cured while the bed is cold). I run at 75C on the bed. It sticks like crazy while hot, but pops right off when the bed cools.



The hairspray I use is Suave Extreme Hold: [http://mobile.walmart.com/ip/10293440](http://mobile.walmart.com/ip/10293440)


---
**Brandon Cramer** *July 13, 2015 20:10*

What temperature did you print this at? I didn't have much luck at 230 degrees.

[https://www.dropbox.com/s/qb40b254j7q7uz1/2015-07-12%2011.00.01.jpg?dl=0](https://www.dropbox.com/s/qb40b254j7q7uz1/2015-07-12%2011.00.01.jpg?dl=0)


---
**Eric Lien** *July 13, 2015 20:52*

**+Brandon Cramer** somewhere around 260. I know I gave settings in an older post.


---
**Eric Lien** *July 13, 2015 21:46*

**+Brandon Cramer** found it: "I was using 60mm/s print speed, outermost perimeters at 60% speed, bed 75C, hotend 255C, retract 7.25mm at 65mm/s. No real blobs or strings. Just a few tiny hairs.﻿"


---
**Brandon Cramer** *July 13, 2015 21:51*

Thanks **+Eric Lien** I currently have a jammed extruder on my Printrbot Simple Metal. Guess when I have a free minute I will have to try to unplug it. Got any recommendations? Seems like PLA feeds down just enough into the cold end of the extruder. Not sure I've experienced this one before.

After that I will definitely need to give this filament another try. Your print looks awesome!!!


---
**Øystein Krog** *July 18, 2015 13:06*

I'm having terrible issues with PETG and overhangs curling:/

I've never run as high as 255C though, do you think that could help?


---
**Eric Lien** *July 18, 2015 13:24*

**+Øystein Krog** higher temps likely won't help.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/hpVRzToEPFo) &mdash; content and formatting may not be reliable*
