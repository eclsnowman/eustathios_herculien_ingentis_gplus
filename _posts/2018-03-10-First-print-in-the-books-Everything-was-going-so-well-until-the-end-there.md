---
layout: post
title: "First print in the books. Everything was going so well until the end there..."
date: March 10, 2018 17:56
category: "Show and Tell"
author: Ryan Fiske
---
First print in the books. Everything was going so well until the end there... not exactly sure what caused the shift as I was not immediately in the area at the time. Will have to check out why. Other than that, I'm really impressed with how nice of a print I'm getting on my first attempt!



Thanks to everyone here who has asked questions and also those who have helped answer mine. Special thanks to **+Eric Lien** and **+Dennis P** for their generous help with printed parts when my initial parts were cracking and failing on me.



Looking forward to continued improvements and tweaking!

![images/b8ae358c35d9836cca63f175871b994c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b8ae358c35d9836cca63f175871b994c.jpeg)



**Ryan Fiske**

---
---
**James Rivera** *March 10, 2018 20:41*

Have you run a break in gcode script yet? It might just be binding a little.


---
**Ryan Fiske** *March 10, 2018 20:52*

**+James Rivera** yes quite a few times before attempting pushing plastic. I thought perhaps that was it as well, but I'm seeing it with the benchy I'm currently printing, but not hearing any noises from binding.


---
**James Rivera** *March 10, 2018 22:02*

Are any of the stepper motors getting hot?


---
**Ryan Fiske** *March 10, 2018 22:21*

**+James Rivera**  nope, they're just warm. Driver chips are a little hot, but not too bad and are pretty open to air right now. I have noticed that all my shifting is relegated to the X axis, so that kinda helps! I went back and ensured all pulleys are tightened up. Trying to decide what is next, I've got a little binding in both axes when pushing my carriage around which is a big disappointing since I thought I was done with all of that. Might need to run around and adjust the bottom two rods again :(


---
**Sébastien Plante** *March 10, 2018 23:20*

look like one motor have skipped step


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/Z5uEKExfUVm) &mdash; content and formatting may not be reliable*
