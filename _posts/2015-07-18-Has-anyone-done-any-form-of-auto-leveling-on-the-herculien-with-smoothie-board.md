---
layout: post
title: "Has anyone done any form of auto leveling on the herculien with smoothie board?"
date: July 18, 2015 04:38
category: "Discussion"
author: Gunnar Meyers
---
Has anyone done any form of auto leveling on the herculien with smoothie board?  I have auto level on the printrbot and although not needed it is sometimes nice. The printrbot was limited to three points due to the printer board and I was wondering if with smoothie you could do like a 9 point level?





**Gunnar Meyers**

---
---
**Frank “Helmi” Helmschrott** *July 18, 2015 05:09*

I haven't done auto levelling with the Smoothieboard but the limitation of 3 point has nothing to do with the electronics AFAIK. Repetier Firmware also does 3-point auto leveling with cartesian printers and that works quite well for me on the ordbot.



Let me know if you start working on a fixed mount for any kind of leveling sensor. I'd be interested in having that, too.


---
**Eric Lien** *July 18, 2015 05:33*

I haven't leveled my printer in 4 months. Perfect first layer every time. IMHO autolevel is a crutch for imprecise mechanics. Although when you dramatically change bed temp or hotend temp you do need to reset level. Aluminum has a high coefficient of thermal expansion. So temp changes do change target distance. But with a fixed carriage sensor auto bed level won't account for that anyway (in regards to hot end growth). Only bed growth.﻿


---
**Gunnar Meyers** *July 18, 2015 19:03*

I was hoping to have some insight on limitations of the smoothie. Also the mic 6 aluminum I'm using for he bed is incredibly flat. I possibly just wanted auto level for the new users at the makers space who do not understand the entire leveling/tramming process. 


---
**Eric Lien** *July 18, 2015 19:55*

**+Gunnar Meyers** good point. It is helpful for new users.


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/3hEnpGkzFUR) &mdash; content and formatting may not be reliable*
