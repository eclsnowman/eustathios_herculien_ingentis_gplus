---
layout: post
title: "First of the many orders for a Herculein build has arrived"
date: February 09, 2015 03:30
category: "Show and Tell"
author: Bruce Lunde
---
First of the many orders for a Herculein build has arrived. Probably a month before I get them all, but exciting none the less! I am building with one extruder, E3D Volcano 1.75 Eruption. Thanks to Filastruder for a fast delivery!

![images/b6f9100499b0fc9cc0d625dcec369cc3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b6f9100499b0fc9cc0d625dcec369cc3.jpeg)



**Bruce Lunde**

---
---
**Dat Chu** *February 09, 2015 03:38*

Sounds awesome Bruce. Now that you are on the list to get the Smoothieboard, it will only be a short time before more stuff are in place. I still need to get more electronics (heated bed, bed platform, ...) :( Lots of things to buy.


---
**Eric Lien** *February 09, 2015 06:25*

So happy to see another one coming together.



**+Alex Lee**​​ this photo should help you audit what's needed: [https://raw.githubusercontent.com/eclsnowman/HercuLien/master/Photos/Picture%20of%20all%20Printed%20Parts.jpg](https://raw.githubusercontent.com/eclsnowman/HercuLien/master/Photos/Picture%20of%20all%20Printed%20Parts.jpg)



**+Bruce Lunde**​, I was reminded by someone recently that between the robotdigg 32tooth pullies and the corner bracket bearings that a small shim washer is needed or the pulley will rub on the bearing. They are fairly cheap, but forgetting them will cause drag. Something like this would work: 10mmx16mmx0.5mm_Shim(McMaster_90214A422)﻿


---
**Eric Lien** *February 09, 2015 06:29*

Also the heated bed mat had the power cable offset 30mm to one side to avoid a screw hole at the back. If your mat was ordered with the lead centered just move the back screw hole and cable outlet pocket in the MDF slightly. Here was the drawing I sent alirubber: [https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Silicone%20Heater%20Thermistor%20and%20Cable%20Locations%20for%20Alirubber.jpg?raw=true](https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Silicone%20Heater%20Thermistor%20and%20Cable%20Locations%20for%20Alirubber.jpg?raw=true)﻿


---
**Eric Lien** *February 09, 2015 06:44*

**+Alex Lee**​ they are drill start guides for drilling the extrusions. I hate measuring. Just bolt them to the extrusion, start the hole to establish centers, then pull off the guides and finish the holes by hand with a cordless drill.



Top one is for the 20x80 extrusions on the sides of the frame to access the allen heads of the m5x10mm button heads that hold the horizontal cross support for the z-belt drive as well as the vertical vslot for z guides.



Middle one gives hole centers where the 10mm rods come out through the  20x80 frame for the knobs and drive pulleys.



Last one is for the 20x20 verticals for bolting into the corners. This allows you to access the allen heads of the m5x10mm button head bolts.


---
**Eric Lien** *February 09, 2015 06:49*

[http://imgur.com/9xvRiD3](http://imgur.com/9xvRiD3)

[http://imgur.com/9hqMmwA](http://imgur.com/9hqMmwA)

[http://imgur.com/sDkb0vh](http://imgur.com/sDkb0vh)

[http://imgur.com/O4N58s7](http://imgur.com/O4N58s7)

﻿

Some of the m5 bolts go into tapped holes in the extrusion. Some go into t-nuts dropped into the extrusions.


---
**Eric Lien** *February 09, 2015 06:58*

If a guy had a Bridgeport they wouldn't be needed. But all I have is a Ryobi ;)


---
**Eric Lien** *February 09, 2015 07:19*

**+Bruce Lunde**  , also note the vertical V-Slot guides are not centered in the frame. They are offset since the hot ends hang off the front of the carriage. Hence these vertical guides are forward to center the bed in the travel area of the nozzle:



[http://i.imgur.com/51MYrky.png](http://i.imgur.com/51MYrky.png)

[http://i.imgur.com/NXad7p5.png](http://i.imgur.com/NXad7p5.png)


---
**Bruce Lunde** *February 10, 2015 01:35*

**+Eric Lien**  Thanks for all this information, I have copied this post to my notes pages! **+Alex Lee** Excellent news!!


---
**Eric Lien** *February 10, 2015 01:38*

Hopefully you can help me make it cohesive and collected together. It is great having you on Team HercuLien!


---
**Bruce Lunde** *February 10, 2015 01:41*

**+Dat Chu** Excited about the smoothie board!


---
**Dat Chu** *February 10, 2015 02:10*

**+Bruce Lunde** you and I both :)


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/iojfxUeG4Bx) &mdash; content and formatting may not be reliable*
