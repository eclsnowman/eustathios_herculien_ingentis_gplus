---
layout: post
title: "On HercuLien, For the feed from the side mounted filament feed unit out to the carriage, how did you decide on a length for the tubing?"
date: January 24, 2016 16:52
category: "Discussion"
author: Bruce Lunde
---
On HercuLien, For the feed from the side mounted filament feed unit out to the carriage, how did you decide on a length for the tubing?  What are the lengths you all are using?





**Bruce Lunde**

---
---
**Zane Baird** *January 24, 2016 17:22*

anywhere in the 950-1050mm range I would imagine. My current tubing is around 1050mm, but I could go shorter. You want it as short as possible while still maintaining a gentle arc when the carriage is positioned at back left corner.


---
**Eric Lien** *January 24, 2016 18:09*

**+Zane Baird** hit the nail on the head.


---
**Bruce Lunde** *January 24, 2016 18:15*

Thanks, I will proceed with that length!


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/Z6b3mAZ9QZS) &mdash; content and formatting may not be reliable*
