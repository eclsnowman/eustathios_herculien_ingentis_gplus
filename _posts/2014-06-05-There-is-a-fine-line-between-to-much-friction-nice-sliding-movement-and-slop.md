---
layout: post
title: "There is a fine line between to much friction, nice sliding movement and slop"
date: June 05, 2014 01:51
category: "Discussion"
author: Wayne Friedt
---
There is a fine line between to much friction, nice sliding movement and slop.





**Wayne Friedt**

---
---
**Tim Rastall** *June 05, 2014 02:18*

Are you talking about printers or something else here? :)


---
**Wayne Friedt** *June 05, 2014 02:26*

Haha, ya printers.



 I was printing some PLA LM8UU bushings so that's where this came from.


---
**Whosa whatsis** *June 05, 2014 04:29*

I would say that "nice, sliding movement" IS a fine line between too much friction and too much slop.


---
**Wayne Friedt** *June 05, 2014 05:03*

You do have that in a better order than i did.     That's why they call me the  " Junk Shop Engineer "


---
*Imported from [Google+](https://plus.google.com/+WayneFriedt/posts/i1nNFoxBXbn) &mdash; content and formatting may not be reliable*
