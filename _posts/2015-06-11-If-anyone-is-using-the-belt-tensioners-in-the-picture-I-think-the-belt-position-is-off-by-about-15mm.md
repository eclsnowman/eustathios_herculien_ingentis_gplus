---
layout: post
title: "If anyone is using the belt tensioners in the picture ( ), I think the belt position is off by about 1.5mm"
date: June 11, 2015 08:28
category: "Discussion"
author: Walter Hsiao
---
If anyone is using the belt tensioners in the picture ([http://www.thingiverse.com/thing:854301](http://www.thingiverse.com/thing:854301)), I think the belt position is off by about 1.5mm. I fixed it and added a new version (1.3), but haven't had a chance to test it yet.  If you printed a set, sorry about that.

![images/3215f98fd3a9d53b7915fe9c4bf2803a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3215f98fd3a9d53b7915fe9c4bf2803a.jpeg)



**Walter Hsiao**

---
---
**Isaac Arciaga** *June 11, 2015 09:22*

Doh! :(


---
**Frank “Helmi” Helmschrott** *June 11, 2015 13:20*

Good that i printed the wrong (10mm) ones anyway :-D Did you update the 8mm version, too?


---
**Chris Brent** *June 11, 2015 16:26*

I know you don't recommend that filament, but darn those parts look so good!


---
**Walter Hsiao** *June 11, 2015 17:07*

**+Frank Helmschrott** Yeah, just make sure you use the files that have 8mm and v1-3 in the name.  I left the older version there too for now, but I'll remove them eventually.



**+Chris Brent** Thanks!  So far, the bearing holders and the z-axis clamps cracked, everything else looks fine.  I've been able to replace the bearing holders with the same plastic by printing them flat (instead of upright) but I have a PETG set on standby, just in case.



So, yeah I wouldn't recommend using this plastic for this part, but I'm using it anyway in the reprint :)  For many other things, it's fine, and it comes in 1kg spools now which is nice.


---
*Imported from [Google+](https://plus.google.com/+WalterHsiao/posts/WeNtFJNwXHH) &mdash; content and formatting may not be reliable*
