---
layout: post
title: "Eustathios build getting closer. I am calling it the Eustathios \"Spider\" due to the color scheme and a few design items I am still working on"
date: May 14, 2014 02:30
category: "Show and Tell"
author: Eric Lien
---
Eustathios build getting closer. I am calling it the Eustathios "Spider" due to the color scheme and a few design items I am still working on.



My azteeg x3 v2 came in the mail Monday. Just waiting on my power supply, and the steppers I orders from aliexpress (over a month ago).



Sorry for the blurry shots. Just took a break during assembly and the lighting in the basement is sub-optimal.



![images/e20fdb02a0cdbae1cd7fe7be9c297142.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e20fdb02a0cdbae1cd7fe7be9c297142.jpeg)
![images/3ef8ca57557d2e10cba37f96cf7e9570.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3ef8ca57557d2e10cba37f96cf7e9570.jpeg)

**Eric Lien**

---
---
**Chad Nuxoll** *May 14, 2014 02:34*

Looks good what kind of power supply you going to be using. I'm looking for mine 


---
**Eric Lien** *May 14, 2014 02:40*

Openbuilds 24v 20A.﻿ [http://openbuildspartstore.com/24v-20a-power-supply/](http://openbuildspartstore.com/24v-20a-power-supply/)


---
**Chad Nuxoll** *May 14, 2014 03:28*

May i ask why 24v instead of 12


---
**Eric Lien** *May 14, 2014 03:39*

Better for large/high watt heated bed, and I have been told the steppers run better at 24v.


---
**Chad Nuxoll** *May 14, 2014 03:57*

Humm. Might have to do the same with mine then. This would make it so i wouldn't have to use a SSR for my hotbed. Noob question do you have to buy 24v steppers or do most work on 12v


---
**Eric Lien** *May 14, 2014 04:02*

**+Chad Nuxoll** steppers should have no issue with 24v. Printer control board on the other hand... May need some mods.



I use the Azteeg X3 and it can handle 24V and high current on the heat bed no problem.


---
**Chad Nuxoll** *May 14, 2014 04:12*

Ok, Ya i just looked and the azteeg x5 will work with 24v. Btw keep up the pictures everything on your printer is coming together very nicely :) 


---
**Daniel Fielding** *May 14, 2014 07:47*

Looking good


---
**ThantiK** *May 19, 2014 00:51*

Where'd you get the pulleys that have no set screw shaft on them?  I did the group order that **+D Rob** helped everyone out on, but now I see everyone with these GT2 pulleys that don't have a shaft. :[


---
**Eric Lien** *May 19, 2014 01:12*

**+Anthony Morris** Misumi. They are on the Eustatios BOM on github. But they are 16$ a piece. Got them on the first150 before Robs deal came up.


---
**Gus Montoya** *October 01, 2014 05:09*

Hi Eric



    Are you willing to print a full set of plastic parts for your Lien3D Eustathios Spider? I would like to buy a set from you. I currently have a QU-BD Twoup but it just broke down and it's not repairable (Wooden chassis broke in multiple locations). I've been searching for another platform that I can reuse my parts from the Twoup and your's fits the bill. Can you contact me in regards to this? I'm sure your busy since your a family man (I have little cousins that keep me busy haha) but this help would be greatly appreciated. I can paypal you.




---
**Eric Lien** *October 01, 2014 05:26*

**+Gus Montoya** any chance you want to try #HercuLien instead?


---
**Gus Montoya** *October 01, 2014 22:22*

I will try, but I think the aluminum extrusions are out of my price affordability. I currently found 1.5x1.5 extrusions for really cheap. I beleive at $3.00 per 2 ft.


---
**Gus Montoya** *October 01, 2014 22:23*

If you google chat we can communicate much faster. I'll be free after 7pm pst.


---
**Gus Montoya** *October 01, 2014 22:24*

If you google chat we can communicate much faster. I'll be free after 7pm pst.


---
**Eric Lien** *October 02, 2014 00:17*

Sorry I am on kid duty tonight. Some redesign of the files would be required for the extrusion size changes. Are you able to modify the files?


---
**Gus Montoya** *October 02, 2014 00:35*

I am as fresh of a beginner as you can get. I've watched a lot of videos but am unable to create anything at the moment. What size extrusion are the files made for? I tried opening your files on github, but doe some reason I can't. Not even your pdf file is viewable. Can you create a regular pdf file that does not require 3d? Your build is a 1 ft x 1ft correct?


---
**Eric Lien** *October 02, 2014 01:19*

**+Gus Montoya** the PDF requires actual acrobat, not foxit or others. Freecad can open the step file, and sketch up is also in there. Plus you can get the rhino free trial for step.


---
**Seth Messer** *February 16, 2015 19:45*

**+Eric Lien** did you end up getting a 24v 14a power supply? if so, could you recommend the make/model/etc? thanks!


---
**Eric Lien** *February 16, 2015 21:04*

**+Seth Messer**​ this is what I use on Eustathios with a 400W DC heated bed: [http://openbuildspartstore.com/24v-20a-power-supply/](http://openbuildspartstore.com/24v-20a-power-supply/)



But I you go with an AC heated bed and SSR you could get a substantialy smaller power supply (around 100w would do).


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/NUMRLe9tpkT) &mdash; content and formatting may not be reliable*
