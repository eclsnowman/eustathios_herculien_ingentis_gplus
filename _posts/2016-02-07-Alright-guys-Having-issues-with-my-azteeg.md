---
layout: post
title: "Alright guys. Having issues with my azteeg"
date: February 07, 2016 20:23
category: "Discussion"
author: Jim Stone
---
Alright guys.



Having issues with my azteeg.



well maybe my problem is universal, as it involves the Arduino IDE and Pronterface



When Azteeg X3 connected with the jumper set to usb and marlin upload is attempted. it succeeds



When Azteeg X3 connected with the jumper set to Select ( which uses the internal regulator) and the external PSU is used and marlin upload is attempted. it times out or hangs...or fiales in some way



with azteeg x3 having a succesful write. connect to pronterface with hte jumper set to usb and external power applied.



pronterface can see and echo the board.



but m119 and m114 return NOTHING not even x00 y00 z00

same when jumper set to select with pronterface

motors will not make a clonk noise even.



Vref set to 1v

1.5a max motors with SD8825 Stepper Drivers.

motor wires have been swapped to all possible configurations.



Connecting...

start

Printer is now online.

echo:Marlin1.0.0

echo: Last Updated: Feb  5 2016 21:55:29 | Author: (Eric Lien, Lien3D_HercuLien)

Compiled: Feb  5 2016

echo: Free Memory: 3721  PlannerBufferBytes: 1232

echo:Hardcoded Default Settings Loaded

>>> m114

SENDING:M114

>>> m119

SENDING:M119





Any ideas? this is with the marlin version in the herculien git repository.



i also need to do a new one for the viki2 as the one in the repository doesnt support it. but the viki2 isnt connected yet and im just trying to shake the problems out before i attempt to modify marlin ( yay i have no idea what im doing there lol, if someone could throw a marlin for viki2 that would be great )





**Jim Stone**

---
---
**Eric Lien** *February 07, 2016 22:22*

I wonder if the onboard 5v regulator is toast?


---
**Jim Stone** *February 07, 2016 22:23*

if that were the case would switching to usb power not fix that? im thinking maybe the bootloader is somehow corrupt ( this is similar to what i head to deal with a sainsmart melzi)


---
**Jim Stone** *February 07, 2016 23:32*

I love brainstorming ideas. But I have to hear back from Roy if I can desolder the zener to make sure


---
**Jim Stone** *February 08, 2016 19:09*

new developments.



uploaded the test code from the azteeg site. and everything functions as it should.



tested power fro ma 5v pin to negative. 4.948v



even whilst running the code.



flashed with herculien git marlin. flashed fine.



went into pronterface. connected fine



went to home or do a status on the limit switches i get nothing.


---
**Jim Stone** *February 08, 2016 19:31*

m114 will not even post a single thing neither will m119



ie.



Connecting...

start

Printer is now online.

echo:Marlin1.0.0

echo: Last Updated: Feb  5 2016 21:55:29 | Author: (Eric Lien, Lien3D_HercuLien)

Compiled: Feb  5 2016

echo: Free Memory: 3721  PlannerBufferBytes: 1232

echo:Hardcoded Default Settings Loaded

>>> m114

SENDING:M114

>>> m119

SENDING:M119


---
**Jim Stone** *February 08, 2016 19:35*

yes. by using the home button. sending a g28. etc.



they will not budge and i get no response back from any gcode

or mcode


---
**Jim Stone** *February 08, 2016 19:53*

they were all at the axis end stops. i have also tried with everything at center.



multimeter says continuity when not depressed and circuit break when pressed ( NC switch)


---
**Eric Lien** *February 08, 2016 20:28*

**+Jim Stone** I run normally open on mine. You will need to invert the endstops.


---
**Jim Stone** *February 08, 2016 20:45*

just did the invert. and no dice :( i was hoping that was it.





// The pullups are needed if you directly connect a mechanical endswitch between the signal and ground pins.

const bool X_MIN_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.

const bool Y_MIN_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.

const bool Z_MIN_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.

const bool X_MAX_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.

const bool Y_MAX_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.

const bool Z_MAX_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.



they were true before


---
**Jim Stone** *February 09, 2016 00:16*

New Development.



its something to do with the marlin in the herculien github ( maybe the azteeg is too new or something idk)



but i started to use a new marlin config on it. and started to slowly modifying it to match the one in the github.



so far the new one im doing is working. i can get triggers. and i can get position


---
**Jim Stone** *February 09, 2016 00:44*

Break in gcode seems to be only using half the area. i have the max area setup correctly. perhaps i dont have the steps set. not sure how to do that.



halp?


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/LbNGUDMBYNK) &mdash; content and formatting may not be reliable*
