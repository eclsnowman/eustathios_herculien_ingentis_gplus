---
layout: post
title: "How easy should these slide back and forth?"
date: July 13, 2015 16:00
category: "Discussion"
author: Brandon Cramer
---
How easy should these slide back and forth? I'm concerned that it isn't sliding easy enough. I can slide it with one finger fairly easy, but it definitely doesn't slide as easy as the carriage bearings do. (I will have to confess here, that when I first put the bearings in, I pressed them in gently with the vise) So I'm trying to decide if I need to purchase new ones. I did pop them out and sand the PLA piece to make them fit easier. Should I order new bearings or just go with it? One more question? Why don't we use the LM10UU type bearings here? They seem like they would work a lot better!!

![images/b94f042ea2b3dd7cd20b604e3c54853a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b94f042ea2b3dd7cd20b604e3c54853a.jpeg)



**Brandon Cramer**

---
---
**Erik Scott** *July 13, 2015 16:07*

Chuck some 10 mm rod in a drill and spin it while sliding the piece back and forth. Use lots of oil. This will align the bearings and make things smooth.



These rods rotate in addition to being support rails. LM10UU bearings don't support rotation and so would not work. 


---
**Igor Kolesnik** *July 13, 2015 16:07*

Lm10uu do not like rotations. They were ment for sliding only. lmuu can be used for carriage


---
**Brandon Cramer** *July 13, 2015 16:09*

Thanks **+Erik Scott** I will give that a shot! I bet that will help! Good to know about the LM10UU bearings. :)


---
**Ben Delarre** *July 13, 2015 16:11*

I found when they were bedded in right that they would slide under their own weight. Try putting the shaft in a drill and running it while sliding the carriage up and down. If after a few minutes of that they are still sticky then try knocking the carriage around a bit then try again with the drill, you want the bushings to self align and sometimes they are a little tight on their housings. 


---
**Chris Brent** *July 13, 2015 16:25*

I'd favor these over LMXXUU's now that I found the drill spinning trick to align them. The self aligning thing is huge as it allows for all sorts of errors in printed part to be ignored. The LMU's look like they'd be better but I'm pretty sure these are better in most ways. The contact surface is greater too so they're going to run a lot truer under load. 


---
**Brandon Cramer** *July 13, 2015 16:28*

Sweet! These are now sliding a lot better after running these back and forth with the drill. Thanks for the help!


---
**Brandon Cramer** *July 13, 2015 17:36*

I have it all put back together with rods and carriage. Its sliding all over the place like butter. I feel 110% better about this. :)


---
**Jim Wilson** *July 13, 2015 18:23*

^ yup! Should slide like greased butter.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/BuKn3y7Nb7u) &mdash; content and formatting may not be reliable*
