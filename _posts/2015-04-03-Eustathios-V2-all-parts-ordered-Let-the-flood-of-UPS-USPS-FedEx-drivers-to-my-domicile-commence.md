---
layout: post
title: "Eustathios V2.. all parts ordered. Let the flood of UPS/USPS/FedEx drivers to my domicile commence"
date: April 03, 2015 03:58
category: "Discussion"
author: Seth Messer
---
Eustathios V2.. all parts ordered. Let the flood of UPS/USPS/FedEx drivers to my domicile commence.





**Seth Messer**

---
---
**Brandon Cramer** *April 03, 2015 05:28*

Do you need my address to send all those parts to? I need to help make sure everything is correct. :)


---
**Gus Montoya** *April 14, 2015 05:06*

Hi Seth, can I get the rest of your parts list order? or is it the same as the google sheets list you've been working on?


---
**Seth Messer** *April 14, 2015 05:17*

Hey there Gus. Glad you're back. The Google Sheets document is all you need. Complete with purchase links. 


---
**Gus Montoya** *April 14, 2015 05:20*

thanks **+Seth Messer**  yeah I'm glad I didn't have to sell anything. My nephew is almost out of the hospital and in great spirits. 


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/H2J26QjiDUy) &mdash; content and formatting may not be reliable*
