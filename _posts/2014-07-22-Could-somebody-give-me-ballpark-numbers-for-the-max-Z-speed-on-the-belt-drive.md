---
layout: post
title: "Could somebody give me ballpark numbers for the max Z speed on the belt drive?"
date: July 22, 2014 19:22
category: "Discussion"
author: Nick Parker
---
Could somebody give me ballpark numbers for the max Z speed on the belt drive? And is there backlash?



I'm setting myself up with a new machine for experiments, and one of my requirements is a reasonably fast, lash free Z for non-planar slicing. 15 mm/s is about the fastest I'm likely to use.





**Nick Parker**

---
---
**Eric Lien** *July 22, 2014 23:49*

I would recommend the corexz by +Nicholas Seward. I am really interested in it. I think it is a prime candidate for non-planar slicing. ﻿


---
**Nick Parker** *July 23, 2014 01:14*

Yeah that's my other candidate. That's got a moving build platform though, which I really don't like.



Once I have a workhorse printer going I'm probably building his sextupteron. That's where the real slicing fun will happen.



I think I'll just build an Ingentis as a work horse, and test non-planar stuff on it even if it has to go slow. If I get non-planar working well I can upgrade the Z, right now I'm kind of counting chickens before they hatch.


---
**Eric Lien** *July 23, 2014 01:18*

The quality in XY on the Ingentis/eustathios is truly exceptional. If you go belt Z, might I suggest a 5:1 gear reduced nema planetary drive for Z. No fear of drop unless the belts snap on both sides. The planetary has enough reduction to stay put even when not energized. ﻿


---
*Imported from [Google+](https://plus.google.com/+nickparker/posts/N71MbJGXmQK) &mdash; content and formatting may not be reliable*
