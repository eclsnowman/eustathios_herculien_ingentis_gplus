---
layout: post
title: "So i have the frame built. Just the outer cube though"
date: March 28, 2015 18:37
category: "Discussion"
author: Ethan Hall
---
So i have the frame built. Just the outer cube though. I'm getting in the last parts to fix my mendel, so I can finish printing out the rest of the parts. In the meantime I was wondering how far down these extrusions need to be placed and how large should I make the bed? Thanks!

![images/9abb59e9ae56111fed3e0a06fbae96c9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9abb59e9ae56111fed3e0a06fbae96c9.jpeg)



**Ethan Hall**

---
---
**Erik Scott** *March 28, 2015 19:10*

Not sure bout the Ingentis, but on the Eustathios, it's 20 mm from the bottom of the top extrusion to the top of the bottom bearing holder. That's a bit confusing, so I made a pic for you: [http://imgur.com/G74pCPU](http://imgur.com/G74pCPU)



Eric Lien also made some printable alignment things that work for the Eustathios. Again, can't be certain about the Ingentis. 


---
**Ethan Hall** *March 28, 2015 19:11*

Thanks!﻿ could someone confirm this so that I don't go drilling holes in my tslot in the wrong spot?




---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/46htDk51owG) &mdash; content and formatting may not be reliable*
