---
layout: post
title: "HercuLien or Eustathios V2? Am I ready to build one now, or should I wait?"
date: January 10, 2016 05:04
category: "Discussion"
author: Greg Johnson
---
HercuLien or Eustathios V2? Am I ready to build one now, or should I wait?



Hi all. I just recently jumped into 3d printing with a Monoprice Maker Select (rebadged Wanhao Duplicator i3) to see what the fuss is about and I'm hooked. I have experience with 3d modeling, CAD, waterjet, composite materials, metal, wood, etc. but additive manufacturing is new for me. The i3 was bought because if I didn't like it, I wouldn't feel bad about it collecting dust. Now I just want faster, bigger, better quality.



Someone turned me onto the HercuLien, so I'm now  dialing in my current machine for PETG so I can print the parts and get started. Then I started reading about the Eustathios v2, and now I'm confused as to the strengths and weaknesses of each and which one is better suited for a first timers build.



I was also considering getting a Rostock MAX V2 because they're only $1k and I think it'd be nice to have experience with a delta.



Give this info, I'd love some input from others. Thoughts? Suggestions?



Happy to have found this community and I'm looking forward to contributing and posting my progress.





**Greg Johnson**

---
---
**Jim Stone** *January 10, 2016 05:13*

Herculien 100%

and make it enclosed :D


---
**Zane Baird** *January 10, 2016 05:15*

If you want the possibility to utilize a large build volume, another vote for the Herculien. **+Jim Stone** how are those parts working for you? I could definitely print them at higher quality now... :(


---
**Jim Stone** *January 10, 2016 05:16*

lol im still only just fitting my panels. havent even wired yet. **+Zane Baird**



may have split the hotend carriage. but meh.

doesnt seem to have hurt it.


---
**Eric Lien** *January 10, 2016 05:22*

I would really say it depends upon how you planned to use the printer. If you planned to print very large things, or print a lot in ABS then the HercuLien is a better option. But if you plan on printing slightly smaller items or less ABS... Then Eustathios is good. Both are great printers. But do note the larger you go on your printer the more difficulty you can run into with tuning.



For maximum speed there really is no comparison, deltas are your best option. But due to the round build area you are substantially more limited in X & Y. I'm currently printing a lot right now on the Delta designed by **+Alex Lee**​. And I love it. If I have something smaller to print and I want it done fast it is definitely my go to printer. Then again I have four printers which is more than most people. I gotta admit it's nice to have options.


---
**Eric Lien** *January 10, 2016 05:25*

Also the longer I've had it collection dust, the less I really care about dual extrusion. For how often you actually use it and the tuning it takes to get it to run well I'm not really sold on it being an nessisary option. To be honest using simplify3d for my slicer makes support removal so easy that I never even think about printing with dissolving support.


---
**Eric Lien** *January 10, 2016 05:28*

Lastly do yourself a huge favor and get a Bondtech extruder. Mine is fine (Hercustruder) and worked for a long time for me...but there is nothing else that compares to Martins extruder, not even close.


---
**Jim Stone** *January 10, 2016 05:29*

i havent even got my bondtech qr plugged in and working yet and i can say i agree just by the FEEL the thing FEELS amazing and the man behind it is quite accomidating


---
**Ted Huntington** *January 10, 2016 05:30*

It would be interesting to run a price comparison. I was adding up the Eustathios Spider v2 and it's at least $700 (includes shipping and tax). I kind of wonder what the Herculien would be. I built a Eustathios and it's awesome, only now I want to build a bigger one. For what I do, I mostly need large X and Y, and less large Z- but that is nice to have too.


---
**Jim Stone** *January 10, 2016 05:38*

**+Ted Huntington** I stopped counting the price on my herc. but i know im past 2k CAD but thats sort of because i cant use mcmaster. and i had to oddly source some things. because im in the great white north.



but either way thats 1.5k in usd. so thats UM2 money. but its soooo much better looking than the UM2 and larger. and im sure...less problematic.


---
**Eric Lien** *January 10, 2016 05:42*

**+Jim Stone** even the best printers are problematic every once in awhile, and learning to wrangle in a printer that you could fit a prusa I3 inside of is no exception:)



The difference is you've got a whole clan of people on G+ who have made them and designed them to help when you run into issues.


---
**Eric Lien** *January 10, 2016 05:46*

Another piece of advice... go with ball screws vs lead screws. The prices from golmart  (on aliexpress) are way better than the Misumi leadscrew prices anyway, they will machine them to any size/end geometry you want, and they have lower rotating resistance.


---
**Zane Baird** *January 10, 2016 05:50*

I'm in agreement with **+Eric Lien**, for enclosed ABS, the Herculien is the way to go. Likewise, I have yet to buy a second bondtech and get dual extrusion going as previous experience says it's not great unless absolutely necessary. That being said, my next printer will absolutely be a delta 


---
**Jim Stone** *January 10, 2016 05:56*

**+Eric Lien**

 looks like i may be ordering some from them. but i hate dealing with ali. cause its confusing for me :(i tried to do the heater from there. and yeah... i went elsewhere

cause i was confused.


---
**Zane Baird** *January 10, 2016 06:08*

**+Eric Lien** regarding the ball screws, is a low resistance screw worth it on the z axis? it seems the gravity and the high load negates the reasoning behind their use. 


---
**Eric Lien** *January 10, 2016 06:35*

**+Zane Baird** I think ease of motion on every axis is good. Motors provide the stall. And a faster moving Z could provide Z-hop if you wanted on travel moves. After experiencing the benefits of Z-hop on my Delta my slow moving HercuLien Z stage seems glacial by comparison.


---
**Zane Baird** *January 10, 2016 06:39*

**+Eric Lien** I'm using screws with an 8 mm lead on my Herculien. I'll include a z-hop test in my upcoming post out of curiosity...


---
**Ben Delarre** *January 10, 2016 07:49*

Did anyone ever get a working belt driven z going? From thinking about it it could give some big advantages in resolution and in having absolutely no possibility of z wobble. Pretty sure someone was working on it but can't find it now. 


---
**Michaël Memeteau** *January 10, 2016 09:50*

**+Ben Delarre** Look for what **+Maxim Melcher** or **+Shauki B** have done on Z stage in the quadrap-3d-printer community. You may find an alternative solution...


---
**Jo Miller** *January 10, 2016 16:05*

**+Eric Lien**

Hi Eric , do you have any details about that Delta ?.   I´m curious because your Herculien Design still impresses me  (And I working with the herculien for about 4 months now )

Im asking because I still have an unopened Box with a BI 2.5 Delta - based on fihing-lines instead of gt2 belts.  (turned out to be a foolish design)

So now its time to start on a  Delta with  Due Eletronics and Openbuilds V-wheels....


---
**Eric Lien** *January 10, 2016 16:48*

**+Jo Miller** [https://github.com/alex7575/Talos3D/tree/master/Tria%20Delta](https://github.com/alex7575/Talos3D/tree/master/Tria%20Delta)


---
**Greg Johnson** *January 11, 2016 01:47*

This community rocks. I was reading these replies last night on my phone, but wanted to wait until I could sit at a keyboard to respond.



**+Eric Lien** My question about the delta was more along the lines of "Should I get the Rostock v2 kit, build it, and use that instead of the i3 to build the HercuLien?" Is there value in having a delta in your arsenal, filling a need that the i3 and HercuLien will not?



As for dual extrusion, I like the idea of having disolvable support, but I've only talked to one person who swears by it. It seems the general concensus is the one you share, and I'm good enough listening to the collecting wisdom on this; it's not something I need to figure out myself.



If the Bondtech extruder is 'the' extruder to run, what is 'the' hotend to run? I'm not interested in saving a little money, I'm interested in buying right and buying once (or as seldom as possible). I see the E3Dv6 is pretty popular. I like the idea of being able to run either 1.75 or 3mm, but other than the kickstarter coming up this month, I don't know what that solution looks like.



**+Ted Huntington** From what I can see, the HercuLien will cost about $2k USD total. I'll find out.



Thanks again for all the responses guys, I appreciate it.


---
**Eric Lien** *January 11, 2016 02:01*

**+Greg Johnson** it really depends on your wallet ;) if you can afford it deltas are amazing, and lots of fun to print with. Then again my friends highly tuned prusa I3 has turned out some of the highest quality prints I have ever seen in person. Plus the ability to run Flexible filaments with a direct drive extruder is a real bonus.



For the hot end E3D v6 is hands down the best for just about every filament... (Unless you are all PLA, then a genuine J-head is hard to beat).



If you want to be able to run both 1.75 & 3.0 just get one of each and setup dual extrusion with the primary as 1.75, and the secondary as 3.0. That might also help print flexibles via the Bowden because 3.0 in ninjaflex would perform much better (like an Ultimaker).






---
**Chris Brent** *January 11, 2016 17:52*

I'd just add something about the Delta-> HercuLien route. I bought a Kossel mini kit to basically go down that path. I thought I was a reasonably skilled builder, but I'm having a heck of a time getting dimensionally accurate prints out of it. Admittedly I have very little spare time and spend an hour here and there on it, but there are so many mechanical variables in the build to get right. YMMV. I do love watching the delta work though, it's pretty magical. The other thing is that I'd love a direct drive printer for flexible filaments, if I was starting now I'd look at building Alex's Talos ([https://plus.google.com/u/0/communities/106411395795447750286](https://plus.google.com/u/0/communities/106411395795447750286)) and then move onto a Euth/Herc.


---
*Imported from [Google+](https://plus.google.com/117128629201142457691/posts/PhAziZV1FZz) &mdash; content and formatting may not be reliable*
