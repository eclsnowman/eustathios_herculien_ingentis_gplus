---
layout: post
title: "First moves in xy. Y moves seem great"
date: April 12, 2015 04:34
category: "Discussion"
author: Ben Delarre
---
First moves in xy. Y moves seem great. Not even run the burn in gcode completely yet. X moves are a little noisy. Not yet sure why. I moved one of the pulleys away from the bearing block a little that seemed to help, so perhaps they are rubbing on the bearings? 



Z axis pulleys arrive tomorrow then hopefully I can finish that stage.  Printing will have to wait till after I move house and get the shapeoko up and running again to cut the alu plate. 



For those with a careful eye you might notice something odd about my eustathios, can you guess what? Bear in mind you are looking at it from the left. 


**Video content missing for image https://lh3.googleusercontent.com/-Xd--f9dmyM0/VSn1zuuFD5I/AAAAAAAAEU4/n3UnlVI-NKU/s0/VID_20150411_205408.mp4.gif**
![images/3535ed4153bc4664adf0f5c43bf420c6.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3535ed4153bc4664adf0f5c43bf420c6.gif)



**Ben Delarre**

---
---
**Eric Lien** *April 12, 2015 05:08*

Looks like you mirrored the z drive motor to drive from the front. Is that it?


---
**Ben Delarre** *April 12, 2015 05:11*

Yep.  Completely by accident. The prusa i3 I used to print all the parts was setup wrong. All the parts came out mirrored in the y axis. I didn't notice till I tried to assemble the main carriage!



It was easy enough to fix but I didn't want to reprint everything so I just redid the non symmetrical parts like the carriage and duct. Hopefully it won't mess anything up! 


---
**Eric Lien** *April 12, 2015 05:11*

Also did you put a shim in between the pulleys and the bearings: [http://i.imgur.com/BD8fGpP.png](http://i.imgur.com/BD8fGpP.png)


---
**Eric Lien** *April 12, 2015 05:13*

If not the pulley tends to rub on the bearing. If the pulley is not seated against the shim, then the shim to the bearing the rod floats. That can let the rod rub against the printed bearing pillow block and wear or cause friction.


---
**Eric Lien** *April 12, 2015 05:14*

But all-in-all looking great and can't wait to see more updates and progress.


---
**Ben Delarre** *April 12, 2015 05:15*

Aha! I totally missed that. Damn. Guess I am taking it all apart again. Thanks **+Eric Lien**​ I would not have caught that. 


---
**Oliver Seiler** *April 12, 2015 06:26*

Bugger, I missed that shim, too.


---
**Eric Lien** *April 12, 2015 06:36*

I hope when **+Seth Messer**​​ gets his build guide up I can have you guys add your tips/trick learned from your builds. There are lots of things I just know from experience... Or because I modeled them. So I forget how to "not already know the answer". 



For example I know the Misumi pulleys need no shims because the have a small step built into the hub... But the robotdigg ones do need them.



Thats why I am glad someone else like Seth is making the guide... It means there are no assumptions.﻿


---
**Oliver Seiler** *April 12, 2015 09:44*

No worrie **+Eric Lien**, this forum is awesome for getting all the support needed. Yes, it's a somewhat steep learning curve, but I do enjoy that quite a bit (remind me next time something doesn't work).


---
**Oliver Seiler** *April 12, 2015 09:45*

Oh, and kudos to **+Seth Messer** of course B-)


---
**Seth Messer** *April 12, 2015 19:20*

Awesome job **+Ben Delarre**! No kudos or thanks to me just yet. I'm just now getting started, snapping pictures and staging everything. One thing that **+Eric Lien** told me as a tip, which is holding to be very true, is make sure I insert all the t-nuts needed before assembling the frame. :) (forgot some already, doh). 



I've been using this eDrawings app to try and reference everything, but it's horrendously slow.



**+Ben Delarre** which of the files did you reference to do the assembly of everything thus far?


---
**Ben Delarre** *April 12, 2015 19:24*

I used the eustathios v2 solidworks files. But honestly I am rubbish at following instructions,  so I didn't pay enough attention to the model.



I can also highly recommend getting the tnut count right but you will always forget one.



Oh and for those ordering from robot digg.  Don't get their post insertion nuts.  They are rubbish compared to the misumi ones. The misumi ones fix themselves in place with a little bump to create friction.  This means you can place them then screw into them reliably. The robotdigg ones rotate into place when doing them up,  sometimes they fail to rotate and you just end up tightening the nut not pulling your piece against the extrusion. 


---
**SalahEddine Redjeb** *April 13, 2015 09:53*

Hello, looks good so far, smooth movements, do you mind telling me what gcode do you use to make it move? i just startedmoving mine and still have to test it intensively.


---
**Eric Lien** *April 13, 2015 11:34*

**+SalahEddine Redjeb**



​ [https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Documentation/Break%20In%20Gcode](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Documentation/Break%20In%20Gcode)﻿


---
**SalahEddine Redjeb** *April 13, 2015 11:38*

Thank you very much **+Eric Lien** !


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/RHkAvGRfi6m) &mdash; content and formatting may not be reliable*
