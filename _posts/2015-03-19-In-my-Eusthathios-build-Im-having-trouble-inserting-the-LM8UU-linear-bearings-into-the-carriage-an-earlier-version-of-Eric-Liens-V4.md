---
layout: post
title: "In my Eusthathios build, I'm having trouble inserting the LM8UU linear bearings into the carriage (an earlier version of Eric Lien's V4)"
date: March 19, 2015 02:32
category: "Discussion"
author: Eric Bessette
---
In my Eusthathios build, I'm having trouble inserting the LM8UU linear bearings into the carriage (an earlier version of Eric Lien's V4).  I've tried applying acetone (from nail polish) with a q-tip as well as shaving off the inside with an exacto knife.  Neither seems to be helping and I worry about the alignment.  Has anyone else had this problem or any suggestions?



If I need a stronger solution of acetone, where do you suggest getting it from in the US?



Thanks for your help.

![images/5a7f393744f4107f95c7c69a6257c578.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5a7f393744f4107f95c7c69a6257c578.jpeg)



**Eric Bessette**

---
---
**Jason Smith (Birds Of Paradise FPV)** *March 19, 2015 02:37*

My carriage is pla, and I always just heat it up with a hair dryer and the bearings slip right in.  Not sure if that will work with abs though. 


---
**Eric Bessette** *March 19, 2015 02:38*

Thanks, I'll look into that.


---
**Eric Lien** *March 19, 2015 03:02*

Sounds like you are undersized. You can drill out the hole. But my recommendation is for ABS to print with a 1.008 scale factor to account for abs shrink factor and make sure to dial in your extrusion multiplier with a single perimeter cube calibration before printing highly toleranced parts.


---
**Chris Brent** *March 19, 2015 03:32*

If you want straight up acetone just go to a hardware store. You'll find it in the paint section. Running an 8mm drill bit or reamer down there is probably a better idea if you need to actually remove material.


---
**Isaac Arciaga** *March 19, 2015 03:33*

**+Eric Bessette** I'm looking through my copy of **+Eric Lien** 's V2 repository. You might have the carriage meant for bronze bushings? I don't see an stl for lm8uu's.


---
**Eric Lien** *March 19, 2015 04:08*

**+Isaac Arciaga** shoot, I never made a final of that. Anyone mind relinking one they have modded. My file system is a mess right now.


---
**Jason Smith (Birds Of Paradise FPV)** *March 19, 2015 04:13*

**+Eric Lien**. Here you go:

[https://drive.google.com/file/d/0B2629YCI5h_wVlBUVmJ2cG5pUDQ/view?usp=sharing](https://drive.google.com/file/d/0B2629YCI5h_wVlBUVmJ2cG5pUDQ/view?usp=sharing)


---
**Eric Bessette** *March 19, 2015 14:56*

It looks like I didn't get the appropriate hardware for this version of the carriage. Thanks for your help guys.


---
**Roberto Viglione** *April 08, 2015 05:56*

hi guy's, I doing an ingentis with my friend and i use LMU8 for the exstrusor, you can share the file? 


---
*Imported from [Google+](https://plus.google.com/106288190144199995561/posts/RTrij16vCxL) &mdash; content and formatting may not be reliable*
