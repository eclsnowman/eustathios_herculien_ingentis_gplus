---
layout: post
title: "The last time this happened was 39 weeks ago when I posted a picture of my broken boro glass on here"
date: June 07, 2017 15:25
category: "Discussion"
author: Brandon Cramer
---
The last time this happened was 39 weeks ago when I posted a picture of my broken boro glass on here. Only this time I don't have a spare piece. These pieces are sticking so well that when they cool, the PLA pulls the glass with it. I shouldn't complain since this piece of boro glass lasted 39 weeks, but what a bummer. 



Should I order boro glass again or something else? Maybe the boro glass isn't thick enough?



![images/ffc2442e9ad5dfa134aa1662ee6c139f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ffc2442e9ad5dfa134aa1662ee6c139f.jpeg)

**Brandon Cramer**

---
---
**Eric Lien** *June 08, 2017 04:42*

Go with standard window glass. That's what I use.


---
**Eric Lien** *June 08, 2017 04:44*

Also you seem to be over clamping it. That's a lot of concentrated point loads on the glass (which glass doesn't like). Especially when the bed wants to crown during heatup/cooldown.


---
**Brandon Cramer** *June 08, 2017 05:49*

Standard window glass sounds a lot cheaper than boro glass. Any certain thickness? I normally have 4 of those clamps. Should I just use two?


---
**Daniel F** *June 08, 2017 21:07*

30x30cm 3mm thick mirror tile with 4 clamps works fine for me.


---
**jerryflyguy** *June 11, 2017 01:58*

I use 6mm Standard glass, 1 year in and no issues yet(printing almost every day)


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/17WqaXB2gCx) &mdash; content and formatting may not be reliable*
