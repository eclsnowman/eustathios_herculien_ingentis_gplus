---
layout: post
title: "Shared on March 08, 2014 06:40...\n"
date: March 08, 2014 06:40
category: "Show and Tell"
author: D Rob
---


![images/64dba983dbb285e21b675a85a950a0ce.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/64dba983dbb285e21b675a85a950a0ce.gif)



**D Rob**

---
---
**Nuker Bot (NukerBot 3D Printing)** *March 11, 2014 22:08*

High powered Magnets and a some kind of valve to slow it down?


---
**D Rob** *March 12, 2014 00:15*

Yes, as you pull the drawer, the magnets pull the rod. The rod has a spring on it; inside a cylinder. The other end of the cylinder has a bleeder valve and diaphragm intake. The intake allows fast compression of the spring then seals when positive pressure is applied. The bleeder valve regulates the escaping air to maintain a decreasing positive pressure. This gives a controlled descent and even a pull, when the air pressure normalizes, due to the spring. The magnets separate when the rod is fully extended, and a piece sticking out on the side flips a lock into place to hold the rod fully extended. As the drawer closes the mechanism trips the lock, after the magnets connect, and a retracts the drawer in a controlled manner.


---
**Nuker Bot (NukerBot 3D Printing)** *March 12, 2014 00:20*

Holy crap, now how to make that a part you can 3D print. In think the device is called a slow close draw slide. Maybe there is a patent that can be reviewed


---
**D Rob** *March 12, 2014 00:32*

probably but im positive the 20 year period is expired which makes it public domain in the US﻿


---
**Nuker Bot (NukerBot 3D Printing)** *March 12, 2014 00:42*

Even better. Maybe you can make the diagram from ninjaflex or flexible PLA?


---
**D Rob** *March 12, 2014 00:44*

I've was thinking maybe a inflatable mattress patch


---
**Nuker Bot (NukerBot 3D Printing)** *March 12, 2014 00:46*

That too, just cut it in a circle


---
**D Rob** *March 12, 2014 00:48*

Cylinder (printed), spring, plunger (printed), rod  (coat hanger?), bleeder valve (screw), neodium magnets, and a printed two way toggle latch.﻿


---
**D Rob** *March 12, 2014 00:49*

With the right amount of plunger slop and rod slop might not even need a bleeder valve


---
**Nuker Bot (NukerBot 3D Printing)** *March 12, 2014 00:52*

Sounds like you have got this all figured out. What programs you use to model your parts in?


---
**D Rob** *March 12, 2014 00:59*

Solid works I can't imagine any other way after using it. But I have a little experience in 3dmax


---
**Nuker Bot (NukerBot 3D Printing)** *March 12, 2014 11:20*

Nice. I am still learning solidworks but I have been using 123D but it crashes all the time.


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/bMitp3R4Q2v) &mdash; content and formatting may not be reliable*
