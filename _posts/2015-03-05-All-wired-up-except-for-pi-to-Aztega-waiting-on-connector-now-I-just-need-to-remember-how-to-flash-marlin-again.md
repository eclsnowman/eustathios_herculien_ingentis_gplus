---
layout: post
title: "All wired up except for pi to Aztega (waiting on connector) now I just need to remember how to flash marlin again"
date: March 05, 2015 05:48
category: "Show and Tell"
author: Derek Schuetz
---
All wired up except for pi to Aztega (waiting on connector) now I just need to remember how to flash marlin again

![images/d02a508e03f88161739f62deb933d4d0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d02a508e03f88161739f62deb933d4d0.jpeg)



**Derek Schuetz**

---
---
**Eric Lien** *March 05, 2015 11:40*

May I recommend some black paint on the backside of the cover. That's what I did to hide the wiring mess through the lexan.


---
**Liam Jackson** *March 05, 2015 14:11*

Found the hidden SPAM! :-D


---
**Derek Schuetz** *March 05, 2015 14:32*

I kind of like the seeing into its guts. I contemplated the bracing it out at first


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/gAZSWPDoCWD) &mdash; content and formatting may not be reliable*
