---
layout: post
title: "I have a quick question about placement of the Z axis ballscrews in the Eustathios"
date: November 01, 2015 00:35
category: "Discussion"
author: Bud Hammerton
---
I have a quick question about placement of the Z axis ballscrews in the Eustathios. I noticed on the HercuLien that the lift axis is centered on each side, of course it is more robust than the Eustathios, rightfully so. But in all the diagrams of the Eustathios Sv2 the guide rod is centered and the ballscrew is offset. Was this on purpose or just how it ended up fitting together?



I might also be looking at the wrong working models





**Bud Hammerton**

---
---
**Jason Smith (Birds Of Paradise FPV)** *November 01, 2015 00:37*

It was on purpose, but then again, I suppose it could work either way. 


---
**Bud Hammerton** *November 01, 2015 00:38*

More curiosity than anything technical, but **+Jason Smith** what was the rationale?


---
**Igor Kolesnik** *November 01, 2015 01:06*

This was done to negate the torque of leadscrews. It might be extensive but every bit of precision counts. 


---
*Imported from [Google+](https://plus.google.com/+BudHammerton/posts/aspjx1QYueh) &mdash; content and formatting may not be reliable*
