---
layout: post
title: "What is the best supplier to buy all the Herculien hardware from?"
date: April 14, 2015 18:55
category: "Discussion"
author: Gunnar Meyers
---
What is the best supplier to buy all the Herculien hardware from?  Also is there a good place for all the electronics? I think I'll buy the aluminum parts precut from Misumi. 





**Gunnar Meyers**

---
---
**Dat Chu** *April 14, 2015 18:56*

Smw3d.com. The second supplier links on that spreadsheet. 


---
**Eric Lien** *April 14, 2015 20:18*

Hardware as in nuts/bolts I would get what you can from [http://www.trimcraftaviationrc.com/](http://www.trimcraftaviationrc.com/)



They don't carry everything on the BOM, but for what they do the prices are great, and good quality.


---
**Gunnar Meyers** *April 14, 2015 23:44*

I've only seen the pdf which doesn't have any links on it. The second BOM option will not load all the way. Which has true links?


---
**Eric Lien** *April 14, 2015 23:55*

**+Gunnar Meyers** all the links are there in both documents. You need to click the suppliers pricing.


---
**Gunnar Meyers** *April 14, 2015 23:59*

Thank you so much for the help. I hope your ready for more newb reprap question in the future. This will be the largest most complicated printer I've built and it's great to see an active community behind it. 


---
**Eric Lien** *April 15, 2015 00:23*

**+Gunnar Meyers** I will help in any way I can. It is a complicated build... But worth it in the end. But I am probably biases ;)


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/ikHauxwVH8C) &mdash; content and formatting may not be reliable*
