---
layout: post
title: "Hey guys. What are you doing for extruder mounting on the Spider V2?"
date: February 03, 2017 15:36
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hey guys. What are you doing for extruder mounting on the Spider V2? I'm using dual saintflints, and want to keep the tube as short as possible (I plan to do something like the herculien later and have an enclosed chamber with extruders mounted at the top.) Anyone have stls for a stepper clamp that works?





**Stefano Pagani (Stef_FPV)**

---
---
**jerryflyguy** *February 03, 2017 16:46*

I am using a Titan and the mount that it came with. Works great, I to intend to enclose my V2 at which point I'll find a better way which gives a shorter Bowden tube.


---
**Sean B** *February 04, 2017 16:07*

I use this one for my Saint Flint, works perfectly. Stepper mount for 2020 rail found on #Thingiverse [https://www.thingiverse.com/thing:1357814](https://www.thingiverse.com/thing:1357814)


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/XWtMLiQQgg7) &mdash; content and formatting may not be reliable*
