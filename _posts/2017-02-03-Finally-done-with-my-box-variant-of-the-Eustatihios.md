---
layout: post
title: "Finally done with my box variant of the Eustatihios"
date: February 03, 2017 12:21
category: "Show and Tell"
author: Markus Granberg
---
Finally done with my box variant of the Eustatihios. Works very well, but i still have som fine tuning to do. I went with igus self aligning bearings and I can only say good things about them..


**Video content missing for image https://lh3.googleusercontent.com/-_R9d4Vlp9YU/WJR1xb7-kpI/AAAAAAAAY7U/qhj4PWw9_v0pOT1jJ2VQh-dnTbHYf4PaACJoC/s0/20170203_131432.mp4**
![images/e5dc489641f3ae5607083bc5135baf1c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e5dc489641f3ae5607083bc5135baf1c.jpeg)



**Markus Granberg**

---
---
**jerryflyguy** *February 03, 2017 16:48*

**+Markus Granberg** sorry mark but now that you've posted a tease, your going to have to give us MUCH more info! Would love to hear and see whatever details you would like to share.


---
**Ted Huntington** *February 03, 2017 16:53*

wow great work-  looks really different from the original- where is the carriage from- great to see an inductive sensor- did you have to put a sheet of steel on top of the aluminum plate? Looks like graphite bushings on the side too. Did you have to machine the aluminum bed? It looks like the traditional machining was not needed.


---
**Markus Granberg** *February 04, 2017 07:33*

 I will post more info soon ;)


---
**Ben Marks** *February 06, 2017 04:27*

I've got that same probe on my Wilson II and it's great.  I'll never go back to a printer without a probe on any printer that moves the bed in X or Y.


---
**Eduardo Ribeiro** *February 24, 2017 16:22*

Hey Markus can you share with us the printhead? i'm looking for some with the sensor as you did.  


---
**Markus Granberg** *February 25, 2017 14:05*

**+Eduardo Ribeiro** Shure! Here is the whole printer. 

[https://drive.google.com/open?id=0B7kgdYk7GVumTENpRzVaVzJ6NE0](https://drive.google.com/open?id=0B7kgdYk7GVumTENpRzVaVzJ6NE0)


---
**Frank “Helmi” Helmschrott** *March 24, 2017 18:01*

**+Markus Granberg** great to see someone else trying the igus bushings. Very keen to here more from them. Since my printer is extruding reliably now and i'm doing quite some tuning work I'm more and more suprised how great they perform.



Very interesting also to see your direct extrusion setup with the titan. I have a titan around still and I'm curious how it works. Thought about doing that doo but currently everything runs good. Also your box version looks great.



You should definitely post more details.


---
*Imported from [Google+](https://plus.google.com/+MarkusGranberg/posts/d7ytxYmCDce) &mdash; content and formatting may not be reliable*
