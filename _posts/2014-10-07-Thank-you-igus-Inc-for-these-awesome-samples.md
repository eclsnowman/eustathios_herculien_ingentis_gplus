---
layout: post
title: "Thank you igus Inc. for these awesome samples"
date: October 07, 2014 22:06
category: "Show and Tell"
author: D Rob
---
Thank you **+igus Inc.** for these awesome samples. I will do my best to give a full and detailed review of the 1050 lead screws in the gantry of my Ul-T-Slot MKIII can't wait to get the anti backlash screw's as well﻿

![images/4ec3209940de4d7e58d59c7390e21829.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4ec3209940de4d7e58d59c7390e21829.jpeg)



**D Rob**

---
---
**Nuker Bot (NukerBot 3D Printing)** *October 07, 2014 22:11*

+igus is awesome!


---
**Matt Miller** *October 07, 2014 23:14*

Those look absurdly expensive.  Like more than my 3 printers combined expensive.  D:



But if they work then hooray!  :D


---
**D Rob** *October 08, 2014 02:25*

**+Matt Miller** they are free samples. 8 nuts and 4 x 500mm 1050 lead screws


---
**Nuker Bot (NukerBot 3D Printing)** *October 08, 2014 02:26*

How do you get the free samples?


---
**Nuker Bot (NukerBot 3D Printing)** *October 08, 2014 02:28*

I am looking to design a new printer that could use something like this for the z axis.


---
**Matt Miller** *October 08, 2014 02:40*

**+D Rob** I know.  Do you have a retail amount for them?


---
**D Rob** *October 08, 2014 03:36*

no but I will find out and get a price.


---
**D Rob** *October 08, 2014 08:20*

**+Nuker Bot** on the page with the desired part click request sample


---
**Nuker Bot (NukerBot 3D Printing)** *October 08, 2014 14:04*

Thanks **+D Rob** 


---
**D Rob** *October 08, 2014 15:36*

no prob


---
**D Rob** *October 08, 2014 15:39*

[http://www.igus.com/wpck/7835/DryLin_Steilgewindemuttern](http://www.igus.com/wpck/7835/DryLin_Steilgewindemuttern) fill in length and size and right above dimension click the text saying request quote/samples


---
**Øystein Krog** *October 08, 2014 16:09*

Wow, would be interesting to have a fully screw-based machine with these.

I guess it would be both fast and silent..


---
**D Rob** *October 08, 2014 16:17*

this is my goal. I also want to run  the machine with closed loop brush less dc servos(BLDC)


---
**igus Inc.** *October 09, 2014 12:12*

**+Nuker Bot** **+D Rob** **+Shauki Bagdadi** You guys are awesome!! Good luck with the lead screw set up - let us all know how they work! 


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/NUpxi4uDB2K) &mdash; content and formatting may not be reliable*
