---
layout: post
title: "Thanks to the fantastic @Eric Lien I now have a functioning Eustathios V2!"
date: June 11, 2015 05:07
category: "Discussion"
author: Ben Delarre
---
Thanks to the fantastic @Eric Lien I now have a functioning Eustathios V2!



I'll post some better photos later (wife has the camera), but I have to say that it is SO much easier to assemble when you start from accurate parts and Erics parts are absolutely lovely, I almost thought they were molded when I got them out of the box.



Anyway, once I am fully functional and have my heated bed installed I will be offering up a set of Eustathios V2 parts for the Print It Forward program. However....(of course, this couldn't be that easy could it!).



Anyone experienced a temperature drop using an E3D V6 hotend? I've PID autotuned on the smoothieboard and updated my values appropriately. The hotend reaches 200 degrees in about a minute which is awesome using a 24v cartridge from e3d. However once the print starts the temperature drops a good 20 degrees down to around the 180 mark and won't climb any further.



I thought it might be the powersupply but thats not even working all that hard, the fan isn't even coming up and its a Jameco 240W 24V supply....is that enough? Did I screw up my math? Or is my smoothieboard just a bit pants? I do suspect the power supply as I noticed when printing smaller areas where the movements were less the temperature rises again.



The hotend is connected to the smoothieboard via 22AWG wire and its warm but not hot.

![images/592e369485ad801834f99f8c1a70ca8c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/592e369485ad801834f99f8c1a70ca8c.jpeg)



**Ben Delarre**

---
---
**Oliver Seiler** *June 11, 2015 05:15*

I had the very same problem. Check that you got the right heater cartridge from E3D. I had two V6 from them with a supposedly 20W cartridge that underdelivered and after complaining they sent me two 40W cartridges. Measure the resistance of the cartridge and compare to the ones quoted on the V6 assembly page.


---
**Oliver Seiler** *June 11, 2015 05:15*

[http://wiki.e3d-online.com/wiki/E3D-v6_Assembly](http://wiki.e3d-online.com/wiki/E3D-v6_Assembly)


---
**Ben Delarre** *June 11, 2015 05:28*

Hmm...its reading 27ohms at the smoothieboard end. I guess thats probably close to 20W, they also have blue leads so are 25W heaters I guess. I wonder if I failed to select the 40W option.


---
**Oliver Seiler** *June 11, 2015 05:36*

As far as I know they supply 20W by default but I have read many people having issues with that and switching to 40W.

Try heating up without printing and then turn your fan on. Mine couldn't keep 180 degrees with the fan going even when not printing. With a 40W cartridge it works well even at higher temperatures.


---
**Ben Delarre** *June 11, 2015 05:39*

Hmm... I may just order a 40w heater off amazon so I can get it by the weekend. 


---
**Ben Delarre** *June 11, 2015 05:48*

Just ordered a pack of these [https://www.amazon.com/gp/mobile/dp/B00SUYVLYC/](https://www.amazon.com/gp/mobile/dp/B00SUYVLYC/) probably rubbish but worth a shot to have something to play with over the weekend. 


---
**Gus Montoya** *June 11, 2015 06:58*

keep us posted.


---
**Eric Lien** *June 11, 2015 12:55*

Wow, 20watts. That seems much too low. One note is pid auto tune doesn't work on Smoothieware (IMHO). I had to tune mine by hand to get stable temps. Also I would use heavier gauge on the heater when you put on the new cartridge. If the wire is warm at 20 watts, then 40 watts might be unsafe.﻿


---
**Derek Schuetz** *June 11, 2015 13:42*

Just throw PID auto tune at the window and experiment with the settings til you get a stable temperature


---
**Frank “Helmi” Helmschrott** *June 11, 2015 13:52*

hmm guess i have never gotten 20W ones from E3D and i ordered quite some v5 and now some v6 (also the 24V ones). I haven't seen anything below 30W from them (i think it was 30W back with the V5 but unsure - maybe also 40W there)


---
**Ben Delarre** *June 11, 2015 15:08*

What wire gauge is everyone else using? Not sure I can get greater than 22awg into the microfit connector. 


---
**Derek Schuetz** *June 11, 2015 15:16*

24 for all small components. 18 for heater and powering azteeg controller and heated bed


---
**Ben Delarre** *June 11, 2015 15:21*

Hmm.  Maybe I'll just give up on the connectors and solder them they seem unreliable anyway. I have 8x 22awg being run up to the hot end in two 4 conductor cables and an additional 24awg pair for the variable fan.  So I could bundle up 4 of the 22awg and use those for the hotend then the other 4 for the always on fan and thermistor. 


---
**Ben Delarre** *June 13, 2015 04:18*

So I installed the cheap 24V 40W heater from amazon this evening. Much better, temperature is still bouncing around a little but now its oscillating from 199-201 with a 200 target so thats much more like it. Guess I just need to manually tune the PID values now, really beginning to dislike this smoothieboard.


---
**Derek Schuetz** *June 13, 2015 04:45*

Everything is awesome about smoothie except auto PID


---
**Eric Lien** *June 13, 2015 04:50*

**+Ben Delarre** smoothie is great, only issue I had was the PID auto tune issue. 



This post helped me a lot: [https://groups.google.com/forum/m/#!topic/smoothieware-support/woDiL6iFa0M](https://groups.google.com/forum/m/#!topic/smoothieware-support/woDiL6iFa0M)



Especially the comments by Triffid Hunter: 



"I use P10 I0.02 D100 with a 45W nozzle heater....



the D term opposes oscillations and causes the heater to turn off early during warmup to prevent overshooting.



the P term provides coarse drive when the temperature is far from the target



the I term causes the temperature to slowly move towards sitting right on the target instead of below or above it."


---
**Ben Delarre** *June 13, 2015 04:52*

Hmm interesting....I've definitely had an overheating problem. While the board was sat on my table it really got quite hot, hot enough I couldn't touch the driver chips. Had to put it up on some spacers to get some airflow underneath it to keep it from getting too toasty.



I'll give that PID adjustment a try.


---
**Eric Lien** *June 13, 2015 05:07*

**+Ben Delarre** I have a fan designed into the lower skirt on V2, if you don't have a cooling fan for the board I recommend it.


---
**Ben Delarre** *June 13, 2015 05:17*

Yeah I've not mounted the skirt yet, I have some fans I can fit probably. Got to cut some holes in the acrylic bed to mount the smoothie onto. Also need to print a mount that fits it and the Banana Pro I'm going to run it all with.



Next up is restoring the Shapeoko 2 to its former working glory and cut the aluminium bed!


---
**Ben Delarre** *June 13, 2015 05:24*

Right PID is I think tuned... hovering between 189.9 and 190.1 for a 190 target while printing a 3dbenchy. PID values are P50 I0.7 D85. Autotuned values were P55 I6.192 D122!



Things I have left to solve:

1. Stop the horrible rattle which is appearing to come on some diagonal moves. I suspect its the blower fan vibrating inside its housing. Anybody got a good source for a quality 24v 4020 fan? I got a very cheap one from china and I think its bouncing around inside the case.



2. Figure out a good start/end gcode that doesn't result in filament being dragged all through the first layer of the print. This is probably a bed adhesion issue, I'm using mirrored glass and I don't think I've spread enough glue stick over the starting area yet.



3. Dial in e-steps a little more, getting some stringiness and possible over extrusion.



4. Figure out correct retraction settings, currently using 2mm at 60mm/s. Am somewhat afraid of dialling it up any more because of the warnings about the heatbreak in the e3d v6. What do the rest of you use for retraction with the bondtech extruder?


---
**Eric Lien** *June 13, 2015 06:21*

**+Ben Delarre** I use around 6mm retract. Since it is Bowden you use much more than direct drive.


---
**Frank “Helmi” Helmschrott** *June 13, 2015 06:32*

**+Ben Delarre** funny thing - i just read about this warnings on the E3D site the other day and tuned my retract down from 6mm to 2mm which they suggest for bowden use. They say the E3D needs much less than others. I already had 6mm on a v5 bowden before which worked great and haven't had too much issues with that so far on the v6.



Turns out 2mm is way too less for me. I turned it up to 4mm and this is already closed to perfect. On materials that come out rather liquid (like PLA and PETG) it's quite not enough but i think 4,5 or 5mm will do it. Definitely keep the speed at 60mm/s or even higher if you can. That will help.



Regarding e-steps tuning on the extruder: Be sure to watch every aspect - especially filament diameter and set your slicer accordingly. I lately played with esteps too much not noticing that the filament diameter was the issue which i totally forgot to set in the Slicer. I'd also suggest to use a more accurate slicer like Slic3r for tuning. Cura or other more simple slicers sometimes offer enough settings if you're going to print to exact measures.



For the start/end g-code i often do a line from left to right at the starting point, 5mm forward and then a parallel line back. All of that with a little bit overextrusion and a clean manual retract at the end. that helps me keep the nozzle clean and get the filament right to the tip of the nozzle.



Here's an example of a Start-G-Code i used with Cura in Repetier Host (you may of course use it anywhere but the temp variables only work with cura) before [http://pastebin.com/raw.php?i=vkwV3zgh](http://pastebin.com/raw.php?i=vkwV3zgh) some comments included - maybe that helps as a starter.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/SKXKLWx6YLX) &mdash; content and formatting may not be reliable*
