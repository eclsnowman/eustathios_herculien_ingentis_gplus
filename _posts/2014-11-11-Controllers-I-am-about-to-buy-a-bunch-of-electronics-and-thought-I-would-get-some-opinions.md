---
layout: post
title: "Controllers? I am about to buy a bunch of electronics and thought I would get some opinions"
date: November 11, 2014 10:32
category: "Discussion"
author: Mike Thornbury
---
Controllers?



I am about to buy a bunch of electronics and thought I would get some opinions. I want to settle on one well-supported board that can drive a single extruder, a heated bed, an LCD panel and SD-card interface.



There are lots of choices these days - GRBL, Melzi/marlin, G-shield, RAMPS... what to do? 



I am pretty sold on the TI DRV8825 drivers - wide range of tolerance, good heat range - but there is the A4988, which is cheap as.



So, at the moment I am looking at a Uno/Ramps/DRV8825 setup, but I am going to pick up a GRBL board as well (to try TinyG code on a 3D printer).



Any advice/gotchas/success stories?



 #FlatSli3DR   #Sli3DR  





**Mike Thornbury**

---
---
**Eugene Lee** *November 11, 2014 11:44*

You should be able to get a Ramps+Arduino mega + drv8825 + LCD board for under $40 if you look on aliexpress. Your mileage might vary in terms of quality but I've had no problems so far.


---
**Mike Thornbury** *November 11, 2014 12:23*

Which isn't at all the question I was asking, but thanks. I spend about $10-12k a year on Aliexpress, I am actually quite good at finding stuff on there.



I was looking for opinions on combinations and real-life recommendations of controllers people were using with Eustathios-type printers. Specifically, how they perform in acceleration, jerk, etc. What firmware, combinations of driver/stepper/controller, that sort of thing.


---
**Daniel F** *November 11, 2014 13:09*

I use arduino + Ramps 1.4 with 24V for heat bed and Steppers. Heater is 350W which is quite high I would probably go with an ssr if could start from scratch. I had to replace the Fuses with automotive fuses.

Stepper drivers are A4988 4 Layer PCB with heatsinks and active coolingwith a fan. I lost a lot of time with the very cheap stepper drivers, they worked for small prints but as soon as the nozzle had to traverse the whole bed for a while (printnig a big surface), one stepper stoped for some ms and the whole print had a shift of some mm. So it was not cost effective but cheap. It doesn't matter that much which board and drivers you use, I would just not recommend going with 5 stepper drivers for 8$. I'm happy now with my HW, no hiccups with the new 4 Layer PCB drivers. Firmware is latest versoin of repetier host which works stable for me.


---
**Eugene Lee** *November 11, 2014 13:27*

I'm sorry that didn't answer your question. It doesn't matter if you're using an eustathios, corexy or delta. You've asked for a well-supported controller that is sufficient for a single extruder and has support for an LCD with sd card. Ramps/mega/drv8825/full graphic LCD is my answer because it's cheap and works well with the 3 printers I've built.



If you want a better answer please tell us what sort of heaters you'll be using, speeds and your budget.


---
**Brandon Satterfield** *November 11, 2014 16:26*

Far as well supported, hard to beat a RAMPS. People have dropped the DVRs on there with no issues. Another solution may be the RAMBO board. A little pricey and doesn't have the drivers you are looking for, but solved all the RAMPS issues. You can try the wiki for Rambo and looks like this [http://www.smw3d.com/search.php?Search=&search_query=RAMBO](http://www.smw3d.com/search.php?Search=&search_query=RAMBO) 



Update us on what you decide and why, always like to learn. 


---
**Kalani Hausman** *November 11, 2014 19:43*

I use Arduino Mega+RAMPS+Pololu drivers for most builds, but RAMBO when I want a smaller form factor and am willing to pay $85 USD each instead of $45 USD for the RAMPS setup (EBay). The interface for RAMBO also requires an adapter for LCD connections, but those are usually only $5-6 USD each. Oh. and the RAMBO board's MX ports require revering the + and - poles of a Servo if you use those ports for external additional automation.


---
**Juan Manuel Herrera** *November 11, 2014 21:30*

You could try the sav-mki

[http://reprap.org/wiki/SAV_MKI](http://reprap.org/wiki/SAV_MKI)

It can handle up to 15A in the heatbed without heatsinks and is compatible with standard stepper drivers.



It's quite known among the Spanish reprap community.

It's now on rev.D that includes the option of connecting an inductive sensor directly into the card without messing with voltage adaptations.



[http://blog.electrofunltd.com/2014/10/sav-mki-revd-novedades-y-conjunta-video.html?m=1](http://blog.electrofunltd.com/2014/10/sav-mki-revd-novedades-y-conjunta-video.html?m=1)



You could talk directly to its creator [http://google.com/+FMalpartida](http://google.com/+FMalpartida)



I'm using drv8825 drivers (Kliment version) and I'm very happy with their behaviour  without heatsinks






---
**Mike Thornbury** *November 12, 2014 09:00*

It's an interesting board **+Juan Manuel Herrera** , but his pricing is a bit too steep for us in Asia. For less than the cost of his board (44 Euros), I can buy a RAMPS with 5x DRV8825s, a Mk2b heated bed, optical endstops, Mega2560, LCD display, SD-card and all needed cables.



That's got to be my prototype setup, right there. If it turns out to be a pile of poo, it's US$55 to find out, and some of it is bound to be recoverable.


---
**Kalani Hausman** *November 12, 2014 16:45*

**+Mike Thornton** That sounds like a good workable solution, although after the printers were dropped by my students moving them across campus, I removed the optical endstop and went for a simpler mechanical one with better performance overall: [http://www.stemulate.org/2014/08/26/first-3d-printer-fully-recovered/](http://www.stemulate.org/2014/08/26/first-3d-printer-fully-recovered/)


---
**Tim Rastall** *November 13, 2014 04:22*

Might want to look at the CRAMPS, RAMPS-FD and RADDS boards for 32 bit smoothness.


---
**Mike Thornbury** *November 13, 2014 06:11*

Thanks Tim and Kalani. I would love 32-bit, Tim - I had no idea a Due-based board was so affordable. I will have to do more digging.


---
**Mike Thornbury** *November 13, 2014 06:20*

Ouch! It won't be the RADDS - for about $120 less I can buy a TinyG. For $140 less I can buy a Smoothie. Why so expensive for the RADDS?


---
**Tim Rastall** *November 13, 2014 06:36*

**+Mike Thornbury** German made I guess. I do like the look of CRAMPS [http://pico-systems.com/osc2.5/catalog/index.php?cPath=5](http://pico-systems.com/osc2.5/catalog/index.php?cPath=5)


---
**Mike Thornbury** *November 13, 2014 13:44*

But you have to add in the cost of a Beagle Bone - not a cheap option.



I am not 100% sold on the TinyG, even though I have two of them, but, bang for buck, $129 for a DRV8825-equipped 4-motor driver board with some super-cool G-code algorithms is hard to beat.



One of the great things about this hobby - new stuff coming out all the time and a visible process of continual improvement.



On a budget of close to zip, I can still try out different hardware and software with my machines - you could put together a 3D printer for under $100, with some sweat and salvage.



And that's what makes it so cool. Rules? There ain't no rules! (except, back it up and check your voltages... and maybe some more... like rememeber to eat and sleep... but they may just be service advisories...)


---
**Tim Rastall** *November 13, 2014 19:17*

**+Mike Thornbury** yes to all of that. I didn't realize you had tiny g already. The likes of **+Jason Smith**​ have been using them successfully with the eustanathios I memory serves.


---
**Mike Thornbury** *November 14, 2014 00:26*

I've got lots of stuff :) (don't tell my wife!)


---
**Tim Rastall** *November 14, 2014 00:32*

**+Mike Thornbury** hah. Sure, as long as you don't tell mine ;).


---
**Mike Thornbury** *November 15, 2014 07:58*

The next thing is - which firmware for RAMPS - Repetier, Marlin or GRBL? Any other options?


---
**D Rob** *November 17, 2014 05:33*

I'll be reviewing an Azteeg x3 pro from Panucatt​ here shortly. Lots of room for growth.


---
*Imported from [Google+](https://plus.google.com/101708620681849403392/posts/PEAcr1F2VTq) &mdash; content and formatting may not be reliable*
