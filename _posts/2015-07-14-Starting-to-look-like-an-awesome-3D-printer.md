---
layout: post
title: "Starting to look like an awesome 3D printer"
date: July 14, 2015 23:58
category: "Discussion"
author: Brandon Cramer
---
Starting to look like an awesome 3D printer. I think I spent most of the day tearing the frame apart to get the remaining T-Nuts in for the Plexiglas to attach to the frame. 



Is there a bracket to hold the corner of the Azteeg controller holder (red piece) and the bottom right corner of the Eustathios Name plate on the front. I see there is a hole in both pieces, like there should be something there. 



![images/df4daff4f577751ba097b0ed46aa0242.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/df4daff4f577751ba097b0ed46aa0242.jpeg)
![images/77f4abf7c5474c2c23c954e89610afef.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/77f4abf7c5474c2c23c954e89610afef.jpeg)
![images/405a53d123aaeb832308df43d50c7ba8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/405a53d123aaeb832308df43d50c7ba8.jpeg)

**Brandon Cramer**

---
---
**Eric Lien** *July 15, 2015 00:06*

You are the first one that I know of to do the front logo... Man that looks sexy.


---
**Eric Lien** *July 15, 2015 00:07*

BTW the two small holes near each other are to put a corner bracket if you have one to support the two pieces together.


---
**Brandon Cramer** *July 15, 2015 00:42*

One of those metal corner brackets that I use on the frame? I will have to see if I have any spares tomorrow. 



Thanks! I'm definitely getting closer to having this thing printing! 


---
**Eric Lien** *July 15, 2015 00:59*

Or just print one it's an easy model to make and print. But to be honest it likely isn't even needed.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/GN9bfB1LVi5) &mdash; content and formatting may not be reliable*
