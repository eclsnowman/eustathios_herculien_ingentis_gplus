---
layout: post
title: "Hi peeps, Long time reader, first time poster :) I have built myself a Eustathios (Not v2 as I didn't realise that existed when i ordered all my parts) I was wondering if anyone in the UK could point me to a good place to"
date: February 01, 2016 15:05
category: "Discussion"
author: Adam Morgan
---
Hi peeps,

Long time reader,  first time poster :) I have built myself a Eustathios (Not v2 as I didn't realise that existed when i ordered all my parts)



I was wondering if anyone in the UK could point me to a good place to source the print bed? Currently using a bit of plywood with a heat bed and glass. Whilst I can get ok-ish prints off I would like to improve it as I have issues keeping the bed level. I have looked at the metal beds other people are using however I cannot see how you would sandwich the heat bed between this and the glass (as my 300x300mm heat bed has a lump sticking out the underside where the cables connect which makes things not flat).



Does anyone have 3d files which I could use to get a bed milled or cast with a recess for the heat bed already? (The heat bed is the same as this one: [http://ooznest.co.uk/24V-Silicone-Heated-Bed-300mm?search=heat%20bed](http://ooznest.co.uk/24V-Silicone-Heated-Bed-300mm?search=heat%20bed)) 



Cheers for the help guys! Love the information I have found here so far! 





**Adam Morgan**

---
---
**Daniel F** *February 01, 2016 15:53*

You would glue the heat bed underneath the metal heat bed with adhesive tape (you can order the silicon heater with the adhesive tape). To prevent the beed from radiating downwards you can add cork or some other heat isolation material like rockwool. Then you can either add a special coating for 3d printing like buildTak or any kind of tape. You can also add a 3mm glass plate, I use a 300x300x3mm mirror tile which is cheap, flat and easy to remove with clips. For a build plate of that size I recommend a cast and milled aluminium plate with a thickness of 4 to 6mm to prevent it from bending due to thermal tension (better take 6mm). You don't need a special shape, square is ok. Cast and milled means its really flat. I don't know about UK sources, I got one from Germany: [https://www.aluminium-online-shop.de/de/shop-aluminium-kleinstmengen/Feingefraeste-Gussplatten-_-26/index.html](https://www.aluminium-online-shop.de/de/shop-aluminium-kleinstmengen/Feingefraeste-Gussplatten-_-26/index.html)

This is just to get an idea, unfortunately they only ship to Germany/Austria


---
**Adam Morgan** *February 01, 2016 15:58*

I got to admit, I hadn't considered gluing the heat bed to the bottom of the build plate! I currently use a buildtak on top of my glass. Whilst I love it its starting to get annoying because my crappie implemented bed leveling keeps plunging the nozzle through and melting it! (Another problem for another time :P) In regards to the plate, I had seen your link previously and I have looked at the site, however I will need the plate shaped to fit the mounts and I didn't see an option to send them a 3d cad file to copy. Or have I just missed this out at is was in German? 


---
**Daniel F** *February 01, 2016 17:15*

No, you're right, I think they only cut rectangular shapes--what if you change the mount in a way you can install a square plate?


---
**Ben Delarre** *February 01, 2016 17:20*

If you can't find someone locally to cnc cut the aluminum to shape,  lit is possible (though slow and time consuming) to cut the plate with a hand saw or even a powered jigsaw. Can print out the dxf of the heated bed over a bunch of sheets of a4, glue them to the plate then drill and saw till you have what you need. 


---
**Adam Morgan** *February 01, 2016 18:12*

**+Daniel F** That is an option, however my main aim is to maximise build area, so if possible I was hoping to keep to the Eustathios design. I will be keeping that url to possibly order when I run out of options :)


---
**Adam Morgan** *February 01, 2016 18:14*

**+Ben Delarre** Again a good option that I had considered but I was concerned I would end up bending the sheet when my aim was to get as flat as possible which is why I was hoping to get fabricated by a pro 


---
**Ben Delarre** *February 01, 2016 18:16*

Well the flatness isn't too much of a concern since you'll be putting a glass plate over it which is absolutely flat. If you use handtools on 1/4inch aluminum plate I doubt you'll bend it anyway, pretty sturdy. But yes, getting it machined is obviously better.


---
**Adam Morgan** *February 01, 2016 18:22*

fair point, I will definitely consider that as an option thanks. 


---
**Nicholas Brown** *February 01, 2016 19:30*

I got my aluminium plate from this website in the UK [http://www.aluminiumwarehouse.co.uk/](http://www.aluminiumwarehouse.co.uk/) and then just used a jigsaw to (slowly) cut it out, then lots of filing round the edges.  They do 3 types of aluminium plate, one of which is machined on both sides, but as **+Ben Delarre** mentioned once you put glass on top it does not need to be that accurate, I ended up going for 6082 aluminium, as it was about half the cost and has worked out well for me.


---
**Mikael Sjöberg** *February 01, 2016 22:00*

I bought this type from robotdigg [http://www.robotdigg.com/m/#](http://www.robotdigg.com/m/#)

Quick delivery, though I have had to complement with a double Sided Adhesive Transfer Paper


---
**Adam Morgan** *February 02, 2016 12:22*

Cheers for the suggestions guys, as I have already bought my heat bed, can someone suggest the best adhesive to use to stick it to the metal plate?


---
**Daniel F** *February 02, 2016 12:31*

3M: [http://www.amazon.com/super-strong-double-sided-discounts-digitizers-3M9474-all/dp/B00IQWOA50](http://www.amazon.com/super-strong-double-sided-discounts-digitizers-3M9474-all/dp/B00IQWOA50), but it's not cheap. I think carpet tape should work as well.


---
**Adam Morgan** *February 02, 2016 12:45*

And that will manage the heat ok? 


---
**Adam Morgan** *February 02, 2016 12:56*

[http://www.amazon.co.uk/3M-LS8071-Double-Adhesive-Transfer/dp/B00FS7AVD6/ref=sr_1_fkmr0_2?ie=UTF8&qid=1454417141&sr=8-2-fkmr0&keywords=Sheet+of+3M+9474LE+300LSE](http://www.amazon.co.uk/3M-LS8071-Double-Adhesive-Transfer/dp/B00FS7AVD6/ref=sr_1_fkmr0_2?ie=UTF8&qid=1454417141&sr=8-2-fkmr0&keywords=Sheet+of+3M+9474LE+300LSE)



Found this, would this be suitable?


---
**Nicholas Brown** *February 02, 2016 13:04*

Yep, thats what I used, 300LSE was the thing to search for.  I got mine from here [http://www.ebay.co.uk/itm/3M-300LSE-Double-Sided-Sticky-Sheet-Adhesive-Tape-210mm-x-148mm-A5-Pack-of-5-/141887013675?hash=item210920032b:g:pIYAAOSw~gRVr7Q-](http://www.ebay.co.uk/itm/3M-300LSE-Double-Sided-Sticky-Sheet-Adhesive-Tape-210mm-x-148mm-A5-Pack-of-5-/141887013675?hash=item210920032b:g:pIYAAOSw~gRVr7Q-) as it seemed the cheapest way to do it.


---
**Daniel F** *February 02, 2016 13:06*

My silicon heater came with a pre-installed adhesive and so far it didn't fall off. I stay below 100°C with my heat bed and it seems to handle that temp. 


---
**Adam Morgan** *February 02, 2016 14:04*

Wicked, thanks again guys!


---
**Mikael Sjöberg** *February 02, 2016 20:33*

I bought this one

[http://m.ebay.com/itm/291519172795?_mwBanner=1](http://m.ebay.com/itm/291519172795?_mwBanner=1)




---
*Imported from [Google+](https://plus.google.com/117208540149253709671/posts/TAJkH96YM1F) &mdash; content and formatting may not be reliable*
