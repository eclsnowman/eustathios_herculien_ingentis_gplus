---
layout: post
title: "Is the HFSB5-2020-355-AH177_5 in the BoM for the Eustathios Spider V2 supposed to be HFSB5-2020-355-AH177.5?"
date: October 04, 2015 21:32
category: "Discussion"
author: Matthew Kelch
---
Is the HFSB5-2020-355-AH177_5 in the BoM for the Eustathios Spider V2 supposed to be HFSB5-2020-355-AH177.5?





**Matthew Kelch**

---
---
**Eric Lien** *October 04, 2015 21:46*

 If you go to the Google docs version of the BOM from the links in this group most of the parts are hyperlinked to where you can buy them. 


---
**Sébastien Plante** *October 05, 2015 00:19*

According to the part number, look like it's 2020 Extrusion 177.5mm, from Misumi


---
**Chris Brent** *October 05, 2015 02:38*

Yes it's .5 if you copy and paste the part numbers into their tool there are a couple that don't work because the have _ instead of .


---
*Imported from [Google+](https://plus.google.com/+MatthewKelch/posts/MwY3U1Lz8RL) &mdash; content and formatting may not be reliable*
