---
layout: post
title: "Has anyone purchased this 12V heater mat from RobotDigg?"
date: January 06, 2016 18:03
category: "Discussion"
author: Chirag Patel
---
Has anyone purchased this 12V heater mat from RobotDigg? 

[http://www.robotdigg.com/product/209/200mm+Silicone+Rubber+Heater+Pad](http://www.robotdigg.com/product/209/200mm+Silicone+Rubber+Heater+Pad)



I assumed I could plug this into my RAMPS board like a standard heated bed but the large polyfuse on the board got very hot and stopped powering the mat. Should I try modding my board and replace the polyfuse with a wire? Would I be able to run this mat on 120V with a SSR?





**Chirag Patel**

---
---
**Eric Lien** *January 06, 2016 18:14*

No the resistance would be off to run it at 120v AC. I recommend you get a 120V heater even though I think that's not the answer you want to hear.


---
**Chirag Patel** *January 06, 2016 18:32*

And 30 minutes after posting this I realized I had the bed hooked up to the wrong port on my RAMPS board. I was running the bed off the extruder heater port and was tripping the small polyfuse not the large one. Problem solved and this mat is working perfectly.


---
**Eric Lien** *January 06, 2016 19:05*

I have been using the 120V AC silicon heater mats from Alirubber on Aliexpress for a long time. They have always performed great for me. Cost is only like $35 and you can specify everything you want about them and they are built to order. I made mine for HercuLien 800W, 120V, 380X380mm, dual thermistors (in case one failed), 1.5M cable length for routing to the SSR.



I always talked directly with Daisy Huang (daisyhuang "at" [alirubber.com.cn](http://alirubber.com.cn))



[http://www.alirubber.com/](http://www.alirubber.com/)


---
**dstevens lv** *January 06, 2016 19:27*

The poly fuses on the lower cost RAMPS sometimes aren't up to spec.  You can try to replace it with a like rating (specs in the reprap wiki for RAMPS) or replace the fuse entirely with an automotive type fuse and holder.


---
**Ted Huntington** *January 06, 2016 21:26*

Chirag- how long does the 12V mat take to reach 120C- have you tried that?


---
**Chirag Patel** *January 06, 2016 21:36*

**+Ted Huntington** I'm not sure. After I fixed my problem I ran a PID autotune to 90C which seemed to have heated up a lot faster than my other printer's MK2 heater bed. I'm about to mount my mat and a sheet of PEI to my aluminum bed so I'll see if I can run some timed tests tomorrow.


---
**Ted Huntington** *January 07, 2016 04:06*

**+Chirag Patel** I use 120V (the mains in the US) and for a 300x300mm 500w heating pad it usually heats up to 120C in under 10 minutes, and I wonder if the 12V version can heat up that quickly with 12V.


---
**Kevin Conner** *January 07, 2016 05:58*

**+Chirag Patel**, I recommend wiring your 12v positive lead directly to your power supply. wire the negative leat to the RAMPS for switching.  This bypasses the fuse and all those tiny little circuits. I did this more than a year ago for all my heaters  by recommendation from a #reprap irc member and it's solved all the polyfuse problems inherent to the RAMPS.




---
**Jim Stone** *January 07, 2016 07:11*

If you do that you MUST put a fuse inline so it stays fused otherwise thats dangerous



i run mine like that. i completely removed the fuses and run better blade fuses. the poly fuses are trick finicky things and like to just fail.



another thing i did was removed the stupid main power connector and hard wired it to some powerpole connectors. much better.


---
**Kevin Conner** *January 07, 2016 07:15*

Yes. Add a fuse. I should have included that.


---
**Jeff DeMaagd** *January 07, 2016 15:24*

The cheaper RAMPS polyfuses might be OK with 12V. At 24V it might be an issue because sometimes the assembler cuts corners and uses 16V polyfuses.



I did the Alirubber route too. Custom built to a custom layout per blueprint. Wiring length & type to request. Added a second thermistor for free or $1 more (I forget). Did dual voltage to my request too, rewire it from parallel to series to work with double the voltage. I bought a spare heater incase I screwed up the first. I think it took them 3 days to fabricate. It's been working great, but maybe only 50 hours of power applied so far.


---
**Chris Brent** *January 08, 2016 21:13*

**+Eric Lien** Do you run the printer power supply and the heated bed on the same house circuit. What does an 800W heater plus a the printer power supply get for its peak current? My math can get to 20 amps, but I don't think switching power supplies work they way I think they do. i.e. Does a 24V DC, 400W power supply pull 16.66 amps from the supply side? And if not, how do you get to 400W. I guess I'm asking, how big can you go on your heater in Watts?


---
**Jeff DeMaagd** *January 08, 2016 21:46*

**+Chris Brent**

What is your mains voltage? If 120VAC, the bed takes 6.7A peak. It's only full on for warmup. The rest of the 3D printer probably only adds another amp, so I would say maybe 8A peak for planning. Average current might not even be half that when it's up and running.


---
**Chris Brent** *January 08, 2016 22:38*

Yeah 120VAC. Just wondering what the worst load case could be.


---
**Eric Lien** *January 08, 2016 22:39*

Yeah, on my HercuLien I was running the bed at 24V originally. Hence why I have such a high power PSU on it. But to be honest now that the bed runs at mains a 250W would more than take care of two 40W heaters and 5 steppers.


---
**Chris Brent** *January 09, 2016 20:35*

So I also got concerned because I lived most of my live in 240VAC countries, which usually have 10 or 11 Amp fuses. I only just realized my circuit breakers here in 110VAC land are 20 amp!


---
*Imported from [Google+](https://plus.google.com/101451310698405542910/posts/7ChdVNLgfgx) &mdash; content and formatting may not be reliable*
