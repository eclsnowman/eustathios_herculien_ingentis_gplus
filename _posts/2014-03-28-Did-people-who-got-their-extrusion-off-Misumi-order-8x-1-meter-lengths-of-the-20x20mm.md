---
layout: post
title: "Did people who got their extrusion off Misumi order 8x 1 meter lengths of the 20x20mm?"
date: March 28, 2014 20:17
category: "Discussion"
author: Tony White
---
Did people who got their extrusion off Misumi order 8x 1 meter lengths of the 20x20mm? Or 2x 4m lengths?





**Tony White**

---
---
**William Frick** *March 28, 2014 20:22*

I ordered 2 meter lengths.  Hurry ... First 150 ends soon !


---
**Jason Smith (Birds Of Paradise FPV)** *March 28, 2014 20:23*

I think a lot of folks just get everything pre-cut, tapped, and drilled when ordering from misumi. See the #Eustathios BOM for example part numbers:

[https://docs.google.com/spreadsheet/ccc?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing](https://docs.google.com/spreadsheet/ccc?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing)


---
**Mike Miller** *March 28, 2014 20:59*

I purchased 8 2m lengths. What came is closer to 7 feet tall, and by my gestimations enough for two printers...


---
**Mike Miller** *March 28, 2014 21:00*

I'd recommend ordering t-slot nuts, too, they weren't available anywhere in metropolitan Parker, co. ;)


---
**Nazar Cem** *March 29, 2014 00:00*

I ordered mine only precut. They have holes in the ends already sized for an M5 tap, so I can just do that myself and save cost. I also got rods and a package of 100 M5 extrusion nuts too. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyWhiteMechE/posts/JqCNBEWAYg1) &mdash; content and formatting may not be reliable*
