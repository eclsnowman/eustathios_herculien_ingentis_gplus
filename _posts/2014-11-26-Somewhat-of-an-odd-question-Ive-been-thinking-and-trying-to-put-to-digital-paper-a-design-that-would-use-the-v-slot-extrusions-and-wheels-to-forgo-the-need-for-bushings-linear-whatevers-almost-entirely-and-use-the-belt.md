---
layout: post
title: "Somewhat of an odd question: I've been thinking and trying to put to digital paper a design that would use the v-slot extrusions and wheels to forgo the need for bushings/linear whatevers almost entirely and use the belt"
date: November 26, 2014 19:25
category: "Deviations from Norm"
author: R K
---
Somewhat of an odd question: I've been thinking and trying to put to digital paper a design that would use the v-slot extrusions and wheels to forgo the need for bushings/linear whatevers almost entirely and use the belt system to move the v-wheel platforms along the X/Y/Z axis. 



I noticed the Eustathios design has it set up such that the belt motion on each axis happens on both sides rather than just a single side - I assume this is to stop or prevent one side of the rod from moving faster than the other.



Could this also be mitigated by using extrusions in place of rods? or using double rods? It would reduce somewhat the complexity of the belt-driven system if that were the case.



Cheers!





**R K**

---
---
**D Rob** *November 26, 2014 19:58*

The Ingentis uses the same tried and true setup as the Ultimaker


---
**R K** *November 26, 2014 21:18*

I might be misunderstanding, but it looks like both Ultimaker and Ingentis use rotary motion and pulleys/string to move both ends of the rod at the same time.



I'm hoping I don't have to do that, if I can stiffen up the actual carriage such that the amount of flex/slop from only moving one side of the rod is minimal.



If there's no way around that, then its fine, I'll figure something out. Was just curious


---
**D Rob** *November 26, 2014 21:30*

Lay a pencil on your desk. Push one end to roll it. What happens? This causes a cantilevered force. One side moves forward but the other resists. With out equalizing the force the system cannot operate. There are two ways to equalize the force. Have the ends forced in tandem, or push from the center. The former is the most viable technique.


---
**R K** *November 26, 2014 21:38*

Hm. Good point. Part of me is hoping that by having - well to keep with the analogy, two pencils with a strong connector between the two (e.g. the hotend carriage) might mitigate this enough where it wouldn't effect build quality.



That said its probably not worth that extra hassle. Will think of something to get around it :) Thanks


---
**D Rob** *November 26, 2014 21:41*

The Ingentis will not move if it it's unevenly pushed this can break printed parts or even bend rods


---
**Dale Dunn** *November 26, 2014 22:27*

It is possible to drive from one side with appropriate bearing design, but the mass of material and complexity needed to make the system rigid enough for quality prints would negate the benefits of driving it from a single side.


---
**hon po** *November 27, 2014 02:23*

Being the proud owner of a makibox (my first printer). I'd say no, no, no, no, no....please!!!



Don't go down this path.


---
**R K** *November 27, 2014 13:22*

hon po, i'm assuming you mean 'don't try to drive it from one side' and not 'don't use extrusions/vrails/ :)


---
**Mike Miller** *November 27, 2014 15:11*

**+D Rob** How are those wormscrews working out? Having a single wormscrew in each direction 'pushing from the middle' might be a kinky design. 


---
**hon po** *November 27, 2014 15:18*

**+R Kumar** Of course you are correct. I think I am naturally drawn to the cantilever effect and forget your other question. Give you another analogy, find a long stick and put some weight on one end, now try to move that weight horizontally by holding the other end. You will need exceptionally strong wrist to keep that weight synchronize with your hand. If you have strong hand, then try to move faster.



I like v wheel & v slot, they are quiet but expensive in my region. Also, their use in cartesian usually need more space for the gantry, less build volume.


---
*Imported from [Google+](https://plus.google.com/102580771315197780541/posts/FjQ8cpbRwAf) &mdash; content and formatting may not be reliable*
