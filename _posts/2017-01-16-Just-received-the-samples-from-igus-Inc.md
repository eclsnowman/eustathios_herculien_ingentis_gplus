---
layout: post
title: "Just received the samples from igus Inc"
date: January 16, 2017 19:19
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
Just received the samples from **+igus Inc.** today and put them straight into my first print. Fits quite nice and feels great though they have a little bit of play. At the end its still less than I had with the LM8LUU before. It's really hard to get some good ones of these linear ball bearings.



How's the play with the bronze bushings you guys are using?



For reference: These Igus ones are Igubal ECLM-08-02. If they work well here I will probably also give them a try on the 10mm rods on the outsides.

﻿



![images/61b5b0679b283e138287b0284d5020a2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/61b5b0679b283e138287b0284d5020a2.jpeg)
![images/a1c344f320e06fbbae564507844a99e0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a1c344f320e06fbbae564507844a99e0.jpeg)

**Frank “Helmi” Helmschrott**

---
---
**Eric Moy** *January 18, 2017 02:12*

Have you measured the diameter of your rods, sometimes they are slightly undersized, depending on where you sourced them


---
**Jo Miller** *January 18, 2017 18:45*

zero play on my herculien bronze bushings 


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/6Z2JwH7jXFP) &mdash; content and formatting may not be reliable*
