---
layout: post
title: "Holy Crap I might actually finish this thing before MRRF"
date: March 12, 2015 17:46
category: "Deviations from Norm"
author: Joe Spanier
---
Holy Crap I might actually finish this thing before MRRF. right now the printable dimensions are looking like 14x14x19ish". Also known as bigger then Im ever gonna print :D



This is also the farthest Ive made it through a project in fusion360. Which I think is an accomplishment in itself. And all the joints move like the should. 



If anyone wants to watch the progress on this project through the fusion collaboration tools shoot me a pm and Ill add you. 



![images/a9c01ad224d0518e9d6f06326ca46051.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a9c01ad224d0518e9d6f06326ca46051.png)
![images/be03de49567c88b7283878b4c1d7e8a1.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/be03de49567c88b7283878b4c1d7e8a1.png)
![images/09989ea37d5e562d76de1a3cf2999126.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/09989ea37d5e562d76de1a3cf2999126.png)
![images/2aecf2816f94f5cad37a089afc24bcd9.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2aecf2816f94f5cad37a089afc24bcd9.png)
![images/ca469fd60edfa3f2cea9f1fa43748211.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ca469fd60edfa3f2cea9f1fa43748211.png)
![images/c165241e45c0c17eac2c3947a781ccbc.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c165241e45c0c17eac2c3947a781ccbc.png)

**Joe Spanier**

---
---
**Eric Lien** *March 12, 2015 21:10*

I love it. This would save a lot of cost to someone building an xy gantry printer. Plus way less printed parts for someone just getting into it.


---
*Imported from [Google+](https://plus.google.com/+JoeSpanier/posts/9kQVK2dV3sC) &mdash; content and formatting may not be reliable*
