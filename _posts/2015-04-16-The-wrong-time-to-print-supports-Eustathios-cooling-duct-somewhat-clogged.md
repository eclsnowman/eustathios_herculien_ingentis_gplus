---
layout: post
title: "The wrong time to print supports. Eustathios cooling duct, somewhat clogged"
date: April 16, 2015 00:08
category: "Show and Tell"
author: Rick Sollie
---
The wrong time to print supports. 

Eustathios cooling duct, somewhat clogged.

![images/a65c7286b5812077c7816b29e377a3bf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a65c7286b5812077c7816b29e377a3bf.jpeg)



**Rick Sollie**

---
---
**Eric Lien** *April 16, 2015 00:30*

Yup, this one should be able to print totally unsupported. Also there is some brim already modeled into the stl, so it should be ready to print. Is that PLA? If so I might recommend abs for that part due to it's location by the heat.


---
**Rick Sollie** *April 16, 2015 00:32*

Thanks for the heads up on the heating issue. I will have go to the lab for this piece.


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/1DuX19JVAkk) &mdash; content and formatting may not be reliable*
