---
layout: post
title: "I own the Pegasus 12\" printer and I would like to one from this group"
date: May 13, 2016 23:19
category: "Mods and User Customizations"
author: Dovid Teitelbaum
---
I own the Pegasus 12" printer and I would like to one from this group. I would like to keep as many parts as possible. I also want to make it a heated chamber. Can anyone suggest a proper build for me [http://www.makerfarm.com/index.php/12-pegasus-kit.html](http://www.makerfarm.com/index.php/12-pegasus-kit.html)





**Dovid Teitelbaum**

---
---
**Mike Miller** *May 13, 2016 23:28*

That's one way to reduce vibrations...I guess. 




---
**Dovid Teitelbaum** *May 13, 2016 23:58*

It really is a solid frame. 


---
**Sean B** *May 14, 2016 00:55*

Those are good vibrations




---
**Sébastien Plante** *May 14, 2016 01:51*

why is she jumping with a printer on her head? and why did you post the picture reversed? 


---
**Pieter Swart** *May 14, 2016 10:11*

**+Dovid Teitelbaum** If this is the only printer you have and you do not have free access to another 3d printer, or somebody that can print parts for you, it might be a good idea to rather keep this printer in tact so that you can print the parts that you need on the new printer.


---
**Sean B** *May 14, 2016 13:32*

Yeah I agree with Pieter, unless you are completely strapped for funds.  You could potentially print all the needed parts ahead of time, but it would be very difficult scavenging parts.  Do you know which printer you want to build?  Also, why do you want to build one?  I've heard good things about the Pegasus.


---
**Jeff DeMaagd** *May 14, 2016 20:13*

Seems so weird to have an exo frame with a sliding bed.


---
**Dovid Teitelbaum** *May 15, 2016 17:51*

I happen to have another printer but I guess I will print the parts out first anyhow. I like the printer but I dont like that the y bed moves because i cant properly enclose it. Thats the biggest reason but I also like the ultimaker type gantry with these printers seems to have.


---
*Imported from [Google+](https://plus.google.com/+DovidTeitelbaum/posts/XCB2ZM2fYBA) &mdash; content and formatting may not be reliable*
