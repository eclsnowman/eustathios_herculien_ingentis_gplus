---
layout: post
title: "BTW big props to whomever made the Herculien BOM with links included"
date: January 20, 2015 20:04
category: "Discussion"
author: Daniel Salinas
---
BTW big props to whomever made the Herculien BOM with links included.  Made things really really easy to start chasing down parts.





**Daniel Salinas**

---
---
**Brandon Satterfield** *January 20, 2015 20:07*

That would be "THE GREAT AND POWERFUL OZ" or **+Eric Lien** for short.


---
**Daniel Salinas** *January 20, 2015 20:17*

well everything except 2 belts and the extrusions are already in the air on their way to me.  Parts are printing as I type.  So excited to get started on this thing.


---
**Eric Lien** *January 20, 2015 20:31*

**+Daniel Salinas** keep us posted with updates, question, notes, and results. Now that this printer will be in a few makers hands I can get feedback on how to make it better.


---
**Eric Lien** *January 20, 2015 20:32*

Also if you find any cheaper sources let me know.


---
**Daniel Salinas** *January 21, 2015 22:10*

I got the first batch of stuff today.  Just hardware and some electronics.  The suspense is killing me.  I'll definitely be taking my time with this as much as possible.  I was thinking about getting a build guide started with pics of each step.  I've never tried doing something like that but it would be a way to contribute something to the project.


---
**Eric Lien** *January 21, 2015 22:51*

**+Daniel Salinas** thank you. I have wanted to make better guides, but I haven't had time to disassemble the printer for pictures. It has been running non-stop since I built it.


---
**Jim Wilson** *January 22, 2015 15:34*

**+Martin Brown**​ and I are going to be building two Eustathioses (Eustathiosii?) Late Jan or mid Feb and will be also focusing on nice assembly pics and steps as we go, but if anyone is more impatient they're welcome to beat us to it ;)


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/MERDuFQKrxB) &mdash; content and formatting may not be reliable*
