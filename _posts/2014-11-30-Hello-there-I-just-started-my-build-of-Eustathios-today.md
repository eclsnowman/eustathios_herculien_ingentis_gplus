---
layout: post
title: "Hello there. I just started my build of Eustathios today"
date: November 30, 2014 20:53
category: "Discussion"
author: Florian Schütte
---
Hello there.

I just started my build of Eustathios today. I have some questions about the pulleys. The 32teeth/10mm ones are hard to get. Has anyone experiences with other dimensions of pulleys? Or is there an other source for the 32T/10mm  than aliexpress?

I will also be happy about some suggestions for shopping parts like belts/bearings/motors/hotend.

FYI: the parts for the frame are ordered at [http://www.smt-montagetechnik.de/](http://www.smt-montagetechnik.de/) (see partlist pic).﻿



![images/d7badc17cd6ba54dff96c2806edcf07c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d7badc17cd6ba54dff96c2806edcf07c.jpeg)
![images/ef75cd1426595059f98982474074707f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ef75cd1426595059f98982474074707f.jpeg)
![images/6eb6808d338ea27a1bcd0f86230214c1.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6eb6808d338ea27a1bcd0f86230214c1.png)

**Florian Schütte**

---
---
**David Heddle** *November 30, 2014 21:42*

[http://www.robotdigg.com/product/149/10mm-bore-32-teeth-gt2-pulley](http://www.robotdigg.com/product/149/10mm-bore-32-teeth-gt2-pulley) they do have an ali-express shop but last time I checked their website was cheaper


---
**Florian Schütte** *December 01, 2014 01:34*

much cheaper. thank you. Are there any disadvantages when using 20 teeth pulleys? Speed? And 36 teeth? to big? 


---
**Eric Lien** *December 01, 2014 06:23*

20 tooth on the stepper then 32 on the rods yields a nice balance of top speed along with the added resolution of the gear reduction.



One note is you will need two pullies with no collar (i.e. the misumi ones where the grub screw is in the tooth area) or the belts won't line up with the printed parts. These two go in the corners where the stepper drives up to the shaft.


---
**Frank “Helmi” Helmschrott** *December 01, 2014 13:37*

Hey **+Florian Schütte** - i also just started planning with 10mm rods and 10/32 pulleys. Ordered them on robotdigg too and there prices are really great. Be aware that shipping to germany is quite high. They only send with DHL express which makes it around 25-35$ additional depending on the amount of stuff you're ordering. Good thing is that everything normally arrives within a week after ordering. 



What size are you building? Still thinking if i should go with the 760 or the 610 teeth closed belts or just use open ones. For the closed i'd probably do the smaller size as i don't want so much footprint. Anyways... just started planning.


---
**Rick Sollie** *December 01, 2014 14:40*

I'm ready to start ordering parts for my build as well and had a question. Has anyone tried using 10mm tubes versus the solid rods? Would they be strong enough or is the extra weight no issue?


---
**Eric Lien** *December 01, 2014 14:48*

**+Rick Sollie** the 10mm side rods would be better solid. They are fixed to the frame on bearings and hence weight is not a real concern like it is on the two cross bars.


---
**Florian Schütte** *December 05, 2014 12:41*

**+Eric Lien** when i use 20T pulley on the steppers, i need shorter belts, right? do you have specifications for them?


---
**Eric Lien** *December 05, 2014 17:38*

**+Florian Schütte**​ all the motors should be able to slide to tension them. And the ones on the gantry are open belts cut to length and tensioned.


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/TZWbpqdLmSL) &mdash; content and formatting may not be reliable*
