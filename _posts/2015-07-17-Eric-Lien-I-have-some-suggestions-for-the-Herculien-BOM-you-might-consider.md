---
layout: post
title: "Eric Lien I have some suggestions for the Herculien BOM you might consider"
date: July 17, 2015 14:07
category: "Discussion"
author: Zane Baird
---
**+Eric Lien** I have some suggestions for the Herculien BOM you might consider. I don't believe the M4 screws used to attach the 24V power supply are included in the BOM, it may be helpful to add those. Also, I was looking for alternative shims for the pulleys and came across these:

[http://us.misumi-ec.com/vona2/detail/110100143060/?ProductCode=LRB10-0.5](http://us.misumi-ec.com/vona2/detail/110100143060/?ProductCode=LRB10-0.5)



These are $0.23 when ordering 20 or more so it's less than half the price of the McMaster shims for twice the quantity. Additionally, McMaster lists the ID tolerance of the shims as 10 +/-0.3mm, whereas the Misumi shims have an ID tolerance of 10 +0.10/+0.15mm so they are sure to fit on the rods listed in the BOM. 





**Zane Baird**

---
---
**Brandon Cramer** *July 17, 2015 14:22*

I did have to go purchase 2 M4 screws to attach the power supply. 


---
**Frank “Helmi” Helmschrott** *July 17, 2015 16:20*

Good find, **+Zane Baird** - additionally everything that is not McMaster helps us non-US-guys as McMaster only delivers inside the US.


---
**Eric Lien** *July 17, 2015 16:42*

**+Zane Baird** Thanks. I will try to get it added over the next few days. I have just been short on time lately. I have several items of cleanup on these two printers that needs to get done. 


---
**Eric Lien** *July 17, 2015 17:23*

**+Frank Helmschrott** can you share your sources and prices for international customers. I can add that to the sheet.


---
**Frank “Helmi” Helmschrott** *July 17, 2015 19:55*

**+Eric Lien** sure i will. I just finished cabling mine today for now and did the first movement tests. as soon as i know everything is running fine i can sum up where i sourced my stuff.


---
**Eric Lien** *July 17, 2015 20:26*

**+Frank Helmschrott** Great Thanks.


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/WZmX2xkH3d9) &mdash; content and formatting may not be reliable*
