---
layout: post
title: "Finally got around installing Walter Hsiao 's mini space invaders carriage, while redoing a few other bits and pieces"
date: August 28, 2016 05:24
category: "Show and Tell"
author: Oliver Seiler
---
Finally got around installing **+Walter Hsiao**'s mini space invaders carriage, while redoing a few other bits and pieces. Such a great upgrade!

![images/e8422da73e7420cbda80ae33388c0731.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e8422da73e7420cbda80ae33388c0731.jpeg)



**Oliver Seiler**

---
---
**Eric Lien** *August 28, 2016 11:23*

Looks great. Plus you can actually see the first layer. I have thought of doing this myself. I already have my HercuLien, so I can easily handle the small loss in X/Y travel. Plus it just plain looks cooler than my design, in addition to the extra cooling.


---
**Roland Barenbrug** *August 28, 2016 18:10*

Question, can this carriage be combined with the E3D Volcano or does this require a redesign?


---
**Eric Lien** *August 28, 2016 18:33*

**+Roland Barenbrug** it can be used with a volcano. There is an elongated lower duct that moves the outlets lower down and inwards towards the heatsink since it is narrower.


---
**Eric Lien** *August 28, 2016 18:34*

The lower duct is removable, held on by magnets.


---
**Oliver Seiler** *August 28, 2016 19:09*

**+Roland Barenbrug** haven't tried the volcano, but have a look here [http://www.thingiverse.com/thing:1358895](http://www.thingiverse.com/thing:1358895) or in the Github repository.


---
**Oliver Seiler** *August 28, 2016 19:11*

**+Eric Lien** I need to double check, but it seems that I have actually gained some room on the Y axis and lost nothing on X. Visibility of the print is much better, though a bit limited by the angle you're looking from. The lights really help!


---
**Eugene Lee** *August 29, 2016 02:09*

I'm trying to decide if I need this mod. I can't work out where the LEDs go. Also what are the holes directly under the bearings for? I assume they're vent holes? Can I get away with using 4010s fans?



Has anyone tried this with the stock leadscrews? Walter says it might not work because the hotend sits a bit higher. Can anyone confirm?


---
**Oliver Seiler** *August 29, 2016 02:43*

**+Eugene Lee** the LEDs go in the two rectangle shaped holes on the back sides, I could just push them in. The holes under the bearings must be for ventilation, although there's not that much air coming through. I used bog standard fans from AliExpress.

When upgrading the carriage I also upgraded to ball screws, but I am pretty sure that if you use the bed supports from Walter's ball screw mod, that allow you to mount the bed a little higher, then you'll be fine. Happy to take more photos later when I'm back home.

[https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/walterhsiao/1204%20Ballscrew%20Bed%20Support](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/walterhsiao/1204%20Ballscrew%20Bed%20Support)


---
**Eugene Lee** *August 29, 2016 03:03*

**+Oliver Seiler** Thanks a lot! Yes more pictures would help. Still a bit confused about the LED, do they face inwards so that they illuminate the entire carriage? That means you would have to use transparent filament.



Sorry, when i said "4010s" I should have said 40mm blowers (instead of the 50mm walter uses).



On a separate note, is it worth moving from lead screws to ball screws?


---
**Oliver Seiler** *August 29, 2016 06:19*

**+Eugene Lee** printing in transparent or translucent filament would make sense, except when you plan not to use the LEDs - but I find the lights really handy (and they look cool).

I use 50mm blowers on top - I guess you could use smaller ones if you could fit them into the holes - but why bother when you can buy them cheaply?

I can't attach photos to the original post, but here's a link: [https://goo.gl/photos/hjaMiGHyAWQn629A6](https://goo.gl/photos/hjaMiGHyAWQn629A6)

I had to move the bed to the upper holes in the new Z bed holders to fit the carriage.

About the ball screws - I can't really comment yet. They run very well, but my pevious lead screws were working mostly fine, too.


---
**Benjamin Liedblad** *September 26, 2016 18:18*

Just started building an Eustathios V2 from the Spider documentation... Ordered my frame last night. 



Woot Woot!



Anyway... 



Did some printing ahead of time and decided to print out this mod. All the parts came out great and bushings align perfectly. I'm having a hard time working out how the E3D hot end is held in place.  



I'm not sure if I need 6 inch (exaggeration) screws, if my screw holes didn't turn out right, or if I'm supposed to drill out the holes to a certain depth. 



For anyone who has this mod, what size screws did you use to hold the hot end? 



Do the screws just press the small pieces up against the hot end heat sink?


---
**Oliver Seiler** *September 26, 2016 18:28*

Yes, you will need long screws (don't drill out the holes) and have them press against the two little parts. At least that's what I've done and it works fine;)

I have made a slightly different lower holder that links into the fins of the E3D making it a little less fiddly to hold in place.

Make sure the lowest fin of the E3D aligns with the bottom of the carrige when installing it - if it's a little too high or low then the hotend gets slightly misaligned.


---
**Oliver Seiler** *September 26, 2016 19:07*

![images/213bfc67750ba6cc60ebc9b3ba1fa6a5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/213bfc67750ba6cc60ebc9b3ba1fa6a5.jpeg)


---
**Oliver Seiler** *September 26, 2016 19:07*

![images/f36d9263e898d3753c6722e4f7f294cd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f36d9263e898d3753c6722e4f7f294cd.jpeg)


---
**Oliver Seiler** *September 26, 2016 19:07*

![images/fc6da79a8fef2b84a8987c7816432d7a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fc6da79a8fef2b84a8987c7816432d7a.jpeg)


---
**Oliver Seiler** *September 26, 2016 19:07*

![images/999838850447e07d9828d044e254e12d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/999838850447e07d9828d044e254e12d.jpeg)


---
**Oliver Seiler** *September 26, 2016 19:08*

![images/a64bcfd4e8c3b1d210d52caa2ef5e694.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a64bcfd4e8c3b1d210d52caa2ef5e694.jpeg)


---
**Oliver Seiler** *September 26, 2016 19:09*

I'm using 40mm screws but you can see the that the upper one is too short and I've wedged in a little block that I had lying around from a test print. Hope the photos help. 


---
**Benjamin Liedblad** *September 26, 2016 19:11*

Perfect! Thanks.


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/UduPCz8UnFN) &mdash; content and formatting may not be reliable*
