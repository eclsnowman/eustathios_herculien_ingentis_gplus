---
layout: post
title: "Big fan of \"the ideal\" and \"getting as close to perfection as possible: I have a question which I think we could ALL benefit from knowing the answer to: The Ingentis ( / UI-T-Slot / Eustathios ) is worlds ahead of my current"
date: March 13, 2014 01:28
category: "Discussion"
author: Jarred Baines
---
Big fan of "the ideal" and "getting as close to perfection as possible: I have a question which I think we could ALL benefit from knowing the answer to:



The Ingentis ( / UI-T-Slot / Eustathios ) is worlds ahead of my current Mendel machine. Comparatively, it is much more ideal, using the x/y gantry design and being driven simutaneously from both sides which disallows the 'twisting' action when the drive belts are only attached on one side - Reducing vibration, allowing higher speeds etc.



Who knows / cares to help speculate, which of these pictured setups is better for the X/Y ends?



I am of the understanding that the position of the belts in both the pictured cases is slightly inferior to the Eustathios, where the belts are on the OUTSIDE of the linear rails. The belts on that machine are also (look like) driving at the same Z-height as the linear rails - In the two designs pictured the belts attach ABOVE the X/Y ends, which means when the belt 'pulls' the x/y end, it starts to "tip it over" a little, then the end catches up and moves. The amount it 'tips over' by is MICROSCOPIC... but - it happens nonetheless... and if it can be avoided, ideal conditions can be met and the highest possible speed and accuracy are achieved! (yay!)



I need more minds to weigh in on this!



**+D Rob** **+Whosa whatsis** **+Tim Rastall** **+Jason Smith** **+Eric Lien** **+Anthony Morris** **+Thomas Sanladerer**  + Anyone else who might know!



Have a look at the diagram and description of this CNC machine:

[http://www.alfametalmachinery.com/en/select_by_brand/you%20ji/cnc%20horizontal%20machining%20center/HMC-Ram%20Spindle/162/0/#structure](http://www.alfametalmachinery.com/en/select_by_brand/you%20ji/cnc%20horizontal%20machining%20center/HMC-Ram%20Spindle/162/0/#structure)



It shows how, being driven from 2 'ends' rather than 1 end affects vibration, stability (and therefor max speed and accuracy).



In the first pictured X/Y end I've just posted, the first and second linear rails are also on the same PLANE... I THINK this is more ideal than the second type in which the bar is offset from the plane of motion, but, I'm not SURE!



Let's discuss and determine the ideal setup, then I, Tim or anyone else can come up with a design that will perform the closest to 'ideal'.

![images/2c4ae2e37c0e1fedc28d8ab3b7707ea8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2c4ae2e37c0e1fedc28d8ab3b7707ea8.jpeg)



**Jarred Baines**

---
---
**Tony White** *March 13, 2014 01:46*

The blue version requires more of a length tolerance for your shafts - but as all the motion occurs on that plane, I can see an advantage in high acceleration situations -  especially if you can get the c.g. of your effector on the same plane as well!


---
**Brian Gudauskas** *March 13, 2014 01:51*

Blue is better, as you say... no tipping which would encourage binding.  Also I would think it would help square the side rails on setup.


---
**Jarred Baines** *March 13, 2014 01:59*

That's what I am thinking **+Anthony White** - the length tolerance on the bar WOULD need to be accurate, but also - the cross rail will support the outside linear rail from flexing inward - another plus for stability! Placing the drive (effector) directly opposite from that cross-bar (on the outside of the x/y end, same z level as the bars) seems like the best of all 3 designs (sorry no Eustathios picture, refer to "tinyg" video a few posts back ;-)


---
**Tim Rastall** *March 13, 2014 02:13*

The blue version is using straight bronze bushings like the Ultimaker. My version on the right uses self aligning bushings.  I actually like the blue one but I don't see now you secure the spectra.  Hmmm,  actually it won't work as the spectra that spans the 2 shafts will fowl against the plastic.  This is why there is a curved notch on the outer edge of the one on the left.  I originally started with Sublime xy ends and only made minor mods as I didn't see the point of reinventing the wheel. There are better ways to capture spectra line than Sublime method imo. 


---
**Eric Moy** *March 13, 2014 02:13*

The blue is better as is does not apply moment on the plastic part when moving. In terms of length tolerances, the blue design does not need to be tight at all. Simply make the sockets/ferrules deeper than they need to be and as a screw and nested nut to clamp the rod in place. You just need to get the blue parts into the correct position by moving the stages back and forth while gradually tightening all the clamps


---
**Mike Miller** *March 13, 2014 02:21*

The Blue one is very unforgiving of imprecise bearing rod lengths. I dunno if that's an issue or not, but the Grey one allows some overhang. 


---
**Whosa whatsis** *March 13, 2014 05:11*

The consensus seems to be that the blue is better, so I'll concentrate on the potential issues with that design.



First as others have stated, the length of the rod is crucial with the blue design whereas the other is tolerant of length differences. Even if the length is perfect, any flexing of the outer rods could cause it to become loose or even fall out. This is made much easier by any looseness in the part surrounding the end of the rod, and such looseness would allow the blue piece to pivot around the outer rod (resulting in the end of the inner rod moving up and down. This also means that any up/down force could cause it to loosen over time.



Two ways to strengthen it against this type of loosening are to lengthen the part so that encloses more of the inner rod (this will of course reduce the travel space available to the effector) and to make a teardrop shape (or a polygonal hole) with a corner that the rod can be pressed into with a set screw so that it is pressed between five points (two pairs of points colinear, parallel to the axis of the inner rod and the fifth point, the set screw, on the opposite side, centered between the other four).



This version also makes it harder to put the attachment of the drive belt/line as close as possible to the attachment point of the rod.



I would go with a design closer to the grey one, but extended to wrap around more of the end of the rod to fight any flexing in that mounting point. I would also carefully design the shape (possibly printing in multiple parts) to ensure that both the bushing and the rod are held <i>firmly</i> at right angles to one another, with the tight clamping of each keeping it from rotating around the other.



Of course, if the outer rod could actually pass through the inner one so that there is no joint between the two to potentially flex, all of these problems would go away, and the ideal attachment point of the belt/line would be through the center of the inner rod, outside the outer rod. This means that the inner rod would have to be a larger diameter, which would normally mean more mass (a bad thing), but if you used a hollow tube, I think this would get you pretty close to "the ideal".


---
**Nazar Cem** *March 13, 2014 06:00*

Hm, I noticed on the Ingentis that only one end of the x/y rods is clamped down to keep the rod from binding. The blue design doesn't have any clamping, only press fit(unforgiving of loose tolerances for movement reasons), and as long as the rod is the right length, it'll be constrained and still be free to move slightly if it needs to. Is there a flaw with this idea?



You can also just tie the spectra right onto the clamping screws.


---
**Whosa whatsis** *March 13, 2014 06:04*

**+Nazar Cem** If the rod is loose enough to slide in and out of the hole, it will also have room to wobble (and more, because of flexing plastic), making the inner rod unstable in the Z direction.


---
**Nazar Cem** *March 13, 2014 06:24*

**+Whosa whatsis** Ok, so the solution is to use a screw to clamp it as you said? Would you need one on both ends or only one?


---
**Nazar Cem** *March 13, 2014 06:48*

like this? [http://imgur.com/HGF6OZf](http://imgur.com/HGF6OZf)


---
**Tim Rastall** *March 13, 2014 07:01*

**+Nazar Cem** yep, that would do it.


---
**Whosa whatsis** *March 13, 2014 07:06*

No, that's not good because you're pressing it into a flat side. You want to press it into a corner to get a solid connection, so you should use a teardrop shape or a polygonal hole.


---
**Tim Rastall** *March 13, 2014 07:08*

Also, the spectra needs to be very tight so you have to keep it under tension then tighten the screws to clamp it in place. I would make the gap small enough to allow it to completely close once tightened.  I might even introduce a toothed/ridged profile to the inner surface to improve grip on the line. 


---
**Tim Rastall** *March 13, 2014 07:33*

Hmmm. **+Nazar Cem** on second thoughts that sort of nut trap screw clamping thing is prone to splitting the plastic if you over tighten.  Id go for a clamping solution similar to the way you are clamping the bushings.


---
**Nazar Cem** *March 13, 2014 08:28*

**+Whosa whatsis** Oh I see what you mean. [http://imgur.com/a/9V2M2](http://imgur.com/a/9V2M2)



**+Tim Rastall** To tighten the line, could you tie it to one or both of the screws, wrap it around the shafts, wrap it around the screw(s) on its way back, and pull on it lawnmower style and make a knot?


---
**Whosa whatsis** *March 13, 2014 08:33*

Come to think of it, what I said earlier about hollow tubes being ideal would be perfect for use with this: [https://plus.google.com/116889746506579771100/posts/8ZmKAXfWphz](https://plus.google.com/116889746506579771100/posts/8ZmKAXfWphz)


---
**Whosa whatsis** *March 13, 2014 08:36*

**+Nazar Cem** Your first picture there is what I was talking about, yes. Your second picture will not work, as the hole will be tight at the opening but looser at the back, so the mouth of the hole will be a pivot point for exactly the type of play I was warning about.


---
**Jarred Baines** *March 13, 2014 09:32*

[http://i.imgur.com/LKLrJHF.jpg](http://i.imgur.com/LKLrJHF.jpg)

[http://i.imgur.com/sY74zYI.jpg](http://i.imgur.com/sY74zYI.jpg)



These are pics of the mockup I just did, the effector is on the same axis as both rods and since there is an effector on both sides, it is driven from the centre of gravity :-)



The hole in mine is not teardrop shaped, but it should be, other than that... What do you guys think?


---
**Tim Rastall** *March 13, 2014 09:39*

**+Jarred Baines** Looks good and should print well on it's end. Only comment is that nut trap doesn't have enough plastic above it. You'll push the nut through (or crack) the plastic if you overtighten.


---
**Tim Rastall** *March 13, 2014 09:42*

BTW If I sound like I'm going on about those  M3 nut traps it's only because I've learned through bitter experience to avoid them.


---
**Jarred Baines** *March 13, 2014 09:51*

Yep... I noticed it was too thin but left it for the purpose of the mockup, the counterbores for the cap screws need more meat also ;-) revision 0.0.1 ;-)


---
**Nazar Cem** *March 13, 2014 10:08*

now with more nut traps... [http://i.imgur.com/Bo2cI4W.jpg](http://i.imgur.com/Bo2cI4W.jpg)


---
**Dale Dunn** *March 13, 2014 12:24*

This conversation hasn't yet touched on this idealized configuration being in competition with the goal of maximizing build volume within the machine envelope. Stacking the rods has a proven track record with Tantillus and now Tim's Ingentis. Is there a problem here that needs to be solved by giving up 20-25 mm of X/Y travel?



What I see in the Tantillus design (gray) is the drag of the bushing being added to the larger torque that exists in both designs caused by the line attachment being offset from the crossbeam's load. It seems what we want is the line to attach at the same elevation as the crossbeam. WIth the rods stacked, it should attach slightly below the crossbeam to somewhat balance the inertial loads with the drag from the bushing. Edit: Xmaker has this, but gives up a lot of build volume ot do it.


---
**Whosa whatsis** *March 13, 2014 17:02*

I briefly mentioned the loss of build volume caused by extending the part.



While the iterations being posted here are getting better, I still don't think that any design could be called ideal in which the two rods don't cross. Some part of the inner rod, not a piece of plastic attached to it, extends outside the inner rods.



If the rods don't cross, the attachment point of the belt/line should be on the same side of the outer rod as the end of the inner rod, not the opposite side.


---
**Dale Dunn** *March 13, 2014 18:26*

Sorry, I missed your mention of the volume reduction. I really don't disagree with your assessment.


---
**Tim Rastall** *March 13, 2014 19:47*

**+Whosa whatsis** this is where we get back into looking at the whole system. Anchoring the line or belt on the inside of the xy end will reduce the xy build volume because of where you will have to locate the pulleys/Spectra winding.  Of course,  this I one of the dangers of designing a part in isolation.

Actually,  that brings up a point I was intending to make about system design and how your initial requirements define the level of deviation from an ideal configuration for each component. I'll do that in another post to avoid hijacking this one further. 






---
**Nazar Cem** *March 13, 2014 20:51*

If you use 8mm rods you can use 688zz bearings and shove them as close as possible into the corners. The travel range can be restored back to original.﻿


---
**Jarred Baines** *March 13, 2014 23:08*

Build volume, in my opinion (and I understand not everyone will feel this way) comes second to quality. I would always focus on quality first, then if the build volume was not adequate, scale the design to suit... This is why I haven't even CONSIDERED that I'm cutting into the build envelope at this point, that is a trivial thing to overcome later.



I was under the impression that driving the axis from the furthest point from the center gives the best allowance for belts stretching / mechanics flexing - for example:



If the 2 drive points are only 5mm from the COG and when in motion your left effector takes .1mm more movement to 'move' due to the overall backlash between the left and right pulleys, this magnifies toward the outside, with 450mm rods you will be twisting the axis by 4.5mm in this extreme case...



Point being; the further outboard your effector is, the less you are effected by backlash and rigidity issues... Although **+Whosa whatsis** is probably right in that the more rigid point is close to the rod instead of my original design...


---
**Jarred Baines** *March 14, 2014 01:39*

What I was saying is, if you have a pencil on a table and with 2 fingers spaced only 10mm apart you push the pencil, if one finger pushes .1mm MORE than the other finger, the ends of the pencil will magnify this and you will have (if your pencil is 450mm long) around 4.5mm difference in how far the ENDS have moved...



If you push the pencil from its putter edges and you have this .1mm error, your total error is pretty much .1mm instead, as it won't be magnified ;-)


---
**Jarred Baines** *March 14, 2014 01:56*

I like to use extreme cases to show points I'm trying to make, if it actually moved by 4.5mm you'd have an EXTREME problem! But extreme cases are simply magnified versions of less extreme cases... By pushing that pencil at all the different points you would find the further toward the outside you push, the more accurate and smooth the movement... It could end up being a factor effecting speed and accuracy or introducing vibrations etc.


---
**Whosa whatsis** *March 14, 2014 04:30*

**+Jarred Baines** good point. I hadn't been thinking in terms of a machine that can have that kind of error. This spectra thing is still really weird to me, and I always just think of belts.



On the other hand, you have to think about how this type of error will interact with the (slop or lack of slop) that there might be in the bushings that there might be in the bushings and in the attachment point between the rod and the bushings. Will the bushings angle themselves to stay perpendicular to the rods, with the rod ends flex in their mounts to the bushings? Will they resist both and either stretch or break the spectra, or make it slip where it's wrapper around the other rod? What's the failure mode?


---
**Jarred Baines** *March 14, 2014 14:15*

Yeah, I'm going to use belts, I just prefer them.



Sorry, what do you mean what is the failure mode?



I don't see it as being TOO different from the original design, it too had the centre bar going into a linearly constrained (by the outer bushing / linear rail) plastic X/Y end. In my mockup it is also has the same constraint, doesn't it? It should behave similarly regarding slop etc? 


---
**Whosa whatsis** *March 14, 2014 16:47*

I consider the extreme cases to find the potential flaws in a design to find the ideal solution too, but you can't just say "If this was really loose, that would be really bad". You have to think these things through to their logical conclusions and figure out <i>how</i> they will fail. No design is completely without compromise, but you have to choose which compromises to make based not only on how difficult the failures are to avoid, but also on how acceptable the failure mode is.



To take an extreme example, if you're choosing between design A that has a 10% chance of failing, and the failure would result in a little Z-wobble, and design B with only a 1% chance of failure, but the failure would result in causing the sun to prematurely go supernova and wipe out the entire Earth, clearly you have to choose option A.


---
**Tim Rastall** *March 14, 2014 21:43*

Have we talked about printability yet?  One of things I tried to do with the original Ingentis design,  aside from minimising the risk of triggering any catastrophic stellar explosions,  was to ensure everything could be 'easily'  printed. This often has the most significant impact on the design choices for a part ime.


---
**Whosa whatsis** *March 14, 2014 23:25*

**+Tim Rastall** Good point, you have to consider the failure modes in the production of the parts as well as their operation.


---
**Jarred Baines** *March 15, 2014 01:43*

Oh man I don't literally lol often, but I love those 2 comments guys, thanks for putting the universe first and printability second **+Tim Rastall** ;-)



The mockup I made, although a few things need more meat / teardrop holes, is quite printable with the bushing hole pointing upwards.



And I didn't consider what might happen if the rods do bind because as much as I keep looking at it trying to logically conclude something, surely it would be the same risks as the original design but with reduced likelihood of failure?


---
*Imported from [Google+](https://plus.google.com/+JarredBaines/posts/VLrDpAJTfU6) &mdash; content and formatting may not be reliable*
