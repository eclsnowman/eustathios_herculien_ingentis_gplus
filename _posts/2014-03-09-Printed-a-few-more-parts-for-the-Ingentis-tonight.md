---
layout: post
title: "Printed a few more parts for the Ingentis tonight"
date: March 09, 2014 06:08
category: "Show and Tell"
author: Brian Bland
---
Printed a few more parts for the Ingentis tonight.  Z axis parts.

![images/f70af91a7b13087c90f658014ce3f565.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f70af91a7b13087c90f658014ce3f565.jpeg)



**Brian Bland**

---
---
**Jarred Baines** *March 09, 2014 06:22*

Wicked color!


---
**D Rob** *March 09, 2014 06:38*

Those are very pretty prints


---
**Brian Bland** *March 09, 2014 06:39*

Violet ABS from JUSTPLA.


---
**D Rob** *March 09, 2014 06:39*

ABS?


---
**D Rob** *March 09, 2014 07:16*

Lol abs from those who call themselves justpla


---
**Jarred Baines** *March 09, 2014 10:14*

Yeah I noticed that :-P


---
**ThantiK** *March 09, 2014 21:31*

I'm sad to say that I was an idiot...printed in PLA, left the ingentis in my car, and melted my corners.  I'm waiting on new 6900zz bearings, because I crushed the shields on one of my bearings trying to get it out of the melted PLA...



Still haven't figured out my Z. :[


---
**D Rob** *March 09, 2014 22:18*

:( that makes me sad. I was supposed to vid today but havent my mendel 90i3 x idler broke had to epoxy trying to put it together now and print another. I have the Ul-T-Slot almost broken down now so I can add my new extrusions and rebuild it on camera for a video build guide,


---
**Jarred Baines** *March 09, 2014 22:54*

Shit **+Anthony Morris** - That's the worst! Left my printer in the garage when we had a heat-wave and the same thing happened to it :-(


---
**ThantiK** *March 09, 2014 22:58*

I hate fucking PLA, but I don't have a running printer at the moment to print in ABS.  Dealing with what I have at the moment. :(


---
**Jarred Baines** *March 09, 2014 23:02*

Lol- I feel your pain! My hbp died and ALL I wanna do right now is work on some Ingentis parts but I'm NOT doing it in PLA!


---
**Jarred Baines** *March 09, 2014 23:04*

Considering machining half of it in aluminum actually :-)


---
**Brian Bland** *March 10, 2014 00:39*

**+D Rob** **+Anthony Morris** If you need some parts let me know what you need and I will get them to you so you can print again.


---
**D Rob** *March 10, 2014 01:21*

i just used epoxy to fix the part long enough to print some extras


---
**John Lewin** *March 11, 2014 02:08*

These parts look a bit different than what I've been fixing to print. Are these part of a repo that I've missed?


---
**Brian Bland** *March 11, 2014 02:43*

They are from the V4 .stp file on the Youmagine Ingentis page.


---
**John Lewin** *March 11, 2014 04:01*

Ah, I see it now. It's in the z-stage and I was thinking it was a variant of the xy bearing mounts. thx


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/Bey7oL6fVVz) &mdash; content and formatting may not be reliable*
