---
layout: post
title: "Got home today to a nice Little surprise"
date: April 14, 2017 01:22
category: "Show and Tell"
author: Amit Patel
---
Got home today to a nice Little surprise. Thx **+Brad Hill** and **+Patrick Woolfenden** for the amazing little printer, even the wife gave me a Aww for the size of the printer. #KittenPrinter 



Brad I printed the Benchy on the SD card and that was amazing, any chance you could let me know what u sliced it with and if you have a profile u don't mind sharing. I would really appreciate it. Thx



![images/22a5dca6dff563ffd8de230b6e6be009.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/22a5dca6dff563ffd8de230b6e6be009.jpeg)
![images/4bdf759ea26e2caed28ed5bb3684eee1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4bdf759ea26e2caed28ed5bb3684eee1.jpeg)
![images/2d8d6bd11bde4e0c5c21c83a386230f9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2d8d6bd11bde4e0c5c21c83a386230f9.jpeg)
![images/6ab9e9f320be08baabb9bd787561ae54.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6ab9e9f320be08baabb9bd787561ae54.jpeg)
![images/954af7caaeb157c59e3c302547acbf9f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/954af7caaeb157c59e3c302547acbf9f.jpeg)
![images/e2a91c4d4add1942101d11e6ac599ea5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e2a91c4d4add1942101d11e6ac599ea5.jpeg)
![images/96d09f68205e3d51e3ac2556107e6326.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/96d09f68205e3d51e3ac2556107e6326.jpeg)

**Amit Patel**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 14, 2017 01:41*

Glad this thing found a new home. 



So is it Nema 14 motors on this thing? I was reviewing the BOM and that caught my eye. 



I caught one of these at MRRF btw. I agree with the awww. 


---
**Eric Lien** *April 14, 2017 02:21*

The wiring skills of **+patrick woolfenden**​ are far beyond my skills. All wiring exposed... Still looks amazing.


---
**Whosa whatsis** *April 14, 2017 02:22*

Yeah, all 14s except the extruder, IIRC. I always liked 14s, and I was ready to build one of these until I read the BOM to start ordering parts and found that most of them were imperial.


---
**Brad Hill** *April 14, 2017 03:19*

**+Amit Patel**​ glad to hear it arrived safely! If you go to the printer kitten github there is a cura profile, I used that.


---
**Jeff DeMaagd** *April 14, 2017 04:37*

The g code might say what slicer was used. Some slicers will even read the profile settings from the g code header.


---
*Imported from [Google+](https://plus.google.com/100854251935781152705/posts/bP59ssz182Z) &mdash; content and formatting may not be reliable*
