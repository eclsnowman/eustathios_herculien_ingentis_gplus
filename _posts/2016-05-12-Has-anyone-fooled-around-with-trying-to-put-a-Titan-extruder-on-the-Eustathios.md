---
layout: post
title: "Has anyone fooled around with trying to put a Titan extruder on the Eustathios?"
date: May 12, 2016 13:56
category: "Discussion"
author: Bud Hammerton
---
Has anyone fooled around with trying to put a Titan extruder on the Eustathios? I have one but I am still trying to picture a head design for direct drive. If anyone has used the Titan, how was the performance? I already have one, it's been in it's box together with a 20 mm NEMA17 for over a month with little time to play with it.





**Bud Hammerton**

---
---
**Pieter Swart** *May 12, 2016 14:53*

Yesterday I installed one on my printer which has a similar design, but I'm using it as a bowden extruder. From my initial impressions (unfortunately only one print so far), It works quite well and it's easy to clean too. Will do some more printing soon - I'm rebuilding the printer after it has been in storage for about a year. I'll post a pic of the installed extruder


---
*Imported from [Google+](https://plus.google.com/+BudHammerton/posts/irD1oNSwwTZ) &mdash; content and formatting may not be reliable*
