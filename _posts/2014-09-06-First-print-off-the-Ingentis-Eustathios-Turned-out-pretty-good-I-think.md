---
layout: post
title: "First print off the Ingentis/Eustathios. Turned out pretty good I think"
date: September 06, 2014 05:08
category: "Show and Tell"
author: Brian Bland
---
First print off the Ingentis/Eustathios.  Turned out pretty good I think.  Still need to figure out print speeds with it.

![images/947bd26225ba4fe1e11bfd0487b5fecf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/947bd26225ba4fe1e11bfd0487b5fecf.jpeg)



**Brian Bland**

---
---
**Erik Scott** *September 06, 2014 10:55*

Very nice! Layers look pretty uniform. 



I'm still playing with speeds a bit myself. I'm using Cura, at the moment, and off the top of my head, I believe I do 20mm/s for the first layer, and then 20 or 30 for the outer perimeter, 40 for the inner, and 60 for the infill. Works pretty well for me. Maybe it'll work for you as well. 


---
**Eric Lien** *September 06, 2014 12:55*

**+Erik Scott** I use similar speeds for high quality on my two bots ( #Eustathios and #Herculien). I have gone as high as 200mm/s in abs and 170mm/s in pla with OK results but extrusion become finicky at those speeds and PLA has tended to jam on me. But those speeds are great for proof of concept test fit of parts for iterating.﻿


---
**Mike Miller** *September 06, 2014 15:19*

So using G0 X<> Y<> F50000 isn't going to net me quality prints? :D


---
**Erik Scott** *September 06, 2014 16:45*

**+Eric Lien** I have also gotten some weird extrusion issues with PLA (The only kind of plastic I can do right now). I have to turn it up to 220C and print the first layer pretty slow, or else the extruder stepper suddenly skips back and the pressure in the hottend releases, resulting in no extrusion for about 5 seconds. This also happens if the bed is too close to the nozzle, which is really a pain as there's a really delicate sweet spot where the plastic will still stick to the bed, but doesn't cause the extruder to skip. 


---
**Eric Lien** *September 06, 2014 18:53*

**+Erik Scott** I print some PLA at 230 on my e3d v5. I have verified these are the actual extruder temps with a multimeter and  thermocouple. Do a test push by hand find that sweet spot where filament pushes easily but temperatures above that don't make it any easier.﻿


---
**Erik Scott** *September 06, 2014 19:04*

**+Eric Lien** Good to know. Thanks. Maybe I haven't done anything wrong then. 


---
**Jim Squirrel** *September 06, 2014 20:26*

very nice hope to see it sometime to encourage me to finish my build


---
**Mike Miller** *September 07, 2014 13:30*

**+Shauki Bagdadi** Very few of us can create a printer a day and two on Sunday. ;) #VixQR, #QuadRapStrapFlapBap. :D


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/B48KXYE3fTN) &mdash; content and formatting may not be reliable*
