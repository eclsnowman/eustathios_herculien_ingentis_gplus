---
layout: post
title: "It Lives! After wrestling with configuring Marlin and cleaning up some wiring, I have successful motion"
date: February 23, 2018 19:55
category: "Show and Tell"
author: Dennis P
---
It Lives! 



After wrestling with configuring Marlin and cleaning up some wiring,  I have successful motion. Happy dance for me... I could use some help with a couple of things with Octoprint and Marlin. 



Video or it didn't happen proof:  
{% include youtubePlayer.html id=p2olp9YGjwg %}
[https://youtu.be/p2olp9YGjwg](https://youtu.be/p2olp9YGjwg)



Note the origin marked on the heatbed and the axis notations for +X & +Y directions.

(The video is taken from behind and above the printer because the webcam is clamped to the floor joists above for now. Its a screen grab through Camstudio of Octoprint) 



Does anyone know how to directly record the video stream of mpeg-streamer from Octoprint? I don't want a time-lapse, I want to pull the stream directly... because



There appears to be a lag in the Y motion. One side appears to move before the other. I checked the belts and pulleys, they seem ok. I cracked them all loose and tried using the alignment gauges to reset the cross-rod to shaft alignment. I thought I could see it with my eyes watching it up close, but you can really see it on the live stream. Its hard to see on the capture. 



Has anyone used Marlin to run their printer? Can you share any tips or tweaks?  This is the first printer I am configuring from scratch. **+Eric Lien** pointed me to his original Marlin configuration from a few versions ago that I am reviewing to try and update to ver 1.1.8.  

 

Has anyone set up any of the bed leveling functions in Marlin?  I would like to try the manual, menu based positioning scheme to level with feeler gauges. 

Here are my Marlin config files for now: 



[https://drive.google.com/file/d/1p-rEMEX_g7luHYzYwN2aWoA3nPDUwjZT/view?usp=sharing](https://drive.google.com/file/d/1p-rEMEX_g7luHYzYwN2aWoA3nPDUwjZT/view?usp=sharing)

 

Thanks again to everyone's help and encouragement along the way so far, I have a little more to go before I can start pushing some plastic.



Dennis 





 





**Dennis P**

---
---
**Eric Lien** *February 23, 2018 21:02*

Looks like maybe if you use ffmpeg you can capture the stream:



[https://stackoverflow.com/questions/33362310/record-incoming-mjpeg-stream](https://stackoverflow.com/questions/33362310/record-incoming-mjpeg-stream)



[https://stackoverflow.com/questions/47292785/recording-from-webcam-using-ffmpeg-at-high-framerate](https://stackoverflow.com/questions/47292785/recording-from-webcam-using-ffmpeg-at-high-framerate)



[https://gist.github.com/indiejoseph/a0547b1c996cea428311](https://gist.github.com/indiejoseph/a0547b1c996cea428311)




---
**Eric Lien** *February 23, 2018 21:05*

For the lag, i can only think of belt tension, or pulley grub screw issues. Otherwise they are coupled to the same shaft... So it should not be possible unless one has dramatically more drag causing a static friction differential between the two sides... Hence a difference in backlash.


---
**Dennis P** *February 23, 2018 22:18*

Thanks Eric! I found OBS too, [obsproject.com](http://obsproject.com). 



I will try to make a better recording of what I see. 

 


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/NgGEg5k4nYb) &mdash; content and formatting may not be reliable*
