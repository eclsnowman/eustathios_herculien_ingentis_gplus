---
layout: post
title: "Here's the binding at slow speed"
date: May 25, 2015 19:52
category: "Discussion"
author: Derek Schuetz
---
Here's the binding at slow speed

![images/a6d1e0c9888dfc2308fba7fa9e8b7c40.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a6d1e0c9888dfc2308fba7fa9e8b7c40.gif)



**Derek Schuetz**

---
---
**Bruce Lunde** *May 25, 2015 20:15*

The movements appear to not be impeded by the binding you describe. It looks pretty smooth at the slow speed?


---
**Derek Schuetz** *May 25, 2015 20:17*

You can't hear the noise?


---
**Isaac Arciaga** *May 25, 2015 20:35*

That doesn't sound like binding. It sounds more like resonance. Are you belts too tight?


---
**Gústav K Gústavsson** *May 25, 2015 21:04*

That seems not to be a binding noise. More like resonance. Belts to tight or to loose? Wheels not aligned so belt is rubbing against sides?


---
**Derek Schuetz** *May 25, 2015 21:14*

I can't see any rubbing and it only occurs at two areas that's what makes me believe it's binding


---
**Eric Lien** *May 25, 2015 21:21*

One problem with my carriage design is the bushing placement is asymmetric to the center. Does it happen heading in one direction but not the other? In one direction it's torquing down on the bushings, in the other direction it's pulling up. It's an unfortunate side effect of trying to keep the carriages compact as possible. Try putting a very light oil on all of the rods and run the break in at 100% speed, re-oil, then run again. as I mentioned bushings are more sensitive than bearings so break-in takes a while.


---
**Gus Montoya** *May 25, 2015 21:36*

So this is for everyone, what was your average time to get your bushings to "break-in"? Are we looking at a day or two of running? Or are we looking at 1 wk???


---
**Isaac Arciaga** *May 25, 2015 21:37*

**+Derek Schuetz** I would loosen the belts on X and Y in case they are too tight for that slow of a speed. Easy thing to do just for process of elimination.


---
**Isaac Arciaga** *May 25, 2015 21:42*

**+Gus Montoya** It doesn't take long to break in. Tim's process of running the drill should do the trick. What's more important is that the carriages holding the bushings don't have too tight of a fit to impede the bushing's ability to swivel 5 degrees. If the fit is too tight, it clamps the bushing down preventing it from "self aligning".


---
**Vic Catalasan** *May 25, 2015 22:02*

The motor to belt should not be a big issue and it appears you binding on corners? I would loosen some screws on the corners bearing mounts and tighten them gradually when the carriage is on corner. I would run it again and continue to tighten more each time.  What micro step setting are you using? I am not at this stage of my build and unsure what is the best step setting for my steppers


---
**Vic Catalasan** *May 25, 2015 22:18*

I would like to ask and really unsure if it is really necessary to have two bushings needed for the X carriage when the Y carriage already has two bushings installed. This could easily bind if it is not perfect. I think 3 bearings is enough to keep the head leveled.


---
**Eric Lien** *May 25, 2015 22:34*

If the corner bearing mounts are at all over sized they will not allow the bars to get far enough apart. I should undersize the width of those slightly I think to allow more vertical adjustment due to variation of the printed carriages rod spacing (since no printer is perfect).﻿


---
**Derek Schuetz** *May 25, 2015 22:35*

I'll try taking some tension off the belts tonight but I'm not sure if that's the issue. How smooth should it move without the motors attached? To me it feels smooth but I'm not sure if that's enough


---
**Derek Schuetz** *May 25, 2015 22:38*

**+Vic Catalasan** I'm now running 1/16 I was doing 1/32 but thought to try 1/16 for a difference


---
**Eric Lien** *May 25, 2015 23:05*

**+Derek Schuetz** when you push by hand into the corners do you feel any sticking when you push back out. A good test is to turn the stepper pullies by hand very slowly (you will have to take off the belt guards, and go slow so you find drive current back into the board). It should be easy and even, both in to and out of the corners.


---
**Vic Catalasan** *May 26, 2015 06:07*

**+Derek Schuetz** On the carriage, can you try removing the x-bushing closest to the y-rod.Try it again and see how it does with bind. In my opinion it may not be needed and will only cause bind when not perfect, with warpage 3D printing is not perfect.


---
**Derek Schuetz** *May 26, 2015 07:04*

**+Vic Catalasan** I'm going to try one other thing in the morning if the issue still exist after this I'll take apart the whole gauntry and remove a bushing. But I dont think my issue is binding in the carriage but a corner it self


---
**Eric Lien** *May 26, 2015 07:46*

**+Vic Catalasan**​ I don't think that is necessary. I know what you mean, only three points are required to establish a plane. But several people including myself have been using the carriage without issue. So I think confirming corner alignment is the best first step.﻿


---
**Derek Schuetz** *May 26, 2015 17:45*

**+Vic Catalasan** removing a bushing is not possible. adds to much play to the carriange


---
**Øystein Krog** *May 26, 2015 20:43*

I also think this is resonance of some kind.

Try 1/8 microstepping, it could be an artifact of the steppers+motors.

Is it possible to add some dampening?


---
**Derek Schuetz** *May 26, 2015 23:14*

Just an update I got it back to 1/32 micro stepping the binding is manageable and only occurs still The back left corner still but not as severe. Slow movements aren't as bad so I think it should be well for now


---
**Vic Catalasan** *May 26, 2015 23:48*

**+Derek Schuetz** What you need is a 10+ hour print job to break it in.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/VcgKrgxZrAr) &mdash; content and formatting may not be reliable*
