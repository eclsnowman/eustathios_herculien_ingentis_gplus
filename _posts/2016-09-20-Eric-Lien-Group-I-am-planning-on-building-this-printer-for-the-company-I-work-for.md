---
layout: post
title: "Eric Lien & Group... I am planning on building this printer for the company I work for"
date: September 20, 2016 01:46
category: "Discussion"
author: Jim Betsinger
---
**+Eric Lien** & Group...



I am planning on building this printer for the company I work for.  I have a budget of $3K, so I would appreciate some input as to what bells and whistles to add to the machine's GitHub baseline.



Most importantly where is the best place to get the printed components made?



Here are some of the things I would like to achieve:

- Flexible materials

- Carbon \ Glass reinforced materials

 

Here are some nice to haves

- Auto bed leveling

- More than two extruders



Anything else you experts can recommend I would appreciate it.



Thanks,

--Jim







**Jim Betsinger**

---
---
**Jim Stone** *September 20, 2016 02:02*

So...for the versatility of material....you're going to need to have an extruder mounted to the carriage ...for at least one of the hotends 



As flex filament may clog otherwise...just an insurance policy.



As for the other exotics. A hardened nozzle....hardened not stainless. ....HARDENED 



My suggestion is for extruder use bond tech. ...the damn thing is bulletproof 



Another bell and or whistle. Cable chain/drag chain for the heated bed connection. It makes it so much cleaner looking.



Get a raspberry pi....you're gonna love cloud printing. Also get a high quality HD Webcam. 



If possible make a small streaming rig just for video feed. So if wanted you can stream/save for youtube live.



Acrylic/lexan if you go the stream route. Keep the front door clear for clarity. But the sides tinted if your workplace is iffy on the whole Webcam thing.....some people don't like Webcam.  But are excellent for print monitoring 


---
**Jim Stone** *September 20, 2016 02:02*

When I get to a pc. I'll clean my thoughts up


---
**Jim Betsinger** *September 20, 2016 02:13*

**+Jim Stone** - Great input.  Thank you very much.



So could I assume the BondTech direct drive extruder would be the best choice for the filled materials?



What kind of speed \ accuracy loss should I expect when going from a Bowden extruder to the direct drive extruders?



How is the Raspberry Pi camera for a streaming cam?


---
**Jim Stone** *September 20, 2016 02:16*

Bond tech can be both. You would imo....just mount the one on the carriage if you use flex filaments.



But ask Eric about that. I haven't had personal experience with 1.75 flex yet.



But if you don't plan on using flex materials. Both can be bowden instead of just one.


---
**Eric Lien** *September 20, 2016 16:09*

You have three main options for flexible:



1.) You can do an on carriage extruder (I made a removable one a long time ago based on my HercuStruder: [https://plus.google.com/+EricLiensMind/posts/Y4V8sHcjmgu](https://plus.google.com/+EricLiensMind/posts/Y4V8sHcjmgu)). To be honest I never did much testing on this.



2.) You can get one of the extruders and hotends designed for 3mm filament. 1.75mm ninjaflex in a long Bowden tube is like pushing a rope. In 3mm you can actually do Ninjaflex, CheetahFlex, and other TPU filaments in Bowden very well (see Martins example on an Ultimaker: 
{% include youtubePlayer.html id=iycGOh_6Loc %}
[https://www.youtube.com/watch?v=iycGOh_6Loc](https://www.youtube.com/watch?v=iycGOh_6Loc)). 



3.) Another option is a smaller printer dedicated for flexibles. Thats what I do now and to be honest direct drive on flexible is the way to go.


---
**jerryflyguy** *September 20, 2016 16:16*

**+Jim Betsinger** as for printed parts, check down the list a ways there was a person offering a set of parts for cheap. you'd be ahead of the game w/ them! It took me 3-4 months to get all the printed parts assembled to the point I could start building.



The Misumi extrusion kit is worth the money (comes drilled and sqr right from the get-go) buy your linear rods from Misumi at the same time, worth the extra you pay from them vs Chinese rods.


---
**Eric Lien** *September 20, 2016 16:44*

One nice thing to upgrade to is cast aluminum heat spreader. I haven't had any issues with my standard 1/4" plate. But a cast aluminum mic6 plate will result in less stresses during heating due to the nature of the cast aluminum grain structure having less internal stress which can flex during heating. 



Also I would use a Panucatt based smoothieware controller or a Duet Wifi controller board to gain the advantages of a 32bit controller.


---
**jerryflyguy** *September 20, 2016 17:19*

**+Eric Lien** where's a good place to get a mic6 plate? From my limited search it kinda seems a bit rare? It'd be great if we could order one waterjet or laser cut. 


---
**Jim Stone** *September 20, 2016 17:22*

**+jerryflyguy** [www.onlinemetals.com](http://www.onlinemetals.com) is where i got mine.


---
**Eric Lien** *September 20, 2016 17:38*

[http://www.discountsteel.com/items/C250_Aluminum_Cast_Tooling_Plate.cfm?item_id=152&size_no=1](http://www.discountsteel.com/items/C250_Aluminum_Cast_Tooling_Plate.cfm?item_id=152&size_no=1)



Cost is around $75 + Tax and shipping for the raw plate with no holes drilled. Discount Steel can also laser cut, and machine if you have custom requirements: [http://www.discountsteel.com/services/?page_id=14](http://www.discountsteel.com/services/?page_id=14)


---
**jerryflyguy** *September 20, 2016 20:37*

**+Eric Lien** geez! That's pretty reasonable really. If I get any warping on my 1/4" laser cut plate I'll be getting them to cut me one. Will require a new silicone heater as well (I don't think I'll get the old one off w/out wrecking it). If I was building new, I'd do it w/ this plate.. Wouldn't increase the cost hardly at all vs the plate I got cut locally.


---
**Jim Betsinger** *September 21, 2016 02:25*

**+jerryflyguy** Thanks for the input.  You mention the Misumi extrusion kit... where is that ordered from,  Misumi, or a specific distributor?


---
**jerryflyguy** *September 21, 2016 05:11*

**+Jim Betsinger** you can order it directly from Misumi, just use the part numbers as listed on the BOM and like magic, it's just perfect. All thanks to **+Eric Lien**  😉👍🏻


---
*Imported from [Google+](https://plus.google.com/101859507581389458920/posts/YCYZ8JEJ8sc) &mdash; content and formatting may not be reliable*
