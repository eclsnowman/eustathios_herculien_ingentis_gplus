---
layout: post
title: "Blatant abuse of community alert! With 3 more email addresses"
date: January 21, 2015 01:19
category: "Discussion"
author: Tim Rastall
---
Blatant abuse of community alert!

With 3 more email addresses. I get a free kit of this big-boy's mecano stuff. I'm  also going to get in contact with these guys to see if they are interested in a Procerus version using their components.... 



<b>Originally shared by Tim Rastall</b>



This stuff looks pretty awesome.

[https://uberblox.com/?ref=gTL48](https://uberblox.com/?ref=gTL48)





**Tim Rastall**

---
---
**Wayne Friedt** *January 21, 2015 03:06*

Kicker is if you give that link for someone else to sign up under your link it takes them back to the original signup page not to your signup link. Did you get different results **+Tim Rastall** 


---
**Tim Rastall** *January 21, 2015 03:26*

Works fine for me - have got 7 valid sign ups so far.


---
**Liam Jackson** *January 21, 2015 14:51*

Hopefully you got one more now! 


---
**SalahEddine Redjeb** *January 21, 2015 18:50*

And here you go boss, you've got another sign-up!


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/fDw4mLT3rdd) &mdash; content and formatting may not be reliable*
