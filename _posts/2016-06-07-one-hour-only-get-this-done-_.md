---
layout: post
title: "....one hour only get this done ... -_-"
date: June 07, 2016 06:49
category: "Build Logs"
author: Botio Kuo
---
....one hour only get this done ... -_-

![images/722d7079b08a074cb72cb0b11f1a1c6e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/722d7079b08a074cb72cb0b11f1a1c6e.jpeg)



**Botio Kuo**

---
---
**Eric Lien** *June 07, 2016 13:57*

Always exciting to see a new printer taking shape. As always, please ask questions as you go through the process. We have so many great minds in the community. Many of our community builders have even been through this process more than once. Here are just a few words of advice from someone who has had my printers apart more times than I can count:



1.) pay close attention to where the t-nuts will be going. Have a laptop open with the 3d model so you can roll it around. It is better than any ikea instructions :) Because once you get it built to a certain point there are locations where t-nuts cannot be installed after the fact since the open end of the slot is now blocked.



2.) OK so you forgot a t-nut :) thats ok it happens to the best of us. you can pull the printer back apart... or get yourself some drop in t-nuts ([http://goo.gl/uOKnzb](http://goo.gl/uOKnzb)  or [http://goo.gl/njVvLU](http://goo.gl/njVvLU))



3.) Alignment is critical to a cross rod gantry printer functioning properly. Many of the axis have rods that both translate and rotate. This leaves very little margin for error on the bronze bushings. It takes time to get all the bushing seated, aligned, axis squared, rods trammed, etc. Don't loose hope. You will try and run your break in gcode the first time and say, oh, crap it chatters and makes crazy noises. It just means a little more alignment is required. I always tell people building a HercuLien or Eustathios... "When it's right, you will just know".



If you run one of my configs from the github... drop acceleration down a bit. I had it at like 9000. More is better right? in hindsight I would say no. 3000-4000 accel is more than enough to not limit your printing speeds while limiting corner ringing and other artifacts.


---
**Botio Kuo** *June 07, 2016 15:49*

How's everyone fixing the power supply on this machine? (I meant lock in or with machine)  or just put it on ground ? but when I want to move this machine will be a trouble. 


---
**Eric Lien** *June 07, 2016 15:57*

**+Botio Kuo** I have holes drilled into base plate to mount the PSU and the SSR: [http://i.imgur.com/MCVJx4r.png](http://i.imgur.com/MCVJx4r.png)


---
**Maxime Favre** *June 07, 2016 19:02*

Or you can use a L bracket to mount it. Depends on how and where you want it. (This one is not for the BOM power supply)

[http://www.thingiverse.com/thing:845862](http://www.thingiverse.com/thing:845862)


---
**Roland Barenbrug** *June 07, 2016 22:18*

Took the approach of **+Maxime Favre**. Essentially insert an additional bar to create a closed frame in which controller and PSU can be secured.


---
**Roland Barenbrug** *June 07, 2016 22:19*

BTW, don't worry about the hour: you'll need more than one ;-)


---
**Botio Kuo** *June 08, 2016 01:49*

**+Maxime Favre** Hi Maxime, I saw your way for mount it. I think it is great and clean.


---
**Botio Kuo** *June 08, 2016 01:52*

**+Eric Lien** Hi Eric, But i didn't see the holes in DXF file ?? @@" OH... I need to drilled ~~ Thanks


---
**Eric Lien** *June 08, 2016 02:02*

**+Botio Kuo** the reason was depending on the PSU you purchase the holes are in different locations. Sorry I didn't include them. I just didn't want to add holes that might not line up with people's PSU choice. Plus on this sort of work I usually use a "blind hole spotter" or "hole transfer screws" to mark the location as I am manually laying things out.


---
**Botio Kuo** *June 08, 2016 08:14*

About the Belt Tensioner parts. I can't put my Steel Hex Nut in the printed belt tensioner. because I print in ABS? 


---
**Maxime Favre** *June 08, 2016 09:28*

It may depends on material and the precision of the printer used. Are you far off from the opening dimension ? You can file the openings of try to push the nuts inside with a soldering iron.﻿


---
**Daniel F** *June 08, 2016 10:44*

also added an additional extrusion and fixed it with flat aluminium bars to keep it below the extrusions and leave some room for air convection. See here: [https://plus.google.com/111479474271942341508/posts/guJX2R5Bic2?pid=6164051000726899346&oid=111479474271942341508](https://plus.google.com/111479474271942341508/posts/guJX2R5Bic2?pid=6164051000726899346&oid=111479474271942341508)


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/7sEhYBubpUF) &mdash; content and formatting may not be reliable*
