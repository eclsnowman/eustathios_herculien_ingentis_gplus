---
layout: post
title: "In my quest to further lighten everything possible gantry-wise, I received a bunch of polymer bearings and CF tubes today and they are just the ticket"
date: December 04, 2014 09:48
category: "Discussion"
author: Mike Thornbury
---
In my quest to further lighten everything possible gantry-wise, I received a bunch of polymer bearings and CF tubes today and they are just the ticket. Not a hint of stiction, even when you twist the bushes on the rods.



I have been assured by the chemical bod at the plastics place that the surface of the bearing is energised enough to take nearly any kind of adhesive - PVA (for fitting in MDF carriages), epoxy, even superglue and hot glue, for testing. so, I'm going to go minimalist and use a piece of 2.5mm CF plate with a couple of 3mm CF tubes for alignment and epoxy the bushes in place The whole carriage should come in under 30g and as we know, mass is not our friend, so it should be right friendly ;)

![images/5c9781ba541a8f37e0f957a814aa17f0.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5c9781ba541a8f37e0f957a814aa17f0.png)



**Mike Thornbury**

---
---
**R K** *December 04, 2014 13:20*

Where are you picking up the CF parts, out of curiosity? I see them on mcmaster-carr for around 8-20$ each for a 4' length - ~8mm width tubes look to be about 18-25$. 4' of length is about 1.2m, which means you can probably take one tube and get plenty of use out of it.


---
**Mike Thornbury** *December 04, 2014 13:43*

I live in Asia, so I normally buy from China or Singapore.



I pay $11-15 for 1M of 12mm OD in gloss twill, with free shipping



Depends on the machine how many pieces from a metre, but I can get rid of the shorter offcuts to the RC guys here. They are always after me for bits.



If I am ordering any quantity, I order them at the length I need, so I'm not paying for the waste. Typically a CF rod comes at 2-2.4M from the manufacturer.﻿


---
**Dale Dunn** *December 04, 2014 14:17*

What kind of printer are you planning?



Will you be able to make those rods close enough to parallel to prevent binding? Maybe one of the rods will have a soft mount to allow self-alignment?


---
**Miguel Sánchez** *December 04, 2014 15:56*

Zortrax printer uses double-beam cross design, but they did not seem to appreciate my comment about them using an Ultimaker-like mechanism at the Rome Maker Faire :-)

[https://zortrax.com/product-details/technical-details](https://zortrax.com/product-details/technical-details)


---
**Mike Thornbury** *December 05, 2014 01:58*

Nothing like it **+Miguel Sánchez** -this is a carriage for a Sli3Der.



I am making it on a CNC router out of MDF, so the tolerances will be close.



The Sli3Der is already fast, even with heavy steel rails. My idea is to get rid of as much inertia as possible and to simplify the build. Cutting a flat-pack from 6-12mm mdf means it will be simple to set up, will be aligned correctly "out of the box" and will be cheap to make ($4 for the chassis, vs about $80-100 for extrusion).﻿ plus it's quick to make. Initial cutting out took 8 minutes from scratch, vs days printing plastic parts.



The current challenge is to decide on the z-axis mechanism. I've been working on belt/spectra, but think I am going to go screw, for the complete lack of maintenance. Set and forget.


---
**Mike Thornbury** *December 05, 2014 02:10*

**+Dale Dunn**, it is no more prone to binding than a steel/LM8UU setup used in lots of printers, and cutting out using CNC should make it within .01mm accurate over 300mm.﻿ 



But... I like the idea of keeping one end of one rail floating. I will look into it. Thanks.


---
**Miguel Sánchez** *December 05, 2014 07:11*

**+Mike Thornbury** I see. You can't go wrong with Richard's Sli3Der. 


---
**Mike Thornbury** *December 06, 2014 02:18*

Or as it will be, Ply3Der :)


---
*Imported from [Google+](https://plus.google.com/101708620681849403392/posts/QLyiSRAz7BY) &mdash; content and formatting may not be reliable*
