---
layout: post
title: "MRRF is a blast!"
date: March 23, 2018 23:10
category: "Show and Tell"
author: Bruce Lunde
---
MRRF is a blast!







![images/730cd20d12b0d8b7d23e91e25925104b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/730cd20d12b0d8b7d23e91e25925104b.jpeg)
![images/4219fd897405eca6041b0c7266a4fc7a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4219fd897405eca6041b0c7266a4fc7a.jpeg)
![images/da24df98a67930e8a705dc8089292a10.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/da24df98a67930e8a705dc8089292a10.jpeg)
![images/98d4ee3eca916977c6294f4f36ce9a4b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/98d4ee3eca916977c6294f4f36ce9a4b.jpeg)

**Bruce Lunde**

---
---
**Eric Lien** *March 24, 2018 11:06*

Sure is. It's my favorite weekend of the year.


---
**Brandon Satterfield** *March 24, 2018 18:17*

Wish I could have made it this year. Had to choose the kids spring break instead. It was a win. 


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/USE1KZufTvS) &mdash; content and formatting may not be reliable*
