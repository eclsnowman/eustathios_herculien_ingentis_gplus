---
layout: post
title: "Azteeg and pi mounted well ain't it purty"
date: January 29, 2016 01:13
category: "Show and Tell"
author: Jim Stone
---
Azteeg and pi mounted well ain't it purty﻿

![images/2a552bbee70a0750a49525a1491be849.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2a552bbee70a0750a49525a1491be849.jpeg)



**Jim Stone**

---
---
**Eric Lien** *January 29, 2016 01:24*

Looking great. Keep posting. I love watching these come together.


---
**Eric Lien** *January 29, 2016 01:25*

What's the part number and source of that box again, seemed like it was a much better price than the Hoffman one I had.


---
**Eric Lien** *January 29, 2016 01:26*

BTW don't forget cooling fans for the box :)


---
**Jim Stone** *January 29, 2016 01:32*

Yeah I gotta figure out what size holes the fans need. The Centre of the damn fan is so large


---
**Jim Stone** *January 29, 2016 01:36*

BUD Industries PN-1335-DG High-Impact ABS NEMA 4x Indoor Box, 10-27/64" Length x 7-17/64" Width x 3-47/64" Height, Dark Gray Finish



from [amazon.com](http://amazon.com)



 was like 20 bucks. and they have a file for the bottom tray so i was able to really crappily model something. the pegs are so fragile when you screw into them. i ended up drilling thru the piece. lol.



i would honestly eliminate the pegs after that and use brass pillars. and just use the pegs as drill guides.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/dDJhf4WY6Xx) &mdash; content and formatting may not be reliable*
