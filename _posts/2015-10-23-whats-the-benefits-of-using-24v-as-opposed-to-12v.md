---
layout: post
title: "whats the benefits of using 24v as opposed to 12v?"
date: October 23, 2015 05:33
category: "Discussion"
author: Jim Stone
---
whats the benefits of using 24v as opposed to 12v?





**Jim Stone**

---
---
**Владислав Зимин** *October 23, 2015 05:40*

the difference in current

less current, thinner wire


---
**Jim Stone** *October 23, 2015 05:41*

Wait

..more volts less current? 


---
**Владислав Зимин** *October 23, 2015 05:43*

P=U*I

P - power (const)

U - voltage

I - current


---
**Jeff DeMaagd** *October 23, 2015 05:45*

Higher motor performance, even for the same motor. Motors have a back EMF when moving that counters applied voltage. Back EMF increases as the speed goes up. Once the back EMF matches the drive voltage, you literally can't go any faster. Within reason, higher voltage doesn't hurt the motors since the drivers are current limiting.


---
**Stephanie A** *October 23, 2015 05:53*

Thinner wires, less fires. Things like connectors, mosfets, and wires won't get overheated.


---
**Jim Stone** *October 23, 2015 05:59*

Wow and here was me thinking 24v was more dangerous


---
**Tomek Brzezinski** *October 23, 2015 13:04*

Not meaningfully dangerous until 36-48V, more typically honestly I'd say 48V-60V is where you need to be mindful.  Eitherway those are just rules of thumb. You also don't want to go very close to the voltage limits of the chips, like if the chip is 45V rated (like DRV8825), it's best not to go above 35-ish [depending how good your input capacitors are]



There is a point at which you lose benefits for high voltage, because your stepper drivers are just acting as DC-DC converters with big input-output differences. 



But most people don't reach that, and 24V would be much better than 12V for reprap electronics.



An application note I read once suggested "32*SquareRoot(inductance in mH)" is your recommended running voltage. For a 1.5mH motor, that's 40V.   Another rule of thumb is to take 8*ratedVoltage, in this case for many repraps it's 2-4V (so 16-32 as desirable voltages). Eitherway, the steppers could benefit from more voltage in most cases. Why do people use 12V then? answer: because the cheapest/most common supplies people use are salvaged server or gaming power supplies, with a 12V rail. Also, I guess 12V usually works. There's the principle of higher voltage being better for that reason. But if you want to push speeds get a higher voltage.



<b>*make sure you get appropriate heaters and heat beds though if you change voltage. A 12V heater running on 24V *is</b> dangerous, because it will be capable of drawing 4 times as much energy.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/6Y8wduLz3Vo) &mdash; content and formatting may not be reliable*
