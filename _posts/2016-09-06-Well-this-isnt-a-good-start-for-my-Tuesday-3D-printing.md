---
layout: post
title: "Well this isn't a good start for my Tuesday 3D printing..."
date: September 06, 2016 18:02
category: "Discussion"
author: Brandon Cramer
---
Well this isn't a good start for my Tuesday 3D printing... I started this print on Friday and it sat on the printer until today. I'm not sure if the printed red handle caused this boro glass to break or what happened. My pieces have been sticking awesome lately so I don't know if it stressed the glass enough to break it. 



 I'm glad I have a second piece of boro glass....



![images/c8c54cbd1e1ebfcaa34313c728fa252b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c8c54cbd1e1ebfcaa34313c728fa252b.jpeg)
![images/5c4db4e79932c66729e3a851dd3d9a74.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5c4db4e79932c66729e3a851dd3d9a74.jpeg)

**Brandon Cramer**

---
---
**Pieter Swart** *September 07, 2016 05:07*

I had similar problems and got myself something close to pyrex glass (can't remember now what it's called). In my case, the heated pad is a bit smaller than the bed and this of course causes uneven heating of the glass plate


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/EZvonSoCidh) &mdash; content and formatting may not be reliable*
