---
layout: post
title: "I started with a rework for all the printed parts in Fusion 360"
date: December 14, 2016 09:00
category: "Discussion"
author: Tobias Rodewi
---
I started with a rework for all the printed parts in Fusion 360.

Mainly to get some practice in Fusion, also because most of us can't leagualy use/afford solid works.

My first question is, what thread type / pitch is used in the 50mm_Threaded_Spool_Holder?



//Tobias





**Tobias Rodewi**

---
---
**Eric Lien** *December 14, 2016 19:14*

8mm pitch. Here is the profile extruded into the receiving mount (female thread): 

![images/097f6d3295b2139dbab5a10de094b2ea.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/097f6d3295b2139dbab5a10de094b2ea.png)


---
**Eric Lien** *December 14, 2016 19:20*

Here is the profile cut into the spool mount (male thread):

![images/b177f54bf1f21820dc6ae183823cc600.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b177f54bf1f21820dc6ae183823cc600.png)


---
**Tobias Rodewi** *December 18, 2016 13:08*

Thanks alot!

![images/7afff2c604feba6c0d957a2fd752d3cb.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7afff2c604feba6c0d957a2fd752d3cb.png)


---
**Eric Lien** *December 18, 2016 14:03*

**+Tobias Rodewi** looks great!


---
**Tobias Rodewi** *December 18, 2016 23:57*

Some parts are a bit complex though... :/ 

Is there a reason for the slanted top on the Double Carriage or is it just for looks?

![images/f7f2da5d0812d0c7a747a477249187cd.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f7f2da5d0812d0c7a747a477249187cd.png)


---
**Eric Lien** *December 19, 2016 01:04*

It has been a long time since I modeled it. I forget if it was for looks or function :)


---
**Tobias Rodewi** *December 19, 2016 01:19*

Well, then i'll add it to be on the safe side...

Btw, you have done a great job on the parts, all those details... 

11 parts reworked by me so far, maybe it's time to order the extrusions and start printing...


---
**Eric Lien** *December 19, 2016 01:23*

Thank you. I tried to put all required components into the model assembly (especially all the hardware since that is something often overlooked in other models I was reviewing when doing my original design).. For me a good model is more important than a user manual :)


---
*Imported from [Google+](https://plus.google.com/105932104970509575596/posts/gmbuC5yxeZS) &mdash; content and formatting may not be reliable*
