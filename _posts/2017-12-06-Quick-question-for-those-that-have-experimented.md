---
layout: post
title: "Quick question for those that have experimented"
date: December 06, 2017 02:35
category: "Discussion"
author: Gus Montoya
---
Quick question for those that have experimented. How fast have your printer run reliably? I kinda recall that some have run 300mm/s just fine without any alterations to their prints. Can anyone confirm what "normal" printing speeds I'm looking at getting out of my printer? Also any suggestions for looking into changes to the design to improve? Aside from going the linear guide rail route?



Thanks





**Gus Montoya**

---
---
**Stefano Pagani (Stef_FPV)** *December 06, 2017 02:42*

I have seen 300mm/s on some printers with carbon cross rods and a very minimal carriage. In my first prints I tried travel of 300 and print speed of 150 with not too bad results


---
**Gus Montoya** *December 06, 2017 02:47*

I wonder if I need to change the belts to ball screws or threaded rod to give the rigidity and accuracy at higher speeds. Belts are only good for so  much..but then again what about steel reinforced belts?


---
**Zane Baird** *December 06, 2017 02:50*

My travel moves are generally done at 250-300 mm/s and for printing I rarely exceed 80mm/s as it yields diminishing returns in my opinion. More importantly, I run acceleration at 2500mm/s^2 as this is more important for the short moves and corners where acceleration really is the limitation. I have done experiments with printing at 200mm/s before with a Volcano but the quality is not ideal...




---
**Gus Montoya** *December 06, 2017 02:59*

So what your saying is that the limiting factor is really the type of printing movement ie: right angles ?  Rather then the mechanical attributes of the printer? The reason I want to come back to building a new printer from this group is because I hate (strong word but accurate) the creality cr-10's 60mm/s. Days for a part that should take  hours it's ridiculous. Also I just like the design of the herculien and Eustathios, it's industrial and solid.


---
**Zane Baird** *December 06, 2017 03:20*

It really depends on the part as far as accel is concerned. Something with a lot of small line segments means that the full speed won't ever get reached before it starts decel for the next movement. And 60mm/s really isn't that bad for printing. If I want a "perfect" surface finish I'll run infill at 60mm/s and outline at 30mm/s. My normal printing speeds are 80-100mm/s with 50-70% speed on outlines depending on how much I care about the surface finish. It will always be a balancing act as far as speed and quality are concerned. If you want to really decrease print times, go for a larger nozzle and thick layers, but you have to make a sacrifice with Z-resolution going that route. 



I'm running steel core belts on my herculien and my honest opinion is that they are not any better than the fiberglass reinforced neoprene belts. On top of that, they will exhibit wear faster. 






---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/iTAmfRYZ7Eo) &mdash; content and formatting may not be reliable*
