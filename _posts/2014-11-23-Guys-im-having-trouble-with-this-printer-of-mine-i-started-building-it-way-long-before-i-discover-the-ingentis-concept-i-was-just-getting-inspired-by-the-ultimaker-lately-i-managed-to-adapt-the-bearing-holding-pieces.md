---
layout: post
title: "Guys, i'm having trouble with this printer of mine, i started building it way long before i discover the ingentis concept, i was just getting inspired by the ultimaker, lately i managed to adapt the bearing holding pieces"
date: November 23, 2014 17:00
category: "Deviations from Norm"
author: SalahEddine Redjeb
---
Guys, i'm having trouble with this printer of mine, i started building it way long before i discover the ingentis concept, i was just getting inspired by the ultimaker, lately i managed to adapt the bearing holding pieces that also carry the rods where the extruder goes.

My problem is that after wiring it all (i mean the belts principally) i cannot manage to move the assembly easily by hand, the translation is not smooth at all, and that is when i manage to translate.



my bearing are LM10UU (yes i have 10mm rods) and some 6000ZZ for holding the rods, standard GT2 belts, and printed pulleys that seems to do well the job, the motor holder is custom made.



Should i switch to brass oiled bushings? does they exist for 10mm or do i have to remake the whole assembly for 8mm?

![images/094ede4064327bc8d21f714008d035cf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/094ede4064327bc8d21f714008d035cf.jpeg)



**SalahEddine Redjeb**

---
---
**Miguel Sánchez** *November 23, 2014 17:09*

Once you have everything in place (belts and motors) motion will become harder than before. Anyway, LM10UU are not designed for rotating axis but for linear motion. That is is why bushings are used.


---
**Mike Thornbury** *November 23, 2014 17:14*

There's not much in the world with less friction than a steel ball on a hardened steel surface. The 10mm bearings you have should have very, very little friction. If they have, it's because you have a faulty bearing. 


---
**Mike Thornbury** *November 23, 2014 17:15*

Oh, I see... The shafts are rotating... Ignore what I said... Not applicable in your installation


---
**Mike Thornbury** *November 23, 2014 17:20*

10mm graphite-impregnated dry sintered bronze bush: [http://www.aliexpress.com/item/5pcs-10-14-10mm-JDB-Graphite-Lubricating-Brass-Bearing-Bushing-Sleeve-Oilless/1421421455.html](http://www.aliexpress.com/item/5pcs-10-14-10mm-JDB-Graphite-Lubricating-Brass-Bearing-Bushing-Sleeve-Oilless/1421421455.html)


---
**Tim Rastall** *November 23, 2014 17:20*

Yep.  It's the lm10uu bearings. Replace with bushings. 


---
**Eric Lien** *November 23, 2014 17:21*

[http://www.ultibots.com/10mm-bronze-bearing-4-pack/](http://www.ultibots.com/10mm-bronze-bearing-4-pack/)


---
**Eric Lien** *November 23, 2014 17:23*

Take a look at HercuLien. [https://github.com/eclsnowman/HercuLien](https://github.com/eclsnowman/HercuLien) 



Very similar design principles. I like your x/y belt drive arrangement. I haven't seen one like that yet.


---
**Jason Smith (Birds Of Paradise FPV)** *November 23, 2014 17:37*

You should probably switch to bushings. It looks like you're using linear bearings, which don't support rotation. I suspect this is why movement is inhibited. The bearings won't last long if they're forced to rotate on the shaft either. Nice looking machine though. ﻿

Edit: looks like a bunch of others beat me to it. That's what I get for reading posts in chronological order :)


---
**Stephanie A** *November 23, 2014 17:49*

The lm10uu bearings on the middle crossbars are fine, as they don't rotate. 


---
**SalahEddine Redjeb** *November 23, 2014 20:13*

**+Shauki Bagdadi** Well, the mechanic should be okay then.

**+Miguel Sánchez**  i suspected something with the bearings, but had no clue where on the bearing.

**+Mike Thornbury** thanks for the link, you spared me a lot of candle light research ;)

**+Tim Rastall** thanks for the tip!

**+Eric Lien** yep, i had a look not long ago, but i was at a late stage of printing the parts, had only the time to reuse the bearing and rod holder.

Like said before, the design was inspired by the ultimaker 2, with this small modification for the motor given i didnt want to implement another belt (trying to make some economies) or gears (which to be frank, i do not trust much my ability to get them right)

**+Jason Smith** thank you!

**+Stephanie S** Yep, i was planning on keeping those, thanks for the tip.

Thank you everyone for answering and helping, i am truly grateful, will post after modification.


---
**Eric Lien** *November 23, 2014 20:47*

**+SalahEddine Redjeb**​​ looks like it will be a great printer. Cannot wait to see it run.



Edit removed: incorrect info. That's why I should stick to mechanical things and leave electronics to others... I tend to let the smoke out of mine.


---
**Miguel Sánchez** *November 23, 2014 20:56*

**+Eric Lien** Do you mean shorting the motor leads? (If so, no it won't help the motor to move freely but the opposite, motor will become more difficult to turn then).


---
**SalahEddine Redjeb** *November 23, 2014 21:33*

Do you guys think i should use one bush per side or two?


---
**Eric Lien** *November 23, 2014 21:52*

**+SalahEddine Redjeb**​ two bushings per side if you use misalignment bushings. They are a ball and socket internally. Or you could use a long 10mm graphite bushings like the ultimaker. But misalignment would be more likely if prints aren't perfect: [http://m.aliexpress.com/item/1961470017.html?productId=1961470017&productSubject=JDB-101410-oilless-impregnated-graphite-brass-bushing-straight-copper-type-solid-self-lubricant-Embedded-bronze-Bearing&tracelog=wwwdetail2mobilesitedetail](http://m.aliexpress.com/item/1961470017.html?productId=1961470017&productSubject=JDB-101410-oilless-impregnated-graphite-brass-bushing-straight-copper-type-solid-self-lubricant-Embedded-bronze-Bearing&tracelog=wwwdetail2mobilesitedetail)


---
**Mike Thornbury** *November 24, 2014 00:31*

Didn't I already link to those, **+Eric Lien** ? :)


---
**Eric Lien** *November 24, 2014 00:56*

**+Mike Thornbury** sorry. Just saw that.


---
**Mike Thornbury** *November 24, 2014 01:15*

No worries - just pulling your leg. You can never have too much information, unless it's about your co-workers sexual escapades, then any information is too much information.



And your parents - never a good idea for your Dad to tell you how your Mum likes it... 0.o it's just wrong!


---
**D Rob** *November 24, 2014 08:18*

Hey man get rid of the printed pulleys. Robotdigg has them gt2 32t 2mm pitch. We crowd sourced to make that happen. Or if you want i can sell you some. I have extras. Pm me if you do.


---
**Mike Thornbury** *November 24, 2014 11:59*

Just out of interest, D Rob, why such a large pulley?


---
**D Rob** *November 24, 2014 15:03*

Because it's a 10mm bore. And gives gearing options. (I'll be driving 16t to 32t to double resolution)﻿


---
**Mike Thornbury** *November 24, 2014 16:52*

Gotcha


---
**D Rob** *November 24, 2014 17:08*

Yeah I tried to get [robotdigg.com](http://robotdigg.com) to make them 20t but not enough metal. So it went up to 32t. Now they keep them in stock after our first order.


---
**SalahEddine Redjeb** *November 24, 2014 19:26*

Yeah well, i printed thos pulleys because i wasnt sure of the design, wanted something disposable, that i would replace in case things went right.

I was about to ask the same question for the 32 tooth pulleys, but i got the answer, which drives me to another question, what are the speeds you think i can achieve with this printer, remove from account the extruder speed (i have another trick coming for that one, i'll share whenever i got the thing working), i mean what are the maximum movement speeds i can achieve?


---
**D Rob** *November 24, 2014 20:28*

**+Jason Smith**​ what's the fastest you've printed?


---
**D Rob** *November 24, 2014 20:29*

Click the name i just plussed. He has prints on there at 160mm/s plus﻿


---
**SalahEddine Redjeb** *November 24, 2014 21:53*

Okay, can we reach a travel speed of arround 300mm/S ?more? less?


---
**Eric Lien** *November 24, 2014 23:12*

I have taken Eustathios to 180mm/s with OK quality. Much above 150mm/s and harmonics on printers become a real problem. HercuLien has been tested at 250mm/s.﻿ [https://plus.google.com/109092260040411784841/posts/gnn6rmyWWC6](https://plus.google.com/109092260040411784841/posts/gnn6rmyWWC6)



Extrusion is also difficult at those speeds.


---
**Jason Smith (Birds Of Paradise FPV)** *November 24, 2014 23:24*

**+D Rob** , I've printed at 200mm/s and 220mm/s, but the quality drops significantly due to (speculative):

-simply not being able to pump enough power into the hotend to maintain the necessary extrusion temp of 285-290C for PLA ( It seems to require those temps in order to fully melt the filament as it's being pushed through the hotend so fast)

-not being able to change the nozzle pressure fast enough to keep up with the widely varying carriage travel speeds (possibly made worse by using a bowden vs direct setup).  This results in blobs near the corners where the plastic overextrudes when the speed is low and gaps on straight sections when the speed is very high.

For reliable printing at decent quality, 150mm/s is what I normally use.  For much higher quality or for smaller pieces, I drop it down to 80mm/s or so.

I haven't actually tested the max travel speeds, but you should be able to get close to 300mm/s. 


---
**SalahEddine Redjeb** *November 25, 2014 17:30*

Okay, thank you for the information guys.


---
*Imported from [Google+](https://plus.google.com/114163985775197430740/posts/466Mz9Rj9FA) &mdash; content and formatting may not be reliable*
