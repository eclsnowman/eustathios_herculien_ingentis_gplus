---
layout: post
title: "Hey guys, how are you wiring your fans?"
date: March 28, 2017 14:28
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hey guys, how are you wiring your fans? I have my heatsink fan going to the 24V directly from the PSU, but then it stays on all the time. I'm new to marlin configuration and I'm having trouble setting pins and getting them to work in S3D. I have an Azteeg X3 (with shield) that came with a defective HBed FET so I set #define HEATER_BED_PIN 17 to put that on the Ext 3 spot. I have my print fan on E4 (#define FAN_PIN 16). I can run GCode to turn these on in S3D, but how do I link them to the HBED and fan control?





**Stefano Pagani (Stef_FPV)**

---
---
**Daniel F** *March 28, 2017 15:04*

I use Repetier, so I don't know if this helps but I could set a threshold in the Repetier configuration tool to start the fan so it only starts when the HE reaches 50°C. You define "Enable extruder cooler at" and "Extruder cooler pin" in the config tool: [repetier.com - Repetier-Firmware Configuration Tool 0.92.9](https://www.repetier.com/firmware/v092/)

under "Tools"


---
**Frank “Helmi” Helmschrott** *April 03, 2017 12:27*

I'd also suggest the way **+Daniel F** goes with Repetier. That's how I do it (though I'm using the RADDS board instead of the Azteeg).




---
**Stefano Pagani (Stef_FPV)** *April 03, 2017 13:05*

**+Daniel F** Thank you, I will look into that.


---
**Stefano Pagani (Stef_FPV)** *April 04, 2017 13:56*

Dang, I wish I found the tool earlier :)=


---
**Frank “Helmi” Helmschrott** *April 04, 2017 14:11*

great, isn't it? BTW you should switch to the development version. It's quite stable and much advanced. It's the upcoming 1.0 :)




---
**Stefano Pagani (Stef_FPV)** *April 10, 2017 17:55*

**+Frank Helmschrott** will do, thanks!


---
**Stefano Pagani (Stef_FPV)** *April 14, 2017 02:24*

**+Daniel F** **+Frank Helmschrott** Couldnt get the Dev working, trying .92 now. I kept getting strings of errors of undef variables


---
**Daniel F** *April 28, 2017 11:04*

**+Stefano Pagani** Sorry to hear, must be due to the difference in hw setup. I just upgraded to dev, it runs on an arduino due (original) with RADDS. I used IDE 1.8.1 to compile. You might need to figure out what version of the IDE matches the corresponding repetier firmware config tool version.




---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/Wdf4XCSXF6H) &mdash; content and formatting may not be reliable*
