---
layout: post
title: "Just installed the first version of a dual v6-Volcano carriage for the Herculien"
date: November 17, 2015 03:17
category: "Show and Tell"
author: Zane Baird
---
Just installed the first version of a dual v6-Volcano carriage for the Herculien. Printed with my beta sample of PushPlastic PETG through a Volcano with the 0.4mm nozzle. So far so good. Nice and clean looking with an additional 10-15mm in the Z axis (not that the Herculien needs more print volume). So far performance has been excellent. Minor changes necessary for the final version



![images/a4a797882aac053ae0b556f287a0a8af.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a4a797882aac053ae0b556f287a0a8af.jpeg)
![images/b2d66c4fb3df22b10b2aeaa66c1aaf42.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b2d66c4fb3df22b10b2aeaa66c1aaf42.jpeg)
![images/3344aa3f5097a5027c6ab3f506bdc3e7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3344aa3f5097a5027c6ab3f506bdc3e7.jpeg)
![images/101033d8b94431977c2b54df01ae2128.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/101033d8b94431977c2b54df01ae2128.jpeg)
![images/aa16435e475d02eebf6e677b4a6f2bbe.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/aa16435e475d02eebf6e677b4a6f2bbe.jpeg)

**Zane Baird**

---
---
**Jim Stone** *November 17, 2015 03:21*

is it a bad thing that this gave me a nerd chub? :P


---
**Eric Lien** *November 17, 2015 03:34*

**+Jim Stone** that makes two for us ;)



**+Zane Baird**​ amazing work. Looks elegant and mean at the same time. Like  a sports car.


---
**Zane Baird** *November 17, 2015 03:57*

**+Eric Lien** **+Jim Stone** make that 3 of us. Now I just need to get a v6...


---
**Jim Stone** *November 17, 2015 04:36*

you know what would be sweet. a Quad holder. 2 v6 and 2 volcano.


---
**Stephanie A** *November 17, 2015 05:02*

You guys are gross. 


---
**Jim Stone** *November 17, 2015 05:04*

nothing gross here. just a few grown men nerding over a part.


---
**Bud Hammerton** *November 17, 2015 15:05*

**+Zane Baird** Aren't those little brass screw inserts wonderful. I modded every part on my Spider (which still isn't finished yet, wiring left) to use inserts anywhere there was a nut trap. So much easier to design for. In our low torque screw setting, more than enough holding power. In the US, McMaster has both M5 and M3, of course from China you can get more sizes.


---
**Bud Hammerton** *November 17, 2015 15:11*

**+Zane Baird** I had a similar design but abandoned it. Just reading all the nightmares about using two extruders and calibration. May I ask about parts cooling. If you go dual extruder, then I assume you will be dual cooling fan also. If so, with the inlet on the inside of the right fan, how do you propose to get enough airflow? It will be partially obscured by the left fan, once installed.


---
**Zane Baird** *November 17, 2015 15:29*

**+Bud Hammerton**  Yes, those brass inserts are amazing. I bought mine from China, but I think I'll buy my next batch from McMaster (quality seems like it would be much better.) As to the cooling fans, there is actually quite a bit of space in between the two fans, and the arrangement is such that the intakes are not facing each other (if I understood your question correctly). See my earlier post for a rendering of the space between the two.



[https://plus.google.com/115824832953735584348/posts/aPhpWmxCyBh](https://plus.google.com/115824832953735584348/posts/aPhpWmxCyBh)


---
**Bud Hammerton** *November 17, 2015 16:03*

**+Zane Baird**  You mostly got it, obviously if you could get these fans in left and right handed versions I would never mount them with the intakes facing each other. But since that is not the case, my concern was the lack of space in-between them.



The heat set ones McMaster offers have a little flange, so your mounting surface is a bit bigger. They are also a bit thicker walled than the Chinese ones I have. I actually have both kinds in my parts box. But I need some M4 and M2.5 so my only option is a ton of money and difficulty finding a US supplier (who likely buys them from China) or go to AliExpress myself.


---
**Eric Lien** *November 18, 2015 02:08*

**+Zane Baird**​ I just noticed the larger center tube. What does that do? Does it act like a backbone to keep the wiring in a nice arc?


---
**Zane Baird** *November 18, 2015 05:57*

**+Eric Lien** That is correct. It's a piece of 1/4" PTFE tubing I had laying around lab. Works wonderfully and is fairly universal with any 1/4" tubing that is reasonably stiff. I connected this, along with a printed clip that functions to fix the wires for strain-relief ([http://imgur.com/a/Clt31](http://imgur.com/a/Clt31)), to the opening of the lid on the top of the 80x20 extrusion. Obviously I underestimated the diameter of the wire bundle in the design, but a fix is on the way.


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/CM8R6pPkfod) &mdash; content and formatting may not be reliable*
