---
layout: post
title: "This is the best I am getting it"
date: January 26, 2018 23:28
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
This is the best I am getting it.

Using the profile that Eric sent me(slightly modified)



Any tips? No z hop, it’s making my printer crap out and loose z steps.



Using extra restart doesn’t help the blobs and just makes airgaps.



![images/9466eabb6181278f0e107f959ba1fa05.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9466eabb6181278f0e107f959ba1fa05.jpeg)
![images/6fea5f0b69e10c22ab52f373cae20753.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6fea5f0b69e10c22ab52f373cae20753.jpeg)
![images/b07c0b88083d2188837968af75a6be19.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b07c0b88083d2188837968af75a6be19.jpeg)

**Stefano Pagani (Stef_FPV)**

---
---
**wes jackson** *January 27, 2018 00:14*

Whats the accel of your retract? And speed and length?


---
**Carter Calhoun** *January 27, 2018 00:22*

Are you selecting 'aligned z seam' from slicer? 



Layer adhesion - to me- seems mediocre, temp too low? 



Does the printer hitch momentarily at those zits? 



I noticed perhaps what could be slight ghosting on the bow, but nowhere else.  I just noticed this on my eustathios after printing for the better part of a year, it ended up being ACC El values too high.  1500 seems better for me than 2000 or 3000


---
**wes jackson** *January 27, 2018 01:03*

z is too low, accel needs lowering or you need to use igus lm8uu bearings. aligned z seam would've caused a line, this looks like random, so fine tuning retract will help. high accel on retract, lower accel on recover, speed at about 60-70mm/s, length is dependent on your tube. is this a volcano nozzle? those layers look way to big for anything smaller than 0.5. is your extrude width less than 75% of your nozzle size?




---
**Stefano Pagani (Stef_FPV)** *January 27, 2018 02:12*

**+wes jackson** Thanks for all the advice!



What do you mean by z is too low?



The blobs are not random, they are happening at layer start points.



Nozzle diameter is .4, extrusion width is .4 ( will bring to .48)



Coasting distance of .1mm



retact of 4.5mm



retract speed of 60mm/s


---
**wes jackson** *January 27, 2018 21:08*

**+Stefano Pagani** 

z too low meaning your squishing ito the bed a little too much. Random meaning the layer change isnt aligned, the setting is random :) width put at .5, set filament size to 1.72 (this is the actual size, like 2.85 is the actual size of 3mm) reftract looks good, whar is your acceleration for retract? Do m500 and look for o think m207


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/Ps1ZmY3uZKg) &mdash; content and formatting may not be reliable*
