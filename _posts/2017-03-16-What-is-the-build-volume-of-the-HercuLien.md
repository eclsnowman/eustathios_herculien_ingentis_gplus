---
layout: post
title: "What is the build volume of the HercuLien?"
date: March 16, 2017 02:05
category: "Discussion"
author: Jesse Szypulski
---
What is the build volume of the HercuLien? I keep googling around and cannot find a number written in stone or on the GitHub repo. Just that it has a "large build volume". Thanks! :)





**Jesse Szypulski**

---
---
**Jim Stone** *March 17, 2017 03:46*

XYZ i have mine at currently 330,330,320 and i can still go a liiiiitle bit more. consider the machine ATLEAST 1ft in all directions for build




---
**Eric Lien** *March 18, 2017 12:35*

It depends on which carriage you use. Mine is 338x358x320. I think **+Zane Baird**​ recently got his substantially more with his new single extruder compact version.


---
*Imported from [Google+](https://plus.google.com/117035778170308033701/posts/7jQibsDhVci) &mdash; content and formatting may not be reliable*
