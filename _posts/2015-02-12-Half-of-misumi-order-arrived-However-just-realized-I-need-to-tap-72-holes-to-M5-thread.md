---
layout: post
title: "Half of misumi order arrived!! However just realized I need to tap 72 holes to M5 thread..."
date: February 12, 2015 16:12
category: "Show and Tell"
author: Derek Schuetz
---
Half of misumi order arrived!! However just realized I need to tap 72 holes to M5 thread...





**Derek Schuetz**

---
---
**Dat Chu** *February 12, 2015 16:17*

:| Yea, always get misumi to tap both ends just to save time. Or you can print this out and tap yourself with a drill [http://www.thingiverse.com/thing:31492](http://www.thingiverse.com/thing:31492)


---
**Derek Schuetz** *February 12, 2015 16:20*

i thought it was taped....didnt even think it had to be requested


---
**Dat Chu** *February 12, 2015 18:09*

Yeah, you have to specify alterations afterward [http://us.misumi-ec.com/pdf/fa/PDFViewer.html?catalog=metric2010&page=2407](http://us.misumi-ec.com/pdf/fa/PDFViewer.html?catalog=metric2010&page=2407)


---
**Derek Schuetz** *February 12, 2015 18:24*

dam that would have made my life so much easier now i have to use my hand drill


---
**Dat Chu** *February 12, 2015 19:36*

With the 3d printed thing I posted above, life might not be too hard. Hard but not too hard.


---
**Brad Hopper** *February 12, 2015 20:17*

Wow where are 72 holes needed?


---
**Eric Lien** *February 12, 2015 23:13*

Sorry. I was trying to keep costs down. I just bought a $5 tap, a 6pack of beer, and a cap of cutting oil. By the end you get pretty fast.



Also look at this: 
{% include youtubePlayer.html id=BR85I2ILeGw %}
[http://youtu.be/BR85I2ILeGw](http://youtu.be/BR85I2ILeGw)


---
**Derek Schuetz** *February 12, 2015 23:14*

All finished broke 2 taps...


---
**Derek Schuetz** *February 12, 2015 23:23*

So infortanetly 2 extrusion ends will only have 3 screws =\


---
**Dat Chu** *February 12, 2015 23:32*

**+Eric Lien** do you know a supplier for those 3-piece tap set? The one Tom provides is $30 for US people.


---
**Eric Lien** *February 12, 2015 23:53*

I just got a single pass 5mm tap from ebay. But you have to take it slower or they snap ;)


---
**Derek Schuetz** *February 13, 2015 00:35*

Ya take it slow..or widen he holes more prior to tapping them.eventually I widened mine to about 4.5 mm then taped them with an m5 worked perfect


---
**hon po** *February 13, 2015 01:34*

I forgot who mention these combined drill bit before. I'm using cheaper chinese version, and do recommend it:


{% include youtubePlayer.html id=rTODJVYy1r8 %}
[https://www.youtube.com/watch?v=rTODJVYy1r8](https://www.youtube.com/watch?v=rTODJVYy1r8)


---
**Derek Schuetz** *February 13, 2015 02:04*

Those would have cut the time in half 


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/TL2Ef4PjdRo) &mdash; content and formatting may not be reliable*
