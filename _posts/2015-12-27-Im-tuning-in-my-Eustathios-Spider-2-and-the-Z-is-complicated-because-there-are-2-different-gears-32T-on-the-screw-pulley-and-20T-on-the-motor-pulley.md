---
layout: post
title: "I'm tuning in my Eustathios Spider 2 and the Z is complicated because there are 2 different gears 32T on the screw pulley and 20T on the motor pulley"
date: December 27, 2015 19:43
category: "Discussion"
author: Ted Huntington
---
I'm tuning in my Eustathios Spider 2 and the Z is complicated because there are 2 different gears 32T on the screw pulley and 20T on the motor pulley. The ball screw is 1204, so 4mm pitch (I see Eric uses a 2mm pitch lead screw). Looking at Eric's config file my values are somehow double in Marlin. He has 160,160,2560 but mine are closer to half that: 80,80,(1280?) - on a 3D benchy test print I find that 1280 is about 1.2x too small. I reached 1280 by:

200steps * 16microstep / 4mm pitch = 800  * 32/20= 1280 steps/mm

but 1.2x = 1536steps/mm

does anybody know the formula there?





**Ted Huntington**

---
---
**Walter Hsiao** *December 28, 2015 00:33*

The calculations seem fine to me, are you sure you received a 20 tooth pulley and not a 16 tooth version?  If you're using a 16 tooth pulley (or a 40 tooth pulley on the ballscrew, but I don't think that would fit), it should be 1600 steps per mm on z.


---
**Ted Huntington** *December 28, 2015 00:45*

**+Nathan Walkner**

I got these: [http://www.robotdigg.com/product/29](http://www.robotdigg.com/product/29) 1.8degrees so definitely 200 steps (with 16 microsteps set on my ramps 1.4 board). I am realizing now that perhaps Eric is using 0.9 degree (400 steps) motors- but all Robotdigg has in 60mm size is that one I bought, which is 1.8degrees.


---
**Ted Huntington** *December 28, 2015 01:08*

**+Walter Hsiao**

I'm fairly sure they are 20 tooth pulleys- how are you calculating 1600 steps/mm on the z? I calculated 1280, but in practice I have tuned it to around 1462-1494 steps/mm on the z.


---
**Walter Hsiao** *December 28, 2015 01:20*

1600 is only if you're using a 16 tooth pulley, same as your math above, 800 * 32/16 = 1600.  I think I'm running at 3200 z-steps/mm (16/32 pulleys on z, 400 step motor, and 4mm pitch ballscrew).


---
**Ted Huntington** *December 28, 2015 04:29*

**+Nathan Walkner**

maybe there is someway to check on the LCD in Marlin- I just started using the LCD interface.


---
**Ted Huntington** *December 28, 2015 04:33*

oh ok- so the equation I am using is correct. I have no idea, but for whatever reason 1494 seems to be pretty accurate. Thanks everyone for your help.


---
**Ishaan Gov** *December 28, 2015 05:05*

I don't know if **+Eric Lien** runs ball screws or TR12x2 Misumi Leadscrews; if it's the latter, that may explain why his numbers are about double yours


---
**Ted Huntington** *December 28, 2015 06:03*

**+Ishaan Gov**

Thanks Ishaan- the last time I saw Eric still had lead screws on his Eust Spid 2 but wanted and recommended using ballscrew which I did. That explains why the Z is double but what explains the x and y being double? The BOM doesn't specify 1.8 or 0.9 degree so I'm not sure what Eric has, but the RobotDigg only sells 1.8 for 60mm size.


---
**Daniel F** *December 28, 2015 09:05*

Does the system turn smooth? Maybe you are loosing steps. Did you adjust the current of the z stepper? Are the pulleys tightened properly? It could also be slip on the pulleys, as you configured more steps that it should actually be.﻿


---
**Ishaan Gov** *December 28, 2015 13:47*

**+Ted Huntington**, what's the microstep setting on your drivers? 


---
**Ted Huntington** *December 29, 2015 03:56*

**+Daniel F** I tightened the belt and I think that helped somewhat- and I am noticing something unusual on the Z. There is a "thunk" sound in between layers (when the X and Y are moving but not the Z). I changed the step stick and still the thunk is there, regularly for every layer. The firmware (Marlin 1.1.0-RC2) is pulsing the z motor- but I have seen this when the step stick is damaged, and when the speed was set too high. I have another RAMPS board I may try- just to eliminate if it is the PCB- but I think it must be firmware.


---
**Ted Huntington** *December 29, 2015 03:56*

**+Ishaan Gov**

they are all set for 16 microsteps (all 4 jumpers in place on the RAMPS).


---
**Ted Huntington** *December 29, 2015 04:46*

ok wow I totally solved it, whew! When I upgraded from Marlin-1.1.0-RC2 to RC3 I found what was wrong.  In Marlin, never set

#define DISABLE_Z false  

to true- in particular on a Eustathios, because it will temporarily unlock the Z stepper motor and the stepper motor falls back a little. I had set this to true on a different printer because sometimes it is nice to slightly adjust the Z on the first layer- but it totally caused havoc - so I will never do that again. I guess on a RepRap Mendel it doesn't matter as much because there is maybe less weight on the Z motor?



After I set that back to false I find that 1280 works fine- thanks everybody for all your help.


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/cjFDFaqCBxB) &mdash; content and formatting may not be reliable*
