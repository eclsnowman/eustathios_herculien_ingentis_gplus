---
layout: post
title: "wanting to create samples of 3D EeZ that I can give away.."
date: June 02, 2015 23:13
category: "Discussion"
author: argas231
---
wanting to create samples of 3D EeZ that I can give away.. I am thinking like a Ketchup packet... does anyone have any suggestions or know where i can get such a thing done on a low budget





**argas231**

---
---
**Eric Lien** *June 03, 2015 00:08*

EeZ?


---
**Mutley3D** *June 03, 2015 02:08*

**+Eric Lien** Appears to be a print surface treatment/substrate. **+argas231** im usually able to improvise on suppliers for certain things, but a blister packaging for a liquid in small low cost volumes.......interesting challenge. anything beyond plain cardboard boxes for packaging, in small volumes is usually cost prohibitive. When faced with such supplier problems, it often ends up that a cheap home brew solution can be your answer. I did a basic google search (to satisfy curiousity) for "low cost packaging for a liquid" which turned up a few interesting links and other keywords. It also turned up this YT link 
{% include youtubePlayer.html id=CbxBxtbZvUM %}
[https://www.youtube.com/watch?v=CbxBxtbZvUM](https://www.youtube.com/watch?v=CbxBxtbZvUM) which (depending on your volumes) might spark some ideas for a very basic filling machine. You could probably get everything you need from a home depot shop.

I think using a blister pack akin to a small ketchup/mayo pack that you get with your burger is your real issue/hurdle here. Its an automated plant process to hold the blister packet, fill it and seal it. It also wont ship very well, ie due to it being a soft pouch might get easily burst in the post, so in itself it would subsequently need a small protector like a box. Sometimes with such things you just have to hit them head on. I think the small plastic containers you have used previously are probably your ideal solution if you can get smaller ones. Even a medical syringe with an aquarium hose would help with volume control into several small sample containers. Perhaps build a small lever contraption for the syringe a bit like in the linked video above. Would be quite easy to do. HTH.


---
**Mutley3D** *June 03, 2015 02:13*

Occured to me the possibility of using just a small syringe similar to CPU heat paste etc. Supplied with a push on cap? Easy volume control when filling, easy to fill, easy to ship.....just a thought :)

Bear in mind - samples and low budget might be opposing strategies also.


---
**Brandon Satterfield** *June 03, 2015 03:43*

I would like a sample of this, like one to go to **+Eric Lien** as well, if possible. I may be able to assist with volume... 


---
**Isaac Arciaga** *June 03, 2015 07:55*

Dropper bottles! [http://amzn.com/B00KD76CO8](http://amzn.com/B00KD76CO8)


---
**argas231** *June 03, 2015 11:27*

you guys have been very helpful.. Thank you much   as for samples   Contact me  i will send you some   right now  my sample size is larger than i would want...   hence my Inquiry


---
**Brandon Satterfield** *June 03, 2015 18:13*

Very cool. When you get the samples to the size you like, please let me know. I'll happily pay shipping. 


---
**Isaac Arciaga** *June 03, 2015 18:24*

I would like to give it a shot as well. I'm currently using PEI sheet but there will be a printer or two that will have bare glass.


---
**argas231** *June 04, 2015 12:52*

**+Isaac Arciaga** I now have a small smaple size that I like that I can give away for free to various Print Hubs to try out  and for individuals  that would like to try 3D EeZ   send me your address  and I can send you a sample


---
*Imported from [Google+](https://plus.google.com/118416530699505149662/posts/fYraCv1KK8M) &mdash; content and formatting may not be reliable*
