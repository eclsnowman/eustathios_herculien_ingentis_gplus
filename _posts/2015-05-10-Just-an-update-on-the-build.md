---
layout: post
title: "Just an update on the build"
date: May 10, 2015 15:41
category: "Show and Tell"
author: E. Wadsager
---
Just an update on the build.



![images/92f383813d3b8b9fcfed522e33b71880.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/92f383813d3b8b9fcfed522e33b71880.jpeg)
![images/55b4b84ec300e2746dfd1898500cd0f2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/55b4b84ec300e2746dfd1898500cd0f2.jpeg)
![images/9421df873e4c1103d9882f090324339b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9421df873e4c1103d9882f090324339b.jpeg)
![images/607632b9d914daa0b2fbf41e66f8ff40.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/607632b9d914daa0b2fbf41e66f8ff40.jpeg)
![images/dbcd5197c4c7106a33c6fc0196babaf6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dbcd5197c4c7106a33c6fc0196babaf6.jpeg)

**E. Wadsager**

---
---
**Derek Schuetz** *May 10, 2015 16:29*

I love the lack of printed parts on this


---
**Eric Lien** *May 10, 2015 16:37*

I just noticed you are using linear rod bearings on the side. I think you will have issues there. Those shafts rotate, and those bearing can't handle both translation and rotation.


---
**E. Wadsager** *May 10, 2015 16:51*

I do not own a 3D printer . But I can replace some parts with printed parts when its build.



on the linear bearings . I 've thought about it too. If it will be a problem i'm going to replace them with bushings. They is correct also less noisy



btw this is my first build so It will not be perfect the first time. or second :)



thanks for your input


---
**Eric Lien** *May 10, 2015 17:36*

**+E. Wadsager** experimentation is the majority of the fun in this hobby (for me anyway). My first printer took a lot of tweaking too ;)


---
**Владислав Зимин** *May 10, 2015 19:19*

steampunk :D


---
**SalahEddine Redjeb** *May 12, 2015 13:36*

So beautiful, i love it


---
*Imported from [Google+](https://plus.google.com/103157635215674778495/posts/QsjLKMJHPLC) &mdash; content and formatting may not be reliable*
