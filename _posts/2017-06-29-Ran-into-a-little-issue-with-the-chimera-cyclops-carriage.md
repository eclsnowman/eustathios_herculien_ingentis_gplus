---
layout: post
title: "Ran into a little issue with the chimera/cyclops carriage"
date: June 29, 2017 01:21
category: "Discussion"
author: James Ochs
---
Ran into a little issue with the chimera/cyclops carriage.  It doesn't hang down anywhere near as low as the original carriage, so there's nowhere good to mount the z endstop for z-min.  I switched to z-max, so not a huge deal (it's also how my current printer is set up, so I'm used to waiting for the homing sequence), but something to be aware of.



Also, I don't know if the dimensions changed, but I couldn't get the heater blocks oriented to allow the "bird beak" on the fan, so I cut it off :-D



Maybe one of these days I'll get motivated and redo the carriage to hold a bltouch or something to home to z-min.



![images/880989f12bb15cfa3896b1d4417f981a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/880989f12bb15cfa3896b1d4417f981a.jpeg)
![images/8301202a97c26b211f53b9210354e11a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8301202a97c26b211f53b9210354e11a.jpeg)

**James Ochs**

---
---
**Oliver Seiler** *June 29, 2017 01:30*

Nice. The new E3D Heaterblocks using thermistor cartridges are longer, that's why the duct doesn't fit any more. 


---
**Eric Lien** *June 29, 2017 01:30*

Yes, the heater blocks changed. Thanks for the note. I will try to get that changed.



Also you are the first person to do the Chimera on the HercuLien. Sorry about the X-Min endstop. I never thought of that one. There should be a way to fit something in there. I will see if I can think of anything. If I do, I will let you know.



Thanks for the progress updates. I love watching a new printer come together.


---
**Eric Lien** *June 29, 2017 01:39*

Another thing looking at your picture that I wanted to confirm. Many people building the HercuLien miss the fact that the lower leadscrew mounts have a gap between them and the vertical 20x80 vslot. This tolerance is to allow for adjustment of the screws position since the printed parts at top and bottom are likely not perfect. The leadscrew position is set by the upper mount, then lower the bed down with the lower mounts left loose to allow them to find their equilibrium. Then tighten down the lower mounts. if you push them all the way against the 20x80 you will likely get binding when the bed is driven to the bottom. 


---
**Eric Lien** *June 29, 2017 01:39*

![images/8b2f4d803b5f7f97e5302a469a05a1fb.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8b2f4d803b5f7f97e5302a469a05a1fb.png)


---
**James Ochs** *June 29, 2017 01:40*

No worries.  I think an endstop switch with wires that exit in a different direction might work, the problem is that there's not very much room until you are either running into the belts or interfering with the movement.



 I can send you the file for my mod on the fan duct, bit I literally just trimmed off the beak in fusion360 and I don't think its going to be a very good part cooling duct... Still better than nothing.


---
**Eric Lien** *June 29, 2017 01:40*

![images/fce8ef70798e42bce589bc34ec7033f5.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fce8ef70798e42bce589bc34ec7033f5.png)


---
**James Ochs** *June 29, 2017 01:43*

Yeah I caught that, I've got a little over a sixteenth there, It's hard to see in the pictures.  Movement seems pretty smooth by hand, but it might need a touch more adjustment at the top.


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/7fYJShfpi9s) &mdash; content and formatting may not be reliable*
