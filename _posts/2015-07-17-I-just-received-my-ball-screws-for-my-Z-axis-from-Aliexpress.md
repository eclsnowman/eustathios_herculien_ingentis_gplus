---
layout: post
title: "I just received my ball screws for my Z-axis from Aliexpress"
date: July 17, 2015 23:10
category: "Discussion"
author: Bryan Weaver
---
I just received my ball screws for my Z-axis from Aliexpress.  Has anyone else used these?  Can you flip the ball nut around the other direction on the screw, or will I end up with a mess of ball bearings when I take it off?  They are not facing the right direction..



Also, if anyone has used ball screws, do you have some pictures of your Z-axis lead screw mount?





**Bryan Weaver**

---
---
**Oliver Seiler** *July 17, 2015 23:19*

Don't take the nut off the screws just like that or the balls will all fall out. You can screw the nut onto something else of same diameter though and if you buy a nut by itself it comes with a little tube.

Should the balls fall out you can normally carefully feed them back in, but be warned it can easily turn into a mess.

I have tried using ballscrews from aliexpress but given up in the end because I couldn't make them fit easy enough.


---
**Bryan Weaver** *July 17, 2015 23:34*

That's kind of what I figured, I'm nervous to try to flip them around. I think I've got it figured out how to make it work if I can get them flipped.


---
**Eric Lien** *July 17, 2015 23:46*

Smart guy **+Walter Hsiao**​ has just the tool for you: [http://www.thingiverse.com/thing:809141](http://www.thingiverse.com/thing:809141)


---
**Eric Lien** *July 17, 2015 23:47*

You can see he made a custom mount for it: [http://www.thingiverse.com/thing:854267](http://www.thingiverse.com/thing:854267)


---
**Bryan Weaver** *July 18, 2015 00:04*

Awesome! You're the man. 



I've got those bed supports printed. I'm curious about the lead screw base and how that's rigged up.  I also think I screwed up a little bit ordering the 500mm screws instead of the 450mm..


---
**Dat Chu** *July 18, 2015 00:05*

Wow, +Walter Hsiao stuff is so good. His designs are amazing. :(


---
**Eric Lien** *July 18, 2015 03:00*

**+Bryan Weaver** I would hit up Walter for some close-up shots of how he did the base.


---
**Jeff DeMaagd** *July 18, 2015 03:21*

That's a very clever tool. I wondered about reversing the nut. I was tempted to try, but I'd had a bad experience with linear ball rail cars spewing their bearings.


---
**Frank “Helmi” Helmschrott** *July 18, 2015 04:59*

You could have ordered the Nuts in the right direction. Worked for me. Also I ordered the screws machined identically to the lead screws from the bom. This way the original screws support works. 


---
**Bryan Weaver** *July 18, 2015 13:26*

Yeah, I got a little trigger happy and ordered before doing enough research. I still think I can make it work with some minor modifications to the base. Going to try to print it today and see how it works out. 


---
**Igor Kolesnik** *July 23, 2015 10:08*

**+Frank Helmschrott** where did you order them? were those ballscrew of a good quality?


---
**Bryan Weaver** *July 23, 2015 12:51*

I got mine through the Golmart store on Aliexpress.  I'm very happy with the quality.  I ordered the 500mm, so I'm going to end up having to cut mine down.  I didn't read the fine print, it seems it's as easy as sending them a sketch to machine the screws exactly how you want them.



[http://www.aliexpress.com/store/group/Ballscrew-and-shaft-coupler/920371_253896180.html?categoryId=100004740](http://www.aliexpress.com/store/group/Ballscrew-and-shaft-coupler/920371_253896180.html?categoryId=100004740)


---
**Frank “Helmi” Helmschrott** *July 23, 2015 16:46*

**+Igor Kolesnik** like **+Bryan Weaver** i ordered mine from Golmart. Just send them a message with a simple sketch, tell them the lenght and you will only pay the price of the next length they offer and you will get one that perfectly fits your need. I also suggest to tell them how you want the nut mounted (additionally show it in the sketch). That worked well for me. Otherwise you would need to print yourself a tool to change the nuts direction. 


---
**Frank “Helmi” Helmschrott** *July 23, 2015 16:48*

here's a quick screenshot of the sketch i sent them. I wouldn't do the upper 14mm of machining down to 12mm again. that was just useless and doesn't make it prettier [https://www.evernote.com/l/AAFIXkPA9uxLk6zdERXhYckfWzgZ2IbuIBo](https://www.evernote.com/l/AAFIXkPA9uxLk6zdERXhYckfWzgZ2IbuIBo)


---
**Eric Lien** *July 23, 2015 17:00*

**+Frank Helmschrott** Only nice thing about turning down the top is you have the option to make the top captive if you wanted. I might have made it 8mm instead of 10mm to use standard skate bearings though. 


---
**Frank “Helmi” Helmschrott** *July 23, 2015 17:07*

you're righ of course, **+Eric Lien** but i'm not sure if there would be any sense in making it captive. I'd just leave it away if i had to order one again.


---
**Eric Lien** *July 23, 2015 17:37*

**+Frank Helmschrott** for small screws like a 5mm on a prusa captive ends is a detriment, but I get less Z-ribbing on my HercuLien with captive tops than I do on Eustathios. Odd thing is it comes and goes on Eustathios. Never bad, but I see it in the correct lighting. So on the stiffer screws used in larger printers I think I prefer both ends captive. But I think that's just my subjective preference.


---
**Frank “Helmi” Helmschrott** *July 23, 2015 18:25*

Hmm strange. Would be interesting if this also happens with Ballscrews (you are on the ones from the BOM, right?) - Also this should normally be avoided by the linear bearing on the Z-Rods.


---
**Eric Lien** *July 23, 2015 19:17*

Yes I am using similar lead screws from the BOM on both HercuLien and Eustathios. I think it comes down to if there is friction between the screw and the nut, it creates a torque when doing fast layer changes. The force likely lowers as it breaks the static friction barrier, so this changing rotational friction based on the right hand rule translates to a horizontal force into the linear guide rails and bearings. The guide rail and bearing are of course not perfectly ridged, so they create minor variations. 



Long story short ball screws are better than lead screws for fast layer changes because there is little to no static friction to overcome, so a much lower moment induced horizontally. I.E. I am jealous of the fact that you have ball screws :)


---
**Ted Huntington** *July 29, 2015 21:28*

So in Frank's sketch, the ballscrew should be reversed- is that correct? Nothing else is needed to tell the ali vendor besides the 46x8mm end (and a 14x8mm other end if you want to have the option of making the top end captive).


---
**Bryan Weaver** *July 29, 2015 21:33*

Frank's sketch is what you want.  If you order them for Golmart as-is, the nut will be opposite of that and you will have to flip them around.


---
**Ted Huntington** *July 29, 2015 21:34*

actually I should correct myself the ballnut should be reversed


---
**Ted Huntington** *July 29, 2015 21:35*

oh ok got it thanks!


---
*Imported from [Google+](https://plus.google.com/111820797809026464429/posts/PDXRJrXDSPU) &mdash; content and formatting may not be reliable*
