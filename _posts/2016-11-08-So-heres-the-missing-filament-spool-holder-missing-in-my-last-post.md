---
layout: post
title: "So here's the missing filament spool holder missing in my last post"
date: November 08, 2016 15:05
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
So here's the missing filament spool holder missing in my last post. Shout out to the creator Maxime. Find his post here [https://plus.google.com/117610054892104446719/posts/b5PfGWD7yuD?sfc=true](https://plus.google.com/117610054892104446719/posts/b5PfGWD7yuD?sfc=true) 





![images/d2bf7e187f88bda5e184d56904815094.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d2bf7e187f88bda5e184d56904815094.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**Maxime Favre** *November 08, 2016 16:19*

Nice ! I'll try the double bearing mode 


---
**Frank “Helmi” Helmschrott** *November 08, 2016 17:23*

oh yeah didn't mention that I did this "upgrade". Looks a bit more trusty to me :)


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/SjePNAbdYLh) &mdash; content and formatting may not be reliable*
