---
layout: post
title: "Does anyone know if the hexagon hotends will fit in the carriage without modification?"
date: January 18, 2015 21:18
category: "Discussion"
author: James Ochs
---
Does anyone know if the hexagon hotends will fit in the carriage without modification? (and any thoughts re hexagon vs e3d would be appreciated...)





**James Ochs**

---
---
**Ubaldo Sanchez** *January 18, 2015 21:23*

Haven't used the e3d, but the hexagon is plain out awesome. It's very small and filament flows well. 


---
**Eric Lien** *January 19, 2015 00:22*

It would not work for my carriage since it is shorter. But if there is a model out there for the hexagon I could adjust the carriage to fit. Another concern is if the bed can reach the nozzle if it is too short.


---
**James Ochs** *January 19, 2015 04:27*

Ahh ok, I'll take that into consideration when I look at it.  Got a while before I really even need to think about hotends ;)  Thanks for the info...


---
**Eric Lien** *January 19, 2015 14:29*

I just thought are you referencing mounting it on HercuLien or Eustathios? I was thinking Eustathios. For HercuLien there should be no issue since there is Z travel to spare.


---
**James Ochs** *January 19, 2015 14:49*

I'm planning a herculien, so I guess that makes the hexagon an option without too much major tinkering.  I'm still waiting for a kickstarted printer to show up, but one of the parts I have is a hexagon so I can take some measurements and see how that lines up with the carriage model.  One I get a working printer, I'll fiddle with it and see how it goes in to the existing design.


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/HThmX7iJ3Y8) &mdash; content and formatting may not be reliable*
