---
layout: post
title: "Hey All, I am trying to upload new firmware to my RAMBO board and I am getting this error message.....I changed the COM port and the board type already;"
date: September 23, 2015 18:05
category: "Discussion"
author: kaley garner
---
Hey All,



I am trying to upload new firmware to my RAMBO board and I am getting this error message.....I changed the COM port and the board type already; but, I must be missing something. Any ideas?

![images/8d75b176e7ecb8180f0cc6e5aaea9b58.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8d75b176e7ecb8180f0cc6e5aaea9b58.png)



**kaley garner**

---
---
**Gústav K Gústavsson** *September 23, 2015 20:35*

What's  the bauð rate? What's  the default baud  rate for your board? Have you configured that?


---
**Gústav K Gústavsson** *September 23, 2015 20:36*

Type of board?


---
**Miguel Sánchez** *September 23, 2015 21:15*

are you powering the logic of the board? remember Rambo does not work from just USB power.


---
**Gústav K Gústavsson** *September 23, 2015 21:25*

Am not familiar with RAMBO per see but found this:

USB Driver



Windows requires a driver to communicate with RAMBo. Download this file File:RAMBo USBdriver.zip and unzip it into a known location on your computer. In windows 7, plug in your RAMBo board, and let windows fail to find the driver. Then, go to the start menu, right click on computer and click properties. On the left, click on Device Manager. Scroll down to Unknown Devices, and right click on RAMBo. Choose Update driver. CLick on "Browse my computer for driver software", then click on "Let me pick from a list of device drivers on my computer", then click the button for "Have Disk" and then click browse and point it to the file you downloaded above.

Linux and Mac use the built in CDC driver. RAMBo should show as a option in your 3D printer control interface (/dev/ttyACM0 , etc.).


---
**Gústav K Gústavsson** *September 23, 2015 21:26*

[http://reprap.org/wiki/Rambo](http://reprap.org/wiki/Rambo)


---
**Miguel Sánchez** *September 23, 2015 21:30*

Make sure you provide power to "Motors&Logic" input and try again


---
**kaley garner** *September 24, 2015 15:45*

Thanks for your input! I checked the baud rate and it looked good, its also getting good power...im thinking that the com port is not being recognized- gonna reload the driver and see if that does anything....any other ideas?


---
**Miguel Sánchez** *September 24, 2015 16:11*

What is your OS version? If windows 8.1 are you aware of how to install non signed drivers?


---
**kaley garner** *September 24, 2015 16:19*

Its windows 7, 32 bit. ....its strange: if you look at the drivers in the preferences it shows COM5 and thats what I have it set on


---
**kaley garner** *September 24, 2015 16:22*

but it doesn't seem to be recognizing it in arduino


---
**Miguel Sánchez** *September 24, 2015 16:53*

Did you manage to upload a firmware on it before? Does it still connect using pronterface on COM5?


---
**kaley garner** *September 24, 2015 18:57*

Hey Miguel, This was one that we bought pre-built so I didn't have to put the initial firmware, but it was running well... We are now upgrading to dual extruders and I have assembled all of the mechanical side to do it so now having to update the firmware for the dual extruder support.


---
**Miguel Sánchez** *September 24, 2015 20:47*

My point is that if the port is working and you cannot upload a new sketch then the bootloader might not be working. 


---
*Imported from [Google+](https://plus.google.com/104940700298630363140/posts/fNohfkYsz1B) &mdash; content and formatting may not be reliable*
