---
layout: post
title: "Hey Eustathios Spider builders, did anyone who ordered Ball Screws from Golmart need to redesign the Z_Axis_Bed_Support to accommodate the larger Ball Screw Nut?"
date: November 15, 2015 18:54
category: "Discussion"
author: Bud Hammerton
---
Hey Eustathios Spider builders, did anyone who ordered Ball Screws from Golmart need to redesign the Z_Axis_Bed_Support to accommodate the larger Ball Screw Nut? The original BOM lists a Misumi lead screw which would accept either an MTSFR, MTSGR or MTSNR Flanged Lead Screw Nut, all of which are significantly smaller. Fortunately I did not pre-print that particular part and comparing the model file to the actual part tells me it doesn't fit. Aside from the very obvious moving of the mounting holes, the overall Ball Screw Nut is bigger. If someone else has already done the redesign I would appreciate a link to a copy. If not I will take the one **+Walter Hsiao**  designed and modify it accordingly. Just trying to save myself a bunch of work/delay.

![images/419fc292b712224cf40f862a8133bf46.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/419fc292b712224cf40f862a8133bf46.jpeg)



**Bud Hammerton**

---
---
**Eric Lien** *November 15, 2015 21:29*

I haven't made one yet but the one by **+Walter Hsiao**​ is really nice. I would probably go with that one. Do you mean you're going with a larger nut than the one Walter has? He's using the ball bearing lead, and looking at the layer stacking he's getting on his prints I wish I'd gone that way too.


---
**Bud Hammerton** *November 15, 2015 21:50*

His doesn't match the GolMart part.  So It looks like I will be doing a slight mod. After I am done I'll post the source files. ﻿


---
**Eric Lien** *November 15, 2015 22:44*

Weird. He was the one who suggested golmart as a source.


---
**Igor Kolesnik** *November 15, 2015 23:30*

It does match. His part was designed for 1004 ballscrew. Fits like a glove


---
**Bud Hammerton** *November 16, 2015 00:01*

I don't see a 1004 ball screw from Golmart, I see a 1204, and I see that he only used 4 of the 6 mounting holes. So where did he source that if not Golmart?


---
**Igor Kolesnik** *November 16, 2015 00:03*

My bad, 1204.


---
**Bud Hammerton** *November 16, 2015 00:04*

I just saw that on Thingiverse.


---
**Bud Hammerton** *November 16, 2015 00:08*

Except he must have built his bed differently. If you look at the actual model, not the photograph you will see that he used mounting holes in sets of three, which simply doesn't make sense when your Bed only has a single width 2020 extrusion.



I am not even sure, since it's obscured by the perspective, that the photo matches the model.



I am re-downloading his STEP file to make sure I didn't mess with it at some point and forgot. But I'm still thinking a simplification may be in order.


---
**Eric Lien** *November 16, 2015 00:15*

I think the sets of holes was to give options for mounting heights.


---
**Bud Hammerton** *November 16, 2015 00:21*

I see what the issue was, as I mentioned and forgot. When I first downloaded and imported it I modified it for use of a leadscrew. Then promptly forgot about it, since I did that over three months ago. Now that I have the Ball Screws, I went to print it and saw that it didn't match. Totally my fault for not remembering what I did.


---
**Bud Hammerton** *November 16, 2015 00:25*

In any case, while we were trying to figure out my stupidity. I modified his design slightly and changed some of the aesthetics. Didn't like that the LM10LUU was not totally encased and  added the remaining two mounting holes for the flanged nut. My first attempt is printing as we type.

 


---
**Walter Hsiao** *November 16, 2015 20:26*

Yeah the extra holes are for adjusting the bed height, I wasn't sure how high I was going to mount the hotend when I designed it.  I ended up needing the extra holes when I changed the extruder (currently using the middle row).  Enclosing the bearing will probably decrease the z-build volume by about 15mm if that's something you care about.  I don't think I've used more than half the z-build height myself.  


---
**Florian Schütte** *November 18, 2015 22:16*

I'm also on ordering ballscrews. Is the  Z_Axis_Bed_Support the only thing i have to change in printed parts? **+Walter Hsiao** may you provide a direkt link to the ball screws you used? Is there any difference in Measurements (46x8mm OD Step, 425mm length)?


---
**Igor Kolesnik** *November 18, 2015 22:49*

You can just ask the seller to machine them the same way as leadscrews. This way they will be drop fit for existing lower holder and use Walter's parts for upper 


---
**Florian Schütte** *November 18, 2015 23:06*

Thank you **+Igor Kolesnik** . So same dimensions in length and od-step.


---
**Igor Kolesnik** *November 19, 2015 13:37*

+Florian Schütte The original leadscrew has 2 mm/rev so you need to take that in to a count (ballscrew is 4 mm/rev). One side faced other machined to L 46 mm OD 8.


---
**Igor Kolesnik** *November 19, 2015 13:38*

**+Florian Schütte** The original leadscrew has 2 mm/rev so you need to take that in to a count (ballscrew is 4 mm/rev). One side faced other machined to L 46 mm OD 8.


---
**Bud Hammerton** *November 19, 2015 14:16*

**+Florian Schütte** I ordered direct from their link for a 450 mm Ball screw, [http://www.aliexpress.com/item/Ballscrew-RM1204-L-450mm-with-end-machining-Single-Ballnut-for-CNC/1588701082.html](http://www.aliexpress.com/item/Ballscrew-RM1204-L-450mm-with-end-machining-Single-Ballnut-for-CNC/1588701082.html) as 450 mm is long enough I did not ask them to cut it down in length, just to machine the ends per PDF drawing. I had to attach the PDF to the order. Price remained unchanged from the link. I asked them to invert the nut, but they did not. So be prepared to print this item from Thingiverse (thank you **+Walter Hsiao**) [http://www.thingiverse.com/thing:809141](http://www.thingiverse.com/thing:809141). Also the little bit extra by using 450 mm that sticks out the top does not interfere with any of the gantry mechanicals.


---
**Florian Schütte** *November 19, 2015 14:30*

**+Bud Hammerton** Invert the nut?






---
**Bud Hammerton** *November 19, 2015 16:24*

Yes, in order to mount the ball nut properly, it needs to be removed, turned upside down and re-installed. If you do that you will have a mess of balls all over unless you use the item I linked to to keep the balls in place.


---
**Igor Kolesnik** *November 19, 2015 16:52*

Or you can just wrap some paper around the 8 mm part of the screw so it will be around 11.4-11.5 mm in total. Screw the nut on it, flip it and screw it back


---
**Bud Hammerton** *November 19, 2015 16:55*

You could, but there is risk involved there, that doesn't exist to the same level as with the printed threaded model. 


---
**Florian Schütte** *November 19, 2015 20:04*

**+Bud Hammerton**, **+Igor Kolesnik** I'm sorry, but i dont understand why i have to flip the nuts.


---
**Igor Kolesnik** *November 19, 2015 20:07*

It is usually flange down. To connect it to carriages it has to be flange up


---
**Igor Kolesnik** *November 19, 2015 20:21*

**+Bud Hammerton**​ you can use fancy tools if you have a printer to make them. This is my first so I was not able to do that. Had to improvise, not the first time))))


---
**Florian Schütte** *November 19, 2015 20:21*

Ok. Good to know. Next time i will tell them to machine the right side of the ballscrew ;)


---
**Bud Hammerton** *November 19, 2015 21:30*

Well **+Igor Kolesnik** , I totally understand, my first printer was and still is a Robo3D that I got cheap. Not the best, not the fastest, but helped me learn a lot about printing, Marlin and other things. So I have been building what I need to build the Spider. But I didn't exactly start from scratch. I have some background in architecture and mechanical drawing, among other things. Built a lot of stuff out of wood, even built my own UAV. So that kinda helps. Sometimes I forget that some people interested in 3D printing really have very little background in manufacturing or in making stuff in general.


---
**Igor Kolesnik** *November 20, 2015 01:13*

**+Bud Hammerton** It is not that I don't have any background. I worked two years for Materialize and have an engineering degree. My problems were that I did not have access to the printer and I really like McGyvering things as long as it is not affecting quality)))))  


---
**Seth Mott** *November 24, 2015 03:33*

I use Golmart 1204 ball screws with Walters bed support.  It works out great. 


---
**Bud Hammerton** *November 24, 2015 15:20*

Thanks for all the replies drawing attention to my forgetfulness, I haven't finished up my printing yet since I ran out of filament about 3/4 of the way through printing **+Walter Hsiao** bed parts. I just got more yesterday, but since I am upgrading my source printer (from threaded rods to a real lead screws) I am holding off printing until it is completed. In the meanwhile I started extending the stepper wiring and putting on sheathing and connectors. Paracord makes a much more flexible wire sheathing material than TechFlex for small wires like stepper and limit switch wiring. All bigger bundles will be in TechFlex.


---
*Imported from [Google+](https://plus.google.com/+BudHammerton/posts/QVFF4fgZzJ6) &mdash; content and formatting may not be reliable*
