---
layout: post
title: "So... if there is anyone out there willing to help me unravel the mysteries of dc motors w/ optical rotary encoders, for ingentis variants, my research has led me here"
date: September 24, 2014 03:21
category: "Discussion"
author: D Rob
---
So... if there is anyone out there willing to help me unravel the mysteries of dc motors w/ optical rotary encoders, for ingentis variants, my research has led me here. I state once again that I suck with the arduino coding. I believe this is the next logical step in 3d printing. DC motors are faster lighter and cheaper. The encoders are cheap and easy to salvage as are the motors (2d printers). 2d printers moved to this tech while gaining resolution and cutting costs. HELP ME!!! I can figure out the circuitry necessary and the microcontroller hook ups to make it work. I haven't the foggiest on how to make it play nice with current boards though; which is a must to make this viable.

[http://arduino-pi.blogspot.com/2014/05/arduino-pid-library-for-position.html](http://arduino-pi.blogspot.com/2014/05/arduino-pid-library-for-position.html)





**D Rob**

---
---
**D Rob** *September 24, 2014 03:31*

**+Miguel Sánchez**  mentioned to me that this would be best with a mcu for each motor. if so the leonardo micro knock offs are like 8 bucks eack on ebay. we could have each programmed to send signals to the normal reprap electronics. Even have the micro translate rotary encoder position to endstop commands. Then the output pins on the micro would send a signal telling the mega,due,etc... that max/min endstop has been hit and no physical endstop would be required. and with the micros taking the processing power the mega could have room for other features. Since the stepper drivers wouldn't be there to utilize all pins. The micros and h bridges could free some pins up. Anyone with a better idea see something I'm missing? I could very well be wrong.


---
**Miguel Sánchez** *September 24, 2014 05:59*

The way they do in in the article above is no longer based on pulse/direction pins which was your previous requirement. For doing it on a single processor you need speed and to capture all the interrupts caused by the encoders. In the example above I do not see how they can keep up with the four encoders, as only two external interrupts are available and the source code mentions only X/Y (two axis support). Not keeping the pulse/direction interface will require some modification of current firmware (Marlin?). Still, it is a very interesting direction you are exploring (though I am doubtful about its cost effectiveness if all elements are to be bought).


---
**D Rob** *September 24, 2014 07:02*

**+Miguel Sánchez** you are talking about cost effectiveness with the guy who put 6 1204 c5 ground ball screws into 1 printer ;). I think the speeds and super accurate encoders would be worth $16 dollars in arduino micro clones, $ 5 bucks in h bridges, and scrounged encoders and motors. Dead printers are everywhere. hell drv8825 drivers are most of the replaced cost. I would probably keep the z as a stepper as I am not looking to increase its speed like the x and y axis. What I believe is easiest is for the arduino micro to emulate a stepper. It would read all of the encoder information and process commands to and from the board. It would be possible to print an enclosure that houses the motor, encoder, arduino micro, h-bridge, and also serves as it's own t-slot mounting bracket. Just add a drive pulley and connect to the power, and maybe use something like a blank a4988 driver board as an interface for connecting wires to a standard printer electronics board. Also if we like this setup we could design some dev boards using a mpu directly on the same board as the h-bridge and the stepper driver adapter could be wired on the other end. This would be less to put in an enclosure. I want this to be an easy upgrade anyone could do should they desire. If we make the boards popular china will do the rest! Then we'll just get it off ebay and resolder everything. Then we'll crack open a inkjet or two and away we go!


---
**D Rob** *September 24, 2014 07:14*

**+Miguel Sánchez**  do you think the due could handle all 4? use it alongside the standard electronics. Or even embed the mpu into a dev board with h-bridges and send 4 or 5 wires to each motor+encoder unit. two to power the motor and 2 or 3 for the encoder signals.


---
**Wayne Friedt** *September 24, 2014 07:33*

Wish i could help you out with this friend but my coding skills are far from this also. Great direction you are going tho.


---
**Miguel Sánchez** *September 24, 2014 07:49*

**+D Rob** I haven't given much thought but in order to keep up with the pulses at least one of the two signals of each encoder needs to cause an interrupt. Arduino UNO has two external interrupt pins (but Mega has six and Leonardo five!) , so I can see how it can handle two encoders.



But given the price of a microcontroller like Attiny85 it may well be better to have one controller per motor and keeping the step/dir interface for compatibility with existing software. 



I had organized 3D printer building workshops and I have a critical eye for easy scaling. While using scrapped motors or controllers you have on your desk is great, other people may need to buy all of it in order to replicate your build. It is in that case when an approach might prove not cost effective unless the sum of all the costs is still lower than what you are replacing. Currently a stepper driver can cost less than $4 and an stepper around $8 in bulk, so that is the cost we are fighting for. I guess a DC motor can be had for $5 so this leaves around $7 for encoder and driver electronics. But I can see motor+encoder can reach interesting price points if bought in bulk: [http://www.aliexpress.com/item/Wholesale-20pcs-Johnson-Standard-130-motor-Green-Micro-DC-motor-with-encoder-free-shipping/1918720646.html](http://www.aliexpress.com/item/Wholesale-20pcs-Johnson-Standard-130-motor-Green-Micro-DC-motor-with-encoder-free-shipping/1918720646.html)



However, let's not forget low cost brushed motors have a limited lifetime :-)


---
**D Rob** *September 24, 2014 08:05*

**+Miguel Sánchez** see what I mean about cheap 3pcs arduino nano: [http://www.ebay.com/itm/Mini-USB-Nano-V3-0-ATmega328P-CH340G-5V-16M-Micro-controller-board-For-Arduino-/321437771366?pt=LH_DefaultDomain_0&hash=item4ad72f2666](http://www.ebay.com/itm/Mini-USB-Nano-V3-0-ATmega328P-CH340G-5V-16M-Micro-controller-board-For-Arduino-/321437771366?pt=LH_DefaultDomain_0&hash=item4ad72f2666)



h-bridge:

 [http://www.ebay.com/itm/For-Arduino-HG7881-H-bridge-Stepper-Motor-Dual-DC-Motor-Driver-Controller-Board-/171095655752?pt=LH_DefaultDomain_0&hash=item27d6188548](http://www.ebay.com/itm/For-Arduino-HG7881-H-bridge-Stepper-Motor-Dual-DC-Motor-Driver-Controller-Board-/171095655752?pt=LH_DefaultDomain_0&hash=item27d6188548)



 or:

 [http://www.ebay.com/itm/800-MA-TWO-L9110S-H-bridge-DC-Stepper-Motor-Drive-Controller-Board-for-Arduino-/201176049640?pt=LH_DefaultDomain_0&hash=item2ed706e7e8](http://www.ebay.com/itm/800-MA-TWO-L9110S-H-bridge-DC-Stepper-Motor-Drive-Controller-Board-for-Arduino-/201176049640?pt=LH_DefaultDomain_0&hash=item2ed706e7e8)



but this is probably best for this application:

[http://www.ebay.com/itm/Dual-H-Bridge-DC-Stepper-Motor-Drive-Controller-Board-Module-Arduino-L298N-HMY-/291163316473?pt=Home_Automation_Modules&hash=item43caafa0f9](http://www.ebay.com/itm/Dual-H-Bridge-DC-Stepper-Motor-Drive-Controller-Board-Module-Arduino-L298N-HMY-/291163316473?pt=Home_Automation_Modules&hash=item43caafa0f9)



motor/encoder  which is typical in a 2d printer. these things have much higher resolution than the 200 step per rotation stepper motors.:

[http://www.ebay.com/itm/DC-3V-6V-12V-encoder-Speed-motor-Worm-gear-motor-AB-phase-888-lines-/111415953731?pt=LH_DefaultDomain_0&hash=item19f0e88943](http://www.ebay.com/itm/DC-3V-6V-12V-encoder-Speed-motor-Worm-gear-motor-AB-phase-888-lines-/111415953731?pt=LH_DefaultDomain_0&hash=item19f0e88943)


---
**D Rob** *September 24, 2014 08:32*

**+Miguel Sánchez**  look at this gold nugget:

[http://www.ebay.com/itm/DAGU-4-Channel-DC-Motor-Controller-with-Encoder-Support-/121293557293?pt=Model_Kit_US&hash=item1c3da8ce2d](http://www.ebay.com/itm/DAGU-4-Channel-DC-Motor-Controller-with-Encoder-Support-/121293557293?pt=Model_Kit_US&hash=item1c3da8ce2d)


---
**D Rob** *September 24, 2014 08:36*

**+Mike Miller** you are invited to this discussion as you showed some interest the other day.


---
**Miguel Sánchez** *September 24, 2014 08:38*

**+D Rob**  It seems you can pair this with a Mega2560 board to have a four motor controller. More info [http://www.robotshop.com/en/dagu-4-channel-brushed-dc-motor-controller.html](http://www.robotshop.com/en/dagu-4-channel-brushed-dc-motor-controller.html)


---
**D Rob** *September 24, 2014 09:20*

**+Miguel Sánchez** do you think this could be "plugged" into the vacant stepper driver plugs on a ramps, rumba, etc? If so this is a simple solution for around $10 per motor/encoder and $23 for the driver board which covers 4 motors. A single extruder all DC motor conversion would only cost $63. 4x $5 a4988 is $20 and $12 per steeper motor is common so $48 for 4 total $68. DC motor are $5 cheaper.﻿


---
**D Rob** *September 24, 2014 09:25*

Anything similar at 24v?


---
**Miguel Sánchez** *September 24, 2014 09:48*

**+D Rob** For plug and play I reckon one driver per motor is going to be simpler. More cost effective might be to have a single controller for four (or more) motors.  



Now that I think about it, while encoder signal is time critical, the step input might become critical too once you reach a certain speed (meaning you might want it to be an interrupt-based input too).



Going up in the voltage does not work the same as for the steppers as these H-bridges are (usually) not constant current drives, meaning that raising the voltage above nominal voltage may damage the DC motor (and maybe increase the current beyond H-bridge limit).


---
**Mike Miller** *September 24, 2014 11:20*

I think the guy that pulls this off will do a GREAT service to the hobby. The motors are significantly cheaper, and our printer suffer (when not properly adjusted) from a lasck of closed-loop-ed ness. 



I'll watch with great interest as it's a bit out of my skillset. 


---
**Mike Miller** *September 24, 2014 11:29*

This is an interesting Proof of Concept, if not quite there from a precision standpoint...
{% include youtubePlayer.html id=4YLTHjbZVP0 %}
[DC Motor with Quadrature Encoder](https://www.youtube.com/watch?v=4YLTHjbZVP0) 



Thinking about the tech in inkjet printers:Anything that's capable of reliably spitting out 22 pages per minute, laying down 300dpi ink droplets on a sheet of paper (8.4 million addressable dots from 4 channels (CMYK) with dithering) could SURELY place some plastic.


---
**ThantiK** *September 24, 2014 11:44*

**+Mike Miller**, the ink droplets are that high res due to thousands of nozzles on the end of the print head.  Not due to positioning accuracy of servos.


---
**Mike Miller** *September 24, 2014 11:53*

Interesting thing that. :) There's also the benefit of steady state operation, the motor doesn't need to stop partway across the page and change direction. 



Still, it's an interesting lesson in driving the technology to the cheapest possible solution. 


---
**Nick Parker** *September 24, 2014 15:18*

I think a BBB running LinuxCNC will be your fastest route to a servo 3d printer.



Or really just a LinuxCNC desktop box to start with. You can hack together a velocity controlled extrusion setup pretty quickly using slave axes and such, and LinuxCNC was born for servo control.


---
**D Rob** *September 24, 2014 15:36*

**+Nicholas Seward** you are the guru of all things unique in reprap. What say you?


---
**Nicholas Seward** *September 24, 2014 17:12*

**+D Rob** I would disagree that DC motors would be cheaper for a hobbyist...currently.  What I am saying is that we are close to a local minimum with our current designs so we will have to spend more money and time in the short term to develop it and then you will have to wage an uphill PR battle to win many over to the more expensive solutions that will eventually be cheaper.



That said, it sounds like a fun project.  You just won't want to use the standard RepRap electronics that have the stepper solutions built in.  I am sure you could make a Pololu drop in that will help you out.  (Of course this won't allow for it to be backdrivable and trainable which would be awesome.)  If I were you I would grab the RAMPS plans and throw away all the stepper circuits and insert your own drive and sensor circuits.


---
**Miguel Sánchez** *September 29, 2014 16:39*

I have been doing some more browsing. These motors [http://www.aliexpress.com/item/New-boutique-12V-DC-motor-385-servo-motors-speed-encoder-888-line-AB-phase-encoder-For/1345460690.html](http://www.aliexpress.com/item/New-boutique-12V-DC-motor-385-servo-motors-speed-encoder-888-line-AB-phase-encoder-For/1345460690.html) seem like they could do the job and come with, what I suspect, a low teeth count MXL pulley that might directly move a carriage (not sure about the torque of these motors).



These motors cost less that $6 each with shipping. Torque information is not provided but plugged to the board **+D Rob**  mentioned above that can manage four motors you would have a per-channel cost of around $12 motor+electronics drive which starts to seem competitive. Still you need to couple that with the brains. Cheapest solution might be to drop one ATtiny85 for each channel to offer dir/step interface to interact directly with Marlin/Repetier software running over a Mega. I start to see that as feasible cost-wise.



Aren't you sold **+Nicholas Seward** ?


---
**Miguel Sánchez** *September 29, 2014 16:44*

I stand corrected: Stall torque: 440 g-cm (43.2 mN-m) seems to be the motor's torque. Way way below the 4kg-cm of many steppers. Extruders would need a higher gear rate here and carriages may not achieve very high acceleration then.


---
**D Rob** *September 29, 2014 19:03*

with the 1204ball screws used in my gantry the torque conversion should already be taken care for the most part.


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/8C1TGDxnAjv) &mdash; content and formatting may not be reliable*
