---
layout: post
title: "Had a print fail on me this weekend due to my Bowden tube between extruder and hotend popping out from the bondtech side"
date: October 17, 2017 07:05
category: "Discussion"
author: Ben Delarre
---
Had a print fail on me this weekend due to my Bowden tube between extruder and hotend popping out from the bondtech side. Considering all the problems I've had tuning the bowden I am considering switching to the Bondtech BMG. **+Eric Lien**​ I saw you posted a new carriage for that. How was the experience?





**Ben Delarre**

---
---
**Eric Lien** *October 17, 2017 10:47*

I never tested that one. Just a proof of concept carriage. But I use a BMG on the BoxR prototype. I love it.


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 15:23*

I also love the BMG but again, on different printers. 


---
**Ben Delarre** *October 17, 2017 15:24*

Mostly concerned with whether or not the eustathios 8mm rods will take the weight and what sort of print speeds I can expect. I've never been able to get a good print above 60mm/s with the bowden anyway so wasn't sure how much I'd lose switching to direct.


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 15:27*

Hmmm... I run the BMG as a Bowden extruder on my DICE and print at 100mm/s.  Now that is a shorter tube length by far, but I'm happy with it. 



I'm using 60mm/s on my i3's. I've found that the best as far as print quality. 85mm/s gets more into "draft" quality territory. 


---
**Ben Delarre** *October 17, 2017 15:28*

Probably worth a shot then. I also have an i3 I need to refurb so maybe I'll do that first then I won't be without a printer in case it all goes horribly wrong!


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 15:33*

I'm reworking a P3Steel and multiple Wanhao Di3's. I've gotten quite fond of the Titan aero for that application. 

These are currently my workhorses. 



Anything not as simple as direct drive at 60mm/s, I'll recommend Bondtech. ![images/f7f5998b365c722d46a941d33dc51842.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f7f5998b365c722d46a941d33dc51842.jpeg)


---
**Ben Delarre** *October 17, 2017 15:51*

I feel like much tinkering is about to happen....half tempted to take the leadscrews out of my eustathios, trade them up for the ballscrew version then install the leadscrews in the i3. Titan aero in the i3, and BMG in the eustathios!


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 15:52*

As long as you have one working printer at all times... 


---
**Ben Delarre** *October 17, 2017 15:53*

Right? In theory what could go wrong? Haha...also, man I've always wanted to print that castle, but never imagined I could get the detail so good. Great job.


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 15:54*

It was a last minute print for Maker Faire. Took 18 hours. Then someone at the faire walked off with it. Oh well, plastic is cheap. 


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 16:14*

My BMG is on DICE with say a 1ft Bowden tube length. I'm printing at 100mm/s thru a 0.3mm airbrush nozzle. The backpressure is insane. Quality is awesome. ![images/b73c41c76b6baa0876ef3ab23cc5867e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b73c41c76b6baa0876ef3ab23cc5867e.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 16:15*

He's currently printing the rework parts for the P3Steel (toolson rework, with some remixes). ![images/acbea940c6c1cba6e567e87aad9b7203.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/acbea940c6c1cba6e567e87aad9b7203.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 16:17*

The P3Steel had a Titan + v6 before, but I wasn't very happy with where I had to put the cooling fan and a bunch of other little things, so figured I'll keep going with the aeros. The quality was phenomenal from this head too though, for a while the P3Steel was my main/ best printer. ![images/774f3edfdfb619bc5d280435a802948e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/774f3edfdfb619bc5d280435a802948e.jpeg)


---
**Ben Delarre** *October 17, 2017 16:17*

Interesting. I've switched the Bowden on my eustathios to a similar length by mounting the stepper centrally above the bed. Worked pretty good but with PETG never been able to tune out the blobs and I've had several issues with the bowden popping out. 


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 16:19*

Are you using the collet clips? 

That print I showed on the DICE was petg. There's stringing alright, I may have to increase retraction even more, but it's really just a matter of fast travel moves so it can't leak much. 


---
**Ben Delarre** *October 17, 2017 16:21*

Yes I have the collet clip on the insert sides of the bowden...i.e. one on the in side of the stepper and one on the in side of the hotend. But I'm having the popouts on the out side of the bondtech extruder. Went out to the garage and found 5m of plastic spooled out over the floor!


---
**Ray Kholodovsky (Cohesion3D)** *October 17, 2017 16:24*

Well, it could be: 

A faulty fitting (is this possible)?

Too much motor current = too high an extrusion force. 

Friction in the tube. 

Hotend jamming. 


---
**Ben Delarre** *October 17, 2017 16:27*

Could be the fitting. I probably have another one knocking around somewhere I can try. Hadn't thought about the motor current, will try tweaking the smoothieboard settings.



It could potentially be the fact that the tube twists around a lot with this placement. There's a good groove worn into the tube where the fitting grips it.


---
**Hung Vo** *November 19, 2017 17:39*

**+Ben Delarre** 


---
**Hung Vo** *November 19, 2017 17:39*

**+Ben Delarre** 


---
**Hung Vo** *November 19, 2017 17:39*

**+Ben Delarre** 


---
**Hung Vo** *November 19, 2017 17:40*

**+Ben Delarre** 


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/S68iyDzFhWB) &mdash; content and formatting may not be reliable*
