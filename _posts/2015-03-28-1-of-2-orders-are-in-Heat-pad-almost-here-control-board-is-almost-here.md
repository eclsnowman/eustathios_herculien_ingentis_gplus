---
layout: post
title: "1 of 2 orders are in. Heat pad almost here, control board is almost here"
date: March 28, 2015 01:00
category: "Discussion"
author: Gus Montoya
---
1 of 2 orders are in. Heat pad almost here, control board is almost here. Now I need build plate, belts, and plastic extruded parts. I'm having a building party at my house with me, myself, and I attending. Much needed selfish me me me time. NICE!

![images/d8707aa92c702352303de076f79d6ac1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d8707aa92c702352303de076f79d6ac1.jpeg)



**Gus Montoya**

---
---
**Gus Montoya** *March 28, 2015 01:08*

I'm really feeling my nerd roots now LOL. Oh how I miss thee.


---
**Dat Chu** *March 28, 2015 01:45*

Sorry that I have not been able to start shipping you the heated pad. Life got in the way. Will start the unpacking and repacking process this weekend.


---
**Gus Montoya** *March 28, 2015 02:16*

No worries Dat. Life is life gotta take care of it.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/hm7vpw9c694) &mdash; content and formatting may not be reliable*
