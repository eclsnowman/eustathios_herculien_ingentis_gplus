---
layout: post
title: "I have been away for SOOO long, I just started working at a new company in Australia making 3d printers (3D industries)"
date: September 25, 2014 11:09
category: "Show and Tell"
author: Jarred Baines
---
I have been away for SOOO long, I just started working at a new company in Australia making 3d printers (3D industries). That, my new house and my ingentis / eustathios mash up "Techne" have taken up so much time I've simply not checked my G+ for months!



If you guys recall I was tossing up the idea of CNC machining some components. I then went on to have a lot of trouble printing anything on my existing printer and so I decided I'd redesign the machine to be almost completely CNC machined...



... Oh  also while I was cutting the frame uprights I had this crazy idea... I'll upload some pics rather than go on describing it ;-)



I am currently printing a 630mm talk 40mm screw for a test, left the printer at work so I'm hoping that it's going well! Video link:  
{% include youtubePlayer.html id=190cWV_YmTw %}
[http://youtu.be/190cWV_YmTw](http://youtu.be/190cWV_YmTw)



I cannot express enough gratitude to +Tim Rastall, +Jason Smith (who get a special mention for initial inspiration) and, sincerely, all the rest of you who helped turn this awesome project into what it is!



I would love to share my design files should anyone want to build a similar machine (in inventor format or, possibly other formats, I've never tried to export before!)







**Jarred Baines**

---
---
**Jarred Baines** *September 25, 2014 13:36*

Oh, to keep with the original naming convention, I even named it after a Greek goddess;



Techne - "goddess of art, technical skill and craft"


---
**Daniel Fielding** *September 26, 2014 00:22*

Are now in qld?


---
**Jarred Baines** *September 26, 2014 09:29*

Nope? Does it say I am somewhere? In Melbourne ;-) u in qld Daniel?


---
**Daniel Fielding** *September 28, 2014 22:48*

Yeah I am in qld. I figured you were too if you are working with 3d industries. 


---
**Jarred Baines** *October 02, 2014 08:12*

3d industries is in Melb? Who are you referring to in QLD? I'll move to the QLD branch quick-smart if there is one! ;-p


---
**Daniel Fielding** *October 08, 2014 09:17*

There must be 2 different companies.


---
*Imported from [Google+](https://plus.google.com/+JarredBaines/posts/cimbxXktmsd) &mdash; content and formatting may not be reliable*
