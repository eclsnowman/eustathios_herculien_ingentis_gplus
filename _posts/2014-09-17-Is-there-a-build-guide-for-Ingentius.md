---
layout: post
title: "Is there a build guide for Ingentius?"
date: September 17, 2014 16:58
category: "Discussion"
author: Caesar Samsi
---
Is there a build guide for Ingentius? I'm interested in building one.





**Caesar Samsi**

---
---
**Mike Miller** *September 17, 2014 17:10*

It's kind of a 'second printer' printer. If you've built one, it'll be pretty easy to do, if it's your first, I wouldn't recommend it. 


---
**Caesar Samsi** *September 17, 2014 17:35*

Thanks Mike!


---
**Erik Scott** *September 18, 2014 03:24*

I have an eustathios, but I'm probably going to be rebuilding much of it soon with all the new parts I've printed. I can probably take some pictures and put together a guide. 


---
*Imported from [Google+](https://plus.google.com/108101185146883738933/posts/6cusesX6MH3) &mdash; content and formatting may not be reliable*
