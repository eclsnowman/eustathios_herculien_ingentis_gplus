---
layout: post
title: "If my printer is unable to succesfully print off a set of Herculien Parts in ABS Whom here could print me some Herculien parts in ABS?"
date: October 24, 2015 01:53
category: "Discussion"
author: Jim Stone
---
If my printer is unable to succesfully print off a set of Herculien Parts in ABS



Whom here could print me some Herculien parts in ABS? Color doesnt really matter.





**Jim Stone**

---
---
**Zane Baird** *October 24, 2015 02:05*

What parts do you need?


---
**Jim Stone** *October 24, 2015 02:31*

If my printer keeps up being unreliable....all of them for the herculien


---
**Zane Baird** *October 24, 2015 02:35*

I can print the mechanical necessities to get you up an running, just waiting on a couple PC panels so I can fully enclose my printer for high quality

ABS prints


---
**Jim Stone** *October 24, 2015 02:38*

that would be most appreciated. then i can forcefeed/ babysit my unreliable POS for the not so much needed bits.



all my parts are arriving mid nov - early dec im gettin an early xmas :') hehe



just left to source are small things. like hobs etc. oh and the 24v psu. ( not sure if one on ebay is legit or good)



[http://www.ebay.ca/itm/1-Switching-Power-Supply-SE-450-24-24V-19A-450W-AC85-264Vin-225x124x50-Mean-Well/131221595780?_trksid=p2050601.c100085.m2372&_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D20140211132617%26meid%3D2f901ae0edf140d396824f5e053894c7%26pid%3D100085%26rk%3D3%26rkt%3D4%26sd%3D370888481015%26clkid%3D683245070653082922&_qi=RTM2067269](http://www.ebay.ca/itm/1-Switching-Power-Supply-SE-450-24-24V-19A-450W-AC85-264Vin-225x124x50-Mean-Well/131221595780?_trksid=p2050601.c100085.m2372&_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D20140211132617%26meid%3D2f901ae0edf140d396824f5e053894c7%26pid%3D100085%26rk%3D3%26rkt%3D4%26sd%3D370888481015%26clkid%3D683245070653082922&_qi=RTM2067269)



[http://www.ebay.ca/itm/Mean-Well-MW-24V-18-8A-450W-AC-DC-Switching-Power-Supply-SE-450-24-UL-CUL-PSU/370888481015?_trksid=p2050601.c100085.m2372&_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D20140211132617%26meid%3D2f901ae0edf140d396824f5e053894c7%26pid%3D100085%26rk%3D1%26rkt%3D4%26sd%3D370888481015%26clkid%3D683250862226277460&_qi=RTM2067267](http://www.ebay.ca/itm/Mean-Well-MW-24V-18-8A-450W-AC-DC-Switching-Power-Supply-SE-450-24-UL-CUL-PSU/370888481015?_trksid=p2050601.c100085.m2372&_trkparms=aid%3D111001%26algo%3DREC.SEED%26ao%3D1%26asc%3D20140211132617%26meid%3D2f901ae0edf140d396824f5e053894c7%26pid%3D100085%26rk%3D1%26rkt%3D4%26sd%3D370888481015%26clkid%3D683250862226277460&_qi=RTM2067267)


---
**Zane Baird** *October 24, 2015 02:48*

It should be fine. Just don't skimp on the the extruder. That ultimately dictates the quality at which you can print.


---
**Eric Lien** *October 24, 2015 04:28*

**+Jim Stone** also that PSU is overkill. I really should update the BOM. Now that the bed runs on an SSR you are only driving steppers and the hot end from the PSU.


---
**Eric Lien** *October 24, 2015 04:29*

But Meanwell really makes great PSU's. 


---
**Jim Stone** *October 24, 2015 05:05*

hahaha you dont know overkill XD im currently running my printer from 12v 85a server PSU hehehehehe


---
**Jim Stone** *October 24, 2015 05:06*

so serious question. what psu would be fine? theres one in my area. it was just a lower wattage.


---
**Eric Lien** *October 24, 2015 06:55*

I recently used this one on the delta. Not a meanwell, but good quality and seems to work just fine:



Genssi 24V 14.5A 350W Regulated Switching Power Supply [https://www.amazon.com/dp/B005CLDOW8/ref=cm_sw_r_other_awd_YRYkwbHTBSMWJ](https://www.amazon.com/dp/B005CLDOW8/ref=cm_sw_r_other_awd_YRYkwbHTBSMWJ)


---
**Eric Lien** *October 24, 2015 06:56*

But check the panel cutouts and mounts for the enclosure if you change PSU's


---
**Jim Stone** *October 25, 2015 16:31*

would the 350w meanwell of the same range work then? usually when i build a pc i usually go overkill on the PSU as i cant figure out how much i really do need


---
**Eric Lien** *October 25, 2015 16:36*

**+Jim Stone** should be plenty. I think even 300W would do it fine. But there are plenty of nice supplies in the 350W range to choose from.


---
**Jim Stone** *October 26, 2015 01:27*

yup. my current printer just let out the blue smoke. friend was near and spilled their tea into it :(



so looks like i will need some halp on ALL the pieces. can comp for plastic used and shipping


---
**Zane Baird** *October 26, 2015 14:59*

**+Jim Stone** I should be able to start printing them later this week. I will likely print the pieces in a combination of white and silver ABS


---
**Jim Stone** *October 26, 2015 15:42*

Thank you so much


---
**Jim Stone** *October 27, 2015 02:31*

**+Zane Baird** what are you wanting for them btw. plus my cost of shipping of course


---
**Zane Baird** *October 27, 2015 15:05*

**+Jim Stone** If you want I will print them at no cost (other than shipping) if you buy a roll of ABS in the color you want them printed and have it shipped to me. Send me a message and we can discuss the details.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/PfUHBg7Bm6N) &mdash; content and formatting may not be reliable*
