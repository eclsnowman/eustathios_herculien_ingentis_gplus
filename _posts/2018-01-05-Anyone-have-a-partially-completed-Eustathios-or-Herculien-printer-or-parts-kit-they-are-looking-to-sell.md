---
layout: post
title: "Anyone have a partially completed Eustathios or Herculien printer (or parts kit) they are looking to sell?"
date: January 05, 2018 20:22
category: "Discussion"
author: jerryflyguy
---
Anyone have a partially completed Eustathios or Herculien printer (or parts kit) they are looking to sell? I’m thinking about building a second printer.. figured I’d ask here first before going out to buy components. 





**jerryflyguy**

---
---
**Peter Kikta** *January 11, 2018 18:34*

Hi, I would sell mine, but I'm from Slovakia, so the shipping cost make it probably overpriced.


---
**jerryflyguy** *January 11, 2018 21:11*

**+Peter Kikta** you’re probably right, thanks though!


---
**Gus Montoya** *January 13, 2018 22:40*

I only have the plastic parts printed or half printed


---
**jerryflyguy** *January 14, 2018 23:44*

**+Gus Montoya** thanks. I’ve got an Eustathios v2 so am able to print parts. I’m looking to build an ‘experimental ‘ printer to see if I can adapt the design for what I want. Thanks though!


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/YfxBz1VBrTc) &mdash; content and formatting may not be reliable*
