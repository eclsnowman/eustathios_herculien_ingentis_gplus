---
layout: post
title: "Does a parts cooling fan make a difference;"
date: March 02, 2018 17:15
category: "Show and Tell"
author: Bruce Lunde
---
Does a parts cooling fan make a difference; YES!



Before:                              After:

 



![images/1d3b7cbdb82299572a3d0efe114e345e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1d3b7cbdb82299572a3d0efe114e345e.jpeg)
![images/85eea89b8035c6b7cd477f8bfd010642.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/85eea89b8035c6b7cd477f8bfd010642.jpeg)

**Bruce Lunde**

---
---
**Eric Lien** *March 03, 2018 04:38*

Congratulations Bruce. I am so happy to see you getting great results. It has been a long road to here, but hopefully a worthwhile one. Can't wait to see even more progression in the future. Keep it up.


---
**Bruce Lunde** *March 03, 2018 17:44*

**+Eric Lien** It is great to be doing decent. I think I was really having more problems with slicing that the printer, but I just did not realize it. Now a bit more tuning and fixing a few self-induced problems and I will be really happy. I will be  bringing it to the MRRF and you can make more suggestions!


---
**Eric Lien** *March 04, 2018 03:55*

**+Bruce Lunde** that is great to hear.

 I hope there are a few people from the group making it out to MRRF. It will be great to talk shop, and troubleshoot together.


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/KGQL9Scpv7N) &mdash; content and formatting may not be reliable*
