---
layout: post
title: "My Eustathios Spider V2 is stopping mid print"
date: November 14, 2018 03:34
category: "Discussion"
author: Brandon Cramer
---
My Eustathios Spider V2 is stopping mid print. It gets so far and just stops printing. As of right now it will not complete a full print. I’m using the X5 Mini Wi-Fi and today I swapped out the SD card for a newer one. I’m using the ESP3D web GUI for the X5 Mini Wi-Fi, where I upload the sliced Simplify3D files and then start the print. 



After the print has failed the bed and extruder actually turn themselves off. To get it working again I have to power cycle the printer. 





**Brandon Cramer**

---
---
**Jeff DeMaagd** *November 14, 2018 13:19*

There might be a corruption in the file, Can you confirm that the file size was correct? How big is the g code file?


---
**Brandon Cramer** *November 14, 2018 16:04*

This turned out to be a weird Simplify3D problem. I started printing with the extruder temp at 250 and then at layer four lowered it to 245 degrees. I tried multiple files when I was having this issue. After deleting the temp change I was able to finish a print. 



 I don't really see anything in the gcode that would cause this: 



[dropbox.com - base.gcode](https://www.dropbox.com/s/27qom04kelo4ea5/base.gcode?dl=0)






---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/aka6c7FcfWY) &mdash; content and formatting may not be reliable*
