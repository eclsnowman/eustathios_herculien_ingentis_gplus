---
layout: post
title: "Fine detail is much easier on the Eustathios than it was with my corexy"
date: June 13, 2014 22:27
category: "Show and Tell"
author: Eric Lien
---
Fine detail is much easier on the Eustathios than it was with my corexy. Man I love this printer. Printed in PLA at 190c with 0.4mm E3D v6. Left to right:



0.2 layer height

0.16 layer height

0.12 layer height

US dime for scale.﻿

![images/df41e2db5c4d92bbbb46c995557af6b8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/df41e2db5c4d92bbbb46c995557af6b8.jpeg)



**Eric Lien**

---
---
**Tim Rastall** *June 13, 2014 22:39*

Did you get one of the new 0.25 nozzles? Should improve your XY  resolution. Allthough with an airtripper,  your ability to reliably extrude very low flow rates will be limited by the low steps/mm of the drive. 


---
**Eric Lien** *June 13, 2014 22:52*

**+Tim Rastall** not yet. To be honest my resolution on the corexy was more limited by the mechanics than the nozzle. I am still toying with making a carriage where I can snap on a nema 11 gear reduced stepper when times arise where it is useful. But run Bowden most of the time for speed. Haven't figured out a good configuration yet.


---
**Riley Porter (ril3y)** *June 14, 2014 00:30*

Looking good **+Eric Lien** 


---
**Jason Smith (Birds Of Paradise FPV)** *June 14, 2014 00:39*

That looks great!


---
**Pieter Swart** *June 14, 2014 09:40*

Nice prints! How does the v6 compare to the v5?


---
**Eric Lien** *June 14, 2014 13:51*

**+Pieter Swart** I like how compact it is. No jams so far. But cold pulls for a filament change seem harder than v5.


---
**Pieter Swart** *June 15, 2014 09:26*

Great! I also bought 2 of these and was a bit worried that I might have made a mistake. I've only used Jheads so far and they were relatively easy to use. Are your 8mm shafts stainless steel, or  bearing steel? I see that the price almost doubles if you pick  stainless steel on the misumi website, and I'm not sure if it's actually necessary. 


---
**Jason Smith (Birds Of Paradise FPV)** *June 15, 2014 11:12*

**+Pieter Swart**, I use Misumi bearing steel, and they're plenty strong. 


---
**Riley Porter (ril3y)** *June 15, 2014 15:35*

One thing I did not realize until **+Jason Smith** point it out to me is... That is a DIME not a quarter.  Who that is super small!  


---
**Eric Lien** *June 15, 2014 17:31*

**+Riley Porter** yup. I can only imagine what a 0.25 nozzle could do.


---
**Pieter Swart** *June 16, 2014 08:57*

Thanks Jason


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/5EgoZQbsULu) &mdash; content and formatting may not be reliable*
