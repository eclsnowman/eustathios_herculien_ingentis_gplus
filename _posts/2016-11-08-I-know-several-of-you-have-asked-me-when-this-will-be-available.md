---
layout: post
title: "I know several of you have asked me when this will be available..."
date: November 08, 2016 00:20
category: "Discussion"
author: Eric Lien
---
I know several of you have asked me when this will be available... Looks like Roy is taking the preorders now for delivery in December.



<b>Originally shared by Roy Cortes</b>



Azteeg X5 GT is available for pre-order. For those waiting for this, I did a small batch and should here early December. Some new drivers and add-on boards will be ready by that time as well.



[http://www.panucatt.com/azteeg_X5_GT_reprap_3d_printer_controller_p/ax5gt.htm](http://www.panucatt.com/azteeg_X5_GT_reprap_3d_printer_controller_p/ax5gt.htm)













**Eric Lien**

---
---
**Jeremy Marvin** *November 08, 2016 01:03*

Nice!


---
**Øystein Krog** *November 08, 2016 09:07*

I did not know about the new bigfoot format, love it!


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/ZL6fLDPb3nq) &mdash; content and formatting may not be reliable*
