---
layout: post
title: "So I've been on and off with my Herculien build and I finally installed the bed to notice the hotends not get near the bed"
date: November 18, 2015 16:45
category: "Discussion"
author: Gunnar Meyers
---
So I've been on and off with my Herculien build and I finally installed the bed to notice the hotends not get near the bed. Did I mount the bed wrong or did I somehow screwup the gantry?  Thank for the help. 



![images/65a8b05b7eb9755f4f6f7490d2cebd81.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/65a8b05b7eb9755f4f6f7490d2cebd81.jpeg)
![images/97239f2c69da9b8e16e8b80212f8aa88.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/97239f2c69da9b8e16e8b80212f8aa88.jpeg)
![images/0b4aadaefdc182e5494f44be4e2ea256.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0b4aadaefdc182e5494f44be4e2ea256.jpeg)
![images/b3419801ba2af2b916d8b5b1d1dbe63a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b3419801ba2af2b916d8b5b1d1dbe63a.jpeg)

**Gunnar Meyers**

---
---
**Eric Lien** *November 18, 2015 17:29*

Looks like you may have used taller angle aluminum which pushes the bed down further (see image: [http://imgur.com/F6XzS03](http://imgur.com/F6XzS03)).



Also the assembly uses an MDF insulator board below the bed and springs below that. Do you have those installed? If not you may have to provide spacers to get the bed up to the correct height.


---
**Gunnar Meyers** *November 18, 2015 18:37*

The springs are installed under the Mdf so it must be that the aluminum needs to be cut down. 


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/GKbzPHQyav1) &mdash; content and formatting may not be reliable*
