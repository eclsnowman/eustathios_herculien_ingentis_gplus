---
layout: post
title: "Shared on March 08, 2014 06:41...\n"
date: March 08, 2014 06:41
category: "Show and Tell"
author: D Rob
---


![images/b87838f7aef278b4a8250830739abb32.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b87838f7aef278b4a8250830739abb32.gif)



**D Rob**

---
---
**Dale Dunn** *March 08, 2014 12:49*

At the very least, it's an interesting mechanism. 


---
**Eric Lien** *March 08, 2014 14:16*

I have a 5:1 nema 11 gear reduced stepper I have used for the extruder. They go for like $30. If the normal nema on the Z belt system was replaced on z with the 5:1 I am pretty certain the z could never free fall. Plus you would get some pretty sweet reduction to help z resolution.


---
**Mike Miller** *March 08, 2014 15:29*

Is that IKEA? I believe all their drawers have a similar dampened close behavior.


---
**Mike Miller** *March 08, 2014 15:32*

nvm, I saw the other video, if the slides were reasonably precise, it's a definite possibility 


---
**D Rob** *March 09, 2014 06:46*

**+Mike Miller** no it's Ashley furniture


---
**D Rob** *March 09, 2014 06:52*

I was thinking about the thing on a mount to work with rods. It has a magnet that grabs another magnet on the slide. When it gets to a trip point a plastic tab flips stopping the cylinder from going all the way back. The magnet releases the slide and when it comes back to close the lever is tripped the opposite way. the bleeder valve regulates air exiting to slow it to a stop. The cylinder contains a screw like bleeder valve and a single direction valve for rapid air intake but prevents rapid air loss. Most likely a diaphragm.﻿


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/HeAsC7ExakQ) &mdash; content and formatting may not be reliable*
