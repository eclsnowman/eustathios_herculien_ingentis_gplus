---
layout: post
title: "I purchased Simplify3D to use on my Eustathios Spider V2.."
date: October 02, 2015 16:32
category: "Discussion"
author: Brandon Cramer
---
I purchased Simplify3D to use on my Eustathios Spider V2.. When I try to print something it scraps off the 3D-EEZ film with the hot end. I'm not sure why this is happening. 



There are definitely a lot of settings in Simplify3D. A little overwhelming at the moment. :)





**Brandon Cramer**

---
---
**Blake Dunham** *October 02, 2015 16:34*

Make sure your first layer height percentage is at 100%. It should be in the layer tab in print configuration. Ideally you would want the first layer to be thinner so the print adheres to the bed better but I havnt had a problem with having it at 100%


---
**Brandon Cramer** *October 02, 2015 18:10*

Wow! This is a mess... It doesn't seem to be pushing enough filament. Not sure where to check for this. 


---
**Bud Hammerton** *October 02, 2015 20:14*

**+Blake Dunham** This to me seems like the old way to adhere. If you seek dimensional accuracy your z_offset should be set to make a 0.2 mm first layer 0.2 mm, then your first layer in S3D should be set to 100% like you first mentioned. If you need additional adhesion then, IMO a better way is to adjust First Layer Width, also in the Layer tab. It really is an extrusion multiplier for the first layer of your print and will facilitate adhesion if you increase this number.



**+Brandon Cramer** I started out with Slic3r, and there is a heck of a lot more setting to mess up in that software than in S3D. But it is likely Z_offset if you are using a Marlin based board.


---
**Chris Brent** *October 02, 2015 20:18*

You mean there's not enough filament coming out and it doesn't stick? Check the Extruder Tab under "Edit Process Settings" -> "Show Advanced" -> Extruder and make sure the nozzle diameter and extrusion width are correct. Maybe start with the extrusion multiplier at 1. Once you get it printing you can tweek the extrusion multiplier so that a single walled object is the correct thickness.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/D9u7SdFDSjM) &mdash; content and formatting may not be reliable*
