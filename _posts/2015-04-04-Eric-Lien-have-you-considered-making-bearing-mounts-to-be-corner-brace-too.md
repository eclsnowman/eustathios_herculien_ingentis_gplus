---
layout: post
title: "Eric Lien have you considered making bearing mounts to be corner brace too?"
date: April 04, 2015 23:25
category: "Discussion"
author: Mykyta Yurtyn
---
**+Eric Lien** have you considered making bearing mounts to be corner brace too? are there any cons to this approach?

![images/b4cc33a96f2b799b86695c0f81ff267a.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b4cc33a96f2b799b86695c0f81ff267a.png)



**Mykyta Yurtyn**

---
---
**Eric Lien** *April 04, 2015 23:46*

A good idea... But as modeled there is material in the way of the belt path. 


---
**Isaac Arciaga** *April 05, 2015 02:13*

I thought about that as well but found you would not be able to reposition the block to make any alignment adjustments using Eric's alignment tools (correct me if im wrong). A good alternative to make the block more rigid is to go with a non-printed part all together but would require slightly longer 10mm shafts.  [http://tinyurl.com/k54ylgb](http://tinyurl.com/k54ylgb) or here [http://www.ebay.com/itm/121520504594](http://www.ebay.com/itm/121520504594)


---
**Mykyta Yurtyn** *April 05, 2015 03:30*

**+Eric Lien** doh. thx for pointing it out. I guess I need to finish building it as is before trying to "improve". for now I'm just killing time while parts are printing.



Now as I'm thinking more about it, as **+Isaac Arciaga**  pointed out, alignment will not be possible with this part - it will have to follow the frame. So frame has to be super straight, which might be harder to achieve


---
*Imported from [Google+](https://plus.google.com/103799104844197134662/posts/4FqqMPV36J8) &mdash; content and formatting may not be reliable*
