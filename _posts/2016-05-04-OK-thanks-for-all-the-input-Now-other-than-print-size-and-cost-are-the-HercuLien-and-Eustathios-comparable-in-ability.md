---
layout: post
title: "OK, thanks for all the input. Now other than print size and cost are the HercuLien and Eustathios comparable in ability?"
date: May 04, 2016 15:01
category: "Discussion"
author: Robert Clayton
---
OK, thanks for all the input. Now other than print size and cost are the HercuLien and Eustathios comparable in ability? Even **+Eric Lien**​ has commented on using his Talos3D Tria Delta a lot recently. From your experience, considering ability more important than cost, if you all were beginning again what would you build first? Then comes the chicken or the egg conundrum having printed plastic components to build a machine to print plastic components.





**Robert Clayton**

---
---
**Eric Lien** *May 04, 2016 15:30*

Ability is a open question unfortunately. It depends on what ability you want. For ability to go fast I would go with a Delta hands down so long as you didn't need  large parts in X/Y. For larger parts that you also want to be able to print fast I would recommend the Eustathios or HercuLien (HercuLien enclosed f you're doing large abs parts). For simplicity of construction and calibration and the ability to do flexible filament using a direct drive extruder I would do something like the Spartan and scale it up to the size you need. A moving Y bed printer would be the slowest of the bunch due to bed mass and direct extruder mass... but not by a lot in the scheme of things. All 3D printing is slow in a relative sense and all printers are still limited by the thermodynamics of filament cooling anyway to achieve good surface finish.


---
**Derek Schuetz** *May 04, 2016 16:02*

Having built a Herc and Euth I would go with a Euth once the machine is tuned it prints excellent. And it's print bed is still very large and the build is significantly easier than the Herc 


---
**Robert Clayton** *May 04, 2016 23:52*

**+Eric Lien**​ As an initial build I lean toward the Eustathios but the problem of sourcing the original printed parts makes me assume the Spartan a better beginning as I think there is a kit for the unique parts. Do you know of anyone selling the printed parts for the Eustathios?


---
**John Barron II** *May 06, 2016 00:53*

I can't seem to locate the Spartan printer you speak of **+Eric Lien**​. I really like your design, but it's a bit large for my current needs. I have two Deltas at the moment, but I need something smaller, 100-150mm xy with a heated bed and all metal hot end. My Deltas are great but it seems like overkill to fire then up for small prints


---
**Eric Lien** *May 06, 2016 03:40*

**+John Barron II** 



[https://github.com/alex7575/Talos3D/tree/master/Spartan](https://github.com/alex7575/Talos3D/tree/master/Spartan)



[https://plus.google.com/u/0/communities/106411395795447750286](https://plus.google.com/u/0/communities/106411395795447750286)



[https://plus.google.com/u/0/+EricLiensMind/posts/4cYjjETKoum](https://plus.google.com/u/0/+EricLiensMind/posts/4cYjjETKoum)


---
**Jeremy Marvin** *May 10, 2016 16:33*

**+Eric Lien** How have the top mounted Z motors worked for you?  I have an old Ecksbot (2nd gen Reprap) that I really struggled with.  I'm slowly bringing it out of retirement.


---
**Eric Lien** *May 10, 2016 18:18*

No problem here. There is no couplers which helps. But don't expect super fast Z for z-hop like on a Delta :)



I have never seen Z ribbing ever on the Talos3D Spartan. It has essentially been 100% maintenance free. Just load the gcode, heat up, start printing, and walk away.


---
*Imported from [Google+](https://plus.google.com/103318213942990361542/posts/iC28HAY9GRu) &mdash; content and formatting may not be reliable*
