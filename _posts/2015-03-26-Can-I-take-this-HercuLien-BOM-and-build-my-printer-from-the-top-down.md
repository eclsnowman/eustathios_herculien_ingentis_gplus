---
layout: post
title: "Can I take this HercuLien BOM and build my printer from the top down?"
date: March 26, 2015 16:12
category: "Discussion"
author: Brandon Cramer
---
Can I take this HercuLien BOM and build my printer from the top down? It does seem to be setup that way. 





**Brandon Cramer**

---
---
**Bruce Lunde** *March 26, 2015 19:41*

I have been, although it has cost me extra in freight as I have ordered bits at a time. Please pay good attention to the summary BOM page, as I used main tab and missed some quantities that were clearly labeled (sheepish grin) and now have had to place additional orders.


---
**Eric Lien** *March 26, 2015 19:50*

The layout of the BOM is driven by solidworks. It has the number of subassemblies needed, then the quantities per subassembly. So it can be a little confusing. I still need to cleanup the BOM layout a bit.


---
**Mykyta Yurtyn** *March 27, 2015 00:41*

I'm going through BOM and ordering parts too. So far I had trouble finding the 10MM ID radial bearings (ended up ebaying lot of 20).Ordered various parts from openbuilds, smw3d, lulzbot and robotdigg. will keep you posted. 

2nd **+Bruce Lunde**  Summary page is very helpful

btw, **+Eric Lien**  if you are working on BOM you might want to fix vendor for that 10mm ID (5972K164)

bearing since currently it is in 2 rows on the summary page


---
**Seth Messer** *March 28, 2015 02:12*

**+Eric Lien** **+Mykyta Yurtyn** yeah, that 5972K164 is one of the few items on the Eustathios V2 BOM that I'm having troubles finding on Robotdigg. I'll check other places and make the changes necessary on the BOM and submit for review.


---
**Mykyta Yurtyn** *March 28, 2015 13:45*

**+Seth Messer** I ordered some here: [http://www.ebay.com/itm/SET-OF-20-6900-Double-Shield-Bearings-10mm-x-22mm-x-6mm-/371277926081?pt=LH_DefaultDomain_0&hash=item5671e38ec1](http://www.ebay.com/itm/SET-OF-20-6900-Double-Shield-Bearings-10mm-x-22mm-x-6mm-/371277926081?pt=LH_DefaultDomain_0&hash=item5671e38ec1)

Amazon link on the original Euth BOM also has them [http://www.amazon.com/gp/product/B00I0X2TGM/ref=oh_details_o00_s00_i00?ie=UTF8&psc=1](http://www.amazon.com/gp/product/B00I0X2TGM/ref=oh_details_o00_s00_i00?ie=UTF8&psc=1)


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/3AGLNEcZK18) &mdash; content and formatting may not be reliable*
