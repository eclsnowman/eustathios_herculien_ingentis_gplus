---
layout: post
title: "It moves! Jerkyness as I was using jog controls"
date: March 02, 2015 10:02
category: "Deviations from Norm"
author: Tim Rastall
---
It moves! Jerkyness as I was using jog controls.  Noisy because I've not epoxied the v shaped extrusions on yet (so they vibrate) 


**Video content missing for image https://lh3.googleusercontent.com/-BuRheSrIDQQ/VPQ1Nv240CI/AAAAAAAAWjI/sPlaSznB0T0/s0/VID_20150302_210410.mp4.gif**
![images/a1a9c9404002e5b2e5c8451de168681f.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a1a9c9404002e5b2e5c8451de168681f.gif)



**Tim Rastall**

---
---
**Tim Rastall** *March 02, 2015 10:33*

**+Oliver Schönrock** no planetary gearbox yet.  Just a normal nema 17.


---
**Tim Rastall** *March 02, 2015 17:54*

**+Oliver Schönrock** yes,  they are adjustable can slide back on forth along the T-Slot. They are a horrible kludge in fact.  Totally mangled them with the hacksaw! 


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/CigBbsV7A92) &mdash; content and formatting may not be reliable*
