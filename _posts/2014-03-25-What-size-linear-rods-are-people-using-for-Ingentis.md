---
layout: post
title: "What size linear rods are people using for Ingentis?"
date: March 25, 2014 12:10
category: "Discussion"
author: Lynn Roth
---
What size linear rods are people using for Ingentis?  The BOM shows a combination of 10mm and 8mm.  Is that still the suggested types?





**Lynn Roth**

---
---
**Mike Miller** *March 25, 2014 12:21*

I'm going through that as we speak. 8mmX2 for extruder carriage, 10mmX4 outside of carriage, 8mm driving Z axis and as of now, 12mmX2 for Z-build plate. 


---
**D Rob** *March 26, 2014 06:48*

I am using 10mm all around on my Ul-T-Slot (I got some in bulk via aliexpress)


---
**Jarred Baines** *March 26, 2014 07:24*

That's not a bad idea actually, keeping them all the same size makes for same size hardware to suit (bushes, pulleys etc)


---
**Lynn Roth** *March 26, 2014 11:12*

Yeah, I kind of like the idea of using 1 size.  What kind of rod are people using? Has anyone used anything from Misumi?


---
**Mike Miller** *March 26, 2014 11:55*

As stated earlier, one of the hard parts in designing these hobbiest-grade things is knowing where and how much you can cut corners. If you go to the 'professional grade' chrome coated and hardened high-precision parts on Misumi, the parts specifically designed to this purpose, you'll be spending $150 on just linear rails. If you go through ebay, You might pay $50 for all of them, and then you'll be looking at the youtube video on how to bend them straight. (I can't find the link...but I'm sure you can.)



Rods and bushings could cost you $500, or they can cost you $30...whether $30 worth of dillrod and Chinese third tier bronze bushings will work or not...is anybody's guess. 



Then again, you could use 'mineature linear guides' which are the 'right tool for the job'

 and will ensure that part of your printer isn't suspect...but you'll be paying $40 per, and need 6 of 'em (about $260)



I'll be going McMaster or Misumi for the rods, and ebay (and crossed fingers) for oillite bushings.



(to say nothing for when these things happen to coincide with other, more-common uses. If your 12mm bushing is used in Every Ford Made, you might end up paying $15 for 60....or $300 for 4)


---
**William Frick** *March 27, 2014 16:50*

**+Mike Miller** Or when we can get our hands on that igus filament .... print our own !


---
**Mike Miller** *March 27, 2014 18:03*

No doubt. 


---
**Jarred Baines** *March 28, 2014 05:01*

POM sounds good for now... We call it acetal at work, or delrin (it has many names) - makes for good bushes, don't know how it would compare the the igus stuff, but at least it's not patented ;-)



I'm itching to try it (making a trial all-metal hot end as we speak :-)


---
**Mike Miller** *March 28, 2014 17:16*

I've got some scrap Acetal in the garage, I may just order the rods and delay ordering the bushings long enough to make my own for testing. It's self lubricating, but I wonder if a little silicone, PTFE grease, or Lithium Grease wouldn't help. 


---
**Jarred Baines** *March 29, 2014 04:32*

You would find acetal bushes off-the-shelf SOMEWHERE, surely. So perhaps you could ask / get details from an actual bush supplier as to what lubrication to use ;-)


---
**Mike Miller** *March 29, 2014 04:36*

Now what's the fun in that? ;)


---
*Imported from [Google+](https://plus.google.com/+LynnRoth/posts/KvbYuhfbUhq) &mdash; content and formatting may not be reliable*
