---
layout: post
title: "I am having trouble figuring how to get the 3D printed parts to create my first 3D printer without spending a ton of money"
date: February 19, 2015 02:21
category: "Discussion"
author: Eric Bessette
---
I am having trouble figuring how to get the 3D printed parts to create my first 3D printer without spending a ton of money.  I saw in another comment here something about the "Print It Forward Program".  Is that a real thing?  If so, how would I go about requesting an invite?





**Eric Bessette**

---
---
**Daniel Salinas** *February 20, 2015 00:19*

what are you building Eric?


---
**Eric Bessette** *February 20, 2015 00:30*

An Eustathios.  I'm currently trying out different local 3DHub locations.  All of the commercial options seem like they would be over a $1000 to print all of the parts.


---
**Daniel Salinas** *February 20, 2015 00:54*

I can offer you the same deal I gave **+Derek Schuetz**​  send me 2 spools of whatever color hatchbox abs and cover freight and I'll print them for you.  You'll get back all unused abs.  Its the best I can do ATM.


---
**Daniel Salinas** *February 20, 2015 01:00*

Just to be clear with you though, I've never printed a part for the Eustathios but I have printed half an ingentis set so I think I already have a good idea for part orientation and what not.


---
**Daniel Salinas** *February 20, 2015 01:00*

I'm currently building and documenting a Herculien so I'm happy to let the printer churn out parts while I'm busy doing other things.


---
**Eric Bessette** *February 21, 2015 05:59*

Awesome, that's an amazing offer!  I'll definitely take you up on it.


---
*Imported from [Google+](https://plus.google.com/106288190144199995561/posts/9bdQte7XXKJ) &mdash; content and formatting may not be reliable*
