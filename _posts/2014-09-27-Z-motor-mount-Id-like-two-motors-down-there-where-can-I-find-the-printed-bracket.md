---
layout: post
title: "Z-motor mount: I'd like two motors down there, where can I find the printed bracket?"
date: September 27, 2014 02:38
category: "Discussion"
author: Mike Miller
---
Z-motor mount: I'd like two motors down there, where can I find the printed bracket? Its not on the youmagine site, and the one I have was printed by someone else. 





**Mike Miller**

---
---
**Eric Lien** *September 27, 2014 02:52*

Which variant?﻿ picture would help.


---
**Brian Bland** *September 27, 2014 03:48*

I have it.   Let me find it and post it 


---
**Mike Miller** *September 27, 2014 03:50*

This one: [https://drive.google.com/a/millertwinracing.com/file/d/0B09sUvvteXCBWVB6aDZCQnMzcFk/edit?usp=sharing](https://drive.google.com/a/millertwinracing.com/file/d/0B09sUvvteXCBWVB6aDZCQnMzcFk/edit?usp=sharing)


---
**Brian Bland** *September 27, 2014 04:10*

[https://drive.google.com/file/d/0B8slcmnIxOMKVVItWEVlb3ZkYmc/edit?usp=sharing](https://drive.google.com/file/d/0B8slcmnIxOMKVVItWEVlb3ZkYmc/edit?usp=sharing)


---
**Brian Bland** *September 27, 2014 04:10*

That should be it.


---
**Mike Miller** *September 27, 2014 05:30*

Thanks! I got one motor wired up, but there's a whole lot of deflection in the aluminum shaft and the driver is working a whole lot harder than is like. (.8 v wouldn't stand a chance at lifting it.)


---
**Mike Miller** *September 29, 2014 03:45*

Thanks Brian. Printed, and nearly ready for service. (Have to relocate the PSU)


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/GYYhgmidTQQ) &mdash; content and formatting may not be reliable*
