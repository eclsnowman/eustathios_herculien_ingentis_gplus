---
layout: post
title: "Finally got some work done for my modified Spider V2 build"
date: June 03, 2015 09:15
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
Finally got some work done for my modified Spider V2 build. Thanks to **+Eric Lien** for this wonderful printer and the near-to-perfect github repo with all the stuff. I had some extrusion lying around so i did some cutting and sanding with a friend yesterday and we managed to finish the frame. Instead of drilling holes we used some great connectors that i also had from other projects. We added some additional corner brackets - geometry is perfect as far as we could measure.



At the end this will be partly a **+Walter Hsiao** modded version with ballscrews. Against Walters strategy i ordered them with 8mm end milling so they should fit in the bearings like the original planned ones. The only mod will then be his bed support ([http://www.thingiverse.com/thing:854267](http://www.thingiverse.com/thing:854267)) for the ballscrew nuts. I also ended up getting the chinese friends to turn the ballnut in the right direction for me - that hopefully saves me from printing Walters inverter ([http://www.thingiverse.com/thing:809141](http://www.thingiverse.com/thing:809141)) :-)



I'm still unsure which material to use for the printed parts and what electronic options i choose. I still have a smoothieboard from the kickstarter lying around but RUMBA or one of the newer DUE-Boards (like RADDS) would also be an option. I ordered at Robotdigg today and added some 0,9° motors - therefore a fast electronic with 1/128 microstepping would be deluxe of course.



Will see... and keep you posted.

![images/8843d16b47a17afc7eb8dd3b59c65fc7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8843d16b47a17afc7eb8dd3b59c65fc7.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**Gus Montoya** *June 03, 2015 09:20*

nice!


---
**Eric Lien** *June 03, 2015 12:24*

Looking good :)


---
**Eric Lien** *June 03, 2015 13:08*

How are the upper cross extrusions held in place?


---
**Frank “Helmi” Helmschrott** *June 03, 2015 13:14*

**+Eric Lien** all connections are primarily done via these connectors [https://www.evernote.com/shard/s1/sh/64a88902-5268-4408-bc88-0d36a2191f35/7573d9f39a6f47d0/res/667baaa4-be30-449a-8ae7-7bd0d3a6773d/skitch.png](https://www.evernote.com/shard/s1/sh/64a88902-5268-4408-bc88-0d36a2191f35/7573d9f39a6f47d0/res/667baaa4-be30-449a-8ae7-7bd0d3a6773d/skitch.png) - i love them as they don't need any drilling/cutting on the extrusion. They're inserted with a self cutting thread on themselves (straight into the slots from the side) and are connected with the slot nut. The corner brackets are just there for some more alignment but at the end you most likely wouldn't ever need them.


---
**Frank “Helmi” Helmschrott** *June 03, 2015 13:17*

**+Eric LeFort** **+Eric Lien** i buy them at [http://motedis.com](http://motedis.com) which is my extrusion supplier for most of of my needs (located in germany as i am). I know that there are others offering them but you don't get 'em everywhere. 



One of the most important facts about these connectors is that you can easily adjust them later - just undo the screw a bit and you can move the slot not to wherever you want. The only downside is that it blocks the slot a bit. That's why we turned it upside on the two crossings that hold the upper end of the z-bars so we can fit a bracket underneath. All others are turned downwards so you don't see them. 



For additional stability (which is not needed on a machine of this type i guess) you could of course also do two connectors on each connection.


---
**Frank “Helmi” Helmschrott** *June 03, 2015 13:20*

sidenote: I had the extrusion in stock in my shop as i said but the estimated price for everything cut to the right dimension would have been costs of around 25€ at Motedis. Adding the connectors (which aren't that cheap of course than the simple screws you need if you drill holes in there) would make it about 40-50€ i think. Misumi would have cost me around 120€ for all the profiles cut, drilled and tapped. So maybe a good solution if you're looking to save some dollars.


---
**Derek Schuetz** *June 03, 2015 19:15*

**+Frank Helmschrott** those connectors look awesome could you video of how they work that looks like a great way to cut cost and assembly time


---
**Frank “Helmi” Helmschrott** *June 03, 2015 19:46*

**+Derek Schuetz** i could try to video that the next days. Actually they're not that cheap but still cheaper than ordering the drilled and tapped extrusion from misumi.


---
**Martin Bondéus** *June 03, 2015 20:50*


{% include youtubePlayer.html id=KYqqfSeUpCA %}
[https://www.youtube.com/watch?v=KYqqfSeUpCA](https://www.youtube.com/watch?v=KYqqfSeUpCA)


---
**Frank “Helmi” Helmschrott** *June 03, 2015 20:53*

yeah those are the bigger ones but actually they work quite similar. 


---
**Seth Mott** *June 14, 2015 22:59*

Are there any US sellers for those fasteners?


---
**Frank “Helmi” Helmschrott** *June 15, 2015 06:04*

I think McMaster has them


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/Xy1qqmJpr6V) &mdash; content and formatting may not be reliable*
