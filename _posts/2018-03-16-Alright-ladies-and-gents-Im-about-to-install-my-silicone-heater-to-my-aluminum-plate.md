---
layout: post
title: "Alright ladies and gents, I'm about to install my silicone heater to my aluminum plate"
date: March 16, 2018 20:42
category: "Discussion"
author: Ryan Fiske
---
Alright ladies and gents, I'm about to install my silicone heater to my aluminum plate. Been hesitant to do it as the install is likely pretty permanent and I'm worried I'll make a mistake on the cable routing. Where did you guys put your cable exiting? How did you handle the cabling? Cable chain? For there record, I think I have the square one with the centered wire that is called out in the BOM.





**Ryan Fiske**

---
---
**Dennis P** *March 16, 2018 21:44*

Yeah, its a step of no return... but dont fear, you can 'soak' the adhesive off with mineral spirits and re-adhere it with a sheet of 3M adhesive if you don't like how it came out. 



I centered the heater on the bed and ran the cable out the 'back'. Marked location with a pencil then taped it to the the bed along 2 adj edges. I peeled the release paper diagonally from a corner from underneath and pulled it horizontally and slowly squeegeed it to get the air bubbles out as i went and pulled the tape away. You can see the air bubbles and work them out. I also worked more bubbles out at the initial heating.  

 

Support: I tried a 10x10 cable chain, but i did not like how it loaded the bed, causing it to tip towards the chain. I figured the deflection would change depending how much weight was hanging and mess up a print. I put mine so it ran out the back, bundled it all together in a woven poly cable sleeve. I drilled and tapped a hole for the ground cable and zip tied the bundle together to that bolt to anchor it. Then I looped the bundle horizontally towards the center 20x20 extrusion spreader and then let it drape over it and loop down towards everything at the bottom. I am hoping that the weight then at least is constant and at the middle so no eccentric forces to tip the bed.  



Here is the place I get PEI and Kapton and adhesive from. I have gotten good service from them overall. 



[catalog.cshyde.com - 3D Printing: 3M® Adhesive Sheets](http://catalog.cshyde.com/viewitems/3d-printing-materials/ategories-3d-printing-materials-3m-adhesive-sheets)


---
**Daniel F** *March 16, 2018 22:00*

Cable chain in the back, but not from the beginning, I changed it. The cable exit is on the left side, I couldn’t rotate the heater after I stuck it to the plate...

Good to think about it beforehand. If the cable is long enough it’s still possible to route the cable around the bed though, that’s what I had to do. Quite happy with the cable chain in the back, as there are the shafts and rods on the left ang right.


---
**Ryan Fiske** *March 16, 2018 22:02*

Sounds like the consensus might be routing it out the back. Does anyone have some shots showing how they did it?


---
**Daniel F** *March 16, 2018 22:23*

![images/505b1121506c6394bf30ab8b2243405d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/505b1121506c6394bf30ab8b2243405d.jpeg)


---
**Daniel F** *March 16, 2018 22:24*

![images/c8d3620e87d334be341166fc6dbec747.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c8d3620e87d334be341166fc6dbec747.jpeg)


---
**Daniel F** *March 16, 2018 22:24*

![images/e06af8b73bef2506de7b62b0a198fbfe.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e06af8b73bef2506de7b62b0a198fbfe.jpeg)


---
**Daniel F** *March 16, 2018 22:24*

![images/59eda838f22af6ca8202f8c2913dc6fc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/59eda838f22af6ca8202f8c2913dc6fc.jpeg)


---
**Bruce Lunde** *March 16, 2018 22:37*

I did mine on the side, I Agree, it would have been better out the back![images/14626a9ca698d2bded7d5d8eb67bae48.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/14626a9ca698d2bded7d5d8eb67bae48.jpeg)


---
**Bruce Lunde** *March 16, 2018 22:37*

![images/9fbd0525b4f3022728ff4d9f5e9bcd4d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9fbd0525b4f3022728ff4d9f5e9bcd4d.jpeg)


---
**Ryan Fiske** *March 16, 2018 22:38*

Awesome, thanks for the pics Daniel!


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/fRwCFsfXfAK) &mdash; content and formatting may not be reliable*
