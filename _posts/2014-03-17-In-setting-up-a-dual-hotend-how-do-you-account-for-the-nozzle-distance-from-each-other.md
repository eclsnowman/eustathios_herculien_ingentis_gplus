---
layout: post
title: "In setting up a dual hotend, how do you account for the nozzle distance from each other"
date: March 17, 2014 04:58
category: "Discussion"
author: Wayne Friedt
---
In setting up a dual hotend, how do you account for the nozzle distance from each other. I could make them 50mm apart or 55mm. How do you set that up. 





**Wayne Friedt**

---
---
**Jarred Baines** *March 17, 2014 06:34*

It's calibrated in the slicer or firmware, not a design consideration, you could make it 46.795mm offset - you just set your slicer / firmware to that value and it works it all out (possibly with different g-code coordinates for the second extruder)



I don't actually have a 2nd extruder but I've been interested ;-) But from a design standpoint (sounds like your designing a carriage or something?) you just make it whatever suits the design best ;-)


---
**Eric Lien** *March 17, 2014 13:20*

One thing I will say is design in height adjustment for your hot end into the assembly. Leveling the two nozzle heights is important and an oversight I had in my first design.


---
**Wayne Friedt** *March 17, 2014 13:48*

Right good point. I had missed that already. I was trying to design them in between the rods. I  see you have put them on the outside, in front.



Did you make it adjustable at the hotend mounting or the whole coldend will adjust also. Prob only need one of them to be adjustable. 


---
**Eric Lien** *March 17, 2014 17:41*

Currently my e3d has some vertical slop in the ezstruder where the band clamps it in. I am currently shimming into position. Long term I am designing a more elegant and adjustable solution, but its not finished yet.


---
**Mike Miller** *March 21, 2014 14:16*

Funny thing that, my e3d was sloppy on my 3DR, I wedged toothpicks in the captured nut holes to keep them from spinning, and wrapped a rubber band in the groove for the J-head mount. The rubber took up the slack and gave the screws something to bite into. Took all the slack out of the system, and still has a little give with the rubber bands if necessary. 



That far up the cold end, temps shouldn't be a problem. 


---
*Imported from [Google+](https://plus.google.com/+WayneFriedt/posts/AGKxQ8pAMMD) &mdash; content and formatting may not be reliable*
