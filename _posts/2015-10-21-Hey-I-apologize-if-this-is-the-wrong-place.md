---
layout: post
title: "Hey I apologize if this is the wrong place"
date: October 21, 2015 19:20
category: "Discussion"
author: Tomek Brzezinski
---
Hey I apologize if this is the wrong place. Just found the herculien a few hours ago and diving into it. But I have a question about the support rail, so I figured I'd ask here. 



Is there a term for the "cross" type support rail pattern of the Herculien and the Ultimaker? This is in contrast to the typical double-rail single-axis moving support. 



I'm looking at the Sli3DR (richrap), and some variant of the Herculien, for a new printer (either modifying designs or following the designs.) But I don't know enough about the basics of the design choice for this "cross" type support rail vs the vaguely more classical design (makerbot,solidoodle,etc).  I've been searching to no avail for an hour now for some discussion on it, I think the problem is I don't know what to call this variance (my search terms stink.)



Principally the main reason why I'd like if there wasn't a "cross" to the design, is that it would allow me to use aluminum openbuild for the shorter (rectangular build plan here) action, but I can see how the "Cross" may have less space wasted for linear bearings while still keeping the carriage and motion quite square. :/:/:/



All in all I like the Herculien, but I'm having trouble finding the original design threads where people discuss why certain choices were made, which typically help inform me. 





**Tomek Brzezinski**

---
---
**Bryan Weaver** *October 21, 2015 22:37*

Is "gantry" printer the term you're looking for? I'm pretty sure it's actually pretty commonly referred to as an "Ultimaker-style gantry".


---
**Ryan Carlyle** *October 22, 2015 00:24*

Bridge gantry vs Ultimaker gantry. You can also call it a cross gantry, but that's less common. 


---
**Tomek Brzezinski** *October 22, 2015 02:09*

Ah!  Thank you folks.  I will be able to find more info with these terms.  Sweet!  Amazing how much it helps to have the right words :p


---
**Ryan Carlyle** *October 22, 2015 02:25*

By the way, the gantry shape isn't really quite as important as the motor arrangement. CoreXY and UM gantry are both very high-performance because they use parallel (not serial) actuator arrangements. The gantry doesn't have to sling around motor mass. That's huge for performance. Eg, CoreXY and a typical Replicator 1/2 use the same basic bridge gantry shape, but CoreXY is much higher performance. 


---
**Tomek Brzezinski** *October 22, 2015 03:37*

Hmm,  so I'm totally  on board with the offshore motors kind of business.  In that sense I'm definitively going for a corexy/UM design.   



I'm mostly still unsure about the support rail details.  I had some luck finding discussions based on your terms,  but it was polluted by conversations but moving platform(y axis) vs moving hot end designs(x and y.) I Haven't found as many details comparing the UM style of rails vs the non-UM style of rails.  


---
**Ryan Carlyle** *October 22, 2015 13:20*

I'm on my phone and can't link it, but check my profile page, I just posted a question about that a few days ago and got some good responses. My opinion is that CoreXY is simpler and scales up better for larger printers. Lots of people love UM gantries though.


---
*Imported from [Google+](https://plus.google.com/113469146086436912611/posts/PGEL4QZSvYK) &mdash; content and formatting may not be reliable*
