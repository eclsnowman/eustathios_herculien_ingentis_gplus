---
layout: post
title: "Hello All I'm putting out an SOS for parts print in the USA"
date: October 02, 2014 00:44
category: "Discussion"
author: Gus Montoya
---
Hello All



   I'm putting out an SOS for parts print in the USA. I had a QU-DB Twoup which has recently met it's end with multiple chassis fractures. It's unrepairable. I have chosen **+Eric Lien** Lien3D_Eustathios_Spider design to reincarnate my twoup parts as a bigger and better printer. If anyone in the USA and in this community or not from this community, can help me print the parts. I will be willing to paypal for the work. I am not rich but I can offer something as gratitude.





**Gus Montoya**

---
---
**Gary Hangsleben** *October 02, 2014 00:50*

Where are u located at ? Have provided parts for other local makers before.


---
**Jim Squirrel** *October 02, 2014 01:02*

this guy took upgraded his twoup as much as he could with printed parts before he designed a better printer using two up parts to replace it. [https://www.youtube.com/user/3DprintedLife](https://www.youtube.com/user/3DprintedLife)


---
**Gus Montoya** *October 02, 2014 04:58*

**+Jim Squirrel**  yes I know his "upgrade" kit is about $300+ . I prefer to spend that on a better/bigger platform.


---
**Gus Montoya** *October 02, 2014 05:04*

**+Gary Hangsleben** I am located in San Diego Ca. I need the entire 3d printed parts. I am searching for another T-slot deal. I just missed one :(   Entire frame for $45. I acted tooo slow dang!


---
**Gus Montoya** *October 03, 2014 23:57*

Hello **+Gary Hangsleben** , are you still ok with printing out parts for me? If not let me know. I'm still trying to get things going on my end. 


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/foVmiEP1NeQ) &mdash; content and formatting may not be reliable*
