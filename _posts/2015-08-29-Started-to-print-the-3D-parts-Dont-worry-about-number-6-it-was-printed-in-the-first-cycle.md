---
layout: post
title: "Started to print the 3D parts. Don't worry about number 6, it was printed in the first cycle"
date: August 29, 2015 08:25
category: "Show and Tell"
author: Roland Barenbrug
---
Started to print the 3D parts. Don't worry about number 6, it was printed in the first cycle.

![images/1f2dd9a16b7e82d51f2c49ab20353f7c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1f2dd9a16b7e82d51f2c49ab20353f7c.jpeg)



**Roland Barenbrug**

---
---
**Eric Lien** *August 29, 2015 13:45*

I am excited to watch your progress. It's a fun build.


---
**Chris Brent** *August 29, 2015 17:20*

I really wish I had a printer big enough to print a new printer! Have fun.


---
**Gunnar Meyers** *August 30, 2015 13:48*

I was able to do all of my printing on a Printrbot Metal, that printer only has a 6x6 inch print area. 


---
**Chris Brent** *August 30, 2015 18:22*

**+Gunnar Meyers** for a Eustathios or a HercuLien? Some of the Herc parts don't fit on my Thing-O-Matic for sure, it's only 4x4.


---
**Roland Barenbrug** *August 31, 2015 08:39*

Printing on a Prusa i3. Up to now all parts fit. Usually printing equal parts at once (2, 4 or 6).

Getting close to halfway. Justing taking some time, opted for quality printing rather than as fast as possible.


---
**Chris Brent** *August 31, 2015 17:16*

A few times I've looked at a Prusa thinking it would be great to able able to print a bigger printer with it. Current thinking is to scale down the Euth so that I can reuse the rods and steppers off my TOM but switch to the XY of these printers and get a bigger bed. Need to CAD it all out first to see if that's reasonable.


---
**Roland Barenbrug** *September 01, 2015 21:33*

Why would you scale down to a Prusa when you have a Euth?


---
**Roland Barenbrug** *September 01, 2015 21:38*

Small printing accident today. The fan of my E3D extruder stopped immediately showing the relevance of this fan. The hotend started to be come loose in a slowly melting extruder. Had to completely dismantle the extruder/hotend as filament could not be removed. Once dismantled had to heat the hotend to 250 C. When the top part of the hotend became hot, the filament could be removed.

Happy printing again but this was really scary. Lesson: in case you're printing with an E3D, make sure the fan is OK.


---
*Imported from [Google+](https://plus.google.com/118296832015849309457/posts/j2JsZ1EVZmq) &mdash; content and formatting may not be reliable*
