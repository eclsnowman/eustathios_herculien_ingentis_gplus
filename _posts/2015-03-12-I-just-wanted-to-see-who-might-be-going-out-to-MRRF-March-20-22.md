---
layout: post
title: "I just wanted to see who might be going out to MRRF March 20-22"
date: March 12, 2015 02:47
category: "Discussion"
author: Eric Lien
---
I just wanted to see who might be going out to MRRF March 20-22. I will have both my printers out there. And **+Martin Bondéus**​ from Bondtech is flying in from Sweden and driving down with me (I am very excited to talk shop and crack some beers with my friend from across the pond). I would love to meet some of you guys in person.





**Eric Lien**

---
---
**Brandon Satterfield** *March 12, 2015 13:19*

Wish I was! Would love to have some cold suds with you guys and geek out for a few days!


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/3o5WMxfAs9N) &mdash; content and formatting may not be reliable*
