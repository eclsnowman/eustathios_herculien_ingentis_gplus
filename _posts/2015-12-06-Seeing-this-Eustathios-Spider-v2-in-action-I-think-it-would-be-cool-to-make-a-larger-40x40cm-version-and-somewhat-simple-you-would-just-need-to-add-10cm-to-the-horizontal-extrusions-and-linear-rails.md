---
layout: post
title: "Seeing this Eustathios Spider v2 in action- I think it would be cool to make a larger 40x40cm version - and somewhat simple: you would just need to add 10cm to the horizontal extrusions and linear rails"
date: December 06, 2015 23:08
category: "Discussion"
author: Ted Huntington
---
Seeing this Eustathios Spider v2 in action- I think it would be cool to make a larger 40x40cm version - and somewhat simple: you would just need to add 10cm to the horizontal extrusions  and linear rails. You could even add 10cm more to the Z. - In fact a person could probably add a lot more to the Z if they wanted to- without any performance issues- I mostly don't need a large Z for the work I do- but it seems possible.





**Ted Huntington**

---
---
**Eric Lien** *December 06, 2015 23:45*

As things scale up, you have greater chance of deflection, so components have to be made more rigid. Which adds weight. For example on the HercuLien it has a usable 338 X 358 X 320. I went with 10mm cross rods. That could be increased with a more compact carriage (since the 50mm blower fans hang off the back). But 400mm is getting pretty huge, ask anyone who has a HercuLien. For that I would look into using ground linear guides on the sides instead of rods. You could still couple the sides to drive off one stepper with a common drive shaft at each end. Look into what **+Tim Rastall**​ was working on before he went into hiding... aka back to the real world :)


---
**Ted Huntington** *December 07, 2015 02:38*

when you say deflection, are you saying the force of gravity pulls down the middle of the cross rods so the printing level would vary/bow near the middle? I can see that the M8 cross rods can bend relatively easily, hopefully M10 cross rods would be enough to add 10cm more in X and Y. Interesting that linear guides on the perimeter would provide more stability than the M10 rods- then the belt tensioner with the belt attaches to the guide. It's hard to believe that 10cm would make a huge difference- it's just going to be a thought experiment for a long time since I'm moving onto to other projects. I will look at Tim Rastall's work- thanks for the tip!


---
**Ryan Carlyle** *December 07, 2015 02:52*

Deflection due to forces on the rods increases with the cube of length, so yeah, fairly modest length increases can add a LOT of flex. 


---
**Eric Lien** *December 07, 2015 03:00*

**+Ted Huntington** what also becomes an issue is rods on the edges remaining straight over longer distances. Since these are both a bearing surface (translation) and a drive train for the opposing axis (rotation) longer distances can begin to create harmonics that can cause issues.



I guess a good person to ask is **+Ashley Webster**​, I think he had plans to construct the largest printer of this style so far... But I am not sure how it is running.


---
**Ted Huntington** *December 07, 2015 03:18*

ok thanks for the explanation and pointer to Ashley Webster- I will take a look.


---
**Ted Huntington** *December 07, 2015 03:29*

**+Ashley Webster**

oh ok I see- is that at Herculean or Eustathios you are working on? With the standard Eustathios Spider V2 I am not seeing any deflection- but I just started printing and making small objects- it seems to me that 10cm more would not drastically change matters- I would probably change the M8 carriage rods to M10 just to be on the safe side- but I wonder if I really would have to.


---
**Ted Huntington** *December 07, 2015 03:45*

oh ok thanks


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/hHghQEkUSmY) &mdash; content and formatting may not be reliable*
