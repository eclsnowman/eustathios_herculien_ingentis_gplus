---
layout: post
title: "Eustathios build progress"
date: May 13, 2015 22:56
category: "Show and Tell"
author: Daniel F
---
Eustathios build progress



![images/c04006928908a0e7b68b12fd9387fe04.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c04006928908a0e7b68b12fd9387fe04.jpeg)
![images/fbbd027edd929de8cfa409562301721f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fbbd027edd929de8cfa409562301721f.jpeg)
![images/744160c87ef77c915838a68441d113f4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/744160c87ef77c915838a68441d113f4.jpeg)
![images/6e137fdb444081abc3a78a8c01f51adb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6e137fdb444081abc3a78a8c01f51adb.jpeg)
![images/97fe23e64619c1d5c0b11b7b5933879b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/97fe23e64619c1d5c0b11b7b5933879b.jpeg)
![images/542c9a9db488a147610b9eb7812acfb2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/542c9a9db488a147610b9eb7812acfb2.jpeg)
![images/644950f4283e7953da1b91ffec4b07c5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/644950f4283e7953da1b91ffec4b07c5.jpeg)
![images/2604a0d5e76cfdb530c89fbb58e84318.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2604a0d5e76cfdb530c89fbb58e84318.jpeg)
![images/7c462325d7ae03a27ceb1c29798e19bf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7c462325d7ae03a27ceb1c29798e19bf.jpeg)
![images/5fcff0c1c905708eb4f148c3178fe107.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5fcff0c1c905708eb4f148c3178fe107.jpeg)
![images/a25b059902473d4aa2c56a59255fb757.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a25b059902473d4aa2c56a59255fb757.jpeg)
![images/a922455c69ddd9db2e3222f096abce05.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a922455c69ddd9db2e3222f096abce05.jpeg)
![images/dcbab7b493c1657780a0f5622dc62a8c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dcbab7b493c1657780a0f5622dc62a8c.jpeg)
![images/3e6b1b0286b5ffca560189028ef084ea.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3e6b1b0286b5ffca560189028ef084ea.jpeg)
![images/bc23ace53f74979bcaf4898e1a10a07b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bc23ace53f74979bcaf4898e1a10a07b.jpeg)

**Daniel F**

---
---
**Daniel F** *May 13, 2015 23:15*

Started with my Eustathios build. Decided to build a mix between V1 and **+Eric Lien** V2. Used the bearing holders with holes and longer shafts so I can add hand wheels for manual positioning. I built new stepper holders for X and Y that allow to use Robotdiggs gt2 976mm belts so there is only one size of closed loop belts. The steppers stay below the lower cross rails and hence fit under a cover. I also used a different ACME nut and built a holder that can be screwed to the Z axis bed support (had to change the distance between the holes to fix this holder). Leadscrew is 10mm ACME with 2mm pitch, 32T pulley can be directly mounted to it. I used 6009 bearings  (10mm id, 22mm od) in the Z axis leadscrew support. Designed a little drilling template to drill the holes in the 2020 profile (fits for Motedis 6mm type B if anyone is interested)


---
**Daniel F** *May 13, 2015 23:31*

As a build manual I use **+Eric Lien** s  Eustathios'V2 3d model and turn it around, make measurements, again and again to understand how to assemble it. T-slot nuts are a problem, I forgot some and had to take the frame apart to add them. Ordered some M5 hammer nuts that can be added through the slot for that reason.


---
**Gus Montoya** *May 13, 2015 23:31*

Your on your way nice!


---
**Eric Lien** *May 14, 2015 00:20*

Love it. I like your motor mount. It eliminated the cantilever ;)


---
**Vic Catalasan** *May 14, 2015 17:50*

Very nice, was thinking something similar to my Herculien build, but I wanted to keep the belt shortest possible, or even just using a single belt driving the rails. 


---
**Mike Thornbury** *May 16, 2015 00:41*

Don't the hand wheels add more mass, lowering potential acceleration?


---
**Eric Lien** *May 16, 2015 01:23*

**+Mike Thornbury** the mass is so small I don't see how. I guess I could calculate the torque required... But the bushings would be the major contributor.


---
**Mike Thornbury** *May 17, 2015 05:04*

DOH! I was thinking metal, but of course they would be plastic... it was in the middle of the night - that's my excuse and I'm sticking to it >.<


---
**Rick Sollie** *May 23, 2015 03:21*

**+Daniel F**

 Could you make your drilling template available?  Looks to be a very useful part to start with!


---
**Daniel F** *May 23, 2015 23:58*

yes of course, here a link: [https://www.dropbox.com/l/aLWpJqJl8D534ySAnMkTnn](https://www.dropbox.com/l/aLWpJqJl8D534ySAnMkTnn)

formats are autodesk, step and stl

please note that the distances from the top are 2mm more than in Erics Design as I don't use the plasitc caps.

Distances are 10mm and 95mm from top (hole 1 and 3). 77mm for the bottom one (hole 2)

I use this profile: [http://www.motedis.com/shop/images/product_images/popup_images/1_1.JPG](http://www.motedis.com/shop/images/product_images/popup_images/1_1.JPG)

You might have to adapt the template if you use a different one


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/7wMEKzx9aw6) &mdash; content and formatting may not be reliable*
