---
layout: post
title: "Hello, I'm interested in building the eustathios spider v2"
date: May 06, 2017 20:48
category: "Discussion"
author: Christian Kemme
---
Hello, I'm interested in building the eustathios spider v2.

Recently I visited the github page and searched for some kind of construction manual or a guide but couldn't find one.

I only have experience with building printers from kits, so forgive me my lack of knowledge.





**Christian Kemme**

---
---
**Eric Lien** *May 08, 2017 00:37*

Hello **+Christian Kemme**​, unfortunately we don't have a manual. We had a member once who was going to do one, but he ended up not completing the build and hence didn't make a manual.



But since the model is very complete, that is what I recommend as your guide.



Every nut, bolt, etc is in the model. So having the model up on a laptop during the build process is unbelievably helpful.



There are also countless community mods that you might want to incorporate. Many are on the GitHub, but some are sprinkled through the posts which I recommend you read over. There are so many talented builders in the group. So always feel free to ask questions and advice here if you decide to take on the build.


---
**Christian Kemme** *May 08, 2017 08:34*

Ok, I'll take a look at the model. Thanks for the clarification **+Eric Lien** .


---
**Christian Kemme** *May 09, 2017 12:05*

How are the estimated cost if I use the recommended  parts?


---
**Eric Lien** *May 09, 2017 14:31*

Some people have been frugal and self sourced parts around 900-1000. Others I know have been closer to the 1200-1300.



I recommend you do not get the hardware from McMaster. Try [https://www.trimcraftaviationrc.com](https://www.trimcraftaviationrc.com) instead. Also you save using the AliExpress ball screws vs misumi leadscrews. But I do recommend the misumi precision cut extrusions since the sizing is critical. That is unless you have a method to accurately cut them yourself.


---
*Imported from [Google+](https://plus.google.com/115230246637682827085/posts/Pr4BZeyjVjr) &mdash; content and formatting may not be reliable*
