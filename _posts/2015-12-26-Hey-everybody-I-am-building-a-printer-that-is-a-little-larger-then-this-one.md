---
layout: post
title: "Hey everybody. I am building a printer that is a little larger then this one"
date: December 26, 2015 03:16
category: "Discussion"
author: Orrin Naylor
---
Hey everybody. I am building a printer that is a little larger then this one. I need an aluminum sheet that is 35cm*35cm for the build plate. I got one and it wasn't straight:( if anyone knows where I could get one that would be super helpful. Thanks!!!





**Orrin Naylor**

---
---
**Mike Miller** *December 26, 2015 03:26*

I overcome flatness issues on the buildplate with glass. 




---
**Tomek Brzezinski** *December 26, 2015 03:29*

You have to go fairly thick or expensive (ground) with aluminum to get commonly flat stock.  



Glass is cheap and super flat though!


---
**Orrin Naylor** *December 26, 2015 03:34*

Thanks!


---
**Tomek Brzezinski** *December 26, 2015 04:20*

I meant to add, that if for whatever reason you do want flat aluminum stock, I have used "aluminum jig stock" from mcmaster before.



This is mostly if you need it for a different project. Not really worth it for most 3D printers.  And if you machine the stock you will find internal stresses can rebend the aluminum.


---
**Zane Baird** *December 26, 2015 04:20*

And don't get it shear cut. That will warp it. Easiest to "flatten" it yourself with some wood blocks at the corners and a rubber mallet. I forgot about this issue initially. Now I have an unevenly heated surface as I've propped up the corners of the glass to keep it flat. Works fine unless you want to use the corners of the build area. 


---
**Frank “Helmi” Helmschrott** *December 26, 2015 07:08*

I got mine from a local metal shop. They sold me one that was milled plain for around 40€ in the size you are looking for. It's not that cheap but i think fair pricing. It's near to perfect regarding flatness but of course Aluminum isn't that static when heated up so you will notice some small bumps here and there but will probably have near to perfeact contact with a glass sheet or whatever you use. I'm printing without glass for around half a year now and that works fine.


---
**Daniel F** *December 26, 2015 08:13*

I also use cast aluminium that was milled flat, it is 6mm thick and takes a while to heat up but it is flat. It cost around 40€ plus shipping.


---
**Orrin Naylor** *December 26, 2015 16:43*

Could you send me a link to the place you bought that?


---
**Daniel F** *December 26, 2015 17:22*

[https://www.aluminium-online-shop.de/de/shop-aluminium-kleinstmengen/Feingefraeste-Gussplatten-_-26/index.html](https://www.aluminium-online-shop.de/de/shop-aluminium-kleinstmengen/Feingefraeste-Gussplatten-_-26/index.html)

But it only makes sense if you live in Europe. It might be helpful to get an ideea though.


---
**Matthew Kelch** *December 26, 2015 19:11*

Finding a place to make the heat spreader for the build platform has been a struggle for me as well. Local places I have talked to aren't interested in doing a single piece for me. 


---
*Imported from [Google+](https://plus.google.com/108930960082385878809/posts/JCw8jDSvpEf) &mdash; content and formatting may not be reliable*
