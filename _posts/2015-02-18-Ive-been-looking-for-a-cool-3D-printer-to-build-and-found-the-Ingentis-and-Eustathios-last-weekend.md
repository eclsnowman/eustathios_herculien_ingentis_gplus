---
layout: post
title: "I've been looking for a cool 3D printer to build and found the Ingentis and Eustathios last weekend"
date: February 18, 2015 05:43
category: "Discussion"
author: Eric Bessette
---
I've been looking for a cool 3D printer to build and found the Ingentis and Eustathios last weekend.  I just wanted to say that seeing this active community sealed the deal for me.  I've ordered a bunch of the materials and am excited to get started.  Thanks for all the help you've provided so far and (hopefully) will provide in the future.





**Eric Bessette**

---
---
**Seth Messer** *February 18, 2015 14:39*

good luck eric!! it is definitely a lively community and extremely helpful. it's constantly evolving for all 3 machine designs too. keep us updated on your progress.


---
*Imported from [Google+](https://plus.google.com/106288190144199995561/posts/X1hUBzSfEX2) &mdash; content and formatting may not be reliable*
