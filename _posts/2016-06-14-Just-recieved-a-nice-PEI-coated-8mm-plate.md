---
layout: post
title: "Just recieved a nice PEI coated 8mm plate;)"
date: June 14, 2016 15:25
category: "Build Logs"
author: Maxime Favre
---
Just recieved a nice PEI coated 8mm plate;)

![images/8e63eef922a8053024cb3bb202bf4f36.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8e63eef922a8053024cb3bb202bf4f36.jpeg)



**Maxime Favre**

---
---
**Michaël Memeteau** *June 14, 2016 15:34*

Looks nice!

Where from? Not too bloody hell expensive?


---
**Sean B** *June 14, 2016 16:27*

We need details man!!  I am actually about to purchase a plate and want pei but haven't found any affordable options.﻿


---
**Maxime Favre** *June 14, 2016 17:10*

Bloody hell expensive isn't the term but 120€ is something.

I contacted Frank from clever3d in Germany, sent him the drawing and now it's listed on his shop: [https://estore-sslserver.eu/clever3d.de/epages/7a4290fc-7c7f-46cc-9b99-eadef22228e2.sf/en_GB/?ObjectPath=/Shops/7a4290fc-7c7f-46cc-9b99-eadef22228e2/Products/c3d-DDP-PEI](https://estore-sslserver.eu/clever3d.de/epages/7a4290fc-7c7f-46cc-9b99-eadef22228e2.sf/en_GB/?ObjectPath=/Shops/7a4290fc-7c7f-46cc-9b99-eadef22228e2/Products/c3d-DDP-PEI)
I'll give you a feedback soon


---
**Matthew Kelch** *June 15, 2016 21:28*

Looks like it's $162.00 shipped to the US.  Looks very nice, though I think I'd rather just continue using PEI on glass.


---
**Jussi** *March 20, 2017 10:19*

I have the same aluminum plate, but in black. Can someone tell me what silicone heating plate is suitable for this? 230 volts.

My powersupply is Mean Well SP-200-24 and I have Azteeg X5 mini 32bit All-in-one controller V3 and [digikey.com - R99-12 FOR G3NA](http://www.digikey.com/products/en?keywords=Z2253-ND)


---
**Maxime Favre** *March 20, 2017 10:37*

I bought this one. Make sure to select 220V with tape.

[robotdigg.com - 300mm Silicone Rubber Heater Pad - RobotDigg](http://www.robotdigg.com/product/115/300mm-Silicone-Rubber-Heater-Pad)


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/AAEAYXfPB1o) &mdash; content and formatting may not be reliable*
