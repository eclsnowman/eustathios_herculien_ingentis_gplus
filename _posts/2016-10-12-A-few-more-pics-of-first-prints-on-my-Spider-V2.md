---
layout: post
title: "A few more pics of first prints on my Spider V2"
date: October 12, 2016 00:33
category: "Show and Tell"
author: jerryflyguy
---
A few more pics of first prints on my Spider V2.



Marvin was print #1- 0.2mm and 205C PLA. No part cooking fan was a negative. Re-calibrated the extruder also



I did a second Marvin but it appears that he has been 'lost' according to my 6yr old



Benchy was done at 0.2mm also, some issues w/ improper retracts (the blobs after the rapid moves) also that one layer where there was limited perimeter fill, not sure what that is? I increased my retracts (to 1.5mm from 1mm). I'm printing a second Benchy right now.



Here's another odd thing.. this print (video) is off the simplify3d page on 'perfect first layers' 


{% include youtubePlayer.html id=YOgl0TLksRE %}
[https://youtu.be/YOgl0TLksRE](https://youtu.be/YOgl0TLksRE)



You'll see one edge of this print is terrible quality.. it appears to be all positional error, the side opposite is fairly good but for some reason that one edge/side just looks rotten. 



I'll find a place to post my FFF file, it's basically the one in the GitHub w/ a couple tiny tweaks.







![images/dcc6f01ce38cf3bd5a0ccab04dd5d2dc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dcc6f01ce38cf3bd5a0ccab04dd5d2dc.jpeg)
![images/162de0bb2bb21f93eb827649d1f836ba.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/162de0bb2bb21f93eb827649d1f836ba.jpeg)
![images/c6f7869f5200f96b21a6e9f623577acd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c6f7869f5200f96b21a6e9f623577acd.jpeg)
![images/4bfd378703cdd2852ea9890daa7754cb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4bfd378703cdd2852ea9890daa7754cb.jpeg)
![images/be3b1eeeb763866987d32d4129ec8fdb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/be3b1eeeb763866987d32d4129ec8fdb.jpeg)
![images/ba73f98ac85aca804c06a45071ccc88b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ba73f98ac85aca804c06a45071ccc88b.jpeg)
![images/74d7fa61c9b35dad606a4180d8ba0084.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/74d7fa61c9b35dad606a4180d8ba0084.jpeg)

**jerryflyguy**

---
---
**Eric Lien** *October 12, 2016 01:06*

One thing I will say is with Bowden you need far longer retraction than with direct drive. For example I run between 4.5mm to 6.5mm depending on the filament type I am running on my Eustathios. The reason is there is slack in the system that must be overcome, and if you have a softer compressible filament it needs to be longer because the length will change between being in compression (extrude) vs tension (retract).



I will go over your factory file once you are done uploading it to make some recommendations.


---
**jerryflyguy** *October 12, 2016 01:26*

Second Benchy 0.1mm layers 1.5mm retract![images/fb390157ea50bdf54409f3318b4fb6e3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fb390157ea50bdf54409f3318b4fb6e3.jpeg)


---
**jerryflyguy** *October 12, 2016 01:28*

Scratch that.. that is the same pic of the original Benchy print


---
**Roland Barenbrug** *October 15, 2016 08:21*

**+jerryflyguy** to support you further, it looks like extrusion stopped somwhere like 10mm from the bottom. Anything happende there? Further I notice some kind of moiree like pattern on the outside. At what speed are you printing? Changing speed might solve this.


---
**jerryflyguy** *October 15, 2016 18:38*

**+Roland Barenbrug** yes, I determined that the roll feeding was an issue. I've printed a new filament roll mount and that issue seems to have been solved. This part (pictured here) was printed at 80mm/s . I've posted a video of a more recent print at 60mm/s which has a different surface texture. 


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/8CczF76KihN) &mdash; content and formatting may not be reliable*
