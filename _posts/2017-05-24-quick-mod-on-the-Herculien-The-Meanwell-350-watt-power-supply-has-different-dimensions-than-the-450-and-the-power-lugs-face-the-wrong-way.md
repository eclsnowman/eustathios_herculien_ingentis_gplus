---
layout: post
title: "quick mod on the Herculien. The Meanwell 350 watt power supply has different dimensions than the 450, and the power lugs face the wrong way"
date: May 24, 2017 22:36
category: "Show and Tell"
author: James Ochs
---
quick mod on the Herculien.  The Meanwell 350 watt power supply has different dimensions than the 450, and the power lugs face the wrong way.  The lower brackets in the bom work fine, but the upper one doesn't fit   I added two printed brackets for the top, and some extrusion on the frame to create a cutout for the power supply and a place to attach the upper brackets.  There's an extra upright in there that acts as a slider so that I can put a small peice of lexan to protect the lugs, but still provide airflow to the fan and vents on the power supply.  Here are some pics, I'm planning to write it up and submit a pr for the community mods.﻿



![images/703486bd153281ed35159ea391da40e4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/703486bd153281ed35159ea391da40e4.jpeg)
![images/c01b88c1bc4ee20c0c148a2fd725dcd8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c01b88c1bc4ee20c0c148a2fd725dcd8.jpeg)
![images/5ec509069f0a33afff684d2f075f66c7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5ec509069f0a33afff684d2f075f66c7.jpeg)
![images/e38c41310007898fd1c823371ca1b3c4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e38c41310007898fd1c823371ca1b3c4.jpeg)

**James Ochs**

---
---
**Eric Lien** *May 24, 2017 23:28*

Looks great. Much better than the original. Yeah please make a submission to the GitHub with your mods.


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/Tz8v7mHzBR1) &mdash; content and formatting may not be reliable*
