---
layout: post
title: "Some pics of the plexiglass parts"
date: March 26, 2014 01:22
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
Some pics of the #Eustathios plexiglass parts.



![images/05ecd31c58087ccee30fe6de7f8ab496.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/05ecd31c58087ccee30fe6de7f8ab496.jpeg)
![images/12420a45011644876d2ac4a6e28261a4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/12420a45011644876d2ac4a6e28261a4.jpeg)
![images/2079464722abeeee945c2d5ef2dc1c23.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2079464722abeeee945c2d5ef2dc1c23.jpeg)

**Jason Smith (Birds Of Paradise FPV)**

---
---
**Mike Miller** *March 26, 2014 02:31*

So...a little googling shows plexi melts at 160C...the build plate isn't plexi, is it? (other than that, it looks gorgeous)


---
**Jason Smith (Birds Of Paradise FPV)** *March 26, 2014 02:42*

**+Mike Miller** , The current Acryllic design definitely won't support a heated bed (that's something I'll probably add down the road), but I took a page out of the Ultimaker's book:

[https://www.ultimaker.com/products/build-platform](https://www.ultimaker.com/products/build-platform)


---
**Mike Miller** *March 26, 2014 02:48*

There's residual heat from the extruder, you might want to make sure there's insulation between the build plate and the bed, or ensure a good draft to keep it all cool. 


---
**Mike Miller** *March 26, 2014 03:30*

You're also giving me ideas, I've played about with plexi, and this would be a good exercise... 


---
**Brian Bland** *March 26, 2014 10:06*

What bits do you use on the Acryllic?  I am wanting to cut some Acrylic on my CNC router.


---
**Eric Lien** *March 27, 2014 04:29*

I just started purchasing the extrusions to build this design. Love the steppers mounted under neath. The sketchup models needed some dressing up when output to stls. Rhino and netfab seem to have done the trick. Looking forward to getting everything mechanically assembled.﻿



Let me know if you have any tricks or notes you ran into during your assembly.


---
**Jason Smith (Birds Of Paradise FPV)** *March 27, 2014 08:27*

**+Brian Bland**, this is the bit I used for milling the plexi: [https://www.amazon.com/gp/aw/ya/or](https://www.amazon.com/gp/aw/ya/or)

I used a feed rate of 600mm/min, and it turned out great. Good luck!


---
**Jason Smith (Birds Of Paradise FPV)** *March 27, 2014 08:34*

**+Eric Lien**, glad to hear you like the design. Cleaning up the model to make exporting easier is on my todo list, but I've had pretty good luck using netfab to fix things up for printing. The only tip I have is: if you're using pre-assembly t-nuts for the extrusion, make sure you place the correct number on each side of each piece of extrusion before assembly. I won't embarrass myself by telling how many times I had to disassemble pieces to insert more... Good luck!


---
**Eric Lien** *March 28, 2014 23:22*

**+Jason Smith** do you have a vitemins list (bolts used)?


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/UJRypXL8iTN) &mdash; content and formatting may not be reliable*
