---
layout: post
title: "Does anyone have the exact dimensions of the aluminum build plate for the herculien I can not seem to find the measurements in the BOM or on the instructions"
date: June 10, 2015 22:49
category: "Discussion"
author: Gunnar Meyers
---
Does anyone have the exact dimensions of the aluminum build plate for the herculien I can not seem to find the measurements in the BOM or on the instructions. 





**Gunnar Meyers**

---
---
**Erik Scott** *June 10, 2015 23:03*

[http://i.imgur.com/DAvkN9t.png](http://i.imgur.com/DAvkN9t.png)


---
**Eric Lien** *June 11, 2015 01:29*

I will put up a PDF and dxf for it tonight on github. I thought it was there already... Weird.


---
**Eric Lien** *June 11, 2015 01:30*

**+Erik Scott** thats for Eustathios


---
**Eric Lien** *June 11, 2015 01:44*

Nevermind.. they were there. I had them in a folder called drawings:



PDF: [https://github.com/eclsnowman/HercuLien/blob/master/Drawings/ALUMINUM_HEAT_SPREADER.PDF](https://github.com/eclsnowman/HercuLien/blob/master/Drawings/ALUMINUM_HEAT_SPREADER.PDF)



DXF: [https://raw.githubusercontent.com/eclsnowman/HercuLien/master/Drawings/Aluminum_Build_Plate.DXF](https://raw.githubusercontent.com/eclsnowman/HercuLien/master/Drawings/Aluminum_Build_Plate.DXF)    (note for some github in the browser tries to open dxf in the text window, you will need to download the RAW from the link at the top)


---
**Erik Scott** *June 11, 2015 02:28*

D'oh! Sorry 'bout that. I need to learn to read...


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/Ay9PRxUu514) &mdash; content and formatting may not be reliable*
