---
layout: post
title: "Anyone know the connectors on this little board?"
date: December 12, 2017 18:59
category: "Discussion"
author: Gus Montoya
---
Anyone know the connectors on this little board? I beleive that's a 3 pin JST XH connector but what about the black ones? The 10 pin male and female?



![images/1e3c794b537614e4a6354fd3ac0f4ac6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1e3c794b537614e4a6354fd3ac0f4ac6.jpeg)
![images/493d8822ec057b41243e193761819765.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/493d8822ec057b41243e193761819765.jpeg)
![images/4dd16265914119e4c951101835a71c55.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4dd16265914119e4c951101835a71c55.jpeg)

**Gus Montoya**

---
---
**Jeff DeMaagd** *December 13, 2017 13:39*

Those are colloquially known as DuPont connectors or 0.1” / 2.54mm header connectors, after the spacing between the pin centers.


---
**Ryan Fiske** *December 16, 2017 02:26*

The first black one is Dupont yes, but the one with pins is a 10 pin IDC connector. Usually used with ribbon cables.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/KvhunvNdMLp) &mdash; content and formatting may not be reliable*
