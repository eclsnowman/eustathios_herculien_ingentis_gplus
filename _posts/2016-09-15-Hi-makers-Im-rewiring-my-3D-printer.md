---
layout: post
title: "Hi makers I'm rewiring my 3D printer"
date: September 15, 2016 01:07
category: "Discussion"
author: jonjon f
---
Hi makers



   I'm rewiring my 3D printer. And i notice that I'm using a 110v heated bed. And I try to ground it to the chassis. And I notice the my bed was not connected to the chassis of the 3dprinter. So should I ground the be or not.?



Thanks in 





**jonjon f**

---
---
**Daniel F** *September 15, 2016 07:34*

Don't connect it to ground, keep the two wires of the heater isolated but ground the chassis itself. Also connect the bed plate (if metal) to the rest of the chassis with a thick wire. This is an important safety measure to prevent an electric shock in case the phase wire of the 110/230V heater gets loose (or the isolater burns) and touches the chassis.

This of course implies that your mains connector has 3 pins L,N,PE and PE is connected to the chasis--don't use a 2 wire mains cable to connect your printer.


---
*Imported from [Google+](https://plus.google.com/111250467064356525268/posts/JpgNkhxGoqk) &mdash; content and formatting may not be reliable*
