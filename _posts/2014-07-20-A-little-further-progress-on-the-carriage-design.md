---
layout: post
title: "A little further progress on the carriage design"
date: July 20, 2014 17:47
category: "Deviations from Norm"
author: Eric Lien
---
A little further progress on the carriage design. Front clamp/fan-mount works great. I will be adding some directional ducts that mate the outlet of the fan to the heat sink later. Also I still need to design the part fan ducts. But it's getting closer.



![images/43d257e3d326078259f2d9480de32bcf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/43d257e3d326078259f2d9480de32bcf.jpeg)
![images/d09b87a0314a9d1bac12911085edc614.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d09b87a0314a9d1bac12911085edc614.jpeg)
![images/c9b7236fefa78a0b16f1d6c3fda4c5e7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c9b7236fefa78a0b16f1d6c3fda4c5e7.jpeg)
![images/75455f8437ac747bc64d218505d30959.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/75455f8437ac747bc64d218505d30959.jpeg)
![images/bd046f8e6882fa8396500caa00f543c0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bd046f8e6882fa8396500caa00f543c0.jpeg)

**Eric Lien**

---
---
**Eric Lien** *July 20, 2014 18:37*

**+Shauki Bagdadi** thanks. Now for the part fan duct. Ugh. I am no good at splines, lofts, and organic forms. 



You have any suggestions of a good plan of attack? My brain thinks in right angles :)﻿


---
**Eric Lien** *July 20, 2014 18:49*

**+Shauki Bagdadi** yes. That was my problem with solidworks also. Let's just say there was a lot of blue lines on my last spline/loft model :)



Then I would change one dimension due to an interference and.... Boom, model would freak out and "crap the bed".


---
**Maxim Melcher** *July 20, 2014 20:38*

Both heatsink of hotends are covered with motors of fans.   The air flow goes not  to heatsinks.   A single fan in the middle would be more effective.   


---
**Dale Dunn** *July 20, 2014 21:36*

Hehe. SolidWorks splines do take some getting used to.


---
**Eric Lien** *July 20, 2014 22:31*

**+Maxim Melcher** it will when its ducted. Ducts will bolt to the lower fan holes. Also fans like this move the majority of the air near the tips. So it would actually work just fine how it is. 


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/fzg8gRQ89QQ) &mdash; content and formatting may not be reliable*
