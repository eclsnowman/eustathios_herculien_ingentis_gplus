---
layout: post
title: "What would cause thermal runaway errors? This is Marlin on Ramps and its been working like a champ"
date: July 18, 2018 04:11
category: "Discussion"
author: Dennis P
---
What would cause thermal runaway errors? This is Marlin on Ramps and its been working like a champ. Then today all the sudden, wham! There are little blips like this asking the temp track. the timelapse  shows nothing.  printer is in the basement in a draft free area (home brew kegs and carboys right next to this. I am a little mature as to where to start diagnosing. 

![images/7723f949b65297b1a675107a6e795e7e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7723f949b65297b1a675107a6e795e7e.png)



**Dennis P**

---
---
**casey dunn** *July 18, 2018 04:20*

That's not normally what would be called "thermal run away".



Are you concerned about the rough little drops or the big one at the end?


---
**Oliver Seiler** *July 18, 2018 04:57*

A faulty connection on the thermistor or heater cartridge can cause that. I've had a very similar issue recently after I replaced a thermistor and the new one was faulty, but it can also happen with wear.

I assume it's not a part cooling fan that turned on and the heater couldn't keep up? 


---
**Dennis P** *July 18, 2018 07:57*

**+casey dunn** you right, its not like the heater got away from itself. Marlin lumps all unchecked heater errors as 'runaway'. I checked the heater for resistance from the board end, and checked the hotend thermistor as well and they both seem fine. they are cheap enough to just swap out, but I'd like to figure out why this is happening. 

 


---
**Dennis P** *July 18, 2018 08:15*

here is the results of the next print I tried. I started off by preheating the pritner by PLA in the Temp settings 180/60 then starting a PETG print at 240/70.  

![images/71085965e46858491ec64844ab79eb29.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/71085965e46858491ec64844ab79eb29.png)


---
**Oliver Seiler** *July 18, 2018 08:24*

If you look at how rapidly the temperature changes at the more extreme spikes then I'd say that can only be a loose connection because the hotend itself will never gain/loose temperature so quickly. 

Maybe compare it to how quickly it cools down after turning off the heater. Also try wiggeling the cables and see if that causes spikes.


---
**hon po** *July 18, 2018 13:11*

Could be broken wire stay connected when wire is straight, but open when bent. Wiggle the wire when checking resistance usually expose it.


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/7RbDMVUNsEP) &mdash; content and formatting may not be reliable*
