---
layout: post
title: "Hi guys, May I ask you a service;"
date: November 21, 2017 18:07
category: "Discussion"
author: Maxime Favre
---
Hi guys,



May I ask you a service; can someone running a eustathios V2 with a smoothieboard send me his config.txt ? (especially if you run a viki2 lcd)



My board encountered a strange thing a few days ago, it could read the sd flash anymore, wiped the whole config.txt and refused to do anything.



I somehow made it work again by flashing by many try of formatting the card and flashing the firmware but I don't have any backup of the config file (I know.... <s>_</s>).  Any help welcome so I don't start from scratch again ;)



Cheers



Max





**Maxime Favre**

---
---
**Eric Lien** *November 21, 2017 22:35*

I know recent changes to the firmware required new Config files due to some changes. I haven't updated my smoothie in too long. I should go through and make some more recent configs for people to use. It doesn't take too long. 


---
**Maxime Favre** *November 22, 2017 07:02*

**+Eric Lien**​ yep I made that, new config with the latest firmware. I had to try a bit for the step/mm, pining, inverted I/O and LCD config. It works ok now. I will save the file this time ...


---
**Eric Lien** *November 22, 2017 07:09*

**+Maxime Favre** any chance you want to push it out to the github in the user mods area, or link it here for others?


---
**Maxime Favre** *November 22, 2017 07:15*

That's the plan :)


---
**Maxime Favre** *November 22, 2017 18:29*

I made a pull request on github




---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/M4zuNBoZxsg) &mdash; content and formatting may not be reliable*
