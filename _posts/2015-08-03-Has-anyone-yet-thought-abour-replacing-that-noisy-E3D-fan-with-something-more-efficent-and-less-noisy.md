---
layout: post
title: "Has anyone yet thought abour replacing that noisy E3D fan with something more efficent and less noisy?"
date: August 03, 2015 08:11
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Has anyone yet thought abour replacing that noisy E3D fan with something more efficent and less noisy? As my printer is rather silent now the main noise comes from my 50mm board cooler (which is my fault, will replace this with at least 60 or even 80mm) and the 30mm E3D blower. I'm thinking about using a second 50mm blower like on the duct to cool the Hotend. That ones are really silently spinning. What do you think? Maybe i'll just play around a bit with a redraw of the carriage.





**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *August 03, 2015 11:09*

I would love to see what you can come up with. I agree the heatsink fan is loud by comparison.


---
**Dale Dunn** *August 03, 2015 16:19*

Those fans got some attention a few years ago when PC components (especially the motherboard chipsets) were generating so much heat. On the PCs, the fans turned at a speed that was very annoying. So, it's not just how loud the fan is that matters. The cure on the PCs was to replace the chipset heatsink with something that allowed you to use a larger fan to move the same amount of air at lower rpm. Larger fans isn't usually something we want to do to a 3D printer, so it would be nice if another kind of fan could do the job with less annoying noise. Maybe a new shroud for a 40 mm fan would be a good compromise.



An idea mentioned elsewhere for a heated chamber was to use an airbrush compressor to feed compressed air to the heatsink through a tube. That has the added effect of more cooling as the compressed air expands.



This is all theoretical to me, since I still haven't mounted my E3D extruder and I therefore don't have a cooling fan on it.


---
**Vic Catalasan** *August 04, 2015 06:54*

Interesting, the E3D fan is actually the quiet of the 3 other fans I have running, the bed fan being the loudest and the intermittent power supply fan. 


---
**Frank “Helmi” Helmschrott** *August 04, 2015 07:21*

**+Vic Catalasan** it's probably a matter of exact product you are using. The power supply is personally a bit too noisy for me too but the E3D one is definitely much louder - i think mine additionally has a problem with it's bearing or doesn't even have one as there is some additional crackling noise with it. Maybe i'd just have to replace it. Also the 50mm duct fan is extremely quiet. I was really suprised by that as i have other 60mm radial fans that are way noisier.


---
**Frank “Helmi” Helmschrott** *August 04, 2015 07:29*

btw. these were the 50mm blowers i had purchased [http://www.ebay.de/itm/391148255949?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.de/itm/391148255949?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT) - i'm not sure about their power but i have a slight feeling that the duct could use a bit more airflow. Maybe those are just quiet because they're not powerful enough :)


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/bfocPby5vYc) &mdash; content and formatting may not be reliable*
