---
layout: post
title: "Testing big"
date: July 25, 2015 09:29
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
Testing big. 

![images/d4466352e33ba0d6c626f2918dd0e4bc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d4466352e33ba0d6c626f2918dd0e4bc.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**Vic Catalasan** *July 26, 2015 14:01*

Very nice! what is that surface you printing on? 


---
**Frank “Helmi” Helmschrott** *July 26, 2015 17:26*

It's a Buildtak print surface mounted to the aluminum direct.  [http://www.buildtak.eu/](http://www.buildtak.eu/)


---
**Vic Catalasan** *July 27, 2015 07:07*

Thanks will try it


---
**Ivans Nabereznihs** *September 02, 2015 11:03*

I had two pieces Buildtak on my first 3d printer. It was good from the start but after I got problems with buildtak. After about two weeks it started bubbling. Now I use ordinary glass, plus hair spray L'oreal Super Strong Hold. Works great. :-)


---
**Frank “Helmi” Helmschrott** *September 02, 2015 11:07*

no real problems so far but good to know - i'll watch out for bubbles.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/iyW3KbHd7Ro) &mdash; content and formatting may not be reliable*
