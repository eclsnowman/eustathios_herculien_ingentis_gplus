---
layout: post
title: "Well, here are some more picture of Eirikur Sigbjrnsson test prints which I hope shows more details (good and bad)"
date: September 14, 2015 10:19
category: "Show and Tell"
author: Gústav K Gústavsson
---
Well, here are some more picture of **+Eirikur Sigbjörnsson** test prints which I hope shows more details (good and bad).

The Carbon fibre (black) is the first test print without any tweaking done. We probable will do more tests when we have access to hardened steel nozzles so we will not ruin the printer. The wood fibre one shows when we where finding the right temperature and shows clearly color/texture changes with temperature. Started to hot and the room filled with wood burning smell ;-(



![images/885b780a566a59df6fe9058b0a8d5ee6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/885b780a566a59df6fe9058b0a8d5ee6.jpeg)
![images/e9e7dc767ffc90950afafea2e16fec3f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e9e7dc767ffc90950afafea2e16fec3f.jpeg)
![images/61b856e1ef5237097fd93f577678d2e6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/61b856e1ef5237097fd93f577678d2e6.jpeg)
![images/ca31cac783d654f182a013779230b998.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ca31cac783d654f182a013779230b998.jpeg)
![images/19f6054c823076ffdc98aa771f238f59.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/19f6054c823076ffdc98aa771f238f59.jpeg)
![images/16fafeb31bce67293b32570113fc2e19.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/16fafeb31bce67293b32570113fc2e19.jpeg)

**Gústav K Gústavsson**

---
---
**Miguel Sánchez** *September 14, 2015 10:37*

awesome print quality


---
**Frank “Helmi” Helmschrott** *September 14, 2015 10:57*

Looks nice. What materials were you using and what temperature did you end up with the wooden one?



Print quality looks good - should be something below 0.2mm layer height, right?


---
**Gústav K Gústavsson** *September 14, 2015 11:41*

The layer height is 0.15 and the temp on the wood fibre was 210° C in the end if memory serves me right. The wood piece was very rigid but we managed to break it by bending and twisting the rod clamp. If you look closely you can see the break. I leave it up to **+Eirikur Sigbjörnsson** to answer the exact materials used :-)


---
**Bruce Lunde** *September 14, 2015 12:05*

These look great! Were you able to test fit bronze bearings and associated rods?


---
**Eirikur Sigbjörnsson** *September 14, 2015 12:35*

**+Frank Helmschrott** I put descriptions in my earlier post about this prints. The black one is CF XT20, the wood filament is Woodfill and the "transparent" one is XT-Clear, all from ColorFabb. For comparison the blue one is a PETG. I more or less took the reccommended setting from the ColorFabb site and went the middle way and it turned out great. With more fine tuning the prints could be even better.



**+Bruce Lunde**  I did test the bronze fitting and they fit perfectly to me. A bit stiff exactly like I would want them. Probably no need for any glue.



I ordered some samples from [globalfsd.com](http://globalfsd.com) last week and hope to test them the same way this week. I still belive I would like to use the CF XT-20 for all the parts for my Herculien though. It simply prints great.


---
**Gústav K Gústavsson** *September 14, 2015 15:38*

The carbon fibre part shows some minor drooling of filament on the outside, could have been optimized with retraction setting maybe or by keeping all traveling movements inside the perimeter of the part. We will for sure experiment more when we have a printer which can stand this filament without ruining the nozzle. 


---
**Chris Brent** *September 14, 2015 18:09*

Wow that CF XT-20 looks amazing! Want!


---
**Brent ONeill** *September 14, 2015 23:36*

**+Gústav K Gústavsson**  Please excuse my lack of experience with FDM tech printing...you mention the break in the wood model by way of bending and twisting the rod clamp. So would the infill be the major determining factor of shear strength for a given material? I'm an SLA tech printer, potentially holding the top spot for the trophy for the longest  Herculien variant build. Really sweet CF XT20 print btw...kudos :)


---
**Gústav K Gústavsson** *September 15, 2015 00:53*

**+Brent ONeill** Infill would definitely play an important part in shear strength but also outer layer thickness, layer height and bonding between layers which is related to temperature of the print head and speed of printing. We could get a very good print from wood filament but I don't think we will use that in anything which needs strong parts. Great filament but I think it is to brittle for anything which needs strength. Just my feeling for this filament, not confirmed. Have to do more testing to confirm/reject my feelings


---
**Brent ONeill** *September 15, 2015 01:20*

**+Gústav K Gústavsson** , thanks for the details. So given the other factors you have mentioned are adjusted appropriately, would the infill be the variable to adjust giving the widest range of strengths available for a model with more infill volume than the outer layer volume.


---
**Erik Scott** *September 15, 2015 04:15*

Wow! That carbon fiber one looks almost perfect! I can't see any layers! I notice both have a little bit of derp around the hole for the smooth rod. Is this a result of the material causing problems or is it just a tuning issue?



I like the color variation in the wood one. Makes it look like it's actually wooden, even if the "grain" doesn't make total sense. 


---
**Gústav K Gústavsson** *September 15, 2015 13:39*

**+Erik Scott** well we probable could have minimized the derp as you call it. Probable retraction issue but we were basically trying all kinds of materials just on basic settings to find one which look promising and are candidates for further experimentation. But yes we have been having issues with the Ultimaker II recently, stringing between parts and such. Probable happened after firmware update? Haven't had time to address but will get to it when we reach tuning stages in experimentation. And we are NOT going to fine tune carbon fibre on poor Ultimaker II we have access to, it will ruin the nozzle rapidly.



(never heard that word derp before in this context but doesn't it mean meaningless, foolish or nonsense basically? Sorry English lesson 101 ;-))



**+Brent ONeill** I would say that more infill gives you stronger parts. But 100% infill gives you also much heavier part, much more filament use and much longer print time.


---
**Erik Scott** *September 15, 2015 13:44*

Ah, thanks for the info. Yeah, "derp" is more internet slang than anything; far from essential English. But yes, you're on the right track. 


---
**Gústav K Gústavsson** *September 15, 2015 13:44*

**+Brent ONeill** and I forgot, 100% infill would probable lead to more warping issues and/or the part getting unstuck from the print bed during printing resulting in a ruined print.


---
**Brent ONeill** *September 15, 2015 21:09*

**+Gústav K Gústavsson** ahhha, I see now why you mention adjusting the other variables and leaving the infill at settings that play well in concert. SLA tech is a different animal, on the surface, one might think there are limited methods and/or variables involved printing a model using SLA tech, but there are a ton of variables to consider, which can only be realized after years of experience. Yes, usually the software tuned for an SLA printer can print a model with default settings, but if you rely on defaults, there will be only the simplest of models printed reliably. Thanks muchly for your time **+Gústav K Gústavsson** , I really appreciate the lesson.


---
**Gústav K Gústavsson** *September 22, 2015 18:08*

Ready for a coconut filament pictures? Beautiful finish, maybe rival to carbon fiber? Or second place? Who knows.... And other filament pictures? Coming soon. If **+Eirikur Sigbjörnsson** keeps getting samples of filament (at least 3-4 waiting for a test run!!!) I will try to send pictures of our test prints ;-)


---
**Chris Brent** *October 07, 2015 16:28*

Any updates? :)


---
**Gústav K Gústavsson** *October 07, 2015 17:54*

Well not really, those exotic filaments are not nice to the Ultimaker ptfe insulator and 0.4 mm brass nozzle. Wear them down rapidly and clogs the nozzle. We should have access to Coblebot soon, have to assemble and tune so it will be ready to print Herculien variant. If we start with a hardened steel nozzle on the Coblebot then we will do some more tests I'm sure ;-)


---
**Gústav K Gústavsson** *October 07, 2015 17:56*

But I think we have some test prints from exotic filament, still laying around which I haven't shown, will check tomorrow and post...


---
*Imported from [Google+](https://plus.google.com/+GústavKGústavsson/posts/b7h6p6vArFL) &mdash; content and formatting may not be reliable*
