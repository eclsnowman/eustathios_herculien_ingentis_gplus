---
layout: post
title: "Would anyone be able to provide the mounting hole dimensions for the Azteeg X5 Mini V3?"
date: August 10, 2016 17:56
category: "Discussion"
author: Sean B
---
Would anyone be able to provide the mounting hole dimensions for the Azteeg X5 Mini V3?  I would like to build upon Eric's recently redesigned mount and allow it to be mounted directly to the 2020 rail rather than the bottom surface of the printer.  The Raspberry pi would also be built in, to make a stacked mount.





**Sean B**

---
---
**Eric Lien** *August 10, 2016 19:24*

Will This Work?



[http://imgur.com/0P06KCt](http://imgur.com/0P06KCt)



[http://imgur.com/B2DCCNA](http://imgur.com/B2DCCNA)


---
**Eric Lien** *August 10, 2016 19:33*

It is weird, I don't see this post show up under the community? Just here when I view it from my notifications.﻿



Edit: Oh, it shows up now


---
**Sean B** *August 10, 2016 19:51*

That's perfect, thanks!  I was also have trouble with the post even showing up.  I posted it and it never appeared so I wasn't sure if it went through, google + must be glitchy today.


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/g7F7aE3x21c) &mdash; content and formatting may not be reliable*
