---
layout: post
title: "Man do I love this inland PETG from Micro center (rebranded Esun I believe)"
date: May 18, 2015 23:50
category: "Show and Tell"
author: Eric Lien
---
Man do I love this inland PETG from Micro center (rebranded Esun I believe). It prints so nice, no curl, no shrink, great diameter consistency, sticks like crazy to hairspray treated glass at 75C, layer adhesion is unbelievable. I just wish there were more colors. For $25 this stuff is hard to beat.



![images/776479ea78a3c58ca879765a32416569.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/776479ea78a3c58ca879765a32416569.jpeg)
![images/43aa4ae35d002e1be73c3b4fe705ba03.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/43aa4ae35d002e1be73c3b4fe705ba03.jpeg)
![images/83a37a28acc2396e1604e970ef31898a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/83a37a28acc2396e1604e970ef31898a.jpeg)

**Eric Lien**

---
---
**Erik Scott** *May 19, 2015 00:01*

Is that the beginning of a VR headset I see there?


---
**Chris Brent** *May 19, 2015 00:08*

Wow, some nice looking prints!


---
**Mykyta Yurtyn** *May 19, 2015 00:12*

I've reprinted most of my Eustathios parts, except carriage, with inland PETG (one of the reasons build is taking so much longer...).

Do you think I can use it for carriage too? or is it too hot there for PETG?


---
**Eric Lien** *May 19, 2015 00:25*

**+Mykyta Yurtyn** I think PETG would work great on the carriage.


---
**Eric Lien** *May 19, 2015 00:26*

**+Erik Scott** yup, sure is. I am going to have to make some mods for my S4. It was designed for the one+ so camera location is off.


---
**Erik Scott** *May 19, 2015 00:29*

I'm building a flight sim cockpit, and this VR stuff interests me. The problem is, all VR headsets completely cut off your vision to what's around you. I want to make a headset that kinda works like bifocals, with windows in the bottom so you can look down at the physical cockpit, while also having the 3d VR environment when you look forward and up. I haven't had the time to think it through though. 



Anyway, fantastic prints! 


---
**Oliver Seiler** *May 19, 2015 07:18*

What nozzle size and print speed are you using?

I've done some prints with t-glase which should be essentially very similar and love it, but it still requires a fair bit of fine tuning. The main issue I'm having is that it oozes quite a bit on every retraction and leave blobs of filament behind.


---
**Isaac Arciaga** *May 19, 2015 10:57*

Looks like you have it dialed in. Looking good! Did you figure out your pet+ issues?


---
**Eric Lien** *May 19, 2015 11:16*

**+Isaac Arciaga** nope. Pet+ still gives me issues.


---
**Eric Lien** *May 19, 2015 11:20*

**+Oliver Seiler** I was using 60mm/s print speed, outermost perimeters at 60% speed, bed 75C, hotend 255C, retract 7.25mm at 65mm/s. No real blobs or strings. Just a few tiny hairs.


---
**Walter Hsiao** *May 19, 2015 17:55*

According to intservo, they will be getting opaque black, opaque white, semi-transparent green and semi-transparent orange eSun PETG.  I'm not sure when/if Microcenter will stock it though.


---
**Jason Perkes** *May 19, 2015 19:57*

Ive been enjoying a couple rolls of uPET here recently, the clear is very versatile and loves a permanent marker.


---
**Isaac Arciaga** *May 19, 2015 20:40*

**+Walter Hsiao** did Intservo indicate how soon they will be releasing the other colors?


---
**Walter Hsiao** *May 19, 2015 20:58*

Here's the page where they mention it: [http://www.intservo.com/products/esun-petg-filament-1-75mm-1kg-spool?variant=1266770711](http://www.intservo.com/products/esun-petg-filament-1-75mm-1kg-spool?variant=1266770711)



It says May, and I think they had the white and black in stock for a while, but they're out now.  When they initially introduced their PETG, they air freighted some spools early, and had the rest of the shipment arrive by boat so they may have done the same thing this time.  I'm curious how their opaque filaments behave compared to the transparent ones but haven't ordered any.


---
**Oliver Seiler** *May 19, 2015 23:16*

Thanks **+Eric Lien**​, may I also ask what nozzle diameter? 


---
**Eric Lien** *May 19, 2015 23:26*

**+Oliver Seiler** 0.4mm on e3d v6


---
**Isaac Arciaga** *May 20, 2015 00:24*

**+Walter Hsiao**​ thank you. I know someone who uses opaque petg from Alchement. No difference other than his only issue was it didn't stick to PEI as well as the translucents. 


---
**Nuker Bot (NukerBot 3D Printing)** *July 15, 2015 03:18*

I am loving this stuff. I bought the magenta a few months ago from microcenter and I just recently started using it. I am really happy with the low warping and the incredible strength compared to ABS and PLA. Layer bonding once you have it set to the right temp, for me is around 245c on my E3D v5, it is almost like it was once piece. I am thinking of using only PETG but they don't have too many colors to choose from so that might have to wait. lol


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/SuMaWvoBHHm) &mdash; content and formatting may not be reliable*
