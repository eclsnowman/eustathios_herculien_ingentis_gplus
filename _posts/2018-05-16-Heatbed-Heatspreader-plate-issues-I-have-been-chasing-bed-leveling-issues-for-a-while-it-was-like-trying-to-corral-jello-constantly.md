---
layout: post
title: "Heatbed - Heatspreader plate issues- I have been chasing bed leveling issues for a while, it was like trying to corral jello constantly..."
date: May 16, 2018 16:57
category: "Discussion"
author: Dennis P
---
Heatbed - Heatspreader plate issues- 



I have been chasing bed leveling issues for a while, it was like trying to corral jello constantly...  I even broke down and bought a bed leveling sensor (different adventure). I took of my heatbed and saw my spring keepers had mushroomed and fused to the springs. I had designed a collar with a hex recess to center the spring on the bed bolt. I have some thing similar on my i3 clone, liked it and figured I would put them on the the Eustathios. 



I run the bed at 80-85 for PETG, these are PETG. I am surprised that they deformed like this. In engineering speak, we call this creep - 'plastic flow under load creating permanent deformation', meaning even under lower than design loads, the material by its nature will set into a deformed condition.  Glass does this too.  I think this was the source of me bed always being out of level. I think I am going to come up with metal spring keepers somehow. 



I also insulated the bed by taking some 1" vinyl faced fiberglass pipe  insulation tape strips and sticking it to a piece of cardboard and slipped it between the frame and the heatbed. Some packing tape prettied up the edges and keeps the fiberglass encapsulated. 



![images/fcf65af653191be66a4894af4afafcfe.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fcf65af653191be66a4894af4afafcfe.jpeg)



**Dennis P**

---
---
**Jeff DeMaagd** *May 16, 2018 20:17*

The heat deflection temperature of PETG is about 70°C give or take. Heat conducted through the springs and radiated from the bed was probably enough.


---
**Dennis P** *May 28, 2018 03:51*

ah, that is what did it then.. on the Di3, I have fibre washers between things. I bet those are  insulating things thermally.


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/Lv7h1e5CiUf) &mdash; content and formatting may not be reliable*
