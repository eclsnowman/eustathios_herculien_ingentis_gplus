---
layout: post
title: "Stepper selection time. Comparing the 2.0A Nema 17 at SMW3D Pn-1705HS200A ( ) which has more torque vs the 1.5A motors at Robotdigg (as listed on the BOM)"
date: May 28, 2016 23:49
category: "Discussion"
author: jerryflyguy
---
Stepper selection time. Comparing the 2.0A Nema 17 at SMW3D Pn-1705HS200A ( [http://smw3d.com/nema-stepper-motors/](http://smw3d.com/nema-stepper-motors/)) which has more torque vs the 1.5A motors at Robotdigg (as listed on the BOM). 



It appears that the 2 amp motors have higher torque but I seem to recall that as you get to higher the torque level motors within the same motor frame size, eventually you reached a point where they may have higher holding torque but produce less 'power' due to rpm losses. 



Wondering what people's opinions are between these two motors, as it applies to the best option for a Eustathios V2? 





**jerryflyguy**

---
---
**Eric Lien** *May 29, 2016 00:43*

Both are great choices. I have never run the smw3d ones. But knowing **+Brandon Satterfield**​ he sells nothing but the best. I ran the Robotdigg ones because I needed more torque for HercuLien, and liked that they had modular wiring. Plus I was making an order at Robotdigg anyway at the time.


---
**Tomek Brzezinski** *May 29, 2016 00:57*

Good recollection on rpm current loses over higher speeds. Rule of thumb I've used for other applications is 32*squart root(inductance in mH) should be your driving voltage. The issue with higher inductance motors at higher speeds is inability to drive the current into the motors. Most repraps use 35-45V capable drivers at measly 12V. If you can drive your motor at a higher voltage you can be ok. Compare the voltage*squartroot(inductance) of expected motor and expected driving voltage to whatever you can use.


---
**Brandon Satterfield** *May 29, 2016 13:00*

These motors were actually selected for the hurculien build that I have never had the opportunity to make. 



Sound advice in this post. The torque does of course exponentially drop off with RPM, that's inherent. These have a little more ump capability (potential) as I knew the hurculien called out for Roy's drivers which I would assume would be closer to rated than others in the market. 






---
**jerryflyguy** *May 29, 2016 15:21*

**+Brandon Satterfield** thanks Brandon, it's great to have suppliers that are so helpful as to post on the forums like this! 


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/2ZPWEpGj5fi) &mdash; content and formatting may not be reliable*
