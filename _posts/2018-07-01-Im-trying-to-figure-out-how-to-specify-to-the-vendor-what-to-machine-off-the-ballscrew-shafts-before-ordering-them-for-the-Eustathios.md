---
layout: post
title: "I'm trying to figure out how to specify to the vendor what to machine off the ballscrew shafts before ordering them for the Eustathios"
date: July 01, 2018 22:21
category: "Discussion"
author: Don Barthel
---
I'm trying to figure out how to specify to the vendor what to machine off the ballscrew shafts before ordering them for the Eustathios. This is but one of dozens of questions I have but I don't want to clog up the forum because the questions will come one at a time. 



Then I got the bright idea to just print the "Z Axis Leadscrew Support V2" and measure where the ballscrew shaft inserts into it (also taking the "608 skate bearing" into account). But when I loaded the STL file into Slic3r there seems to be extraneous stray 'pads' (thin circles) attached that don't look right. Are those pads expected? Please see attached screen shot.



[Supplementary question: the holes don't seem to go through the part. Is that expected? Will I be drilling them through? I don't mind doing post processing of parts but that seems unusual in this case.]

![images/e9f9b4dfdfb7d29af95cb0a3452c62cc.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e9f9b4dfdfb7d29af95cb0a3452c62cc.png)



**Don Barthel**

---
---
**Eric Lien** *July 02, 2018 05:00*

Part is meant to be printed upside down from how it is shown. The pads are to help the parts not lift at the corners (parts were originally all printed in ABS on mine, and these lillypads help hold the part on the bed and fight warping without going to a full brim)



Holes don't go all the way through so you can print it without support. Its call a bridge diaphragm and lets the center perimeters of the hole have a diaphragm to print ontop of.




---
**Eric Lien** *July 02, 2018 05:02*

Ballscrew dimensions for the ones on the spider v2 are in this listing:



[http://www.aliexpress.com/store/product/Best-quality-1pc-Ball-screw-SFU1204-L425mm-1pc-RM1204-Ballscrew-Ballnut-standard-processing-for-CNC-BK10/920371_32592258963.html](http://www.aliexpress.com/store/product/Best-quality-1pc-Ball-screw-SFU1204-L425mm-1pc-RM1204-Ballscrew-Ballnut-standard-processing-for-CNC-BK10/920371_32592258963.html)

[aliexpress.com - Best quality 1pc Ball screw SFU1204 - L425mm + 1pc RM1204 Ballscrew Ballnut for 12mm screw rail](http://www.aliexpress.com/store/product/Best-quality-1pc-Ball-screw-SFU1204-L425mm-1pc-RM1204-Ballscrew-Ballnut-standard-processing-for-CNC-BK10/920371_32592258963.html)


---
**Eric Lien** *July 02, 2018 05:03*

You can read more about walters adding the ballscrews to his build here:



[http://thrinter.com/eustathios-initial-build/](http://thrinter.com/eustathios-initial-build/)


---
**Eric Lien** *July 02, 2018 06:35*

Also please don't worry about clogging things up in the community. Questions are always welcome. And trust me that if you ask it... someone before you has wondered the same thing and perhaps not asked. And someone after you will wonder as well. If you don't ask the future builders might not find the answer within these posts. So ask away. 


---
*Imported from [Google+](https://plus.google.com/+DonBarthel/posts/8w8i8EojbS6) &mdash; content and formatting may not be reliable*
