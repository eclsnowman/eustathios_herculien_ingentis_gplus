---
layout: post
title: "So I think I've talked about late night dumb moves recently, and I think I just pulled another"
date: November 26, 2016 04:18
category: "Discussion"
author: jerryflyguy
---
So I think I've talked about late night dumb moves recently, and I think I just pulled another. 



I grounded the two leads for my part cooling fan while things were powered up. I tried testing them afterwards and it appears as though no power is coming out of that pair. Would dead shorting the part cooling pins, burn out a component on the Azteeg x5v3? 





**jerryflyguy**

---
---
**Eric Lien** *November 26, 2016 05:06*

Yeah, probably killed the small FET for those pins. It is a low current FET. So a dead short would overdrive it.


---
**Ray Kholodovsky (Cohesion3D)** *November 26, 2016 06:21*

Got a hot air rework station? Feel like doing some rework? 


---
**jerryflyguy** *November 26, 2016 15:49*

**+Ray Kholodovsky** sorry Ray, I don't have the equipment (or the ability, or the parts) to fix stuff at this level.. stupid brain fart on my part. 


---
**jerryflyguy** *November 26, 2016 15:49*

Might just hard wire it into the hot end cooling fan and run them together? Or will that pull too much current? 






---
**jerryflyguy** *November 26, 2016 16:24*

**+Ray Kholodovsky** do you have a description of which FET would need replaced? I can have a look at least and see if I dare attempt to fix it. 


---
**Ray Kholodovsky (Cohesion3D)** *November 26, 2016 16:27*

Hotend cooling fan should be always on, tapping it into the part cooling fan is a bad idea. 

I don't have any Azteeg boards and the sources for that one aren't available so there isn't any information I can work off of. 


---
**jerryflyguy** *November 26, 2016 16:29*

**+Ray Kholodovsky** so then go buy a different board ? 


---
**Ray Kholodovsky (Cohesion3D)** *November 26, 2016 16:34*

Wait I got my fans mixed up. Is your hotend fan always on 12v?  

You do want to control the part cooling fan as it should be off for first layer and varied by the slicer. 



Anyways if you're looking at new boards check out what we just launched at [cohesion3d.com](http://cohesion3d.com)



I think the Mini should compare fairly nicely.  I'll be happy to answer any further questions. [cohesion3d.com - Cohesion3D: Powerful Motion Control](http://cohesion3d.com)


---
**jerryflyguy** *November 26, 2016 16:37*

**+Ray Kholodovsky** I'm running 24v, my hot end fan is always on.  I've been running the part cooling full time as well with fine results. 



For now I think I'll just connect it to the 24v supply which will give me cooling at least. Hot end fan works fine. 



Sorry Ray I thought you were the guy who produced/sold the Azteeg stuff. 


---
**Eric Lien** *November 26, 2016 19:06*

Ray, he could steal some extra pins that are open, map part cooling to those, and use your external MOSFET board if that's ready for sale?


---
**Eric Lien** *November 26, 2016 19:07*

**+jerryflyguy**​ perhaps you can contact **+Roy Cortes**​ at Panucatt and see if he can do a board repair for a price?


---
**jerryflyguy** *November 26, 2016 19:14*

**+Eric Lien** yeah, I'll have to log in and put a support request in through their site. If **+Ray Kholodovsky** has an external mosfet board that could be made to work. I know there are a couple free pins on one of the EXP headers for additional use. 



My plan had been to buy one of the Azteeg boards which would allow me to run multiple extruders (eventually; my own Christmas present?) but don't have the pennies right now. 



I've hard wired the fan to the PS for now and just print the first couple layers much higher hot end temps than normal to help with adhesion.. so far it's working. 



Cannot believe I did this, normally I'm pretty aware of what I'm doing but in this instance.. complete brain fart. 🙄


---
**Ray Kholodovsky (Cohesion3D)** *November 26, 2016 19:20*

Sure thing. I'll get more info about that MOSFET board up ASAP. 


---
**Ray Kholodovsky (Cohesion3D)** *November 26, 2016 19:21*

It's Ray and Roy, which I find an interesting turn of events. 

Panucatt Roy

Cohesion3D Ray


---
**jerryflyguy** *November 26, 2016 19:21*

**+Ray Kholodovsky** thanks. I'm terrible with names.. sorry 😬


---
**Roy Cortes** *November 26, 2016 20:06*

**+jerryflyguy** This is Roy(Panucatt). Happened to me a couple of times already, I now remember to power down for anything I connect/disconnect unless if Im deliberately testing to destroy :-)



You can send it in for repair (repairs are always free) or if you are handy with the soldering iron I can send you the replacement Mosfet.


---
**jerryflyguy** *November 26, 2016 22:22*

**+Roy Cortes** thanks very much for the offer. Can you send me the details of what needs to be changed? I've fixed boards before but only in the simplest of terms. I'll have a look and see if I dare try or not 🤓



Thanks again Roy!!


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/9AAu4CXzqgv) &mdash; content and formatting may not be reliable*
