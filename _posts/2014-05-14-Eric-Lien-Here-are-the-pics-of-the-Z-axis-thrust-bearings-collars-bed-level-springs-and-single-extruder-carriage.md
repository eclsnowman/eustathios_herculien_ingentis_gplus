---
layout: post
title: "Eric Lien , Here are the pics of the Z-axis thrust bearings/collars, bed level springs, and single-extruder carriage"
date: May 14, 2014 03:18
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
**+Eric Lien**, Here are the pics of the #Eustathios Z-axis thrust bearings/collars, bed level springs, and single-extruder carriage.  Keep us posted as you finalize your build!



![images/0a8afd409ec73a1e38fb9d7f6a440e20.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0a8afd409ec73a1e38fb9d7f6a440e20.jpeg)
![images/8c38d3a684bc4b753d21aa42ed97c95e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8c38d3a684bc4b753d21aa42ed97c95e.jpeg)
![images/fc50ab8ee9ff80b039241221fc438dbf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fc50ab8ee9ff80b039241221fc438dbf.jpeg)
![images/f09d5bfd62aed42c0647020ee726e9cb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f09d5bfd62aed42c0647020ee726e9cb.jpeg)

**Jason Smith (Birds Of Paradise FPV)**

---
---
**Eric Lien** *May 14, 2014 03:54*

Thanks. Do you feel the lm8uu travel smoother? Did the change affect the carriage rigidity? I know the bushing version is really nice in this respect.﻿


---
**Jason Smith (Birds Of Paradise FPV)** *May 14, 2014 04:04*

I think the lm8uu may have slightly less friction when traveling, and may allow for a slightly more compact design. However, I feel that both designs are very rigid, as there is no noticeable slop in the movement of the carriage for either. 


---
**Tim Rastall** *May 16, 2014 08:07*

I know this is probably a result of using sketchup but I really like the blockiness of this one :) 


---
**Eric Lien** *May 16, 2014 12:07*

The new carriage must be printed with support correct?


---
**Jason Smith (Birds Of Paradise FPV)** *May 16, 2014 17:09*

Yes, it's a result of sketchup and laziness. It requires support. I would recommend printing it with the rear as the base. I also just added a slightly improved version of the carriage to github and youmagine. The duct for the rear fan has been improved to prevent blowing directly on the hotend, and I've added additional channels for wiring. I also increase the size of the holes for the 8mm shafts to add additional clearance. It is untested as of yet, however. Good luck. Let me know if you have any issues. 


---
**Jason Smith (Birds Of Paradise FPV)** *May 16, 2014 17:14*

Also, it looks like the v2.1 carriage stl I uploaded has a few issues, so it may need to be cleaned up before printing.  I have to run now, but I'll try to take a look at it later this weekend if I get a chance.


---
**Jason Barnett** *August 06, 2014 04:47*

**+Jason Smith** Would you mind sharing the source file for the carriage? I really like the design but need to modify it a bit to work with my printer.


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/h9Ss6vySZaD) &mdash; content and formatting may not be reliable*
