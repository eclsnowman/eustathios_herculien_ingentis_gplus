---
layout: post
title: "Hey all. So what is the deal with Ingentis vs Eustathios?"
date: October 18, 2014 18:23
category: "Discussion"
author: Shachar Weis
---
Hey all. So what is the deal with Ingentis vs Eustathios? I'm thinking of taking apart my Delta printer and building a Cartesian. Any tips on which model to choose? Which is more mature/robust? 



I already have a Ramps 1.4, 5x 17 Nema steppers and an E3D v5 hotend.





**Shachar Weis**

---
---
**Franco Ponticelli** *October 18, 2014 18:51*

What went wrong with the delta?


---
**Jason Smith (Birds Of Paradise FPV)** *October 18, 2014 18:57*

The Ingentis is the original spectra line xy and belt z axis machine. The eustathios was designed later, based heavily on the ingentis, and uses belts for xy, dual lead screws for the z, and houses the coordinate drive motors underneath the build chamber. The Eustathios is more expensive, but is potentially more precise due to the drive mechanisms used. That said, no 2 single builds of either machine are identical, and there are pros and cons to any particular build. 


---
**Shachar Weis** *October 18, 2014 19:25*

Hmm, I never liked spectra lines. The delta is broken (it tried to push the head through the plate for some unexplained reason, and both broke). I'm tried of it, of delta calibration hell and deltas in general :)


---
**Eric Lien** *October 18, 2014 22:06*

Eustathios is my choice. But then again I am biased, its what I built. **+Jason Smith** did a great job designing it.



If you are feeling ambitious, you could build #HercuLien. I should have the github cleaned up and finalized soon for release.


---
**Shachar Weis** *October 19, 2014 13:44*

Thanks all.


---
**Mike Miller** *October 19, 2014 16:31*

I liked having everything inside the build envelope....I then also liked having the motors in the base to simplify wiring. You have some issues with long runs if you're using belts, but they're all easy to overcome. 


---
**Tim Rastall** *October 20, 2014 01:22*

Eustanathios is the more mature design. I've upgraded my v1.0 original ingentis to belts.


---
**Erik Scott** *October 22, 2014 22:39*

I quite like the Eustathios. I haven't had any issues with the standard design.



What's nice is you can mix and match bits from design to design, so there aren't really 2 printers to choose from. You can do the Xand Y axes with belts or spectra line, and you can do the z-axis with either lead screws or belts as well. Additionally, The Eustathios doesn't specify an extruder, so you can either take the one from the Ingentis, or find your own. I wanted to be able to print water-soluble PVA at some point, so I went with a dual extruder and a custom dual hot-end carriage. 



So, basically, the Ingentis and Eustathious are just 2 of many combinations, and their parts and designs are compatible. 


---
**Mike Miller** *October 24, 2014 11:27*

Your experiences mirror mine. Granted it was my first printer ever, and I had no one local with experience to hold my hand on getting it printing...buy my delta was hell to calibrate and never really produced the quality of prints  #IGentUS  made on day one. 


---
*Imported from [Google+](https://plus.google.com/117479393665221551027/posts/AXYb56zuMQW) &mdash; content and formatting may not be reliable*
