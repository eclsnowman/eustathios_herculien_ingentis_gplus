---
layout: post
title: "Very well packed package from +Mitsumi arrived tonight, I wish my shop was not @ 18 degrees tonight"
date: February 26, 2015 23:42
category: "Show and Tell"
author: Bruce Lunde
---
Very well packed package from +Mitsumi  arrived tonight, I wish my shop was not @ 18 degrees tonight.

![images/5cb1825c6801fb6ec28c9bb912d4f339.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5cb1825c6801fb6ec28c9bb912d4f339.jpeg)



**Bruce Lunde**

---
---
**Dat Chu** *February 26, 2015 23:44*

Misumi shipping cost / packaging ratio is extremely good. It was a box in a box contains super wrapped up stuff. 


---
**Eric Lien** *February 27, 2015 01:18*

I wish you guys knew how excited I get each time I see a new HercuLien build starting... And also how nervous I get that it won't live up to expectations.



Bruce as you build feel free to hit me up with questions.


---
**Bruce Lunde** *February 27, 2015 01:22*

**+Eric Lien** If you saw my first attempt to convert a CNC to 3D printer, you would not be nervous!  SInce then, I have studied several designs and many production models before deciding on this one.  I feel like I know exactly what I am getting. I am ready to build!**+Dat Chu** You are dead-on correct, excellent packing, well worth the cost.


---
**Robert Burns** *February 27, 2015 14:53*

HNKK5-5 and HNKK5-3 are the cheaper packs


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/8eMbVB3D3ni) &mdash; content and formatting may not be reliable*
