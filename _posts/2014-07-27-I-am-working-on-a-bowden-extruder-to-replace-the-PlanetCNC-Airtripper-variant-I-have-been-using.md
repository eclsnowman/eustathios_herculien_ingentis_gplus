---
layout: post
title: "I am working on a bowden extruder to replace the PlanetCNC Airtripper variant I have been using"
date: July 27, 2014 03:30
category: "Deviations from Norm"
author: Eric Lien
---
I am working on a bowden extruder to replace the PlanetCNC Airtripper variant I have been using.



I wanted to be able to clean the gear teeth while filament is loaded, so I cut away from the back side support near the hobb gear. I also wanted to be able to run either Nema 17, or Nema 17 with gear reduction which is a different bolt pattern. I wanted to support the filament as close as possible to the contact between the hobb gear and the bearing. Lastly I wanted  to replace the bowden tube pneumatic tube fittings with another retention method. I cannot even remove the tube from the fitting on my current extruder. I have only seen the 4mm nut around the bowden tube trick before online... but I have never done it. Can anyone with experience confirm it works well?



Also I have some updated screenshots of the modeling progress on the printer assembly.  



![images/77eb8e74006da4d365c0cec37862d460.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/77eb8e74006da4d365c0cec37862d460.png)
![images/46567e63c7bea87ae1be6c5b382e3e68.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/46567e63c7bea87ae1be6c5b382e3e68.png)
![images/02952f2af28d675e43bbdebe8ff712f7.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/02952f2af28d675e43bbdebe8ff712f7.png)
![images/ce44e9ddac570afda0407af830586c5a.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ce44e9ddac570afda0407af830586c5a.png)
![images/905e4f22825ae14d7f7b15410d761fcd.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/905e4f22825ae14d7f7b15410d761fcd.png)
![images/117b82863cbcc7da2ebc3dfffb39f87d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/117b82863cbcc7da2ebc3dfffb39f87d.png)
![images/e041622da4f4522f9e8b9f9ead42b5f3.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e041622da4f4522f9e8b9f9ead42b5f3.png)
![images/ff50d9cdb828543674386a963e3a9c57.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ff50d9cdb828543674386a963e3a9c57.png)

**Eric Lien**

---
---
**Joe Spanier** *July 27, 2014 03:37*

I've been using it on my air tripper since December. Ive never had the tube pull out. But you will need to drill the bowden tube back to 2mm after you thread it in. 



Why are you dropping the pneumatic fitting?


---
**Eric Lien** *July 27, 2014 03:49*

**+Joe Spanier** I can bring the tube past the retention point (I like how the E3D v6 does this to put the Bowden tube right at the heat break).



And I have never had it pull out on my current setup. But due to pressure of the filament biting the tube into the fitting, my tube is the veritable sword from the stone to ever get it out of the fitting at my airtripper outlet.﻿


---
**Eric Lien** *July 27, 2014 03:54*

Do they have pass through 4mm tube fittings I could source?


---
**Joe Spanier** *July 27, 2014 04:05*

haha yea they really do bite into the tubes after a while.I usually have to push it in a few mm and then yank it out. I get some 4mm fittings from fastenal and drill them out. I can send you a couple to try if you want. 


---
**Eric Lien** *July 27, 2014 04:09*

**+Joe Spanier** what thread do you use?


---
**Joe Spanier** *July 27, 2014 04:10*

[http://www.fastenal.com/web/products/details/0418576?searchMode=productSearch&zipcode=&filterByStore=&filterByVendingMachine=](http://www.fastenal.com/web/products/details/0418576?searchMode=productSearch&zipcode=&filterByStore=&filterByVendingMachine=)



5/32 is 3.9 something so they bite nicely


---
**Joe Spanier** *July 27, 2014 04:11*

1/8 bsp


---
**Eric Lien** *July 27, 2014 04:38*

The only problem I see is I would have to fatten up the area to accept the thread. I have some M5 thread ones that are OK. But they don't allow the tube to pass through, and they don't have much thread bite into the printed part:



BSP 1/8 Thread:

[http://i.imgur.com/fzcUAnN.png](http://i.imgur.com/fzcUAnN.png)


---
**Eric Lien** *July 27, 2014 11:12*

**+Shauki Bagdadi** I will setup 3mm as a configuration in solidworks.


---
**Jean-Francois Couture** *July 27, 2014 13:01*

I also drill out the 4mm to 1/8" push to connect. Just use a 4.5mm ( i tried a 4mm but it a bit to tight) steel drill bit and the hex design under the fitting.



I find that using airtripper's method of holding the fitting is really easy to remove afterward.


---
**Eric Lien** *July 27, 2014 13:41*

**+Shauki Bagdadi** sure. You have a solid model?


---
**Eric Lien** *July 27, 2014 14:04*

aren't there stepped shoulders on the edge of the cylinder? or is that not a part of the flint wheel, just a part of the lighter assembly?


---
**Eric Lien** *July 27, 2014 15:35*

**+Shauki Bagdadi** I am a smoker unfortunately. But in the states all they have is the safety lighters. They are higher on the side then in the center of the Flint wheel to allow for the safety cover.


---
**Eric Lien** *July 28, 2014 04:18*

FYI I just added pictures of the actual printed version. I need longer bolts. So for now I have cut down springs and the longest bolts I had. But even with that I am printing with it now, and it works great.


---
**Eric Lien** *July 28, 2014 05:26*

Links to the STL's and Solid files up on Youmagine:



[https://www.youmagine.com/designs/hercustruder](https://www.youmagine.com/designs/hercustruder)


---
**George Salgueiro** *August 09, 2014 05:24*

Just a question **+Eric Lien**, how do the z aluminum profile bars couples to the frame? 


---
**Eric Lien** *August 09, 2014 14:09*

**+George Salgueiro** 5mm button head screws slide into the z extrusion tslot. Then they are tightened through holes predrilled through the z extrusion into the end of the horizontal which was threaded. Also I may use a corner bracket on the underside if the horizontal extrusion. 


---
**Eric Lien** *August 09, 2014 15:43*

**+Shauki Bagdadi** I agree. Dual extrusion is a pain. I ditched it before already when my bot was corexy. If I doesn't work well this time I am over them for good and will reprint a single head carriage. Nice thing about having two printers. If at least one is working iteration is easier.



At least this time I have independent hot end adjustment in Z. ﻿


---
**Eric Lien** *August 09, 2014 15:46*

**+George Salgueiro** see the post I just put up about drilling the holes for access with an allen key to the button head bolt allen bolts from the outside of the frame.


---
**Eric Lien** *August 09, 2014 17:09*

**+Shauki Bagdadi** think how big of a bot you can build then :)﻿


---
**D Rob** *August 17, 2014 11:40*

how is the extruder working out? which hobbed pulley you using?


---
**Eric Lien** *August 17, 2014 12:37*

**+D Rob** I am using the 8mm I'd bore one from Trinity Labs on a 5:1 planetary stepper. Unfortunately you cannot get them anymore. 



But I also like the 1.75mm specific one from ultibots if you are using a standard nema. 


---
**D Rob** *August 17, 2014 19:53*

**+Eric Lien** thanks. I orderes some mk7's and mk8's off ebay. a bunch of each/ the mk8's are brass : / and the mk7's are stainless, I'll let you know how they work out. I'm in the process of building an army :)


---
**Eric Lien** *August 17, 2014 21:07*

**+D Rob** build farm? 


---
**D Rob** *August 17, 2014 21:12*

**+Eric Lien**  All shall be revealed in due course


---
**Joe Spanier** *August 17, 2014 21:28*

I'd be curious to know how the brass ones do too. I've had the teeth shred on steel hobbed bolts with PLA jamming so I've been hesitant of brass. 


---
**Eric Lien** *August 17, 2014 21:57*

**+D Rob** I've  been wondering what you've been up to. I look forward to seeing your super secret project. 


---
**D Rob** *August 18, 2014 06:58*

**+Shauki Bagdadi**  depending on the need at the time are they not the same ;)


---
**D Rob** *August 18, 2014 07:12*

**+Shauki Bagdadi**  I know how that is. I only have one up and running right now. My trusty mendel 90/I3 Hybrid. Too many projects, and not enough time. I work 2 full time jobs, and sleep four or five hours a night at best. That is when I get to steal some time to build, design and tinker for myself. 


---
**Daniel F** *August 19, 2014 15:17*

**+Eric Lien** any news on a 3mm version?


---
**Eric Lien** *August 19, 2014 18:11*

**+Shauki Bagdadi** I will try to configure it over the next few days. 


---
**Eric Lien** *September 25, 2014 15:26*

**+Shauki Bagdadi** I have run into under extrusion with high speeds using standard nema 17. They work most of the time just fine, but the planetary only costs a little more.



Yes I still like the design. Thought I am working with another member on something even better. It is his baby so I cannot discuss it. But it should prove to be the best extruder hands down once finalized.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/NjHUCzTAUMn) &mdash; content and formatting may not be reliable*
