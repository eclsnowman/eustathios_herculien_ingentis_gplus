---
layout: post
title: "My Eustathios Spider v2 Hercustruder Bowden tube was somehow stripped and got pulled out of the nut, and so I decided to change the nut trap to hold an embedded m4 lock nut at: ."
date: January 02, 2016 06:53
category: "Discussion"
author: Ted Huntington
---
My Eustathios Spider v2 Hercustruder Bowden tube was somehow stripped and got pulled out of the nut, and so I decided to change the nut trap to hold an embedded m4 lock nut at: [https://www.youmagine.com/designs/eustathios-spider-v2-adapted-hercustruder-bowden-tube-nut-trap](https://www.youmagine.com/designs/eustathios-spider-v2-adapted-hercustruder-bowden-tube-nut-trap). The M4 lock nut holds the Bowden tube more tightly, and in addition the nut cannot rotate freely. But definitely a pneumatic adapter would be best there- let me know if somebody has made that already. You may be able to just enlarge the hole, heat it with hot air, and then screw the pneumatic adapter in to make the threads in the plastic.



I'm loving this Eust-Spid-v2 (we need to make a shorter name). One thing I have found is that, although every print is different even at accel of 1000 m/s^2 some prints are still layer shifting (where the hot end gets moved out of alignment and the top of the print becomes shifted)- so I lowered my accel to only 800 :!,  and that seems to have solved the layer shifting problem. I realized that probably a person can set a very high velocity (I am printing at 80m/s) if the accel is limited, because the hot end will never reach that velocity- and only acceleration matters- in particular for small size prints. So I'm somewhat mystified by why the accel is lower than the 3000m/s^2 average, but on the other hand, my big 40x30cm Mendel requires no more than 400m/s^2 accel- otherwise nearly every print will be layer shifted.





**Ted Huntington**

---
---
**Eric Lien** *January 02, 2016 14:37*

I never run into this issue on HercuLien. But I do from time to time on my Eustathios. So my guess is... it is tied to smaller carriage size in comparison to HercuLien. So the spacing between the two bars can more easily cause a cantilever force which binds the bushings when the nozzle catches. The two easiest solutions would be to go to linear ball bearings on the carriage or make the carriage larger so the cantilever forces can't bind the bushings.


---
**Ted Huntington** *January 02, 2016 16:36*

**+Eric Lien**

Thanks for the advice. Linear bearings on the carriage would lower the cost too. I am thinking that it would be smart to change the carriage to have the hot end+fans detachable so changing hot end/fan configurations would just be a matter of 2 bolts, or even strong embedded magnets, because otherwise changing the hot end/fan configuration requires removing the carriage rods and realigning the carriage.



In terms of the lower accel, on the Eustathios many small prints are fine at high accels- it's only large (Z mostly) prints that cause layer shifting. On the Eustathios the need for slow accel seems to be correlated to print volume (in particular Z height), but the large Y-axis moving bed Mendel I built will shift layers no matter what size above 400m/s^2.


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/77DvKJe6T7Y) &mdash; content and formatting may not be reliable*
