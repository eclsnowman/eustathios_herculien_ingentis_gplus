---
layout: post
title: "Hello, I have a problem with my eustathios V2, the mouvement of my XY axis isnt smooth and I think my axis are not perpendicular"
date: April 30, 2017 12:48
category: "Discussion"
author: Lopez Tom
---
Hello, 

I have a problem with my eustathios V2, the mouvement of my XY axis isn’t smooth and I think my axis are not perpendicular. So my printed holes are not circular. How can i resolve that ? 

Thank's





**Lopez Tom**

---
---
**Zane Baird** *April 30, 2017 14:01*

In general, non-perpendicularity in the XY gantry will result in very tight binding of the axis and you'd have shuddering movement. If your holes are are not circular that is likely due to backlash from a loose pulley or belt. 



I would start by ensuring you have equal tension on the belts and then following along with this video that **+Eric Lien** made regarding alignment: [plus.google.com - I gave a tutorial today on how I align the gantry on a Cross Rod Gantry style...](https://plus.google.com/+EricLiensMind/posts/RFQNLLefxoc)


---
**Eric Lien** *April 30, 2017 14:06*

Can you provide some pictures?



If you are not properly aligned you will have binding which can create backlash on the direction change of an axis. 



Another cause can be belts that are too loose causing a similar backlash.



One more cause is if you forgot the 10mm shim washers between the pulleys and the corner bearings on the sides. If these are forgotten, the pulleys flange can rub on the corner bearings face causing drag.



And the last thing to check is the squaring and equilibrium of the assembly. Everything has error, even high precision machined parts in your car. Break-in, and balancing the degrees of freedom of the assembly to get the smoothest motion of the system is the tricky part. Starting with a very square frame, and tight tolerance printed parts is critical. But even with both of those nailed down, balancing the accumulated error of the parts for free motion is a matter of time, patience, and changing one variable at a time to assess if it makes things better or worse.



It can be time consuming to get right. But once you do, as I say to all new builders... "Once it is right, You will know :)"


---
**Lopez Tom** *May 02, 2017 07:43*

I can't post picture now because i have not my printer with [me.Is](http://me.Is) it possible that is come from the auto aligning bearings on the cross rods ?




---
*Imported from [Google+](https://plus.google.com/107612178038097709041/posts/5RfyNPNd1wX) &mdash; content and formatting may not be reliable*
