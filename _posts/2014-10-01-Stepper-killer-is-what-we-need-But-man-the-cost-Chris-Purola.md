---
layout: post
title: "Stepper killer is what we need!!! But man the cost Chris Purola"
date: October 01, 2014 23:34
category: "Discussion"
author: D Rob
---
[http://www.teknic.com/products/clearpath-brushless-dc-servo-motors/?kw=%2Bservo%20%2Bdriver&ad=27793234206&gclid=CJGz5bvOjMECFedzMgodCzgA_w](http://www.teknic.com/products/clearpath-brushless-dc-servo-motors/?kw=%2Bservo%20%2Bdriver&ad=27793234206&gclid=CJGz5bvOjMECFedzMgodCzgA_w)



Stepper killer is what we need!!! But man the cost

**+Chris Purola** 





**D Rob**

---
---
**Mike Miller** *October 02, 2014 00:25*

But they add "only" in front of the price. Thanks makes it okay, right?﻿


---
**Dale Dunn** *October 02, 2014 00:32*

Sweeet! But not cheap (compared to our NEMA 17 steppers). It looks like it would cost almost $800 for 3 axes.


---
**D Rob** *October 02, 2014 01:51*

Yeah but if one man can make it so can another. I want to scale it down to nema 17 or 11.


---
**Dale Dunn** *October 02, 2014 02:08*

Now there's some ambition!


---
**D Rob** *October 02, 2014 02:21*

firts step is finding the right brushless motors. then tearing one of these open so the electronics can be analyzed and lower rated componnts used. those things are nema 23 and way more powerful than any nema 23 stepper. definitely flies in the face of those arguments about not enough torque. Come on 245 people paypal me a dollar and i will do a tear down and shater the videos and pics. We can crack this as a team. Thats why open development has expanded 3d so far.


---
**Miguel Sánchez** *October 02, 2014 07:55*

Cheapest servos use brushed DC motors. While I do agree a brushless motor is preferred (longer life, quieter), I do not see that happening cost-wise. It is the electronics drive what gets more complicated if you go brushless. 


---
**John Driggers** *October 02, 2014 20:18*

If you are interested, a number of us have already wandered down this path - **+Steve Graber** is actually using these SDSK's on his Gigante delta design.  I'm using Gecko G320Xs on one of my CoreXY designs.  Both of us are just feeding step and dir from semi-traditional hardware and software - in my case from an Azteeg X5 running Smoothieware.



Cost aside, you are still missing a proper closed feedback loop, and the firmware to support it.  With the build in shaft encoders, you know if you motor missed a step, and correct for it - but you don't know if you lost a step anywhere else in your motion system.



The other part is proper acceleration and jerk calculation in this motion control system.  **+Alden Hart** and the tinyG folks are making progress, but when you look at some of the industrial (and patented!) solutions you get a good idea of where we could be.



Pretty much all of this means a move away from 8-Bit AVRs. 



I guess my point is that it's not just hardware we need - it's that hardware integrated with software as well.


---
**Miguel Sánchez** *October 02, 2014 21:03*

Want to know more about BLDC drives? [http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3231115/pdf/sensors-10-06901.pdf](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3231115/pdf/sensors-10-06901.pdf)


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/7Vd4XJyUuMo) &mdash; content and formatting may not be reliable*
