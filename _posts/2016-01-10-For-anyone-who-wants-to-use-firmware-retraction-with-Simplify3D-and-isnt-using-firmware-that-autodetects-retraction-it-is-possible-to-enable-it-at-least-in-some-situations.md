---
layout: post
title: "For anyone who wants to use firmware retraction with Simplify3D and isn't using firmware that autodetects retraction, it is possible to enable it, at least in some situations"
date: January 10, 2016 01:05
category: "Discussion"
author: Walter Hsiao
---
For anyone who wants to use firmware retraction with Simplify3D and isn't using firmware that autodetects retraction, it is possible to enable it, at least in some situations.  Using firmware retraction lets you configure separate retract and unretract speeds, and let you tweak the retraction settings on the fly with M207/M208.



I posted instructions on [http://thrinter.com](http://thrinter.com) on setting it up, but the short version is you can replace the conventional retraction settings with G10 / G11 using some post processing commands.  It should make it much easier to get your retraction settings dialed in vs the print and reprint method.

![images/53140d07e6006c3de739519368efed80.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/53140d07e6006c3de739519368efed80.png)



**Walter Hsiao**

---
---
**Ben Delarre** *January 10, 2016 01:11*

Is it recommended to turn this on for smoothieware? Could be very useful to tube retraction on the fly to help me iron out my last blobs. 


---
**Walter Hsiao** *January 10, 2016 01:16*

It should work fine with smoothieware, that's what I'm using too (though I'm using the edge version - a few months old, I'm not sure what the status of the main branch is).  I figure if you see the firmware retract settings in your config file, you're probably running with a version that supports it.


---
**Ben Delarre** *January 10, 2016 01:16*

Nice, thanks Walter I will give this a go!


---
**Walter Hsiao** *January 10, 2016 10:14*

It looks like in Marlin you may need to recompile with FWRETRACT enabled (uncomment #define FWRETRACT In Configuration_adv.h).  I haven't tried it yet, and at least in the version I'm looking at, that line has an //ONLY PARTIALLY TESTED comment, which isn't very confidence inspiring.


---
*Imported from [Google+](https://plus.google.com/+WalterHsiao/posts/Ri74TFzs4Ap) &mdash; content and formatting may not be reliable*
