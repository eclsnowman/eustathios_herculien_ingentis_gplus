---
layout: post
title: "This is Walter Hsiao part for goldmart 1204 ball screws printed in ABS"
date: April 28, 2016 17:21
category: "Build Logs"
author: Dimitrios Tzioutzias
---
This is **+Walter Hsiao** part for goldmart 1204 ball screws printed in ABS. Also a small video showing how the bushings are doing. As you can see the printer vibrates a lot so I have to lower acceleration from 9000. I am running break g code on a ramps just for testing the movement. Also there are some 2020 corners missing from the top part of the frame, maybe this contributes to vibration. Sorry for the carton paper beneath the printer but I haven't install the rubber legs yet. Unfortunately I only have purple ABS available for now :)




**Video content missing for image https://lh3.googleusercontent.com/-aGX-zpA96ZQ/VyJGoup-aAI/AAAAAAAAAWU/MZk5jMQBB5wxCgbuCd0sWBa3vmXHHpjRQ/s0/20160428_193713.mp4.gif**
![images/3cac99c399368e1af1d498c62e23399e.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3cac99c399368e1af1d498c62e23399e.gif)
![images/af0081129d410ab5589ee9bcdc5dc9c8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/af0081129d410ab5589ee9bcdc5dc9c8.jpeg)
![images/b133352a25355ff24380106bf02bbb43.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b133352a25355ff24380106bf02bbb43.jpeg)
![images/5f4c3dfba6c835a6029d46ce649645dd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5f4c3dfba6c835a6029d46ce649645dd.jpeg)

**Dimitrios Tzioutzias**

---
---
**Eric Lien** *April 28, 2016 18:35*

Yeah, I am down to 3000 on accel now. No real effect on print times. But much better performance on limiting vibration at corners. I need to update the github with newer configs.


---
*Imported from [Google+](https://plus.google.com/108355995361667474020/posts/crVw615r7uY) &mdash; content and formatting may not be reliable*
