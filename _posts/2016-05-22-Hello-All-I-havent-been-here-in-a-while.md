---
layout: post
title: "Hello All I haven't been here in a while"
date: May 22, 2016 06:18
category: "Discussion"
author: Gus Montoya
---
Hello All



I haven't been here in a while. I've been holding onto my Herculien water jet cut aluminum build plate with countersunk holes. I thought I would get into the fray again after selling my Eustathios spider. I've sold my car project, thus no more need for my plans of a herculien. I am offering my new build plate for $95.00 shipped. It's new, no dents etc. It's collecting dust in my closet. The money will go back into my school fund. I'm loosing money on this as is, so please be nice. Additionally I also have a new bondtech extruder new in box up for grabs. $110 takes it.





**Gus Montoya**

---
---
**Jeremy Marvin** *May 22, 2016 13:19*

Where are you located? What's the dimensions of the plate? 


---
**Gus Montoya** *May 22, 2016 22:07*

**+Jeremy Marvin** I'm located in California. The dimensions of the plate are as described in the build requirements of the BOM. 


---
**Gus Montoya** *May 22, 2016 22:09*

I'll have pictures up when I get home tomorrow at the latest. I'm away from home and won't be back till tomorrow.


---
**Gus Montoya** *May 25, 2016 07:03*

Sorry for the delay, pictures to come. Been stuck at work. Gotta love over time and a half :)


---
**Jim Stone** *May 29, 2016 01:38*

which version of the bondtech? the QR? what size filament? does it have the motor attached? shipping incl? im in Canada.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/TMTXook9Jct) &mdash; content and formatting may not be reliable*
