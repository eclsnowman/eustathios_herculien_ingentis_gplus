---
layout: post
title: "I need some help figuring out how to get my xy axis smooth"
date: April 09, 2015 04:12
category: "Discussion"
author: Ben Delarre
---
I need some help figuring out how to get my xy axis smooth.



Before putting on the belts the axis was reasonable. I could move in x or y without too much effort as long as I moved both ends of the axis together to avoid racking. 



I have hooked up the belts on the axis and the motors. I loosened the pulley set screws on each axis and tightened the belts then shifted the axis to one end so it was square and then locked the pulleys. 



Now it takes a lot of force to move the axis.  I can do it with one hand holding the central carriage but it's not exactly easy. Takes a fair force to move it,  once moving it feels reasonably smooth but still requires some force. Is this normal? Obviously there's a lot of friction from the belts and the motors,  I'm just not sure if it's too much or not. 



I have not yet hooked up the motors to my controller, that's a job for tomorrow night so I guess I will see if it moves smoothly then.



Any advice on getting this all optimal? Not added any oil yet, is there a best type? I have some silicone based stuff and some sewing machine oil. 





**Ben Delarre**

---
---
**Oliver Seiler** *April 09, 2015 05:55*

This might help

[https://plus.google.com/u/0/103716759551585317663/posts/LV9RBkZxYGp](https://plus.google.com/u/0/103716759551585317663/posts/LV9RBkZxYGp)


---
**Ben Delarre** *April 09, 2015 06:23*

**+Oliver Seiler** yeah I checked that.  Did the drill thing till the bearings were nice and smooth before mounting the shafts into the frame. Its only after I had the whole thing together did it start to get harder to move.



I also used the alignment pieces to get the axes all at the same level.


---
**Eric Lien** *April 09, 2015 11:18*

Unhook the belts from the motors. If it is still not smooth... Then you are slightly out and the system requires realignment. In my opinion this is the most painful part of the build. When everything is right you will know.


---
**Eric Lien** *April 09, 2015 11:19*

Also run the break in gcode. It really helps.


---
**Whosa whatsis** *April 09, 2015 18:54*

Are you trying it with the motors connected to unpowered stepper drivers? Sounds like what happens when some stepper drivers (DRV8825s in particular) receive the voltage that the motors generate when you move them by hand.


---
**Ben Delarre** *April 09, 2015 18:58*

Nope, experienced that one before. They're disconnected steppers, with no drivers attached or power. I also checked to make sure the wires weren't touching as I've had them self drive like that too.



I disconnected the motor belts this morning, I can move the central carriage in both axis with one hand now without loads of force, I actually found it to get a little smoother after backing off the tension on the belts too so perhaps they were too tight. Still not happy with it tbh, there are points on one of the x axis rods where I can hear and feel the bushings sticking. I suspect things are very slightly out of alignment, but I don't know how to fix that without making it worse. Any tips on how to make fine adjustments? Or to even know which adjustments to make?



I really don't want to take the whole thing apart again as the axes are a real pain to get in place, so I'll probably hook the motors back up tonight and run the break in gcode with a little sewing machine aoil and see if that helps.


---
**Derek Schuetz** *April 09, 2015 19:02*

sounds like your pulleys are not all in line loosen all the grub screws then move the carriage till it moves freely and then tighten your grubs


---
**Ben Delarre** *April 09, 2015 19:04*

Yeah I'll give that another go tonight as well. I noticed one of the end of axis bearings had worked its way out of the holder and was pushing one of the pulleys out a little further than the others. Fixed that and it helped a little so I'll go round and double check all the others.



I read before that the advice was to use the alignment prints to get the 8mm and 10mm rods aligned, then lock up the pulleys when pushed tight against the end bearings. I'm assuming that is to keep the alignment?


---
**Eric Lien** *April 09, 2015 19:05*

**+Ben Delarre**​ do one corner at a time. Set the upper two side rods based on the printed jigs. Then loosen just one of the bearing block on one side of one of the lower rods. Slide the carriage in and out approaching that corner. The block should then settle at it's preferred location. Then retighten that lower block to lock the location. Repeat for the other 3 corners. 



It takes a lot of trial and error. Plus you are correct, too much tension on the driven pulley to the motor pulley will introduce some deflection and hence add friction by pulling things out of alignment. 



Like I said... This is the most painful part of the build. But when it is right you will know it.﻿


---
**Ben Delarre** *April 09, 2015 19:12*

**+Eric Lien** little confused by that. The printed jigs have slots for the upper and lower rods. Do I put the jigs into each corner, push the rods into the slots and then do the procedure described?



So for clarity:



1. Add jigs to all corners, push rods into jigs.

2. Loosen lower bearing block in one corner, move carriage around, approach corner then lock bearing block.

3. Repeat 2 for each corner.

4. Repeat 2 for upper bearing blocks.



Actually that's a thought, the jigs I have printed are a pretty tight fit for the 10mm rods. Should I open them up a little so the rods sit loosely in the groove, or is the tight fit good?


---
**Eric Lien** *April 09, 2015 20:18*

**+Ben Delarre** Do this final adjustment with all jigs removed. The jigs get things really close (like for me they were almost perfect), but then any final adjustment you need the system to find it's own equilibrium. Hence letting one bearing block float and align itself based on the carriage sliding in towards the corner. The carriage is what sets the spacing.


---
**Ben Delarre** *April 09, 2015 20:32*

Aha, ok that makes sense. Thanks **+Eric Lien** I will try this tonight.


---
**Ben Delarre** *April 11, 2015 03:40*

FYI, this worked great, movement is much smoother now until I attach the motor belts, at which point its a bit stiffer but thats to be expected. Tomorrow afternoon is time to run that gcode. Thanks all!


---
**Eric Lien** *April 11, 2015 13:59*

**+Ben Delarre** glad to hear it worked. It's kind of finicky at first... But when it is aligned it is evident. Can't wait to see video of first moves, and your first print. The break-in code helps smooth things out even further.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/4UJJhMDqM7d) &mdash; content and formatting may not be reliable*
