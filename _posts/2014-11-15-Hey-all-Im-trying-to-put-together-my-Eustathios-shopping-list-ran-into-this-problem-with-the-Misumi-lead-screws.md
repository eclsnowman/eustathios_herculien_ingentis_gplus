---
layout: post
title: "Hey all. I'm trying to put together my Eustathios shopping list, ran into this problem with the Misumi lead screws"
date: November 15, 2014 17:19
category: "Discussion"
author: Shachar Weis
---
Hey all. I'm trying to put together my Eustathios shopping list, ran into this problem with the Misumi lead screws. This line in the BOM:



Lead Screws - One End Stepped Type or One End Double Stepped	

MTSRA12-410-S56-Q8



The problem is, that the S value is limited between 3 and 40. S56 is not accepted. Is this a bug in the BOM? Or maybe Mitsumi changed their specs ? Or maybe I am missing something ?



Thanks.





**Shachar Weis**

---
---
**Eric Lien** *November 15, 2014 17:36*

Someone else ran into this. I would give them a call for the updated pn#. Once you do let us know and we can update the BOM


---
**Alex Benyuk** *November 15, 2014 17:49*

Are you trying to get it of misumi in eu? They don't do the specs listed in the original BOM, I had to order ballscrews in China 


---
**Jason Smith (Birds Of Paradise FPV)** *November 15, 2014 17:57*

I think misumi may have changed the options available for lead screws. The part # listed in the BOM was correct/available when I initially ordered it earlier this year. If someone is able to get in touch with them (Misumi) or some other merchant who will provide a suitable option, I'll be glad to update the BOM. 


---
**Eric Lien** *November 15, 2014 18:36*

**+Jason Smith** I think this is it?



MTSBRA12-410-S56-Q8 = $40.89 each


---
**Jason Smith (Birds Of Paradise FPV)** *November 15, 2014 18:50*

**+Eric Lien**  Looks good.  BOM Updated.  You the man.


---
**Eric Lien** *November 15, 2014 18:58*

Looks like they felt the part number needed a "B"

Not sure what it does, but that's ok. I don't want "B" to feel left out. :)


---
**Jason Smith (Birds Of Paradise FPV)** *November 15, 2014 19:05*

Yeah, unfortunately that "B" costs another $5 per item. 


---
**Shachar Weis** *November 15, 2014 20:04*

Thanks guys.


---
**Shachar Weis** *November 15, 2014 22:35*

Hey all, Another question. Why are there 4 different GT2 belts on the BOM? Three from SDP and one from RoboDigg ?


---
**Eric Lien** *November 15, 2014 23:41*

Robotdigg ones have a collar, so they don't work for those locations. But they do in the others and are a lot cheaper.


---
**Shachar Weis** *November 15, 2014 23:54*

ok, so I understand that all 4 are needed. The lengths in the BOM, are those the exact lengths required ? Is there some spare ?


---
**Shachar Weis** *November 15, 2014 23:57*

BTW, what exactly is a collar in the context of a GT2 belt ?


---
**Eric Lien** *November 16, 2014 00:46*

Sorry. I thought you were talking about the pulleys.


---
**Shachar Weis** *November 16, 2014 01:48*

Ah, so back to the question of belts, why are there 4 different belts in the BOM ?


---
**Eric Lien** *November 16, 2014 02:43*

Because there are different length belts needed. The Drives are run in the bottom area, and tie from the drives via a closed loop belt to the x/y gantry. Belt length to the top x/y bar is a different length than the belt to the bottom x/y bar. Then there is the closed loop z axis belt. Finally there is bulk belt used to tie the x/y axis to the drive pullies. Sdpsi has more selection for closed loop belts, but robotdigg is cheaper for bulk belt.



Hope that helps.


---
**Shachar Weis** *November 16, 2014 02:59*

Oh, I didn't realize that the SDP was closed loop. Ok, that makes sense now. Thanks.


---
*Imported from [Google+](https://plus.google.com/117479393665221551027/posts/9jUS4LYW8hr) &mdash; content and formatting may not be reliable*
