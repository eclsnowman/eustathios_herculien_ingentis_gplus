---
layout: post
title: "Hey guys, can you share some photos of your Eustathios build?"
date: October 29, 2014 17:23
category: "Discussion"
author: Shachar Weis
---
Hey guys,

can you share some photos of your Eustathios build? Especially closeups of the rods and pully system, and the carriages.



Thanks !





**Shachar Weis**

---
---
**Jim Wilson** *October 29, 2014 20:46*

I will be shortly, waiting on the last set of bolts.


---
**Eric Lien** *October 30, 2014 00:59*

I have it fully modeled in solidworks out on github (bolts and all). I can save it as a 3d PDF. You need to open it in adobe acrobat (not a 3rd party PDF viewer). You could then zoom in on anything you want.



Would that help.﻿


---
**Shachar Weis** *October 30, 2014 13:06*

Is it the same as the files in the YouMagin ? I've looked at the sketchup file (it doesn't include the belts). I was hoping for some photos but I guess a 3D model is also ok. Question, how do the rod holders connect to the frame, and how does the rod connect to the holder ?


---
**Eric Lien** *October 30, 2014 15:04*

Here is the 3d pdf. No belts modeled, but everything should be clear when you navigate around. [https://drive.google.com/file/d/0B1rU7sHY9d8qZVFzMUpySWtSRVU/view?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qZVFzMUpySWtSRVU/view?usp=sharing)



Also here is the Github to my files, [https://github.com/eclsnowman/Lien3D_Eustathios_Spider](https://github.com/eclsnowman/Lien3D_Eustathios_Spider)

They are slightly more detailed than the main github for Eustathios by Jason Smith. I have been meaning to merge the two with Jason, but I have not had as much time recently. The main github files are solidworks 2014. But there are others in the "Other 3D Formats" folder like .step, sketchup, spaceclaim. The step, pdf, and solidworks files are all up to date. The others are still good, but I have made mods since they were uploaded.


---
**Shachar Weis** *October 30, 2014 15:09*

Thanks Eric. What are the main differences between your fork and the original ?



EDIT: I should have looked the files. They look much nicer, easier to print, no crazy overhangs, and no sketchup-generated-holes and craziness. WIN.


---
**Eric Lien** *October 31, 2014 16:32*

**+Shachar Weis** glad you like them the only tough one is the carriage due to the supports. But it cleans up nice. The stl has hold down tabs built in to fight abs corner lift while printing.


---
*Imported from [Google+](https://plus.google.com/117479393665221551027/posts/Dq52kV84EqS) &mdash; content and formatting may not be reliable*
