---
layout: post
title: "When building the structure I noticed that most you don't use brackets for the 90deg corners"
date: April 25, 2014 02:15
category: "Discussion"
author: Chad Nuxoll
---
When building the structure I noticed that most you don't use brackets for the 90deg corners. Is that because the method you use to hold it together is strong enough and the brackets are to expensive

![images/9a98e035c0e666f720e922ab5b7d7c9a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9a98e035c0e666f720e922ab5b7d7c9a.jpeg)



**Chad Nuxoll**

---
---
**D Rob** *April 25, 2014 02:35*

that's right on both accounts. drill and tap the center hole and use button head screws ( i use 1/4-20 because they fit the slot as good as a tee-nut) using 2020 drill a hole large enough for the allen wrench at 10mm from the end of the receiving extrusion (for 90 degree corners only).  this will square up the corner use a jig to get the distance right then tighten the screw with the allen through the hole. I use a pair of pliers to get it extra tight. Check my vids there is one showing the rigidity and my overlapping ends method to eliminate the necessity of tight tolerances for a frame except for the upright extrusions and the rods that go between them.


---
**Chad Nuxoll** *April 25, 2014 02:39*

O really most people use the 2020 Ive been looking at the 1010. I have watched a couple of your videos and planned on doing it that way. It saves alot of money if you dont buy the 90 deg brackets


---
**Chad Nuxoll** *April 25, 2014 02:45*

Also I was going to ask where is a good place to buy the button head screws. Do you have to get the ones from 80/20


---
**D Rob** *April 25, 2014 02:58*

**+Chad Nuxoll** I have 1010 for first run I now prefer 2020 its smaller and a little lighter 5.4mm narrower which means 10.8 mm more space inside with the same outside measurements


---
**D Rob** *April 25, 2014 02:59*

**+Chad Nuxoll** I bought mine in a local fastenal store. low price and best of all no wait. 1/4-20 still works on 1010. my 2020 is using the left overs from the 1010 frame


---
**D Rob** *April 25, 2014 03:01*

with my frame design the vertical extrusions are critical on lengths as they fit between the top and bottom frames


---
**Chad Nuxoll** *April 25, 2014 03:11*

It would also be a little cheaper if I got the 2020. Ahhhh Ya it would have to be almost perfect for the vertical extrusions. Thats why you file them i guess. Perfect I was hoping that you didnt have to buy there darn bolts everything 80/20 has is so expensive 


---
**D Rob** *April 25, 2014 03:26*

**+MISUMI USA** is super expensive on most stuff but their extrusions are fairly decently priced. they cut to .5mm tolerances and square. It is more costly but they also will pre-drill and tap and drill allen holes to your specs. this service is expensive. I drill and tap myself. However i love their cuts, super square and imperceptible if any difference in cut lengths


---
**Tim Rastall** *April 25, 2014 05:31*

Hey **+Chad Nuxoll**.  In my experience,  the corner brackets are a nice to have,  not critical. I left some in as I had them and wasn't sure how the button screws would work on their own. Here's a link to the screws I used:

[http://www.aliexpress.com/item/Stainless-steel-ISO7380-Button-Head-HEX-Socket-Cap-Screws-M5-10mm-100pcs-High-Quality-Free-Shipping/647522993.html](http://www.aliexpress.com/item/Stainless-steel-ISO7380-Button-Head-HEX-Socket-Cap-Screws-M5-10mm-100pcs-High-Quality-Free-Shipping/647522993.html)


---
**D Rob** *April 25, 2014 07:14*

**+Chad Nuxoll**  remember if you use the ones **+Tim Rastall** linked get the m5 tap and the appropriate drill bit for the tap


---
**Ruben Carracedo Cano** *April 25, 2014 09:10*

Sorry about mi ignorance but... where do you buy this aluminium structures? Thanks!


---
**Tim Rastall** *April 25, 2014 09:30*

**+Ruben Carracedo Cano** I buy mine from China through Aliexpress.com but you may be able to source it locally too.  It's know as 2020 or tslot or aluminium extrusion or a combination of those. 


---
*Imported from [Google+](https://plus.google.com/112388822161261154200/posts/UhT5RdumKaQ) &mdash; content and formatting may not be reliable*
