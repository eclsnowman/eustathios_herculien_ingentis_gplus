---
layout: post
title: "When clicking on Z Home my print bed goes down instead of up to the Z endstop"
date: July 20, 2015 22:17
category: "Discussion"
author: Brandon Cramer
---
When clicking on Z Home my print bed goes down instead of up to the Z endstop. Is this a setting in the Azteeg X5 Mini config? 





**Brandon Cramer**

---
---
**Eric Lien** *July 20, 2015 22:43*

Yes you put an ! After the stepper direction pin for that axis to reverse it. Same goes for any others that go in the wrong direction.



[http://smoothieware.org/troubleshooting#toc8](http://smoothieware.org/troubleshooting#toc8)


---
**Brandon Cramer** *July 20, 2015 23:08*

**+Eric Lien**   Just out of curiosity, would't that be in the config file already? The stepper motor will only hook up one way with jumper that it comes with? 



I got Z home working :-) Thank you!!! Now I need to work on X and Y. 


---
**Eric Lien** *July 20, 2015 23:56*

You need to configure the file for your hardware. Mine had screw terminals. So I just flipped 2 wires to reverse it. I would spend some time learning the config file if I were you. It is easy enough, and it will open your opportunity for tuning properly.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/aFrtPi8eBRN) &mdash; content and formatting may not be reliable*
