---
layout: post
title: "Here are the first 2 print attempts on eustathios!"
date: May 13, 2016 10:08
category: "Build Logs"
author: Dimitrios Tzioutzias
---
Here are the first 2 print attempts on eustathios! Still need to calibrate  s3d retract/coast settings but the second seems to be pretty close to correct ones. In first print it was under extruding when it had to print solid bottom layers maybe because the nozzle temp was to low. On second one I raised the temp to 240 for first 3 layers and the to 235 for the rest of the print. First time I am using bowden extruder so still I don't have the needed know how! Need to be done yet some cable management and to mount the board beneath the printer. 



![images/5d57b3da99a0079d99dc93228d1465be.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5d57b3da99a0079d99dc93228d1465be.jpeg)
![images/4226661c4d5ceb836d465f41f9e905c5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4226661c4d5ceb836d465f41f9e905c5.jpeg)
![images/36df43fe98b544f2f4985fb6fa997872.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/36df43fe98b544f2f4985fb6fa997872.jpeg)
![images/107712f46293731145097480d6d0bde8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/107712f46293731145097480d6d0bde8.jpeg)
![images/87b5d8fb80762e74047a20988d2ef17a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/87b5d8fb80762e74047a20988d2ef17a.jpeg)
![images/0694f0c4fd5d6f0d2634bf652a6c4580.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0694f0c4fd5d6f0d2634bf652a6c4580.jpeg)

**Dimitrios Tzioutzias**

---
---
**Mike Miller** *May 13, 2016 12:44*

Not bad at all! 


---
*Imported from [Google+](https://plus.google.com/108355995361667474020/posts/ikZPzGbvxtb) &mdash; content and formatting may not be reliable*
