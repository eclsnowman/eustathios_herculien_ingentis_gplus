---
layout: post
title: "Laser cut option? .... So I have a big laser cutter (1200 x 960 table) and would like to know if anyone has some ideas on converting this into a laser cut version, I have a bunch of 6mm hardwood marine ply to use (and 4mm if"
date: July 17, 2014 12:49
category: "Discussion"
author: Tim Jacobsen
---
Laser cut option? ....



So I have a big laser cutter (1200 x 960 table) and would like to know if anyone has some ideas on converting this into a laser cut version, I have a bunch of 6mm hardwood marine ply to use (and 4mm if it's of any use) I'm expecting some sort of box looking thing, the idea is to use the mechanics the same as these printers, but the frame and as much of the other parts to be laser cut. Any ideas? I'm trying to take advantage of my tools and not use the extruded aluminium, I have 12mm rods and bearings (or bushings too which I'm using on a Delta printer with great success). I pretty much have all the parts I think except the frame :)





**Tim Jacobsen**

---
---
**Joe Spanier** *July 17, 2014 12:52*

I've been thinking of the same thing but a routered version.  I have a quarter sheet machine and was looking at using 12mm or 3/4 so I can use cross dowels. I haven't started drawing yet though. 


---
**Tim Jacobsen** *July 17, 2014 12:58*

cool, so there would be some support if I were to go down this track ;) I'm keen to share plans and ideas (obviously..?) and I can always laser cut stuff for other people if needed. I think there would be a quite a cost saving using wood for as many parts as possible... My thing though is to ensure that the best rigidity and accuracy is achieved while still using wood. usually they don't go together but I beg to differ..


---
**James Rivera** *July 17, 2014 13:23*

Why not just use Ultimaker design?


---
**Tim Jacobsen** *July 17, 2014 13:30*

hmm, seems pretty similar I know, whats the main differences between that and the Ingentis? First thing that comes to mind is the Z axis having rods at each end which I think is a pretty good option. May be I'm missing the point of the Ingentis/Eustathios ? It seems to be a pretty solid design but yes at least I can start with the Ultimaker design and convert it somewhat...


---
**James Rivera** *July 17, 2014 17:42*

If I had access to a laser cutter and the wood, it would be a no-brainer for me. Ultimaker is a great design, and one the Ingentis/Eustathios was largely based on. Adding a second smooth rod for the z-axis is probably a good idea, but it is also probably unnecessary given the Ultimaker's proven success at making fast, high-quality 3D prints.



For myself, I do not have ready access to a laser cutter, so I'm planning on making something based on the Ultimaker design but with aluminum extrusions, which is basically how I found myself here, since that is effectively what they are (with some minor improvements, such as the z-axis).



Both Ingentis/Eustathios are great designs, and I may just go with one of them as is. I am currently envisioning something similar, but driven with braided fishing line using either CoreXY or DW-G.



The z-axis is the main thing I'm still undecided on, and the indecision is effectively keeping me from doing anything at the moment.



But I am very close to pulling the trigger on buying the mechanical parts for it--I already have a spare Printrboard Rev. E and an Arduino Mega with RAMPS 1.4 with both A4988 and DRV8825 microsteppers to choose from.



I also have a Printrbot+ v1 (with Super-Z upgrade) that I intend to cannibalize to make an extrusion-based bot, which is proving to be another source of universal procrastination. <sigh>



Sorry--I wander off-topic too easily. Just cut the Ultimaker parts and build it--all evidence I've seen suggests you will be extremely happy with it.


---
**Joe Spanier** *July 17, 2014 17:46*

One thing I will point out, Is wood resonates. Im on wood printer 4 and wood machine 7( 3 CNC router gens) and when you bolt a vibrating stepper to a wood box, it turns into a guitar. 



Its the main printer complaint from the wife, "Damn that thing is loud"



So other then that I like wood box designs.


---
**James Rivera** *July 17, 2014 17:50*

**+Joe Spanier** I've seen posts where people use small bits of plastic/rubber tubing in between the stepper and the wood as a vibration dampener. Probably for this very reason. Rubber foam would probably work, too.


---
**Joe Spanier** *July 17, 2014 18:23*

I was going to try some cork gaskets this weekend. Your right though. 


---
**Tim Jacobsen** *July 17, 2014 22:10*

cool, cheers for the comments, I have heaps of these: [http://www.controlsdrivesautomation.com/Dampers-Suitable-For-Retrofit](http://www.controlsdrivesautomation.com/Dampers-Suitable-For-Retrofit) to try for the noise and also to improve vibration through to the hotend. Doesn't the ingentis have a bigger build volume? but yea I think it would be a great thing to start with at least, then I can make upgrades as I see fit, cheers


---
**Matt Kraemer** *July 17, 2014 23:51*

Hey **+Tim Jacobsen**, I'll work with you on this. If you want to make ultimaker clones for nz I'm on board.﻿


---
*Imported from [Google+](https://plus.google.com/+TimJacobsen3D/posts/2suzCXn2Dbs) &mdash; content and formatting may not be reliable*
