---
layout: post
title: "So, yesterday I had to apply excessive force to try and increase the tension in my gt2 belts, but the test circles still failed to print properly so I decided to print some of Eric Lien 's parts, after Miguel Snchez"
date: October 18, 2014 10:51
category: "Show and Tell"
author: Pieter Swart
---
So, yesterday I had to apply excessive force to try and increase the tension in my gt2 belts, but the test circles still failed to print properly so I decided to print some of **+Eric Lien** 's parts, after **+Miguel Sánchez** suggested that I might have to get rid of the igus bearings. Here you can see that I just started the print, but also, that those circles suddenly look almost perfect. I may have to do some more research into what's going on.

![images/2371e6e75deaa2c07d3d3907652d11cc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2371e6e75deaa2c07d3d3907652d11cc.jpeg)



**Pieter Swart**

---
---
**Miguel Sánchez** *October 18, 2014 11:05*

Maybe alignment of bearings was not good and it is getting better after some use? Did you perform any type of  mechanincal run-in?


---
**Pieter Swart** *October 18, 2014 11:13*

I did a 5 hour run of a spiral vase,   but it probably wasn't enough. I've also replaced some shafts which I assume undid any previous run-ins. 


---
**Miguel Sánchez** *October 18, 2014 11:15*

Bended shafts may also disturb smooth motion ...


---
**Pieter Swart** *October 18, 2014 11:32*

That seems to be a sensitive subject on these printers ;-) 


---
**Eric Lien** *October 18, 2014 11:48*

I do suggest making a "break in" g-code and running it for a few hours. Travel to all four corners in a box, then cross over the center going corner to corner. Repeat. Repeat. Repeat....



You can add z travel codes to break in the z stage too.



It really helps to smooth things out. I am pretty sure this is what I used. Just edit travel distances to match your printer usable bed sizes. [https://docs.google.com/file/d/0B1rU7sHY9d8qcVYwUmhBYzBJOUk/edit?usp=docslist_api](https://docs.google.com/file/d/0B1rU7sHY9d8qcVYwUmhBYzBJOUk/edit?usp=docslist_api)﻿


---
**Pieter Swart** *October 18, 2014 11:55*

Thanks Eric!


---
**Eric Lien** *October 18, 2014 12:08*

**+Shauki Bagdadi** I try. This community is an important part of my identity now. We are all both teachers and students. I couldn't have ever achieved what I have without the great minds that struggled before me. And I am learning from the great minds that have followed me then diverged into their own inspirations.



This group and this sense of community is truly amazing.﻿


---
**Pieter Swart** *October 18, 2014 19:02*

Thanks Shauki


---
**Rupin Chheda** *October 26, 2014 17:49*

I face a similar issue, but I am using a MGN12h linear rail and carriage. 


---
*Imported from [Google+](https://plus.google.com/117269052061351663459/posts/cfBab2C6GZ3) &mdash; content and formatting may not be reliable*
