---
layout: post
title: "My solution for a dropping bed. 1:10 worm gear, with a 5mm stainless rod that goes across the entire bed"
date: September 25, 2014 23:20
category: "Deviations from Norm"
author: ThantiK
---
My solution for a dropping bed.  1:10 worm gear, with a 5mm stainless rod that goes across the entire bed.  Keeps everything in sync, no lash (though it took quite a few adjustments and prints to get it that way!), 1 motor, and I can flip it and hide it away under the machine once I extend it.



![images/d9b6c7afb4d112337dcb39c86a679858.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d9b6c7afb4d112337dcb39c86a679858.jpeg)
![images/93e4f0a95cc61f82813bbd59f57ba919.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/93e4f0a95cc61f82813bbd59f57ba919.jpeg)

**ThantiK**

---
---
**Eric Lien** *September 25, 2014 23:45*

I like the look. I have always wondered how belt stretch will effect a large moving bed. Sometime I put a lot of force on the bed pulling off a pesky print. 


---
**Jean-Francois Couture** *September 25, 2014 23:46*

That looks pretty good. Where did you get the worm gear ?


---
**Eric Lien** *September 25, 2014 23:47*

+1 for the heat sink too. I do this to all my steppers now. I found some cheap and tall north bridge heatsinks that work great. Just add a tiny dab of thermal epoxy and you are done.


---
**ThantiK** *September 25, 2014 23:49*

[http://www.ebay.com/itm/321344353749?_trksid=p2059210.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/321344353749?_trksid=p2059210.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT) and [http://www.ebay.com/itm/321344351604?_trksid=p2059210.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/321344351604?_trksid=p2059210.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)


---
**Jean-Francois Couture** *September 25, 2014 23:58*

darn, ebay is blocking the links.. at least for me.. anyone else has that problem ?


---
**Dale Dunn** *September 26, 2014 00:16*

At that price, It certainly does look like a good solution to the problem. It looks like the seller has a limited supply. I wonder what general availability is.


---
**Daniel Fielding** *September 26, 2014 00:19*

I really like this. Looks like belt driven z is more feasible with this.


---
**Matt Miller** *September 26, 2014 01:56*

The worm shows a 6mm ID.  Did you use a NEMA 23 and take the shaft down to 6mm?


---
**ThantiK** *September 26, 2014 02:11*

**+Matt Miller**, 5mm ID, 6mm OD brass tubing used as a shim currently to shore up the diameter.  Can be found at hobby shops.


---
**Matt Miller** *September 26, 2014 02:56*

Good call.  Ordered the stuff tonight as a fall back if my initial idea for a belt-driven Z doesn't pan out.


---
**Mike Miller** *September 26, 2014 03:33*

How heavy is your bed?  




---
**Thomas Sanladerer** *September 26, 2014 06:39*

Worm drives need lube, lots of it. 


---
**ThantiK** *September 26, 2014 13:07*

**+Mike Miller**, it weighs absolutely nothing as of right now.


---
**Mike Miller** *September 26, 2014 13:16*

I'm wondering if you'll need any custom tuning to account for lash in the system for vertical travel. Kind of the opposite of z-lift on a delta. 



I mentioned my lathe having a lot of lash. The compensating mechanism to account for that is to always cut in one direction, and when changing cuts, pull the tool far enough away to take up the lash, plus the distance for the new cut. 



Just dropping the plate the height of the layer may introduce z artifacts due to wear, lubrication anomalies, and stitction...moving it down 1mm+z-height, then back up 1 mm might give more consistent prints. 



Unless it's perfect...then game on! :)


---
**Brandon Satterfield** *September 26, 2014 15:24*

Looking forward to seeing it in action!


---
**ThantiK** *October 10, 2014 00:13*

Still haven't had the time to go lathe a shim for this. :(


---
**Jim Squirrel** *January 09, 2015 00:41*

how's your bed looking now days?


---
**ThantiK** *January 09, 2015 03:47*

Nonexistant.  Been too busy with life. :(


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/JVRsUELM9qt) &mdash; content and formatting may not be reliable*
