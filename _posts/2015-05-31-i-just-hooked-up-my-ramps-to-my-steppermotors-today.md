---
layout: post
title: "i just hooked up my ramps to my steppermotors today"
date: May 31, 2015 18:40
category: "Discussion"
author: E. Wadsager
---
i just hooked up my ramps to my steppermotors today. i can  only get the motors to turn one direction, they are just holding still and making noises. does anyone know what is causing it.





**E. Wadsager**

---
---
**Miguel Sánchez** *May 31, 2015 18:43*

if end-stops are not wired/configured properly, moving only in one direction is normal.


---
**Bruce Lunde** *May 31, 2015 18:49*

Are you sure you have the stepper motor wire pairs correctly identified?  You can google several peoples explanations of how to do this with a multi-meter.


---
**E. Wadsager** *May 31, 2015 18:52*

the conectors where mounted to the steppermotors when i got them. 


---
**E. Wadsager** *May 31, 2015 19:30*

i mounted min endstops. didnt help. 

can it be some settings in marlin or adjustment of stepperdrivers? 


---
**Miguel Sánchez** *May 31, 2015 19:40*

That's what I say wired/configured :-)



End-stops can usually be wired as NC or NO. You can use command M119 to check the status of your end-stops now. If they are not pressed they should read OPEN, if they appear TRIGGERED then your configuration is not correct and you need to go ahead and change it in Configuration.h

 


---
**Mutley3D** *May 31, 2015 19:57*

Motors will only run in one direction even with endstops connected, until you home each axis iirc.


---
**Godwin Bangera** *June 01, 2015 05:27*

also do read about Vref on the Ramps board (google) ... ie current settings for the stepper board, using a multi-meter... Min endstops is fine just unquote the max endstops in your config...


---
*Imported from [Google+](https://plus.google.com/103157635215674778495/posts/aQNabCe3P5z) &mdash; content and formatting may not be reliable*
