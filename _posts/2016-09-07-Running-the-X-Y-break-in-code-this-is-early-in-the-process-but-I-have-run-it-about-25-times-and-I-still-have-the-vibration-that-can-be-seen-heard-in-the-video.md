---
layout: post
title: "Running the X-Y break in code... this is early in the process, but I have run it about 2.5 times and I still have the vibration that can be seen/heard in the video"
date: September 07, 2016 14:11
category: "Build Logs"
author: Sean B
---
Running the X-Y break in code...   this is early in the process, but I have run it about 2.5 times and I still have the vibration that can be seen/heard in the video.  The vibration is most pronounced when moving diagonally or towards the end of the movement.  The carriage is easier to move than it was prior to running the gcode but still isn't quite right.  I have tried doing as was suggested by Walter and moving the carriage into each corner and loosening the lower bearing block a little but it hasn't helped.  It appears that the bearing block may be limited in the allowable adjustment.  The upper bearing block is butted up against the upper extrusion and the lower one appears to be butted against the lower extrusion in all corners.  Would it make sense to pop off the lower bearing blocks and shave a little off one side to allow more "wiggle room" against the lower rail?



I have checked the rain alignment side to side with calipers and everything looks exact.  I also used the tools to help align the rods.



I also set my phone when recording to show the vibrations that transfer through the frame.  



On the plus side, the Z axis moves very smoothly.


**Video content missing for image https://lh3.googleusercontent.com/-H0NoZtCwXYk/V9AgJ2RKUvI/AAAAAAAAZR0/lHcyfSPaKDcB3Li8xO4nMMhmUamb-FO-gCJoC/s0/VID_20160905_001611.mp4.gif**
![images/57a8cf6e02e95b394993527fe20fb546.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/57a8cf6e02e95b394993527fe20fb546.gif)



**Sean B**

---
---
**Eric Lien** *September 07, 2016 14:29*

I think trying to save a little bit off the bottom of your lower bearing block would be a good place to start. If your print is oversized at all there's no tolerance allowed there for it to move beyond because of course it hits that Extrusion horizontally on the right left side. 


---
**Ted Huntington** *September 07, 2016 18:10*

As long as you think the alignment is pretty close I personally would just manually keep working the carriage and running the break-in code- it may be that the carriage bushings just need to find the correct place.


---
**Sean B** *September 07, 2016 19:17*

**+Ted Huntington** The side-to-side alignment is good but I honestly have no idea about the vertical alignment.  Its' entirely possible that my rods have a tiny bow to them causing this vibration so I think I will try and shave a little off each lower block and then adjust each corner.  The gcode has been run enough for me to see it isn't getting any better.  



**+Eric Lien**On a side note, how hot are these motors and drivers supposed to get?  I am running at 1.5 and they are freaky hot, haven't check them with a thermocouple though.  I can also smell a distinct electronics scent presumable from the drivers after about 15-20 minutes of operation.  I also accidentally touched a driver heatsink and shrieked, they are not pleasant to touch.


---
**Roland Barenbrug** *September 07, 2016 20:18*

I don't know about the drivers (always a fan on top) but the steppers should hardly become warmer than ambient with this gcode. Something going wrong when you smell something ... Can you remove the closed belts at the steppers and check how much force it takes to drive the x and y axis by hand?




---
**Eric Lien** *September 07, 2016 20:56*

**+Sean B** You installed the heat sinks and are actively cooling them correct?


---
**Eric Lien** *September 07, 2016 20:59*

**+Sean B** Also the steppers are 1.5A max motors (if you used the ones in the BOM). That doesn't mean 1.5A is required to drive them. Start lower, like 1A and play up and down from there. Go higher if they skip / miss steps. You only need to send just enough current to avoid skipping steps. Any more is just wasted in heat.


---
**Ted Huntington** *September 07, 2016 21:50*

**+Sean B** Definitely check your vertical alignment- I found that even a few mm off was enough to cause friction and jamming.


---
**Sean B** *September 08, 2016 01:51*

**+Eric Lien**​ I did install the heat sinks, but did not install active cooling.  I didn't realize it was necessary; however I was basing my 1.5 values based on the spider v2 config, so I stupidly have been running then at their limit.  There obviously if a bit of resistance on the rods still too so that is only adding to the issue.  I'll have some intimate one on one time this weekend to try and get it sorted out.


---
**Eric Lien** *September 08, 2016 02:10*

Yeah the mini V2 with TI drivers handles the heat a little better. But the new drivers in the V3 are better... Just need more cooling.


---
**Ted Huntington** *September 08, 2016 03:24*

Another thing to verify is that the belts are not chafing against the pulleys- I found that the belt needed to be well centered/aligned on the pulleys. I've never had a problem with motors getting too hot- they get warm- but that's all- and I have no cooling on any of them- but I use ramps. It may mean simply that the motors are working too hard against the friction.


---
**Maxime Favre** *September 08, 2016 06:47*

I ran the break in gcode about 10 times until I gave me a smooth run. self alignment bearings need a few time to put them in place.

Another thing is belt tension. Mines were way too tight. Loosen them a bit gave me smooth mouvement (and less torque on the motors).




---
**Sean B** *September 08, 2016 11:24*

**+Ted Huntington** I'll have to double check, but I remember see your comment about this a long time ago, so I was extremely careful yo keep the pulleys aligned.

**+Maxime Favre** Wow you ran the break in code 10 times!? before it was smooth?  I think my belts are ok, they have some spring to them.  but if all else fails I'll work my way back to this.



A question for everyone running the 60mm steppers from Robotdigg, what do you run your motor currents at?  I am running A5984 drivers.


---
**Ted Huntington** *September 08, 2016 17:07*

yeah I had the problem with my belts being too tight too- I found that the system needs a little bit of slack to be able to shift around. In terms of motor current I have not measured recently, but have the step sticks (ramps) current set pretty high.


---
**Stefano Pagani (Stef_FPV)** *September 09, 2016 01:40*

Are you sure that you don't have a snoring cow in your basement?


---
**Sean B** *September 09, 2016 01:41*

**+Stefano Pagani** lol, there are some dark corners...


---
**Sean B** *September 10, 2016 23:53*

I spent a few hours taking off the bearing blocks, sanding, refitting and it did help a bit; however when all was said and done it still sounded horrible and was skipping steps on certain parts of the rods.

While looking at the carriage rods I could see one would bend slightly when moving the carriage in one direction. 

UGGGG  One of the 8mm gantry rods is bent. 



I really regret trying to save a couple bucks through Aliexpress.  This would be the third bent rod I've received.


---
**Ted Huntington** *September 11, 2016 00:24*

wow - thanks for sharing- that's good to know- I should have thought of that. yeah my rods are from aliexpress too- they are all straight, but one was just a little too thick and had to have about 1mm turned off of it to fit into the bearing- they did say that they would send another free with my next order- but that takes about 2 weeks even with epacket. Some people use m10 rods for the carriage instead of m8- and if I build a +10mm larger Eustathios I would definitely use m10 rods for the carriage if not 2020 extrusion- as the Folger FT-5 does.


---
**Sean B** *September 11, 2016 00:30*

Yeah I think 10mm would be better too.  The 8mm seem like they have a little too much flex.  I'll look around for another supplier.  Misumi shipping was $12 for a $12 dollar rod last time.


---
**Eric Lien** *September 11, 2016 04:42*

8mm is good for the span on a Eustathios. But yeah I used 10mm on HercuLien because of the larger span. 



I have thought of designing a 20x20 extrusion cross beam version of the printer. Just never got around to it.


---
**Ted Huntington** *September 11, 2016 17:33*

yeah 8mm on the carriage works, I just think 10mm, like the sides would be more rigid/less bendable- but it does not effect the print so far as I can see.


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/Au7yBeNofuN) &mdash; content and formatting may not be reliable*
