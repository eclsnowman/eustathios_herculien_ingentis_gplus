---
layout: post
title: "Finally got my printer configured for PLA"
date: December 04, 2015 05:34
category: "Show and Tell"
author: Igor Kolesnik
---
Finally got my printer configured for PLA. 65 mm/s, 0.100 layer hight. 3.4 mm retraction with 4 mm wipe. ABS is still a huge pain in the rear for me. But I will get there. Also planning to enclose Eustathios V2 completely which should help with ABS printing



![images/6eee48a5ffa8a8c8702e71ac4d1651e9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6eee48a5ffa8a8c8702e71ac4d1651e9.jpeg)
![images/891d0e8fe2898beba6005e1a936c2ce2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/891d0e8fe2898beba6005e1a936c2ce2.jpeg)
![images/d71f56da2c71d0bdb97d44d3fb0b1876.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d71f56da2c71d0bdb97d44d3fb0b1876.jpeg)

**Igor Kolesnik**

---
---
**Ben Delarre** *December 04, 2015 07:12*

Looking good. I might try the wipe myself,  not had much luck with it thus far. 


---
*Imported from [Google+](https://plus.google.com/+IgorKolesnik/posts/fmw6Zy2yha4) &mdash; content and formatting may not be reliable*
