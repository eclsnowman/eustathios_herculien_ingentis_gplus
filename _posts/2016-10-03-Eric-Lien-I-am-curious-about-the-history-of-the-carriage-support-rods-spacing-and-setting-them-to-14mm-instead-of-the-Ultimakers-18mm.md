---
layout: post
title: "Eric Lien I am curious about the history of the carriage support rods spacing and setting them to 14mm instead of the Ultimakers 18mm"
date: October 03, 2016 12:21
category: "Discussion"
author: Jim Betsinger
---
**+Eric Lien** I am curious about the history of the carriage support rods spacing and setting them to 14mm instead of the Ultimakers 18mm.





**Jim Betsinger**

---
---
**jerryflyguy** *October 03, 2016 15:57*

What would be the benefit? 



There are several things that would potentially be problematic, from how the belts are clamped and the rod end slider geometry to the spacing between the end pulleys and the opposing rods etc. you'd also magnify and play in the center cross rod bushings. The farther the rods are apart the more stable that makes the nozzle position. You could combat this by making the center nozzle carriage wider but you'd lose valuable x/y size.



All that aside, it'd be an interesting design challenge ! (If one is looking for a challenge) 


---
**Eric Lien** *October 03, 2016 16:06*

The closer you can keep the cross rods together the less cantilever force the opposing rod applies to the carriage as it drives it. That being said... That specific dimension originated from the Ingentis by Tim. It was just kept the same through the Eustathios, Eustathios Spider, and HercuLien for commonality.


---
*Imported from [Google+](https://plus.google.com/101859507581389458920/posts/1qk13Pdjrt6) &mdash; content and formatting may not be reliable*
