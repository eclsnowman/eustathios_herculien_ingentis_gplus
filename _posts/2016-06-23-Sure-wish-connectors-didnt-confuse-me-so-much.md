---
layout: post
title: "Sure wish connectors didn't confuse me so much"
date: June 23, 2016 11:47
category: "Discussion"
author: Mike Miller
---
Sure wish connectors didn't confuse me so much. Finding an ideal, power rated, elegant, cheap solution isn't easy...Check out this Molex part...looks like it'd handle a dual extruder easily. Now, price all the pins, coax connectors, figure out the cable side with stress relief..oh, and they STRONGLY suggest crimp connectors...drop some coin on a good crimper. 

![images/25302b7b476db5881044fdd898ee8ef5.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/25302b7b476db5881044fdd898ee8ef5.png)



**Mike Miller**

---
---
**Gústav K Gústavsson** *June 23, 2016 12:35*

Well have never seen this connector before but this looks definitely not as a crimp connector! Where did you find this connector?


---
**Mike Miller** *June 23, 2016 12:43*

It's a molex part, [digikey.com](http://digikey.com) sells them, but they're not cheap. The larger holes are for coax. 


---
**Jeff DeMaagd** *June 24, 2016 03:58*

Can those coax connectors really take the current? That, and I wouldn't enjoy crimping those coax pins.


---
**Mike Miller** *June 24, 2016 11:23*

I suspect so, on further reflection, I'd be more worried about fatigue due to movement...


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/ZKeGwDQcLN1) &mdash; content and formatting may not be reliable*
