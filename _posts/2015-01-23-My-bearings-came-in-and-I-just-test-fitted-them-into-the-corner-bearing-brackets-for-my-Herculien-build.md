---
layout: post
title: "My bearings came in and I just test fitted them into the corner bearing brackets for my Herculien build"
date: January 23, 2015 20:22
category: "Discussion"
author: Daniel Salinas
---
My bearings came in and I just test fitted them into the corner bearing brackets for my Herculien build.  The fit snug in all but one of them.  I'm assuming that anything short of a snug fit will result in artifacts in the prints correct?  I have one corner plate where the bearing fits exactly, theres no discernable play but it comes out if I turn the plate upside down and give it a light tap.





**Daniel Salinas**

---
---
**Daniel Salinas** *January 23, 2015 20:59*

I guess another question I have is that my robo3d printer prints pla like a champ, after I get this herculien done I'll likely never print abs in the robo again.  Can the corner brackets be PLA if only temporarily until I get the Herc finished and printing?  I could print new ones with that in abs that are likely much better quality.



I have too much ribbing with abs on the robo3d and I've dialed it in to the best I can get it.


---
**Eric Lien** *January 23, 2015 22:45*

**+Daniel Salinas** PLA would likely be fine for the corner brackets since the force is never in a way that should be able to deform PLA even if it's warmed up. Also if you need a slightly tighter fit wrap a little Kapton tape or something similar around the bearing before you press it in. That would also solve the problem of play. Lastly the bearing is held in place by the pullies on the ends of the rod so you should never have to worry about them coming out.


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/VMRM2k2gUnL) &mdash; content and formatting may not be reliable*
