---
layout: post
title: "Print baby, print...I still need three more! (Across two printers)"
date: April 21, 2015 03:00
category: "Show and Tell"
author: Mike Miller
---
Print baby, print...I still need three more! (Across two printers)

![images/7f1c6861c20bb018541ecd660260e133.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7f1c6861c20bb018541ecd660260e133.jpeg)



**Mike Miller**

---
---
**Gus Montoya** *April 21, 2015 04:03*

Nice, looking good.


---
**Daniel F** *April 21, 2015 06:39*

what is your infill percentage? I'm  printing parts for Eustathios V2 and I printed the smaller parts in ABS with 80% infill. Have not printed the bed mounts yet  which are the biggest parts, maybe I could reduce infill for those to save some time and filament.


---
**Mike Miller** *April 21, 2015 11:05*

40%, with a flow of 115% for structural parts. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/6LjpGKpvxJk) &mdash; content and formatting may not be reliable*
