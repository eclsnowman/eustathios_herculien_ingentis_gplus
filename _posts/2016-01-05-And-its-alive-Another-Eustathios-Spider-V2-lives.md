---
layout: post
title: "And its alive! Another Eustathios Spider V2 lives"
date: January 05, 2016 22:55
category: "Show and Tell"
author: Nicholas Brown
---
And its alive!  Another Eustathios Spider V2 lives.  Finally got it printing, just dialing in the settings and working out a few final issues, but already impressed with the quality.  Thanks to all for their helpful posts in this group, **+Eric Lien** for his design, and also **+Ted Huntington** for his mega post ([https://plus.google.com/101412962363141430834/posts/c7t6cRUP2i8](https://plus.google.com/101412962363141430834/posts/c7t6cRUP2i8)) that helped me get it finally moving a few days ago.

![images/23078d0b4d2c9ca327128ac13851f4f1.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/23078d0b4d2c9ca327128ac13851f4f1.gif)



**Nicholas Brown**

---
---
**Ted Huntington** *January 06, 2016 00:48*

Looks great Nicholas- thanks for mentioning my megapost- I'm so glad that it isn't just endless rambling and could actually help somebody else :) Enjoy! I love this printer- I find that some prints get layer shifted at higher accelerations (3000 m/s^2) but most are fine at those high accelerations.


---
**Eric Lien** *January 06, 2016 03:48*

Wonderful job. I like some of the additions you made. Like the stepper heat sinks. Can't wait to see more posts about your experiences, and any improvements you might suggest. For example I am sold that the **+Walter Hsiao**​​ bed system you used (his mounts and ball screws) are the superior choice. I just need to get time to update the BOM and models.﻿


---
*Imported from [Google+](https://plus.google.com/106875376729457897724/posts/f9GMNphaiJN) &mdash; content and formatting may not be reliable*
