---
layout: post
title: "Techne build pics and first few prints :-D"
date: September 25, 2014 11:54
category: "Show and Tell"
author: Jarred Baines
---
Techne build pics and first few prints



:-D



![images/986ea3601ffedbb73700e8fe7dd9ad9e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/986ea3601ffedbb73700e8fe7dd9ad9e.jpeg)
![images/cca4f3191134b06eb14563f61db651d8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cca4f3191134b06eb14563f61db651d8.jpeg)
![images/fd8e8364168f4dfdc8564dbf5c6de8c3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fd8e8364168f4dfdc8564dbf5c6de8c3.jpeg)
![images/e5827bdccc7b0c72a201e7e8437b636a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e5827bdccc7b0c72a201e7e8437b636a.jpeg)
![images/21612d368b48858ae6b020b19d2bca8e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/21612d368b48858ae6b020b19d2bca8e.jpeg)
![images/a32a993e6f3365d01d18a0876919fcca.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a32a993e6f3365d01d18a0876919fcca.jpeg)
![images/f595c06a5c0385f5e97f5825e8120fd2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f595c06a5c0385f5e97f5825e8120fd2.jpeg)
![images/0354e1a29385b36306346d8b1ed6678f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0354e1a29385b36306346d8b1ed6678f.jpeg)
![images/61d34916ea5bc98937e5e77090213c2e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/61d34916ea5bc98937e5e77090213c2e.jpeg)
![images/5ea717f9105558511bc1657b4a027181.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5ea717f9105558511bc1657b4a027181.jpeg)
![images/936a0d18372fa2ed6d6fececac8f66ca.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/936a0d18372fa2ed6d6fececac8f66ca.jpeg)
![images/5884f798fce77a0a5867cb1f2fd1edef.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5884f798fce77a0a5867cb1f2fd1edef.jpeg)
![images/43b4f0501f96bbcc4c24f6092c757ec6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/43b4f0501f96bbcc4c24f6092c757ec6.jpeg)
![images/c3754a1e0ea6e4b2d272786204b288be.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c3754a1e0ea6e4b2d272786204b288be.jpeg)
![images/9ef5fd9e8c16d5708ae3ca91fd10f6c5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9ef5fd9e8c16d5708ae3ca91fd10f6c5.jpeg)

**Jarred Baines**

---
---
**Wayne Friedt** *September 25, 2014 12:25*

Those are some sexy looking parts you got there.


---
**Jarred Baines** *September 25, 2014 12:29*

Too heavy is right! I needed to upgrade to stronger stepper drivers - may even need to get large off-board drivers, I have leant a lot through doing this, I would change a fee things if I were to make it again - the build volume is about 300x280x650 - I would probably beef up the frame at the very least! 



The guys at work all use bulldog XXL (metal planetary geared) extruders on their printers and I tell you what, this original extruder from the ingentis plans is killing them - its probably the bigger motor and my awesome job of hobbing the M8 bolt ;-)


---
**Jarred Baines** *September 25, 2014 12:32*

Ha ha :-) cheers Wayne, I am still unsure whether I will annodise the parts or not! Would be blue or purple though, not gold :-p


---
**Eric Lien** *September 25, 2014 15:16*

Holy crap its beautiful. Nice job. I recommend the 60mm long nema 17 steppers from robotdigg. They are torque monsters. Never had a skipped step yet on #HercuLien.



You might still fight inertial resonance from the weight difference of aluminum to plastic. But looks like you could add some lightening pockets to help. The added inertia will cause ringing on fast direction changes. Also perhaps some angled braces or cables would help the frame.



All that said... Amazing work.﻿


---
**James Rivera** *September 25, 2014 19:12*

Wow!  SO.MUCH.METAL. This thing looks like a serious workhorse! Good job! I like the good old trusty herringbone Wade's pusher, too. :)


---
**Joe Spanier** *September 25, 2014 22:12*

Did you do all the machining? Parts are gorgeous. 


---
**Eric Lien** *September 25, 2014 22:58*

I have a pattern shop that I work with that wants to build a printer. Machining 1 off parts is kind of their gig. Can you share the files? I think they would be interested in a slightly less tall version.


---
**Jarred Baines** *September 26, 2014 09:38*

Lightening pockets I do plan on working on, even if its just some drilled holes - those parts ain't gonna break before the belts do! Some sort of cross bracing will be good but I realized I have to have the front open (or open-able) to remove prints! I will think about it though, I want to enclose it so perhaps I can design the enclosure to also brace it :-)



PLA only for now, just wanted to get her going!



I did do the machining myself, thanks for the compliments! I will up the files soon, I'll just check over then and try an export... Step work for people or native inventor files any better? My only deal is you have to show/tell me somehow if you use them :-) really been wanting to properly contribute for so long.


---
**Jarred Baines** *September 26, 2014 10:04*

Thank you :-) CNC machinist for the last 10+ years, so I should be good at that bit! Electronics took me a while to get right though :-)


---
**Jarred Baines** *September 26, 2014 12:32*

A batch of 1, could be very expensive... 10 or more might cost $1000 AU each? Closer to 50 they might come down to $200 or so, a lot of the cost is in setting up so multiples bring the price-per-part down a lot. It could well be less, but I'd have to look at how I process them, took me a lot of weekends at work to make this lot!



I will get you a more solid cost soon, a few people have shown interest so a larger batch may be a possibility!


---
*Imported from [Google+](https://plus.google.com/+JarredBaines/posts/j94ouxNGaqg) &mdash; content and formatting may not be reliable*
