---
layout: post
title: "Time to get the level out... So I checked the base for level and it is with a little adjustment"
date: August 05, 2015 18:31
category: "Discussion"
author: Brandon Cramer
---
Time to get the level out... So I checked the base for level and it is with a little adjustment. So from there I made the bed level. When I place the level on the smooth rods, they are not exactly level. Is this normal?





**Brandon Cramer**

---
---
**Chris Brent** *August 05, 2015 18:40*

You're not trying to make the bed level with the ground or table, you're trying to make in planar with the extruder. This means you can use the extruder as a reference. Here's a good explanation:


{% include youtubePlayer.html id=ED1Gxvw2Rmw %}
[https://www.youtube.com/watch?v=ED1Gxvw2Rmw](https://www.youtube.com/watch?v=ED1Gxvw2Rmw)


---
**Brandon Cramer** *August 05, 2015 18:45*

That makes much more sense than what I was trying to do. :) 


---
**Eric Lien** *August 05, 2015 20:52*

Yup what **+Chris Brent** said :)


---
**Brandon Cramer** *August 05, 2015 21:47*

Is this Z Endstop in the correct place? I'm asking because I'm not able to tighten the wing nuts enough on the bed when I try to level it against the extruder The wing nuts are on the verge of just barely being tight. Any vibration and they will fall off. 



[https://www.dropbox.com/s/b4dnyi8c4422el6/2015-08-05%2014.43.04.jpg?dl=0](https://www.dropbox.com/s/b4dnyi8c4422el6/2015-08-05%2014.43.04.jpg?dl=0)



Wing Nuts:



[https://www.dropbox.com/s/cs9094fnfj0rc09/2015-08-05%2014.45.54.jpg?dl=0](https://www.dropbox.com/s/cs9094fnfj0rc09/2015-08-05%2014.45.54.jpg?dl=0)


---
**Eric Lien** *August 05, 2015 22:07*

Try a washer on top of the printed piece to move the spring out of the pocket. Perhaps I need a longer spring in the BOM. I tried to find one comperable to what I used (but I already had them so found the PN in the BOM on Mcmaster).


---
**Brandon Cramer** *August 05, 2015 23:51*

**+Eric Lien**  It might help if I have the glass on the bed. What are you using to hold the glass in place?


---
**Eric Lien** *August 06, 2015 00:07*

Printed ABS clips. I hate those bulldog clips.


---
**Brandon Cramer** *August 06, 2015 00:09*

Got an stl for them? 


---
**Eric Lien** *August 06, 2015 00:18*

I made them so long ago I don't. I will look at adding them when I get time.


---
**Frank “Helmi” Helmschrott** *August 06, 2015 07:55*

Do ABS clips really work here? With a bed at 100°C or more the ABS should have problems still having force to clip after while, doesn't it?


---
**Eric Lien** *August 06, 2015 11:54*

**+Frank Helmschrott** they have function well for over a year for me. I just made them thick for strength. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/9PA9erKHbCu) &mdash; content and formatting may not be reliable*
