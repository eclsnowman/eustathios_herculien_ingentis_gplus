---
layout: post
title: "Guys, i'm a bit stuck with my Extrusion and hope to find some help"
date: August 05, 2015 15:00
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Guys, i'm a bit stuck with my Extrusion and hope to find some help. I'm using a Bondtech Extruder on my Eusthatios (Bowden setup, around 80mm bowden tube currently, already had it shorter and a bit longer too) and en E3Dv6 of course. Bascially stuff works quite well with ABS and Colorfabb PLA. I also don't have big problems with some other cheaper PLA i had around.



Unfortunately i'm seeing problems with other Filaments that seem to repeat themselves and i already have tried playing around with but can't seem to find the real cause. My latest and most weird problem is with eSun PETG (transparent blue) that i just ordered at Amazon Germany the other day as i read about it here and thought i try it. 



I tried it with 0.4 and 0.6mm nozzle which doesn't seem to make a difference and as far as i could investigate the problem sits above the nozzle. After printing ok for quite a while the filament extrusion stops, i hear the extruder losing steps or fighting any other problem. The result then mostly is because of the enormous grip from the Bondtech extruder the filament gets folded and knoted inside the extruder. I've attached an image to show what it looks like. This happens just below the upper "outlet" of the bowden tube. I think this is not the cause of the problem but just the result. After seeing this i pulled out the bowden tube from the extruder, pulled the filament out the tube and tried to hand push it through the extruder to find out if there's some clogging in there... nothing. Could move it nice and smooth.



I'm a bit stuck with such situations as i have already seen this with Woodfill, Brassfill, BioFila Linen and now the PETG from Esun. From what it looks is that this happens with Filaments that are just a little bit softer then usual. Also going slow doesn't seem to help a bit here. I basically know the porblems with flexible filaments but these are in no way flexible.



Who has seen similar problems? Any idea? Maybe the cooling for my E3D doesn't work quite well and causes it to block somehow or just to increase force after a while of printing that causes this? 



Any idea and input is highly appreciated. I thought i'll post this here in the group first before putting in the bigger 3d printing group.



EDIT: I forgot to add that i already had the suspect that the pressure on my Bondtech might be set too tight and completely losened it. I then slowly moved forward closing the screws just enough to get a decent pull force so i could not stop it by hand. So this should also be not a real problem.

![images/7418425f87250f4ca3a08c61b86d0bf3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7418425f87250f4ca3a08c61b86d0bf3.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**Jim Wilson** *August 05, 2015 15:08*

I am actually having the identical symptoms out of nowhere at the moment, but haven't had the time to fully troubleshoot. I'll be eyeballing any replies and trying any suggestions as well.


---
**Krijn Schaap** *August 05, 2015 15:08*

What kind of settings do you use with Colorfabb PLA? I'm not using a bondtech extruder but an EzStruder, but am using a similar size bowden tube and an e3d. E3D suggested it might a retraction length issue, but that didn't seem to make much of a difference.


---
**Frank “Helmi” Helmschrott** *August 05, 2015 15:14*

**+Krijn Schaap** the settings don't verify so much to any other filaments. Against everything i heard about that little retraction distance that the E3D needs even on bowdens i still have oozing with critical filaments below 6mm of retraction distance. The thing is that my problems sometimes occure relatively early in print without much retraction happening.


---
**Sal42na 68** *August 05, 2015 15:53*

Have you tried a higher temperature setting on hotend? I run my cheap pla at 210c. 


---
**Chris Brent** *August 05, 2015 16:10*

Maybe the heat is creeping up the heat break and making the filament soft?


---
**Eric Lien** *August 05, 2015 16:59*

To get high speeds on Esun PETG I run 255C for my hot end temp. Can you confirm what temps you are running. If it jams like that, you have a greater back pressure than the compression strength of the filament and it buckles. So I say slow down, or up the temp first.


---
**Eric Lien** *August 05, 2015 17:07*

Also what is the bend radius like on your Bowden tube. If it is too tight you get artificial back pressure from the filament friction inside the tube.


---
**Vic Catalasan** *August 05, 2015 18:42*

I only have this problem when my hot end temp is not within range of the filament OR my heat break and nozzle gets junk in them. I have spare heat breaks and nozzle and swapping them out typically fixes this problem. I torch the dirty ones and clean them with acetone. What I really need to do is find a way to measure the amperage of the stepper on the extruder so it can indicate how hard it is working. It will take a large force and amp draw to bend that filament in a confined space. With a feedback sensor it would be nice to have and kill the job because no printing will happen with a mangled filament.


---
**Frank “Helmi” Helmschrott** *August 05, 2015 21:31*

thanks guys. this is all highly valuable input. I think I'm good with the temps, but I will test playing with them. **+Eric Lien** Bowden tube indeed is something I'm not lucky with yet. don't think I have a radius that is too tight but I'm not lucky with the fact that it's not a stable arc at all. I will shorten it down a bit and try to find a way to control how it moves a bit more. 


---
**Sal42na 68** *August 05, 2015 21:38*

When I use cheap pla and its been out of its original package it clogs about 2 to 5 minutes in. You can hear the water popping in the hotend. The hotter temp works for me so far. 


---
**Vic Catalasan** *August 05, 2015 21:40*

I ran into that Bowden tube dangling so I used a thin piano wire mounted to the frame close to my Bondtech extruder , you only need about 1/3 of the length to keep the Bowden upright. Never had any issues after doing this.


---
**Sal42na 68** *August 05, 2015 21:47*

Picture please


---
**Eric Lien** *August 05, 2015 22:04*

**+Frank Helmschrott** my bowden is around 950mm if that gives you an idea of how my ARC would look. You said 80mm but I assume you mean 800mm? If so you may need it longer not shorter. Also I recommend a guide wire about 60% of the way up the arc of the bowden to keep it upright (Like **+Vic Catalasan** mentioned, but I used a metal coat hanger down into the center hole on the front right vertical corner extrusion next to the extruder.)


---
**Frank “Helmi” Helmschrott** *August 06, 2015 04:25*

**+Eric Lien** ok, 950mm is really long. I'll check mine again and probably need to create some space first - there's a shelf above that could be in the  way for such a big arc. I should have stuff around that works as a guide wire. I'll see how that works out - maybe that will already solve some problems.


---
**Oliver Seiler** *August 06, 2015 06:14*

**+Frank Helmschrott** I tend to have the same problem with t-glase, but interestingly enough only the red flavour which prints fine for about 2 hours then clogs in the extruder. Transparent t-glase and PLA work fine for me.

I've tried cleaning and seasoning the hotend, which seems to have helped a bit but wasn't a permanent solution. 


---
**Frank “Helmi” Helmschrott** *August 06, 2015 06:24*

**+Oliver Seiler** this is really interesting. For me HDglass (which also basically is PETG) works quite well but it also seems to be stiffer and more porous on the spool than the relatively soft feeling eSun PETG. In your case maybe the coloring pigments make a difference in material behaviour or you just got it from a bad batch?


---
**Oliver Seiler** *August 06, 2015 07:08*

**+Frank Helmschrott** I think in my case it's related to the pigments. But having said that, there seems to be a general issue or tendency for the filament to not run very smoothly through the system. It looks like a number of people are having problems with full metal hotends in general. It might pay to have a good look into your's and make sure it's really smooth inside (some people have reported a rough surface) and doesn't have any residue stuck to it.

On a side note, I've retrofitted an E3D to my old printer and now face issues with under-extrusion as the extuder can't build up enough force.

For my red t-glase I might have to run a few more trials with different temperature settings. It took me some time to work through a range of other issues before narrowing it down, for example the bowden couplings used to work themselves loose due to retractions until I printed and applied little clips to make them stop moving around.

Wokring through these issues methodically takes a lot of spare time and I simply don't have that much...


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/SzFEK2SnLLx) &mdash; content and formatting may not be reliable*
