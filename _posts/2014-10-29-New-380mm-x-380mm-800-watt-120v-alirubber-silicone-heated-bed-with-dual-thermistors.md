---
layout: post
title: "New 380mm x 380mm (800 watt 120v) alirubber silicone heated bed with dual thermistors"
date: October 29, 2014 03:45
category: "Show and Tell"
author: Eric Lien
---
New 380mm x 380mm (800 watt 120v) alirubber silicone heated bed with dual thermistors. 3/4" MDF routed insulator bottom board. 1/4" aluminum heat spreader. Omron SSR.



Just sprayed the first coat of high temp enamel black paint on the MDF after taking the picture.



I am excited to complete the build so I can install this upgrade to #HercuLien and run a PID tune.





![images/d1d631b49dc190bd6c7f1ce9a88ca079.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d1d631b49dc190bd6c7f1ce9a88ca079.jpeg)



**Eric Lien**

---
---
**Brandon Satterfield** *October 29, 2014 04:16*

Always excited to see what comes out of the Lien factory!!!


---
**Wayne Friedt** *October 29, 2014 07:32*

What do you do with dual thermistors?


---
**Dan Kirkpatrick** *October 29, 2014 07:49*

Redundancy.  Dual thermistors cover you if there is an open circuit on a thermistor, preventing the bed from getting too hot.


---
**Jean-Francois Couture** *October 29, 2014 11:59*

That thing is going to be a power house ! just make sure you don't heat the glass to fast ;-) Can't wait to hear your feedback !


---
**Eric Lien** *October 29, 2014 12:32*

**+Jean-Francois Couture** I am a little worried about the glass.


---
**Jean-Francois Couture** *October 29, 2014 12:39*

**+Eric Lien** Unless you have borosilate glass (high temp glass like stove or a car window) to many watts/in² could result in glass shatter if the temp is ramped to fast, you have to ajust the firmware to compensate.


---
**Eric Lien** *October 29, 2014 13:26*

I got a 600W as a spare just in case :)


---
**Matt Miller** *October 29, 2014 13:27*

Do not worry about the glass.  I've got a 1000W AC silicone mat and a home depot mirror tile on top of it (5 for $10 or something).  Zero issues.


---
**Jean-Francois Couture** *October 29, 2014 13:41*

**+Matt Miller** and you ramp up the heat as fast as possible ? with 1000W, it should take less than 2min. to got from 21C to 110C


---
**Mike Miller** *October 29, 2014 14:16*

This is why you enclose the build platform. :D


---
**Matt Miller** *October 29, 2014 14:59*

**+Jean-Francois Couture** yes.  I'll look at the PID settings and post them, but 20C to 90C in less than 2 minutes.



I can run a test tonight to give a more definitive timing.


---
**Matt Miller** *October 30, 2014 11:23*

Or tomorrow morning....



Without the heated chamber on, 20C to 110C  = 3min 31 sec.



With the heated chamber on, 36C to 110C = 2min 42sec



Kp 51

Ki 1.76

Kd 368.5


---
**Eric Lien** *October 30, 2014 12:12*

**+Matt Miller** what are you using as an insulator below the bed?


---
**Matt Miller** *October 30, 2014 12:25*

Nothing.  I really should have something under there but it's a moving Y and extra mass is my enemy. 


---
**Wayne Friedt** *October 30, 2014 14:00*

**+Eric Lien**  How are you wiring the 2 thermistors?


---
**Eric Lien** *October 30, 2014 15:14*

**+Wayne Friedt** at first I was thinking just as a backup for failure then I would just swap leads. But if anyone knows how to use two: one as primary and one as a backup(check for a difference) I am all ears.


---
**Jean-Francois Couture** *October 30, 2014 15:40*

**+Eric Lien** If you see all 3 hotends on the LCD, connect to that input and you can compare by eye. ;)


---
**Wayne Friedt** *October 31, 2014 00:31*

**+Eric Lien** Ah i see. I have had that happen before where the thermistor stopped working and i just stuck another one under the bed. Works fine. Cant tell any difference if there is any. One thing as i suspect you guessed is anyway you wire those two thermistors together will change the resistance.


---
**Eric Lien** *October 31, 2014 00:46*

**+Wayne Friedt** the azteeg x3 has plenty of extra thermistor inputs. I just don't know how to handel it in firmware to use one as a watchdog for the other.﻿


---
**George Salgueiro** *November 11, 2014 19:15*

Just get a tem regular glass. The temperated glass will shatter because of the tension inside the material. 


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/B2jDZq3REHn) &mdash; content and formatting may not be reliable*
