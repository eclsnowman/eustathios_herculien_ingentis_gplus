---
layout: post
title: "Hi! Am going to have some free time in a couple of weeks and am planning on building another printer (I already built a smartrap and a rostock)"
date: June 27, 2015 12:15
category: "Show and Tell"
author: Javier Prieto
---
Hi! Am going to have some free time in a couple of weeks and am planning on building another printer (I already built a smartrap and a rostock). I would like it to have 250 mmˆ3 square mm printing volume.The type of printer would be some similar to the ones you have, but would like to have the motors inside (to close it with wood panels) and also without closed belts (instead a closed+open belts, I am going to use only a open one, like Prusa and delta). Also, the Z motor will have an industrial screw atached with a flexible coupling (instead of the belt and the threaded rod). Have you tried it before? Which model do you think could fit those needings? Does all of them use 10 mm rod? I will use 8 mm rod in the square perimeter (guides) and 6 mm rod in the cross.

Thanks in advanced, and will upload photos soon :)





**Javier Prieto**

---
---
**Eric Lien** *June 27, 2015 13:54*

Eustathios would be a great starting point. It Spider V2 is just under 290x290x290 usable. But I think 8mm side and 6mm cross rods may have too much deflection.


---
**Daniel F** *June 27, 2015 16:12*

If you put the steppers in the base, you could also use normal belts and create a loop with fixing them together as the distance from top to down will be bigger than x resp. y travel. You just need to allign the position of the carriage and the vertical belts fixing position.


---
*Imported from [Google+](https://plus.google.com/103033503642891532798/posts/Xu7DJHV4Deg) &mdash; content and formatting may not be reliable*
