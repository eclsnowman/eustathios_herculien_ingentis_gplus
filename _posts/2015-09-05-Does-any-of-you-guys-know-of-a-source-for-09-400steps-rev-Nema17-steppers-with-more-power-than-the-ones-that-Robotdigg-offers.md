---
layout: post
title: "Does any of you guys know of a source for 0,9/400steps/rev Nema17 steppers with more power than the ones that Robotdigg offers?"
date: September 05, 2015 06:44
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Does any of you guys know of a source for 0,9°/400steps/rev Nema17 steppers with more power than the ones that Robotdigg offers? ([http://www.robotdigg.com/product/241/0.9%C2%B0+Step+Angle+Nema17+48mm+Stepper+Motor](http://www.robotdigg.com/product/241/0.9%C2%B0+Step+Angle+Nema17+48mm+Stepper+Motor)) I'm sure they're good enough for normal use but i see too many lost steps on X/Y due to just small "crashes" with existing material. It's too much if only the nozzle touches some parts a little bit which could happen quite easily on some prints. I had one 15 hour print failing 5 times now and that's enough..



I think the fact that the bushings are quite sensible on misalignment and cause relatively high force easily is a bit too much for these motors in this kind of construction.



I know i could use a 200steps/rev motor instead and use the ones that **+Eric Lien** suggests but if anyhow possible I'd like to keep my high resolution and amazing silence of the motors.





**Frank “Helmi” Helmschrott**

---
---
**Whosa whatsis** *September 05, 2015 08:02*

Aside from the *$&# bare wire ends, these are great motors: [https://www.sparkfun.com/products/10846](https://www.sparkfun.com/products/10846)


---
**Frank “Helmi” Helmschrott** *September 05, 2015 08:22*

Thanks **+Whosa whatsis** but at least from the values they look to be the same torque as the ones from Robotdigg which basically are surely also great motors but i just could use some more Power on my Eusthatios Spider V2.


---
**Whosa whatsis** *September 05, 2015 09:27*

Well, not quite the same, but within 10%, yes. I could have sworn Robotdigg's .9 degree steppers were lower-torque the last time I checked...



Anywho, you'll be hard pressed to find .9-degree NEMA17 motors with a higher torque rating than that. The ones from Kysan, Stepperonline, and Lin Engineering top out at pretty much the exact same rating. To go higher, you'll probably have to step up to a 60mm-long stepper body, and I've never seen a .9 degree stepper that size. Just not enough demand for them, I guess. Let me know if you find one, though!


---
**Frank “Helmi” Helmschrott** *September 05, 2015 09:29*

Yeah that exactly is what i'm looking for (60mm). Maybe i have to switch back to 1.8° steppers then to get a higher torque. Not too much of a problem with the 1/128 drivers but i didn't want to step back. 


---
**Matt Miller** *September 05, 2015 11:25*

**+Frank Helmschrott**​ I have those sparkfun steppers and they function perfectly.  They can soak up more current than the usual a4988 can deliver, drv8825 works fine though.  What driver are you using?



Food for thought: [http://reprap.org/wiki/Step_rates](http://reprap.org/wiki/Step_rates)



And from the wiki: [http://www.micromo.com/microstepping-myths-and-realities](http://www.micromo.com/microstepping-myths-and-realities)



Threadjack:  **+Whosa whatsis**​, if steppers are rated at +-10% for accuracy from the manufacturer what is the benefit of microstepping above 8x?


---
**Stephanie A** *September 05, 2015 13:59*

Noise. Resonance.


---
**Eric Lien** *September 05, 2015 14:49*

I had the 0.9 on Eustathios at first and had the same missed steps. The issue is not motion, but when you hit something on a cross rod gantry like Eustathios is side loads the bushing. Bushings handle sideloading very poorly. You could try the lm8luu carriage version which handles it better. Or go to the high torque steppers I now use. I have had zero issues with the nozzle catch shifting after using these beefier steppers. One note is I use the TI drivers which handle the higher current better, but at the expense of higher noise levels of the driver.﻿


---
**Eric Lien** *September 05, 2015 14:51*

Also I have noticed no difference in part quality between 0.9 at 1/16 and 1.8 at 1/32.


---
**Frank “Helmi” Helmschrott** *September 05, 2015 14:54*

**+Eric Lien** what you said about side loading is exactly what i was planning to say with my poor english :) I'm sure i do have the same problem that you had. Think i'll go with the 60mm/1,8° for now and see if there will be other options later. I would definitely like to stay with the much more silent bushings over linear bearings.


---
**Ishaan Gov** *September 05, 2015 15:38*

**+Matt Miller**, there actually is no gained accuracy microstepping above 8; a **+Stephanie S** said, you get smoother motion and less resonance, but only if your driver is tuned right (decay, current, etc)


---
**Whosa whatsis** *September 05, 2015 19:18*

Higher microstepping rates won't really get you better precision, but they can give you smoother surfaces if a bunch of other factors (that are difficult to tune) are tuned properly.



**+Frank Helmschrott** Are you really using .9 degree steppers with 1/128 motors? What firmware are you using? That has to be _severely_limiting your speed because none of the controllers I know of can handle a step rate high enough to get decent speed with that many steps/mm. I guess if you're using enormous belt pulleys, that would mitigate it, but that still seems like it would result in way too many steps/mm.


---
**Frank “Helmi” Helmschrott** *September 05, 2015 19:42*

**+Whosa whatsis** I'm on 32bit with an Arduino Due and the Radds board. Speed is not a real problem. 


---
**Whosa whatsis** *September 05, 2015 20:09*

The clock rate on the Due is only about 5x as fast, and although it can do some things in fewer cycles than an 8-bit microcontroller (mostly the types of things you try to avoid doing in a stepper interrupt anyway), there are lots of reasons that it might not handle a 5x higher step frequency well. Your step rate is 8-16 times higher than on a typical build with one of the 8-bit controllers.


---
**Frank “Helmi” Helmschrott** *September 06, 2015 06:31*

I'm currently going 1280 steps/mm on X and Y and all i can say is that it works quite good. But I'm not the guy who can argument about the facts behind. Maybe **+Roland Littwin** would be the one - he does the Repetier Firmware and i'm in close contact with him for testing new features and talking about possible changes. I've never heard any doubts regarding step resolutions that high.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/DzLaXcUMY8A) &mdash; content and formatting may not be reliable*
