---
layout: post
title: "hey.. been following this forum and i have seen a lot of build logs and improvements etc ..."
date: July 14, 2015 14:36
category: "Show and Tell"
author: Godwin Bangera
---
hey..



been following this forum and i have seen a lot of build logs and improvements etc ... which is superb ... i am in the process of putting both eric's machines together (could not decide which printer to build so decided to build both of them  :P        )



but one thing i really have not seen (or am i missing something) is the Gallery or Showcase for these printers ... the prints which these machines are producing everyday or whenever .. not the initial prints but the prints which are being produced once these machines have settled ...



what do you think Eric and fellow Eustathiosian's and HercuLien's ?? A gallery or showcase would be super and any other points like filament settings etc? Any thoughts ?





**Godwin Bangera**

---
---
**Eric Lien** *July 14, 2015 18:07*

This is on my printer right now: [http://imgur.com/xIkscHZ](http://imgur.com/xIkscHZ)



As to what it is.... That's top secret ;)


---
**Godwin Bangera** *July 14, 2015 20:35*

very nice


---
*Imported from [Google+](https://plus.google.com/102290637403942484150/posts/UiTmE5PnUMg) &mdash; content and formatting may not be reliable*
