---
layout: post
title: "What lengths of 2020 do I need to cut for a default ingentis"
date: December 03, 2014 02:17
category: "Discussion"
author: Ethan Hall
---
What lengths of 2020 do I need to cut for a default ingentis. It doesnt mention on the Bom and Tim on YouMagine said the printer was 465 cm cubed so im assuming 445cm but i just wanna double check before I order from aliexpress

Also im having touble finding a 110v heated bed for the ingentis. The build area is 300mm x 300mm so how large of a bed should i get relative to that?

Eric suggested I should get an ac to dc ssr from omron and i was just wondering what spec I should aim for with that?

Thanks!





**Ethan Hall**

---
---
**Mike Thornbury** *December 03, 2014 04:26*

It depends on how you construct it. Get all 465 and cut them as necessary


---
**Daniel F** *December 03, 2014 07:48*

heat bed can be ordered from ali rubber, it's custom made. You tell them size, power, and if it should be AC or DC, 110V or 220V. They can eastimate the temp. it can reach based on these values. My heteater is 330X330, cost aroung 40$ including shipping to CH, the bed is 350x350, that leaves 1cm around the heater to mount the bed (3 point leveling with springs directly on the 6mm aluminium plate. Proved to work but you loose some heat around the edges. It's a good idea to put some thermal isolation at least below, better around the build plate to keep the heat where you want it.

**+Eric Lien** embeded the heater in an MDF board, see here: [https://plus.google.com/109092260040411784841/posts/B2jDZq3REHn](https://plus.google.com/109092260040411784841/posts/B2jDZq3REHn)


---
**David Heddle** *December 03, 2014 21:26*

Here is a link to the BOM I made up for my Ingentis build its slightly more complete than the one Tim has linked in his blog. [https://docs.google.com/spreadsheet/ccc?key=0AkIDRYw_FT29dHlPSDdTTmxqZ2FvWVJDUS1iN3cta2c&usp=sharing](https://docs.google.com/spreadsheet/ccc?key=0AkIDRYw_FT29dHlPSDdTTmxqZ2FvWVJDUS1iN3cta2c&usp=sharing)  


---
**Ethan Hall** *December 03, 2014 22:43*

Thanks everyone!


---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/WnwfF7ugCwn) &mdash; content and formatting may not be reliable*
