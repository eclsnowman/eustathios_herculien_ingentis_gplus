---
layout: post
title: "would someone be able to give me info on assembling the hercustruder?"
date: July 10, 2015 04:25
category: "Discussion"
author: Gunnar Meyers
---
would someone be able to give me info on assembling the hercustruder?  I was not able to find info on assembly on the build guide.  The end is nearly in sight.





**Gunnar Meyers**

---
---
**Eric Lien** *July 10, 2015 11:19*

Sure. What questions do you have.


---
**Eric Lien** *July 10, 2015 11:21*

This image explains most of it: [https://d36c0vbvwjb9cx.cloudfront.net/uploads/image/file/53553/HercuLien__2_.png](https://d36c0vbvwjb9cx.cloudfront.net/uploads/image/file/53553/HercuLien__2_.png)


---
**Gunnar Meyers** *July 10, 2015 17:13*

Thanks. I must have passed right over that. 


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/9aNEbgFhWLA) &mdash; content and formatting may not be reliable*
