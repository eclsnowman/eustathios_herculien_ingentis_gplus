---
layout: post
title: "Some movements from my Eustathios build. X/Y axes and carrier looking good [besides some wobble in the shafts until I get my replacements]"
date: April 14, 2015 09:34
category: "Show and Tell"
author: Oliver Seiler
---
Some movements from my Eustathios build. X/Y axes and carrier looking good [besides some wobble in the shafts until I get my replacements]. The bed is also almost done and just needs to be put in place when the new Z screws arrive.

Can't wait to finish this and also I love the late nights in my mancave now as we head into winter here in NZ.



![images/90928030d1e125b52a3417200ed44715.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/90928030d1e125b52a3417200ed44715.jpeg)

**Oliver Seiler**

---


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/bZaVvVR44Fw) &mdash; content and formatting may not be reliable*
