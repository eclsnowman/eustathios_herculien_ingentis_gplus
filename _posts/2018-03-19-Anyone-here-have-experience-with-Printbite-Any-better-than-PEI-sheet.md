---
layout: post
title: "Anyone here have experience with Printbite? Any better than PEI sheet?"
date: March 19, 2018 15:39
category: "Discussion"
author: Ryan Fiske
---
Anyone here have experience with Printbite? Any better than PEI sheet? 





**Ryan Fiske**

---
---
**Gus Montoya** *March 19, 2018 17:48*

DO not buy it...I made the mistake and when I inquired the seller for some assistance. He had the worst...but I do mean worse customer service response ever. Arrogant, obnoxious, condescending and straight out liar. I've had better luck with bare glass then using his fake product. Another poor girl who asked from assistance got the most sexist remarks. My 2 cents. (Revised)


---
**j.r. Ewing** *March 19, 2018 18:46*

Wow


---
**Scott Hess** *March 19, 2018 19:07*

I recently replaced the Buildtak on my Monoprice (well, I got the Wanhao version, since it's 1/3 the price), and I'm kind of thinking next time I want to get a PRINTinZ plate.  It's one of those flexible plates you put on with binder clips.  The only negatives I could find in reviews were the thickness (4.5mm) and that if you drop a hot extruder into it it will melt a hole.



I only print PLA, though, and my understanding is that PLA is really in the design zone for this item.



[I'm also hoping someone comes out with a clone of the new Prusa removable build plates...]


---
**Gus Montoya** *March 19, 2018 20:20*

**+Scott Hess** After tomorrow, I'll be able to make the removable plates for this group. Just need some minor finishing on my CNC. **+Eric Lien** Any chance you can point me  to someone with the patients of god to help me iron out my last bugs from my machine. It's up and running. I can give further details off this forum. Don't want to hog the space.




---
**Ryan Fiske** *March 19, 2018 20:25*

**+Gus Montoya** you're making removable plates?!? I'm interested in hearing more about that, you'll have to excuse me, I'm on mobile and can't do a search of the group's previous posts.


---
**Gus Montoya** *March 19, 2018 20:46*

**+Ryan Fiske** That's the plan, I can cut the plate to size. But it will be up to the buyer to purchase the build surface ie: buildtak etc and mount it. I'm working on a kit and have purchased the parts for it. 


---
**Eric Lien** *March 19, 2018 21:50*

**+Gus Montoya**  If the CNC is an OX or R7 those G+ groups are likely the best place to seek support. There are some great people in the group over there. And I always believe in asking the questions out in the open, that way your questions, and the subsequent group answers help everyone.


---
**Gus Montoya** *March 19, 2018 21:52*

**+Eric Lien** It's not an ox it's a c-beam from open builds which use all the same components. I'll look for the groups. thxs


---
**Mutley3D** *March 20, 2018 01:15*

Hey **+Ryan Fiske** PrintBite is an alternative. Parts self release during cooldown, and its a lot more durable than PEI and works with wider range of materials. PB has a lot of good reviews, I wouldnt sell it otherwise. I am accessible, and support my product and customers.



I can only say the comments and allegations made above are a combination of exageration, offensive fabrication and complete poppy, I shant even even warrant them with a response only to say BS comments on FB are not a support enquiry. Large pinch of salt.




---
**Eric Lien** *March 20, 2018 03:19*

**+Mutley3D**and **+Gus Montoya**. I have known both of you in some capacity on G+ for some time. My intention is always to give a place for peoples opinion, but in a civil discourse. 



I ask two things.



Gus, please remove the "punch him in the face" comment. That is uncalled for in a civil discussion. If you stand by your claims of the product... I will not remove your opinion. But please note, your have made a bold claim, please consider if your convictions match your words.



Mutley3D, I know you mentioned to me directly you do not feel like this was an accurate depiction of the events as you remember them. I was not in this transaction in any way, so I will not interject much beyond my role as a moderator. But I understand, from a brand perspective and as a reflection of your character you feel the comments are unjust.



This particular post is not the place to hash out a business grievance. I suggest you both speak directly if there was an issue with a product, or just a perceived one. The internet and typed replies rarely convey nuance. I suggest a reset and talk through what could be done to remedy the situation for both parties. 


---
**Mutley3D** *March 20, 2018 06:54*

Easy tiger, Easy. This guy spews a barage of bull, calls me a liar along with a wish of violence and you say a typed response rarely conveys nuance lol. For real or candid camera?



+Gus Montoya there was NO contact from you so thats a lie your telling, but you call me a liar. You posted a thread on facebook flaming the product before you had used it. Someone pointed the thread out to me. You misunderstood the english humour in my response on that post, which included a sarcastic "wtf" and you went off like a rocket. You then posted a fake picture claiming the product was the wrong size whilst the graphic on the PrintBite clearly showed it was correct, and you refused to post a full picture to back up your claim. You were caught out lying there too. There was no sexist remark. All these claims made you by, yet you call me names? You didnt respond to the emails I sent to try and recover the situation a few days after. Now you write the BS above. Shame on you! The only fake here is the BS you wrote. Heres hoping you get a customer that behaves like you have, in the meanwhile a thousand pigeons pooping on your car every day for a week will do. Yes, only a week.



As you were :)



PS: why not try fitting the PB you bought, you might find yourself pleasantly surprised as it works very well. If you dont your mising out and biting your face off to spite your nose (English humour mate, relax). Like I said, I wouldnt sell it otherwise. Lasts a heck of a lot longer the PEI, and many many people swear by it, not just the reviews customers have left on my site, but all around the net, and reputable reviewers too. That is the only reason I sell it, because it Works!

As you were!


---
**Mutley3D** *March 20, 2018 06:55*

+Ryan Fiske bet you wish you hadn't asked ;) lol




---
**Gus Montoya** *March 20, 2018 08:02*

**+Eric Lien** NP I’ll revise my comment. But I stand by my words. 


---
**Gus Montoya** *March 20, 2018 08:18*

**+Mutley3D** from all that jibber jabber I still think your correct. Ryan has the right to try out the product. I hope he has a better experience. One only can wish for a good outcome because the alternative is just wrong. 


---
**Mutley3D** *March 21, 2018 16:52*

**+Gus Montoya** You may not realise or remember, but I actually provided you some help quite some time ago before you fell on some hard times 2015/16. As per my comment above, you did not contact me directly, but posted on facebook, and then flew off the handle at me. One can hardly expect a bunch of roses under the circumstances.



Now you have told several lies on two occassions, aswell as attack my character with more lies. That isnt cricket! Its slander. I get it, your pissed. So, why not respond to one of my emails, and lets see if I can turn this situation around for you. Unhappy customers dont sit well on my conscience. I have very few of them.



Send me an email reply, and ill call off the pigeons :)


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/f1PGPFi8QqS) &mdash; content and formatting may not be reliable*
