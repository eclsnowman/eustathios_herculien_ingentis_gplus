---
layout: post
title: "Not the most pleasant surprise powering on my Azteeg X5 GT :( I followed all the instructions and all wires were perfect"
date: October 03, 2017 00:02
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Not the most pleasant surprise powering on my Azteeg X5 GT :(



I followed all the instructions and all wires were perfect. Ran the E1 motor and the driver blew. It took the board with it.

Right before the board spat warnings (see img)





![images/59c3fa470b4adcea710afb90cfec7058.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/59c3fa470b4adcea710afb90cfec7058.jpeg)
![images/116d95bf1e01bb02225095310c016e46.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/116d95bf1e01bb02225095310c016e46.jpeg)

**Stefano Pagani (Stef_FPV)**

---
---
**Stefano Pagani (Stef_FPV)** *October 03, 2017 00:03*

![images/13094f52deaeed91a717639496bddcc5.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/13094f52deaeed91a717639496bddcc5.png)


---
**Ryan Carlyle** *October 03, 2017 02:44*

Difficult to blow a 2660... they have all sorts of protection features. Was this the first power-on for the system? Might've been a solder short somewhere. 


---
**Jeff DeMaagd** *October 03, 2017 12:55*

The driver chip says "GEPMANY". I suspect Panucatt's board manufacturer used a counterfeit chip.


---
**Ryan Carlyle** *October 03, 2017 13:36*

**+Jeff DeMaagd** I saw that but just assumed it was a bad label etch job. Now that I look closer, it does look like an actual P. Probably counterfeit. 


---
**Jeff DeMaagd** *October 03, 2017 13:51*

It's worth looking at the other drivers at least. **+Panucatt** **+Roy Cortes**


---
**Roy Cortes** *October 03, 2017 18:04*

As far as I know the manufacturer sourced the chip from an authorized Trinamic regional distributor I specified. Not sure why it says GEPMANY, the ones I checked all have GERMANY on them.



The burn on the chip seem to be where the + VSB supply pin is at, Im suspecting there is a short either in the output or supply line for Channel B (which the output messages seems to indicate as well)



I'll contact you for a replacement.


---
**Jim Stone** *October 04, 2017 22:17*

Roy always trying his best to make things right :)


---
**Stefano Pagani (Stef_FPV)** *October 04, 2017 23:28*

Thank you so much **+Roy Cortes**! I will check for shorts as well.


---
**Stefano Pagani (Stef_FPV)** *October 27, 2017 01:59*

**+Roy Cortes** let me know when you can shoot me a email stefano.pagani@verizon.net


---
**Roy Cortes** *October 27, 2017 03:11*

**+Stefano Pagani** Sent you an email using that email a day or two ago. if you did not get it email me roy@panucatt.com. Thanks


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/EKHtn29D8Ja) &mdash; content and formatting may not be reliable*
