---
layout: post
title: "I just finished up building this Printer Kitten"
date: December 21, 2016 13:04
category: "Build Logs"
author: Patrick Woolfenden
---
I just finished up building this Printer Kitten. This one got an E3D Titan. The Bondech is still my favroit extruder though. A 3Dbenchy as the first print. Polymaker Poly Plus, 0.1mm layer height



![images/2b2db664aa2e31324acb6f80ed5f5c66.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2b2db664aa2e31324acb6f80ed5f5c66.jpeg)
![images/367747e41f4550f39431dc669cff5ea5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/367747e41f4550f39431dc669cff5ea5.jpeg)
![images/6be0a0c6b9c15d194e476fe96aaf1f44.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6be0a0c6b9c15d194e476fe96aaf1f44.jpeg)
![images/58d852784f6a3e189bf12ddb6e6128d8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/58d852784f6a3e189bf12ddb6e6128d8.jpeg)
![images/4384eb44818c6294ccac7299fd41b2b8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4384eb44818c6294ccac7299fd41b2b8.jpeg)
![images/658b7f1abff497ff0d435af3135fc445.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/658b7f1abff497ff0d435af3135fc445.jpeg)

**Patrick Woolfenden**

---
---
**Eric Lien** *December 21, 2016 13:13*

Exceptional first print. It is a testimonial of how well designed your printer is. Great work as always.


---
**Florian Schütte** *February 20, 2017 12:51*

[printerkitten.com - Kitten](http://printerkitten.com) does not load. Javascript error (tried in Chrome and Firefox). But nice build. Now i can't decide if i build this one or the tinyBoy.




---
*Imported from [Google+](https://plus.google.com/105660857457447815256/posts/aT1mzr7Knmj) &mdash; content and formatting may not be reliable*
