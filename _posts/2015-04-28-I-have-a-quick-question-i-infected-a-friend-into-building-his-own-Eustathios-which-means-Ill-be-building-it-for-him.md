---
layout: post
title: "I have a quick question, i infected a friend into building his own Eustathios ( which means I'll be building it for him)"
date: April 28, 2015 06:06
category: "Discussion"
author: Gus Montoya
---
I have a quick question, i infected a friend into building his own Eustathios ( which means I'll be building it for him). He ran across this on kickstarter and wants to know if the hotend is only good for like materials or can you print ABS, PLA, Ninjaflex at the same time? I told him no because of the different temperature requirements for each filament. But as always I want to consult the community to see if I missed something. 



[https://www.kickstarter.com/projects/wr3d/the-diamond-hotend-single-nozzle-multi-color-3d-pr?ref=category](https://www.kickstarter.com/projects/wr3d/the-diamond-hotend-single-nozzle-multi-color-3d-pr?ref=category)





**Gus Montoya**

---
---
**James Rivera** *April 28, 2015 06:39*

Interesting. I had not seen this until now. It looks pretty cool. But to answer your question, I noticed it only has 1 thermistor, which means it could only monitor 1 temperature setting. I am not an expert on thermoplastics, but I think that while it may be able to mix multiple materials, I think they would all need to have similar melt characteristics (e.g. glass transition temperature).


---
**Ben Van Den Broeck** *April 28, 2015 06:53*

I wouldn't personally advise fluxing temperatures with this sort of head. The oozing would be pretty messy mix.


---
**Gus Montoya** *April 28, 2015 07:18*

I was telling my friend the same thing. I'll advice against it. If it's a success then more people will hear about it and thus want it. 


---
**Dat Chu** *April 28, 2015 13:50*

I was pretty excited about this until **+Peter van der Walt** told me about the bad track record of the people behind this project. Apparently they had another kickstarter to create cheaper silent step sticks. Those chips didn't work :|. 


---
**Erik Scott** *April 28, 2015 14:11*

Looks heavy. It's a lot of weight to be adding to the print head. 


---
**Øystein Krog** *April 28, 2015 16:55*

I bought a really bad j-head from them once, I knew it was not the original, but it turned out to be a 3mm version with a 1.5mm "conversion" that did not work at all.

That was a bad experience and they were not responsive to my complaints about it.

On the other hand they also sent the wrong nozzle size (0.5mm instead of 0.4), and when I complained about that they did in the end send a replacement free of charge (as I complained that the cost of returning it would exceed it's value).

I've now moved on to authentic E3D's, and it would take a lot I think for me to use anything else.


---
**James Rivera** *April 29, 2015 17:50*

I had a mixed experience with them. I bought a Printrboard Rev. E from RepRap.me. It arrived quickly but I had problems getting it to work. Close inspection revealed a bad solder joint. I took a picture of it and emailed them about it. They responded, "We are sorry for sending you a defective Printrboard. Unfortunately there has been a mixup. The Printrboard that you received was sent to us for repairs from another customer that had broken of the pads on the USB 5V solder jumper. If you return the defective Printrboard we send you a new one." I sent it, and quickly received a new functional one. My problem was resolved, so I guess I really have no major complaint. But this thread gives me pause.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/NciSBLwzXwg) &mdash; content and formatting may not be reliable*
