---
layout: post
title: "First moves - top \"pyramid\" kraken containment vessel half assembled, limit switches (except z) mounted, just need to run all the thermistor and power resistor wires, and get some longer tubing"
date: December 03, 2014 01:26
category: "Show and Tell"
author: Tony White
---
First moves - top "pyramid" kraken containment vessel half assembled, limit switches (except z) mounted, just need to run all the thermistor and power resistor wires, and get some longer tubing. Then mount 4 extruders on the 4 sides of the pyramid, and figure out how to route the filament off a rack that will hang off the back.



Nabbed the old heat exchanger off a pentium 4 processor water cooling system, using it without a fan to cool kraken. Overkill for sure!


**Video content missing for image https://lh3.googleusercontent.com/-8RFx0w-UUSE/VH5mKo6WO3I/AAAAAAAAvLc/HeyZLcKjoeU/s0/2014-12-02%252B21.30.44.mp4.gif**
![images/70d60b8e2f666cd84f4b03fb62cd130d.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/70d60b8e2f666cd84f4b03fb62cd130d.gif)



**Tony White**

---
---
**James Rivera** *December 03, 2014 01:55*

Nice looking build! What kind of heated bed (if any) will you be using?


---
**Eric Lien** *December 03, 2014 02:08*

Love the bumblebee look. Reminds me of my old cbr600 sport bike.


---
**Eric Lien** *December 03, 2014 02:15*

Also the acrylic bottom panel is great. Mine is only MDF, now I have a new project.



I see you are using the larger pulleys on the motors for x/y. I ended up dropping to 20 tooth pulleys because pla nozzle catches were enough to create layer shift. After I switched to smaller pulleys my issues went away.



Keep us posted, and great job.


---
**Brad Hopper** *December 03, 2014 02:49*

need longer movie!


---
**Tony White** *December 03, 2014 16:33*

Using a silicone heated bed - 24 V 300 watt, bonded to AL heat spreader plate (that is mounted to leveling screws) with glass sheet held on top by binder clips. Metal plate gets in a few days...


---
*Imported from [Google+](https://plus.google.com/+AnthonyWhiteMechE/posts/jgXRxNTiGrW) &mdash; content and formatting may not be reliable*
