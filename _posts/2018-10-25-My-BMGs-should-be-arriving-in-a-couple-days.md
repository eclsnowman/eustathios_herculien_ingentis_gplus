---
layout: post
title: "My BMG's should be arriving in a couple days"
date: October 25, 2018 05:19
category: "Build Logs"
author: William Rilk
---
My BMG's should be arriving in a couple days.  What size stepper have people been driving them with?  I don't know that they need as much torque as the cartesian axes steppers, but then again, it shouldn't hurt anything  Opinions?





**William Rilk**

---
---
**Michaël Memeteau** *October 25, 2018 16:50*

The 25 or 22 mm sold by Bondtech is the usual. I personally went for the 25 mm version to be on the safe side.


---
**Julian Dirks** *October 26, 2018 06:40*

I put the 25 on mine too. It’s still pretty small.


---
**William Rilk** *October 27, 2018 01:42*

Thank you.


---
**Eric Lien** *October 27, 2018 05:39*

As someone who tested several motors for for Bondtech during Martin's beta phase on the BMG... I will say not all motors on the open market are of equal torque and quality. I recommend sticking with the ones Bondtech supplies. They are matched to the holding torque of the gear teeth and of high quality. Any more than the suppled motors can over drive the teeth holding capacity... And any less will leave capacity on the table.


---
**Stefano Pagani (Stef_FPV)** *October 27, 2018 22:40*

I like to be under. If the nozzle clogs or is too close the first layer I loose steps instead of blowing up couplings.


---
**William Rilk** *October 27, 2018 23:58*

Guess I'll put another order into bondtech then!  😁


---
*Imported from [Google+](https://plus.google.com/100191047182984055447/posts/X5xPYcBiCGa) &mdash; content and formatting may not be reliable*
