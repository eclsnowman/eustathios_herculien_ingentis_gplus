---
layout: post
title: "Parts for my printer are trickling in at about the same pace I order them..."
date: November 01, 2016 22:57
category: "Discussion"
author: Benjamin Liedblad
---
Parts for my printer are trickling in at about the same pace I order them...  It's like having Christmas every week!



I'm using the BOM from the Spider-V2, but ordered clear anodized fame, not black. Going to try a Replicape ([http://www.thing-printer.com/product/replicape/](http://www.thing-printer.com/product/replicape/)) and see how it goes - waiting on delivery, but that's OK... only just received the frame.



I have not tightened any screws on the frame yet.. Wanted to ask a few questions before I put in the effort:



1) I purchased ~20 extra T-nuts as I thought they might be handy as 'tie-down' points for extra "stuff"



In addition to the T-nuts shown to mount bearing holders, spool holder, etc., can anyone recommend placement for additional T-nuts? 



2) How accurate do I need to be when tightening up the frame? 



Do I need take it the machine shop and get out the 123 blocks on a surface plate (alternately use my wife's quartz kitchen countertops)? 



Or.. 



Is using another (flat) piece of material against each joint while tightening sufficient?



P.S. Don't tell the wife I'm pondering using her countertops.









**Benjamin Liedblad**

---
---
**Mike Miller** *November 01, 2016 23:18*

I had the benefit of a lathe to true-up the ends. You'll want to make sure everything is perpendicular. While all that ultimately matters is that the XY carriage is parallel to the build plate (and secondarily that the Z axis is perpendicular to the XY plane), the rest of the system has to be 'true-enough' 



The tolerances on 3d printers is relatively loose, when compared to things like Internal Combustion Engines. 


---
**Maxime Favre** *November 02, 2016 06:11*

**+Mike Miller** is right, a square and a level should do it ;)



For t-nuts, grab som post-assembly ones : 

[aliexpress.com - 100pcs/lot High Quality T nut Hammer Head Fasten Nut M5 Connector Nickel Plated for 20 series Slot Groove 6](https://www.aliexpress.com/item/T-nut-Hammer-Head-Fasten-Nut-M5-Connector-Nickel-Plated-for-20-series-Slot-Groove-6/32386017247.html?spm=2114.01010208.3.20.6r958d&ws_ab_test=searchweb0_0,searchweb201602_2_10091_10090_10088_422_10089,searchweb201603_1&btsid=73fc7cda-aad9-497b-b5f9-32e6eb75c585)


---
**Sean B** *November 03, 2016 12:57*

I did my best to lay stuff out on the aluminum build plate.  A relatively flat counter top should work just as well.  Definitely get post assembly T-nuts, you will forget some.  


---
**jerryflyguy** *November 05, 2016 01:02*

I think if your top is flat the rest will be fine using a simple square.  Most of the motion is up at the top. Having the Z vertical and perpendicular to the x/y will eliminate a lot of issues out of the gate.


---
**Stefano Pagani (Stef_FPV)** *November 05, 2016 14:40*

I didn't have a square so I used a spare 2020 and clamped it to each finished joint to get them flush. I aligned everything to .05mm using the step feature of my calipers. Thats probably not necessary (it took me 4 hours) The top beams for me got a bit rotated and I haven't been able to fix that. I used the alignment parts and put the printer on a countertop. (I found 1:30AM is a good time not to be found :P)


---
**jerryflyguy** *November 06, 2016 00:10*

**+Stefano Pagani** oh the idiotic mistakes I've made after 1am lol.


---
**Stefano Pagani (Stef_FPV)** *November 06, 2016 02:37*

**+jerryflyguy** Yeah :P at came back to it in the morning and I was wondering why carriage was upsidown :p


---
**Benjamin Liedblad** *November 10, 2016 16:43*

Forgot about the existence of post-assembly T-nuts....  Thanks for the link.



1:30am is definitely a good time to use the kitchen countertops, but with +Mike Miller getting serious with a lathe, I will probably finish assembly at my dad's machine shop. 



Is 40+ too old to have father-son projects?




---
**Mike Miller** *November 10, 2016 16:45*

**+Benjamin Liedblad** god I HOPE not!


---
**jerryflyguy** *November 14, 2016 14:40*

**+Benjamin Liedblad** absolutely not! Love the projects my dad an I have done in his later years, very rewarding (for me at least!)


---
**Mike Miller** *November 14, 2016 15:31*

If you order one, don't drop it on your foot. And not because it might affect it's precision. ;)


---
**Benjamin Liedblad** *November 14, 2016 17:05*

**+Mike Miller**, not sure I understand... What shouldn't I drop on my foot?


---
**Mike Miller** *November 14, 2016 17:07*

Well, I was talking about the 30 lb chunk of granite the surface plate is made out of...but you probably shouldn't drop a 3d printer on your foot, either!


---
**Benjamin Liedblad** *November 14, 2016 17:22*

LOL... I don't think I'm at risk. The surface plate I'll be using can't be moved by mere mortals - it takes an Egyptian pyramid builder or a forklift.


---
*Imported from [Google+](https://plus.google.com/111192213763748051453/posts/UPo43U1qFrH) &mdash; content and formatting may not be reliable*
