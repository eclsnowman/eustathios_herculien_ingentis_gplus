---
layout: post
title: "Interesting new product (even though I can barely print with one yet!):"
date: September 28, 2016 19:23
category: "Discussion"
author: Bruce Lunde
---
Interesting new product (even though I can barely print with one yet!):




{% include youtubePlayer.html id=KpcH74DXyy0 %}
[https://www.youtube.com/watch?v=KpcH74DXyy0](https://www.youtube.com/watch?v=KpcH74DXyy0)





**Bruce Lunde**

---
---
**jerryflyguy** *September 28, 2016 19:42*

Curios as to how their multi extruder board works to interface w/ their current board. This multi material process was on my 'things to explore' list. Not sure how that'd interface w/ the Azteeg x5 board though.. 


---
**Eric Lien** *September 28, 2016 21:58*

**+jerryflyguy** looks like it just takes the extruder output, and uses SSR switching to split the output. So long as there is enough output gpio on the Azteeg it should be possible assuming **+Josef Prusa**​ will be selling the multiplexer separately to be used for integration on other boards too.



Then it's just writing a color change script in your slicer to switch the io and perform the purge change sequence.


---
**Mauro Manco (Exilaus)** *September 29, 2016 04:49*

That old and work project(2014) in Italy reprap forum any can print and use  need a demultiplex via relay chain. Check Facebook prusa research post for more information(inside comments)   


---
**jerryflyguy** *September 29, 2016 23:57*

**+Eric Lien** is there any extra io on the Azteeg X5 V3 as it's set up (normally) to run a single extruder Eustathios V2? There are some pins I'm not using but haven't read anything about other io options?


---
**Eric Lien** *September 30, 2016 00:38*

**+jerryflyguy** that would probably be a question to ask Roy at Panucatt. I am not sure what all the pin assignments are on the x5 mini v3.


---
**jerryflyguy** *September 30, 2016 00:40*

**+Eric Lien** I'll do that


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/4ii5xMdZViU) &mdash; content and formatting may not be reliable*
