---
layout: post
title: "Enclosure Three years after I started the build, I finally managed to finish the printer and install the enclosure"
date: April 01, 2018 20:16
category: "Show and Tell"
author: Daniel F
---
Enclosure

Three years after I started the build, I finally managed to finish the printer and install the enclosure. The front is enclosed with 4mm acrylic glass to get a good view. The three other sides and the top are covered with 5mm panels used for small glass houses. This material is easy to cut and it's a good thermal insulator with its air chambers. It is also a light weight material compared to acrylic glass which helps to keep the weight low. The door and the top front panel are hold in place with small neodymium magnets. This solution allows quick screw less access to the chamber from the front.



![images/ba128e2d36543c47bab37c5b16f1e0c7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ba128e2d36543c47bab37c5b16f1e0c7.jpeg)
![images/929f1dd141ab247ffeb9c4ed46e29597.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/929f1dd141ab247ffeb9c4ed46e29597.jpeg)
![images/2637196659ed78ea342d61d0ae10cd77.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2637196659ed78ea342d61d0ae10cd77.jpeg)
![images/636e20207343c22f6451109f003a1739.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/636e20207343c22f6451109f003a1739.jpeg)
![images/d8c10dc1bf36ef0a6f2b1d7f8fec72ff.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d8c10dc1bf36ef0a6f2b1d7f8fec72ff.jpeg)
![images/95296b6c8e968f3c438664733518f65b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/95296b6c8e968f3c438664733518f65b.jpeg)
![images/bccf8ea7e233f85099301363150d5cfc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bccf8ea7e233f85099301363150d5cfc.jpeg)

**Daniel F**

---
---
**Eric Lien** *April 02, 2018 11:42*

Looking great **+Daniel F** . Thanks for sharing. I know Eustathios is trickier to enclose than HercuLien. Glad to see you got it done so clean. Really looking nice.


---
**Jim Squirrel** *April 03, 2018 06:26*

Fantastic work! I hope to finish mine as well. Some day..,


---
**Julian Dirks** *April 04, 2018 00:46*

Some really good info in your pics.  I see you have used 3 point bed levelling.  Has that worked well for you?


---
**Daniel F** *April 04, 2018 05:31*

Yes works well, and access from the front is easy as the back level screw is in the mittle. The bed is quite thick (6mm) milled aluminium and therefore solid, I only need to level after changing the nozzle. The level screws are counter sunk on top, the 2 nuts to adjust the hight are tightened against each other so you can turn the screw with them.


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/ZFpd4uTL5cr) &mdash; content and formatting may not be reliable*
