---
layout: post
title: "After i finished the Eustathios Spider V2 last year, i decided to build a smaller one from an old Cl260 (ultimaker clone) Frame: Eustathios Mini"
date: January 08, 2019 22:17
category: "Discussion"
author: Es Kei
---
After i finished the Eustathios Spider V2 last year, i decided to build a smaller one from an old Cl260 (ultimaker clone) Frame: Eustathios Mini.

Workspace 180x180x220mm 

Let the pictures speak for itself: 



![images/8a9dd688455428eb6abed8bf3f84a9f7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8a9dd688455428eb6abed8bf3f84a9f7.jpeg)
![images/dcbc6b623ece7c52057d1d9b4d1f9836.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dcbc6b623ece7c52057d1d9b4d1f9836.jpeg)
![images/6344f4c578d300a271df77c9eab41a2f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6344f4c578d300a271df77c9eab41a2f.jpeg)
![images/66c42e83a471ee032bf09aad635e5d2a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/66c42e83a471ee032bf09aad635e5d2a.jpeg)
![images/cc0cd73bf2dbd1005814c799923c20d0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cc0cd73bf2dbd1005814c799923c20d0.jpeg)
![images/1e867d30800fa99594673b3d397ab201.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1e867d30800fa99594673b3d397ab201.jpeg)
![images/ee59ebc359b2e4c7d39225c5828582fc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ee59ebc359b2e4c7d39225c5828582fc.jpeg)

**Es Kei**

---
---
**Eric Lien** *January 11, 2019 03:22*

Sorry this got caught in the g+ spam filter.



Looking great. Thanks for sharing. How's it running for you so far?


---
**Eric Lien** *January 11, 2019 03:25*

Btw I love the wood base. Adds a certain styling compared to the normal acrylic base. 


---
**Es Kei** *January 14, 2019 19:48*

So far, i' very happy with the built. With this one, i tried a cheap 8bit setup with:

MKS Gen 1.4  > Marlin 1.1.8

TMC2008 from Watterott except the Extruder (E3D Titan) A4988. Ran in trouble with the TMC2208 on it. Travel Movement on x and y 350mm/s without a Problem






---
*Imported from [Google+](https://plus.google.com/112878085435661777265/posts/GzHpPfq8dc5) &mdash; content and formatting may not be reliable*
