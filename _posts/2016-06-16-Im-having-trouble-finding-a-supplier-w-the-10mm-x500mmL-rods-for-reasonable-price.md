---
layout: post
title: "I'm having trouble finding a supplier w/ the 10mm x500mm(L) rods for reasonable price"
date: June 16, 2016 18:58
category: "Discussion"
author: jerryflyguy
---
I'm having trouble finding a supplier w/ the 10mm x500mm(L) rods for reasonable price. Looked at metric drill rod but not sure if that's good enough? Anyone have a good/reputable option from AliExpress? Could order from Misumi but the price is pretty eye watering.. 





**jerryflyguy**

---
---
**Alex Paverman** *June 16, 2016 19:08*

[http://www.tekro.ro/index.php?route=product/category&path=63](http://www.tekro.ro/index.php?route=product/category&path=63)   

a romanian online store with absolutely decent prices﻿; I tegulary buy fro them. The stuff is from China, you can buy it also from Aliexpress. I used to buy fom Ali but it's faster for me to buy directly from Romania with a reasonable markup.


---
**Tomek Brzezinski** *June 16, 2016 19:23*

I wouldn't buy from a super cheap supplier for <10mm rod, but maybe 10mm rod is easy enough to get straight.



But "Enco" is a good low-cost industrial supplier, not sure how much cheaper than Misumi. Check out if their prices are acceptable, and make a decision pro-con vs going with a china supplier.



Sometimes you can call MSC-Industrial and price match with Enco (they might even be the same company, I never really figured that out)


---
**Eric Lien** *June 16, 2016 23:32*

The side rod concentricity is critical. Don't skimp on them. The center cross rods have more leniency. 


---
**jerryflyguy** *June 16, 2016 23:38*

Alrighty, guess Misumi it is.. 


---
**Blake Dunham** *June 17, 2016 01:47*

Try vxb bearing. Very cheap and precise


---
**jerryflyguy** *June 17, 2016 02:00*

**+Blake Dunham** just looked, doesn't appear any more economical than Misumi? Cheap is Swm3D but won't see them for several weeks as they are out of stock and won't have any for a week or more


---
**jerryflyguy** *June 17, 2016 02:31*

Just got an email from Roy in regards to the Azteeg X5 and a delay in shipping. Mine has been ordered since the V3 was released (May) but still hasn't shipped, sooo.. Guess I got time to wait for SWM3D to get stock in :/


---
**Blake Dunham** *June 17, 2016 11:43*

Yeah vxb and misumi are pretty comparable. I will say though, vxb and much faster at shipped.


---
**Sean B** *June 18, 2016 15:22*

I just ordered all my rods from Aliexpress, I'll take some measurements and let you know how they are.  Eric, wouldn't the bearings have a greater effect on concentricity than the rod itself?  I bought some koyo bearings and compared them with cheap skateboard bearings and it's night and day with regards to the play in the bearings.  ﻿


---
**Eric Lien** *June 18, 2016 18:11*

**+Sean B** I just mean that since the side rods translate motion laterally as well as rotate to power the opposing axis (X/Y) that the must remain highly concentric along the length. If the are eccentric, when they rotate that will translate that cyclical motion into the cross rods and hence into the hotend.


---
**Sean B** *June 18, 2016 18:13*

**+Eric Lien** yeah, that makes sense, in that case concentricity would have a stronger effect than the bearings.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/2465q4py1sb) &mdash; content and formatting may not be reliable*
