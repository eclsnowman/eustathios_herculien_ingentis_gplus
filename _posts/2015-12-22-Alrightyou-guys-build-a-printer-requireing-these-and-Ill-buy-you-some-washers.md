---
layout: post
title: "Alright...you guys build a printer requireing these, and I'll buy you some washers!"
date: December 22, 2015 12:50
category: "Discussion"
author: Mike Miller
---
Alright...you guys build a printer requireing these, and I'll buy you some washers!




{% include youtubePlayer.html id=cDlmbMV9ICU %}
[https://www.youtube.com/watch?v=cDlmbMV9ICU](https://www.youtube.com/watch?v=cDlmbMV9ICU)





**Mike Miller**

---
---
**Eric Lien** *December 22, 2015 14:19*

I know many people who swear by these. They say they are the only locknuts that actually work. Thanks for posting the link.


---
**Brandon Satterfield** *December 22, 2015 15:02*

We use these a lot in oil and gas. Several advantages, one is the solid continuity between two members. Great product. 


---
**Mike Miller** *December 22, 2015 15:50*

I know of several 'niche' connectors...it'd just be cool to see a printer that is either large enough, or has enough vibration, to require them. :)



Stage8 us another kinda cool fastener: [http://www.stage8.com/](http://www.stage8.com/)


---
**CkTBrD** *December 22, 2015 19:34*

Thanks for posting!


---
**Ryan Carlyle** *December 22, 2015 22:58*

Yeah, I use them at work in hydraulics subjected to fluid hammer and turbulent flow vibration... They're immune to certain extreme types of vibrations that will loosen everything else. Complete ridiculous overkill for 3D printers though. Nylon locknuts or blue Loctite are 100% adequate for what we're doing in printers. 



My opinion, if the connection isn't important enough to calculate a torque and use a torque wrench, it's not important enough to spend money on Nord-locks. Just add more bolts and snug them up tight and it'll hold fine. 



It's definitely true that every other kind of lock-washer is garbage. Split ring washers are actually worse than nothing. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/fQFFerzEDh8) &mdash; content and formatting may not be reliable*
