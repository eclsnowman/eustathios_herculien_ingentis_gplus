---
layout: post
title: "Hi All! I have a question about hooking up the Viki LCD to the Azteeg X3"
date: September 16, 2015 14:26
category: "Discussion"
author: Luis Pacheco
---
Hi All!



I have a question about hooking up the Viki LCD to the Azteeg X3.



On the wiring PDF with notes that was uploaded there seem to be two connections going from the board to the LCD, one on EXP1 and one on ICSP. How do you connect the ones going to EXP1?



EXP1 seems to be taken up by the board shield, any way around this?



Thanks!





**Luis Pacheco**

---
---
**Eric Lien** *September 16, 2015 16:58*

The diagram is right here: [http://files.panucatt.com/datasheets/viki_wiring_diagram.pdf](http://files.panucatt.com/datasheets/viki_wiring_diagram.pdf)


---
**Luis Pacheco** *September 16, 2015 17:46*

Awesome, hadn't seen that. Appreciate it! I kept trying to figure out how I could use EXP1 with both.


---
*Imported from [Google+](https://plus.google.com/110276424073473119972/posts/fAXxdJtk1kV) &mdash; content and formatting may not be reliable*
