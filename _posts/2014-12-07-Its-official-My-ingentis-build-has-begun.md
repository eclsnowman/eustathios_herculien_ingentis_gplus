---
layout: post
title: "It's official. My ingentis build has begun!"
date: December 07, 2014 05:07
category: "Discussion"
author: Ethan Hall
---
It's official. My ingentis build has begun!﻿

![images/3437b4768f28e4203aff3a119a1645c1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3437b4768f28e4203aff3a119a1645c1.jpeg)



**Ethan Hall**

---
---
**D Rob** *December 07, 2014 05:21*

Welcome to the club! 


---
**Jarred Baines** *December 07, 2014 15:22*

The bearing mounts, every ingentis's first print I think ;-)



They look nice and clean mate!


---
**Ethan Hall** *December 07, 2014 15:33*

Thanks! I was actually impressed by the lack of major z artifacts. That's my main issue with my prusa i2﻿


---
**Gus Montoya** *December 08, 2014 01:28*

Nice :) I am still calibrating my crap QU-BD printer to start my print.


---
**Seth Mott** *December 08, 2014 22:50*

Can you just measure the diameter of the bearing hole?  How close to 22mm should it be?  I printed these, but they came out to about 22.84mm diameter and the bearings fit pretty loosely


---
**Ethan Hall** *December 08, 2014 22:55*

Mines just about 22 in most directions I may have to trim some off of the top first though


---
**Jarred Baines** *December 09, 2014 06:09*

22 spot on here... Aim for that or even up to .05mm undersize as it will seat the bearing nicely... Print a test hollow box / cylinder and measure / calibrate x and y steps


---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/DqE2bLjRjUf) &mdash; content and formatting may not be reliable*
