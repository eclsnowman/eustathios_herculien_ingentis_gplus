---
layout: post
title: "Robots making robots... I decided to use Eric's models directly for the CAM"
date: November 26, 2018 00:43
category: "Build Logs"
author: William Rilk
---
Robots making robots...  I decided to use Eric's models directly for the CAM.  Figured, might as well, right?  Feels almost like cheating...



![images/c7a1fd8cb3c0ead4b4c7ec448b94090f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c7a1fd8cb3c0ead4b4c7ec448b94090f.jpeg)
<a href="https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3aee6bf8afe7c739073e0e4288eec64c.mp4">![images/4cb91cde2d53a629ca7bcc8c37458a0c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4cb91cde2d53a629ca7bcc8c37458a0c.jpeg)</a>

**William Rilk**

---
---
**Eric Lien** *November 26, 2018 01:54*

Consider me jealous. Nice mill. Saves from having to print the drill guides.


---
**William Rilk** *November 26, 2018 02:42*

I'll be wishing I had a cnc'd router when it comes time to route the MDF for the bed.  How have people tackled that one?  I'm thinking of cobbling a router sled together.


---
**Eric Lien** *November 26, 2018 03:40*

I made my insulator with paper 1:1 templates and a hand router. Time consuming... but totally doable.


---
*Imported from [Google+](https://plus.google.com/100191047182984055447/posts/APaVrbT2h9z) &mdash; content and formatting may not be reliable*
