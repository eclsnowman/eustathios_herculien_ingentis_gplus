---
layout: post
title: "Had some problems with the section that the twist tie was supposed to go through,it got clogged during printing"
date: September 06, 2015 17:21
category: "Deviations from Norm"
author: Rick Sollie
---
Had some problems with the section that the twist tie was supposed to go through,it got clogged during printing. After much work I ended up drilling two holes to clear the clog. That left me with a very loose fitting and a wobbly hot end. So I made a small adapter, then used screws to hold it in place. I'm not happy with the end results as there is too much air gap(should be much less) and the original head should have been reworked for screws instead of just drilling a hole and making it work.  But the end results is working solid, just not pretty.  



When I print up all the other parts in abs I'll make some revisions.

![images/0937461c0566387efaefd65c9bde6619.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0937461c0566387efaefd65c9bde6619.jpeg)



**Rick Sollie**

---
---
**Stephen Baird** *September 06, 2015 17:27*

"It's not pretty but it works" is a pretty perfect summary of my 3D printing experience. 


---
**Daniel F** *September 06, 2015 17:48*

Had a similar problem, my mistake was that I printed the part with support. It was very difficult to get it out, I managed to clean the hole for the zip tie with a wire that was bent with the same radius as the channel for the zip tie.﻿


---
**Eric Lien** *September 06, 2015 19:46*

Yeah, that's why I made the version with integrated support and hold down tabs. I have Simplify3d so I can create selective support. But for those that don't the integrated support version should work fine in any slicer.﻿


---
**Erik Scott** *September 07, 2015 04:35*

Feel free to try out my version of the carriage. It has a clamp to hold the hotend in place. Otherwise, it's very similar. 



[https://plus.google.com/+ErikScott128/posts/M8M5RyTRj6b](https://plus.google.com/+ErikScott128/posts/M8M5RyTRj6b)


---
**Rick Sollie** *September 07, 2015 14:14*

**+Erik Scott**

  Thanks for the link. Most my parts are PLA, but I plan to print out a fresh set of ABS everything once it's running, I'll definitely include this in the list.


---
**Bud Hammerton** *September 10, 2015 02:41*

I modded my version of the extruder also, didn't really want to use electrical ties to hold the hotend in place. I went a different route making the whole mount solid and using two M3 set screws to hold the extruder in place. I did use two M3 heat set brass inserts to hold the set screws in place.


---
**Vic Catalasan** *September 10, 2015 04:08*

+Erik Scott , BTW thanks for providing that screw clamp version, I am using it on my Herculien as a single head version.


---
**Erik Scott** *September 10, 2015 04:10*

Glad you like it! How's it working out for you? I haven't got my second build up and running yet, so I haven't even tried it myself. 


---
**Vic Catalasan** *September 10, 2015 04:20*

Worked great! 

[https://plus.google.com/108399297878867262359/posts/8keorQgiKGE?pid=6180538115608397874&oid=108399297878867262359](https://plus.google.com/108399297878867262359/posts/8keorQgiKGE?pid=6180538115608397874&oid=108399297878867262359)


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/fonmGxPqi3S) &mdash; content and formatting may not be reliable*
