---
layout: post
title: "And now that she is pretty much done, here is a Marvin at 500%"
date: September 24, 2017 20:48
category: "Show and Tell"
author: Sonny Mounicou
---
And now that she is pretty much done, here is a Marvin at 500%.  Volcano prints are absurdly strong.

![images/eaccbc4acd19d9e721f2dcd8a743f428.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eaccbc4acd19d9e721f2dcd8a743f428.jpeg)



**Sonny Mounicou**

---
---
**Eric Lien** *September 26, 2017 12:56*

Wow. Looking great. exceptional job getting the direct drive extruder on there. and I love the use of Hiwin style rails on the cross rods. You do exceptionally clean work.



Would you mind doing a little photo shot of the changes and upgrades you did to the printer? Perhaps push some of the upgrades out to the github?



BTW: Sorry your posts got caught in the spam filter for a few days. The amount of spam we get in this community is unbelievable, so depending on how Google auto sorts new users posts it can be hard to tell real content from what it thinks is spam. But I have your posts auto approved now so this doesn't happen again. 


---
**Sonny Mounicou** *September 26, 2017 13:34*

Yep.  I can push the changes up to the GitHub.  I'll take some photos as soon as I'm done cleaning up.  I'm actually reprinting a bunch of the parts like the bearing holders.  I had adjusted some of the offsets when I was trying to figure out calibration issues.  They weren't the cause, so I'm going back to stock on those.  In addition, the volcano parts are so strong, its unreal.  



The biggest changes are the complete redo of Z.  Its now metal mounting from the frame to the bed.  Its very solid.  The PEI plate to print on (its a dream).  The extruder setup, rail mount, and X/Y were all changed to accommodate the new setup.  Using a BLTouch mounted to calibrate for level.  I adapted your spool holder to hold a 3.5 KG roll of filament.  And I built the drag chain mounts.  



My next goal will be to cut panels to enclose it (sides and top).  I'll get there after halloween :)


---
**Ray Kholodovsky (Cohesion3D)** *October 04, 2017 19:01*

Me likey, want to see more!


---
**Gus Montoya** *November 28, 2017 06:17*

**+Sonny Mounicou** Any word on the files being available? I'm starting my build and would like to fill my print bed up with the goodies to print haha  Thanks :)


---
*Imported from [Google+](https://plus.google.com/113710966260079086437/posts/izsQyFXuXqw) &mdash; content and formatting may not be reliable*
