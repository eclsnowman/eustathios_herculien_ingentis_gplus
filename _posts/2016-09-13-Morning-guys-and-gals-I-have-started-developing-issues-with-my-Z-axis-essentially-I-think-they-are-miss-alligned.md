---
layout: post
title: "Morning guys and gals. I have started developing issues with my Z axis, essentially I think they are miss-alligned"
date: September 13, 2016 09:41
category: "Discussion"
author: Adam Morgan
---
Morning guys and gals.



I have started developing issues with my Z axis, essentially I think they are miss-alligned. So tonight I plan to strip the Z down and re-build. However when I originally did this I had issues getting the threaded rod and smooth bar alligned. I just found a spacer on here to use which is very handy, but I wanted to check, is the spacing between these 2 rods the same on the Eustathios v1 and v2? as the spacer is for the v2? 



Cheers guys.





**Adam Morgan**

---
---
**Eric Lien** *September 13, 2016 21:58*

What spacing do you mean, and what spacer. Can you include a picture to help me visualize your question?


---
**Stefano Pagani (Stef_FPV)** *September 14, 2016 01:42*

**+Adam Morgan** Add a picture, please :) I loosened the top rod holder and let it align its self when sliding the bed up. (I don't know if it is different on the V1


---
**Adam Morgan** *September 14, 2016 09:33*

Sorry would have made sense to add the pic. The spacer I was talking about looks like this: [imgur.com - Imgur: The most awesome images on the Internet](http://imgur.com/34r12jo)

Although I printed it last night and found it didnt fit my threaded rod :) guess I will just need to keep correcting it with trial and error :)


---
**Eric Lien** *September 17, 2016 02:43*

I wouldn't recommend using that upper printed part that is making the lead screw captive at the top. It should be free to move at the top. The big issue if you have binding on Z is to make sure the dimensions on the printed parts for the lower leadscrew/Z mount are the same as the bed mounts above them. To get this I always make sure to print the two parts (bed mound and leadscrew/Z mount below it) are printed on the same printer, with the same settings, in the same orientation. Using the same printer orientation means any errors in your printers tolerance are matched between the components. 



Another thing to make sure of is that the Z 10mm rod is running perfectly vertical. Just measure off the front of the frame back to the rod at the top and bottom of the frame. You could also make a temporary spacer and use the spacer to set where the rod is back to the frame. Then use that spacer at the top and bottom of the rods from the front of the frame.



Also before you attach the lead nuts to the bed, make sure the bed travels smoothly from the top to the bottom without any binding (run the lead nut all the way down and move the bed up and down by hand). In the bed assembly there is unfortunately zero adjustment in the width. So any errors in your printed parts or frame compount together. This could cause the bed to bind at the top of bottom if the center to center of the linear bearings in the bed mount are not perfectly aligned with the center to center of the rods bolted to the frame. To be honest this is an area I have been meaning to redesign for a while. 


---
**Adam Morgan** *September 17, 2016 10:46*

Thanks for the help guys. Got it working nicely now, just to note the printer in the picture is not mine, thats the pic the guy who designed the spacer:) turned out my smooth rods were slightly slanted, a square helped me sort that. got it running nicely now. Moved onto adding my second hot end. Will post pics soon. Love this printer.


---
**Eric Lien** *September 17, 2016 12:02*

**+Adam Morgan** great news. Excited to see your first moves, and first print :)


---
**Adam Morgan** *September 22, 2016 21:45*

cheers **+Eric Lien**, sad thing is I have actually been printing with this printer for about a year, but I keep modifying it and havn't been happy to show it off :) Love the printer and your work. Nice one. 


---
*Imported from [Google+](https://plus.google.com/117208540149253709671/posts/VbLqQEYsPTJ) &mdash; content and formatting may not be reliable*
