---
layout: post
title: "We could use this to replace the ABS on our printer parts!"
date: November 10, 2016 01:03
category: "Show and Tell"
author: Stefano Pagani (Stef_FPV)
---

{% include youtubePlayer.html id=0cGk7VK39ZQ %}
[https://www.youtube.com/watch?v=0cGk7VK39ZQ](https://www.youtube.com/watch?v=0cGk7VK39ZQ)

We could use this to replace the ABS on our printer parts! Looks pretty awsome





**Stefano Pagani (Stef_FPV)**

---
---
**Nathan Fisher** *November 10, 2016 01:19*

So I've been wondering about this. I print mostly PETG but I don't think it would be rigid enough for printer parts. Am I correct? Never printed ABS but I have played with some esun ePC. Any suggestions? I'll have to check this stuff out. 


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/YCoJ5bFWGxR) &mdash; content and formatting may not be reliable*
