---
layout: post
title: "Originally shared by D Rob using 1/16th micro stepping the UL-T-Slot will have 136.53 steps/mm if I use 15t g2t pulley on the motor shaft with the 32t g2t 10mm bore pulleys on the screws"
date: November 21, 2014 04:39
category: "Discussion"
author: D Rob
---
<b>Originally shared by D Rob</b>



using 1/16th micro stepping the UL-T-Slot will have 136.53 steps/mm if I use 15t g2t pulley on the motor shaft with the 32t g2t 10mm bore pulleys on the screws. And I can always get mendel 90 type res by backing down to 1/8th micro stepping. Any thoughts? Oh yeah with 20mm Pulleys on the motor it will be 102.4 steps/mm. Lets take a vote on which I order. Discussion.......... Begin!

![images/827985765c120bf3902ab298ebc3c8ba.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/827985765c120bf3902ab298ebc3c8ba.png)



**D Rob**

---
---
**Matt Miller** *November 21, 2014 04:52*

Why not 16T? 128steps/mm


---
**D Rob** *November 21, 2014 05:28*

16t will be ideal as it is half the count of the driven pulley. Thanks **+Matt Miller**​


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/K4cfsaUsDdD) &mdash; content and formatting may not be reliable*
