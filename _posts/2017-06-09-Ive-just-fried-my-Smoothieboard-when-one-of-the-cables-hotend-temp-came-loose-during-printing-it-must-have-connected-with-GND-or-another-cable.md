---
layout: post
title: "I've just fried my Smoothieboard when one of the cables (hotend temp) came loose during printing (it must have connected with GND or another cable)"
date: June 09, 2017 03:36
category: "Discussion"
author: Oliver Seiler
---
I've just fried my Smoothieboard when one of the cables  (hotend temp) came loose during printing (it must have connected with GND or another cable).

What would people recommend replacing it with? I need to drive 5 steppers and LAN or Wifi (I currently use the Smoothie status page to monitor and control).

![images/b575241a8029711c7210a36db7356146.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b575241a8029711c7210a36db7356146.jpeg)



**Oliver Seiler**

---
---
**Eric Lien** *June 09, 2017 04:28*

I am a big fan of the X5 GT, and the ReMix. Also the DuetWiFi is on my list to try. I haven't used it in a build yet, but others I respect swear by the firmware and board.


---
**Markus Granberg** *June 09, 2017 05:12*

I have 2 duet Wi-Fi and they work great! And they are so silent to! (Even on 12v) 


---
**Maxime Favre** *June 09, 2017 05:41*

Stick with smoothieboard. You already have all the connectors in place. Also it support the smoothie project which isn't the case of the other boards.


---
**Oliver Seiler** *June 09, 2017 05:51*

I'm torn. The DuetWifi looks great to be honest. They also say that they have fuses in place to prevent what just happened to my Smoothieboard which is great! 

I take your point **+Maxime Favre** about the re-wiring, and also a new 5X would be cheaper and faster to get here. Having to set everything up again fom scratch means a lot of work.

**+Markus Granberg** how's the Duet firmware - anything lacking? I'm curious as it seems to be support by one gut only (is that true?)


---
**Markus Granberg** *June 09, 2017 08:46*

I don't have any experience with dual extruder so can't say about that. But I do have heated bed, heated chamber, auto bed level and so on. I like that it runs its own web page so you don't need a raspberry and octoprint (it does not support webcam!)

I come from ramps and will never look back! You can update the settings and firmware over Wi-Fi!! Take that arduino!



But I don't have any experience with smoothie I'm sure it's great tho! 


---
**Eric Lien** *June 09, 2017 13:01*

**+Maxime Favre** to my understanding Roy with Panucatt supports the Smoothieware project by contributing for each board purchased.


---
**Matthew Kelch** *June 09, 2017 14:25*

The Duet WiFi is awesome. Really love mine. 


---
**Greg Nutt** *June 10, 2017 02:07*

The v1.1 of the smoothieboard 5xc is quieter now with the 1/32 step drivers replacing the 1/16 drivers from the v1.0. 


---
**Oliver Seiler** *June 10, 2017 06:30*

I've ordered the DuetWifi. Thanks everyone for your advice.


---
**Oliver Seiler** *June 21, 2017 07:17*

I've now installed my DuetWifi and completed a few prints and I must say, I'm amazed at how well it performs!

What stands out is the total quietness of the steppers, despite the fact I didn't really mind the 'noise' before, it's a huge difference. And my prints do looks cleaner using the same settings - I've finished a XT-CF print with no visible layering - something that I have bever achieved before.

Also amazing is the temperature control. After a one-off autotune the hotend stays well within +/-0.5 degrees, even when I turn the fan from 0 to 100%. I've never managed to get it so consistent with Smoothieware, despite hours of auto and manual PID tuning.

The web interface (and the panel) is different, maybe a little overloaded, but time will tell. The process of uploading a file and getting it to print is extremely simple (and much faster than uploading to the sd card over USB as I did before).

I can only encourage everyone to have a good look at this board.

Thanks again everyone.


---
**Oliver Seiler** *June 30, 2017 23:03*

**+Markus Granberg** **+Matthew Kelch** what have you configured for microstepping on your Duets? So far I'm working with the default of x16 microstepping using interpolation. I guess there's no point in going to x128 microstepping over the default and with x32 I can't use interpolation.




---
**Markus Granberg** *July 01, 2017 22:00*

I'm running default 


---
**Matthew Kelch** *July 02, 2017 12:48*

**+Oliver Seiler** I'm still tweaking, but 16x with interpolation turned on seems to be a very good option. 


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/5e6NcxfgSGS) &mdash; content and formatting may not be reliable*
