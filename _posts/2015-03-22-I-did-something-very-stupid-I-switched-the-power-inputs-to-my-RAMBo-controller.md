---
layout: post
title: "I did something very stupid. I switched the power inputs to my RAMBo controller"
date: March 22, 2015 21:17
category: "Discussion"
author: Eric Bessette
---
I did something very stupid.  I switched the power inputs to my RAMBo controller.  Anyone else done this and only had to replace the blow fuses, not the entire board?  (Looking for peace of mind right now :) )





**Eric Bessette**

---
---
**Thomas Sanladerer** *March 22, 2015 21:41*

The RAMBo has built-in reverse polarity protection that, ideally, only blows the fuse(s?). It should be good to go once you replace that.

Let us know how it goes!


---
**Dave Hylands** *March 22, 2015 22:09*

And to further confuse things, the conventions for 120v wiring is that black is live and white is neutral. I noticed that you have it backwards on your PS. I can't see the other end of your 120v wires. The wide blade on the 120v plug is neutral and if you're using a cut up power cord that wide blade should be connected to the white wire in the cable.


---
**Thomas Sanladerer** *March 22, 2015 22:11*

True, the only colors you should be using for low-voltage DC are red and black. Everything else just doesn't make sense... 


---
**Eric Bessette** *March 22, 2015 22:26*

It's been over a decade since my one embedded systems class in college, so I really have no idea what I'm doing.  Anyone know of a good primer online for wiring colors, gauge size, type, etc.


---
**Dave Hylands** *March 23, 2015 00:48*

For low voltage wiring, there isn't much in terms of stndards:

[http://i.stack.imgur.com/eY4it.png](http://i.stack.imgur.com/eY4it.png)



The most common low-voltage color conventions are black (an/or sometimes green) for ground and red is typically your positive voltage.



For 120v wiriing, bare/green is ground, black/red are hot, and neutral/return is white (except when it's not - electricians will often put a piece of black electrical tape on a white wire to indicate that its hot - this happens for some combinations of switches/lights etc). neutral and ground are typically connected together at one place in your house  (often in the main panel).


---
*Imported from [Google+](https://plus.google.com/106288190144199995561/posts/YFjYpYxkn8R) &mdash; content and formatting may not be reliable*
