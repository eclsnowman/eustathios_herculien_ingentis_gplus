---
layout: post
title: "I think I've blown a stepper driver.."
date: September 25, 2016 15:20
category: "Discussion"
author: jerryflyguy
---
I think I've blown a stepper driver.. Last night I got every axis moving except the X. I couldn't even get a 'bump' or hum. After checking wiring and everything else I pulled the stepper driver and found this.



I pushed it back in and resoldered it. Now I get a hum when trying to job but no motion. I did swap it over to the extruder stepper as that's just a motor currently, same result. A bit of a hum and that's it.



Anything I can check using simple tools (I only have a super cheap oscilloscope and not much idea how to use it) on this before giving up? 

![images/9d58fe3e2865b0822a782454de85fece.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9d58fe3e2865b0822a782454de85fece.jpeg)



**jerryflyguy**

---
---
**Eric Lien** *September 25, 2016 15:25*

Double check the x-axis wiring for continuity across the coils. And confirm the coils are wired properly into the board. I had an issue once that I traced down to only one of the coils on the stepper being connected.


---
**Eric Lien** *September 25, 2016 15:27*

Also only ever remove stepper drivers with the controller powered off.


---
**Carlton Dodd** *September 25, 2016 15:42*

If this happened to one of my drivers, I would have re-soldered as you did, then tested.  If the test failed (as yours did), I'd just replace the driver module; not worth the time to fix.


---
**jerryflyguy** *September 25, 2016 15:46*

I'll check, the problem moves as I move the stepper driver around, so I suspect it's on the board somewhere. I may try resoldering all the pins also. 



I've been powering the board down each time.



Is it ok to just pull the 110v going into the 24v power supply(I've got an inline switch rated for 110ac)? It just seems harsh on the smoothie, I get the odd screen flicker on the Viki as the caps in the ps drain


---
**jerryflyguy** *September 25, 2016 15:53*

**+Carlton Dodd** I hear ya Carlton, I'm impatient and don't want to have to wait the ~2 weeks for replacement if I can fix this one. Will buy spares regardless but.. I'm not patient when I get this close to running😎


---
**jerryflyguy** *September 25, 2016 18:42*

Well, there must have been a cold solder joint on one of the other pins also. I looked them

Over and resoldered a couple more pins that looked suspect.. Plugged it back in and it works! 



I'm going to order a set of the SD6128 drivers when they become available (that way I'll have some spares) but for now I'm super pumped to have it running.. Now it's on to calibration!


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/PQxKpArBGMe) &mdash; content and formatting may not be reliable*
