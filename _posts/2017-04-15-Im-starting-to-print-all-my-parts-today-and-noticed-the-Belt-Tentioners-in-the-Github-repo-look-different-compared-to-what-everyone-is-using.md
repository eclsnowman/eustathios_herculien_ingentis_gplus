---
layout: post
title: "I'm starting to print all my parts today and noticed the Belt Tentioners in the Github repo look different compared to what everyone is using"
date: April 15, 2017 13:44
category: "Discussion"
author: Amit Patel
---
I'm starting to print all my parts today and noticed the Belt Tentioners in the Github repo look different compared to what everyone is using. I am using 10mm press fit brass bushing.



Thx





**Amit Patel**

---
---
**Eric Lien** *April 15, 2017 14:00*

There are several versions available. The main one on the GitHub is based on the Original Eustathios by **+Jason Smith**​​.



Then in the Community Mods area there are some really nice ones designed by **+Walter Hsiao**​​



And if you are adventurous, **+Zane Baird**​​ just did a pull request to the HercuLien GitHub repo for his new design. It might need to be narrowed a tiny bit to fit on Eustathios, but the cross rod spacing is identical between HercuLien and Eustathios. So his new compact central carriage and side rod carriages should work as well.



It's an organic community with so many great mods out there. So few are identical... Which is the part I like best :)


---
*Imported from [Google+](https://plus.google.com/100854251935781152705/posts/PS41NdsBC6u) &mdash; content and formatting may not be reliable*
