---
layout: post
title: "I printed the test cubes and lowered the acceleration speed from 9000 to 1500, tighten the tension belt and jerk 20 to 30 but the vertical ring is still there"
date: April 29, 2015 07:28
category: "Discussion"
author: BoonKuey Lee
---
I printed the test cubes and lowered the acceleration speed from 9000 to 1500, tighten the tension belt and jerk 20 to 30 but the vertical ring is still there. I noticed that lining the three cubes in the same print axis, the vertical ring is consistent (see image).  Its not as obvious now but it is still visible.



I tried lowering the voltage but the motor started missing some steps.



So could this be due to the smoothness of the axis rod and the movement?

![images/e55ebe5063c7a1c99d6f9bcc034225e6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e55ebe5063c7a1c99d6f9bcc034225e6.jpeg)



**BoonKuey Lee**

---
---
**Miguel Sánchez** *April 29, 2015 15:27*

have you checked if the ridges match your belt pitch? 


---
**James Rivera** *April 29, 2015 17:19*

It still looks like ringing from the corners to me. Each machine is different, and I may be odd in this respect, but I have my acceleration at 1000.


---
**BoonKuey Lee** *April 30, 2015 02:06*

**+Miguel Ricardo** no. the band is of different width.



**+James Rivera** the reduce acceleration speed does not change much in my machine. The xy movement is not very smooth (like my i3), it may be the cause.


---
**Miguel Sánchez** *April 30, 2015 06:23*

Then it might be the bearing exhibits a varying force while moving, causing a non-smooth movement. Bearing alignment or lack of smooth surface on the rods could be te cause. Do you have an elastic belt tensioner? (like a spring).


---
**Colin Daly** *May 09, 2015 15:00*

Miguel is correct.  I built an ingentis-based printer before and had this problem.  It comes from your bushings stuttering which is caused by small misalignment.  Everything must be as parallel as possible for the bushings to work well.  You should be able to move the carriage freely without belts attached in my opinion.  Longer bushings also help.  I ended up scrapping my old build because of this alignment problem and I'm building an improved one, now with 30mm long bushings from robotdigg and less reliance on printed parts.


---
*Imported from [Google+](https://plus.google.com/105655952080130147525/posts/4ERGyK9iakd) &mdash; content and formatting may not be reliable*
