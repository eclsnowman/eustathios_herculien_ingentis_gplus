---
layout: post
title: "Trying Repetier to run break in code"
date: September 02, 2015 21:28
category: "Discussion"
author: Gus Montoya
---
Trying  Repetier to run break in code. gcode is loaded but won't run. I wish their was a customer support for this... Can someone share their screen shot's of their repetier configuration? Maybe I am missing something? 



UPDATE: I uninstalled the old repetier version and installed the newest version. I also used the web based configuration wizard. When I submit the break in g-code manually I get the following error..." CONNECTION LOST - Uups! I lost connection to the server. Trying to reconnect". When I open Repetier , I am able to connect to the printer and control it manually.

![images/b6bfd5876988759db3af2d7d3a90f483.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b6bfd5876988759db3af2d7d3a90f483.jpeg)



**Gus Montoya**

---
---
**Gus Montoya** *September 03, 2015 08:46*

So I guess no takers?


---
**rafael lozano** *September 03, 2015 10:03*

**+Gus Montoya** hablas español?..  indicame que electronica estas utilizando. 


---
**Gus Montoya** *September 03, 2015 14:40*

Hola Rafael

  Recibi tu pregunta. Recien sali del trabajo. Te respondo cuando despues de dormir. Gracias


---
**Frank “Helmi” Helmschrott** *September 06, 2015 06:40*

**+Gus Montoya** if you're searching for customer support you'd probably have better gone with a commercial printer.



<b>edited</b> 



ERR, sorry, didn't see the second part of your updated message. What do you mean by "submitting manually"? Don't use an older version of Repetier - that won't change anything and the newer version should work as well. Can you tell what the log window says if you just load the break in code and hit start? You can paste it to [http://pastebin.com](http://pastebin.com) and paste the URL here if it's too long and you don't know what the important part is. It would probably be good to have a complete log from connection to print start to help you better.


---
**Gus Montoya** *September 06, 2015 08:07*

I've been stuck at work. 12hrs work, rush home sleep, wake up, eat, shower repeat. Ill try to find time one of these days.






---
**Eric Lien** *September 07, 2015 15:24*

**+Gus Montoya**​ aren't you using a smoothieboard? If so the web firmware config won't help. That is for repetier firmware which I don't think runs on the smoothieboard.


---
**Frank “Helmi” Helmschrott** *September 07, 2015 17:51*

oh i was totally overreading that web configuration part. Of course the Repetier Firmware is not for you Smoothieboard - that only runs with the Smoothieware - but you should have already noticed that part in the board selection dropdown of the Repetier Configurator.


---
**Gus Montoya** *September 09, 2015 04:02*

@Frank helmschrott. I have not noticed that, not sure what you are referring to. I haven't touched the 3d printer since my last post. Been working.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/GYYZGQDNGNE) &mdash; content and formatting may not be reliable*
