---
layout: post
title: "First World Problems: My webcam doesn't have a wide enough angle to shoot my entire build platform"
date: October 22, 2014 03:40
category: "Discussion"
author: Mike Miller
---
First World Problems: My webcam doesn't have a wide enough angle to shoot my entire build platform. 





**Mike Miller**

---
---
**Eric Lien** *October 22, 2014 04:47*

I had to get this for #HercuLien. It works great. 



[http://www.amazon.com/dp/B005C3CSXC/ref=cm_sw_r_udp_awd_dAZrub15MHQZM](http://www.amazon.com/dp/B005C3CSXC/ref=cm_sw_r_udp_awd_dAZrub15MHQZM)



Just apply the metal ring and apply the lens (has a magnet)


---
**Daniel Fielding** *October 22, 2014 09:53*

First class first world problems


---
**Jean-Francois Couture** *October 22, 2014 15:24*

I have a logitech C920 and use OBS to record ([https://obsproject.com](https://obsproject.com)). Works pretty good


---
**George Salgueiro** *October 26, 2014 17:12*

You can adjust the distante of the lens spining it in one or other direction. 


---
**Mike Miller** *October 26, 2014 18:46*

You can, but it's more a field of view issue, I don't want the camera of a 3' stick outside of the printer. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/PAvWH4rNwq6) &mdash; content and formatting may not be reliable*
