---
layout: post
title: "Thank you Derek Schuetz , the build will continue (after my midterms and finals)"
date: April 28, 2015 00:33
category: "Discussion"
author: Gus Montoya
---
Thank you **+Derek Schuetz** , the build will continue (after my midterms and finals). I should have take a selfie with your dog's. They were really happy and joyful :)

![images/9335806b35561465cdd34e0bca3ccc1d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9335806b35561465cdd34e0bca3ccc1d.jpeg)



**Gus Montoya**

---
---
**Eric Lien** *April 28, 2015 00:45*

**+Derek Schuetz**​ you are a gentleman and a scholar. I love watching the support this comminuty brings. **+Gus Montoya**​ I am excited to see your progress. Take notes along your way about how best to document the build for others.


---
**Gus Montoya** *April 28, 2015 02:49*

I'll try to document everything. But honestly Derek's dogs were the show stoppers and not his herculein (which was a sight to see - it was huge). 


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/E9nTpHm9Nn3) &mdash; content and formatting may not be reliable*
