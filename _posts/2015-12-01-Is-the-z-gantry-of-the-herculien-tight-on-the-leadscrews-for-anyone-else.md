---
layout: post
title: "Is the z gantry of the herculien tight on the leadscrews for anyone else?"
date: December 01, 2015 04:27
category: "Discussion"
author: Jim Stone
---
Is the z gantry of the herculien tight on the leadscrews for anyone else? i seem to have to tug on the belt with alot of force.



this is minus the heatbed assembly.



is this just how it is? my other printer with threaded rods requires less force.



im pretty sure its not one side higher than the other. but it seems to be tighter at the bottom than the top.



:(





**Jim Stone**

---
---
**Eric Lien** *December 01, 2015 04:41*

Did you put the lower mounts all the way against the vertical 20x80. There is supposed to be a space allowing for adjustments.


---
**Zane Baird** *December 01, 2015 04:41*

Do you have the bottom bearing holders flush against the V-slot rail? There should be some space between the two. You will have to play with the amount of space until the motion is smooth. It shouldn't require much force on the belt at all to move the bed. 


---
**Jim Stone** *December 01, 2015 04:43*

Ah..yes the bottom holders are flush. So I assume loosen them up a bit and run it up n down slowly tightening the brackets down?


---
**Eric Lien** *December 01, 2015 04:45*

[http://i.imgur.com/oApaLJL.png](http://i.imgur.com/oApaLJL.png)


---
**Zane Baird** *December 01, 2015 04:47*

**+Eric Lien** looks like you beat me to that by less than a minute


---
**Eric Lien** *December 01, 2015 04:58*

**+Zane Baird**  That's OK. I will let you get the next one :)



Also do you think I should modify the part to be sized with no adjustment to fit right against the vertical vslot? This has been a common problem for almost every builders. I left it with degrees of freedom so that perfect alignment could be obtained... but those freedoms come with complexity. I just figured not everyone's prints are perfectly dimensionally accurate, so this freedom would give a little wiggle room.


---
**Zane Baird** *December 01, 2015 05:05*

**+Eric Lien** Based on the problems people seem to have with it, it may be a good idea to make the part flush with the extrusion. If people are really printing parts that are inaccurate to the extent of 1mm, they are likely to have some serious issues with every aspect of the build involving a printed part... Plus, If it's a matter of 100s of microns or so it's unlikely it will effect the alignment too greatly (and if it does, there is always the option of sanding it down or putting a sheet of card stock between the two to get the right alignment).



Just my $0.02


---
**Jim Stone** *December 01, 2015 05:12*

i would just make a note of it in the build doc :P


---
**Jim Stone** *December 01, 2015 06:44*

could one of you measure your extrusion opening. im having a hell of a time getting the cover in for the plexi.i think my opening may be smaller.@5.28mm:( the vslot i have at its narrowest is 6.3 somethingdang it...


---
**Eric Lien** *December 01, 2015 11:46*

**+Jim Stone** I put a slight bevel/chamfer on my plexi edges. It helps them slide into the track easier.




---
**Jim Stone** *December 03, 2015 17:57*

i know that. but i cant even get the plastic track in the v groove to get the plexi in the plastic track D:


---
**Zane Baird** *December 03, 2015 19:34*

**+Jim Stone** Can you send a picture? That's really strange


---
**Jim Stone** *December 03, 2015 19:36*

Yeah gimme a bit. Internet is all weird atm I can however slide the stuff in the rail sideways just not downwards. Mangled a piece of the stuff doing that lol 



But to just hide wires the stuff works fine


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/Bgh7KKhLA9X) &mdash; content and formatting may not be reliable*
