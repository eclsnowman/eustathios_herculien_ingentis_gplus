---
layout: post
title: "Had anyone tried out a remote direct drive extruder such as the Flex3Drive or Zesty Nimble?"
date: October 10, 2017 17:48
category: "Discussion"
author: Ryan Fiske
---
Had anyone tried out a remote direct drive extruder such as the Flex3Drive  or Zesty Nimble? 





**Ryan Fiske**

---
---
**Dmitry Romanovich** *October 11, 2017 13:45*

Here is another design to consider:  [youmagine.com - Zero Gravity Extruder by Gudo & Neotko - For UM2 and UMO+2](https://www.youmagine.com/designs/zero-gravity-extruder-by-gudo-neotko-for-um2-and-umo-2)  I'll be testing a variation of this design on my Ultimaker and maybe scale it up to Eustathios in the future.


---
**Mutley3D** *October 11, 2017 17:50*

Ill be happy to help you out with a couple of design variants/support that would be suited to your machine, or alterations in case you have different gantry rod spacing or diameters. Flex3Drive has key differences in its design providing greater performance when compared to the competition, not to mention being the original developer and also comes with full cooling solution. Happy to field any questions, or ping me an email through the site. Also have dedicated UMO and UM2 designs.


---
**Jim Stone** *October 12, 2017 06:16*

would recommend flex3drive for being awesome and being built tough and engineered well.



the nimble is too brittle in my usage and the guys behind it are ....not my kinda people and ill leave it at that.








---
**Daniel Zwierzu Zwierzchowski** *October 29, 2017 09:51*

J


---
**Daniel Zwierzu Zwierzchowski** *October 29, 2017 09:51*

Hi


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/NvT9ici2Ctf) &mdash; content and formatting may not be reliable*
