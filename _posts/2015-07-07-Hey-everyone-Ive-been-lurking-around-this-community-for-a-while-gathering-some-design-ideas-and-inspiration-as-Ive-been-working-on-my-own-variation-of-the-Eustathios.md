---
layout: post
title: "Hey everyone! I've been lurking around this community for a while gathering some design ideas and inspiration as I've been working on my own variation of the Eustathios"
date: July 07, 2015 19:23
category: "Show and Tell"
author: Chirag Patel
---
Hey everyone! I've been lurking around this community for a while gathering some design ideas and inspiration as I've been working on my own variation of the Eustathios. And here it is!



So a bit of background behind this project: I'm heading off to college in a few months and I decided that I want to bring a printer with me. However my current printer, a Prusa i3, feels rather large, is not easy to travel with, and has a few problems that I realized are addressed by the Eustathios design. And so I decided to try my hand at designing a printer based on the Eustathios design, creating it with CAD in Autodesk Inventor and trying to keep an updated bill of materials to get some good design and organization practice. Hopefully I can show off my documentation of this project when I start applying to internships in college!



I'm still ironing out a few details but I should be ready to start ordering parts and building this soon. This whole printer will fit in a roughly 12.5" cube with a 200mm x 200mm x 180mm print volume. The plan is to get a dual extruder system working eventually (as shown in this picture) but I'll be starting with a single extruder. I'll be posting some updates as I make my next steps in this project.

![images/00c6082c1971eb54a83ee93091f7482a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/00c6082c1971eb54a83ee93091f7482a.jpeg)



**Chirag Patel**

---
---
**Eric Lien** *July 07, 2015 21:45*

I love all the variations the group comes up with. I am excited to follow your progress.


---
*Imported from [Google+](https://plus.google.com/101451310698405542910/posts/DWQByHyaMa9) &mdash; content and formatting may not be reliable*
