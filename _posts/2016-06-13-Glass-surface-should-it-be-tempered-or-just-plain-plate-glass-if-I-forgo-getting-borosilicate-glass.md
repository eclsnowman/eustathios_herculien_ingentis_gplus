---
layout: post
title: "Glass surface, should it be tempered or just plain plate glass (if I forgo getting borosilicate glass)??"
date: June 13, 2016 22:29
category: "Discussion"
author: jerryflyguy
---
Glass surface, should it be tempered or just plain plate glass (if I forgo getting borosilicate glass)?? 







**jerryflyguy**

---
---
**Tomek Brzezinski** *June 13, 2016 22:44*

borosilicate glass is not necessarily best, at least especially because of varied formulations and what it really means these days. It's sometimes doped for temperature resistance and other times for impact resistance.



A lot of people talk about buying specialty glass but I really am happy with cheap thinnest picture frame glass I can get. Breaks sometimes? Yes. But cheap, replaceable, and closer to the temperature of the heater.   Then again, my built platform has never been more than 8x8 and there may be additional strain induced by the size of the herculien plate. 



Exceptions to the need of tempered or temperature-doped glass: If you have a high power heater and especially if you don't have a decent heat spreader.



I don't know the specifics but recall looking into certain specialty glasses that actually sucked because they were more likely to flake on the surface, which is how most of my glass has eventually died. I believe richrap or nophead wrote about this online at some point (explaining it)


---
**jerryflyguy** *June 13, 2016 22:46*

**+Tomek Brzezinski** interesting, I'm using a 500w heater under a 1/4" alum plate. The plain glass is only $5 cheaper than tempered, but way fast to get me hands on. Maybe I'll buy one of each and see which lasts longer. 


---
**Eric Lien** *June 13, 2016 22:47*

Go with standard window glass. Standard window glass is an annealed glass and so has no/less internal stresses. Conversely, tempered window glass is put into tension via the tempering process. This is so if struck from the outside it has and internal force trying to hold it together. But tempered glass does not fair as well when thermally shocked during heat up. When heated (until the aluminum heat spreader below it reaches thermal equilibrium) there will be a gradient of heat across it. So the tempered glass will no longer have even force internally and the tension of itself can cause a crack... especially if you have metal clips holding the glass down creating point loads into the glass.



Borosilicate is another thing entirely from standard tempered glass. Borosilicate is great for low thermal expansion and does not tend to break when thermally shocked. But as you may have have seen, people can have failures with materials like PETG where huge sections of the glass rip out if the plastic contracts with materials that stick hard to your glass surface coating.



Long story short... use standard window glass. Also look into PEI sheet down the road for awesome bed adhesion. You can 3M 468mp tape the PEI right down to the glass. The glass provides the flatness... the PEI provides the adhesion. It's a winning combo.﻿


---
**Ryan Carlyle** *June 13, 2016 23:36*

Don't do tempered unless you like the idea of fishing thousands of small pieces of glass out of the guts of your printer. Tempered is also the least flat kind of glass. It's just not good for print beds. 



Borosilicate is the lowest thermal expansion, which helps with cold-release of stuck prints, but as noted by others, you should use an adhesion layer between the glass and print to avoid damage. 



Regular mirror or picture frame glass is the flattest and cheapest and performs just fine. 


---
**Oliver Seiler** *June 13, 2016 23:51*

+1 for PEI, I've waited way too long to get mine ;-)

Be aware that tempered glass might not be as flat as floating glass - I tried it first and it was way too wobbly. 5mm float glass works fine for me. 


---
**Tomek Brzezinski** *June 14, 2016 02:21*

**+Eric Lien**​ solid information, and way more accurate. Also mentioned tempered being good. Was thinking annealed. Tempered not helpful in this case.  ﻿


---
**Matthew Kelch** *June 14, 2016 02:27*

I'm using PEI on $3 piece of window glass from the hardware store. Definitely the way to go!﻿ Like **+Eric Lien**​ said, use the proper 3M adhesive, preferably a single sheet rather than tape. It's miserable to work with but once your PEI is attached it's not coming off. 


---
**Maxime Favre** *June 14, 2016 05:30*

Agree on PEI sheet. For now I use Ikea mirror, pack of 5 for a few $


---
**jerryflyguy** *June 14, 2016 06:15*

Thanks all, I've got a piece of window glass to pick up tomorrow afternoon, skipped the tempered glass(luckily). I've got wheels turning to have the Alum bed plate etched/engraved w/ a nice Eusathios oriented design (think: BigBox's bed art crossed w/ Eusathios theme's) anyway, would prefer to not cover it up w/ PEI if I can help it.. But it may come down to function over form. 


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/HVh7wQBt5v8) &mdash; content and formatting may not be reliable*
