---
layout: post
title: "Hey guys, as college is approaching, I am looking to build my self a small 3D printer that can hide under a bedside table or something"
date: October 28, 2018 15:37
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hey guys, as college is approaching, I am looking to build my self a small 3D printer that can hide under a bedside table or something. My Eustathios and CR-10S are way too big.



Going to absolute quiet as well with X5 GT with steathchop enabled.



I really like walters remix of the mini. could be something to work off of.



I also have over $1000 of linear rail (like 20 300mm tracks) I gutted from high end printers.



Any thoughts.







![images/8d25b810065d1e1d9909060696b39140.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8d25b810065d1e1d9909060696b39140.jpeg)
![images/146e67f36e7e9d6dc985e4c6f8033169.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/146e67f36e7e9d6dc985e4c6f8033169.jpeg)

**Stefano Pagani (Stef_FPV)**

---
---
**Dennis P** *October 28, 2018 18:50*

Dice if you want to build something. 

[https://well-engineered.net/index.php/en/projects/12-dice-english](https://well-engineered.net/index.php/en/projects/12-dice-english)

See if you can find a Turnigy Fabricator Mini / Tinyboy Design used. Like this one...this sits on top of my workstation.  [photos.google.com - New photo by Dennis P](https://photos.app.goo.gl/XY23a3QPsjUpKMhJ9)


---
**Ray Kholodovsky (Cohesion3D)** *October 28, 2018 19:16*

I do like my DICE. 


---
**Stefano Pagani (Stef_FPV)** *October 29, 2018 02:43*

Woah that looks crazy nice


---
**Patrick Woolfenden** *October 29, 2018 02:56*

Kitten? [github.com - woolfepr/Printer-Kitten](http://github.com/woolfepr/Printer-Kitten) It's kind of like a mini Eustathios.


---
**Dennis P** *October 29, 2018 03:57*

there is also the Tantilus Reborn - [scheuten.me - Tantillus R &#x7c; Spielplatz](http://scheuten.me/?page_id=1056)

which is also in part of the Eustathios evoluition


---
**Stefano Pagani (Stef_FPV)** *October 29, 2018 19:01*

Ok I’m building a dice. I have a ton of rails and most of the parts, now I just need to finish my MPCNC


---
**Stefano Pagani (Stef_FPV)** *October 30, 2018 12:06*

**+Ray Kholodovsky** yeah I saw you build log. Looks like you got all the parts in a kit? I think I get the panels cut. Obviously trying to spend as little as possible. Currently at $250 (+the panels) if my Azteeg X5 Gt works 


---
**Ray Kholodovsky (Cohesion3D)** *October 30, 2018 16:47*

René put together a kit of many parts for me. I provided my own electronics, hotend, extruder...

The BOM cost is approx usd 1250 as of last count. 


---
**Stefano Pagani (Stef_FPV)** *October 30, 2018 20:00*

**+Ray Kholodovsky** yeah I have a ton of parts so I got to $250


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/5Fc4tLQ7s1Y) &mdash; content and formatting may not be reliable*
