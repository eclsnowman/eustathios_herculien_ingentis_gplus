---
layout: post
title: "A mount to fit the Bondtech Extruder to the Herculien-frame, c-c distance is 60 mm to fit on the top 20x80 extrusion"
date: July 22, 2015 17:09
category: "Show and Tell"
author: Martin Bondéus
---
A mount to fit the Bondtech Extruder to the Herculien-frame, c-c distance is 60 mm to fit on the top 20x80 extrusion. Use 35 mm long M5 bolts and M5 T-slot nuts.



STL-file downloadable from [www.bondtech.se/stl](http://www.bondtech.se/stl)

![images/d86997d3c62363a9c260a7e0177f9685.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d86997d3c62363a9c260a7e0177f9685.jpeg)



**Martin Bondéus**

---
---
**Vic Catalasan** *July 22, 2015 17:11*

That is awesome thank you!


---
**Martin Bondéus** *July 22, 2015 17:36*

You are welcome **+Vic Catalasan** 


---
**Eric Lien** *July 22, 2015 19:29*

I know what I'm printing tonight. Great job as always Martin.


---
**Brent ONeill** *July 22, 2015 20:15*

really like the design **+Martin Bondéus** , sexy. I'm going to print your  housing in clear resin on my SLA printer...polish all surfaces to glass clear and smoothness so you can see all interior workings. I printed a glass clear version of **+Walter Hsiao**'s  housing on Thingiverse, it's very cool to see the internal workings pushing filament onward.


---
**Martin Bondéus** *July 22, 2015 21:00*

That is cool **+Brent ONeill** , have you started to use the extruders on your Herculien printer?


---
**Gus Montoya** *July 22, 2015 21:42*

**+Brent ONeill**  Oh man that is very nice in deed. Can you provide pic's of what you have already done? That must be a good sight. The filament available to us (as far as I know) is not 100% translucent and clear. :(


---
**Mike Thornbury** *July 22, 2015 23:51*

Very nice!


---
**Brent ONeill** *July 23, 2015 02:02*

That pic on Thingiverse was an attempt at an artsy type environment, sort-of got there. I have pics of the same housing loaded with the superb BondTech internals...I'll try and get those posted soon. Too many projects, too much funage and not enough time. I should put my name in the hat for the longest Herc build, I haven't got that far yet **+Martin Bondéus** and  **+Gus Montoya**  . Right now my SLA printer is inside the start of the frame for the Herc, running out of shop space. Thinking of moving my stove into storage and setting-up in the kitchen ;)


---
*Imported from [Google+](https://plus.google.com/+MartinBondéus-Bondtech/posts/fWJdpAemrSp) &mdash; content and formatting may not be reliable*
