---
layout: post
title: "I tried to slice the extruder head of Eustathios (v2.1, youmagine) but can't slice it correct"
date: December 08, 2014 21:25
category: "Discussion"
author: Florian Schütte
---
I tried to slice the extruder head of Eustathios (v2.1, youmagine) but can't slice it correct. When using curaEngine, the cable tunnel is interrupted because it is solid filled between the point where it turns to the point where it reaches the splitting of the tunnel. 

With slic3r the tunnel is ok, but the rest of the model is messed up or not even sliced.

Which slicer did you use to slice the printer parts? Which settings did you use?

I played around with wall thickness, infill, layer height ... all failed...





**Florian Schütte**

---
---
**Florian Schütte** *December 08, 2014 21:26*

when i slice in upright position in cura, the tunnel is filled from top down, and will be ok after the point it turns. The comment above described the situation when model is rotated 90° in x direction.


---
**Eric Lien** *December 08, 2014 21:43*

I thought **+Jason Smith**​ repaired that in version 2.1. It is a sketch up to stl issue. Try running it through [https://netfabb.azurewebsites.net](https://netfabb.azurewebsites.net)


---
**Eric Lien** *December 08, 2014 21:45*

Otherwise you could use mine. It uses bronze bushings instead of bearings. It has worked great for me: [https://github.com/eclsnowman/Lien3D_Eustathios_Spider/blob/master/Other%203D%20Formats/STL/Extruder_Carriage_V3.STL](https://github.com/eclsnowman/Lien3D_Eustathios_Spider/blob/master/Other%203D%20Formats/STL/Extruder_Carriage_V3.STL)﻿



But it does require extensive support removal. But I use simplify3d so that is no issue.


---
**Florian Schütte** *December 08, 2014 21:50*

Repairing worked great. Thank you! Didn't knew the site before.


---
**Jason Smith (Birds Of Paradise FPV)** *December 08, 2014 22:39*

Hey all. Sorry, I thought I had repaired that model before posting. If you'd like to share the cleaned up stl, I'll update the repos. 


---
**Eric Lien** *December 08, 2014 23:51*

**+Florian Schütte** It is great. All the mesh repair features of desktop netfab, but automated and in the cloud. 


---
**Florian Schütte** *December 09, 2014 02:21*

**+Jason Smith** [https://drive.google.com/file/d/0BzSHVTmM3bVOZzFIanNMeDFacVE/view?usp=sharing](https://drive.google.com/file/d/0BzSHVTmM3bVOZzFIanNMeDFacVE/view?usp=sharing)


---
**Jason Smith (Birds Of Paradise FPV)** *December 09, 2014 03:34*

Updated. Thanks for sharing.


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/Vr2uFA8irBb) &mdash; content and formatting may not be reliable*
