---
layout: post
title: "Slowly progressing forward"
date: February 26, 2015 07:56
category: "Show and Tell"
author: Derek Schuetz
---
Slowly progressing forward.

![images/85bed9f96ac60f4386902c100a4ed43a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/85bed9f96ac60f4386902c100a4ed43a.jpeg)



**Derek Schuetz**

---
---
**Jason Perkes** *February 26, 2015 11:59*

Looking like a solid and well organised build. Nice work indeed!


---
**Eric Lien** *February 26, 2015 13:01*

Looking good.


---
**Daniel Salinas** *February 26, 2015 14:53*

Looks great Derek!


---
**Derek Schuetz** *February 26, 2015 16:16*

**+Eric LeFort** Wait on **+Daniel Salinas** to finish his guide, i have had to do alot of self solving or asking daniel or eric to get things right and lots of frustrating reassembly. but i picked this because i like the challenge of self solving or having to find things out on the fly.


---
**Derek Schuetz** *February 26, 2015 16:38*

**+Eric LeFort** Shipping is a killer do as much as you can to avoid multiple vendors. you can get all belts on robotdigg which will save you a fair bit of money compared to getting them from 3 vendors at a higher price. also look into doing one extruder. according Eric 2 is just not worth it. (it could potentially save you about $200)


---
**Chris Brent** *February 26, 2015 16:49*

Thanks **+Derek Schuetz** and **+Daniel Salinas**  and **+Eric Lien**. Exciting times! I'm pretty sure a HercuLien is in my future now I see these builds coming together. Some problem solving = good, reinventing the 3D printer = bad.


---
**Eric Lien** *February 26, 2015 18:09*

If people take good notes and we help **+Daniel Salinas**​ with his awesome documentation effort things should go more smoothly for future makers. I am trying to make better prints, but it does take time. I am trying (and unfortunately failing) to strike the proper balance of work, family, HercuLien, and my efforts on the Eustathios Spider V2. Any help from others is always appreciated.



Big thanks should go out to **+Daniel Salinas**​, he is really doing great efforts in this area.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/UBtyiKPLVH3) &mdash; content and formatting may not be reliable*
