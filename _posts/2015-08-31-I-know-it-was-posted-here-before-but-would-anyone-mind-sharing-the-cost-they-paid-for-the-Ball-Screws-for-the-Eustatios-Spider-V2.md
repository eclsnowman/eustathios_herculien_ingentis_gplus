---
layout: post
title: "I know it was posted here before, but would anyone mind sharing the cost they paid for the Ball Screws for the Eustatios Spider V2"
date: August 31, 2015 16:49
category: "Discussion"
author: Eric Lien
---
I know it was posted here before, but would anyone mind sharing the cost they paid for the Ball Screws for the Eustatios Spider V2. Did machining the ends cost more? What was delivery time like.



I am thinking of upgrading mine. And finally converting mine from a V1.5 to V2.





**Eric Lien**

---
---
**Frank “Helmi” Helmschrott** *August 31, 2015 16:53*

Machining was at no cost for me. I sent them a dxf containing the profile drawing of what i wanted. I think i paid the price for the next length they offered. Here's the link to the product i ordered: [http://www.aliexpress.com/item/Ballscrew-RM1204-L-450mm-with-end-machining-Single-Ballnut-for-CNC/1588701082.html?spm=2114.01020208.3.2.JLD3ep&ws_ab_test=201407_2,201444_5,201409_4](http://www.aliexpress.com/item/Ballscrew-RM1204-L-450mm-with-end-machining-Single-Ballnut-for-CNC/1588701082.html?spm=2114.01020208.3.2.JLD3ep&ws_ab_test=201407_2,201444_5,201409_4) (edited post for the international link)


---
**Bryan Weaver** *August 31, 2015 16:59*

I ordered the 500mm rods and didn't get them machined.  Didn't do enough research before pulling the trigger.  Ordered from the same seller (golmart).  $20 each for the ballscrews + $23 shipping for a total of $63.  Ordered on June 30, delivered on July 17.


---
**Joe Spanier** *August 31, 2015 17:02*

I did the screws for my mill conversion from an eBay seller in china. It was 185 or so shipped and less than 6 days order to delivery. Way worth it. 


---
**Frank “Helmi” Helmschrott** *August 31, 2015 17:03*

Delivery also was quite quick here - even with the customization. As long as you're not super experiences and weaponed with a good Lathe i'd suggest letting the guys in China do the machining.



**+Joe Spanier** 185$? Guess those were bigger/longer ones, right?


---
**Joe Spanier** *August 31, 2015 17:11*

**+Frank Helmschrott** yes they were for an x3 mill so 20mm ball screws with a bunch of machining. I was just amazed at how fast it was


---
**Igor Kolesnik** *August 31, 2015 18:10*

1204 machined as per BOM, about two weeks to deliver from golmart. Explained them how it should be machined and they did a good job. 100$ with change including delivery.﻿ Machining was included


---
**Frank “Helmi” Helmschrott** *August 31, 2015 18:28*

additional pro tip: tell them in which direction to mount the nut - saves you time and worries ;-) 


---
**Igor Kolesnik** *August 31, 2015 18:45*

Just a small trick how to flip those nuts. Wrap small sheet of paper around machined end to form a tube one layer more than diameter of the groove and screw the nut down on it. Than take it off, flip it and Bob's your uncle﻿


---
**Joe Spanier** *August 31, 2015 18:57*

mind blown  *<b>*BUSSHISSSHHHH***</b>


---
**Igor Kolesnik** *August 31, 2015 19:15*

No. 1204 can hold it + geared belt


---
**Ishaan Gov** *September 01, 2015 02:40*

 Would it be at all worth to go with the "proper" end machining designed for the BK/BF12 and the preload nut? Or is that kinda overkill for 3D printers?


---
**Igor Kolesnik** *September 01, 2015 03:01*

The ballnut that I got, does not have any noticeable backlash. Plus gravity counters backlash and movement that matters is mostly in one direction so backlash shouldn't be an issue.


---
**Ted Huntington** *September 01, 2015 18:04*

I bought mine from [http://www.aliexpress.com/item/RM1204-Ball-Screw-SFU1204-L-450mm-Rolled-Ballscrew-with-single-Ballnut-for-CNC-parts/1933681333.html](http://www.aliexpress.com/item/RM1204-Ball-Screw-SFU1204-L-450mm-Rolled-Ballscrew-with-single-Ballnut-for-CNC-parts/1933681333.html) for $18.21 each with machining of both ends (total=$33.72), shipping was $65 (but I got all the linear shafts needed for Eustathios Spider 2  - and others for a different 3D printer I'm building). I sent them the .png image from this list and the nut is facing the correct way. I had them turn both ends down to 8mm. Shipping came within 2 weeks. Although I haven't connected them, they are pretty close in size, were well packaged, have no backlash, and seem to have survived the journey.


---
**Roland Barenbrug** *September 17, 2015 14:36*

**+Frank Helmschrott** Are you able to share the exact profile drawing that you used? 


---
**Frank “Helmi” Helmschrott** *September 17, 2015 15:17*

**+Roland Barenbrug** unfortunately not, already tried but i can't find it anymore. Maybe didn't save it for later as it was really just a simple sketch. You shouldn't have any problems recreating this. If you do please let me know and i could try to reacreate it tomorrow or on the weekend.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/gDpGYpPs1pw) &mdash; content and formatting may not be reliable*
