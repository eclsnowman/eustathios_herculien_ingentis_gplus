---
layout: post
title: "Not really and Ingentis, but... Jan Derksen's (jand) CoreXY printing away"
date: May 08, 2014 18:57
category: "Show and Tell"
author: Matt Miller
---
Not really and Ingentis, but...



Jan Derksen's (jand) CoreXY printing away.  



Source:  [https://github.com/jand1122/RepRap-XY](https://github.com/jand1122/RepRap-XY)
Thread: [http://forums.reprap.org/read.php?4,297740,page=1](http://forums.reprap.org/read.php?4,297740,page=1)





**Matt Miller**

---
---
**Jan Derksen** *May 08, 2014 21:57*

Thanks Matt for the introduction here in the Ingentis community. I have followed the ingentis tread in the RepRap forum since the first post. I even considered to build one, but after reading more about corexy I decided to build a corexy system, triggered by the thread Zelogik started.


---
*Imported from [Google+](https://plus.google.com/+MattMiller_akhlut/posts/FqSPufU9TLq) &mdash; content and formatting may not be reliable*
