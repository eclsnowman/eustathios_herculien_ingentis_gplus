---
layout: post
title: "Well, after quite a few attempts, and lots of questions, I am printing pretty decent parts"
date: February 05, 2018 02:15
category: "Show and Tell"
author: Bruce Lunde
---
Well, after quite a few attempts, and lots of questions, I am printing pretty decent parts.  Thanks to all the responses to my questions, I have had 20 hours of good printing.  I know they are not perfect, and I need to make some more adjustments but happy to say I am on the printing road now!









![images/3ef8659ced1092ad2e732fc8aed153a8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3ef8659ced1092ad2e732fc8aed153a8.jpeg)
![images/fd7a51978e1537c85f5a4b86108e560d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fd7a51978e1537c85f5a4b86108e560d.jpeg)

**Bruce Lunde**

---
---
**Eric Lien** *February 05, 2018 03:53*

I am so glad you finally have things up and running. You started your journey longer ago than most. I just looked back and it was 152 weeks ago (give or take) since you got printed parts from **+Alex Lee** 



The most important part is things are running. So now the printer can become it's own method to fabricate parts and even your own upgrades. Thats the part I love most about our little community. All the creative ways people have made these printers better, or modifications people made so they fit people's needs more effectively.



Keep us updated as you proceed. And if at all possible make it out to MRRF 2018 this year. We can spend some time chatting and tuning things in even further.




---
**Bruce Lunde** *February 06, 2018 03:27*

**+Eric Lien** I do hope to get up to MRRF. I did not realize it had been that long since I started! Three job changes since then, lol. I am so excited as I have now printed PLA parts from the add-in’s people have pun into the repository! I needed a smaller filament holder and got it done!

![images/2c44a7bcba26ad1196728a4f45b8575e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2c44a7bcba26ad1196728a4f45b8575e.jpeg)


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/UTMSqAKYwzo) &mdash; content and formatting may not be reliable*
