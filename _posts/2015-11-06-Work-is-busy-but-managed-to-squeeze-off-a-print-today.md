---
layout: post
title: "Work is busy, but managed to squeeze off a print today"
date: November 06, 2015 00:08
category: "Deviations from Norm"
author: Matt Miller
---
Work is busy, but managed to squeeze off a print today.  Some ringing at the corners, and you can probably see where I went to 200% but then brought it back down to 150% after a few layers.  I need a better camera than the potatoe that they built into my phone.



Assembling the repo has stalled in favor of designing and building an enclosure for the machine that my institutions' health and safety committee is requiring.



:/



My engineering club students and I are working on the enclosure now.  Needless to say this is a kink I did not expect.  And don't get me started on UFP's...



![images/c7a80c509cfc2f1cb91d5167c63cfdbe.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c7a80c509cfc2f1cb91d5167c63cfdbe.jpeg)
![images/978eacdfc4ee05a96ce97e15b01432d1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/978eacdfc4ee05a96ce97e15b01432d1.jpeg)

**Matt Miller**

---
---
**Gústav K Gústavsson** *November 06, 2015 00:53*

Well will definitely not start on UFP's as I don't know what it means. But great looking print overall and yes I can see where you speeded it up . But I am interested in what your initial speed was and what filament?


---
**Matt Miller** *November 06, 2015 11:12*

Ultra Fine Particles. :(  they're emitted during the printing process.



Started at 60, went to 90, then 120, then back to 90 for the remainder.


---
**Brandon Satterfield** *November 07, 2015 14:00*

Great job **+Matt Miller**! 


---
*Imported from [Google+](https://plus.google.com/+MattMiller_akhlut/posts/1SVJT6ek7Pt) &mdash; content and formatting may not be reliable*
