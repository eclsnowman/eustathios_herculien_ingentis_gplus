---
layout: post
title: "My first usable part on the printer"
date: June 23, 2015 22:10
category: "Show and Tell"
author: Vic Catalasan
---
My first usable part on the printer. I did not get a chance to modify the V2 spider Viki2 mount but I will eventually adjust it to fit the Herculien.



![images/b1b627da665d75f0234860d6a183432c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b1b627da665d75f0234860d6a183432c.jpeg)
![images/e5de8eaf23a945fbf92d53a624573b4f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e5de8eaf23a945fbf92d53a624573b4f.jpeg)
![images/50098c9773ef52941ad789e308ee5277.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/50098c9773ef52941ad789e308ee5277.jpeg)

**Vic Catalasan**

---
---
**Eric Lien** *June 23, 2015 22:14*

Great job. I never though of using that there :)



Great idea.



I see you are fighting the dangling wire & tube issue I used to. I found a coat hanger wire in the front right corner hole in the vertical extrusion going up about 2/3 the way makes for a nice guide for the bundle.



Keep it up.


---
**Vic Catalasan** *June 24, 2015 00:16*

Yup that spring wire helped out a lot and it was simple to install.


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/jpDZAp2YfKM) &mdash; content and formatting may not be reliable*
