---
layout: post
title: "I have open ended GT2 belt now if anybody needs it"
date: April 11, 2014 21:05
category: "Discussion"
author: Brian Bland
---
I have open ended GT2 belt now if anybody needs it.  $1usd/ft shipped in the US.

![images/f5cb3e5b6cd2c5b4d547e01a69ad5d56.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f5cb3e5b6cd2c5b4d547e01a69ad5d56.jpeg)



**Brian Bland**

---
---
**Eric Lien** *April 11, 2014 21:28*

I'll get some. Let me add up how much I need and let you know.


---
**Dale Dunn** *April 11, 2014 21:30*

What materials is it made of?


---
**Brian Bland** *April 11, 2014 21:46*

Neoprene Rubber, Fiberglass Reinforced


---
**ThantiK** *April 11, 2014 21:46*

Like **+Dale Dunn** I want to know if this is fiberglass reinforced, or kevlar.


---
**Carlton Dodd** *April 12, 2014 04:04*

Good price. I'll see how much I need, then probably greatly over-order since I like to have stuff on-hand.


---
**Brian Bland** *April 13, 2014 04:17*

**+Shauki Bagdadi** Need an address and preferred shipping method to let you know about shipping to Germany.


---
**Nuker Bot (NukerBot 3D Printing)** *May 30, 2014 14:34*

you still have some?


---
**Carlton Dodd** *May 30, 2014 14:40*

Damn.  Forgot about this.  (Thanks, **+Mike G** for bringing this back up for me!)  

If you still have it, I'm in for 20 feet.  PM me your preferred payment method.


---
**Nuker Bot (NukerBot 3D Printing)** *May 30, 2014 14:43*

No problem **+Carlton Dodd** I am sure he has some left. lol 20 feet sounds good for me too.


---
**Brian Bland** *May 30, 2014 14:51*

I still have it.  Will send you both a PM.


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/b3iVtmZpHgk) &mdash; content and formatting may not be reliable*
