---
layout: post
title: "Shared on June 09, 2017 21:57...\n"
date: June 09, 2017 21:57
category: "Show and Tell"
author: Daniel F
---


![images/1d6b7bf0cfc645da6421bede63e59840.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1d6b7bf0cfc645da6421bede63e59840.jpeg)



**Daniel F**

---
---
**Daniel F** *June 09, 2017 21:57*

Reprap...


---
**Eric Lien** *June 10, 2017 14:19*

I have no idea why your post got caught in the spam filter. But it did. Sorry I didn't get it out earlier. The amount of spam posts to filter through is getting crazy.


---
**Eric Lien** *June 10, 2017 14:22*

**+Daniel F**​ Nice MacGyver fix until you could print a replacement.


---
**Daniel F** *June 10, 2017 15:39*

No problem--I was just wondering why there was no reaction and if no one saw the clamp...


---
**Roland Barenbrug** *July 01, 2017 12:02*

Just wondering, will you print the new one in another direction. Looks the broken part was not printed in an optimal direction from a strength point of view.


---
**Daniel F** *July 01, 2017 12:33*

Same direction but different material (asa-x) and 70% infill, we will see how long it lasts...


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/dUdymum4xM5) &mdash; content and formatting may not be reliable*
