---
layout: post
title: "With both lead-screw/ballscrews being driven from the same motor, can the cross-beam of the Z-axis (HFSB5-2020-321-TPW) be eliminated?"
date: November 10, 2016 16:50
category: "Discussion"
author: Benjamin Liedblad
---
With both lead-screw/ballscrews being driven from the same motor, can the cross-beam of the Z-axis (HFSB5-2020-321-TPW) be eliminated?



Is it just there as an alignment aide?







**Benjamin Liedblad**

---


---
*Imported from [Google+](https://plus.google.com/111192213763748051453/posts/gPB1QSrhfFJ) &mdash; content and formatting may not be reliable*
