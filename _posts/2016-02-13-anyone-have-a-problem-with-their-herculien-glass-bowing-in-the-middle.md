---
layout: post
title: "anyone have a problem with their herculien glass bowing in the middle?"
date: February 13, 2016 04:16
category: "Discussion"
author: Jim Stone
---
anyone have a problem with their herculien glass bowing in the middle?



i just went to manually level. and i realised the middle inbetween each corner has a hump.





**Jim Stone**

---
---
**Anthony Webb** *February 13, 2016 05:14*

I don't have a herculein but on my printer there is a bow in the middle of the glass and it makes printing things that have moderately large surface area very difficult. 


---
**Jim Stone** *February 13, 2016 06:32*

found possible solution. hadnt printed the generic "glass clip" in the herculien files. only had the corner ones.will print the other onesand let know how it goes.



yay trying to print ABS on a not herculien :P


---
**Jim Wilson** *February 13, 2016 13:58*

Also: make sure you're leveling the bed when it's already heated, I forgot this all the time early on and it drove me nuts.﻿


---
**Tomek Brzezinski** *February 13, 2016 18:09*

What did you use to determine that there is a bow? I'm just asking because glass is superbly flat by nature of how it's made, so it is surprising. There aren't any forces that should be bowing it, either.   


---
**Jim Stone** *February 13, 2016 18:10*

by eye i can see a gap and when i push down at the apex it moves.





that and if i level all the corners the nozzle would run into the hump in the middle


---
**Tomek Brzezinski** *February 13, 2016 18:18*

What did you source the glass from? And if you flip it upside down do you have the same problem, a reversed problem, or nothing? 



How thick is the glass? 



Sorry I've been a lurker and don't know the exact specs on the herc. 


---
**Jim Stone** *February 13, 2016 19:20*

3.3mm

problem is still there after flipping. the almunim plate itself isnt bowing.



maybe i just need to get clips in the middle area. hopefully it doesnt push the center up


---
**Tomek Brzezinski** *February 13, 2016 20:00*

Hmm, seems unusual. Have to wonder what it's referencing that could force it to bow, if it doesn't have a bow naturally and the alu plate isn't bowed. It should be easy enough to find a 3.3mm piece of glass that is naturally flatter, if your piece happens to be unusually warped.  


---
**Jeff DeMaagd** *February 13, 2016 20:59*

3.3mm is pretty thin for a herc sized plate glass, is it supported in the middle?


---
**Jim Stone** *February 13, 2016 21:38*

**+Jeff DeMaagd**

3.3 is what is referenced and fits under the supplied clips. standard borosilicate glass.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/dsRASiZ1CDr) &mdash; content and formatting may not be reliable*
