---
layout: post
title: "Following up from Seth Messer 's thread here: I received my 2 pieces of 360mmx345mm Borosilicate glass from Cincinnati Gasket & Industrial Glass today"
date: May 08, 2015 05:03
category: "Show and Tell"
author: Isaac Arciaga
---
Following up from **+Seth Messer** 's thread here: [https://plus.google.com/+SethMesser/posts/BhqfYwSN2cB](https://plus.google.com/+SethMesser/posts/BhqfYwSN2cB)



I received my 2 pieces of 360mmx345mm Borosilicate glass from Cincinnati Gasket & Industrial Glass today. I give these guys a thumbs up! The glass was packaged well. Cost per piece when you buy 2 was $29.55 each plus $20 to ship to SoCal. That's a pretty good deal to me! Just make sure you at least buy 2 pieces. Otherwise it will be $50 for 1 piece (min order amount). It's good to have a spare anyways, especially for the custom size needed for the Eustathios.



I know of one 3D Printer company that buy bulk from these guys. They're legit.



![images/df7ebd05c8e575bdb5e2024c920517c4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/df7ebd05c8e575bdb5e2024c920517c4.jpeg)
![images/21a07b707431ba62673ee2de86274080.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/21a07b707431ba62673ee2de86274080.jpeg)
![images/653b304e1e76e063a4500f7ebb1b45ef.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/653b304e1e76e063a4500f7ebb1b45ef.jpeg)
![images/9c3393a626e9bae6b665ecb277e7bd3d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9c3393a626e9bae6b665ecb277e7bd3d.jpeg)
![images/180a5e25cad3d847991f992a769b7e5e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/180a5e25cad3d847991f992a769b7e5e.jpeg)
![images/f9e79d1e39a89706d1308fa5e40eb23d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f9e79d1e39a89706d1308fa5e40eb23d.jpeg)

**Isaac Arciaga**

---
---
**Dat Chu** *May 08, 2015 12:28*

Well if there is another person in Houston who wants one of these. ☺


---
**Isaac Arciaga** *May 08, 2015 15:46*

**+Dat Chu** you'll regret not buying a spare! ;)


---
**Dat Chu** *May 08, 2015 15:46*

True 😅. I am gonna try out a cheap piece of glass before this. $80 is quite some money and I can always upgrade later. 


---
**James Ochs** *May 10, 2015 13:05*

from their website, It looks like they have two different finishes, polished and rolled... Any pros or cons between the two?  Does it matter?


---
**Isaac Arciaga** *May 10, 2015 17:08*

**+James Ochs** I'm not sure if you have seen it yet, but I was following up on **+Seth Messer** 's thread here [https://plus.google.com/+SethMesser/posts/BhqfYwSN2cB](https://plus.google.com/+SethMesser/posts/BhqfYwSN2cB) you should give Steve a call. I did not indicate polished or rolled.


---
**James Ochs** *May 10, 2015 17:11*

No, I hadn't seen that comment... thanks though!  Not quite ready to order it, but when I do, I'll ask them the question.


---
**Isaac Arciaga** *May 10, 2015 17:14*

Yeah my fault. I just edited my post to reference it.


---
**Daniel Salinas** *May 11, 2015 19:07*

Funny story: I called 4 glass shops here in San Antonio and asked for borosilicate glass.  All 4 told me "uh... we don't carry whatever that is."  They only knew it as "pyrex".  How much was it for the sheet?


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/CezgGG1NprA) &mdash; content and formatting may not be reliable*
