---
layout: post
title: "So I'm gearing up to build the Herculien that I've been putting off for a while (even though I have most if not all of the mechanical build components)"
date: October 27, 2016 01:48
category: "Discussion"
author: Luis Pacheco
---
So I'm gearing up to build the Herculien that I've been putting off for a while (even though I have most if not all of the mechanical build components). I did have a question for the community, what controller are you all using and what should I go with?



I know I've seen mentions of the WIP Azteeg X5 GT, the BOM of course lists the Azteeg X3 V2, and I've also seen people using the Azteeg X5 V3. Should the GT be something worth waiting for? I've already waited a long time so a little longer wouldn't be a big deal. 



From a lot of things I read I'd lean more towards a smoothie-compatible board but with the Herculien's dual extrusion that's not easy to come by. What thoughts do you all have on this? Thanks!





**Luis Pacheco**

---
---
**Eric Lien** *October 27, 2016 01:57*

**+Ray Kholodovsky**​ you have any of your cohesion units for sale to get **+Luis Pacheco**​ going on a dual extrusion smoothieware setup.


---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2016 01:59*

I definitely support smoothie. I make another board that runs smoothie called the Cohesion3D ReMix and it has space for 6 drivers and other powerful features such as a 20 amp heated bed mosfet, direct connections for a Graphic LCD, and wifi! (experimental) 

The factory is making a batch for me right now so if you can wait a few weeks, we should have it available.  

And maybe you'll be the first to do triple extrusion on the HercuLien...

The size is 100x100mm so it's smaller than a smoothieboard. In not sure what the size of the GT is. ![images/8245da32b3eee3138eb745e0a0b345f9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8245da32b3eee3138eb745e0a0b345f9.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2016 02:03*

I do still have at least one factory sample board if you need it now. 


---
**Luis Pacheco** *October 27, 2016 02:16*

Thanks **+Eric Lien**​ **+Ray Kholodovsky**​ for the quick replies. I'm not in immediate need for it so I'll be happy to wait for that production batch to roll in. I wanted to do my homework ahead of time to avoid being stuck once I get it all together. I'm hoping to begin assembly this weekend but it'll be a few weeks until full completion anyways.


---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2016 02:17*

Sweet. Follow me on G+ so you know when we're up for ordering,

and touch base with me if you need it sooner. 

Looking forward to seeing your machine.


---
**Luis Pacheco** *October 27, 2016 02:24*

Followed. I'll try to keep an eye out. Appreciate the help!


---
**SalahEddine Redjeb** *October 27, 2016 09:35*

how much does your cohesion board cost? (keep in mind international shipping)


---
**Philipp Tessenow** *October 27, 2016 16:10*

I might be interested as well (Germany)... ;)


---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2016 21:19*

We're looking at $150 USD for the board.  You provide your own drivers, whichever you may prefer: A4988, DRV8825, the SilentStepSticks with Trinamic Drivers or anything new that comes out. 



We can usually do int'l shipping for no more than $35.


---
**Luis Pacheco** *October 27, 2016 21:39*

**+Ray Kholodovsky**​ Sounds good, I'm in the US so I'm assuming it's cheaper shipping? Also, which of the drivers do you recommend out of all the choices? I'll probably get some on order soon in anticipation for the board.


---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2016 21:43*

**+Luis Pacheco** Yeah domestic shipping is around $7. 

Ok, drivers: 

I use the cheap A4988 ones from China.  They're ok. They burn out but are cheap to replace. The DRV8825 can get you some higher torque but they run hot and absolutely need a cyclone of a fan to cool them or they'll overheat and thermal throttle. 

You can get the genuine versions of these from Pololu. 

Trinamics are fun but expensive. Check Filastruder. 

**+Eric Lien** any high torque or other particular requirements for the HercuLien I should be aware of before making generic driver suggestions? :)


---
**Eric Lien** *October 27, 2016 22:59*

**+Ray Kholodovsky** these are the best I have ever used, but out of stock: [http://www.panucatt.com/product_p/sd6128.htm](http://www.panucatt.com/product_p/sd6128.htm)



These are also very good: [http://www.panucatt.com/product_p/sd5984.htm](http://www.panucatt.com/product_p/sd5984.htm)



I would use these over the dvr8825.


---
**Jim Stone** *October 28, 2016 22:04*

**+Eric Lien** I agree. the 8825 drivers of all brands and creeds produce a very audible and annoying whine.



if i had known about the whine problems. i would havve got the 6128 probably.


---
**Ray Kholodovsky (Cohesion3D)** *November 20, 2016 05:57*

Quick update for **+Luis Pacheco** and everyone else: we've been working extremely hard to get to this point and the C3D Remix will be available for sale this week in time for the holiday! Thank you all for your patience and I'm looking forward to getting them into as many hands as possible to see what you all can do with them. 

Here's one sneak peak:![images/20965a2e19417a9fdaf5aca1604089eb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/20965a2e19417a9fdaf5aca1604089eb.jpeg)


---
**Luis Pacheco** *November 20, 2016 06:19*

Nice, appreciate the update **+Ray Kholodovsky**​. Where would they be available for purchase? [cohesion3d.com - Cohesion3D](http://Cohesion3d.com)?


---
**Ray Kholodovsky (Cohesion3D)** *November 20, 2016 06:21*

**+Luis Pacheco** you've got the right url, I'm working on the new site and webstore for it right now. 


---
**Luis Pacheco** *November 20, 2016 06:39*

I'll definitely keep an eye out so I can get my order in one it's up. Thanks!


---
**Ray Kholodovsky (Cohesion3D)** *November 20, 2016 06:46*

Your support is much appreciated **+Luis Pacheco**!


---
**Ray Kholodovsky (Cohesion3D)** *November 24, 2016 01:53*

**+Luis Pacheco** the ReMix is available for order now at [cohesion3d.com](http://cohesion3d.com).   The Mini is available for pre-order now too, Eric has been testing for me on his delta and I will defer to him for some feedback yet. 

Thanks for your support!

[cohesion3d.com - Cohesion3D: Powerful Motion Control](http://cohesion3d.com)


---
**Luis Pacheco** *November 24, 2016 02:07*

**+Ray Kholodovsky**​ Ordered, I've been stalking the website all week haha checkout with PayPal was acting a bit weird but finally went through. Appreciate the heads up!


---
**Ray Kholodovsky (Cohesion3D)** *November 24, 2016 02:09*

**+Luis Pacheco** Glad to hear it, and I see the order on my end.  Thanks!


---
*Imported from [Google+](https://plus.google.com/110276424073473119972/posts/e7eBGjdMTjT) &mdash; content and formatting may not be reliable*
