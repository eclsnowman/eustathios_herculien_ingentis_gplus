---
layout: post
title: "My Eustathios Spider V2 keeps pausing multiple times mid print"
date: March 06, 2018 21:53
category: "Discussion"
author: Brandon Cramer
---
My Eustathios Spider V2 keeps pausing multiple times mid print. I swapped the USB cable. My laptop is a Core i7 running Windows 10. I'm using the Azteeg X5 Mini WIFI. 





**Brandon Cramer**

---
---
**Jeff DeMaagd** *March 06, 2018 22:04*

What is your slicer and how high is the model detail? Can you post a video of the behavior?


---
**Brandon Cramer** *March 06, 2018 22:08*

I'm using Simplify3D. .2 at the moment. I have tried printing different stl's and get the same result. I don't have a video currently but can try to get one next time it happens. 


---
**Daniel F** *March 06, 2018 22:09*

I don't know the Azteeg X5 in particular but I had similar problems when printing from my PC over USB. Can you print from an SD card? Or upload the file to the card through wifi and then start printing the file from the card? I wouldn't print from a Windows PC, they do all sort of weird things like rebooting due to SW upgrade. Power saving could be a source of issues as well.


---
**Oliver Seiler** *March 06, 2018 22:21*

I'd avoid streaming over USB, and I always had issues getting USB to work reliably. Why don't you print form SD card?


---
**Brandon Cramer** *March 06, 2018 22:26*

I usually don't have any trouble. Where would I put the SD card, in the Viki Display unit?


---
**Jeff DeMaagd** *March 06, 2018 22:38*

Smoothieware and S3D have historically had some compatibility issues. Smoothieware can’t handle a lot of extremely short lines and it causes stuttering and S3D does zero path cleanup for infeasibly short lines. I personally have not seen these issues but my smoothie machine didn’t do small objects. You could try dialing down the speed and see if that clears it up.



But they do recommend running g code from SD card because it’s faster and has less interference issues. You can copy over the file through the virtual USB drive.


---
**Oliver Seiler** *March 06, 2018 22:38*

You already have an SD card in the main board. When you connect via USB you should be able to simply upload a file to this card.




---
**Brandon Cramer** *March 06, 2018 22:47*

I see! I'm trying this now. Just waiting for the bed and extruder to heat up. Thanks for the help!!


---
**Bruce Lunde** *March 06, 2018 23:15*

I had a problem with my CNC doing weird things, I ended up having to turn off the WiFi to my laptop and restarting it due to all the windows patched that seems to be coming in waaaaay too often...I also had some early problems on my Windows desktop / Pronterface/USB driving the HercuLien but now that I have the octopi and octoprint, no more problems!


---
**Eric Lien** *March 07, 2018 03:13*

**+Brandon Cramer** if you have the x5 mini wifi, you can upload the gcode to the sd card over wifi, and start the print over the web interface. 


---
**Eric Lien** *March 07, 2018 03:14*

[http://panucattdevices.freshdesk.com/support/solutions/articles/1000258070-setting-up-wireless-connection-on-x5mini-wifi](http://panucattdevices.freshdesk.com/support/solutions/articles/1000258070-setting-up-wireless-connection-on-x5mini-wifi)


---
**Brandon Cramer** *March 07, 2018 04:05*

**+Eric Lien** I copied the gcode over to the SD card and started printing. The print didn’t pause this time around. The X5 mini works great!!


---
**Scott Hess** *March 07, 2018 05:14*

I'm just a lurker, here, but this kind of thing is the reason I popped a Raspberry Pi Zero running Octoprint on my printer.  It's cheap enough to consider a consumable, and it can devote full attention to the printer until the print is done, while letting you check status from a web page.  Being able to upload gcode and start things running over wifi from whatever random device I have at hand is really nice.


---
**Oliver Seiler** *March 07, 2018 05:46*

That's exactly why I love my DuetWifi, it gives me all of that without having to add another component (octoprint) that could fail to the mix. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/1rVzYi7Rz2Y) &mdash; content and formatting may not be reliable*
