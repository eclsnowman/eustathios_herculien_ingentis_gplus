---
layout: post
title: "Hey guys, a buddy of mine let me know about this"
date: February 08, 2015 23:09
category: "Discussion"
author: Isaac Arciaga
---
Hey guys, a buddy of mine let me know about this. If you want a Smoothie Board check out this group buy that Arthur is doing with the Delta group. Less than 48 hours to go. I'm in for a 5x :)

[https://groups.google.com/d/msg/deltabot/WNTOnAk4jZM/Z5fluseXS24J](https://groups.google.com/d/msg/deltabot/WNTOnAk4jZM/Z5fluseXS24J)





**Isaac Arciaga**

---
---
**Isaac Arciaga** *February 08, 2015 23:11*

Sorry if this is a repost :)


---
**Dat Chu** *February 08, 2015 23:30*

I made a post there earlier. Thanks for posting the link again since I recheck the OP's spreadsheet and he got me down for the wrong type. I want a 5XC since the Herculien is going to be dual extruder.


---
**Isaac Arciaga** *February 08, 2015 23:40*

**+Dat Chu** I think at this point it won't matter. As long as there's enough people buying 1 of their boards he will be giving us a discount code to use on RobotSeed and UberClock for the total purchase.


---
**Dat Chu** *February 08, 2015 23:43*

I see. I tend to do as much due diligence as possible so others can go on about their day. Looks like I just need to hold tight a bit longer. 


---
**Bruce Lunde** *February 09, 2015 01:01*

Exactly what I need for the Herculein...added my request to the chain.


---
**Derek Schuetz** *February 09, 2015 01:26*

It won't let me reply to the thread on my phone


---
**Mike Thornbury** *February 09, 2015 02:44*

I wonder what the untaxed prices would be? I'm assuming they are the taxed prices, because that is only a small discount on what you pay, untaxed, on the website. Anyone outside of a tax zone gotten a price?


---
**Isaac Arciaga** *February 09, 2015 03:06*

**+Mike Thornbury** Uberclocks is handling the North American sales. They're in Oregon.


---
**Mike Thornbury** *February 09, 2015 03:25*

I'm not. Nor am I subject to any taxation.



Which is why I asked what the untaxed price is - there are only two regions for Smoothieboards - USA and non-USA.


---
**Dat Chu** *February 09, 2015 04:10*

I couldn't find an untaxed price. But the 5xc is 125 with discount from normally 170.


---
**Mike Thornbury** *February 09, 2015 10:19*

That's a serious discount! Thanks.


---
**Ryan Jennings** *February 09, 2015 13:58*

I added my name to the list for my new build!


---
**Isaac Arciaga** *February 09, 2015 16:56*

**+Ryan Jennings** thanks for sharing the group buy with me :)


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/RMDKZSdmPC7) &mdash; content and formatting may not be reliable*
