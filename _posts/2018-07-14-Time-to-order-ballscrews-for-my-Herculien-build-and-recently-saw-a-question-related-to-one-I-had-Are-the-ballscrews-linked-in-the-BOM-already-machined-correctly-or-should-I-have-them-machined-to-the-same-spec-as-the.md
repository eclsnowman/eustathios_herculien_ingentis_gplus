---
layout: post
title: "Time to order ballscrews for my Herculien build and recently saw a question related to one I had: Are the ballscrews linked in the BOM already machined correctly or should I have them machined to the same spec as the"
date: July 14, 2018 06:29
category: "Discussion"
author: William Rilk
---
Time to order ballscrews for my Herculien build and recently saw a question related to one I had:

Are the ballscrews linked in the BOM already machined correctly or should I have them machined to the same spec as the leadscrews from misumi (part # Mtsbrw12-480-f10-v8-s26-q8) ? That is: 480mm overall length and 8mm wide shoulders at both ends, 10mm and 26mm respectively?

Maybe I'm not looking hard enough.



Also- should questions like this be posted under "Build Logs"?



Thanks guys.





**William Rilk**

---
---
**Eric Lien** *July 14, 2018 13:08*

Whichever location is fine to post the questions. The linked ballscrew should be machined. But it might be a good idea to confirm it with them over email just to be safe.


---
**William Rilk** *July 14, 2018 14:58*

**+Eric Lien** thank you very much!


---
**William Rilk** *July 14, 2018 16:11*

Aaaaand now I feel dumb. Couldn't tell from my phone screen that there are dimensions on their drawing.  The exact dimensions I was asking about...😐


---
**Jim Stone** *November 04, 2018 01:43*

if its the ball screws from the ali express page. with a shitty paint drawing for the nut orientation. yeah thats correct LOL thats my shitty drawing.


---
**Jim Stone** *November 04, 2018 01:44*

that is IF its for the HERCULIEN




---
*Imported from [Google+](https://plus.google.com/100191047182984055447/posts/eqqRwKQYva6) &mdash; content and formatting may not be reliable*
