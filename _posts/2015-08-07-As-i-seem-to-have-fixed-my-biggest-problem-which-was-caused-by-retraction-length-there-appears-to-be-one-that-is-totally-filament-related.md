---
layout: post
title: "As i seem to have fixed my biggest problem which was caused by retraction length there appears to be one that is totally filament related"
date: August 07, 2015 19:08
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
As i seem to have fixed my biggest problem which was caused by retraction length there appears to be one that is totally filament related. The Filament is eSun PETG (transparent blue). That stuff basically prints quite well as long as the layer or "island" is big enough. I'm only using 2,5mm retraction in this print and you can easily spot the problem. It appears in the small posts of 3DBenchys house and on the chimney. Not only that extrusion is totally unreliable it also seems to overextruder after a short while. I'm not sure where this may come from and havent seen this before. I've double checked the filament transport in the Feeder and that doesn't show any problems.



The Filament generally seems to be more on the problematic side. It appears totally creamy when it comes out (no matter if at 245°C or 260°C - tried everything in between in 5°C steps) and tends to grease up. But that extrusion problem definitely makes it unusable for smaller parts.



Does anyone yet have seen such a thing?



![images/1c930ae4aa449186c46e47b00057fa16.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1c930ae4aa449186c46e47b00057fa16.jpeg)
![images/6ceb03c4eb346013a94259b363820e91.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6ceb03c4eb346013a94259b363820e91.jpeg)

**Frank “Helmi” Helmschrott**

---
---
**Maxim Melcher** *August 07, 2015 19:23*

With Winbo PETG: [https://plus.google.com/112467558426863470913/posts/2MnjeLSYiqr](https://plus.google.com/112467558426863470913/posts/2MnjeLSYiqr) and my hotend with PTFE inlay going immediately to heating barrel ([https://plus.google.com/112467558426863470913/posts/1KG7V5GCRPQ](https://plus.google.com/112467558426863470913/posts/1KG7V5GCRPQ)). Bowden extruder, retract 7,5mm. Zero issues. 


---
**Isaac Arciaga** *August 07, 2015 19:47*

It's not the filament that's problematic or unreliable. It's fine for smaller parts if you use some cooling and also slow down to provide enough time for each layer to cool enough before the next layer. PET based filament does ooze a lot more than ABS/PLA so it is a bit trickier to tune to beat the ooze during non-print moves. Even more so with small island stops with a bowden. You can tune the bowden and ext temps to print the pillars on the boat correctly, but imo printing this model would probably give you a better time to tune for it [http://share.makerstoolworks.com/physibles/66](http://share.makerstoolworks.com/physibles/66)



I was able to print a small dual ext Aria Dragon with clear and black ESun PETG with no cooling. Its not perfect, but I think it passes for decent considering it was my first dual print ever.

[http://i.imgur.com/UfbLUbD.jpg](http://i.imgur.com/UfbLUbD.jpg)

[http://i.imgur.com/szky5Oc.jpg](http://i.imgur.com/szky5Oc.jpg)


---
**Eric Lien** *August 07, 2015 20:01*

Here was the PETG benchy I did a while back on my Eustathios: [http://imgur.com/wt1RBwj](http://imgur.com/wt1RBwj)



I will try to find my factory file for this so you can try.


---
**Isaac Arciaga** *August 07, 2015 20:09*

**+Eric Lien** nice!


---
**Eric Lien** *August 07, 2015 21:53*

**+Frank Helmschrott**​ what do you have your minimum layer time set at under the cooling tab in S3D?


---
**Frank “Helmi” Helmschrott** *August 08, 2015 05:24*

Just to be sure guys: I know about the minimum layer time problematic and layers beeing to soft if the time is too low. The thing is this isn't what caused these pronblems primarily. As you might see on the chimney the thing is that extrusion is really way irregular when retract comes into the game. Whenever there are these small printing islands the amount material coming out of the nozzle after retract is extremely low. Every 8 or 10 layers all that material that was missing before seems to pop out at once.



I should have probably done a video instead of a photo of the result. You would see that the problem isn't the minmum time or generally not enough cooling. I will do a test with my better working PETG (HDGlass) and see how it comes out with the same values.



**+Eric Lien** your benchy looks awesome, would love to see the factory file for that.


---
**Isaac Arciaga** *August 08, 2015 10:29*

**+Frank Helmschrott** you're asking for help. Some of us who have HAVE experience with PETG and Bowden systems have taken the time out of our busy schedules to ask you questions in order to help you troubleshoot. When you respond saying you know what's (not) causing your issue, then please, figure it out yourself.


---
**Frank “Helmi” Helmschrott** *August 08, 2015 10:31*

**+Isaac Arciaga** sorry if have been misunderstood - no offense at all. I just wanted to say that i'm aware of these possible problems as i know them already. Sorry if expressed this the wrong way - i'm not that safe when it comes to the english language.


---
**Mutley3D** *August 08, 2015 15:15*

**+Frank Helmschrott** sometimes if the retract acceleration is too high, or the E axis jerk is too high, retract operations might not complete reliably. Sometimes this can mean it will retract, but then not unretract fully (as might be possibly in your case) leaving gaps, or vice versa leaving blobs. Try turning down retract acceleration and E jerk to safe levels (maybe post the current values?). FWIW my E jerk is always below 1mms and retract accels (way) below 1000mms^2


---
**Frank “Helmi” Helmschrott** *August 08, 2015 21:12*

**+Mutley3D** thanks – that is definitely a valid approach. I haven't thought about that but definitely could be the case as i see some small signs of the same issue even with other material now (especiall with the opague PLA where it is clearly visible on what i printed today). My accel for the extruder was at 1000. Reptier FIrmware doesn't have an E jerk value, at least not in the Eeprom settings but i will check the config later. It has an Extruder start speed value which was at 25mm/s before, i lowered that to 10 for testing and lowered the accel for 500 for now. I'm repeating my last print and see how it goes.


---
**Maxim Melcher** *August 08, 2015 21:52*

PETG objects with small parts need a good object cooling.... Have You a fan with duct or one without?


---
**Mutley3D** *August 08, 2015 22:43*

**+Frank Helmschrott** great, glad it helped. Extruder Start Speed sounds the equivalent of e jerk


---
**Frank “Helmi” Helmschrott** *August 09, 2015 15:02*

**+Mutley3D** it helped a bit but unfortunately only with the smaller problems with PLA. I tried the PETG benchy again today and unfortunately had the same thing happening again. Overall quality was great until the smaller islands.



**+Eric Lien** i have minimum time per layer set to 20 seconds and it prints those parts of the benchy real slow already. The Problem seems to clearly be with the extrusion - i have to fix that but just for my curiosity what value do you use there?


---
**Eric Lien** *August 09, 2015 16:34*

I use 15 but allow slowing down to 20% if needed. What percentage slow down do you use?


---
**Frank “Helmi” Helmschrott** *August 09, 2015 17:22*

**+Eric Lien** tested with 10% and 20% which both worked very well with PLA but the PETG problem persists nearly without any noticable change.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/3NLkUXuTaHP) &mdash; content and formatting may not be reliable*
