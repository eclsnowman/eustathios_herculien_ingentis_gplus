---
layout: post
title: "Went on a little shopping spree yesterday"
date: May 07, 2014 07:08
category: "Show and Tell"
author: Brian Bland
---
Went on a little shopping spree yesterday.  Can you guess what I went with for hot ends by the Azteeg X3 Pro I ordered.  Probably going to go ahead and get Simplify3d also.

![images/166b47ad4108489da341f245189fa0c0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/166b47ad4108489da341f245189fa0c0.jpeg)



**Brian Bland**

---
---
**Tim Rastall** *May 07, 2014 07:09*

KRAKEN! 


---
**D Rob** *May 07, 2014 17:33*

I'm seriously drooling right now


---
**Mike Kelly (Mike Make)** *May 07, 2014 21:43*

I want one of those so bad 


---
**Eric Lien** *May 08, 2014 01:59*

I just noticed the three 20a fuses. Wow... Its a beast.


---
**Brian Bland** *May 08, 2014 03:09*

Pretty bad that the Kraken will get here from E3d before the X3 will get here.


---
**ThantiK** *May 08, 2014 20:20*

Hah! I have one of those! 


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/JK8Uxh1Hg8d) &mdash; content and formatting may not be reliable*
