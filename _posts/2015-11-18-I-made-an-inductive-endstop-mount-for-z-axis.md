---
layout: post
title: "I made an inductive endstop mount for z-axis"
date: November 18, 2015 22:37
category: "Show and Tell"
author: Florian Schütte
---
I made an inductive endstop mount for z-axis. May also be used for auto bed leveling. It is mounted with existing screws . One on cooling duct mount and one on hotend fan mount. I tried a few versions, but this place for mountig is the one where i lost minimal build volume. Only 3-4mm on y axis. When **+Eric Lien** has set up his github for modified parts, i will upload there.



![images/a572cce14e06ecd7503b17442d13c68c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a572cce14e06ecd7503b17442d13c68c.jpeg)
![images/c4e5cc513fc41d4a18f0f0741cafe6fd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c4e5cc513fc41d4a18f0f0741cafe6fd.jpeg)
![images/0fb504ff72bfee413449695e4af17e26.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0fb504ff72bfee413449695e4af17e26.jpeg)

**Florian Schütte**

---
---
**Eric Lien** *November 18, 2015 22:53*

Great job, I hope to have time tonight to set up the folder structure so people can begin uploading.


---
**Jean-Francois Couture** *November 19, 2015 17:41*

**+Florian Schütte** How are the results when the bed is hot ? I was told that those sensors goes bunkers when they try to sense a heated area ? 


---
**Eric Lien** *November 19, 2015 18:10*

**+Jean-Francois Couture** no issues here on my Delta printer


---
**Jean-Francois Couture** *November 19, 2015 18:12*

**+Eric Lien** Thats great news. It should be a standard feature on all printers :)



**+Florian Schütte** Could you tell us where you got the part ?


---
**Florian Schütte** *November 19, 2015 19:42*

To be honest, I didn't really tried to use it until now. I have to remove my glass plate to detect the bed (detection distance ~2-3mm, but 4mm glass). The problem is, that i can't bring the bed up anymore because i use old bed supports with new carriage. But new parts are on their way.

**+Jean-Francois Couture** It's a standard sensor called LJ12A3-4-Z/BY. You can find these for a few bucks on ebay or amazon. They have 12mm OD and a detection distance of 4mm. But it does not react that much on aluminium, so you end up at ~2-3mm. There is also an other sensor with 8mm detection distace, but it has 18mm OD (LJ18A3-8.....). So you will lose some millimeters more in build volume.




---
**Ted Huntington** *November 19, 2015 22:44*

I found that the ebay sensors will only detect 1mm away from my aluminum sheet- so it won't work even with 1/16" glass. Some people add a thin galvanized steel sheet on top of the aluminum which has more inductance - I haven't tried that yet.


---
**Florian Schütte** *November 19, 2015 22:53*

As i say. My Sensor works at 2-3mm. Maybe i will try the 8mm version when i habe time for that.


---
**Ted Huntington** *November 19, 2015 23:45*

maybe there are differences between ebay sensors. This is the one I got: [http://www.ebay.com/itm/221457637744?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/221457637744?_trksid=p2057872.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT) - the only difference in the number is /BX (LJ12A3-4-Z/BX ) which is an NPN. Maybe the PNP version is able to work for 2-3mm while the NPN only 1mm.


---
**Florian Schütte** *November 20, 2015 00:09*

I got this one: [http://www.ebay.de/itm/New-LJ12A3-4-Z-BY-Inductive-Proximity-Sensor-Detection-Switch-PNP-DC6-36V-/351323770970?hash=item51cc87505a:g:MX4AAOSwfcVUBFG-](http://www.ebay.de/itm/New-LJ12A3-4-Z-BY-Inductive-Proximity-Sensor-Detection-Switch-PNP-DC6-36V-/351323770970?hash=item51cc87505a:g:MX4AAOSwfcVUBFG-)

Manufacturer is called "SHNRQ". Maybe tere are differences between manufactueres. As far as i understand electronics, the difference between NPN and PNP is that PNP is normally open (NO) and NPN is normally closed (NC).




---
**Ted Huntington** *November 20, 2015 03:58*

Maybe voltage makes a difference- I am only using 12v?


---
**Chris Brent** *November 20, 2015 16:52*

Glad I read this, I'm about to finally add the one that came with my delta but I've added a glass plate. You guys might have saved me some trouble shooting already :)


---
**Eric Lien** *November 20, 2015 17:50*

**+Chris Brent** on my Delta I am using an aluminum heated bed with mirror glass on top no problem ([http://www.amazon.com/Signswise-Aluminum-Heatbed-Heated-Printer/dp/B00VG7HDEC](http://www.amazon.com/Signswise-Aluminum-Heatbed-Heated-Printer/dp/B00VG7HDEC)  [http://www.michaels.com/artminds-round-mirror/M10025203.html](http://www.michaels.com/artminds-round-mirror/M10025203.html)). I also coat the mirror glass with 3D-EEZ by **+argas231** . To me this setup with inductive bed level probing is a winning combination.


---
**argas231** *November 21, 2015 00:32*

Thank you....   and our newest formulation is even better     3D EeZ   Rules


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/XkiRVRmgBjo) &mdash; content and formatting may not be reliable*
