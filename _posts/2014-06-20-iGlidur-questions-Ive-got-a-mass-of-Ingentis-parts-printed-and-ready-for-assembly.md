---
layout: post
title: "iGlidur questions: I've got a mass of Ingentis parts printed and ready for assembly"
date: June 20, 2014 12:09
category: "Discussion"
author: Mike Miller
---
iGlidur questions: I've got a mass of Ingentis parts printed and ready for assembly. At the time I was leaning towards a bushing solution, but **+Shauki Bagdadi**'s experience with iglidur parts interests me...problem being I've got parts with a 15.4mm opening, and the nearest iglidur part is 14mm in diameter...thoughts on how to properly solve that difference in diameter? (It's near enough to 5/8" that I think that was the desired dimension in the plans....which doesn't seem to make sense with 8mm and 12mm shafts...)





**Mike Miller**

---
---
**Mike Miller** *June 20, 2014 13:02*

/slapsForehead #YouveGotAPrinter-Useit!


---
**D Rob** *June 21, 2014 06:16*

**+Mike Miller** sometimes I too forget. It's like Where can i find this specialized part... 3 days later... if only I had a way to make... damn isn't that why I built a damn printer in the first place!!! Happens to the worst of us lol!


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/ausvPZ1urkL) &mdash; content and formatting may not be reliable*
