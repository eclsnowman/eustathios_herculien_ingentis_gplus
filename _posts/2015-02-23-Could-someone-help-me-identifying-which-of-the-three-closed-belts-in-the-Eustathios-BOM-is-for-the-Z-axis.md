---
layout: post
title: "Could someone help me identifying which of the three closed belts in the Eustathios BOM is for the Z axis?"
date: February 23, 2015 19:34
category: "Discussion"
author: Oliver Seiler
---
Could someone help me identifying which of the three closed belts in the Eustathios BOM is for the Z axis? Thanks.





**Oliver Seiler**

---
---
**Tim Rastall** *February 23, 2015 22:55*

I can help you with some maths and a process of elimination: I assume you can measure the distance between the center of the x/y shafts and the corresponding motor shaft. Once you have this value for both X and Y axis, multiply it by 2 and add 64mm (pitch circumference for 32tooth GT@ pulley). That should give you the x and y belt lengths. The lengths on the BOM are 502mm, 497mm and 475mm.


---
**Erik Scott** *February 24, 2015 02:20*

It's the 502mm one (the longest one)


---
**Oliver Seiler** *February 24, 2015 04:42*

Thanks **+Erik Scott** 


---
**Oliver Seiler** *February 24, 2015 04:43*

**+Tim Rastall**  I haven't figured out yet how to measure the distance - otherwise I would have tried ;)


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/Sc5b8aKown2) &mdash; content and formatting may not be reliable*
