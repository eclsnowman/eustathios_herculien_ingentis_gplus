---
layout: post
title: "I dont understand. On the github for spider v2 it says \"Build Volume: 285x285x295mm\" but the bed it uses is much larger?"
date: May 23, 2016 08:09
category: "Discussion"
author: Dovid Teitelbaum
---
I dont understand. On the github for spider v2 it says "Build Volume: 285x285x295mm" but the bed it uses is much larger?





**Dovid Teitelbaum**

---
---
**Daniel F** *May 23, 2016 08:30*

the bed is just larger than the printable area. I guess this design allowed to fix the bed in an easier way to the Z threaded rods.

You can also us a squre build plate, see here [https://plus.google.com/communities/108524206628971601859](https://plus.google.com/communities/108524206628971601859)

build plate is 310x310x6mm cast & milled aluminium, mirror tile on top is 300x300mm, printable area is 290x290mm


---
**Dovid Teitelbaum** *May 23, 2016 08:37*

the eustathios says its 300mmx300mmx270mm build volume. does that meean its larger than the spider in regard to l and w?


---
**Daniel F** *May 23, 2016 08:55*

I think it depends on the XY carriage, it's possible that the spider design needs a little bit more room than the orignal design, I think the lenght of the extrusions are the same. **+Eric Lien** knows all the details about the v2 spider.


---
**Eric Lien** *May 24, 2016 01:41*

**+Dovid Teitelbaum** the original Eustathios couldn't reach 300x300 in my testing. I could get 290x290 perhaps. But yes depending on which carriage is used print volume goes up or down.


---
**Dovid Teitelbaum** *May 24, 2016 02:23*

**+Eric Lien** Thanks for the info. Can I ask you a question. I have this pegusus kit already and I really want to convert it to a ultimaker gantry. Pretty much all the extrsions I have are 2040 500mm length. Is there any design that will work where I would be able to use my current frames. If not maybe I can cut them down. Would like to be able to have my original 12" build volume if possible.  [http://www.makerfarm.com/index.php/12-pegasus-kit.html](http://www.makerfarm.com/index.php/12-pegasus-kit.html)


---
**Eric Lien** *May 24, 2016 03:31*

Perhaps combining the HercuLien and Eustathios design could get you what you want. But would require you do some part redesign. At the end of the day the extrusion is actually one of the cheapest parts. For example you can get the entire extrusion kit from **+Makeralot**​ for only 39$



[http://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207/](http://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207/)


---
**Dovid Teitelbaum** *May 24, 2016 05:13*

**+Eric Lien** wow that is cheaper than i thought. but shipping is another $50


---
**Sean B** *May 24, 2016 12:25*

I am going to be purchasing my extrusion fromm them (Makeralot), even with the shipping I can't get it any lower with local alternatives.


---
*Imported from [Google+](https://plus.google.com/+DovidTeitelbaum/posts/VWG1sUqUW5v) &mdash; content and formatting may not be reliable*
