---
layout: post
title: "Working on getting the heated bed element for my z plate of the Herculien"
date: February 15, 2015 15:44
category: "Discussion"
author: Dat Chu
---
Working on getting the heated bed element for my z plate of the Herculien. Does anyone have an email with the exact values to email the chinese supplier? The pig tail alteration would be useful as well.





**Dat Chu**

---
---
**Derek Schuetz** *February 15, 2015 16:38*

380x380

120v

800w 

Thermistor in center leads coming from back side off center 30mm

I also sent them a pic from Eric's build pictures. They are on holiday right now until the 26th


---
**Dat Chu** *February 15, 2015 16:43*

Will they also make the thermistor wire into a hook like **+Eric Lien**​ suggested. 


---
**Eric Lien** *February 15, 2015 16:54*

Send them this pic: [https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Silicone%20Heater%20Thermistor%20and%20Cable%20Locations%20for%20Alirubber.jpg](https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Silicone%20Heater%20Thermistor%20and%20Cable%20Locations%20for%20Alirubber.jpg)


---
**Eric Lien** *February 15, 2015 16:56*

Also I might suggest going with 700w. I have 800w but had to turn down max bed output in marlin via pwm because the bed would overshoot trying to pid tune.


---
**Marc McDonald** *February 15, 2015 20:00*

**+Eric Lien** **+Dat Chu** I recently performed several upgrades to my Herculien and one of them was to replace the 700W with a 1000W heated bed. The smoothieboard has no problems in my setup controlling the 1000W heater.


---
**Dat Chu** *February 15, 2015 20:08*

Very good to know **+Marc McDonald**​. I am planning on using a smoothieboard as well. I will tell the supplier to give me a 1000w version then. 


---
**Marc McDonald** *February 15, 2015 20:36*

**+Dat Chu** The 700W took 30+ minutes to heat the bed to 90deg C and following **+Thomas Sanladerer** guide it was low power for the bed size in the Herculien. **+Eric Lien** recommendation works and may be the best option, I will wait and see as I can always switch back if required.


---
**Dat Chu** *February 15, 2015 20:44*

Let us know what you find out with the 1000w version. 30 minutes is a long time. 


---
**Eric Lien** *February 16, 2015 00:15*

**+Marc McDonald** I have heated my bed to 110C with 400W in way less time. Something seems wrong with the bed if 700W takes that long. If you used my marlin from github you likely have max pwm output turned down in combo with a 700w heater.


---
**Marc McDonald** *February 16, 2015 01:37*

**+Eric Lien** The replacement heat bed reaches 110C in less than 10min at this time and is very stable over several hours of operation. I have a smoothieboard installed and have limited the SSR control frequency as per the smoothie instructions so that the SSR correctly switches, the PWM range is not limited.


---
**Eric Lien** *February 16, 2015 02:26*

**+Marc McDonald** odd. Sorry you had this issue, I can't understand why I have to turn down my bed at 800W to not overshoot, but with the same setup you need 1000W. 


---
**Marc McDonald** *February 16, 2015 04:08*

**+Eric Lien** no issues, I was planning on making some updates during the upgrade process and the heat bed was one of the items.


---
**Eric Lien** *February 16, 2015 04:19*

I am always open to improvements, any you make and want to share would be great. I was actually thinking of revamping the cover a bit. As you can tell from my github pics I made mine from 1x1 angle aluminum and panels glued in. I think making a door on the front with hinges would make more sense for 95% of the time you need to access inside you don't need the full hood opened. What do you think?


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/D8qWWRT7CUw) &mdash; content and formatting may not be reliable*
