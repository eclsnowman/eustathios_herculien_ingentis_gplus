---
layout: post
title: "Another \"set of hardware\" in hopes of one day becoming a real printer"
date: December 01, 2016 04:47
category: "Build Logs"
author: Pete LaDuke
---
Another "set of hardware" in hopes of one day becoming a real printer.  Eustathios on the way.

![images/0a65dc55a115a60bcbafc67bb44237ba.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0a65dc55a115a60bcbafc67bb44237ba.jpeg)



**Pete LaDuke**

---
---
**Eric Lien** *December 01, 2016 05:01*

That's an exciting pile of potential there. Look forward to watching you make it a reality :)


---
*Imported from [Google+](https://plus.google.com/100658478011121421875/posts/8tbxYmessaJ) &mdash; content and formatting may not be reliable*
