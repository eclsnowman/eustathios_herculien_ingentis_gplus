---
layout: post
title: "Had to pull my AliRubber silicon heater off my bed and about 1/3 of the sticky stuff came off"
date: May 14, 2015 06:18
category: "Discussion"
author: Oliver Seiler
---
Had to pull my AliRubber silicon heater off my bed and about 1/3 of the sticky stuff came off. It seems that it would probably stick ok-ish if I put it back on, but I'm keen to hear other people's thoughts first.

Anyone knows what they use (some 3M stuff from memory) and if that can be sourced somewhere separately? Thanks.





**Oliver Seiler**

---
---
**Derek Schuetz** *May 14, 2015 06:31*

Get the reb high temp gasket maker for engines. Can find it any auto parts store 


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/Z5N4JhcbtRd) &mdash; content and formatting may not be reliable*
