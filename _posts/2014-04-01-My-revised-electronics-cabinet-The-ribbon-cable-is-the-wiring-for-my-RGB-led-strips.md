---
layout: post
title: "My revised electronics cabinet. The ribbon cable is the wiring for my RGB led strips"
date: April 01, 2014 06:06
category: "Show and Tell"
author: D Rob
---
My revised electronics cabinet. The ribbon cable is the wiring for my RGB led strips



![images/fc4a1884940349e27fe8038de4df9439.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fc4a1884940349e27fe8038de4df9439.jpeg)
![images/bce3199bd0fe77c3d5d0ac01c5745484.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bce3199bd0fe77c3d5d0ac01c5745484.jpeg)
![images/30034e7daa6444f875156dd39097d5fe.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/30034e7daa6444f875156dd39097d5fe.jpeg)
![images/80159ce18f8d5cf6201615f27acf09ca.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/80159ce18f8d5cf6201615f27acf09ca.jpeg)
![images/65dd9c59c2cbc46dd6bf2ee6f7e1622d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/65dd9c59c2cbc46dd6bf2ee6f7e1622d.jpeg)
![images/b47fa95829d28c4426c3edf9f2bc7f34.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b47fa95829d28c4426c3edf9f2bc7f34.jpeg)
![images/515145a4d6b889c40d8433215291b967.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/515145a4d6b889c40d8433215291b967.jpeg)

**D Rob**

---
---
**Dale Dunn** *April 01, 2014 12:32*

Tidy. It looks like you're pretty confident with that printed worm gear. Has it been working well for you?


---
**Dale Dunn** *April 01, 2014 13:06*

Is that the board +Thomas Sanladerer just reviewed?


---
**Mike Miller** *April 01, 2014 16:19*

Will you be adding any under-platform cooling?


---
**D Rob** *April 01, 2014 22:41*

**+Dale Dunn** no its a geeetech um 1.5.7 board he reviewed the melzi. I think that is what **+nophead** uses though I'm not sure if it is geeetech or not. I repaired the sainsmart 1.5.7 board and have a replacement on the way. for future builds. that huge switching regulator is variable. I will be using a gadgets 3d controller lcd. The lm7812 I removed is mounted to the side of the power supply (giant heatsink) and dropping 24v to 12v for the led controller (I ordered 24v and they sent 12v) ther refunded most of the money and I made it work.


---
**D Rob** *April 01, 2014 22:46*

**+Dale Dunn** Honestly it isnt printing... yet. I have been working 90hr weeks lately on 2 jobs. So I have only had a few spare hours here and there. I am honestly thinking ball screws/ lead screws or the horizontal ballscrew with a linear rod and vpulleys with spectra. guitar tuner on the ballnut carriage. I believe it will be isolated enough that the banding will not be an issue with proper layer heights because the spectra will be constrained by the vpulleys and under tension. I will be trying robot digg fishing line pulleys.


---
**D Rob** *April 01, 2014 22:49*

**+Mike Miller** yes I plan on some fans and will probably a forced hot air blower (hair dryer) that blows from the electronics cabinet to the build area. It will be positioned to blow on the bottom of the bed insilation. This will hopefully spread the heat more evenly. 


---
**Dale Dunn** *April 02, 2014 12:59*

A ball screw would easily be straight enough to not cause any banding issues, even with the most crude nut guidance. I wonder how fine of a pitch those are available with.


---
**D Rob** *April 02, 2014 17:35*

I don't know but with it being horizontal, using gears or pulleys you could change steps/mm easily by changing the ratio


---
**Brian Bland** *April 12, 2014 03:33*

**+D Rob** How tall is your electronics enclosure under your printer?  Thinking about ordering more extrusion to do a enclosure.


---
**D Rob** *April 12, 2014 04:52*

2" between 1" extrusion the panels go between the slots so minus 1 = 3" divider panel to base enclosure panel 


---
**Jason Barnett** *April 14, 2014 04:45*

**+D Rob** Now that you have had a chance to use this, electronics enclosure, design for awhile, how do you like it? I am going to be assembling mine soon and wanted to see what you like and hate about it, before I put mine together. It looks nice, but could be a pain to troubleshoot electrical issues with them mounted like that. At least that is how it looks to me. Is there anything you would change?


---
**D Rob** *April 14, 2014 05:27*

This actually is the change I made. Originally I had a access hatch from inside the machine. I have to tip the machine now to access everything but it is now more easily accessible. Also if there is need for full access or total removal of the base it is now easier. There are limitations but I believe the trade off is worth it. With the hatch it was near impossible to install remove mechanical parts like idlers, or the z drive axle. And difficult to adjust the spectra. Now its wide open. the bottom square frame removes with just 4 bolts. And nothing is attached to it to make removal more difficult


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/WqjoS5jeqN8) &mdash; content and formatting may not be reliable*
