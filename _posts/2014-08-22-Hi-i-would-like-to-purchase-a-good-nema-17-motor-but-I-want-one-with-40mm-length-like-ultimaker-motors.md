---
layout: post
title: "Hi, i would like to purchase a good nema 17 motor, but I want one with 40mm length like ultimaker motors"
date: August 22, 2014 02:58
category: "Discussion"
author: George Salgueiro
---
Hi, i would like to purchase a good nema 17 motor, but I want one with 40mm length like ultimaker motors.  Does anyone recommend a seller for me?  





**George Salgueiro**

---
---
**Miguel Sánchez** *August 22, 2014 08:50*

This seller and motors are a good choice [http://www.aliexpress.com/store/product/Best-sellers-5pcs-NEMA17-CNC-stepper-motor-58-Oz-in-40mm-stepping-motor-1-3A/704350_1129248380.html](http://www.aliexpress.com/store/product/Best-sellers-5pcs-NEMA17-CNC-stepper-motor-58-Oz-in-40mm-stepping-motor-1-3A/704350_1129248380.html)


---
**Eric Lien** *August 22, 2014 09:21*

I am liking this one [http://www.robotdigg.com/product/7/NEMA17-Stepper-Motor-40mm-Long,-1.2A](http://www.robotdigg.com/product/7/NEMA17-Stepper-Motor-40mm-Long,-1.2A)


---
**George Salgueiro** *August 22, 2014 11:25*

Thanks


---
**George Salgueiro** *September 04, 2014 02:40*

**+Eric Lien** Do you know the length of the shaft? 


---
*Imported from [Google+](https://plus.google.com/103579216912687360818/posts/h1bWUdfCJSi) &mdash; content and formatting may not be reliable*
