---
layout: post
title: "15 hour print job for the Gauntlet part of this Raptor Hand by e-NABLE scaled up to 170%"
date: August 19, 2015 20:50
category: "Show and Tell"
author: Brandon Cramer
---
15 hour print job for the Gauntlet part of this Raptor Hand by e-NABLE scaled up to 170%. My Eustathios is super quiet!! I love it. :)



I should probably print directly on the glass one of these days. 



Still waiting on the X5 mini with the proper screw down connectors. I'm hoping **+Roy Cortes**  will be sending my X5 mini soon. Once I get it, I can send this other one back. This is one of the last pieces to finish my Eustathios Spider V2 build. The other part is the heated bed and the cable chain for it. 



I received my 40 Watt extruder heater this morning. I will have to get it installed after this print job.  



![images/e5502c70c0e85c7d5d5dcf8ca1bc88b9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e5502c70c0e85c7d5d5dcf8ca1bc88b9.jpeg)
![images/528efa6cfe7ba7ef6f7d12d3c8ff9bbe.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/528efa6cfe7ba7ef6f7d12d3c8ff9bbe.gif)

**Brandon Cramer**

---
---
**Eric Lien** *August 19, 2015 22:44*

Glad to see you jump right into ambitious prints! Looks great. Can't wait to see your reaction once you dial in print settings above 100mm/s. It is really mesmerizing seeing it at high speed (150mm/s and up).


---
**Brandon Cramer** *August 19, 2015 23:58*

**+Eric Lien** That sounds exciting!!! Although I'm not exactly sure where to start. I do know I need to get this other heater installed though. 



I never did hear back from **+Roy Cortes** at Panucutt about the voltage for the fan on the X5 Mini. The fan for cooling the PLA isn't working with it being wired off the X5 Mini, so I have been running without it. 


---
**Eric Lien** *August 20, 2015 00:58*

The fan runs at whatever your power supply voltage is.


---
**Brandon Cramer** *August 20, 2015 15:47*

I installed the 40 watt heater! What a huge difference in how fast it heats up! 



I'm not getting any voltage out of the X5 mini for the fan. 



Here is what I see in the config file:



# Switch module for fan control

switch.fan.enable                                          true                #

switch.fan.input_on_command                  M106             #

switch.fan.input_off_command                 M107             #

switch.fan.output_pin                                     2.4              #



I allowed the extruder to heat up to temp and tried to turn the fan on and off but I don't see any voltage there. 


---
**Eric Lien** *August 20, 2015 19:59*

Are you using it for part cooling or the hot end heatsink?


---
**Brandon Cramer** *August 20, 2015 20:00*

I'm using it for Part cooling. The hot end heatsink fan I have running all the time. 


---
**Eric Lien** *August 20, 2015 20:44*

So if you send the following command:

M106 S255

You don't get any output?


---
**Brandon Cramer** *August 20, 2015 21:23*

I tried that and nothing. :(


---
**Eric Lien** *August 20, 2015 22:07*

Hopefully it will be a non-issue once you have the new board. You should probably mention the issue to Roy so he can diagnose the problem when he gets the board back.


---
**Brandon Cramer** *August 20, 2015 22:15*

He said he was going to send me a new board. That was probably a month ago now. I'm not sure at this point if he is. Roy hasn't responded to my emails. 


---
**Eric Lien** *August 21, 2015 00:03*

**+Brandon Cramer** he just finished moving into a new shop. So he has been really busy. But I just spoke with him today via email. Try him again, he is a great guy. 



One question, did you order the screw terminal version, or did you order the standard plug version by mistake. That would really determine if you should send yours back first on your dime, or if the mistake was on his side.


---
**Brandon Cramer** *August 21, 2015 00:32*

Roy verified that I did order the correct version. I will send him an email. I'm just glad I didn't send my other one back yet. I can't live without my printer.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/KhJojMn87Q7) &mdash; content and formatting may not be reliable*
