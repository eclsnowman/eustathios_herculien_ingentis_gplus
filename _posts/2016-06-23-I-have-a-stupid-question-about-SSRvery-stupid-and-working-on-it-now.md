---
layout: post
title: "I have a stupid question about SSR.....very stupid and working on it now..."
date: June 23, 2016 02:02
category: "Build Logs"
author: Botio Kuo
---
I have a stupid question about SSR.....very stupid and working on it now... thanks everyone's help







**Botio Kuo**

---
---
**Derek Schuetz** *June 23, 2016 02:20*

The SSR open when it receives input from your bed output on your board which send the 120v power to your bed 


---
**Jeff DeMaagd** *June 23, 2016 02:32*

SSR doesn't transfer power from the input to the output. A signal on the input is what turns on the output to allow current to pass.  The line voltage  is connected to one output connector, one power line from the bed connects to the other output connector. The other power line on the bed goes to neutral on the AC. The AC output doesn't have a polarity but the signal input does, be careful about that.



Make sure you understand that and safe practices before working with AC.


---
**Eric Lien** *June 23, 2016 02:54*

Look at the SSR wiring from the HercuLien wiring diagram. Not the same controller, but same principle: [https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Azteeg_x3_v2_wiring_(with_notes).pdf](https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Azteeg_x3_v2_wiring_(with_notes).pdf)


---
**Botio Kuo** *June 23, 2016 03:17*

**+Eric Lien** Hi Eric, What's the 120V N and L connect with SSR ? Is that meant N and L on power supply ? thx


---
**Maxime Favre** *June 23, 2016 05:55*

120v L and N is the main power coming from your house. It's 120 or 230 depending your country. If you feel not comfortable with that don't hesitate to ask for help, a friend or an electrician, search for guides. Be really careful it's dangerous if done wrong.﻿


---
**Eric Moy** *June 23, 2016 09:56*

When connecting wall power to your project, please be sure you're absolutely sure of what you're doing before you turn anything on. You can easily hit yourself or start a fire.



That being said, an SSR is pretty much a simple normally open single pole single throw switch (NO SPST), which is controlled by a digital signal from your controller board. The input side only controls whether the switch is open or close. When the switch is commanded close by the input signal, the it allows whatever line voltage attached between the output terminals to flow. If you intend to connect +24vdc to one of the outputs of your SSR, and the other output to your bed (the second terminal of the bed would go to your 24vdc return or minus symbol), when the SSR is commanded closed, 24 VDC will be applied across your bed.



Hope that helps


---
**Eric Lien** *June 23, 2016 10:21*

L is Live (or Line to some), N is Neutral, and G is Ground. Do a little reading on mains power if you are not familiar. And don't be scared of it. Your toaster, vacuum, clothing iron, etc all run on mains and you a are not afraid to touch them. The big trick is to be knowledgeable, only work on it while it is off and unplugged from the wall, and use good wiring practices for strain relief and insulation of any exposed wires.


---
**Sébastien Plante** *June 23, 2016 12:35*

**+Eric Lien** But the câble are isolated, chance of touching them are much smaller than on a relay.


---
**Eric Lien** *June 23, 2016 14:10*

**+Sébastien Plante**​ I was more referring to cable ends. People have a bad tendency to strip wires back further than needed leaving exposed copper at terminal connections, and also not twisting wire ends before inserting them to keep frayed wires from sticking out. Also a good SSR (one in the BOM) has a plastic cover that goes over terminals after wires are installed. Just use best practices for wiring and mains voltage is nothing to be so afraid of.﻿


---
**Eric Lien** *June 23, 2016 17:29*

**+Nathan Walkner** that is uncalled for. I am sorry but this is a place to learn and there will be zero tolerance for negative comments towards questions, ideas, and members who are learning new skills. 



To everyone: This is a place to share openly, ask questions openly, without fear of being of being mocked or told you cannot or shouldn't learn skills for which you are currently unfamiliar.



**+Nathan Walkner**​ please do not take this as a personal attack. But I will not allow such statements or attitudes to be a part of this community.


---
**Botio Kuo** *June 23, 2016 17:36*

**+Nathan Walkner** Thanks for your so supportive advice :)  and also everything needs to be learned and needs time for getting it. And I have up-plus 2 and micromaker 3D printers already. And I'm sure I will finish it, I just need more time to figure it out and make sure everything is safe( that's why I ask so many questions.... sorry for that ) and get confused when people said different way on youtube before I asked questions.  


---
**Botio Kuo** *June 23, 2016 18:18*

actually I already get the answer how to use SSR clearly... and thanks everyone .... :) and I'll try not to ask basic questions.... 


---
**Eric Lien** *June 23, 2016 18:24*

**+Nathan Walkner**​ I guess I will never assume someone is incapable of something. I was lucky, I had a father that was mechanical and was always fixing things and so I gained more knowledge than your average person when I was young. Then I studied in the physical sciences during college, and began my work career in a technical field. So I have had a lot of exposure to many of these things prior to 3D printing... but I understand that is not the norm. 



If someone burns down their house using my freely available opensource plans and this wonderful community I would be deeply saddened. But My opinion is the reason for the accident would be from others being afraid to ask the questions, and not because they had asked for help. So I don't think the fear of what might happen will stop me from attempting to help others with what knowledge I have gained. Just as I hope those who know more than me (and god there are many) will help me when I am new to a subject.



I guess it is all about how do you want to approach the world. Is the unknown scary, are others out to get you, should you only attempt what you know you are already capable of doing? Or is the thing that makes life fun the unknown. Is the chance that something might fail exciting or terrifying?﻿


---
**Eric Lien** *June 23, 2016 18:27*

**+Botio Kuo** Please do not stop asking questions. Even basic ones. Because for every person that asks... I can guarantee there were more who don't know, and want to know, but are too afraid to ask.


---
**Eric Lien** *June 23, 2016 22:26*

**+Nathan Walkner** I guess I don't understand.


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/fKC6hMEW3pp) &mdash; content and formatting may not be reliable*
