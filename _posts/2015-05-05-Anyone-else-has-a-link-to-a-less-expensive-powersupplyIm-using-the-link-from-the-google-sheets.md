---
layout: post
title: "Anyone else has a link to a less expensive powersupply(I'm using the link from the google sheets)?"
date: May 05, 2015 00:18
category: "Discussion"
author: Gus Montoya
---
Anyone else has a link to a less expensive powersupply(I'm using the link from the google sheets)? I already have one but it's a 12v unit. :(  

**+Eric Lien** I looked for a link you posted a while ago, but I can't find it.



[http://www.mouser.com/ProductDetail/Mean-Well/SP-200-24/?qs=moO%2F8p2KOt%252bxowKqCFhHzA%3D%3D](http://www.mouser.com/ProductDetail/Mean-Well/SP-200-24/?qs=moO%2F8p2KOt%252bxowKqCFhHzA%3D%3D)





**Gus Montoya**

---
---
**Brad Hopper** *May 05, 2015 00:40*

Jameco has had the best prices and best selection for Meanwell power supplies.


---
**Mykyta Yurtyn** *May 05, 2015 00:48*

I got mine with my smoothie order here: [http://shop.uberclock.com/products/power-supply-ac-to-dc](http://shop.uberclock.com/products/power-supply-ac-to-dc)


---
**Isaac Arciaga** *May 05, 2015 01:45*

**+Gus Montoya** there's not much price difference on Mean Well PSU's from site to site. We're talking 2-3 bucks maybe. If you want more out of your $50 then go with a Wei Hao 400w [http://www.ultibots.com/24v-400w-power-supply-s-400-24/](http://www.ultibots.com/24v-400w-power-supply-s-400-24/)


---
**Gus Montoya** *May 05, 2015 02:34*

Would it be possible to use the one I have momentarily ( 200w 12v) or should I just get the Meanwell. If I am paying more for quality then I won't mind. **+Mykyta Yurtyn** Yeah I tried there when I ordered my smoothie. That site is mostly sold out of products :(   **+Isaac Arciaga**  I haven't heard of WEi Hao, any info on quality and durability? I don't want my apt burning down on me hehe


---
**Isaac Arciaga** *May 05, 2015 03:09*

**+Gus Montoya** Wei Hao is the "good" chinese brand and the 400w unit is widely used in reprap. It's not shit like everything else you see from Amazon etc. It's probably the only psu I recommend for the wattage outside of Mean Well. I have thousands of hours of print time on my machines with it and I leave them on 24/7.


---
**Gus Montoya** *May 05, 2015 03:23*

I did a quick search and found good reviews. I've already ordered it. Thank you :)


---
**Daniel F** *May 05, 2015 07:56*

There are cheap ones but the question is if you want one with convection cooling (no fan) or one that makes noise...

I ordered this one: [http://www.aliexpress.com/item/Original-MEAN-WELL-power-suply-unit-ac-to-dc-power-supply-NES-200-24-200W-24V/1503450254.html](http://www.aliexpress.com/item/Original-MEAN-WELL-power-suply-unit-ac-to-dc-power-supply-NES-200-24-200W-24V/1503450254.html)

It hasn't arrived so I can't tell anything about the quality


---
**Derek Schuetz** *May 07, 2015 15:02*

I ordered mine from ultibots saves on shipping


---
**Gus Montoya** *May 07, 2015 17:06*

**+Derek Schuetz**  Yeah I ordered from ultibots [http://www.ultibots.com/24v-400w-power-supply-s-400-24/](http://www.ultibots.com/24v-400w-power-supply-s-400-24/)   I got the extra big one just in case I decide to upgrade. I already have another printer in my head. I'm working with a manufacturer to see how all that works. 


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/C7diAWJ3Gwg) &mdash; content and formatting may not be reliable*
