---
layout: post
title: "Anyone into 3D scanning? I am still looking for a practical DIY 3D scanner"
date: April 21, 2015 00:30
category: "Discussion"
author: Gus Montoya
---
Anyone into 3D scanning? I am still looking for a practical DIY 3D scanner. I need to scan items that are 3ft x 3ft.  





**Gus Montoya**

---
---
**Dat Chu** *April 21, 2015 01:06*

3 ft is quite a bit but you can use the same system that bq released recently open source. Cyclop? I think it is called.


---
**guillermo gerard** *April 21, 2015 01:31*

Cyclop needs you to buy an electronics kit (as far as i know). I think sardauscan is a better open software/hardware option. I already have the lasers and webcam. Only spare time is needed...sigh...


---
**Gus Montoya** *April 21, 2015 02:27*

I'll look into both. Detail is really important to get eact curves and angles. 


---
**Miguel Sánchez** *April 21, 2015 07:11*

try Autodesk 123D catch, you only need a camera/smartphone.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/cWPJZbfzxbG) &mdash; content and formatting may not be reliable*
