---
layout: post
title: "And so it begins.... extrusions to process and parts to print!"
date: December 07, 2017 05:12
category: "Build Logs"
author: Dennis P
---
And so it begins.... extrusions to process and parts to print!

![images/9d915230ddef3d7e9a4d81cd42d04c1f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9d915230ddef3d7e9a4d81cd42d04c1f.jpeg)



**Dennis P**

---
---
**Dreas VEGAN** *December 07, 2017 05:15*

wow.


---
**Eric Lien** *December 07, 2017 22:26*

I am excited to see your progress. Keep us updated as you work. Watching a new printer come together is my favorite part.


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/Qpu1sy2wnsh) &mdash; content and formatting may not be reliable*
