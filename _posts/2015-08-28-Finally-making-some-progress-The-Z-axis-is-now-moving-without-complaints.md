---
layout: post
title: "Finally making some progress. The Z axis is now moving without complaints"
date: August 28, 2015 02:16
category: "Show and Tell"
author: Rick Sollie
---
Finally making some progress. The Z axis is now moving without complaints. Made a one-off piece to constrain the lead screw, being carefully to give it an adjustable  range in the x and y axis.  



Got some unexpected help from my wife, Sheila, who is anxious to start printing. She offered to help so I handed her the Rasberry pi and told her to load Octoprint....She did!!



![images/91dc3b01d71bfd8c194daeee5d9e37d5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/91dc3b01d71bfd8c194daeee5d9e37d5.jpeg)
![images/2bf23aa962bab1ed044c23b0f68fd215.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2bf23aa962bab1ed044c23b0f68fd215.jpeg)

**Rick Sollie**

---
---
**Ricardo de Sena** *August 28, 2015 02:22*

Good Z axis.


---
**Eric Lien** *August 28, 2015 02:31*

Looking great. Can't wait to see you print something that uses the full Z height on that monster.


---
**Mike Miller** *August 28, 2015 02:46*

Gotta love Techy wives!


---
**Rick Sollie** *August 28, 2015 11:19*

**+Eric Lien** I'm thinking of a classic Marvin the Martian rocket ship. One that will accept the solid rocket fuel used in hobby rockets.    Vrooooom!


---
**Rick Sollie** *August 28, 2015 11:36*

Thantik was asking about height (can't find the post) the corner 2020 is 896mm. The linear &threaded rods were lengthened accordingly (just not accurately). They should have been 40mm longer.


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/Wqpv5Eiz35j) &mdash; content and formatting may not be reliable*
