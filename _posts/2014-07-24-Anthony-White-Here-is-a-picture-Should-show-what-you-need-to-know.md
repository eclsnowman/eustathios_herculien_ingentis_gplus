---
layout: post
title: "Anthony White Here is a picture. Should show what you need to know"
date: July 24, 2014 10:15
category: "Show and Tell"
author: Brian Bland
---
**+Anthony White** Here is a picture.  Should show what you need to know.

![images/58282638a38491bb5212e0a6efa06960.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/58282638a38491bb5212e0a6efa06960.jpeg)



**Brian Bland**

---
---
**Tony White** *July 24, 2014 16:33*

I see - some precise belt trimming is in order that both ends get 1/2 the ridges in mesh with the belt huh?



I'm running my x and y drive belts from the motor up within the frame so I can enclose without as many holes / leaks in the panels. Why are you running them on the outside? Better balance?



I see you and **+ThantiK .**  are both going with a kraken as well - I'd be willing to front an oshpark board set to try and reduce the # of wires for thermocouples with an i2c bus connection to Marlin (I'm using azteeg x3 pro....)



I'm also getting some panels lasercut to enclose the 4 sides, bottom (+ mount electronics to bottom cutouts),  and a rectangular pyramid top, with airtripper extruders mounted on 3 (or 4) of the sloped sides, and a small direct drive extruder perched on the kraken for ninjaflex (that comes last, if it doesn't work through the  super short vertical bowden tube (240mm?.)  I'll post design files if it actually all works (and renders when I finish mocking it all up in SW.)


---
**Brian Bland** *July 24, 2014 16:37*

I have since switched to direct drive and lost the drive belts.  Would be interested to hear more about the board for thermocouple..  I am running X3 Pro also.




---
**Tony White** *July 24, 2014 16:42*

I thought I had seen people using a ADC on a board on the hotend, and were looking for how to enable/connect to Marlin - can't remember if it was feasible or not though! It would reduce the wire count for thermocouples from 8 to 3 (clock, data, and ground - can't use heater ground due to pwm signal I believe....)



If I had more time and a tad more EE experience I'd knock up a board that moved the mosfet drivers to the hotend as well, and design some good cooling into the overall board mount. Almost positive such a control feature isn't yet a thing in Marlin though.... **+Whosa whatsis** ?


---
**Nick Parker** *July 24, 2014 19:05*

**+Anthony White** The mosfet move would be as simple as changing pins.


---
**Tony White** *July 24, 2014 20:50*

true - if someone can pick out appropriate mosfet, ADC with I2C and 4 channels (and suitable for thermocouples) I could knock something up in kicad with the datasheets...  maybe we could all try out Hackaday's platform for project sharing / publication / etc


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/b6inGeP5EF4) &mdash; content and formatting may not be reliable*
