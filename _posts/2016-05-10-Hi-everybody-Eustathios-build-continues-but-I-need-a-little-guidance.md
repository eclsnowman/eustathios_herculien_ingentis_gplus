---
layout: post
title: "Hi everybody! Eustathios build continues, but I need a little guidance"
date: May 10, 2016 16:40
category: "Build Logs"
author: Dimitrios Tzioutzias
---
Hi everybody! Eustathios build continues, but I need a little guidance. 

It is ok if I use 1/32 microstepping for the extruder since my mks sbase board has only one jumper for all 5 DVR drivers? I tested the extrusion and it seems that it under extrude at 972 steps so I calibrate them to ~994 steps. I use the standard geared stepper that is mentioned on BOM. Also some initial results from heated bed shows that it can reach 110c in 2 min it seems that 800w rubber heater does it job pretty good. I still have to run PID autotune for K values. After it reaches 110c it consumes ~230 watt to maintain the temp. Also does anyone knows how I can change in smoothieware the standard "extrude 5mm" menu option to a bigger value? In marlin I can extrude as much length as I want but in smoothie is different since there is no option in JOG menu for the extruder only for x,y,z axes movement.





**Dimitrios Tzioutzias**

---
---
**Daniel F** *May 11, 2016 11:24*

1/32 steps is no problem, you just need to adapt the steps/mm. The max  extrusion speed will go down but that's probalby ok. I calculated it for my Eustathios. If the 32 bit board can generate 100k steps/s and the extruder needs 500Steps/mm in 16 microstep configuration you get 200mm/s max extrusion speed with 16 microsteps and 100mm/s with 32 microsteps. So you should be fine assummig your extruder does not use an extreme gear and 400 steps/relevation steppers. The fastest speed I use is for retraction which is 30mm/s in my case.



Max steps/s of the board divided by steps/mm of extruder = max speed of extruder [mm/s]


---
*Imported from [Google+](https://plus.google.com/108355995361667474020/posts/5fJWS3z1Nj7) &mdash; content and formatting may not be reliable*
