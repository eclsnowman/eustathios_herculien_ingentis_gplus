---
layout: post
title: "my Ul-T-Slot but I'm making some revisions"
date: March 07, 2014 05:13
category: "Show and Tell"
author: D Rob
---
my Ul-T-Slot but I'm making some revisions. As I speak. The access plate is inconvenient. instead I will flip the board and install it upside down in place of the clear Plexiglas and have a panel on the bottom for adjustments a la Ultimaker. The risers for the electronics/ z cabinet were an afterthought and are being replaced along with the longer upright extrusions by 22"long extrusions from top square frame to bottom square frame, and 4 x 16" lengths will run between for the bottom of the build area/top of the electronics cabinet. The bottom will be sealed by a panel secured to the tslot by m4 and t nuts. the panel corner will be routed to fit conformed to the Goopyplastic foot corners

![images/b66ff54cb75b3b34f5c42b2f3660a9b0.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b66ff54cb75b3b34f5c42b2f3660a9b0.gif)



**D Rob**

---
---
**D Rob** *March 07, 2014 05:20*

oh yeah I moved to 24v and ultiboard 1.5.7 but if anyone can convert the pdf to gerber for me I want the ultiboard 2.1.1 which is designed better and for 24v without heatsinks on the 7812 or replacing it for a drop in switching regulator like ill be using


---
**Jarred Baines** *March 08, 2014 14:08*

That looks great man :-)



How did you enclose the sides, is it just perspex / polycarb wedged in between the extrusions or have you sealed it with silicone / something?



Is it heated bed?


---
**D Rob** *March 08, 2014 14:38*

**+Jarred Baines**  its blue 6mm acrylic with silicone caulk to stop rattling from vibration


---
**Jim Squirrel** *March 08, 2014 22:32*

[http://swannman.github.io/pdf2gerb/](http://swannman.github.io/pdf2gerb/)


---
**D Rob** *March 09, 2014 01:05*

**+Jarred Baines** sorry for the incomplete reply. yes heated bed. 24 v now custom made w/nichrome from [www.jacobsonline.biz](http://www.jacobsonline.biz) the acrylic fits in the slots then i silicon-ed the inside corners where acrylic meets extrusion to dampen the vibration/ kill the rattling. I plan on vacuum forming a hood for it and a hair dryer or 12v heater for a heated build chamber. it would come from below and blow towards the bottom of the bed's insulation to prevent drafts on the part.


---
**D Rob** *March 09, 2014 07:01*

Unfortunately I can't use the pdf2gerb. The pfd includes too many layers. The program can only help me with 2 layers and a silk screen **+Jim Squirrel** you don't know anyone with the skills to help turn that into a gerb so I can get some PCBs do you? **+Daid Braam** any way I could score a copy of the 2.1.1 gerbers? I want to try my hand at building my own board and I would like it to be one of ya'll's  I got the PDF and pasrts list from the git hub


---
**Jarred Baines** *March 09, 2014 10:14*

Rob, you using bushes or bearings? Am I able to access your build files?


---
**D Rob** *March 09, 2014 13:31*

[bushings.pm](http://bushings.pm) me your email and ill send a zip


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/ie8Uxc3DYjG) &mdash; content and formatting may not be reliable*
