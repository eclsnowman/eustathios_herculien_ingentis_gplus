---
layout: post
title: "Eric Lien I recently started using Simplify3d (it's about time, I bought it months and months ago)"
date: July 27, 2017 20:31
category: "Discussion"
author: Brandon Cramer
---
**+Eric Lien** 



I recently started using Simplify3d (it's about time, I bought it months and months ago). My Z direction was not right. When I pressed up on the z axis to make it go up it would go down. It was brought to my attention to swap the wiring on the z axis going to the Azteeg X5 Mini. I did this, but now when I press Z home, it doesn't go towards the end stop, but up and down are going the correct direction.



Here is the config I have on the X5 mini:



[https://www.dropbox.com/s/c5561rlrealst5v/Azteeg%20x5%20mini%20config%2007272017.txt?dl=0](https://www.dropbox.com/s/c5561rlrealst5v/Azteeg%20x5%20mini%20config%2007272017.txt?dl=0)









**Brandon Cramer**

---
---
**Zane Baird** *July 27, 2017 21:00*

"up" in simplify3d corresponds to an increase in z, which means the bed moves down. If you revert to the original settings you had it is correct


---
**Brandon Cramer** *July 27, 2017 21:03*

**+Zane Baird** Do you think I will just have to remember that up goes down and down goes up then? I don't see a way to swap the controls in simplify3d. 



Since I have the memory card out of the x5 mini, should I update the firmware from 2015 to the latest?




---
**Eric Lien** *July 27, 2017 22:24*

Don't think about it as up makes the bed go down. Think of it as up increases nozzle to bed distance, down decreases nozzle to bed distance.



As far as upgrading smoothieware, I think it would be a good idea. But keep your old firmware.cur, config, and config-override files. That way if you have anything you don't like or have problems you can just go back.



Do note that the newer firmware requires the newer config file. It shouldn't take long to edit it. But just be aware you can't use old config with new firmware since several things have changed or been renamed.


---
**Brandon Cramer** *July 27, 2017 22:35*

I messed this thing all up. Argh. 



It won't extrude filament anymore after putting the wiring back to normal. 



Any chance you can look this over and see if I have something wrong here. 



[dropbox.com - x5mini.zip](https://www.dropbox.com/s/ytsxo54cly921pv/x5mini.zip?dl=0)










---
**Brandon Cramer** *July 28, 2017 03:54*

Why wouldn't the extruder put out filament? 


---
**Brandon Cramer** *July 28, 2017 03:55*

**+Eric Lien** where would I find the new config file to run with the latest firmware version? 


---
**Jim Stone** *July 28, 2017 19:09*

i believe if you look at the controls in S3D its non directional but more.... graph and plot like. ie + and - as well as number values. as there technically is no "up down left right" its all relative to the extruder i believe and where it moves to.


---
**Jim Stone** *July 28, 2017 19:11*

just think of it as "up" is increasing a gap between the bed and hotend because "up" increases the positive value. 



but i myself dont find myself often manually jogging the printer.




---
**Eric Lien** *July 28, 2017 22:35*

**+Brandon Cramer** here: [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/tree/edge/ConfigSamples/AzteegX5Mini) 



(Which version will depend on what version board you have).



You will need to hand edit it. Just open your old config, the new config, and your config override file. It gets pretty clear what needs to be added after that.


---
**Brandon Cramer** *July 28, 2017 23:39*

Looks like I have the Azteeg X5 mini V1.1. Is there a config file for that? I'm not 100% sure I have my old config file. 


---
**Brandon Cramer** *July 28, 2017 23:55*

I guess maybe that is the config file that isn't in the V2 or V3 folder. I think my old firmware file is corrupt. 



[dropbox.com - firmware.bin](https://www.dropbox.com/s/y7iysg8pytg8mq0/firmware.bin?dl=0)



So which firmware version should I run with the V1 config file?


---
**Eric Lien** *July 29, 2017 00:51*

Use the config file in the main folder I linked, not the V2 or V3 folder.


---
**Eric Lien** *July 29, 2017 00:54*

And this firmware (renamed to [firmware.bin](http://firmware.bin))



[raw.githubusercontent.com - github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-latest.bin?raw=true](https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-latest.bin?raw=true)


---
**Eric Lien** *July 29, 2017 00:55*

But again, you need to edit it.


---
**Brandon Cramer** *July 29, 2017 01:30*

**+Eric Lien** Thanks Eric. When you say I will need to edit it... Got anything I can compare my config file to? Also what needs to be on the SD card? Just firmware.bin and config.txt?


---
**Eric Lien** *July 29, 2017 03:18*

Yes, [firmware.bin](http://firmware.bin) and the config. I would just compare it to your original config, or the one on the GitHub. Also if you did PID tuning originally then look in your original config-override file. It will have things you modified via gcodes and saved via m500.


---
**Eric Lien** *July 29, 2017 03:19*

Here's the original config files from the GitHub: [https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Smoothieware](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Smoothieware)


---
**Brandon Cramer** *July 31, 2017 23:59*

I'm starting to think there is a problem with my X5 Mini. I don't see any reason why the attached files wouldn't allow the extruder to work. 



[dropbox.com - Azteeg X5 Mini](https://www.dropbox.com/sh/e7ldbbc78dob74n/AAAeHDDeJD5rHuBwL0LuNuDka?dl=0)



I can test for power if I know what I'm looking for. 



Time to get a new X5 mini? 


---
**Eric Lien** *August 01, 2017 01:23*

What started the problem again? Because it was working until you tried reversing the z axis correct?


---
**Brandon Cramer** *August 01, 2017 03:05*

Yes. It was working until I decided to swap the z axis. I remember unhooking the extruder thinking it was the z axis. I've tried swapping the wiring and messed with firmware and config file to no end. I also remember looking up at the extruder and it might have been partially unplugged. Since then, it hasn't worked. The wiring is good and the stepper motor works.


---
**Eric Lien** *August 01, 2017 04:03*

might have toasted the driver if it was unplugged while powered on.


---
**Brandon Cramer** *August 01, 2017 04:26*

If that is the case, should I stick with the X5 mini? I think the drivers are built in on v1.1. 


---
**Eric Lien** *August 01, 2017 05:09*

Mini v3 has separate drivers. But I might talk to Roy at Panucatt first. He might have some troubleshooting you could try first.


---
**Brandon Cramer** *August 01, 2017 05:13*

**+Eric Lien** Are you able to share his contact information or find out what I should do next? 


---
**Brandon Cramer** *August 01, 2017 20:16*

I sent an email over to **+Roy Cortes**. My X5 mini has screw type terminals. I'd like to keep this configuration if possible. 


---
**Brandon Cramer** *August 02, 2017 00:34*

I want to get a new controller ordered asap. Since this mini v3 wont have screw type terminals, what will I need for connectors? 


---
**Brandon Cramer** *August 03, 2017 03:05*

I can't wait to get my Azteeg x5 mini v3 so I can get back to printing. I guess it might be time to hook up the heated bed. 


---
**Brandon Cramer** *August 23, 2017 15:52*

So I got my X5 Mini V3. Thanks **+Roy Cortes**!! No thanks to USPS sending it back! 



Should I use Eustathios V2 Config File and firmware from here: 



[panucatt.com - panucatt.com](http://www.panucatt.com/azteeg_X5_mini_reprap_3d_printer_controller_p/ax5mini.htm)



Do I need to use 1/32 microstepping for all of the axis? 


---
**Brandon Cramer** *August 23, 2017 16:53*

Ok. It's Monday for me... 



I set it up the same as my X5 Mini V1. X 1/32 Y 1/32 Z 1/16 Extruder 1/16.



In the config file it shows:



alpha_steps_per_mm                           160              

beta_steps_per_mm                             160               

gamma_steps_per_mm                       2560             



Do I need to adjust these numbers? 




---
**Eric Lien** *August 23, 2017 21:19*

I would start with a fresh Config and the latest Firmware. My V2 Config is so old it will not work with the latest firmware. But if you pull my old Config up along with the latest base Config from the smoothieware GitHub... It will take you no time to make the needed changes.



Latest base Config for the Azteeg X5 Mini V3: [https://github.com/Smoothieware/Smoothieware/raw/edge/ConfigSamples/AzteegX5Mini/Version3/config](https://github.com/Smoothieware/Smoothieware/raw/edge/ConfigSamples/AzteegX5Mini/Version3/config)



Latest Firmware to load on SD Card: [https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-latest.bin?raw=true](https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-latest.bin?raw=true)


---
**Brandon Cramer** *August 25, 2017 16:22*

So my new X5 Mini will not work with these two files above **+Eric Lien**. The Viki2 will not boot up and show the display.  I had the SD card from my other X5 Mini and at least the Viki2 will boot up all the way and show the display. 



I'm not sure how to tell what version of firmware I have here:



[dropbox.com - 08-25-17](https://www.dropbox.com/sh/pt5gv38bp6pv6r3/AADnJ9Z2XU7xe7kyvBiZI9Tna?dl=0)






---
**Eric Lien** *September 10, 2017 23:54*

**+Brandon Cramer** sorry work and other projects has had me pretty busy. You get everything working again?


---
**Brandon Cramer** *September 11, 2017 01:15*

**+Eric Lien** I had the wiring wrong for the steppers. Once I figured that out I had some movement. I think for X and Y stepping I need it set to 1/32 stepping. For Z and Extruder I believe I was doing 1/16 stepping. That's when it all went bad. I pulled the driver off the Z and Extruder and put the driver back on the Extruder backwards. After I put the driver back on correctly it fried my new X5 mini. 



**+Roy Cortes** is sending me a new X5 mini. So hopefully I will have it back working this week. Thanks again **+Roy Cortes** for your help!!!


---
**Brandon Cramer** *September 11, 2017 19:54*

I received the X5 Mini this morning. I have a few issues. 



When I try to home x or y it moves about a 1/2" each time I try to home. 



If I try to move the Z axis it just squeals and doesn't move. 



My hotend fan shuts off after it gets to temp. This should run the entire time I have the hot end on. This is probably just a wiring issue.



I'm not sure about the other two issue though. 



I didn't update the firmware because I believe it came with the latest already installed. 



Here is my config file. 



[dropbox.com - config.txt](https://www.dropbox.com/s/xk681p284uv52fg/config.txt?dl=0)





 


---
**Brandon Cramer** *September 11, 2017 20:44*

I fixed the fan for the hotend. I just wired it directly to the main 24 volt power feed. If there is a better place I'm not aware of it. 



I lowered the current to 1 Amp for the Z axis but I still don't have any movement. I had it set to 1.5 Amps. 



This X5 mini with 5984 drivers is so quiet!!!! I can't wait until I can print something. 


---
**Eric Lien** *September 11, 2017 21:32*

normally that 1/2" movement means it tries to home, but then it sees the endstop is met. Confirm endstop wiring and endstop code looks correct. I will look once I get home.




---
**Brandon Cramer** *September 11, 2017 21:46*

What should the end stops be wired to?  I currently have it wired to GND and SIG. should it be GND and V+?


---
**Eric Lien** *September 12, 2017 00:11*

Is it wired NO or NC


---
**Eric Lien** *September 12, 2017 00:12*

Then make sure the Config lines up with that.


---
**Brandon Cramer** *September 12, 2017 00:14*

NO. When I hold down the end stop it's going to max when I press home. 


---
**Brandon Cramer** *September 12, 2017 00:15*

SIG and GND to C and NO


---
**Eric Lien** *September 12, 2017 00:33*

if it goes to max when you hit home you have two problems. Your motor is inverted (going the wrong direction) and needs to be inverted with wiring or in the config.



And your endstop logic is wrong (need to flip the logic on the endstop). If you have the ! take it off, and vice versa. 


---
**Brandon Cramer** *September 13, 2017 19:04*

I'm at a loss. I've tried just about every combination and it's still not working like I would expect. The way I have it below it's at least allowing the printer to go home when I press home for X and Y. But when I try to move it manually it goes in the opposite direction that it should for both X and Y. 



alpha_dir_pin                                0.11!

beta_dir_pin                                 0.20



alpha_min_endstop                            1.24^!

alpha_homing_direction                       home_to_min



beta_min_endstop                             1.26^!

beta_homing_direction                        home_to_min



Z axis doesn't work at all so far. It moves when I hold the end stop down about 1/2". When I press up or down for Z it just squeals. I currently have it setup like this:



gamma_dir_pin                                       0.22

gamma_min_endstop                            1.28^!

gamma_homing_direction                       home_to_min

gamma_current                                     0.50


---
**Brandon Cramer** *September 14, 2017 17:26*

**+Eric Lien** I got X, Y, and the Extruder working correctly now. I flipped X in S3D software. 



Z isn't working like it should. If I hold down the end stop and press home it goes about 1/2 inch towards the end stop. 



If I try to move Z up or down it just squeals. It does this with the endstop open or closed. Since it moves when I press home and holding the endstop I don't think there is anything wrong with the stepper. 


---
**Brandon Cramer** *September 15, 2017 00:07*

Ok. Somehow my gamma steps per mm was set to 2560 while the others are set to 160. Should these be the same? I changed it to 160.  



Now I can move the Z axis up and down, but when I press home it moves about 2-3 inches or so and then give the halt or m999 message on the Viki display. 



What should the stepping really be for X, Y, and Z?



I'm so close to getting this working... 


---
**Eric Lien** *September 15, 2017 00:31*

What stepper drivers


---
**Eric Lien** *September 15, 2017 00:32*

And what jumper settings under the drivers for microstepping.


---
**Brandon Cramer** *September 15, 2017 00:46*

**+Eric Lien** SD5984. I have 1/32 for X and Y. 1/16 for E and Z. I'm not sure if that is what I should really use. 


---
**Brandon Cramer** *September 15, 2017 20:59*

**+Eric Lien** **+Roy Cortes** Everything is working good with one exception. I can't home Z if it's more than 17mm away. If it's more than 17mm away I get a halt / M999. 



My config file is located here:



[dropbox.com - config.txt](https://www.dropbox.com/s/6b3umy8mnzbprdu/config.txt?dl=0)






---
**Eric Lien** *September 15, 2017 21:29*

when you tell it to move 10mm does it go the correct distance on Z?


---
**Brandon Cramer** *September 15, 2017 21:39*

No. 


---
**Brandon Cramer** *September 15, 2017 21:52*

Is this going to be a microstepping issue? Is microstepping only adjusted on the X5 Mini pins under the SD5984 drivers? I think it's currently set to 1/32. 




---
**Brandon Cramer** *September 15, 2017 23:37*

**+Eric Lien** I need to update this a little bit.... 



I have all X, Y, and Z set to 1/32 microstepping and steps per mm set to 160. 



I don't understand why X and Y move the correct distance but Z barely moves? 



I'm a little in over my head here. 






---
**Eric Lien** *September 16, 2017 03:49*

At 1/32 microstep on Z using 2mm pitch leadscrews your steps/mm on Z would be

![images/4382a16fb2e664ff296ba8478338b399.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4382a16fb2e664ff296ba8478338b399.png)


---
**Eric Lien** *September 16, 2017 03:49*

5120


---
**Brandon Cramer** *September 18, 2017 20:40*

**+Eric Lien** I tried setting it to 5120 but I get this error when I try to home Z if Z isn't close to the end stop: Error: Homing cycle failed. 



Also whenever I try to move Z down 100mm with S3D, it only moves the Z axis about 10mm. 


---
**Brandon Cramer** *September 18, 2017 23:55*

Ok. It's not having any trouble homing Z anymore, but moving Z down 100mm doesn't work. It only moves about 10mm. 


---
**Brandon Cramer** *September 21, 2017 20:30*

I finally got my Eustathios Spider V2 printing. I'm having an issue where it shifts the X axis over a bit during the print. 

![images/f06e906fbee0bb47f5c8cd61f49f1e57.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f06e906fbee0bb47f5c8cd61f49f1e57.jpeg)


---
**Eric Lien** *September 21, 2017 21:01*

I would check two things. Make sure you aren't catching the nozzle (over extrude or curling feature), and make sure you have stepper currents adjusted correctly that you are not running the motors hot (causing thermal shutdowns) or too low (not enough torque). 



last thing to check would be to confirm the gantry moves smoothly by hand (no binding).



Sorry I have been slow to reply recently. Work and life are burning me at both ends.


---
**Brandon Cramer** *September 21, 2017 21:24*

**+Eric Lien** I'm running 1 amp for X and Y, and 1.5 for Z. The X and Y steppers  are warm I guess I would call it. If it happens again I will try some of the steps you mentioned. 



No problem. I knew eventually I would get it working, I just didn't know when it would be. Thanks for your help. 



**+Roy Cortes** Thank you for your help on getting my 3D printer back up and working. 


---
**Brandon Cramer** *September 22, 2017 23:32*

I'm not sure what this is about. The carriage moves around a little bit and then starts printing. 



[dropbox.com - File Sep 22, 4 28 47 PM.mov](https://www.dropbox.com/s/bf2gdvzr7gdpduy/File%20Sep%2022%2C%204%2028%2047%20PM.mov?dl=0)




---
**Eric Lien** *September 23, 2017 04:31*

Looks like a wipe gcode. Look at your start gcode in your slicer.


---
**Brandon Cramer** *September 23, 2017 17:05*

**+Eric Lien** thanks. I will check it out. 



This is my last two prints. The one on the right is .2mm and the one on the left is .1mm. Do you have any recommendations on improving this print? I normally print at .1mm. I turned off auto extrusion width thinking it might be causing me some trouble. 



It might be time for me to print some new Eustathios Spider V2. A few pieces have seen better days but it was printing great before I swapped the X5 mini. 

![images/41f5cc56b87904e1b97795758062420c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/41f5cc56b87904e1b97795758062420c.jpeg)


---
**Eric Lien** *September 23, 2017 18:43*

Off the bat, try outside in instead of inside out on perimeters. Then try slightly lower temp. Looks like you might need some dialing in on first layer height (i always run 0.3mm first layer for adhesion , then switch to your desired layer height on layer2). 



Looks like maybe some coast to end/retraction tuning is required.



But this is all speculation. Mind posting your settings you are currently using? What slicer, etc?


---
**Eric Lien** *September 23, 2017 18:44*

Also, what do you have acceleration currently set at?


---
**Eric Lien** *September 23, 2017 18:45*

For example I disable junction deviation on Z (essentially turn off jerk on Z).


---
**Brandon Cramer** *October 09, 2017 19:14*

I guess I wore out my stepper for Y axis. One of the bearings froze up and looks like it leaked all the grease out. I ordered the bearing on Amazon today. 2 years of pretty much constant printing! 


---
**Eric Lien** *October 09, 2017 20:58*

What current are you running right now on Y? I only ask because I have been slowly stepping down my current on a few printers with good success. Early on when I first built these I was on the more is better mentality. But I am slowly learning more is better, but the right amount is best :)


---
**Brandon Cramer** *October 09, 2017 21:15*

1.5 but I was thinking since the bearing might have been causing me trouble I might go back down to 1 for both X and Y.  (I'm a victim of more is better for sure) Are you running around that range? 


---
**Eric Lien** *October 10, 2017 01:21*

yeah, try 1.0A as a starting point.


---
**Brandon Cramer** *October 11, 2017 23:48*

Now I remember why I was at 1.5. on X and Y. After I went to 1.5 this issue stopped happening. 

![images/903eabbb60d5da50f6bbe90a93523907.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/903eabbb60d5da50f6bbe90a93523907.jpeg)


---
**Brandon Cramer** *October 11, 2017 23:56*

I really need to work on the stringy issue. Looks like a hairy groot. 


---
**Brandon Cramer** *October 13, 2017 00:00*

Well I tried. The stepper is finished. I ordered a new one from Robotdigg. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/afFaWsPHGyi) &mdash; content and formatting may not be reliable*
