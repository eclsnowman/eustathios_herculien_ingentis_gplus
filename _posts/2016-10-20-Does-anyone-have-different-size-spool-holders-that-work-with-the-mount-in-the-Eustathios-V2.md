---
layout: post
title: "Does anyone have different size spool holders that work with the mount in the Eustathios V2?"
date: October 20, 2016 02:31
category: "Discussion"
author: Sean B
---
Does anyone have different size spool holders that work with the mount in the Eustathios V2?  I have a new spool of PETG from MG chemicals that has a small opening and the default spool stl included is too larger.





**Sean B**

---
---
**Stefano Pagani (Stef_FPV)** *October 20, 2016 02:39*

I think, **+Walter Hsiao** made one that works by a lever action using bearings to roll on the side of the roll and a small peg in the center




---
**Sean B** *October 20, 2016 02:46*

**+Stefano Pagani**​ I've seen that before but wasn't sure who posted it.  I'll hunt around a bit.  Thanks.


---
**Eric Lien** *October 20, 2016 02:53*

Several different sizes here: [youmagine.com - Spool Holders](https://www.youmagine.com/designs/spool-holders)



There are also the solidworks files which length and diameter can be changed. But if you need something specific let me know, I can modify it pretty quickly.


---
**Stefano Pagani (Stef_FPV)** *October 20, 2016 02:56*

**+Eric Lien** while your here :) could I use PET for the bearing holders and swap to abs later? I dont have a heated bed on my rep 2


---
**Eric Lien** *October 20, 2016 03:23*

The bearing holders on a Eustathios is no problem, to be honest PLA for everything other than the hot end duct will work just fine for Eustathios.



For HercuLien on the other hand ABS is recommended for most all parts inside the enclosure because of the higher TG. The reason is with an enclosed printer like HercuLien the chamber get to around 50-55C inside with the passive heat from the heated bed. Most PLA (other than some of the newer annealing PLAs coming out) will not handle that so if there are stresses on the components they will elongate and deform.



Some PET based filaments have TG around 75-80C which should work, while some because of the modifiers added are lower approaching the PLA ranges (though still usually higher than PLA).


---
**Stefano Pagani (Stef_FPV)** *October 20, 2016 03:36*

**+Eric Lien** Thanks for the clarification! Ill just go with PET for now bc all of my parts cracked :( Plus I don't really like the red and black (I've got too much of it!) :P I'll then go with orange ABS and acetone smooth it. I also may be able to print the parts on a carbon 3D machine :)




---
**Maxime Favre** *October 20, 2016 05:12*

I made one based on walter's design: [http://www.thingiverse.com/thing:1570166](http://www.thingiverse.com/thing:1570166)

[thingiverse.com - Low profile spool holder by PookY](http://www.thingiverse.com/thing:1570166)


---
**Ted Huntington** *October 20, 2016 05:18*

In terms of spool holders- this design is crude and in-elegant to say the least, but as a last resort and in a pinch I usually throw one of these together: [https://www.youmagine.com/designs/3d-printer-filament-spool-holder](https://www.youmagine.com/designs/3d-printer-filament-spool-holder) The inside plastic parts and most if not all of the nuts are not needed either- it just needs the 2 plastic "reprap" parts and 5 cut rods.

[youmagine.com - 3D Printer filament spool holder](https://www.youmagine.com/designs/3d-printer-filament-spool-holder)


---
**jerryflyguy** *October 22, 2016 09:01*

**+Sean B** if your still needing a smaller mount let me know. New filament order arrived today w/ a 32mm center hole. No matter how I tried, just couldn't get that 50mm holder inside the 32mm spool (kidding). 



So I modified the SolidWorks model to fit a 25mm hole, w/ the outside/tip lip expanding to 30mm. Printed it out and it works great. Glad to send you the file if it helps you. It screws into the original base/mount for the 50mm holder.


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/h3X4K3c8hsq) &mdash; content and formatting may not be reliable*
