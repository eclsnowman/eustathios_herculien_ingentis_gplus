---
layout: post
title: "Hi everyone ... I want to make an offer about wiring this 3D printer"
date: September 29, 2016 21:45
category: "Discussion"
author: Botio Kuo
---
Hi everyone ... I want to make an offer about wiring this 3D printer. I have finished everything frame parts and have every component ready to go. But just don't have time to wire it and test. So if anyone can help me to wire and finish it, I can pay 50 per hour for wiring and location is in downtown Los Angeles, CA. Thank you. 





**Botio Kuo**

---
---
**Eric Lien** *January 01, 2017 22:44*

Were you ever able to find help with the wiring? I know we have some group members out in that area. And perhaps there is a good makerspace in the area where some knowledgeable people could give you some advice.


---
**Botio Kuo** *January 02, 2017 10:52*

**+Eric Lien** Hi Eric, I haven't find help for the wiring... and also too busy for school in last three months. If anyone can help me in CA that will be wonderful and I'm grateful for that.


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/1uckwXpBNXF) &mdash; content and formatting may not be reliable*
