---
layout: post
title: "I have been slowly building my Eustathios from Eric Lien s design for awhile now, I just recieved my 1204 ball screws and last parts I need are my alum bed plate and bed heater now"
date: September 22, 2016 20:38
category: "Deviations from Norm"
author: Ryan Jennings
---
I have been slowly building my Eustathios from **+Eric Lien** s design for awhile now, I just recieved my 1204 ball screws and last parts I need are my alum bed plate and bed heater now.  



In my impatience I have already designed a upgrade to mine to allow for a direct drive titan to be mounted on 2 MGN12 rails with a direct drive Titan.  From my CAN XY seem to have the same size, while adding about 10mm in Z.  However the whole unit sits slightly above the top of the frame now. 



I have the parts in stock, and started a rough cut on my aluminum angle.  That leaves the printed XY carriages and I should be ready to go.  



My goal here is to see if someone else has a suggestion for improvement or other idea that might work better than what I have planned already.  



![images/1a9d5439acd89c328ded4b382f1203b5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1a9d5439acd89c328ded4b382f1203b5.jpeg)
![images/deddeaa417fb1a2a36aff8467f6b7ff4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/deddeaa417fb1a2a36aff8467f6b7ff4.jpeg)

**Ryan Jennings**

---
---
**Ted Huntington** *September 22, 2016 23:02*

Interested to see how the MGN12 rails work out- I wonder how the price compares between the two designs. It would be great to eventually be able to just screw a wide variety of hot ends onto the carriage.


---
**Eric Lien** *September 22, 2016 23:28*

My only suggestion might be to move the extruder back to get the center of mass over the carriages more. But excited to see you finally building one up. I am interested to see how the ground rails weight affects performance.


---
**Ryan Jennings** *September 22, 2016 23:35*

So my titan actually has a pancake stepper on it and I already have edited that design to account for moving back about 6 mm the whole assembly on the cars, that should help level out the weight


---
**Ryan Jennings** *September 25, 2016 02:51*

I have the rough version cut and drilled to test 

![images/4e97b0a190302cd7166d3b47ae8406ea.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4e97b0a190302cd7166d3b47ae8406ea.jpeg)


---
**Eric Lien** *September 25, 2016 04:03*

**+Ryan Jennings** nice. I love the in line images in comments now on G+. Makes updates on existing conversations so much easier.


---
**Ryan Jennings** *September 25, 2016 04:06*

**+Eric Lien**  Yea it comes in handy, although to be honest i havnt used G+ in a long time other than this, irc just seems so  much easier


---
**Eric Lien** *September 25, 2016 04:42*

**+Ryan Jennings** yeah I know you guys love IRC. I like the idea of it. But I love the asynchronous nature of G+ threads. And how easy it is to collate my interests and people to follow.


---
**Peter Kikta** *September 28, 2016 16:40*

Aren't the rails supposet to be mounted to something? Do they have enough strengh and won't bend?


---
**Ryan Jennings** *September 28, 2016 16:43*

**+Peter Kikta** Normally the rails are mounted to something, but in this use case the short span of ~400mm I dont believe that will be a issue.  I got this idea from **+Jeff DeMaagd** and his large printer that uses 15mm rail with a 700mm unsupported span 


---
**Ryan Jennings** *September 28, 2016 16:44*

![images/44281aa0d7f92efb0ad7f9cfd8c63eb9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/44281aa0d7f92efb0ad7f9cfd8c63eb9.jpeg)


---
**Frank “Helmi” Helmschrott** *September 29, 2016 18:13*

Awesome approach. I'm so keen to see how this runs. There's basically not that much beating these rails :)


---
**Matthew Kelch** *January 23, 2017 19:25*

**+Ryan Jennings** Any updates on this? Very curious how the rails are working out for you.


---
**Ryan Jennings** *January 23, 2017 19:31*

**+Matthew Kelch**  Funny you should ask, I actually started a new set of carriages a couple hours ago to adress size issues so i should have those done tonight. I unfortunately do not have a aluminum bed or heater yet so am unable to print for a test. everything else is ready to go including the ballscrew Z upgrade


---
**Ryan Fiske** *April 03, 2017 02:34*

I know this is digging up an older post, but I just stumbled on this and am really interested to see how/if this worked well. I've got a set of 400mm MGN12 rails from my Kossel Mini that I'd love to incorporate into my next printer build. Any progress or findings?


---
*Imported from [Google+](https://plus.google.com/100961646236572561479/posts/Q9V7Cbn8T73) &mdash; content and formatting may not be reliable*
