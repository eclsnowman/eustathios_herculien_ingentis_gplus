---
layout: post
title: "Getting a lot of z-ribbing still. It perfectly lines up with my leadscrew so I'm assuming some sort of software vs mechanical problem?"
date: June 14, 2015 06:06
category: "Discussion"
author: Ben Delarre
---
Getting a lot of z-ribbing still. It perfectly lines up with my leadscrew so I'm assuming some sort of software vs mechanical problem?



This is printed at 0.1920 layer height which according to the prusa calculator should be an even number of steps for the eustathios.



I've checked and its definitely not a wobble since the rib expands and contracts across the piece rather than to one side or the other.



This is on a 16microstepping smoothieboard, with the 20 tooth pulley on the 1.8deg stepper and the 32 tooth on the leadscrews.



I plan to try a bunch of other layer heights tomorrow.



![images/26912ac1a0f42a7bef050d658520b044.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/26912ac1a0f42a7bef050d658520b044.jpeg)
![images/99cffe28d2ef8e6189e5c891ce9c7258.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/99cffe28d2ef8e6189e5c891ce9c7258.jpeg)
![images/d1bf6fbf4b934cdc5d20540a886fd571.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d1bf6fbf4b934cdc5d20540a886fd571.jpeg)
![images/824c1bcff68a8c0759e74313365c89a9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/824c1bcff68a8c0759e74313365c89a9.jpeg)
![images/679e8a3eb049a99fbb65e72ad26294f4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/679e8a3eb049a99fbb65e72ad26294f4.jpeg)
![images/d38ab974fc8f8e61ff420a6aa3482a7d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d38ab974fc8f8e61ff420a6aa3482a7d.jpeg)

**Ben Delarre**

---
---
**Brad Hopper** *June 14, 2015 11:28*

It's a nice effect on the teapot though.


---
**Eric Lien** *June 14, 2015 11:28*

Did you try that superlube on the leadscrew?


---
**Ben Delarre** *June 14, 2015 14:43*

Yep it's all lubed up ;-)


---
**Eric Lien** *June 14, 2015 16:26*

**+Ben Delarre** did you remember the shim washer between the bed pulley and the upper bearing on the lead screw mount assembly?



[https://drive.google.com/file/d/0B1rU7sHY9d8qUVl4RFZSZzR3aGM/view?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qUVl4RFZSZzR3aGM/view?usp=sharing)


---
**Ben Delarre** *June 14, 2015 16:35*

Yep double checked that one.



As I see it it could be one of two things.  My layer heights are not actually providing the heights I'm asking for hence the 0.1920 isn't the correct value and I get classic z ribbing due to rounding errors. Possibly due to over or under extrusion causing the layer height to be wrong? 



Or the hot end temp is fluctuating again and the filament is not extruding evenly (seems unlikely) 



Or something I haven't thought of yet. 


---
**Eric Lien** *June 14, 2015 16:38*

**+Ben Delarre** you seem to know where to be looking. And prints keep getting better. So you are moving the right direction :)


---
**Eric Lien** *June 14, 2015 16:44*

Another thought is microstepping on Z, I run lower microstepping on Z. Because ideally you should be an even number of non-microstepped heights for Z, so microstepping on Z isn't even really needed anyway. I think I am at 1/8 if I recall, maybe even 1/4.


---
**Ben Delarre** *June 14, 2015 16:45*

Yeah I was just wondering that. I know the smoothieboard has it fixed at 1/16 unless I cit or add a jumper can't remember which. I think I will try that first today. 2560 steps per mm on the z is a bit insane. 


---
**Erik Scott** *June 14, 2015 18:23*

I'm surprised you can't do an even .2mm layer height. Prusa calculator says that should be possible, even without microstepping. 


---
**Ben Delarre** *June 14, 2015 18:27*

The z axis is connected in the following way:



1.8deg stepper motor

20 tooth pulley (motor shaft)

32 tooth pulley (leadscrew)

2mm pitch leadscrew



As I understand the calculator I put these values in, note the 20:32 gear ratio, and you get 0.1920 as the closest layer height that is an even number of steps.



Besides, I just finished a test print at various layer heights (Simplify3d rocks btw), seems like that makes no difference at all.



However on this print the ribbing is at a much higher frequency, but the print is much smaller in diameter. So I'm thinking this indicates its definitely an over/under extrusion problem not a layer height one.


---
**Erik Scott** *June 14, 2015 18:31*

Just took another look at the calculator. You're right for 200 steps/mm steppers. I have a 400 step/mm motor, so that's what I put it. 



I get this issue as well, and I'm interested in how you fix it. 


---
**Ben Delarre** *June 14, 2015 18:36*

Ah yes that would give you more step resolution so the layer height rounding might disappear then.



I just tried printing at 20% over or under my current estep value (changing it on the fly with M92) but that also seemed to make no difference. Oddly enough I finished the print at Estep value of 589, which is a full 20% more than the recommended 491 esteps recommended by **+Martin Bondéus** for the bondtech v2 extruder. Even at 20% over the top layers were actually underfilled towards one corner of the print.



Now I can't believe the extruder is that much out of normal bounds, even with the long bowden tube on the Eustathios. And given that its more on one side of the bed than the other I'm wondering if I have a levelling problem.I was under the impression though that after the first few layers a levelling problem goes away, so is only really relevant for the first 5-10 layers.


---
**Walter Hsiao** *June 14, 2015 22:39*

Wouldn't you want to use 0.2mm (or 0.19375mm) layer height?  if you ignore the microstepping (2560/16) you get 160 full steps per mm so 32 steps should be 0.2mm (32/160).  Or am I missing something?  I built mine with a 16 tooth pulley on z (at the expense of z travel speed) just so I wouldn't have to do any math when choosing new layer heights ( should work out to 0.005mm/step on mine).


---
**Eric Lien** *June 14, 2015 23:06*

**+Walter Hsiao**​ he has 1.8deg steppers, not 0.9 like the ingentis and Jason's Eustathios. I go with robotdigg 60mm steppers in my BOMs. I have had great luck with them. 20 tooth on stepper, 32 on shaft, 1/16 microsteps. His math is correct.﻿


---
**Ben Delarre** *June 14, 2015 23:09*

Yep, turns out all along that it was me being an idiot.



I failed to tighten the belt between the x axis motor and the 32 tooth pulley. So there was considerable backlash in the x axis which caused all of my problems. Just posted a new benchy that has come out wonderfully!


---
**Jim Wilson** *June 14, 2015 23:16*

That's what I love the most about this kind of work, its all, always our fault!


---
**Ben Delarre** *June 14, 2015 23:16*

Haha very true!


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/1iLyxB6MsCp) &mdash; content and formatting may not be reliable*
