---
layout: post
title: "Hi folks, I am a student from Slovenia"
date: January 03, 2016 09:48
category: "Discussion"
author: Janez Klemenčič
---
Hi folks,

I am a student from Slovenia. A few weeks ago I started searching for

information how to build DIY 3D printer. I found Eustathios on youtube and

immediately knew that this is the right model for me. So l am asking you to

help with designing my printer.I have a problem with finding pullies with

inner diameter 10 mm. It would be also great if I could get the part which

holds hotend in .step file ,because I want to use wade`s extruder mounted directly on hotend. I will modify it myself using Solidworks. I want to use direct type because I heard that it is better for printing Ninjaflex and nylon. Did any of you use direct type on Eustathios? How did it go? I also can't decide which bed heater to buy: silicon or PCB.( I will buy aluminium plate 30x30cm.)

I think this will be all for now. (sorry for my bad english).







**Janez Klemenčič**

---
---
**Miguel Sánchez** *January 03, 2016 10:03*

One of the design's intents is to reduce moving mass, that buys you faster acceleration. By putting the extruder motor with the hotend you will increase the moving mass.



Adding more weight to the cross will increase the flex distance of the moving smooth rods. You want to make sure that is going to be negligible compared to the layer heights you are planing to use (what is worse is that this distance is not fixed all along the axis). Of course that is something you might compensate for in software ...



Printers like Zortrax do exactly what you propose and still it manages to get a superb output quality (if you are patient enough).


---
**Janez Klemenčič** *January 03, 2016 11:02*

What diameter of rods are you using for the cross?


---
**Miguel Sánchez** *January 03, 2016 11:13*

Mine is a scaled-down version so it won't help you. I am using hollow rods to reduce moving mass, 8mm OD.


---
**Janez Klemenčič** *January 03, 2016 11:25*

I have been thinking of buying 10mm rods so that the difference in height won´t be that big... where did you get the designs of parts?


---
**Janez Klemenčič** *January 03, 2016 11:30*

what kind of printing surface are you using?


---
**Miguel Sánchez** *January 03, 2016 11:30*

I designed my own parts [http://www.thingiverse.com/thing:513955](http://www.thingiverse.com/thing:513955)


---
**Janez Klemenčič** *January 03, 2016 11:44*

Do you have heated bed? Where did you buy 8mm GT2 pulleys?


---
**Miguel Sánchez** *January 03, 2016 11:45*

yes, ebay.


---
**Janez Klemenčič** *January 03, 2016 11:53*

which type of heated bed do you prefer silicon or PCB?


---
**Miguel Sánchez** *January 03, 2016 11:55*

Silicon heaters will be available in custom sizes, with PCB there are fewer choices unless you DIY. I used power resistors instead.


---
**Janez Klemenčič** *January 03, 2016 12:05*

Thank you Miguel


---
**Daniel F** *January 03, 2016 12:51*

10mm pulleys can be bought from robotdigg: [http://www.robotdigg.com/product/149/10mm+bore+32+teeth+gt2+pulley](http://www.robotdigg.com/product/149/10mm+bore+32+teeth+gt2+pulley)

Depending on the version of the eustathios you  want to build, you need to remove the collar and cut a 3mm thread into the pulley (if you want the motors in the base, v1). For the spider v2 you can use them as they are.




---
**Eric Lien** *January 03, 2016 14:40*

If you look at the BOM the vendors I used are listed (please note there are multiple tabs on the sheet). Also if people want to add additional notes and vendors please do.



[https://docs.google.com/spreadsheets/d/1ATz5AoIUtASowBtlXsOb8FVt8YLd0A-wu1EWHHHPDnA/edit?usp=docslist_api](https://docs.google.com/spreadsheets/d/1ATz5AoIUtASowBtlXsOb8FVt8YLd0A-wu1EWHHHPDnA/edit?usp=docslist_api)


---
**Janez Klemenčič** *January 03, 2016 16:32*

**+Eric Lien** thanks

**+Daniel F** thanks


---
*Imported from [Google+](https://plus.google.com/113433408952961174117/posts/a9aY4NJ6YAd) &mdash; content and formatting may not be reliable*
