---
layout: post
title: "A third Benchy print and the results I've tuned the extrusion multipliers etc (as noted by Eric Lien and I've upped my print temps on this one to 225(PLA)"
date: October 14, 2016 07:05
category: "Show and Tell"
author: jerryflyguy
---
A third Benchy print and the results




{% include youtubePlayer.html id=InQM0juCAfg %}
[https://youtu.be/InQM0juCAfg](https://youtu.be/InQM0juCAfg)



I've tuned the extrusion multipliers etc (as noted by **+Eric Lien** and I've upped my print temps on this one to 225(PLA). Not sure if that might not be a tiny bit high? My bridging still needs work. 4mm retractions have helped some of the blobs between various rapid start/stop points. Tiny bit of stringing but not too bad. Increased the # of top layers (5) which seems to have helped fill in the 'decking' a bit better vs my last print. Not sure what caused the defect at the underside of the bow?



Still more to do but .. I'm getting there.



Btw **+Eric Lien** I did post my FFF on my last posting, hope it works.





**jerryflyguy**

---
---
**Michaël Memeteau** *October 14, 2016 08:23*

You may still have a bit of whether under-extrusion or need to increase overlap between shell and infill. Looking good otherwise. What was the speed it was printed at?


---
**Eric Lien** *October 14, 2016 10:54*

Looking much better. Sorry I haven't gotten to look at the factory file yet. I will try to review it this weekend.


---
**jerryflyguy** *October 14, 2016 18:11*

**+Michaël Memeteau** I'm not in front of the PC but where do I adjust the overlap? It feels like I'm under extruding a bit (I'm using 84% of 1.75mm filament) .. dunno if that much is normal. It was printed at 60mm/s I believe?



**+Eric Lien** no panic on the FFF, I'd forgot to tag you on that post so wasn't sure if you'd seen it. 



Going to keep chipping away at the quality, and also start working on reliability as well.. not always consistent on first layer adhesion. Might be the homing speed? Not sure.


---
**Michaël Memeteau** *October 14, 2016 18:47*

**+jerryflyguy** Which slicer are you using?


---
**jerryflyguy** *October 14, 2016 19:35*

**+Michaël Memeteau** simplfy3d


---
**Eric Lien** *October 14, 2016 20:00*

**+jerryflyguy**​ if you are running that low of a multiplier on PLA, do an air extruder of 100mm of filament to double check your Esteps on the extruder. Mark filament back to a datum on the extruder body, measure, extrude 100mm, measure, take difference between first and second measurement (should be 100mm). If not adjust Esteps by %error between theoretical and actual, rerun air extrude until it comes in correct:)


---
**jerryflyguy** *October 14, 2016 21:59*

**+Eric Lien** I did it originally w/ out the extruder and then redid it last week while extruding 300mm of filament.. was w/in 1mm (less) of perfect (after a slight tweak) so I'm confident it's correct.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/YnAJ64SCj2u) &mdash; content and formatting may not be reliable*
