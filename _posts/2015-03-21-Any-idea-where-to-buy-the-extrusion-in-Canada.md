---
layout: post
title: "Any idea where to buy the extrusion in Canada"
date: March 21, 2015 15:51
category: "Show and Tell"
author: Sébastien Plante
---
Any idea where to buy the extrusion in Canada. I wish to avoid Misumi as they are in US (bad exchange right now) and they charged me 200$US  of shipping on my last 90$ order (and you don't know that until they ship).





**Sébastien Plante**

---
---
**Oliver Seiler** *March 21, 2015 18:28*

I got mine from Aliexpress. They cut to length if you ask them to and you can do the drilling and tapping yourself easily.


---
**Dave Hylands** *March 21, 2015 21:39*

I haven't purchased from them, but there is also [protodrake.ca](http://protodrake.ca)


---
**Dat Chu** *March 21, 2015 22:09*

Which vendor **+Oliver Seiler**​? 


---
**Sébastien Plante** *March 21, 2015 23:05*

Same question as **+Dat Chu**​, what vendor did you use **+Oliver Seiler**​ ? 


---
**Sébastien Plante** *March 21, 2015 23:06*

Thanks **+Dave Hylands**​, totally forgot about [protodrake.ca](http://protodrake.ca) ! 


---
**Oliver Seiler** *March 22, 2015 00:28*

**+Dat Chu** **+Sébastien Plante** just search for 2020 extrusion [http://www.aliexpress.com/w/wholesale-2020-extrusion.html?initiative_id=SB_20150321162726&site=glo&groupsort=1&SortType=price_asc&SearchText=2020+extrusion](http://www.aliexpress.com/w/wholesale-2020-extrusion.html?initiative_id=SB_20150321162726&site=glo&groupsort=1&SortType=price_asc&SearchText=2020+extrusion)


---
**Oliver Seiler** *March 22, 2015 00:29*

I bought from these guys [http://www.aliexpress.com/store/937165](http://www.aliexpress.com/store/937165)

Quality was good.


---
**Dat Chu** *March 22, 2015 01:15*

That link is full of win. Wow. 


---
**Sébastien Plante** *March 22, 2015 01:21*

AliExpress is full off good and bad things, it's nice to have some references :)


---
**Oliver Seiler** *March 22, 2015 03:01*

**+Sébastien Plante** I've got most of my parts from AliExpress with the exception of electronics. So far the only issue I had was 95 nuts in a package that should have contained 100. The vendor gave me a 5% refund, which worked fine for me as I had enough of them anyway.

Check the reviews. The vendors die for a good review - so if you receive anything that you're not happy with get in contact with them and normally they sort it out straight away.

The lead time can be a pain, but for me that's also true for orders from the US :(


---
**Dat Chu** *March 22, 2015 03:05*

I can't seem to find the 2020 dual corner bracket from that store. Do they only carry 2020 brackets? 


---
**Mike Thornbury** *March 22, 2015 10:22*

Comparison shop - especially the shipping. That link Oliver posted before would have cost $150 in shipping to me for 10 pieces, but for the same extrusion another vendor only charged $80. I always ask them to get a confirmed shipping price - I just got a shipping reduction from $150 to $50.


---
**Sébastien Plante** *March 22, 2015 12:58*

Nice to know! 


---
**Oliver Seiler** *March 22, 2015 20:20*

**+Mike Thornbury** you need to shop around - often there are multiple shipping options and as you pointed out it's well worth contacting the vendor.

Fast shipping tends to be a bit more expensive.


---
**Mike Thornbury** *March 24, 2015 00:26*

I don't have any choice. Non-fast shipping is often not offered to my location, and if it is, it takes a long time. I am still waiting for items shipped in January


---
**Oliver Seiler** *March 24, 2015 09:05*

**+Mike Thornbury**  Really? My extrusions turned up in NZ 2.5 weeks after I ordered and that was the slowest shipping option.


---
**Mike Thornbury** *March 24, 2015 23:30*

NZ isn't a third world country.... Yet.



But let Key have his head and you never know...


---
**Oliver Seiler** *March 25, 2015 00:02*

**+Mike Thornbury**​ we'll soon be printing sheep and cattle ;-)


---
**Mike Thornbury** *March 25, 2015 08:55*

Dunno about an ABS hangi... might leave a bit of a tang...


---
*Imported from [Google+](https://plus.google.com/+excessnet/posts/fez8Bv4Mkic) &mdash; content and formatting may not be reliable*
