---
layout: post
title: "Originally shared by Rick Sollie Got the frame finished but I think I made a mistake somewhere"
date: July 05, 2015 23:22
category: "Discussion"
author: Rick Sollie
---
<b>Originally shared by Rick Sollie</b>



Got the frame finished but I think I made a mistake somewhere. I placed the rods on the alignment stands and I get a gap between the bearing holder and the top piece of extrusion. What is the distance between the top of the machine and the hole that supports the upper level cross extrusion?



![images/ce713a0636a2a21d9370cd3766969e89.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ce713a0636a2a21d9370cd3766969e89.jpeg)
![images/5fc57d9630ed72dd978ceb15d3119e19.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5fc57d9630ed72dd978ceb15d3119e19.jpeg)

**Rick Sollie**

---
---
**Eric Lien** *July 05, 2015 23:37*

CC on the cross rods is 14mm on both HercuLien and Eustathios.


---
**Eric Lien** *July 06, 2015 00:01*

CC (or top/top) of the top horizontal extrusion to the lower horizontal extrusion is 85mm (as best I can tell, I am using edrawings viewer from my phone)﻿


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/EBHm8BAMBnU) &mdash; content and formatting may not be reliable*
