---
layout: post
title: "Does anyone have a good comparison for controller boards;"
date: August 05, 2015 19:01
category: "Discussion"
author: Bryan Weaver
---
Does anyone have a good comparison for controller boards;  Panucatt vs. Smoothieboard vs. RAMPS (those seem to be the main 3)?  I have just about everything mechanical good to go, just don't have a controller board picked out yet.  I'm anxious to start seeing some things move around.



My Folger Tech i3 runs on a RAMPS board, so I pretty familiar with that already.  I've been doing a little research on the Panucatt boards and Smoothieboard, I'm just not familiar enough with these boards to see why they're more than double the price of a RAMPS..





**Bryan Weaver**

---
---
**Erik Scott** *August 05, 2015 19:06*

I would consider the RAMBo over RAMPS. The RAMBo is rock solid and gives you a lot of options. RAMPS can be finicky and fragile from what I understand. 



I'm upgrading to an azteeg X5 mini for my new printer build. Can't comment on that yet, but my understanding is Smoothie is the future and much easier to configure. 


---
**Zane Baird** *August 05, 2015 19:31*

The Azteeg X5 Mini and Smoothieboard both run smoothieware firmware as they are based on the same microcontroller. RAMBo and RAMPS both use 8-bit microcontrollers which are just not as powerful as the 32-bit ARM cortex controller used by the Azteeg X5 and Smoothieboard. You might not even notice the difference until you crank up your printing/movement speed. The 8-bit controllers will start to stutter when encountering many high-speed segmented moves (slicers cut arcs into small segments rather than using true arcs in the g-code). 



I'm not certain on the hardware differences between the X5 and smoothieboard, but I have seen it mentioned in this group that some prefer the screw-terminal connections of the X5 over the smoothieboard


---
**Erik Scott** *August 05, 2015 19:38*

The segmented arcs are a result of the tesalated geometry of the STL file. There's not much the Slicer can do about it unless it implemented some really clever curve fitting and corner detection techniques. 



I like the screw terminals on my RAMBo, and I expect the X5 to work well too. I have heard the Smoothie board has some shoddy electronics from one of my friends, but I haven't heard anything else to support that claim, so take that with a grain of salt. 


---
**Eric Lien** *August 05, 2015 20:46*

I can run my Azteeg X3 on my HercuLien up to about 175mm/s before getting signs of stuttering. I really love the X3 because of all the IO, and how well it is built.



Above 175mm/s or with many small segments 32bit boards really shine. I like smoothieware on my Eustathios running the Azteeg X5 mini for ease of configuration, and the motion planning seems much better and motion is smoother. But smoothieware still has a few bugs from time to time, like no software max endstops (last time I checked) .


---
**Bryan Weaver** *August 06, 2015 01:04*

Thanks for the input guys.  I think I'm leaning toward the Azteeg X5 mini, I just don't foresee needing all the extra I/O of the X3.  Also, all things considered, the X5 mini comes out about $40 cheaper..


---
**Ricardo Rodrigues** *August 06, 2015 07:09*

The Rumba board seems interesting. 


---
**Eric Lien** *August 06, 2015 12:09*

**+Ricardo Rodrigues** I have seen many people have hardware failures with Rumba. I know someone who had 4 of them fail. So I might avoid the Rumba.


---
**Bryan Weaver** *August 07, 2015 13:47*

Ordered an X5 mini yesterday.   Should be here Monday.  Also have a small order of some miscellaneous stuff from SMW3D arriving Saturday and I should be everything I need in-hand.  Just have to put it all together and get things moving.



I have everything mechanical assembled.  Unfortunately, I think I'm going to have to pull the ganrty apart and reassemble it, I'm still getting quite a bit of resistance.  **+Eric Lien** , I've seen you say on here that when it is right, you will know.  I'm still unsure, so i'm guessing it's not quite right...  ;)


---
**Eric Lien** *August 07, 2015 14:13*

Take a little video of the issue, I will see what I can do to help.


---
**Bryan Weaver** *August 07, 2015 14:45*

I'm just pushing the carriage around with my hand for now.  I can feel some resistance, I just don't know yet if it's going to be too much for the steppers to handle.  I've just been eyeballing it and going by feel (and using calipers) to try to align everything.  I think my next move is to print the alignment tools to see if those help.  Once I get my controller and get my steppers moving i'll shoot some video.  I'm tied up this weekend, so I probably won't get to mess with it until next week.


---
**Eric Lien** *August 07, 2015 16:13*

I just wanted to confirm you have the side belts attached? If not it will never be able to run smooth.



The big trick is set the upper corner bearing mounts based on a fixed height from the top of the frame (this sets the assembly parallel to the top of the frame. Then run the carriage to as close as you can to each corner and allow the lower corner bearing mounts to settle where the assembly wants them. There is a lot of stacked tolerances here, and we all know prints aren't perfect.



So I would use the "Carriage to the Corners" method to set the rod spacing... because in my experience it yields the smoothest motion.


---
**Bryan Weaver** *August 07, 2015 17:01*

Yep, belts are attached.


---
*Imported from [Google+](https://plus.google.com/111820797809026464429/posts/1o672g7gEZt) &mdash; content and formatting may not be reliable*
