---
layout: post
title: "Finally \"installed\" some LEDs on the ."
date: December 21, 2014 23:55
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
Finally "installed" some LEDs on the #Eustathios.  By "installed", I mean I zip-tied some Ikea dioders on...but hey, at least I don't have to leave the light on in the room to check the prints via webcam now.  Printing some bottle-openers for a few friends, since that's what 3D-printers are made for.



![images/a0f89a038fb61ca93290bf999453faef.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a0f89a038fb61ca93290bf999453faef.jpeg)
![images/cb1f090589f212ba55104eadbbdd5855.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cb1f090589f212ba55104eadbbdd5855.jpeg)

**Jason Smith (Birds Of Paradise FPV)**

---
---
**Mike Miller** *December 22, 2014 00:14*

Yeah, I need light because my night vision is going to hell and an unlit build plate might as well be pitch black. 



What's your build plate made out of?


---
**Jason Smith (Birds Of Paradise FPV)** *December 22, 2014 00:29*

The build plate is glass over a silicone heat pad over a cork and foam layer. I plan on adding an aluminum heat spreader under the silicone at some point. 


---
**Riley Porter (ril3y)** *December 22, 2014 00:37*

Looking good man


---
**Eric Lien** *December 22, 2014 00:47*

I printed little housings that the LEDs slide into from the end and bolted to the frame. 



Looks great. I agree it helps with those webcam print checks late at night.


---
**Mark “MARKSE” Emery** *December 22, 2014 06:55*

Wouldn't you want to add the aluminium heat spreader above the pad?﻿ Love the lights, my local model shop does lights on a flexible sticky strip that you cut to length and solder power wires to. I was given a short sample but they're all dead. The LED spacing isn't as good as your IKEA strips though, will have to check them out.


---
**Jason Smith (Birds Of Paradise FPV)** *December 22, 2014 12:51*

**+Mark Emery**, You're right, the heat spreader should go above the heater. 


---
**Erik Scott** *December 23, 2014 04:01*

I'm looking to add a heated bed as I'm having some terrible warping issues with large, flat things. When you way a cork and foam pad, what do you mean? 2 layers? Or something else?



Also, how do you secure this to the existing acrylic bed? 


---
**Jason Smith (Birds Of Paradise FPV)** *December 23, 2014 04:12*

I used a sheet of both of these:

[https://www.amazon.com/gp/product/B00J8PMXEE/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B00J8PMXEE/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)

[https://www.amazon.com/gp/product/B001680USC/ref=oh_aui_search_detailpage?ie=UTF8&psc=1](https://www.amazon.com/gp/product/B001680USC/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)

I then used silicone to attach the glass bed to some aluminum c-channel I had laying around which I cut to extend beyond the edge of the glass. I used large binder clips from Walmart to attach the channel to the acrylic while sandwiching the foam, cork, and heater pad between the acrylic and glass. Not the prettiest job I've done, but it works. I will be adding an aluminum plate under the glass soon to dissipate the heat more evenly. 


---
**Erik Scott** *December 23, 2014 04:35*

Awesome! Thanks for that. How do you power it? I want to wire mine to my RAMBo, but it's not looking good. 15 amps doesn't seem to be enough, especially if I want to get it up to 120C for ABS. 


---
**Eric Lien** *December 23, 2014 05:07*

**+Erik Scott** I am now sold on 120V AC beds with SSR. Low current draw due to higher voltage means smaller wires. Plus you can run it with any controller since draw does not run through the board.


---
**Jason Smith (Birds Of Paradise FPV)** *December 24, 2014 22:30*

I'm powering my bed from the Rambo, but I'm running a 14a 24v power supply. I've only been printing pla so far, and the bed takes about 4 min to reach 70C. If you're looking to get to 120C, 120v is probably the way to go. 


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/SAUxpMZUkhD) &mdash; content and formatting may not be reliable*
