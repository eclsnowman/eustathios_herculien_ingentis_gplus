---
layout: post
title: "So I've finally got some time to start my Eustathios build and need to start putting all the parts together"
date: September 17, 2014 16:53
category: "Discussion"
author: Ben Delarre
---
So I've finally got some time to start my Eustathios build and need to start putting all the parts together. Couple of questions for those of you who have already built yours.



From the photos on YouMagine I can't quite see where the motors are all mounted, are they contained within the build chamber or are they all in the lower section? I only ask because I'd really like to fully enclose the build area and heat it up for good ABS printing and would rather my steppers don't have to live through that heat.



Also I've been going over the BOM, and it seems there are still some parts we have to order from RobotDigg, but their shipping is kind of horrendous last time I ordered from them. I think I have enough GT2 belt, and I have a whole load of these misumi LMU-N8 bearings ([http://us.misumi-ec.com/vona2/detail/110300026540/?HissuCode=LMU-N8&ProductCode=LMU-N8&Tab=wysiwyg_area_3](http://us.misumi-ec.com/vona2/detail/110300026540/?HissuCode=LMU-N8&ProductCode=LMU-N8&Tab=wysiwyg_area_3)) do you think these will work with the standard parts? And is there anywhere else I can get the 10mm bore 32 tooth gt2 pulleys?



Finally, is anyone out there printing these parts in ABS/Nylon? Would love to buy a set as I don't think I can get good enough quality ABS prints out of our shoddy i3 at work.





**Ben Delarre**

---
---
**Jean-Francois Couture** *September 17, 2014 19:10*

**+Ben Delarre** [robotdigg.com](http://robotdigg.com) for the 10mm pulleys.. and other great stuff.. parts are cheap (don't cost much) but are great. shipping is kind of pricey but you get the parts fast. :-)


---
**Ben Delarre** *September 17, 2014 19:21*

**+Jean-Francois Couture** yeah they are great but their shipping is way too high to just be ordering the pulleys in their own. I may look into getting the steppers too, anyone used their steppers?


---
**Jason Smith (Birds Of Paradise FPV)** *September 17, 2014 21:26*

As far as the motor locations, XYZ motors are all mounted in the lower section. The extruder motor is usually mounted on the exterior of the upper frame.


---
**Ben Delarre** *September 17, 2014 21:34*

**+Jason Smith** awesome, thanks for that. Glad to hear its easy to box up.


---
**Erik Scott** *September 18, 2014 03:22*

I got my 32 tooth 10mm bore pulleys on aliexpress, but I think they're the same ones as from robotdigg. You can buy all your pulleys from misumi, but its pretty expensive, and having the flange is nice when you go to square up your axies as the set screws are exposed. 



All steppers are in the lower section, except the extruder, the placement and design of which is left for the builder to decide. 


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/g6i2uCL58tw) &mdash; content and formatting may not be reliable*
