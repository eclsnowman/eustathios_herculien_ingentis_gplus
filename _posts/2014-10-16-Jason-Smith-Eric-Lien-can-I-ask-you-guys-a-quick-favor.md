---
layout: post
title: "Jason Smith Eric Lien can I ask you guys a quick favor?"
date: October 16, 2014 12:57
category: "Discussion"
author: Mike Miller
---
**+Jason Smith** **+Eric Lien** can I ask you guys a quick favor? You both have great carriage designs, but my printer's got 10mm cross-beams...The Herculien just has SLDPRT's published, and the Eustathios STL's are sized for smaller rods. 



I don't supposed one of you would be willing to upsize the openings for 10mm rods and 19mm linear bearings? My CADCAM skills are lacking. ;)





**Mike Miller**

---
---
**Eric Lien** *October 16, 2014 16:26*

Sure thing. I will work on it tomorrow. I am at work now, and I have a family member birthday tonight.


---
**Mike Miller** *October 16, 2014 16:28*

No worries. Bearings won't get here til...whenever the ship comes in. :)


---
**Eric Lien** *October 16, 2014 16:29*

Which bushing are you using? Still the IGUS? WHats the part#.



The nice thing is if you change to brass misalignment bushings on steel rods, then my carriage already works, because 8mm & 10mm ID have the same housing OD.


---
**Mike Miller** *October 16, 2014 16:29*

No worries. Bearings won't get here til...whenever the ship comes in. :)


---
**Mike Miller** *October 16, 2014 16:31*

Vxb.com lm10uu...there are no torsional loads, and I'm staying igus around the edges. 


---
**Eric Lien** *October 16, 2014 16:39*

**+Mike Miller** you may have to space the cross bars further appart on your frame to run lm10uu. The diameters might intersect.



IfvI can make a suggestion, use one lm10luu per axis on the carriage, not two lm10uu. That way there is no chance for bearing misalignment. You will be glad you did.



Also are you looking for a single or dual extruder carriage? And with which hot end?﻿


---
**Mike Miller** *October 16, 2014 16:43*

recalibrating shouldn't be too big a problem (considering everything would need looking at. :P ) and I was expecting just two linear bearings, press-fit at 19mm in diameter. 



The Igus bushings needed two, a reasonable distance apart, to keep the carriage tolerances acceptable. 



Single hot end for now, e3d (so, J-head compatible) bowden.


---
**Eric Lien** *October 16, 2014 17:47*

**+Mike Miller** the nice thing about an L bearing is you get the length of two bearings... Without the misalignment if your hole is imperfect or has warp.


---
**Eric Lien** *October 18, 2014 11:58*

Sorry I haven't got this done yet. Loads going on at work. Working this weekend. But I haven't forgotten about you.


---
**Mike Miller** *October 18, 2014 11:58*

No problem. This weekend I'm sleeping on the ground. #scouting


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/5RhVsRJ7ZQa) &mdash; content and formatting may not be reliable*
