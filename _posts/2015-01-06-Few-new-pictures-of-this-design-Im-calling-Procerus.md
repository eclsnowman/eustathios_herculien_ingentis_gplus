---
layout: post
title: "Few new pictures of this design I'm calling Procerus"
date: January 06, 2015 05:49
category: "Deviations from Norm"
author: Tim Rastall
---
Few new pictures of this design I'm calling Procerus.  All going well so far.  Initial movement tests are positive,  no issues with the linked nemas and it sure is torquey. Starting on the highly experimental and untried Z axis soon, gulp.



![images/a196c2b9c4e5ed5a3abb0f7300b62114.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a196c2b9c4e5ed5a3abb0f7300b62114.jpeg)
![images/07cc34b802921fe03a0292a045a8ece1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/07cc34b802921fe03a0292a045a8ece1.jpeg)
![images/bd052f80ee476ef59db5ecee9c85ae9f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bd052f80ee476ef59db5ecee9c85ae9f.jpeg)
![images/5f840f37b34b018d65a67d6da0041123.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5f840f37b34b018d65a67d6da0041123.jpeg)

**Tim Rastall**

---
---
**Wayne Friedt** *January 06, 2015 07:29*

Looking good.


---
**Mike Thornbury** *January 06, 2015 13:08*

What do you mean by 'linked nema's', Tim? 


---
**Dale Dunn** *January 06, 2015 13:19*

**+Mike Thornbury**  "Nema" has become a colloquialism in the 3D printing hobbyist community meaning "stepper motor". Since any kind of motor can be in a Nema frame, you might be thinking he has the motors combined in some kind of double housing, but that's not what he's up to. Every axis has two steppers working together, from opposite sides of the machine. They are linked only electrically, like the Prusa Z axis.


---
**Dale Dunn** *January 06, 2015 17:08*

Ah yes, I had forgotten Tim had a shaft linking the steppers on the opposite side.


---
**Stephanie A** *January 06, 2015 19:33*

Why not go full crazy and use 4 steppers per axis.


---
**Tim Rastall** *January 06, 2015 19:36*

**+Stephanie S** mainly because i don't have 8 steppers lying around. It would also be more than the drivers could handle and make it tricky to link the steppers as there wouldn't be a lay shaft on one end. 


---
**Dale Dunn** *January 06, 2015 20:24*

I'll jump back in, since Tim should be asleep this time of night. Microstepping doesn't really improve accuracy, since the system can still be 1/2 step out of position before skipping regardless of the commanded position. It can be out of position due to drag, or inertia effects, etc. So, under hard acceleration such as turning a corner, the 0.9° stepper will have half the theoretical overshoot and subsequent ringing of the 1.8°. So your machine's resolution is determined by whole steps. Microstepping really just helps with noise of various kinds.


---
**Dale Dunn** *January 06, 2015 21:11*

OK, I didn't read your post that way. Sorry.



Target position accuracy is probably up to the individual builder. For a very smooth finish or close fitting parts, someone may want as fine as 0.05 mm, or 0.3 for purely structural, rough parts. Somewhere in that range is where I would expect most people to fall. Come to think of it, that's not far from a typical range of layer heights, so maybe we can say they roughly correlate for most hobby machines.


---
**Dale Dunn** *January 06, 2015 21:33*

For me, I'm choosing smaller pulleys. This transforms the steppers' torque to higher forces for higher accelerations also. The trade-off is a loss of maximum speed (limited by controller's ability to send pulses and the drivers' ability to deliver current). But this is fine with me, because I can't extrude very well at 300 mm/s. Maybe with E3D's new Volcano hot end. I haven't run any numbers on that yet.


---
**Mike Thornbury** *January 06, 2015 22:59*

Thanks **+Dale Dunn**, I have a couple of dozen Nema 17 and 23 stepper motors, but I didn't see enough of Tim's design to understand what he meant by 'linked'.



The enemy of acceleration (and the friend of missed steps) is mass and to a lesser extent, stiction. We need to stop lugging around hardened steel rods and heavy pulleys and using better materials for bushings. If you are getting stretched belts over such a short distance, decrease weight and increase belt width -simples! My 20+ kilo gantry/spindle manages to not stretch the 5mm belts it is tugging on, I think the <2kg gantry on my printer isn't going to stress my 9mm belts as much.



The friend of increased stepper performance is available watts. Given the relative price of switch-mode power supplies, using 500w/40v would seem a good place to start, along with higher torque steppers. Of course this will add a noise and packaging dilemma. The highest-torque Nema 17 steppers I have seen are up in the 200-oz range -should be more than  enough, if you can reduce overall 'unsprung' weight. the other issues is with control boards - the all-in-one boards like the TinyG and the Smoothie have a limited amount of voltage they can take, which limits the amount of power you can get from your stepper, but again I come back to my CNC router... I am pushing much larger forces around than you can find in the largest of 3D printers - if I can do that with great accuracy and at high speed, then solving those problems for light printer mechanisms is definitely do-able.



The answer is always "Moar Power". What was the question? :)


---
**Dale Dunn** *January 07, 2015 00:13*

**+Mike Thornbury** , for a lower moving-mass design, have a look at the quadrap designs. They're related to Ingentis but make some different choices.



[https://plus.google.com/communities/103539262895202172380?utm_source=chrome_ntp_icon&utm_medium=chrome_app&utm_campaign=chrome](https://plus.google.com/communities/103539262895202172380?utm_source=chrome_ntp_icon&utm_medium=chrome_app&utm_campaign=chrome)


---
**Tim Rastall** *January 07, 2015 01:22*

**+Oliver Schönrock** don't buy RUMBA.  I have 3 and they are all fucked.  It may be worth considering stand alone stepper drivers if you want to push >24v.

Also, just to clarify,  I'm using 2 steppers per axis because I've got a bunch of nema17s lying around I'd like to use. 


---
**Mike Thornbury** *January 07, 2015 02:06*

**+Tim Rastall** that makes perfect sense :)



**+Oliver Schönrock** you can put up to 30V into the TinyG, if that's any help - but you are limited to 2.5A per winding with forced cooling using their built-in Ti DRV8825 drivers (although the DRV8825 itself can handle up to 45V).



**+Oliver Schönrock** The Electro-Craft RP17 was rated 200 oz-in or 141 Ncm, but it looks like they have changed their peak figures now to around 140oz. And of course torque is a factor of RPM - the higher the rpm, the less torque you have, so running a smaller sprocket may actually make things worse. 



**+Dale Dunn** Thanks, Shauki and I are well acquainted... but my total carriage weight, not including mount for the hot end or the hot end itself is around 150 gms... his is around 1kg. When I talk about light weight, I am not using metals at all.


---
**Mike Thornbury** *January 07, 2015 12:06*

Looks like it - the M and non-M designations refer to whether the rating is in Imperial or Metric... but as you can see on that page, the difference between continuous and red(?) is huge. I didn't realise how steep a ratio there was for torque.


---
**Mike Thornbury** *January 07, 2015 12:10*

Always is :) I was going to do something with my wife's luggage weighing meter and see what the difference between my 1.8, .9 and geared is, plus my larger Nema 23s.


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/jSHDnJro5Xp) &mdash; content and formatting may not be reliable*
