---
layout: post
title: "Heating pads came in yesturday. 12\" x 12\". 5 total"
date: March 22, 2014 01:54
category: "Show and Tell"
author: Wayne Friedt
---
Heating pads came in yesturday. 12" x 12". 5 total.



![images/497075091464904cb97ed4c5c47984ed.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/497075091464904cb97ed4c5c47984ed.jpeg)
![images/e20fe85736242b64de4962d9cc8167d0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e20fe85736242b64de4962d9cc8167d0.jpeg)

**Wayne Friedt**

---
---
**Chad Nuxoll** *March 22, 2014 01:55*

What are you doing with 5 of them 


---
**Nazar Cem** *March 22, 2014 01:58*

and how much did they cost?


---
**Chad Nuxoll** *March 22, 2014 02:01*

They cost 60 bucks each plus 12 bucks for shipping if they are the same ones I bought 


---
**Wayne Friedt** *March 22, 2014 02:04*

I think mine were cheeper than that but I will have to pull the invoice to make sure.


---
**Chad Nuxoll** *March 22, 2014 02:06*

Really where did you get them I just got mine in the mail today. Why did you get 5 you building 5 machines or a really large one haha.


---
**Nazar Cem** *March 22, 2014 02:09*

cool, I'd also like to know the source because the 24v ones from QUBD are $60 and are slightly smaller than 12"x12"


---
**Wayne Friedt** *March 22, 2014 02:10*

From Aliexpress. Yep 5 bots. I will post a link later when I get on a PC.


---
**Nazar Cem** *March 22, 2014 02:18*

wait... these ones are only $24. says 24v and 400 deg F. I guess they're lower quality?



[http://www.aliexpress.com/item/24V-300W-300-300MM-Silicone-Heating-pad-Heater-For-3D-Printer/1648562915.html](http://www.aliexpress.com/item/24V-300W-300-300MM-Silicone-Heating-pad-Heater-For-3D-Printer/1648562915.html)


---
**Chad Nuxoll** *March 22, 2014 02:20*

humm darn this is where I got mine



[http://store.quintessentialuniversalbuildingdevice.com/product.php?id_product=29](http://store.quintessentialuniversalbuildingdevice.com/product.php?id_product=29)


---
**Brian Bland** *March 22, 2014 02:34*

What wattage and voltage did you get?  I ordered a 110V 600W from Alirubber.  $48.90 with 3-5 day shipping via DHL.


---
**Wayne Friedt** *March 22, 2014 02:37*

170W 12v. 


---
**Eric Lien** *March 22, 2014 03:45*

Alirubber on alibaba makes the ones for QU-BD. The will make them to size and voltage you spec. My 13.5x13.5 24v 400w was $25 plus shipping.


---
**Wayne Friedt** *March 22, 2014 03:48*

Plus they will customize them as mine are with the 3 holes i needed for mounting. That,s where i got mine also Alirubber. 


---
**Martin Bleakley** *March 22, 2014 08:26*

Yep I got mine from alirubber as well, was $20 plus shipping


---
**Chad Nuxoll** *March 22, 2014 12:59*

Well darn I should have shopped around a little more than 


---
**Jarred Baines** *April 02, 2014 12:11*

**+Eric Lien** - How does your HBP perform? I would like to get one from Alirubber too if they are that cheap and good quality - is 400W working well for you?


---
**Wayne Friedt** *April 02, 2014 12:40*

I Have A 350w with 12v. Not the best choice. I have to run 2 ATX power supplies in parallel. Good part is it heats faster than the hotend. The big ones i bought are 170W 12V. I hope thats correct as there is a lot of coiled wires in there. Next time i am considering an AC powered one if not then a 24V could be good so a SSR should be needed. Keep everything on the board. Just adds more rats nest using a SSR or a MOSFET board like i am now. So yes they are cheep and good quality. they can be ordered with any length wiring you may need also. 


---
**Martin Bleakley** *April 02, 2014 19:35*

they made mine as a 500W beast and I have a 500W switching power supply powering it super fast to heat up :D


---
**Jarred Baines** *April 04, 2014 10:30*

What sort of costs guys?



And how did you spec these up? I don't exactly know what to ask for in a custom build plate.


---
**Brian Bland** *April 04, 2014 10:34*

Depends on how you are going to power it.  I got a 300mm×300mm 110v 600w with thermistor and adhesive.  $21.00 usd plus shipping.﻿  daisyhuang@alirubber.com.cn is who I dealt with.


---
**Wayne Friedt** *April 04, 2014 10:46*

Ya around $20 for me also. What is your power supply you plan to use then go from there, remember AC powered is also a good way to go but requires a SSR.


---
**Martin Bleakley** *April 04, 2014 19:40*

mine cost 40 delivered via dhl


---
**Jarred Baines** *April 06, 2014 00:11*

Ac powered is more efficient yes? Because no loss from ac/DC conversion? Maybe I'll go that route... So if I ask for a "300 x 300mm 240v 600w silicone reaprap heat pad" will that be enough info for them? And should be no problem wiring the bed in parallel with the 12v power supply for the other electronics?


---
**Wayne Friedt** *April 06, 2014 00:40*

AC  gives the advantage of not having to use a big power supply and keeping the heatbed current off of the controller board. A lot of the newer boards it doesn't matter but for some it can be a bit much. 600W should heat up in 1 minute or less maybe. Then you will be waiting on the hotend to heat up instead of visa-versa.  Determine the length of leads you will need then add another 50%. Cost is the same. Another option is weather you want it with adhesive already applied or not. If you dont really have another option of what to do to attach it i would get the adhesive already on it. There is a 3M paper covering it so it wont be sticking to everything until you peal it off . If you need holes in the heating pad make a detailed drawing and submit it before hand and get a conformation before the order. I had 3 holes put in my 1x1 pads for the bed leveling screws. You will also need a SSR. Get from Ebay or locally if you have such a store for that.


---
**Jarred Baines** *April 06, 2014 01:20*

Cheers **+Wayne Friedt** ;-)



And the rest of you of course ;-)


---
*Imported from [Google+](https://plus.google.com/+WayneFriedt/posts/XCMFncdAfjQ) &mdash; content and formatting may not be reliable*
