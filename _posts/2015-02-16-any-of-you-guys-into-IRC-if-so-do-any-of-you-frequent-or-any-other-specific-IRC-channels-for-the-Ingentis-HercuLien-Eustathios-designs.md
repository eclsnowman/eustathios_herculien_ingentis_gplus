---
layout: post
title: "any of you guys into IRC? if so do any of you frequent or any other specific IRC channels for the Ingentis/HercuLien/Eustathios designs?"
date: February 16, 2015 18:48
category: "Discussion"
author: Seth Messer
---
any of you guys into IRC? if so do any of you frequent  #reprap  or any other specific IRC channels for the Ingentis/HercuLien/Eustathios designs?





**Seth Messer**

---
---
**Dave Hylands** *February 16, 2015 19:02*

#reprap seems to have alot of traffic. Is there a channel for Ingentis/HercuLien/Eustathios ?


---
**Dat Chu** *February 16, 2015 19:04*

What is this ai arrrr see  you are speaking of? 😄


---
**Seth Messer** *February 16, 2015 19:05*

Nah,  anytime I've asked about these three designs, I'm generally left with virtual blank stares. The kthx bot in #reprap finally has a link to the eustathios youmagine page at least. 


---
**Seth Messer** *February 16, 2015 19:07*

I'm usually replicant on freenode#reprap in case y'all ever wanna just chit-chat. 


---
**Tim Rastall** *February 16, 2015 19:16*

Most of the guys on IRC are the original hard-core reprap folk.  They are generally very technically knowledgeable but also not very friendly and not very progressive in their thinking about new designs.


---
**Seth Messer** *February 16, 2015 19:19*

**+Tim Rastall** that's definitely the feels I've gotten anytime i chat about components and things in there. they are delta friendly though.. 


---
**Mike Thornbury** *February 16, 2015 21:35*

And here's me thinking IRC was the last refuge of child pornographers... I pretty much stopped using it when wildcat BBS came with an Internet module.



There has to be something better than G+ though... If it was designed to be this difficult to communicate, then they had the whole 'people speaking to people' thing very wrong.



I hate it, but it has gotten so much traction.


---
**Seth Messer** *February 16, 2015 21:59*

No way Mike! Many development projects use freenode specifically, to host an irc channel to provide support, etc. Much like anything on the internet, though, there are dark/bad areas. Anyway, irc is so handy for real time discussion of useful topics. I think you're thinking more of Usenet (newsgroups).


---
**Mike Thornbury** *February 17, 2015 03:42*

LOL - just messing with you Messer ;) 



Judging by your pic, you weren't on the planet when i got my first internet connection... I am quite familiar with IRC.



And yes, alt.* has a lot to answer for... 



But, more seriously, these days I prefer to talk while I type - so Skype, Messenger, etc. have become my go-to. Especially Skype, where you can look over the shoulder of the person you are talking to and have multi-way convo's, as well as use the type/chat facility.



And for hands-on of those who insist on using the Dark Side (windoze), I use TeamViewer.



The issue I have with IRC as the basis for a product/app sharing forum is the loss of the knowledge gained. And while I abhor this awful kludge that is G+, at least you can compartmentalise stuff and find it again later.


---
**Seth Messer** *February 17, 2015 03:56*

Looks can be deceiving, thanks for the compliment though! No offense taken. I agree though, I dislike google+.


---
**Mike Thornbury** *February 17, 2015 04:49*

LOL- you're a kid! :) (I look like Father Christmas's grumpy older brother...).Get off my lawn!


---
**Seth Messer** *February 17, 2015 04:51*

Lol. Cranky old geezer, don't start throwing aluminum extrusion off-cuts at me. 


---
**Mike Thornbury** *February 17, 2015 04:53*

I'll fire them out of my patented aluminium offcut machine pistol.



Distance is everything when protecting lawns.


---
**Seth Messer** *February 17, 2015 20:41*

ooh, perhaps we can do a subreddit? though since this community is one that is made up of 3 builds, not sure what/how we'd name the subreddit.


---
**Tim Rastall** *February 17, 2015 20:48*

There is no way this community will shift to reddit without fracturing. I'd prefer we created a wiki or similar if we need a better place to keep stuff or consolidate /aggregate. I'm not a fan of G+ particularly but it serves it's purpose  albeit in elegantly.


---
**Seth Messer** *February 17, 2015 20:49*

**+Tim Rastall** definitely was not my intent to break the community in any way. a dedicated wiki or central point of information would be nice.


---
**Seth Messer** *February 17, 2015 22:55*

actually **+Tim Rastall**, seeing as how you guys already have relevant links for the different builds in the community "sidebar", i think that really the big thing is, maybe having someone to periodically verify BOM is still valid for the builds, maybe present alternatives for things (e.g. smoothieboard, azteeg, etc) and in general handy other things like with the alirubber orders that **+Dat Chu** is working on, etc.


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/5rJYuNLtr8w) &mdash; content and formatting may not be reliable*
