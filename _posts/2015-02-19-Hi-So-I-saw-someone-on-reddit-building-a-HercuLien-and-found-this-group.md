---
layout: post
title: "Hi, So I saw someone on reddit building a HercuLien and found this group"
date: February 19, 2015 17:28
category: "Discussion"
author: Chris Brent
---
Hi,

 So I saw someone on reddit building a HercuLien and found this group. I have a couple of questions as I ponder taking on this as another project. 1) Is there a consensus on which variant to build? It's hard to really see the differences other than spectra vs drive belts. Maybe one has more documentation?

2) I currently have a Thing-O-Matic, can someone point me at the biggest part I'd need to print so I can see if that's even possible.

Thanks, and I'm in awe of the amazing builds here.

Chris





**Chris Brent**

---
---
**Erik Scott** *February 19, 2015 17:38*

The eustathios is probably the most popular of the three varients, but the hurculien seems to be taking off. The ingentis is basicly a spectra-line, beltless version on the X and Y axis and a belted Z axis. You can mix and match designs between the ingentis and eustathios, however, as they are really just variations on the same basic design. The hurculien is basically a larger, beefed up eustathios. 



I think you'll have some trouble printing the z-bed supports if you go with the ingentis or eustathios. If memory serves me correctly, I don't think the hurculien has anything that big despite its size. 



Best thing you can do is pull open the stl files in blender or whatever program you prefer and measure them. It'll also make you more familiar with the build. 


---
**Chris Brent** *February 19, 2015 17:43*

Thanks Erik, I started playing with the STL's. You're right the Z bed supports aren't going to fit on my build surface, but I might be able to find someone around here to print them.


---
**Jim Wilson** *February 19, 2015 20:39*

**+Chris Brent**​ Tell me what parts you're unable to print and I'll gladly print them up for you, my printer has some free time for the next few weeks. 



I'd charge you only the cost of plastic and shipping.﻿


---
*Imported from [Google+](https://plus.google.com/+ChrisBrent/posts/9L21uSvdZfE) &mdash; content and formatting may not be reliable*
