---
layout: post
title: "Made some adjustable feet that fit the 20 x 20 aluminum"
date: November 11, 2014 06:16
category: "Show and Tell"
author: Wayne Friedt
---
Made some adjustable feet that fit the 20 x 20 aluminum. 5 mm bolt.



![images/98981064ea2072882a9c9d62bc9e9f26.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/98981064ea2072882a9c9d62bc9e9f26.jpeg)
![images/36c91f3058c947370ff3f6d1bba64db7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/36c91f3058c947370ff3f6d1bba64db7.jpeg)
![images/ed7ad462371054f5abc3d331cbc6cda5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ed7ad462371054f5abc3d331cbc6cda5.jpeg)
![images/b504caad49076a3763422c7c568b567f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b504caad49076a3763422c7c568b567f.jpeg)

**Wayne Friedt**

---
---
**Mike Thornbury** *November 11, 2014 07:38*

Nice prints Wayne.What did you print them on?


---
**Wayne Friedt** *November 11, 2014 08:33*

Thanks. If you look at my profile. My main picture is the printer i used.

Plus these. [https://plus.google.com/u/0/+WayneFriedt/posts/921n62qQX1f?pid=6027545984279955922&oid=103640360813900380904](https://plus.google.com/u/0/+WayneFriedt/posts/921n62qQX1f?pid=6027545984279955922&oid=103640360813900380904)


---
*Imported from [Google+](https://plus.google.com/+WayneFriedt/posts/RyfueQauyeJ) &mdash; content and formatting may not be reliable*
