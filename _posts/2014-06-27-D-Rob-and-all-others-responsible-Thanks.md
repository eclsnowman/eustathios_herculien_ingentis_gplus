---
layout: post
title: "D Rob , and all others responsible, Thanks"
date: June 27, 2014 23:59
category: "Show and Tell"
author: Chet Wyatt
---
**+D Rob** , and all others responsible, Thanks. I ordered the 32 tooth 10mm  pulleys from robotdigg 5 days ago, and I have them already :)

![images/b04149b369d4c93127302eb7c6461156.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b04149b369d4c93127302eb7c6461156.jpeg)



**Chet Wyatt**

---
---
**ThantiK** *June 28, 2014 00:24*

It was thanks to him that they even started stocking them in the first place!  **+D Rob** thank you!


---
**Joe Spanier** *June 28, 2014 01:00*

Do they have 32 tooth 5 mm bores or are you using a different tooth count on the motor


---
**D Rob** *June 28, 2014 01:31*

It was obvious we needed them and I saw they made custom pulleys. We did it together. I didn't have a need big enough by myself. Couldn't have done it alone


---
**Eric Lien** *June 28, 2014 02:25*

**+Joe Spanier** they said they would make them when I asked... But I didn't need qty: 50﻿



Jason is using the standard 20 tooth on his with no speed issues. I went with 30 tooth from openbuilds. Turns out that was a poor decision due to non integer steps on z.


---
**Joe Spanier** *June 28, 2014 02:26*

What did you end up going with **+Eric Lien** I didn't see them in the bom


---
**Eric Lien** *June 28, 2014 02:39*

**+Joe Spanier** sorry I edited my first comment, must have been while you were typing. see above.


---
**Joe Spanier** *June 28, 2014 03:10*

Awesome. Thanks. Ive been putting together an order lisr


---
**Eric Lien** *June 28, 2014 03:22*

**+Joe Spanier** hope you try an arm based board. I really want to see this thing sing. Also I would go for higher torque 1.8 degree steppers than the wanati 0.9 I used. I see robodigg has some beefy ones I am interested in [http://www.robotdigg.com/product/29/Nema17-60mm-1.5A-high-torque-stepper-motor](http://www.robotdigg.com/product/29/Nema17-60mm-1.5A-high-torque-stepper-motor)


---
**Joe Spanier** *June 28, 2014 04:00*

Im planning on using the 2 amp  82oz/in motors from mass drop. For the controller I have an azteegX3 and a Cramps 2.1. Ones going on the Core build and ones going on this build. Just gotta decide which is which. 


---
**Eric Lien** *June 28, 2014 04:13*

Both are good. I love my azteeg. I am just running into throughput issues at warp speed on the 2560 in the Azteeg. Plus running 32 microsteps and 0.9deg motors doesn't help.



But to be honest in actual printing conditions I have more issues with my airtripper being able to keep up without shredding filament at high speeds than I do my controller.



One thing to check is the larger nemas might give you issues for the z stepper on the Eustathios. The motor runs shaft down and I dont think the bottom cover plate will fit. May need to make a cutout in that plate for clearance.


---
**Joe Spanier** *June 28, 2014 04:33*

The CRAMPS is 32bit with the beaglebone, so Im hoping to take advantage of some of that power. Im planning to run 1/32 for the microsteps as well, Im hoping that will help with noise. 


---
*Imported from [Google+](https://plus.google.com/107282537971480145759/posts/BULh6WtPTsv) &mdash; content and formatting may not be reliable*
