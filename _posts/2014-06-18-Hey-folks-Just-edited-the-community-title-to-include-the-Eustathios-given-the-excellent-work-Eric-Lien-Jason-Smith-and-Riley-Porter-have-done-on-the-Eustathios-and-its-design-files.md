---
layout: post
title: "Hey folks. Just edited the community title to include the Eustathios, given the excellent work Eric Lien , Jason Smith and Riley Porter have done on the Eustathios and it's design files"
date: June 18, 2014 10:51
category: "Discussion"
author: Tim Rastall
---
Hey folks. Just edited the community title to include the Eustathios, given the excellent work **+Eric Lien** , **+Jason Smith**  and **+Riley Porter** have done on the Eustathios and it's design files. All three promoted to owner status accordingly :)





**Tim Rastall**

---
---
**Tim Rastall** *June 18, 2014 10:53*

Whoops - Jason and Riley only moderators for now as I can't promote directly to owner.


---
**Eric Lien** *June 18, 2014 11:42*

Thanks Tim


---
**Jason Smith (Birds Of Paradise FPV)** *June 18, 2014 21:10*

Awesome. Thanks!


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/3qwRDYNZxRM) &mdash; content and formatting may not be reliable*
