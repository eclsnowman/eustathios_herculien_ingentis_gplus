---
layout: post
title: "Anyone have tips for pressing and aligning the self aligning bearings?"
date: March 19, 2015 14:23
category: "Discussion"
author: Joe Spanier
---
Anyone have tips for pressing and aligning the self aligning bearings? I'm having trouble getting them to slide nicely. Getting lots of drag. 





**Joe Spanier**

---
---
**Jason Smith (Birds Of Paradise FPV)** *March 19, 2015 14:29*

**+Joe Spanier**. I just re-shared some vids that **+Tim Rastall** made a while back that should help you out. 


---
**Miguel Sánchez** *March 19, 2015 14:30*

I have seen the smooth bar turned on a hand drill while moving the carriage up and down to help bearings to get properly aligned. 


---
**Erik Scott** *March 19, 2015 16:18*

Yup, put the 10mm rod in a drill and spin it while sliding the rider with the bearings up and down. Also, use liberal amounts of oil. 


---
**Eric Lien** *March 19, 2015 17:08*

If it binds you have misalignment. It slides like butter once aligned. Align on the rod from one side, then flip, align, flip, etc. Then put on a rod and spin with a drill while torquing the carriage to help it settle in. Once aligned right you will know.﻿


---
**Derek Schuetz** *March 19, 2015 18:25*

best luck i had with this was push the bushings in and then slide on smooth rod then place the rod perpendicular to a flat surface and <b>knock</b> the ends of the carriage into the table while riding the rail this will knock the bushings into alignment. you can then use a completed carriage and knock it into the ends of a binding carriage to knock the bushing into alignment. works really well.


---
*Imported from [Google+](https://plus.google.com/+JoeSpanier/posts/LV9RBkZxYGp) &mdash; content and formatting may not be reliable*
