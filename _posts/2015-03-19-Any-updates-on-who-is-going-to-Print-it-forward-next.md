---
layout: post
title: "Any updates on who is going to Print it forward next?"
date: March 19, 2015 16:58
category: "Discussion"
author: Gus Montoya
---
Any updates on who is going to Print it forward next? Having wishful thinking that someone will print my parts so it'll arrive by end of month. And I can start building away. 





**Gus Montoya**

---
---
**Seth Messer** *March 19, 2015 17:22*

so i'll go ahead and set expectations for you that i doubt you'll get prints done before the end of month. several folks are going to midwest reprap festival this month AND judging from my own experiences (i found a super helpful/nice dude on  #reprap   to print for me), it is NOT a quick process. especially if they run into technical or life-related difficulties (remember, they are doing us a favor!) 


---
**Godwin Bangera** *March 19, 2015 17:44*

hey Gus,

if you need any help printing parts, I can help you ... i am starting my prints for the Eustathios-Spider-V2 soon ... you can mail me and we can take it from there

thanks 

GSB


---
**Derek Schuetz** *March 19, 2015 17:51*

i can't promise end of month, i figure as soon as my belts show up ill be then just 1 day of calibrating, and then printing away


---
**Gus Montoya** *March 20, 2015 00:08*

@Seth I'm just keeping the ball rolling. I'm not expecting end of month either.


---
**Gus Montoya** *April 14, 2015 01:31*

Anyone have updates on this? I myself received the frame. Need printed parts to finish up and some wiring. Moving to another place before I can put time into this.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/ZEY4gvUgLS7) &mdash; content and formatting may not be reliable*
