---
layout: post
title: "The \"B\" pile. Those who know me, know how picky I am with parts I create for my personal use"
date: May 23, 2015 07:56
category: "Show and Tell"
author: Isaac Arciaga
---
The "B" pile. Those who know me, know how picky I am with parts I create for my personal use. They would also probably say these parts are perfectly fine. A mixture of PETG, PET+, ABS and CF ABS. I think enough to make 4-5 Eustathios V2 gantries. Once I have some spare time to sort through it all I'll donate to the Print it Forward campaign.





**Isaac Arciaga**

---
---
**Gus Montoya** *May 23, 2015 08:14*

Can I request only the parts that have the bronze bushings? My parts are cracking and delaminating horizontally along the print. I don't think glue will help unless I dep the entire part in glue.


---
**Isaac Arciaga** *May 23, 2015 08:19*

**+Gus Montoya** You cracked all the carriages? Or just the center carriage that has the hotend? That's the only carriage that prints 2 holes horizontally.


---
**Seth Messer** *May 23, 2015 12:08*

I'd guess he's referring to that one as I managed to crack mine too. :/ Going to let it ride for now though.


---
**Eric Lien** *May 23, 2015 14:00*

I made it a tight fit so that all bushings would be acetone welded in. Should I make a loose fit version that the bushings are meant to be glued in? I made it tight because I had two bushings come out on my first version. Plus peoples printers all vary on inside holes depending on how well it is tuned.


---
**Gus Montoya** *May 23, 2015 14:40*

I cracked the hot end center carriage and also the 4 which slide on the on the linear rails which are also delaminating. 


---
**Seth Messer** *May 23, 2015 14:42*

**+Eric Lien** nah, i think it's fine with a tight fit. i clearly didn't put enough acetone to melt it down. i only used a brush, but next time i'd use a syringe and a small foam brush. i was able to salvage everything on this particular one, some acetone to brush over some abs and then some CA glue took care of it.


---
**Eric Lien** *May 23, 2015 14:57*

**+Seth Messer** a qtip is what I found works best.


---
**Seth Messer** *May 23, 2015 14:59*

that's hilarious. literally need to put some qtips in the garage. <b>sigh</b> of all my tools here in the shop, the simplest must versatile applicator i didn't even think of. oh well once i get it built, first thing i'll reprint is a new carriage and rebuild that.


---
**Mike Miller** *May 23, 2015 18:26*

I generally up the perimeters, then set the final dimension with a drill, if necessary. 


---
**Eirikur Sigbjörnsson** *May 23, 2015 23:27*

Would acetone work on PETG printed parts?


---
**Isaac Arciaga** *May 23, 2015 23:40*

**+Eirikur Sigbjörnsson** not for smoothing. But the part will soften a little then harden back up after the acetone evaporates.


---
**Eirikur Sigbjörnsson** *May 24, 2015 03:18*

**+Isaac Arciaga**  I meant for welding the bushings in.


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/hxbWZJcgSRg) &mdash; content and formatting may not be reliable*
