---
layout: post
title: "eu (edit: eu? I don't remember typing eu...) Originally shared by Mike Miller Here we go!"
date: September 10, 2015 15:56
category: "Show and Tell"
author: Mike Miller
---
eu

 (edit: eu? I don't remember typing eu...)



<b>Originally shared by Mike Miller</b>



Here we go! THIS is  #Ingentilire  It's my attempt at a 3.0 printer (being the third one I've built) and it's got lots of little details that don't come through. Case in point: The frame is held together using button head screws, as are the bearing supports. Why? Because they're clean, can be installed blind, are cheaply available, and rely on the cuts of the extrusions to provide perpendicularity. Order them cut to length and tapped from Misumi and it helps an end user without heavy tooling build a rigid and square printer. 



It reuses a lot of identical printed parts. Why? Because once you get one part dialed in (e.g. bearing supports), you make 7 more just like it. Why are X and Y axes seperate? Because they let you play with any size carriage you want, plus it's easier to set a datum in one corner, get the carriage moving smoothly, then set the bed equidistant from the carriage. 



Like I said...lots of thought for lots of reasons. Looks like it turned out okay. (Github location to come shortly)





**Mike Miller**

---
---
**Michaël Memeteau** *September 10, 2015 16:07*

Like Jessie would says "Magnets, bitch!"...

Now you need to tell what the exact diameter of this hobbed wheel of yours. This great printer deserve a ring (unless you intend to adopt the ceramic drill and the solid flint). Anyhow good job (and I really like the magnets).


---
**Eric Lien** *September 10, 2015 16:08*

Well done **+Mike Miller**​. Nice to see it done, or as done as printers get before we upgrade them again ;)


---
**Ricardo de Sena** *September 10, 2015 16:13*

Great job!!! Pretty much the printer, the extruder like him will put his ideas in my extruder for better traction filament. Hugs.


---
**Mike Miller** *September 10, 2015 16:43*

I'll see if I can't find the exact gear...what you see is actually a Printrbot gear, used to hold MY gear in place. :D


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/CqQwooTKt54) &mdash; content and formatting may not be reliable*
