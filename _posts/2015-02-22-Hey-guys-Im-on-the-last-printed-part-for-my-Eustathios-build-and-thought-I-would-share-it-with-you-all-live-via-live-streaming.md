---
layout: post
title: "Hey guys, I'm on the last printed part for my Eustathios build and thought I would share it with you all live via live streaming"
date: February 22, 2015 20:40
category: "Show and Tell"
author: Isaac Arciaga
---
Hey guys, I'm on the last printed part for my Eustathios build and thought I would share it with you all live via live streaming. It's **+Eric Lien** 's fixed Z-Bed Support for his V4 carriage that I slightly modified to accept an Igus lead screw nut. 4 hour'ish print time using PET+ on my MendelMax 3 beta machine.



[http://www.streamup.com/isaax3d](http://www.streamup.com/isaax3d)



Stepping out for an hour. Hopefully I don't come back to a fail ;)





**Isaac Arciaga**

---
---
**Isaac Arciaga** *February 22, 2015 22:05*

Ack, issue with the model I didn't see. Had to stop the print. Will resume later this evening.


---
**Isaac Arciaga** *February 23, 2015 03:41*

Stream back up. Take 2!


---
**Isaac Arciaga** *February 23, 2015 05:50*

**+Alex Lee** they can be.


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/RGq3Jn5q3XM) &mdash; content and formatting may not be reliable*
