---
layout: post
title: "Playing around with Cura 15.06 today and printing replacement parts for my Herculien"
date: September 12, 2015 20:37
category: "Discussion"
author: Zane Baird
---
Playing around with Cura 15.06 today and printing replacement parts for my Herculien. So far I'm very happy with this implementation of Cura. Has anyone else tried Cura 15.06? It seems that most here are using Simplify3D. Perhaps once I get beyond my grad student salary I will invest in S3D as most seem to be very happy with it.

![images/44072203146440a95e89bec3a3139d7d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/44072203146440a95e89bec3a3139d7d.jpeg)



**Zane Baird**

---
---
**Eric Lien** *September 12, 2015 20:44*

I haven't printed using Cura in a while. I should try it again to see what's changed.


---
**Eric Lien** *September 12, 2015 20:45*

BTW what's the blue tape for on the carriage?


---
**Zane Baird** *September 12, 2015 20:56*

**+Eric Lien** Ha... The blue tape is due to that bushing sliding out of the carriage at one point. It's printed in PETG so I couldn't fix it in place with the acetone push-fit method like ABS. Eventually I plan on replacing the carriage, but I've been avoiding until I really feel like going through the alignment and break-in again. I considered fixing it in place with some loctite, but I'd really like to reuse that bushing in the next carriage. I haven't had problems with any of the other bushings in the carriage, so I don't know what the deal with that one was


---
**Ian Hoff** *September 12, 2015 23:46*

s3d and smoothie have some compatibility issues some days, there's a whole discussion in the Smoothie channel pretty commonly about how s3d does very strange nonsensical gcodes things. 

http://smoothieware.org/forum/t-1274968/firmware-unresponsive-again



In #reprap, people tend to use slic3r which is currently a little buggy, and in a fragile state. 1.1.7 does some thing better, and 1.2.1 is implementing a rather fascinating autospeed feature 



 It is a power user tool, it lets you manually do stupid things like layer heights well past nozzle diameter.

https://github.com/alexrj/Slic3r





or cura 15.04.02 cause 15.06 didn't come with non ulitimaker profiles.



Many of the reprappers are moving over to cura I suspect that might swing back toward slic3r if the programmer comes back to work on it, his real life gets in the way.



Or KISS for single extruder machines doing art rather than machine parts 

 https://groups.google.com/forum/#!topic/kisslicer-refugee-camp/avvwS8Tk5Yk%5B1-25-false%5D


---
**Zane Baird** *September 13, 2015 01:01*

**+Ian Hoff** I am a big proponent of KISS. Lack of recent updates has made me move in other directions. I will have to update myself on the S3D issues with smotthie as this is the first I've heard of them. I've been using Slic3r up until this point. However, with similar settings I have not been able to achieve the print quality I desire. The main thing that drew me away from Cura in the past was the limited ability to change advanced settings which seems to have been relegated in the recent changes to Cura. Overall, I was very pleased with the slicing profiles produced by Cura, but the inability to change settings to my desire kept  me from embracing it. If KISS had a more dedicated development team, I think that would easily be my main slicer (even though for many projects it still holds that place)


---
**Gústav K Gústavsson** *September 13, 2015 12:20*

Had a quick look at Cura 15.06 on Ultimaker 2 at work. 



(Updated the firmware on Ultimaker which seems to have fixed double retraction problem on cancelled print. However got another nasty problem. When starting a new print the machine does not any more start the print by homing the head. So if the head is not at endstop when starting print the head hits the front or right side of the machine and grinds the belts.) 



Tried Cura with the simple interface and everything seems fine. Switched to advanced and turned on every feature, there seems to be an impressive amount of things to fiddle with. Left most at default, did a quick test. Speed to high so cancelled and adjusted. Tried again and outer layer came out fine but infill speed was way to high and ruined. Cancelled and lowered infill speed. Didn't seem to change anything. Tried again with infill speed set to a crawl. Didn't change anything, infill speed still some super speed. Didn't have time to investigate further, switched to Cura 15.04 to do my print, worked as expected. You can have both versions installed at the same time. Will have a look again when I got the time.


---
**Gústav K Gústavsson** *September 13, 2015 14:32*

Oh other thing with Cura 15.06

Printing with USB on Cura in the past has been problematic haven't tried for a while. Decided to try. When I hit the print button it started to print. Immediately without heating the bed or nozzle! Cancelled and started again by manually heating the bed and nozzle on the printer before hitting print with USB. The head hit the side and the glass on the bed hit the nozzle hard, forgot to manually 'home' the bed and head!!!. EMERGENCY SHUTDOWN!

Haven't tried now but having the USB connected from your computer to a Ultimaker 2 even though you are printing from a SD card can ruin your print. Have a couple of time been printing from SD card and working on my computer. Switching between apps and accidentally switch to open Cura session and the printer immediately shuts down, turns on and reboots. Another ruined print :-(


---
**Zane Baird** *September 13, 2015 15:15*

**+Gústav K Gústavsson**  I have my own g-code that runs at the start of the print to home, heat the nozzle and bed, and prime the extruder. I also had to make my own custom machine profile (found in Cura/resources/settings) to fit the build area of the Herculien. I haven't tried printing via USB through Cura for a few versions. I send the g-code to the printer via Repetier host. I still need to tune the printing profile to get the best results and optimize settings, but I was happy with the first few results.


---
**Gústav K Gústavsson** *September 13, 2015 15:31*

Yes **+Zane Baird** I know you can add your own g-code start script in Cura to do the initialization. Just seems strange that it isn't automatic for Ultimaker 2 in Cura. Correct me if I'm wrong, Cura is a Ultimaker project, right? But all the same this version doesn't reach beta status in my option, don't get me wrong, I like the interface and the options they are implementing. They aren't just quite there yet, they will certainly scare away newbies to Cura/3d printing with this release.


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/hCuJFx6YsLx) &mdash; content and formatting may not be reliable*
