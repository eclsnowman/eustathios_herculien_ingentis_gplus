---
layout: post
title: "HercuLien first test prints. Calibration is needed..."
date: August 24, 2014 23:43
category: "Deviations from Norm"
author: Eric Lien
---
HercuLien first test prints. Calibration is needed... But a good start.



![images/af45314141f166b799c3936683d074af.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/af45314141f166b799c3936683d074af.jpeg)
![images/8d9b449a19c38a1cfa10ab08f8ddc1cc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8d9b449a19c38a1cfa10ab08f8ddc1cc.jpeg)
![images/b368f161cfe1e12e265e04643dd10100.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b368f161cfe1e12e265e04643dd10100.jpeg)
![images/cd0963b0a7ba11aa96132b3ea32f8dd4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cd0963b0a7ba11aa96132b3ea32f8dd4.jpeg)
![images/3d4b39c484c121d640b73dd4621244b3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3d4b39c484c121d640b73dd4621244b3.jpeg)
![images/957b56fb8ab2a4e7389de9334d06339c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/957b56fb8ab2a4e7389de9334d06339c.jpeg)
![images/1832d1d6302e91a08b0d7bf338c6c669.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1832d1d6302e91a08b0d7bf338c6c669.jpeg)
![images/5ee6611256a9b50107a5af5b4015edde.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5ee6611256a9b50107a5af5b4015edde.jpeg)
![images/05f64b283ea33660384f56051baec6c8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/05f64b283ea33660384f56051baec6c8.jpeg)
![images/4f287a1196887dea3b1b58297f75c096.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4f287a1196887dea3b1b58297f75c096.jpeg)
![images/1dd4664d2a1b01b3db229b6491a1c767.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1dd4664d2a1b01b3db229b6491a1c767.jpeg)

**Video content missing for image https://lh6.googleusercontent.com/-LJMF3j2B4VA/U_p4lesqb5I/AAAAAAAAi5M/d5QMOV2rMcI/s0/VID_20140824_172143.mp4.gif**
![images/6590b260bbedbea98083cdffbc4f236e.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6590b260bbedbea98083cdffbc4f236e.gif)

**Video content missing for image https://lh5.googleusercontent.com/-Tkst3oksNAA/U_p4lYsBapI/AAAAAAAAi5M/mk9kqU1742U/s0/VID_20140824_170111.mp4.gif**
![images/391096d8b3fdb921c7f016bc2eea19ee.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/391096d8b3fdb921c7f016bc2eea19ee.gif)

**Video content missing for image https://lh3.googleusercontent.com/-bzeb5qV1LKc/U_p4lX4_FGI/AAAAAAAAi60/nrwiiGm9kuw/s0/VID_20140824_134549.mp4.gif**
![images/365498c7f450c9dcc5027c6ed0103de6.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/365498c7f450c9dcc5027c6ed0103de6.gif)

**Eric Lien**

---
---
**Mike Miller** *August 24, 2014 23:44*

Damn sight better than my delta!


---
**Brandon Satterfield** *August 24, 2014 23:51*

That is great start Eric!!!


---
**James Rivera** *August 25, 2014 00:43*

Nice job **+Eric Lien**! That's <b>before</b> calibration? Those are some really nice looking bridges!  It'll probably look like injection molding once you have it dialed in!  Is that PLA or ABS?  What slicing and host software did you use?


---
**Eric Lien** *August 25, 2014 00:55*

**+James Rivera** I use simplify3d as the slicer and octoprint as the host.



I guess it is disingenuous to say uncalibrated. Just that these are the first prints to determine the speeds, feeds, and temps the hardware likes. All the steps/mm etc are calibrated from the pulley and lead screw math. I tend not to fudge that stuff to make things look better. If those are off something in the math is wrong.



But on my eustathios it took weeks to determine optimal slicer settings the machine likes. ﻿



Update: pla


---
**Eric Lien** *August 25, 2014 02:12*

Ahhhh the pla jams are back. I love e3d... But I have always fought pla on the e3dv5 on my last printer, and now this one. My v6 has been a dream. Just a cold pull with nylon when switching materials.



I may look into jheads for this build. I plan on printings some larger parts for work on this one. I really cannot afford to jam on what might be a multi day print. 



:(


---
**Mike Miller** *August 25, 2014 02:26*

So it's not just me then? 



This weekend's lesson was ordering heated rubber pads from alirubber...letting the delta and the IGentUS both print abs. 


---
**Eric Lien** *August 25, 2014 02:52*

**+Mike Miller** I like abs soooo much more than pla. But when I try and print 12" cube parts for work abs warp can be a real problem.


---
**Jean-Francois Couture** *August 25, 2014 11:49*

**+Eric Lien** What speeds was your machine dialed in ? that looks fast.. also, looks like the second head is higher. you have a drop mechanism on it ?



The prints really looks good ! :-) 


---
**Eric Lien** *August 25, 2014 13:58*

**+Jean-Francois Couture** I have it raised when not in use. I will drop it when printing dual color. The hot end is pinched in place by the three bolts on the front holdind the front mount/fan shroud.﻿



Speed was around 60mm/s but acceleration is 10,000 :)


---
**Jean-Francois Couture** *August 25, 2014 14:59*

**+Eric Lien** Very cool, I've got it down to 3000 for acceleration.. I have to try it faster.. just for the fun of it ! BTW, I like the way you tension the Z-axis belt.. look simple and fast to set. :-)


---
**Brandon Satterfield** *August 25, 2014 15:32*

**+Eric Lien** I am using PLA for paid prints as the material is readily available and cheap. In addition, the use of the heated bed (needed for ABS) running 24 hours around the clock, adds up quickly on to the bottom line. Some prints are 36 hours. 



I have been fighting my hot end, running both the V5 and V6, concerning jams. The V6 is a LARGE improvement, but I have been looking at going back to my Budda or purchasing a J-head. Who have you looked at purchasing from? 


---
**Jean-Francois Couture** *August 25, 2014 15:40*

**+Brandon Satterfield** **+Eric Lien** I got my self some new j-head a few weeks back and what I can say is #1, get them at the source [hotends.com](http://hotends.com) and #2, they don't have a push to connect tap for bowden tube setup (I screw the fitting on m y custom carriage. Just pointers if you guys are thinking of going the j-head way :-)


---
**Brandon Satterfield** *August 25, 2014 16:18*

**+Jean-Francois Couture** thanks for the advice I will search this out tonight. 


---
**Øystein Krog** *August 25, 2014 16:30*

Wow, acceleration at 10000... that is a lot. Does anyone know how high it's reasonable to go on the x-axis for a bowden prusa i3?

I just need some sense of scale here, I really can't go above 2000 on my current Prusa i3 (not bowden).

At this point I'm really quite sure I will build a machine like this:)


---
**James Rivera** *August 25, 2014 18:03*

I haven't really tested this, but my gut says high acceleration (say, beyond 300) has negligible benefits and will exaggerate any otherwise minor errors caused by hysteresis in the filament feed (aside: is there any setting in any slicing software to account for bowden tube length, or even hot end nozzle distance from extruder pushing/pulling it in?).



On the other hand, I think velocity will, of course, speed up prints, but that is also where you may easily see problems magnified if your machine's rigidity (or other important tolerance factors) are not up to the task. I had huge problems until I slowed my first machine waaay down (aside2: but that may attributable to slop in my 'bot that I've been unwilling to bother addressing...yet).



Again, I have no data to support this claim--just instinct/intuition based on my experiences/anecdotal evidence--so feel free to disagree.


---
**Eric Lien** *August 25, 2014 18:29*

**+James Rivera**  if your bots rigidity and motor torques are up to the task then acceleration will have a much greater impact on total print time than velocity. During a print you spend very little time going at top speed in one direction. How long it takes you to get there is a bigger component of the time. Just like how a drag race car would suck at an autocross event.



When you need to turn down acceleration (or jerk) is when you get ringing at corner changes or fast moves. Also if you begin to miss steps or hit resonance frequency of the frame.


---
**Øystein Krog** *August 25, 2014 19:33*

After looking at some of these prints it looks like there are some extrusion artifacts, maybe related to the bowden feed?

This got me thinking about bowden feed in general..

I have yet to try anything but "direct extrusion" but it seems like even then  retraction and extrusion pressure can make a big difference.

In general I am a bit worried that the added inaccuracies from a bowden feed will be the weak point in this kind of machine.

What does everyone think about bowden vs direct?

Is it is possible to get "perfect" quality with bowden feed?

Are you guys using 1.75 or 3mm filament?


---
**Jean-Francois Couture** *August 25, 2014 20:02*

**+Øystein Krog** thats a topic onto its self ! ;-)



Personally, I have both at home, and they both work great.. On a bowden system, if its tuned right, it can do very good prints (in my case, my bowden outruns my direct drive printer) but, its another story when printing with flexible meterials. 


---
**James Rivera** *August 25, 2014 20:23*

**+Eric Lien**  nice explanatory metaphor. Easy for a Corvette driver like me to wrap his head around.  :-D



After tightening everything I could think of, I was still having some terrible print problems at speeds of around 60-90 (and maybe 150 on non-print?) and I think I had my acceleration around 9000 (Slic3r popup tips implied a well-tuned 'bot could handle this). So, I brought <b>everything</b> down to 30 for speed, and only 300 for acceleration to see if that would help, and the results were dramatically better.



But, perhaps I've been too conservative with my settings.  In hindsight, my original fast settings now seem ridiculous for my old Printrbot LC+, and perhaps my slow settings were equally off the mark.  I think on my next print I will try something more balanced.



I don't really want to bother spending the time futzing around with tweaking my settings until they start to fail (as proper scientific method would dictate, but my available time to devote to this is limited), so I think I'll try something like Lulzbot's PLA/Fine speed and acceleration settings for Slic3r and just see if my printer is up to it.



Their Slic3r acceleration settings for their PLA/Fine config are:



Perimeters: 700

Infill: 1100

Bridge: 600

First layer: 900

Default: 800



[https://www.lulzbot.com/support/taz-slic3r-profiles](https://www.lulzbot.com/support/taz-slic3r-profiles)


---
**James Rivera** *August 25, 2014 20:29*

**+Øystein Krog** I did not mean to cast aspersions on bowden tube systems. Ultimaker is famous for having fast, high quality prints, and both v1 and v2 use bowden tube extruders.



That being said, I find it odd that no slicing engines take the bowden tube length into account. But perhaps such a thing would be very hard to calculate and/or have negligible benefit (or maybe even cause more problems than it solves).



EDIT: because the main problem with bowden tube extruders is hysteresis (basically, lag), but with the main benefit being a lesser-mass carriage that can go faster it can probably run without any retraction at all (which may quasi-negate the hysteresis problem) and still yield good results.


---
**Øystein Krog** *August 25, 2014 20:33*

**+James Rivera** You have a point with ultimaker.

Actually there is a guy (edit; Illuis) that has made and is still working on a version/fork of slic3r that <i>does</i> take bowden tube lag into account. It's a little bit similar to the old advance algorithm (that never seems to have worked for anyone), except this seems to help a lot with prints.

I can probably dig up the link, it can be found in the slic3r github repo pull requests/issues.

Edit:

[https://github.com/alexrj/Slic3r/issues/1677](https://github.com/alexrj/Slic3r/issues/1677)

[https://github.com/alexrj/Slic3r/pull/2018](https://github.com/alexrj/Slic3r/pull/2018)

His images shows a marked improvement (for his setup).


---
**James Rivera** *August 26, 2014 01:38*

**+Øystein Krog** nice find! I hope they can get it integrated into Slic3r (and Marlin). I may have missed it, but I did not see anything regarding the length of the bowden tube. I could be wrong, but I thought that was the source of the hysteresis (i.e. - increased bowden tube length == increased hysteresis).



BTW, my apologies **+Eric Lien** for highjacking your post--I feel like we've gone way off-topic here.


---
**Eric Lien** *August 26, 2014 03:57*

**+James Rivera** I like the conversations. I say chat-on.


---
**Jean-Francois Couture** *August 26, 2014 12:37*

I've seen that the ultimaker can do 100mm/s but is happy at 80mm/s. those are fast speeds but still, some printers can go faster.



**+Eric Lien** i'm sure that your new puppy can do 100mm/s easy ?


---
**Eric Lien** *August 26, 2014 13:49*

**+Jean-Francois Couture** the hard part is cooling & extruding reliably at really high speed. The gantry mechanics is the easy part.


---
**Øystein Krog** *August 26, 2014 16:15*

**+Eric Lien** Have you seen the cooling solutions where they use pumps and tubes to provide airflow?

I'm wondering if that kind of solution and perhaps even some way of pushing refrigerated/cooled air would work at super-high speeds :P


---
**Jean-Francois Couture** *August 26, 2014 17:14*

I tried that.. used a aquarium water pump.. wasn't very effective. 


---
**Jean-Francois Couture** *August 26, 2014 17:15*

**+Eric Lien** maybe this could help on the extruder heating part  ? [https://github.com/ErikZalm/Marlin#autotemp](https://github.com/ErikZalm/Marlin#autotemp)


---
**Eric Lien** *August 26, 2014 17:23*

**+Jean-Francois Couture** thanks. I forgot about that feature.


---
**Eric Lien** *August 26, 2014 17:26*

**+Jean-Francois Couture** pumps are tough due to static pressure loss across lengths of small ID tube. You get higher pressures... But very small cfm so not much volume to handle the btu load to take plastic below the tg temp.﻿ 



Maybe a compressed cylinder and step down regulator, but who wants that cost.


---
**Jean-Francois Couture** *August 26, 2014 17:32*

At one point, I wanted to try my air tool compressor.. but yeah.. I didn't want a 20 gallons / 5HP compressor by my side when printing ;-) 


---
**James Rivera** *August 26, 2014 18:12*

I think turbine fans (right name? I keep getting that mixed up) can push more air than the regular fans.


---
**Mike Miller** *August 26, 2014 18:34*

AKA Squirrel Cage Fans? [https://www.google.com/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=squirrel%20cage%20fan](https://www.google.com/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=squirrel%20cage%20fan)


---
**James Rivera** *August 26, 2014 19:31*

**+Mike Miller** I'm at my full PC now...I meant to say Centrifugal fan, but yes, that looks like another name for the same thing.



[http://en.wikipedia.org/wiki/Centrifugal_fan](http://en.wikipedia.org/wiki/Centrifugal_fan)


---
**James Rivera** *August 26, 2014 21:48*

I bought 4 of these a few months ago.  One of these days I will get around to actually installing one on my 3d printer.



[http://imgur.com/Iyb1KQo](http://imgur.com/Iyb1KQo)


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/9YtEb9Dwch8) &mdash; content and formatting may not be reliable*
