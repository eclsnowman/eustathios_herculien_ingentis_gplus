---
layout: post
title: "Fitting bushings It seems my bushings for the 10mm rods are a 'size on' fit"
date: September 09, 2016 01:57
category: "Discussion"
author: jerryflyguy
---
Fitting bushings



It seems my bushings for the 10mm rods are a 'size on' fit. Anyone have any secrets for getting them to fit smoothly onto the rods? First attempt is to put the rods in the freezer and see if they shrink enough to make a difference. (I'm waiting for the rod to chill).



Any other suggestions? I'd rather not have to spin them down w/ emery if possible. I'm assuming it's just a 'wear-in' thing that will get better after I have it assembled and can run gcode.



Same issue w/ my drive pulleys on the ends.. May put those in a zip loc and dip in boiling water(not sure if the bushings can take that much heat? 





**jerryflyguy**

---
---
**Eric Lien** *September 09, 2016 02:35*

Sounds like you might not have used the misumi rods? The BOM has misumi rods toleranced with an asymmetric tolerance 10mm - 0.**mm/+0.00. if you went with a general 10mm hardened rods with a 10mm symmetric + / - and they are oversized you will probably never get them to run correctly unless you ream the bushings which would be very difficult (sorry). Try to avoid using any abrasive method to open up the bushings since the abrasive will become impregnated into the sintered oilite bronze and likely never come out and eventually they will score the rods.



As much as it stinks, you might need new rods.


---
**Sean B** *September 09, 2016 02:39*

Where did you get the rods and bushings?  Mine slid on without any issue.  Are you able to measure the roots?  They might be oversize.  I would avoid sanding the rods, that is the last thing you want to do.  If necessary you can probably open up the bushings with a reamer and an electric drill, this would be the safest route.﻿


---
**Eric Lien** *September 09, 2016 02:40*

You could turn an extra rod into a reaming tool to open up the bushings (spin it in a drill and grind a taper, then cut slots along the length to act as flutes). But it would take experimenting.


---
**jerryflyguy** *September 09, 2016 02:54*

Thanks guys, rods are smw3d and bushing from SDP/SI.. May have to try building a reamer I guess.. Do have extra rod cut offs that my be 'adaptable'


---
**jerryflyguy** *September 09, 2016 02:56*

Best way to hold the bushing while reaming so they doubt go out of round?


---
**Tomek Brzezinski** *September 09, 2016 03:58*

**+jerryflyguy** my guess parralels (or closest thing you have) and the most axially supported tool you have. Absolutely not a hand drill, but a drill press might be enough. 


---
**Eric Lien** *September 09, 2016 04:01*

One thing to keep in mind while trying to ream these is that the bronze is essentially epoxy potted into the outer black housing so if you try and ream it... it's going to have to be very gradual otherwise it'll probably spin inside that housing that allows it to act like a spherical joint. Also you need to keep the heat low so the epoxy doesn't break down. The first time I tried to do these bushings I tried to heat press them into the plastic which worked great other than the fact but the bronze pieces began flopping around inside the housing :(﻿


---
**Eric Lien** *September 09, 2016 04:12*

**+Brandon Satterfield**​ I have had someone else mention the 10mm shaft fit before. Can your supplier provide G6 shaft tolerance fit rods for these Eustathios kits? The rods you have work great for lmu bearings. But for smooth running linear guide fits and rotating shafts fits using bushings they are oversized.


---
**Eric Lien** *September 09, 2016 04:13*

**+Tomek Brzezinski** perhaps chucking the bushings in the drill press chuck, then running it down onto the reamer held in a drill press vice?


---
**jerryflyguy** *September 09, 2016 04:37*

I made a nice little (ugly) reamer.. Worked great.. Got the bushings to fit and w/ some mucking, one 32 tooth pulley. But now I'm realizing the end bearings don't slip fit either.. Guess I could emery the ends of the rods to fit the bearings ? I think I got some crap rod from SMW3D. I'm noticing that the bushings still hang up part way down the rod (tighten up to the point of jamming) and then at other parts they slip along very easily. Is this normal for first fit? I've been at it for 4 hrs and haven't got one shaft assembled complete to the point I could install it into the frame. 



Wondering if I should trash these rods and order more bushings and matched rod from SDP/SI ? That'll be a nice cheap touch(not) but at least I think it'd work? Or will I run into the same issues? (I.e. I just need to keep 'fitting' these till they work?) 



Not my first rodeo building stuff, built a 20ft x 10ft CNC router table a couple years back.. Don't think I'm this inept.. But 


---
**Eric Lien** *September 09, 2016 05:13*

**+jerryflyguy**​ my recommendation is the misumi rods (and some un-reamed bushings). The specs on the misumi rods is just what is needed (in regards to shaft fit, concentricity, parallelism, etc). 


---
**jerryflyguy** *September 09, 2016 05:24*

Luckily I've got 4 spare bushings so that's ok.. Guess that's what I'll do tomorrow. Screwed up one of my 32tooth pulleys also so will have to pick another up


---
**Brandon Satterfield** *September 09, 2016 11:16*

+jerryflyguy don't mess with the rest of the rods you have. Send everything back happy to refund. Will take care of the return shipping as well. 



Have not had luck with this BOM as these tolerances where not called out. Ordered "10mm x XX hardened precicion rods for x/y axis guides", needs to be updated to note tolerances on ball screws, rods, etc. would also suggest going Mic 6 on the bed. Beating 5052 into submission isn't too much of a good time. 


---
**Eric Lien** *September 09, 2016 12:39*

**+Brandon Satterfield** yeah I will get the BOM updated to avoid future issues. Sorry to everyone for the confusion.


---
**jerryflyguy** *September 09, 2016 15:24*

Thanks **+Brandon Satterfield** I'll get them packaged up and sent off this weekend. It appears that the LM10 bearings still fit these rods? 


---
**Brandon Satterfield** *September 09, 2016 16:32*

Correct. 


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/TFKUL4z9VfX) &mdash; content and formatting may not be reliable*
