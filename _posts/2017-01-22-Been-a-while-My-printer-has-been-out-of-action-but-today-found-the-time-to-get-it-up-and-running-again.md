---
layout: post
title: "Been a while. My printer has been out of action but today found the time to get it up and running again"
date: January 22, 2017 04:06
category: "Discussion"
author: Ben Delarre
---
Been a while. My printer has been out of action but today found the time to get it up and running again. Have spent the day considering the great "Flying Extruder" modification since I think I've just about had enough of long Bowden. **+Walter Hsiao**​ what length did you cut the stepper supporting extrusions to? And could you point me to the files for the bondtech mount and the little stem to support the small Bowden? I have put together something similar to your 2020 pivots using the vitamins I had on hand, just need to print the mount and try it out.





**Ben Delarre**

---
---
**Eric Lien** *January 22, 2017 04:59*

**+Maxime Favre**​ I know you did the flying extruder mod. Can you give some notes on the conversion?



[https://plus.google.com/117610054892104446719/posts/jdwfKiusLM8](https://plus.google.com/117610054892104446719/posts/jdwfKiusLM8)



[https://plus.google.com/117610054892104446719/posts/CJ8D3ELUGHA](https://plus.google.com/117610054892104446719/posts/CJ8D3ELUGHA)


---
**Markus Granberg** *January 22, 2017 07:01*

I am running a e3d titan direkt drive. Works great..


---
**Ben Delarre** *January 22, 2017 07:05*

Thanks for the links **+Eric Lien**​. Missed that video... So sold now just because it looks so cool!


---
**Ben Delarre** *January 22, 2017 07:07*

**+Markus Granberg**​ do you have a mount for the Titan? Would be willing to give that a go too. I am just way too impatient and lazy to properly tune a Bowden. By the time I get it right from a given spool I'm out of plastic!


---
**Markus Granberg** *January 22, 2017 07:46*

![images/e4d700705ee940f99cd4623dc8304fef.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e4d700705ee940f99cd4623dc8304fef.jpeg)


---
**Markus Granberg** *January 22, 2017 07:47*

This is the mount I'm using now

![images/3926f2a89711e289722708b3bf08a210.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3926f2a89711e289722708b3bf08a210.jpeg)


---
**Ben Delarre** *January 22, 2017 07:49*

Cool! Can you share the STL? I have a few sets of bearings so could make that up and shove an e3d lite in it for flexibles. Does that mount also include the sensor? Would be very interested in giving that a go. We should get that mount added to the git repo too.


---
**Markus Granberg** *January 22, 2017 08:28*

I will share the files when I get home :-) yes the sensor is a part of it to.


---
**Maxime Favre** *January 22, 2017 09:37*

I tried the flying extruder for 4-5 months with great results. The only weakness are the pivot joints. They aren't rigid enough on the long run and then the flat roller bearings start wearing quickly adding plays one the arm.



For the arm extrusions length I simply cutted a 425 spare in two. It's quite simple: the total extrusion length + the pivot joints length must be able to reach each corners of the print area + a safe margin. (it depends where you choose to mount the base of the arm. In the middle of a side in my case).



If I found the time I'll test to mill aluminium joints on the cnc.






---
**Daniel F** *January 22, 2017 10:58*

Another solution to shorten the bowden is to place the extruder on a construction above the gantry. I built a square top on top of the frame (25cm high).  This way I could reduce the lenght of the bowden from 80 to 30cm. Tuning for ABS is easierthan before. I haven't tried flexible filaments. 

The advantage of this solution is that it allows to build an enclosure around the printer. This would be difficult with the flying extruder, as the "elbow" sticks out of the frame. 


---
**Ben Delarre** *January 22, 2017 22:33*

**+Maxime Favre**​ do you have the STL for the extruder mount and Bowden support?


---
**Ben Delarre** *January 22, 2017 22:34*

**+Daniel F**​ I had considered this approach as well. How did you mount the extruder? I was worried that it would result in much twisting of the Bowden. I might give that a go today since I have appropriate parts on hand and it requires less modification than the flying extruder.


---
**Daniel F** *January 22, 2017 22:54*

**+Ben Delarre** I placed it on a 2020 extrusion, see here [plus.google.com - Extruder distance/bowden lenght when mountig extruder above bed I'm trying to…](https://plus.google.com/111479474271942341508/posts/3czE4PrCUBy)


---
**Maxime Favre** *January 23, 2017 06:03*

**+Ben Delarre** here it is [https://drive.google.com/open?id=0B3rOLUu_uwqNZlItU0ROOEFVNkU](https://drive.google.com/open?id=0B3rOLUu_uwqNZlItU0ROOEFVNkU)


---
**Markus Granberg** *January 23, 2017 09:45*

[onedrive.live.com - OneDrive](https://1drv.ms/f/s!AgMdpPc7a2rwgfUId9tf0uU6rrle1Q)




---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/a636gnZjjAp) &mdash; content and formatting may not be reliable*
