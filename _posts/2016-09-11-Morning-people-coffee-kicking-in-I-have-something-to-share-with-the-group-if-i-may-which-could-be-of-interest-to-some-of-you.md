---
layout: post
title: "Morning people, coffee kicking in. I have something to share with the group if i may which could be of interest to some of you"
date: September 11, 2016 09:07
category: "Show and Tell"
author: Mutley3D
---
Morning people, coffee kicking in.



I have something to share with the group if i may which could be of interest to some of you. Are looking to deploy dual filament but not sure of an ideal solution, or have dual bowdens and wish for dual direct drive if it were available in a light enough form? I may have a solution for you.



I have created a dual filament version of the Flex3Drive specifically for Chimera/Cyclops hotends and am now running a pre-release program. E3D are also supporting the program by offering a discount code to those involved.



The program now has enough participants (via private invitations) to go ahead however a couple of places are still up for grabs and so I thought why not shout out in here to see if any members want to get their hands on this great piece of kit. If I fill all places, a further discount kicks in for all involved.



Below is a link to more info and a contact form should you want to dive in. If you have any questions comment or suggestions just drop them in the comments below or via the form on the linked page. It would be great to hear back from you.



[http://mutley3d.com/mutley3d_wp/flex3drive-cyclops-preorder/](http://mutley3d.com/mutley3d_wp/flex3drive-cyclops-preorder/)﻿





![images/e5e5e23356a93b9adfd8bc52823b5e02.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e5e5e23356a93b9adfd8bc52823b5e02.jpeg)



**Mutley3D**

---
---
**Eric Lien** *September 13, 2016 00:25*

Sorry, for some reason this got kicked into the new G+ spam filter for the community. Looking good. 


---
**Eric Lien** *September 13, 2016 00:29*

Any chance you want to help design a carriage that can mount this on to a HercuLien or Eustathios?  The way the rods cross makes this a slightly difficult exercise in design. Nice thing is the Eustathios and HercuLien share the same cross rod spacing. So if it works for one, it can work for both.﻿


---
**Mutley3D** *September 13, 2016 02:47*

Hi Eric, thanks for the response and update. years of work turned to spam...dang! The aim of the program is to work with those involved to fine tune mounting arrangements on different machines ahead of wider release. For HercuLien Eustathios I envisage the unit sitting above the top bar, with the Cyclops mounted back against X or Y cross rod. All files available as this will be open source along with all my other single and dual drive designs going up on the new website.


---
**Mutley3D** *September 14, 2016 02:45*

**+Eric Lien** Feel free to join in :) last couple of places left, would be great to get a full house.


---
**Ben Malcheski** *September 16, 2016 22:19*

Are there any spots left? I have a Herculien style machine that I scaled up some. I am using a Kraken and the bowden tubes are killing me. I have to keep the machine way below it's maximum speed capabilities in order to maintain consistent extrusion. I had previously been looking at the Flex3 Drive and was just hoping there was going to soon be one that would be more suitable for printing 3 or 4 materials at a time.



I would at least like to get more information and maybe a basic CAD model to see what kind of real estate I need on the print head, how long of drive shafts I would need etc...


---
**Mutley3D** *September 22, 2016 03:58*

**+Ben Malcheski** yes there are a couple of spots left. Forgive my delay in responding, i dropped the ball a little bit as i had to get the new website live, which it is now. You can follow the links at [www.flex3drive.com](http://www.flex3drive.com) would be great to have you on board


---
*Imported from [Google+](https://plus.google.com/+Mutley3D/posts/A8vsZXzZ2CW) &mdash; content and formatting may not be reliable*
