---
layout: post
title: "Has anyone here built a Kitten Printer?"
date: June 08, 2016 18:36
category: "Discussion"
author: Alexander Larson
---
Has anyone here built a Kitten Printer? I am very interested in hearing about it if so.





**Alexander Larson**

---
---
**Alexander Larson** *June 08, 2016 18:48*

Close [http://printerkitten.com/](http://printerkitten.com/)  I was directed here to this group from a person who suggested looking at that printer and I thought I would ask if anyone here has any experience.


---
**Brad Hill** *June 08, 2016 23:41*

Great guys and a great design, it's on my to-do list.


---
**Alexander Larson** *June 08, 2016 23:57*

**+Brad Hill** I'm looking at them for a classroom set of 12. I really like the design. I was looking to initially purchase 12 Ultimaker 2 GO printers. I have one and like it, but for the same or less cost we could have a much more capable and accurate machine from what I've seen. Plus students get a different learning experience.


---
**John Driggers** *June 09, 2016 03:17*

Saw a couple at MRRF this year, they are cool little massively over engineered printers.  If you have the budget they should be great projects in and of themselves.  If you don't already have a working 3d printer to print your parts on, I'd consider a pair of Go's to seed the kittens...


---
**Brad Hill** *June 09, 2016 16:35*

**+Alex Larson** If you end up wanting more go's let me know I might have one you would be interested in. I'm probably going to try cutting the bed plates myself for the kitten if you are looking for a source of those also. I really loved the Tantillus unique design style, and the Kitten is very similar.


---
**Alexander Larson** *June 09, 2016 18:22*

**+Brad Hill** Let me know how the bed plates go. I am in overload on looking at many different designs right now.  Just came across [http://www.instructables.com/id/DICE-a-Tiny-Rigid-and-Superfast-3D-printer/?ALLSTEPS](http://www.instructables.com/id/DICE-a-Tiny-Rigid-and-Superfast-3D-printer/?ALLSTEPS)



Mini COREXY named DICE.  Looks really interesting as well.  I may even design my own printer while borrowing some components and ideas from Kitten and DICE.  



DICE is a little smaller than I want for a build plate, Kitten is more of the size I am looking for but may be a little small for the need as well.



Long story short, I am looking for a printer that prints accurate parts primarily out of PLA for the need and to keep material cost down. I teach engineering classes at a high school and we use 2 Stratasys 1200SST printers, obviously print costs are high. I want more access to more kids and I could go the route of Ultimakers, or build something. ( I like the latter)


---
**Brad Hill** *June 09, 2016 19:01*

The DICE is interesting, I read through the bom and assembly yesterday and came to the conclusion there is just too much complexity and I would likely never complete a build of one lol. I would add the hiwin rail cost alone would probably approach almost $300 us for genuine rail. I also haven't seen good prints from one. I have see the high speed benchy video ... and the benchy is pretty horrible but somewhat expected given the print settings.



I will say I did testing with a printrbot play and it was surprisingly good..


---
**Alexander Larson** *June 09, 2016 21:47*

The rails are expensive, the build is a little difficult, I would most likely modify the concept if I consider that.  I want to see what it can do for a proper print speed. The high speed runs are cool and all, but I had the same concerns with accuracy. I am going to reach out to the designer and see if he can provide more info on the print quality and accuracy. 



I will do the same for the Kitten.



I am less concerned with cost as I am with accuracy and repeat-ability which is why the linear rails appealed to me as well as a rigid chassis.  



Thank you for the insight, if you end up making the Kitten print beds let me know, I am just looking at as many options as I can. 


---
**Alexander Larson** *June 17, 2016 14:04*

So I am looking at the Kitten primarily right meow.  Simple and clean.  Prints come out looking great and I am talking with the designers through email now about some of the details.  I may end up making the printing envelope a little larger (120X120X120) so I will most likely have to machine my own beds.  I am considering not going with the heated plate though.  I have lots of success with PLA on a Print in Z plate.  Is there any reason I should reconsider that and get a heated bed like in the design initially? 



Just trying to simplify the design as much as possible for the intended purpose.


---
*Imported from [Google+](https://plus.google.com/117084726865589765120/posts/PNr6cV7Rh7N) &mdash; content and formatting may not be reliable*
