---
layout: post
title: "Full BOM is up for HercuLien Originally shared by Eric Lien BOM is up for HercuLien"
date: November 22, 2014 09:57
category: "Discussion"
author: Eric Lien
---
Full BOM is up for HercuLien



<b>Originally shared by Eric Lien</b>



BOM is up for HercuLien





**Eric Lien**

---
---
**Chandler Wall** *November 26, 2014 05:44*

I'm preparing to purchase a decent portion of the BOM's hardware (specifically the frame subassembly and various nuts/screws).



First, big thanks for including multiple vendor links in the BOM. Second, I have a fairly simple question: how do the screws and nuts differ between vendors? Which vendors have you used in the past? Can you provide any personal opinions/comparisons between MiSUMi T-nuts and OpenBuild T-nuts? If it matters, I plan to use MiSUMi's 20x80 and 20x20 extrusions except for the vertical axis' V-slot extrusions.



From the pictures alone, it appears that the MiSUMi T-nuts use a slightly different profile than the OpenBuild variety. Based on price/convenience, I plan to purchase OpenBuilds' M5x10mm low profile screws. I realize that any M5 screw [with coarse pitch] will fit the M5 T-nuts, but I'm interested if you have a personal preference on screws or nuts.



Screws and nuts are such a minor line-item compared to the aluminum extrusions, but my experience has shown me that lousy hardware can frustrate/jeopardize even the most diligent plans. Also, this is a basic technique of mine to stall the purchase of $200+ in "unnecessary" parts.



Thanks in advance; I'm really looking forward to this build!



If there's a better forum for these questions, please let me know.


---
**Eric Lien** *November 26, 2014 06:18*

This is the perfect forum for these questions. I say we discuss any questions you run into like this so others can review.



Either T-Nut will work great. The T-nuts from Misumi are more substantial since they step into the slot so you get more thread engament.



I have bought hardware from open builds, the local hardware store, McMaster, Trimcraftaviationrc. for nuts/bolts if trimcraft has it it will be cheaper, great quality, and stainless. But the owner has medical issues right now so no shipments. (Edit looks like they are back to shipping, so trimcraft is where I would go first). 



Any hardware will work fine. I would just shop the hardware. I really adds up which is why I spent so much time documenting the hardware for others. Also some of the hardware may be best to source at another location than one I have listed especially the oddball stuff that I only used in a few locations. If you find good sources let be know, I will update the BOM.



One word of advice. I would say the hardest part of my build was the holes for the shafts out the side of the extrusion for the knobs and the drive. They go through half on a rib. My recommendation is find someone with a mill. it took me 3 hours on a drill press with a table vice (used a 1/2" bit) and I still fought bit walk.﻿


---
**Eric Lien** *November 26, 2014 06:23*

Also print the drill guides I included. They make life so much easier drilling all the holes in the extrusion. Also for the M5x10mm bolts don't do with the open builds flat heads. It will not fit in the tee slot because it is too wide. Get button head bolts.


---
**Eric Lien** *November 26, 2014 06:30*

These images show how the corners attach without brackets.



[http://imgur.com/hMUm7RH](http://imgur.com/hMUm7RH)

[http://imgur.com/rfB2g1f](http://imgur.com/rfB2g1f)


---
**Jo Miller** *November 30, 2014 13:09*

Hi Eric, BIG compliment   your BOM is great,



i´v never seen such a complete List before in the german reprap community



some first questions..

could i use (instead of item 19.3.13 =20x20x519.5mm Extruded Aluminum) the same aluminium extrusion as  item 1.1  

(20mmx20mm609.5mm aluminum extrusion l) ?



Refering to BOM

19.2.4 & 19.1.1 / whats the exact lenght ?  What would you think (pricewise) of this ?



[http://www.aliexpress.com/snapshot/6220007216.html](http://www.aliexpress.com/snapshot/6220007216.html)

I used them on my xy Core Sparkcube, and they work fine..



What was your reason for choosing the Azteeg X3 V2 board ? , was it the Digital pot control for stepper drivers ? ( on the SD8825 drivers),



Is there an performance benefit compared to using a regular RAMPS 1.4 + SD8825 driver-setup ?.



Im sort of waiting for the next generation of Boards especially the TinyG 3d Board development.. ..



Did you already printed something using the maximum hight of the herculien-build platform ?



:-)   Jo


---
**Eric Lien** *November 30, 2014 14:11*

**+Jo Miller**​



19.3.13 is set to allow the v wheels to land perfectly in the v track. It could change, but not without other changes.



19.1.1 is 480mm overall length, one end stepped to 8mm for 10mm, the other end stepped to 8mm for 26mm. Depending on the size of the ball screw nut some things may require changes to avoid interference.



I used azteeg x3 for its many inputs/outputs and high power MOSFETs, and easy 24v operation. A ramps will do just fine. I actually don't need the high power MOSFETs now that I run the bed at 110v via an SSR. But 24v does make the steppers run better.



I am excited about the tigershark project that will bring tinyg to 3d printing. They worked last week to setup the chilipeppr environment for this board. I am watching it closely.



I printed a 12"w x 13"d x 10"h Prototype pattern. 3+ day print. Turned out great. I would show pics but it is a customers proprietary design.﻿


---
**Chandler Wall** *December 01, 2014 06:28*

I've been pouring over the BOM and various photos for a few days. I made a few small purchases since there were deals on Friday. In order to minimize the cost of shipping, I added the primary supplier's name to the BOM. I also reorganized/reformatted a few items so that pivot tables could be easily generated. The result is a list of parts grouped by the primary supplier. This is critical when ordering parts from companies like MiSUMi and McMaster-Carr. I also updated the total quantities by part number sheet to use a pivot table.



Here's the link to my updated spreadsheet:

Published HTML - [https://docs.google.com/spreadsheets/d/1wmu_0KDhdMIUvXJtXerx0vLjDn5ZSmWVhdcxFXamgwo/pubhtml](https://docs.google.com/spreadsheets/d/1wmu_0KDhdMIUvXJtXerx0vLjDn5ZSmWVhdcxFXamgwo/pubhtml)

Google Spreadsheet (I hope I have the permissions set up correctly) - [https://docs.google.com/a/swall.us/spreadsheets/d/1wmu_0KDhdMIUvXJtXerx0vLjDn5ZSmWVhdcxFXamgwo/edit?usp=sharing](https://docs.google.com/a/swall.us/spreadsheets/d/1wmu_0KDhdMIUvXJtXerx0vLjDn5ZSmWVhdcxFXamgwo/edit?usp=sharing)



I have not changed the quantities or any critical information. The only part numbers I changed were related to HFS5-2080-569.5. I moved the information in parentheses to the description (this allows the pivot table to produce a more accurate total quantity for the part). Since Google spreadsheet don't natively support multi-column sorting, it takes three steps to organize the BOM so that it matches the original: sort by the third column, sort by the second column, and then sort by the first column.



I hope this information may be useful to others. If changes are made to the original BOM, I'll try to update my spreadsheet, too.



Eric, if you think this information could be useful to others, I can adapt my approach so that it can be copied/pasted into your BOM document (I removed the various subassembly header rows for the sake of sorting). Please let me know.



Thanks again for the effort that went into creating/publishing such a detailed spreadsheet!


---
**Chandler Wall** *December 01, 2014 06:48*

On a related note, I was able to find additional suppliers/options for a few items on the list. I'll make a list/take note of all the items tomorrow.



For now, I'll just mention that switches can be purchased at low prices from electronic suppliers ($1.11 and below vs $1.99 and higher). The mechanical end stops sold by most RepRap suppliers are equivelant to SS-3GL13PT ([http://www.digikey.com/product-detail/en/SS-3GL13PT/SW768-ND/664729](http://www.digikey.com/product-detail/en/SS-3GL13PT/SW768-ND/664729) or [http://www.mouser.com/ProductDetail/Omron-Electronics/SS-3GL13PT/?qs=SXIVkn%252bm38nQebWCPjda9A%3D%3D](http://www.mouser.com/ProductDetail/Omron-Electronics/SS-3GL13PT/?qs=SXIVkn%252bm38nQebWCPjda9A%3D%3D)).



Unfortunately, the total savings isn't that great, since switches aren't very expensive. If you need a handful of these for multiple projects, the bulk savings kick in fairly quickly. Mouser has a better price and better bulk price, but they run out of stock on these more often. At any rate, it's nice to have additional options.


---
**Eric Lien** *December 01, 2014 12:27*

**+Chandler Wall**​ looks great. I will pour over it in more detail later. I like the sorting using pivot tables. Thank you for the hard work.


---
**Marc McDonald** *December 11, 2014 04:50*

**+Chandler Wall** Hi Chandler, I have also been working on ordering parts and have been comparing Eric's original BOM, your BOM and the Solidworks model and see some issue with quantities of some items in your BOM.  Have you managed to source all the parts, I am having issues with the two short GT2 belts as SDP-SI is out of stock?


---
**Eric Lien** *December 11, 2014 05:02*

**+Marc McDonald** you can go slightly longer the motor mount will just slide down further on the vertical upright.


---
**Chandler Wall** *December 11, 2014 05:04*

**+Marc McDonald** Hi Marc, no, I have not managed to source all of the parts. It's entirely possible that my list is out of sync if there have been recent updates. My list originated from **+Eric Lien**'s BOM, and I was mostly focused on the higher dollar items that I don't currently have. I'm still working out prices and availability before making any purchases.



With regard to the belts, yes, they appear to be out of stock. I found an equivalent belt through MiSUMi. It's a 113 tooth, closed loop GT2 belt. The product number is GBN2262GT-60, and it can be found here: [http://us.misumi-ec.com/vona2/detail/110302190510/?ProductCode=GBN2262GT-60](http://us.misumi-ec.com/vona2/detail/110302190510/?ProductCode=GBN2262GT-60). I have not personally purchased this item yet, so you  may want to review it further for compatibility. I hope this helps you!


---
**Marc McDonald** *December 11, 2014 05:11*

**+Chandler Wall**

Hi Chandler, thanks for the information on the GT2 belt, will look into it.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/MXfsUwqkgXL) &mdash; content and formatting may not be reliable*
