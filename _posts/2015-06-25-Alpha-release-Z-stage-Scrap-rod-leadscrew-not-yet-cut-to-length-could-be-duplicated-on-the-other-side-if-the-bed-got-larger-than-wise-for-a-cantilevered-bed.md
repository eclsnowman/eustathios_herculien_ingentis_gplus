---
layout: post
title: "Alpha release Z-stage. Scrap rod, leadscrew not yet cut to length, could be duplicated on the other side if the bed got larger than wise for a cantilevered bed"
date: June 25, 2015 03:37
category: "Deviations from Norm"
author: Mike Miller
---
Alpha release Z-stage. Scrap rod, leadscrew not yet cut to length, could be duplicated on the other side if the bed got larger than wise for a cantilevered bed. The built plate will be on 3 points for easy tram adjustment. 



![images/e3f9947d15b70a478cf257be76225ec2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e3f9947d15b70a478cf257be76225ec2.jpeg)
![images/bac646e62d192cf987ba93a5ae8ce217.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bac646e62d192cf987ba93a5ae8ce217.jpeg)
![images/c59cf0a7d6232e8ad41597d37d9458bb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c59cf0a7d6232e8ad41597d37d9458bb.jpeg)

**Mike Miller**

---


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/3xYPZCnj7Mx) &mdash; content and formatting may not be reliable*
