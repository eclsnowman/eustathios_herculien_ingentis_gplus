---
layout: post
title: "The carriage in question looked something like this...the block on the right is the 40mm fan to cool the e3d, the block you can see on the back left was for part cooling, and each bearing surface was supported by two bushings"
date: July 12, 2014 13:48
category: "Discussion"
author: Mike Miller
---
The carriage in question looked something like this...the block on the right is the 40mm fan to cool the e3d, the block you can see on the back left was for part cooling, and each bearing surface was supported by two bushings.



It also had slots for zipties to retain the extruder. 



(Yeah, and for those of you saying 'you're 90% of the way there, just press print', I hear ya! :) )

![images/5214890ae55588a01a98dbb007251442.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5214890ae55588a01a98dbb007251442.jpeg)



**Mike Miller**

---
---
**Eric Lien** *July 12, 2014 14:16*

Looks kind of like Jason's: [https://github.com/jasonsmit4/Eustathios/blob/master/STL/Eustathios%20Carriage%20-%201%20-%20v2.1.stl](https://github.com/jasonsmit4/Eustathios/blob/master/STL/Eustathios%20Carriage%20-%201%20-%20v2.1.stl)


---
**Eric Lien** *July 12, 2014 14:21*

And also like mine a few pictures into this post: [https://plus.google.com/109092260040411784841/posts/Gzuz2VEpQSr](https://plus.google.com/109092260040411784841/posts/Gzuz2VEpQSr)﻿



Stl: [https://docs.google.com/file/d/0B1rU7sHY9d8qbDh1SGprSXBaRFU/edit?usp=docslist_api](https://docs.google.com/file/d/0B1rU7sHY9d8qbDh1SGprSXBaRFU/edit?usp=docslist_api)


---
**Mike Miller** *July 12, 2014 14:27*

Yeah, that's the one! How's it working for you?


---
**Eric Lien** *July 12, 2014 14:34*

Works great. It is a tough supported print because I made it one piece. But cable routing is good and it is designed for a standard computer 8 pin connector to be mounted to the  carriage so all wiring is modular.



One thing I would change is widen the carriage to have a part cooling fan on each side if the nozzle for unsupported overhangs in PLA.﻿ I get asymmetric results depending on the overhang location on the part. More cooling in PLA = better. 


---
**Eric Lien** *July 12, 2014 14:42*

One note on mine is I made a small adjustable deflector for the cooling fan output. Just a "U" shaped piece with a flap that slides over the bottom of the carriage to direct the air more towards the nozzle verses straight down. It helped a lot. When I redesign it will be part of the carriage once I find the perfect  orientation with my temporary piece.


---
**Eric Lien** *July 12, 2014 14:52*

Timelapse of PLA print with deflector installed (prior to optimizing retraction):



[https://docs.google.com/file/d/0B1rU7sHY9d8qVlRSRW5yZjZtZTg/edit?usp=docslist_api](https://docs.google.com/file/d/0B1rU7sHY9d8qVlRSRW5yZjZtZTg/edit?usp=docslist_api)


---
**Eric Lien** *July 12, 2014 14:57*

And just for fun my prints that just finished for converting my big printer from CoreXY to Eustathios/Ingentis style. These are beefed up versions to handle the 24" span and use 10mm diameter cross bars:



 [https://docs.google.com/file/d/0B1rU7sHY9d8qaDh3MTJuVFRuWDA/edit?usp=docslist_api](https://docs.google.com/file/d/0B1rU7sHY9d8qaDh3MTJuVFRuWDA/edit?usp=docslist_api)


---
**Mike Miller** *July 12, 2014 15:01*

What's the hole opening diameter for the 10mm bushings?


---
**Eric Lien** *July 12, 2014 15:07*

I think 15.9mm od.﻿ I made it in solidworks... So if you want to try it let me know the OD and depth you want. Also how much the hole should be oversized if your printer IDs end up undersized.


---
**Mike Miller** *July 12, 2014 15:33*

The OD of the igus bushings are 14.1 mm and 12.76mm deep 



My printer is a work in progress. :) Currently a 3%-5% upscale is necessary to get external dimensions correct. I haven't calibrated internal dimensions yet...that's what the drill is for. 


---
**Eric Lien** *July 12, 2014 15:41*

For me I find a .2 oversize on IDs help. Also printing in ABS and wiping the hole with acetone before pressing the bushing in to avoid cracking.



Measure the ID of the hole on your current setup versus nominal and we can get a good idea of the optimal oversize you need.


---
**Eric Lien** *July 13, 2014 01:36*

**+Mike Miller** I'm am out this weekend. I will adjust the model and send it to you Sunday night.


---
**Mike Miller** *July 13, 2014 01:40*

No rush, I'm battling my own printing demons. ;)


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/C5XUKNAXnxY) &mdash; content and formatting may not be reliable*
