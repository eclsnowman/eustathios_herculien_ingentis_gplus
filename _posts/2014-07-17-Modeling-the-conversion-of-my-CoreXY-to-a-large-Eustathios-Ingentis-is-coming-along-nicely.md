---
layout: post
title: "Modeling the conversion of my CoreXY to a large Eustathios/Ingentis is coming along nicely"
date: July 17, 2014 01:16
category: "Deviations from Norm"
author: Eric Lien
---
Modeling the conversion of my CoreXY to a large Eustathios/Ingentis is coming along nicely.



Uses all 10mm rod, large nema17 60mm steppers from Robotdigg. I will be redesigning the extruder carriage, but it is a placeholder for now.

![images/39c579b482267190fa539d005182cc87.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/39c579b482267190fa539d005182cc87.png)



**Eric Lien**

---
---
**Mike Miller** *July 17, 2014 01:58*

That's one stout box!


---
**Eric Lien** *July 17, 2014 02:38*

**+Mike Miller** yup. It's pretty over built :)﻿


---
**Matt Kraemer** *July 17, 2014 07:08*

It is overbuilt for vertical resonance, but underbuilt for torsional forces. However, it is above average. I think all the ingentis/quadrap/ringrap machines are all lacking diagonal supports.


---
**Wayne Friedt** *July 17, 2014 10:29*

I am at the end of my COREXY building also. The first one was pretty good but the next 2 just didn't have any mojo. Never could get them to work very well especially the non round hole issue. I converted my last one over to have the same layout as my Solidoodle. It does make round holes now.



Looking forward to seeing how yours goes.


---
**Eric Lien** *July 17, 2014 12:15*

**+Matt Kraemer** it gets fully enclosed. I have side lexan panels not shown. So I don't think it'll be an issue. It wasn't on the CoreXY.


---
**Brandon Satterfield** *July 17, 2014 13:11*

**+Eric Lien** you have become quiet the SW user man! Design looks solid! 


---
**Eric Lien** *July 17, 2014 13:28*

**+Brandon Satterfield** thanks. It has been a long road learning it since end of last year. I still need to preplan my parts a little better to simplify the design trees. But what a fun software to use.


---
**Brandon Satterfield** *July 17, 2014 14:00*

**+Eric Lien** it really is. I started using it shortly after you. I designed my recent giant x-y gantry in SW. Used the open builds files, everything I printed fit almost perfect with the exception of the v-wheels. I think that file may be off. 

I never got this with AutoCAD 2014, tolerances were always off. Love this software!


---
**Eric Lien** *July 17, 2014 17:15*

**+Shauki Bagdadi** I like it. May vary it to HercuLien.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/JRfiwMmpGHQ) &mdash; content and formatting may not be reliable*
