---
layout: post
title: "Three jaw chuck, four sided extrusion....works well enough for a facing cut"
date: January 11, 2015 06:30
category: "Show and Tell"
author: Mike Miller
---
Three jaw chuck, four sided extrusion....works well enough for a facing cut. 

![images/08df6c55abc6ae72fcff9487841c85c7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/08df6c55abc6ae72fcff9487841c85c7.jpeg)



**Mike Miller**

---
---
**Hendrik Wiese** *January 11, 2015 10:52*

I cringed...


---
**Mutley3D** *January 11, 2015 12:22*

Fair cop :)


---
**Eric Lien** *January 11, 2015 14:12*

You lucky machinests and your cool toys ;)


---
**Mutley3D** *January 11, 2015 16:58*

20x40 next? ;p


---
**Mike Miller** *January 11, 2015 17:22*

Gonna need an upgrade for that...how bout we stitch two 20x20's together for ya? :D


---
**Mike Miller** *January 11, 2015 20:21*

**+Hendrik Wiese** why?


---
**Mutley3D** *January 11, 2015 22:09*

I think i can understand why **+Hendrik Wiese** might cringe but i can actually see how that would work quite well so long as the jaws dont clamp so tight that they mark the extrusion


---
**Mike Miller** *January 11, 2015 22:25*

It's a pretty heavy lathe, interrupted cuts don't bug it (Carbide inserts), and I'll throw a little brass shim stock in there if surface finish is important. 


---
**Mutley3D** *January 11, 2015 22:35*

Yea something between jaws and stock material would prevent any jaw marks on the extrusion, and a large (600mm?) digital caliper to measure length to know how much to face off.


---
**Mike Miller** *January 11, 2015 22:38*

I used a bandsaw to get them close, faced off one end, then sat them on a surface plate and used a scribe to get them all equal. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/bAtAjgNEZw2) &mdash; content and formatting may not be reliable*
