---
layout: post
title: "Hello all. I am an upstart to this group and to 3D printing generally"
date: April 10, 2016 15:10
category: "Discussion"
author: Fash Azad
---
Hello all.  I am an upstart to this group and to 3D printing generally.  I was wondering if anyone would be willing to print me a set of the printed parts for the Eustathios-Spider-V2. I would of course be willing to pay for parts.  Let me know.  I'm in Massachusetts if that helps at all.  Thanks so much!





**Fash Azad**

---
---
**Alex Paverman** *April 10, 2016 19:34*

If you won't have a better (more proximate) offer, I will do it. Made in Romania!


---
**Maxime Favre** *April 10, 2016 19:56*

Same here but I hope you can find somebody closer than switzerland﻿


---
**Fash Azad** *April 10, 2016 22:07*

Either would be fine.  No particular preference. 


---
**Fash Azad** *April 11, 2016 17:30*

**+Mark Ellison** Mark, Maxime has offered to help, thanks so much for your reply however.  I'm sure I'll have plenty of questions for the group as I begin the build.  Thanks again Alex, Maxime and Mark. 


---
**Alex Paverman** *April 11, 2016 17:42*

Good luck!


---
*Imported from [Google+](https://plus.google.com/112233904479727772335/posts/MT5jei6j51x) &mdash; content and formatting may not be reliable*
