---
layout: post
title: "Hi guys, I've been contemplating about building a 3D-printer for a while now and I came across these wonderful designs"
date: June 11, 2015 05:55
category: "Discussion"
author: Pekka Pasanen
---
Hi guys, I've been contemplating about building a 3D-printer for a while now and I came across these wonderful designs. I think I have my mind set on building the mighty HercuLien since I recently gained access to Ultimaker 2 and I can now print the parts :)



My actual question is, which parts should be printed with ABS or all of them? What print settings should be used (layer height, fill ratio, shell thickness)? I've never printed printer parts before :)





**Pekka Pasanen**

---
---
**Oliver Seiler** *June 11, 2015 06:04*

I've printed all the parts for my Eustathios V2 in PLA, which might not be ideal but it actually works pretty well. Only the fan duct got a bit hot and whereas it stayed functional I have replaced it with a t-glase (PET) print.

From memory I've used 60% infill, 0.2mm layers and 2 perimeters for pretty much everything.


---
**Pekka Pasanen** *June 11, 2015 06:56*

Ok, thanks for advice. I haven't printed anything yet because I'm worried how PLA parts will survive in enclosed HercuLien.


---
**Oliver Seiler** *June 11, 2015 07:36*

Sorry, I missed the fact you were going for the Herculien. For that one obviously PLA wouldn't be a very good idea...


---
*Imported from [Google+](https://plus.google.com/103026823217167936002/posts/ACTHgP2d655) &mdash; content and formatting may not be reliable*
