---
layout: post
title: "Just wondering if anyone is using the Eustathios in a dual extrusion setup"
date: January 08, 2017 21:09
category: "Discussion"
author: Oliver Seiler
---
Just wondering if anyone is using the Eustathios in a dual extrusion setup. I've never tried, but am keen to give it a go.

I know the Herculien carriage should fit the Eustathios and supports dual extruders, but was wondering if someone came up with a better fit?

Should I look at the Chimera or rather twin V6?





**Oliver Seiler**

---
---
**Eric Lien** *January 09, 2017 00:51*

I would go chimera or cyclops over two V6 just due to the size and lost build volume. **+Stefano Pagani**​ is doing a dual I think. He did some carriage mods to my old (and never tested) chimera carriage I shared a long ways back: [drive.google.com - HercuLien_Eustathios_Chimera - Google Drive](https://drive.google.com/folderview?id=0B1rU7sHY9d8qTjZHcjUwaVhDd1E)


---
**Eric Lien** *January 09, 2017 00:52*

![images/2cb58cd64ea4895cb50676862e6cc3d1.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2cb58cd64ea4895cb50676862e6cc3d1.png)


---
**Adam Morgan** *January 09, 2017 18:21*

I've currently got this (github link) carriage running on my Eustathios. 



You do lose a bit of space but I was over concerned. I mainly used it because I bought a volcano. However if I was to go back I would go with the chimera or cyclops as **+Eric Lien** said. 



[github.com - HercuLien](https://github.com/eclsnowman/HercuLien/tree/master/Community%20Mods%20and%20Upgrades/Zane%20Baird/V6-Volcano_dual_carriage)


---
**Stefano Pagani (Stef_FPV)** *January 10, 2017 13:34*

**+Oliver Seiler** Yeah, I'm using a chimera. Have not printed with it yet, but I have a cleaned up/corrected version of Eric's design. I plan to re design it completely later, but I can share the current version if you want. Note that the V6.1 hotends are longer, so they will no longer fit into the arrangement Eric proposed in the design,


---
**Oliver Seiler** *January 10, 2017 19:02*

Thanks **+Stefano Pagani**, I'd love to see your current design changes to give it a try.


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/LxMGCQ3vLdd) &mdash; content and formatting may not be reliable*
