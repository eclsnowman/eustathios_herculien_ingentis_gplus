---
layout: post
title: "Design decisions upfront. While we already built the frame there are still some design decisions left"
date: June 10, 2015 18:29
category: "Deviations from Norm"
author: Frank “Helmi” Helmschrott
---
Design decisions upfront.



While we already built the frame there are still some design decisions left. I started to print the first parts with Colorfabb XT (clear PET) and i'm basically quite happy. Unfortunately i still have a little bit warping with the MTPlus printing plate ([http://www.mtplus.de/Dauerdruckplatte0.html](http://www.mtplus.de/Dauerdruckplatte0.html)). For the carriage i'm not sure if PET will be good enough or if i would have to print that in ABS? What do you guys think?



Regarding the cross bars i noticed too late that the X/Y-Belt-tensioners from **+Walter Hsiao** are made for 10mm by default. I have prepared to have 8mm ones but at the end I will have all the material to go with 10mm alternatively - what do you think about that? Go with the higher weight and more rigidity (10mm) or the the lighter (8mm) ones?



I still have to order the aluminum plate. On Walters version the bed doesn't have to be CNC'd wich at the time is a good win - i guess i'll go with that one, too. What are you guys using on top of the glass? Any additional printing plates? I've just ordered some of the Buildtak plates to try out. Basically i'm happy with the MTPlus i named above but the PETG warping annoys me a bit.



The ballscrews (1204) are already ardered and E3Dv6 24V is also waiting to be built. What's left is the electronics. Maybe i'll go with the Rumba for now. I'll also try some smaller Motors that i still have around, think they should work too - will watch their temperature though, the 60mm ones should be able to get more done without heating up too much.





**Frank “Helmi” Helmschrott**

---
---
**Walter Hsiao** *June 10, 2015 22:41*

fwiw, I might switch to 8mm for the cross rods.  I went with 10mm thinking I'd eventually switch to direct drive bondtech, but I probably won't (it's pretty heavy and the length makes it hard to fit in without a significant reduction in build area).


---
**Eric Lien** *June 11, 2015 00:28*

**+Walter Hsiao** did you ever design a direct drive carriage version in CAD? I have one for HercuLien... But it is more of a retrofit than a ground up design.



Also I agree on sticking with 8mm on Eustathios. For it's span 8mm is plenty. 


---
**Eric Lien** *June 11, 2015 00:29*

**+Frank Helmschrott**​ for PETG I have found three wet coats of hairspray on cold glass makes PETG stick amazing. Zero warp.


---
**Walter Hsiao** *June 11, 2015 00:51*

**+Eric Lien** I have one that I've been working on, kind of a strange convertible design that lets me swap between bondtech, gearhead, direct drive and bowden.  I'm not sure it's something others will want to use, but I'll upload it once I've iterated on it a bit more.


---
**Eric Lien** *June 11, 2015 01:46*

**+Walter Hsiao** That would be great. I never got that one figured out, so I would love to see what you came up with.


---
**Frank “Helmi” Helmschrott** *June 11, 2015 13:54*

Thanks for your hints.



Additionally i was just thinking about how to fix the silicone heater pad. It has to go to the bottom of the aluminum sheet right? Ho do you guys fix it down there - maybe i'm just tilting my brain for no reason but i'm a bit stuck with that (in theory)


---
**Eric Lien** *June 11, 2015 14:03*

**+Frank Helmschrott** you can order it with an adhesive backing from alirubber. Also you can use high temp silicone, spread it thin on the heater and heat spreader, then install the heater and let it cure. Thats what I did on my first printer before I knew adhesive backing was an option.


---
**Frank “Helmi” Helmschrott** *June 11, 2015 14:06*

Argh - i ordered mine from Derek (Robotdigg) but of course didn't say i want adhesive backing. Maybe i'll do the silicone thing then. Shouldn't be a problem to get the high temp one.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/Anvg1ZXHhUH) &mdash; content and formatting may not be reliable*
