---
layout: post
title: "I aborted my built and went with the dual wire gantry, right now stuck on the drawing board somewhere"
date: February 07, 2015 12:35
category: "Discussion"
author: hon po
---
I aborted my built and went with the dual wire gantry, right now stuck on the drawing board somewhere.



This is how I plan the rods positioning back then. One axis use different bearing holders and use the frame extrusion as reference. I imagine need to loose up one axis and burn in to achieve smooth motion before tightening up again.



A simple stl is in here for the adventurous one to try:

[https://drive.google.com/file/d/0B4bLJHgWDzxfLTk0Y0JiUV9oUWc/view?usp=sharing](https://drive.google.com/file/d/0B4bLJHgWDzxfLTk0Y0JiUV9oUWc/view?usp=sharing)



![images/127c93b708ec93cdfc3cad364ae0a14f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/127c93b708ec93cdfc3cad364ae0a14f.jpeg)
![images/09f63aec99decbde1e36580070affa15.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/09f63aec99decbde1e36580070affa15.jpeg)

**hon po**

---


---
*Imported from [Google+](https://plus.google.com/111735302194782648680/posts/Bi5QHGMSgR3) &mdash; content and formatting may not be reliable*
