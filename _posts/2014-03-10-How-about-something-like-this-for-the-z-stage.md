---
layout: post
title: "How about something like this for the z stage?"
date: March 10, 2014 20:36
category: "Discussion"
author: Jim Squirrel
---
How about something like this for the z stage? 





**Jim Squirrel**

---
---
**Dale Dunn** *March 10, 2014 21:40*

Well, after you add guide rods to stabilize it, it's not less complex. Worse, Z steps / mm is not longer constant. Even with firmware or postprocessor support for the nonlinear axis, you'd get Z banding artifacts that look like a diffraction pattern.


---
**Wayne Friedt** *March 10, 2014 22:51*

It would be smoother if the coupler was bored straight, even if that was true it doesn't look linear in its movements. Moves slower at the top faster at the bottom.


---
**Jarred Baines** *March 10, 2014 23:06*

Movement along the axis of the screw is amplified when moving the 'scissor' part of this contraption;



Any backlash in the nut will also be amplified... And there is also (albeit minor) play in every joint of the scissor action mechanism.



I think, if you're going to have a motor, and you're going to have a screw, might as well have a screw-driven Z, you only add more components and more points which can introduce error with a scissor design.



It would certainly look cool tho :-)


---
**Kalani Hausman** *July 12, 2014 08:35*

I have been working on a box-format lab jack like the Newport 271, only built using 3D printed panels linked using thin brass nails.



I am currently scaling the adjustment using M3x20mm screws passing through a captive M3 nut on the near side and deflecting the opposite side by pushing away a small metal cup (best so far is a partial (arc) of a M5-M6 flat washer, dimpled using a center punch).



Assembled, the side with the cup is first, then the captive nut assembly with a split nail adjoining the two sides, and finally the third box segment set at 90 degrees.



I am striving for a final scale of 1/4-3/4 inches (outer) to make fine adjustment easier. I was thinking of threading a hole in the base to allow an M4 anchor in the place of the usual jack screw.


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/WVVg9PmPb3F) &mdash; content and formatting may not be reliable*
