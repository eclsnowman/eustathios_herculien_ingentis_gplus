---
layout: post
title: "New status update request on Herculien and Eustathios builds"
date: April 14, 2015 04:39
category: "Discussion"
author: Gus Montoya
---
New status update request on Herculien and Eustathios builds. We've had some come online with minor tweaks needed. How have everyone progressed in their builds? I essentially have all order-able parts, now waiting on 3d printed parts from the "print it forward". 





**Gus Montoya**

---
---
**Brandon Cramer** *April 14, 2015 04:56*

Can I ask what Print it Forward means?


---
**Gus Montoya** *April 14, 2015 05:13*

Hi **+Brandon Cramer** , "print it forward" is a list on google sheets that **+Eric Lien** created for those not able to 3D print their own parts to make the Herculein or V1 or V2 Eustathios printers. ( [https://docs.google.com/spreadsheets/d/1R3hqplEJYiaQFLmFX04-vTjgyQ5SVY1PGjbx-7S-J8k/edit](https://docs.google.com/spreadsheets/d/1R3hqplEJYiaQFLmFX04-vTjgyQ5SVY1PGjbx-7S-J8k/edit) )   The condition is for the first person to receive the "printed forward" parts for free, is that they in turn print a batch of parts for the next person to proceed with his/her build. The first two winners are **+Dat Chu** and **+Bruce Lunde**. Both are building Herculein 3Dprinters and are in various stages in the build.


---
**Oliver Seiler** *April 14, 2015 05:53*

I've had some issues getting the correct Z lead screws for my Eustathios, also half of my 10mm shafts are not exactly straight and I'm currently  waiting for replacements. Everything else has arrived and is assembled as much as possible. With a bit of luck I should have it working in a couple of weeks - then some further tidy up and tweaking.


---
**Gus Montoya** *April 14, 2015 06:10*

**+Oliver Seiler**  Well I'm sure it's a minor set back. Feeling excited for your build. Your almost done!


---
**Dat Chu** *April 14, 2015 14:38*

Baby delay is still in progress. A friend suggests a build party so he can come over and help me build. Not sure yet since a toddler is unpredictable. 


---
**Gus Montoya** *April 14, 2015 15:21*

Yeah I hear you. My little cousins were sick a lot. Spent many nights trying to make them sleep.


---
**Dat Chu** *April 14, 2015 15:28*

Given that you are building a Eustathios, perhaps someone here can help you print those components? Also, can the frames be installed without the printed components?


---
**Gus Montoya** *April 14, 2015 15:41*

**+Dat Chu**  I already built the frame. But I can't install the linear rods and other components.


---
**Dat Chu** *April 14, 2015 16:54*

I think someone here might be up to helping you print this components in ABS. 


---
**Gus Montoya** *April 14, 2015 17:17*

I just messaged a fellow member who lives rather close to me compared to you or many other members. I will see what he says.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/4RtuNL2pkFK) &mdash; content and formatting may not be reliable*
