---
layout: post
title: "Redoing the HercuLien heated bed today with the proper adhesives"
date: February 20, 2016 22:15
category: "Show and Tell"
author: Eric Lien
---
Redoing the HercuLien heated bed today with the proper adhesives. Last time I used a light duty carpet tape to bond the PEI sheet to the aluminum taped window glass. This time I'm using the proper 3m 468 MP adhesive tape from [zoro.com](http://zoro.com). The light duty carpet adhesive worked great up until about 95C where it would let loose.



The nice thing about adding the aluminum tape is if I use an inductive level sensor later it will allow closer sensing than trying to read through both the PEI sheet and the glass. I know other people have said the aluminum tape doesn't work, but Alex Lee recommended it to me and it does work great. It's a great combination, the flatness of glass and the ease of removal for different bed treatments.



![images/f27c5506fdfe2d05eeeda75196a67718.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f27c5506fdfe2d05eeeda75196a67718.jpeg)
![images/068e8b29ad07f4e80ea5171af8ed40a2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/068e8b29ad07f4e80ea5171af8ed40a2.jpeg)
![images/dbf11597fa6e926c4cd521e0a6568705.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dbf11597fa6e926c4cd521e0a6568705.jpeg)

**Eric Lien**

---
---
**Alex Lee** *February 20, 2016 23:45*

**+Eric Lien** I really want to build a Herculien... Wife's not going to be happy... LOL


---
**Jim Stone** *February 20, 2016 23:47*

your wife may have a collection of shoes.....that may a be a good point to start with...or a bad one...


---
**Alex Lee** *February 20, 2016 23:50*

**+Jim Stone** Hmmm, maybe I can try to print some shoes with it... LOL


---
**Jim Stone** *February 20, 2016 23:53*

Lol. Not the angle I was going for...but it might work **+Alex Lee**​ lol


---
**Luis Pacheco** *February 20, 2016 23:53*

**+Alex Lee** Get flex working you might be able to! 
{% include youtubePlayer.html id=Zdoay2bo0yo %}
[https://www.youtube.com/watch?v=Zdoay2bo0yo](https://www.youtube.com/watch?v=Zdoay2bo0yo)


---
**Luis Pacheco** *February 20, 2016 23:55*

I've never heard of PEI **+Eric Lien** Would someone be able to explain what's being done here? Confused with the PEI, aluminum tape, and 3M adhesive.


---
**Alex Lee** *February 21, 2016 00:02*

**+Luis Pacheco** The PEI (or Ultem) can be used for part adhesion, and so far it has been the best option I've tried (hair spray, glue stick, Buildtak, praying...). You need the 3M adhesive to set the PEI on the glass surface. The aluminum tape is useful if you're using an inductive probe for the auto leveling feature some firmwares offer, the aluminum tape will be as flat as your glass surface, rather than trying to tram your bed against the aluminum bed (if you have one) which is usually not really flat.


---
**Eric Lien** *February 21, 2016 00:09*

**+Alex Lee** couldn't have said it better myself


---
**Alex Lee** *February 21, 2016 00:10*

**+Eric Lien** Hehehe, I'm learning...


---
**Luis Pacheco** *February 21, 2016 00:22*

**+Alex Lee**​ That explanation was very helpful, definitely a great ELI5. Thanks!


---
**Zane Baird** *February 21, 2016 01:53*

**+Eric Lien** Did you have a use 2 separate sheets of PEI or did you find a supplier who makes them more that 12" wide?


---
**Oliver Seiler** *February 21, 2016 02:10*

So, just to confirm, your stack would look like this?

heater - aluminium plate - glass - aluminium tape - 468 tape - PEI


---
**Eric Lien** *February 21, 2016 02:18*

**+Oliver Seiler** Exactly! It is a heck of a sandwich :)



 But really pretty simple to replace my PEI glass for regular glass, or other materials as the need arises. 


---
**Eric Lien** *February 21, 2016 02:19*

**+Zane Baird** I got the 0.040"x24"x24"thick stuff from Zoro:



[http://www.zoro.com/ultem-natural-grade-pei-sheet-stock-24-x-24/g/00101343/](http://www.zoro.com/ultem-natural-grade-pei-sheet-stock-24-x-24/g/00101343/)



[http://www.zoro.com/ultem-sheet-stck-24-in-w-24-in-l-0040-in-t-1nrh8/i/G2846611/](http://www.zoro.com/ultem-sheet-stck-24-in-w-24-in-l-0040-in-t-1nrh8/i/G2846611/)


---
**Joe Spanier** *February 21, 2016 03:56*

McMaster Carr sells PEI in sizes of really anything you want. After 24*24 you really start paying for it though. 


---
**Zane Baird** *February 21, 2016 04:12*

**+Joe Spanier** The largest PEI sheet that McMaster sells is 12"x24" from what I can see. That was one of the first places I looked.


---
**Joe Spanier** *February 21, 2016 04:45*

I stand corrected. I swore they sold bigger sheets


---
**Eric Lien** *February 21, 2016 07:21*

**+Ashley Webster** on the Delta I use the razor tool designed by **+Walter Hsiao**​ ([http://www.thingiverse.com/thing:1072713](http://www.thingiverse.com/thing:1072713)) to remove prints that stick too well...and even with using a razor it has held up surprisingly. It is a hard plastic, scratch resistant (within reason).



I can print ABS right on it with zero bed warp on the Delta in open air in my office during Minnesota winters with a temperature of 16C in the office on cold nights.



I am so happy I made the switch. I always hated the glues, slurries, kaptons, hairspray. I just wipe it down with acetone once in a blue moon to remove any hand oils... Hit print, and walk away.




---
**Oliver Seiler** *February 21, 2016 07:38*

How's the tolerance on the thickness of the PEI sheet **+Eric Lien** as  +0.025"/-0.000" doesn't sound too good?


---
**Eric Lien** *February 21, 2016 07:46*

**+Oliver Seiler** consistency is great. They are giving the tolerance of what they are providing. So they guarantee it will be 0.040" thick, -0.000" (i.e. not undersized), but up to as thick as 0.065". But that is a CYA tolerance if you ask me. Mine is spot on 0.042" everywhere I can measure.


---
**Oliver Seiler** *February 21, 2016 08:04*

Good to know.


---
**Vic Catalasan** *March 15, 2016 17:32*

I have recently printed large item using the PEI sheet and was extremely happy with it. I wanted to know if the glass is really needed? I have 1/4 aluminum tooling plate which is very flat and will try applying the PEI sheet directly. I too notice the extended wait for the PEI to reach stable temp with glass in between. I also tried PEI on a .060 aluminum sheet and place over the aluminum head bed, it heated up faster than on glass but both 060 aluminum and PEI warped during large ABS prints. 


---
**Eric Lien** *March 15, 2016 23:51*

**+Vic Catalasan** when measured with a dial indicator I still get cupping on the 1/4" cast aluminum tooling plate when heated to ABS temps. That's why I stick with the glass. But YMMV.


---
**Joe Spanier** *March 16, 2016 00:15*

Need a bigggggg chunk of aluminum to not change shape with all that heat. 


---
**Eric Lien** *March 16, 2016 03:17*

**+Ashley Webster** [http://i.imgur.com/77PnIXU.jpg](http://i.imgur.com/77PnIXU.jpg)



They are my hinged corner clips. They are on the github but you will need to tune the thickness to meet your needs based on the as-built thickness. Mine are designed as a snug fit when closed, but easily open to remove the bed.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/cSZcrhYp6LL) &mdash; content and formatting may not be reliable*
