---
layout: post
title: "Printing some big stuff for a friends company :) It's times like this that the \"go big or go home\" nature of the HercuLien sure comes in handy :)"
date: February 27, 2016 01:40
category: "Show and Tell"
author: Eric Lien
---
Printing some big stuff for a friends company :)



It's times like this that the "go big or go home" nature of the HercuLien sure comes in handy :)



![images/d5d8c4373e7e8f40e6d27882efaff48c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d5d8c4373e7e8f40e6d27882efaff48c.jpeg)
![images/832a114c59fb4370e4d450cf92fd408a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/832a114c59fb4370e4d450cf92fd408a.jpeg)

**Eric Lien**

---
---
**Zane Baird** *February 27, 2016 02:51*

Wow... ABS? One day I'll use the entire build area offered by the Herculien, one day...


---
**Eric Lien** *February 27, 2016 02:54*

**+Zane Baird** PETG


---
**Zane Baird** *February 27, 2016 02:57*

Ah, I see. The white eSun PETg? (that for some reason I can't get working to my satisfaction)


---
**Eric Lien** *February 27, 2016 05:19*

**+Zane Baird** it is a tricky one for sure. White has been the most difficult PETG for me too. Must have something to do with the colorant. Too cold and extrusion balls up and strings. Too hot and it burns and smears if there is any over extrusion or curling causing discoloration on the prints.


---
**Vic Catalasan** *March 08, 2016 07:39*

Nice printing! Is that PEI sheet your printing on? I was looking for larger than 12x12 sheets


---
**Eric Lien** *March 08, 2016 08:56*

**+Vic Catalasan** yes it is. I found 24" x24" sheets of PEI reasonably priced on [zoro.com](http://zoro.com).


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/4iEnkspzJbo) &mdash; content and formatting may not be reliable*
