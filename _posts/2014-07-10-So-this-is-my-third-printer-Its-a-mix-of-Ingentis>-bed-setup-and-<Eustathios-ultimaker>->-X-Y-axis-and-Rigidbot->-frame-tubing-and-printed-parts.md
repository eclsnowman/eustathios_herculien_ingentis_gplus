---
layout: post
title: "So, this is my third printer. Its a mix of Ingentis> bed setup and <Eustathios/ultimaker> > X/Y axis and Rigidbot > frame tubing and printed parts"
date: July 10, 2014 13:09
category: "Show and Tell"
author: Jean-Francois Couture
---
So, this is my third printer. Its a mix of Ingentis> bed setup and <Eustathios/ultimaker> > X/Y axis and Rigidbot > frame tubing and printed parts.  A short vid while printing a part @ 100mm/s. Tell me what you think ;-)﻿





**Jean-Francois Couture**

---
---
**Jean-Francois Couture** *July 10, 2014 13:10*

This one should work :-)


---
**Ricardo de Sena** *July 10, 2014 13:26*

Good!!!


---
**Carlton Dodd** *July 10, 2014 13:48*

Very nice.  I'd like to see more detail on your build.


---
**Chet Wyatt** *July 10, 2014 13:54*

What does that "Kill-A-Watt" tell you about power consumption on that printer :)


---
**Jean-Francois Couture** *July 10, 2014 13:58*

**+Chet Wyatt**  around 2.3A when running and 5.2A when bed is heating up. (the heat pad is on 120Volts along side the powersupply line.. thats why its on the kill-a-watt.. to see that I dont go over the cable's rating (8Amps)


---
**Jean-Francois Couture** *July 10, 2014 13:59*

**+Carlton Dodd** sure, what would you like to know ?


---
**Carlton Dodd** *July 10, 2014 15:00*

Oh, just some more about the parts you used, any problems/advantages.  I'm looking around for a design to build.  Debating between a delta and this type of design.


---
**Jean-Francois Couture** *July 10, 2014 18:31*

**+Carlton Dodd** Well, the frame is 1/8" (3.2mm) square tubing. I have 2x10' + 1x48" in all.. some parts are 1/16" tubing to have more space in the tube to allow more wiring (like the to ones for the LCD / microSD / leds). I design the plastic parts my self. some of the pulleys/belts was purchased from robodigg, rods from Misumi, J-head from a local 3D company,the rest is from ebay. the headbed heater kapton/rubber pad is custom made from a china seller on ebay.. 120V/500W giving me around 10W/inch² of heat.


---
**James Rivera** *July 10, 2014 21:49*

**+Jean-Francois Couture**  Nice! It looks like a clean design. How are you fastening your plastic parts to the square tubing? Did you drill holes, or do the screws just press against the tubing?


---
**Jean-Francois Couture** *July 11, 2014 12:09*

**+James Rivera**  the major parts are trough holes.( I also build a jig that i could screw down on my drill press table so every tube was dead on.) 



the top holder for the power supply is a screw that is pressing against the tubing.. its amazing how strong it can get and not even braking the plastic or stripping threads.


---
*Imported from [Google+](https://plus.google.com/105576148076542448710/posts/YoDfNuZZ4Gz) &mdash; content and formatting may not be reliable*
