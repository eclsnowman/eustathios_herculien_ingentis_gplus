---
layout: post
title: "have any of you considered using a tablet with usb for the printer"
date: August 23, 2015 14:00
category: "Show and Tell"
author: E. Wadsager
---
have any of you considered using a tablet with usb for the printer. i have tried it and it works fine, and there is a lot of opportunities. 

![images/fbb4fe37fb07964ced579d8242458347.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fbb4fe37fb07964ced579d8242458347.jpeg)



**E. Wadsager**

---
---
**Jeff DeMaagd** *August 23, 2015 14:36*

I know several that have. There aren't a lot of tablet optimized programs. I had tried talking someone I know into helping develop one. But I don't know what software you're showing there. That looks promising.


---
**E. Wadsager** *August 23, 2015 14:42*

its called gcodeprintr 


---
**Jim Wilson** *August 23, 2015 15:25*

Octodroid works fairly well too, I recently moved control of my i2 to a raspberry pi and controlling it from an old tablet or phone is a great way to repurposed the tech and free up a laptop/desktop from printer duty.﻿



[https://play.google.com/store/apps/details?id=com.mariogrip.octoprint](https://play.google.com/store/apps/details?id=com.mariogrip.octoprint)


---
**Eric Lien** *August 23, 2015 15:56*

I used this for a while, but moved back to octoprint on a RPI2.



[https://plus.google.com/+EricLiensMind/posts/CSVZc4vW9Be](https://plus.google.com/+EricLiensMind/posts/CSVZc4vW9Be)




---
**Robert Burns** *August 23, 2015 21:18*

The biggest issue with GcodePrintr, is that 99% of tablets charge with their usb port...the port you need to print with. You need a tablet with DC charge port, or try one of the "probably not going to work" workarounds.


---
**E. Wadsager** *August 24, 2015 07:35*

my china tablet have, hdmi, usb in, ubs out and charger port. 


---
*Imported from [Google+](https://plus.google.com/103157635215674778495/posts/5q8tW7cSNDB) &mdash; content and formatting may not be reliable*
