---
layout: post
title: "T slot Aluminum Extrusion n V slot Aluminum Extrusion"
date: March 26, 2016 15:01
category: "Show and Tell"
author: Makeralot
---
T slot Aluminum Extrusion n V slot Aluminum Extrusion





**Makeralot**

---
---
**Eric Lien** *March 26, 2016 15:48*

Thank you for the post. Please note this  community is for sharing builds, techniques, etc. I will let the post stay for now. But in the future do not post advertising only content or posts will be removed. Please feel free to join our discussions and be an active community member.



Take care,



-Eric﻿


---
**Makeralot** *March 28, 2016 16:03*

**+Eric Lien**  Thank you Eric, I got it . 


---
**Eric Lien** *March 28, 2016 16:22*

**+Makeralot** Thanks, Like for example I did not know you had a specific kit for the Eustathios printer here: 



[http://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207/](http://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207/)



and the HercuLien kit here:



[http://www.makeralot.com/aluminum-extrusion-kit-for-herculien-2020-2080-v-slot-p215/](http://www.makeralot.com/aluminum-extrusion-kit-for-herculien-2020-2080-v-slot-p215/)



This sort of thing is exactly what people here would be interested in and is a great fit as a post into the community. Many people here have asked for a single source for Eustathios and HercuLien kits. If this is something you could help with I know the community would appreciate it greatly. 



There are also some cross drilled holes required for Eustathios and HercuLien to allow an Allen Wrench to pass through for bolting together parts. Are these included at that kit price? If so that is a very good deal depending on shipping costs and the accuracy of the cuts which is critical on this type of frame construction.


---
**Eric Lien** *March 28, 2016 16:28*

You can see an example of the holes in the documentation. But also the 3D model is the best source because they are in the correct position in the actual extrusions right in the assembly.



[http://imgur.com/a/Yy0dw](http://imgur.com/a/Yy0dw)


---
**Jim Stone** *March 28, 2016 16:37*

Pretty neat to have specific kits. That would have helped a bit xD﻿



Wow AND all vslot too


---
**Chris Brent** *March 28, 2016 16:53*

Nice to have a kit! Might as well have the T-nuts and bolts in the there too though? The price is amazing for the Euth, does the Herc really have 4X the extrusion?


---
**Eric Lien** *March 28, 2016 17:04*

**+Chris Brent**​ most of the extrusion is 20x80 on HercuLien. So I assume that's the main source of the price difference. Also it uses v-slot as the Z-axis vertical guides. So that offsets cost against some 10mm rods and bearings for Z in the Eustathios BOM.﻿


---
**Makeralot** *March 29, 2016 07:10*

**+Eric Lien** Yes, we are happy that you guys like the two specific extruded aluminum kits, our purpose is try to provide some reasonable price of parts for people here. The price was not included drilling services, I will try to work out where and how to drill holes for the kits, then will include these into the price




---
**Eric Lien** *March 29, 2016 13:10*

**+Makeralot** sounds good. If you run into any questions let me know. Also if you are open to generation of a complete kit (corner brackets, T-nuts, bolts, rods, bearings, etc) let me know and I will work with you on the quantity's.


---
**Makeralot** *March 29, 2016 15:48*

**+Eric Lien** Thanks. I am happy if you can help me with the quantity's  for the mating parts. And I am glad to generate the complete kit.


---
**Eric Lien** *March 29, 2016 16:13*

**+Makeralot**  I will make a simplified BOM of the main components for each that I think would make a good kit as well as drawings and qty's for the hole locations to be drilled in the extrusion so you can generate a cost.



This is very exciting. I know people have been looking for kits for these for a long time. This should lower the barrier to entry, and if they can get many of the from a single source it should help people save on the shipping costs as well.



I will message you in the next few days with some detailed information about the kits, and maybe there could be a few versions (extrusion only, extrusions and hardware, full kit with motors etc.)


---
**Chris Brent** *March 29, 2016 16:21*

Well this is exciting! :)


---
**Makeralot** *March 30, 2016 03:07*

**+Eric Lien** Great! Waiting for the detail information. 


---
**Matthew Kelch** *April 01, 2016 22:23*

Very exciting news!


---
**Eric Lien** *April 02, 2016 17:10*

I am just getting started. Trying to get a good balance of family and 3d printing. But here is a BOM for the frame. I will make a print for the hole locations in the extrusion next.



[https://drive.google.com/open?id=0B1rU7sHY9d8qelBud2JUS0tkY1k](https://drive.google.com/open?id=0B1rU7sHY9d8qelBud2JUS0tkY1k)


---
**Makeralot** *April 03, 2016 15:36*

Great!  As the inner diameter of our T slot aluminum extrusion is 5mm, with our extrusion kit for the Eustathios, I would recommend M6 x 16  screws (item 3). And end fastener is also a good choice [http://www.makeralot.com/end-fastener-for-20mm-aluminum-extrusion-p224/](http://www.makeralot.com/end-fastener-for-20mm-aluminum-extrusion-p224/)


---
**Sean B** *May 20, 2016 01:27*

**+Makeralot** So 6 mm bolts should be used for the extrusion?  or is this just for the end?  I couldn't find any 6 mm bolts on your site.


---
**Makeralot** *May 20, 2016 15:03*

**+Sean B** 6mm bolt is for T slot extrusions end, the inner hole size of our T Slot extrusions is 5mm , ([http://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207/](http://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207/)), after tapping, should use M6 bolt for the end holes. Recommend 20 series end fastener for you ( [http://www.makeralot.com/end-fastener-for-20mm-aluminum-extrusion-p224/](http://www.makeralot.com/end-fastener-for-20mm-aluminum-extrusion-p224/)). If you want to buy M6 bolt separately, we can add it on the page. :)  Any question please feel free to leave me messages or email us.


---
*Imported from [Google+](https://plus.google.com/+Makeralotcom/posts/9wrerHj33rC) &mdash; content and formatting may not be reliable*
