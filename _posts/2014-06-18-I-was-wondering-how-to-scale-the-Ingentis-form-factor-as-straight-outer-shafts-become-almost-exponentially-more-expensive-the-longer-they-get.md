---
layout: post
title: "I was wondering how to scale the Ingentis form factor as straight outer shafts become almost exponentially more expensive the longer they get"
date: June 18, 2014 20:57
category: "Show and Tell"
author: Tim Rastall
---
I was wondering how to scale the Ingentis form factor as straight outer shafts become almost exponentially more expensive the longer they get.  This is one of a number of ideas I have to get around it.  The horizontal extrusion is vslot. Thought **+Shauki Bagdadi** might like it particularly.  KISS Kim! 

![images/1187b5258d562bb92b64759b40a2652b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1187b5258d562bb92b64759b40a2652b.jpeg)



**Tim Rastall**

---
---
**Daniel Fielding** *June 18, 2014 21:07*

I was looking at doing something very similar to that with makerslide. Now that there is a supplier of makerslide in Australia I might just do it instead of ingentis/eustathios.


---
**Mike Miller** *June 19, 2014 00:56*

**+Shauki Bagdadi** what was your motivation for going dual stepper?


---
**Dale Dunn** *June 19, 2014 01:03*

That looks like my zillion-bearing version, turned inside out and using makerslide.


---
**Nazar Cem** *June 19, 2014 02:32*

How about removing smooth rods completely and moving a v-slot x gantry?  I was hoping to test another idea, but maybe someone can before I me. If you put v-slot as all four vertical supports, and put two bearings on all four corners of the z platform, it may add a lot of stability to it. I would also like to find an alternative to v-slot because at 3 dollars a wheel it'll get expensive.


---
**Tim Rastall** *June 19, 2014 02:47*

**+Nazar Cem** I've got a Z axis design using belts that uses 4 corner supports as you describe.


---
**Tim Rastall** *June 19, 2014 07:44*

Oh Snap.  I just realised the vslot and carriages in this design could be replaced by 12mm linear shafts with one lm12luu for each x/y end.  Not perfect for  a very large bot but 600mm should be easy. Total cost for 12mm rails and bearings would be about 50 including shipping from China,  vslot and carriages more like 120 with open builds vslot. . You'd need 8mm hollow aluminium shafts that drive the GT2 Belt and, 608zz bearings and probably 10mm aluminium shaft for the cross beams to keep weight low. Possibly igus Iglide even. Hmm hmm. Could work out pretty cheap for a 550 square build volume.

I now await **+Shauki Bagdadi** to turn up and tell me why I should use box section and mason cord :p. 






---
**Tim Rastall** *June 19, 2014 08:41*

**+Shauki Bagdadi**  AH, I get the twin drive thing without shafts (hadn't understood previously). The problem with your suggestion is that you would have built 3 bots before I had finished one - there is no spare time with a full time job and high energy Toddler! 


---
**Mike Miller** *June 19, 2014 11:32*

**+Shauki Bagdadi** Okay, now this is pretty interesting, You're moving away from the Quad/Rap/Strap gantry in favor of something similar to the Ingentis (printhead floating on round rod)...I'm at the point where I'm either buying a TON of bearings, building cheap but non-precision substandard bearings...ooor I could just use all of the Ingentis parts that have already been printed for me. I spent last weekend working on structural aspects of the printer and I'm at a fork where I continue with a Quadrap design, or go Ingentis. 


---
**Mike Miller** *June 19, 2014 11:49*

I liked the simplicity of the bushing/shaft structure on the Ingentis, and the difference in price is probably less than $100 (he says with a wink)....I'm going to have to re-evaluate.


---
**Mike Miller** *June 19, 2014 11:56*

That's the thing, though, the ingentis uses the 8mm and 10mm shafts to keep everything in alignment...There's not a lot of twist to a solid steel rod across the length of the printer. ;)



To be honest, I'm a little jealous for the printers that spend a little more on GT2 belts and hide the motors in the electronics bay in the base...such a clean design...


---
**Mike Miller** *June 19, 2014 12:11*

OW! She may not look like much, but she's got it where it...well, no, she really doesn't. :)



Still, I know why a lot of 3d printer videos have a 'wall of fame' in the background with all of their retired printers. This one has taught me a LOT. 


---
**Mike Miller** *June 19, 2014 12:20*

I have everything but the precision ground stuff (rods and bearings) which makes this the inflection point...but it really doesn't have anything to do with scaling ingentis, I'll start another thread. 


---
**Joe Spanier** *June 19, 2014 13:44*

So in this design, could you use Drill rod for your drive shafts, and only splurge for nice stuff on your cross rails? That said Ive had great luck running drill rod as my bearing rods using the cheapo igus sleeve bearings. Opinion?


---
**Joe Spanier** *June 19, 2014 14:35*

Ive never tried alumium with sleeve bearings other then the igus iglide stuff thats ridiculously expensive (38. I can get 12mm drill rod for 5$ for a meter. Aluminum is about the same but not as stiff.  What bearings are you using for your aluminum **+Shauki Bagdadi**?


---
**Joe Spanier** *June 19, 2014 14:40*

well then lol 


---
**Joe Spanier** *June 19, 2014 14:46*

ok so is there a benefit to tube over rod stock? Other then the weight drop? Because rod is a 1/3 of the price from where I get it. 


---
**Joe Spanier** *June 19, 2014 14:52*

lol Yea I saw. Just silly fast. I guess more xy mockups are in my future. 


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/JGSEdCLrNLy) &mdash; content and formatting may not be reliable*
