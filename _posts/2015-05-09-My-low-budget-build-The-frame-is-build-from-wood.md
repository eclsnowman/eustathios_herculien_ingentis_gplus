---
layout: post
title: "My low budget build. The frame is build from wood"
date: May 09, 2015 19:28
category: "Show and Tell"
author: E. Wadsager
---
My low budget build. The frame is build from wood. 

The build volume is about 300x300x250



![images/99c0712cd35a78725bdf34413b2d6658.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/99c0712cd35a78725bdf34413b2d6658.jpeg)
![images/c71f89856cc18b9ca2f20943d8130ccc.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c71f89856cc18b9ca2f20943d8130ccc.png)
![images/592a95920337511c892c80bc15891eda.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/592a95920337511c892c80bc15891eda.jpeg)
![images/3cd61b28dcd18aebe81f2ce095b211bc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3cd61b28dcd18aebe81f2ce095b211bc.jpeg)

**E. Wadsager**

---
---
**Eric Lien** *May 09, 2015 19:32*

Love it. Great job.


---
**SalahEddine Redjeb** *May 10, 2015 08:35*

looks gorgeous


---
*Imported from [Google+](https://plus.google.com/103157635215674778495/posts/bEamJ7UDTB6) &mdash; content and formatting may not be reliable*
