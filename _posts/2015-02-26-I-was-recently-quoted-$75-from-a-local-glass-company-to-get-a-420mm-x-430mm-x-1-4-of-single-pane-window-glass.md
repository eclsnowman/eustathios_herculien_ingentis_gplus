---
layout: post
title: "I was recently quoted $75 from a local glass company to get a 420mm x 430mm x 1/4\" of single pane window glass"
date: February 26, 2015 23:02
category: "Discussion"
author: Dat Chu
---
I was recently quoted $75 from a local glass company to get a 420mm x 430mm x 1/4" of single pane window glass. I did ask them to chamfer the piece so I don't cut myself handling it.



I feel that this is rather steep given that Home Depot is selling a 24"x24" for $10. How did you guys get the glass bed? Any recommendation on chamfer-it-yourself ?





**Dat Chu**

---
---
**Daniel Salinas** *February 26, 2015 23:03*

there was a vendor online someone recommended after I paid $127 dollars for a pyrex sheet.  But $75 for a piece of window glass is just silly.



let me see if I can dig up the link.


---
**Daniel Salinas** *February 26, 2015 23:05*

[http://www.voxelfactory.com/products/custom-cut-borosilicate-glass](http://www.voxelfactory.com/products/custom-cut-borosilicate-glass)


---
**Derek Schuetz** *February 26, 2015 23:05*

Honestly cutting straight cuts in glad is easy just give it a tryi was discouraged at first too


---
**Tim Rastall** *February 26, 2015 23:06*

That's what I would expect to pay for a piece of borosilicate float glass. Try somewhere else. I would recommend getting toughened stuff.


---
**Dat Chu** *February 26, 2015 23:06*

Well they are quoting me $650 to fix two 3' by 4' double pane windows (all included) so I thought their prices are reasonable. 


---
**Daniel Salinas** *February 26, 2015 23:11*

for the herculien you need a 1/8" sheet IIRC. **+Eric Lien** will a 1/4" sheet fit in the bed glass clips?


---
**Daniel Salinas** *February 26, 2015 23:19*

Also it should be 420mm x 430mm, not 420mm square


---
**Dat Chu** *February 26, 2015 23:25*

I was told by the glass company that 1/8" will be very easy to break so I should watch out. And yes I mistyped, 420mm by 430mm.


---
**Dat Chu** *February 26, 2015 23:25*

**+Tim Rastall** , do you mean tempered? What is the keyword I should ask for when I call around. Talking about calling around, I need the metal brackets for the bed mount too :(


---
**Tim Rastall** *February 26, 2015 23:31*

**+Dat Chu** borosilicate float glass is what I ask for. 


---
**Mike Miller** *February 26, 2015 23:35*

I think I paid $12 a piece for 1/4 " plate window glass. It has worked fine. ﻿edit:12" *12" 


---
**Dat Chu** *February 26, 2015 23:40*

I dug through the posts that **+Eric Lien** made over the years and he mentions only getting a simple piece of window glass to go on top of the aluminum plate. Hmm.


---
**James Rivera** *February 26, 2015 23:56*

I just went ti the local Ace Hardware store and asked for an 8"×8" piece of glass. I also asked him to hack a little bit off the corners. I don't remember off hand exactly what I paid for 3 if them, but it was dirt cheap, like less than $20 for all 3.


---
**James Rivera** *February 26, 2015 23:57*

And I've been using them for well over a year now. Currently using one on top of my heated aluminum bed and the probe is now sensing the aluminum underneath the 1/4" glass just fine.


---
**Dat Chu** *February 27, 2015 00:00*

I need to go to ace anyway. Sounds like a trip coming up. Do you know if ace also sell the L brackets? 


---
**James Rivera** *February 27, 2015 00:26*

**+Dat Chu** I'm not sure which L brackets you need, but I'm pretty sure they do sell L brackets.


---
**Eric Lien** *February 27, 2015 00:42*

I have had boro glass, and even higher temp neoceramic glass before. But currently I am using standard window glass. Avoid tempered glass, it is less ductile than standard glass and is more prone to cracking. The best trick is to avoid too much force CLAMPING the glass to the bed. On large pieces of glass  the difference in compression around those metal binder clips was what I determined to be the cause of cracks using window glass on my old corexy.



I have been using standard window glass on HercuLien now for 6 months with no sign of issues.﻿ 1/4" thick is unnecessary and an added insulator from the heat spreader which isn't good.


---
**Dat Chu** *February 27, 2015 01:14*

So 1/8" then? or 3/16"? 



**+James Rivera** "ASTM B221-08 6063-T52 Aluminum Architectural Unequal Leg Angle 3-

1/2"" x 1-1/4"" x 1/8""" is what I am looking for.


---
**Eric Lien** *February 27, 2015 01:38*

**+Dat Chu**​ you can use 2"x2" in a pinch, that's what I have on mine but I wanted the wheels further apart. The key is architectural angle because the inside corner is square. If you can't find architectural angle just cut the 20x20 cross bars on the bed shorter so they don't hit the inside fillet and skip the tapped holes into the end of the extrusion.


---
**Dat Chu** *February 27, 2015 03:22*

I think I understand what you mean **+Eric Lien** . How did you get the holes into these aluminum architecture angle?



One thing the BOM doesn't say is how long :D. My guess would be 430mm ish?


---
**Mike Thornbury** *February 28, 2015 03:40*

a glass mirror is a good option - mirrors are very flat.



You can cut it to size yourself and use silicon carbide cloth to soften the edge. 


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/QmHyiLFsGwx) &mdash; content and formatting may not be reliable*
