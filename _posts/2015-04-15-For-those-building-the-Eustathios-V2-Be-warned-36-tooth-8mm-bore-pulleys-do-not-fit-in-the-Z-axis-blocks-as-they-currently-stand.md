---
layout: post
title: "For those building the Eustathios V2. Be warned, 36 tooth 8mm bore pulleys do not fit in the Z-axis blocks as they currently stand"
date: April 15, 2015 03:42
category: "Discussion"
author: Ben Delarre
---
For those building the Eustathios V2. Be warned, 36 tooth 8mm bore pulleys do not fit in the Z-axis blocks as they currently stand. Not a big deal, but 36 tooth are easier to find than 32 tooth.



Have ordered some 32 tooth replacements from ebay...z-axis on hold!





**Ben Delarre**

---
---
**Dat Chu** *April 15, 2015 03:49*

Thank you for the information. Can you give us a link to what you bought on eBay as well please?


---
**Ben Delarre** *April 15, 2015 03:57*

Sure thing. 



[http://www.ebay.com/itm/261834454745](http://www.ebay.com/itm/261834454745)



Not sure if they are any good but the shipping is a lot cheaper than robotdigg for just these pulleys. Obviously if you are ordering your whole set get them from robotdigg since the motor prices are great there and the shipping amortised over the whole order is not bad. But if you are rubbish like me and can't read a bom then these are not a terrible deal. 


---
**Gus Montoya** *April 15, 2015 05:29*

oh gee, 36 teeth don't fit???


---
**Ben Delarre** *April 15, 2015 05:46*

Yeah.  Well at least the ones I got from inventables don't. The rim of the pulley rubs on the extrusion. There's not much in it a mm or two smaller might fit fine.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/VG9WmzfQ5Mq) &mdash; content and formatting may not be reliable*
