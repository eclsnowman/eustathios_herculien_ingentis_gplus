---
layout: post
title: "I have inconsistent layer registration. Is this likely caused by belts?"
date: March 14, 2018 13:58
category: "Show and Tell"
author: Ryan Fiske
---
I have inconsistent layer registration. Is this likely caused by belts? Too tight? Too loose? Something else?

![images/4093f386271966afa4c666ce4e111b0f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4093f386271966afa4c666ce4e111b0f.jpeg)



**Ryan Fiske**

---
---
**Dennis P** *March 14, 2018 16:34*

How is it in the other direction? Which axis are we looking at? 


---
**Ryan Fiske** *March 14, 2018 20:38*

**+Dennis P** It's visible in both x and y. I actually just read your z height post and when I put in my stuff, my z heights are a little funny, so I might experiment with that


---
**Eric Lien** *March 14, 2018 21:01*

**+Ryan Fiske** How did the bearings fit into the corner bearing holders. Were they loose? If so you might want to look into putting a layer or two of thin tape around the bearing before pressing it into the corner bearing holders. It acts like a shim to take up the gap and make for a tight fit.



Have you done PID tuning on your temps yet? If not take a look at this post as to why that's a good idea.



[https://plus.google.com/+EricLiensMind/posts/LuS8izruZUS](https://plus.google.com/+EricLiensMind/posts/LuS8izruZUS)



Lastly, Have you calibrated your extruder E-steps with actual testing. You look heavily over extruded.



[http://reprap.org/wiki/Triffid_Hunter%27s_Calibration_Guide#E_steps](http://reprap.org/wiki/Triffid_Hunter%27s_Calibration_Guide#E_steps)




{% include youtubePlayer.html id=YUPfBJz3I6Y %}
[https://www.youtube.com/watch?v=YUPfBJz3I6Y](https://www.youtube.com/watch?v=YUPfBJz3I6Y)




---
**Ryan Fiske** *March 14, 2018 21:33*

**+Eric Lien** the bearings did have some slight play on install so I'll definitely take a look into shimming those.



I have PID'd with a setpoint at my current temp.



I have calibrated my extruder steps by extruding 100mm, measuring actual and adjusting steps based on the difference which is pretty close to what the triffid doc tells you to do. Perhaps I'll have to take a look at my extrusion multiplier, though if you're saying it's heavily over extruded, that might just really be a band-aid to a larger issue.


---
**Eric Lien** *March 14, 2018 22:23*

The more I look at that top surface, I might take back my heavily over extruded comment. 


---
**Eric Lien** *March 14, 2018 22:24*

Try checking all the grub screws. And make sure the hotend seems well secured.


---
**Eric Lien** *March 14, 2018 22:27*

Do you have the shims between the pulleys and the bearing blocks. Without the thin shims, the pulleys tend to rub on the bearing face in the corner bearing blocks.


---
**Ryan Fiske** *March 15, 2018 00:55*

**+Eric Lien** So I checked my hot end when I got home and it was loose! Not the nozzle, but the break to the heatsink. Tightened that up nice and snug. The next thing I did was I actually used that optimal layer height calculator and found out that 0.1 and 0.2 are NOT on a set step. I figured I'd give that a shot, figuring that maybe that might have something to do with it as well, and the print that I am doing right now looks much better! Which one it was, I am unsure of.


---
**Eric Lien** *March 15, 2018 01:34*

**+Ryan Fiske** great to hear


---
**Dennis P** *March 15, 2018 05:19*

**+Ryan Fiske** The Prusa calculator appears to have the gear cells swapped. Trust your original math, it was probably right. Walter straightened me out. 


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/XtntSAxQrUZ) &mdash; content and formatting may not be reliable*
