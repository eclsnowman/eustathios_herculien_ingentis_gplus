---
layout: post
title: "Hey all, just a quick note that I uploaded an \"Improved Eusthathios Spider 2 adjustable Z stop\" at: This one now has two screws (instead of one) to the extrusion to help keep it more stable"
date: April 17, 2017 03:41
category: "Mods and User Customizations"
author: Ted Huntington
---
Hey all, just a quick note that I uploaded an "Improved Eusthathios Spider 2 adjustable Z stop" at: [https://www.youmagine.com/designs/improved-eustathios-spider-v2-adjustable-z-stop](https://www.youmagine.com/designs/improved-eustathios-spider-v2-adjustable-z-stop)



This one now has two screws (instead of one) to the extrusion to help keep it more stable.

![images/a4502c0baaa9ce25266634ba567e8bc5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a4502c0baaa9ce25266634ba567e8bc5.jpeg)



**Ted Huntington**

---


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/5D9w5QEei9o) &mdash; content and formatting may not be reliable*
