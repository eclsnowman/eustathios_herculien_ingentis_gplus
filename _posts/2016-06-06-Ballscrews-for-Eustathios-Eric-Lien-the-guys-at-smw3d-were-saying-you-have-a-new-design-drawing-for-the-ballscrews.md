---
layout: post
title: "Ballscrews for Eustathios. Eric Lien the guys at smw3d were saying you have a new design/drawing for the ballscrews?"
date: June 06, 2016 18:25
category: "Discussion"
author: jerryflyguy
---
Ballscrews for Eustathios. **+Eric Lien** the guys at smw3d were saying you have a new design/drawing for the ballscrews? Just thought I'd ask before ordering them.





**jerryflyguy**

---
---
**Eric Lien** *June 06, 2016 18:50*

Here is the print i made for Brandon to cut the ballscrew for me. I added threads at the bottom so I can avoid having to use a collar on the bottom and can just use a nylock nut. Please note I haven't tested it yet, but I know others had good luck with the SFU 1204 ball screws. So I figured I had to give it a try. Also note that this print has two overall lengths. One is for the Eustathios Spider V1, and the other is for V2. In V2 I dropped the bottom plate and frame location to gain 30mm more Z. 



[https://drive.google.com/file/d/0B1rU7sHY9d8qb3Ayd3JaY0JTMWs/view?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qb3Ayd3JaY0JTMWs/view?usp=sharing)


---
**jerryflyguy** *June 06, 2016 19:42*

**+Eric Lien** thanks Eric! Much appreciated!


---
**Matthew Kelch** *June 07, 2016 22:07*

Can you actually them machined like this from SMW3D?


---
**Eric Lien** *June 07, 2016 22:22*

**+Matthew Kelch** I think Brandon is looking into equipment upgrades before taking orders. The price the market will handle for machining ball screws (compared to China prices) versus the labor cost of manual lathe work is the wrong ratio right now.


---
**jerryflyguy** *June 08, 2016 00:15*

It sounds like it could be a few weeks before the new lathe arrives. He mentioned the current machine is being a bit difficult.


---
**Matthew Kelch** *June 08, 2016 01:30*

Gotcha, I'll keep an eye out! Maybe they can post here when they are ready to accept orders?


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/VeAusFBvEGv) &mdash; content and formatting may not be reliable*
