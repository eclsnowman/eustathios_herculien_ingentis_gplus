---
layout: post
title: "Update on my build. Slowly working out where everything goes and hopefully hard wiring everything shortly"
date: May 03, 2018 20:55
category: "Build Logs"
author: Julian Dirks
---
Update on my build. Slowly working out where everything goes and hopefully hard wiring everything shortly. End stops in and working. Have had the z axis moving. Got a replacement screw from Golmart - straight as an arrow but it has a little rough spot which is a bit disappointing. Am going to have mains heated bed - got a keenovo heater for that. 10mm cast Ali plate (worried about the weight) - going to try 3 point levelling. Also got a panel due. A few more mounting parts to finish designing/ print and I’ll take the bond tech off my wanhao and put it on this one. Hopefully post a break in vid in a week or so!

![images/db50786c107801f99ebcf3e043f537bf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/db50786c107801f99ebcf3e043f537bf.jpeg)



**Julian Dirks**

---
---
**Eric Lien** *May 03, 2018 21:59*

Glad to see you are still making progress. Excited to see the first print.



BTW does the ball screws rough spot seem to be at the bottom where the lathe would chuck onto the OD to turn down the end? If so I might contact them again to see what they say.


---
**Oliver Seiler** *May 03, 2018 22:02*

Nice! Where did you get the 10mm plate from?




---
**Julian Dirks** *May 03, 2018 22:40*

**+Eric Lien** No it's on the actual screw.  It's not bad but there.  I switched the nut back onto bent screw and it's fine so it's not the nut.  This is a bit like the situation with rods.  I read that people had got bent ones and had hassles with tolerances etc so I bought those from Misumi and they are perfect first time.


---
**Julian Dirks** *May 03, 2018 22:42*

**+Oliver Seiler** Little metals in ChCh. I think it's called Formodium 30.  Really good service and fast shipping.  Was $82 for the 10mm plate inc courier.  He has 6 and 10 but not 8mm


---
**Oliver Seiler** *May 03, 2018 23:09*

Awesome, keen to see how that works for you.


---
**Dennis P** *June 09, 2018 04:21*

Julian, how is your build going?


---
**Julian Dirks** *June 09, 2018 06:53*

Hi Dennis, getting there thanks. Extruder and hot end in. Wiring is complete and have been running break in code. This afternoon I’ve been working on mounting the bed (3points) and should have that in tonight. My PEI isn’t here yet and I’ll wire up the bed heater (220v)once everything else is done. I’ll post a picture tonight if I get the bed in. Cheers


---
**Julian Dirks** *June 09, 2018 09:25*

Got the bed in and have trammed it. Tempted to tape on the spare “buildtak” that came with my wanhao so I can bang out a print.....![images/73b6accf1a3b4faaf93b9f53c055caaf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/73b6accf1a3b4faaf93b9f53c055caaf.jpeg)


---
*Imported from [Google+](https://plus.google.com/113795478307151372873/posts/1e3sBrc6YPx) &mdash; content and formatting may not be reliable*
