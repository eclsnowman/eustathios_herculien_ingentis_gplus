---
layout: post
title: "Has someone yet built a carriage for E3D and bed levelling?"
date: December 15, 2014 10:02
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Has someone yet built a carriage for E3D and bed levelling? How do you guys do Bed levelling on your Eustathios Builds? Still finishing my outer frame but yet thinking about how to best do the carriag and auto bed levelling is a must have for me.

![images/100eb851c6e2b418a5e1c58221166049.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/100eb851c6e2b418a5e1c58221166049.png)



**Frank “Helmi” Helmschrott**

---
---
**Wayne Friedt** *December 15, 2014 10:30*

Good start.


---
**Daniel F** *December 15, 2014 11:54*

3 point bed leveling with a spring and knurled nut. I don't have an eusthatios (I built a Quadrap), but the bed size is about the same. Leveling can be done from underneath, see here: [https://plus.google.com/u/0/photos?cfem=1&pid=6034927126307058530&oid=111479474271942341508
M5x50](https://plus.google.com/u/0/photos?cfem=1&pid=6034927126307058530&oid=111479474271942341508%0AM5x50) countersunk screws from top, insert nut from bottom, two knurrled nuts tightened against each other to adjust the level (this way you can move the screw from underneath without a screwdriver

With 3 points, there is no over constraint, you can never bend the bed. Works well provided you have access from at least one side. With this setup you can even tweak leveling while printing the 1st layer.


---
**Frank “Helmi” Helmschrott** *December 15, 2014 12:09*

Oh maybe i just wasn't clear enough - i want to integrate automatic bed levelling (with a microswitch or something similar in the carriage - maybe any other way). Manual bed levelling isn't an option anymore after i did some automatic on other printers already.


---
**Brad Hopper** *December 15, 2014 13:02*

You probably still want manual adjustment to get things set initially. No matter how careful you are half a mm in frame unevenness will make a big difference without the ability to dial the bed. The autolevelling just compensates, doesn't fix these issues.


---
**Brad Hopper** *December 15, 2014 13:05*

I have had good success with I ductive sensor (get an 8mm range one) like Printrbot uses although I just saw one in person this weekend and their sensor is much smaller. Have also seen a very nice hall sensor based effector sensor somewhere. I think the mechanical switches tend to get out of alignment over time both as endstops and autoleveling sensors.


---
**Frank “Helmi” Helmschrott** *December 15, 2014 13:32*

I have to say i have a rather good experience with not using springloaded mounts when using auto bed levelling. Sure thing this is a bit tricky when building the machine so you be as much level as possible for a good starter but that worked quite well for me on different printers. Not having spring loaded bed mounts often make things a bit easier when mounting the bed.



Regarding the inductive sensors - you're right, they tend to be more reliable on the long run but the problem is they only work with metal beds or at least metal below something else. Also everything above the metal must stay the same or you need to adjust values over and over again for different heights of permanent printing beds, kapton tapes or whatever you put on top.



I won't go with a metal bed but with glass instead (well at least i plan so) and may switch to different setups on top. Thats where auto bed levelling should make my live easier. This unfortunately won't work with inductive sensors. The only alternative is to use FSRs but that setup again is a bit difficult. Therefore i decided to go with switches for now - i'm just not sure how to set them up (fold down, retract, attach,...)


---
**Mike Miller** *December 15, 2014 13:39*

Auto-levelling is generally there to overcome calibration problems in a printer. A well tuned printer won't need it. And for cartesians, you can get by without endstops as well. 



Incorporating a sprung bed does a GREAT job of protecting against head crashes and the inevitable unexpected when initially dialing in your printer. 



I have found that my printer stays in-tune rather well, so long as I don't mix up printing parameters. If I'm using a cold bed + PLA, the bed starts out at Z-max, which is set to something like 358.2, I'll then watch it start, and give each corner half a twist till the first bead is nice and fat. All done til I swap to ABS. 



I'm using a thin sheet of diamondplate as the print surface, and it's thermal expansion that throws everything off. I've got some garolite on order and will report back how well it stays in tune with varying heat settings. That said, It's never more than a half-twist at each corner away...and maybe a restarted print 30 seconds in if I feel it's un-salvageable. 



I went with four points instead of three...I've got a machinist background (I'll not go so far as saying I'm a machinist...that belittles the folks that really are)...so picking one corner, setting it to mid-adjustment, then walking around the bed til the gap is equal, really wasn't that hard to do. 


---
**Frank “Helmi” Helmschrott** *December 15, 2014 13:48*

Mike, thanks for your input. I don't want to sound harsh here but my intention wasn't to discuss if auto bed levelling is a good thing or not. I think each one of us should decide for themselves if they're going better with or without auto levelling. I sure now the benefits of having screws in 3 or 4 corners (tested both started with 4 and ended with 3) of your print bed and beeing able to easily adjust in the early stage of a print job. I've had this 2 years or so on my first printers.



About 1,5 years ago i've built auto levelling the first time on an ordbot without any safety installation for head crashes and of course i've been a bit unsure first but after 1,5 years i can say that this isn't a problem for me. I think i crashed into bed one or two times but i was able to recover from that accidents without bigger issues. On the other hand i've been able to easily switch printheads (extruders, nozzles) and beds (different permenant printing plates, different layovers) without having to relevel manually. My setup on the ordbot is still a bit unhandy as i do the leveling with an external switch (one that is used for CNC tool length measurement) but i think this could easily be improved with a permanently installed switch.



This said i totally understand that not everybody will like this process but to me it is the better choice.


---
**Mike Smith** *December 15, 2014 16:23*

you might want to look at this post



[https://plus.google.com/111513417734753522166/posts/bGCQBZPRYc7](https://plus.google.com/111513417734753522166/posts/bGCQBZPRYc7)



**+Mike Kelly** has done a real clean job of implementing this on his printer and I don't think it would take too much to add it to an eusthatios build.....I know I'll be adding it to mine.


---
**Eric Lien** *December 16, 2014 00:51*

I unfortunately am inclined to agree with **+Mike Miller**​​. I know you don't seek a discussion on this, but the greatest benifit to ingentis/ultimaker/eustathios is the fast accelerations made possible by low moving mass.



All I can say is I use 4 corner adjustment w/ springs on my Eustathios, and I have not paralleled the bed for 1 month+. I print daily. So if you are set on autoleveling I totally understand. But in my opinion, for eustathios, autoleveling is solution in search of a problem... With the added side effect of increased inertia.



Just my 2cents after a lot of time using this printer.﻿


---
**Frank “Helmi” Helmschrott** *December 16, 2014 07:52*

Highly valuable input, Eric. Thanks. I will definitely think that over. I terms of movable mass you're completely right. this is one of the main reasons I am building one of these printers. Whatever I do in the end it will only be good with the least possible mass. 


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/QNaybc44WfE) &mdash; content and formatting may not be reliable*
