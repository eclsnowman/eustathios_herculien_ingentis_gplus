---
layout: post
title: "Currently doing some remodeling on my OpenBeam printer"
date: May 12, 2016 15:24
category: "Show and Tell"
author: Pieter Swart
---
Currently doing some remodeling on my OpenBeam printer. It's been sitting in storage for a whole year. About two month back I suddenly got the urge again to try and complete the printer. Replaced most of the printed parts except those on the Z-axes. I've also installed a Titan extruder. At the moment, I'm trying to sort out power, controller and cabling. Going slowly with a torn ligament though. Would have liked everything in a single color but using what is available at the moment.



![images/a6d904754e265c2fb6e29b0ae4f9bd2d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a6d904754e265c2fb6e29b0ae4f9bd2d.jpeg)
![images/5cda273966e540c2819f4b2eecd0252a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5cda273966e540c2819f4b2eecd0252a.jpeg)

**Pieter Swart**

---


---
*Imported from [Google+](https://plus.google.com/117269052061351663459/posts/S8A7SHPZ9b4) &mdash; content and formatting may not be reliable*
