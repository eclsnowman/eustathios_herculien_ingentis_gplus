---
layout: post
title: "I made some modification to the Viki2 (Eustathios) mount to be used on my Herculien"
date: July 16, 2015 08:02
category: "Show and Tell"
author: Vic Catalasan
---
I made some modification to the Viki2 (Eustathios) mount to be used on my Herculien. It is not printing pretty yet as I am still learning how to dial in my printer settings and also creating a 3D drawing that is acceptable to a 3D printer format is another challenge. My next attempt is to print my new drawing of an Azteeg X3 box. I cant wait for my Bontech extruder to arrive as I know I am probably printing too fast on the stock extruder. Printing to me is like watching grass grow but the things you pull from the build plate is a gift! 



![images/93f392d3cecf9b9e2b560e487a86ae67.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/93f392d3cecf9b9e2b560e487a86ae67.jpeg)
![images/fa6deac1feb48556e872cf8aa526fa4f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fa6deac1feb48556e872cf8aa526fa4f.jpeg)

**Vic Catalasan**

---
---
**Eric Lien** *July 16, 2015 10:53*

Looks great. Keeps the modifications coming. And share your files on Youmagine when you are done so others can use them once you are happy with the results.


---
**Vic Catalasan** *July 22, 2015 07:35*

Thanks Eric, I went through a couple of revisions and settled on something I can personally use. I am not sure everyone can use though, however I did upload it to youmagine in case anyone wants to try it.



[https://www.youmagine.com/designs/azteeg-x3-box-mount](https://www.youmagine.com/designs/azteeg-x3-box-mount)

and

[https://www.youmagine.com/designs/viki2-lcd-panucat-mount-20x80-frame](https://www.youmagine.com/designs/viki2-lcd-panucat-mount-20x80-frame)


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/6xvgr9b8eWu) &mdash; content and formatting may not be reliable*
