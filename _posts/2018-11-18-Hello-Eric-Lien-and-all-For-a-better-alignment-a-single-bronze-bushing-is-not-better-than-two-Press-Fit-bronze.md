---
layout: post
title: "Hello Eric Lien and all :) For a better alignment, a single bronze bushing is not better than two Press-Fit bronze ?"
date: November 18, 2018 11:15
category: "Discussion"
author: Yoann Gandin
---
Hello **+Eric Lien** and all :)



For a better alignment, a single bronze bushing is not better than two Press-Fit bronze ?

Two Press-fit Bronze bushing reduce the constraints ?



I live in France, and the cost of shipping for press-fit bushing is too expensive. I do not find it in Europe or in China. I think to replace them with standard bushing bronze..





Thank you





**Yoann Gandin**

---
---
**Eric Lien** *November 21, 2018 00:42*

The two pressfit allows for a wide contact length, with a low contact surface area. It is ideal but others have tried other solutions. For example walter originally tried these (thought he eventually came back to the self alignment type I believe).



He eventually used 10mm long bushings in a ninjaflex sleeve on the central carriage, and 30mm long ones on the side carriages:



[http://thrinter.com/eustathios-initial-build/](http://thrinter.com/eustathios-initial-build/)



[https://www.thingiverse.com/thing:893849](https://www.thingiverse.com/thing:893849)![images/9d8dd0797c7d2be87c36ea8575b41c00.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9d8dd0797c7d2be87c36ea8575b41c00.jpeg)


---
**Yoann Gandin** *November 23, 2018 12:02*

**+Eric Lien** thank you very much for your detailed explanation :)


---
*Imported from [Google+](https://plus.google.com/114649953341157144322/posts/fstJ3nvoYnW) &mdash; content and formatting may not be reliable*
