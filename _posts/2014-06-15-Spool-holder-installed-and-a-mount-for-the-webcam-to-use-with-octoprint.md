---
layout: post
title: "Spool holder installed and a mount for the webcam to use with octoprint"
date: June 15, 2014 23:15
category: "Show and Tell"
author: Eric Lien
---
Spool holder installed and a mount for the webcam to use with octoprint. Man I love octoprint. Webcam is a Logitech C270 which I disassembled to adjust the lense focal ring better for the distance to the center of the bed. I unscrewed and pulled off the standard monitor mount for space savings. 



![images/cf69024cdb53506fc5bebcb4d19457c3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cf69024cdb53506fc5bebcb4d19457c3.jpeg)
![images/051227fda4234b2c37c03d6df4fad81a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/051227fda4234b2c37c03d6df4fad81a.jpeg)
![images/b32bf3805ad0b8499fb2ed0cc8d5ddac.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b32bf3805ad0b8499fb2ed0cc8d5ddac.jpeg)
![images/ddfd9ca26504ffb4a8f0eff25c5e78ba.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ddfd9ca26504ffb4a8f0eff25c5e78ba.jpeg)
![images/8c392d4a715383e9b8cf85267e8c3507.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8c392d4a715383e9b8cf85267e8c3507.jpeg)

**Eric Lien**

---
---
**Pieter Swart** *June 16, 2014 09:29*

That looks pretty sweet. **+Eric Lien** is that an extruder that I can download somewhere? I'm using a 3mm hotend. The only way I will get it to work reliably with direct-drive is with a geared motor and there doesn't seem to be a lot of designs floating around for geared motors.


---
**Tim Rastall** *June 16, 2014 10:29*

**+Pieter Swart** that's an airtripper.  You'll find them on Thingiverse. 


---
**Pieter Swart** *June 16, 2014 10:43*

Hey **+Tim Rastall** , I've looked at the Airtripper before and somehow missed the geared motor. Still glad I asked the question!


---
**Jean-Francois Couture** *June 17, 2014 15:51*

those spool holders are pretty nice.. good job on them ! is the source code available so i could mod the holder onto my printer ?


---
**Eric Lien** *June 17, 2014 16:13*

**+Jean-Francois Couture** Solidworks 2014 & STL available:



[http://repables.com/r/272/](http://repables.com/r/272/)


---
**Jean-Francois Couture** *June 17, 2014 18:15*

Thanks ! i'll look into it ! :)


---
**Brandon Cramer** *July 15, 2015 22:19*

This is a mighty old thread, but almost exactly what I was needing. Do you know if the 18.5mm threaded spool holder can be lengthened to 100mm to hold the wide Bumat reels? 


---
**Eric Lien** *July 15, 2015 22:44*

**+Brandon Cramer** 



[https://drive.google.com/file/d/0B1rU7sHY9d8qbXhFaTQ4cmQ4NDg/view?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qbXhFaTQ4cmQ4NDg/view?usp=sharing)


---
**Brandon Cramer** *July 15, 2015 22:44*

Thank you!!!


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/awwm1L8SdMq) &mdash; content and formatting may not be reliable*
