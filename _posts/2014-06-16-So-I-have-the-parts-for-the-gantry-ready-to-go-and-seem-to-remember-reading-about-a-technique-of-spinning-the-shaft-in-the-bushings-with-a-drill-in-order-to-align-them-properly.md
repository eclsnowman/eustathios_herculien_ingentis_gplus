---
layout: post
title: "So I have the parts for the gantry ready to go, and seem to remember reading about a technique of spinning the shaft in the bushings with a drill in order to align them properly"
date: June 16, 2014 03:47
category: "Discussion"
author: Tony White
---
So I have the parts for the gantry ready to go, and seem to remember reading about a technique of spinning the shaft in the bushings with a drill in order to align them properly. Anyone know of either the post or the technique I'm thinking of? However, my prints for carriage and the x/y axis supports seem a bit of a tight fit with the bushings - worth reprinting tweaked to get the measured diameter found in the part file? Or sanding it out a bit to fit? Or heating the bushing and press fitting in somehow?



I'll post pics sometime next week - doing a kraken with 1 nozzle direct drive (ninja-flex) the other 3 bowden (fat tip ABS, fine tip ABS, and HIPS), enclosed build volume with a pyramid acrylic top. I'll post up all part files.



Also - I plan on using a 24V power supply to make the steppers and heated bed extra happy. Is limiting the max pwm level from 255 to 127 sufficient for the heater cartridges (standard e3d 12v). Azteeg x3 pro.





**Tony White**

---
---
**Eric Lien** *June 16, 2014 04:39*

Careful heating the bushings. The bronze is held into the outer sleave with a binder that allows for the misalignment. I found out the hard way :(



The binder breaks down when heated hot enough for softening the abs. The bronze is then sloppy in the sleave. I had to rebuild the gap from the missing binder by dripping in CA glue and sprinkling in baking soda into the crack.



By the way if you have never used the CA glue with baking soda trick I recommend it. It kicks over the glue instantly and allows for a sandable STRONG gap filler.


---
**Tim Rastall** *June 16, 2014 05:20*

Huh.  Funny, I've never had the issue **+Eric Lien** describes. These were bushings from [sdp-si.com](http://sdp-si.com)? 


---
**Tony White** *June 16, 2014 05:45*

I ordered mine from there - are they heated by spinning the drill? I can push them in a quarter of the way without serious application of force (by hand, and still can pull out as well.)


---
**Eric Lien** *June 16, 2014 05:51*

**+Tim Rastall** yup. But I used a propane torch. I thought it was sparingly. But then the amber chucks started falling out :(


---
**Eric Lien** *June 16, 2014 05:53*

Soften the holes with acetone. Same effect. No heat. That is so long as you printed them in abs?


---
**Tony White** *June 16, 2014 05:55*

acetone seems like a good option, thanks!


---
**Eric Lien** *June 16, 2014 05:58*

It has the benefit of almost glueing them in too.


---
**Tim Rastall** *June 16, 2014 06:07*

Mine go 3/4 of the way in and needed heating to go all the way. Acetone sounds like the safe bet. 


---
**Jason Smith (Birds Of Paradise FPV)** *June 16, 2014 11:12*

Another trick I've used is to heat the holes with a lighter briefly, and then just press the bushings in while the plastic is soft. This "glues" them in fairly securely as well. My parts are pla though, so I'm not sure how well this would work for abs. 


---
**Tony White** *June 16, 2014 16:12*

A quick brush of acetone and the vise got them in smoothly without any cracking noises or plastic discoloration. Ran it in the drill with the shaft for about 1 minute and it feels pretty smooth!


---
*Imported from [Google+](https://plus.google.com/+AnthonyWhiteMechE/posts/5gwqNaqNvNF) &mdash; content and formatting may not be reliable*
