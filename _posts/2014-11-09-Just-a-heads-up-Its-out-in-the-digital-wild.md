---
layout: post
title: "Just a heads up. It's out in the digital wild"
date: November 09, 2014 20:11
category: "Show and Tell"
author: Eric Lien
---
Just a heads up. It's out in the digital wild.



<b>Originally shared by Eric Lien</b>



HercuLien is ready for download on Github: [https://github.com/eclsnowman/HercuLien](https://github.com/eclsnowman/HercuLien)



I have been editing this openly on Github for a while now. But I feel like the model is 95% complete so it's time to let people know it exists so they can download it. Almost all items including hardware are included. I will work on it regularly and update the Github as things change. I still have plans to add some minor missing nuts and bolts, include the side panels, etc. But I figured the community could have fun playing with it for now.



Hope you enjoy.



I also have just the printable stl files out on youmagine: [https://www.youmagine.com/designs/herculien](https://www.youmagine.com/designs/herculien)



 





**Eric Lien**

---
---
**James Rivera** *November 09, 2014 21:20*

Given the remarkably good prints you've been displaying, I'd call this a great day in history!  


---
**Eric Lien** *November 09, 2014 21:37*

**+James Rivera** thanks.


---
**Gary Hangsleben** *November 10, 2014 00:01*

Seen Eric's handy work in person.  He's developed a nice machine that performs excellently.  Great that people keep sharing their work with the community.


---
**Chandler Wall** *November 12, 2014 04:51*

**+Eric Lien** Where is the best place to ask questions about #HercuLien? I've just finished some basic maintenance on my printer, and I'm ready to start printing your models. I have a few basic questions. I'm replying to this post, since it's a smaller, focused community,  but I can move these questions elsewhere.



1. Can you recommend basic settings for printing the models? I generally rely on 25% infill and 0.2 mm layers, unless I have a specific need otherwise. Should the parts be printed more solid than 25%?



2. How do you feel about nozzle diameter for these models? I haven't had a chance to review output GCode for small details and tight edges. I have 0.8, 0.6, and 0.4 mm nozzles. I've installed the 0.4 mm to be safe, but I prefer 0.6 mm if I can get away with it.



3. Which parts should be printed first? I realize that I'll eventually need everything, but I somewhat expect models to change over the next few weeks. Do you consider any models/groups of models to be the most "stable"?



Thanks in advance for your help, and I'm really looking forward to building this guy! As I mentioned in my initial comment, I plan to provide updates with my progress. I'll try to document the process in case others may benefit from my notes.


---
**Eric Lien** *November 12, 2014 06:51*

**+Chandler Wall**​​ this is great news. I am pushing an update with additional hardware and panels tonight.



For printing I used my defaults of .4mm nozzle, .144 layers, 3 perimeter, 4 bottom, 4 top, 25% infill, forced .52 extrusion width.



Most of the mechanicals are finalized, just go off the latest github files from tonight. I may adjust the power supply brackets... But all the rest of the solidworks files should be corrected after tonight. I will get around to tweaking the stl's soon (run them through netfab and orient them properly).﻿


---
**Gus Montoya** *November 27, 2014 08:09*

nice


---
**Eric Lien** *November 27, 2014 15:25*

**+Gus Montoya** you ever find someone to print you parts?


---
**Gus Montoya** *December 04, 2014 02:59*

Hi Eric, sorry for the late reply. Yes I was able to find someone. They were crappy prints, but worked just fine. I am working on printing out parts for the next 3d printer. I'm still undecided on your Herculein or red spider. My idea is to adapt a DLP resin setup under it and dual use the printer bed. Thank you for the follow up. Hope you had a great thanksgiving :)


---
**Eric Lien** *December 04, 2014 04:05*

**+Gus Montoya** I had a great Thanksgiving. Thanks for asking. Glad to hear you got the parts. Nice thing about having your first printer is now you can make more printers. Bad thing is now you have the ich and the only way to scratch or is with more printers ;)



The dual use idea is great. I would love to see the design once you get along with it.


---
**Gus Montoya** *December 04, 2014 09:47*

I have to save some money. My other project (1986 mercedes benz cosworth) ate my bank account. But the idea is cooking in my head. I'll have to make some adjustments to your red spider design but it shouldn't be too hard. 


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/1J773dXJSdz) &mdash; content and formatting may not be reliable*
