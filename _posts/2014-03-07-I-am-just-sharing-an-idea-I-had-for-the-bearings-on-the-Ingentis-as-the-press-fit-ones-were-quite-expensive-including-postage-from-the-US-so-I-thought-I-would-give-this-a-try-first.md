---
layout: post
title: "I am just sharing an idea I had for the bearings on the Ingentis as the press fit ones were quite expensive including postage from the US so I thought I would give this a try first"
date: March 07, 2014 02:47
category: "Discussion"
author: David Heddle
---
I am just sharing an idea I had for the bearings on the Ingentis as the press fit ones were quite expensive including postage from the US so I thought I would give this a try first. It is just a tapered sleeve to hold the bearing, allowing it to move just like the self aligning ones in the BOM.



I am using oilite bearings self lubricating bearings these are pretty cheap. I made up a bearing holder in Rhino that I just added into the XY end before I exported it as a stl file to print.



I am having extruder problems on my printer so I have only managed to print half of one XY end but it was enough to test my idea. The video is attached to this post to show the bearing fitted into the XY end. The other picture just shows a rendering of the sleeve.




**Video content missing for image https://lh6.googleusercontent.com/-AkfGEQzRuVQ/UxkuFUynSJI/AAAAAAAADXg/MmV0DmFpb8g/s0/20140223_5373.mp4.gif**
![images/6ff0176bce51a3f6091fbf4607989c11.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6ff0176bce51a3f6091fbf4607989c11.gif)
![images/5fb16cf0fb1f3cfc0a312c8f72b41d39.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5fb16cf0fb1f3cfc0a312c8f72b41d39.png)

**David Heddle**

---
---
**Whosa whatsis** *March 07, 2014 02:57*

That tiny contact area will wear out as the busing moves over the bar, resulting in an increased inner diameter and slop.


---
**D Rob** *March 07, 2014 03:08*

**+David Heddle**  Its less important for the wobble than you would think. If you use 1 long oil lite bushing and have it tightly fit, for the xy end, the self aligning bearings would not be necessary. the xy ends can afford a little misalignment to the bearing if it is a single long one. The point for the self aligning bearings is because there is a gap between they can be made to line up. not the case with a single long one. if the xy end were to tilt with a single bushing its ok as the rod attaches at a single point on each xy end and all adjustments for being parallel can be made with bed leveling. a single long bushing will ride the rod fine. A good design for the xy with single long bushings would be a slightly smaller hole for it and an expansion gap in the center of the crest (long ways) it would expand a little and give extra retaining retention. I mention all of this as it is in my next build and the oil lite bushings are in my misumi 150 ;)


---
**Tim Rastall** *March 07, 2014 06:43*

This would work with epoxy if you let it cure while rotating a shaft thats been pushed through the bushings. The bushings would warm up and cure the epoxy pretty quickly. I use that methosld to get oilite bushings in the kraken carriage aligned. 


---
*Imported from [Google+](https://plus.google.com/+DavidHeddle/posts/3RqSUoYFxMj) &mdash; content and formatting may not be reliable*
