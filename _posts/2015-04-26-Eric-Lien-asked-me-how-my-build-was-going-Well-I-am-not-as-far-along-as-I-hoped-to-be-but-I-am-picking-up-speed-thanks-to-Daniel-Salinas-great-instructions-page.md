---
layout: post
title: "Eric Lien asked me how my build was going--- Well, I am not as far along as I hoped to be, but I am picking up speed, thanks to Daniel Salinas great instructions page"
date: April 26, 2015 22:13
category: "Show and Tell"
author: Bruce Lunde
---
**+Eric Lien**​ asked me how my build was going---  Well, I am not as far along as I hoped to be, but I am picking up speed, thanks to **+Daniel Salinas**​ great instructions page. I missed a line item for some bearings, and will be ordering them shortly. here's a picture of my current state of the build. 

![images/c31507a99bcd96bccfbf94f8cce55f1f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c31507a99bcd96bccfbf94f8cce55f1f.jpeg)



**Bruce Lunde**

---
---
**Eric Lien** *April 27, 2015 00:04*

Great job. Looking very nice. Love the whiteboard in the background...



Takes me back to physics classes ;)


---
**Bruce Lunde** *April 28, 2015 13:24*

**+Alex Lee** No problems at all, they are all good!


---
**Gus Montoya** *May 05, 2015 00:19*

It looks like it's coming along. Slowly but surely :)


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/HhekSQ9FEUf) &mdash; content and formatting may not be reliable*
