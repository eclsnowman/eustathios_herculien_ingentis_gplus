---
layout: post
title: "Powered up my radds board today to heat up the hotends for tightening, and on first power up popped the capacitor under the E1 driver, letting out a fair bit of magic smoke in the process"
date: July 01, 2017 04:17
category: "Discussion"
author: James Ochs
---
Powered up my radds board today to heat up the hotends for tightening, and on first power up popped the capacitor under the E1 driver, letting out a fair bit of magic smoke in the process.  Board seems to be ok otherwise, do you think I can switch to E2, or should I just bite the bullet and replace the board?﻿



Edit: the only things connected on power up were the hotends, the thermistors, power supply connections, lcd, and usb toy computer.  No motors we're connected.

![images/bfc0e56100e788d5edaba4f11580be95.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bfc0e56100e788d5edaba4f11580be95.jpeg)



**James Ochs**

---
---
**Jeff DeMaagd** *July 01, 2017 04:31*

I think you would need to go to whoever makes the board or to the communities around that board and ask them. The firmwares I use allow pins to be reassigned to get around damaged drivers and such.


---
**Daniel F** *July 01, 2017 11:13*

I think you can use E2 if the rest of the board is not damaged. It's easy to change if you use repetier, there is an online configurator.

You could even replace the capacitor, just get a 35 or 50V version (if you run your steppers on 24V).


---
**James Ochs** *July 03, 2017 16:08*

Thanks, looks like everything is good except that cap and the driver that was in that slot.  New driver on the way....


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/QhsSg52AtHz) &mdash; content and formatting may not be reliable*
