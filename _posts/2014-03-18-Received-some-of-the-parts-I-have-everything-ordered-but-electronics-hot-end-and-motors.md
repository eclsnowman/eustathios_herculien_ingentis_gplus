---
layout: post
title: "Received some of the parts. I have everything ordered but electronics, hot end, and motors ."
date: March 18, 2014 06:03
category: "Show and Tell"
author: Jim Squirrel
---
Received some of the parts. I have everything ordered but electronics, hot end, and motors . 

![images/9aab95b987a81e82ac9b732bcd263c23.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9aab95b987a81e82ac9b732bcd263c23.jpeg)



**Jim Squirrel**

---
---
**D Rob** *March 18, 2014 06:17*

I advise an electronics box below.you could make this from anything, mdf plywood etc. this will maximize your z for the frame size and allow you to hide stuff amd pretty it up ;- D


---
**Jarred Baines** *March 18, 2014 07:09*

I got extra aluminum to make a plexiglass bottom :-)


---
**D Rob** *March 18, 2014 15:11*

Cool I had an extension too but just got some extrusion to make it into one unit. I replaced the upright with longer ones and cut the extra adequate frame for inside to inside cross pieces


---
**Eric Moy** *March 19, 2014 11:18*

I just ordered my e3d, and extruder gears. Now to finish my CAD design and start dissecting my Eventorbot. Good luck


---
**Jarred Baines** *March 19, 2014 11:45*

Oh you made an eventorbot? How does it go? I almost made one before I found my Mendel kit...


---
**Eric Moy** *March 19, 2014 20:21*

How does it go you ask, not much at all nowadays. The design got switched to steel sheet metal during the campaign, which increased the moving mass while not increasing rigidity where it counts. The Bowden system is finicky, the extruder has a hot zone the size of Texas, and the wiring is all solid core. But at the time, was the cheapest thing I could find. I've printed quite a few successful prints but very very slow as the bot oscillates like like a metronome. My extruder motor finally died. I was going to rig mine up to give more support, but then realized that I was better off just redesigning.  The guy who ran the campaign tried really hard, but alas, he wasn't an engineer and the design changes he made mid campaign were terrible.



So at least I have all the guys of a printer to start with. I'll eventually upgrade the sang electronics, and upgrade to dual extrusion. At least I have a replicator 2x at work at my disposal... When it's not broken. That sucker has taught me a lot about printing though, because it is temperamental


---
**Jarred Baines** *March 20, 2014 06:14*

oh...



Glad I didn't jump on that bandwagon then!



Brother pointed that out to me, and then I started my search, it looked cool but was a bit small for my liking.



If u get an ingentis going you'll be the envy of everyone with a makerbot :-P


---
**Eric Moy** *March 20, 2014 09:24*

In order to reuse parts, I'm going with a core xy, which can be easily configured back to a standard Cartesian if I run into to many problems. The Ingentis would require all new linear motion parts. My MISUMI box is sized to match the linear motion parts of the Eventorbot, in order to reuse my parts.



As much as I'd love to build an Ingentis, the differential part cost blew my budget. After using the Makerbot with its standard Cartesian and heavy gantry, I figure as a worst case, it's not that bad. Changing a Cartesian to a Bowden reduces weight even more, then switching to core xy is even more weight removed, so I figure I should approach the speed of the Ultimaker style printer. I also opted for gt2 belts as 3dprinterhell ran into lots of issues trying to use spectra with core xy/ hbots.



So given my constraints of party reuse, I'm designing my own printer... which is taking forever, since I design mechanical machines at work, it's generally the last thing I want to do when I get home :-\. But the fact that I got the MISUMI frame built, and am purchasing other parts, will hopefully force me to complete my design. In the Eventorbot forums, I named my printer the Millennium Falcon, because had so many hacks. I figure I need to rename it with the new design...



In the meantime, I'll be costly watching all the Ingentis and core xy build on the forums to hopefully pick up and lessons learned.﻿


---
**Jim Squirrel** *March 21, 2014 01:37*

bushings bearing and printed parts arrived. assembly this weekend maybe...


---
**Jim Squirrel** *March 22, 2014 03:01*

well  the frame is about as much as i'm gonna get. The plastics i commissioned someone to print were sent and i discovered he printed every part the way they came out of the archive so i have one of everything listed on youmagine download link. :( i guess i should have made a parts plate for the fella


---
**Brian Bland** *March 22, 2014 04:50*

**+Jim Squirrel** Let me know what parts you need,  I will print them.﻿ You are close so you could get them pretty quick.


---
**Jim Squirrel** *March 22, 2014 04:59*

well i borrowed the prusa i2 to print parts. it's not very reliable but does work as of now i'm just working on the 10 mm  bar corner pieces kinda starting from scratch. i'll let you know. btw if you need any pc parts i work at RK Computers in Tulsa. 


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/cSunsgm59vD) &mdash; content and formatting may not be reliable*
