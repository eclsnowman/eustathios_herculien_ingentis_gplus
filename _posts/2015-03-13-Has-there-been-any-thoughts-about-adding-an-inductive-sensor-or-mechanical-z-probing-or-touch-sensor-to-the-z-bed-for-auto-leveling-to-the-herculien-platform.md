---
layout: post
title: "Has there been any thoughts about adding an inductive sensor or mechanical z probing or touch sensor to the z bed for auto-leveling to the herculien platform?"
date: March 13, 2015 05:30
category: "Discussion"
author: Jeff T
---
Has there been any thoughts about adding an inductive sensor or mechanical z probing or touch sensor to the z bed for auto-leveling to the herculien platform? 





**Jeff T**

---
---
**Eric Lien** *March 13, 2015 05:45*

I only need to level once a month or so unless I really change my temps. So I hadn't planned on it. I never found the need.


---
**Jim Wilson** *March 13, 2015 21:02*

Yeah it seems like a nice feature, but its just compensating for an issue that would really be best addressed in hardware. 



Its great for older machines like the prusa i2/i3 where the bed would get kicked out of level, but modern builds like these are more stable.


---
*Imported from [Google+](https://plus.google.com/103527135201379694785/posts/Y8A1UHiQM87) &mdash; content and formatting may not be reliable*
