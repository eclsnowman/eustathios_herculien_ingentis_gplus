---
layout: post
title: "Just looking at buying parts through AliExpress"
date: June 22, 2015 17:30
category: "Discussion"
author: Jeff Kes
---
Just looking at buying parts through AliExpress.  Would this be a bad choice or has a bunch of others had no problem?  Any feedback would be awesome before I order some stuff.



Thanks,

Jeff





**Jeff Kes**

---
---
**Jim Wilson** *June 22, 2015 17:32*

I have never had issues with them other than Chinese holidays I forgot to account for in estimating shipping delays, but that's certainly not their fault.


---
**Dat Chu** *June 22, 2015 17:47*

Perhaps you can give us some links on what parts you are thinking of buying in replacement for what part in the BOM? Aliexpress is like ebay, it works well as long as you know what you are buying.


---
**Daniel F** *June 22, 2015 17:49*

Bought a lot from Aliexpress, it's like it is with ebay, check the ratings. My experience is good, sometimes shipping (to Europe) takes its time but in the end everything arrived. You get what you pay for, if its very chaep, it might not be the best quality, but many items of the same quality cost less than from ebay.﻿


---
**Oliver Seiler** *June 22, 2015 18:25*

I also bought lots of parts for my Eustathios from AliExpress. Stick with 'standard' parts, like 2020 extrusions, screws, etc.

I had some issues with half of the rods arriving in a slightly bent state. The sent replacements, but you'll have to wait another 3-6 weeks for them to arrive. Of the replacement half was bent again <b>sig</b>.

Check the ratings and if you are not happy with a shipment let them know before you confirm on AliExpress. They normally do a lot to make you give them a good rating.


---
**Dat Chu** *June 22, 2015 18:30*

I got my T-nut for aluminum extrusion from Aliexpress as well. **+Oliver Seiler** when you buy extrusion, which provider are you getting from? Do you cut them yourself?


---
**Oliver Seiler** *June 22, 2015 18:57*

I used these and had **+Tim Rastall** helping me cutting them

[http://de.aliexpress.com/item/Aluminum-Profile-Extrusion-2020-20-series-20-20-length-1-meter-1m-L-1000mm/1407643934.html](http://de.aliexpress.com/item/Aluminum-Profile-Extrusion-2020-20-series-20-20-length-1-meter-1m-L-1000mm/1407643934.html)


---
**Øystein Krog** *June 22, 2015 21:42*

I love aliexpress, just watch out, suddenly you find yourself ordering all kinds of stuff:P


---
**Jeff Kes** *June 23, 2015 02:30*

Thanks for the feedback! I am working on a Ultimaker and needed some parts. Then hopefully one of Eric's designs.


---
*Imported from [Google+](https://plus.google.com/+JeffKes/posts/69T784qSN1y) &mdash; content and formatting may not be reliable*
