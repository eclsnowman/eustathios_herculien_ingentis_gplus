---
layout: post
title: "I not being careful when trying to figure out an LCD and crossed some pins on my smoothieboard v1.0a with a multimeter probe and saw some smoke"
date: August 22, 2015 21:50
category: "Discussion"
author: Zane Baird
---
I not being careful when trying to figure out an LCD and crossed some pins on my smoothieboard v1.0a with a multimeter probe and saw some smoke. I should know better.



I've already ordered a new board, but are there any tips for troubleshooting and possible repair of the board I fried? 



What I've noticed and tried:

1. There was a noticable burn on the 3.3 Vreg (LM1117MPX-3.3V/NOPB). I removed and replaced this part, but  that did not fix the problem. 

2. When connecting the board via USB it is not recognized by my computer any longer.

3. I've tried reformatting the card and updated to the latest config and firmware.bin files.

4. The 3.3V LED is orange when plugged into my computer

5.Providing 24VDC power to VBB gives me red LEDs on VBB and 3.3v but nothing else

6. When connecting 24VDC to ground I can here a small noise briefly that I don't remember hearing before, but I could be crazy on this one...



Any ideas?



I also posted this on the smoothieboard forums ([https://groups.google.com/forum/#!topic/smoothieware-support/HT-YAChtXyA](https://groups.google.com/forum/#!topic/smoothieware-support/HT-YAChtXyA)) but I'm looking for advice any of you might have





**Zane Baird**

---
---
**Stephen Baird** *August 22, 2015 22:03*

Look closely at the pins near where your multimeter was to get an idea which lines might have been crossed, then trace back along those lines to see what might have burned. I'm guessing an IC or two bit the dust, but discovering which ones it might have been will tell you if it's repairable and how much work that will be. 


---
**Zane Baird** *August 22, 2015 22:12*

**+Stephen Baird**, It was the pins on the 5V regulator (RECOM R-78E5.0-1.0). Either VBB to GND (most likely) or 5V to GND. 


---
**Stephanie A** *August 23, 2015 05:39*

First step, check all of the power rails after applying power. Use your multimeter and a steady hand.

After that Check for expected behavior, the i/os, mosfets, etc. Finally move to individual component debug, chect the value of the resistors, check for shorts to ground.



Depending on the pins you shorted, anything on the 3.3v rail could be fried. Check the schematics. 


---
**Mike Miller** *August 23, 2015 14:57*

I've experienced the same failure. I replaced the RUMBA that died, and fiddled with the dead one, but was never able to resuscitate it, and the attached components (LCD + SD) was never quite the same. e.g. can't read off an SD card. 


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/QVorsi8Nvm6) &mdash; content and formatting may not be reliable*
