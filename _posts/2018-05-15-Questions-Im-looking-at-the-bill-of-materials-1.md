---
layout: post
title: "Questions, I'm looking at the bill of materials: 1"
date: May 15, 2018 02:39
category: "Discussion"
author: Don Barthel
---
Questions, I'm looking at the bill of materials:



1. There's an assembly "XY_Axis_A" and another "XY_Axis_A (Driven Version/Configuration)" - is this 'and' (I need them both) or 'or' (I need just one of them)?



2. What's the significance of the lines with green background as opposed to white background?



3. It would be helpful to have a photo or exploded diagram of each subassembly. Is that what's in 'Solidworks/Sub Assemblies' directory? I'm on Ubuntu and I can't figure out how to view those files (say in Blender). Help appreciated!

 





**Don Barthel**

---
---
**Eric Lien** *May 15, 2018 13:18*

One side has a longer shaft that extends outside the frame and has a pulley, that pulley is then coupled via a belt to a drive assembly which consists of a mount, motor, and motor pulley.



I have known for a long time I need to do some better documentation. Exploded views would be great. And If anybody is willing to help with some of those it would be amazing.



I hope to get around to it eventually. I apologize that I have not done it so far. Life just gets in the way at times.


---
**Eric Lien** *May 15, 2018 13:20*

Another note is I have the model in a bunch of formats other than solidworks:







[https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Other%203D%20Formats](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Other%203D%20Formats)

![images/cc64f68c92fcaad0c0b451aa155c3d64.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cc64f68c92fcaad0c0b451aa155c3d64.png)


---
**Eric Lien** *May 15, 2018 13:23*

One option on Ubuntu is opening the files in a Web Based Cad like: [onshape.com - Onshape: Modern CAD. Finally.](https://www.onshape.com/)




---
**Eric Lien** *May 15, 2018 13:40*

Here is one of the non-driven axis (shown in Fusion360 since it is the only CAD package I can get to my files in right now):

![images/c009766fc6166b8e2dc11b1d3b870962.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c009766fc6166b8e2dc11b1d3b870962.png)


---
**Eric Lien** *May 15, 2018 13:41*

And one of the driven axis for comparison:

![images/621267cca04229ae78c60cb29603bd2a.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/621267cca04229ae78c60cb29603bd2a.png)


---
**Don Barthel** *May 15, 2018 15:23*

I've tried AutoDesk Viewer and Onshape with the files in the 'Solidworks/Sub Assemblies' directory and they both say that the SolidWorks files are malformed or in an unsupported form. 



Onshape says "It looks like you want to import a SolidWorks assembly. Onshape needs a SolidWorks Pack and Go .zip file to import a SolidWorks assembly with all of its parts. Learn more: [cad.onshape.com - Onshape Help](https://cad.onshape.com/help/index.htm#cshid=importfiles) "




---
**Don Barthel** *May 15, 2018 15:35*

I can see the 'Other 3D Formats/Fusion 360/Eustathios Spider V2.f3d' file in AutoDesk Viewer so maybe that's sufficient.


---
**Eric Lien** *May 15, 2018 18:49*

**+Don Barthel** X_T imports nicely into Onshape. X_T is basically the backbone of the Solidworks file format... and then Solidworks loads a tone of metadata on top of the X_T file. So X_T is the best Import/Export from Solidworks if you can do it.

![images/97b0273a3291ea2d1d330ad38edc6178.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/97b0273a3291ea2d1d330ad38edc6178.png)


---
**Eric Lien** *May 15, 2018 18:54*

Here is the file on Onshape: [cad.onshape.com - Onshape](https://cad.onshape.com/documents/1ea7232ceb7d8d3fc2b90e8f/w/09eceaada891ccf15b7a47f6/e/5aabe619e99daaa088ffe8aa)


---
**Dennis P** *May 28, 2018 03:54*

i think that the eDrawings for Solidworks, even as buggy as it it, is the best for viewing the full model. You can get extended meta-data on the part and even generate single part drawings from the model if you need. 

Exporting to any Autodesk product for me lost all extended info and assemblies got all jumbled around due to what F360/Acad considered insertion points. YMMV.  


---
**Doc Shipley** *June 30, 2018 19:52*

**+Eric Lien** 

I'd be willing to do some exploded views in Solidworks for the printer models.  PM me and discuss it?


---
*Imported from [Google+](https://plus.google.com/+DonBarthel/posts/UEhLYQ7HCYY) &mdash; content and formatting may not be reliable*
