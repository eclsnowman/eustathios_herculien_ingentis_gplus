---
layout: post
title: "Has anyone yet done an Eustathios build with the motors on the inside?"
date: June 27, 2015 10:38
category: "Deviations from Norm"
author: Frank “Helmi” Helmschrott
---
Has anyone yet done an Eustathios build with the motors on the inside? Just thinking about a smaller version that fits well on my desk and is portable as well. Therefore i'd better not have any parts hangin on the outside. I'd be keen to see the ideas how to do an inside mount the best way. Like Ultimaker does it? Probably any better ideas for aluminum extrusion frames? Still not sure if i will go with extrusion on the smaller one.





**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *June 27, 2015 10:55*

Look at the original Eustathios by **+Jason Smith**​. I put mine on the outside to enable shorter belts and use of cheaper pulleys.


---
**Frank “Helmi” Helmschrott** *June 27, 2015 11:03*

well yeah i totally forgot that one. Thanks for pointing that out. indeed the belt is a bit too long for me, I'll see how i could mount the motors more to the top.


---
**Mike Miller** *July 03, 2015 17:21*

I'm using a bottom motor mount system...it works surprisingly well. I have a plywood floor to house the electronics which does a great job of muffling vibration from the motors. Running all motors from the floor simplifies wire runs a bit, as well. 


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/iix2Ax4PeLJ) &mdash; content and formatting may not be reliable*
