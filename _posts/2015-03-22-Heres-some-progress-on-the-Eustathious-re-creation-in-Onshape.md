---
layout: post
title: "Here's some progress on the Eustathious re-creation in Onshape"
date: March 22, 2015 04:52
category: "Discussion"
author: Erik Scott
---
Here's some progress on the Eustathious re-creation in Onshape. I'm going to start by recreating the vanilla Eustathios as best I can, though of course with a few tweaks here and there. Of course, the printed parts are all blue to match my printer's parts (The Blustathios maybe?). I'll be adding a few things specific to my printer (XY carriage comes to mind) but it should be pretty stock. 



I will then move on and fork some modifications such as the new short-belt external motor design (Called the Extathios maybe?) by **+Eric Lien** and a few other things (Hardware mounts, etc.). I actually already have the external motor mounts and pass-through bearing holders ready to go as I think that's something I'm going to have to try on my printer. 



Anyway, enough of my silly printer names. Hope people will find this useful. 

![images/fb032bac7a3b819131f4fdcbf2762de8.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fb032bac7a3b819131f4fdcbf2762de8.png)



**Erik Scott**

---
---
**Jeff DeMaagd** *March 22, 2015 12:11*

Looking good.


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/FPa2DaXQDwX) &mdash; content and formatting may not be reliable*
