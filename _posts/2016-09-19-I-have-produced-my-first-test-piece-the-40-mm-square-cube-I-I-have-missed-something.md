---
layout: post
title: "I have produced my first test piece, the 40 mm square cube, I I have missed something"
date: September 19, 2016 01:16
category: "Discussion"
author: Bruce Lunde
---
I have produced my first test piece, the 40 mm square cube, I I have missed something.  The X and Y setup are pretty good they came out at 39.69; but the z axis is wrong, coming in at 20 mm which is half the height, I know I have missed a setting for this, but  do I double my steps for z, or divide in half? #notgoodwith math





**Bruce Lunde**

---
---
**Sean B** *September 19, 2016 01:23*

What pitch lead screws did you use?  What are your steps per mm?


---
**Bruce Lunde** *September 19, 2016 01:33*

I used the 8mm leads screws as listed in the bom; I have it currently set at 1600 in the smoothie config file;


---
**Eric Lien** *September 19, 2016 01:44*

I use lower microstepping on my Z axis, so you probably have the microstepping jumper set higher so you travel half the distance I do in the same number of steps.


---
**Eric Lien** *September 19, 2016 01:46*

So your options are move the jumpers so you used lower microstepping, or you double your steps per millimeter in the firmware.


---
**Bruce Lunde** *September 19, 2016 01:51*

Thanks **+Eric Lien** , I am using a smoothieboard, so I think is is fixed at 1/16, so I will double and see how that runs.


---
**Sean B** *September 19, 2016 02:16*

Oops forgot to ask, what is your microstepping?  


---
**Bruce Lunde** *September 19, 2016 02:53*

+Sean B I think the SmoothieBoard  is 1/16. I will go with doubling per Eric's post and see how that goes. Getting excited about actually producing some parts now that I am down to making adjustments!


---
**Tomek Brzezinski** *September 19, 2016 04:03*

The reason for using less microstepping on the Z axis is because with the reduction of a threaded rod there is no need to have such fine adjustment, and it puts undue processor load for platforms like the arduino. It probably will be ok on the smoothieboard to use the 1/16th stepping, but if you plan to do heavy Z movements for one setting or another, it seems reasonable to try and reconfigure the driver for 1/8th stepping. 



either microstepping half as much, or double the config settings, will work - as others have said




---
**Bruce Lunde** *September 19, 2016 14:59*

**+Tomek Brzezinski** thanks for the feedback. On this setting, it is hardwired in the smoothieboard, I will have to look into what it takes to change it.  I will be trying the doubled value this evening to see if that clears it up for me.


---
**Bruce Lunde** *September 20, 2016 04:23*

Thanks again for the advice, although this print has several problems, it is 40mm x 40 mm x 40 mm!  Real progress!![images/9b6f27dbe545b580b9f0a19cf5666ff4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9b6f27dbe545b580b9f0a19cf5666ff4.jpeg)


---
**Bruce Lunde** *September 20, 2016 04:51*

![images/40f534e633d37cd2fdd52975b364f9db.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/40f534e633d37cd2fdd52975b364f9db.jpeg)


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/S4DEwRAEo1H) &mdash; content and formatting may not be reliable*
