---
layout: post
title: "Hey all, As I'm pulling my hair out with alignment issues, I noticed that several of the parts I printed are fracturing, mostly around where the bushings are inserted"
date: February 12, 2018 20:05
category: "Discussion"
author: Ryan Fiske
---
Hey all,

As I'm pulling my hair out with alignment issues, I noticed that several of the parts I printed are fracturing, mostly around where the bushings are inserted. As I cannibalized my old printer to assemble this guy, I'm stuck without a way to reprint these parts. Is there anyone with a dialed in printer that could help me and print out some parts for me? I'm more than happy to compensate!





**Ryan Fiske**

---
---
**Brad Vaughan** *February 12, 2018 20:26*

I haven't had a chance to get mine back to where I want it, so I'm not your guy. However, when you do get new parts, make them out of ABS, PETG, or something else less brittle than PLA (not sure what your original parts are made of).



You can try heating up the parts just a bit before pressing the bushings into place. You don't want super soft plastic because it could cool and harden at an off angle, but just a little heat might help the cracking. Also, slowly pressing (e.g., in a vise) is easier on the parts than smacking with a hammer.


---
**Ryan Fiske** *February 12, 2018 21:49*

**+Brad Vaughan** thanks, my parts are currently all abs, I have some bearing holders printed in PETG that I might swap. I used acetone to soften the parts prior to installing the bushings, but my parts must still be slightly undersized


---
**Neil Merchant** *February 13, 2018 01:58*

I had the same problem with my original set of bearing holders printed with abs - I found that they had a tendency to split along the layer lines, so I replaced them with PLA versions that have held up just fine for about a year so far. I'd be happy to print and mail you a set (PLA or PETG, whichever you'd prefer) but I'm located in Canada so shipping might be a bit high - up to you if you want to try or wait and see if someone closer can print some for you.


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/SUY57PxAmNy) &mdash; content and formatting may not be reliable*
