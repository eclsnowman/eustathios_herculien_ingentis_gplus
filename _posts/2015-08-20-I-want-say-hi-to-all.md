---
layout: post
title: "I want say hi to all ..."
date: August 20, 2015 08:25
category: "Discussion"
author: Danut Lipsa
---
I want say hi to all ... its my first post into this community. You are doing a great work here and very valuable information. There is one question I can't fully answer in my head due to my limited mechanical knowledge. I see for XY carriage motors they get connected to driving rods using a small close loop belt. What are the reason behind this? Why they are not connected directly at the end of the rod using a motor coupling? To make it more compact or to reduce the stress on motor axis due to push back forces during accelerating/decelerating? Sorry if this had been answered before.





**Danut Lipsa**

---
---
**Frank “Helmi” Helmschrott** *August 20, 2015 08:31*

Hi Danut,



welcome to the party - good to have you here :)



I have already seen modifications of this drive systems where rods were extended and coupled to motors directly. This may have some small advantages over belts but of course this isn't really more compact as motors stand out on the sides. As with the Spider V2 version the belts driving the motors are quite short so there's no real disadvantage here. There's also no reasonable stress on the rods as they are kept safe in bearings that are made for these kind of forces. I know some people have their printers running for quite a while. Mine is just running for a few weeks now. Ultimaker printers are working nearly the same way (with the difference that they have their motors inside the housing) and they do run quite well for a few years now. Guess all that makes up a rather good drive system which maybe can be optimized but then also has its downsides. 



If you're going to modify it to what you think is better just let us know - i would be highly interested in seeing how things go and what advantages you see.


---
**Danut Lipsa** *August 20, 2015 08:51*

Thanks for your answer. I don't want to go for a direct drive approach. I just wanted to understand the reason behind current design. From your answer I understand that the main reason is to keep motors more compact to the frame  and since using a short belt there is no problem with backlash or play due to belt wear. 


---
**Frank “Helmi” Helmschrott** *August 20, 2015 08:53*

Oh, i cannot tell you what the main reason is - most probably **+Eric Lien** can, as he did the design. I just can tell you that i don't see a real problem there though there may be room for improvement.


---
**Eric Lien** *August 20, 2015 10:41*

**+Frank Helmschrott**​ **+Danut Lipsa**​​ Also there is a 20tooth pulley on the motor and 32 tooth pulley on the rod. So this yields a reduction which has some advantages.﻿


---
*Imported from [Google+](https://plus.google.com/118377606512643149056/posts/HCzQtS3oKFm) &mdash; content and formatting may not be reliable*
