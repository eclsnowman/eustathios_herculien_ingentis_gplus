---
layout: post
title: "Thanks Gus Montoya for posting those pictures of your progress!"
date: July 06, 2015 23:55
category: "Discussion"
author: Brandon Cramer
---
Thanks **+Gus Montoya** for posting those pictures of your progress! It finally got me motivated to put my Eustathios Spider V2 together. Can't wait to work on this some more tomorrow. :)



![images/946239f65a1b09409f35d2b6692956cf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/946239f65a1b09409f35d2b6692956cf.jpeg)
![images/a97da5c254817e40d264222c35f70394.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a97da5c254817e40d264222c35f70394.jpeg)

**Brandon Cramer**

---
---
**Gus Montoya** *July 07, 2015 00:05*

**+Brandon Cramer** Me motivating? I guess theirs always a first for everything LOL. But I'm glad your moving along. I recommend you install all plastic parts first. And I mean everything. Thus far I've had to disassemble 6 times because I didn't forsee additional screw nuts needed into the frame. And what ever you do DON'T LUBRICATE THE BRONZE BEARINGS. And doin't force the bushings into the plastic. They are suppose to easily press in with almost zero effort. This is causing me serious set back and $$$.Your build is looking good :)


---
**Eric Lien** *July 07, 2015 00:07*

Love to see so many coming together. I must admit black and red is a good looking color scheme, but maybe I am biased ;)


---
**Brandon Cramer** *July 08, 2015 00:02*

Question. This 608ZZ skate bearing doesn't seem to be large enough to slide over the SFJ-458 linear shaft. Am I missing something here? 



Just when I was starting to have fun.... :)


---
**Gus Montoya** *July 08, 2015 00:10*

Were they per the BOM ? If they are they should The linear shaft should be 10mm spot on. Maybe measure the inner bearing with calipers. See what you get. Very snug fit but they do fit. 


---
**Eric Lien** *July 08, 2015 00:14*

Almost all shafts are SJF10 which means 10MM OD (except the two cross rods). For all bearings used for that you can't use 608zz (8mm ID), you need the 6900 series (10mm ID).



[http://www.fasteddybearings.com/10-units-10x22x6-rubber-sealed-bearing-6900-2rs/](http://www.fasteddybearings.com/10-units-10x22x6-rubber-sealed-bearing-6900-2rs/)


---
**Rick Sollie** *July 08, 2015 02:06*

**+Gus Montoya** what problems did you run into with the bushings be forced in the plastic?  I did that but have run into any issues. Mine slide on the rails with no issues.


---
**Rick Sollie** *July 08, 2015 02:07*

Nice looking build! 


---
**Brandon Cramer** *July 08, 2015 03:21*

Not sure where I went wrong. I will have to look at my order and the BOM and see if the quantities are correct. Thanks **+Eric Lien** for the link to the correct bearings. I got them ordered. Now I just have to wait..... 


---
**Brandon Cramer** *July 08, 2015 03:23*

**+Gus Montoya** they are definitely not even close to being the correct size. Oh well guess I will just have to wait a little longer. :)


---
**Gus Montoya** *July 08, 2015 03:35*

**+Rick Sollie**  if you force the bearings and or bushing's in, the 3d parts will crack (or can crack) and with the bronze bushings. The black housing will flex and squeeze the bronze bushing. So when it comes time to align the bronze bushing it won't fare so well or slide as freely as it should. 


---
**Rick Sollie** *July 08, 2015 12:22*

**+Gus Montoya** thanks for the info


---
**Brandon Cramer** *July 08, 2015 15:03*

**+Eric Lien** The google doc BOM shows that I need 5 608ZZ bearings and doesn't mention the 6900-2RS-10 bearings. 



Just wanted to bring that to your attention. Not sure who keeps this document updated. 


---
**Eric Lien** *July 08, 2015 15:11*

I just looked and the Excel and PDF docs do call out the 6900, I will have to look into the google docs version, that was modified by Seth back in the day.


---
**Brandon Cramer** *July 08, 2015 15:26*

That is what the google docs was showing when I downloaded the latest version this morning.



Are you sure it's showing the correct bearing on the Excel spreadsheet?



When I go here:



[https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Documentation/BOM](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Documentation/BOM)



It has 5 608 skate bearings. 



Just wanting to help prevent someone else from ordering the wrong bearings. 


---
**Brandon Cramer** *July 08, 2015 22:55*

I was just looking through my Eustathios Spider V2 parts and I came across a FastEddy Bearing bag and guess what was inside. The part I needed. :)


---
**Eric Lien** *July 08, 2015 23:27*

Nice


---
**Brandon Cramer** *July 13, 2015 19:36*

Hey! I finally figured out where the 608ZZ skate bearing go!!! :) This is getting exciting!!! 


---
**Eric Lien** *July 13, 2015 20:53*

**+Brandon Cramer** I am excited too.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/ca6d8rsVswF) &mdash; content and formatting may not be reliable*
