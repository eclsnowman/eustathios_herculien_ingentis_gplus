---
layout: post
title: "Anyone know if I can use the smoothie config posted on the guthub for a X5 GT with Bigfoot BSD2660 drivers?"
date: September 10, 2017 16:06
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Anyone know if I can use the smoothie config posted on the guthub for a X5 GT with Bigfoot BSD2660 drivers? I assume I will need to change a few things, but is the config different for the new board (sorry first time on smoothie)





**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *September 10, 2017 16:21*

My GitHub Config is pretty old now. I would start with a fresh New Config for use with spi drivers = [http://panucattdevices.freshdesk.com/support/solutions/articles/1000244740-support-files](http://panucattdevices.freshdesk.com/support/solutions/articles/1000244740-support-files)



Then edit that file to include the important values from my old Config.


---
**Stefano Pagani (Stef_FPV)** *September 10, 2017 16:29*

alright thx **+Eric Lien** Hopefully I will be printing soon!


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/EptWN5HRygQ) &mdash; content and formatting may not be reliable*
