---
layout: post
title: "Huh. Look at that, 301 members. Weird to think that this all started 18 months ago after ThantiK pointed me towards the T-Slot Tantillus ."
date: January 09, 2015 04:22
category: "Deviations from Norm"
author: Tim Rastall
---
Huh.  Look at that, 301 members.  Weird to think that this all started 18 months ago after **+ThantiK**​ pointed me towards the T-Slot Tantillus . Thanks to everyone who's contributed so far. I'm super keen to see how the design evolves as people improve and itterate on it. Oh, and if anyone wants to produce a Eustathios build guide as they go,  I may be able to arrange some sort of printer hardware/filament bounty as a reward. 





**Tim Rastall**

---
---
**ThantiK** *January 09, 2015 04:51*

There are literally HUNDREDS OF US!


---
**Tony White** *January 09, 2015 05:21*

Awesome!  I haven't seen much discussion of build procedure in general- and agree we need the documentation to really make the design truly accessible. Has there ever been a thread discussing peoples approaches to component assembly order? I built the frame, mounted xy motors and gantry, then z axis bits, mounted electronics, installed build platform, and am wrapping up wiring up the kraken / finishing the enclosure (pyramid acrylic top) now. 


---
**Jason Smith (Birds Of Paradise FPV)** *January 09, 2015 06:14*

I want to second the point that the innovation and collaboration within this community continually impresses me. **+Anthony White**, I think you just wrote the most complete build guide to date :). In all seriousness, I agree that the lack of a build guide is one of the primary weaknesses of this design. <crazy talk>Since there are over 300 of us now, maybe we could all chip in $5 to pay for an entire machine's worth of parts. We could then give these parts to a deserving maker with the stipulation that they produce an awesome build guide in exchange for a free machine. Probably wouldn't work, but it's something to think about.</crazy talk>


---
**Chris English** *January 09, 2015 11:13*

I'm fairly new to this community and would gladly pitch in some dollars for a build guide. Great idea **+Jason Smith**​! 


---
**Tim Rastall** *January 09, 2015 11:36*

As far as I' concerned Procerus is just an early prototype and even if it does work out well, I suspect there will be a lot of tweaking to do as there was with the original Ingentis. Procerus is also all metal, so not much good for those without aluminium cutting tools.

I'd suggest a Eustathios build guide as it's the latest proven iteration.

Great idea to fund a build, I can also reach out to the E3d guys and maybe Misumi/igus to see if we can get some freeby hardware in exchange for stipulating their products in the BOM...

Does anyone know of anything better than instructibles to document a build process?


---
**Eric Lien** *January 09, 2015 12:27*

The Eustathos BOM could be generated from the solidworks model pretty fast. It should have every nut, bolt, etc... I just don't have belts modelled. I did this for HercuLien. I just have been pretty tied up at work.


---
**Eric Lien** *January 09, 2015 12:28*

Also I think the github is a good spot for the guide.


---
**Eric Lien** *January 09, 2015 13:34*

**+Oliver Schönrock** yes I did try to export to many formats. I also need to cleanup the model into subassemblies. My problem is I really am a modeling noob. I am entirely self taught, and much of my learning was done while making the model. Also a lot of the hardware was made using solidworks toolbox which makes for pretty ugly naming conventions.


---
**Tim Rastall** *January 09, 2015 21:50*

If we are doing a build guide,  it's probably more critical to have a complete set of stls that are relatively easy to print.  The solid works files might be useful for creating some instruction images, though stl meshes would also work.




---
**Tim Rastall** *January 09, 2015 21:54*

Ok. So, I will hit-up some suppliers to see what freebies we could get.  Once that's established we can work out what we'd need to subsidise. 

I do wonder if we should get an experienced maker to do the build and we could then donate the end result to a worthy cause like e-nable.


---
**Jo Miller** *January 10, 2015 10:05*

+Tim Rastall



Well, I think the BOM and  Documentation of Eric´s Herculien  are   good and  easy understandable.   In general I think these kind of documentation should clearly be written for people who are considering  of building their SECOND (ore more) machine.

I rather rather see a need for some kind of  documentaion for the calibration-stage of these Ingentus/herculien printers.


---
**Eric Lien** *January 10, 2015 13:28*

**+Jo Miller** calibration and troubleshooting is the tough part. I posted my marlin config but that is less useful for people with other controllers, motors, drivers, pulley combinations, etc. But it does give a starting point.



I will try to get some modeling time in soon on making subassemblies and BOM. It is just been crazy at work recently. I have been implementing a new ERP system for work the last 6 months that went live on January 1st. So my printing time and family life had suffered accordingly with the long hours. I hope by the end of this month things will go back to a more normal pace.


---
**SalahEddine Redjeb** *January 10, 2015 23:25*

Well, i do not totally agree that someone experienced is the best fit for this role, my humble opinion on that is that some newbies might come over here, and what is obvious to you is not to them, which at a certain point may leave them with a feeling of a skipped part on the guide, i read some guides for different types of hardware and got that feeling from time to time, especially when i was totally new.

I would help with what i know, but given my machine is no traditional machine, and almost fully made before i even discover ingentis, its parts and logic might be slightly different.

What i think is doable is to give someone the parts to build a machine, and make his assembly journalised in a log, which is by voluntary participation enriched to a higher level, the conclusion making a draft build, revised from time to time with data from new versions/improvements.


---
**Tim Rastall** *January 11, 2015 08:46*

**+Oliver Schönrock** Yes, makes sense to do this at some point but that could be a separate work-stream to documenting the mechanical build.

I'd still be keen to have an experienced maker produce the guide - a newbies perspective is important but someone who isn't struggling with an undocumented build can focus more on the process of creating a coherent and well considered guide.


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/CBmhoRk1D3A) &mdash; content and formatting may not be reliable*
