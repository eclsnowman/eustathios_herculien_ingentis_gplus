---
layout: post
title: "Well here is my eustathios for today"
date: May 17, 2015 06:40
category: "Discussion"
author: Gus Montoya
---
  Well here is my eustathios for today. I would have to say I'm glad I am doing this. As a teenager building motors, model airplanes, RC airplanes etc. I use to be good at this stuff. I have to get back on my game and this will help me out. 

   I will need to do a complete tare down again, and align those bushings. I followed Tim Rastall's instuction video using a drill. But for some reason the bushings are still really stiff. Going to glue up the crack on the extrusion housing. But all will wait for a week till finals are over. 

![images/e569b33c1aca6233d51da1285c9164be.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e569b33c1aca6233d51da1285c9164be.jpeg)



**Gus Montoya**

---
---
**Gus Montoya** *May 17, 2015 06:42*

Yes I know the bondtech has really long screws, I just stuck it on the frame to make sure I have the proper location. Also the bushings on the gantry are reversed (my mistake). Waiting on bushing for the x-linear shafts. 


---
**Eric Lien** *May 17, 2015 13:29*

Looks good. FYI on the bondtech you will pop the index key on the bottom of the extruder housing out from the keyway on the base. That way you can rotate to extruder vertically.


---
**Ivans Nabereznihs** *May 17, 2015 13:57*

I also tried to align the bushings using a drill but it helped not very good. Put  XY shaft mount to shaft and lightly shaking shaft align the bushings with your hands.


---
**Gus Montoya** *May 17, 2015 16:11*

**+Ivans Nabereznihs**  Can you repeat that again? I didn't really understand your tip. 


---
**Ivans Nabereznihs** *May 17, 2015 16:51*

I see that XY shaft mount is already mounted. You can try to move the mount up/down/right/left. Then you can feel the mount will move more smoothly. I hope that you understand  my explaining. Good luck :-)


---
**Eric Lien** *May 17, 2015 17:38*

I have heard others had good luck putting the rod perpendicular to a surface like a counter and tapping/sliding the carriage down to the counter (by hand). The little vibration from the impact is enough to allow the bushings to settle into position.﻿


---
**Gus Montoya** *May 17, 2015 22:54*

Thank you **+Ivans Nabereznihs**  I understand now. **+Eric Lien**  I'll give that a try is Ivan's idea dosen't work for me. 


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/LwgY4ukhrBq) &mdash; content and formatting may not be reliable*
