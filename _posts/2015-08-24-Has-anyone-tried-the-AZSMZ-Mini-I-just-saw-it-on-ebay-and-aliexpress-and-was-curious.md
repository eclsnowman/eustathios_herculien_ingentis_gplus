---
layout: post
title: "Has anyone tried the AZSMZ Mini? I just saw it on ebay and aliexpress and was curious"
date: August 24, 2015 07:13
category: "Discussion"
author: Jim Squirrel
---
Has anyone tried the AZSMZ Mini? I just saw it on ebay and aliexpress and was curious [http://www.aliexpress.com/item/32-bit-ARM-based-Motion-controller-for-3D-printers-CNC-Machines-and-Laser-cutters-Like-Azteeg/32257951240.html](http://www.aliexpress.com/item/32-bit-ARM-based-Motion-controller-for-3D-printers-CNC-Machines-and-Laser-cutters-Like-Azteeg/32257951240.html)





**Jim Squirrel**

---
---
**Frank “Helmi” Helmschrott** *August 24, 2015 07:55*

nope. For me it would definitely not have enough switchable Outputs. There seems to be only 3 (Extruder, Fan, Bed) - i currently use 5 (Extruder, Print Fan, Bed, Hotend Cooling Fan, Board cooling Fan). Additionally thinking about adding lightning to the switchable outputs.



Also i'm more a fan of quality products with some reputation - never heard of this Board before.


---
**Jo Miller** *August 24, 2015 08:28*

I´ve got it on the herculien,  + the additional beautiful mini display, works fine for me so far, . I´ll give a more detailed report in september when I finnally hope to finish the calibration-of that bondtech extruder. (a divorce got me out out the reprap circuit over the last few months)   grrrrr



Ref:Fan outputs: I never really understood the need of connecting your Hotend E3D fan directly on your printboard (or your board-cooling fan).: both are connected to my power supply directly - and therefore- always are on.   allthough my printboard-fan is coupled to  a temperature-sensor directly.


---
**Frank “Helmi” Helmschrott** *August 24, 2015 08:36*

**+Jo Miller** it's quite simple: Connected to the board you can switch them on when needed and they don't need to run all the time when the printer is powered (for example while standing idle after a long print that ends mid night or just while you are working at something else in the shop). Of course it's no must have but i prefer to have this comfort. I switch on E3D cooling fan when the heater is powered or the temp is over 50°C on the hotend and switch on the board cooling whenever the motors are powered.


---
**Javier Prieto** *September 10, 2015 21:00*

A friend, **+rafael lozano**​ bought one. It will arrive in some days and we will have some feedback about it :)


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/YoZK1tgTEWK) &mdash; content and formatting may not be reliable*
