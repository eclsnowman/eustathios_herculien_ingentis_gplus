---
layout: post
title: "Hey all, I decided to start building a Eustathios"
date: October 26, 2014 21:15
category: "Discussion"
author: Shachar Weis
---
Hey all,

I decided to start building a Eustathios. I have a Ramp2 1.4, E3D hotend, and a bunch of Nema17 steppers. A few questions:



1. Besides the impossible to remember (or spell) name, what are the pitfalls and gotcha's of this design? What should I be weary of?



2. How did you guys make your buildplate ? Can someone recommend a 110v hotbed + SSR ?



3. Is anyone sporting a z-probe, or is unnecessary ?



4. Is PLA ok for the printed parts ?



Cheers,

SW





**Shachar Weis**

---
---
**Eric Lien** *October 26, 2014 22:06*

1.) Aligning the rods all perfectly level, evenly spaced, and square takes time. I made printed spacer blocks so no measuring was required. Also sliding in a missing t-nut you forgot is a pain when you are half way through assembly. Get a small pack of drop in t-nuts for this. You will be glad you did.



Also run a 1hr break in gcode that sends the carriage around the perimeters before running your first print. This will break everything in.



2.) My 120v heated bed will be here Monday. My 400w 24v bed was good... But heat up time is longer than I wanted.



3.) I set bed level a month ago and have never touched it since. Z probing masks a problem, it doesn't solve it. 



4.) Pla will be fine for everything except the carriage. But if you enclose it you might run into problems. My printer is 100% abs parts.



Hope that helps.


---
**Jason Smith (Birds Of Paradise FPV)** *October 26, 2014 22:24*

Eric pretty much covered it all. I have 100% pla parts on mine, including the carriage. The only issue I've had is that the lower motor mounts have sagged slightly due to heat since I enclosed the bottom. I'll be replacing the solid bottom plate with a 2 piece design with a gap in the center as soon as I work up the motivation to install it.  I'm hoping that will help. I've had no issues with the carriage though. 


---
**Eric Lien** *October 26, 2014 22:55*

**+Jason Smith** good point on the motor mounts. If it were me I would make those in ABS.



I am surprised you never ran into issues on the carriage. But I guess there is so much fan action you would be OK. I print almost exclusively in abs now. So the area where my part cooling duct is I think would have issues deforming since no part fan is enabled during abs printing.﻿


---
**Jason Smith (Birds Of Paradise FPV)** *October 26, 2014 23:14*

I'm not sure if the carriage movement keeps things cool or what, but even when I don't run the part cooling fan, it doesn't seem to cause any issues. 


---
**Jim Wilson** *October 26, 2014 23:56*

Modern hot ends like the E3Ds are really efficient with their heat, it focuses the heat at the nozzle and the sinks radiate most of the waste heat away very rapidly. 



I used a PLA extruder mount for an older self-made RepRap hot end that was mostly kapton tape around a resister and a length of pipe, the heat bled out to the mount and melted it to ribbons. After switching to an E3D the exact same mount has worked flawlessly for (so far) nearly 6 months.﻿


---
**Eric Lien** *October 27, 2014 00:40*

I guess I just ran into more issues back on my enclosed corexy. Pla gets soft as low as 60C. My chamber temp would reach 50C with the bed at 110C. That doesn't leave much headroom. 


---
**Mike Miller** *October 27, 2014 12:23*

I have a big fan blowing across the RAMPS 1.4....with the Z-stage at the bottom (you know, it's repeatable), the fan keeps the bed from reaching temp. I'm currently using cardboard to shield the bed from the airflow, but am working on a more permanent solution. The sides of the printer are currently open. 


---
*Imported from [Google+](https://plus.google.com/117479393665221551027/posts/PfAg2u1UXiZ) &mdash; content and formatting may not be reliable*
