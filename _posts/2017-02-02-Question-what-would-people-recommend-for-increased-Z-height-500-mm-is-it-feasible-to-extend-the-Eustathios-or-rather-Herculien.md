---
layout: post
title: "Question, what would people recommend for increased Z height (500 mm), is it feasible to extend the Eustathios, or rather Herculien?"
date: February 02, 2017 19:26
category: "Deviations from Norm"
author: Oliver Seiler
---
Question, what would people recommend for increased Z height (500 mm), is it feasible to extend the Eustathios, or rather Herculien? 

Sounds like easy enough to do, I'm just wondering whether it might get a little too wobbly without reinforcing the corners (hence thinking about the Herculien). 

Or should I rather look for a delta? 

I've been approached by a team working on printing artifical limbs and we're looking for some options. 





**Oliver Seiler**

---
---
**Eric Lien** *February 02, 2017 19:46*

Should be able to be done. But maybe look into a back and side panels bolted onto the outside of the frame. (though it would require some cutouts for where the X/Y motors bolt onto the frame or a new motor mounting and primary closed loop belt tensioning setup). That would give plenty of strength to fight against the increased Z height... plus have the added benefit of fighting drafts.


---
**Eric Lien** *February 02, 2017 20:47*

**+Oliver Seiler** cool that you might be getting some work for your printer. Always nice to have your hobby be a profit center vs an expense :)


---
**Oliver Seiler** *February 02, 2017 21:03*

**+Eric Lien**, I'm solely volunteering my time to advise and potentially build a printer for this cause. There won't be any money involved.

Having said that I've been taking orders on 3DHubs and have completed about 80 print jobs through them (plus some more orders through other channels) and the overwhelmingly positive reviews I'm getting speak for the quality of the Eustathios. 


---
**Oliver Seiler** *February 02, 2017 21:06*

Also, offering paid printing services really helped me increasing print quality and processing efficiency. Plus there are some nice perks on 3DHubs, like discounts on ColorFabb and other filament after completing 20(?) orders.

Also it's amazing to see what other people want printed, so much creativity out there!




---
**Stefano Pagani (Stef_FPV)** *March 21, 2017 22:33*

**+Oliver Seiler** I'm on 3D hubs as well! (With just a makerbot rep2 that my school gave me) Can't wait to get my Eustathios on there as well


---
**Oliver Seiler** *March 21, 2017 22:37*

**+Stefano Pagani** awesome! I've just finished a massive job going through most of a 2.2kg ColorFabb XT spool


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/Ah75Wp2h65V) &mdash; content and formatting may not be reliable*
