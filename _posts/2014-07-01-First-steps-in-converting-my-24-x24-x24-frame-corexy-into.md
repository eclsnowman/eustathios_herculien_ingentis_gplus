---
layout: post
title: "First steps in converting my 24\" x24\" x24\" frame corexy into ..."
date: July 01, 2014 22:58
category: "Show and Tell"
author: Eric Lien
---
First steps in converting my 24" x24" x24" frame corexy into ... Megagentis ... Megathios... Megalien? Name still needs work. I will give more updates as I get further along. Name suggestions welcome.



This will be the forth iteration of this printer. I love that printers make more/new printers. But I will admit two printers makes the job much easier than a printer iterating itself. It leaves more flexibility for "Oops... Forgot about that" situations :)



![images/990d96ac8728841d76cfb740b32de0f1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/990d96ac8728841d76cfb740b32de0f1.jpeg)
![images/2eb89f7cb15897b2fbb2b21258162da4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2eb89f7cb15897b2fbb2b21258162da4.jpeg)

**Eric Lien**

---
---
**Mike Miller** *July 01, 2014 23:45*

I wonder what the effective upper envelope limit is for FFF printing? Where smaller nozzles are impractical and bigger nozzles look ugly...


---
**Eric Lien** *July 02, 2014 00:14*

**+Mike Miller** This one I am going to try dual extrusion again (big nozzle, small nozzle). But this time I will take into account height adjustment. Since I already have the printer enclosed it is a great ABS machine.



I plan to use it almost exclusively for making prototype foundry tooling before cutting the expensive billet tooling, so I need the size. Ductile Iron is a real animal. Heat stresses, shrink, mold tear-up, hot spots, misrun, you name it. If it can go wrong in castings... it likely will at least once. 



So the prototype tool will let us look for those gotchas before you are in 10K+ into a production tool. Especially important is shrink factor since it is generally geometry dependent and non-radial (different in X/Y/Z).



I have done a few so far just for fun. But I think it will be a cool side business.


---
**Joe Spanier** *July 02, 2014 01:31*

We have a 3'x2'x3' FDM machine at work. Makes full scale bulldozer parts. Kinda fun. But has all the same issues we do except warp because the machine is a damn oven. ﻿



I've been considering building a "Reprap" that size just to piss with the RP manager who is always ripping on my "toys". 


---
**Mike Miller** *July 02, 2014 01:45*

**+Joe Spanier** what size is the nozzle?


---
**Joe Spanier** *July 02, 2014 01:51*

They have different sizes but all of them are standard Reprap sizes. The thing is literally a giant makerbot. 


---
**Stephanie A** *July 02, 2014 03:34*

I ran into the "oops, forgot about that" over 9 months ago. I still haven't recovered.

I plan on designing my next printer around that, by using mostly off the shelf parts.


---
**James Rivera** *July 02, 2014 06:11*

"Gigagentistathios"  ;-)


---
**James Rivera** *July 02, 2014 06:16*

**+Shauki Bagdadi** yeah, but then you can switch to tera-, peta-, exa-...  ;-)


---
**Liam Jackson** *July 02, 2014 10:33*

Kilogentis? Next one megagentis, next one gigagentis, next one teragentis, etc... :-D 


---
**Eric Lien** *July 02, 2014 11:25*

**+Shauki Bagdadi** Thanks. I am still fighting z-ribbing. I have to order a lower tooth count for my z stepper motor that results in integer steps to avoid the rounding errors. I noticed Jason does not have this z-ribbing issue on his. He is using a 20 tooth I believe.



But I am SUPER pleased with my x&y tolerances. This was printed at around 100mm/s. The only defect I find is around 100mm/s it begins to introduce very mild ringing around sharp corners that shows on vertical faces. But in simplify3D I can minimize this by turning down my exterior perimeter modifier but let the other perimeters fly.


---
**Jean-Francois Couture** *July 02, 2014 11:52*

That looks good.. anywhere i can find the specs of the machine that printed that ?


---
**Eric Lien** *July 02, 2014 15:42*

I just checked. I had dropped max acceleration to 7000 mm/s2 when I started it up until the bushings smoothed out as I had chatter at 9000. Yes I have default at 4000mm/s2.


---
**James Rivera** *July 02, 2014 16:01*

**+Eric Lien** Does it have a Bowden tube extruder? ﻿


---
**Eric Lien** *July 02, 2014 18:15*

**+James Rivera** the new one will be hotswap hybrid mount. 5:1 nema 11 onboard. 5:1 nema 17 Bowden. I am working out the details of the setup now.


---
**Eric Lien** *July 02, 2014 19:20*

**+Shauki Bagdadi** single motor but I think I will use that new robodigg beefy nema 17, or maybe a nema 23. I like the simplicity of single motors... I just need the torque sized appropriately.


---
**Eric I.** *July 02, 2014 19:33*

Wow, so how big do you think it will be able to print when it's done?


---
**James Rivera** *July 02, 2014 20:19*

**+Eric Lien** interesting...is it a fair guess to assume your plan is to use the Bowden for a larger nozzle extruder for blasting in large amounts of infill or support material quickly?


---
**Eric Lien** *July 02, 2014 21:03*

**+James Rivera** I wouldn't run them at the same time. Direct Drive for flexible filaments, Bowden for speed. Wiring would be modular so I change from the bowden and mount the nema 11 to the carriage. Then connect a modular wiring extender from where the airtripper usually connects to the harness. 


---
**Eric Lien** *July 02, 2014 21:06*

**+Shauki Bagdadi** Less motors to fail, simplified wiring, and I just plain like it better. I guess on this one discussion we differ on our interpretation of simplicity. For me mechanics are simple, belts and pulleys easy to manage and trouble shoot. I like mechanical solutions over additional electronic ones.


---
**Eric Lien** *July 02, 2014 21:07*

**+Eric Ironshell** My estimate right now before designing the carriage... 16"x16"x13"


---
**Joe Spanier** *July 02, 2014 23:11*

Technical wiring two steppers to a driver isn't a good practice either. 


---
**Eric I.** *July 03, 2014 01:49*

**+Eric Lien** Nice! Personally, I'm hoping someday people will be able to have REALLY big 3D printers. I'm picturing one day having one that doubles as my coffee table and can print things the size of baseball bats. I'd never have to go shopping again! ;D 


---
**James Rivera** *July 03, 2014 05:58*

It is also standard wiring on my Printrbots. But I feel like they get slightly out of sync with each other sometimes...


---
**James Rivera** *July 03, 2014 06:33*

**+Shauki Bagdadi** Couldn't you make the same argument if the stepper drivers were not tuned to output the same current when using two steppers?


---
**Joe Spanier** *July 03, 2014 11:55*

**+Shauki Bagdadi** I know ramps does it all the time. Doesn't make it right though. Particularly when you get into higher torque and amp motors. Look at CNC routers with dual motors on y. You'll never see a reputable company wiring two motors in parallel for that. They are always seperare drivers slaved by software. 


---
**James Rivera** *July 04, 2014 20:55*

I know it adds to the cost (perhaps unnecessarily) but adding some kind of rotary encoder so it is always known exactly how many revolutions have passed would help the 2 motor solution keep in sync. Again, perhaps unnecessarily complicating the design, but it would be more reliable w/r/t keeping the two motors in sync.  Or...just use belts and pulleys with a single motor. ;-)


---
**Eugene Lee** *July 05, 2014 10:47*

I've been following your builds from your corexy build to your current conversion. I am just about to build my corexy so I'm curious why you decided to swap to an ingentis style gantry. I read that you had trouble getting round circles at higher speeds. Did you think that could've been fixed with proper belt tension and smoother linear guides, or does it just make more sense to swap over to the ingentis style.



The corexy appeals to me because it is less complex, require less pulleys and smooth rods. Anything else I'm getting myself into that I don't know about? 


---
**Eric Lien** *July 05, 2014 11:48*

**+Eugene Lee** I liked the corexy a lot too for simplicity. But after running the Eustathios I like how many prints I have made with zero adjustment to the mechanics.



I think if I redesigned my corexy now I could do much better than my older designs. For example making sure no drive forces of the belts are cantelivered, but instead in perfect alignment with the guide rods/rails. Also design in a simple belts tension balancing system. Look at what **+Daniel F** is doing. He made some nice design choices.



One nice thing is my corexy gantry plate is 100 percent modular. I can unclip two motor wire harnesses and 6 bolts and it removes as 1 piece. So it will still be around... Just not in use ultil I can give it more thought.﻿


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/UY5C175nk9V) &mdash; content and formatting may not be reliable*
