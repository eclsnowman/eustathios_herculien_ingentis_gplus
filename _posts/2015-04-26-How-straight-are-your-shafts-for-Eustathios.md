---
layout: post
title: "How straight are your shafts for Eustathios?"
date: April 26, 2015 22:42
category: "Discussion"
author: Daniel F
---
How straight are your shafts for Eustathios? Just noticed that my 10mm shafts are no entirely straight, one is around 80 micron untrue in the center of the xy gantry. This seems to be (allmost) within the specs of my supplier as it says "straightness: 50µm/300mm".  Will I sees "slalom" lines in my printed objects? Or nothing to worry?﻿





**Daniel F**

---
---
**Eric Lien** *April 27, 2015 00:02*

 Unfortunately they need to be very straight. If you have a dial indicator you can likely straighten them.


---
**Oliver Seiler** *April 27, 2015 00:53*

**+Eric Lien** How would you best go about straightening them?


---
**Eric Lien** *April 27, 2015 01:20*

Here is how one guy did it. I think **+Tim Rastall** is the one who linked to this first a long time ago. But if it were me I would put something soft in between the crowbar and the rod: 
{% include youtubePlayer.html id=mRtIxG2co5w %}
[https://www.youtube.com/watch?v=mRtIxG2co5w](https://www.youtube.com/watch?v=mRtIxG2co5w)


---
**Daniel F** *May 13, 2015 22:44*

tried to straighten some shafts, I used a similar approach as **+Tim Rastall**. It took a while as my shafts are made from hardened carbon chromium steel with lots of flex. Managed to get two 10mm shafts to +- 15 microns. The 8mm ones are really hard to work with, either they do not deform--or too much and then they are untrue in the opposite direction. Got one within 50microns (+-25) in the center. I'm not sure if it would help to heat it with a hot air gun (no too much, otherwise the hardened surface gets soft) while straightening it. It took me about an hour for each shaft so I'm thinking about ordering some new ones for those that are really bad.


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/eLXZfBsovGf) &mdash; content and formatting may not be reliable*
