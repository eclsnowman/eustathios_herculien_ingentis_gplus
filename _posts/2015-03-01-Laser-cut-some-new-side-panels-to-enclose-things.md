---
layout: post
title: "Laser cut some new side panels to enclose things"
date: March 01, 2015 23:55
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
Laser cut some new side panels to enclose things.  Now I just need to sort out a lid. 

Also installed a new raspberry pi 2 with octoprint/octopi. The speedup in loading and visualizing  gcode files is amazing. The webcam is much more responsive too. ~30 fps at 1280x720. 



![images/1d117272115059cafbd4cc86dd1fb1cd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1d117272115059cafbd4cc86dd1fb1cd.jpeg)
![images/c2b1cd616b10932809d8386bbe366d30.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c2b1cd616b10932809d8386bbe366d30.jpeg)

**Jason Smith (Birds Of Paradise FPV)**

---
---
**Eric Lien** *March 02, 2015 00:16*

Looking good. How are you liking the new carriage and hot end combo?


---
**Jason Smith (Birds Of Paradise FPV)** *March 02, 2015 00:58*

It's awesome.  You have a really great design there.  I just need to take the time to extend the bottom duct ring down so it's properly aligned with the volcano hotend.  


---
**Dat Chu** *March 02, 2015 00:58*

Wow. Much light so beautiful. ﻿ I also cannot comment enough on how much better the pi 2 is. My motion pie setup runs smoothly with 2 wireless cameras and the pi camera too. 



I wish it is easier to get access to them. Everywhere they are out of stock or highly marked up. 


---
**Gus Montoya** *March 02, 2015 02:03*

**+Dat Chu**  Have you found an alternative? Or is this just a waiting game?  **+Jason Smith**  Nice setup, thank you or updating us. 


---
**Dat Chu** *March 02, 2015 02:17*

Alternative for the pi? Not really, I just have to shell out for those $15 shipping cost of MCMElectronics :(. Can't wait for Amazon to start stocking these.


---
**Jason Smith (Birds Of Paradise FPV)** *March 02, 2015 02:24*

Paid $45 for mine on Amazon:

[http://www.amazon.com/RASPBERRY-PI-Model-Quad-Core/dp/B00T2U7R7I](http://www.amazon.com/RASPBERRY-PI-Model-Quad-Core/dp/B00T2U7R7I)


---
**Sébastien Plante** *March 02, 2015 04:02*

Damn, I really need to build an Eustathios! 


---
**Daniel Salinas** *March 03, 2015 19:46*

Looks great Jason!


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/3PiiY91LoUJ) &mdash; content and formatting may not be reliable*
