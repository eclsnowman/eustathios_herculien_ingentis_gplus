---
layout: post
title: "Hi everybody. This is probably going to seem like a lazy question, and that's because it is"
date: January 31, 2015 21:19
category: "Discussion"
author: Nathan Buxton
---
Hi everybody. This is probably going to seem like a lazy question, and that's because it is. I have the HercuLien BOM, but I don't want to do math.



How much do all the parts come to to build a HercuLien? I'm Canadian, so it will obviously be a bit more once shipped, etc. I also already have a printer so I can print all the printed components.



I'm just looking for a ballpark from people who have assembled a HeruLien.



Thank you very much. I apologize for my laziness.





**Nathan Buxton**

---
---
**Dat Chu** *January 31, 2015 21:20*

1500 minimum 


---
**Nathan Buxton** *January 31, 2015 21:25*

Is that based on just a seat of the pants guess, or is that the range you paid to gather the parts for you to assemble one? I figured it's in that range.


---
**Dat Chu** *January 31, 2015 21:34*

It's what my total bill will come to once all the extras are ordered (i.e. top cover, panels, ...). My bill is less because I have been stocking up OpenBuilds vslots and related OpenBuild things like a mad maker on a mission.



If you are willing to shop around, I think you can keep at at around $1200-$1500. Comparing to other printers of the same price range, this machine is much more solid (literally).


---
**Nathan Buxton** *January 31, 2015 21:37*

If I can do it for that price, I'm laughing all the way from the bank. :) How daunting is sourcing all the parts? Are they mostly easy to acquire? 


---
**Dat Chu** *January 31, 2015 21:46*

If you have none of the parts, it's basically click on the link, count the number required in the bom (sometimes you have to multiply yourself since you need two sets but the Bom says: 2 set of 1 extruder), add to cart and check out. Mcmaster will tell you about duplicate entries. The stuff you buy from robotdigg will have a huge shipping cost ($70) but it arrives super fast and very well packaged from Shanghai. Misumi is solid provider. The bronze bushings might be delayed since I ordered the seller remaining stocks a few days ago. In short, it will take a few hours if you don't want parts you already own. Otherwise, pretty much click add click add, checkout on several separate sites. 



For the openbuilds stuff, I recommend going with the alternative link. It's supplied by **+Brandon Satterfield**​ and he is a super nice guy who helps out fellow makers. You can get from the openbuilds store as well but Brandon also has the extruder, psu and other electronics. Might as well make it worth that one shipping. 


---
**Nathan Buxton** *January 31, 2015 21:49*

Wow, okay. I didn't realize it was setup so well. Thank you. I won't be ordering in the very near future, so they should hopefully have time to restock. :P


---
**Jason Perkes** *January 31, 2015 22:05*

LOL Lazy, brilliant! Market for prebuilt? :)


---
**Eric Lien** *January 31, 2015 22:56*

Anybody who wants to kit this up and sell it can do it with my blessing. I just want the feedback so I can make my printer better :)


---
**Jason Perkes** *January 31, 2015 23:07*

I was more friendly joking at **+Nathan Buxton**​ laziness, after all a machine build is not exactly an armchair exercise :) 


---
**Nathan Buxton** *January 31, 2015 23:31*

Haha yeah. I'm not lazy for the build, just the math. Only because I had brain surgery in the summer. I would definitely consider putting together a kit and selling it. After, of course, I build my first one and help Eric refine the design (if needed).


---
**Jason Perkes** *January 31, 2015 23:33*

Brain Surgery? Wow, ouch. Hope your better. Personally I'd have tried ibuprofen first.


---
**Nathan Buxton** *January 31, 2015 23:36*

Actually I'm not that bad at math, just really lazy. I still blame the laziness on the surgery :-P I don't know how much ibuprofen it takes to remove a blood clot, I'd guess a LOT.


---
**Jason Perkes** *January 31, 2015 23:54*

Always takes a while (if ever) to get over surgery, but a blood clot on the brain....cant get more serious than that. Good to see medical science was able to put you right :) Now go get an Abacus!


---
**Marc McDonald** *February 01, 2015 00:00*

**+Nathan Buxton** **+Eric Lien** The BOM is good but there are some vendors that unfortunately do not sell/ship outside of the USA and you will have to look for alternative sources. I can let you know where I sourced them if you have any issues.


---
**Nathan Buxton** *February 01, 2015 00:16*

Thanks Marc! When I'm ready to take the plunge, I'll get in touch. I'm hosting the 3d hubs meetup next week, are you going to come?


---
**Marc McDonald** *February 01, 2015 00:31*

**+Nathan Buxton** Thanks for the invite, unfortunately I am not available on the day.


---
**Eric Lien** *February 01, 2015 00:33*

**+Marc McDonald** perhaps I should make the BOM a google doc so the community can add sources.


---
**Daniel Salinas** *February 01, 2015 02:45*

I just got done buying everything by sourcing from all places listed on the BOM and the bill is just north of $1400 USD with shipping.


---
**Daniel Salinas** *February 01, 2015 02:45*

oh but I haven't bought the lexan yet, sorry.  I think the last time I priced the lexan it was around $190 USD shipped.


---
**Eric Lien** *February 01, 2015 02:57*

**+Daniel Salinas** I lucked out. I found 24x24 sheets of lexan at a surplus house here. $5 each.



A note on the lexan, acrylic will work fine and is cheaper. Plus I plan on thickening up the item in the BOM. I think 3/16 will fit in the track along with the track guide and result in less rattle. The panel tends to act like an amplifier for vibration. 


---
**Daniel Salinas** *February 01, 2015 03:02*

Well to be fair I priced most of the lexan as smoked and the front panels clear. The smoked lexan was a little cheaper.  I wonder if we couldn't do a little firm fitting with clear silicone caulking to reduce the vibrations. 


---
**Eric Lien** *February 01, 2015 03:32*

**+Daniel Salinas** that's what the slot tracks are for, but they were too loose. 3/16 makes for a good snug fit.


---
**Daniel Salinas** *February 01, 2015 04:40*

Oh OK. Good thing I didn't order the lexan yet huh?


---
**Eric Lien** *February 01, 2015 05:42*

The perfect thickness I have found would be 4.3mm or about 0.1725in. That fits snug, but not too snug to make assembly difficult.


---
**Marc McDonald** *February 01, 2015 07:31*

**+Eric Lien** **+Daniel Salinas** Thicker lexan would definitely remove the rattles that I had before inserting clear plastic but it may not stop the panel vibrating when operating. The current assembly method does however make it very difficult to service as you cannot remove the panels go gain access. I may consider on future printers to have the side panels removable.

  

I have also found the enclosure seems to make the ABS fumes far stronger in the printing area and will need to add a extraction/ventilation unit when time permits.  


---
**Nathan Buxton** *February 01, 2015 20:22*

I might personally look into soft clear panels that attach with snaps, like they have on boats.


---
*Imported from [Google+](https://plus.google.com/+NathanBuxton3D/posts/hnTNxJGJi51) &mdash; content and formatting may not be reliable*
