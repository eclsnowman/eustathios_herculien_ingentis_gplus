---
layout: post
title: "Hot off the CNC. Another pair tomorrow morning and the Y-axis can be upgraded to provide some more stiffness to the top of the machine"
date: January 18, 2015 02:44
category: "Show and Tell"
author: Matt Miller
---
Hot off the CNC.  Another pair tomorrow morning and the Y-axis can be upgraded to provide some more stiffness to the top of the machine.



Cut with #ChiliPeppr



![images/bba673236a79db432942ee7c8d76fcff.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bba673236a79db432942ee7c8d76fcff.jpeg)
![images/87478270c4ca32fcdb627c60f0573b44.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/87478270c4ca32fcdb627c60f0573b44.jpeg)

**Matt Miller**

---
---
**Tim Rastall** *January 18, 2015 05:15*

I have mill envy. Really must submit that business case for a cnc project to herself. 

Did you build the cnc yourself? 


---
**Tim Rastall** *January 18, 2015 05:17*

Huh.  I assume the grooves align with the channel in the 2020? If so,  that is going to make one very rigid frame.  Double awesome. 


---
**Matt Miller** *January 18, 2015 05:29*

I too had mill envy for a long time.  Finally took the leap and it has been 100% worth the investment.



Started out as a shapeoko 2.  Upgraded a couple of things for added stiffness.  Abandoned makerslide completely.  Now it's a hybrid shapeoko/openbuilds/misumi machine.  600x600 footprint.  Sits on my desk.  



Open source CNC has come a long way, and the barriers to entry are getting lower all the time which is fantastic.



Yes.  Those grooves are 1.65mm deep and allow the remaining ribs to slip into the 6mm wide channel of the 2020.  The frame is very stiff, and will only get stiffer with the rods and these new end plates.


---
**Mike Thornbury** *January 18, 2015 07:03*

What did you change to be able to mill aluminium? I have enough bits left over from my build to make two more smaller  machines - I am already making a Routy to sell, I could really use something that can cut metal.


---
**Mike Thornbury** *January 18, 2015 09:52*

I've got an Ox with a 3kW water-cooled spindle, and I didn't think you could cut aluminium with it - For that I believe you should have screw-actuation, not thin 3mm belts.



But, maybe I'm not giving it enough credit. I'll have to give it a go.


---
**Matt Miller** *January 18, 2015 13:37*

**+Mike Thornbury** I moved to HFS5-4040-600 on X and Y and mounted openrail to it.  On Z I moved to HFS5-2060 with openrail and cut a new mount from 1/2" aluminum.  NEMA23's for everything.  Still using GT2-6mm belts on X and Y.  3/8-12 Leadscrew on Z.  That's really it.



Your OX should cut aluminum no problem.  With that spindle your real limitation is going to be gantry flex.  But you should be able to cut AL with zero issues.  Just go slow (at least initially) and slow down the spindle.  A little lubrication goes a long way too.  WD-40 or some A9.  Lately I've been dry milling though.



If you want to do smallish parts in AL, I'd highly recommend you over-build something small, but with a decent cutting envelope 400x400.  When I rebuild this machine it's going to be an OX.  But it'll be much heavier-duty than what I have now, and heavier duty than the existing OX.  :D



**+Ashley Webster** you should check out the F117 machine on #OpenBuilds.  It's the bees knees.  No belts, all rack and pinion.  Delicious.


---
**Jean-Francois Couture** *January 18, 2015 16:26*

A friend of mine built a Burch plywood CNC.. works great on wood.. don't think it would do aluminium tough. Could you post some pictures of your model ? this is blowing my mind ! LOL 



Great job BTW :)


---
**Brandon Satterfield** *January 18, 2015 19:45*

Great looking cut! 



All my OX cuts is aluminum. I'm running on 6mm wide GT2 belts. I do have to run a little slower than a rack and pinion would cut, I imagine. Run at 180mm/m, 4mm carbide single flute upcut, .4mm per pass, 12k RPM. Pushed it to 250mm/m but would walk occasionally. I've been running dry too for a while. Tool doesn't last as long but the clean up is much easier. 



Nice use of holding tabs. Are they triangle with a lead in? 



Our spoiler boards look about the same, beat up. Ha. I'm about ready to replace mine. Like your stock holders too!




---
**Matt Miller** *January 18, 2015 21:16*

**+Brandon Satterfield** these plates are a 2 hour job.  Roughing @ 7.5ipm with 0.03in DOC.  Finish passes @ 10ipm with 0.02" removed @ full depth.  1/8" 2 flute upcut.  All this with the makita set at 3.5/5.  I could go faster and deeper, but the whine of the spindle hurts my head, even with hearing protection.  Yup, triangle holding tabs.  A spiral ramp lead-in in the profiles and a tangential one for the climb mill finish pass.  Everything was done in inventor fusion and cambam.



My spoil board sorely needs replacement.  I want to get some HFSQN4-15250 from misumi soon to replace the existing bed.



Your OX looks very rigid.  Do you have mid-span supports on the Y-Axis?  Is you speed limitation due to the spindle cutting power or machine flex?


---
**Brandon Satterfield** *January 18, 2015 22:45*

Hey thanks **+Matt Miller**, I can't take credit for the OX. It was first built by Mark over at Openbuilds. 



Yes the standard kit has a single Y support so the spoiler board sits on an "I" foundation. Been doing a lot of work in many aspects of the machine. Think I have found a way to get it out to a 1000mm X on an ACME drive but much testing is needed. 



The slow speeds are a two-fold. The 400W is small so I must cut on RPM more so than torque. This is fine though as I just reduce my chip load and everything is very happy. The main reason I run slow though is because my personal machine resides in my garage which is next to my master. I can run at this speed and be next to the machine and have a conversation. Note, not at a whisper, but far from a yell. I set the thing up at night and can sleep all night with it in the next room without issue. When I run a higher chip load she chatters a bit. I figure it can work all night, it's a CNC right..:-). 



Be building a Herculien soon, there will be some pieces off my little OX going on it as well...


---
**Matt Miller** *January 19, 2015 13:44*

**+Oliver Schönrock** Nope.  Good old NEMA 17's.  42BYGHM809's.  Intend to run them at 1/8 step with 32T GT2 pulleys.  50steps/mm should be enough resolution on X and Y.


---
*Imported from [Google+](https://plus.google.com/+MattMiller_akhlut/posts/5nXFkYtcB9Y) &mdash; content and formatting may not be reliable*
