---
layout: post
title: "A few photos of my build. I'm calling it finished but I already have a few mods in mind"
date: July 01, 2018 23:58
category: "Build Logs"
author: Julian Dirks
---
A few photos of my build.  I'm calling it finished but I already have a few mods in mind.  Since my last post i have got the bed working - 10mm cast aluminium with 3 point levelling and it is working really well.  It's bulletproof flat  although it takes quite a while to cool down after a print so might look for a removable build surface.  Initially the bed moved a bit.  The cause was  play in the LM10LUU linear bearings which I got from Robotdigg.  I got the rods from Misumi so I ordered new bearings from them and they are perfect - just tolerance differences.  



Duet wifi is fantastic.  The interface is great and the printer is quiet apart from the blower/fan so might look to modify cooling to dual noctua fans or some other mod - ideas welcome! 



Have also fitted Gates GT3 2m belts to X/Y.



Benchy at 0.16 layer height, 45mm/sec.   Still workining on optimising speed/retraction/coast settings etc but really happy.  Thanks again to all who offered advice along the way.



![images/25a85b8a3b6eaed50c271ea469b2d881.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/25a85b8a3b6eaed50c271ea469b2d881.jpeg)
![images/2d6a1154abc5b34ea855af800705a8fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2d6a1154abc5b34ea855af800705a8fa.jpeg)
![images/45f935fa3424bac6e687aa06acb78e95.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/45f935fa3424bac6e687aa06acb78e95.jpeg)
![images/465bd8670aa7970488861b1cd1149b40.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/465bd8670aa7970488861b1cd1149b40.jpeg)
![images/21be32eb380d8f5231d2cb448156cb1b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/21be32eb380d8f5231d2cb448156cb1b.jpeg)

**Julian Dirks**

---
---
**Oliver Seiler** *July 02, 2018 00:47*

Looks beautiful!

(and way tidier than mine)



Have you seen this event?

[wellington.makerfaire.com - Home Page - Maker Faire Wellington](https://wellington.makerfaire.com/)




---
**Eric Lien** *July 02, 2018 01:02*

Your work is exceptionally clean and professional. Bravo. And great looking benchy. Thank you so much for sharing during your journey. 


---
**Eric Lien** *July 02, 2018 01:14*

**+Julian Dirks** , I Don't have a duetwifi. Does ReprapFirmware allow you to backup your settings? If so can you supply a backup of yours for use out on the GitHub to help others?


---
**Oliver Seiler** *July 02, 2018 01:18*

**+Eric Lien** with the duetwifi all settings are stored as gcode files, so very easy to backup. 

I'd be happy to add mine, but have some deviations from the norm, so I'm not sure how useful that yould be...


---
**Julian Dirks** *July 02, 2018 01:45*

**+Oliver Seiler** **+Eric Lien**  Thanks for your kind comments.  My first build so it's been a great learning exercise and so far has delivered everything I was hoping for.  


---
**Julian Dirks** *July 02, 2018 01:48*

**+Eric Lien** Yes of course I'm happy to.  **+Oliver Seiler** is this just the config.g file? or should I inlcude more?


---
**Oliver Seiler** *July 02, 2018 02:05*

**+Julian Dirks** It's the whole folder under /sys/ with the exception of eventlog, config-override.

I've got a copy of mine here, just ned to get it into Github...




---
**Oliver Seiler** *July 02, 2018 05:08*

I put my config here

[github.com - DuetWifi configuration by oseiler2 · Pull Request #18 · eclsnowman/Eustathios-Spider-V2](https://github.com/eclsnowman/Eustathios-Spider-V2/pull/18)

**+Eric Lien** I hope I've done everything the right way


---
**Eric Lien** *July 02, 2018 06:32*

Thank you for the contribution **+Oliver Seiler**. I hope it can help builders in the future. One of these days I am going to get a reprapfirmware based board to try out. Everyone I have talked to and trust raves about them. 


---
**Maxime Favre** *July 05, 2018 05:24*

Looks familiar ;) Great job !


---
**Julian Dirks** *July 05, 2018 06:15*

**+Maxime Favre** Thanks! Yes I really liked your build and also found your photos super useful when trying to lay everything out. Hopefully others will find my photos useful. I thought I might write a post with a few learnings as well. Cheers


---
**Eric Lien** *July 05, 2018 12:43*

**+Julian Dirks** If you have time, please do write a post with some pictures of your specific design choices and things you learned along the journey. Thats great information for a future potential builder.


---
**Manuel Rumpl** *July 08, 2018 16:25*

Great work!


---
**Mike Kelly (Mike Make)** *July 10, 2018 13:50*

Beautiful build. Is the pad heater sagging or is that a wire? Might want to put a backing plate behind it to prevent droop which could burn out your heater or worse


---
**Eric Lien** *July 10, 2018 16:38*

**+Mike Kelly** looks like the thermistor wires going to the center of the pad going over the top of the heated section.


---
**Julian Dirks** *July 10, 2018 20:12*

**+Mike Kelly** Thanks Mike. Yes Eric is correct it’s the built in thermistor on the Keenovo heater. I’ve noticed that people don’t have a great deal of faith in built in thermistors in general and make allowances for adding another one (custom hole in the heater pad to allow access to the middle of the bed). I don’t have that but if I have to add one later on I’ll put it at the edge. Cheers


---
*Imported from [Google+](https://plus.google.com/113795478307151372873/posts/emr9uTcmSta) &mdash; content and formatting may not be reliable*
