---
layout: post
title: "I could use some pointers on making some of the printed parts- i am not liking the slicing and support results I am getting"
date: December 29, 2017 05:42
category: "Discussion"
author: Dennis P
---
I could use some pointers on making some of the printed parts- i am not liking the slicing and support results I am getting. 



What orientations did people find for the more complicated geometries?  



I know failure is a good teacher, but I want to try and have calcualted failures, not blunders.



The XY Belt tensioners seem problematic no matter which way I slice them. The bushing bores are critical so no matter which way I turn them, one set of  holes will get printed in the best way (UP) and the other will be laying down on the X (or Y). I don't get nice round holes when they print lying down. If I enable dense support, then it gets better. But then the support fills in the more delicate features... like the belt ribs. 



Same issues on the carraige prints. The Z-Axis Bed doesnt look so bad anymore. 



I have been using KISS slicer becasue I get good prints out of it. I can do the demo of S3D again, but get overwhelmed by all the options.  



Thanks in advance. 



Dennis





**Dennis P**

---


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/anyyvUUzC5D) &mdash; content and formatting may not be reliable*
