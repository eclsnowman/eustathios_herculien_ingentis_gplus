---
layout: post
title: "The build is definitely going to be challenging to get correct"
date: May 14, 2015 08:43
category: "Discussion"
author: Gus Montoya
---
The build is definitely going to be challenging to get correct. I'm sure to have alignment issues. Also I hope I didn't royally screw up my bushings, all I had was vice grips to press them in. I don't have a clamp of any kind or a bench vice. Received the one wrong size linear bushing... LM12UU vs a LM10UU. Lastly need to enlarge the hole in the frame. The hole is suppose to be bigg enough for the screw to go in correct?



![images/e256c40be858e86b332dcfd20e8f8dd3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e256c40be858e86b332dcfd20e8f8dd3.jpeg)
![images/e045f85a0964e1861a8c5a69c9c23846.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e045f85a0964e1861a8c5a69c9c23846.jpeg)
![images/7094bb95e36b1438069cad609284b9a2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7094bb95e36b1438069cad609284b9a2.jpeg)
![images/6191c0edc6f418cc57a190a28fe7b79c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6191c0edc6f418cc57a190a28fe7b79c.jpeg)

**Gus Montoya**

---
---
**Daniel F** *May 14, 2015 08:56*

I used screws with different heads for this purpose, not sure how they are called in english, see my pictures in my recent post. You then slide the head into the profile and tighten the screw through the hole with an allen key. They they don't have to go throuht the bar.


---
**Isaac Arciaga** *May 14, 2015 10:09*

You are using the wrong bolt at the wrong location. You need to use a m5 button head inserted in the extrusion slot on the opposite side. Those are 'wrench' holes, not screw holes. Did you happen to download any of the available cad formats Eric provided to use as an assembly reference? You will certainly need it. [https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Other%203D%20Formats](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Other%203D%20Formats) 



A few tips. 1) You will need to buy a square if u dont have one already. This is vital for squaring up the frame. If the frame isnt square you will have issues lining up your xyz axis. Take your time with the frame 2) follow the cad drawing 3) dont forget to pre-insert all the tnuts while assembling the frame, unless you will be using post-install tnuts. Otherwise you will have to disassemble the frame and start over again.


---
**Isaac Arciaga** *May 14, 2015 10:40*

Here is a video from Tim on how the wrench hole is used. It's the 4th video. (long ass link)

[https://plus.google.com/u/0/photos/116889746506579771100/albums/5910454945753778305/5910454946857768434?pid=5910454946857768434&oid=116889746506579771100](https://plus.google.com/u/0/photos/116889746506579771100/albums/5910454945753778305/5910454946857768434?pid=5910454946857768434&oid=116889746506579771100)


---
**Eric Lien** *May 14, 2015 10:42*

The linear bearing needed is an lm10luu. The second "l" is important, that what makes it a double length bearing.﻿



Do not use two smaller ones stacked, you will cause binding issues.


---
**Gus Montoya** *May 14, 2015 16:13*

**+Eric Lien**  Oh damn...I'll have to relook at my order. I might have missorderd (thus an error in the google spreadsheets) or the vender sent me the wrong bearings completely. 


---
**Vic Catalasan** *May 14, 2015 16:16*

Wow your really crunched up those bushings... If you do not have a press, you can make wooden plates (MDF), put holes in the middle of the plates, press the plates together with clamps to get the bushings into the parts. 


---
**Gus Montoya** *May 14, 2015 16:33*

Yeah I'm going to need new bushings. I doubt I will be able to take them out so I'm going to need new platic parts too...@Isaac I wish I watched the videos before I started. It would have saved me some parts :(


---
**Eric Lien** *May 14, 2015 17:28*

**+Gus Montoya** no error on the sheet. The link is right... But you have to specify the "l" when ordering. I guess that part of the sheet should have a note.


---
**Eric Lien** *May 14, 2015 17:30*

**+Gus Montoya** if the carriage and rod ends are ABS (mine are), use acetone on a qtip. Soften the hole (takes several applications), then press in the bushings. It needs to be a tight fit.


---
**Isaac Arciaga** *May 14, 2015 17:59*

**+Gus Montoya** You can purchase the LM10LUU here: [http://cncsuperstore.com/index.php?route=product/product&product_id=190](http://cncsuperstore.com/index.php?route=product/product&product_id=190) They are in SoCal. No need to wait for China shipping.


---
**Gus Montoya** *May 14, 2015 19:12*

Purchased !  Thank you **+Isaac Arciaga** 


---
**Isaac Arciaga** *May 14, 2015 20:00*

I should get off my butt and gather all the alternate links I have from my BoM. The majority of the items can be purchased on Amazon, especially all of the fasteners (cept the tnuts). Saves a ton of $$


---
**Gus Montoya** *May 14, 2015 20:45*

Those bearings I just bought came out to $14.00. Man not cheap lol but it will be worth it in the end


---
**Eric Lien** *May 14, 2015 20:55*

**+Gus Montoya** you should only need 2, was the rest of the $14 shipping?


---
**Isaac Arciaga** *May 14, 2015 21:25*

**+Eric Lien** they're 3.99 a piece, 4.95 shipping, plus CA tax. I didn't think he wanted to wait for china shipping times.


---
**Eric Lien** *May 14, 2015 21:40*

**+Isaac Arciaga** price is fair to me.


---
**Gus Montoya** *May 14, 2015 21:51*

No I didn't want to wait for china shipping.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/W197D3STDHX) &mdash; content and formatting may not be reliable*
