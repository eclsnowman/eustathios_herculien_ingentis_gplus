---
layout: post
title: "Thinking of playing with something like this: for the Z carriage"
date: March 25, 2014 12:44
category: "Deviations from Norm"
author: ThantiK
---
Thinking of playing with something like this: [http://www.thingiverse.com/thing:126537](http://www.thingiverse.com/thing:126537) for the Z carriage.  Has anyone played with this before?  Worth looking at maybe?  Benefits are very little friction, still holds its place on poweroff, and given enough force, the bearings will actually slide on the rod...





**ThantiK**

---
---
**Wayne Friedt** *March 25, 2014 12:48*

It is very accurate except if using a Z lift. Its not the same steps going up as down


---
**Mike Miller** *March 25, 2014 12:54*

It looks fiddly, and prone to losing calibration, but I'm no authority...and it's not like you couldn't reuse the bearings on something else.


---
**Wayne Friedt** *March 25, 2014 12:56*

It is deadly accurate


---
**Whosa whatsis** *March 25, 2014 18:54*

Like spectra drives, threadless ballscrews need to be calibrated empirically and lack the self-correcting nature of a toothed drive system.



Because the steps/mm needs to be measured rather than calculated, you aren't going to get good increments like  5 microns per full step, so you can expect rounding errors and Z ribbing when you try to print thin layers.


---
**Wayne Friedt** *March 25, 2014 23:21*

**+Whosa whatsis** What would you say is the absolute best for a Z, ball screw?


---
**Whosa whatsis** *March 25, 2014 23:32*

**+Wayne Friedt** there's no categorical answer. It depends on your goals. Fast screws (multi-start) move faster at the expense of resolution and requiring more torque. Ballscrews have less friction, but more inertia due to the added mass.


---
**ThantiK** *March 26, 2014 02:07*

**+Whosa whatsis** even with something like this: [http://www.thingiverse.com/thing:124706](http://www.thingiverse.com/thing:124706) where I can set a perfectly round number?  I can print them so that they're exactly 10mm per rotation, etc.  Wouldn't the weight of the carriage also preload them?


---
**Whosa whatsis** *March 26, 2014 02:23*

**+Anthony Morris** If you set the steps/mm based on that 10mm/rotation number, you will not get the type of rounding errors I mentioned (assuming you choose a good layer height for that value), but your prints may not come out to the right height due to manufacturing tolerances and slip. Any deviation in the angle of the bearings due to irregularities in the surface will affect the effective pitch, then there's **+Wayne Friedt**'s comment about the up/down steps not matching (which could be due to slippage inherent in the system, changes in the angle of the bearings as the direction of force on them changes, etc.). Screw threads and belt teeth correct for this type of error (any delay in this self-correction is visible as backlash), ensuring that it is non-accumulating. Threadless ballscrews have nothing to prevent backlash from turning into accumulating error.


---
**Wayne Friedt** *March 26, 2014 02:29*

I know that the people that are using this say its one of the" i should have done this a long time ago thing" if Z lift is not used.


---
**D Rob** *March 26, 2014 05:36*

**+Anthony Morris** I am very interested in this. could you please keep us posted if you attempt to use this? imagine xy ends like this on ingentis / Ul-T-Slot I may try this on a build


---
**D Rob** *March 26, 2014 05:40*


{% include youtubePlayer.html id=Rkhm-hp9788 %}
[Threadless Ballscrew (customizeable, no backlash)](https://www.youtube.com/watch?v=Rkhm-hp9788) worth watching says no backlash


---
**Whosa whatsis** *March 26, 2014 05:43*

**+D Rob** What I'm trying to say is that backlash is not the concern, it's repeatability.


---
**D Rob** *March 26, 2014 06:24*

**+Whosa whatsis** I see. this is my first time laying eyes on this type of drive. I saw a guy say he figured around 50lbs was what he was pushing to make it slip. so i say probably more like 30 as most people are a bad judge at such things. (That's why we use machines for accurate readings on such matters.) In almost all we do there are tolerances and some are more tolerable than others. I wonder if the trade off is worth while. it definitely looks cost effective.I don't know as I'd use it in a z system but am considering it in a x/y. Why are the steps different in different directions? is it the pitch (can't see how though) and if so then there is probably a golden number for an angle that would even out the steps forward to back. Are there any good resources for this? I defer to your knowledge in these things. Mostly because I am new to such things and I am learning as I go. (I'm a 9th grade drop out with a semester in college and a GED.) I missed a lot of math but have taught myself much. Also I know all bearings aren't created equal so maybe ymmv with a better brand named bearing. 


---
**Whosa whatsis** *March 26, 2014 06:35*

I can think of three reasons the steps would be different in different directions.



1. There is a small amount of slip inherent in the way the bearings contact the rod, and this slip will bias slightly in the direction of force (gravity), so it will take slightly more steps to go up than down.



2. Tf there is any minute amount of play in the bearing mountings (which there is), the change in direction of the rotational force will cause the angle of the bearings to shift, changing the effective pitch of the screw.



3. If the bearings aren't exactly at the same angle (which #2 pretty much guarantees), there will be more slip, enhancing the effect of #1.


---
**D Rob** *March 26, 2014 06:37*


{% include youtubePlayer.html id=l25R_nUwyJM %}
[threadless Ballscrew Printing](https://www.youtube.com/watch?v=l25R_nUwyJM)


---
**D Rob** *March 26, 2014 06:45*

**+Whosa whatsis** I have seen a version with two sets one on each side of the (thread-less nut?) do you think this would counteract this effect. Also you mentioned gravity. I really don't know if I would implement this in z (although I have seen some that do, in my googling tonight) I have a larger heavier bed 12x12. I'm more interested in using this for spectra free and belt free carriage movement. This would also remove the bushing wear **+Tim Rastall**  was talking about in a post of his.


---
**Whosa whatsis** *March 26, 2014 06:57*

Two sets of bearings would help, as would not using them in a vertical orientation. Used that way, I think they'd be roughly equivalent to spectra, though with more rotational inertia and more mass to move back and forth, both of which would negatively impact your safe acceleration/jerk settings.


---
**D Rob** *March 26, 2014 07:07*

**+Whosa whatsis** in a design like the ingentis I believe the rotational inertia would be canceled out by the cross bars the carriage runs on. The rod running to opposite sides should act as a stabilizer; no?


---
**Whosa whatsis** *March 26, 2014 07:43*

No, I'm talking about the angular momentum of the rod itself, which is proportional to the mass of the rod times the square of its radius.


---
**Mike Miller** *March 29, 2014 17:25*

**+D Rob** posting printer videos online should require a minimum bit of training. 90% is out of focus, badly oriented, or using digital zoom. :/﻿ (commenting on the video you posted...not YOUR videos...)


---
**Wayne Friedt** *March 29, 2014 23:10*

The video **+D Rob**  has put a link to clearly shows the Z lift working fine as well as the other axes working fine for the " up/down" or for both directions. 


---
**D Rob** *March 30, 2014 03:48*

**+Mike Miller** no you hit the nail on the head or you haven't seen mine. I'm no videographer, that's for sure.


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/crr9dY6HXTA) &mdash; content and formatting may not be reliable*
