---
layout: post
title: "I'd like to start a discussion on the printing environment, ie"
date: February 03, 2015 17:27
category: "Discussion"
author: Paul Sieradzki
---
I'd like to start a discussion on the printing environment, ie. heated bed/chamber.



Something I've noticed is that finding heated beds can either be very hard or very expensive. What if we can print in large build sizes (I'm thinking in terms of PLA, but I suppose other "friendly" plastics that aren't ABS could work too) without this expensive/hard-to-find part?



Instead of a large heated bed, what if the chamber of the printer itself was heated? For PLA the max would be around 45C before we risk it getting soft. A pair of extra 50W cartridge heaters in an Aluminum block (Misumi makes these) and a CPU heatsink and fan could surely take care of this, no? Coding in a "warmup time" especially with metal build plates that heat up quickly could be a great solution to warping problems!



Thoughts?





**Paul Sieradzki**

---
---
**argas231** *February 03, 2015 18:05*

Well...   Yes Heated beds can be expensive...   I came up with something that helps  in either case.. heated and non heated.. Richard Horne even did a blog on the stuff.. [http://richrap.blogspot.co.uk/2015/01/stick-with-it-3d-printing-print-bed.html](http://richrap.blogspot.co.uk/2015/01/stick-with-it-3d-printing-print-bed.html)  I make it..  it is called 3D EeZ... Something to think about


---
**Jim Wilson** *February 03, 2015 18:16*

The heat is more to ensure the adhering of the part to the bed with PLA rather than to prevent warping. I prefer the heated bed because tape/glue/hairspray/juice/beer is just a mess.


---
**Paul Sieradzki** *February 03, 2015 18:22*

**+Jim Wilson** it's also a permanent solution, whereas the other coatings have to be replaced every couple of prints, at the best. Are the legends true that parts (even large ones) completely loosen when the bed cools back to room temp (~25C) from a decently warm temp (~40-45C)? (Wouldn't want to get too close to the glass transition temp range of PLA (~50-60C)


---
**Jim Wilson** *February 03, 2015 18:30*

Yep, you could print a solid 100mm cube and just pluck it off the bed once it cooled on my i2. I print on a glass sheet overtop the hbp and it still comes off with no fuss. I wipe with rubbing alcohol every few prints and have no adhesion problems.


---
**Stephen Baird** *February 03, 2015 18:40*

A heated, or at least enclosed,  build chamber is more about preventing inter-layer cracking than bed adhesion. Although it can help with minimizing curl even on the first layer. You're sort of looking at a good idea as a solution to the wrong problem. 



With build volumes this large, enclosed build spaces are extra helpful as you can potentially get the active build layer so far away from the effects of the heated bed that it's as good as printing without one, so the risk of cracking goes up. 


---
*Imported from [Google+](https://plus.google.com/+PaulSieradzki/posts/DUsrXVspFKz) &mdash; content and formatting may not be reliable*
