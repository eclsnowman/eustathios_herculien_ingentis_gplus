---
layout: post
title: "Are any of you printing with PETG in your HercuLien?"
date: December 29, 2018 16:50
category: "Discussion"
author: Bruce Lunde
---
Are any of you printing with PETG in your HercuLien?  Are you doing anything special with it other than a higher temperature on the heater and bed.  I read 240-265 for the extrusion and 80-100 for the bed. Any other special considerations?





**Bruce Lunde**

---
---
**Eric Lien** *December 29, 2018 17:06*

I run 235 to 250C on the hotend depending on the filament and the print speed. Bed is 75C on my PEI surface (I ran higher on glass but PETG stick really well to PEI so it can be run cooler). For retraction I set it a little longer than my normal, and a little slower on retract speed. For extrusion multiplier I am usually around 0.93 because it helps keep there from being a lot of buildup on the nozzle and I find I get die swell out of the nozzle so 0.93 gives me a perfectly smooth top layer with no ridges from over extrusion.


---
**Stefano Pagani (Stef_FPV)** *December 29, 2018 17:13*

**+Eric Lien** what’s your extrusion width? I run .97 with .5 width .4 nozzle 


---
**Stefano Pagani (Stef_FPV)** *December 29, 2018 17:14*

I use a e3d silicone “pro” sock, wrap copper wire around it so it doesn’t slip down![images/eaf00b9ab79c46ae10fc745a89b9c4fd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eaf00b9ab79c46ae10fc745a89b9c4fd.jpeg)


---
**Dennis P** *December 29, 2018 17:15*

i print petg with aquanet on glass, most petg's are 230/235 range for me on mine.  Zane Baird replied to a post of mine from ~17Weeks ago re:speeds and feed go with good settings that i used for comparison. 

1. go slower. i use 20mm/s for outer perimeters, 30mm for inner loops and 40-50 mm/s for infill. You could go faster, but I get really nice surface finish and I am on a 8bit RAMPS\Mega combo.   

2. don't squish the first layer- PETG is really sticky and does not like it. It will pile up on your nozzle and make a mess. in fact, i will remove and clean and polish my nozzle every 2-3 rool changes.   

3. cooling- turn cooling down or even off. it likes it hot and it shrinks less.

 4. Extrusion multiplier- I never touch it. I calibrate with 1.0 and almost never touch it. YMMV.




---
**Greg Nutt** *December 29, 2018 17:28*

240-265 sounds a little high.  I print it at 235 with the PETG I use.  I've tried higher and it ended up jamming up my nozzle.  I don't use heated bed when printing PETG.   Using IPG blue tape on the bed works amazing well for adhesion.


---
**Eric Lien** *December 29, 2018 18:19*

**+Stefano Pagani** For strength .5mm width, for detail 0.46mm width.






---
**Eric Lien** *December 29, 2018 18:21*

**+Dennis P** yes forgot to mention cooling. Keep it hot for good layer strength. But at high speed or for short layer times on small features some cooling may be required. But that's geometry dependant.


---
**Bruce Lunde** *December 29, 2018 18:47*

Thanks all. I have some PETG coming today, so will use all these tips, much appreciated!



Anyone come up with a good place to “meet-up” on line after G+ closes this site in April? Saw they moved up the date due to a new bug they found.


---
**James Rivera** *December 29, 2018 20:17*

**+Bruce Lunde** I can’t speak for everyone here, but I’m on FB and MeWe.com.


---
**James Rivera** *December 29, 2018 20:19*

I print PETG at 35mm/s (slower than PLA), 235C, on a cold bed with blue tape. Works great for me.


---
**Dennis P** *December 30, 2018 01:55*

**+Bruce Lunde** remember that temps are relative, 230 on your machine might be 235 on mine or 225 on Eric's. You need to play with your temps to dial it in.   

i have found that the CLEAR or Trans PETG's need higher temps than the solid colors.  I print white and black and light gray at 230 all the time. The worst is natural PETG-  I had to go up to 250 for it to behave better and get good layer adhesion. The TransBlue not so bad. TransMagenta- 2nd worse ever!  


---
**Jeff DeMaagd** *December 30, 2018 15:19*

80-100 for the bed sounds like too hot to waaay too hot. Where did you get that setting?


---
**Bruce Lunde** *December 30, 2018 15:43*

**+Jeff DeMaagd** It was in a All3d Printing. Generic All About PETG article. I looked at several articles and pulled the averages.


---
**Jeff DeMaagd** *December 30, 2018 15:50*

OK but it seems extreme or at least unnecessary, I’m curious how they arrived at those numbers. 100 is ABS bed temps for some people. 75 has been my personal upper limit for PETG. For PETG on PEI I use 65 or less to prevent accidental fusing to the PEI.


---
**Dennis P** *December 30, 2018 15:57*

 **+Bruce Lunde** 80C is the glass transition temp for most PETGs. ie, it starts getting soft. You don't want to go above that. IF i print with heated bed, its 65-ish for PETG on glass or blue tape. 




---
**Bruce Lunde** *December 30, 2018 16:05*

**+Jeff DeMaagd** I will keep that in mind. My PETG filament arrived so I will use these suggestions and give it a shot today. I know it will take a bit of playing, but that’s what Sunday afternoons are for (watching football and playing with tech tools); I am printing on glass, with aqua net like **+Dennis P**, need to look into the PEI several of you have mentioned.


---
**Bruce Lunde** *January 01, 2019 02:49*

Thanks all again, after some playing with settings, I am getting some good prints with PETG. Hoping they will be stronger that the PLA for my robots shoulder assembly.![images/97ac19c4f0acfb14a40ef98a6f32b8db.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/97ac19c4f0acfb14a40ef98a6f32b8db.png)


---
**Bruce Lunde** *January 02, 2019 01:46*

**+Jeff DeMaagd** **+Dennis P** **+Eric Lien** **+James Rivera** I was successful, thanks for the tips!![images/35b8b8df32be1868f0ff1839f3a66935.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/35b8b8df32be1868f0ff1839f3a66935.jpeg)


---
**James Rivera** *January 02, 2019 04:09*

**+Bruce Lunde** Glad to hear it! I’m curious about your robot though...


---
**Bruce Lunde** *January 02, 2019 04:43*

**+James Rivera** it is the Inmoov unit, designed by Gael Langevin


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/QCJ5fDVPbJi) &mdash; content and formatting may not be reliable*
