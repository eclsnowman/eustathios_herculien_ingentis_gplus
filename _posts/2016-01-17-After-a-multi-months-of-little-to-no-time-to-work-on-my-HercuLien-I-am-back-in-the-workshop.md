---
layout: post
title: "After a multi-months of little to no time to work on my HercuLien, I am back in the workshop"
date: January 17, 2016 21:23
category: "Discussion"
author: Bruce Lunde
---
After a multi-months of little to no time to work on my HercuLien, I am back in the workshop. Of course, I have a couple of questions:

1. If you ordered the Ali Rubber 120v heat bed, does anyone know the thermistor type  they use, so I can set it up properly in my Smoothie Board config?

2. Same question for my E3D Volcano, what setting do I use;  I did not see the model for the one they sent, I looked all around e3d site, but maybe I missed it.







**Bruce Lunde**

---
---
**Jake Shi** *January 17, 2016 21:34*

The rubber heater should be NTC100 about 100k ohm.


---
**Oliver Seiler** *January 17, 2016 21:40*

1. The beta value for Alirubber seems to be 3950 (see here [https://plus.google.com/+OliverSeiler/posts/efA1VPMvRmX](https://plus.google.com/+OliverSeiler/posts/efA1VPMvRmX) ) or check out Erik's thermistortables.h

2. The doc at refers to using 'Semitec' 

[http://wiki.e3d-online.com/wiki/E3D-v6_Assembly#Smoothieware](http://wiki.e3d-online.com/wiki/E3D-v6_Assembly#Smoothieware)



Doc at Smoothieware:

[http://smoothieware.org/temperaturecontrol#toc5](http://smoothieware.org/temperaturecontrol#toc5)


---
**Oliver Seiler** *January 17, 2016 21:41*

So in your config you would have

temperature_control.hotend.thermistor        Semitec 

and

temperature_control.bed.beta                 3950


---
**Oliver Seiler** *January 17, 2016 21:42*

I'm sure someone here has found more accurate ways (**+Eric Lien** maybe), but it works fine for me ;)


---
**Eric Lien** *January 17, 2016 21:58*

**+Oliver Seiler** yes, I confirmed on the alirubber bed via a thermocouple the beta value of 3950 is accurate within 1deg C. Good enough for me.


---
**Bruce Lunde** *January 17, 2016 22:01*

Awesome, thanks for the quick information on both, and the links, I missed that on the E3D wiki!


---
**Zane Baird** *January 17, 2016 22:20*

**+Bruce Lunde**, I used the data sheet for the thermistor and fit it to the Steinhart-Hart equation. This should give you the most accurate values over the entire operating range: [https://plus.google.com/115824832953735584348/posts/Tr9kzbSucjR](https://plus.google.com/115824832953735584348/posts/Tr9kzbSucjR)



But because it's only the bed, stability is more important than truly accurate temps


---
**Ray Kholodovsky (Cohesion3D)** *January 17, 2016 22:32*

It's #11 in Marlin, I believe. 


---
**Jeff DeMaagd** *January 18, 2016 06:00*

For the bed thermistor from Alirubber, I found I needed to add a configuration of R0 to be a bit less than 3000. R0 is the resistance of the thermistor at 25C. You can measure that with a multimeter if you want.


---
**Bruce Lunde** *January 18, 2016 19:40*

Thanks all, I have updated my setting, now I just have to wait for an extruder gear and some screws to finish up (how is it that parts just disappear from the workbench,) . Lucky for me Filastruder and Trimcraft have already packaged for shipment, so with luck (and USPS) , I may be finalizing the build this weekend.


---
**Mikael Sjöberg** *January 23, 2016 20:04*

Hi

I know many orders from alirubber and Thats probably good

I ordered My from robotdig, i thought that might be interesting for you to look at as well

This is the one i ordered 



Silicone Rubber Heater Pad



300*300mm, 12V or 24V 300W








---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/apBrASsR6cT) &mdash; content and formatting may not be reliable*
