---
layout: post
title: "Tramming & Bed Leveling- I am pretty sure my bed is level but it does not trammed to the axis of the hot end"
date: June 27, 2018 00:03
category: "Discussion"
author: Dennis P
---
Tramming & Bed Leveling- I am pretty sure my bed is level but it does not trammed to the axis of the hot end.



I get a file tooth effect on my top layers, and to some extent on my bottom layers. I am guessing it's pretty far out if I can feel it touching the print moving it against the infill pattern in each direction. 



How do people align the bed to be closer to the axis of the hot end?  



I am picturing how we do it on a mill, but without a proper spindle, I dont know if I trust the hotend. 





**Dennis P**

---
---
**Eric Lien** *June 27, 2018 00:52*

Your hot end has to be pretty far out for that to be a big affect. Contact area at the flat of the nozzle and angle will show how much angle it takes to get an appreciable difference one side to the other. More likely cause is usually over extrusion on top surfaces, or too close to the bed on first layers. This causes the nozzle to plow through existing plastic causing ridges.


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/ehuMyZ6tj1f) &mdash; content and formatting may not be reliable*
