---
layout: post
title: "Hey all, just started planning my Eustathios-kinda build"
date: December 01, 2014 13:44
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Hey all,



just started planning my Eustathios-kinda build. It will be more like a re-planning of another printer (core-xy). I decided to switch due to some things that happened on the first prototype and that made me think it would be a better way to test this way out now.



Anyways. I'm planning to use 10mm rods outer and 8mm inner. I'm planning to with **+igus Inc.** Aluminum Rods and there RJUM-11-10 linear gliders. They are known to be not that tight than ball bearings (which of course don't fit here due to the rotation) and also not that tight than bronze bushings. Did anyone yet use them there? Are they good enough in this place?



I'm also not sure yet what to use on the inner. smaller rods. RJMP-01-08 probably won't be precise enough there. What do you guys think?



Here's a first "move around concept drawing" of the XY-plane. For everything else i will mostly reuse what has been build already for the other printer.

![images/2a8888b796c0be50efe0c8035a0fd449.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2a8888b796c0be50efe0c8035a0fd449.png)



**Frank “Helmi” Helmschrott**

---
---
**Mike Miller** *December 01, 2014 16:13*

I'm using the **+igus Inc.**  plastic inserts at those locations... they work well as they're positively held in place by GT2 belts...slipstick isn't really a factor here. 



I used them in the carriage, and after a reasonable amount of bed-in time, had very little problems with them, but the carriage was a little larger than usual to give the igus bushings some rotational resistance. I've since changed to linear ball-bearings for the carriage, mostly because I had them on hand. 


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/JgdZ5yhYHtH) &mdash; content and formatting may not be reliable*
