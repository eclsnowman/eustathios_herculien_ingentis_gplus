---
layout: post
title: "Does anyone here know of a good glass supplier in the UK (or preferably Wales)?"
date: August 24, 2016 14:47
category: "Discussion"
author: Adam Morgan
---
Does anyone here know of a good glass supplier in the UK (or preferably Wales)?



Currently using a 300x300 borosilicate plate but loosing to much space due to the clips holding it to the plate, want to bump this up to cover all of the build plate but having issues finding someone who will cut to size.



Also, I have read on a few threads here that plain window glass will work fine, can someone confirm this? 



Cheers





**Adam Morgan**

---
---
**Stefano Pagani (Stef_FPV)** *August 24, 2016 15:10*

Plain window glass will work fine, that's what I use on my printers


---
**Mutley3D** *August 24, 2016 15:21*

Personally I avoid plain window glass as it is traditionally not "float" glass and depending on quality, can have undulations or bulges in it. Mirror glass will be intrinsically flat as it is a float glass. In UK HomeBase do 300x300 mirror tiles in packs of 4 for £10-12. Other home depot stores have similar offerings. Otherwise a local glazier is your best bet. Just be sure to ask them to polish or debur the edges to avoid cracking under heat stress. Then add a durable print surface on top.


---
**Stefano Pagani (Stef_FPV)** *August 24, 2016 17:01*

**+Mutley3D** Good point. I've been getting mine from framers so it's very good quality


---
**Adam Morgan** *August 24, 2016 20:23*

Thanks **+Mutley3D** , great info. Just checked out Homebase but they don't offer anything bigger, so guess I will have to try the glaziers. Do I need to specify "float" glass to get what I need or is that the norm?


---
**Adam Morgan** *August 24, 2016 20:24*

oh re-read the responses and realise **+Stefano Pagani** pretty much answers my question :)


---
**Adam Morgan** *August 24, 2016 20:28*

just looking at a local glazier and they offer toughening, is this needed?


---
**Stefano Pagani (Stef_FPV)** *August 24, 2016 20:37*

+Adam Morgan I've heard that it can introduce internal stresses in the glass and make it break when heating or if you ram the nozzle into the bed. Correct me if I'm​ wrong


---
**Mutley3D** *August 24, 2016 20:46*

Toughened (tempered?) glass is fine, ie its the stuff in the door of your oven so pretty durable and impact proof to a point. However if you buy a mirror cut to size from your glazier, it will/should be float glass but you can always ask the question. Tradesmen know most about their industry. However, if your heater or heatspreader is 300mm, and your glass is 350mm, so cold edges, this will introduce stresses that can lead to breakages when heated


---
**Daniel F** *August 24, 2016 20:47*

Ikea (or other) mirror tile 30x30cm works great


---
**Adam Morgan** *August 24, 2016 22:38*

cheers **+Mutley3D** and **+Stefano Pagani**, I have found a local guy that seems to have what you reccomended. The sheet is same size as heat spreader (I have Eustathios v1-2 hybrid) so that shouldnt be an issue.


---
**Stefano Pagani (Stef_FPV)** *August 24, 2016 23:01*

Glad to help! Good luck with the rest of the build! Make sure to post your progress under the build logs section. (If you want)


---
*Imported from [Google+](https://plus.google.com/117208540149253709671/posts/TVNsU1GwXd5) &mdash; content and formatting may not be reliable*
