---
layout: post
title: "Here are the Inventor Pack n Go files for the stepUP printer that I have been working on (an Ingentis variant) Access the Google Drive Folder (I have yet to set up a github): The main assembly is In the directory: /stepUP"
date: June 03, 2015 16:42
category: "Show and Tell"
author: Ishaan Gov
---
Here are the Inventor Pack n Go files for the stepUP printer that I have been working on (an Ingentis variant)

Access the Google Drive Folder (I have yet to set up a github): 

[https://drive.google.com/folderview?id=0B5eMHpN6y6dGfm53T1ZMU2duNkJOTFR2WWpMLXBjc0tkR01SRUNLN3B1b2xOY1ljOUR2eE0&usp=sharing](https://drive.google.com/folderview?id=0B5eMHpN6y6dGfm53T1ZMU2duNkJOTFR2WWpMLXBjc0tkR01SRUNLN3B1b2xOY1ljOUR2eE0&usp=sharing)

The main assembly is In the directory:

/stepUP Inventor Files/Workgroups/Workgroup/Ingentis Revision.iam

![images/501a7f8f1ce04cf86999cb218e69dce7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/501a7f8f1ce04cf86999cb218e69dce7.jpeg)



**Ishaan Gov**

---
---
**Godwin Bangera** *June 04, 2015 11:55*

nice work .. whats the build volume ??


---
**Ishaan Gov** *June 04, 2015 13:57*

**+Godwin Bangera**​, shooting for 340mm x 340mm x 325mm


---
**Godwin Bangera** *June 04, 2015 14:25*

very nice


---
*Imported from [Google+](https://plus.google.com/113675942849856761371/posts/R5t66QXTK9z) &mdash; content and formatting may not be reliable*
