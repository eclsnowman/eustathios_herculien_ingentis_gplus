---
layout: post
title: "Having a little struggle with my extruder that refuses to extruded consistently"
date: October 29, 2016 20:58
category: "Discussion"
author: Roland Barenbrug
---
Having a little struggle with my extruder that refuses to extruded consistently. Used for a long time a traditional geared hobbed bolt, but this one is extruding unreliable. Used for a while a gunstruder (belt driven) [http://www.thingiverse.com/thing:1247952](http://www.thingiverse.com/thing:1247952), which extrudes far more constant but the inner belt is running of the idlers after a while. So my question is what are you using as extruder and would you recommend it? 





**Roland Barenbrug**

---
---
**Greg Nutt** *October 29, 2016 21:18*

E3D Titan.  It just works.  [store.timeshell.ca - E3D Titan Extruder](https://store.timeshell.ca/index.php?route=product/product&product_id=85&tracking=581511f96e7a7)


---
**Pieter Swart** *October 29, 2016 21:22*

I also use the Titan. You should  look at bondtech too. Rather get one of these commercial extruders. They will save you some frustration.﻿ Titan has reliable grip and is 'self cleaning' it's also small and light.. others here will tel you that the bondtech has unbelievable grip. I wouldn't mind having one of those myself.


---
**Jim Stone** *October 29, 2016 21:35*

Bond tech is leaps and bounds better than the titan. I have both and the bond tech just....works



It will do every job j have put at it and still wants more.



IMO e3d us just junk for extruder. ...hotends aren't bad tho


---
**Eric Lien** *October 29, 2016 21:57*

For extruders... nothing beats a Bondtech by **+Martin Bondéus**​ . IMHO.


---
**jerryflyguy** *October 29, 2016 22:02*

I'll give my +1 to the Titan by E3D. For the price its dynamite. Not to take anything away from Bondtech, I've never used one but the Titan has never come up short for me yet. 


---
**Ted Huntington** *October 30, 2016 00:51*

for cheapest extruder that is still dependable I humbly submit the Wade's gear ;)


---
**Jim Stone** *October 30, 2016 00:54*

I've still got a wades around for emergencies! I remember giving it herringbone gears as an upgrade too! Haha.



Oh. Also another extruder that is just as good as the bond tech if you need the extruder on carriage for those tough filaments like flexible....



The Flex3drive is fantastic!﻿ made by **+Mutley3D**​ he also has a wonderful printing surface called print bite that performs extremely well


---
**Ted Huntington** *October 30, 2016 02:46*

Thinking more about it- the direct-drive hot ends from China are like $25- I bought this one [https://www.aliexpress.com/item/1-75MM-Upgrade-MK8-Extruder-Nozzle-Latest-Print-Head-for-3D-Printer-Makerbot-Prusa-i3-with/32563869751.html](https://www.aliexpress.com/item/1-75MM-Upgrade-MK8-Extruder-Nozzle-Latest-Print-Head-for-3D-Printer-Makerbot-Prusa-i3-with/32563869751.html), made an attachment to a rep rap Mendel for it, and it works- I have yet to try flexible filament with it yet- but as a direct drive hot end- it probably can. Just amazing how cheap it is- but yet functional.


---
**Ray Kholodovsky (Cohesion3D)** *October 30, 2016 04:09*

**+Ted Huntington** my steel prusa came with one of those. I gutted their hotend and printed an adapter for an E3Dv6 on the end of that, and replaced the stepper motor on it, but the core is ok. 


---
**Ted Huntington** *October 30, 2016 06:41*

**+Ray Kholodovsky** I have only used it for maybe 5-10 prints and then at 40mm/s but so far no serious problems with it. 


---
**Luis Pacheco** *October 31, 2016 00:42*

Anyone using the Titan or Bondtech as a direct drive on the Herculien? If not, anyone tried flexible materials using the bowden setup?


---
**Eric Lien** *October 31, 2016 00:49*

**+Luis Pacheco**​ flexible in Bondtech Bowden is good in 3mm filament... But in 1.75 it's only doable with TPU and slow (harder shore d hardness value than something like Ninjaflex).



Pics below are Bondtech with sainsmart TPU on the Talos Tria Delta.



![images/5cc6bf063f6c6546ee67b9b31908b5a5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5cc6bf063f6c6546ee67b9b31908b5a5.jpeg)


---
**Eric Lien** *October 31, 2016 00:49*

![images/dd451ff1b668da4fd749d73f29886bb4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dd451ff1b668da4fd749d73f29886bb4.jpeg)


---
**Luis Pacheco** *October 31, 2016 01:06*

**+Eric Lien**​ Those prints look great. Any idea how good a configuration like that would work in the HercuLien? Might have to do a dual extruder setup with a 1.75 and a 3 hot end.


---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2016 01:16*

I have a side question: does a direct drive variant for the Hercu design exist yet? 


---
**Eric Lien** *October 31, 2016 01:52*

**+Luis Pacheco**​ HercuLien has a longer Bowden tube... By quite a bit. My guess is, it won't work as well on 1.75 due to the length.



But there is always the **+Walter Hsiao**​​ flying extruder which uses a Bondtech.



Also I made this year's ago. Don't think it's ever been tested: [https://plus.google.com/+EricLiensMind/posts/Y4V8sHcjmgu](https://plus.google.com/+EricLiensMind/posts/Y4V8sHcjmgu)


---
**Luis Pacheco** *October 31, 2016 02:09*

Interesting, I'll have a look at that design.


---
**Luis Pacheco** *October 31, 2016 02:11*

Worst case scenario I'll build the HL for nonflexible filaments and repurpose my old, currently nonworking small printer for flexible.


---
**Eric Lien** *October 31, 2016 02:14*

**+Luis Pacheco** That's what I tend to do. Small direct drive Talos Spartan for flexibles. Larger machine for the big stuff.


---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2016 02:31*

What are your guys' thoughts on the PG35L Micro Geared Stepper Motor from Ultibots?  Long time on the to do list has been to make a dual direct drive extruder for Chimera with those guys.  The last I worked on that design I used 2 Nema 14 side by side (no gearing) but I fear that will not have enough torque. 


---
**Eric Lien** *October 31, 2016 03:04*

**+Ray Kholodovsky** never played with a PG35L. I think they run hot, so not ideal for an enclosed printer. But I have heard good thing for torque... But with that gear ratio I think retraction speed would suffer.


---
**Ray Kholodovsky (Cohesion3D)** *October 31, 2016 03:20*

**+Eric Lien** All valid points, I had them in mind for an open printer and definitely intended to have a fan on them as they advise.


---
**Mike Miller** *October 31, 2016 11:49*

I can't believe no one has mentioned @stflint. [http://www.thingiverse.com/thing:979113](http://www.thingiverse.com/thing:979113) I've been using one for more that a year without issue...mk4 hobbed gear, two skate bearings, some PTFE and some nuts...it's a VERY inexpensive VERY strong extruder designed by our own **+Michaël Memeteau** 



Note, folks have had luck with lighter flint wheels, I found the, brittle and unreliable. 


---
*Imported from [Google+](https://plus.google.com/118296832015849309457/posts/jEvgnX2dENL) &mdash; content and formatting may not be reliable*
