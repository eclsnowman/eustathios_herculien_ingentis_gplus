---
layout: post
title: "What's a good webcam for monitoring your print job?"
date: August 23, 2015 22:52
category: "Discussion"
author: Brandon Cramer
---
What's a good webcam for monitoring your print job? I'm thinking the Logitech HD Pro C920 but I don't know if it would work or if there is a better option. 





**Brandon Cramer**

---
---
**Dat Chu** *August 23, 2015 23:02*

The rpi camera is very good.


---
**Jason Smith (Birds Of Paradise FPV)** *August 23, 2015 23:06*

I use a logitech c310, and it works pretty well.  I use an add-on wide angle lens in order to be able to see the entire build area ([http://www.amazon.com/dp/B005C3CSXC/ref=pe_385040_30332190_TE_3p_dp_1](http://www.amazon.com/dp/B005C3CSXC/ref=pe_385040_30332190_TE_3p_dp_1)).  The camera has held up to the fairly high temperatures of a semi-enclosed build chamber and is cheap enough that I won't be heartbroken if it gives out eventually.  I'm sure the c920 would work fine as well.


---
**Brian Jacoby** *August 23, 2015 23:14*

Think that's the one I have.  Will have to make a mount for it; it's sitting on a tripod at the moment, which is a bit too far away for detail.


---
**Isaac Arciaga** *August 24, 2015 01:34*

**+Brandon Cramer** the C920 is probably the best one you can buy at the moment. It has a H.264 encoder built into it.


---
**Brandon Cramer** *August 24, 2015 03:51*

I'm not using Octopi. Bandwidth is not an issue. I think I will give the C920 a try. 


---
**Mike Kelly (Mike Make)** *August 24, 2015 04:11*

I've used a microsoft lifecam the C920 and a smattering of cheaper options. The C920 is the best I've used. 


---
**Isaac Arciaga** *August 24, 2015 04:45*

**+Nathan Walkner** that is exactly why I suggested the C920. It does all the encoding on-board.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/6Cj1ahxqZAy) &mdash; content and formatting may not be reliable*
