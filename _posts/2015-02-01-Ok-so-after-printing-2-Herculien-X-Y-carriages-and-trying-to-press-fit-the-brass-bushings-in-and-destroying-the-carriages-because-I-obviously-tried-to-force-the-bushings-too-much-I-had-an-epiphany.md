---
layout: post
title: "Ok so after printing 2 Herculien X/Y carriages and trying to press fit the brass bushings in and destroying the carriages because I obviously tried to force the bushings too much I had an epiphany"
date: February 01, 2015 02:54
category: "Discussion"
author: Daniel Salinas
---
Ok so after printing 2 Herculien X/Y carriages and trying to press fit the brass bushings in and destroying the carriages because I obviously tried to force the bushings too much I had an epiphany. I printed one more, cleaned it up a bit then put it in a vapor finishing container with some acetone just as if I was going to vapor smooth the part.  When the abs was just soft enough I press fit the bushings in.  Please please please correct me if I shouldn't have done this but they lined up perfect and not they're securely in and the part is now sitting on a piece of glass hardening again.  I think the only crappy part is I put a big ol thumb print on it and smudged an edge a little but I feel like I just solved my problem in an interesting way and it looks awesome.  Any feedback?





**Daniel Salinas**

---
---
**Dat Chu** *February 01, 2015 03:36*

**+Eric Lien**​  suggested something similar.


---
**Eric Lien** *February 01, 2015 04:37*

This is how I install them. Acetone soaked on a qtip. Rub the inside if the bushing pocket, resoak, repeat until it is soft. Push in bushing, then allow it to cure. It also helps to act as glue to hold in the bushing.



And no chance of thumb prints.


---
**Daniel Salinas** *February 01, 2015 04:38*

Ooo I like that idea. I feel like there's one last carriage print in my near future. :-)


---
**Eric Lien** *February 01, 2015 05:50*

At least you didn't try what I did on my first Eustathios carriage. I heated up the bushing and pressed it in. Not only did this cause all the oil to come out of the sintered bronze, but it heated up the epoxy potting compound between the outer housing and the brass spherical bushing which caused it to break and fall out. So... You could have done worse ;)﻿


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/gSH8iv44NP2) &mdash; content and formatting may not be reliable*
