---
layout: post
title: "Can someone explain to me, using the latest image of octopi, EXACTLY how to set up the damn WiFi?"
date: September 19, 2015 21:57
category: "Discussion"
author: Erik Scott
---
Can someone explain to me, using the latest image of octopi, EXACTLY how to set up the damn WiFi? I've followed a number of how to guides, and nothing seems to work. I tried following the video by Thomas Sanladerer and the desktop doesn't have the WiFi config shortcut. I tried the guide by TESTED and the complicated guide they link to. Neither work. 



I'm getting quite sick of this damn thing. There's no reason it should be this complicated to simply connect to WiFi, no is there any reason they should have made it MORE complicated. 





**Erik Scott**

---
---
**Eric Lien** *September 19, 2015 22:08*

Sorry. I run it wired.


---
**Isaac Arciaga** *September 19, 2015 22:59*

It really isn't complicated at all. Is your wifi adapter a known compatible unit for Raspbian? Sometimes the shortcut will not appear if the adapter isn't compatible. You can also try running the setup from a terminal window (command line) if you haven't tried it yet. [https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md](https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md)


---
**Erik Scott** *September 20, 2015 00:20*

I'm using the octopi distribution, not rasbian. 



Yes, my wifi dongle is compatible. It's an Edimax adapter. 


---
**Erik Scott** *September 20, 2015 00:49*

See, here's the problem, I don't see ANYTHING on the desktop except the wastebasket. I can get to what I believe is the equivalent application by typing wpa_gui into the command line, but I can't select wlan0 in the adapter drop-down. What the hell?!


---
**Isaac Arciaga** *September 20, 2015 00:58*

**+Erik Scott** OctoPi distribution is basically a Raspbian image with the OctoPrint software pre-installed/configured. You're best bet would probably be the OctoPrint community here: [https://plus.google.com/u/0/communities/102771308349328485741](https://plus.google.com/u/0/communities/102771308349328485741) or on IRC, FreeNode at  #octoprint  


---
**Ishaan Gov** *September 20, 2015 02:04*

I think it involves modifying the wpa supplicant and network config files, forgot the details, sorry


---
**Gústav K Gústavsson** *September 20, 2015 03:28*

I almost NEVER have been able to use the wi-fi GUI tool in Raspian and have always used terminal mode (or ssh into RPI) and edited the configure files from there. Have been able to set it up without manually edit configure files, did it like that I think. Installed new Raspian image on the SD card. Installed the wifi adapter, put SD card in and THEN powered up. The setup configured the wifi on first boot but never on 2 boot! Now I just have to find my notes on what to edit and post it here...


---
**Gústav K Gústavsson** *September 20, 2015 03:32*

Here it is I think 

[https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md](https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md)


---
**Gústav K Gústavsson** *September 20, 2015 04:04*

Humm think I used this one the last time I did this... [https://learn.adafruit.com/adafruits-raspberry-pi-lesson-3-network-setup/setting-up-wifi-with-occidentalis](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-3-network-setup/setting-up-wifi-with-occidentalis)


---
**Erik Scott** *September 20, 2015 06:17*

I figured it out. For whatever reason, the OctoPi devs decided it would be best to enter your network information into a text file in the root of the SD card before you even put it in the Pi. I think it's called octopi-network.txt or something. In any case, I find this much more annoying than using a GUI. 



The GUI does seem to still exist; you can type sudo wpa_gui into the terminal. Don't forget the sudo; it will still launch if you leave it out, but you won't be able to use any of the drop downs and it'll be useless. Now, I didn't actually confirm that the GUI works, as I tried running as sudo after I got it working, and I was curious as to what the difference might be. 


---
**Erik Scott** *September 20, 2015 06:20*

On, and if you're on windows, notepad won't display the new lines correctly. You have to either use notepad++ or some other editor that supports Unix character encoding or whatever it is that messes this up. Or use Linux, but I don't have any Linux boxes in the house right now. 


---
**Gústav K Gústavsson** *September 20, 2015 10:47*

Well you actually have a Linux box in the house, it's called Raspberry Pi :-). I usually just ssh into it from my desktop and use nano on the Pi to edit.


---
**Erik Scott** *September 20, 2015 15:27*

Well, yeah, but I needed to edit the file before I put it in the Pi.but it seems to be working now. I have bonjour on my laptop, so all I have to do is put OctoPi.local into my browser and I'm all set. I think. 


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/Ky4FFWt5UGs) &mdash; content and formatting may not be reliable*
