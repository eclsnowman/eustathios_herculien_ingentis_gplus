---
layout: post
title: "Now that I am coming closer to working on my z-bed for my Herculien"
date: June 02, 2015 04:12
category: "Discussion"
author: Dat Chu
---
Now that I am coming closer to working on my z-bed for my Herculien. I am in need of some help getting the MDF bed routed. I could stop my build and start working on my OX again but I would rather finish one before getting on another.



Is there any gentleman who would be able to help me route the Herculien MDF piece?





**Dat Chu**

---
---
**Derek Schuetz** *June 02, 2015 04:38*

If you want to save weight, money, and time you could skip the mdf and just insulate with cork


---
**Dat Chu** *June 02, 2015 12:31*

Oh? I take it I still need to route out the piece of cork?


---
**Brad Hopper** *June 02, 2015 13:00*

Just get a couple of cork layers and cut the shapes from the top and middle ones.


---
**Dat Chu** *June 02, 2015 14:01*

That I can do. Any recommendations on vendor for cork layers?


---
**Brad Hopper** *June 02, 2015 14:02*

You can get cork sheets at home depot/ Lowes typically in tile form. Then also the art supply shops like Michaels


---
**Dat Chu** *June 02, 2015 14:08*

Sweet. Another trip to Home Depot. Thank you guys. I wonder, can one route cork plate?


---
**Eric Lien** *June 02, 2015 14:40*

I should have sent you my old one. I used cork for the first year or so. It works fine. I used 1/4" hard board on the flat bottom to sandwich the cork up to the aluminum plate. One problem is most craft store 1/4" cork only comes in 12" squares. So you need to patchwork it together.﻿


---
**Derek Schuetz** *June 02, 2015 23:21*

You could always insulate with foil that's what I did on my euth and have had no issues with heat up 


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/ACppNbGA9h3) &mdash; content and formatting may not be reliable*
