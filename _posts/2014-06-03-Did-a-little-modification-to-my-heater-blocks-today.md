---
layout: post
title: "Did a little modification to my heater blocks today"
date: June 03, 2014 23:03
category: "Show and Tell"
author: Brian Bland
---
Did a little modification to my heater blocks today.

![images/24867d266239898336bf4a893efa1e8e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/24867d266239898336bf4a893efa1e8e.jpeg)



**Brian Bland**

---
---
**Jason Smith (Birds Of Paradise FPV)** *June 03, 2014 23:37*

I'm doing this this weekend! My #Eustathios is down for the second time due to kapton tape issues causing thermistor failures. This is a terrible, terrible part of the e3d v5 hotend design. I've debated redesigning my carriage for the v6, but I'll give this a shot first. It looks great. Are those thermocouples?


---
**Carlton Dodd** *June 04, 2014 00:01*

Yep, gonna have to do this.


---
**Brian Bland** *June 04, 2014 00:16*

Thermistors.  


---
**Eric Lien** *June 04, 2014 01:50*

**+Jason Smith** I am trying to design one for the v6 using 40mm fans. Its a tight fit. I am gonna start over tonight and take a fresh look at it again.


---
**Tony White** *June 04, 2014 01:56*

Makes me wonder if the kraken I just ordered for my  #Eustathios build will come with the fiberglass sleeves or if I'll be fighting kaptop with the thermistors. **+Sanjay Mortimer**?


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/SWJX9eYMpKi) &mdash; content and formatting may not be reliable*
