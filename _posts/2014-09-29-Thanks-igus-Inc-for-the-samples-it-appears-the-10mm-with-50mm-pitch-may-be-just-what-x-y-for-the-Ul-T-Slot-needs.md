---
layout: post
title: "Thanks igus Inc. for the samples it appears the 10mm with 50mm pitch may be just what x/y for the Ul-T-Slot needs"
date: September 29, 2014 15:42
category: "Show and Tell"
author: D Rob
---
Thanks **+igus Inc.** for the samples it appears the 10mm with 50mm pitch may be just what x/y for the Ul-T-Slot needs. I will also try out these other samples and share my results.﻿



![images/61569da58679d069ec0dd3cb5d076266.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/61569da58679d069ec0dd3cb5d076266.jpeg)
![images/641eb8e29f5f999d7c9bf95443adfe0b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/641eb8e29f5f999d7c9bf95443adfe0b.jpeg)

**D Rob**

---
---
**D Rob** *September 29, 2014 18:33*

**+Shauki Bagdadi** I got a call after getting a bearing sample box a few weeks ago. I told them I loved how professional their sample kit was. I then asked about their lead screws. She said she could get me some samples to test and see if they would fit my needs. There is a little too much play in the more traditional pitches but the 1050 has no perceivable backlash. 


---
**D Rob** *September 30, 2014 00:43*

**+Shauki Bagdadi**  There is no discernible backlash in the nut in the forward and backward movement for the 1050, but the others: the 1002 and the 1012 do have some backlash. All of the nuts on all 3 types have a wobble when lateral force is applied. I think a carriage using the minimalistic nut in the second picture, for the 1050 screw, would work well using 2 nuts with a spring between to add an anti-backlash load.


---
**D Rob** *September 30, 2014 07:02*

**+Shauki Bagdadi** the screws I got are .5m and I cant flex them by hand without a fulcrum. 


---
**D Rob** *September 30, 2014 07:26*

**+Shauki Bagdadi** you know I don't know where to find them. I was sent 3x .5m screws in 3 different pitch types each with 1 nut to run some tests so I could see if they were a viable choice.


---
**D Rob** *September 30, 2014 07:28*

**+Shauki Bagdadi** here you go : [http://www.igus.com/wpck/7835/DryLin_Steilgewindemuttern?C=US&L=en](http://www.igus.com/wpck/7835/DryLin_Steilgewindemuttern?C=US&L=en)


---
**D Rob** *September 30, 2014 07:47*

I actually plan to build a gantry when I receive 3 more and 7 more nuts. The spring load should increase the life as well since as wear occurs the load will remove the backlash automatically. This will be a much more goal oriented testing.


---
**D Rob** *September 30, 2014 21:27*

Thank you **+igus Inc.** for the 3 more 1050 sample lead screws on the way and the 7 nuts for them. I will definitely share my results when the Ul-T-Slot mkIII gets IT'S upgrade.


---
**D Rob** *September 30, 2014 21:29*

**+Shauki Bagdadi** I requested the new samples on the site last night while replying to you with the page link. They called me an hour ago asked what I wanted and why. I told them and they are sending me the rest to be able to build a revised gantry


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/HDmSyULEUrc) &mdash; content and formatting may not be reliable*
