---
layout: post
title: "I hope it's all I need to print ... Haha"
date: May 23, 2016 06:47
category: "Build Logs"
author: Botio Kuo
---
I hope it's all I need to print ... Haha

![images/17122491d0682face97191f56b96ca16.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/17122491d0682face97191f56b96ca16.jpeg)



**Botio Kuo**

---
---
**Roland Barenbrug** *May 23, 2016 07:02*

Good work. Missing a mount for your X (or Y) moter. Can find only one. Some for the bed mount block (can find only one). You have sufficent passthrough bearing holders ;-)


---
**Eric Lien** *May 23, 2016 12:39*

Here is what you need: [https://raw.githubusercontent.com/eclsnowman/Eustathios-Spider-V2/master/Documentation/Pictures/Printed_Parts(Black_With_Hercustruder_Extruder).jpg](https://raw.githubusercontent.com/eclsnowman/Eustathios-Spider-V2/master/Documentation/Pictures/Printed_Parts(Black_With_Hercustruder_Extruder).jpg)


---
**Eric Lien** *May 23, 2016 12:40*

Here it is without the extruder: [https://raw.githubusercontent.com/eclsnowman/Eustathios-Spider-V2/master/Documentation/Pictures/Printed_Parts(Yellow_No_Extruder).jpg](https://raw.githubusercontent.com/eclsnowman/Eustathios-Spider-V2/master/Documentation/Pictures/Printed_Parts(Yellow_No_Extruder).jpg)


---
**Eric Lien** *May 23, 2016 12:42*

Btw if you use the GolMart ball screws you need Walters bed mounts instead of mine: [https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/walterhsiao/1204%20Ballscrew%20Bed%20Support](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/walterhsiao/1204%20Ballscrew%20Bed%20Support)


---
**Botio Kuo** *May 24, 2016 04:25*

One question, if I want to use [http://www.thingiverse.com/thing:854267](http://www.thingiverse.com/thing:854267), Do I still need this parts : MTSGR12*2 ? Thanks


---
**Eric Lien** *May 24, 2016 04:39*

**+Botio Kuo**​ nope. Just go with the ballscrews from GolMart. The lead-screws and lead nuts from the BOM are replaced by the ballscrew. Which is nice. The ballscrew is cheaper.﻿


---
**Botio Kuo** *May 24, 2016 04:41*

**+Eric Lien** Hi Eric, could you tell me where is the lead-screws and lead unts on the BOM pls ? I'm confused ... = _ =  sorry


---
**Eric Lien** *May 24, 2016 04:48*

Ball screw SFU1204 - L425mm RM1204 Ballscrew Ballnut [http://s.aliexpress.com/ZziyQVfM](http://s.aliexpress.com/ZziyQVfM)


---
**Botio Kuo** *May 25, 2016 08:11*

**+Eric Lien** Hi Eric, I didn't see wire on the BOM list. Could you tell me what's kinds of wire I need to buy for Eustathios, please? thank you [http://www.smw3d.com/22-gauge-wire-for-endstops-motors-and-sensors/](http://www.smw3d.com/22-gauge-wire-for-endstops-motors-and-sensors/) or [http://www.ultibots.com/basic-wire-kit/](http://www.ultibots.com/basic-wire-kit/) 


---
**Eric Lien** *May 25, 2016 11:59*

**+Botio Kuo** that gets more tricky. Give me a little time and I will try to make a list. 


---
**Botio Kuo** *May 26, 2016 03:43*

OMG... I just found out I already spent more then 1500 for this printer .... I hope it is working out at all ... God bless me!!! 


---
**Botio Kuo** *May 26, 2016 21:34*

**+Eric Lien** Hi Eric, could I ask a favor , pls pls pls... I'm really stupid with wiring thing and scared to burn my board or my house... if you have time, could you make a list about what kind of cable or wiring I should buy pls? Trust me ... I did some homework... but I don't trust myself ... and just want to make sure everything I used is safe and stable. And you are the best. Thank you. 


---
**Eric Lien** *May 26, 2016 23:03*

**+Botio Kuo** I will try. I am just swamped with work and work travels right now. If someone else could help with what they purchased it would be awesome. My problem is I keep all this sort of stuff around. Or have it at work. So I never made a good list yet since I have no receipts, I just grabbed a chunk from my pile of wires 😞


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/e9oey92a9Ef) &mdash; content and formatting may not be reliable*
