---
layout: post
title: "Hi guys Im building a printer similar to the Ingentis/Eustathios, but using OpenBeam (only extrusions I could find in South Africa when I started the project)"
date: June 10, 2014 12:30
category: "Discussion"
author: Pieter Swart
---
Hi guys

I’m building a printer similar to the Ingentis/Eustathios, but using OpenBeam (only extrusions I could find in South Africa when I started the project).  



At the moment I feel somewhat stuck as I can not seem to find perfectly straight shafts anywhere. I’ve tried companies here in South Africa, and overseas (Makers Toolworks and igus) but all the shafts that I have received to date have had a slight wobble in them. This might not have been such a big issue if the shafts were static, but since they rotate (to facilitate movement in the X and Y directions), any flaw is magnified and you can literally see how the XY blocks move up and down when the shafts turn. Have you had similar issues? How did you address it or where did you get your supplies from? (a supplier that will export).



![images/23f2f7ad8fe5553fbdb915c468058cb2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/23f2f7ad8fe5553fbdb915c468058cb2.jpeg)
![images/e1e5ee81633039492b88a8aa67dd5426.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e1e5ee81633039492b88a8aa67dd5426.jpeg)
![images/a70bfbc83fb5e80ad14b8e575cd47b7b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a70bfbc83fb5e80ad14b8e575cd47b7b.jpeg)

**Pieter Swart**

---
---
**ThantiK** *June 10, 2014 12:34*

Misumi is going to be your friend here.  I'm almost certain they will be able to get rods to you, and they are of high quality.


---
**Pieter Swart** *June 10, 2014 12:49*

Hi Anthony 

Stupid question, but did you use these same guys for the linear shafts on your Ingentis. Getting stuff to South Africa is crazy expensive at the moment and knowing myself I'm worried how I'm going to react if I receive another set of ‘useless stuff’


---
**ThantiK** *June 10, 2014 12:51*

Yep, used them for my rods; they were packed very well, in individually sealed plastic containers - perfectly straight.  I was lucky and got them through the  #First150  promotion that was going on a couple months ago.


---
**Pieter Swart** *June 10, 2014 13:15*

I can't register on the American site. If I go to the European site, it states on the registration page that "MISUMI does not deliver to any private individuals...". That doesn't sound good.


---
**Eric Lien** *June 10, 2014 13:21*

Looks like they are a distributor :[https://www.google.com/url?sa=t&source=web&rct=j&ei=SQaXU5ebLo-WyATR34LIAQ&url=http://oilless.engler.co.za/misumi&cd=2&ved=0CCEQFjAB&usg=AFQjCNGlSU04I7ED7SnibR6putHtOzhHag&sig2=Ifmkbgo8d4sPEfD_pKNKtQ](https://www.google.com/url?sa=t&source=web&rct=j&ei=SQaXU5ebLo-WyATR34LIAQ&url=http://oilless.engler.co.za/misumi&cd=2&ved=0CCEQFjAB&usg=AFQjCNGlSU04I7ED7SnibR6putHtOzhHag&sig2=Ifmkbgo8d4sPEfD_pKNKtQ)﻿


---
**Pieter Swart** *June 10, 2014 13:29*

Thanks Eric. I was really hoping for an online shop  but will definitely try them. 


---
**Tim Rastall** *June 10, 2014 17:50*

**+Pieter Swart** reship.Com 

Set up an account,  they will give you a us address,  you can use that to register. 

It took me 3 trys with a Chinese supplier to get shafts without run out. You can straighten hardened shafts though! 

How to straighten bent 8mm metal shaft accurately…: 
{% include youtubePlayer.html id=mRtIxG2co5w %}
[http://youtu.be/mRtIxG2co5w](http://youtu.be/mRtIxG2co5w)


---
**Pieter Swart** *June 10, 2014 18:15*

Great tip. Thanks Tim. The first batch I received from igus was really bad. The second batch looked better, but I was a bit disappointed because I thought they at least would provide me with proper shafts. 


---
**Pieter Swart** *June 12, 2014 12:39*

Hey guys.

So I got a quote from that local distributor. For four shafts at 430mm it will cost about $318 and take 3 weeks to deliver. That sounds a bit steep. The price I got from the euro site excluding shipping was $86 with 4 days to ship. I'll have to look at [reship.com](http://reship.com) to see if they can do this any cheaper.


---
**Miguel Sánchez** *October 21, 2014 11:13*

I  paid $12 for four 250mm-long 8mm rods (mine is a small printer) shipped from a random seller on Aliexpress [http://www.aliexpress.com/item/Free-Shipping-for-Diameter-8mm-L-200mm-chrome-plated-Linear-Shaft-Hardened-Rod-Linear-Motion-for/1097013413.html](http://www.aliexpress.com/item/Free-Shipping-for-Diameter-8mm-L-200mm-chrome-plated-Linear-Shaft-Hardened-Rod-Linear-Motion-for/1097013413.html). Three of them were bent (like 0.040mm according to my dial indicator). Unfortunately I have already provided feedback to the seller as the problem only become evident when turning the rods. I contacted the seller and got a replacement at no extra cost. This time they are ok.  


---
**Pieter Swart** *October 21, 2014 12:31*

I ended up installing locally produced shafts that weren't hardened. Hope your new set runs fine


---
*Imported from [Google+](https://plus.google.com/117269052061351663459/posts/Z7fQQZxHDNY) &mdash; content and formatting may not be reliable*
