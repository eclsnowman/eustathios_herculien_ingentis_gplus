---
layout: post
title: "Here's a printed version of the spectra model xy end"
date: March 16, 2014 04:17
category: "Show and Tell"
author: Tim Rastall
---
Here's a printed version of the spectra model xy end.  And some renders of the belt version. 



![images/2bb6f44d77c2c5ec5d1da8b8e58902f9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2bb6f44d77c2c5ec5d1da8b8e58902f9.jpeg)
![images/4964876bbab5c125b8e7ac1bbbefc6e5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4964876bbab5c125b8e7ac1bbbefc6e5.jpeg)

**Tim Rastall**

---
---
**Tim Rastall** *March 16, 2014 04:18*

**+Riley Porter**


---
**Riley Porter (ril3y)** *March 16, 2014 04:18*

Love it. Just tell me when to print. 


---
**D Rob** *March 16, 2014 04:34*

**+Tim Rastall**  what tensioning method


---
**Riley Porter (ril3y)** *March 16, 2014 05:03*

**+D Rob** the method **+Jason Smith**  and I came up with.  See the separation?  the belt pulls it apart.  I can take a pic tomorrow to illustrate it more clearly.


---
**Tim Rastall** *March 16, 2014 05:04*

**+D Rob** the tensioner on the belt version uses the 2 screws oriented parallel to the shaft to pull the floating clamp towards the fixed one. Designed to use 40mm m3 screws and has about 10mm of movement.  

Spectra one just uses the standard Tantillus spectra tightening method,  just has much more surface area to clamp the line. 


---
**D Rob** *March 16, 2014 05:19*

**+Tim Rastall** will the floating clamp cause problems with the play that it could probably have? Why not do the floating clamp on spectra? have an anchor tied on the ends and tighten the bolts also this would allow for the tensioner to be separate using elongated slots for m3 bolts to attach to the xy end. this would allow for easy fine tuning


---
**Tim Rastall** *March 16, 2014 05:28*

**+D Rob** basically because I want to move to xy end independent of the spectra once tensioned.  Fixing one of both ends will prevent that. 


---
**Nazar Cem** *March 16, 2014 05:28*

I wonder if it would help if you added a rail design between the main part and the floating part so the screws only have to worry about axial force?


---
**Tim Rastall** *March 16, 2014 05:30*

Oh,  and on the belt version, I'm working on a guide slot for the floating clamp but the 2 screws should keep it steady as they are centred above and below the middle of the belt. 


---
**Tim Rastall** *March 16, 2014 05:32*

**+Nazar Cem** Hah.  Just read your comment.  Yes,  that's what I'm doing tonight :) 


---
**John Lewin** *May 19, 2014 15:51*

What became of this? The design seemed like a big improvement with a lot of shared ideas coming together but then never appeared. Is this part of the forthcoming set of files to be released or did this approach not pan out?


---
**Tim Rastall** *May 19, 2014 18:41*

**+John Lewin** the design is complete but I've not tested it yet. The files are up on google drive in IGES format. 


---
**John Lewin** *May 20, 2014 17:23*

**+Tim Rastall**  Thanks for the update and it's exciting to finally have one printed to review. Does the additional vertical riser (not present in these images) add support for the Spectra scenario as well?


---
**Tim Rastall** *May 20, 2014 19:03*

**+John Lewin** Yep,  that's the intent.  An additional clamping plate would be required to secure the spectra.  I'd intended to use a piece of aluminium. 


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/AYCNTAtAQtb) &mdash; content and formatting may not be reliable*
