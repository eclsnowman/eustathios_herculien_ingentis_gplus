---
layout: post
title: "What a difference a tap makes. I was having awful problems tapping the centre of the extrusions, the tap was getting gummed up with aluminium so I decided to treat myself to a new tap"
date: February 01, 2015 08:02
category: "Show and Tell"
author: Richard Mitchell
---
What a difference a tap makes. I was having awful problems tapping the centre of the extrusions, the tap was getting gummed up with aluminium so I decided to treat myself to a new tap.



Once I started using the new tap everything was fine. I used a piece of wood with a 5mm hole drilled through it with a pillar drill to start it off nice and parallel. After that I backed the tap out and continued by hand. Using a ratchet tap handle was ideal.



I then made a load more of my cheap tnuts (grind two edges down a bit off normal M5 nuts) and loaded them into the extrusions and put the thing together.



It's terribly out of true and after looking the Eustathios design does have many braces so that's my next job.



![images/6c181fdfad387567b6647e294fd3e3e8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6c181fdfad387567b6647e294fd3e3e8.jpeg)
![images/f81b3f662125ef7d2530a099f0ee1959.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f81b3f662125ef7d2530a099f0ee1959.jpeg)
![images/f9cf634b15336531e8cefc7bbdb9544b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f9cf634b15336531e8cefc7bbdb9544b.jpeg)
![images/7910167c6bf88edc80e2659de0d775cb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7910167c6bf88edc80e2659de0d775cb.jpeg)
![images/0795d229fe9629a650c93bc7a684f511.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0795d229fe9629a650c93bc7a684f511.jpeg)
![images/5cf460ee5ea788d3994f45de94e54e04.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5cf460ee5ea788d3994f45de94e54e04.jpeg)
![images/96448e36089d07e519fc0e5735ba5a52.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/96448e36089d07e519fc0e5735ba5a52.jpeg)

**Richard Mitchell**

---
---
**Mutley3D** *February 01, 2015 08:17*

I put my tap into a cordless drill, dip the tip of the tap into some lube/cutting fluid and then just drive it in all the way and reverse out. Another dip in cutting fluid and straight into next extrusion. Brutal but effective :) your new tap also appears to be coated so that will make it much better too.


---
**Richard Mitchell** *February 01, 2015 10:13*

I did think about doing that but the issues with the first tap made me very wary of messing up the horizontals. I was practicing on the verticals to get the technique right.


---
**Brad Hopper** *February 01, 2015 13:46*

I've heard others swear by self tapping screws. Seems like less work if it dies indeed work.


---
**Richard Mitchell** *February 01, 2015 16:04*

I wanted to be able to take it apart and put it back together without problems and you can only do that so many times with self tappers in my experience.


---
**Mutley3D** *February 01, 2015 16:41*

Self tapping screws for extrusions have a triangular form about the circumference of the thread that allows them to be self-tapping. They work but tapped hole with corresponding standard machine screw provides greater strength.


---
*Imported from [Google+](https://plus.google.com/111547506541230089782/posts/dgaTpVNFBrr) &mdash; content and formatting may not be reliable*
