---
layout: post
title: "MendelMax Mega Y axis: everyone watching and doubting the dc servo technique I'm wanting to explore check out the y on this"
date: October 01, 2014 15:05
category: "Discussion"
author: D Rob
---
MendelMax Mega Y axis: 
{% include youtubePlayer.html id=BsK9JfH_l6M %}
[http://youtu.be/BsK9JfH_l6M](http://youtu.be/BsK9JfH_l6M) everyone watching and doubting the dc servo technique I'm wanting to explore check out the y on this. Dc motor w/rotary encoder. It uses gecko drive plugged into your current electronics and firmware. The gecko reads steps/dir and handles the rest. Looking petty awesome!





**D Rob**

---
---
**Dale Dunn** *October 01, 2014 16:06*

Very nice. How is holding torque compared to a stepper of similar size? Also, how expensive do you think a complete set would be? Are we looking at a few hundred, or more than a thousand?


---
**Miguel Sánchez** *October 01, 2014 16:28*

**+D Rob** I have already ordered 5 DC motors w/AB encoder 888 pulses/rev :-)


---
**Ben Delarre** *October 01, 2014 17:02*

Very interested in this too. As I understand it moving to DC servos will help with accuracy (smaller step gradations?) Speed and noise? These sounded quite loud on this video though or is that just bearing noise?


---
**Miguel Sánchez** *October 01, 2014 17:05*

**+Ben Delarre** main advantage would be the closed feedback loop (no more skipped steps). Sample motor is likely too expensive and too large, same goes with the driver, but it proves the point. 


---
**ThantiK** *October 01, 2014 17:28*

**+D Rob**, that is one of my local friends, **+Chris Purola** who did that. 


---
**Chris Purola (Chorca)** *October 01, 2014 17:54*

For reference, and to show it actually does print, here's it's first print: 
{% include youtubePlayer.html id=3xsy0GEQRPA %}
[http://youtu.be/3xsy0GEQRPA?t=4m43s](http://youtu.be/3xsy0GEQRPA?t=4m43s)



The reason I went with it is that as our machines get bigger and bigger, and we are printing bigger things, they become worth more plastic if something fails, and the one thing I didn't want, especially with the y axis having to move all that weight, was to skip a step on a huge print and lose hours of time and dollars of plastic. The servo is oversized because it is a silly printer design, but it's the insurance that counts.

I'd like to see some code for our firmwares that can accept an output from the GeckoDrive to determine when a motion failure occurs, pause the print, re-home the x/y, and pick back up where it left off!


---
**Chris Purola (Chorca)** *October 01, 2014 17:59*

An open-source servodrive that doesn't cost $120 would be nice too! Since each motor requires a separate drive, running 4 motors on a printer would be almost $500 in drives alone! The Geckos are huge, handling 90VDC at 20A, which is way more than a small printer needs.


---
**D Rob** *October 01, 2014 21:13*

**+Chris Purola** I bet these could be reverse engineered and ramped down for lower power applications (and cheaper components). Getting this out there and raising interest is the first step to getting prices down.


---
**Chris Purola (Chorca)** *October 01, 2014 23:06*

I believe Makerbot tried to make one way back in the day... [http://www.thingiverse.com/thing:5614](http://www.thingiverse.com/thing:5614)
I'm not sure about the firmware on it, but the board was specced to run 2.8A for the motors. That could be bumped with some FETs and heatsinking, a little redesign.

There's various things throughout the net; one of them that seems popular on the CNC forums is [http://gsst.wikispaces.com/](http://gsst.wikispaces.com/) , however it appears that while the board layouts are public, the software being used on the chips is kept closed.



I believe the GeckoDrives use a CPLD running their own software to get analog-like response speeds.. that's something pretty important when moving loads around quickly. Maybe ARM would be advantageous?


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/XfgmeaYSDBW) &mdash; content and formatting may not be reliable*
