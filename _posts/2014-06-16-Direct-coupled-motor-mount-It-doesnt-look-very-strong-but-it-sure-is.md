---
layout: post
title: "Direct coupled motor mount. It doesn't look very strong but it sure is"
date: June 16, 2014 01:00
category: "Show and Tell"
author: Wayne Friedt
---
Direct coupled motor mount. It doesn't look very strong but it sure is.



![images/f14b5eec516809095453634637fc73fc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f14b5eec516809095453634637fc73fc.jpeg)
![images/b7057f81f00e5d757f4f40d570e41bd9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b7057f81f00e5d757f4f40d570e41bd9.jpeg)
![images/248876795788dd1056418ed11d821d81.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/248876795788dd1056418ed11d821d81.jpeg)

**Wayne Friedt**

---
---
**D Rob** *June 17, 2014 15:14*

Really! 2 days and no comments on this..  Well I think your design is awesome. I believe **+GoopyPlastic** did this on his t-slot tantillus. But I really like the execution here compared to the 4 tube risers he used. Good work!


---
**Wayne Friedt** *June 17, 2014 23:59*

Thanks **+D Rob** I always follow your work.


---
*Imported from [Google+](https://plus.google.com/+WayneFriedt/posts/78sPML65gZi) &mdash; content and formatting may not be reliable*
