---
layout: post
title: "welp. my Viki2 keeps giving me a white screen that i have to change back manually in contrast setting in its menu"
date: February 13, 2016 04:49
category: "Discussion"
author: Jim Stone
---
welp. my Viki2 keeps giving me a white screen that i have to change back manually in contrast setting in its menu. but it keeps coming back.



every boot it starts white.



have to change it back manually



even after changing back manually something simple like an m106 changes it back to white



Firmware settings hi number. low number doesnt matter always will boot snow white.





**Jim Stone**

---
---
**Tomek Brzezinski** *February 13, 2016 18:15*

I have no experience with the Viki2, but I'd check any ground wires if relevant between the MCU and the screen.  I'd also see which commands change it back. E.g, is M106 the only thing or do other commands? IDK, just troubleshooting to find more details. 



How do you change it back manually?


---
**Jim Stone** *February 13, 2016 18:17*

Click the click wheel  go into the control menu and change lcd contrast.



Same as any other lcd. 


---
**karabas3** *February 14, 2016 07:17*

Ok, I played with AZSMZ display on arduino2560+ramps. It's the same as viki2 for firmware. Default contrast is 31 but I need to set it to max (63) for good picture. So go to firmare, find and change default contrast.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/bcmbhhDAaAB) &mdash; content and formatting may not be reliable*
