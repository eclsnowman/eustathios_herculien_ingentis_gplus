---
layout: post
title: "Hey, where can I find the 6900 series bearings?"
date: July 11, 2016 03:53
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hey,  where can I find the 6900 series bearings? I can't find them on Robotdigg. (Listed on BOM as 5972K164)





**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *July 11, 2016 04:04*

They are from fasteddies bearings: [http://www.fasteddybearings.com/10-units-10x22x6-rubber-sealed-bearing-6900-2rs/](http://www.fasteddybearings.com/10-units-10x22x6-rubber-sealed-bearing-6900-2rs/)


---
**Sean B** *July 11, 2016 12:39*

I purchased from Fasteddy also.  Make sure you order 8.  For some reason my BOM had 2 6900 bearings as 5972K164 from Robotdigg, and the 6 others from fast eddy.  I would jsut order all 8 from Fast Eddy.  Eric, is this a mistake or is my BOM messed up?


---
**Eric Lien** *July 11, 2016 12:44*

I will have to check the BOM


---
**Eric Lien** *July 11, 2016 12:46*

Looks right on the summary tab to me: [http://i.imgur.com/Vgx53Dq.jpg](http://i.imgur.com/Vgx53Dq.jpg)


---
**Sean B** *July 11, 2016 12:47*

I've modified mine a bit, apparently to my own detriment :-)




---
**Makeralot** *July 11, 2016 16:59*

We sell 6900ZZ bearing.  :) [https://www.makeralot.com/6900zz-ball-bearing-10x22x6mm-p314/](https://www.makeralot.com/6900zz-ball-bearing-10x22x6mm-p314/)


---
**Stefano Pagani (Stef_FPV)** *July 12, 2016 02:55*

Odd, mine says 2 in the Robotdigg section, same as **+Sean B** I'll be sure to order them, thanks! Does it really matter what PSU I get if it has 24V 200W? I was thinking of this one: [https://www.amazon.com/gp/product/B019RNKSNY/ref=crt_ewc_title_gw_3?ie=UTF8&psc=1&smid=A1THAZDOWP300U](https://www.amazon.com/gp/product/B019RNKSNY/ref=crt_ewc_title_gw_3?ie=UTF8&psc=1&smid=A1THAZDOWP300U) | but I don't want to risk the PSU failing and frying my printer. Will the 40+ units of the same specs have cleaner power? Those are also UL listed.


---
**Eric Lien** *July 12, 2016 04:20*

**+Stefano Pagani** that one looks fine. Just look for good rating and good sellers.


---
**Stefano Pagani (Stef_FPV)** *July 12, 2016 12:37*

Ok, thanks!


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/eh16ZhzWTm2) &mdash; content and formatting may not be reliable*
