---
layout: post
title: "My baby's first steps... I couldn't be more proud"
date: September 02, 2014 19:50
category: "Deviations from Norm"
author: Jason Barnett
---
My baby's first steps... I couldn't be more proud.



OK, so lots of problems during this print; the first ~4mm are all jacked up due to poor bed adhesion and improper feed rate for the extruder. After that, it was printing nicely, for about 45min then I stepped out to brush my teeth and when I came back in it had popped loose from the bed and did a nice air print.



The X, Y and Z calibration all look good, but found a couple of design issues I need to address. Right now, the bed is held in place with painters tape and shimmed with credit cards to make it level :) 



Lots of work left to do, but it looks promising, and MUCH quieter than my Cupcake.




**Video content missing for image https://lh4.googleusercontent.com/-kqbPJ7SCIO0/VAYdrfUkxvI/AAAAAAAAFno/9-F62yzMaPo/s0/VID_20140902_011207.mp4.gif**
![images/2084641850133b14899ffd973328feba.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2084641850133b14899ffd973328feba.gif)
![images/26d2c1adbe9381b17f13773caa908cda.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/26d2c1adbe9381b17f13773caa908cda.jpeg)
![images/5279117ea266bdf3208c38d4cf72e8e6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5279117ea266bdf3208c38d4cf72e8e6.jpeg)
![images/9ddd4ce43d99787a94e501965b90fb98.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9ddd4ce43d99787a94e501965b90fb98.jpeg)
![images/f50b57718d3bea36b6220fb5b19aff07.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f50b57718d3bea36b6220fb5b19aff07.jpeg)
![images/e1b8d9e29f14289bd80975abd4d7f781.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e1b8d9e29f14289bd80975abd4d7f781.jpeg)
![images/e54e3dcaeb35a129b5bc7d96327c8682.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e54e3dcaeb35a129b5bc7d96327c8682.jpeg)
![images/f6f5a834cea7af16b9712d750c23a08f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f6f5a834cea7af16b9712d750c23a08f.jpeg)

**Jason Barnett**

---
---
**James Rivera** *September 02, 2014 20:01*

Looking good!  Your prints will likely get much better and more reliable once the bed is properly secure and leveled.  Getting that first layer right is important.


---
**Jason Barnett** *September 03, 2014 04:53*

I was amazed at how well the wall came out. The rest was crap, but for known reasons which I am now trying to fix.

I had begun to loose motivation on this printer and needed a nice win, to get my motivation back and this did it! :)

Need to re-design my rod mounts, as they kept popping loose during the printing. I have an azteeg mini x5 that I will be using once this is done, but am using a cheap (read as crappy) Printrboard clone that I don't care if it accidentally gets blown up, while testing. 

My goal is, to have this printer working well enough to print its own replacement parts and upgrades, by the end of the weekend. 


---
*Imported from [Google+](https://plus.google.com/108834075676294261589/posts/FzQFMLnxpdD) &mdash; content and formatting may not be reliable*
