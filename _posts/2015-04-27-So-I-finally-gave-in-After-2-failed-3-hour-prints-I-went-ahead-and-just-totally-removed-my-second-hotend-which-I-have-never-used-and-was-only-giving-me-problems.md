---
layout: post
title: "So, I finally gave in. After 2 failed 3-hour prints, I went ahead and just totally removed my second hotend which I have never used and was only giving me problems"
date: April 27, 2015 19:52
category: "Discussion"
author: Erik Scott
---
So, I finally gave in. After 2 failed 3-hour prints, I went ahead and just totally removed my second hotend which I have never used and was only giving me problems. So, my question to you all is, has anyone gotten dual extrusion to work on an Eustathios? I'm 99%sure I'm going to move to Eric Lien's single extruder carriage during my coming printer re-build, but I wanted to pick some brains to see if it's possible to salvage the dual extrusion configuration I was so keen on having. If you HAVE got dual extrusion working reliability, can you share your setup?





**Erik Scott**

---
---
**Dat Chu** *April 27, 2015 19:53*

Can you elaborate more on why it didn't work for you? 


---
**Erik Scott** *April 27, 2015 20:01*

If I have parts laid out along the x axis, the second extruder would tend to catch plastic that might have curled up, and the printer will miss steps. 



Additionally, I couldn't get the 2 nozzles at the exact same height, and so the few times I did try dual extrusion, it just didn't work. Luckily, the second was higher than the first, so I could do most prints just fine without interference. 



I really wanted to use water-soluable PVA so I could print complex parts without having to worry about breakaway supports. But now I'm wondering if it's worth it. 


---
**ThantiK** *April 27, 2015 20:22*

Really, the only multi-material/color solution I could recommend is the Cyclops.  We've had some pretty good experience with a replica-ish version on the DeltaMaker.  Even that though, is prone to potential issues.  Incompatibility with material types, etc


---
**Eric Lien** *April 27, 2015 20:32*

**+Erik Scott** I fully agree, the idea of dual extrusion setup is far less glamorous than the reality and headaches. But I should make a variant of my carriage for cyclops, just so the option is available.


---
**Chris Brent** *April 27, 2015 21:00*

I saw this today:

[https://www.kickstarter.com/projects/mosaic3d/the-palette-3d-printing-evolved?ref=nav_search](https://www.kickstarter.com/projects/mosaic3d/the-palette-3d-printing-evolved?ref=nav_search)

No idea if it would work but it's an interesting approach. I still think the pain point will be generating a slicing multiple STL's to get it right.


---
**Erik Scott** *April 28, 2015 00:14*

Pallet is interesting. I would totally go for the cyclops. However, E3D seems to imply that you have to use the same kind of material in both sides as they're mixed together. It might be do-able with thorough purging though. 



My understanding is many industrial printers use a single nozzle to avoid the spacing and collision issues. 



At any rate, the cyclops may be worth it anyway, even if only for the dual color abilities. 


---
**Erik Scott** *April 28, 2015 00:20*

**+Eric Lien** Just had another look at the cyclops. They say individual temps aren't supported, nothing about different materials. Seems like the ABS/HIPS combination may work then. I think a cyclops-compatible carriage would be a great idea. I may have a go at it myself!


---
**Jeff DeMaagd** *April 28, 2015 04:38*

Part of the problem is you need very closely matching nozzle heights. On my machines, I ended up using slips of precision plastic shims to raise one end of the dual extruder body more than the other. That said, I don't have this kind of machine, the Eust and Herc hot end mount is apparently integrated to the carriage, making such fine adjustments a bit difficult without redesigning it.



I think Stratasys, etc. use a rocking cradle to raise the idled nozzle. I don't think that eliminates the need to match the nozzle heights.﻿ Dual nozzle extrusion is attainable without that though.



If you're new to 3D printing, then it's best to get very good at single first.﻿


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/NkAn2Hfqy6u) &mdash; content and formatting may not be reliable*
