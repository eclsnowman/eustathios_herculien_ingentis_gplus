---
layout: post
title: "Here's what my herculien has been up to these past few weeks"
date: April 18, 2015 03:56
category: "Show and Tell"
author: Daniel Salinas
---
Here's what my herculien has been up to these past few weeks.  

![images/f36c899cc86bb950a26662139da6f7a8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f36c899cc86bb950a26662139da6f7a8.jpeg)



**Daniel Salinas**

---
---
**Eric Lien** *April 18, 2015 04:31*

Wow, your prints are looking great. I need to find time to get a hobby like this that isn't just building more printers.﻿


---
**Brandon Satterfield** *April 18, 2015 05:02*

Totally agree! I want a drone so bad but spend all my time making more machines!


---
**Eric Lien** *April 18, 2015 05:24*

Did you model these? They look great.


---
**Daniel Salinas** *April 18, 2015 05:30*

Yes I made every piece in solidworks


---
**Daniel Salinas** *April 18, 2015 05:41*

The big one is 800mm motor to motor diagonally. It is going to carry a couple gopros and lots of batteries to do aerial photos. The smaller one is just a project I started one night when I was feeling inspired.


---
**Carl Hicks** *April 18, 2015 06:29*

The small one looks like the main section is one peice. Good use of the big build area. 


---
**Øystein Krog** *April 18, 2015 07:03*

Looks really nice!

What material are you printing with?

How well does 3d printed drone parts do compared to things you can buy?

I suppose it probably requires more mass for the same strength/stiffness?


---
**Mutley3D** *April 18, 2015 08:27*

**+Daniel Salinas** To say those were awesome would be understated, would love a quad myself, I'm in a great location for flying them too but as others have mentioned I find myself so busy with the printers themselves. Do you print the propellors too or buy those in?


---
**Gus Montoya** *April 18, 2015 09:11*

I have a couple if things up my sleeve also. Great prints! You inspire me to keep going.


---
**Eric Lien** *April 18, 2015 11:57*

**+Daniel Salinas** for a guy who didn't know solidworks before you are doing great. Holy cow. Sorry we never got that hangout going to show you some tricks... But it looks like you don't need my help ;)


---
**Daniel Salinas** *April 18, 2015 13:36*

**+Carl Hicks**​ yep it started as a 310x310mm square.



**+Øystein Krog**​ I print almost exclusively in ABS. I've found that you definitely have to go for thickness with strategic cutouts to keep rigidity high and weight low. 



**+Mutley3D**​ I don't print props yet. But I haven't given up on the idea.



**+Eric Lien**​ its been a ton of work but a whole lot of fun. 


---
**Eric Lien** *April 18, 2015 13:56*

**+Daniel Salinas** you willing to share the model and component selection? I really want to get into this, but I am not up to speed on what to buy.


---
**Daniel Salinas** *April 18, 2015 14:18*

Totally. Once I get the last components in and I test. Then I'll put everything on youmagine. In the interim I'm happy to chat about it. 


---
**Brandon Satterfield** *April 18, 2015 15:17*

Looking forward to this as well, want a drone so bad.




---
**Joe Spanier** *April 18, 2015 15:49*

I'm building a hovership right now. Already found about 12 things I want to change on the frame so I'll probably start modeling something in a few weeks. 



I wanna hear how your big one flys


---
**Daniel Salinas** *April 18, 2015 21:34*

**+Joe Spanier**​ it will likely need bigger motors and 15 inch props. It should have plenty of lift with the current props but once I load it down with a bunch of battery packs and a camera / gimbal it will likely need more power. 


---
**Oliver Seiler** *April 19, 2015 01:42*

I so want to build one of these as well, but first need to finish my printer and then get up to speed with everything.


---
**Oliver Seiler** *April 19, 2015 01:42*

This looks really interesting (challenging), too

[http://www.cprize.nz/](http://www.cprize.nz/)


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/fmeEUNxuJBN) &mdash; content and formatting may not be reliable*
