---
layout: post
title: "Hand tapping while watching football. After spending my recent hobby budget on trying to fix my existing printer, I'm back to working on the Herculien"
date: January 21, 2019 02:20
category: "Build Logs"
author: William Rilk
---
Hand tapping while watching football.  After spending my recent hobby budget on trying to fix my existing printer, I'm back to working on the Herculien.

![images/15d7412e7887cdc1208fedc86daed27d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/15d7412e7887cdc1208fedc86daed27d.jpeg)



**William Rilk**

---
---
**Ben Delarre** *January 21, 2019 08:48*

I recently discovered spiral point taps which can be used with a drill without having to back them out every half turn. Can tap a whole bunch of extrusions really quick. Totally worth the investment!


---
**William Rilk** *January 22, 2019 04:03*

I've been sorely tempted to use a drill, but I've gotta admit I'm more than a little chicken.  I have some SAE spiral taps, but I've only used them on thinner material with a hand drill.  Even then, if you breathe wrong, they're likely to get a few choice words out of you.  Again, I'm jus too chicken😕


---
**Jeff DeMaagd** *January 22, 2019 11:46*

You’re right to be wary because taps are easy to break. You could use the spiral point tap but in a tapping handle. You keep the hand feel but save a lot of time by avoiding the reverse quarter turn for every turn / half turn.



Really, I feel like I’m far more likely to get a good thread by hand taping with a spiral point machine tap than hand tapping with a “hand” tap. Machine taps seem easier to start.


---
**Michael K Johnson** *January 27, 2019 20:40*

When I am tapping plastic (usually, threads in 3d printed objects) I exclusively use spiral taps. They throw a nice spiral chip out the flutes. I have broken an M3 spiral tap in 6061 aluminum, but only when it was at nearly full depth.


---
*Imported from [Google+](https://plus.google.com/100191047182984055447/posts/eqjEkeTNBoT) &mdash; content and formatting may not be reliable*
