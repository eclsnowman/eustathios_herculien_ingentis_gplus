---
layout: post
title: "I posted this pic over at the 3D Printing page but thought I'd post here too"
date: April 14, 2017 20:46
category: "Discussion"
author: jerryflyguy
---
I posted this pic over at the 3D Printing page but thought I'd post here too. Broke one of the rubber belts last night. The other 3 are the white steel core variety. 



Not sure if I should try to buy higher quality rubber (Genuine Gates or similar) or stick to the steel core? 





![images/dd5c1030b2207760b55c534dc930158b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dd5c1030b2207760b55c534dc930158b.jpeg)



**jerryflyguy**

---
---
**Jeff DeMaagd** *April 14, 2017 20:48*

Go with Gates GT3 belt. Steel core belt is just going to break again because it's flexing too tightly, the cords will fracture eventually. Genuine Gates belt doesn't stretch under typical 3D printer loads.


---
**jerryflyguy** *April 14, 2017 23:15*

Where's my best option for buying them in North America? I'm in Canada which can curb my supply scope a fair bit further 🤔


---
**Jeff DeMaagd** *April 15, 2017 03:34*

I''m lost short of buying from SDP/SI. It looks like they do have a Canadian distributor but I don't know anything about them.



[http://emtcanada.com/](http://emtcanada.com/)


---
**jerryflyguy** *April 15, 2017 17:48*

**+Jeff DeMaagd** yeah.. but their shipping is mind boggling, even for small orders. Unfortunately. I'm sure Misumi does also but same problem 😏


---
**Matthew Kelch** *April 16, 2017 12:12*

I've not had any luck finding as US based distributor for the Gates GT3 belts. 



If anyone finds one please post it. 


---
**jerryflyguy** *April 17, 2017 21:06*

I ended up buying closed loop belts from SDP/SI. As much as I hate paying their shipping rates it was the only way I could find to get reliable quality belting in Canada. Had to pick and choose to find a length in stock that was long enough. Will post back when I'm up and running. 



I also picked up a spare set of the 10mm bushings while I was at it and will replace a couple that are suspect on my machine. (Still on the hunt for the perfect print, now in the realm of diminishing returns but feel there is still a small amount of room for improvement)


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/he2sbKq67Eg) &mdash; content and formatting may not be reliable*
