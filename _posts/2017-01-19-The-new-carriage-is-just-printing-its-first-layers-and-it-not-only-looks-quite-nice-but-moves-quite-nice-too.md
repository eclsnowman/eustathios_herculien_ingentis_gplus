---
layout: post
title: "The new carriage is just printing its first layers and it not only looks quite nice but moves quite nice too"
date: January 19, 2017 22:30
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
The new carriage is just printing its first layers and it not only looks quite nice but moves quite nice too. I'm happy how this turned out. Didn't think that my first printed version of the new carriage would fit instantly in all terms - even with the clips for the **+igus Inc.** ECLM bushings.



The play of the carriage is <i>way</i> lower than with the ball bearings (i would never talk about zero play just because that doesn't exist <b>g</b>).



Here's some footage.



![images/160b3fc6dc51fd785bf42c140f55aeb5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/160b3fc6dc51fd785bf42c140f55aeb5.jpeg)
![images/4b6d90aba78ce1d1fc59e84994f6474e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4b6d90aba78ce1d1fc59e84994f6474e.jpeg)
![images/10b81bf715f2b125b7f2a31d97c3151f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/10b81bf715f2b125b7f2a31d97c3151f.jpeg)
![images/07f1779ad03d36ed5bf7312654431a09.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/07f1779ad03d36ed5bf7312654431a09.jpeg)

**Frank “Helmi” Helmschrott**

---
---
**Stephen Baird** *January 19, 2017 22:33*

That does look super sharp. Nice work.


---
**Matt Miller** *January 20, 2017 00:00*

Nice videos!  Looking good!


---
**Matthew Kelch** *January 20, 2017 00:56*

Looks great, nice work!


---
**C Hopper** *February 06, 2017 15:55*

Nice carriage, did the Igus bushings live up to your expectations? 


---
**Sean B** *February 27, 2017 20:52*

Any follow-up to this now that you've had it going for a few weeks?  I am looking at alternatives since my bushings are too much play.


---
**Frank “Helmi” Helmschrott** *March 14, 2017 15:24*

Sorry guys for beeing quiet for so long. I didn't have much time for printing lately. Unfortunately I'm not that happy so far. I do still have some serious quality issues and the bushings could definitely be the reason for that. I'm still lacking time to find out so I won't know for sure anytime soon. I will have to nail it done to eventually get to success with the Eusthatios that has been a constant "Build in progress" for me so far.


---
**Frank “Helmi” Helmschrott** *March 17, 2017 13:09*

I'm back after some (but not much) testing. Unfortunately as always if you beginning to change things there are different areas of war fighting. I still struggle to get extrusion done reliably especially in small areas. Still experiencing some blobbing on the e3d and don't know if still cooling might be an issue. But somehow it has always been really nifty with that.



This may also lead to bad layer quality due to the ever changing reliability of extrusion. Hard to say how much the tolerances of the igus bushing play into that.



And if this wasn't enough I've updated the firmware due to some fixes in the distortion correction and now messing up my bed level sensor offsets which leads to some curious faults. And then I just had a random reset during print. So... enough work to do and enough problems to fight where for most of them I don't yet know any reason.



Frustrating situation. I might either spend the weekend in the basement or again delay this to some future point in time. 


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/gDhNkJBJM2J) &mdash; content and formatting may not be reliable*
