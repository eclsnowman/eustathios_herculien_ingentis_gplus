---
layout: post
title: "Hey All, Is there any software out there that allows you to write custom Gcode script and export as .gcode for importing it into repetier or simplify3D?"
date: November 13, 2015 15:02
category: "Discussion"
author: Kaley Garner
---
Hey All,



Is there any software out there that allows you to write custom Gcode script and export as .gcode for importing it into repetier or simplify3D?





**Kaley Garner**

---
---
**Pieter Swart** *November 13, 2015 15:08*

Something like [http://www.openscad.org/](http://www.openscad.org/) ?﻿ Not sure I understand the question!


---
**Kaley Garner** *November 13, 2015 15:27*

Hey Pieter, yeah sort of... it might help to explain what Im trying to do. Essentially,  I created a 5mm(length and width) by .1 mm(height) crosshatch pattern in solidworks and I am using simplify3d to do the slicing. Due to the size and detail of internal parts of the design the slicer doesn't work correctly-either doesn't produce anything at all or loses all of the detail. To bypass this, since it is a fairly easy design I was thinking that I could just write the gcode out in an editor and upload the script into simplify3d, im using an external set up for the extrusion so I only really need the x,y,z movements.


---
**Jason Smith (Birds Of Paradise FPV)** *November 13, 2015 16:05*

One option may be to just scale the part up by a factor of 10 (or 100), generate the gcode, manually modify the gcode in a text editor to scale it back down, and then send to the machine via terminal. 


---
**Pieter Swart** *November 13, 2015 18:30*

Have you tried to fix the generated STL file with Netfab first? The online service (if they still provide it) has worked quite well for me in the past. Stupid question, but what is the thickness of the walls of the pattern? I've designed things in the past with "support material walls" - the walls were so thin though that the slicer couldn't do anything with them (was a long time ago..if anybody wants to know why!.) 


---
**Ian Hoff** *November 13, 2015 20:17*

sounds like you need to detect thin walls, at least thats the slic3r setting not sure about s3d.



Any feature thats smaller than the min extrudiate its problematic it either needs to skip it or draw it larger than the model is.



Detect thin walls tells it to draw it If the lattice onf te model is too much smaller than your minimum extrusion width resizing up and down isnt going to really fix the problem of trying to draw fine detail with a fat line.. 


---
*Imported from [Google+](https://plus.google.com/108812315338031740366/posts/7PuArM7K23X) &mdash; content and formatting may not be reliable*
