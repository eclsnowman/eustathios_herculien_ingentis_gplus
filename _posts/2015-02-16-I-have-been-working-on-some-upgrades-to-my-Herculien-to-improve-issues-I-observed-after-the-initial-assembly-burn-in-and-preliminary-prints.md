---
layout: post
title: "I have been working on some upgrades to my Herculien to improve issues I observed after the initial assembly, burn-in and preliminary prints"
date: February 16, 2015 07:24
category: "Discussion"
author: Marc McDonald
---
I have been working on some upgrades to my Herculien to improve issues I observed after the initial assembly, burn-in and preliminary prints.

Here are some of the changes implemented: 1000W heat bed (Smoothieboard has no issues controlling it using the PID and SSR), all bearings have inner race spacers installed between the GT2 pulley and bearing. All bearing mounts (X, Y and Z) have been replaced with some CNC machined mounts i quickly made.



The smoothieboard and Bondtech extruder are both working weel. I still need to work on some tuning to improve prints.



![images/9728862651003c530a9226265c3527d7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9728862651003c530a9226265c3527d7.jpeg)
![images/9ac7b36af2dcbf675683924374a5bbbf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9ac7b36af2dcbf675683924374a5bbbf.jpeg)
![images/e75ab922118d3770b6b703481f9fbc17.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e75ab922118d3770b6b703481f9fbc17.jpeg)
![images/a27eaacfaa56857c254fe295cb39ea72.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a27eaacfaa56857c254fe295cb39ea72.jpeg)
![images/b4e1f8b288fc9e37b4e289a4902d5bd0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b4e1f8b288fc9e37b4e289a4902d5bd0.jpeg)

**Marc McDonald**

---
---
**Derek Schuetz** *February 16, 2015 07:31*

i like those machined brackets, looks very solid


---
**Marc McDonald** *February 16, 2015 07:48*

**+Derek Schuetz**

The bearing mounts were quick to CNC, I should have cleaned them up better but wanted to test them out. The plastic mounts allowed far to much movement in the bearings that resulted in vibrations I observed in the print head, frame/plastic panels and prints. The Herculien runs far smoother, all the vibrations disappeared, reduced the noise and improved print quality using these machined mounts.


---
**Eric Lien** *February 16, 2015 07:59*

Wow, those brackets are looking great. Wish I had a CNC. Keep us updated. I love to watch all the creativity and variation on these printers.


---
**Dat Chu** *February 16, 2015 16:54*

Do you make changes to the stl files Marc? Can you share the files? I am planning to mill these out when I finish my CNC as well.


---
**Marc McDonald** *February 17, 2015 02:56*

**+Dat Chu** The Z axis mounts are the same as the printed units, the X&Y are new and I will release the two STL files when time permits. I have a CNC mill that is far larger than the OX desktop mill and the current Z axis mounts may be an issue to cut as they are thicker than 1/8". I see most users using an OX mill cut 6061 with 1/8" tools as they have limited power, feed rates, tools do not have long flute lengths and the mill is not really designed to cut 6061/metal. I expect it will take you a long time to cut these on an OX mill.


---
**Dat Chu** *February 17, 2015 03:19*

I see. I do have access to a local makerspace. Perhaps I can fabricate these there once you share the files. Thank you for making this post. The finished Aria Dragon looks really good.


---
**Marc McDonald** *February 17, 2015 03:24*

**+Dat Chu** That was my second print after the upgrade, the Bondtech extruder is working very nicely.


---
**Daniel Salinas** *February 17, 2015 14:31*

**+Marc McDonald** if you're looking to put together a little side business for these aluminum replacements I'd be interested in a set.


---
**Mike Kelly (Mike Make)** *February 17, 2015 19:32*

I'd love the files as well. I have access to someone with a CNC and I'd like an all aluminum herculien. 


---
**Eric Lien** *February 17, 2015 19:51*

**+Mike Kelly** if people make these maybe making more than one set is a good idea. The setup time is already in the first set, so making more shouldn't be too difficult. I would be interested also.


---
**Dat Chu** *February 17, 2015 19:57*

Count me in. I think we are having a mini group buy for these. If Marc can give us some pricing for say 5+ / 10+/ 20+, a mod can make a pinned post so we can get enough people go in on this


---
**Derek Schuetz** *February 17, 2015 21:51*

I'd take a set also 


---
**Eric Lien** *February 17, 2015 21:57*

My only preference would be machining them so the size is fixed. The hardest part about a Eustathios build is alignment of the rods. That's why I made the corner brackets 1 piece. Plus it helps reinforce the corner for rigidity of the frame.


---
**Nathan Buxton** *February 18, 2015 21:51*

I would be interested in buying a set of these too, Marc! (Sorry the prints didn't work out! :( )


---
**Marc McDonald** *February 20, 2015 06:04*

All, I did not expect requests for mounts when I originally posted. The current design uses 12 CNC machined bearing mounts (8 for x/y and 4 for Z axis). The 8 pieces would require small modification (thickness increase in the bearing mount region to make them compatible with the McMaster spacer Eric suggested to correctly line-up the GT2 pulleys and belt tensioners. I used a thicker alternative inner race spacer as McMaster will not ship to Canada and alignment would not be the same with the very thin spacer Eric suggested.



The current design allows for individual shaft alignment in both the x & y axis, a 0.5mm error in cut tolerance/frame assembly can result in a larger error at a 600mm offset (the second mount location) and will be noticeable with the more accurate CNC bearing mounts. A suggested machined single corner dual mount would not allow any adjustment that would be required and could result in poor movement of the print head due to increased stresses. The current printed mounts are forgiving as the bearing fits are very loose and are able to be tilted in the bearing sockets to overcome the frame assembly defects. The plastic corner mounts provide a small amount of corner rigidity, I was able to deform my plastic parts slightly to fit in the corners. I was planning on installing a 

corner bracket for increased rigidity if required but have not found it to be an issue in my build.



I have discussed your request with my business partner and the set-up time, programming, and fixturing would need to be updated as they would be different for a small production run of parts as we would want to improve throughput with minimal material wastage. The cost of any CNC machined component will always be higher in low volume custom work than equivalent printed plastic mounts due to tooling and general machine operating costs that are higher than our 3d printer operating costs. The CNC machined mount provides improved performance, bearing fit (outer races do not spin in the mounts), are not deformed by the enclosure heat or movement of the head assembly mechanics. 



Please contact me If you are interested in ordering the CNC machined Herculien 12 piece upgrade (with 8 updated X/y mounts) and I can get a quote for you.


---
*Imported from [Google+](https://plus.google.com/118118158840729615568/posts/R85X7tzwb7u) &mdash; content and formatting may not be reliable*
