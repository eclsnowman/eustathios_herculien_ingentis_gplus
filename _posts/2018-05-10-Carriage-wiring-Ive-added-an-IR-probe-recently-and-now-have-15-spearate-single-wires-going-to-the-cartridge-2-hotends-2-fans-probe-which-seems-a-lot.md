---
layout: post
title: "Carriage wiring I've added an IR probe recently and now have 15 spearate (single) wires going to the cartridge (2 hotends, 2 fans, probe) - which seems a lot"
date: May 10, 2018 08:21
category: "Discussion"
author: Oliver Seiler
---
Carriage wiring



I've added an IR probe recently and now have 15 spearate (single) wires going to the cartridge (2 hotends, 2 fans, probe) - which seems a lot.

If I would use common GND and 24V wires I could reduce it down to 10. Has anyone done that?

Also, what cables are people using? Would it make sense to use some sort of multi wire cable loom? Are you using cables that are resistant to heat/flames/vibrations?







**Oliver Seiler**

---
---
**James Rivera** *May 10, 2018 15:51*

I’ve heard of some people using ribbon cables for this, and combining several wires for the higher loads likes the hot end, and the ground(s).


---
**Ryan Carlyle** *May 10, 2018 18:32*

Typically all the +24vdc lines can be combined into one wire with enough current capacity. Grounds are switched by the MOSFET on the controller so you need separate wires for those. Always-on loads like lights and hot end fans can be totally combined if you want. Steppers and temp sensors need dedicated wiring. 


---
**Julian Dirks** *May 10, 2018 23:25*

I'm pondering this sort of thing at the moment too.  I have 15 wires too (direct drive extruder).  Also connectors at the hot end - I like the idea of one common connector for all wires rather than a bunch of connectors or soldered connections but interested to hear what others are using?


---
**Oliver Seiler** *May 10, 2018 23:35*

I'd recommend against a single connector, simply because if you ever need to replace a part of it you need to replace the whole connector (unless you have the tools to properly remove the little pins from the plug). I've had this in the very beginning and it just didn't work (and I think there's a post from Eric about that somehwere buried deep down in the group). Also it'd have to be a pretty large one.

I ended up using Molex connectors, 4-pin for hotends, 2-pin for fans, another 4 pin for the IR probe, etc. I've made sure to use the opposite connector type for those I don't want to swap accidentially (e.g. part cooling fan is male on the wiring loom and hotend cooling fan female).

This also gives me heaps of flexibility to plug in hotends in different configurations or use them in my other printer.




---
**Oliver Seiler** *May 10, 2018 23:36*

**+Ryan Carlyle** thanks, I'm aware of that, I was more wondering what others are using and general thoughts before I might go there. Have you tried it?




---
**Oliver Seiler** *May 10, 2018 23:38*

**+James Rivera** hmm, from my experience ribbon cables aren't very robust and I'd be concerned about them simply not lasting (and potentially even frying my board).




---
**Oliver Seiler** *May 10, 2018 23:43*

**+Julian Dirks** Just to clarify, I'm using 3 different carriages and tend to swap hotends between carriages (but not the fans). If hotend and fans were on the same plug it simply wouldn't work

![images/71dc772ce146a6f89a7fdf8cdc9b5dfa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/71dc772ce146a6f89a7fdf8cdc9b5dfa.jpeg)


---
**Oliver Seiler** *May 10, 2018 23:44*

That's the mess I'd like to clean up a little ;)

![images/26f89a90fe2ccd5f739bf70dd8010d3f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/26f89a90fe2ccd5f739bf70dd8010d3f.jpeg)


---
**Julian Dirks** *May 11, 2018 00:03*

**+Oliver Seiler** that’s the one! I like your modular hotend system. I’ve been toying with embedding header pins soldered to chopped up prototyping boards as junction points. I’ve built one at the top of my drag chain so permanent wires from control board there. Good point about having different plugs to avoid accidental frying....


---
**Oliver Seiler** *May 11, 2018 00:20*

I'm using Molex MicroFit 3.0 43020 and 43025. RS Components has them here in NZ

[nz.rs-online.com - 43020-0401 &#x7c; Molex MICRO-FIT 3.0 43020, 3mm Pitch, 4 Way, 2 Row Male Connector Housing &#x7c; Molex](https://nz.rs-online.com/web/p/pcb-connector-housings/6795864/)


---
**Oliver Seiler** *May 11, 2018 00:25*

For 2 pin you can also use both of these to prevent plugging into the wrong plug (clip on the long or short side)

[nz.rs-online.com - 43025-0200 &#x7c; Molex MICRO-FIT 3.0 43025, 3mm Pitch, 2 Way, 2 Row Female Connector Housing &#x7c; Molex](https://nz.rs-online.com/web/p/pcb-connector-housings/2332747/)

[https://nz.rs-online.com/web/p/pcb-connector-housings/4476754/](https://nz.rs-online.com/web/p/pcb-connector-housings/4476754/)


---
**Dennis P** *May 11, 2018 20:01*

**+Oliver Seiler** I would not combine the grounds from 5V and 12/24V. If you somehow get a short, you could backfeed 12V to the 5V side of things.  I have 11 wires for single hot end, fans, therm & senesor. its not pretty either. I though about combining the %v grounds and 12V grounds, but that gets me from 11 to 8 only. 

 


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/JZSV9zXZYwN) &mdash; content and formatting may not be reliable*
