---
layout: post
title: "Any idea why the printer is pausing?"
date: July 14, 2016 23:46
category: "Discussion"
author: Brandon Cramer
---
Any idea why the printer is pausing? 4 hours into a print and I see a couple bubbles and noticed that the printer is pausing.





**Brandon Cramer**

---
---
**Brandon Cramer** *July 14, 2016 23:51*

I guess resting my hand on the printer and recording doesn't produce the best video. 


---
**Brandon Cramer** *July 14, 2016 23:57*

[https://www.dropbox.com/s/85xrk3jnll4bu5f/2016-07-14%2016.55.31.jpg?dl=0](https://www.dropbox.com/s/85xrk3jnll4bu5f/2016-07-14%2016.55.31.jpg?dl=0)




---
**Brandon Cramer** *July 14, 2016 23:57*

[https://www.dropbox.com/s/1k2fwygdm2flze6/2016-07-14%2016.55.24.jpg?dl=0](https://www.dropbox.com/s/1k2fwygdm2flze6/2016-07-14%2016.55.24.jpg?dl=0)


---
**Botio Kuo** *July 15, 2016 00:28*

over hot ?




---
**Derek Schuetz** *July 15, 2016 02:06*

What controller are you using?


---
**Stefano Pagani (Stef_FPV)** *July 15, 2016 02:11*

Are you over USB? This happened to me, too much USB traffic.






---
**Brandon Cramer** *July 15, 2016 02:38*

X5 mini


---
**Miguel Sánchez** *July 15, 2016 10:57*

If using USB please note that besides traffic, communication errors due to interference may slow down data transfer too.


---
**Brandon Cramer** *July 15, 2016 15:09*

Right after this job was done I sped it up 30mms to 80mms. It printed great that time.



It is using USB to the laptop. Do I need to look at getting a high quality USB cable or is there possibly an issue with the X5 mini?




---
**Derek Schuetz** *July 15, 2016 15:31*

Don print over USB is the best solution. Use an SD card


---
**Roland Barenbrug** *July 15, 2016 20:03*

Power saving in laptop  interfering with USB. Made me crazy until last weekend. Problems disappeared when switching off all power savings. Printing now for hours via USB without any glitch.


---
**Jim Stone** *August 15, 2016 05:02*

Use an SD card. you can print much better. less pausing in the prints. and. if the PC goes down. your print wont fail



WIN WIN WIN


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/dLvWiw1xWdZ) &mdash; content and formatting may not be reliable*
