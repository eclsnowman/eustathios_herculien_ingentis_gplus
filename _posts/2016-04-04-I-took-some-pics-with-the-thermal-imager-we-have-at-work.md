---
layout: post
title: "I took some pics with the thermal imager we have at work"
date: April 04, 2016 16:37
category: "Build Logs"
author: Maxime Favre
---
I took some pics with the thermal imager we have at work.

Bed temp is good, sp was 60°C 58-59 in the center, 2°C less on the edges.

I will do some test later at higher temp when the second 3d benchy is finished.

The smoothie driver are hot, a little will be welcome I think.

Motor temp is ok ?



![images/7dd03840ab25a9e79e541aa06ec2e44e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7dd03840ab25a9e79e541aa06ec2e44e.jpeg)
![images/31e070a18bfc81db088d4f3758a6e1eb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/31e070a18bfc81db088d4f3758a6e1eb.jpeg)
![images/4ea6ea6aee0ce026a13bf48cb2cc3229.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4ea6ea6aee0ce026a13bf48cb2cc3229.jpeg)

**Maxime Favre**

---
---
**Eric Lien** *April 04, 2016 16:57*

I would probably add a fan blowing over the drivers just to be safe, and the motor seem to be running a little hot what currents are you running? Also super jealous you have these fun toys at work.


---
**Maxime Favre** *April 04, 2016 17:04*

Hahaha yeah fun toy is the word ;)

I ordered exact same motors as BOM,  17HS2003-N27B so 1.5A.

I double checked, alpha, beta and gamma current are at 1.5A in the config. Maybe belt tension is too high ?!

I can try to run a print with no charge by removing the motors belt


---
**Eric Lien** *April 04, 2016 17:14*

You can probably run at less than max current with good results. I might try 1.2A as a starting point and then tune up or down based on performance.


---
**Maxime Favre** *April 04, 2016 17:15*

update, after 65min print XYZ motors are at 57-62°C and z axis has very little belt tension... I think I will lower the current for now. Any idea were to start searching ?




---
**Jim Stone** *April 04, 2016 18:35*

1.2a and go from there.


---
**Jeff DeMaagd** *April 04, 2016 19:15*

I tend to turn off the visible light camera correlation because it's distracting with little benefit.


---
**Ryan Carlyle** *April 04, 2016 21:14*

1.2A will be much cooler for both the motors and drivers. But keep in mind that they're designed to be run at those temps. You don't NEED to do anything to cool them, those are perfectly acceptable temps as far as the drivers and motors themselves are concerned. (Just watch out for plastic mounting brackets.) 



Any airflow over the entire Smoothieboard will help cool the drivers... They have a ton of heatsinking via copper fills in the PCB. 


---
**Maxime Favre** *April 05, 2016 05:29*

Yep I decreased to 1.2, temp dropped 10-15°. **+Ryan Carlyle**​ Sure smoothieboard has very good thermal dissipation. When turned off it cools from 80° to ambiant in a few minutes.


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/XSa6JwLRdWj) &mdash; content and formatting may not be reliable*
