---
layout: post
title: "Just wanted to share my development in terms of new Extruder Carriage for the Spider V2"
date: January 11, 2017 19:15
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
Just wanted to share my development in terms of new Extruder Carriage for the Spider V2. 



See my current design state with this public Fusion360 link: [http://a360.co/2jw5CQK](http://a360.co/2jw5CQK) (It may change over time and be unusable, I will share it once I have a working "final" version.)



I decided to take some ideas from other Designs I had seen and add some stuff that came to my mind. Of course this is notably inspired by  **+Walter Hsiao** again but I turned the fans around this time as **+Matt Miller** did in his design. This gives plenty of room behind the E3D and hopefully works better with cooling. Therefore I decided to step back to the stock 30mm fan at first and see how it works.



One of the biggest changes of course is the fact that the fan duct isn't a separate part now - i put everything together into one piece. The primary reason is that the bed level sensor I'm using (an IR Sensor) that needs to be mounted quite deep down to the bed and this works much better if those are not separate parts. Also this should be simpler at the end. Design was a bit challenging of course to be able to print it without tons of support.



For the linears I decided to test something new. I had a guy from Igus in my basement today and will receive some ECLM samples around in a few days ([http://www.igus.com/wpck/3778/igubal_Clips_Gelenklager_ECLM](http://www.igus.com/wpck/3778/igubal_Clips_Gelenklager_ECLM)). They are similar to the SDP-SI bushings and may work in this situation. They could be a cheap and easier to source alternative. I will try them on the carriage at first and if it works will probably give the 10mm ones a try on the outer ends.



The thing is already printed and test in terms of hotend fit, cable routing and all that stuff – I'm just waiting on Igus now.



Happy to hear your comments.

![images/e2e5024c4708bf6052f07f0fc7668b7b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e2e5024c4708bf6052f07f0fc7668b7b.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**Matt Miller** *January 11, 2017 19:35*

I really like this monoblock design - very clean!  


---
**Michaël Memeteau** *January 11, 2017 21:19*

Really clean indeed... I wished it was transparent to check the first layers easier.

Congrats'


---
**Eric Lien** *January 11, 2017 23:34*

Great design. And I am excited to see how the igus bearings perform.


---
**larry huang** *January 12, 2017 04:50*

Your Extruder Carriage is pretty inspiring, I like it a lot. Thanks for sharing.


---
**Eric Moy** *January 13, 2017 01:13*

Elegantly simple, less parts equals better assembly tolerances


---
**Eduardo Ribeiro** *January 16, 2017 18:23*

nice work ! but there is any way to add a proximity sensor? 


---
**Frank “Helmi” Helmschrott** *January 16, 2017 18:24*

Yes, but only for the one i'm using (IR sensor on a small pcb). Will post some photos later. 


---
**Stefano Pagani (Stef_FPV)** *January 17, 2017 17:14*

Nice! I'm going to adopt it for the chimera and try to keep the same form factor




---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/LciWHvpa1Ey) &mdash; content and formatting may not be reliable*
