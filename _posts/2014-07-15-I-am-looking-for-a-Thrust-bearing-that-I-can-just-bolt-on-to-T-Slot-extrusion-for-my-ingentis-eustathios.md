---
layout: post
title: "I am looking for a Thrust bearing that I can just bolt on to T-Slot extrusion for my ingentis/eustathios"
date: July 15, 2014 11:56
category: "Discussion"
author: Daniel Fielding
---
I am looking for a Thrust bearing that I can just bolt on to T-Slot extrusion for my ingentis/eustathios. I would like to get it through Ali-Express but I don't know what to search for. Every time I look through Ali-Express I get a million results and get frustrated trawling through the results. Anyone have any tips. I plan on getting some of these for my z and have the ends turned to match thrust bearings and gt2 pulleys.





**Daniel Fielding**

---
---
**Daniel Fielding** *July 15, 2014 11:58*

Helps if I add the link. [http://www.aliexpress.com/item/HighQuality-Bracket-Anti-Backlash-Rolled-ballscrew-1pcs-12mm-SFU1204-L500mm-with-1pcs-1204-Flange-single-ballnut/1422600526.html](http://www.aliexpress.com/item/HighQuality-Bracket-Anti-Backlash-Rolled-ballscrew-1pcs-12mm-SFU1204-L500mm-with-1pcs-1204-Flange-single-ballnut/1422600526.html)


---
**Chet Wyatt** *July 15, 2014 12:32*

I was going to go with this BF10:

[http://www.ebay.com/itm/1antibacklash-ballscrew-1204-430mm-C7-BK-BF10-coupler-/250581054910?pt=BI_Heavy_Equipment_Parts&hash=item3a57cb75be](http://www.ebay.com/itm/1antibacklash-ballscrew-1204-430mm-C7-BK-BF10-coupler-/250581054910?pt=BI_Heavy_Equipment_Parts&hash=item3a57cb75be)



I ordered from him for my CNC conversion. Just email him with drawings of how many, what length, how you want the ends machined, and he'll send you an estimate. I think it was less than 200 bucks for my 3-axis mill conversion, and It only took a week :)



OR some combo like this.



 [http://www.aliexpress.com/item/Free-shipping-BEST-1204-Rolled-Ballscrew-1pcs-SFU1204-L350mm-61mm-machining-parts-1pcs-ballnut-1set-BK10/1854520843.html](http://www.aliexpress.com/item/Free-shipping-BEST-1204-Rolled-Ballscrew-1pcs-SFU1204-L350mm-61mm-machining-parts-1pcs-ballnut-1set-BK10/1854520843.html)



Anybody have any Pro's or Con's for a setup like this?


---
**Daniel Fielding** *July 15, 2014 12:42*

Hi Chef,  I am after thrust bearings not the leadscrew.  :)


---
**Chet Wyatt** *July 15, 2014 12:44*

The BK/BF pair is the thrust (A/C) bearing.


---
**James Rivera** *July 15, 2014 21:22*

Yikes! I guess precision is pricey. Not really useful for CNC, but I am still targeting braided fishline (Dyneema/Spectra) for my 3D printer motion.


---
**Chet Wyatt** *July 15, 2014 22:55*

Precision is cheaper :) Two Z-Axis ballscrews, with ballnuts, for ~$70 is the same cost as just the thrust bearings and bearing blocks from the Eustathios BOM. And the support block  replaces the thrust bearing. Unless I'm missing something?


---
**Daniel Fielding** *July 16, 2014 02:24*

I was kinda hoping that **+Tim Rastall** might whip out his aliexpress-fu. 


---
**Tim Rastall** *July 16, 2014 06:06*

Do you mean this sort of thing? 



[http://www.aliexpress.com/item/Ballscrew-Support-1pc-BK12-1pc-BF12-1605-ballscrew-End-Supports-CNC-Parts-for-SFU1605/1677110991.html](http://www.aliexpress.com/item/Ballscrew-Support-1pc-BK12-1pc-BF12-1605-ballscrew-End-Supports-CNC-Parts-for-SFU1605/1677110991.html)


---
**Daniel Fielding** *July 16, 2014 06:41*

Thanks **+Tim Rastall** although they work out dearer than the leadscrew and coupling.


---
**D Rob** *July 17, 2014 05:45*

**+Daniel fielding** print one man. You have a printer!


---
**D Rob** *July 17, 2014 05:47*

Sfu1204 work out better too


---
**Daniel Fielding** *July 17, 2014 05:48*

Yeah but I kinda like the idea of proper machined parts for precision. I like the ones on the eustathios. 


---
*Imported from [Google+](https://plus.google.com/110967506247660788897/posts/GZEf3Qoqk7L) &mdash; content and formatting may not be reliable*
