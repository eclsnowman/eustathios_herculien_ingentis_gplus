---
layout: post
title: "Got this current meter in the trash from a company (still in the unopened packaging) Planning to put on the 24V line to the X3, should look pretty cool!"
date: August 23, 2016 21:57
category: "Show and Tell"
author: Stefano Pagani (Stef_FPV)
---
Got this current meter in the trash from a company (still in the unopened packaging) Planning to put on the 24V line to the X3, should look pretty cool!

![images/3fdb9a5992ff94d6682b148bfebce9b6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3fdb9a5992ff94d6682b148bfebce9b6.jpeg)



**Stefano Pagani (Stef_FPV)**

---


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/4b4nMkd7wvE) &mdash; content and formatting may not be reliable*
