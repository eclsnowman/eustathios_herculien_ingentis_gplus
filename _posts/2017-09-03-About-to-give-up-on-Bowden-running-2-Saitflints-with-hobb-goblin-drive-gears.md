---
layout: post
title: "About to give up on Bowden, running 2 Saitflint's with hobb goblin drive gears"
date: September 03, 2017 15:09
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
About to give up on Bowden, running 2 Saitflint's with hobb goblin drive gears. getting abysmal print quality got retraction perfect but theres so many blobs around the print and none of the layers even line up :/. 



I think I will swtich off the chimera and onto a E3D Titan or Titan aero. Any tips? Using S3D and a Azteeg X3. Just bought a X5 GT because my X3 came defective (no response to 5+ emails)













**Stefano Pagani (Stef_FPV)**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 03, 2017 15:30*

[https://www.thingiverse.com/thing:669459](https://www.thingiverse.com/thing:669459)

[thingiverse.com - Compact Direct Drive MK8 Bowden Extruder for 1.75mm Filament by 0110-M-P](https://www.thingiverse.com/thing:1132201)



Try these. I have one with a Hobb Goblin running my delta at 150mm/s, Rene uses it on the DICE for extremely high print speeds, both are bowden. 


---
**Michaël Memeteau** *September 04, 2017 16:00*

Another option could be to try the brand new version of KissSclicer 1.6 Release Candidate 3... Apparently there is an option that tries to compensate for bowden tube length.

More info here:

[http://www.kisslicertalk.com/viewtopic.php?f=6&t=1902](http://www.kisslicertalk.com/viewtopic.php?f=6&t=1902)




---
**Stefano Pagani (Stef_FPV)** *September 06, 2017 01:38*

**+Ray Kholodovsky** Thanks! Printing 2 right now.


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/JJqDK5cyoVv) &mdash; content and formatting may not be reliable*
