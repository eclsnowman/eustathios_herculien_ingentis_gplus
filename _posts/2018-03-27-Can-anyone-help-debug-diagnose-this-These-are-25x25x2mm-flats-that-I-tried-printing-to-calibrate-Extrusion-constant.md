---
layout: post
title: "Can anyone help debug/diagnose this? These are 25x25x2mm flats that I tried printing to calibrate Extrusion constant...."
date: March 27, 2018 15:46
category: "Discussion"
author: Dennis P
---
Can anyone help debug/diagnose this?  These are 25x25x2mm flats that I tried printing to calibrate Extrusion constant.... the right side of the print is missing completely on the top layer. The print direction from the gcode is top left to bottom right of the flat. 



The print path looked like it would print from the top left towards the bottom right of the object, stop extruding , continue the path then get to the end and then double back and jog at the place the filament is missing. I put a snip of the paths as an example.  But the gcode does not reflect that path at all. 



I know my bed level needs to be tweaked a little but not by much. 



My cable/bowden bundle droops a little bit, but I did not think it would affect it like this. 



Can someone post how their cable and bowden tubes are arranged? 



I am using Marlin 1.1.8 on RAMPS if that matters. 





**Dennis P**

---
---
**Dennis P** *March 27, 2018 15:52*

![images/08fd20413cfc22dbc4f21beae8a9e357.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/08fd20413cfc22dbc4f21beae8a9e357.jpeg)


---
**Bruce Lunde** *March 27, 2018 18:09*

![images/6cb40fed34a97073c66a03d15e3090b2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6cb40fed34a97073c66a03d15e3090b2.jpeg)


---
**Bruce Lunde** *March 27, 2018 18:10*

**+Dennis P** here is my Bowden setup


---
**Eric Lien** *March 27, 2018 19:33*

Can you get a little clearer picture. Maybe closeup of each. I can't really tell with these pictures.



Also please confirm. Do you think the hotend is vertical? Not at some angle causing one side of the nozzle to drag while the other is raised.


---
**Eric Lien** *March 27, 2018 20:34*

Have you done an esteps calibration yet? Not through the hotend, but into open air. 



1.) Mark the filament in the top of the extruder

2.) Extrude a length of filament, as long as the longest device you have to measure accurately (caliper etc).

3.) Compare actual length to intended length.

4.) Adjust firmware esteps by % difference.

5.) Repeat step 1 and go through again until you are happy with the correlation.


---
**Dennis P** *March 28, 2018 01:35*

yup, i did and i actually made a thing for it. 

i will do it again too now that you mention it. 

[thingiverse.com - Extrusion Calibration by Calipers by sinned6915](https://www.thingiverse.com/thing:2790629)




---
**Dennis P** *March 28, 2018 04:36*

**+Ryan Carlyle** what was your final impression of the SaintFlint extruder?  I am thinking I want to try a different extruder on this. 


---
**Dennis P** *March 29, 2018 00:15*

This is really driving me batty.... the gcode preview and the sliced model in both KISS and Slicer appear similar.  The infill appears to be straight across the diagonal. But at 6-7 passes from the corner, the printer will jog and overlap the infill. 



Here is a video of it in action- you can actually see it 'jumping over' at about 3 sec & 7 sec, on the uphill pass of infill. 



 [https://drive.google.com/file/d/1aQYu7mu_e5hj4WkZ983rfCFHnO2NYBR2/view?usp=sharing](https://drive.google.com/file/d/1aQYu7mu_e5hj4WkZ983rfCFHnO2NYBR2/view?usp=sharing)



Here is the new extruder- a Saintflint with a Mk8 drive gear, hastily printed and tossed onto the printer with a mini bar clamp. 

[https://www.thingiverse.com/thing:979113](https://www.thingiverse.com/thing:979113)

[https://drive.google.com/file/d/1Z9q52C8cQhwyfQPtlzysRd3-OhLvXri-EA/view?usp=sharing](https://drive.google.com/file/d/1Z9q52C8cQhwyfQPtlzysRd3-OhLvXri-EA/view?usp=sharing)



I am waiting on new pulleys, with hopefully concentric bores this time... could it be related? 



![images/c756aaa2c568d8cff8e6d665291642f4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c756aaa2c568d8cff8e6d665291642f4.jpeg)


---
**Dennis P** *March 31, 2018 00:04*

The answer is YES- the slightest run-out of the pulleys seems to get magnified in the motion of the system.  




---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/Ci482cN5oJB) &mdash; content and formatting may not be reliable*
