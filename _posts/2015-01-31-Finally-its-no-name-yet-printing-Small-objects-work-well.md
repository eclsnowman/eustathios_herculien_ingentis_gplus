---
layout: post
title: "Finally it's (no name yet) printing. Small objects work well"
date: January 31, 2015 19:52
category: "Discussion"
author: Florian Schütte
---
Finally it's (no name yet) printing. Small objects work well. But when is speed up or print lager objects, the filament starts slipping back while printing. 

I tried different things to get a constant filament flow. 

1. max Power on Extruder Motor

2. cooling extruder motor

3. reduceing flow

4. higher nozzel temperature

5. check filament diameter

6. reduce print speed (normally 50mm/s (80mm/s infill, 40mm/s outer lines))

But nothing helped. The filament still slips back. It's not the torque of filament drive. the whole motor gets turned back.

I'm useing a "airstripper" extruder and an cheap china e3d v5 remake. 



Any suggestions what i need to change?



in the first picture you can see, what happens because of the slip back. The rest of the pics show my first print - a model of the comet which is observed by rosetta orbiter. The dimension test shows nearly perfect dimensions.



![images/85345e1fb07fa3d06d7f6bb02584df83.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/85345e1fb07fa3d06d7f6bb02584df83.jpeg)
![images/157c080f73b6978463832ed0102fa970.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/157c080f73b6978463832ed0102fa970.jpeg)
![images/f458fd41687baf249b7a2feed810a361.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f458fd41687baf249b7a2feed810a361.jpeg)
![images/5e2ad7e11a9e24abef9e91f929f24a87.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5e2ad7e11a9e24abef9e91f929f24a87.jpeg)

**Florian Schütte**

---
---
**James Rivera** *January 31, 2015 20:04*

Details on your current print settings would help to diagnose this problem. (i.e. - PLA or ABS? Print speed? Filament diameter? Nozzle diameter? Layer height? etc.)


---
**Ubaldo Sanchez** *January 31, 2015 20:09*

Things I would check would be the extruder filament drive gear. The motor shaft used to slip on mine and I had no idea why my filament was getting all crazy like yours. Also the thermistor and nozzle temp might be off. 


---
**Erik Scott** *January 31, 2015 20:20*

I had, and occasionally continue to have, the same issue. Does it only happen on the first or second layer for you? Once I get past that point, everything goes smoothly. I've found the cause is that the extruder is trying to push more plastic through the nozzle than the nozzle can handle. This could be because you're printing too fast, the nozzle is too close to the bed, it's not hot enough, or a combination of the above. I've recently been heating my hotend up to 230C for PLA and it seems to help. Seeing as you've tried a hotter temp and slowing it down, I would check the height of the print bed. I don't (yet) have a heated bed, and so I'm printing on blue-tape-covered 6mm acrylic. Unfortunately, 6mm acrylic tends to droop a bit in the middle, so I have added a 5th support in the middle of bed, attached to that crossbar and I adjust that on just about every print to make sure the level is prefect. There's a very fine line between good adhesion for small features (holes and such) and keeping the extruder from skipping, and it's really just about having a good eye for it than any exact science. Even after some practice, It still annoys the hell out of me. 



I'm currently using this extruder: [http://www.thingiverse.com/thing:185005](http://www.thingiverse.com/thing:185005)  (Very sorry for the thingiverse link) but I'm going to be upgrading to **+Eric Lien**'s HurcuStruder ([https://www.youmagine.com/designs/hercustruder](https://www.youmagine.com/designs/hercustruder)). I even printed the parts last night. However, to really take advantage of any benefit it provides, I'm going to have to get a planetary stepper motor. I don't know if this offers any significant advantage over you're airtripper, as the designs are rather similar.

 

I'm interested in what some of the experts here say, as I'd love to put this issue to bed for good myself. 


---
**Florian Schütte** *January 31, 2015 20:26*

**+James Rivera** Cura settings: [http://pastebin.com/1Kw4htT6](http://pastebin.com/1Kw4htT6)

I print black PLA. Nozzle is 0.3mm.



**+Ubaldo Sanchez**  Nozzle temp is ok. i checked all before first print.



**+Alex Lee** I think it's not underextrusion. It seems like to much filament is pushed into extruder and so it slips back. Fans are running. When you say 'mount' do you mean print head? Its made from HIPS.  Lower temperatures dont make it better. 



**+Erik Scott** Bed level is ok. The problem exists the whole time. Not only first layers. i also tried up to 203°C. Dont make it really better.


---
**Florian Schütte** *January 31, 2015 20:35*

What Hotend(s) do you prefer?


---
**Florian Schütte** *January 31, 2015 20:44*

**+Alex Lee** attached a few drops to my filament filter sponge...will see if it works. Thanks so far.

I also thought about investing in a original e3d


---
**Jason Perkes** *January 31, 2015 22:08*

~Release the filament tensioner and try to push filament through by hand. If you can with reasonable firm force, then its a weak torque on the motor. Ive seen E motors bounce back a full step if they cant microstep through the step position they are in.

If you cant push filament through, then likely a PTFE tube necking (narrowing) as mentioned above.


---
**Eric Lien** *January 31, 2015 22:28*

**+Jason Perkes** has the idea. Bowden's are tricky. Sometime too much idler tension is the issue, sometimes too little. But even proper tension is no match for an overly restrictive feed path.  Check your feed path restriction by pulling off the nozzle and hand feed filament through the system.



Hand push to find the proper temp is how I determine my settings on each roll of new filament. Also Black is always the worst to extrude in my experience.


---
**Mutley3D** *January 31, 2015 22:40*

godamn i think i have to unjoin as **+Jason Perkes** Im getting me mixed up with me. :)


---
**Florian Schütte** *February 01, 2015 00:31*

Thanks **+Eric Lien** **+Jason Perkes** 

A friend just told me he widened his PTFE tube inside the extruder to 2mm when he had the same issues a while ago. I think i will try this tomorrow.


---
**Riley Porter (ril3y)** *February 01, 2015 04:37*

Temp too low. Done.


---
**Eric Lien** *February 01, 2015 04:43*

**+Riley Porter** What temps do you run. Depending on the PLA my go to temp is 220C for 60mm/s and 230C for 100mm/s printing speeds.


---
**Riley Porter (ril3y)** *February 01, 2015 05:32*

I do 220 as well. Also a knockoff Chinese extruder is not optimal. There is a lot of variables in this thread. **+Jason Smith**​ prints at 230 at 90mm/sec.


---
**Florian Schütte** *February 05, 2015 18:01*

Widening the PTFE tube with a 2mm drill helped a lot. Now i can print at lower temps without any slip. I also increased the thrust at filament feed and attatched a few drops of cold pressed olive oil to my filament filter...the good one ^^

Now i have to test which speeds i can reach. What are your printig speeds @ Eustathios?


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/8sBi3CSbcFj) &mdash; content and formatting may not be reliable*
