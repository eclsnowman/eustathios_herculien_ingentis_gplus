---
layout: post
title: "Hi, I have a question about fans with extruder, the main fan 30*30 and 4020 blower should be wired with different ports, right?"
date: June 26, 2016 02:34
category: "Build Logs"
author: Botio Kuo
---
Hi, I have a question about fans with extruder,  the main fan 30*30 and 4020 blower should be wired with different ports, right? which port I should wire for 4020 blower. Because I want to set which layer it will start to blow the print. Thanks





**Botio Kuo**

---
---
**Eric Lien** *June 26, 2016 03:31*

Can you confirm which controller you have?


---
**Botio Kuo** *June 26, 2016 03:37*

**+Eric Lien** Hi Eric, it is this one [https://github.com/makerbase-mks/MKS-SBASE/blob/master/MKS-SBase_Pin2.jpg](https://github.com/makerbase-mks/MKS-SBASE/blob/master/MKS-SBase_Pin2.jpg)


---
**Eric Lien** *June 26, 2016 03:59*

I am not familiar personally with that controller, but it looks like you should use p2.7 for the heat sink fan, and p2.4 for the plastic cooling fan. You can repurpose p2.6 for your LEDs by editing the config.txt.


---
**Botio Kuo** *June 26, 2016 04:02*

**+Eric Lien** But P2.7 is for E3D V6...I think... and P2.4 is for hot sink fan ( I think ) .... but so P2.6 can be cooling fan? Thank you. 



I also think about .... maybe I just wait for the new (unreleased) controller you tested from Roy.


---
**Eric Lien** *June 26, 2016 04:33*

P2.7 would be for the E3d heat sink (30x30), P2.4 is for part cooling (4020 cooling extruded plastic). P2.6 would be needed if you have a second extruder which you do not.


---
**Botio Kuo** *June 26, 2016 05:03*

**+Eric Lien** But where can be the power for my first extruder ???? @@" should be P2.7 right ? I meant the power for heating extruder..


---
**Botio Kuo** *June 26, 2016 05:23*

Oh... I just realized I can just wire the heat sink with the power port (12-24V) Thank you 


---
**Eric Lien** *June 26, 2016 14:00*

**+Botio Kuo**​ sorry you are right. P2.7 for e3d heater cartridge, P2.6 for e3d heatsink fan, P2.4 for the part cooling fan.﻿


---
**Eric Lien** *June 26, 2016 14:01*

Also I run the e3d heatsink fan to come on at 50C, saves wear on the fan.


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/E3fkUpoZAgM) &mdash; content and formatting may not be reliable*
