---
layout: post
title: "So Im about ready to use my Panucatt X5 GT as a damn frisbee"
date: July 02, 2018 00:24
category: "Discussion"
author: jerryflyguy
---
So I’m about ready to use my Panucatt X5 GT as a damn frisbee. Thing has cost me more in wasted plastic due to freezing and other crap. Haven’t looked at what’s new for boards these days. What’s everyone using these days?





**jerryflyguy**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 02, 2018 00:28*

Duet Wifi is a popular one. 


---
**jerryflyguy** *July 02, 2018 00:53*

**+Ray Kholodovsky** yeah that’s one, what do you have on the market that will do everything that the X5 GT will do? 256x ? Prefer WiFi if I can get it? 32bit? Are your boards smoothie based?


---
**Eric Lien** *July 02, 2018 01:08*

**+jerryflyguy** what issues specifically (what leads up to the freeze, and what does the terminal do when it freezes).



Are you running the katest firmware?



**+Zane Baird** have you run into any freezing on your x5 gt? Mine in my HercuLien is a beta board from Roy, but I haven't run into any issues.


---
**Eric Lien** *July 02, 2018 01:10*

I do not disagree that the duet wifi is the goto board now, but I just haven't run into any issues with my x5 GT and its been running for multiple years.


---
**jerryflyguy** *July 02, 2018 01:16*

**+Eric Lien** it locks up randomly after anywhere from one to 30hrs of printing. Can not access it w/ terminal afterwards. I’ve tried several firmware iterations.  I’ve got a 40hr print that uses 3/4 of a 1kg spool and it’s failed on me 6(!) times.. the last time the extruder stepper stopped ‘extruding’. Every time it happens a  power cycle resets everything and it works again for a few hrs. It’s been suggested I try a different firmware that stops the USB from working (only way to load files is via the SD card) but had lockup’s then too. Tired of a board that just doesn’t ‘work’.. I shouldn’t have to put up w/ these lockup’s as ‘normal’.


---
**Zane Baird** *July 02, 2018 01:32*

**+Eric Lien** **+jerryflyguy** I have had no problems with my x5 GT. I can’t say the same for the smoothieboard I had on there beforehand. Almost always when printing through usb though. The duet WiFi on my other printer has been great though. I’d go that way before a board running smoothieware on any new build... 


---
**jerryflyguy** *July 02, 2018 02:06*

**+Zane Baird** thanks, it’s one option I’ve been looking at. 


---
**jerryflyguy** *July 02, 2018 02:07*

I’m trying one last thing, I’ve installed a new power supply. I had a spare and figured why not. If it locks up one more time mid-print, I’m done with this board and will go a completely different direction 


---
**Brandon Cramer** *July 02, 2018 02:14*

I’ve been in that same place you are having trouble with. Lately my X5 Mini has to be rebooted after printing every part. My Viki display doesn’t come on when pulling the power and plugging it back in, so I have to do this 3-5 times to get the display working. Kind of frustrating to say the least. 



I slice the file with Simplify3D and upload it to the X5 Mini web interface. Other than those issues I’m able to print just fine. 


---
**jerryflyguy** *July 02, 2018 03:05*

**+Brandon Cramer** I’ve never been able to get the WiFi side to work


---
**Brandon Cramer** *July 02, 2018 03:52*

**+jerryflyguy** Do you see it as a wireless network that you can connect to? 


---
**jerryflyguy** *July 02, 2018 03:58*

**+Brandon Cramer** I have a piggyback board (from Panucatt)on the viki that connects to my router. It then gives me a IP address that I can log into from a web browser and it acts like a terminal. Can upload files, change temps etc.. super elegant if it worked but it’s never worked 


---
**Ray Kholodovsky (Cohesion3D)** *July 02, 2018 16:33*

I don’t make anything for what you need it to do.  



If you are tossing boards, I’ll be happy to take them of your hands. 


---
**Eric Lien** *July 03, 2018 05:44*

**+jerryflyguy** have you ever thought of loading marlin 2.0 onto the x5 GT to see if a different firmware fixes the stability issues?



 There is a HAL for the X5 GT, and then you can play around with some of the cool things Marlin has introduced like linear advance.



[https://github.com/MarlinFirmware/Marlin/tree/bugfix-2.0.x?files=1](https://github.com/MarlinFirmware/Marlin/tree/bugfix-2.0.x?files=1)

[github.com - Marlin](https://github.com/MarlinFirmware/Marlin/tree/bugfix-2.0.x?files=1)


---
**Eric Lien** *July 03, 2018 05:46*

[http://marlinfw.org/docs/basics/install_platformio.html](http://marlinfw.org/docs/basics/install_platformio.html)


---
**jerryflyguy** *July 03, 2018 15:02*

**+Eric Lien** nope, never heard of this!! Might have to try it? The new power supply has worked with only one lockup so far. I put a new spool on the machine and started a 36hr print last night. We’ll see how far it gets. Something to try I guess, if this fails again. 


---
**Jeff DeMaagd** *July 03, 2018 22:58*

Have you tried the network interface? I too had trouble with the X5 GT but the network interface module wasn’t available at the time so I was at my wit’s end. I ended up switching to Duet and all my problems went away and their network interface is far better too.


---
**Gary Hangsleben** *July 04, 2018 06:37*

I agree on the other options the Cohesion and Duet are great  boards.  On my X5 GT, I also had random stall issues, it would stall with the heat still on at random times. I tried same changes as well, ps, 12v, 24v, new  USB cable with similar results.  As the last option I mounted 2 30mm fans directly on top of the board and it has been working without issue since.  


---
**jerryflyguy** *July 05, 2018 00:44*

**+Jeff DeMaagd** I’ve only tried the WiFi daughter board on the Viki. 


---
**jerryflyguy** *July 05, 2018 00:46*

**+Gary Hangsleben** I’ve got an independent 4” 110vac cooling fan blowing across the entire board. (Stole it off an industrial box I was scrapping) so I’m assuming cooling isn’t the issue?


---
**Stefano Pagani (Stef_FPV)** *July 10, 2018 14:02*

Never had much freezing, but I have had only bad experiences with the Panucatt boards.



My X3 came with a bad H-Bed fet and my first X5 was DOA. Took a month for a replacement for both. About to buy a Duet when I can afford it.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/hkaFoM54mWp) &mdash; content and formatting may not be reliable*
