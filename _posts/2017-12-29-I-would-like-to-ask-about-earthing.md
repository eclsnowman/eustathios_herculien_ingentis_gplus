---
layout: post
title: "I would like to ask about earthing"
date: December 29, 2017 06:01
category: "Build Logs"
author: Jussi
---
I would like to ask about earthing. How to wiring it?

Is that correct that I install the ground (yellow-green) wire from the power supply to the frame of the printer and same to the aluminum heating plate?







**Jussi**

---
---
**Zane Baird** *December 29, 2017 13:35*

Earth ground (green yellow) should be tied to both your power supply and the frame. For the heated bed you need line power (Black in US, brown in Europe) and neutral (white) but this is connected through an SSR. Do not try and use the earth ground as an electrical connection on the bed


---
**Ben Malcheski** *January 03, 2018 18:26*

**+Zane Baird** Good being specific about the connections. It's probably a good idea to also provide an earth ground for the plate or any other parts that could come into contact with mains voltage. I'm thinking in the event of a heated bed burning out and exposing traces or just a broken wire going astray.


---
**Jussi** *January 29, 2018 07:24*

Is it correct that two red silicon-coated cables connected through an SSR and black cable is the temperature sensor?

The red wire does not have polarity?

![images/f99b251687564aaca7ed5cf09ea0f320.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f99b251687564aaca7ed5cf09ea0f320.jpeg)


---
**Jussi** *July 24, 2018 07:03*

up


---
*Imported from [Google+](https://plus.google.com/105458234404886856933/posts/ThTqfVpEw7e) &mdash; content and formatting may not be reliable*
