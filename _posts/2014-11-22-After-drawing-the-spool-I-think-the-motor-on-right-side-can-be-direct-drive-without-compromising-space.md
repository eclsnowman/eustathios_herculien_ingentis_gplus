---
layout: post
title: "After drawing the spool, I think the motor on right side can be direct drive without compromising space"
date: November 22, 2014 12:36
category: "Deviations from Norm"
author: hon po
---
After drawing the spool, I think the motor on right side can be direct drive without compromising space. The extruder will sit on this side as well.



Is there anything to watch out for direct drive? The X motor still driven by belt, and latest design has 20:36 gear ratio. any problem with the original 1:1 ratio?



I want a modular carriage, so I may swap single/dual hotend, or put stepper motor on carriage without removing the rods. Any existing design I may borrow?



![images/ca26b3172a1b138376e5224e3c63b924.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ca26b3172a1b138376e5224e3c63b924.png)
![images/28c9b1fce552c26bbe7616634ffbafb2.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/28c9b1fce552c26bbe7616634ffbafb2.png)
![images/3782515b00df3e9843666241e86fdc2a.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3782515b00df3e9843666241e86fdc2a.png)
![images/8991b90e8423369e7e8d170548a7e6e0.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8991b90e8423369e7e8d170548a7e6e0.png)
![images/5143afe5ae7c2a4598e4b01a1c9240f2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5143afe5ae7c2a4598e4b01a1c9240f2.jpeg)

**hon po**

---
---
**Eric Lien** *November 22, 2014 14:25*

HercuLien has an optional modular Bowden/Direct system. But it could use some help. If you come up with an elegant/compact design I may have to borrow some ideas.


---
**Eric Lien** *November 22, 2014 14:26*

[https://plus.google.com/109092260040411784841/posts/Y4V8sHcjmgu](https://plus.google.com/109092260040411784841/posts/Y4V8sHcjmgu)


---
**hon po** *November 22, 2014 16:40*

Thanks, I hope to find a skeleton carriage that leave room for addon possibility. Right now not much idea.



Like your source file a lot, very detailed model, down to the balls inside the bearing. Make me wish for a faster PC since I quit gaming long time ago.


---
**Eric Lien** *November 22, 2014 17:25*

**+hon po** it runs slow on mine too. I made the mistake of using bolts with threads. Too much geometry. I do most of my work in subassemblies.


---
**Eric Lien** *November 22, 2014 17:26*

BTW very clean print in the last pic.


---
**hon po** *November 23, 2014 10:00*

That's a very slow print from my Prusa i3. A quick build for another project. But the corner overshoot kill that use. I think the retraction setting isn't right also.


---
*Imported from [Google+](https://plus.google.com/111735302194782648680/posts/7bgouCxcDCy) &mdash; content and formatting may not be reliable*
