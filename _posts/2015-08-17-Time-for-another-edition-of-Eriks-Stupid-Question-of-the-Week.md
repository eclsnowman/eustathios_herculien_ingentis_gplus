---
layout: post
title: "Time for another edition of \"Erik's Stupid Question of the Week!\""
date: August 17, 2015 03:18
category: "Discussion"
author: Erik Scott
---
Time for another edition of "Erik's Stupid Question of the Week!"



Actually, I have a couple. 



I have a few extra stepper motors from SparkFun. They're 400steps/rev 65oz-in steppers and the seem to have done well for me on my current printer, though they do sometimes skip steps if anything sticks up from the model and the nozzle gets caught. So, would upgrading to the 92oz-in (65N-cm) motor specified in the BOM give me any real advantage here, despite essentially halving the resolution? (this would be a 200step/rev motor) I currently run 1/16 microstepping on my RAMBo, and I understand the new Azteeg X5 mini that I have can do 1/32, which would essentially negate that loss. Still, it's lost precision. I do plan to go to a 200step/mm motor for my Z-stage with 1:1 gearing, as that will result in a nice even full step per 0.01mm. 



Secondly, Is there any documentation on how to setup the azteeg X5 mini and how it connects to all the other various devices? (Raspberry Pi, Viki2.0, etc)



Thanks!





**Erik Scott**

---
---
**Jeff DeMaagd** *August 17, 2015 06:11*

200 step/rev motors at 1/32 won't necessarily give the same results as a 400 step/rev motor at 1/16 settings.



I use 400 step/rev motors about that strength on my main machine and I don't get skipping problems. I would calibrate esteps if you haven't already, just to rule out overextrusion. It might also be a matter of getting enough drive current.



Go to [http://smoothieware.org](http://smoothieware.org) to configure the X5. Go to Azteeg on how to wire it. It's rather different from the Arduino based boards but most people come to like smoothieware once they understand it. I think the site has a section on the Viki. I don't know about connecting to a Pi, Google should show you relevant pages.


---
**Frank “Helmi” Helmschrott** *August 17, 2015 07:00*

I'd also first test raising the drive current before changing the motors. I still have to fight this challenge too as i do face the same problem.


---
**Eric Lien** *August 17, 2015 11:48*

I had skip steps every once and a while on my Eustathios. That is the reason when I built HercuLien I used the stronger steppers and the shorter belt by moving the steppers up to the top of the gantry. I have almost never skipped steps on my HercuLien, which is why I incoperated a similar design in V2. For the wiring on the Azteeg it is very simple. Panucatt has a great wiring diagram on the site. The only downside is the lack of outputs to control a LED lighting strip, so for that I used a SSR relay board and the Pi gpio. Now I control the LED from octoprint. I have a post from a while back on how I wired that.



The motion on smoithieware is significantly smoother than 8bit boards, and config via the text file is super simple which I love.


---
**Erik Scott** *August 17, 2015 14:37*

Yeah, my understanding is Smoothie is super easy. 



**+Jeff DeMaagd**​ mind explaining how 200steps/rev at 1/32 wouldnt be the same resolution as 400steps/rev at 1/16? I understand maybe the torques should be different, but the steps/mm should be the same. 



I looked at the wiring diagram. Shouldn't be too hard. Just wanted to know if there was anything specific to the Eustathios. 



The printer will only skip X or Y steps on the first or second layer, and that's only if I happen to have the bed a bit too close to the nozzle and the plastic forms ridges in the first layer in-fill. The thing is, because I have an unheated bed at the moment, I can't get some of the smaller features (small holes, etc) to stick even if the nozzle is at the ideal distance for the in-fill. So I'll often end up raising the bed slightly more than I need just to get things to stick. 


---
**Eric Lien** *August 17, 2015 14:50*

**+Erik Scott** What do you use for Bed Treatment? I still use Hairspray and run the first layer very slow. I have had really good luck with first layer adhesion using this method. I have used Kapton, Glue Stick, Painters Tape, Etc. But over the years I have found putting a wet coat (spray until the bed has a wet appearance, not a fogged appearance) using Suave Extreme Hold Unscented Hairspray works great on glass. I apply 3 coats, letting them air dry between each coat.



[http://www.walmart.com/ip/Suave-Extreme-Hold-10-Unscented-Hairspray-11-oz/10293440](http://www.walmart.com/ip/Suave-Extreme-Hold-10-Unscented-Hairspray-11-oz/10293440)



With this I can use the bed for weeks without any re-application, just try to avoid getting any oils from your hands on the bed, use a razor scraper to remove any difficult prints.  I am a very big fan of this simple razor handle scraper tool: [http://www.thingiverse.com/thing:34481](http://www.thingiverse.com/thing:34481)


---
**Eric Lien** *August 17, 2015 14:55*

**+Erik Scott** For the steps per mm, I think I agree with **+Jeff DeMaagd** . A stepper designed for 400steps/rev will likely perform better than running at higher microsteps. But how much is really the question. The resolution possible by 1/32x200 or 1/16x400 are likely moot anyway. My feeling is the tolerances possible from GT2 belts and any backlash in our assemblies is likely greater than these differences by an order of magnitude. So I doubt it would be noticeable on the parts anyway.


---
**Jeff DeMaagd** *August 17, 2015 15:37*

The thing is microsteps approximate positioning by adjusting the current between the two motor phases. The finer the microstepping, the less you can count on it being within that level of precision at any given point. I've personally found that the step detents had more of an effect than anything else. I had noticeable aliasing at the interval of step detents on 1.8˚ motors that significantly less noticable with 0.9˚ motors, and smoother looking parts too.


---
**Erik Scott** *August 17, 2015 15:46*

Interesting. Might stick with my 400steps/rev motors for X and Y then. I would like to up the torque a bit, but I can't seem find any stronger NEMA 17 motors with that resolution. 



**+Eric Lien**​ I use blue tape at the moment as I only print PLA. I will be switching to hairspray in the future, though I thought that was only for ABS and the other, "hotter" plastics. 


---
**Eric Lien** *August 17, 2015 15:53*

The funny thing is I repeatably get better surface finish on my HercuLien than I do with my Eustathios. It seems odd, because my HercuLien has larger spans, and lower steps/rev steppers than my Eustathios. So I think there is a lot more than just theoretical maximum resolution that come into play when it comes to surface quality.


---
**Erik Scott** *August 17, 2015 15:58*

Do you still have the longer belts on the Eustathios? Could be backlash developing in those belts resulting in surface ripple. Eustathios also has thiner rods supporting the carriage and arguably a less rigid bed. 


---
**Eric Lien** *August 17, 2015 16:06*

**+Erik Scott** Yes, I have wanted to upgrade my Eustathios to a V2... but right now it is more like a Eustathios V1.5. I would need longer lead screws, and longer side rods to make it a full V2. But my Eustathios is tuned so well right now I would hate to take it apart. You guys know how much of a pain it can be to find that alignment sweet spot. Once it is there, I hate to mess with it. Especially since I don't need the extra travel of the V2 because I already have a larger printer.


---
**Jeff DeMaagd** *August 17, 2015 16:09*

**+Erik Scott**

In all fairness, I am not running anything like a Eustathios yet, the torque question might be different. It probably doesn't hurt to try a stronger 200 step motor and see if that makes a difference. But do make sure your current where it should be for the motor in question. If the motor is cool or only a bit warm to the touch after running a while, you could use a higher current setting.


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/XnKgiGRe4p1) &mdash; content and formatting may not be reliable*
