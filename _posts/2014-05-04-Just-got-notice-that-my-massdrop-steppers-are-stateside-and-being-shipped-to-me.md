---
layout: post
title: "Just got notice that my massdrop steppers are stateside and being shipped to me"
date: May 04, 2014 10:09
category: "Show and Tell"
author: Jim Squirrel
---
Just got notice that my massdrop steppers are stateside and being shipped to me. so got to print my belt conponents (eustathios design) to replace my current spectra design. 



![images/6cc9b355605e0381489632a153fa7ee1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6cc9b355605e0381489632a153fa7ee1.jpeg)
![images/fc38c500ce811a28a7711f535f53b4df.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fc38c500ce811a28a7711f535f53b4df.gif)
![images/a171e22de7de5acb26b62747d7801c57.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a171e22de7de5acb26b62747d7801c57.jpeg)
![images/4215b4ba405edcf40650797f25f16b09.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4215b4ba405edcf40650797f25f16b09.gif)
![images/476102b522b5445b9f0f446cba57d67a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/476102b522b5445b9f0f446cba57d67a.jpeg)
![images/a87b624dc706f39062db528adb1c6821.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a87b624dc706f39062db528adb1c6821.jpeg)

**Jim Squirrel**

---


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/Npnb64YUGqw) &mdash; content and formatting may not be reliable*
