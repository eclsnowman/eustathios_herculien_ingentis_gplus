---
layout: post
title: "So I have basically everything wired up to what I believe is correct"
date: May 22, 2015 17:16
category: "Discussion"
author: Derek Schuetz
---
So I have basically everything wired up to what I believe is correct. If anyone has a diagram for wiring please share it. Specifically how to wire the relay and the plug. Last thing I want is to fry everything. Also I still plan to clean it up.

![images/5104ec3dfc2472fbbc7622dc724c43d4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5104ec3dfc2472fbbc7622dc724c43d4.jpeg)



**Derek Schuetz**

---
---
**Gus Montoya** *May 22, 2015 18:19*

Nice job, I'm gonna work on my printer today.


---
**Walter Hsiao** *May 22, 2015 19:38*

Here's a SSR wiring diagram from +Thantik

[http://i.imgur.com/TsoUdHF.png](http://i.imgur.com/TsoUdHF.png)


---
**Derek Schuetz** *May 22, 2015 20:18*

 Hmm that looks different then how I have wired in the past


---
**Eric Lien** *May 22, 2015 21:32*

**+Derek Schuetz** , **+Walter Hsiao**  is correct. Your normal bed output on your printer controller is hooked up to 3 and 4 on the SSR (3 is positive, 4 is negative). Then mains live power is run into and out of the SSR (into 1, out of 2). Then power travels through the bed and back to neutral.



The SSR acts as a switch breaking the live connection. Just think of hooking the bed direct to the mains (it would be always on). Now instead insert a switch on the live side to allow the circuit to be powered intermittently via input from the normal bed output acting to drive that switch.



[http://www.alliedelec.com/images/products/Large/70178667_large.jpg](http://www.alliedelec.com/images/products/Large/70178667_large.jpg)



[https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Azteeg_x3_v2_wiring_(with_notes).pdf](https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Azteeg_x3_v2_wiring_(with_notes).pdf)


---
**Gus Montoya** *May 23, 2015 08:18*

@Derek Schuetz,   How is your wiring coming along?


---
**Gus Montoya** *May 23, 2015 08:18*

@Derek Schuetz,   How is your wiring coming along?


---
**Derek Schuetz** *May 23, 2015 09:05*

All done and seems good


---
**Gus Montoya** *May 23, 2015 14:41*

detailed pics pics please :)


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/bbj4JSaUqFT) &mdash; content and formatting may not be reliable*
