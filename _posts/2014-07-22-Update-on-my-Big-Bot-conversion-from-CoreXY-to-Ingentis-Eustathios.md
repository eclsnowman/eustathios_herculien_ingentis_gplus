---
layout: post
title: "Update on my \"Big Bot\" conversion from CoreXY to Ingentis/Eustathios"
date: July 22, 2014 22:55
category: "Deviations from Norm"
author: Eric Lien
---
Update on my "Big Bot" conversion from CoreXY to Ingentis/Eustathios. I now know how to use loft and guide curves better in solidworks which is fun. It is a really great way to make more organic shapes... but keep Solidworks happy with fully defined geometry. I also learned some more tricks from our design engineer at the foundry about drawing in assembly for part placement. I would have never been able to define the duct mounting/pivot tab easily without this (since the tab sits at a compound angle).



The first duct is printing now. Once I get it test fit I will be uploading the files to my Github for others to do with as they wish. Again this is not up and running yet so some things may not work well, and might require changes. But I think it is getting close.



I will be ordering the misumi smooth rods this week.﻿



![images/c65a046d50d0e322e11e96f9a5788591.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c65a046d50d0e322e11e96f9a5788591.jpeg)
![images/ef929a4d5538a69fccaf2a1994d4088e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ef929a4d5538a69fccaf2a1994d4088e.png)
![images/edfff6a7c6416a45624b16d37aa50488.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/edfff6a7c6416a45624b16d37aa50488.png)
![images/3cc7013f676d8cffc9f5f4261f3d37e7.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3cc7013f676d8cffc9f5f4261f3d37e7.png)
![images/8a85b685afa8406fa083669a9b247b67.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8a85b685afa8406fa083669a9b247b67.png)
![images/792f44284d5ebf28bdb72ee2b450b5c0.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/792f44284d5ebf28bdb72ee2b450b5c0.png)
![images/60ddef4e11b5399f5f158d3cc175d6a2.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/60ddef4e11b5399f5f158d3cc175d6a2.png)

**Eric Lien**

---
---
**James Rivera** *July 22, 2014 23:06*

**+Eric Lien** It's looking really good! I can't wait to see the prints this behemoth makes!  :-)



Just an FYI (and I can't remember exactly where I read this, but I recall it being a good source), but using turbine fans for cooling the molten plastic is supposed to be better than a radial fan because the back pressure reduces the airflow from the radial fan. I'm sure what you have will work, because so many people use them. But I have some turbine fans I intend to use on my custom build (if/when I ever pull the trigger and actually get started on it).


---
**Eric Lien** *July 22, 2014 23:26*

**+James Rivera** do you mean centrifugal fans? I keep wanting to test some. But I did try to keep the duct opening at least 50% of the fan surface area. In my experience with these fans that can make a huge difference on the fan airflow curve. Also I tried to keep the duct curve to a minimum. Worst thing you can do is create a standing wave by slamming the air flow into a corner. That will often cause the fan to cavitate and flow almost no air at all.


---
**Matt Kraemer** *July 22, 2014 23:34*

Centrifugal fans seem to push 10x as much air in my experience. (Tested with hand... not very quantitative)﻿ 


---
**Eric Lien** *July 22, 2014 23:39*

**+Matt Kraemer** they really are better. Shoot. Well I will look into picking up some soon. I should be able convert the existing carriage once I find some I like.


---
**James Rivera** *July 23, 2014 00:12*

**+Eric Lien** **+Matt Kraemer**:



Sorry, I had my terminology all wrong



To clarify, yes, I meant to say a centrifugal fan should be better than an axial fan for cooling the molten plastic from the extruder.



[http://www.engineeringtoolbox.com/fan-types-d_142.html](http://www.engineeringtoolbox.com/fan-types-d_142.html)



[http://industrialfans.wordpress.com/2012/07/11/whats-the-difference-between-a-centrifugal-fan-and-an-axial-fan/](http://industrialfans.wordpress.com/2012/07/11/whats-the-difference-between-a-centrifugal-fan-and-an-axial-fan/)


---
**D Rob** *July 23, 2014 06:11*

#Fan-gentis


---
**Eric Lien** *July 27, 2014 13:40*

**+Shauki Bagdadi** I have thought about it. But question the static pressure through the tube on a large bot.


---
**Eric Lien** *July 27, 2014 14:02*

I likely do, but I have never used it. I just usually use the duct calculators out on the web. But flex duct is often tricky. The surface is not optimal for laminar flow.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/Zewvy2sRa54) &mdash; content and formatting may not be reliable*
