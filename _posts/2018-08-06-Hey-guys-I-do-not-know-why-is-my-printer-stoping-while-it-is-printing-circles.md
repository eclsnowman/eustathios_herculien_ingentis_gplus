---
layout: post
title: "Hey guys! I do not know why is my printer stoping while it is printing circles"
date: August 06, 2018 20:27
category: "Discussion"
author: Janez Klemenčič
---
Hey guys! I do not know why is my printer stoping while it is printing circles. My controller is smoothieboard. 

Here is a link to my video: 
{% include youtubePlayer.html id=WvesQwOQkTg %}
[https://youtu.be/WvesQwOQkTg](https://youtu.be/WvesQwOQkTg)





**Janez Klemenčič**

---
---
**Eric Lien** *August 07, 2018 00:34*

Says video is unavailable when I go to the link.


---
**Arthur Wolf** *August 07, 2018 06:58*

Same


---
**Janez Klemenčič** *August 07, 2018 08:14*

ok you can try now


---
**Arthur Wolf** *August 07, 2018 09:23*

How are you sending Gcode to the board ? What host program are you using ? What slicing program ? Do you print from SD card ? 


---
**Janez Klemenčič** *August 07, 2018 11:31*

i tried printing from SD card now and it works fine. Thanks anyway


---
**James Rivera** *August 07, 2018 20:40*

Some slicers (ok, most) convert circles to a series of small straight lines. My hunch: this is a lot of gcode commands and may be overwhelming your buffer, hence the slowdown.


---
*Imported from [Google+](https://plus.google.com/113433408952961174117/posts/JK1L7d2LE1c) &mdash; content and formatting may not be reliable*
