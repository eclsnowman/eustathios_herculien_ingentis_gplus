---
layout: post
title: "I don't know who or where you guys found Alirubber for the heaters, but they sure are fast"
date: October 27, 2015 00:05
category: "Discussion"
author: Bud Hammerton
---
I don't know who or where you guys found Alirubber for the heaters, but they sure are fast. Ordered a heater just a week ago and it will be here on Wednesday. Been exchanging emails with Sivia Liang and it has been one of the easiest transactions I have ever done. I just hope Golmart is just as easy with the ball-screw.





**Bud Hammerton**

---
---
**Eric Lien** *October 27, 2015 00:08*

Yeah, they are really great. And will basically customize it any way you like.


---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2015 00:15*

Agreed. Very quick to respond to messages. Pulling the trigger on that once my sheet metal is ordered. As a side note I've learned that the silicone heaters are priced "as-is" but having a kapton heater made incurs a $100 mold fee. 


---
**Igor Kolesnik** *October 27, 2015 00:18*

Had more problems with UPS than with Golmart. They were very fast and did a good job with dimentions of the screw. Ask them to invert nuts. Otherwise you will have to do it yourself. It is not hard but might be stressful for the first time


---
**Ray Kholodovsky (Cohesion3D)** *October 27, 2015 00:21*

Do they or anyone else have decent prices on 8mm leadscrews?  I need to get a 300mm length (or slightly longer) priced at under $6 per rod.  


---
**Jim Stone** *October 27, 2015 00:22*

invert nuts? o.O

 and i got my leadscrew from misumi usa


---
**Bud Hammerton** *October 27, 2015 00:30*

Misumi is just too expensive for that stuff. I had everything priced for the mechanicals in a Misumi shopping cart and it was near $500. No electrical parts and no fasteners included. Got my extrusion from SMW3D in TX, got the build plate from Midwest Steel Supply in MN, rest of the stuff was sourced from various Chinese retailers, some eBay some AliExpress. All the nuts and bolts came from McMaster, but next time I will try [trimcraftaviationrc.com](http://trimcraftaviationrc.com). Ended up just over half that price.


---
**Jim Stone** *October 27, 2015 00:38*

Only place I could get my leads crew from where I ensured it was to spec.  I couldn't figure out these other places


---
**Jim Stone** *October 27, 2015 01:06*

that and the alirubber guy kinda confused me in live chat wasnt even sure if it was the right alirubber. so i just got a keenovo pad slight undersize. but should be ok for what its doing.


---
**Eric Lien** *October 27, 2015 01:11*

**+Bud Hammerton** you in Minnesota?


---
**Bud Hammerton** *October 27, 2015 02:20*

**+Eric Lien**​, no North Carolina. 


---
**Colin** *October 27, 2015 03:22*

I can vouch for [http://trimcraftaviationrc.com/](http://trimcraftaviationrc.com/), I have bought many pounds of screws, nuts, and bolts. Quality is excellent and price is better than Aliexpress most of the time. Ships quickly and cheaply too. Good guys.


---
**Bud Hammerton** *October 27, 2015 13:49*

**+Ashley Webster**  so maybe if you need them cut for you Misumi might be inexpensive for 2020, but the Openbuilds 2020 from Smw3d.com was only $5.50 per piece in 1 meter lengths, same price as the 500 mm was. I don't know if he was closing them out or what, but that price couldn't be beat. Of course I had the tools necessary to cut, drill and tap the extrusion.



**+Ray Kholodovsky** you do know that leadscrews and ballscrews, even though serving a similar function are not the same, So are you looking for leadscrews with trapezoidal threads or ballscrews? Golmart has both.


---
**Roland Barenbrug** *October 28, 2015 18:45*

Great experience with Golmart. Just send them a simple picture to show them how you want the ballscrews to be manchined.


---
**Bud Hammerton** *October 29, 2015 19:04*

**+Roland Barenbrug** Do you remember who you dealt with at Golmart and how long it took for them to respond?


---
**Bud Hammerton** *October 29, 2015 19:08*

Should also mention that I got my heater yesterday and it is now attached to the build plate. Just need the ball screws and I think I have everything to put this thing together.



The pad looks good, but they did not use 3M adhesive, they used another pressure sensitive adhesive from Crown Plastics.


---
*Imported from [Google+](https://plus.google.com/+BudHammerton/posts/6VLC3X2Lr5J) &mdash; content and formatting may not be reliable*
