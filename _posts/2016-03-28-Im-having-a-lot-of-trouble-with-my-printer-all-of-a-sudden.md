---
layout: post
title: "I'm having a lot of trouble with my printer all of a sudden"
date: March 28, 2016 22:56
category: "Discussion"
author: Erik Scott
---
I'm having a lot of trouble with my printer all of a sudden. For some reason it stops printing part way through the print (It's happened on a couple long-ish 3 hour prints) and I'm pretty sure it's due to the board reconnecting (I get the stupid windows autoplay popup). 



Anyone know how to fix this? It's actually fairly critical I get this fixed very soon as getting these prints done is time-critical. 



I have an Azteeg x5 Mini and a Windows 7 laptop. 



Thanks!





**Erik Scott**

---
---
**Eric Lien** *March 28, 2016 23:07*

Is your Azteeg board getting power from the PSU or via USB (there is a jumper to choose).


---
**Eric Lien** *March 28, 2016 23:27*

I ask because I had issues with USB power a while back with similar symptoms. Switched to PSU power and the issue went away. Also if time is critical do you have the display where you can try printing from the SD card? Takes one more variable out of the mix.


---
**Oliver Seiler** *March 28, 2016 23:28*

Do you have to have it connected to your computer? I occasionally had a similar problem with my smoothie board and made it a habit to disconnect after saving the gcode on the board's sd card. 


---
**Eric Lien** *March 28, 2016 23:33*

Jp6 is the jumper to select Internal(PSU) or USB power for the 5v that runs the board: [http://i.imgur.com/5SX5Y7j.jpg](http://i.imgur.com/5SX5Y7j.jpg)


---
**Erik Scott** *March 29, 2016 00:02*

I'll give those options a try. I'm pretty confident the SD card thing will help. I'll take a look at the jumper, but the board and Viki display work fine without any computer connected. 


---
**Paul de Groot** *March 29, 2016 01:45*

I have an occasion where the filament diameter went up blocking my bowden cable (between extruder and drive) which took me a long time to find out...


---
**Erik Scott** *March 29, 2016 02:04*

**+Paul de Groot** That's not the issue in this case. Everything just stopped mid-print. 



I just tried printing from the SD card. Seems to work fine. I might just do this from now on as I don't have to keep my laptop tied to my printer. 


---
**Erik Scott** *March 30, 2016 02:29*

So, SD printing works fine. Problem is, sometimes I need to connect to my printer to level the bed, and I'm not struggling to maintain connection for any length of time (It stops responding after about a minute). This is driving me insane. 


---
**Oliver Seiler** *March 30, 2016 03:27*

Can you use the web front end? I use it for most things now other than uploading gcode. 


---
**Sean B** *March 31, 2016 01:50*

**+Erik Scott** I had similar issues with the laptop that was connected to a scale and would gather data over a period of days and it would disconnect... Terribly annoying, but there is a setting for USB ports where they go to sleep it's possible that you were maybe doing the same is it on battery or is it plugged in.  [http://www.tomshardware.com/answers/id-1672951/usb-ports-sleeping.html](http://www.tomshardware.com/answers/id-1672951/usb-ports-sleeping.html) 


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/TcwLbDWHU3c) &mdash; content and formatting may not be reliable*
