---
layout: post
title: "Had a productive day. This is print #4"
date: October 12, 2014 01:52
category: "Show and Tell"
author: Mike Miller
---
Had a productive day. This is print #4. #IGentUS

![images/6ac52a74b338dc9ef2d8a68e9ebbeb6e.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6ac52a74b338dc9ef2d8a68e9ebbeb6e.gif)



**Mike Miller**

---
---
**Mike Miller** *October 12, 2014 02:44*

Of course, just after I post this, my Arduino dies. I suspect something thermal as the Mega board was rather toasty. 



Looks like a $20 trip to Microcenter in the morning. :/ And I'll throw a fan on the Arduino/RAMPS board. (Now **+Shauki Bagdadi**'s dual fan solution makes sense. 


---
**Brandon Satterfield** *October 12, 2014 04:22*

Run dual fans on mine too. I have still lost components but that was due to my own mistakes through the years.



The thermal issue... Man, that's just because that printer is hauling so much butt it's setting everything a blaze!! Ha. Seriously, looks great Mike.


---
**Brandon Satterfield** *October 12, 2014 12:36*

Ah good question about SD extension, mine is on the LCD.


---
**Mike Miller** *October 12, 2014 14:23*

I've gotta be up front. There's some CRAZY high acceleration left in the firmware from when I was testing things out. Dunno if I'll turn them down to something sane or not. And the printer's power supply fan is on all the time, creating noise that makes the printer sound a little quieter on video...still, it's not <i>bad</i>, and the infill had me giggling like a schoolgirl. 


---
**Jim Squirrel** *October 12, 2014 21:44*

Soon I will also have mine running


---
**James Rivera** *October 13, 2014 17:28*

The speed on that infill is INSANE!


---
**Mike Miller** *October 13, 2014 18:11*

I was printing a ramp for my son's fingerboard park...if you turn things up too fast, the filament won't lay down on the lower level. While you might be able to bump up print speeds 15 or 20% above everyone else, it won't be without cost. I had a bunch of strings I had to carve off with an Xacto knife. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/fBG3fiSJUrc) &mdash; content and formatting may not be reliable*
