---
layout: post
title: "Greetings all- I have been lurking and browsing for a bit trying to absorb as much as I can before starting my build"
date: November 22, 2017 23:01
category: "Discussion"
author: Dennis P
---
Greetings all-



I have been lurking and browsing for a bit trying to absorb as much as I can before starting my build.  I am wanting to build an Eustathios machine. 



If you could indulge me in a few questions, I would appreciate your time. 



1. Extruders- I am going to be moving up from a Wanhao Di3, so all I know is a direct drive extruder. I understand the Bowden type, and all of the drawbacks of the bowden type seem to be centered around the constraint of the filament in the guide tube.  



To me its no different than a MIG welding gun-- for aluminum you either use a spool gun (much like a direct drive extruder) or a push-pull system like a Cobramatic. That overcame the limitations of soft aluminum weld wire- something very much like trying to print TPU, or trying to push a cooked noodle in a tube.



Has anyone thought to try a 2 motor extruder setup? To keep the weight low on the carriage, a small NEMA8 type motor could give the filament just enough 'oomph' to get through to the nozzle or back out while the NEMA17 at the spool would do the heavy lifting. We should be able to split the motor drive output to 2 motors like the less expensive i3 clones do for Z axis.  



2. Electronics- Do we really need the 32bit boards? Can you get by with a 8bit RAMPS?  My build may take a little while and who knows what the next latest/greatest will be coming out. 



3. Has anyone tried using LinuxEMC to drive one of these pritners? A MESA 5i25 buffered I/O card seems to already do what the 32bit boards do. 



4. Name- does anyone know why Jason named the printer Eustathios? I have not seen anything referencing the origins of the name. 



Best regards, 



Dennis





**Dennis P**

---
---
**Eric Lien** *November 23, 2017 15:50*

Hello Dennis. Glad you are looking at the build. And sorry your post got caught in the spam folder for a few hours.



Here are my answers to your questions:



1: The problem with dual drive extruders (push/pull) is synchronization. Others have tried the solution, but the fact that it is not common is probably explanation enough about its desirability.



That being said you can try the optional Bondtech BMG on the carriage (there are printed parts to try that). Or one of the remote drive solutions like flex3drive extruders.



That being said Bowden is zero problem except for flexibles. My personal opinion... Keep the DI3 for flexibles, and build the Eustathios for speed on everything else.



2: 32 bit is by no means required, just desirable at very high speeds with a tight surface mesh. The stream of code at high speeds can limit you above 100+ mm/s or so.



3: I know **+Joe Spanier**​​​​ has run printers on LinuxCNC before. Perhaps he can chime in. 



4: As for the names,



"This line of printers and names are based on its predecessors:



Tantillus: By Sublime, [http://forums.reprap.org/list.php?279](http://forums.reprap.org/list.php?279)/, similar to the name Tantalus (Tantalus was a Greek mythological figure, most famous for his eternal punishment in Tartarus).



T-slot Tantillus: By GoopyPlastic (aka Brad Hill), [https://github.com/goopyplastic/tslot-tantillus](https://github.com/goopyplastic/tslot-tantillus), Same name convention applies.



Ingentis: By Tim Rastall, [http://ingentistst.blogspot.com](http://ingentistst.blogspot.com)/, I cannot find a definition for Ingentis so I can only assume it is just based on a similar sounding name to Tantillus.



Eustathios: by Jason Smith, [https://github.com/jasonsmit4/Eustathios](https://github.com/jasonsmit4/Eustathios), Greek: [https://en.wikipedia.org/wiki/Eustathius_of_Thessalonica](https://en.wikipedia.org/wiki/Eustathius_of_Thessalonica)



Then my first iteration I called the Eustathios Spider because I made some changes from Jason's design and of course the color scheme.



V2 had more pronounced changes.



And I made HercuLien too which is based on what I learned building the Eustathios, and has a Latin theme to it's name of course."


---
**Maxime Favre** *November 24, 2017 11:25*

I second Eric for the Bowden. You won't have any issue until you start using flexible filaments.


---
**Dennis P** *November 24, 2017 19:47*

Thanks for you input. I think that this will be an on going adventure... 

As far a printing the parts go, accuracy aside, is PLA good enough or go with PETG? 

I have read references about a build documentations and the like but it seems that those efforts have fizzled. Are the in-progress docs accessible?   

thanks again

dennis


---
**Eric Lien** *November 24, 2017 20:06*

The documentation (like a step by step) will likely never come about. But with access to this group and the full 3D model available... there is not a large need. 



Several people have done PLA only parts. I prefer PETG or ABS personally But there is really no need. PLA should hold up for almost any part just fine.


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/MoWRZYhVFwq) &mdash; content and formatting may not be reliable*
