---
layout: post
title: "I am finally ordering all of my herculien parts and I can not find the circular gt2 belts and I am a bit confused with the ordering of the metal for the bed and I can not find the stls for the drill templates"
date: June 08, 2015 22:22
category: "Discussion"
author: Gunnar Meyers
---
I am finally ordering all of my herculien parts and I can not find the circular gt2 belts and I am a bit confused with the ordering of the metal for the bed and I can not find the stls for the drill templates. I hope someone can help and I am excited to finally start. 





**Gunnar Meyers**

---
---
**Godwin Bangera** *June 08, 2015 22:26*

On the Git you will find all the dxf, pdf etc for all the drawings ... pls look here

[https://github.com/eclsnowman/HercuLien/tree/master/Drawings](https://github.com/eclsnowman/HercuLien/tree/master/Drawings)


---
**Godwin Bangera** *June 08, 2015 22:27*

also close loop belts check at [http://www.robotdigg.com/](http://www.robotdigg.com/)


---
**Godwin Bangera** *June 08, 2015 22:38*

closed loop belt

[http://www.sdp-si.com/eStore/Catalog/PartNumber/A%206R51M113060](http://www.sdp-si.com/eStore/Catalog/PartNumber/A%206R51M113060)


---
**Godwin Bangera** *June 08, 2015 22:39*

hope this help :)

Good luck


---
**Derek Schuetz** *June 08, 2015 22:56*

Misumi also has the belts but robot digg has the best deals


---
**Dat Chu** *June 08, 2015 23:17*

Robotdigg belts are best. 


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/CVETWEMzXG4) &mdash; content and formatting may not be reliable*
