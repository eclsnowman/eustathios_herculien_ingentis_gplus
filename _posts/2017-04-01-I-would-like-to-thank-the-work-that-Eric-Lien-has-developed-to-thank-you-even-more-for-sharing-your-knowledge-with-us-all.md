---
layout: post
title: "I would like to thank the work that Eric Lien has developed, to thank you even more for sharing your knowledge with us all"
date: April 01, 2017 18:12
category: "Discussion"
author: lineuve santos
---
I would like to thank the work that Eric Lien has developed, to thank you even more for sharing your knowledge with us all. You have created a community far beyond the horizons of the State of Minnesota.



My sincere thanks.





**lineuve santos**

---
---
**Eric Lien** *April 01, 2017 18:22*

**+lineuve santos**​​ thank you so much for the kind words. I had so much help from great G+ community members when I first started. I have to give back if only to repay my numerous debts. Plus seeing people transition from "newbies" asking the questions to "experts" answering the questions is an unbelievably rewarding endeavor. 



The people are what keep me so interested. If it was just the technology... I probably would have been like a raccoon moving onto the next shiny thing long ago. The friends I have made are the most rewarding part of my journey in this hobby.


---
**lineuve santos** *April 01, 2017 18:53*

It's true, Eric. I fully agree with you. I imagine how rewarding it is for you, to see your project gain space in the world. And I'm very happy to see that in you. You have become a leader and teacher to many people around the world. His design is beautiful, there is beauty in it, I also use Solidworks, and I was very happy with the fonts. Thank you and never too much, and again I say Thank you.


---
**Ray Kholodovsky (Cohesion3D)** *April 01, 2017 20:29*

Yeah, Eric's kinda cool like that. 


---
**lineuve santos** *April 01, 2017 21:08*

Cool


---
*Imported from [Google+](https://plus.google.com/113480415086237436022/posts/GZJoAvinxrD) &mdash; content and formatting may not be reliable*
