---
layout: post
title: "Mock up of how I think the kraken could be mounted"
date: April 18, 2014 13:20
category: "Discussion"
author: David Gray
---
Mock up of how I think the kraken could be mounted. It would involve drilling and tapping 1 side and adding 90 degree thread/barbs. I'm putting this here to see what smarter people than me might think.. Would it be worth the hassle?



![images/00742ab00b2424f8663770fe534a9d15.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/00742ab00b2424f8663770fe534a9d15.png)
![images/3da16e12855cd81029e55bbbb8d5af1b.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3da16e12855cd81029e55bbbb8d5af1b.png)
![images/d0a1bd97663fc88a309e4510d0650e16.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d0a1bd97663fc88a309e4510d0650e16.png)

**David Gray**

---
---
**Dale Dunn** *April 18, 2014 13:49*

It looks nice and clean, but I have no experience with a Kraken. I'm curious what **+Tim Rastall** has to say.


---
**Daniel Fielding** *April 18, 2014 13:55*

Neat


---
**D Rob** *April 18, 2014 13:57*

Doesn't kraken already have side plugs that can be relocated to the top allowing the hoses on the sides. It's how the water canal is made. Drill 2 holes down to the stopping point and one through the side through all. The to ones meet the through all plug 2 hold and for water through the other 2.


---
**David Gray** *April 18, 2014 14:00*

afaik the beta version has 2 side holes for the well, but the v1 release (the version I own) has only 1 side tapped. [http://files.e3d-online.com/Drawings/Kraken/KR1.1_Cooler_Block.pdf](http://files.e3d-online.com/Drawings/Kraken/KR1.1_Cooler_Block.pdf)


---
**Mark Hindess** *April 18, 2014 15:01*

Hopefully **+Sanjay Mortimer** and/or **+Joshua Rowley** will share their thoughts.


---
**D Rob** *April 18, 2014 20:57*

"Ah... I see", says the blind man. Didn't the first one **+Sanjay Mortimer** showed have two side plugs and I thought was described as a possible water way. Although through the sides it's less efficient for thermaldynamics. The top pockets of water created would not be recirculated as well since flow will go past them the top to top flow is a longer u shaped canal and would have higher efficiency for removing the heat.﻿


---
**Tim Rastall** *April 19, 2014 05:52*

Yes, the v1 only has one hole tapped on the side. They did away with the other as it wasn't necessary and added complexity. You could always drill one though; pretty straight forward as you could go in from the cut side and drill through.

The design is very efficient, the only down side I can see is that the holes the bearings would go in could not be printed with their axis aligned to z. It's often tricky to get a perfectly circular hole if its printed on its side and any imperfections can result in bearing misalignment and binding issues. I'd suggest hexagonal holes as they print more cleanly and provide good alignment for bearings.


---
**Dale Dunn** *April 19, 2014 13:57*

**+Tim Rastall** , have you tried printing such holes diagonally? I haven't. **+nop head** did some experiments with that a few years ago, I think.


---
**David Gray** *April 19, 2014 14:04*

The holes are a non issue IMO. I would "teardrop" the holes on a functional part (this is just a mock up). **+D Rob** does bring up a good point about the cooling efficiency though. I would love to get some more insight on that aspect.


---
**Sanjay Mortimer** *April 21, 2014 20:10*

Hi guys,

To clarify - kraken has now only one side hole, having the extra redundant side hole added a really large amount of manufacturing complexity because it required a human to manually take the part, flip it, drill it and tap it, without CNC. Which added more cost than it was worth.



You can of course drill and tap the extra hole without much hassle. However the issue I see with this design is that the 90degree barbs have to screw into a BSPT (Taper thread) hole. There is no way to ensure that the barb once tightened will be at that desired angle. You could work around this by using swivel barbs, but that may add come cost and size/weight.


---
*Imported from [Google+](https://plus.google.com/117535437951142576212/posts/WvmvhXsRHzL) &mdash; content and formatting may not be reliable*
