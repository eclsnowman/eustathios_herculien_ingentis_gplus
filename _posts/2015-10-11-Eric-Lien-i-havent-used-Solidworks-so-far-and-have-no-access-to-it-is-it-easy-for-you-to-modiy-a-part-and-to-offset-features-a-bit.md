---
layout: post
title: "Eric Lien i haven't used Solidworks so far (and have no access to it) - is it easy for you to modiy a part and to offset features a bit?"
date: October 11, 2015 07:26
category: "Deviations from Norm"
author: Frank “Helmi” Helmschrott
---
**+Eric Lien** i haven't used Solidworks so far (and have no access to it) - is it easy for you to modiy a part and to offset features a bit? If so could you probably modify the XY end belt tensioner ([https://github.com/eclsnowman/Eustathios-Spider-V2/blob/master/STL%20Print%20FIles/XY%20Axis%20Belt%20Tensioner%20A.STL](https://github.com/eclsnowman/Eustathios-Spider-V2/blob/master/STL%20Print%20FIles/XY%20Axis%20Belt%20Tensioner%20A.STL)) and move the whole for the crossbars 3mm away from the outer bars? I'm going to design an LM8LUU-Carriage and i think it would be better to be able to cross the bearings which wouldn't be possible with the curerent distance of bars. 6mm more would give roughly 1-2mm between the bearings and should still work fine.



If it's too much of a work don't worry - in this case i'll completely redraw them.





**Frank “Helmi” Helmschrott**

---
---
**Dave Hylands** *October 11, 2015 07:30*

Onshape is a free online 3d cad that can import solid works files


---
**Miguel Sánchez** *October 11, 2015 07:34*

If you can use Onshape, here you have the part as a public model [https://cad.onshape.com/documents/c7d31a3cbfe14bcf8d3b22c7/w/0ae5f95013104a38b0d3d585](https://cad.onshape.com/documents/c7d31a3cbfe14bcf8d3b22c7/w/0ae5f95013104a38b0d3d585) . Basic editing is possible.


---
**Frank “Helmi” Helmschrott** *October 11, 2015 12:47*

thanks guys, i'm using Fusion360 that can also import solidwors files but it's not able to edit basic sketches that underly an object. I thought Eric might be able to edit some parametric values and do it quickly. If that's possible with OnShape i just don't know how - i have never used it. I tried it but it never worked well for me.


---
**Miguel Sánchez** *October 11, 2015 13:15*

**+Frank Helmschrott** You can move sides and holes in Onshape with the move face command [https://forum.onshape.com/discussion/143/how-to-import-x-t-assembly-and-modify-a-part](https://forum.onshape.com/discussion/143/how-to-import-x-t-assembly-and-modify-a-part)


---
**Eric Lien** *October 11, 2015 13:26*

I will work on this later today. 


---
**Eric Lien** *October 11, 2015 14:23*

[https://drive.google.com/folderview?id=0B1rU7sHY9d8qVkZLdWxtT2FDY28&usp=sharing](https://drive.google.com/folderview?id=0B1rU7sHY9d8qVkZLdWxtT2FDY28&usp=sharing)



There is the solidworks files, along with step, x_t, and stl. Hope it is what you were looking for.


---
**Miguel Sánchez** *October 11, 2015 14:47*

I have done the same thing in Onshape 



[https://cad.onshape.com/documents/c7d31a3cbfe14bcf8d3b22c7/w/0ae5f95013104a38b0d3d585](https://cad.onshape.com/documents/c7d31a3cbfe14bcf8d3b22c7/w/0ae5f95013104a38b0d3d585)


---
**Frank “Helmi” Helmschrott** *October 11, 2015 14:49*

thanks for helping guys, this is highly appreciated. maybe i should look into onshape at a later point but it looks like it only can do the things that Fusion360 can too. But then i'm probably missing its advantages.


---
**Miguel Sánchez** *October 11, 2015 14:51*

**+Frank Helmschrott** for moving some features (faces and holes) you can select them, select a direction and distance to be moved. I am sure other tools can do that too, but Onshape being online it means it is a matter of seconds to get it done (once you know the move face icon, of course :-)



You can see the transformation history on the left pane, so you can see I did it in two steps as I missed one face in the first move face command.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/UeRLsY8f4k8) &mdash; content and formatting may not be reliable*
