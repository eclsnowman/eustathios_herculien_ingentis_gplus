---
layout: post
title: "Vase anyone? :-) At least the continous extrusion seems to work well now"
date: August 06, 2015 12:57
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Vase anyone? :-) At least the continous extrusion seems to work well now. I've selected a print that works without much retracts and printed that vase with the 0.6 nozzle and 0.4 layer height quite well. I've changed the bowden tube to a longer one (950mm is really massively long, **+Eric Lien** - sure we need that much of an arc?) and found a carbon stick to support it - that works ok for the moment.



I've also just reset the Bondtechs setscrews as they seem to have used too much force and also rechecked all Slic3r settings for now (**+Eric Lien** do you still not forget me regarding your factory files? :-))



I now changed the nozzle to a 0.3 and try to print Marvin. As it prints right next to me now i see quite some overextrusion that shouldn't happen - i have to remeasure the filament but from what i saw so far it seems to be of rather bad quality when it comes to diameter consistency - that could be an additional problem.



![images/3f44149f77309902d9303cf20e0b0784.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3f44149f77309902d9303cf20e0b0784.jpeg)
![images/6963ffbd7ce5defdeb6a125b1c5839c3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6963ffbd7ce5defdeb6a125b1c5839c3.jpeg)

**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *August 06, 2015 13:15*

**+Frank Helmschrott** my 950mm is likely excessive. I just made sure I had a gentle curve with the carriage at the far corner and that's where the length ended up. But I am sure other lengths will work just as well. The big trick is making sure the bundle has a gentle arc which from your pictures looks very nice. 



I have not forgotten about the factory files. I tried to look last night, the problem I have is I have over 200 factory files. Many times I forget which were the good ones, and which ones cause me problems :) I learn something each print, make changes and sometimes they work out, sometimes it is two steps backwards. Also S3D V3 is giving me issues with a few of my old presets. For some reason it is telling me it cannot find the .fff files tied to them. So please know I am working on it, I want to give you my best setups.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/gKtKkrqQkBG) &mdash; content and formatting may not be reliable*
