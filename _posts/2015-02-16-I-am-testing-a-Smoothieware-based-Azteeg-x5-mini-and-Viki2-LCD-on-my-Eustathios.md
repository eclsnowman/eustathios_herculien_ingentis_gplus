---
layout: post
title: "I am testing a Smoothieware based Azteeg x5 mini, and Viki2 LCD on my Eustathios"
date: February 16, 2015 04:38
category: "Discussion"
author: Eric Lien
---
I am testing a Smoothieware based Azteeg x5 mini, and Viki2 LCD on my Eustathios. The motion platform is amazing. I can run 50% faster than I could with my Azteeg x3 mini before I begin to see chatter on decel and quick moves. My only issue is I cannot seem to get the heated bed and hot end to heat up. The display on the Viki2 shows that it is enabled and heating... but the temp never rises.



If there are any Smoothieware experts out there I would appreciate any help you can provide. Here is the config file as I have it right now: [https://drive.google.com/file/d/0B1rU7sHY9d8qZDZyQWFNQXhiSHM/view?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qZDZyQWFNQXhiSHM/view?usp=sharing)





**Eric Lien**

---
---
**Tim Rastall** *February 16, 2015 05:06*

Have you tried controlling it through repetier? Which configuration file and firmware build are you using? 


---
**Eric Lien** *February 16, 2015 05:55*

**+Tim Rastall** I tried on the viki2 and via simplify3d. I grabbed the latest firmware file from the Smoothieware builds, and the config from the github for the azteeg sample.


---
**Seth Messer** *February 16, 2015 06:03*

I'm assuming you've verified the hotend,bed, and thermistor pins are correct? I poked around that config, but of course am not much use yet other than to make sure nothing obvious is out of sorts there. Crazy timing of posting this. Was just thinking about posting to see who all has gone smoothieware for their board. Good luck!  Hopefully someone chimes in. 


---
**Eric Lien** *February 16, 2015 06:09*

Tim, Here is where I got the firmware: [https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware.bin](https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware.bin)



And here is where I got the config file before editing it: [https://github.com/Smoothieware/Smoothieware/tree/edge/ConfigSamples/AzteegX5Mini](https://github.com/Smoothieware/Smoothieware/tree/edge/ConfigSamples/AzteegX5Mini)



**+Seth Messer**  I assume the pins are correct from the sample config... but I cannot say for certain since I don't know how to confirm it.


---
**Tim Rastall** *February 16, 2015 06:59*

Here's my config and firmware files. You'll need to change the extension of the firmware file to get the board to read it. This version definitely works for hot-end heating, haven't got around to plugging the viki I have in yet or the heat bed.   [https://drive.google.com/folderview?id=0B3uiE-urF0rKfkYyQmd1b3NnQTZRd3dIN1R1QnBxYlVpS0lTaGYzVlhTTVZ6cHNTS2NDQWs&usp=sharing](https://drive.google.com/folderview?id=0B3uiE-urF0rKfkYyQmd1b3NnQTZRd3dIN1R1QnBxYlVpS0lTaGYzVlhTTVZ6cHNTS2NDQWs&usp=sharing)


---
**Martin Bondéus** *February 16, 2015 11:17*

On my smoothie there's a small led at the mosfet that goes on when the output is set, I do not know if the azteeg has this?


---
**Ivans Nabereznihs** *February 17, 2015 12:28*

I have 300*300mm 24v 270w ali rubber heatbed  and 24v 600w powersupply. It's run without problem on 120C. Have you done autopid for heatedbed?


---
**Eric Lien** *February 17, 2015 13:04*

**+Ivans Nabereznihs** I have tried. But nothing heats up.


---
**Ivans Nabereznihs** *February 17, 2015 14:42*

I remember that as default heatedbed was disabled in the original config. Maybe you have it disabled?


---
**Eric Lien** *February 17, 2015 17:42*

Nope. It is enabled. Think I may have a bum board. Roy is sending out a replacement.


---
**Seth Messer** *February 17, 2015 20:39*

would be nice if that's all it ends up being.


---
**Seth Messer** *February 18, 2015 00:10*

**+Eric Lien** if it helps, you can chat with the smoothieware guys on [irc.freenode.net](http://irc.freenode.net) >  #smoothieware


---
**Eric Lien** *February 18, 2015 01:00*

**+Seth Messer** I was there a few days ago for about 3 hours. Not much for help on an Azteeg. Most guys knew the smoothieboard, but not the X5 mini.


---
**Seth Messer** *February 18, 2015 01:01*

:/ fair enough. ahh well, hopefully the replacement does the trick.


---
**Eric Lien** *February 21, 2015 05:52*

New board and all systems go. PID tune in process. Can't wait to try a first print using smoothie.


---
**Seth Messer** *February 21, 2015 14:18*

exccccellent. glad that it was just the board (still a pain, but an easy fix).


---
**Gus Montoya** *March 07, 2015 01:23*

**+Eric Lien**  How is the board doing now two weeks out?  Whats the avg print speeds your experiencing now on your Eustathios?


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/fL1Yce4dbnt) &mdash; content and formatting may not be reliable*
