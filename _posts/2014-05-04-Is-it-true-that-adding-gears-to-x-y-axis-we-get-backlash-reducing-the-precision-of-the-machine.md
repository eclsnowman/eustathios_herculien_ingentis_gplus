---
layout: post
title: "Is it true that adding gears to x/y axis we get backlash, reducing the precision of the machine"
date: May 04, 2014 15:19
category: "Discussion"
author: George Salgueiro
---
Is it true that adding gears to x/y axis we get backlash, reducing the precision of the machine. Anyone has a machine that has it? What would be the best gear reduction and model. 





**George Salgueiro**

---
---
**George Salgueiro** *May 04, 2014 15:30*

How about 2 to 5 times reduction. Will I get more precision? 


---
**George Salgueiro** *May 04, 2014 17:15*

Which stepper do you consider the best? 


---
**George Salgueiro** *May 04, 2014 17:16*

1/32 half steps would be better than 1/16. 


---
**William Frick** *May 05, 2014 19:34*

**+George Salgueiro** You will get a higher resolution but sacrifice speed.  The key is to find the balance that works for you. More components add their own errors to the final result.....


---
**George Salgueiro** *May 09, 2014 15:35*

**+William Frick** i increase the resolution, but what about the precision?


---
*Imported from [Google+](https://plus.google.com/103579216912687360818/posts/MNh3XGwpaGN) &mdash; content and formatting may not be reliable*
