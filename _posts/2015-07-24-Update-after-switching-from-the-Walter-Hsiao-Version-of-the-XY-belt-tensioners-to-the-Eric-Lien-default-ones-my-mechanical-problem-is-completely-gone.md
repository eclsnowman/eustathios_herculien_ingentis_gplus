---
layout: post
title: "Update: after switching from the Walter Hsiao -Version of the XY belt tensioners to the Eric Lien default ones my mechanical problem is completely gone"
date: July 24, 2015 13:02
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Update: after switching from the **+Walter Hsiao**-Version of the XY belt tensioners to the **+Eric Lien** default ones my mechanical problem is completely gone. Printer runs again. \o/



Additionally i swapped out the electronic to the best i have seen so far. I'm gonna tell you later about it.

![images/f8cb2bab552ee3af9223b0fcf6af9809.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f8cb2bab552ee3af9223b0fcf6af9809.gif)



**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *July 24, 2015 13:17*

Great news. I look forward to hearing about the electronics you are using.


---
**Igor Kolesnik** *July 24, 2015 13:43*

BTW. It would be nice to hear who is using what board and their impressions. I know there are a lot af aztegs but I use RAMBo for example


---
**Isaac Arciaga** *July 24, 2015 19:55*

**+Frank Helmschrott** in regards to Walter's XY carriages, were you using his 1st or 2nd version? His first version had a miscalculation in measurement.


---
**Frank “Helmi” Helmschrott** *July 24, 2015 19:58*

The second. They were basically ok, dunno if it was my print quality or just some conceptional problems but they caused some high friction (see my last posts for details)


---
**Isaac Arciaga** *July 24, 2015 20:10*

Got it, thanks. I printed the 2nd rev as well but have not set up the gantry yet. Thanks for the info.


---
**Walter Hsiao** *July 24, 2015 20:21*

Hmm, should I remove them from thingiverse or add some kind of warning?  It's just going to confuse people if it doesn't work.  I'm using the 10mm version, but I never tested the 8mm one.


---
**Isaac Arciaga** *July 24, 2015 20:25*

**+Walter Hsiao** for the 10mm version it looks to be fine when adding it to the rest of the cad model. I wouldn't remove them in my opinion. They look nice :)


---
**Walter Hsiao** *July 24, 2015 20:34*

Thanks, but it only makes things worse if it looks good, then doesn't work :)


---
**Eric Lien** *July 24, 2015 20:37*

**+Walter Hsiao**​​ on these printers initial traming and break-in are critical.  So it may have just been minor variations of the assembly of the gantry between your parts and mine, and no real difference in the functionality of the part. I had my printer acting up when I first built it, took it apart, then reassembled and it worked perfectly using the exact same parts. 



That's why my statement to people during the X/Y gantry assembly is always... WHEN IT'S RIGHT YOU WILL KNOW. It's like the definition of pornography. Its hard to put into words and pin it down... but you'll know it when you see it ;)﻿


---
**Isaac Arciaga** *July 24, 2015 20:37*

I'm planning on working on the Eust this weekend. I'll let you know if I encounter the same issue.


---
**Walter Hsiao** *July 24, 2015 21:08*

Thanks Isaac!



**+Eric Lien** Yeah, I think the graphite bushings are particularly challenging.  On the other hand, they let you know when things are even slightly out of alignment which is useful when adjusting the gantry.  I brought my printer to the tech shop yesterday and transporting it changed the alignment enough that it needed a bit of adjustment when it came back.  I'll probably try switching the extruder to self aligning bushings next time I print one.


---
**Frank “Helmi” Helmschrott** *July 25, 2015 05:47*

Just to be clear and as I think I was unprecise in what I said: I'm still using the graphite bushings on the XY tensioners. I did modify the default pieces to take them (pushed the whole through at the outer diameter of the self aligning bushings (think it's 15.9mm or something around that). then I reamed the holes a bit to be perfect and to fit the sleeves I printed in Filaflex. The sleeves are around 0,7mm in wall thickness (so rather thick) and they give everything a tight fit. Currently I'm still using the smaller bushings in the carriage but I may switch back to the graphite ones there too as I found out that they haven't been the problem.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/DNsuPDAjNca) &mdash; content and formatting may not be reliable*
