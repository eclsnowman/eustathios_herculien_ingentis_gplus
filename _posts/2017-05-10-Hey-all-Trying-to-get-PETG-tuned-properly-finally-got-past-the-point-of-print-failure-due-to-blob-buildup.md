---
layout: post
title: "Hey all. Trying to get PETG tuned properly, finally got past the point of print failure due to blob buildup"
date: May 10, 2017 03:00
category: "Discussion"
author: Ben Delarre
---
Hey all. Trying to get PETG tuned properly, finally got past the point of print failure due to blob buildup. But just can't quite get these little zits to go away.



Now running at the very edge of under extrusion (as can be seen in the top surface of this photo) with 0.45mm coast and 5.5mm retract with -0.5mm restore at 242c at 3600mm/min.



Lowering the temp seems to result in poor layer adhesion on a brand new roll of eSun PETG. I can I suppose keep increasing coast and decreasing restore but it feels like they are high already.



Any suggestions?

![images/57cc32ec30cdedf49765d021e6e78509.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/57cc32ec30cdedf49765d021e6e78509.jpeg)



**Ben Delarre**

---
---
**Benjamin Liedblad** *May 10, 2017 03:48*

Just went through a 1kg roll of eSun PETG natural myself...

Same issues - still trying to dial it in.



My largest improvement came when I lowered my speed from 60mm/s to 40mm/s.



I've tried to vary temp from 225C to 245C (no telling how accurate that is with my setup). I haven't been very scientific about this, but other than 225C not sticking perimeters for large prints - only difference seems to be more oozing at hotter temps. 



I've currently settled on 235C (again, my temp may not correlate to actual value) and will try to optimize around that.



Still trying to tweak coast/wipe/retract... Too much or to fast on retraction with my bowden causes issues. I was about to run a test without any retraction, but SUPER wipe and/or MAJOR coast.



With or without retraction, I've found that large differential speeds between perimeters and infill can cause under extrusion when infill starts.



My best quality layers so far have been 40mm/s for perimeters AND infill (first layer at 100% speed) with retraction turned off and travel speed at 120mm/s.



Made a nice solid part, but this isn't going to fly on parts where there are a lot of 'islands' as I'm working on now.



Looking at your part, no retraction at 40mm/s might work well .



Keep us posted.


---
**Ben Delarre** *May 10, 2017 04:28*

Thanks for the info **+Benjamin Liedblad** will run another print tomorrow night and we'll see how it goes at 40mm/s. Maybe I'll reduce my retraction too.


---
**Oliver Seiler** *May 10, 2017 05:44*

I'm printing ColorFabb XT (PETG) using the following settings. There are some minor blobs, but far less worse than in your photo. Also consider aligning the layer start points for easier removal.

Retraction: 2.5mm@65mm/sec

layer height: 0.15mm

Hotend: 250-255 degrees

Bed: 85 degrees

Fan: 60% from layer 3

Speed: 50mm/sec, 90% for outer perimeter

Extrusion multiplier: 100%

No coasting/restart

I used to have more retraction (5.5mm), but using less helped.

You can also try wiping the nozzle.


---
**Brandon Satterfield** *May 10, 2017 18:44*

I too have been playing a little with this family of plastics. The blobs were killing me. And running dual extrusion the idle extruder was just dripping away. 



I finally ended up satisfied with a 6mm retract @ 1800 and at temp of 230, coasting .3 wiping 5mm. My Bowden tubing is super long though. At tool change I'm actually pulling up I think around 25mm and either dumping, purging and wiping or using a purge tower. So far so good. This all was done on PET though so may not work as well on PETG. 


---
**Dani Epstein** *May 10, 2017 20:33*

Your PETG might be damp. Before i Print in PETG I dry it in a food drier for a few hours at 45C.


---
**Benjamin Liedblad** *May 11, 2017 20:12*

Any progress +Ben Delarre? 



With the last bit of eSun natural, I set off to print a new spool holder for the eSun spools. 



+Eric Lien was right in his response to a previous question of mine... How fast you can go really depends on the geometry. 



As you can see from the images, first attempt (right) was a fail. I tried to merge two spool holder designs, which ended up being thin walled islands. Even with the print speed SLOW (40mm/s), I had blobs which resulted in layer shift. This print had some retraction (~2mm @4mmm/s), but no coast and under 1mm wipe.



For the second attempt, I changed the design to eliminate the islands and just for fun decided to change my settings based on recommendations by +Oliver Seiler and +Brandon Satterfield.



Kept temp at 235C, increased speed to 50mm/s, with 90% for outer perimeters. Added 50% fan (one of two fans running FULL ON). Retract 6mm @30mm/s, .3mm coast, 5.0mm wipe



Other than some light sanding to get rid of the retract/restart artifacts, it came out great - the infill also looks a lot cleaner.



Not sure how much of the improvement was due to the design change, but I'll try to print the first design again with the new settings to see if it makes the difference.







![images/41f5971b90191117cbdb62fb8a49b0b8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/41f5971b90191117cbdb62fb8a49b0b8.jpeg)


---
**Ben Delarre** *May 12, 2017 04:00*

Haven't had a chance to try another print yet. But I think the trend seems to be slower and less and slower retraction. Which all runs counter to what I was expecting and might explain why my increasing retraction didn't seem to help at all... Will post as soon as I get a chance to run another print.


---
**Ben Delarre** *May 21, 2017 18:35*

So I tried a couple more prints yesterday but very strange things are going on. If I use any wipe at all then some of the first perimeters don't print properly resulting in a failed print early on. When I have wipe on, doesn't matter if its 0.5mm or 5mm the first perimeter goes down fine, it then retracts and wipes and moves to another perimeter where it appears to not have enough filament in the hotend. Eventually it starts printing again but only after moving to a part of the print with no more retraction points.



So it seems like retraction and wipe are not interacting well. So I tried reducing retraction to zero and then I get filament blobbing during moves. Figuring that there wasn't enough filament in the hot end during these bad perimeters I stopped the print and moved the head directly up, expecting it to come away with no filament coming out. However it seemed to have backed up filament for some reason as it extruded a whole bunch as the head moved up. This of course sounded like a bed level issue (nozzle too close to the bed). But if I remove the wipe settings it prints fine and that first layer is perfect.



I don't have the time for more investigation at the moment, but wondering if there's some strange interaction between firmware retraction, wipe, and the 'retract during wipe' setting.


---
**Oliver Seiler** *May 21, 2017 19:39*

The filament backing up and then coming out all at once can be a symptom of not enough cooling and the filament getting soft in the cold part of the extruder or too much retraction.

Check the fan and make sure you have used heat paste on the cold part of the heat break.


---
**Ben Delarre** *May 21, 2017 20:45*

Did the heatpaste thing recently. I just don't understand why this only occurs after wipe though. Prints fine without it.


---
**Oliver Seiler** *May 21, 2017 21:22*

Strange indeed. Have you tried wipe and no retraction (just out of curiosity)?

I had some issues with Smoothieware when using wiping that it didn't seem to apply acceleration properly to the extruder when wiping at the same time and my otherwise trustworthy BondTech started skipping steps and wouldn't move at all.



With the filament building up in the hot end, I'd think it's a mechanical problem though. Is your hotend and nozzle smooth and free of any obstacles internally?

Also some people seem to have success seasoning the hotend for less friction.




---
**Ben Delarre** *May 22, 2017 01:30*

Figured it out by looking at the Gcode.



tldr: Do not use firmware retraction if using retract during wipe



I've been using firmware retraction with Simplify3d by using a script which replaces the retract moves with the firmware retraction commands. This has generally worked great, but it is incompatible with the 'retract during wipe' option in Simplify3d.



When retracting during the wipe it would use the retract setting from the main retract value that was being used by the script (4.5mm retract) but split up over a bunch of moves during the wipe. Therefore the script did not correctly replace this retraction with my firmware retract command. However, the restore was still done in the same manner and was replaced by my script. Therefore I had a mismatch between the retraction and the restore.






---
**Benjamin Liedblad** *May 22, 2017 01:36*

**+Ben Delarre**, just out of curiosity, what is your extrusion multiplier set to?



Pretty sure I had a similar problem on my first day printing with the eSun PETG.



I took great care to verify my counts per mm of the E axis was perfect (it was the same as I had used for ABS). 



I naively assumed that this should translate to 100% extrusion multiplier (which was what I used for the ABS roll I was using before the switch). 



It wasn't until I tried to print a single shell cube with .4mm walls that I found an oddity. It was printing at ~.6mm!



I had to adjust to 68% - might need fine tuning still, but it improved my prints a bit and eliminated the issue you described of massive amounts of plastic pouring out if you cancel and move straight up. 


---
**Ben Delarre** *May 22, 2017 01:39*

**+Benjamin Liedblad** I did the tuning with the 40x20_ piece to tune the wall size to 0.4mm, so I'm pretty sure its right now. Using 1.13 multiplier to get the 3 straight walls pretty much spot on, the curved one is a little out. I then backed off further till I noticed a little under extrusion just to make sure.


---
**Ben Delarre** *June 02, 2017 03:18*

Ok more trials this week. 



Tried some of the above, have found that printing slower (40mm/s) definitely helps. Wipe of 5mm seems to aid in getting rid of the blobs, but I'm still getting build up, even after going a little past the point of under extrusion.



I decided trying to use 100% fan since I've got some upcurl on some overhang corners, and consequently I'm only reaching about 10 degrees under target temp with the fan on max. The smoothieboard then halts after around 10mins due to never getting to temp. This of course seems seem odd since I PID autotuned to 250 with fan on full, and am now printing at 250 but only reaching 240 during the print which is why it eventually halts.



One day I'll be able to use my printer without constantly battling issues, today is not that day.


---
**Oliver Seiler** *June 02, 2017 05:28*

What wattage heater cartridge are you using? For me the E3D ones didn't deliver what they were supposed to and I'm using more powerful ones now to reach the temps required. Also check if the board is sitting at 100% power for the hotend while at 240 degrees. From my experience the autotune in smoothieware doest work that well and needs some manual improvements on the parameters. 


---
**Ben Delarre** *June 02, 2017 05:37*

Ok so I reverted to some manual PID settings and things are definitely better. Holding steady at 240 with 54% power with fan on 30%, still need to do some more tuning so I can push that fan up, but my prints are a lot better now. This 3d printing lark is a huge time sink isn't it? :-)


---
**Ben Delarre** *June 03, 2017 23:30*

Well what a difference some tweaking makes! Printed this part of the pangolin critter yesterday, much much better. I've got a little under extrusion on the top which will need dialing back a little. I think I might have something wrong in my setup still to be honest because this is definitely under extruded, and i've had to set coast to 1mm and 5mm wipe to avoid the blobs. From careful watching of the print its clear there's a backup of pressure in the nozzle, since when the head moves away from a wall to a free area a few mm of filament extrudes immediately. Under extruding and this large coast have helped reduce this to something small enough to not really be an issue, but I'm still unsure as to why the pressure build up is happening in the first place. Any ideas? This is at 40mm/s, 245c. 

![images/d7816c87457bf355385c6aeb82249a8a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d7816c87457bf355385c6aeb82249a8a.jpeg)


---
**Benjamin Liedblad** *June 12, 2017 03:06*

**+Ben Delarre**, you're spot on with two of your comments... 

1) this being a time sink... 

2) what a difference some tweaking makes...



I recently switched to a new roll re-calibrated the filament parameters and tried a BIG print. 



Even with the parameter tune-up, the first few layers were coming out way underfilled. I kept kicking up the flow rate multiplier in OctoPrint until the infill started looking good and figured it would fail a few hours in due to blobs... but 22hrs. later it actually finished! (see image of time sink below)



Since then, I've been back on the hunt, trying to get the right parameters for PETG. Again.



I hear you on the "pressure build up" problem. After my most recent experiments, I'm convinced that retraction may be causing more issues than it solves. My latest prints have not had any "islands", so I set the checkbox to "only retract when crossing open spaces" and told the slicer to "avoid crossing outlines" with a detour path of 10mm allowed. This added a bunch of extra travel, but I've never seen the material print so good. Oh... and I was able to bump print speed back to 60mm/s @240C!



For my next trick, I think I'll experiment with retraction of zero length, but very long "wipe". Probably calls for custom G-code to test how far the nozzle needs to travel after Extruder stops before material stops pouring out.



Cheers!



![images/39e87c77c0e0bb2aab8c8f724b442430.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/39e87c77c0e0bb2aab8c8f724b442430.jpeg)


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/F52Z5CgVk1D) &mdash; content and formatting may not be reliable*
