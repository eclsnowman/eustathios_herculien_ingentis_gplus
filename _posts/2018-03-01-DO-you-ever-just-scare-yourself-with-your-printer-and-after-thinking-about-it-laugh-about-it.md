---
layout: post
title: "DO you ever just scare yourself with your printer, and after thinking about it, laugh about it?"
date: March 01, 2018 03:41
category: "Deviations from Norm"
author: Bruce Lunde
---
DO you ever just scare yourself with your printer, and after thinking about it, laugh about it?



Tonight I had a chance to get my parts cooling fan hooked up to be controlled by the smoothieboard/pwm.  Everyone was telling me this would improve my pillowing problem. So I decided while I was doing the wiring to go ahead and install the smoothie into the enclosure I printed a couple weeks ago.



Seems simple until I went to print again.  I also needed to print a new duct for the fan to keep it pointed towards the parts, as otherwise it cooled the extruder too much. 



Well, a minute or so into the print, the Octoprint interface came up and told me I was having a T heater runaway, disconnect and reset, etc. Well, you know I did what I was told, and then went back through all my connections, tested every wire, and finally tried again, only to have the message reappear. Man, I was mad at myself for making any changes, so I went upstairs and watched some TV with my wife.  



Now you get to laugh with me.  I went back to the basement and spent some

time thinking through the process, and realize that the temp. change came after about 3-4 layers, LOL.  The fan was turning on about then, and cooling down the extruder, while I was trying to print the duct to avoid that problem.LOL!   Now I re-sliced the part WITHOUT the automated cooling and am printing away!

![images/b2efaf4f869ebd79f5e7d72f480368be.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b2efaf4f869ebd79f5e7d72f480368be.png)



**Bruce Lunde**

---


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/eUiSe2GxrKg) &mdash; content and formatting may not be reliable*
