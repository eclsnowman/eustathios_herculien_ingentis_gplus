---
layout: post
title: "Designed a case for the 12864 display"
date: May 30, 2016 19:44
category: "Show and Tell"
author: Daniel F
---
Designed a case for the 12864 display



![images/5781019c80eeb3305d2cc85f34652321.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5781019c80eeb3305d2cc85f34652321.jpeg)
![images/d60ec1807584ff3bd352aefd7056e5ed.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d60ec1807584ff3bd352aefd7056e5ed.jpeg)
![images/c778d45808d4c2c88840ffe022a4d799.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c778d45808d4c2c88840ffe022a4d799.jpeg)
![images/63b9cbff7ea74f1eeafd82739d34b744.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/63b9cbff7ea74f1eeafd82739d34b744.jpeg)

**Daniel F**

---


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/g3MkqfJJSKT) &mdash; content and formatting may not be reliable*
