---
layout: post
title: "Has anyone yet made alternative filament spool holder for the Eusthatios Spider V2?"
date: August 30, 2015 14:27
category: "Deviations from Norm"
author: Frank “Helmi” Helmschrott
---
Has anyone yet made alternative filament spool holder for the Eusthatios Spider V2? I've got some spools that don't fit like (holes) 88x60mm, 95x30, 125x30.



I'm also thinking about doing a different holder but thought i'll first ask if anyone has yet done something.





**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *August 30, 2015 14:54*

I know some people have made different dimension versions of the hub by editing the solidworks sketches. I also have a version that uses skate bearings... But it moves so easy that I had spools unravel so I stuck with the static hub myself.



If you had some ideas I could make them and add them to the github. Also on Youmagine I have more versions of the static hub.


---
**Frank “Helmi” Helmschrott** *August 30, 2015 15:05*

Thanks, **+Eric Lien** - unfortunately i don't have Solidworks. Didn't check your Fusion360 file yet but you probably don't have parametric stuff in there, do you? Otherwise i'd use that.



Apart from that i'm a bit worried about the length of the hubs for the bigger/wider spools (especially the rare 125mm ones). I currently use them on a holder that just stands on the desk next to the printer. Probably i'll add some extrusion to the frame. That would make it a more stable solution and could also be a bit more flexible for different spools. Unfortunately they differ a lot.


---
**Michaël Memeteau** *August 30, 2015 15:23*

**+Frank Helmschrott** Even without Solidworks, you could use Onshape to get either Solidworks or STEP file in it and work it out. It's free and very serious (from the team that was behind Solidworks I believe).


---
**Frank “Helmi” Helmschrott** *August 30, 2015 15:58*

**+Michaël Memeteau** thank you for the hint. I looked at onshape some weeks ago but found it quite a bit complicated at first sight. Maybe i should invest some more time as soon as i have some left :)


---
**Michaël Memeteau** *August 30, 2015 16:05*

**+Frank Helmschrott** You should, it's really easy for someone that has some Solidworks background. It wasn't the case for me, but still I am very happy to have invested some time in it. I know already quite a lot of CAD systems (CATIA, AutoCAD, Rhino). Depending on what you do, this is one can really be great, especially for 3D printing. Parametric is nice.

The  #saintflint   was entirely developed with Onshape for example... 

[http://www.thingiverse.com/search?q=saintflint&sa=](http://www.thingiverse.com/search?q=saintflint&sa=)


---
**Erik Scott** *August 30, 2015 18:08*

I'm a fairly happy OnShape user. It's not open source, but it's a nice free service for hobbyists that don't mind sharing their work. 



I use the skate-bearing versions of Eric's filament holders. I made a 30mm version for some filament I couldn't get at Ultimachine. Is that what you're looking for? I can give you the STL, but the file is in Designspark format. 


---
**Frank “Helmi” Helmschrott** *August 30, 2015 18:10*

Looks like the same is true for **+Autodesk Fusion 360** - somehow i've come across this 2 years ago and didn't really take note of OnShape until a few weeks aog. The only thing is you don't have to share anything with the Fusion360 Startup/Enthusiast license.



If you don't mind sharing your stl i'd be happy to see if it helps.


---
**Erik Scott** *August 30, 2015 18:12*

You can find the files here: [https://www.dropbox.com/sh/szwq3u7e5lmjmrm/AADEbCv2GVeG8AN08sdr8Al8a?dl=0](https://www.dropbox.com/sh/szwq3u7e5lmjmrm/AADEbCv2GVeG8AN08sdr8Al8a?dl=0)


---
**Michaël Memeteau** *August 30, 2015 18:15*

**+Frank Helmschrott** You're right, Fusion is also an excellent product. It does not, however, works from your browser or allow for collaborative design (like google docs but for CAD). Version control is also the forte from Onshape. Maybe the only weak spots would be: working on very complex/heavy models and also organic/complex geometry (NURBS and the like).


---
**Frank “Helmi” Helmschrott** *August 30, 2015 18:19*

**+Michaël Memeteau** hmm not trying to advertise for Autodesk here but Fusion360 does support collaboration though not "live" but i think they're working on some more here. Version control is there for a long time now - i think nearly from the beginning. It's true that Fusion360 is not a browser app but it works on Windows and Mac OSX quite well - so if you're not using Linux you should be good. Additionally it offers quite a good browser viewer.



But as i said: i'm not anyhow affiliated with Autodesk (apart from beeing in their expert elite program but they don't pay me) and i'm sure OnShape is great as well - hope to find some time to try it a bit more.


---
**Jeff DeMaagd** *August 31, 2015 03:43*

Collaboration seems to be a feature that OnShape excels at. I'm not aware of anything else that is like that. Maybe some super high end stuff that I don't know about. You can make private projects in OnShape free tier, but it's very limited, I think two projects. If only they had a paid hobbyist tier because pro is prohibitive.


---
**Erik Scott** *August 31, 2015 03:49*

I agree pro is pro-hibitive (eh, see what I did there?). They just further restricted the private docs to 100mb, which is plainly ridiculous. You basically have to do everything publically. I wish there was a real open source solution. I just can't do FreeCAD. I've tried, I just can't...


---
**Eric Lien** *August 31, 2015 04:54*

**+Frank Helmschrott**  Let me know if these ones look like what you needed. I added ones based on your numbers above. They are in Solidworks/Step/STL. I also added them to Youmagine.



[https://drive.google.com/folderview?id=0B1rU7sHY9d8qQnZwT0ppOHVLQUk&usp=sharing](https://drive.google.com/folderview?id=0B1rU7sHY9d8qQnZwT0ppOHVLQUk&usp=sharing)


---
**Frank “Helmi” Helmschrott** *August 31, 2015 05:02*

Great, **+Eric Lien** - thank you!


---
**Bud Hammerton** *September 02, 2015 04:32*

Just so you know Fusion360 will allow direct import of Solidworks Part files. Assemblies also if everything is in the correct directories. I have made a lot of modifications to the Spider files for my own use and that is how I started out. I am a Fusion360 user.


---
**Eric Lien** *September 02, 2015 05:06*

**+Bud Hammerton** would you mind sharing your modifications? I really like to see how people improve the platform.


---
**Frank “Helmi” Helmschrott** *September 02, 2015 05:07*

**+Bud Hammerton** of course, the reason why we talked about this here is (if you follow all the way up) the fact that changing the dimensions on sketches to change the size of the hubs. This isn't possible with imported STEP files.


---
**Eric Lien** *September 02, 2015 05:22*

**+Frank Helmschrott** nice thing with the step file is you can save just the thread section, but extrude/cut off the barrel. Then just define new geometry for a new barrel in your favorite CAD package.


---
**Bud Hammerton** *September 02, 2015 12:02*

**+Eric Lien** I will share as soon as I know they are workable. Of course since they are Fusion360 files I can provide IGES, STEP, SMT or Fusion Archive versions. Even though Fusion360 can import SLDPRT and SLDASM, it can't directly export them. I am just in the process of printing now. Have the motion items on order and should be here this week. Will have to wait until next month for the frame material and other hardware (there sure are a large number of screws). 



**+Frank Helmschrott** My point was why import the Step when you can import the actual Solidworks source file. But I guess it really doesn't matter how you get it into Fusion360 since you will need to rebuild the sketches anyway. 



But directly to your point, once in Fusion360 you don't always need a sketch, you can manipulate the faces directly in either Sculpt or Model (I prefer Model since it acts on the entire face rather than sections) and you can Modify (pull or push) the face to the desired dimension. Could it be easier, maybe, but it is very workable.



Another reason to use Fusion360, is that licensing is free per year to entrepreneurs and academia per three years. The current version works with Windows 10.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/1ZZcDVaznwL) &mdash; content and formatting may not be reliable*
