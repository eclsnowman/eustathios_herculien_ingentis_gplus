---
layout: post
title: "Anyone have any thoughts on building a printer to be as quiet as possible?"
date: February 22, 2016 04:00
category: "Discussion"
author: Matthew Kelch
---
Anyone have any thoughts on building a printer to be as quiet as possible? I've always been envious of how quiet an Ultimaker 2 can be, what can I do to achieve that with a Eustathios?





**Matthew Kelch**

---
---
**Mike Miller** *February 22, 2016 04:31*

Cooling. e3d's want constant-on cooling with PLA....and the fan blowing across the controller  to keep things cool. 



So if you had good passive heatsinks on the motor drivers, and print ABS, you could get rid of a lot of noise. (Than and ensuring the reference voltage is <i>enough</i> to not skip steps and not a millivolt more)






---
**Zane Baird** *February 22, 2016 05:19*

Dampers on the stepper motors cut down on the noise a lot. It's made a big difference on my HercuLien


---
**Stephen Baird** *February 22, 2016 05:20*

Part of it is dampening the resonation of the motors and frame, which can be done by changing the resonant frequency of the frame to be low enough that you can't hear it, and part of it is changing the way the steppers work to decrease direct motor noise.



Drivers based on the Trinamic TMC2100 are surprisingly quiet.


---
**Michaël Memeteau** *February 22, 2016 08:06*

DC Servos + closed loop control will surely help to lower some few decibels. Take a look at the work **+Miguel Sánchez** is doing with some help of **+Mauro Manco**. 


---
**Miguel Sánchez** *February 22, 2016 08:13*

**+Michaël Memeteau** I can confirm the noise reduction when switching to closed-loop DC motors, but it could be even quieter once I manage to get the speed under control while moving. But part of the problem is that  I wanted to keep compatibility with step & direction signals for ease of use, but speed is only implicit on step signal and measuring it accurately while keeping the loop running is easier to say than to do. 


---
**Michaël Memeteau** *February 22, 2016 08:38*

So, still WIP I guess. Given your talent and motivation, I'm sure a solution isn't that far in the pipeline... ;)


---
**Øystein Krog** *February 22, 2016 10:51*

TMC2100 stepper drivers + IGUS bushings eliminate most noise from motion. I'm currently working on reducing fan noise. 


---
*Imported from [Google+](https://plus.google.com/+MatthewKelch/posts/7DPwnX932gT) &mdash; content and formatting may not be reliable*
