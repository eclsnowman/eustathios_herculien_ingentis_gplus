---
layout: post
title: "Hey All, The packages have been arriving fast and furious"
date: July 01, 2016 01:13
category: "Discussion"
author: Sean B
---
Hey All,



The packages have been arriving fast and furious.  I have a few questions I hope you can help me with.



1.  Are the shims necessary?  McMaster charges an arm and a leg when we only need a few for different sizes.  Also, what is the 1/8" aluminum spacer used for?

2.  Which carriage would you recommend?  I have printed the mini space invaders and the standard Spider V2 carriage.  I will be using the new E3D V6 with the cartridge Thermistor.  Additionally I will be running ball screws with Walter's Z mounts.







**Sean B**

---
---
**Eric Lien** *July 01, 2016 02:27*

The shims certainly are necessary but there are far cheaper options (I just use McMaster-Carr for convenience in the bill of materials). Even printing your shims will work, you just need to space the pulley off of the bearing because the pulley will rub on the bearing seal. I got the shims locally from a hardware store for under a dollar.



For the carriage it's really all preference, Walter did an amazing job on his but it also limits your X and Y travel.



I'm on my phone so I can't look it up the BOM easily. What sub component are you referencing for the 1/8" shim. That's why I have two different sheets in the bill of material, one that is a sum of all the parts for ordering and one that is all of the parts per sub assembly.﻿


---
**Sean B** *July 01, 2016 02:30*

Thanks for the info Eric, the 0.125 s a precision spacer for the z axis screw support.


---
**Eric Lien** *July 01, 2016 02:45*

Yes. You need those too. Again it's to keep the pulleys from rubbing on the bearing.


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/ctvpu1duyCv) &mdash; content and formatting may not be reliable*
