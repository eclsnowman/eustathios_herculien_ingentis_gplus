---
layout: post
title: "Guys, how many linear bearings are needed for the Eustathios?"
date: November 12, 2014 03:38
category: "Discussion"
author: Shachar Weis
---
Guys, how many linear bearings are needed for the Eustathios? The BOM says two 8mm and two 10mm, but that seems not enough? **+Eric Lien**, in your version, on the hotend carriage, are those linear bearings or plastic bushels? Do you have a BOM ? Thanks.





**Shachar Weis**

---
---
**Eric Lien** *November 12, 2014 04:02*

On the side rails there are qty:8 of a 10mm sdpsi self aligning bronze bushing.  On the carriage there are qty:4 of a 8mm sdpsi self aligning bronze bushing. For the z axis its two lm10luu (double length) linear bearings.﻿


---
**Eric Lien** *November 12, 2014 04:04*

[http://www.ultibots.com/10mm-bronze-bearing-4-pack/](http://www.ultibots.com/10mm-bronze-bearing-4-pack/)


---
**Eric Lien** *November 12, 2014 04:04*

[http://www.ultibots.com/8mm-bronze-bearing-4-pack/](http://www.ultibots.com/8mm-bronze-bearing-4-pack/)


---
**Eric Lien** *November 12, 2014 04:14*

[http://m.aliexpress.com/item/514938216.html](http://m.aliexpress.com/item/514938216.html)


---
**Eric Lien** *November 12, 2014 04:17*

This applies for my Eustathios variant. Jason Smith's uses lm8uu (or perhaps lm8luu) on the carriage.


---
**Shachar Weis** *November 12, 2014 04:46*

thanks !


---
**Eugene Lee** *November 12, 2014 07:03*

Thanks Eric, those are the best prices i've seen so far. But I was abit surprised to see an $8.75 handling fee. Can't remember the last time I was charged a handling fee. It's still slightly cheaper than buying directly from SDP-SI.


---
**Shachar Weis** *November 12, 2014 14:10*

$8 here, $10 there, every nut and bolt creeps up the cost and it adds up so quick.


---
**Eric Lien** *November 12, 2014 14:33*

This is a great place for cheap high quality bolts/nuts. I bought a large variety since they are so cheap. I will be set for years.



[http://www.trimcraftaviationrc.com](http://www.trimcraftaviationrc.com)


---
**Eugene Lee** *November 12, 2014 14:35*

"IMPORTANT NOTICE: TrimcraftAviationRC.com store temporarily closed"


---
**Eric Lien** *November 12, 2014 16:02*

**+Eugene Lee** thanks for the heads up. That didn't show up on the mobile view. I hate being forced to mobile sites. They usually suck.


---
**Shachar Weis** *November 12, 2014 16:16*

How important is the quality of the bearings? What do you think about these: [http://www.aliexpress.com/item/10-mm-bearing-kirksite-bearing-insert-bearing-with-housing-KP000-pillow-block-bearing/1121200956.html](http://www.aliexpress.com/item/10-mm-bearing-kirksite-bearing-insert-bearing-with-housing-KP000-pillow-block-bearing/1121200956.html)


---
**Eugene Lee** *November 12, 2014 16:43*

If you mean to use it on the sliders, those are pillow block bearings and are used to support rotating shafts. In the eustathios, the slider bearings need to support a rotating shaft and also allow linear motion. They also look very heavy. You will want this instead, though they're not much cheaper.



[http://www.aliexpress.com/item/5pcs-10-14-10mm-JDB-Graphite-Lubricating-Brass-Bearing-Bushing-Sleeve-Oilless/1421421455.html](http://www.aliexpress.com/item/5pcs-10-14-10mm-JDB-Graphite-Lubricating-Brass-Bearing-Bushing-Sleeve-Oilless/1421421455.html)



If you mean to use it as the support for the z-axis threaded rods, they will work with some modifications to the printed parts.


---
**Shachar Weis** *November 12, 2014 16:57*

I was thinking of using those to hold the 10mm rods (instead of the eight 3d printed holders).


---
**Eric Lien** *November 12, 2014 18:09*

**+Shachar Weis** the frame would require adjustment. The bearing you linked will not fit in the standard frame.


---
**Shachar Weis** *November 12, 2014 18:19*

Ah, good point.


---
**Mike Thornbury** *November 15, 2014 07:56*

Those prices are awesome, **+Eric Lien**  - I thought buying in China was cheap, but they have screws in lots of 25 that I pay the same price for 5! Going to make friends with them - just wish I had done so before this  week - just spent over $700 on hardware and electronics :(


---
**Eric Lien** *November 15, 2014 12:34*

**+Mike Thornbury** they are high quality to boot. I just wish they had a few more items.


---
**Mike Thornbury** *November 15, 2014 15:18*

If they have M3 and M5 screws and the odd nut and washer, they'll do me ;)


---
*Imported from [Google+](https://plus.google.com/117479393665221551027/posts/5e2kyxFuQMy) &mdash; content and formatting may not be reliable*
