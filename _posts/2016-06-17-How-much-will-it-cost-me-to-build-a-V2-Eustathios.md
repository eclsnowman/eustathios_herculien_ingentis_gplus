---
layout: post
title: "How much will it cost me to build a V2 Eustathios?"
date: June 17, 2016 19:46
category: "Discussion"
author: Grayson G
---
How much will it cost me to build a V2 Eustathios?





**Grayson G**

---
---
**Derek Schuetz** *June 17, 2016 20:00*

I'd estimate $1400

Controller LCD $170

Hotend $90

Frame/lead screws $300

Power supply $40

Heated pad $70

Aluminum bed and acrylic $200

Steppers $80

Relay $30

Misc $200


---
**Botio Kuo** *June 17, 2016 20:24*

Mine is over 1500... 


---
**jerryflyguy** *June 17, 2016 20:38*

**+Botio Kuo** ditto, in fact it's going to be all of $2K by the time I'm done.


---
**Ted Huntington** *June 18, 2016 16:42*

you can do it on the cheap with ebay parts for hot end, ramps+lcd, but even then around $800-$1000


---
**Botio Kuo** *June 19, 2016 07:02*

I have some extra printing parts and screws, washers, other parts for this printer. If you are interested on buying them, let me know.


---
*Imported from [Google+](https://plus.google.com/106318478009356847611/posts/5QxK6QDsGGk) &mdash; content and formatting may not be reliable*
