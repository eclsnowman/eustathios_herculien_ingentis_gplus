---
layout: post
title: "So looks like I did in fact spend the Weekend in the workshop trying to get my Eusthatios running"
date: March 19, 2017 21:19
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
So looks like I did in fact spend the Weekend in the workshop trying to get my Eusthatios running. Overall it hasn't all gone bad but at the end the frustration isn't really any lower than before ( see last comment under this one: [https://plus.google.com/+FrankHelmschrott/posts/gDhNkJBJM2J](https://plus.google.com/+FrankHelmschrott/posts/gDhNkJBJM2J)).



For the good parts: I fixed all firmware issues, got everything setup right again and basically the machine runs well. On second chance I'm much happier than I thought with the new carriage. It runs amazingly silent and smooth.



The bad part though is that I'm continueing to see problems with filament jamming, blobs comming out of the nozzle on slow speeds and small parts with PLA and even on filament that isn't as easily meltable as PLA is the problem is visible, Take a look at this light grey Moai that I printed in Colorfabb nGen. You can clearly see the irregular layer lines and given the fact that they run all around and seeing everything much worse with PLA I'm relatively sure that this is the same temperature problem that I kind of always had.



On the current carriage the airflow shouldn't be a real issue, I'm already running the original fan to have the performance that E3D says the hotend needs but I can't seem to get constantly good results.



After reading through [https://wiki.e3d-online.com/wiki/E3D-v6_Troubleshooting](https://wiki.e3d-online.com/wiki/E3D-v6_Troubleshooting) a bit (thanks **+Sanjay Mortimer**) I will try out some more tuning like thermal paste in the heat break and some remounting of everything but my hopes dissapear that I will get that E3D/bowden combination get to run great anytime

I'm already thinking about what a radical solution could be. Changing hotends? Trying the titan for direct extrusion (who wants to carry the weight?)? I don't know but it's frustrating. That printer runs so great but the extrusion seems to be a permanent problem and it seems to be clear temperature problem.



![images/db1d22a51dcea38b46bc88a6dd1436fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/db1d22a51dcea38b46bc88a6dd1436fa.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *March 19, 2017 21:24*

Can you share your current slicer profile settings, so I can look it over to see if anything stands out?


---
**Frank “Helmi” Helmschrott** *March 19, 2017 21:36*

right now I can only share some important facts from my head, I can export from Slic3r tomorrow or so if that helps.



This is for standard PLA 1.75mm (Orbitech but also tried others):



-  200°C first layer / 190 afterwards (the filament doesn't seem to go soo much lower without problems)

- 60° bed (no sticking issues)

- retraction set to 4mm@60mm/s (also tried lower which lowers the problem a bit on retraction intense prints but doesn't change the general swinging extrusion problems). Also tried higher and lower speeds which didn't really change anythign significantly

- speed set relatively slow (30-40 perims, 50-60 infill). Does seem to get a bit better on higher speeds which supports the thermal suspicion.



overall you can say that the lower the extrusion speed is, the higher is the problem. It even gets bigger when retracts come into the game. This points relatively clearly to a problem where the temp is rising up the heatbreak which is most likely heating up too far. Additionally the relatively long retracts needed with PLA due to the ugly oozing makes it even more hard because it pulls the soft filament juice up to the cold zone and pushes it back in. This then leads to not extracting enough some time and suddenly a big blob of cream gets spit out the nozzle.



We already discussed this some time ago with **+Sanjay Mortimer** - i had it with some cheap PETG back then and thought the material would be the cause or a broken E3D - it looks like it's kind of the same problem now again even though the symptoms look a bit different.


---
**Oliver Seiler** *March 19, 2017 21:59*

+1 for thermal paste in the heat brake. How stable is the temperature in the heat block? Are the PIDs well tuned?


---
**Oliver Seiler** *March 19, 2017 22:02*

I noted that the silicone covers E3D offers for the newer heat blocks do a good job isolating the hot end against changing cooling fan usage. This isn't goint to solve your problem I reckon, but more FYI.


---
**James Rivera** *March 19, 2017 22:03*

What **+Oliver Seiler** said: have you PID tuned both the hot end and the heat bed?


---
**Frank “Helmi” Helmschrott** *March 19, 2017 22:11*

**+Oliver Seiler** thanks, ihave thrown them away a while ago. i noticed they can cause serious issues when in some (maybe rare) ocassions they can move and fill up with filament and block everything. after removing my second big blob of filament that had surrounded my extruder i removed them again. 



apart from that the air on my extruder has no measurable impact on temperature. I tested all of that to exclude every problem I could think of. 


---
**Frank “Helmi” Helmschrott** *March 19, 2017 22:14*

and ye,s I have PID tuned and even switched to dead-time control which is much more stable. maybe I could still try to improve that. what is a good value of deviance of temp while printing? +/- 1C? how good does your setup get temps regulated when sinking or rising temps during print? does it over/undershoot much?



this could probably be an issue for the image pisted above but the pla/retraction problems are far more extreme and shouldn't suffer from smaller deviation. 


---
**Oliver Seiler** *March 19, 2017 22:33*

How much does the reported temperatur deviate during a print like in the photo above? It shouldn't be more than maybe 1-2 degrees after initial heat up.


---
**Oliver Seiler** *March 19, 2017 22:35*

BTW I have replaced the E3D heater cartridges with more powerful variants as they barely could keep the required temperatures reliably (for PETG and other filaments).




---
**Eric Lien** *March 19, 2017 23:45*

Try 210C for both first layer and all other layers. I think you are getting inconsistent extrusion from being too cold.


---
**Frank “Helmi” Helmschrott** *March 20, 2017 05:49*

**+Oliver Seiler** more than 40W? I believe E3D somewhen back in the days delivered a 30W with the hotend which should be a 40W again now. You have anything else? Mine is definitely a 40W (24V) and I've also trimmed it down a bit in power as it was too powerful to keep it stable in dead-time-mode. Heatup takes a bit longer but the stability shouldn't depend on the power - at least not at ~200-220° PLA range.



**+Eric Lien** I will though I fear that will be too high for several other things. But I will keep both in sight - the stability and maybe rise the temperature a bit overall.


---
**Eric Lien** *March 20, 2017 05:58*

**+Frank Helmschrott** this was at 55mm/s at 210C just the other day (on a new printer design). I think 190C is just too cold for an all metal hotend IMHO.

![images/58683df5001673940bf382c0cf5ce08b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/58683df5001673940bf382c0cf5ce08b.jpeg)


---
**Frank “Helmi” Helmschrott** *March 20, 2017 06:02*

Interesting. I'll definitely give that a try as soon as I find the time.



BTW: Where's the info about the new printer design? :)


---
**James Rivera** *March 20, 2017 06:32*

Just to add another data point, I have an all metal hot end and I print PLA at 210 just fine.


---
**Eric Lien** *March 20, 2017 12:23*

**+Frank Helmschrott** going to be at MRRF. I will be releasing information soon. It was something I designed along with a fellow G+ community member. More details soon I promise :)


---
**Oliver Seiler** *March 20, 2017 18:46*

**+Frank Helmschrott** the heater elements from E3D I have received were suppoosed to be 40W, but never relly had more than 25W (even the replacements they sent me after I asked for it). If your hotend can reach and keep the temperature reliably, then obviously that's not the problem.




---
**Roland Barenbrug** *March 22, 2017 17:14*

Can you also share the details of your hotend (E3D standard or Volcano), nozzle size especially.


---
**Frank “Helmi” Helmschrott** *March 22, 2017 17:16*

Just a plain default E3D v6. Not lite, not volcano, 0.4 default nozzle (though the problems happened with every other nozzle i tried identically). see my newer post for the solution I found


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/jKf2qfxjc7H) &mdash; content and formatting may not be reliable*
