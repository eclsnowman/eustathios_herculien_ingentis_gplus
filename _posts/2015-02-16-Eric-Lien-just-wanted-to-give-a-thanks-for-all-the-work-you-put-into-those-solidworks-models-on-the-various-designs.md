---
layout: post
title: "Eric Lien just wanted to give a 'thanks' for all the work you put into those solidworks models on the various designs"
date: February 16, 2015 00:30
category: "Discussion"
author: Seth Messer
---
**+Eric Lien** just wanted to give a 'thanks' for all the work you put into those solidworks models on the various designs. i finally got around to being able to view them. wow at the detail! wish i had the full blown app instead of just a viewer. this'll come in handy just being able to view/reference them now.





**Seth Messer**

---
---
**Eric Lien** *February 16, 2015 00:39*

**+Seth Messer**​ to view there is also the 3d PDF file in other formats as well as well as an edrawings version. I know not everyone has solidworks, I only have it because of work. But I hoped those other formats can help people with different CAD software have access too.


---
**Seth Messer** *February 16, 2015 00:43*

thanks much **+Eric Lien** 


---
**hon po** *February 16, 2015 02:06*

I imported the step file into FreeCAD and hide one part at a time. Better than any documentation on how to build one of these beast.


---
**Eric Lien** *February 16, 2015 02:27*

**+hon po** I agree the model is the best user manual. But I wish I had more time to do more proper documentation :)


---
**hon po** *February 16, 2015 04:00*

Wish you luck. With so much to do & to learn, who has appetite for doing documentation? Until then, I'm happy hiding one bearing ball at a time. ;)


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/FyAaSo6K7Yc) &mdash; content and formatting may not be reliable*
