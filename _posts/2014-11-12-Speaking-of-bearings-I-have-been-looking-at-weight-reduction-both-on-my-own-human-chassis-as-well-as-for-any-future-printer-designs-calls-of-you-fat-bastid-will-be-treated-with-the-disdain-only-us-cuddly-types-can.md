---
layout: post
title: "Speaking of bearings... I have been looking at weight reduction, both on my own (human) chassis as well as for any future printer designs (calls of 'you fat bastid' will be treated with the disdain only us cuddly types can"
date: November 12, 2014 10:17
category: "Deviations from Norm"
author: Mike Thornbury
---
Speaking of bearings...



I have been looking at weight reduction, both on my own (human) chassis as well as for any future printer designs (calls of 'you fat bastid' will be treated with the disdain only us cuddly types can muster!). 



I had a very interesting chat today with a technical specialist in the polymer bearing industry who assures me he has a linear bearing capable of working on carbon fibre tube, at only slightly less than double the friction of a ball-bearing. That is significant. 



They are selling them into robotics, space and aerospace engineering and have been getting great results. As far as wear goes, it is better than with un-anodised aluminium, worse than with chromed steel. With a wear rate of .0.00053 millimeter/km, that is also significant - compare that to a roller bearing on a hardened steel shaft of  0.00002 mm/km. 



At 82c each for a flanged bearing of 8*10*12mm, they represent good value, <i>if</i> I can get a carriage's weight significantly down. My 300mm carbon shafts weigh just 3g each. Compare that with the same in steel of 120g each. <i>Each!</i>



So, while there isn't 'no' wear, the wear is certainly very low (being 25 times that of a ball-bearing on a hardened steel shaft).



The other advantage is in the reduction in overall size and weight of the carriage and supports. Because the Y-axis mass is reduced, the central (hotend) mount can be made lighter and because the X-axis doesn't have to locate 240gm of wildly oscillating steel shafts, it can also be made lighter.



Considering the cost of the bearing and the cost of the carbon shaft over the potential speed gains through reduced inertia and overall reduction in wear and tear, I think it is certainly worth progressing.



While we were discussing carbon, he mentioned aluminium shafts as another option - I was initially dismissive, being that aluminium has the strength of an udon noodle in similar dimensions, but he assures me they are selling lots in comparable situations to carbon fibre. A hard-anodised 8mm aluminium shaft weighs around 40gm in a 300mm length at around $13/metre. The wear with their matched polymer bearing is comparable to chromed steel.



40gm vs 120gm and $4 each vs $3, but with significantly higher (for me) shipping costs is a no-brainer.



I also just bought a bench power supply that can show (in mA) the current draw - so that will be useful in determining which method is the most efficient, from a stepper/driver/controller perspective.



I will be trying out both CF and aluminium shafts as soon as I have cut out my new printer chassis. I am looking forward to seeing the results. 



 #FlatSli3DR   #Sli3DR   #Bearing  





**Mike Thornbury**

---
---
**Dale Dunn** *November 12, 2014 22:04*

Be sure to also compare rigidity of any tubing that will be moving a load. For example, carbon fiber is very strong, but in the same package not nearly as rigid as steel. Figure out what trade-offs you want to make to optimize your machine.


---
**Eric Lien** *November 12, 2014 23:50*

I have been down the ceramic coated 1/2" aluminum rod with composite bearing path before. I won't be going back. Look into **+Shauki Bagdadi**​​ post about static friction barrier (what he calls the slip/stick phenomenon).  



These composite bearings have a substation friction difference in motion versus at rest. Plus they have a nagging tendancy to stutter with sideload forces like those seen on our gantry.



If you can get it to work that would be great. But after 6 months of headaches on my corexy I won't be going back.﻿


---
**Tim Rastall** *November 13, 2014 04:19*

What **+Eric Lien**​ said. Anything that doesn't have a mechanical 'rolling' element is so much more sensitive to misalignment and this juddering slip/stick issue mentioned. Even the ubiquitous bronze bushings suffer from it. Given the incredibly low weight of the cf.  It might be worth considering a rolling wheel bearing. Perhaps even a custom hmwpe wheel bearing with a curved profile to match the shaft diameter.


---
**Mike Thornbury** *November 13, 2014 06:34*

These are all things I brought up with the tech specialist - he assures me this is exactly the environment they are selling into, with success - robotics, mostly - side load stutter, stiction, etc. Usually it is a function of being <i>too</i> smooth - there is a requirement for a surface roughness of 5 micron for this to work - my CF tubes are well in excess of that.



they have a grooved surface (crenellated in cross section) which seems to do away with the slip/stiction problem. Anyway, early days yet and as I speak there are samples on the way to play with.


---
*Imported from [Google+](https://plus.google.com/101708620681849403392/posts/goX3orwywox) &mdash; content and formatting may not be reliable*
