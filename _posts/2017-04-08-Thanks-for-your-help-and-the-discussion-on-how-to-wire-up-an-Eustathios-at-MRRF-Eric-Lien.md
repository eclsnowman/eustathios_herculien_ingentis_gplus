---
layout: post
title: "Thanks for your help and the discussion on how to wire up an Eustathios at MRRF Eric Lien ."
date: April 08, 2017 16:30
category: "Build Logs"
author: Pete LaDuke
---
Thanks for your help and the discussion on how to wire up an Eustathios at MRRF **+Eric Lien**.  As I mentioned I was going to put together a wiring diagram to confirm that I have my wiring layout understood and "accurate."   I'm almost to the wiring portion of my build.  Look forward to the feedback.

![images/606d69417d695ec6548e92a9456e8745.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/606d69417d695ec6548e92a9456e8745.jpeg)



**Pete LaDuke**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 16:42*

Well stepper motors have 4 leads so I'd start by nitpicking that. 

The wire gauges seem fine to me. I've done a lot worse over the same AWG you quote there. 



Also did we meet at Mrrf? I was the table on the other side of Eric. 


---
**Pete LaDuke** *April 08, 2017 17:06*

Thanks for nitpicking me Ray!  lol. I understand though.  Thanks for your input on the Wire Gauges.  And yes we did meet @ MRRF.  I also picked up a new Cohesion3D ReMix board from you.  So once I'm done with this project, I can start another one. ;) 


---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 17:10*

Yep, I knew I recognized you.  Cheers!


---
**Eric Lien** *April 08, 2017 17:27*

I will look over in more detail later. But my preference is run L into the SSR, then through the bed, then back to N. Wiring it the way you have shown means if the bed shorts, it could go through the frame or you to ground even if the SSR is not latched.



Also the wall wart for the PI would run on mains, not the DC. Unless you run the DC to a buck converter instead of the wall wart for DC24V to DC5V (2A minimum).



Lastly, try and make sure you connect the PSU ground to the frame. Otherwise in case of a short it would become a capacitor if there is no path to ground.


---
**Pete LaDuke** *April 08, 2017 18:08*

**+Eric Lien** good catch on the D.C. power source I had it incorrect. you are correct.  As far as the wiring of the SSR, I'm asking because I want to do this right, and am not sure how to best wire it up.  Also I was not even aware of a "DC Buck Converter"  Something like this? [http://amzn.to/2nPvDw1](http://amzn.to/2nPvDw1)  Attached is a new schematic.  Closer to correct I hope.


---
**Pete LaDuke** *April 09, 2017 01:44*

![images/c696e5bf76ef9eaef72eff257a5a8e6d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c696e5bf76ef9eaef72eff257a5a8e6d.jpeg)


---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2017 01:51*

While I have used the LM2596 regulator itself to power a Pi  (that's the regulator on the ReMix), I don't really trust those modules. I'd stick with the wall wart.  Eric's way of wiring it up to the IEC socket is really neat - the pi stays on all the time while the red switch would turn the machine off.  



Something looks very very wrong with how the SSR is shown.  Please look up more things regarding SSR AC wiring. The output from the controller would go to the top and the ac side wiring would go to the bottom only.  **+Eric Lien** please double check me on this. 



Don't forget the thermistors of the E3D and the bed, plus endstops, fans, lights, etc... 


---
**Pete LaDuke** *April 09, 2017 02:00*

Ray, I agree on how I have the SSR wired up here on the second version.  It just doesn't look right.  Which is exactly why I'm asking this question here.  Trying to be corrected and learn.... and do it right.  Wall Wart is totally an alternative solution.  Thanks for your input here Ray.



Your comments on thermistors, fans, lights etc.  is completely valid.  I'm just trying to follow "power" and components here.  Until I talked with Eric @ MRRF I wasn't exactly sure what the purpose of the Raspberry Pi was!  So I learning slowly but surely and trying to lay everything out so I can see what the road-map looks like.


---
**Eric Lien** *April 09, 2017 02:16*

The problem on the SSR is you have it as a dead short L to N in the second picture once the SSR is latched :)



Here is the path.



On the DC side of the SSR, hook up the controller bed outputs (+ and -, just two wires)



On the AC side:

L Into the SSR AC side (feed L from either out of the IEC socket, or jumpered off the PSU L input)

Then

Out of the SSR AC side into the bed

Then

Out of the bed to N (Either back at the IEC or on the PSU N input).




---
**Eric Lien** *April 09, 2017 02:18*

![images/d5cdce2c1635976b23a5a3709e1cbd62.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d5cdce2c1635976b23a5a3709e1cbd62.png)


---
**Eric Lien** *April 09, 2017 02:28*

Here is how I wire my IEC plug to get unswitched power to the wall wart so the PI never gets powered down, but switched power to the printer PSU so I can shut it down. I do this because the PI doesn't like having it's power randomly cut, it can corrupt the SD card.

![images/7a03f2f080a3f773574773e830f11778.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7a03f2f080a3f773574773e830f11778.png)


---
**Eric Lien** *April 09, 2017 02:33*

It is an ugly mess, but you can see how I did it on here on Herculien (I soldered it which I would not do again).

![images/7233d562eb37a794ef81d017894712b6.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7233d562eb37a794ef81d017894712b6.png)


---
**Eric Lien** *April 09, 2017 02:35*

Much easier to use spade connectors (note this one is just switched power. Not a combination of switched plus non-switched outputs). It is off an arcade I built for my kids a while back, but still shows how easy spade connectors work)

![images/27ed32fb5e09d312aa4bc3ea9b32a25d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/27ed32fb5e09d312aa4bc3ea9b32a25d.png)


---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2017 02:37*

I'm in the same boat, I soldered my first one but now have the spades to do everything properly going forward.  I think it's a rite of passage. 


---
**Eric Lien** *April 09, 2017 02:38*

Depending on the spade connector you use you will likely have to pinch them down a tiny bit since the IEC plugs spade male blades are smaller than conventional spade connectors. But a tiny bit of work with a pliers and the spade female from your standard DIY store fits like a glove.


---
**Eric Lien** *April 09, 2017 02:40*

BTW the red symbol here is just showing where the fuse sits (on the other side of course)

![images/5ce4c18952614642084d807133baa186.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5ce4c18952614642084d807133baa186.png)


---
**Eric Lien** *April 09, 2017 02:43*

And these squiggly green wires are not ground, they are just showing you jumpering from the unswitched power on the back of the IEC plug down to the switch.

![images/82b65dfc40f87d115e42faef63e121f7.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/82b65dfc40f87d115e42faef63e121f7.png)


---
**Eric Lien** *April 09, 2017 02:45*

You could just jumper one wire either L or N down through the switch. But if you run both through the two different poles of the switch the indicator light on the switch works. That way when you turn the switch on the red switch light indicates, and when you switch it off the red light goes off..


---
**Pete LaDuke** *April 09, 2017 02:45*

Thanks **+Eric Lien** while the wiring colors are confusing me a bit, I understand the layout, and it makes much more sense.  The image is my updated layout reflecting your input.  The power port layout for the PI maintaining power is 1) Awesome input, and 2) Something I'll have to digest tomorrow.   **+Ray Kholodovsky** **+Eric Lien** major thank you for your input / feedback and guidance today.

![images/560fa0a2684a725d2542f3c6826c0e3d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/560fa0a2684a725d2542f3c6826c0e3d.jpeg)


---
**Pete LaDuke** *April 09, 2017 02:51*

I missed all the feedback writing up my post, but yeah  I have a box of spades ready to go!  Just like high school making up speaker boxes, only attempting to make a printer this go around.  Thanks again guys.


---
*Imported from [Google+](https://plus.google.com/100658478011121421875/posts/DeXnvx3XU4t) &mdash; content and formatting may not be reliable*
