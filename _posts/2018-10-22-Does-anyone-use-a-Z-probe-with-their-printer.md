---
layout: post
title: "Does anyone use a Z probe with their printer?"
date: October 22, 2018 16:37
category: "Discussion"
author: Dennis P
---
Does anyone use a Z probe with their printer? Would you care to share your leveling results? 



I get a dip in the corner that I can not seem to get rid of. I am starting to think its mechanical or motion related. I get the same basic result with both 3point and 4point leveling. 



This is the Bed Visualizer map of a brand new 1/4 piece of Mic6, no glass. I threw a straight edge across it and I could not see any light leaking under it no matter which way I put it.  Short of spotting it on a surface plate, I would say its flat enough. I changed the differential IR sensor to a plain simple Omron lever switch like I used for the endstops for now.



I am using the Bilinear bed leveling in Marlin, I even went as far as 5x5 grid.   

I have turned the plate 90, 180 and 270 degrees. I flipped it over and did the same thing. I basically get the same pattern of hi's and low's. The iso-lines may move, but the same general shape. I would think that if it was switch related, I would seem lots of variation in the readings. 



How rigid is your Z platform? If you push on a corner, does the platform flex? If so, in which way? I am wondering if its related to flex or movement induced by the play in the LMU bearings or Z screw. 

 

![images/234b6d73bd7d0abed94c024287e2c240.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/234b6d73bd7d0abed94c024287e2c240.png)



**Dennis P**

---
---
**Dennis P** *October 28, 2018 18:44*

I have pretty much worked out that the low corners are due to play in the Z motion system. No I need to figure out how to live with it. 

The dip corresponds to the rotation of the platform arms which magnifies the play in the LMU10 bushing and the backlash in the Z screw. Its not much, but it seems that the corners dip down when the motion reverses.    

I have tried both the IR sensor and an Omron switch to eliminate the sensor. I basically get the same surface contours. 


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/7hFF3bNp8UH) &mdash; content and formatting may not be reliable*
