---
layout: post
title: "First movements"
date: June 24, 2015 21:09
category: "Show and Tell"
author: Daniel F
---
First movements



![images/e61bf5581e8a0c1647787decea363f70.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e61bf5581e8a0c1647787decea363f70.gif)
![images/04df5a1fcd8716d96f51bd62200e326b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/04df5a1fcd8716d96f51bd62200e326b.jpeg)
![images/417a6cd719a71a6cae073471fd3e2935.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/417a6cd719a71a6cae073471fd3e2935.jpeg)

**Daniel F**

---
---
**Daniel F** *June 24, 2015 21:17*

FInally I found some time to set up and wire the controller board. Its an Arduino Due with a RADDS shield and 1/128 drivers. Y axis runs smooth but there are some vibrations in X direction. I think this is caused through warping of the carriage. I tried to "allign" the carriage with a hot air gun but the angle might still not be exactly 90°. I like the steppers with 128 micro steps, they are really silent. the noise in x direction is mechanical vibration.


---
**Eric Lien** *June 24, 2015 21:41*

The clicking makes me think something cyclical, like a pulley or bearing. The robotdigg pulleys require a shim washer between the corner bearings and the pulley because the pulley has no shoulder on that face. If the shim is missing it could makes that noise.﻿


---
**Daniel F** *June 24, 2015 22:33*

That's what I thouhgt as well but I placed a shim between the pulleys and the bearings. I tried to locate the spot it comes from acoustically and I think it comes from the corners at the top where the bearings sit. I also noticed that the clicking noise does not occur on a fixed position of the carriage. There is a hysteresis, means, I move x until the click occurs, then I have to move it further back until the noise occurs again. I guess I need to spend more time to troubleshoot...


---
**Daniel F** *July 09, 2015 21:03*

figured out the cause of the clicking noise. Although I missed one shim between a bearing and a pulley, it was still there after I added the missing shim. I could acoustically locate it and it cam from one of the motors. It was the grub screw of the pulley that was loose. The motor shaft has a flat surface where the screw should sit. The screw was loose so each time the motor changed it's direction there was a bit of backlash and the screw hit the flat surface of the shaft. That was making the noise. Running the dial in gcode and hope to get smoother movements, there is still some binding and vibrations in y direction. I changed the 8mm rods for straighter ones but it didn't help. I think the holes int the carriage are not exactly perpendicular. Not sure if I can "align" that carriage.


---
**Eric Lien** *July 09, 2015 21:34*

That makes sense with the pulley set screw as the cause. To be honest I hate set screws as a retention method... but this stuff is all to small to use proper methods like keyways.



For alignment I think it should be possible. Unless it is majorly warped. The big issue is all the compound tolerances. Run the break-in code for a while, then take the carriage to each corner by hand and feel for binding and adjust if needed. Then rerun the break-in, then recheck for binding at corners and adjust. It takes a while and some patience... but when it is right you will know and things will move like butter.


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/guJX2R5Bic2) &mdash; content and formatting may not be reliable*
