---
layout: post
title: "What's wrong with this picture (video)? Bushing fell out mid-calibration cube - I thought the holes tight enough, they were light press fit..."
date: March 27, 2017 19:01
category: "Discussion"
author: Benjamin Liedblad
---
What's wrong with this picture (video)?



Bushing fell out mid-calibration cube - I thought the holes tight enough, they were light press fit... But I guess that's not enough.



Do you all glue in your bushings?

![images/bf6fb21307f190c003549dd803eb587f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bf6fb21307f190c003549dd803eb587f.jpeg)



**Benjamin Liedblad**

---
---
**Maxime Favre** *March 27, 2017 19:13*

Same happened to mines. Put 2 dots of glue on each and you'll be fine.


---
**Eric Lien** *March 27, 2017 19:32*

I do my carriage in abs, then soften the hole (which on my model is slightly undersized for this purpose) with acetone on a Qtip. I keep applying acetone on the Qtip until it softens enough to be pressed in. This helps glue it in, and also if you do both sides at once with the rod through the middle of the bushings while you do it, it makes for a perfect alignment.


---
**Pete LaDuke** *April 03, 2017 00:57*

**+Eric Lien** I'm in the middle of my build currently and found that when "gluing in/ acetone setting" the bushing into your components doing both sides at once with a rod thru them to setup the alignment made a huge difference in overall performance and reduction in efforts to move the components later.   This is a great Tip for anyone looking to start their build.  Doing them without a rod to align them allows for misalignment that you end up fighting afterwards.    


---
**Benjamin Liedblad** *April 05, 2017 04:00*

I had a longer response,  but G+ deleted my draft. :(



I'll keep it shorter this time. 



First off, thanks to everyone! I'm more than pleased with my build, and there are more improvements to make. 



Second, a few drops of superglue got be back up. 



Third, my initial prints were in PLA so I couldn't use the acetone trick, but I'm printing replacements in ABS on the new system now. 



My carriage slides with immeasurable friction, but I didn't spend enough time post-processing the belt tensioners. They bind a bit, so I can't move the carriage assembly with "pinkie force".



With the extra friction, I had to slow travel speed down to 60, print speed to 40 for reliable prints. 



Question: 

With properly acetone-aligned bushings, how fast should I be able to print ABS with a Bowden 1.75mm setup?



Thanks again. 






---
**Eric Lien** *April 05, 2017 04:08*

Print speed, that depends on part geometry. But I will say my travel moves are 150mm/s if that helps. I have printed at 200+, but quality is very poor at speeds like that. 80-100 is doable at reasonable quality with the right size and shape of parts. 60-80 yields good results on most parts (unless layer time becomes a problem for the part like overhangs and small features).


---
**Benjamin Liedblad** *April 05, 2017 21:32*

Thanks **+Eric Lien**, that helps. I almost have a complete set of ABS for replacements, hopefully I don't break it in the process of switching over. 



Aside from a little stringing, parts are perfect, but it would be nice to increase performance. 



Will increased speed help stringing, or should I look to retract more? 


---
**Eric Lien** *April 05, 2017 23:17*

At higher speeds you will likely need to increase temps because the dwell time is too low. If going up in temp doesn't help clean up retractions, then try some coast to end... and after that if there are still strings try longer retraction or play with retraction speeds (some plastics like slower retraction, some like it faster). I run ABS at 65+ retraction speeds using a bondtech.




---
*Imported from [Google+](https://plus.google.com/111192213763748051453/posts/LNvyq5ArX2u) &mdash; content and formatting may not be reliable*
