---
layout: post
title: "Sneak peek from the lab (#IGentus Mark 2)"
date: November 22, 2015 16:04
category: "Show and Tell"
author: Mike Miller
---
Sneak peek from the lab﻿ (#IGentus Mark 2)



![images/cac55582a081366726ba4a31f7cdb798.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cac55582a081366726ba4a31f7cdb798.jpeg)
![images/948ba6f990c58bbe0b6dc8204aba0ec6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/948ba6f990c58bbe0b6dc8204aba0ec6.jpeg)

**Mike Miller**

---
---
**Eric Lien** *November 22, 2015 17:18*

Looks great. Nice and compact.


---
**Mike Miller** *November 22, 2015 17:27*

It's pretty much going to require redesigning all of the kinematics. The nozzle won't reach the bed any longer, so I'll be using the z-stage from #Ingentilire, doubled...I'm a little concerned as the worm screws add more surface defects than the current belt based system...for all of it's warts, the belts make for perfect intra-layer alignment. 


---
**Seth Mott** *November 23, 2015 13:52*

Looks nice.  What kind of rods are those?


---
**Mike Miller** *November 23, 2015 14:05*

They're IGUS Ceramic coated aluminum rods...very light, kinda pricy. 


---
**Eric Lien** *November 23, 2015 14:12*

I have some qty:4 x 1/2" ceramic coated rods and a bunch of PBC Linear 1/2" Simplicity bearings from my old Corexy design. If anyone wants to try them out I would be happy to send them. I would just ask them cover shipping. 


---
**Mike Miller** *November 26, 2015 00:02*

I'll most likely swap rods and bronze bushings eventually. That'll lick the circle problem if it cant be tuned out with belt tension. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/YkLq8ySytii) &mdash; content and formatting may not be reliable*
