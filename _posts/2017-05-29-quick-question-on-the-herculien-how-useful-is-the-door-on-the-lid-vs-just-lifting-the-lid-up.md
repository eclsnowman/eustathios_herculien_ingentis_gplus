---
layout: post
title: "quick question on the herculien, how useful is the door on the lid vs just lifting the lid up?"
date: May 29, 2017 23:08
category: "Discussion"
author: James Ochs
---
quick question on the herculien, how useful is the door on the lid vs just lifting the lid up?





**James Ochs**

---
---
**Eric Lien** *May 29, 2017 23:37*

More often I open the door, versus lifting the full lid.


---
**James Ochs** *May 29, 2017 23:40*

Thanks!  I'll be putting the door in then ;)


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/CM75NyKDN77) &mdash; content and formatting may not be reliable*
