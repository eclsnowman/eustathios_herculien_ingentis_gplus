---
layout: post
title: "Robotdigg.com is a good provider. Their shipping cost is huge but the arrival date is very quick"
date: February 12, 2015 01:17
category: "Discussion"
author: Dat Chu
---
Robotdigg.com is a good provider. Their shipping cost is huge but the arrival date is very quick. Their packaging is very good as well. FedEx definitely didn't take good care of my box but hey, everything looks tiptop.



Robotdigg costs me $70 in freight and arrives as quick as $15 in freight of Misumi. Misumi packaging is crazy. Like crazy good. Considering this into the price that you are paying for Misumi to realize that their pricing is actually not bad at all.

![images/10efe174efdd4c71405f1962e3f79e84.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/10efe174efdd4c71405f1962e3f79e84.jpeg)



**Dat Chu**

---
---
**Eric Lien** *February 12, 2015 01:28*

Ahh, some nema 23's for something fun?


---
**Matt Miller** *February 12, 2015 01:55*

**+Derek Meng** is the best.  Shipping may be pricey, but their stuff is really high quality and the prices cannot be beat.  My go-to supplier for belt, pulleys and bearings!


---
**Dat Chu** *February 12, 2015 02:29*

Time to upgrade my ox from nema 17 to nema 23 **+Eric Lien**​. Brandon hooked me up with the plates. Might as well make the upgrade. 



Their price is definitely very very good. And the shipping speed for robotdigg is insane. All the way from China in a week. 


---
**Eric Lien** *February 12, 2015 03:31*

**+Dat Chu**​​ that is why I have them in my BOM. I only put in vendors I trust.



 [robotdigg.com](http://robotdigg.com) and [http://smw3d.com](http://smw3d.com) are two of the best out there. **+Brandon Satterfield**​​​ and **+Derek Meng**​​​ you guys rock.﻿


---
**Brandon Satterfield** *February 12, 2015 16:36*

You amazing guys make owning a shop fun, makes all the work totally worth while.


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/YHLm83EHr3J) &mdash; content and formatting may not be reliable*
