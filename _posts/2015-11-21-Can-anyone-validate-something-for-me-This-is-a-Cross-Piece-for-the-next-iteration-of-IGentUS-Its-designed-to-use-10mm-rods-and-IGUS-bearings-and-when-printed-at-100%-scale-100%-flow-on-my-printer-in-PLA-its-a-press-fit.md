---
layout: post
title: "Can anyone validate something for me? This is a Cross-Piece for the next iteration of IGentUS, It's designed to use 10mm rods and IGUS bearings and when printed at 100% scale, 100% flow on my printer in PLA, it's a press-fit"
date: November 21, 2015 17:07
category: "Show and Tell"
author: Mike Miller
---
Can anyone validate something for me? This is a Cross-Piece for the next iteration of IGentUS, It's designed to use 10mm rods and IGUS bearings and when printed at 100% scale, 100% flow on my printer in PLA, it's a press-fit affair. 



Comments and concerns are welcome!

[https://github.com/sniglet/Ingentilire/blob/master/Printable%20Parts/10mmCarrierIGUSV3.stl](https://github.com/sniglet/Ingentilire/blob/master/Printable%20Parts/10mmCarrierIGUSV3.stl)

![images/cc38bccea77391c39344cb37f1b45ded.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cc38bccea77391c39344cb37f1b45ded.jpeg)



**Mike Miller**

---
---
**Ishaan Gov** *November 21, 2015 17:37*

You kinda have to be careful when it comes to the press fit Igus bushings--they come slightly oversize, and when you push it in a block of aluminum (with h7 tolerance hole) they will be the correct  size; not sure what degree of success you may achieve with printed parts and slightly less accurate hole  dimensions 


---
**Ishaan Gov** *November 21, 2015 17:37*

That looks like a really nice print, though! 


---
**Mike Miller** *November 21, 2015 17:39*

Agreed, A previous iteration used a press-fit hole and I ran  10mm reamer though to get it to fit. They act as sliding surfaces in this use as the actual dimensional accuracy is maintained by the belts on the outside perimeter of the printer. When everything is adjusted correctly, there is no rotational force for the bearings to overcome, therefore, they just provide a sliding bearing surface, wherever they happen to meet the rod. 


---
**Mike Miller** *November 21, 2015 18:54*

Next question: Think I should greate a shoulder in the part so that the IGUS bushing is flush with the part? (**+Shauki B**, whadda ya think?)


---
**Sébastien Plante** *November 22, 2015 02:46*

you could also redrill to the right size.


---
**Mike Miller** *November 22, 2015 02:51*

Within reason. The outder dimension can be drilled to fit, you'd need something better than a drillbit to finish the bearing surface. 


---
**Mike Miller** *November 25, 2015 09:24*

You'll have to let us know how the 8mm rods work, I haven't seen anything authoritative WRT "Don't use less than X mm for a Y span due to deflection." I was surprised just how heavy 10mm steel rods were vs. the Igus aluminum ones. 


---
**Ishaan Gov** *November 25, 2015 18:10*

Yeah, when I was thinking about using aluminum shafts for an ingentis style gantry, I had to go with 10mm over a 380mm span for deflection reasons

In the end I just used some 8mm steel shafts to avoid having to ream a precise 12mm hole for bushings and just used some Misumi gear


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/dLFKRyfjSJ6) &mdash; content and formatting may not be reliable*
