---
layout: post
title: "Hello everybody, I review the HercuLien machine and it has all that I have ever wanted"
date: November 02, 2016 00:09
category: "Build Logs"
author: Wilmer Gaona
---
Hello everybody, 



I review the HercuLien machine and it has all that I have ever wanted. Now I have a mendel90 and it works in an excellent way but I decided to go further, For this reason, I am planning to build the HercuLien machine.



Moreover, I am at Mexico and I haven't found a supplier for the HFS5 20 x 80 parts. I  request a quote form Misumi but the cost for the freight is more than the parts itself.



Any ideas / links like from ebay or Aliexpress in order to get this parts?



Thanks 





**Wilmer Gaona**

---
---
**Eric Lien** *November 02, 2016 00:27*

Glad to hear you are excited about the build. I haven't personally used them them yet, but **+Makeralot**​ has frame kits for both HercuLien and Eustathios.



HercuLien: [makeralot.com - Aluminum Extrusion Kit for Herculien 2020 2080 V Slot : MAKERALOT, Maker Tools and Materials](https://www.makeralot.com/aluminum-extrusion-kit-for-herculien-2020-2080-v-slot-p215)/



Eustathios:[https://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207](https://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207)/



Has anyone in the group used them yet? If you have can you give your opinion on the quality?


---
**Ariel Yahni (UniKpty)** *November 02, 2016 00:52*

**+Eric Lien** Its very interesting that there is a seller providing a complete kit set for the build. What size build bed will fit here? 300x300?


---
**Eric Lien** *November 02, 2016 01:21*

HercuLien usable build area: 338x358x320



Eustathios Spider V2 usable build area: around 290x290x295. 


---
**Sean B** *November 02, 2016 02:42*

**+Eric Lien**​ I built my Eustathios with the extrusion from Makeralot.  Overall it was great!  All dimensions were perfect.  The only difference is that the end holes are 6mm instead of 5mm so the rubber feet need to be drilled out more and the aluminum bed frame mounts need modifications.


---
**Benjamin Liedblad** *November 02, 2016 04:49*

Am I the only one seeing $39 for the Eustathios frame in the link Eric sent?


---
**Sean B** *November 02, 2016 11:00*

**+Benjamin Liedblad** it's correct, but the shipping is pricey.  Still cheaper than Misumi


---
**Wilmer Gaona** *November 02, 2016 11:28*

Thanks for the info. The more information I have better decisions can be taken


---
*Imported from [Google+](https://plus.google.com/109742485522388793995/posts/QMVba91ApRx) &mdash; content and formatting may not be reliable*
