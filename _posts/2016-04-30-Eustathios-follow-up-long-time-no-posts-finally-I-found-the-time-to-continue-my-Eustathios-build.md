---
layout: post
title: "Eustathios follow up long time no posts--finally I found the time to continue my Eustathios build"
date: April 30, 2016 23:09
category: "Show and Tell"
author: Daniel F
---
Eustathios follow up

long time no posts--finally I found the time to continue my Eustathios build. Added a display and encoder to the RADDS board. Installed 450W 230V heater with SSR. Tried a setup with short bowden, seems to work ok but I need to figure out the propper retract value. Will post some videoes soon



![images/2eb4203390282527932bdec4cc758b8b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2eb4203390282527932bdec4cc758b8b.jpeg)
![images/05e66364b77ca06accf161fab1d0f3be.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/05e66364b77ca06accf161fab1d0f3be.jpeg)
![images/5dd4702db3ce50ceed54a9ff4aef75e6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5dd4702db3ce50ceed54a9ff4aef75e6.jpeg)
![images/d9c2ae348f8002d9bd31ab580e04f77e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d9c2ae348f8002d9bd31ab580e04f77e.jpeg)
![images/f6e391d71aa16296a92212e8b4d3e41c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f6e391d71aa16296a92212e8b4d3e41c.jpeg)
![images/24cbd5082793d13af4df00cd1bd6a22d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/24cbd5082793d13af4df00cd1bd6a22d.jpeg)
![images/0c6e88e420feac6da03bc44701928ba3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0c6e88e420feac6da03bc44701928ba3.jpeg)
![images/1ff3757cb8bb0556ac3ab7d7e44a2bb1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1ff3757cb8bb0556ac3ab7d7e44a2bb1.jpeg)
![images/40eb18c86334fddf2186c624d07a8bb7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/40eb18c86334fddf2186c624d07a8bb7.jpeg)
![images/1fb4787384fa026fe8f0d40e9c185e7a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1fb4787384fa026fe8f0d40e9c185e7a.jpeg)

**Daniel F**

---


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/W3yXQnzVAgP) &mdash; content and formatting may not be reliable*
