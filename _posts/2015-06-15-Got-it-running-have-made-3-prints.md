---
layout: post
title: "Got it running have made 3 prints"
date: June 15, 2015 19:39
category: "Show and Tell"
author: E. Wadsager
---
Got it running have made 3 prints. I think the result is pretty good. 



![images/919b45284ca187d0888de4bf1503b277.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/919b45284ca187d0888de4bf1503b277.jpeg)
![images/e002ed554091da24d34819235ace488c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e002ed554091da24d34819235ace488c.jpeg)
![images/2550e9b64382e9db12795a5e7fda7b0a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2550e9b64382e9db12795a5e7fda7b0a.jpeg)

**E. Wadsager**

---
---
**Jim Wilson** *June 15, 2015 19:40*

Gotta love progression


---
**Eric Lien** *June 15, 2015 21:20*

Great work. Love the creativity in this build.


---
**Richard Teske** *June 16, 2015 07:50*

Do you have an MK3 Heatbed?


---
**E. Wadsager** *June 16, 2015 14:49*

yes it is. but I consider upgrading to something bigger.


---
*Imported from [Google+](https://plus.google.com/103157635215674778495/posts/bkyKiKyyKaX) &mdash; content and formatting may not be reliable*
