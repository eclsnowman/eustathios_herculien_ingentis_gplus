---
layout: post
title: "So I guess this means I'm building a Herculien"
date: April 24, 2015 04:32
category: "Show and Tell"
author: Gunnar Meyers
---
So I guess this means I'm building a Herculien. I only have the extruded left to print out then I think I'm done.   I also just placed the order for all the smooth rods and lead screws from Igus. I can't wait to get started and place my order for Misumi tomorrow.   p.s. I think I will also get the smoothie board 5 with E3d light hotends. 

![images/8eadc1d11076610029821cef800e10f1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8eadc1d11076610029821cef800e10f1.jpeg)



**Gunnar Meyers**

---
---
**Vaughan Lundin** *April 24, 2015 06:22*

Looking good, let us see the end product when you finished


---
**Eric Lien** *April 24, 2015 12:05*

Based on the picture you need a bit more than the extruder. ( [https://github.com/eclsnowman/HercuLien/blob/master/Photos/Picture%20of%20all%20Printed%20Parts.jpg](https://github.com/eclsnowman/HercuLien/blob/master/Photos/Picture%20of%20all%20Printed%20Parts.jpg))



But congratulations. I am always glad to see another one enter the family ;)


---
**Gunnar Meyers** *April 24, 2015 12:33*

I thought it felt like not enough parts. I guess I was a little confused with the Bom. Thank you for the photo!


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/J96zdVvFezT) &mdash; content and formatting may not be reliable*
