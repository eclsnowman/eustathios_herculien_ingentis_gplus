---
layout: post
title: "Decided to test some XT material from ColorFabb in my search for the best material for printing the parts needed for the Herculien"
date: September 10, 2015 08:31
category: "Show and Tell"
author: Eirikur Sigbjörnsson
---
Decided to test some XT material from ColorFabb in my search for the best material for printing the parts needed for the Herculien. This is actually a great material and a bit better than PETG. I printed these on an Ultimaker2 with Simply3d. I used the ABS profile as a base, changed the nozzle heat to 250°c, the bed to 70°c and turned the fans on to 50%, infill at30%. No other ajustments. Everything printed out great. There is similar material shrinkage as is with ABS. The black one is a CF20 carbon fibre (I think the nozzle is still ok btw). The next two are green and transparent XT (not so much transparent with the infill :)). The blue one is a PETG for comparison. The Carbon Fibre part looks best of the lot, with a slight sandpaper feel to the surface.



![images/460ce89b5774678c0eac5ff6e77bc11b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/460ce89b5774678c0eac5ff6e77bc11b.jpeg)
![images/7ae919a5d9cde37041a627e0c824c93c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7ae919a5d9cde37041a627e0c824c93c.jpeg)

**Eirikur Sigbjörnsson**

---
---
**Michaël Memeteau** *September 10, 2015 08:38*

Nice prints, which speed and machine?


---
**Eirikur Sigbjörnsson** *September 10, 2015 08:48*

2500mm/min on a Ultimaker2


---
**Hakan Evirgen** *September 10, 2015 08:52*

can you send me all your settings please? I have a bq prusa i3 Hephestos and have difficulties printing with XT-CF20. Do you also get much residue around the nozzle?


---
**Øystein Krog** *September 10, 2015 09:19*

If shrinkage is similiar to ABS, that is a big negative when comparing with PETG.


---
**Eirikur Sigbjörnsson** *September 10, 2015 09:29*

on closer examination the shrinkage is about 1 - 1.5%


---
**Eirikur Sigbjörnsson** *September 10, 2015 09:43*

As to the setting I only did this one print of the CF20 since I don't want to ruin the brass nozzle of the Ultimaker. I got no residue around the nozzle. Your print speed might be to high? Apart from the settings I changed (as I listed above) I used the standard ABS settings in Simply3d. Layer height of 0,15mm, retraction was 4mm. Im sure with fine tuning of the settings it's possible to get even better prints.


---
**Øystein Krog** *September 10, 2015 09:45*

Good call, if you haven't seen the latest E3D article you should read it. A bit crazy how quickly brass and even normal steel nozzles wear down with abrasive materials! 

I have to say that the carbon fibre part looks like it has really nice surface texture!


---
**Eirikur Sigbjörnsson** *September 10, 2015 09:53*

I need to look at that since I plan to use E3D and use stainless steel nozzles for materials like the CF20. The matte appearance of the CF20 hides the print layers perfectly so the print looks better than anything else I've tried.


---
**Øystein Krog** *September 10, 2015 09:54*

Their analysis is very good, and they just launched hardened steel nozzles:

[http://e3d-online.com/is-carbon-killing-your-nozzle](http://e3d-online.com/is-carbon-killing-your-nozzle)


---
**Hakan Evirgen** *September 10, 2015 10:21*

I have already ordered from E3D the hardened nozzle together with the Chimera. But first I need to make my Delta working again.



On the prusa i3 Hephestos the nozzle is steel and I was told that it is immune to abrasion. But it prints with problems. Still need to figure out. I was told that this is normal but after seeing the print here I do not think that this is normal.


---
**Øystein Krog** *September 10, 2015 10:22*

E3D's analysis shows that atleast their steel nozzles are <i>not</i> immune.


---
**Eirikur Sigbjörnsson** *September 10, 2015 10:24*

Yeah I need to order few of those when I order my E3Dv6s


---
**Hakan Evirgen** *September 10, 2015 11:23*

Yes I read that from E3D. Good that I did not print so much yet on my Hephestos.



Anyway XT-CF20 is difficult to handle.


---
**Eric Lien** *September 10, 2015 16:43*

**+Eirikur Sigbjörnsson**  Those are all such clean prints. Very well done. I will take 4 of the carbon reinforced ones please :)



Kidding aside I look forward to your printer once completed. There have been several additions to the HercuLien/Eustathios/Ingentis family the last few  weeks. I still get excited every time. I just love seeing these things come to life and improve with everyone's input and ideas.


---
**Eirikur Sigbjörnsson** *September 10, 2015 18:18*

haha that would ruin the Ultimaker nozzle :)

but I might be tempted to print all the parts in this material if my incoming cobblebot can handle it. The part looks so good. It was much harder to remove the support on this piece than the others, there is an obvious strength  difference. I printed another one with ColorFabb's wood filament today and varied the nozzle heat a bit so different layers  have different colors (started with 250°c (very dark and wood burning smell all over the place), then went down to 220°c and ended on 210°c. Great material also and definitely something I will use more off in the future.


---
**Zane Baird** *September 12, 2015 20:42*

**+Eric Lien** I printed the corner brackets of my Herculien in the carbon fiber reinforced PETG from Colorfabb. I didn't think about it when I was doing it, but it turned my 0.4mm nozzle into a +0.6mm nozzle... I have to say, I'm very happy with their rigidity and the the print quality I got from them. I'm been waiting to use the rest of my CF20 roll until I find a 0.6mm hardened steel nozzle for the Volcano. I was a bit upset to see that they only went as small as 0.8mm for the hardened nozzles on the Volcano. Perhaps when I finally find a nozzle I'll reprint all of the mechanical parts for the Herculien in that material


---
**Gústav K Gústavsson** *September 13, 2015 14:56*

I really have to agree with **+Eirikur Sigbjörnsson** on the quality of the carbon fibre print. We have been playing with the Ultimaker 2 printer at work and trying different settings and filament (and modifications) and had great print from various settings/filament. But the quality of the carbon fiber print beats it all, I have to send in some more picture of that, **+Eirikur Sigbjörnsson** photos don't do them justice (sorry Eiríkur). Don't think we fiddled much with the setting, he hit it right on (I think, correct me if I'm wrong) had no major problems with it at least.


---
**Mike Miller** *September 13, 2015 19:31*

What's the layer height? That Carbon Fiber part doesn't even seem 3D printed!


---
**Eirikur Sigbjörnsson** *September 13, 2015 20:09*

0.15, and well it was 3d printed like the rest, but you can't see the layers which should say something about the material. If you can master it you can get great, strong parts out of it


---
*Imported from [Google+](https://plus.google.com/118262882256504121671/posts/4Af4GjbjZM8) &mdash; content and formatting may not be reliable*
