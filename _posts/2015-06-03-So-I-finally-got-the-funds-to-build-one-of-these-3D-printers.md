---
layout: post
title: "So I finally got the funds to build one of these 3D printers"
date: June 03, 2015 21:52
category: "Discussion"
author: Brandon Cramer
---
So I finally got the funds to build one of these 3D printers. I'm having a hard time choosing which one. Either the HercuLien or the Eustathios. 



Should I just go for the HercuLien or is there a reason I should go with the Eustathios? 



Huge props to whoever created these instructions. I don't remember seeing them a while back when I was looking to build the HercuLien.





**Brandon Cramer**

---
---
**Derek Schuetz** *June 03, 2015 22:38*

I have built both the euth and the herc, I'd build the euth 10 times. The herc is an awesome printer but requires a lot of fabrication which is a pain and can be quite time consuming.


---
**Brandon Cramer** *June 03, 2015 23:24*

Is this the correct part number MTSBRA12-410-S56-Q8 for the Eustathios? I can't find it on the misumi website.


---
**Eric Lien** *June 03, 2015 23:43*

I would say unless your goal is large ABS parts, then Eustathios Spider V2 is a better option due to cost and ease of build. 



The instructions for HercuLien were done by **+Daniel Salinas**​ , he is awesome.


---
**Brandon Cramer** *June 03, 2015 23:55*

I think for now, I will stick with the Eustathios. Can someone verify the part number above for the Lead Screws? I'd like to order this tonight. 


---
**Daniel Salinas** *June 04, 2015 00:13*

I have to agree.  I'm pretty badass.  LOL


---
**Brandon Cramer** *June 04, 2015 00:28*

Those are some amazing instructions! Those are bad ass! Lol


---
**Daniel Salinas** *June 04, 2015 00:30*

I can actually say that I built the herculien 4 times to get those right. I love the printer and if I had to choose again I'd build it again. But I have issues where my office is very drafty so I have to have a fully encloses printer otherwise I fight with part delamination


---
**Eric Lien** *June 04, 2015 00:31*

**+Brandon Cramer** they keep changing the bloody leadscrew number on me. Ahhhhh



I will try to find the correct one tonight.


---
**Eric Lien** *June 04, 2015 00:41*

**+Brandon Cramer**​ I think you are looking at V1, I would recommend V2



Github for V2:

[https://github.com/eclsnowman/Eustathios-Spider-V2](https://github.com/eclsnowman/Eustathios-Spider-V2)



Lead screw:

[http://us.misumi-ec.com/vona2/detail/110302642010/?CategorySpec=00000029094%3A%3A425&PNSearch=MTSBRB12-425-S46-Q8-C3-J0&SeriesSpec=S%3A%3A46%09Q%3A%3A8%09C%3A%3A3%09J%3A%3A0&HissuCode=MTSBRB12-%5B80-1000%2F1%5D-S%5B5-63%2F1%5D-Q7%2C8%2C9-C%5B3-60%2F1%5D-J%5B0-58%2F1%5D](http://us.misumi-ec.com/vona2/detail/110302642010/?CategorySpec=00000029094%3A%3A425&PNSearch=MTSBRB12-425-S46-Q8-C3-J0&SeriesSpec=S%3A%3A46%09Q%3A%3A8%09C%3A%3A3%09J%3A%3A0&HissuCode=MTSBRB12-%5B80-1000%2F1%5D-S%5B5-63%2F1%5D-Q7%2C8%2C9-C%5B3-60%2F1%5D-J%5B0-58%2F1%5D)﻿


---
**Eric Lien** *June 04, 2015 00:42*

Here is the V2 BOM: [https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Documentation/BOM](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Documentation/BOM)


---
**Eric Lien** *June 04, 2015 00:44*

Here are the required printed parts: [https://raw.githubusercontent.com/eclsnowman/Eustathios-Spider-V2/master/Documentation/Pictures/Printed_Parts(Black_With_Hercustruder_Extruder).jpg](https://raw.githubusercontent.com/eclsnowman/Eustathios-Spider-V2/master/Documentation/Pictures/Printed_Parts(Black_With_Hercustruder_Extruder).jpg)


---
**Eric Lien** *June 04, 2015 00:48*

Eustathios is easier, and I love the printer. But if like Highlander... There could be only ONE, then myself I would choose HercuLien.


---
**Brandon Cramer** *June 04, 2015 01:53*

**+Eric Lien**   Thanks for pointing me to V2. It's a good thing that part number didn't work. I might have ordered it. 


---
**Øystein Krog** *June 04, 2015 08:24*

Has anyone enclosed their Eustathios?

I want to print semi-large ABS items, but I don't need the extreme size of the Herculien.


---
**Chris Brent** *June 04, 2015 15:51*

What's the difference in parts cost between the two? I'd never thought that it might be significant.


---
**Brandon Cramer** *June 04, 2015 15:57*

This part is not coming up when I search for it on Misumi:



HFSB5-2020-355-AH177_5

20x20x562mm Extruded Aluminum W/ Holes



The part# and description don't seem to match. 


---
**Vic Catalasan** *June 04, 2015 16:19*

The instruction on the Herculien is definitely a time saver when putting things together. Thanks for putting that together.


---
**Eric Lien** *June 04, 2015 16:50*

should be HFSB5-2020-355-AH177.5 



[http://us.misumi-ec.com/vona2/detail/110302683830/?OptionSpec=AH%3A%3A177.5&PNSearch=HFSB5-2020-355-AH177.5&SeriesSpec=D001%3A%3A355&HissuCode=HFSB5-2020-%5B50-1800%2F0.5%5D&Keyword=HFSB5-2020-355-AH177.5](http://us.misumi-ec.com/vona2/detail/110302683830/?OptionSpec=AH%3A%3A177.5&PNSearch=HFSB5-2020-355-AH177.5&SeriesSpec=D001%3A%3A355&HissuCode=HFSB5-2020-%5B50-1800%2F0.5%5D&Keyword=HFSB5-2020-355-AH177.5)


---
**Eric Lien** *June 04, 2015 16:52*

it is an extrusion 355 long with a cross hole at 177.5 ( i.e. half way).


---
**Eric Lien** *June 04, 2015 16:55*

Here is a google docs version of the V2 BOM people have been working on: [https://docs.google.com/spreadsheets/d/1ATz5AoIUtASowBtlXsOb8FVt8YLd0A-wu1EWHHHPDnA/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1ATz5AoIUtASowBtlXsOb8FVt8YLd0A-wu1EWHHHPDnA/edit?usp=sharing)


---
**Brandon Cramer** *June 04, 2015 17:10*

I'm already committed to this excel spreadsheet. As long as everything is pretty much the same I will stick with it. 



I do have  a question with McMaster. The total is coming up to $224.67 which I know is way overkill with extra parts since most of these parts come in packs of 50 or 100. Should I just order it, or is there really an alternative?


---
**Eric Lien** *June 04, 2015 17:21*

[http://www.trimcraftaviationrc.com/](http://www.trimcraftaviationrc.com/)



much much better prices on bolts, all stainless, better qty sizes. They don't have all the bolts... but they have 90% of them.


---
**Brandon Cramer** *June 04, 2015 22:01*

Well I guess it's too late to back out now. I ordered almost everything from Bolt Depot instead of McMaster-Carr. 



Bolt Depot $40.33

McMaster-Call $62.50 plus whatever shipping is

Misumi $483.90



I need to get busy and order the rest of the items now. That was a drooling task, but I couldn't see spending the $200+ at McMaster-Carr.


---
**Brandon Cramer** *June 05, 2015 17:13*

This Aluminum plate from Discount Steel. Is this the correct one?



ASTM B209-10 5052-H32 Aluminum Plate The closest I can get to 400mm x 360mm is 15.750 x 14.188 inches. It cost $27.07. 



Is there a piece of glass I should put on top of it when it comes time to print?


---
**Eric Lien** *June 05, 2015 19:09*

**+Brandon Cramer** yes, I use window glass of the same size. Also for the aluminum... You can probably find a local vendor and save on shipping. I just have used discount steel for years because they are in the twin cities.


---
**Brandon Cramer** *June 05, 2015 22:31*

**+Eric Lien** I think I have 95% of it ordered. :) So on the Aluminum Plate, glass print surface, and acrylic. The DXF file, doesn't have dimensions on it does it? I'm thinking I will have the acrylic custom cut. Are you just using some red acrylic below the black to make the cutouts stand out? 



Is the glass a certain thickness? Is it true glass, like a window that could shatter into a bunch of fun pieces? Also, is this something I should have two of? I imagine placing the blue painters tape on it and then loading it onto the printer. I no longer want to know how much this thing cost. lol I'm not keeping track. :)


---
**Eric Lien** *June 06, 2015 00:31*

**+Brandon Cramer** the trick is not to get tempered glass, tempered glass is in tension by design... But when heated causes forces that often lead to cracked glass. I have Boro glass... But never use it. I run window glass on both my printers now with no issue other than sharp edges which can be easily knocked down with crocus cloth or very very fine emry cloth. To hold it to the aluminum I use small printed ABS C-clips. Tape works fine also to hold it down.



The red is for effect on the model... But could be milled in and use that craft store pourable faux stain glass resin in the cavity for contrast.



The dxfs I think have dimensions... But I cannot remember.


---
**Brandon Cramer** *June 06, 2015 16:09*

**+Eric Lien** are you printing right on the glass? 



 What are the actual dimensions the Spider V2 will print?



Kind of late to ask this question, how much should I expect to spend on this Spider V2 printer build? I just want to see if I'm in the ballpark.






---
**Eric Lien** *June 06, 2015 16:56*

The usable area depends on the part cooling duct you use. Estimated area is 285x285x295 I think (double check the readme on the github).



I use 3 wet coats of hairspray on the glass. Works awesome.



Cost I think is around $850 I think, but others can confirm.﻿


---
**Derek Schuetz** *June 06, 2015 18:46*

Much more then $850 to build due to shipping cost and if you don't have tools to cut acrylic and aluminum. Misumi order alone is close to 500. Hotend is 100 electronics is about 250. Miscellaneous parts like wire, crimps, steppers, bushings is a couple of hundred.


---
**Brandon Cramer** *June 06, 2015 23:42*

I'm close to $1500 without the acrylic, glass, and aluminum plate. 


---
**Eric Lien** *June 07, 2015 01:48*

**+Brandon Cramer** that seems like more than I spent... But maybe I am just remembering how much I told my wife it cost :)


---
**Derek Schuetz** *June 07, 2015 02:43*

1500 sounds about right


---
**Brandon Cramer** *June 07, 2015 03:27*

**+Eric Lien** Good one! I've been there! :) As long as it doesn't cost more to build this Spider V2 than the HercuLein.... I'm good! 


---
**Brandon Cramer** *June 08, 2015 16:11*

I got a quote back on getting the acrylic cut. Does this look about right for dimensions?



[https://www.dropbox.com/s/kjem02dvbllkf0s/SpiderV2.JPG?dl=0](https://www.dropbox.com/s/kjem02dvbllkf0s/SpiderV2.JPG?dl=0)


---
**Eric Lien** *June 08, 2015 16:27*

**+Brandon Cramer** Yes those dimensions look good. the frame outside dimension is 465mm so that is 18.307... inches.


---
**Brandon Cramer** *June 08, 2015 19:27*

**+Eric Lien** What is the thickness of the glass? I'm thinking I will just order two pieces of this unless you think I shouldn't use Boro glass. 



Clear Borofloat Glass, Edges Cut & Swiped 360mm (14.17”) X 345mm (13.58”)



 



Qty. 2                   3.3mm thick                                                     $28.50/each



Qty. 2                   5mm thick                                                        $39.00/each



Qty. 2                   6.5mm thick                                                     $57.00/each


---
**Eric Lien** *June 08, 2015 19:59*

**+Brandon Cramer** 3.3 should be fine. Those are good prices on the glass. Plus less likely to cut yourself than window glass if the soften the edges.


---
**Brandon Cramer** *June 08, 2015 21:14*

**+Eric Lien** I can't wait to get all the parts for this and get started using it. 



The bottom acrylic plate. Have you actually done this with the logo? I dont think I realized that the center piece would be free floating. Seem like this will be hard to keep clean. 



5052-H32 ALUM .125 HEAT SPREADER $88.00 with shipping. Is this the correct piece? :)


---
**Eric Lien** *June 08, 2015 22:43*

**+Brandon Cramer** the logo is more for looks on the model. The logo is only recessed, not a full cut.



On the plate I had a CNC friend cut it. So to be honest I don't know market rate for it. You could always do what **+Walter Hsiao**​ did, it is just square then and should be much cheaper. Then you could countersink the holes yourself for the bolts so the glass can fit over them.


---
**Brandon Cramer** *June 15, 2015 23:55*

Is this an installation guide on how to put all these parts together for the Eustathious Spider V2? 



Misumi needs to hurry up and ship my order. :)


---
**Eric Lien** *June 16, 2015 00:39*

**+Brandon Cramer** sorry, just for the HercuLien currently. **+Seth Messer**​ is working on one, and I think **+Ben Delarre**​ might have taken some pics and notes. When you build yours if you could take some notes as well that would be great. Assembly is pretty easy. Others can attest: "The hardest part is remembering the t-nuts in the right spots and the aligning everything." :)


---
**Brandon Cramer** *June 16, 2015 04:16*

So I take it that the Spider V2 is being built from looking at others building theirs or just asking questions? I don't mind documenting the process of putting mine together but I'd like to make sure I'm doing it correctly. I think I will just called it Spider V2... :)


---
**Eric Lien** *June 16, 2015 04:34*

**+Brandon Cramer** pretty much yes for now. But the group (myself included) should be able to walk you through. Also getting a model viewer (like edrawing viewer) to roll around the 3d model will get you 90% of the way there. Any help documenting is greatly appreciated. 


---
**Derek Schuetz** *June 16, 2015 17:08*

**+Brandon Cramer** it's just like assembling ikea furniture all you have is an allen key and pictures...﻿


---
**Brandon Cramer** *June 18, 2015 18:55*

I got the aluminum plate today! :) I should be getting the acrylic today as well. On Monday I will get my Misumi order. 



This raspberry pi. Is it just used for OctoPrint? I don't think OctoPrint and I are meant for each other. I never can get it working like I want. I already have a dedicated laptop for my 3D printer. 



**+Eric Lien** What was the program are you using for communicating with your 3D printers. I saw it a while back but I can't remember the name of it. I remember it wasn't free. :)


---
**Eric Lien** *June 18, 2015 19:12*

I slice with Simplify3D, but as a G-code sender I use Octoprint again. I had switched to a microcenter windows tablet for a while... but the newer versions of octoprint with the raspberry PI2 are a match made in heaven. 


---
**Brandon Cramer** *June 18, 2015 20:23*

If I don't mind just keeping the dedicated laptop for printing, would it make sense to just use Simplify3D over Repetier Host or even octoprint? I like receiving the notifications every hour or at the beginning or end of the print jobs. That is a requirement for me. Getting this setup in octoprint almost seems impossible for me. 


---
**Eric Lien** *June 18, 2015 21:25*

No gcode sender is notably better than any other IMHO. So Repetier, Simplify3d, Octoprint should all work equally well. I like octoprint because since it is a single purpose OS and computer I don't need to worry about a system update happening mid print :)



FYI: For octoprint there is a plugin for pushbullet that will send you a notification that the print has finished and even include a picture from the webcam.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/2UyGUWiAGmi) &mdash; content and formatting may not be reliable*
