---
layout: post
title: "Trying something new this week. Ninjatek Cheetah filament"
date: July 04, 2018 22:38
category: "Discussion"
author: Ben Delarre
---
Trying something new this week. Ninjatek Cheetah filament.



Finally got it printing after discoving my bowden tube was not fully inserted as it had worn a groove in the tubing. Replaced that and can reliably extrude and finish prints. But I simply can't get the top layers looking right.



I calibrated my extrusion multiplier so the walls come out slightly over spec 0.51mm rather than 0.5mm) walls and layers and bonding nicely, but the top layers are still under extruded. I tried bumping up the extrusion multiplier another 15% to no effect (top in the image is the one with extra multiplier and actually looks worse).



Anyone else tried this filament before? Can't find much out there about it.





![images/0509891e9a27c01396dd29342dc7adf9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0509891e9a27c01396dd29342dc7adf9.jpeg)



**Ben Delarre**

---
---
**Oliver Seiler** *July 05, 2018 00:30*

I've printed NinjaFlex before in a direct drive setup and had to tighten the screws pushing the cogs in the Bondtech extruder a fair bit closer together (1.5 turns) to get reliable extrusion rates. Might be worth checking. 


---
**Ben Delarre** *July 05, 2018 01:47*

I actually found the opposite. If I made the tension tighter the filament stuck to the drive wheels and wrapped itself up in the extruder.



I think I figured it out though. My usual filament setting has a 0.5mm extrusion width from a 0.4mm nozzle. This usually works fine, but it appears the Cheetah doesn't swell at all coming out of the nozzle, may even shrink a bit so reducing down to 0.4mm extrusion width seems to have helped a lot.



Other weird thing I have happening is my Bowden tube is still riding up out of the extruder somehow. I can't physically pull the Bowden tube up with the Bowden clips in place on the grommets but somehow between prints the tube is still backing out of the hotend which then gives the filament somewhere to buckle. Very strange indeed.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/9JoBeHSkYnD) &mdash; content and formatting may not be reliable*
