---
layout: post
title: "Well isn't this interesting... lol haaaalp It's like things aren't one solid rug roh"
date: February 27, 2016 08:29
category: "Discussion"
author: Jim Stone
---
Well isn't this interesting... lol haaaalp 



It's like things aren't one solid rug roh 


**Video content missing for image https://lh3.googleusercontent.com/-52BhGxIOuCo/VtFeXSQT96I/AAAAAAAAATc/pIXad1lR9Ec/s0/20160227_032704.mp4.gif**
![images/4f368eef528a7a1b3d4751b2a6cfa807.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4f368eef528a7a1b3d4751b2a6cfa807.gif)



**Jim Stone**

---
---
**Eric Lien** *February 27, 2016 21:52*

Can you show the whole screen so we can see what's happening?


---
**Eric Lien** *February 27, 2016 21:53*

Looks like features are under defined so they have unintended degrees of freedom.


---
**Luis Pacheco** *February 28, 2016 05:13*

Like **+Eric Lien** mentioned, looks like features are under defined. Is it supposed to rotate at all? If so maybe some limit mates may work.


---
**Erik Scott** *March 04, 2016 22:04*

Fist, make sure you've fully defined all your sketch. OnShape is pretty good about letting you know when your sketch is fully defined, unlike some other CAD applications I could mention (Fusion360, I'm looking at you).



You could also be rotating a feature or sketch, which, depending on how you define your sketches, could cause wonky behavior. Generally, I don't bother with any of the direct modeling features. I like to keep things parametric. Like Eric said, it would be nice to see what tool you're using and what the general context is.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/dGeDCpwVgHg) &mdash; content and formatting may not be reliable*
