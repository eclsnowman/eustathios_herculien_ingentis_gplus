---
layout: post
title: "Sorry for asking, but what is the length of the aluminum bars?"
date: May 24, 2015 20:21
category: "Discussion"
author: George Salgueiro
---
Sorry for asking, but what is the length of the aluminum bars? What is the length of the bars in the platform? And what is the length of the closed belts components, the one in the bottom and the ones in the x,y? Can't find it in the BOM.





**George Salgueiro**

---
---
**Daniel F** *May 24, 2015 20:50*

**+George Salgueiro** it depends, V1 or V2? V2 BOM on github: [https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Documentation/BOM
V1](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Documentation/BOM%0AV1) uses different belts, 3 different lenghts but they are only available from the US. BOM for V1: [https://docs.google.com/spreadsheet/ccc?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing#gid=0](https://docs.google.com/spreadsheet/ccc?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing#gid=0)

lenght (number of teeth) is in the product ID 3 digits after the M -> 

A 6R51M502060 has 502 teeth L=1004mm, a.s.o.

To make it even more complicated, I used the V2 BOM but V1 design with the XY motors in the base. I designed new mounts for x and y motors that work with robotdiggs 976mm belts, see here: [https://plus.google.com/u/0/111479474271942341508/posts/7wMEKzx9aw6?cfem=1](https://plus.google.com/u/0/111479474271942341508/posts/7wMEKzx9aw6?cfem=1)

There is not much room to adjust/tension the belt and it's a bit fiddly to put the belt on the lower pulley but it works. This way I only use one type of (closed loop) belt and it is easier to build an enclosure around the printer.


---
**Eric Lien** *May 24, 2015 21:48*

**+George Salgueiro**​ also the part names of the rods and extrusions have the length in the name.


---
**Erik Scott** *May 24, 2015 22:11*

It should all be in the BoM. In addition to looking at the BoM, I highly suggest downloading the CAD files and having a good look around and find all those key dimensions. Before I built my machine, I took the time to re-cad everything, which served 2 purposes; 1, to look for little issues that I could correct and do differently, and 2, to become intimately familiar with the build before even purchasing the parts. This way, when it comes to actually assembling things, there are few to no surprises. 



Eric Lien was kind enough to export his cad models to a variety of formats. You can download a free CAD package, like FreeCAD or DesignSpark and have a look without paying anything. 



To answer your question, for the V1 Eustathios, the horizontal frame extrusions are 425mm. The vertical ones are 565mm. For the print bed, the single center one is 321mm, and the other 2 are 361mm. Do note that things have changed in V2. 


---
**George Salgueiro** *May 25, 2015 00:25*

Does anyone use a heated bed?


---
**Mike Thornbury** *May 25, 2015 00:42*

I do, nice and toasty in winter... Oh wait, I see what you mean :)


---
**Eric Lien** *May 25, 2015 01:00*

**+George Salgueiro** yup.


---
**George Salgueiro** *May 25, 2015 15:51*

How much supply is needed for a bed sized like that? Do you use a relay and silicon on bottom of the aluminum plate? 


---
**Eric Lien** *May 25, 2015 16:07*

**+George Salgueiro** correct. If you look at the BOM it is all in there. Omron SSR, and Silicon 120v heated bed.


---
**George Salgueiro** *May 27, 2015 01:09*

Thanks again.


---
*Imported from [Google+](https://plus.google.com/103579216912687360818/posts/3GeKcZ8LhkS) &mdash; content and formatting may not be reliable*
