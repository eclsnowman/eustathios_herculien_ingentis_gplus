---
layout: post
title: "Yay! Misumi 150 came in build 2 in t minus..."
date: March 13, 2014 15:38
category: "Show and Tell"
author: D Rob
---
Yay! Misumi 150 came in build 2 in t minus...

![images/97dd5f2e8cf0d0c67247c13187612df8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/97dd5f2e8cf0d0c67247c13187612df8.jpeg)



**D Rob**

---
---
**Eric Moy** *March 13, 2014 20:32*

Awesome, thanks **+MISUMI USA**


---
**D Rob** *March 14, 2014 00:36*

Thanks Misumi Usa!!!!!!!!!!


---
**Daniel Fielding** *March 14, 2014 03:32*

Wish I lived in the states right now.


---
**D Rob** *March 14, 2014 03:39*

decided to try some non self-centering bushings.the long ones are for the xy ends and the the short ones are for the carriage. I got the black 8020  so come on lets hear some suggestions for the plexi color and plastic color


---
**Daniel Fielding** *March 14, 2014 03:41*

Pink :-)


---
**D Rob** *March 14, 2014 06:05*

**+Daniel fielding** i respectfully decline your submission. :-P


---
**Jarred Baines** *March 14, 2014 09:52*

Lol :-) an "electric" blue or purple for plexiglass and black or silver printed parts I reckon :-)

Or that awesome fluro yellow/green colour (that a lot of cars have these days) for both!




---
**Jason Barnett** *March 14, 2014 15:06*

Mirrored Plexiglas


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/cRGCymxJh76) &mdash; content and formatting may not be reliable*
