---
layout: post
title: "Hmmm for the azteeg. sd8825 or sd6128 i have heard n seen issue with the 8825 ( probably other makers) thoughts?"
date: October 21, 2015 17:19
category: "Discussion"
author: Jim Stone
---
Hmmm for the azteeg. sd8825 or sd6128



i have heard n seen issue with the 8825 ( probably other makers)



thoughts? ideas?





**Jim Stone**

---
---
**Eric Lien** *October 21, 2015 17:57*

I need to right my review of the sd6128. But initial impression is they are great drivers. Noise difference is not lower than sd8825 but I haven't played with the solder jumpers yet to change how they run from defaults. They do seem to run hotter than the sd8825 at the same current, but Roy is sending some larger heat sinks which should take care of that. 



I love the idea of being able to up the microstepping, but currently the 8bit processor is the bottle neck, so I run them at 16x to avoid stutters at high speed.


---
**Eric Lien** *October 21, 2015 17:58*

BTW I have been running roys sd8825 for years now, on three controllers, at high current with zero issues. ﻿


---
**Øystein Krog** *October 21, 2015 18:14*

I've grown to love steppers that upsample the microstepping themselves, as I primarily want higher microstepping for less noise.

The TMC2100's have been working great for me.


---
**Jim Stone** *October 21, 2015 19:43*

a little offtopic but **+Eric Lien**  where did you source this part? i can only find ONE place and they want 20usd shipping for just the 2 teeny belts 113 tooth (226mm long) closed loop GT2 Belt (2mm pitch x 6mm wide)


---
**Eric Lien** *October 21, 2015 21:23*

**+Jim Stone**  I get all my belts from robotdigg. Since a lot of my parts are sourced from there the cost of adding it is nothing compared to the weight of the steppers/etc.



Here is one that is really close (114 tooth): [http://www.robotdigg.com/product/282/228mm+or+232mm+long+gt2-6+closed-loop+belt](http://www.robotdigg.com/product/282/228mm+or+232mm+long+gt2-6+closed-loop+belt)


---
**Ryan Carlyle** *October 22, 2015 00:28*

8825s don't perform well with low-inductance motors and 24v PSUs. They're marginal with 12v PSUs. They can't hit low-current microsteps in mixed or slow decay mode, and they often make annoying hissing noises in fast decay mode. The THB6128 is a fantastic driver from a current control standpoint, it has some clever decay mode optimization that gives it much better current control than 8825s or 4988s. 


---
**Eric Lien** *October 22, 2015 03:40*

**+Ryan Carlyle** I have run mine at 24V, and no issues so far in several years. 


---
**Ryan Carlyle** *October 22, 2015 13:11*

**+Eric Lien** then you're one of the lucky ones. I see tons of people with low-speed ripple in mixed decay and/or noise issues in fast decay. Some 8825+motor combos work fine, some don't. 



Sidenote, having issues finding motors that work with the 8825 was a major reason why I made my stepper+driver simulator. [https://github.com/rcarlyle/StepperSim](https://github.com/rcarlyle/StepperSim)

If you have good motor specs, it will tell you how well a particular motor will work with a particular driver. (Hint: THB6128 works with everything.)


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/j9GMf8VLffx) &mdash; content and formatting may not be reliable*
