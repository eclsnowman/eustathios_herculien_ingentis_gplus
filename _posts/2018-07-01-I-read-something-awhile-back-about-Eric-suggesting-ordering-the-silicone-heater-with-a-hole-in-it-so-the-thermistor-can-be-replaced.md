---
layout: post
title: "I read something awhile back about Eric suggesting ordering the silicone heater with a hole in it so the thermistor can be replaced"
date: July 01, 2018 18:37
category: "Discussion"
author: William Rilk
---
  I read something awhile back about Eric suggesting ordering the silicone heater with a hole in it so the thermistor can be replaced.  Any ideas how big the hole should be?  My idea was 15-20mm, but I'm not sure if that'll leave enough surface area for the thermistor to stay adhered to the heat spreader





**William Rilk**

---
---
**Zane Baird** *July 01, 2018 20:46*

I leave a 25x25mm hole in all my heaters, drill a small divot in the spreader, and place the thermistor in some thermal paste (very small amount). Then secure it to the heat spreader with some polyimide tape


---
**Eric Lien** *July 01, 2018 20:59*

I have gone as small as 8x8 hole on one for the adoptabot I did for a local robotics club:



 

![images/80f1d1de496a3723253136480e2c8e17.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/80f1d1de496a3723253136480e2c8e17.png)


---
**William Rilk** *July 01, 2018 21:41*

Very helpful, thank you!  I neglected to ask if it would be a good idea to go ahead and order a spare as well.  Have they been fairly reliable in your experience?


---
**Eric Lien** *July 01, 2018 22:46*

Mine from alirubber have been running well for years. Same with people I know who bought keenovo brand. I think if it is properly spec'd, wires are strain relieved, etc... That you should see a good service life.


---
**William Rilk** *July 01, 2018 22:48*

Cool, thank you.


---
**William Rilk** *July 09, 2018 18:27*

So, for whatever reason, alirubber hasn't responded to my inquiries so I turned to Ebay.  The seller, jsr2industries, was very responsive and I was able to order a 380mm x 380mm heater with a 25mm hole for $65.80, shipped.  I expect to have it middle of August.  I'll let everyone know how it turns out!


---
**Eric Lien** *July 10, 2018 16:20*

**+William Rilk**  Sorry to hear they weren't responsive. I always had good luck... but things change with time. My last contact with them was end of 2017 I talked with Nicole Liu (nicole AT [alirubber.com.cn](http://alirubber.com.cn))


---
*Imported from [Google+](https://plus.google.com/100191047182984055447/posts/4rio79GJNVu) &mdash; content and formatting may not be reliable*
