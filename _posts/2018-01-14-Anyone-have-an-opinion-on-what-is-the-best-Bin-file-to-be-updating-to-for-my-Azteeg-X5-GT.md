---
layout: post
title: "Anyone have an opinion on what is the best Bin file to be updating to for my Azteeg X5 GT ?"
date: January 14, 2018 23:47
category: "Discussion"
author: jerryflyguy
---
Anyone have an opinion on what is the best Bin file to be updating to for my Azteeg X5 GT ? 





**jerryflyguy**

---
---
**Eric Lien** *January 15, 2018 03:12*

[github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/blob/edge/FirmwareBin/firmware-latest.bin)


---
**Eric Lien** *January 15, 2018 03:18*

Plus I think **+Wolfmanjm**​​ implimented some improvements recently to the meshlevel routine. So if you use a probe, it's a good time to update and test the changes.


---
**jerryflyguy** *January 16, 2018 02:42*

Is it better to use the newest one or the older one? What about the one with the PC connection disabled? 


---
**Eric Lien** *January 16, 2018 04:27*

Not sure about pc connection disabled item you mentioned. 


---
**jerryflyguy** *January 27, 2018 16:26*

Sorry for the slow reply **+Eric Lien** I was meaning the published firmware.bin vs firmware.bin.md5sum firmware’s? The md5sum is supposed to isolate the printer from the pc to stop certain errors (unresponsive curser etc) from happening. I’ve had the unresponsive curser thing a few times and **+Roy Cortes** mentioned this might help (but that was nearly a year ago)? I tried it and still get the ‘pause’ on the curser randomly as I’m scrolling. Anyone else heard of this or why it does this? (Pause while scrolling)


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/6ZGJhhVvujK) &mdash; content and formatting may not be reliable*
