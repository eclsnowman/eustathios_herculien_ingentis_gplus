---
layout: post
title: "Not a bad start :)"
date: June 13, 2014 16:38
category: "Show and Tell"
author: Eric Lien
---
Not a bad start :)

![images/7d7ce050117946e2d587a32fb788da6c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7d7ce050117946e2d587a32fb788da6c.jpeg)



**Eric Lien**

---
---
**Ricardo de Sena** *June 13, 2014 16:53*

Good start.


---
**Eric Lien** *June 13, 2014 19:15*

**+Shauki Bagdadi** I hope I can keep it running smoothly. My wife now calls them my mistresses. ﻿


---
**Mike Miller** *June 13, 2014 22:31*

**+Shauki Bagdadi** is that a consistent .2 bigger, or does that scale with size?


---
**Mike Miller** *June 14, 2014 01:46*

I keep meaning to ask...are you heating the bed when printing ABS? **+Shauki Bagdadi** 


---
**Mike Miller** *June 14, 2014 11:40*

I was able to print abs on superglue on blue tape, but the fumes were NASTY, and it went through a lot of glue. 


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/6hXdFqkH4me) &mdash; content and formatting may not be reliable*
