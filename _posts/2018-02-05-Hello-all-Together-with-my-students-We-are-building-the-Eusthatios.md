---
layout: post
title: "Hello all. Together with my students, We are building the Eusthatios"
date: February 05, 2018 13:45
category: "Discussion"
author: Wilmer Gaona
---
Hello all. 

Together with my students, We are building the Eusthatios. Now, We are buying the components and I have a question.



 Is the Hercu-extruder by default for the filament of 1.75 mm?





**Wilmer Gaona**

---
---
**Eric Lien** *February 05, 2018 18:13*

Yes the Hercustruder is designed for 1.75mm filament by default. But after using many other extruders... my recommendation is to go with a Bondtech extruder. The Bondtech QR is the defacto standard for bowden extruders in my opinion. Nothing performs better.



In a close second is the Bondtech BMG. It falls just short of the QR in raw pushing power. But with the right stepper motor it can achieve faster retraction speeds that the QR due to the built in gear reduction being slightly lower (around 3:1 on BMG vs 5.18:1 on the QR).



Long story short I designed the Hercustruder back before the Bondtech extruder existed. It performs well. But if you want unparalleled reliability, you really can't go wrong with a Bondtech. There is a reason Prusa, and BCN3D on the Sigmax moved to Bondtech (I think other MFG's also have it in the works).



Full disclosure Martin is a great friend of mine, and I even worked with him a little at first when he was developing the Bondtech Extruder concept. But I only recommend it because it's the best, hands down. And the extruder can be the single biggest headache on a printer if it isn't performing correctly. 


---
**Wilmer Gaona** *February 06, 2018 11:47*

So many thanks for your comments **+Eric Lien**. We will work with the Hercustruder because this is the first contact of my students with 3D printing and the cost of Bontech is some out of range of our budget. 



But in the future, I definitely have to test the Bondtech. I heard first here in this forum and I did not know about the Prusa and BCN SigmaX adoption of Bondtech. In fact, I have two Prusa i3 MK2S and these work like a charm. 


---
*Imported from [Google+](https://plus.google.com/109742485522388793995/posts/5mUEDcZWQx3) &mdash; content and formatting may not be reliable*
