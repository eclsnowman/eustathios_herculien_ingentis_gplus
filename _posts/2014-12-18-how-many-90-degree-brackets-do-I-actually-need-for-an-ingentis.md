---
layout: post
title: "how many 90 degree brackets do I actually need for an ingentis"
date: December 18, 2014 03:00
category: "Discussion"
author: Ethan Hall
---
how many 90 degree brackets do I actually need for an ingentis. Buying 50 seems wasteful if I'm only gonna end up using 20 or so?





**Ethan Hall**

---
---
**D Rob** *December 18, 2014 03:04*

0


---
**D Rob** *December 18, 2014 03:04*

Check my videos


---
**Ethan Hall** *December 18, 2014 03:12*

If I already ordered the length of extrusion specified in the bom will I still be able to do that without making the build volume smaller


---
**D Rob** *December 18, 2014 04:41*

What lengths did you order?


---
**D Rob** *December 18, 2014 04:43*

If the square frames (top and bottom) are all the same length for the 4 pieces then my method adds 20mm to each one's lengths.


---
**ThantiK** *December 18, 2014 06:06*

Seconding the 0 number.  Way better to assemble the frame with the drilled holes method, especially if the lengths were pre-cut for you.


---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/cUX71svF7vs) &mdash; content and formatting may not be reliable*
