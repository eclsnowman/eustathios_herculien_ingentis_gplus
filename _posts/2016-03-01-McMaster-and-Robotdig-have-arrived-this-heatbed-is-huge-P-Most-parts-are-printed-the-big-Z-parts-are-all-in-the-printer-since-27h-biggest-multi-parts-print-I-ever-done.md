---
layout: post
title: "McMaster and Robotdig have arrived, this heatbed is huge :P Most parts are printed, the big Z-parts are all in the printer since 27h, biggest multi parts print I ever done"
date: March 01, 2016 20:26
category: "Build Logs"
author: Maxime Favre
---
McMaster and  Robotdig have arrived, this heatbed is huge :P

Most parts are printed, the big Z-parts are all in the printer since 27h, biggest multi parts print I ever done. But it's running fine



Misumi package is for tomorrow ;)



![images/a0c9501aa9b438dfdd793135f064c00f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a0c9501aa9b438dfdd793135f064c00f.jpeg)
![images/74d33cc7145ad528b5361bb366423644.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/74d33cc7145ad528b5361bb366423644.jpeg)

**Maxime Favre**

---
---
**Michaël Memeteau** *March 01, 2016 20:38*

Your knife is beautiful! Where did you get it? (If I may ask)




---
**Eric Lien** *March 01, 2016 20:48*

Can't wait to see the build start to take shape. And don't forget to ask questions along the way. We have lots of experienced builder here to leverage for advice.


---
**Maxime Favre** *March 01, 2016 21:06*

**+Michaël Memeteau** Thanks ! I make them ;) I did some forging (Honestly if you have the opportunity to try, do it) but mostly I buy scandinavian blades and craft the handle and sheat myself.

You can find some of my "creations" by scrolling this gallery (3 pages):

[http://www.pentaprisme.com/Autres/Make-make-make/n-frLHHt/i-sDzp7qJ](http://www.pentaprisme.com/Autres/Make-make-make/n-frLHHt/i-sDzp7qJ)


---
**Michaël Memeteau** *March 01, 2016 21:11*

Supercool!




---
**Michaël Memeteau** *March 01, 2016 21:12*

The link you provided doesn't work... Sniff!


---
**Maxime Favre** *March 01, 2016 21:15*

**+Michaël Memeteau** Sorry, corrected


---
**Maxime Favre** *March 01, 2016 21:27*

About endstops: Do you drill them to use M3 screws ? Or use M2 or M2.5  ? (which don't appear in BOM list)


---
**Eric Lien** *March 01, 2016 22:02*

I drilled mine. There's enough meat there and I hate having non-standard hardware in the build if I can help it.


---
**Maxime Favre** *March 01, 2016 22:42*

One more thing for today, how does the heated mat hold in place against the aluminium plate ? heat resistant glue, thermal past ?


---
**Eric Lien** *March 01, 2016 23:01*

If you got the one from the BOM it would have an adhesive pad. If not you can get 3M 468MP tape to hold it to the bottom. I found good prices on that tape at [zoro.com](http://zoro.com), and it is also available on Aliexpress (though likely not actually 3M brand, I think it should work just fine).


---
**Walter Hsiao** *March 01, 2016 23:09*

Those pictures bring back memories...  Mine came with 3M 467MP Adhesive.  I've also heard of people using high temperature silicone.


---
**Maxime Favre** *March 02, 2016 09:57*

I ordered it by Robotdigg , it didn't come with the tape. Thanks for the reference, it shouldn't be a problem to find some.

Ultibot order arrived too.


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/1uASP3RQjeX) &mdash; content and formatting may not be reliable*
