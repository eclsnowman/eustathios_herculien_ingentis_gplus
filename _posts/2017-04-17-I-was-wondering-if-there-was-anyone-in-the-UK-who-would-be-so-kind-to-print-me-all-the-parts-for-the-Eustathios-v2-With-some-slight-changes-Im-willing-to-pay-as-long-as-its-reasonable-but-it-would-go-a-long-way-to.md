---
layout: post
title: "I was wondering if there was anyone in the UK who would be so kind to print me all the parts for the Eustathios v2 (With some slight changes) I'm willing to pay (as long as its reasonable) but it would go a long way to"
date: April 17, 2017 20:22
category: "Discussion"
author: Adam Morgan
---
I was wondering if there was anyone in the UK who would be so kind to print me all the parts for the Eustathios v2 (With some slight changes) I'm willing to pay (as long as its reasonable) but it would go a long way to restoring my sanity. 



For the life of me I just cant seem to get my printer to print decent prints and I been at it for a year. When I first built the printer out of PLA I was printing amazing prints, but for some reason I decided to replace it all with ABS parts and since then I have been in a constant loop just trying to get the thing to work properly. It doesn't help that occasionally the printer would spaz out for some reason resulting in me having to replace parts (Rambo burnt out, print screwed up and totally destroyed my IR probe and fans, the list goes on) I have gone through all the guides and videos on levelling the gantry etc but its just not working, I am at the point now where if I could get a fresh set of parts that I know are printed well then hopefully I can actually start printing things I want :)



So yeah, if there is someone who has a perfectly calibrated printer out there willing to assist, could you please help me out? :)





**Adam Morgan**

---
---
**jerryflyguy** *April 17, 2017 21:10*

Adam I'd be glad to print you a set. My machine is down for at least a week but if no-one else steps up in the next week or two shoot me a note directly and we'll set it up. 


---
**Adam Morgan** *April 17, 2017 21:19*

**+jerryflyguy** that's amazing thank you! I will get in contact if no one else can. Thanks again! 


---
**SalahEddine Redjeb** *April 18, 2017 07:37*

well, i live in belgium, i only have an up plus 2 for the moment, i might be getting an anet 8 soon, but if you can't get someone close to you to do it, well, i guess i'll do it


---
**Adam Morgan** *April 19, 2017 18:08*

Thanks **+SalahEddine Redjeb** i will bear the offer in mind. Really appreciate the offer


---
*Imported from [Google+](https://plus.google.com/117208540149253709671/posts/Aci5QohCVJ3) &mdash; content and formatting may not be reliable*
