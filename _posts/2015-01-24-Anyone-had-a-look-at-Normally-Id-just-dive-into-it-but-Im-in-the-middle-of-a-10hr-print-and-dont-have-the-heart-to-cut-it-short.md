---
layout: post
title: "Anyone had a look at ? Normally I'd just dive into it, but I'm in the middle of a 10hr print and don't have the heart to cut it short"
date: January 24, 2015 02:53
category: "Discussion"
author: Jim Wilson
---
Anyone had a look at [http://www.mattercontrol.com](http://www.mattercontrol.com) ? Normally I'd just dive into it, but I'm in the middle of a 10hr print and don't have the heart to cut it short. I currently only use Slic3r and Cura combined with Pronterface.





**Jim Wilson**

---
---
**James Rivera** *January 24, 2015 03:11*

I used an early version of it. It has a pretty slick UI. But I have not tried their slicer yet.


---
**Ubaldo Sanchez** *January 24, 2015 03:39*

Not too crazy about the software. Not bad looking, just not as easy to use like repetier.




---
**Whosa whatsis** *January 24, 2015 04:47*

It's a very different paradigm that will take a lot of getting used to for experienced users. It's aiming more toward the future of "just click to print" machines, rather than trying to be a sophisticated control interface for a machine that needs one, as older host programs try to be. I think its features will make things easier for new users, though experienced users will need to re-learn some things and get used to some slightly more advanced functions not being front-and-center like they are in other hosts.



I like the slicing. In particular, the supports in MatterSlice work really well. MatterSlice is based on CuraEngine, and its supports are based on Cura's but it adds interface layers with an adjustable gap for release from the print. I've gotten some pretty spectacular results printing with supports in PLA using MatterSlice, orders of magnitude better than I've seen with any other slicer.


---
**Jim Wilson** *January 24, 2015 04:56*

Thanks for the feedback! Love the Cura engine already, with my one complaint being its support, so that bodes well


---
**James Rivera** *January 24, 2015 06:21*

I didn't know it was based on CuraEngine, which is excellent. I'll have to take another look at this--especially when I need to print with support (although I try to avoid support material like the plague).


---
**Dat Chu** *January 24, 2015 23:14*

This app is like the Desktop version of Octoprint. It has better 3d visualization but I think Octoprint and web technologies are the way to go.


---
*Imported from [Google+](https://plus.google.com/101778058628996936791/posts/4FWtpoBmxX6) &mdash; content and formatting may not be reliable*
