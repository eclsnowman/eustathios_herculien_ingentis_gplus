---
layout: post
title: "PETG? I cant get it to print well, what are your tips?"
date: July 10, 2018 14:04
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
PETG? I cant get it to print well, what are your tips?



Bondtech, E3D V6





60mm/s print speed



tried both 40 and 60mm/s retract. 60mm/s worked best.





![images/ec3391d654b902ade0238c9dfdb694f5.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ec3391d654b902ade0238c9dfdb694f5.png)
![images/8ca1bb6f3242dd7d557715988caee251.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8ca1bb6f3242dd7d557715988caee251.jpeg)
![images/26bccf1e6ce78cfa483f0ab6b7c2fe97.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/26bccf1e6ce78cfa483f0ab6b7c2fe97.jpeg)
![images/0b2b0b6e94a90467b14154e390976f04.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0b2b0b6e94a90467b14154e390976f04.jpeg)

**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *July 10, 2018 14:27*

I will say that PETG stringing is highly moisture dependant. Dry it out in cheap food dehydrator like this:

 [https://www.amazon.com/dp/B075ZB3V9S?ref=yo_pop_ma_swf](https://www.amazon.com/dp/B075ZB3V9S?ref=yo_pop_ma_swf)



And then run the identical gcode. You will be surprised by the difference. At first I thought petg was fairly moisture agnostic... But after seeing the difference dry filament makes, My opinion has been converted.



Also for PETG **+Zane Baird** mentioned to me a while back, you want fast acceleration on retraction, but not necessarily fast speed. Maybe he can chime in on the settings that works best for him. He is a PETG Jedi Master :)


---
**Zane Baird** *July 10, 2018 14:39*

**+Stefano Pagani** to add to what **+Eric Lien** mentioned, I generally use a retraction acceleration of 3000-3500 for PETg with the bondtech extruders (both Mini and V2). With these values I can get retraction distances as low as 5mm (@ 50mm/s) for PETg prints on my Herculien which has a bowden length of almost 1m


---
**Stefano Pagani (Stef_FPV)** *July 10, 2018 18:44*

**+Zane Baird** thank you! I will try, you are using firmware retract then right? 


---
**Zane Baird** *July 10, 2018 18:47*

**+Stefano Pagani** Negative on the firmware retract. I just set independent acceleration for the extruder motor and don't use "wipe while retracting". Since there is no other movement during the retraction the extruder motor accel is not limited by any other accel values


---
**Stefano Pagani (Stef_FPV)** *July 19, 2018 14:16*

Thanks for the help! Turns out my extruder was stalling on un-retract. Beefed it up to a 70mm :)


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/N3Gi9aHdHNe) &mdash; content and formatting may not be reliable*
