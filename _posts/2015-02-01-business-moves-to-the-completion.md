---
layout: post
title: "business moves to the completion..."
date: February 01, 2015 18:19
category: "Discussion"
author: Ivans Nabereznihs
---
business moves to the completion...

![images/2d98d107917656c0db60b393026dc7af.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2d98d107917656c0db60b393026dc7af.jpeg)



**Ivans Nabereznihs**

---
---
**James Rivera** *February 01, 2015 20:18*

Nice! What electronic board is that? It looks a bit like a Printrboard but the big green terminal block and automotive fuse say otherwise.


---
**Eric Lien** *February 01, 2015 22:37*

Looks like an azteeg x5 mini.


---
*Imported from [Google+](https://plus.google.com/118257495094694997465/posts/Wpr1kJG4XjB) &mdash; content and formatting may not be reliable*
