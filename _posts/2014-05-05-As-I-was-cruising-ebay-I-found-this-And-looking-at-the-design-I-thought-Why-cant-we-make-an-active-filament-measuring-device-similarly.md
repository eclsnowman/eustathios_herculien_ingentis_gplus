---
layout: post
title: "As I was cruising ebay I found this And looking at the design I thought \"Why cant we make an active filament measuring device similarly?\""
date: May 05, 2014 00:02
category: "Show and Tell"
author: D Rob
---
As I was cruising ebay I found this [http://www.ebay.com/itm/Wire-Measuring-Machine-/331157067818?pt=LH_DefaultDomain_0&hash=item4d1a7fd82a](http://www.ebay.com/itm/Wire-Measuring-Machine-/331157067818?pt=LH_DefaultDomain_0&hash=item4d1a7fd82a)  And looking at the design I thought "Why cant we make an active filament measuring device similarly?". This design would use a spring, 2 x  623vv bearings, a sliding pot, 3 x m3 screws, 1 x m2 screw, and the rest printed. Alternately a second axis, turned 90 degrees, could be added. Calibrate distance from the filament to the nozzle and away we go. My issue is this: my Arduino skills are non-existent. That and lack of firmware support which falls back on my issue again. I made this in all one part with enough space .1mm for each item to be separate. I exported to .stl and cura separated most of it except the top bearing. Any thoughts, ideas, help?



![images/908b88d60f789204bd21355f2753b9d4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/908b88d60f789204bd21355f2753b9d4.jpeg)
![images/ff4b86f2d228f54324416560fa097183.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ff4b86f2d228f54324416560fa097183.jpeg)
![images/6bd19f4d3b2d3ad3cb019d6b8d3c9162.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6bd19f4d3b2d3ad3cb019d6b8d3c9162.jpeg)

**D Rob**

---
---
**David Gray** *May 05, 2014 07:39*

The ebay link measures length, I assume you want to measure diameter. Maybe this could help: [http://airtripper.com/1473/airtripper-extruder-filament-force-sensor-design-3d-print/](http://airtripper.com/1473/airtripper-extruder-filament-force-sensor-design-3d-print/)


---
**Liam Jackson** *May 05, 2014 13:06*

Looks like a good idea! I think it could be made a lot more compact. I wonder if it could be built into the extruder idler?



How accurate are cheap linear pots? I guess we want around +-0.05mm accurary, ideally +-0.01mm?


---
**D Rob** *May 05, 2014 19:14*

**+Liam Jackson** awesome idea. I'll look into that.


---
**Wes Brown** *May 05, 2014 22:00*

These guys are talking about similar systems [https://plus.google.com/communities/107859862288161234107](https://plus.google.com/communities/107859862288161234107)


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/go66cnCfLNM) &mdash; content and formatting may not be reliable*
