---
layout: post
title: "I am completely stuck at this moment"
date: July 07, 2015 16:14
category: "Discussion"
author: Igor Kolesnik
---
I am completely stuck at this moment. My friend who was going to print parts, moved to another country so my project is delayed for unknown period of time.

Will somebody be able to print parts for me? I am ready to cover print time and material. Also I am looking forward to participate in Print Forward as soon as it won't be a shame to show my print results





**Igor Kolesnik**

---
---
**Gus Montoya** *July 07, 2015 20:18*

I should be done with my printer next week (Hopefully). If your not on the list of the print it forward then I suggest you get on it. Also if you don't have a build plate let me know. I might be able to source one for you. After calibration I'll start on the print it forward list.


---
**Igor Kolesnik** *July 07, 2015 20:32*

Can you please share the link for the mentioned list? Also build plate is something that I am looking at. I have small 20x20 one and set up for it, but it is a shame to have a nice large frame with such a small build size


---
**Gus Montoya** *July 07, 2015 20:40*

Here is the link with the print it forward:  

[https://docs.google.com/spreadsheets/d/1R3hqplEJYiaQFLmFX04-vTjgyQ5SVY1PGjbx-7S-J8k/edit](https://docs.google.com/spreadsheets/d/1R3hqplEJYiaQFLmFX04-vTjgyQ5SVY1PGjbx-7S-J8k/edit)



I will keep you posted with the build plate. It should be done this week.


---
**Chris Brent** *July 08, 2015 17:06*

Is that list up to date? **+Gus Montoya** you're still on the list :)


---
**Gus Montoya** *July 08, 2015 17:59*

No It's not up to date. I'll take myself off. 


---
*Imported from [Google+](https://plus.google.com/+IgorKolesnik/posts/HJWAam5WqYm) &mdash; content and formatting may not be reliable*
