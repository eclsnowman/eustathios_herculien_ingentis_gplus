---
layout: post
title: "Hey guys. This'll be my first 3d pro yet build as well as my first post"
date: April 28, 2017 03:36
category: "Discussion"
author: Stefan transF
---
Hey guys. This'll be my first 3d pro yet build as well as my first post. I have decided to build a Eustathios with a fairly large volume (roughly 2*2*2ft cube) and I have some questions regarding the build.

1. Should I use bushings or bearings for the print carriage? And what type? (I currently have lm8uu's on there)

2. I've made sure that the frame is square and measured, but the x/y belt and pully system is pretty stiff (meaning it requires quite a bit of force to pull it along the guide shafts. What could be the issue? 

3. Finally, what's the proper tension to put on the timing belts?



Thanks for taking time to read this. I hope to see some suggestions. Can't wait to see it come to life!



![images/ce351ba9b4e4f1417cafd67be9b0528e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ce351ba9b4e4f1417cafd67be9b0528e.jpeg)



**Stefan transF**

---
---
**Benjamin Liedblad** *April 28, 2017 04:23*

Checkout Eric's video on alignment... Helped a lot.



[plus.google.com - I gave a tutorial today on how I align the gantry on a Cross Rod Gantry style...](https://plus.google.com/+EricLiensMind/posts/RFQNLLefxoc)


---
**Ted Huntington** *April 28, 2017 04:36*

looking forward to hearing how it works out- you can use bearings on the carriage but not on the side rods. You may want to opt for 10mm rods for the carriage for so large a distance. Let us know if it ultimately works. Initially the bushings need a little bit of manual working, but the vertical alignment and belts with the sheaves is important too.


---
**Stefan transF** *April 28, 2017 12:12*

Thanks Benjamin Liedblad, I'll check it out!


---
**Stefan transF** *April 28, 2017 12:14*

Thanks for the advice, Ted. I have self lubricating brass bushings on the side rods. Is there a better bushing out there that I could end up using?


---
**Ted Huntington** *April 28, 2017 15:49*

**+Stefan transF** People have tried different bushings- I only used the bushings on the bill of materials, but others have used graphite impregnated and other kinds- search the list and you will find a variety of bushings that people have commented on.


---
**Eric Lien** *April 28, 2017 22:48*

At those spans I would be interested to see if the rod deflection becomes a problem. Especially the side rods which both have rotational forces and translation down it's length. Any deflection in the side rods becomes an eccentric as the side carriages move down the rod. 



If they become a problem you might have to look at larger diameter rods.


---
**Stefan transF** *April 29, 2017 02:22*

Alright Ted sounds good. And if anything, Eric, I'd have to size down  the printer due to low money and limited time for building.


---
*Imported from [Google+](https://plus.google.com/101411035411905191438/posts/GLpg2hzrup6) &mdash; content and formatting may not be reliable*
