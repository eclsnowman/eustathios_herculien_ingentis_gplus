---
layout: post
title: "Running at 150%. Oh come on! See anything wrong with carriage?"
date: July 30, 2015 21:40
category: "Discussion"
author: Brandon Cramer
---
Running at 150%. 



Oh come on! See anything wrong with carriage? Guess I will start fixing that now.....

![images/e45c04a3b9b359b45e7ee59f98e468e3.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e45c04a3b9b359b45e7ee59f98e468e3.gif)



**Brandon Cramer**

---
---
**Jim Wilson** *July 30, 2015 21:50*

Gotta run before you can walk!


---
**Vic Catalasan** *July 30, 2015 21:59*

Is it backwards? Sure runs fast though


---
**Brandon Cramer** *July 30, 2015 22:14*

Might never run that fast again. This really bites!!!



 **+Vic Catalasan** Yes, its backwards....


---
**Brandon Cramer** *July 30, 2015 23:52*

Alright. I'm done pouting... :-)



I think it's back to normal. That was a pain though. 


---
**Eric Lien** *July 31, 2015 00:10*

**+Brandon Cramer**​​ I guess now is a bad time to mention I have a trick for swapping the cross rods that doesn't require any real disassembly. Just loosen the clamping bolts that hold the cross rod, push the rod deep into one of the edge carriages, and pivot the opposite carriage out of the way. Then remove the rod from the other side.  Then spin the center carriage with rods installed 180deg and reinstall. It takes less than 5min. ﻿


---
**Brandon Cramer** *July 31, 2015 00:27*

**+Eric Lien**  Just a little bit! :) Thanks for the info though. I may need to do that at a later date. 



I think tomorrow I might have to put on the hotend. 



Is it possible to make this a dual extruder? Has anyone done that?


---
**Eric Lien** *July 31, 2015 00:43*

**+Brandon Cramer** this is what I would do: [https://plus.google.com/+EricLiensMind/posts/RKJhQbBKPkz](https://plus.google.com/+EricLiensMind/posts/RKJhQbBKPkz)


---
**Brandon Cramer** *July 31, 2015 17:26*

**+Eric Lien** I guess I would need a different board than the Azteeg X5 mini to use the Chimera extruder?


---
**Eric Lien** *July 31, 2015 18:02*

Yup, X5 Mini is single only.


---
**Sean B** *September 16, 2016 15:03*

**+Eric Lien** Sorry to digg this out of the grave, but do you still recommend removing the cross rods this way?  I just removed both my cross rods and reordered from Misumi because they were bent... may have been low quality steel though.  I am fairly certain they were straight when I purchased them.  Is there a risk of bending using this shortcut, or should everything be dissembled and installed the correct safe way?


---
**Eric Lien** *September 16, 2016 15:48*

There should be enough room to do this without bending them. I have done it several times. Just never force anything and loosen the side belts to allow the side carriages to rotate out of the way. But if you want to be safe you could always do a full disassembly. It's just that (as I am sure you know) alignment can be difficult... so I try to avoid having to relocate all the side bearing mounts if possible.


---
**Sean B** *September 16, 2016 16:06*

**+Eric Lien** Thanks!


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/ePWsyX9ubk6) &mdash; content and formatting may not be reliable*
