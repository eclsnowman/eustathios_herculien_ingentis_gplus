---
layout: post
title: "What hotend does everyone here use? im weary of getting another e3dv6"
date: October 24, 2015 01:05
category: "Discussion"
author: Jim Stone
---
What hotend does everyone here use?



im weary of getting another e3dv6. my last/current one jams and jams and jams have tried everything. 3mm Direct ( non bowden )



maybe ill go to 1.75





**Jim Stone**

---
---
**Tomek Brzezinski** *October 24, 2015 01:08*

What kind of jamming is it? Particulate or heat related? Because jamming is typically considered quite rare in an e3D, mine was a huge improvement over some basic hot ends I used before. 



If you don't have a fan properly working on your e3d, you may also get jams.



But maybe there's something peculiar about 3mm that encourages jamming for the e3d. I don't know what that would be.


---
**ThantiK** *October 24, 2015 01:08*

IMHO, unless you're looking to do exotics, a hot end with a PTFE liner is just fine. The lite6 is an option.  I wish the legit J-head was still being sold, I'd suggest that too


---
**Jim Stone** *October 24, 2015 01:10*

I can get legit jheads. But Idk if he does a 24v option.


---
**Igor Kolesnik** *October 24, 2015 01:12*

V6 1.75 bowden. Works like a charm. Your filament might be to thick. Have you tried something else? Try 2.85 mm filaments. They should work on 3mm extruder. Also you need to make sure that your retraction is set fairly low, 2-3 mm and even lower for direct drive. Plus you need to properly cool the heatsink to make sure plastic only melts were it is suppose to. ﻿


---
**Ian Hoff** *October 24, 2015 01:34*

reifsnyderb put his site back up...

Legit jheads are availible for now at [http://www.hotends.com](http://www.hotends.com) again.



I guess the "going out of business" generated enough sales to forestall that while he works on other concepts.



I have used an e3dv5, and e3dv6lite and a jhead. and for PLA there is simply no comparison to a real jhead, I dont even need a hotend fan and get no jamming.



Little known fact you can email ReifsnyderB and ask for matched length hotends for dual extrusion setups on an order. He sent me a beautiful pair. :)


---
**Zane Baird** *October 24, 2015 01:42*

I'm currently running an E3D volcano. PLA can be somewhat problematic, but that is solved by "seasoning" the hotend with some extra light olive oil or vegetable oil ([http://www.dragonflydiy.com/2014/06/nozzle-seasoning.html](http://www.dragonflydiy.com/2014/06/nozzle-seasoning.html)). With ABS I've experienced no jams. If you want to make the best use of the large print volume and high speeds possible with the Herculien (or Eustathios) I almost think the Volcano is a necessity. I'm currently printing 0.2mm layers with 100 mm/s perimeter speeds and the quality is flawless. 


---
**Ian Hoff** *October 24, 2015 01:56*

I read the article that link was based off and olive oil is not the best oil, but seasoning does help one of the metal hotend pla issues.



PLA is like liquid sugar when hot and sticks real well to stainless steel, and pla's low Tg can be a problem with heat creep, thus the fans on the fins.



the scientific approach to seasoning

[http://sherylcanter.com/wordpress/2010/01/a-science-based-technique-for-seasoning-cast-iron/](http://sherylcanter.com/wordpress/2010/01/a-science-based-technique-for-seasoning-cast-iron/)



Suggested oil is flaxseed


---
**Eric Lien** *October 24, 2015 02:07*

**+Ian Hoff** I love that post. Used it to season my dutch oven :)


---
**Zane Baird** *October 24, 2015 02:08*

**+Ian Hoff** Thanks for the info. I need to get some flax seed oil. However, the vegetable oil (I believe it was canola) I used seemed to do the trick. 


---
**Ryan Carlyle** *October 24, 2015 02:54*

I think most people use Canola, but these days I'm actually leaning towards a light machinery oil. The cold zone is where PLA jams happen, and the cold zone never gets hot enough to "season" the hot end, so my train of thought these days is that you're aiming for a liquid oil film, not a hardened seasoning layer. (Either could work though. I haven't seen any rigorous A/B testing.)


---
**Jim Stone** *October 24, 2015 02:56*

**+Ian Hoff**​ I know I'm wookie from #reprap lol


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/isx1DFkNhSZ) &mdash; content and formatting may not be reliable*
