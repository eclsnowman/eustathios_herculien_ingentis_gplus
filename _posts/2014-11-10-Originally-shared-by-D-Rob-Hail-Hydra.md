---
layout: post
title: "Originally shared by D Rob Hail Hydra"
date: November 10, 2014 06:25
category: "Show and Tell"
author: D Rob
---
<b>Originally shared by D Rob</b>



Hail Hydra. Chop off one print head and 2 grow in its place! Included in the pics are a guide (step by step) to building the hydra air. Soon to follow the end of the Hydra Air testing I will introduce the Hydra Ice.



![images/c093b8e31c135415fb8e3544c9b4fe80.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c093b8e31c135415fb8e3544c9b4fe80.jpeg)
![images/fc676f9386039ca7c78352d49ab08f0b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fc676f9386039ca7c78352d49ab08f0b.jpeg)
![images/fc7d9af682448af68b0b42876420a982.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fc7d9af682448af68b0b42876420a982.jpeg)
![images/3742429adb36bbf3056cf3fa07f1db98.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3742429adb36bbf3056cf3fa07f1db98.jpeg)
![images/3dccb54aca779afd4a7af6f90995134a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3dccb54aca779afd4a7af6f90995134a.jpeg)
![images/4e8e2df8353d6466a5332c58089213a4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4e8e2df8353d6466a5332c58089213a4.jpeg)
![images/85d48e949dfc5003b099de9309f61bd1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/85d48e949dfc5003b099de9309f61bd1.jpeg)
![images/54ed10e3338ad72c26e32cad9b8e286f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/54ed10e3338ad72c26e32cad9b8e286f.jpeg)
![images/e37efb95525e57ad686ea20d5d9ee0dd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e37efb95525e57ad686ea20d5d9ee0dd.jpeg)
![images/94e71f7c3d88db9b10f3b52e42c8ecb5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/94e71f7c3d88db9b10f3b52e42c8ecb5.jpeg)
![images/d90f90f8de1bf8db4fcaa56293f700de.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d90f90f8de1bf8db4fcaa56293f700de.jpeg)
![images/e75d8c592915cfae0dd257dd4f12dac4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e75d8c592915cfae0dd257dd4f12dac4.jpeg)
![images/9e60ea0c6bbb14ccd3305991ece51e54.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9e60ea0c6bbb14ccd3305991ece51e54.jpeg)
![images/667f75ad556d9112c6c126c065ac9675.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/667f75ad556d9112c6c126c065ac9675.jpeg)
![images/3c05ccd5dc83311029843020c0d119a9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3c05ccd5dc83311029843020c0d119a9.jpeg)

**D Rob**

---
---
**Nick Parker** *November 11, 2014 00:56*

What is it? Air cooled e3d hydra?


---
**D Rob** *November 11, 2014 01:59*

technically not e3d though I have used some of their parts. I love their parts and hotends but I am not afiliated with them. This is and will be a hotend in and of its self. I call it the Hydra Air. Next comes the Hydra Ice!


---
**Nick Parker** *November 11, 2014 02:04*

Have you printed with it yet? I thought the hydra was water cooled by necessity - heatsinking that much power is a real challenge.


---
**D Rob** *November 11, 2014 02:37*

The Kraken is water cooled and has twice the hot ends as the hydra. The hydra will have 2 fans. No I have not yet printed it. First I will take a spare ramps board and test cooling with the fans and a spare thermistor to monitor the heat sink temp as well. when I am satisfied it can properly cool I will test print with it.


---
**Jean-Francois Couture** *November 12, 2014 16:02*

I like the fact that its got only 2 hotends.. *4 is a bit to much.. and using E3D v6 heads should be great.. you have potential with that hotend.. keep us updated. BTW, I showed your work to a local 3D printed company and they found it interesting. :)


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/LThGavZyAzq) &mdash; content and formatting may not be reliable*
