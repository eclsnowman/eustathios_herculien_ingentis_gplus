---
layout: post
title: "Hey All, I just about have my first printer together and I am hoping that someone with experience on the ramps board can confirm my wiring"
date: December 14, 2015 20:16
category: "Discussion"
author: kaley garner
---
Hey All,



I just about have my first printer together and I am hoping that someone with experience on the ramps board can confirm my wiring.

Please let me know if this looks correct!



Thank you!

![images/202652d3971d606b12321d9add814dc5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/202652d3971d606b12321d9add814dc5.jpeg)



**kaley garner**

---
---
**Kevin Conner** *December 14, 2015 20:27*

looks right.

I recommend wiring the ground of your heaters (D8 and D10) directly to ground though. It will run less current through the board and your fuses will last longer.


---
**Wes Tabor** *December 14, 2015 20:32*

Looks correct, depending on setup i have reversed the stepper connections on X and E0 - I  agree with Conner, please solder those wires directly to the board as far as the power feeders go and the power to the bed and heater core. the connectors are the weak link. This looks like Folgertech 2020 ..


---
**kaley garner** *December 14, 2015 20:39*

Okay cool, thanks Kevin and Wes! I will do that. Wes, it is. I would love to eventually build a eustathios but for now, this is all my budget would allow..... Plus, I figure its a good starting point.


---
**Wes Tabor** *December 14, 2015 20:40*

It is. we Idle on [irc.freenode.net](http://irc.freenode.net) in  #reprap  and  #folgertech  . I've done many upgrades to the folgertech and it's pretty solid now with lead screws and auto-leveling (servo/endstop). hope to see you there.


---
**kaley garner** *December 14, 2015 20:46*

cool, thanks for sharing that. I'll check it out.


---
**Jean-Francois Couture** *December 16, 2015 18:29*

If I may add, the wires for the bed looks a bit thin.. are those 18AWG ? what king of bed do you have ?



Also, you could use D9 to add a fan to cool the prints.. installing it on D9 would PWM so you can control the speed.



I also would remove those connector.. especially the  green one for the BED.. had one of my RAMPS connector melt because they're so cheap and can't support 10-15A current .


---
*Imported from [Google+](https://plus.google.com/104940700298630363140/posts/DcKEeQXCGiY) &mdash; content and formatting may not be reliable*
