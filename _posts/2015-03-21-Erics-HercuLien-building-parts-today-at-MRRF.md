---
layout: post
title: "Eric's HercuLien building parts today at MRRF"
date: March 21, 2015 20:47
category: "Show and Tell"
author: Jeff DeMaagd
---
Eric's HercuLien building parts today at MRRF.

![images/2af4f7d282eb2c9f4bceb0e0df4f10d8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2af4f7d282eb2c9f4bceb0e0df4f10d8.jpeg)



**Jeff DeMaagd**

---
---
**John Driggers** *March 21, 2015 22:11*

It looked even better in person!


---
**Jeff DeMaagd** *March 22, 2015 02:14*

Gigantic in person. The renderings and photos don't do it justice.


---
**ThantiK** *March 22, 2015 04:33*

Agreed. This thing is a monster. Pictures don't do it justice. ﻿ it's a work of art. 


---
**Eric Lien** *March 22, 2015 12:34*

And like all printers: a source of fun and frustration all rolled into one ;)


---
*Imported from [Google+](https://plus.google.com/+JeffDeMaagd/posts/WoimMJksNng) &mdash; content and formatting may not be reliable*
