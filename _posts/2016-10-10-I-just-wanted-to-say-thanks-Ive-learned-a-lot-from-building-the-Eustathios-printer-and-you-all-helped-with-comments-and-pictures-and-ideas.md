---
layout: post
title: "I just wanted to say thanks. I've learned a lot from building the Eustathios printer and you all helped, with comments and pictures and ideas"
date: October 10, 2016 23:42
category: "Show and Tell"
author: Rick Sollie
---
I just wanted to say thanks.  I've learned a lot from building the Eustathios printer and you all helped, with comments and pictures and ideas.  I've taken what I've learned and moved in  a slight different direction and built a small vacuum former.   I know this is way off topic, and I can easily guess what the comments will contain ;)   I've got information on the build on github ([https://github.com/RickSollie/Fornax-Vacuum-Former](https://github.com/RickSollie/Fornax-Vacuum-Former)) if you are interested.  



<b>Originally shared by FamiLAB</b>





**Rick Sollie**

---
---
**Eric Lien** *October 11, 2016 00:53*

Very cool project. And cool to put a face and a voice to the name :)



Congratulations on a fast and successful build. And thanks for sharing your files on Github for other to mix/match/modify to fulfill their needs.


---
**Rick Sollie** *October 11, 2016 00:57*

**+Eric Lien** thanks !


---
**Ted Huntington** *October 11, 2016 04:19*

yeah really cool- we are just scratching the surface of all the low cost machines that could be made open source - like a vacuum chamber with remote control 2 part plastic pouring device, injection molding, cameras, robots, ...the future looks bright for low cost, well designed, open source builds.


---
**Mike Miller** *October 14, 2016 13:40*

That's awesome! (And just reinforces my want for a laser cutter. Oh, if I only had the space.)


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/jRD9k17rRHp) &mdash; content and formatting may not be reliable*
