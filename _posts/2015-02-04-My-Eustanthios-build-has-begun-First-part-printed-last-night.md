---
layout: post
title: "My Eustanthios build has begun! First part printed last night!"
date: February 04, 2015 00:56
category: "Show and Tell"
author: Isaac Arciaga
---
My Eustanthios build has begun! First part printed last night! **+Eric Lien** 's V4 carriage.

![images/9bc6140503a8f2e2e2e38e90adfee28b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9bc6140503a8f2e2e2e38e90adfee28b.jpeg)



**Isaac Arciaga**

---
---
**Eric Lien** *February 04, 2015 01:45*

Nice. Is that pet+?


---
**Isaac Arciaga** *February 04, 2015 01:54*

Yes, PET+ Ruby Red. Hard to do a full plate though. It oozes quite a bit.


---
**Jean-Francois Couture** *February 04, 2015 02:50*

That looks really good ! I like the color


---
**Isaac Arciaga** *February 04, 2015 03:14*

**+Oliver Schönrock** with support. The photo of it right after the print didn't look that great [http://i.imgur.com/6JlwDgm.jpg](http://i.imgur.com/6JlwDgm.jpg)


---
**Isaac Arciaga** *February 04, 2015 03:51*

The gantry will be in PET+. Everything below will be in carbon fiber ABS.


---
**Isaac Arciaga** *February 04, 2015 04:47*

**+Oliver Schönrock** I used Slic3r 1.2.4 (experimental). There's a newer one out now. Support and slicing times have vastly improved.


---
**Isaac Arciaga** *February 04, 2015 07:57*

**+Oliver Schönrock** The support inside the cable tie slots weren't difficult to remove at all. I was expecting to use my dremel for the tie slots but wasn't needed. Just a needle nose and a quick swipe with a blade to clean it up. Everything else peeled off without using tools. I wouldn't be able to do this on 1.1.7 Stable with PET+. Weird though, I told slic3r to not support bridges and it still did.



Selectively adding or removing support like S3D would be great. Just being able to disable supporting holes would be a great start.


---
**Isaac Arciaga** *February 23, 2015 02:27*

Hi **+Anan Leelasmanchai** it's PEI (Ultem) sheet. [http://amzn.com/B00CPRDDLY](http://amzn.com/B00CPRDDLY)


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/HWAjTa8uNQF) &mdash; content and formatting may not be reliable*
