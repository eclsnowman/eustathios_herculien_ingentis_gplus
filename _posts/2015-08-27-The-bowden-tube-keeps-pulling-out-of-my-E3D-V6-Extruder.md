---
layout: post
title: "The bowden tube keeps pulling out of my E3D V6 Extruder"
date: August 27, 2015 16:31
category: "Discussion"
author: Brandon Cramer
---
The bowden tube keeps pulling out of my E3D V6 Extruder. Did I wear out the piece that holds the tube in place?





**Brandon Cramer**

---
---
**Frank “Helmi” Helmschrott** *August 27, 2015 16:38*

I guess you have the pushfit on top of your extruder with the black plastic ring that you push down to get the tube in? If so, check your tubes diameter - i'm still debugging my extrusion/retraction issues and found out that 4,02mm outer diameter of the bowden tube may not be enough in some situations - my new one has 4,10mm and does fit much better. I know why i hate these pushfits.


---
**Jim Wilson** *August 27, 2015 16:44*

The tube itself will wear out and no longer be grippable, but snipping a smidge off the tube length would correct that.


---
**Brandon Cramer** *August 27, 2015 16:54*

**+Frank Helmschrott**  I do have the pushfit. Not a fan of this.... The tubing has a 3.90mm outside diameter. 



**+Jim Wilson** I did clip off some of the tube but it still pops out. 


---
**Martin Bondéus** *August 27, 2015 17:13*

The diameter of the PTFE-tube is critical for the gripping force.

See if you can get a hold in a metric size if you have the imperial one today **+Brandon Cramer** 


---
**Eric Lien** *August 27, 2015 17:17*

Weird, I have been running the same tube in my setups for a year plus without any issues. Since moving to push fits from trapped nuts I have never had a tube push out.


---
**Brandon Cramer** *August 27, 2015 17:34*

If the nozzle was too close to the bed, do you think it would push it out?



What is the Z calibration process for the Eustathios? I'm not 100% clear on that. I was having trouble with my prints sticking to the 3D-EEZ film. So I though maybe my calibration wasn't set correctly. 


---
**Brandon Cramer** *August 27, 2015 17:44*

Turning off my Eustathios Spider until I get the parts I need. I don't think this tubing is large enough or the bowden coupling is worn out. 



It just printed a job fine and when I went to print the same job a second time the tube pushed out. Argh!


---
**Eric Lien** *August 27, 2015 17:58*

Are you using that printed clip under the Bowden tube fitting. It helps keep the tube pre-tensioned.



[http://www.bondtech.se/STL/Bowden-clip%201.75.STL](http://www.bondtech.se/STL/Bowden-clip%201.75.STL)


---
**Brandon Cramer** *August 27, 2015 17:59*

That might be part of my problem if that is needed. 


---
**Eric Lien** *August 27, 2015 18:09*

Not needed just recommended. also if it is pushing out you may have too much back pressure. This can be from being too close to the bed on the first layer, or too low a temp for the speed. I tend to run a thicker first layer than the rest of my print, and up the extrusion width on the first layer for better adhesion.



For leveling I set the corners with a sheet of paper just under the nozzle with everything at temp. Then I adjust the height so the nozzle just starts to grab the paper. I go around two time, once to set it, then once to confirm changes to one had no effect on the others. 



Once it is set I don't have to re-level unless I change printing settings a lot (temp affects parts because aluminum grows for every degree hotter).


---
**Brandon Cramer** *August 27, 2015 18:38*

I printed the clip. I also leveled the bed. So far the clip is holding the tube in. 



I just want to print and walk away.... and come in the next morning and see the print job complete. :)


---
**Eric Lien** *August 27, 2015 20:28*

Another trick is to always set the level by tightening the screw to the final position, not loosening it into its position. Otherwise slop in the screw/spring/bed leveler may release afterwards since a machine screw/nut have slop.﻿


---
**Frank “Helmi” Helmschrott** *August 28, 2015 04:29*

**+Eric Lien** have you ever used a caliper to measure your tube? I didn't ever think that this could make such a difference but when having a tube with a slight oversize (4,10 is mine) it fits <i>much</i> better. At least in this little sunken pushfits like in the E3D and in the Bondtech extruder. My old one still was over 4mm but didn't fit quite as well. Even though i had the clips in place and always had a special eye on pressing the whole tube into the extruder and hotend down until the end i had slight movement (really slight) of the tube when retracting. I didn't do so much testing yesterday but the first print already showed that i didn't have to stop prints where they previously failed after clogging the hotend. My investigation is still going on but this is like the most probable cause yet.


---
**Eric Lien** *August 28, 2015 05:20*

**+Frank Helmschrott** mine is 4.00 spot on. From robotdigg. One thing it is the more white Teflon tube (not clear) which I think is a harder material.


---
**Frank “Helmi” Helmschrott** *August 28, 2015 05:52*

**+Eric Lien** Actually i do have this non-clear, harder tube in place now, It's the one that E3D used to ship with their hotends earlier (i think they don't include it anymore but not sure currently). I had one around that had about the right length for the Eusthatios and that seems to be more stable. I still have to test another tube that I have here which is also around 4,10mm but clear and a bit softer. I think it's more a thing of the diameter not so much about the material hardness but i could be wrong of course.



I will open a different thread or continue my old one to not clog this thread with my case but on a short note this does look much better now. will test the problematic esun PETG later today to get a proof.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/cj8STKVtv6Z) &mdash; content and formatting may not be reliable*
