---
layout: post
title: "I'm going to buy a heated bed from aliexpress and was wondering the exact specs this I what I'm thinking 330mm 700 watts Ac or DC not sure?"
date: December 13, 2014 15:16
category: "Discussion"
author: Ethan Hall
---
I'm going to buy a heated bed from aliexpress and was wondering the exact specs this I what I'm thinking

330mm²

700 watts

Ac or DC not sure?

And I'll also need an SSR to match. Any ideas?





**Ethan Hall**

---
---
**Bijil Baji** *December 13, 2014 15:27*

cant you get aluminum plate and cartridge heaters for heat bed??

if you go for that you can even get larger sizes.


---
**Ethan Hall** *December 13, 2014 15:29*

With the silicone heated bed I can go as large as I need and it will have better heat spreading than having several hot spots with heat gradients around them

﻿


---
**Bijil Baji** *December 13, 2014 17:03*

thats true


---
**Eric Lien** *December 13, 2014 20:35*

Go 120v, and 700w is likely overkill for 330x330. I would say 500w would be the sweet spot for that size. I went 800w for 380x380 but had to tune down max output in marlin to avoid overshoot on pid tuning. I found an equivalent of 700w would have been perfect for the 380x380 size.



Just my 2cents.﻿


---
**Tim Rastall** *December 14, 2014 04:10*

Going for 24v is a reasonable compromise as the lower current required should allow you to use standard reprap electronics without an ssr or risk electrocution by wiring a moving part up with mains power.:). 


---
**Ethan Hall** *December 14, 2014 04:12*

**+Tim Rastall** so for a regular ingentis 330mm² is the proper size right?


---
**Tim Rastall** *December 14, 2014 04:16*

I used 300mm if I recall but 330 should be fine.  The principal constraint is the distance between the Z shafts which is 370 I think.


---
**Florian Schütte** *December 17, 2014 11:51*

I ordered a custom one 340x340mm 550W. Powerred directly by 230V main power.


---
**Ethan Hall** *December 17, 2014 11:53*

**+Florian Schütte**​ so is that AC or DC﻿


---
**Florian Schütte** *December 17, 2014 12:04*

AC Main power regulatrd by ssr


---
**Daniel F** *July 09, 2015 13:09*

**+Florian Schütte** how is your heater performing? How fast does it heat up? I Followed this an other posts about sizing the silicone heater and it turns out that 0.47 to 0.5W per cm^2 should be the right size.

I plan to heat a 310x310mm, 6mm thick aluminium bed with a 290x290mm silicone heater @230V. I'll go for 450W.


---
**Eric Lien** *July 09, 2015 14:43*

Sounds good. I am still using my old 24v 400w one and heatup on a 3mm plate is fast. AC with 450 watts would be plenty.﻿


---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/NhBaLPcgr4d) &mdash; content and formatting may not be reliable*
