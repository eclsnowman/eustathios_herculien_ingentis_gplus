---
layout: post
title: "Dam this thing looks nice hopefully I'll have it printing today knock on wood"
date: May 23, 2015 16:40
category: "Show and Tell"
author: Derek Schuetz
---
Dam this thing looks nice hopefully I'll have it printing today knock on wood



![images/0781c4f4cc0b1633c018aafe6f2ee192.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0781c4f4cc0b1633c018aafe6f2ee192.jpeg)
![images/22275789eaa769d8c7001fee2298701a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/22275789eaa769d8c7001fee2298701a.jpeg)

**Derek Schuetz**

---
---
**Mike Miller** *May 23, 2015 17:01*

Printing? yes. Calibrated? Not if it's your first printer. :D  #AskMeHowIKnow  


---
**Derek Schuetz** *May 23, 2015 17:20*

Not my first I think this is number 5 in my built list


---
**Eric Lien** *May 23, 2015 18:31*

Let me know how the side panels enclosing things works. If you have a good design to get around the motors I will get it added to the github for others.



Great looking printer. Now I am jealous. I still have a Eustathios V1.5 hybrid.


---
**Derek Schuetz** *May 23, 2015 19:46*

Ya I was gonna have them laser cut but I decided to just get the basic squares and then make the cuts and measurements around motors by hand


---
**Gus Montoya** *May 25, 2015 19:49*

In terms of the acrylic covering, wouldn't it be sufficient to cover the top portion and leave the sides alone? The purpose of the covers is to keep heat in correct? 


---
**Derek Schuetz** *May 25, 2015 20:21*

I was looking more to block drafts


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/8etxqPSw1We) &mdash; content and formatting may not be reliable*
