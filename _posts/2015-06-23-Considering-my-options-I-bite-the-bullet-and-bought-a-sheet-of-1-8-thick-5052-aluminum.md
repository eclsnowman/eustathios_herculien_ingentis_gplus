---
layout: post
title: "Considering my options I bite the bullet and bought a sheet of 1/8\" thick 5052 aluminum"
date: June 23, 2015 02:43
category: "Discussion"
author: Gus Montoya
---
Considering my options I bite the bullet and bought a sheet of 1/8" thick 5052 aluminum. I think it's enough to cut out 3 build plates with some material to spare. Anyone needing a build plate for their Eustathios V2? After I have them cut I'll post pic's and I'll also have a price. ﻿

![images/48f13bc6c1901824192f6c63d0ffea1b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/48f13bc6c1901824192f6c63d0ffea1b.jpeg)



**Gus Montoya**

---
---
**Erik Scott** *June 23, 2015 02:57*

I would very much like one. I will be getting a CNC router soon, but not immediately. My printer re-build could be done much sooner if I had the plate now. 


---
**Gus Montoya** *June 23, 2015 02:59*

I'll try to get it cut tomorrow.


---
**Dat Chu** *June 23, 2015 13:13*

Can you cut one for Herculien? 


---
**Gus Montoya** *June 23, 2015 20:39*

Not sure if I can Dat, I'll prep the sheet and see what I can do. But I'm not making any promises I can't keep.


---
**Dat Chu** *June 23, 2015 20:55*

Sure **+Gus Montoya** , thank you for sharing with the community. Let me know what you find out.


---
**Gus Montoya** *June 30, 2015 20:11*

So I found a place that can use a plasma cutter to cut the build plate. Some minor filing will be required when you receive the build plate. I can fit 3 Eustathios or 2 Eustathios and 1 herculein build plate. Total comes out to  $49 plus shipping each. I am located in San Diego so naturally the closer you are the cheaper the shipping.


---
**Erik Scott** *June 30, 2015 20:14*

That's pretty reasonable! 


---
**Gus Montoya** *June 30, 2015 20:35*

I need one more person for either an Eustathios or herculein. Anyone else? Given that the plasma cutter is fully digital, the cuts will be spot on. Screw holes will be included for exception of counter sinking. That will be done by you.  @Erik  your in for a Eustathios correct?


---
**Erik Scott** *June 30, 2015 21:03*

Yes, an Eustathios bed for me, please!


---
**Gus Montoya** *June 30, 2015 23:40*

Thank you Erik, **+Dat Chu**  are you in? Or will you use your supplier? 


---
**Gus Montoya** *July 01, 2015 00:16*

**+Erik Scott** Sheet metal dropped off. Will pick up tomorrow. I hope to have it mailed out Thursday. Can you send me your address? I'll have a final price for you after I get the peices and boxed, weighed etc.


---
**Dat Chu** *July 01, 2015 01:05*

**+Gus Montoya** I will go with the supplier on the BOM since I need the aluminum angles as well. Thanks for organizing this and especially for checking back with me.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/ZyQKaAoPmHp) &mdash; content and formatting may not be reliable*
