---
layout: post
title: "Here's my take on that xy end conversation"
date: March 14, 2014 11:01
category: "Deviations from Norm"
author: Tim Rastall
---
Here's my take on that xy end conversation. Started from scratch with due consideration of the things we discussed.





**Tim Rastall**

---
---
**Eric Lien** *March 14, 2014 12:06*

I like it. Clean and simple. BTW what do you model in. Your renders are always beautiful.


---
**Jarred Baines** *March 14, 2014 12:53*

Anyone else have issues with imgur website on android? Never works here, page loads with blank image...



Looks good in the thumbnail though Tim ;-) your renders really are crisp and clean :-)



The ideal position for the spectra is on the same plane as the rods (meaning it pretty much has to go on the outside) but that's ideal for stability, for footprint and motor positioning, I think yours might be a good bet!



Are you thinking of trying it? No going back once the rods are on size unfortunately!


---
**Jarred Baines** *March 14, 2014 12:54*

How were the bushings held into the original design? Just press fit?


---
**Jarred Baines** *March 14, 2014 14:03*

Jeez! The spectra even fades off into the distance! That's great! Do you set up camera's and environments and stuff? Pro stuff :-)



Oh I see that the split in this is just for the spectra and not to clamp the bush, looked a bit different as a thumbnail on my phone! 



Also, are the 2 bars at the same height there or slightly offset?


---
**Liam Jackson** *March 14, 2014 14:08*

**+Jarred Baines** no problem here, I use chrome for Android in desktop mode though. 


---
**Jarred Baines** *March 14, 2014 14:16*

I'll try out desktop mode next time - I also use chrome ;-) Cheers **+Liam Jackson** 


---
**Tim Rastall** *March 14, 2014 17:57*

**+Eric Lien** It's modelled in Rhinoceros with vray render. Simple environment setings with 1.4mm aperture camera depth of field enabled.  It's all vray really,  awesome renderer. 




---
**Tim Rastall** *March 14, 2014 18:01*

**+Jarred Baines** the bars are 3mm offset,  that's the way the need to be so I don't  have to redesign the carriage :). 

Yep,  I'm going to print it with Bridge,  today if I get the chance.  The bushings are heated then press fit. 


---
**Tim Rastall** *March 14, 2014 18:03*

Oh,  I should have mentioned,  It will easily convert for belts too.


---
**Riley Porter (ril3y)** *March 15, 2014 00:05*

Did you see how **+Jason Smith**  did the tensions in this part?  Its great cause you can get ultimaker belt results with a cut to fit length (0pen belt)


---
**Tim Rastall** *March 15, 2014 00:11*

Her's another angle [http://imgur.com/TFogG1K](http://imgur.com/TFogG1K)


---
**Tim Rastall** *March 15, 2014 00:13*

**+Riley Porter** Yeah. Although this version is for belts. The method Jason used to clamp the belts is pretty well established one unless I missed something. I've got the belts and pulley loaded on the model in Rhino - Just need to add the clamps to this version of then ends.

You know that Ultimaker use open loop belts too eh?


---
**Riley Porter (ril3y)** *March 15, 2014 00:22*

No I was saying that the way his "remake of yours X Y slide" ie part above, allowed to tension the belts by 2 bolts

its 2 pieces that pull the belt together.  I like you we look.  If you could get it to make it so you could tension a belt too that would be perfect.


---
**Brian Gudauskas** *March 15, 2014 12:06*

**+Jarred Baines** use desktop mode to see it on Android I had the same problem, oops should've read all the comments first.


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/6jYntxx3vuk) &mdash; content and formatting may not be reliable*
