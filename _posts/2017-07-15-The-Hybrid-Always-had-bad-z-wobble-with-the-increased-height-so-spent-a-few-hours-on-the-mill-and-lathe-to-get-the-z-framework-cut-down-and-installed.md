---
layout: post
title: "The Hybrid. Always had bad z wobble with the increased height, so spent a few hours on the mill and lathe to get the z framework cut down and installed"
date: July 15, 2017 03:09
category: "Show and Tell"
author: Rick Sollie
---
The Hybrid.

Always had bad z wobble with the increased height, so spent a few hours on the mill and lathe to get the z framework cut down and installed.  The aluminum L brackets were shipped bent so some delays.

![images/8e5b312683b5b96b5bcc00be2727c25b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8e5b312683b5b96b5bcc00be2727c25b.jpeg)



**Rick Sollie**

---
---
**Buhda “Pete” Punk** *November 14, 2017 18:06*

I assume the wobble is from frame-flex. have you considered X members and maybe some weight on the base (plate steel).  Reason I ask is I am cloning a CR-10, shooting for 24" cubed


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/Y5FJ2n6yKcn) &mdash; content and formatting may not be reliable*
