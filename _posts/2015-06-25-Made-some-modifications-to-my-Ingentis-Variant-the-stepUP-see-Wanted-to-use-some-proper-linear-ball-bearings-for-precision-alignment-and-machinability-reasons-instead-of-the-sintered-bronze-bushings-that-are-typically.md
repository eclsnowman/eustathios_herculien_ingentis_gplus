---
layout: post
title: "Made some modifications to my Ingentis Variant, the stepUP (see ) Wanted to use some proper linear ball bearings for precision, alignment, and machinability reasons instead of the sintered bronze bushings that are typically"
date: June 25, 2015 17:03
category: "Show and Tell"
author: Ishaan Gov
---
Made some modifications to my Ingentis Variant, the stepUP (see [https://plus.google.com/113675942849856761371/posts/fPm4NqVuJzZ](https://plus.google.com/113675942849856761371/posts/fPm4NqVuJzZ))

Wanted to use some proper linear ball bearings for precision, alignment, and machinability reasons instead of the sintered bronze bushings that are typically used, kind of like **+Tim Rastall**'s Procerus Build

Linear shafts are 565mm long x 10mm diameter chromed steel with SC10UU linear bearings riding on them. The shafts that span the X-Y axes are AWMP-08 ceramic coated aluminum shafts by **+igus Inc.** riding on JSM-0810-16 plastic bushings. Rotating shafts are low-precision 8mm Aluminum rods on KP-08 bearing pillow blocks. Belts can be tensioned by simply sliding the pillow blocks further away from each other. Each motor Will be driving ~400g total, but with 2:1 drive reduction



![images/e2426cf943f7eb6f94eff80f9cac40b1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e2426cf943f7eb6f94eff80f9cac40b1.jpeg)
![images/7df0bee7dec9827c8eaa6f6d484c78fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7df0bee7dec9827c8eaa6f6d484c78fa.jpeg)
![images/19876b6dadc189e9962e06fce46ab8a3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/19876b6dadc189e9962e06fce46ab8a3.jpeg)
![images/fa4b23f2992f34b20281cd78771745f5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fa4b23f2992f34b20281cd78771745f5.jpeg)
![images/a368b480fa663ca1349b4294ed397832.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a368b480fa663ca1349b4294ed397832.jpeg)
![images/ab3d5fee4fb1ce6864a771ec6001a586.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ab3d5fee4fb1ce6864a771ec6001a586.jpeg)

**Ishaan Gov**

---
---
**Vic Catalasan** *June 25, 2015 17:45*

Very nice mods, I like the idea of lightweight aluminum rods for driving the gantry. I am not a fan of roller bearings at all so I am replacing mine with 3 ballscrews to drive the Z gantry and just use shafts with bearings to isolate the ballscrews movements.



Have you thought about using guide rails instead of linear shaft for the xy gantry?like these [http://www.hiwin.com/pdf/lg/0809/Hiwin%20Linear%20Guideway%20Catalog_G99TE13-0809.pdf](http://www.hiwin.com/pdf/lg/0809/Hiwin%20Linear%20Guideway%20Catalog_G99TE13-0809.pdf)



You can get them with lower profile and thus give you a larger print envelope. I am using larger 30mm guide rails on my CNC aluminum mill and they work excellent, I was thinking 10mm rail width or even less should be fine on a 3d printer.



 


---
**Ishaan Gov** *June 25, 2015 18:33*

**+Vic Catalasan**, Thanks for your feedback

Took a quick look at some MR9 and MGN9C rails and carriages; they do look good, but are kinda pricey, about $50 more fully implemented than the 10mm linear shafts. I also had those aluminum shafts for the span sitting around at home, and think those will be the ones to limit the print volume before the linear guides do.

One of my friends is actually is using some THK made MR and MGN linear guides on his 5-Axis that he's building, and they look SUPER nice


---
**Øystein Krog** *June 25, 2015 19:06*

It looks like you have everything (motors) etc contained within the outer frame bounding box, is that correct?

Are you planning to enclose it?


---
**Ishaan Gov** *June 25, 2015 19:11*

**+Øystein Krog**, Most of the electronics (except motors, heaters, etc) will be outside the build area, so yeah, hoping to enclose it soon after it gets finished; something like Dibond or some other aluminum composite panel


---
**Øystein Krog** *June 25, 2015 19:13*

Cool, I'm really craving an enclosure, makes your design more interesting to me :P


---
**Ishaan Gov** *June 25, 2015 19:15*

**+Øystein Krog**, thanks! It does require some milling and CNC'ing of aluminum and lathing some leadscrews, but if you have access to those tools, it should definitely be possible. I may work on a version that is more tool-friendly, shouldn't be too hard


---
*Imported from [Google+](https://plus.google.com/113675942849856761371/posts/TBcZ1WwA7W5) &mdash; content and formatting may not be reliable*
