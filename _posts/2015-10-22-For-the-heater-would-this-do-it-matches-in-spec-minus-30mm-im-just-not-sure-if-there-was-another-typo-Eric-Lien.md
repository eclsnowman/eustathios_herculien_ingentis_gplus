---
layout: post
title: "For the heater. would this do? it matches in spec minus 30mm im just not sure if there was another typo Eric Lien"
date: October 22, 2015 00:37
category: "Discussion"
author: Jim Stone
---
For the heater. would this do? it matches in spec minus 30mm

[http://www.ebay.ca/itm/281658618264](http://www.ebay.ca/itm/281658618264)



im just not sure if there was another typo **+Eric Lien** 





**Jim Stone**

---
---
**Ray Kholodovsky (Cohesion3D)** *October 22, 2015 01:09*

Keenovo is good, I had them custom make me heaters for my printers. Other people should weigh in on whether AC is a good idea here, you'd need an SSR to switch it. 24v would be fine for this size. I'm running slightly smaller ones on 12v which is probably not a good idea for this size but 24v dc would be ok. 


---
**Jim Stone** *October 22, 2015 01:13*

how would i run it on 24 v with ramps?

or even the azteeg. they are both 12v...right?


---
**Joe Spanier** *October 22, 2015 01:59*

120v is totally worth it. The ssr makes for much less stress on your controller and your bed will heat in a couple minutes. 


---
**Jim Stone** *October 22, 2015 02:00*

So the bed connects to the ssr and the 12vdc psu? But how can a 800w mat be powered by a 450w psu....wait...is it a voltage difference? 


---
**Jim Stone** *October 22, 2015 02:00*

How does it even get 110v


---
**Joe Spanier** *October 22, 2015 02:20*

It's wired to mains. On mine its in parallel to the power supply. The positive leg is switched with the ssr. Negative goes back to the neutral leg. 


---
**Jim Stone** *October 22, 2015 02:27*

Ahhh I get it. I also looked at the wiring diagram. Didn't think I would seen it there. Makes sense now


---
**Eric Lien** *October 22, 2015 03:38*

**+Jim Stone**​ I custom ordered mine from alibrother aka alirubber on aliexpress. Cost less than $35 if I recall. Just contact Daisy Shi at Alirubber. They even added a second thermistor and 1.5 meter leads at no charge.


---
**Ray Kholodovsky (Cohesion3D)** *October 22, 2015 03:42*

**+Eric Lien** Time for me to do that.  Keenovo is expensive!  Any problems with smell?  I recall a conversation regarding initial burn in of the silicone, or lack thereof?


---
**Jeff DeMaagd** *October 22, 2015 03:46*

I don't recall any kind of burning smell when firing up my silicone heat bed. I think you want to stick it to whatever plate it will be stuck to before firing it up, the protective waxy paper backing for the adhesive might not like the temperature.﻿


---
**Eric Lien** *October 22, 2015 05:30*

No undercured silicone smell on mine. I have ordered 4 from them now. Every one was great.


---
**Joe Spanier** *October 22, 2015 13:41*

You've done Keenovo Eric? We just had two more beds delaminate for the supplier we went through a while back. I think that was 5 failures out of 22. I think paying a bit for Keenovo is worth it if the quality is there. 


---
**Ray Kholodovsky (Cohesion3D)** *October 22, 2015 14:23*

**+Joe Spanier** I think Eric's last statement was in response to my inquiry about Ali Brother...

What happened to yours? Did the adhesive come off the silicone? The silicone itself separate? 


---
**Joe Spanier** *October 22, 2015 14:27*

silicone separated when they heated. 2 Started smoking pretty quickly. Kinda scary. All had a silicone smell. I posted about it a while back. 


---
**Ray Kholodovsky (Cohesion3D)** *October 22, 2015 14:30*

Yes, and I asked you the same questions I am asking now in that thread. 


---
**Eric Lien** *October 22, 2015 14:32*

**+Joe Spanier** that's so odd, I have never had anything but great luck with Alibrother, but I am sure China QC is less than ideal. I guess mileage may vary. But mine have been printing for a few years now with zero issues. Then again I only ever run 120V heaters. Were yours DC or AC that failed?


---
**Joe Spanier** *October 22, 2015 14:43*

Mine werent Alibrother. They were from Amy's Paradise. Such strange names. Anyway the person that did the group buy had a bad experience with one from Alibrother and refused to try again. So we got this crap. They were all 120v. 


---
**Jean-Francois Couture** *October 22, 2015 14:57*

Also got my 2 heaters from Keenovo.. both 230mmx320mm 500W@110V(runs at arount 550W because its actually 120V on the mains line.) Both still work great. paid 100$CAN in total.. about 2 years ago.



I should point out that, you should put a thermal fuse inline with the mains line (install the fuse on the bed and make good contact. the reason is that, #1 a SSR can short out. and #2, your on main line. those heat up real hot on continous power feed ;)


---
**Orrin Naylor** *October 22, 2015 16:59*

I have one and it works great. You do have to use an SSR and run AC through it. Here is a good guide on solid state relays: 
{% include youtubePlayer.html id=TiEwNf1H_Tc %}
[https://www.youtube.com/watch?v=TiEwNf1H_Tc](https://www.youtube.com/watch?v=TiEwNf1H_Tc)


---
**Jim Stone** *October 22, 2015 21:45*

anyone able to help with these two springs? mcmaster carr wont ship to me period. unless i can provide a tax number and an import liscence :(



and finding the right springs seems to be incredibly difficult



Spring (0.75"L x 0.187" OD x 0.029" Wire) 

Spring (0.75"L x 0.50"OD x 0.062"wire)



i know everyone here couldnt have used mc master :P


---
**Chris Brent** *October 22, 2015 23:32*

**+Jim Stone** Get them shipped to me in California and I'll ship them to you? Be forewarned though they don't quote shipping until <b>after</b> you order (they ship from different locations based on stock I think) so it's worth ordering a reasonable amount from them. Nothing worse than paying more in shipping than the parts cost!


---
**Eric Lien** *October 22, 2015 23:38*

**+Jim Stone** I can get them from the local hardware store. PM me your address, I can send you some.


---
**Jim Stone** *October 23, 2015 00:53*

**+Eric Lien** Did i PM you? im not sure. im not used to using google+ haha


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/JoGZbswN2FW) &mdash; content and formatting may not be reliable*
