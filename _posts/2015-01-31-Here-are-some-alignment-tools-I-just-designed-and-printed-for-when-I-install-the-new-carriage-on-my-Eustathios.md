---
layout: post
title: "Here are some alignment tools I just designed and printed for when I install the new carriage on my Eustathios"
date: January 31, 2015 01:15
category: "Discussion"
author: Eric Lien
---
Here are some alignment tools I just designed and printed for when I install the new carriage on my Eustathios. The vertical-alignment/squaring/paralleling of the components is the trickiest part if you ask me. Hopefully this can help others, and I am hoping they help me as well. 



STL files are here: [https://drive.google.com/folderview?id=0B1rU7sHY9d8qVnp2WWxxZTFCNVU&usp=sharing](https://drive.google.com/folderview?id=0B1rU7sHY9d8qVnp2WWxxZTFCNVU&usp=sharing)



The large red and blue parts are for setting the rods at consistent heights at the corners, the smaller bars are for setting the cross bars parallel to the side rods when tightening on the pulleys after you tension the drive belts.



I modeled the corner levelers around the components from my solidworks assembly. So I can only guarantee they should work for the files out on my github. I included bolt holes that may help hold things even more ridged during assembly. Lets hope they work.﻿



![images/3ae313a0028363541b974a56b2cf4024.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3ae313a0028363541b974a56b2cf4024.jpeg)
![images/b22b31777b9d4a0cbdbe90b1148399b6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b22b31777b9d4a0cbdbe90b1148399b6.jpeg)

**Eric Lien**

---
---
**Isaac Arciaga** *January 31, 2015 01:26*

Thank you **+Eric Lien** !


---
**Mike Thornbury** *January 31, 2015 02:54*

I found a digital protractor to be the most helpful tool for squaring: [http://www.aliexpress.com/item/Stainless-Steel-2in1-Digital-Angle-Finder-Meter-Protractor-Gauge-Scale-Ruler-360-degree-400mm-with-Moving/2044728284.html](http://www.aliexpress.com/item/Stainless-Steel-2in1-Digital-Angle-Finder-Meter-Protractor-Gauge-Scale-Ruler-360-degree-400mm-with-Moving/2044728284.html)


---
**Simply7** *January 31, 2015 13:25*

This is going to come in very handy this week!


---
**Gus Montoya** *February 08, 2015 01:05*

great work!


---
**Dat Chu** *February 16, 2015 00:54*

Awesome. Can you include a pix using the small spacer?


---
**Eric Lien** *February 16, 2015 01:35*

Small spacer is for alignment of the cross rods to the side rods. Install the pulleys, belts, tensioners, etc but leave the pulleys free wheeling on the rods. Then use the small spacer to make the cross bar parallel with the side rod. Then push the pulley up tight against the bearing and tighten the set screws. It is important that the pulley is up tight against the bearing.



But this spacer is for Eustathios 8mm rod and 10mm rod. This won't work for HercuLien which is 10mm rod all around.


---
**Rick Sollie** *April 10, 2015 01:06*

**+Eric Lien**

 Can this be uploaded the Github?  Had a lot of scroll back to find it :)


---
**Eric Lien** *April 10, 2015 03:24*

**+Rick Sollie** done


---
**Simply7** *April 10, 2015 10:18*

**+Eric Lien** thanks!


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/HRkZe5NuH8s) &mdash; content and formatting may not be reliable*
