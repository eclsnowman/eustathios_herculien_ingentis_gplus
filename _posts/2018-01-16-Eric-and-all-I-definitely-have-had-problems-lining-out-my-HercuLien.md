---
layout: post
title: "Eric and all, I definitely have had problems lining out my HercuLien"
date: January 16, 2018 01:26
category: "Discussion"
author: Bruce Lunde
---
Eric and all, I definitely have had problems lining out my HercuLien.  I have had it built for quite a while, and I have had some luck getting small prints to be successful but they are far from perfect. I have asked a few questions now and again trying to get it better, but I still am not where I can get a print without a lot of attempts.



Some of my challenges were with the hotend I chose (e3d volcano), and I updated from 12 to 24 volts for the cartridge heater thinking that was the my problem.   The components of that assembly have given me fits, and I have taken it apart a couple times trying to get the pieces to fit better, to limited success. The same is true for the drive mechanism for the   filament feed. It really does not fit  as well as I had hoped, and I have to fiddle with it for every print attempt.  Both of these items are more likely due to the printer that produced then, no blame to the design!



I constantly have had feed problems, and changed the gears a couple times trying to get better feed. I also have had problems with the feed, where one end of the tubing at the feed side, or extruder side come free, which may have been part of the problem with jamming at the hot end, but it happens so often it causes me to throw up my hands and head back to the woodworking tools, lol.  Note that I have not given up, I just circle back and try another approach to trouble shhoting it.



I feel like I am really close, and my goal is to print some big parts for my robotics and IoT electronics.  I did chime in on a thread to ask about the Bondtech units, and ordered the BMG unit today.  My hope is to replace the existing filament drive unit to eliminate that problem. I got the Bowden adapter and will try it on the side where my existing unit is, but I also wonder if it could be mounted directly with the extruder?  here are a couple pictures of my problem areas.  Any questions are welcome, as I want to get this going and am not sure what to look at next. 



![images/e81beee87e25d6f6a3fb5e68fd950edb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e81beee87e25d6f6a3fb5e68fd950edb.jpeg)
![images/787ee8886a7750e5bc9e80ddac61bae1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/787ee8886a7750e5bc9e80ddac61bae1.jpeg)

**Bruce Lunde**

---
---
**Eric Lien** *January 19, 2018 01:49*

Sorry  for the delay getting back to your post. I am out of town working long hours for work.



Yeah the HercuLien uses an old method of a threaded nut over the Teflon tube. It works so long as you don't jam if you do it can push out. The better way (by todays standards) is to integrate the John Guest barb fitting (called a half cartridge fitting) like is used on the E3D and Bondtech components.



I think you will have great luck with the BMG. Bowden tubes take a while to dial in settings... But to be honest once you do they are just as good as direct Drive in my experience. I have no blobs or printing issues using my Bondtech extruders even with my meter long Bowden tube. Only thing you can't do is flexibles in a 1.75mm Bowden system with that long a feed.


---
**Bruce Lunde** *January 19, 2018 02:26*

**+Eric Lien**  Thanks, the BMG unit is due in any day and I will feed back how it is going.  I will also look into the barb fitting to be sure that I don't knock the tubes out again! 


---
**Eric Lien** *January 19, 2018 10:27*

You can get them at the link below. You just slightly undersize the hole, then use a soldering Iron to insert the brass section into the plastic housing. Then the barb pushes down into the brass ring and you have a very strong connection.



[smw3d.com - E3D Embedded Bowden Coupling](https://www.smw3d.com/e3d-embedded-bowden-coupling/)


---
**Bruce Lunde** *January 22, 2018 01:37*

**+Eric Lien** Thanks for that link, I have ordered some of those. I guess I missed that in the build. I was desperate enough to try superglue, of course it did not work.  Postal service  lost my new Bondtech for a few days, so I have not gotten that part taken care of, but did get an update that they found the package and I should have it tomorrow!



Will you be at the MIdwest Reprap fest this year?


---
**Eric Lien** *January 22, 2018 01:45*

**+Bruce Lunde** certainly. I have been going since 2014. I wouldn't miss it for the world.




---
**Bruce Lunde** *January 22, 2018 23:45*

**+Eric Lien**  i received my Bondtech BMG, and am installing. Question, did you use your geared stepper motor from the HercStruder on the BondTech, or did you install a regular Nema17 stepper on it?


---
**Bruce Lunde** *January 23, 2018 01:14*

After thinking about this, I am going with a regular nema-17 I have. Now I have to make a temporary mount, then I can print a better one when I am successfully printing.


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/5nT5Aok3DKj) &mdash; content and formatting may not be reliable*
