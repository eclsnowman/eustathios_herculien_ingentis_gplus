---
layout: post
title: "Finally managed to do it, PrintBite - a useful and cost effective print surface for all your printing needs is now available at "
date: October 05, 2015 12:46
category: "Show and Tell"
author: Mutley3D
---
Finally managed to do it, PrintBite - a useful and cost effective print surface for all your printing needs is now available at [http://www.mutley3d.com/PrintBite/](http://www.mutley3d.com/PrintBite/)﻿





**Mutley3D**

---
---
**Frank “Helmi” Helmschrott** *October 05, 2015 12:52*

hmm really sticks to nylon without additions? Sounds interesting then. Would it be possible to get 350x350mm? how would you stick it to a heated aluminum bed ideally? My printing area is only 265x265 but i need to stick it to the 350x350 plate somehow.


---
**Eirikur Sigbjörnsson** *October 05, 2015 12:53*

well I would need somewhat over 400x400mm :)


---
**Mutley3D** *October 05, 2015 13:51*

**+Frank Helmschrott** 350^2 no problem, sticks down with double sided acrylic tape like the 3M 468 adhesive sheets, but thats very expensive, im concious of costs, so I use double sided flooring tape for sticking linoleum flooring down. Available from any hardware store or major home depot. Its acrylic too, very sticky. Dont use carpet tape though, thats different. Ill be putting more detailed instructions onto the site next 24/48 hrs


---
**Mutley3D** *October 05, 2015 13:51*

**+Eirikur Sigbjörnsson** 400x400, no problem, but will increase cost, ill work out the price but it will be in line with what you see on the site.


---
**Bud Hammerton** *October 05, 2015 15:57*

What is the thickness of the Printbite sheet and can it be used as a substitute for the glass build plate of the Spider & HercuLien?


---
**Eirikur Sigbjörnsson** *October 05, 2015 21:17*

**+Mutley3D**  As I said, I'm a bit over 400x400mm. Something like 440x470mm :)


---
**Mutley3D** *October 06, 2015 03:37*

**+Bud Hammerton**

 Hi Bud, the thickness is 0.5mm and it needs to be applied to a flat printing foundation surface such as mirror tile.


---
**Mutley3D** *October 06, 2015 03:39*

**+Eirikur Sigbjörnsson**

 I've added a couple of new buttons for larger sizes, on this occassion hit the 400x400 button and ill happily do the oversize for you, but add a clear note to the transaction referencing this post, thanks.


---
**Frank “Helmi” Helmschrott** *October 12, 2015 08:43*

**+Mutley3D** your plates arrived this morning. that was quick, given the fact that it had to cross the gap between UK and central Europe :D



I've already sticked it to the aluminum plate just over the Buildtak plate i was using before and it seems to work well with the current PETG print. Will try Nylon next as this is what i really wonder how it works.



Do you give any temperature suggestions for your plate? Would be nice to have a rought orientation line here.


---
**Mutley3D** *October 12, 2015 15:03*

**+Frank Helmschrott** Wow that was quick, great to hear. Regarding temps, you may want to go a little warmer than your normal settings in general terms. So for ABS i use 110bed and 245 on the nozzle for bottom layer with the ABS I use. PLA I use slighty hotter nozzle aswell. Nylon go with your normal temps but bed at 60-70 (can print onto cold bed with nylon but with heat, you get some self release help. More instructions are here [http://mutley3d.com/gallery/printbite.pdf](http://mutley3d.com/gallery/printbite.pdf)


---
**Eirikur Sigbjörnsson** *October 12, 2015 15:07*

Still waiting for my order which I intend to test on the Ultimaker2 I have here. If things go well I will order for my other printers later


---
**Mutley3D** *October 12, 2015 15:13*

**+Eirikur Sigbjörnsson** Ahh thats a shame I prepped some UM2 sheets over the weekend and added the button on the site. If I had known i would have been able to cut yours for the UM2.

 


---
**Eirikur Sigbjörnsson** *October 12, 2015 15:21*

well, Im sure I'll manage somehow :)


---
**Frank “Helmi” Helmschrott** *October 12, 2015 15:49*

**+Mutley3D** I've already been able to print some nylon 618 vase but failed on some gears. Had the bed at 75deg which basically worked but then warped a bit. It also looks like the adhesion is rather light as they peel of if the nozzle just touches when striving across.. Maybe i'll add some temp to the nozzle and retry later.


---
**Mutley3D** *October 12, 2015 16:08*

add temp to nozzle and bed in that case - what were your settings on nozzle ??


---
**Frank “Helmi” Helmschrott** *October 12, 2015 16:11*

had played with the nozzle between 240 and 255 (E3D which always needs a few deg more).


---
**Mutley3D** *October 12, 2015 16:44*

OK go to 110 on the bed should do it


---
**Frank “Helmi” Helmschrott** *October 12, 2015 17:08*

ok will try that next time i'm printing.


---
**Eirikur Sigbjörnsson** *October 12, 2015 20:41*

I usually have the bed on 110 (using MDF plate) and nozzle temp at 228  on the Ultimaker2 for Nylon. 110 on the bed should do the trick for you


---
**Mutley3D** *October 18, 2015 01:56*

**+Frank Helmschrott** have you managed to have a further go with the nylon? let me know how you get along with it.


---
**Mutley3D** *October 18, 2015 02:01*

More testing of the PrintBite over the weekend with some new Polycarbonate filament from Polymaker. This material is super tough and strong, surprisingly tough in fact. And heres the good news :) it sticks to PrintBite and self releases when cold. Im actually surprised at this as Polymaker say it will only stick to Buildtak and in fact supply a small sheet with each roll. I did have a go with the Buildtak and it is now a permanent fixture. On the PrintBite it worked really well, Bed at 120 and nozzle at 275


---
**Eirikur Sigbjörnsson** *October 23, 2015 11:53*

OK so I have been testing this plate on the Ultimaker2 I have sccess to. Printed Nylon, PLA and ABS so far and I must say I'm impressed. I need to do some more tests later, but so far this seem to be a great surface and by far teh best surface I've tested. I just clamped the plate down, haven't glued it down permanantly yet, which explains slight problems with ABS, (the plate lifted a bit so one end of the print loosened from the bed at the end of the print). But I dodn' even have to use skirt or brim on the ABS which is a first for me (I was test printing the 2020 extrusion handle), so things are looking promising and I expect to use this on the Herculien when I start puting it together.


---
**Mutley3D** *October 24, 2015 00:19*

The feedback from PrintBite has been incredible in its first two weeks and has taken me a bit by surprise. Whilst I had no doubts about its performance, the reviews have been simply fantastic. Please spread the word :)


---
*Imported from [Google+](https://plus.google.com/+Mutley3D/posts/GfLrKf8nJZ8) &mdash; content and formatting may not be reliable*
