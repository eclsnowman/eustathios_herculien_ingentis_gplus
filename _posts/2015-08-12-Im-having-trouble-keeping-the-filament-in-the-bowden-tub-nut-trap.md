---
layout: post
title: "I'm having trouble keeping the filament in the bowden tub nut trap"
date: August 12, 2015 21:28
category: "Discussion"
author: Brandon Cramer
---
I'm having trouble keeping the filament in the bowden tub nut trap. Is the tubing supposed to just push in there? 



I have microstepping set at 1/16 for the extruder motor. Help???





**Brandon Cramer**

---
---
**Frank “Helmi” Helmschrott** *August 12, 2015 21:30*

Guess you're talking about the Hercustruder? I'm not using it but normally to connect the tubing to the nut you put a 2mm drill or similar into the end of the tube to keep it stable and then screw on the nut. It should cut into the PTFE tubing.



Hope that helps!


---
**Brandon Cramer** *August 12, 2015 21:32*

**+Frank Helmschrott** Thanks, I missed that part. Right after posting this, I made that part transparent and saw the nut. My bad!!!


---
**Eric Lien** *August 12, 2015 22:36*

Are you using a 4mm nut?


---
**Bryan Weaver** *August 12, 2015 22:48*

I was wondering how exactly this assembly went together.  Is that what the 8-32 nut is for?


---
**Eric Lien** *August 12, 2015 23:16*

**+Bryan Weaver** I changed from the 8-32 back to the 4mm nut. It holds it better.


---
**Brandon Cramer** *August 12, 2015 23:22*

I'm currently using the 8-32 nut. The extruder was jammed again so I gave up for the day.



What is the recommended way to clean out the extruder?


---
**Brandon Cramer** *August 12, 2015 23:24*

**+Bryan Weaver** Here is what it looks like:



[https://www.dropbox.com/s/716a1714ygkec1z/Bowden_Nut_Trap.JPG?dl=0](https://www.dropbox.com/s/716a1714ygkec1z/Bowden_Nut_Trap.JPG?dl=0)


---
**Eric Lien** *August 13, 2015 00:06*

**+Brandon Cramer** I think someone has also made a top piece that integrates a pushfit instead of the captive nut.


---
**Erik Scott** *August 13, 2015 00:19*

**+Eric Lien**​ I did, yes. I can get you the files for that if you like. 


---
**Erik Scott** *August 13, 2015 00:43*

This is the piece you're looking for: [http://i.imgur.com/q8SDDek.png](http://i.imgur.com/q8SDDek.png)

STL: [https://www.dropbox.com/s/z153yzn1jmseqhl/Eustathios%20-%20Extruder%20Bowden%20Push-fit%20Adapter.stl?dl=0](https://www.dropbox.com/s/z153yzn1jmseqhl/Eustathios%20-%20Extruder%20Bowden%20Push-fit%20Adapter.stl?dl=0)
STEP: [https://www.dropbox.com/s/lrscpza059w6yzr/Eustathios%20-%20Extruder%20Bowden%20Push-fit%20Adapter.step?dl=0](https://www.dropbox.com/s/lrscpza059w6yzr/Eustathios%20-%20Extruder%20Bowden%20Push-fit%20Adapter.step?dl=0)


---
**Brandon Cramer** *August 13, 2015 16:34*

What microstepping setting should I use for the Hercustruder? I haven't received the 40 watt heater yet so I'm still using the 25 watt heater. 


---
**Brandon Cramer** *August 13, 2015 17:02*

Something is definitely wrong. Seems like it can't melt the filament fast enough??? I also believe it should be feeding the filament faster than what I have the 1/32 micro stepping set at. 



[https://www.dropbox.com/s/0v9m245l15j3ghk/2015-08-13%2009.57.58.jpg?dl=0](https://www.dropbox.com/s/0v9m245l15j3ghk/2015-08-13%2009.57.58.jpg?dl=0)


---
**Eric Lien** *August 13, 2015 18:06*

What are your settings? Temps, etc. Also idler tension takes a while to get it right. That's why I moved to the bondtech. If you want you can buy just the hardware kit from **+Martin Bondéus**​. You can print the housing and use the same geared stepper. I have not had a feed issue since installing my bondtech, and I usually print every day.


---
**Brandon Cramer** *August 13, 2015 18:14*

I have it at 220C, 1/32 micro stepping. It pushes the filament but seems to have trouble melting it fast enough, even on the slowest speed possible. 



I think it must be the 25 watt heater. Not sure what else it could be. 


---
**Erik Scott** *August 13, 2015 21:21*

When you say 25W heater, do you mean the heater carriage in the hotend? Aren't you using the one that came with your E3D hotend?


---
**Brandon Cramer** *August 13, 2015 21:23*

**+Erik Scott** Yes, that is the one that came with my E3D hotend. Should it work with this, or does it need to be 40W?


---
**Eric Lien** *August 13, 2015 22:11*

At 220 the 25watt can keep up, but at 240C I have found the 25watt to be lacking, so I replaced it with a 40watt.﻿



Still not sure why E3D changed to 25watt.


---
**Brandon Cramer** *August 13, 2015 22:38*

After about 2 days of frustration... I found my problem. The fan that cools the extruder.... don't hook that up with the Extruder heater. Once I moved the fan over with the constant 24 volts, it started working. I didn't notice the fan shutting off when the extruder got up to temp. Oops. 



Now the fan that cools the print or tip. If that is running, I can't get above 150C. Got any ideas?


---
**Eric Lien** *August 13, 2015 23:04*

I would say get the 40watt heater. That 25watt is crap. 


---
**Eric Lien** *August 13, 2015 23:05*

Also get up to temp before turning on the part cooling fan.


---
**Brandon Cramer** *August 13, 2015 23:35*

**+Eric Lien** How do I go about turning on just that fan? Not sure how I would wire it? Not sure if I need to have a manual switch or if there is some place to hook it up that I'm not seeing? 


---
**Brandon Cramer** *August 13, 2015 23:37*

I ordered the 40watt heater but they messed up my address. :(﻿


---
**Eric Lien** *August 13, 2015 23:55*

**+Brandon Cramer** there should be an output on the azteeg for the part cooling fan. The heatsink fan should be on constant 24v. Also you should run the PID tuning for now until you get the new heater. My PID based on a 40watt heater is likely limiting your lower output heater.


---
**Brandon Cramer** *August 14, 2015 00:03*

PID?



There is but it says low voltage only. The fan for cooling the print is 24 volts. Not sure how I'm going to be able to hook that up and be able to turn it on and off in Repetier Host.... 



That brings up my next question. Do you run that fan at full speed? If so I will just hook it up to have 24 volts constant once I get my 40 watt heater. 


---
**Eric Lien** *August 14, 2015 00:16*

There is a fan output on the x5 mini for the part cooling fan, I am sure of it. And no you don't want the part cooling fan on constant or you won't be able to do ABS and other plastic that doesn't like part cooling. Only the heatsink fan should be on 100%.



[http://smoothieware.org/temperaturecontrol-pid-autotuning](http://smoothieware.org/temperaturecontrol-pid-autotuning)


---
**Erik Scott** *August 14, 2015 01:28*

Just curious, is the 25W heater the one with the blue wires and the 40W with the red? Because my new hotend came with blue wires. 



**+Brandon Cramer** Make sure you don't have air blowing directly on the nozzle. That will cool it down too much. You want it blowing just around the nozzle. 


---
**Erik Scott** *August 14, 2015 01:37*

**+Brandon Cramer** PID stands for Proportional Integral Derivative, and it's a kind of universal controller for things where you don't know or can't model the exact behavior of a system (the system in this case being the temperature and heating/cooling of the nozzle.) You adjust 3 gains, Kp, Ki, and Kd which are multiplied by the error (the difference between the actual and commanded temperature), the integral of the error, and the derivative of the error, respectively. These are summed together and become the new value to send to the heater cartridge. 



So how do you adjust these values? You can run PID Autotune, which should figure it out for you. You can also do it manually. You can start by setting the Ki and Kd values to zero and increasing the Kp value until you get a decent rise time. Then you increase the Ki until you minimize the offset between the commanded and steady-state temps. You then increase the derivative gain (Kd) to reduce any oscillations. 


---
**Eric Lien** *August 14, 2015 02:42*

**+Erik Scott** that is just about the best and most concise PID explanation I have seen so far. Very well done.


---
**Brandon Cramer** *August 14, 2015 04:18*

**+Erik Scott** Yep! You got the 25watt version. The red ones are 40watt. 



Thanks for the PID info. I will have to see if all that makes sense tomorrow when I'm in front of it. 


---
**Erik Scott** *August 14, 2015 14:01*

**+Eric Lien**​ it's how I wish it had been explained to me when I was first learning controls. Teach the concepts first, then do the math to explain how and why. 



**+Brandon Cramer**​ No problem. Hope it helps. I never actually had to change the PID values in my first hotend. It worked perfectly out of the box. That said, I was using Marlin, not Smoothie. 



I have an extra red 40W cartridge from the hotend I removed when I gave up on dual extrusion. I'm going to use that in my new build then. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/SrQtgSDJMQr) &mdash; content and formatting may not be reliable*
