---
layout: post
title: "Can any of the Bondtech-Extruder users (e.g"
date: August 10, 2015 09:37
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Can any of the Bondtech-Extruder users (e.g. +Eric Lien) or **+Martin Bondéus** say something about the acceleration and e jerk they're using or think is best? Also i'd be glad to get some tips for setting the screws on the bondtech to a right value. So far i completely screwed them out and while feeding some filament in slowly screwed them in while giving some pulling force to the filament to see how good the transport force is.





**Frank “Helmi” Helmschrott**

---
---
**Martin Bondéus** *August 10, 2015 10:28*

Hi **+Frank Helmschrott** Regarding acceleration for the extruder it is set to 3000 mm/s in my setup, for the jerk i do not recall the value, since I am running Smoothieboard i think there is no value for Jerk but it is specified in another way. Regarding the setting of the set-screws I recommend the setting as described in the manual, 0.5 mm outside the housing. You can disconnect the bowden and feed filament through, take a close look at the filament and adjust the setting until you get clear bite marks on both sides, also measure the dimensions of the filament after it has passed through the extruder, you should stay under 1.8-1.85 mm width in order to not create too much friction inside the bowden tube.



I send send you images of the bite marks later tonight that you can have as a reference.


---
**Frank “Helmi” Helmschrott** *August 10, 2015 11:45*

Thanks, **+Martin Bondéus** thats about what i did already and the 0.5 should also fit what i have now. So i'm guess i'm fine with that.



Regarding the accelleration i'm much lower now due to the problems that i already had seen but i guess lowering the extruder start feedrate (which i the name of the e jerk value in Repetier Firmware) was the more important part. Maybe i'll try rising the acceleration a bit again.



The only problem that stays is with the eSun PETG and at the end i'll probably just not use it anymore. I still have to test the other more problematic filaments from the past (like woodfill, cf20) but the PETG behaves completely strange and maybe i've just gotten a strange spool.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/BcZPowGd4wE) &mdash; content and formatting may not be reliable*
