---
layout: post
title: "It's Alive! This is my first time running through the break-in Gcode - it's a lot smoother than I expected, although far from perfect"
date: November 23, 2016 18:49
category: "Build Logs"
author: Neil Merchant
---
It's Alive!



This is my first time running through the break-in Gcode - it's a lot smoother than I expected, although far from perfect. I still have a fair bit of work to do to make this thing actually print - the hotend and extruder isn't even connected, for one thing. I also need to clean up my wiring a bit - I rushed to get it moving and skipped most of the cable management.



But all that aside - Yay, it works!


**Video content missing for image https://lh3.googleusercontent.com/-fQwB7as5Atk/WDXkvMkrUaI/AAAAAAAAJa0/O79ZIZf73dsUeQ6FJisVRtriAuJhwuy8QCJoC/s0/VID_20161122_000038.mp4**
![images/e780535bed4e05bfb3e123c24e78dcde.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e780535bed4e05bfb3e123c24e78dcde.jpeg)



**Neil Merchant**

---
---
**Nicholas Brown** *November 23, 2016 19:11*

I like the black color scheme, very nice.


---
**Eric Lien** *November 23, 2016 20:36*

Looking great. I agree, the "Knight Rider" look is nice :)



As I always say, it makes me so happy every time I see a new printer make its first moves. Congratulations. I am excited to see your first prints next.


---
*Imported from [Google+](https://plus.google.com/105839137113604539428/posts/Ek7jZ3BaFA8) &mdash; content and formatting may not be reliable*
