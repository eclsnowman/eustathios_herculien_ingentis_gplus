---
layout: post
title: "Finally happy with this print quality! After gluing my bowden adapter ring back into its housing and tweaking my bowden path a little, and also reducing my retract speed (3600mm/min is too much even for the might bondtech!)"
date: July 02, 2015 03:10
category: "Show and Tell"
author: Ben Delarre
---
Finally happy with this print quality!



After gluing my bowden adapter ring back into its housing and tweaking my bowden path a little, and also reducing my retract speed (3600mm/min is too much even for the might bondtech!).



Generally pretty happy with this result now, I have a couple of small holes in the top layer near the front right of the bow, not quite sure how they're sneaking in, perhaps my filament is slightly smaller than I specified, and there was one random string when the head moved from the tip of the bow to the cabin area, again not sure what caused that but I think mostly an anomaly. Good enough now though I think!



Tips for those tuning:



1) If you think you got your E steps right the first time, think again, go back and try again. Mine was out by almost 4% even though I could have sworn it was correct last time I set it.



2) Use Eric Liens settings, they're good:



0.5mm extrusion width

5.5mm retraction distance

-0.1 extra restart distance

1500 retraction speed

0.2mm coast



If you're using Simplify3d, use the 'Only retract when crossing open spaces', and 'Force retraction between layers' settings also.



3) Ensure your filament doesn't have a knot in it! (yes...no idea how that happened but the filament must have got under itself at some point and created a knot which definitely will ruin your day).



![images/59db6467cf9510f2c9ff1f4b5c4f6923.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/59db6467cf9510f2c9ff1f4b5c4f6923.jpeg)
![images/421ca3ce7257654fe3a883442a2f2951.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/421ca3ce7257654fe3a883442a2f2951.jpeg)
![images/cec70e7e5b329d90eddbfc0b7fb8f645.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cec70e7e5b329d90eddbfc0b7fb8f645.jpeg)
![images/6158f4ea5e8c8469b66757748051dd43.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6158f4ea5e8c8469b66757748051dd43.jpeg)
![images/78dd26cc81eecb7939714e95ba1920d6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/78dd26cc81eecb7939714e95ba1920d6.jpeg)
![images/4ceb6984a157f803f8c80b1dff8723ea.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4ceb6984a157f803f8c80b1dff8723ea.jpeg)
![images/dc3c482e1b8fb6cc756bb884e6be1c6a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dc3c482e1b8fb6cc756bb884e6be1c6a.jpeg)
![images/4504b68f56cb42c8f62361593e32db9f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4504b68f56cb42c8f62361593e32db9f.jpeg)
![images/9b8b180c281233df4b0ae1a590f18001.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9b8b180c281233df4b0ae1a590f18001.jpeg)
![images/6f57ab9f33bd89a2a407658488eb7b89.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6f57ab9f33bd89a2a407658488eb7b89.jpeg)
![images/d2eda747f4bd6ab249b6b70d0f80b0d6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d2eda747f4bd6ab249b6b70d0f80b0d6.jpeg)
![images/d1d2b1c151c38f0ac32c6ce77c877c04.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d1d2b1c151c38f0ac32c6ce77c877c04.jpeg)
![images/492ae37fcb00ce09b0b4af3faddcf53f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/492ae37fcb00ce09b0b4af3faddcf53f.jpeg)
![images/90323ad5691a07e97ade64cc1a47c65d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/90323ad5691a07e97ade64cc1a47c65d.jpeg)

**Ben Delarre**

---
---
**Eric Lien** *July 02, 2015 03:29*

Looking good sir. And that black on yellow color scheme of the printer is looking good, I am kind of jealous.


---
**Ben Delarre** *July 02, 2015 03:34*

Yes the waspstathios is pretty sharp looking ;-) 



Thanks again for the design and for the help with the printed parts **+Eric Lien**​! 



Hopefully I will get my alu bed cut in a couple of weeks and I can start printing abs then print a set for the next guy/gal.



Next up I am planning to start printing some nylon 618 soon as I need some working gears for this year's Burning Man project.


---
**Ricardo de Sena** *July 02, 2015 13:17*

Good morning, this boat ever see here in the group and wanted to know where to test down here on my printer. Thanks.


---
**Chris Brent** *July 02, 2015 19:56*

**+Ricardo de Sena**  it's call 3D Benchy [http://www.3dbenchy.com/](http://www.3dbenchy.com/) It's more fun than calibration cubes or plates with ridges.


---
**Ben Delarre** *July 02, 2015 20:05*

**+Chris Brent** yeah, and honestly its a pretty good test. It has a nice selection of overhangs of various kinds, smooth curves, straight lines etc. I have a collection of half a dozen little boats on my shelf :-)


---
**Ricardo de Sena** *July 02, 2015 20:09*

Chis thanks!!!


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/aEi6tDZhFYa) &mdash; content and formatting may not be reliable*
