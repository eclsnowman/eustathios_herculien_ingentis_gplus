---
layout: post
title: "Is there a PDF of the through holes for the 562 mm extrusions?"
date: July 09, 2016 22:21
category: "Discussion"
author: Sean B
---
Is there a PDF of the through holes for the 562 mm extrusions?  I know there is 1 hole at 80 mm, another at 470 mm; however I don't see where the top hole is located.  As I understand it from reading other posts, it's shifted to allow room for the extrusion end caps.  So those holes would be 10 - end cap thickness?





**Sean B**

---
---
**Eric Lien** *July 09, 2016 22:43*

**+Sean B**​ correct.


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/N9fcUyPpE6K) &mdash; content and formatting may not be reliable*
