---
layout: post
title: "Much much better with 6mm retract and modifying layer height to 0.1920"
date: June 13, 2015 19:46
category: "Discussion"
author: Ben Delarre
---
Much much better with 6mm retract and modifying layer height to 0.1920.



Still getting some definitely z banding on the tower though, not sure if thats an artifact of slicing or not since the main body of the boat does not exhibit it.



I used cool z lift to try and get the chimney to not melt since I was having problems with that before, but its left a lot of artifacts from ooze so maybe I'll try again without that.



Very happy with this result though!



![images/1b2a9378400182a3316e541ade85b517.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1b2a9378400182a3316e541ade85b517.jpeg)
![images/79b7ac6fb52a74b3db7538f951b15775.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/79b7ac6fb52a74b3db7538f951b15775.jpeg)
![images/b257207f2c0ea9f0330b4e88326838b0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b257207f2c0ea9f0330b4e88326838b0.jpeg)

**Ben Delarre**

---
---
**Eric Lien** *June 13, 2015 20:04*

Looks like you may have some belts rubbing on Z. I have seen something almost identical on mine when the bed belt was overly tight and the belt would start to ride the side of the pulley, then relax, then start again, ... 


---
**Ben Delarre** *June 13, 2015 20:05*

Ah....yes it definitely does that looking at it, it moves up and down periodically.



How did you fix it? Seems like the pulley position vertically is out perhaps?


---
**Eric Lien** *June 13, 2015 20:06*

What speeds are you using? Do you have minimum layer time enabled which will slow things down when you get to things like the tower.


---
**Ben Delarre** *June 13, 2015 20:09*

I'm printing at 50mm/s, travel at 100mm/s and bottom layer at 20. All others set to default to 50mm/s.



Z speed I limited to 500mm/s in the smoothiefirmware. Acceleration for x/y is set to 8000mm/s/s with 500mm/s/s for z.



I had minimum layer time set to 5seconds last time I printed a calibration stepped cube. The top cube however (where its a small area) just started to melt since the hot end was over it for too long. For the benchy pictured I set minimum time to 5 seconds, but enabled cool head lift and set minimum speed for that to 30mm/s. This might have been a bad idea.


---
**Ben Delarre** *June 13, 2015 21:33*

**+Eric Lien** so I think the z ribbing might actually be a bigger problem than just the belt travelling on the pulley.



I disconnected the z stepper, and ran the bad up and down a bit by hand pulling the belt, when going up the belt rides up into the lip of the z motor pulley, but doesn't go over it, and when going down the same thing happens except the belt moves down the z motor pulley.



Anyway, while doing this I noticed the movement of the belt is not completely smooth. It has a cyclic tight spot, in that its smooth then goes tight then goes smooth again. Its not so tight that its stopping the motors evidently but I think this must be a symptom of something being out of alignment again.


---
**Ben Delarre** *June 14, 2015 02:26*

Found it!



Took the z axis apart entirely (again). Stripped both the left and right assemblies down until I had just the smooth rod and the leadscrew then slowly built my way back up.



Turns out that I had to loosen the bolts holding the z axis leadscrew nuts to the supports. When they were done up tight the left leadscrew would not turn smoothly all the way through its axis.



So I undid the bolts holding the nut to the support then moved the axis down to the lowest point of travel. Then very carefully did the nuts up and checked the smoothness of travel on that screw. Eventually found a point where the nut is held securely without any movement but the screw is not binding.



Put it all back together again and the Z axis moves like a charm now and I think the wobble will have gone. Running a print now to check.


---
**Eric Lien** *June 14, 2015 11:22*

Great news.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/cfXSDimoFpW) &mdash; content and formatting may not be reliable*
