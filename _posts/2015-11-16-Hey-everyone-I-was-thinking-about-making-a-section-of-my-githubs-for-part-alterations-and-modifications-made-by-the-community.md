---
layout: post
title: "Hey everyone, I was thinking about making a section of my githubs for part alterations and modifications made by the community"
date: November 16, 2015 00:24
category: "Discussion"
author: Eric Lien
---
Hey everyone, I was thinking about making a section of my githubs for part alterations and modifications made by the community. That way everything is in a centralized location for someone looking to make modifications to the HercuLien and Eustathios Spider v2. If you aren't familiar with github, but want to have your stuff included please comment to this post with a link to the STL and SolidWorks, Step, or other native model if possible as well. Maybe also include a text file with your name, the reason for the modification, and any other details you want to provide. That way I can make sure the creators are getting credit for all their amazing work.



In a week or so I'm going to collect up that which has been submitted and push to the github.  That way everyone can share in all the great modifications this community is bringing about. 





**Eric Lien**

---
---
**Jeff Kes** *November 16, 2015 00:57*

Awesome idea!


---
**Bud Hammerton** *November 16, 2015 01:41*

Agreed, I have made a lot of small modifications to existing parts, mostly for aesthetic reason, some for ease of printing without supports. It makes a lot of sense to put them up on the GIT repository rather than only on places like Thingiverse or YouMagine.


---
**Walter Hsiao** *November 16, 2015 20:07*

Here's a few things that may be worth adding:

[http://www.thingiverse.com/thing:854360](http://www.thingiverse.com/thing:854360) - bearing holder

[http://www.thingiverse.com/thing:854301](http://www.thingiverse.com/thing:854301) - belt tensioner

[http://www.thingiverse.com/thing:854267](http://www.thingiverse.com/thing:854267) - ballscrew bed support

[http://www.thingiverse.com/thing:854260](http://www.thingiverse.com/thing:854260) - print bed block

[http://www.thingiverse.com/thing:893849](http://www.thingiverse.com/thing:893849) - extruder carriage



Thanks for doing this Eric!  The level of support you provide is well beyond what I've seen from most commercial printers.  I have the repositories checked out so I can add any missing files or text once you decide on a format and check it in.


---
**Eric Lien** *November 16, 2015 22:34*

**+Walter Hsiao** Thanks so much Walter. You made so many great additions, I want them on my printer as well. I have been traveling for work, but will be adding the folders tonight on the github. I will also create a folder called "01 - example" to give people an idea how I plan to format the folders.



I am glad so many people seem open to the idea. I see so many great links and models come across this groups feed, I just don't want all this great work to get lost in time.  


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/HnH89HoW51z) &mdash; content and formatting may not be reliable*
