---
layout: post
title: "So for the last two years I've been working on my ingentis/eutathios build"
date: May 17, 2016 04:17
category: "Discussion"
author: Jim Squirrel
---
So for the last two years I've been working on my ingentis/eutathios build. I'm a down to hotend and extruder and electronics. I plan to use e3d v6 and was curious of the ptfe length that everyone is using.





**Jim Squirrel**

---
---
**Jim Squirrel** *May 17, 2016 19:23*

Ptfe tube from extruder to hotend﻿


---
**Eric Lien** *May 17, 2016 21:32*

Mine is right around 900mm. It could be made shorter... But I opted for the most gradual arc instead of shortness. With the Bondtech extruder retraction is so crisp I have no problem with the long Bowden even on small features with lots of retraction.


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/dKvM4dLHEfm) &mdash; content and formatting may not be reliable*
