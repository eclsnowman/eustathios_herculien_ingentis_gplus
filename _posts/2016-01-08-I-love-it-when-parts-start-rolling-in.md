---
layout: post
title: "I love it when parts start rolling in!"
date: January 08, 2016 21:24
category: "Show and Tell"
author: Kevin Conner
---
I love it when parts start rolling in!

![images/adbf1ac030b50b9d09e6e324b7c50156.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/adbf1ac030b50b9d09e6e324b7c50156.jpeg)



**Kevin Conner**

---
---
**Eric Lien** *January 08, 2016 21:40*

Welcome to the club **+Kevin Conner**​. Glad to see another Eustathios preparing to take shape. There are so many knowledgeable makers here in our group. Please ask as many questions as you need throughout the build. The more build posts we get in the group feed the more information is available for others as they start their builds.


---
**Kevin Conner** *January 08, 2016 22:09*

Thanks. This will be my second scratch build. I'm really excited to make a gantry printer. 


---
**Kevin Conner** *January 08, 2016 22:09*

Any reason not to roll the ball screw mod into the primary BOM? Ball screws are $35 on Amazon prime and acme thread/nut came to ~$70


---
**Eric Lien** *January 08, 2016 22:37*

I want to, just need time to edit it. To be honest, I want to get Golmart to create a PN# for us with the proper end machining dimensions. If anyone wants to tackle that for me I would appreciate it. Otherwise I will get to it as time allows.


---
**Walter Hsiao** *January 09, 2016 00:52*

I can contact them and see if they'll add it.  It's 12mm diameter ballnut (1204), 425mm long with a 46mm long, 8mm diameter machined end with the top of the nut pointing away from the shaft, right?  No machining on the top and none of the keyway stuff?



here's a diagram: [https://goo.gl/photos/AFfsLqxi72UCu7q57](https://goo.gl/photos/AFfsLqxi72UCu7q57)


---
**Eric Lien** *January 09, 2016 02:18*

**+Walter Hsiao** thank you so much. I think if we could line this up it would be much easier for new makers to source the parts. Plus with the quality of the parts I see you post out on thingiverse... I need this upgrade too. Your quality is perfect on everything I see that you print. I must admit I have maker envy.


---
**Kevin Conner** *January 09, 2016 02:40*

I picked up these [http://www.amazon.com/gp/product/B00SY8EDUI?psc=1&redirect=true&ref_=oh_aui_detailpage_o00_s01](http://www.amazon.com/gp/product/B00SY8EDUI?psc=1&redirect=true&ref_=oh_aui_detailpage_o00_s01) 

$35 each.  

being about 1" short I'll probably need to make modifications to the lead screw supports eh?




---
**Walter Hsiao** *January 09, 2016 03:37*

**+Eric Lien** That's because you don't see the other side of the prints :)  Using a light box and transparent filaments helps too.  I'm not sure the ballscrews help with print quality, but they're probably faster and I do like them.  Yeah, being able to just order the part without worrying about the machining details and ballnut orientation should make it easier.  Hopefully no one else will have to learn how to pack a ballscrew.


---
**Walter Hsiao** *January 09, 2016 03:46*

**+Kevin Conner**  Just wondering, how are you planning to mount the pulleys on the lead screws?


---
**Kevin Conner** *January 11, 2016 18:44*

I haven't discovered that yet. I've got a lathe so I may have to make modifications. I also may decide that I got the wrong ones and now there is a better alternative available after this discussion!


---
*Imported from [Google+](https://plus.google.com/+KevinConner/posts/BP4ZWtZPnLe) &mdash; content and formatting may not be reliable*
