---
layout: post
title: "Bicycle Chain Z-lift? I'm pitching one of my kid's crappy Walmart bikes and am looking to recycle any parts possible and come to the chain"
date: June 09, 2014 03:09
category: "Discussion"
author: Mike Miller
---
Bicycle Chain Z-lift? 



I'm pitching one of my kid's crappy Walmart bikes and am looking to recycle any parts possible and come to the chain. **+Shauki Bagdadi**'s designs use chain parts all over for connectors and spacers and the like, but I was wondering if using them to drive the Z-axis was something that'd been tried and rejected years ago? There's this: [http://www.thingiverse.com/thing:197896/#files](http://www.thingiverse.com/thing:197896/#files)

Which could be adapted to a Nema17 shaft, I'm sure. 



Thoughts? Warnings? 





**Mike Miller**

---
---
**Wayne Friedt** *June 09, 2014 04:40*

If you could keep the slack out of it it would work.


---
**Mike Miller** *June 09, 2014 11:19*

Hopefully, I can design it to easily swap it for GT2, I had also hoped, since it was beefyier than fishingline and/or GT2 that it might support a counterweight to the build platform. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/DmxfjLAcjNx) &mdash; content and formatting may not be reliable*
