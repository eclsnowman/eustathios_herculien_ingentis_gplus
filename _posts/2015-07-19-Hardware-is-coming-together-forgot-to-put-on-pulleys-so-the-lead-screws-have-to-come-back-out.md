---
layout: post
title: "Hardware is coming together, forgot to put on pulleys so the lead screws have to come back out"
date: July 19, 2015 13:28
category: "Show and Tell"
author: Rick Sollie
---
Hardware is coming together, forgot to put on pulleys so the lead screws have to come back out.



Haven't started on the electronics yet. Anyone know of a good guide for setting up ramps board?

![images/70d7326eff3e1211e1b9b4c0cca94293.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/70d7326eff3e1211e1b9b4c0cca94293.jpeg)



**Rick Sollie**

---
---
**Eric Lien** *July 19, 2015 14:40*

Looking good. I might suggest some cross braces. It could be created by some surface sheets bolted on to semi-enclose the frame. That would stiffen things up nicely.



Also I recommend you drop to a lower tooth count on the motor pulleys. You will never be able to hit the speeds those pulleys could produce. Using a 20 tooth on the motor and 32 on the shafts is a very nice balance of torque, speed, and steps/mm. But that's just my opinion.



Lastly you may want to think about making the top of the lead-screw captive (something similar to on my HercuLien printer). If you print anything as tall as your build is capable of, those lead screws have a pretty massive lever arm when the build plate is at the bottom.


---
**Rick Sollie** *July 19, 2015 16:17*

Eric, thanks for all the suggestion. It is appreciated.



I plan on having this inclosed so it will be braced. I already have the insertion nuts in place ;)



Funny you should mention that about the pulleys. Turns out I ordered the wrong ones and the belts wouldn't fit anyway, so another order to robotdigg. (You must look at these pictures with a microscope)





I was thinking of the same for the leadscrew. And downloaded the herculien files to see if it was possible to use that part. Haven't looked yet.



Thanks again for all the advice.




---
**Eric Lien** *July 19, 2015 18:16*

One thing about the lead-screw on HercuLien is I have both ends turned down to 8mm for bearings. It would be more difficult with one that is not turned down. You could maybe use a bearing with an 12mm ID, or if you know anyone with a lathe turn it down yourself. You will just need to pay attention to travel of the bed to make sure nothing gets in the way of the bed making it up to the hotend.﻿


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/ipuPZq3Z3bb) &mdash; content and formatting may not be reliable*
