---
layout: post
title: "I guess I won't have the time to try this Titan Aero thing out quickly but I was nosey if it could work so I fired up Fusion360 quickly this morning and gave it a quick sit"
date: April 09, 2017 08:17
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
I guess I won't have the time to try this Titan Aero thing out quickly but I was nosey if it could work so I fired up Fusion360 quickly this morning and gave it a quick sit.



Looks like it could work to put the motor above the lower rod but only barely enough. I need to check if I could get the bed up high enough. Also everything is quite tight. I also wanted to give the BLTouch Sensor a test on this so this would need some room. Will definitely kill some x/y movement compared to my last carriage but definitely win some mm in Z.



I merged both in the last picture to give some impression of size. Of course the cooling fans are missing on both.



![images/85a4ce39d4ab635130325cc4eca90121.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/85a4ce39d4ab635130325cc4eca90121.png)
![images/2dd1d62f62b0ca1dc65a8bfd6e334661.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2dd1d62f62b0ca1dc65a8bfd6e334661.png)
![images/70a3d8d60d9822cede16c6b9d7a5e6ec.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/70a3d8d60d9822cede16c6b9d7a5e6ec.png)

**Frank “Helmi” Helmschrott**

---
---
**Markus Granberg** *April 09, 2017 09:30*

Are you running igus bearings to?


---
**Frank “Helmi” Helmschrott** *April 09, 2017 09:31*

yes, guess the same that you have (ECLM 08)


---
**Markus Granberg** *April 09, 2017 10:20*

close, I got the ECLM-10. I really like them. but you need to get the hole just the right size.


---
**Eric Lien** *April 09, 2017 12:19*

Nice. I didn't think it would be long enough.


---
**Frank “Helmi” Helmschrott** *April 09, 2017 15:39*

**+Markus Granberg** sure but thats not so different with the alternatives on that printer :) At least they align quite well. Didn't know you are using 10mm rods.



**+Eric Lien** yeah but definitely thinking about using the standard Titan + E3Dv6 as it would need quite some modification to get the bed up that far. Also there would be more room for a fan and the BLtouch then.


---
**Eric Lien** *April 09, 2017 17:33*

Yeah fitting in a compact part cooling solution would be tricky in that short space.


---
**Jim Stone** *April 09, 2017 18:34*

I would recommend against using a BLTouch. ever. its repeatability is TERRIBLE and you will be left tearing your hair out.



every home leaves you with a different offset because of this.



that titan aero looks neat tho.


---
**Frank “Helmi” Helmschrott** *April 09, 2017 18:45*

That's exactly what I'm trying to find out. I'm currently probing with an IR-Sensor that is a) sensible against external light and b) sensible to mechanical influence (IR diodes are easily moved out of place). Regarding BL Touch I read some problems but also had people that are quite happy with it. I think I'll give it a test and see what's in there in terms of repeatability.


---
**Jim Stone** *April 09, 2017 18:46*

Mine on the best day gave 0.02. On a herculien using ballscrews



If you find anything out would love to know




---
**Frank “Helmi” Helmschrott** *April 09, 2017 18:49*

which sensor did give you better repeatability than 0.02? If it's not reliably that's a problem of course but 0.02 would be something I could live with.


---
**Jim Stone** *April 09, 2017 19:09*

Bltouch it bounced between 0.07 and 0.02


---
**Jim Stone** *April 09, 2017 19:09*

Which can amount to terrible layering between prints


---
**jerryflyguy** *April 09, 2017 19:10*

**+Frank Helmschrott** is the Titan/aero model available somewhere? I'd like to do some modeling with it and see what I can come up with.


---
**Eric Lien** *April 09, 2017 19:13*

**+Jim Stone** was yours an official BLTouch, I have read that the AliExpress ones are junk for repeatability, but had heard the official BL touch is OK.



Then again I have printers with bed compensation. But never use it. I have found a sturdy frame, and a good initial mechanical Tram is all I need. I never have first layer issues on any of my printers.



I just run a 0.3mm first layer at 50% speed regardless of layer height of the rest of the print.


---
**Jim Stone** *April 09, 2017 19:15*

**+Eric Lien** yes sir it is an authentic one. Paris even sent a new roll pin to try and solve the problem. It didnt


---
**Frank “Helmi” Helmschrott** *April 09, 2017 19:21*

**+jerryflyguy** there are different ones available though no official one (they have technical drawings on their site). I found mine through yeggi ([http://www.yeggi.com/q/titan+aero/](http://www.yeggi.com/q/titan+aero/))



[yeggi.com - "titan aero" 3D Models to Print - yeggi](http://www.yeggi.com/q/titan+aero/)


---
**jerryflyguy** *April 10, 2017 03:23*

**+Frank Helmschrott** thanks! I'll check it out


---
**Stefano Pagani (Stef_FPV)** *April 24, 2017 15:38*

**+Frank Helmschrott** You got the link to your current v6 carriage? I was thinking to convert to chimera. May be better to start from scratch though...


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/Y5GtBLBw7pg) &mdash; content and formatting may not be reliable*
