---
layout: post
title: "Hey everyone. Does anyone know of a good European Source to get these pushfit (self aligning) bushings that are used in the original Eustathios design?"
date: January 06, 2017 14:32
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Hey everyone. Does anyone know of a good European Source to get these pushfit (self aligning) bushings that are used in the original Eustathios design? I switched to graphite bushings from Robotdigg first and am now using LM8LUU for the center (carriage) but I'm not happy with the play of the latter. I'd like to give the original ones a try as I do have to redesign my carriage anyways (still for cooling issues).



The only source I know is Lulzbot but they don't have them in there EU warehouse for quite a while now.





**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *January 06, 2017 16:52*

Sdpsi is who makes them: [https://shop.sdp-si.com/catalog/](https://shop.sdp-si.com/catalog/)


---
**Peter Kikta** *January 06, 2017 17:30*

I bought them from [ultibots.com - Quality 3D Printers, parts, and superior phone support &#x7c; UltiBots LLC](https://www.ultibots.com) 

They cost me 7€/pcs with shipping and customs


---
**Sean B** *January 06, 2017 17:47*

I ordered directly from SDPI, although I don't know if they ship internationally.


---
**Ted Huntington** *January 06, 2017 21:24*

**+Sean B** Were they in stock? Because the web shows them constantly out of stock- for like years. [shop.sdp-si.com - Catalog &#x7c; Designatronics Store &#x7c; Stock Drive Products/Sterling Instrument](https://shop.sdp-si.com/catalog/?cid=p469)   Maybe you just order them and they somehow eventually ship even though they list them as out of stock? I got mine from ultibots too.


---
**Sean B** *January 06, 2017 22:00*

**+Ted Huntington** yeah I didn't have to wait long.  They did show out of stock at lulzbot at the time but was surprised to see SDPI in stock because I have also seen them out of stock.


---
**Stefano Pagani (Stef_FPV)** *January 06, 2017 22:10*

**+Sean B****+Ted Huntington** Give them a call. It took 3 months for my oder because they had them out of stock in the warehouse. When they go them in (2 days after the order) they forgot about my order. Emailing tech support did nothing, they said they were coming :|


---
**Ted Huntington** *January 06, 2017 23:37*

**+Stefano Pagani** oh yeah SDPI support is not good- sent email for eta - no reply at all- now just periodic email ads from them :( No way would I wait 3 months for a bushing


---
**Markus Granberg** *January 07, 2017 08:14*

I use a 3 point system to line up two bushings. No need to have those special bushings. Works great.

![images/0b9c4b84e448f216a6391d6153b25dde.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0b9c4b84e448f216a6391d6153b25dde.jpeg)


---
**Frank “Helmi” Helmschrott** *January 07, 2017 08:17*

Thanks everyone for your answers. 



**+Markus Granberg** that's a great idea although it adds a bit complexity. Keeping things simple with a self aligning bushing would be great. But definitely a good alternative that I could also try with my graphite bushings. Probably still a bit of work until you get them aligned, right? I imagine it's not that easy to know which screws to turn if you feel they are misaligned.


---
**Frank “Helmi” Helmschrott** *January 07, 2017 08:52*

Just to be sure, at SDPSI we're talking about this [shop.sdp-si.com - Product &#x7c; Designatronics Store &#x7c; Stock Drive Products/Sterling Instrument](http://shop.sdp-si.com/catalog/product/?id=A_7Z41MPSB08M) (8mm) and this [http://shop.sdp-si.com/catalog/product/?id=A_7Z41MPSB10M](http://shop.sdp-si.com/catalog/product/?id=A_7Z41MPSB10M) (10mm) variants, right?



I think I'll drop them an email and see what happens.


---
**Sean B** *January 08, 2017 22:38*

**+Frank Helmschrott** yep, those are the ones


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/cf4c6JYzMRA) &mdash; content and formatting may not be reliable*
