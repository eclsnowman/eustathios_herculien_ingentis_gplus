---
layout: post
title: "Hey, question: Has someone already modified to fit the Eustathios?"
date: June 08, 2017 20:03
category: "Discussion"
author: Nils Hesse
---
Hey, question:

Has someone already modified [https://magnetic-tool-changer.com](https://magnetic-tool-changer.com) to fit the Eustathios?







**Nils Hesse**

---
---
**Eric Lien** *June 10, 2017 14:24*

Not yet. Are you willing to take on the mod? If you do, and get the files edited to work I would love to get them added to the GitHub under the user modifications.


---
**Nils Hesse** *June 11, 2017 13:11*

At least not currently, sadly I still did not build an Eustathios.



Since I'm about to rebuild one of my printers into something (hopefully) more useful I'm again exploring the possibility of building a Spider v2 and the magnetic tool changer seems to be too good of a match to miss :)


---
*Imported from [Google+](https://plus.google.com/104369017808539158490/posts/TrcybRS9Cpm) &mdash; content and formatting may not be reliable*
