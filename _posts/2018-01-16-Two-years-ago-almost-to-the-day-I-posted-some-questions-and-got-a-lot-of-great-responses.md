---
layout: post
title: "Two years ago almost to the day I posted some questions and got a lot of great responses"
date: January 16, 2018 05:57
category: "Discussion"
author: Greg Johnson
---
Two years ago almost to the day I posted some questions and got a lot of great responses. I now realize I wasn't ready to jump in to building my own printer though. I got way too frustrated with my Wanhao i3 and it say collecting dust for awhile.



A few months ago I decided I was going to do whatever it took to get this thing dialed and I went for it. The printer is probably 20% 3d printed at this point and I've done every mod, including soldering grounds to the awful Melzi board in hopes to get my temps stable.



I finally feel like I understand enough to build my own and troubleshoot effectively, but I do have some more questions.



I'm keen on the Eustathios Spider V2 simply due to the smaller size and it being more affordable to build. I would like to print ABS though, so I'm wondering if anyone has enclosed it and if there is a build-list for that mod.



I'd also like some clarification on lead-screws vs ball-screws. I understand the mechanics of each, but I don't know if they are interchangeable or if the pieces to print are unique to each.



As for printers to print with, I have my original Wanhao i3 that I can get decent prints off of and I have a CR10 arriving some time this week. My i3 will not get past 230 and I don't know about the CR10, so I've decided to print in eSun PLA+ simply because I use it a lot and I know how it works on my i3.



I would LOVE some advice on how to proceed. The most important things to me are reliability and print quality. I'm so tired of spending half my time diagnosing problems and tuning. I just want a workhorse.



So if there are any upgrades I should be making that aren't part of the Eustathios-Spider-V2 github repo, I would <b>*love*</b> to know about them.



You guys are awesome I'm really looking forward to becoming part of the club.





**Greg Johnson**

---
---
**Dennis P** *January 16, 2018 18:13*

I too am building the Eustahios and am using a Wanhao i3 to make my parts but in PETG. Its my only printer and thus my workhorse. I can easily get to 250, I have never gone above that.  I'd be happy to help with tweaking the i3, but I think we should do it off list or somewhere else.



 There are others more experienced than me, but I will offer my thoughts as I am currently building mine. 



Screws: I think that Zane did his with ACME leadscrews too, hopefully he can add to the discussion. 



I decided to go with trapezoidal ACME screws instead of ball screws. Since the screw pitch is effectively higher, we should get faster Z-hops for the same motor steps. And I wanted a little more friction from the anti-backlash nuts of the leadscrews.   



Cost was like $12 for a pair of 500mm screws and nuts to get shipped from Aliexpress instead of $40/each for screws. 



So far, I have identified that the Z platform brackets are the only thing that might need tweaking. As I am waiting on the screws, I will compare the mounting dimensions and bolt patterns of the nuts. 

The business ends of the 12mm ballscrews as specified are 8mm, and the TR8x8 leadscrewws are also 8mm, as are the pulleys so theoretically they should all fit. The only other thing is the shoulder that the screws bear against, but I think that will work out on its own too.  I will modify the parts and post my designs once I get the screws and nuts.



2. Mechanics. The i3 has an issue with when the power goes off, the X axis 'sags' to the side that the carriage is on. That is why the Z axis gets so screwy when the head parks on the left, it sags more on the left, and you have to keep re-leveing. Its actually because the holding power of the motors are essentially 0 with no power attached. Ballscrews have LESS friction than ACME screws, so I suspect that the problem might be worse with them since we are carrying the weight of the build platform. So I am hoping the added friction of the trapezoidal profile will help in this. 


---
*Imported from [Google+](https://plus.google.com/117128629201142457691/posts/THUz42icedc) &mdash; content and formatting may not be reliable*
