---
layout: post
title: "Originally shared by D Rob Move along folks nothing to see here"
date: November 18, 2014 03:33
category: "Show and Tell"
author: D Rob
---
<b>Originally shared by D Rob</b>



Move along folks nothing to see here. Just regularly scheduled upgrades and maintenance.

![images/0829d9942a5f7d26dcf979bf96440a0c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0829d9942a5f7d26dcf979bf96440a0c.jpeg)



**D Rob**

---
---
**Brandon Satterfield** *November 18, 2014 17:16*

Nice!! 

Is your office a furniture store? Ha


---
**D Rob** *November 18, 2014 17:58*

Yeah I manage my family's business which is indeed a furniture store. At night it's my lab. I have a shop with my micro lathe, drill press, table router, and belt sander... Etc... In the back. I play where i work after hours. Some of my vids have me here.


---
**SalahEddine Redjeb** *November 19, 2014 09:33*

what kind of speeds can you achieve with this? are the motors directly coupled to the shaft?


---
**D Rob** *November 19, 2014 16:34*

They are connected to the Motor via pulleys and belt. The motors mount outside on the frame and have gt2 36t pulleys on the Motor. The screws have gt2 32 10mm bore. They connect with a 600mm gt2 belt. The pitch is 1050 so 200 steps=50mm with 1/16 stepping  3200 steps=50mm﻿


---
**SalahEddine Redjeb** *November 20, 2014 17:03*

Oh, well, thank you for the information.


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/MHwA9abpdfk) &mdash; content and formatting may not be reliable*
