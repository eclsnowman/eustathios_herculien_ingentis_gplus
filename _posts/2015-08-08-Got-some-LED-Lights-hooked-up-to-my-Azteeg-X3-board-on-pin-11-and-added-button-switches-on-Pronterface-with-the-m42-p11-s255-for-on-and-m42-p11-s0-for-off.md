---
layout: post
title: "Got some LED Lights hooked up to my Azteeg X3 board on pin 11 and added button switches on Pronterface with the m42 p11 s255 for 'on' and m42 p11 s0 for 'off'"
date: August 08, 2015 07:27
category: "Show and Tell"
author: Vic Catalasan
---
Got some LED Lights hooked up to my Azteeg X3 board on pin 11 and added button switches on Pronterface with the m42 p11 s255 for 'on' and m42 p11 s0 for 'off'. Also got my Eustathios V4 Carriage mounted, Not an easy swap, practically had to disassemble the entire gantry. Other than that it is back to printing. The only negative thing with the new carriage is that I cannot clear unexpected plastic ooze from the hot end while it begins to print because the fan housing is very much in the way. 



![images/a363970f7119f279d6145bced197072c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a363970f7119f279d6145bced197072c.jpeg)
![images/cbe5db0642c53b94b6a0b3790cb2ab4b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cbe5db0642c53b94b6a0b3790cb2ab4b.jpeg)

**Vic Catalasan**

---
---
**Frank “Helmi” Helmschrott** *August 08, 2015 08:19*

Good thing with the lights. i was already thinking about some lightning. Currently i do have to leave the shop lights on to be able to watch it printing via tha Repetier-Server Webcam. I have an LED ring and thought about mounting it to the duct somehow.



But then i'm totally with you regarding the duct beeing in the way sometimes. As i already thought about changing the fan setup and drawing a new carriage/duct i probably should incorporate lightning there too.


---
**Vic Catalasan** *August 10, 2015 07:48*

Let me know what you come up with, I am thinking of at least 20 degree of open access to the hot end. I mounted the light inside the front extrusion at it does not blind me when looking at the prints, consider that some these LEDs are pretty blinding. 


---
**Frank “Helmi” Helmschrott** *August 10, 2015 08:09*

I will of course post it here if i have something but that might really take a while. I still have to fix some printing issues that will have priority for now and there's also lots of other stuff on my list.


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/8keorQgiKGE) &mdash; content and formatting may not be reliable*
