---
layout: post
title: "howdy everyone. :D i was wondering if anyone was still \"Paying it forward\" from ?"
date: March 07, 2015 13:00
category: "Discussion"
author: Seth Messer
---
howdy everyone. :D



i was wondering if anyone was still "Paying it forward" from [https://docs.google.com/spreadsheets/d/1R3hqplEJYiaQFLmFX04-vTjgyQ5SVY1PGjbx-7S-J8k](https://docs.google.com/spreadsheets/d/1R3hqplEJYiaQFLmFX04-vTjgyQ5SVY1PGjbx-7S-J8k)?



anyone want to print the Eustathios (preferably V2) parts for me? will pay shipping and even filament costs.



i originally thought i needed ABS, but now, don't really care. just want to get parts in to start building.



thanks much!





**Seth Messer**

---
---
**Eric Lien** *March 07, 2015 13:13*

**+Alex Lee**​ made a set for **+Bruce Lunde**​



And I made a set for **+Dat Chu**​.



Unfortunately the build process on these printers is a long and somewhat expensive process. 



Perhaps the current recipients could give a status update on their progress. That could give everyone on the list an idea about the timeline. 


---
**Bruce Lunde** *March 07, 2015 13:21*

**+Eric Lien** I am in the initial stage of building the base frame for my Herculein, I expect to have it built in about 6 weeks.  I will be paying it forward as soon as it is tuned and running.


---
**Seth Messer** *March 07, 2015 13:24*

all good! i didn't know if there were folks actively printing for people on the list or what not, so no worries. i just appreciate everyone in the community being so generous!


---
**Dat Chu** *March 07, 2015 14:38*

Baby is on the way so that puts a short hold on the project. I got the rubber mat, extrusions, everything except the mdf z bed and bracket. I am looking at 8 weeks Eta (baby time recovery included). 


---
**Gus Montoya** *March 08, 2015 00:04*

I know this is somewhat of a slimy way to come ahead, but I'll print it forward to both people infront of me and one after, if I'm bumped up. My finger is cocked and ready to click mouse and order all parts once **+Eric Lien** finalizes his Eustathios design. Those in the print it forward I have mentioned, specify the design you want printed on the google doc.


---
**Derek Schuetz** *March 08, 2015 23:59*

I'm waiting on my 2 belts with no ETA and then I'll be printing. Tested my extruders this morning and I have working end stops and motion in all motors. Full control through LCD and wiring set up (heated bed arrives Monday or Tuesday). As soon as I'm all calibrated ill start printing forward a set


---
**Eric Lien** *March 09, 2015 00:24*

**+Derek Schuetz**​ thats great. I should probably seed another set out there. Maybe after MRRF.﻿


---
**Gus Montoya** *March 09, 2015 07:56*

For some reason there is another person infront of me 3 instead of two. Am I missing something?


---
**Daniel Salinas** *March 09, 2015 16:28*

My robo3d is now freed up to anyone that needs parts. I ask only that you send me 2 spools of hatchbox abs in the color of your choice (1 is fine for eustathios or ingentis) and you cover freight.


---
**Seth Messer** *March 09, 2015 16:35*

**+Daniel Salinas** what's your time frame generally from start to completion? Also, 1.75mm for that hatchbox ABS I'm assuming? are you able to print the V2 stuff that **+Eric Lien** has been working on?


---
**Daniel Salinas** *March 09, 2015 17:09*

It takes about a week.  I haven't printed V2 yet so I don't know for sure about the timeline or that 1 spool will be enough.  Theres a little trial and error for the complex parts.  Yes 1.75mm is what my printer uses.


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/W5zcFFmayiQ) &mdash; content and formatting may not be reliable*
