---
layout: post
title: "I have been looking at getting open ended GT2 belt"
date: March 13, 2014 00:20
category: "Discussion"
author: Brian Bland
---
I have been looking at getting open ended GT2 belt.  I am considering buying a bulk order to get it cheaper. If there is enough interest here, I will order and offer it for sale.  In the US it will probably be ~$1.00 a foot shipped.





**Brian Bland**

---
---
**Eric Moy** *March 13, 2014 00:31*

You can get it pretty cheap from aliexpress with free shipping in pretty low quantities


---
**Carlton Dodd** *March 13, 2014 00:47*

I'd definitely be in for some (~20' to have some extra for other projects).


---
**Shachar Weis** *March 13, 2014 02:25*

Get the good stuff that has Kevlar in it.


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/f8etVrqz4uu) &mdash; content and formatting may not be reliable*
