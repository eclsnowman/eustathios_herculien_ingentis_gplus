---
layout: post
title: "After having ordered parts and getting parts printed here and there, I'm finally assembling"
date: July 01, 2015 17:12
category: "Show and Tell"
author: Rick Sollie
---
After having ordered parts and getting parts printed here and there, I'm finally assembling. Have the handycam running so hopefully I will have a timelapse of the entire build.

![images/8a23e3f2a6e059f45890edbc62639be9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8a23e3f2a6e059f45890edbc62639be9.jpeg)



**Rick Sollie**

---
---
**Rick Sollie** *July 01, 2015 17:27*

**+Eric LeFort**​ Eustathios v2 spider with and additional 300mm in height. So I guess I will call it the daddy longlegs ;)﻿


---
**Sébastien Plante** *July 01, 2015 18:05*

300mm more, that's a lot! :o


---
**Eric Lien** *July 01, 2015 21:31*

I look forward to meeting my printers taller brother :)


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/dQPZS7uRCsE) &mdash; content and formatting may not be reliable*
