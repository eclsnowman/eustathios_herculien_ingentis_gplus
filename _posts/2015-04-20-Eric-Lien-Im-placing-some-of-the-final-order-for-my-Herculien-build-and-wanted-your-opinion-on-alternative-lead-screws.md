---
layout: post
title: "Eric Lien I'm placing some of the final order for my Herculien build and wanted your opinion on alternative lead screws"
date: April 20, 2015 18:30
category: "Discussion"
author: Zane Baird
---
**+Eric Lien** I'm placing some of the final order for my Herculien build and wanted your opinion on alternative lead screws. I am considering using 8mm screws in place of the 12mm as listed in the BOM. I'm concerned these may not be sufficiently stiff, but at half the price its tempting. Do you think the weight of the bed necessitates the 12mm screws? 





**Zane Baird**

---
---
**Ben Malcheski** *April 20, 2015 21:19*

Obviously I'm not Eric. My thinking has always been that you want your lead screws to be flexible and your linear guide system as rigid as possible. That way any run out in the screws does not get transferred into movement of the bed. The axial load from the bed of almost any 3D printer is not large enough to cause significant compression or buckling in 8mm screws.


---
**Eric Lien** *April 20, 2015 21:31*

**+Ben Malcheski** I agree with you. I used fine thread 8mm all thread and a fine pitch but for 6 months before buying proper lead screws.


---
**Daniel F** *April 20, 2015 21:46*

I use 10mm ACME lead screws in a similar setup an plan to use them for my eustathios build. No problems with quite a heavy 6mm aluminium build plate at my QR. The steppers and the drivers need to be strong enough. So I think 8mm should work as well if they are straight and not too long. The advantage of a smaller diameter is that you can mount the pulleys directly on the screw without machining the ends to a smaller diameter. I bought 1m and cut 2 pieces for my Z.


---
**Zane Baird** *April 21, 2015 14:04*

**+Daniel F**, that was my thought as to mounting. One step closer to completion. unfortunately smw3d would only let me purchase 5 of the 10mm shafts, so I'm still one short...


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/VBtjDwkNCtE) &mdash; content and formatting may not be reliable*
