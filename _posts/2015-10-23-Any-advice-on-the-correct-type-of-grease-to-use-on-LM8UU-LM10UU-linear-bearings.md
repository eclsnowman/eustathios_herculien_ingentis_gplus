---
layout: post
title: "Any advice on the correct type of grease to use on LM8UU/LM10UU linear bearings?"
date: October 23, 2015 10:25
category: "Discussion"
author: Luke Strong
---
Any advice on the correct type of grease to use on LM8UU/LM10UU linear bearings?



I've spoken to a few distributors and looked at some manufacturer's websites and they all recommend an NGLI 2 lithium grease for general use. What confuses me is that some reprap sources seem to suggest that NGLI 2 greases can stop the balls from recirculating properly.





**Luke Strong**

---
---
**Lee Shawn (3DP2GO)** *October 23, 2015 11:36*

maybe you can try NSK greases


---
**Ryan Carlyle** *October 23, 2015 13:30*

One popular option is "Superlube (with Syncolon)" available on Amazon etc. It's a light silicone grease with PTFE. Non-toxic and works reasonably well on just about everything (bearings, screws, gears). A typical NLGI-2 lithium grease might be a bit thick at lower temps for the kind of light-duty service we're doing. Any grease is way better than no grease though. 


---
**Eric Moy** *October 25, 2015 01:41*

Agree with others that there's not much to worry about, considering the minimal motion. Just make sure there is always a thin film of grease or oil on the bearings and rods to prevent corrosion


---
**Ryan Carlyle** *October 25, 2015 01:52*

To be fair, it's millions of cycles (on X and Y at least) so having some kind of lubrication IS important, but it's typically very light duty as far as forces. Most lubricants will work just fine, you don't need any fancy EP additives or anything like that. Just keep the rods wet. 


---
**Eric Moy** *October 25, 2015 02:40*

Keep in mind, these are roller bearings, not plain bearings. There is minimal friction here that will affect the motion. The lubrication is for corrosion protection, and to migrate debris out of the ball path


---
**Ryan Carlyle** *October 25, 2015 03:04*

Yeah, but even roller bearings can bounce or slide if left dry, there's a fair bit of friction through the turnarounds at the ends of the races. Non-rolling dry contact will rapidly tear stuff up. 


---
*Imported from [Google+](https://plus.google.com/106147943249970698774/posts/EqDWRDVXdUY) &mdash; content and formatting may not be reliable*
