---
layout: post
title: "So, it's finally printing! Ran into some issues with the extruder calibration"
date: August 12, 2014 08:08
category: "Show and Tell"
author: Erik Scott
---
So, it's finally printing! Ran into some issues with the extruder calibration. Thought I had done it right and got 222 steps/mm. Turns out this was way off, causing the extruder to skip steps and allowing the filament to retract suddenly. Did a bit of math, figured out I was off, and then re-calibrated correctly (This time, not actually pushing the plastic through the hotend). Working beautifully now at 189steps/mm. 



Big thanks to **+Eric Lien** and **+Jason Smith** for their work on the Eustathious design, and thanks to everyone who helped me out on my other posts. 




**Video content missing for image https://lh3.googleusercontent.com/-mKtfZXnoDGs/U-nJOyuudsI/AAAAAAAAAjo/3LbAftc3q-Y/s0/VID_20140811_211806.mp4.gif**
![images/a081095e2dc48d0ee21fdede9092a6b1.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a081095e2dc48d0ee21fdede9092a6b1.gif)
![images/10a8ecf3f2f7930bc8a035c825ff8723.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/10a8ecf3f2f7930bc8a035c825ff8723.jpeg)
![images/70b1c7e26c2832e1b8a42e941988b1c4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/70b1c7e26c2832e1b8a42e941988b1c4.jpeg)
![images/283e26593e0f98fffbbabb9d10b909e0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/283e26593e0f98fffbbabb9d10b909e0.jpeg)
![images/77ce3ca41fb6e98169d4f5dd7cb96eb6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/77ce3ca41fb6e98169d4f5dd7cb96eb6.jpeg)

**Video content missing for image https://lh6.googleusercontent.com/-hagGvARDLMU/U-nFIwFdTgI/AAAAAAAAAfQ/PS74tTNt7TI/s0/VID_20140811_221334.mp4.gif**
![images/38f5437def73e37b0f2174eb07b3cc2d.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/38f5437def73e37b0f2174eb07b3cc2d.gif)

**Video content missing for image https://lh4.googleusercontent.com/-ti87atyTdw4/U-nFlGwuzNI/AAAAAAAAAfk/SkWVmtJnj0Q/s0/VID_20140812_021513.mp4.gif**
![images/f99901541dcdef0bdf8a162ab7e24af6.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f99901541dcdef0bdf8a162ab7e24af6.gif)

**Video content missing for image https://lh3.googleusercontent.com/-pp-Im-fHHlo/U-nGLUGgOWI/AAAAAAAAAf8/KEzFpL46jjg/s0/VID_20140812_024638.mp4.gif**
![images/b82f0159c916d51c081e974a7cc8226b.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b82f0159c916d51c081e974a7cc8226b.gif)

**Video content missing for image https://lh5.googleusercontent.com/-kg-ykpo-0T8/U-nGyohFaQI/AAAAAAAAAhs/lPGqp_L9XUI/s0/VID_20140812_024857.mp4.gif**
![images/ab11fdcd5fa3bbde0844973cf693313c.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ab11fdcd5fa3bbde0844973cf693313c.gif)
![images/0dcbb7692ba52e772842bc98a2a614de.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0dcbb7692ba52e772842bc98a2a614de.jpeg)
![images/5983fe6f224f387fa61a25250f13bfce.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5983fe6f224f387fa61a25250f13bfce.jpeg)
![images/530362adcbd22803763fcf76d11c80b3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/530362adcbd22803763fcf76d11c80b3.jpeg)
![images/458307ba46d40a3d20ad25bc0aa20a81.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/458307ba46d40a3d20ad25bc0aa20a81.jpeg)
![images/27bbe545b35232d2f6927d4a28bb25de.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/27bbe545b35232d2f6927d4a28bb25de.jpeg)

**Erik Scott**

---
---
**Erik Scott** *August 12, 2014 08:33*

That would actually be a good idea. It would be a great place to record my "adventure", so other people can learn from it. 


---
**Mike Miller** *August 12, 2014 18:39*

Hmmm. Wished I knew what the cool kids were using for database backed websites, I have the perfect domain name ([metaprintr.com](http://metaprintr.com)) but haven't made a db based website in YEARS. 


---
**Jason Smith (Birds Of Paradise FPV)** *August 14, 2014 01:26*

Congrats. This looks great! I think I may copy your design using 2 separate pieces of plexi for the bottom. Awesome work. 


---
**Erik Scott** *August 14, 2014 01:32*

I made it 2 pieces to fit it in the laser cutter at school. I accidentally cut the holes for the belts in the rear one too small (Same size as the front) and the belts rub ever so slightly. I have some teflon tape over it, so all is good. I'm going to be printing an entire replacement set of plastic parts, so I may replace the X and Y motor mounts with a slight modification to keep the belts from touching the acrylic. 


---
**Eric Lien** *August 14, 2014 02:51*

I like the lmuu bearing carriage version you used. They can be noisier... But I feel they handle misalignment better than the sliding bushings.


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/S2vczuVusgM) &mdash; content and formatting may not be reliable*
