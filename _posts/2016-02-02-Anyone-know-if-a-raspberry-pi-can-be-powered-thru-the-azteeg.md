---
layout: post
title: "Anyone know if a raspberry pi can be powered thru the azteeg?"
date: February 02, 2016 00:12
category: "Discussion"
author: Jim Stone
---
Anyone know if a raspberry pi can be powered thru the azteeg? im trying to cut back a little on the wiring and plugs.





**Jim Stone**

---
---
**Zane Baird** *February 02, 2016 19:16*

You can use something like this:

[http://www.amazon.com/DROK-Regulator-Converter-Switching-Stabilizers/dp/B00Q48BRFO/ref=sr_1_5?ie=UTF8&qid=1454440396&sr=8-5&keywords=drok+dc+dc+5V+converter](http://www.amazon.com/DROK-Regulator-Converter-Switching-Stabilizers/dp/B00Q48BRFO/ref=sr_1_5?ie=UTF8&qid=1454440396&sr=8-5&keywords=drok+dc+dc+5V+converter)



I use a similar step-down converter to power my LEDs. It wires straight into the 24V supply


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/Ba5rfGAuc7Y) &mdash; content and formatting may not be reliable*
