---
layout: post
title: "Hey guys, I was wondering round the internet again last night in search for another 3d printer"
date: May 25, 2015 19:38
category: "Discussion"
author: X Copter
---
Hey guys, I was wondering round the internet again last night in search for another 3d printer. I found this which is pretty much exactly what I was looking for but its a little big. How hard would it be to scale the Eustathios down to around an 8-10" build volume?



Also, you kiwi guys, where are you sourcing your parts? From my understanding misumi doesn't ship to nz





**X Copter**

---
---
**ThantiK** *May 25, 2015 19:50*

If you want to scale it down, know that these builds kind of have a grandparent in the T-slot-tantillus design/tantillus. 


---
**X Copter** *May 25, 2015 20:46*

What do you mean?


---
**Gus Montoya** *May 25, 2015 21:20*

Maybe your looking for something like this?  [http://www.thingiverse.com/thing:513955](http://www.thingiverse.com/thing:513955)


---
**ThantiK** *May 25, 2015 23:31*

**+X Copter** -- this is the Ingentis/Eusthathios' grandparent (the printer their designs originated from): [http://forums.reprap.org/read.php?279,190661](http://forums.reprap.org/read.php?279,190661)


---
**Mike Thornbury** *May 26, 2015 02:41*

You can source most of your parts from Aliexpress. I'm an ex-Kiwi, but order stuff for my Dad in Paraparaumu and for my daughter in Wellington all the time.



You might have to get a friendly American or European for the gears, though, I haven't seen any with an 8mm/10mm hole, but then I wasn't looking.



Extrusion is pretty cheap, as are all the fittings, motors, controllers, etc. certainly a lot cheaper than Misumi.


---
**X Copter** *May 26, 2015 03:34*

I'm looking for something a bit larger than that Gus. 


---
**X Copter** *May 28, 2015 00:07*

So, I've decided I will build an identical replica of the Eustathios except I will be scale it down to 250x250x250mm build volume. 

What's the original build volume (need it to be exact as I will use this to work out how many mm to take off on the parts)

Anything I should look out for?


---
**Gus Montoya** *May 28, 2015 00:30*

You can buy bearings from **+Derek Schuetz** He has a bunch that will be useful to you. At half the size I think what he has will fit the bill.


---
**Derek Schuetz** *May 28, 2015 01:33*

Robotdigg has most what your looking for the only exception is the 10mm is bearings


---
**X Copter** *May 28, 2015 05:31*

Oh and whats the difference between the two versions of xy axis in the BOM?


---
*Imported from [Google+](https://plus.google.com/+XCopter/posts/EHQfqCsDgi4) &mdash; content and formatting may not be reliable*
