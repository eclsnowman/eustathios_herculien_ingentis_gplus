---
layout: post
title: "I think I've got the Falling Z-stage issue licked!"
date: September 22, 2014 00:04
category: "Deviations from Norm"
author: Mike Miller
---
I think I've got the Falling Z-stage issue licked! I figured what the problem needed was a clutch or brake, but couldn't figure out the easiest way to do so. Playing about with a bit of GT2 belt, I recalled a mechanism that would drag in one direction only, because it had a spring on a belt that was only pulled tight in one direction. Looking at the video, I plan to have a mount with two springs, tension for raising and lowering the bed. 



The video is with the full weight bed, roughly 6 lbs. I plan on lifting it with two motors, as someone intimated that that was about the max lift with one motor. 


**Video content missing for image https://lh5.googleusercontent.com/-936q56FCF1Q/VB9nO8c6cAI/AAAAAAAAHzQ/nrSEY5IWl5U/s0/IGentUS%252BBrake.mp4.gif**
![images/83f0318c8eaa6715b0a7cea0d002282b.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/83f0318c8eaa6715b0a7cea0d002282b.gif)



**Mike Miller**

---


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/DRUD47Hvz48) &mdash; content and formatting may not be reliable*
