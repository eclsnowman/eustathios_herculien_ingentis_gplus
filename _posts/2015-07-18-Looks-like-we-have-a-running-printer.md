---
layout: post
title: "Looks like we have a running printer :-)"
date: July 18, 2015 15:26
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
Looks like we have a running printer :-)





**Frank “Helmi” Helmschrott**

---
---
**Brandon Cramer** *July 18, 2015 17:02*

That is awesome! Nice looking printer!!


---
**Eric Lien** *July 18, 2015 17:08*

Looks to be running beautifully. Great job.


---
**Gus Montoya** *July 19, 2015 02:21*

Can you share pictures of your completed wiring.


---
**Jeff DeMaagd** *July 19, 2015 03:45*

That looks very nice. What parts did you use for mounting the ball screws? Have you had previous experience with ball screws on a 3d printer?﻿


---
**Frank “Helmi” Helmschrott** *July 19, 2015 04:32*

**+Gus Montoya** I will once it's completely done. I've not yet cleaned things up. Although you won't see too much details in the wiring as i have pulled everything together to bundles.



**+Jeff DeMaagd** I used the default parts - i had the ball screws machined down to the same dimensions than the leadscrews in the bom. It's my first experience with ball screws at all.



I will write up some build details later on.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/JTb7UVa8DpF) &mdash; content and formatting may not be reliable*
