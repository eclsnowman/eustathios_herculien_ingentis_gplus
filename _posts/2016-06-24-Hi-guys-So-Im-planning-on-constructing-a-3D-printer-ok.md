---
layout: post
title: "Hi guys, So I'm planning on constructing a 3D printer ok?"
date: June 24, 2016 10:39
category: "Discussion"
author: Paulo Teixeira
---
Hi guys, 

So I'm planning on constructing a 3D printer ok?

Where to start? Is there any good "Step-by-Step" guide  you recommend to start?



Thanks





**Paulo Teixeira**

---
---
**Miguel Barroso** *June 24, 2016 11:09*

Hi Paulo, if you are new to 3d Printing, you should check out this topic in portuguese [https://forum.zwame.pt/threads/projecto-impressora-3d-reprap-prusa.782591/](https://forum.zwame.pt/threads/projecto-impressora-3d-reprap-prusa.782591/)

This HercuLien/Eustathios are great machines, but quite expensive and more chalenging. A Prusa style printer might be better for someone (like me also) that is starting.




---
**Michaël Memeteau** *June 24, 2016 12:56*

If you're near Lisbon this week-end, go to the Maker Faire and look for me (stand 158, Black Panther 3D Printer), I can give you direct advice about Herculien and not only. Cheers.


---
**Miguel Barroso** *June 24, 2016 13:35*

I'll pay you a visit Michaël... I guess we've met before, if not in real life, at least on-line (bicycle stuff). I'm assembling a iTopie (with some changes), but I'm really fascinated with these more evolved printers.




---
**Michaël Memeteau** *June 24, 2016 13:59*

Great! Already 2 passions we're sharing! Eager to meet you...


---
*Imported from [Google+](https://plus.google.com/102245887933358980194/posts/ca2DMJRVNUw) &mdash; content and formatting may not be reliable*
