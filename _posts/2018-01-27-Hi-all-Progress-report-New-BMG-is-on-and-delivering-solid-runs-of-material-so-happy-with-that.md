---
layout: post
title: "Hi all, Progress report. New BMG is on and delivering solid runs of material, so happy with that"
date: January 27, 2018 18:45
category: "Discussion"
author: Bruce Lunde
---
Hi all, Progress report. New BMG is on and delivering solid runs of material, so happy with that.  I think I need some input on what I am looking at on these prints, for feedback on what I may need to adjust to get better quality now that the flow is good.  I do have a larger nozzle on this as part of the E3D Volcano setup.  My goal is print  parts for a inMoov robotics project, but I know I need to clean these things up before I attempt to do larger parts.  I ran the extruder at 230 C, and the heated bed at 110 C.  Movement looks good to me, but put a link to a video in as well, so that I can get feedback there as well.  Any and all input is welcome.

TIme-Lapse Youtube:  
{% include youtubePlayer.html id=O_zcwVkv8xs %}
[https://youtu.be/O_zcwVkv8xs](https://youtu.be/O_zcwVkv8xs)



![images/aca127bece885735d7b78555a7dfe64c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/aca127bece885735d7b78555a7dfe64c.jpeg)
![images/7ee29da36f3a070a329fe7f80d31aead.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7ee29da36f3a070a329fe7f80d31aead.jpeg)
![images/80dc1a3d390125384f486cc8eea7a3b8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/80dc1a3d390125384f486cc8eea7a3b8.jpeg)
![images/79c7d1c1eb7ee8e34d5461331813d3dc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/79c7d1c1eb7ee8e34d5461331813d3dc.jpeg)

**Bruce Lunde**

---
---
**Dennis P** *January 27, 2018 20:11*

WOW, that thing really pushes the plastic out.  1.2mm nozzle, your making Pasta! 

That torroid is a challenging model for a well tuned printer on any day. I would not get discouraged buy it. I'm no expert, but it looks to me like there are multiple things to tweak on that print, and some of them are interlaced. To me, I would be looking the slicer settings for speeds, bridging, retraction. Remember, scaling things up like nozzle or filament diameter is not a linear function, but a cubic function, so double the size = 8 times volume and complexity. 

Have you tried printing something a little more predictable?  Here is the KSSlicer calib cube that I like. I scaled it up for you to 50mm and 75mm. That way its proportional to the 0.4mm nozzle /25mm cube dimensions. You've got straight enough segments for straightaways, and then the radius corners eliminate the acceleration issues at corners and direction changes. Print it in vase mode (single extrusion width) and see how it comes out. Easier to diagnose issues on a known good challenge than a complex shape like the torriod or even a benchy.    


---
**Dennis P** *January 27, 2018 20:12*

[drive.google.com - 50mm_cube.stl](https://drive.google.com/open?id=13g_sipdF5KAxQCh6eurdTW8uFbn8Y6Q5)


---
**Bruce Lunde** *January 27, 2018 21:23*

**+Dennis P**. Thanks, I will go give that a try. I admit, I really should have learned on a small printer before attempting a big one like the HercuLien   I am learning the process, and have sli3er and Cura and still need to figure out all the settings for this printer and my smoothie board.


---
**Eric Lien** *January 27, 2018 22:05*

Can you post what plastic material, and what print settings were used (if simplyfy3d post the factory file, otherwise if cura or Slic3r details of all the settings used).


---
**Eric Lien** *January 27, 2018 22:06*

Also have you calibrated esteps, confirmed extrusion numbers in firmware are all correct?


---
**Bruce Lunde** *January 27, 2018 22:51*

**+Eric Lien** ABS, 230C print temp, 110C bed Temp. to start.

E-step for the extruder - confirmed.

E-step for the unit, I did that a year ago, but I will go and double check them, and post.

I did not create this g-code recently, so I will go back and check the slic3r setting I create it wih (also a year ago), the files were still on my sd card.


---
**Brad Vaughan** *January 27, 2018 23:52*

First of all, congrats.  You will be able to run through some plastic in a serious hurry!



Agree with **+Eric Lien**.  I never just set E-steps mathematically and walk away.  You can fine tune it a bit by marking the filament, measuring the distance to the extruder, telling it to extrude a specific length, remeasuring, then adjusting E-steps until you're spot on.  Triffid Hunter's Calibration Guide is always a good place to start if you thing you might have extrusion issues ([http://reprap.org/wiki/Triffid_Hunter's_Calibration_Guide#E_steps](http://reprap.org/wiki/Triffid_Hunter's_Calibration_Guide#E_steps)).





[reprap.org - Triffid Hunter's Calibration Guide - RepRapWiki](http://reprap.org/wiki/Triffid_Hunter%27s_Calibration_Guide#E_steps)


---
**Bruce Lunde** *January 28, 2018 01:23*

**+Eric Lien** Here is the slic3r setup used to generate my gcode that I printed those two examples with.  I am going to go  and get a new version of that software.  I just used a basic Cura download with the .stl that Dennis P sent over, and will attempt that tonight.  I will grab a copy of my smoothieconfig off the SD card and push that out with a link to my google drive shortly.  



# generated by Slic3r 1.3.0-dev on 

avoid_crossing_perimeters = 0

bed_shape = 0x0,350x0,350x350,0x350

bed_temperature = 110

before_layer_gcode = 

between_objects_gcode = 

bottom_infill_pattern = archimedeanchords

bottom_solid_layers = 3

bridge_acceleration = 0

bridge_fan_speed = 100

bridge_flow_ratio = 1

bridge_speed = 30

brim_connections_width = 0

brim_width = 0

compatible_printers = 

complete_objects = 1

cooling = 1

default_acceleration = 0

disable_fan_first_layers = 3

dont_support_bridges = 1

duplicate_distance = 6

end_filament_gcode = "; Filament-specific end gcode \n;END gcode for filament\n"

end_gcode = M104 S0 ; turn off temperature\nG28 X0  ; home X axis\nM84     ; disable motors\n

external_perimeter_extrusion_width = 0

external_perimeter_speed = 30

external_perimeters_first = 0

extra_perimeters = 1

extruder_clearance_height = 20

extruder_clearance_radius = 20

extruder_offset = 0x0

extrusion_axis = E

extrusion_multiplier = 1

extrusion_width = 0.72

fan_always_on = 0

fan_below_layer_time = 60

filament_colour = #FFFFFF

filament_cost = 0

filament_density = 0

filament_diameter = 1.75

filament_max_volumetric_speed = 0

filament_notes = ""

filament_settings_id = 

fill_angle = 45

fill_density = 40%

fill_gaps = 1

fill_pattern = rectilinear

first_layer_acceleration = 0

first_layer_bed_temperature = 110

first_layer_extrusion_width = 0

first_layer_height = 0.3

first_layer_speed = 30

first_layer_temperature = 242

gap_fill_speed = 15

gcode_arcs = 0

gcode_comments = 1

gcode_flavor = reprap

has_heatbed = 1

host_type = octoprint

infill_acceleration = 0

infill_every_layers = 1

infill_extruder = 1

infill_extrusion_width = 0

infill_first = 0

infill_only_where_needed = 0

infill_overlap = 15%

infill_speed = 30

interface_shells = 0

interior_brim_width = 0

layer_gcode = 

layer_height = 0.3

max_fan_speed = 100

max_print_speed = 30

max_volumetric_speed = 0

min_fan_speed = 35

min_print_speed = 10

min_skirt_length = 0

notes = 

nozzle_diameter = 1.2

octoprint_apikey = 

only_retract_when_crossing_perimeters = 1

ooze_prevention = 0

output_filename_format = [input_filename_base].gcode

overhangs = 1

perimeter_acceleration = 0

perimeter_extruder = 1

perimeter_extrusion_width = 0

perimeter_speed = 30

perimeters = 3

post_process = 

pressure_advance = 0

print_host = 

print_settings_id = 

printer_notes = 

printer_settings_id = 

raft_layers = 0

regions_overlap = 0

resolution = 0

retract_before_travel = 2

retract_layer_change = 1

retract_length = 4

retract_length_toolchange = 10

retract_lift = 0

retract_lift_above = 0

retract_lift_below = 0

retract_restart_extra = 0

retract_restart_extra_toolchange = 0

retract_speed = 6

seam_position = random

serial_port = 

serial_speed = 250000

shortcuts = support_material

skirt_distance = 8

skirt_height = 1

skirts = 5

slowdown_below_layer_time = 5

small_perimeter_speed = 20

solid_infill_below_area = 70

solid_infill_every_layers = 0

solid_infill_extruder = 1

solid_infill_extrusion_width = 0

solid_infill_speed = 20

spiral_vase = 0

standby_temperature_delta = -5

start_filament_gcode = "; Filament gcode\n"

start_gcode = G28 ; home all axes\nG1 Z5 F5000 ; lift nozzle\n

support_material = 1

support_material_angle = 0

support_material_buildplate_only = 0

support_material_contact_distance = 0.2

support_material_enforce_layers = 2

support_material_extruder = 1

support_material_extrusion_width = 0

support_material_interface_extruder = 1

support_material_interface_layers = 3

support_material_interface_spacing = 0

support_material_interface_speed = 60%

support_material_pattern = pillars

support_material_spacing = 2.5

support_material_speed = 30

support_material_threshold = 60%

temperature = 240

thin_walls = 1

threads = 4

toolchange_gcode = 

top_infill_extrusion_width = 0

top_infill_pattern = archimedeanchords

top_solid_infill_speed = 20

top_solid_layers = 3

travel_speed = 130

use_firmware_retraction = 0

use_relative_e_distances = 0

use_volumetric_e = 0

vibration_limit = 0

wipe = 1

xy_size_compensation = 0

z_offset = 0

z_steps_per_mm = 0




---
**Bruce Lunde** *January 28, 2018 01:25*

**+Brad Vaughan** I will go back and do a recalibration.  I also found that a couple of belts were a bit loose, so I also made an adjustment on those before I started the cube print that I got from **+Dennis P**


---
**Brad Vaughan** *January 28, 2018 01:34*

Best of luck getting it worked out. Great community here. You'll get the help you need.


---
**Dennis P** *January 28, 2018 06:21*

**+Bruce Lunde** make sure that your mechatronics are  in order. Nothing worse than chasing gremlins and it turns out it was 'cuz of a loose grub screw. DAMHIKT! **+Brad Vaughan**'s suggestion of Triffid's pages is really good. You might confirm steps and the like on the Prusa calculators too:

[prusaprinters.org - RepRap Calculator - Prusa Printers](https://www.prusaprinters.org/calculator/)  

All this stuff is tweakable. You'll get it figured out. 


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/cpUrVv8AmuR) &mdash; content and formatting may not be reliable*
