---
layout: post
title: "I got everything electrical mounted. What size wiring should I use?"
date: July 15, 2015 21:31
category: "Discussion"
author: Brandon Cramer
---
I got everything electrical mounted. What size wiring should I use? Is there a picture or diagram with everything hooked up that I can use for reference? 



![images/05fc927bb42a1bd3fac5679d036df80f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/05fc927bb42a1bd3fac5679d036df80f.jpeg)
![images/e056f6f3e619966ca2612567ad094fb8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e056f6f3e619966ca2612567ad094fb8.jpeg)

**Brandon Cramer**

---
---
**Eric Lien** *July 15, 2015 22:15*

Wire size is a function of current components will use and length of the run. So what bed wattage did you go with. For the steppers using similar to what you have on the current robotdigg wire leads will work fine. The hotend should be 40watt.



There are several wire size calculators online. Here are a few:

[http://www.solar-wind.co.uk/cable-sizing-DC-cables.html](http://www.solar-wind.co.uk/cable-sizing-DC-cables.html)



[http://www.csgnetwork.com/wiresizecalc.html](http://www.csgnetwork.com/wiresizecalc.html)﻿


---
**Eric Lien** *July 15, 2015 22:20*

For the steppers you can also get 4 conductor shielded cables (beldin makes good stuff). Bring the shield wiring back to ground (at just one end, and bring them all together in one place to avoid ground looping.) This shielding helps avoid crosstalk and noise from the steppers.﻿


---
**Eric Lien** *July 15, 2015 22:28*

Fore wire management I like those sticky squares that you can ziptie to. Keeps things clean. Avoid AC mains running parallel to DC wiring if you can help it, and if you need to cross them over do it at 90degree angles.


---
**Brandon Cramer** *July 15, 2015 22:41*

Are you talking about the wiring going from the Azteeg X5 Mini to the stepper motors? Will these leads work that I got from robotdigg? 



I did get the 500W heated bed.



I'm mainly concerned about getting the wiring going from the outlet switch to the Relay and Meanwell power supply. If there is a diagram, that would be awesome!!!


---
**Eric Lien** *July 15, 2015 22:46*

The switch has switched and non-switched contacts available. You can tell by using a multi-meter with a continuity test what is what and what goes where as far as the switch goes.


---
**Eric Lien** *July 15, 2015 22:49*

This is for Herculien but the SSR connects the same way: [http://i.imgur.com/nmrUim8.png](http://i.imgur.com/nmrUim8.png)


---
**Eric Lien** *July 15, 2015 22:50*

120V power from the switch/plug in your picture flows through the load side of the SSR, into the bed, out of the bed then back to neutral. The normal bed output on the board connects into the SSR inputs and acts to signal the SSR load contact to open and close.﻿


---
**Eric Lien** *July 15, 2015 23:07*

**+Brandon Cramer**​ when you are all done can you take some finished pics. This so perfectly matches the model it would be great work the github.


---
**Brandon Cramer** *July 15, 2015 23:30*

I think with the X3 wiring diagram, I can manage to get this wired. I will take some pictures once I get it wired and know it works. :)


---
**Eric Lien** *July 15, 2015 23:48*

**+Brandon Cramer** thanks man.


---
**Brandon Cramer** *July 15, 2015 23:57*

**+Eric Lien** You bet man!! 


---
**Gus Montoya** *July 16, 2015 05:48*

**+Brandon Cramer** I'll send you a link when I get home (about midnight PST). I have a link that will help with the power switch to PSU. I'm still trying to figure out the rest. Especially the Vicki 2.0 LCD screen. Others seem to get it on the fly, I keep scratching my head. ugh...


---
**Gus Montoya** *July 16, 2015 08:03*

I used these instructions to wire up the powerswitch

[http://reprap.org/wiki/Mondrian2.2_Build_Manual](http://reprap.org/wiki/Mondrian2.2_Build_Manual)


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/3ejc9RG2VZH) &mdash; content and formatting may not be reliable*
