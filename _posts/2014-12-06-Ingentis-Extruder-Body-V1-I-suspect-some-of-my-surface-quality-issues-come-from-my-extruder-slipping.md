---
layout: post
title: "Ingentis Extruder Body V1 I suspect some of my surface quality issues come from my extruder slipping"
date: December 06, 2014 04:43
category: "Discussion"
author: Mike Miller
---
Ingentis Extruder Body V1



I suspect some of my surface quality issues come from my extruder slipping. Not completely unexpected considering all of the kilometers of filament it's pushed. It was the extruder for my 3DR and while it worked well enough, the extruder iteration in the Ingentis BOM is significantly better in a number of ways. 



Except it's got a 1/2" hole for the push-fit connectors....



A) I could look for and order a 1/2" connector with a 3/16" push fit, and run the risk of it not working (as has happened before when I SWAGed (*) it

B) Or I can look at the $1.36 I lost printing this and say "hey guys! Where's V2 that was tweaked to use metric push to fit couplers? And print another. 



(Which is a pity...this print was really nice!)





SWAG = Scientific Wild Assed Guess

![images/2e8d0ba570c54f309bd6c72278f3ef6a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2e8d0ba570c54f309bd6c72278f3ef6a.jpeg)



**Mike Miller**

---
---
**James Rivera** *December 06, 2014 06:41*

Excellent holes!


---
**Mike Miller** *December 06, 2014 13:38*

Yeah I really Love the print quality the ingentis printertype is capable of. 


---
**Mike Miller** *December 07, 2014 00:18*

So..Took a Mic to the opening, and the thread diameter of the Push-to-fit connector...printed a cylinder to those dimensions, screwed it on the connector, glued it in the extruder...perfection. :D


---
**James Rivera** *December 07, 2014 01:03*

Sometimes we forget: simple is good. :)


---
**Jarred Baines** *December 07, 2014 15:29*

I only had metric fittings around too **+Mike Miller** - I am still running on a butt-load (actual unit of measurment) of hot-melt glue! (it was only meant to be temporary to test the extruder/hotend when setting up.)



I will go for your soloution if it ever pops out.



Did recently hook up and airtripper BSP edition based on (I think) **+Tim Rastall**'s recommendation - I love it... at least double the retract speeds and with the proper hobbed insert vs hobbed bolt the print quality for extremely fine work is much better.



That V1 extruder also did a great job though, don't throw it out!


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/RKR5THf93HD) &mdash; content and formatting may not be reliable*
