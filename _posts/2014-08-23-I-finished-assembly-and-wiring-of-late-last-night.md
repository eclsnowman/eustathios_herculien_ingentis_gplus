---
layout: post
title: "I finished assembly and wiring of late last night"
date: August 23, 2014 12:11
category: "Deviations from Norm"
author: Eric Lien
---
I finished assembly and wiring of #HercuLien late last night. Here are some pictures. I am really happy with the precision feel on the gantry motion, the total lack of friction when moving the gantry by hand,  and the package as a whole. The only thing remaining mechanically is the new side panels. 



Next up is making some tweaks to the 3d model and uploading it to my git repository. 



Once all that is done I will finish the removable direct drive extruder module that can mount on the carriage.  That was one of my motivations on this project.  I want the versatility of direct drive at low speeds,  and the weight savings of bowden at high speeds. 



Below is a YouTube link to HercuLien's first moves:


{% include youtubePlayer.html id=bbYtnJxhYJA %}
[http://youtu.be/bbYtnJxhYJA](http://youtu.be/bbYtnJxhYJA)﻿



![images/94ed498f02afb2d305fff614dc462161.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/94ed498f02afb2d305fff614dc462161.jpeg)
![images/3425a7ffdf4164c259609ddbf564d6f1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3425a7ffdf4164c259609ddbf564d6f1.jpeg)
![images/62e45319888387be11c2c694cb8fc425.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/62e45319888387be11c2c694cb8fc425.jpeg)
![images/0e8bb98caec1108c7a38e125cc6c8834.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0e8bb98caec1108c7a38e125cc6c8834.jpeg)
![images/48a08b8b4331311b1e34793dee042e17.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/48a08b8b4331311b1e34793dee042e17.jpeg)
![images/25f9088b00b190029958773c1c542f85.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/25f9088b00b190029958773c1c542f85.jpeg)
![images/4850167c2cd5c8cbb244b56b0e852ff0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4850167c2cd5c8cbb244b56b0e852ff0.jpeg)
![images/c4cf33bca9a4beccf3d537ed70061990.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c4cf33bca9a4beccf3d537ed70061990.jpeg)
![images/b7165ce932cffacf9d822a8e67252e47.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b7165ce932cffacf9d822a8e67252e47.jpeg)
![images/b59b49ea322aac82f87933475254ec52.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b59b49ea322aac82f87933475254ec52.jpeg)
![images/14730378a8f4d0fa785a7e2dfee28a80.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/14730378a8f4d0fa785a7e2dfee28a80.jpeg)
![images/6d5cc1dbcfdc967a8504d30a50d1ca09.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6d5cc1dbcfdc967a8504d30a50d1ca09.jpeg)
![images/818df72dd8badda10a397598b04f593c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/818df72dd8badda10a397598b04f593c.jpeg)

**Eric Lien**

---
---
**Jason Smith (Birds Of Paradise FPV)** *August 23, 2014 12:20*

Nice! That thing is a beast. 


---
**Sven Eric Nielsen** *August 23, 2014 12:29*

Wow, nice piece of hardware. Would really like to see some videos. And of course it would be interesting so see the print speed with an acceptable quality ;) 


---
**Eric Lien** *August 23, 2014 12:39*

**+Sven Eric Nielsen** video in the description above.  Print images later this weekend or early next week. I need to catch up on sleep and spend some quality time with the family to recharge. 


---
**Andreas Thorn** *August 23, 2014 14:42*

Shiny hood! Nice machine! 


---
**Eric Lien** *August 23, 2014 15:47*

**+Shauki Bagdadi** I had my Corexy enclosed.  But the panels mounted to the outside. Now they will mount on tabs inside the frame rails to leave room for the slide mounts of the x/y drive motors and other items mounted to the frame exterior. They will be mounted with wing nuts for easy removal for maintenance. 



On my Corexy the chamber reaches thermal equilibrium at around 50c with the bed at 110c. I found this works great for minimizing if not eliminating abs warp. 


---
**Øystein Krog** *August 23, 2014 16:14*

This is such a damn nice printer, please post more details :)


---
**Eric Lien** *August 23, 2014 16:51*

**+Øystein Krog** my entire SolidWorks library of parts and assemblies will be available on github soon. I will also include Step files, STL's, and bill of materials. 


---
**Eric Lien** *August 23, 2014 16:53*

**+Shauki Bagdadi** I don't plan on selling anything. I just really love this as a hobby and the community more than anything . But if anybody feels like hiring me for a high salary to play with this stuff for a living I wouldn't turn it down ;) 


---
**Joe Spanier** *August 24, 2014 13:16*

**+Eric Lien** that's pretty much my life ATM. I'm trying to make it my life for a few years haha. 


---
**Maxim Melcher** *August 24, 2014 18:19*

Direct drive & bowden ist the excellent choice! 


---
**Jarred Baines** *September 26, 2014 09:52*

That machine is awesome **+Eric Lien** ! So many good design choices there, and beautiful packaging - it's like you've done this before or something ;-P



What is the red strip you have in the v slot? Is it v-slot only stuff or... ?






---
**Eric Lien** *September 26, 2014 11:44*

**+Jarred Baines** works on both Misumi and open build v-slot: 

[http://openbuildspartstore.com/slot-cover-panel-holder/](http://openbuildspartstore.com/slot-cover-panel-holder/)


---
**Jarred Baines** *September 26, 2014 22:37*

Looking at it, it doesn't use the "V" part of the extrusion for location, I'm pretty sure it'd fit on my extrusions (dbasix branded) - at least I hope so! Looks great! Might just have to try it.


---
**Eric Lien** *September 26, 2014 23:23*

Misumi also sells them. I think so long as the slot width and flange thinkness is close there should be no problems.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/K8iER92ot7k) &mdash; content and formatting may not be reliable*
