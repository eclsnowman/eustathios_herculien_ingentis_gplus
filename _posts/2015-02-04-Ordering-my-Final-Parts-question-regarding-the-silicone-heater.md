---
layout: post
title: "Ordering my Final Parts, question regarding the silicone heater"
date: February 04, 2015 21:42
category: "Discussion"
author: Derek Schuetz
---
Ordering my Final Parts, question regarding the silicone heater. Can someone send me the email of exactly what i need to tell them i need? so i can just copy and paste it in. 





**Derek Schuetz**

---
---
**Florian Schütte** *February 04, 2015 21:52*

Size in mm

Wattage in W

Voltage in V

with or w/o Thermistor



These were the 4 informations i have to give them.

I ordered here:  [http://www.aliexpress.com/snapshot/6396179264.html](http://www.aliexpress.com/snapshot/6396179264.html)



simply contact them. They will tell you what to do. Very friendly support with extra tips and tricks.


---
**Derek Schuetz** *February 04, 2015 21:58*

is there a reason why the BOM listed one is smaller then the size of the buildplate?


---
**Florian Schütte** *February 04, 2015 22:11*

Which printer?


---
**Derek Schuetz** *February 04, 2015 22:14*

Herculien **+Florian Schütte** 


---
**Florian Schütte** *February 04, 2015 22:49*

It is smaller to fit in the heated bed assembly. Have you ever looked at the drawings?


---
**Derek Schuetz** *February 04, 2015 22:59*

I just realized I was reading it as 280 not 380 that's why I was questioning how small it was compared


---
**Florian Schütte** *February 04, 2015 23:06*

:D


---
**Eric Lien** *February 04, 2015 23:08*

**+Derek Schuetz** it is just a little bit smaller than the aluminum heat spreader so it has a lip to rest on into the MDF insulation layer


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/7Yfakjc1ZMC) &mdash; content and formatting may not be reliable*
