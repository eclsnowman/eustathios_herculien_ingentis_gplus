---
layout: post
title: "Progress of my smaller Eustathios. Thanks Eric Lien for the Eustathios-spider files and Richard Horne for the Sli3DR Z axis"
date: November 15, 2014 05:35
category: "Deviations from Norm"
author: hon po
---
Progress of my smaller Eustathios. Thanks **+Eric Lien** for the Eustathios-spider files and **+Richard Horne** for the Sli3DR Z axis.



Just realize my spectra line routing is wrong, so need to fix that. Still need to change the 6900 bearing holder & geared Z motor mount, and decide on the extruder.



step file is in here:

[https://drive.google.com/file/d/0B4bLJHgWDzxfbGZ4bEYxUXJESjQ/view?usp=sharing](https://drive.google.com/file/d/0B4bLJHgWDzxfbGZ4bEYxUXJESjQ/view?usp=sharing)

zip version:

[https://drive.google.com/file/d/0B4bLJHgWDzxfdmktelhwQ0xUOFk/view?usp=sharing](https://drive.google.com/file/d/0B4bLJHgWDzxfdmktelhwQ0xUOFk/view?usp=sharing)



Would like some feedback on anything I should watch out.

![images/09d498ff9b23a01ec0cd2ad0623e58b8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/09d498ff9b23a01ec0cd2ad0623e58b8.jpeg)



**hon po**

---
---
**D Rob** *November 15, 2014 05:42*

you should really check out tantillus by +sublime he is where ingentis was inspired from


---
**hon po** *November 15, 2014 05:56*

Thanks. Last time I checked, it mixed metric/imperial parts, and it's difficult to get imperial parts here. I want 200x200x350 build volume, it seems easier to shrink the Eustathios than expand the Tantillus.


---
**Mike Thornbury** *November 15, 2014 06:21*

Tantillus? It was awful from a build perspective - I'm with you **+hon po** - the mix of metric and imperial meant it was up for a complete redesign. And the parts he did use were a bit... esoteric. Rather than using standard (and thus, easy to obtain) parts, it looked like he designed it with what he had in his shed.


---
**Mike Thornbury** *November 15, 2014 06:23*

Nice work **+hon po** - I'm keen to see how your z-axis mechanism works. I've realised getting 2GT belts in the right close-loop length is going to add quite a bit to the base cost - $2-3 each - plus finding a 5mm shaft to mount the pulleys on means that I am looking for a different z-axis for the PlySli3DR.


---
**Mike Thornbury** *November 15, 2014 06:26*

And... speaking of extruders, do you have a preferred design? I was hoping to have it mounted inside the chassis, but I think that idea is fast disappearing. To do so would mean I need to find something very petite.


---
**D Rob** *November 15, 2014 06:41*

you really want to use 26 of the 8020 corner brackets? check this out. Please forgive my redneck accent.: [https://plus.google.com/u/0/108729945898131117315/videos?pid=5989375241070804258&oid=108729945898131117315](https://plus.google.com/u/0/108729945898131117315/videos?pid=5989375241070804258&oid=108729945898131117315)


---
**D Rob** *November 15, 2014 06:43*

oh the 1/4-20 button head screws or m5 button head screws work fine with mt technique and **+Thomas Sanladerer** also has a good video on tapping the easy way:
{% include youtubePlayer.html id=BR85I2ILeGw %}
[Quick tips: Outlaw tapping](http://www.youtube.com/watch?v=BR85I2ILeGw)


---
**D Rob** *November 15, 2014 06:46*

you will probably need to drill before tapping. use the recommended drill bit for the tap you use. and edit i meant m6 button head screw


---
**hon po** *November 15, 2014 07:00*

**+Mike Thornbury** there's no perfect design, and you cannot please everyone. So I'm OK with designing with things on hand. By releasing whatever design and let it evolve has been a great thing of the community.



cost wise, sli3dr is better, but ingentis is more proven at the moment. Don't find my preferred extruder yet.


---
**hon po** *November 15, 2014 07:14*

**+D Rob** I found cheap 2028 bracket locally, it's easier to use it than drilling & taping.



On the street now, will check the video later.



I found taping drill which do drilling & tapping in one go. But I'm more worry about alignment of the frame.


---
**D Rob** *November 15, 2014 07:16*

my vid shows how to make most extrusion cuts non critical and alignment easy and drilling/taping like **+Thomas Sanladerer** s vid is quick and simple


---
**Mike Thornbury** *November 15, 2014 07:46*

I work a lot with extrusion - just buy the self-tapping screws - much easier and just as solid as drilling and tapping. I use these a lot, and the M3 ones, with a washer: [http://www.aliexpress.com/item/100PCS-Stainless-Steel-304-M5-20-Hexagon-Socket-Head-Self-Tapping-Screw-Sound-screw-model-screw/1646384044.html](http://www.aliexpress.com/item/100PCS-Stainless-Steel-304-M5-20-Hexagon-Socket-Head-Self-Tapping-Screw-Sound-screw-model-screw/1646384044.html)


---
**D Rob** *November 15, 2014 08:22*

**+Mike Thornbury**​ you use these in the t-slots like t nuts? the button head screw :[https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT5vkVFeqHegupiEQ6TUvpYR9oL8C64gwvpxBTMAw7Gr0ESZDa2mg](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT5vkVFeqHegupiEQ6TUvpYR9oL8C64gwvpxBTMAw7Gr0ESZDa2mg) has the right profile to fit in the slot as if it were a t-nut﻿


---
**D Rob** *November 15, 2014 08:23*

with the head in the slot and a small hole through the opposite side you can fit an allen wrench to tighten them


---
**Mike Thornbury** *November 15, 2014 10:03*

Instead of using t-nuts, you drill a hole through the side in the channel of the extrusion and straight into the end of the extrusion. It works well and grips like a schoolboy ;) You do need to make accurate cuts, though, as the only thing keeping your assembly square is the face of the cut. I dialled in my drop saw a few weeks ago and after about 18 different cuts, my right angles measured between 89.99 and 90.01 degrees, using nothing but self-tapping bolts.


---
**Terje Moe** *November 15, 2014 11:28*

Nice work! Do you have any close up photos of your Z-axis drive mechanism and Spectra routeing or maybe some schematic drawings?


---
**Eric Lien** *November 15, 2014 12:31*

Nice job. One thing I will warn about is when you build remember all the tnuts. Eustathios is a nightmare to get one in that was forgotten part way through the build.



I also agree with **+D Rob**​. Assembly with the tapped ends method is stronger. I did both on mine because I overbuild everything. But I feel the threaded ends method made things more ridged that the brackets did.


---
**Mike Miller** *November 15, 2014 13:11*

You may find you want to relocate the XY axes motors out of the way...they seem to eat into the print envelope in unexpected ways. 


---
**Eric Lien** *November 15, 2014 13:35*

**+Mike Miller** I agree, plus keeping them out of the heated zone if you enclose the sides has its benefits.


---
**hon po** *November 15, 2014 16:58*

D Rob Thanks for the video, now I know there are some room for adjustment even when drilling not accurate. There are combination drill/tap/deburr bit that make tapping easy.



Mike Thornbury, I heard different 2020 suppliers have different end hole size. I could imagine with the correct hole size, self tapping screw will give more strength. Does your method require accurate drilling? Could the screw slide before tightening?



**+Eric Lien** Thanks, I'm sure I'll forget some nuts, and I don't like to have extra nuts in the slot that give strange noise when the frame vibrate. I'll depend on the insertable T-nuts to save the day.



I'm surprised the tapping method is stronger than the bracket. Need to adjust the design accordingly.



**+Mike Miller** Thanks, I checked the spacing is OK. The frame is bigger than necessary. I was tempted to shrink the frame further, but one thing I learn from my makibox experience is the more optimized in space, the more difficult for future mod.



I covered up my Prusa i3 in a big box with no problem, so I assume the stepper will be fine with 20 degree more ambient temp, but I may be wrong. Don't like the long GT2 belt when putting motor at bottom. Also, use of cheaper GT2 pulley block one belt path, so the motor move above the gantry. Need more consideration here.


---
**Mike Thornbury** *November 16, 2014 06:34*

I always drill the end. It doesn't need 'precision' - the drill bit seems to automatically follow the path of least resistance. Always lubricate! I use TF2 (I have it in my toolbox for RC) or silicone spray. I have my cordless drill set on 'driver' speed, rather than drill speed and back it out a lot to remove the swarf. It sounds complicated, but when you have all your extrusion clamped together and with the ends facing you, it's really quick and easy and intuitive. I use a lot of 60 and 80 profile, where you will have 3 or 4  holes to drill every end. If it wasn't for self-tapping bolts I would have gone crazy. 



If you are committed to tapping, for whatever reason (need low-profile heads to fit a particular installation, for example), you can get a drill-driver tapping bit, but not here in the third world.


---
*Imported from [Google+](https://plus.google.com/111735302194782648680/posts/9coNUnX3kf9) &mdash; content and formatting may not be reliable*
