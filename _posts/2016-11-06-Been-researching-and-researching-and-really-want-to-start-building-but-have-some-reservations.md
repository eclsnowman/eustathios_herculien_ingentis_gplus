---
layout: post
title: "Been researching and researching and really want to start building but have some reservations"
date: November 06, 2016 22:40
category: "Discussion"
author: Nathan Fisher
---
Been researching and researching and really want to start building but have some reservations. How hard would it be to extend the x and/or y axis to around 18"? Are ball screws for the x and y axis a possibility? 



Right now I have a printer modified to do14.5"x9" so I would really like to go larger at least on one axis. 



One last thing. Has anyone on here talked about the beast 3d printer? Cultivate3d.com





**Nathan Fisher**

---
---
**Ryan Carlyle** *November 06, 2016 22:59*

Ball screws on X/Y will be expensive and slow. No real point to it. 


---
**Nathan Fisher** *November 06, 2016 23:03*

True, I just want precision. Some of the tooling I print I have to hold within .007". Just an idea I've been thinking about. Plus it would just be awesome. 


---
**Jeff DeMaagd** *November 06, 2016 23:56*

0.007" on 18" part is pretty high expectation for melted plastic. It might take some pretty high spec ground ball screws, not just the typical Chinese rolled ball screws, and a pretty bad ass frame too.



I think it might be best to post process on a mill to get the final tolerance, which would get you a smoother surface anyways.



If it were me, if you're still intent on doing that, I'd much rather design a machine from scratch rather than try to shoehorn ball screws into an existing design.


---
**Eric Lien** *November 07, 2016 01:11*

0.007 across what distance? If you mean across 18" build volume... Not gonna happen. This is for the simple reason of thermal deformation during thermoplastics contraction. This deformation will be exaggerated by thick sections, T-intersections interaction, thin to thick transitions, etc. And it is non uniform in X / Y / Z and is highly geometry dependant and varying throughout the model.



I don't mean to sound negative, I just happen to have a little experience in this area specifically. All thermoplastics have a shrink factor. People printing small low tolerance models don't see it as much. As you scale up it just becomes much more evident.


---
**Ryan Carlyle** *November 07, 2016 01:12*

Putting ball screws in XY in a cross gantry 3d printer is like putting a semi truck transmission in a motorcycle. Difficult, expensive, and tragically low performance. 



You can't just run screws slower to make up for the thread pitch -- the added inertia will make the steppers overshoot and ring more, undoing a lot of the stiffness added by the screw. And lower corner speeds exacerbate corner blobbing due to extruder afterflow effects. 



If you want more precision, switch to 9mm wide belts, 0.9 degree steppers, and drop your accel/jerk settings. That should require minimal design changes. 


---
**Nathan Fisher** *November 07, 2016 01:46*

Thanks for the input guys. Yeah I've tried explaining to people at work how hard it is to hold those kind of tolerances by melting plastic. Even stratasys machines which I'm competing with only guarantee a few thousandths per inch.  I print only petg because it has such a low shrink rate. Most of the stuff I sell doesn't have that tight of tolerances but some of the check fixtures they want within .007". On larger things or things with Ive had to print a few times before they'll buy them off.



Yeah ball screws are probably not worth it. I know even the $100k stratasys still use belts. Pretty great idea to use wider belts tho, didn't even think of that. I've dropped the accel and jerk on my current printers and that has helped out a ton.


---
*Imported from [Google+](https://plus.google.com/113760767936457744939/posts/5XmPcrnSfUN) &mdash; content and formatting may not be reliable*
