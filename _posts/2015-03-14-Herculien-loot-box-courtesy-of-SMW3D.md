---
layout: post
title: "Herculien loot box courtesy of SMW3D ."
date: March 14, 2015 20:26
category: "Discussion"
author: Dat Chu
---
Herculien loot box courtesy of **+SMW3D**​. 😁 One step closer. 

![images/3db55d4184b27a70faf9e57d3312d264.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3db55d4184b27a70faf9e57d3312d264.jpeg)



**Dat Chu**

---
---
**Sébastien Plante** *March 15, 2015 16:40*

Hope it's one full step! 1/32 step closer would take a while! 


---
**Gus Montoya** *March 17, 2015 06:28*

**+Dat Chu** Now that you have the entire frame and plastic done. What else is missing for you too continue?


---
**Seth Messer** *March 19, 2015 04:15*

**+Dat Chu** did **+SMW3D** custom cut the extrusion for you, to your specs?


---
**Dat Chu** *March 19, 2015 12:51*

No. I had a friend helped me cut it. ☺ But message smw3d. They might be able to do it for you for some cost. 


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/b8C6WEubwCK) &mdash; content and formatting may not be reliable*
