---
layout: post
title: "Azteeg X5 V3 connections- I've got a couple questions in hooking it all up"
date: September 13, 2016 00:04
category: "Discussion"
author: jerryflyguy
---
Azteeg X5 V3 connections-



I've got a couple questions in hooking it all up. Are the fans pins controlled by the board for the part cooling and hot end? Which fan hoes to which pin? 



The external fans that are to blow onto the X5 and the Octopi, are they just direct wired to the PS at 100% (always on?)



I'm concerned about the plug polarity on the Vicki. Any way to know I'm hooking it up correctly? I've not found any clear confirmation to which pin set it goes to on the main board, although IIRC there is only one place where it fits.









**jerryflyguy**

---
---
**Sean B** *September 13, 2016 02:42*

I haven't hooked my fans yet, but did you looked at the wiring diagram on Panucatt's site?


---
**jerryflyguy** *September 13, 2016 02:47*

I did but it just calls out the two fans, doesn't say which is which (or if it matters).  Looking at it again here it's possible that it's intended for more than 2 fans but I'm not sure. There are six pins in the cluster, maybe it can run more than 2? Nooo idea 😃


---
**Eric Lien** *September 13, 2016 21:56*

**+jerryflyguy**​ you can run 2 fans. The fans can be run on power supply voltage (12 or 24 depending on what you chose) or alternatively on 5V (hence the 6 pins). In the config you tell smoothieware what pin to use for part cooling, and what to use the other one for (p1.25 or p0.26). I use one for my part cooling, and one for my hotend heatsink and have it turn on only after the hotend reaches 50C. For controller cooling I have that wired directly to the PSU.



![images/a72069ee4b3109ebc2faca9c69db476a.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a72069ee4b3109ebc2faca9c69db476a.png)


---
**jerryflyguy** *September 13, 2016 23:27*

**+Eric Lien** thanks, missed the ability to configure it. Makes sense.



Any guidance on knowing the how to orientate the plug on the cable going to the Viki? I'm assuming it gets connected to the 'Ext2' pin group but have no idea which way to orientate the plug itself? Just wanting to be doubly careful not to let any magic smoke out 😉


---
**Eric Lien** *September 14, 2016 00:23*

**+jerryflyguy**​ if you look at the labels on the viki2 and the pin labels on the wiring diagram you should be able to line up one of the pins and the rest will follow: 

﻿

![images/66ec47c2ee6cc32f93621c8d893135cd.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/66ec47c2ee6cc32f93621c8d893135cd.png)


---
**Eric Lien** *September 14, 2016 00:24*

![images/fab33945fe5d46dc7b4360a6c675c3e4.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fab33945fe5d46dc7b4360a6c675c3e4.png)


---
**Eric Lien** *September 14, 2016 00:25*

[http://panucattdevices.freshdesk.com/helpdesk/attachments/1043587235](http://panucattdevices.freshdesk.com/helpdesk/attachments/1043587235)



[http://files.panucatt.com/datasheets/x5mini_wiring_v3.pdf](http://files.panucatt.com/datasheets/x5mini_wiring_v3.pdf)


---
**jerryflyguy** *September 14, 2016 00:55*

**+Eric Lien** thanks. My confusion came from the picture in this first link on the second page. To me the ribbon cables should both be going to the right(cable extends away from the plug on the right side of both plugs), but then the red wire in the ribbon would seem to go to the wrong end... Was just confusing. Wish there was a orientation or registration clip like in the end stop plugs.



Thanks again


---
**Eric Lien** *September 14, 2016 01:24*

**+jerryflyguy** I know what you mean. And I am happy to help. Just help the next guy down the line once you are the one who knows :)


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/5Hn4125yY4F) &mdash; content and formatting may not be reliable*
