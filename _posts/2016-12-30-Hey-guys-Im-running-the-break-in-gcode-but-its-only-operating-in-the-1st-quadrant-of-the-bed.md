---
layout: post
title: "Hey guys! I'm running the break in gcode but its only operating in the 1st quadrant of the bed"
date: December 30, 2016 05:17
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hey guys! I'm running the break in gcode but its only operating in the 1st quadrant of the bed. Any ideas? I haven't made a S3D profile yet, but if someone would send me one as a starting point it would be greatly appreciated :) 



UPDATE: Ran it again.... Now it is (and I) throughly confused.... It only jerks around in the center... Could it have somthing to do with the ACCEL? I set it down to 200. (It only this this once, it always does the 1/4 thing...)



aaaaaand the video is sideways.



Welp I'm going off to bed.



 turn your head :P





**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *December 30, 2016 09:36*

You likely are running higher microstepping drivers. So you will need to adjust the steps/mm in the firmware config to add more steps (more distance). That's why you are essentially seeing a scaled down version of the break in gcode.



Did you go with the sd6128 drivers. If so those are 1/128 microstepping versus the 1/32 microstepping on the standard configuration.


---
**Stefano Pagani (Stef_FPV)** *December 30, 2016 14:55*

**+Eric Lien** No I have the 1/32 ones. Wish I got a X5 GT and those drivers though :( I just started using my X3, so I could have waited...


---
**Stefano Pagani (Stef_FPV)** *December 30, 2016 14:56*

I did all the configuration in marlin myself so theres probably (defiantly) a problem there.


---
**Eric Lien** *December 31, 2016 21:25*

**+Stefano Pagani** did you get things figured out?


---
**Stefano Pagani (Stef_FPV)** *January 03, 2017 02:09*

**+Eric Lien** Yup! I looked at the smoothie settings and set the steps per mm right. Got the Z axis in and I am running the break in code now :) Thanks for the help! P.S. do you think you could send me your S3D profile for the Eustathios? It would be helpful as a starting platform.


---
**Eric Lien** *January 03, 2017 03:49*

**+Stefano Pagani** what material would you like it for?


---
**Stefano Pagani (Stef_FPV)** *January 03, 2017 14:42*

**+Eric Lien** PLA, PETG, ABS, (ninja flex if you have it). I know ninja flex will be like pushing a rope uphill, but I want to try anyways :) Thanks so much!


---
**Stefano Pagani (Stef_FPV)** *January 05, 2017 13:21*

**+Eric Lien** :)


---
**Eric Lien** *January 05, 2017 16:25*

Sorry, I forgot. I will do when I get home.


---
**Stefano Pagani (Stef_FPV)** *January 05, 2017 19:17*

No problem :) No rush

Do it when you are free


---
**Eric Lien** *January 13, 2017 05:29*

**+Stefano Pagani** Sorry for the long delay. I just kept forgetting. Give these a whirl. Please note I tried to setup some basic ones. But I almost always tune every print specific for the printed parts feature geometry. But this should be some good medium quality settings. Not too slow, not too fast:



[drive.google.com - 001_Eustathios_Spider_V2_Master_Profile.factory - Google Drive](https://drive.google.com/file/d/0B1rU7sHY9d8qVlo2NEx4SUdtaFk/view?usp=sharing)


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/iWJ61SFk9Be) &mdash; content and formatting may not be reliable*
