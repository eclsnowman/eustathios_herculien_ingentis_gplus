---
layout: post
title: "Fixed the noise issues on Procerus and pushed the acceleration up to 22000 mm/s/s"
date: January 16, 2015 11:35
category: "Deviations from Norm"
author: Tim Rastall
---
Fixed the noise issues on Procerus and pushed the acceleration up to 22000 mm/s/s. Speed is set to 833mm/s.


**Video content missing for image https://lh4.googleusercontent.com/-lYBUyLbrMAc/VLjzfCVnq4I/AAAAAAAAVdM/GKQbvHyEMNw/s0/20150116_213421.mp4.gif**
![images/3f49473f58dab29217ff06b0826fb61f.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3f49473f58dab29217ff06b0826fb61f.gif)



**Tim Rastall**

---
---
**Miguel Sánchez** *January 16, 2015 12:53*

wow!


---
**Eric Lien** *January 16, 2015 12:57*

Wow. I need a smoothieboard. Great job.


---
**Brandon Satterfield** *January 16, 2015 13:36*

Excited to see this one print. 


---
**Mike Miller** *January 16, 2015 15:21*

So what do we think ultimate attainable print speeds are? I'd imaging overhangs would SUCK if filament was being flung too quickly...it's obvious we can move a whole lot faster than we can print. 


---
**Jim Wilson** *January 16, 2015 15:22*

[http://i.imgur.com/rIbFvQX.png](http://i.imgur.com/rIbFvQX.png)


---
**Paul Sieradzki** *January 16, 2015 16:41*

Is this 20x20 aluminum extrusion or 30x30? 


---
**Tim Rastall** *January 16, 2015 19:49*

**+Oliver Schönrock** 1. Yes it's the bearings. Just the sound they make at high speeds. 

2. Yep,  it's slowing down very quickly. 

3. Yeah,  slow moves seem to be more noisy,  haven't delved into that yet.

Grub screws seem fine,  there are 2 on these robot digg pulleys. 24 v series with on board drv8825s on the x5 mini. 




---
**Tim Rastall** *January 16, 2015 19:50*

**+Paul Sieradzki** 20x20 from Misumi. 


---
**Tim Rastall** *January 16, 2015 19:51*

**+Mike Miller** God knows but hopefully faster and more stable than ingentis style. May have to buy a e3d volcano. 


---
**James Rivera** *January 17, 2015 18:15*

I didn't get a chance to watch this until now. It's a ROCKET SLED!


---
**Tim Rastall** *January 21, 2015 20:25*

**+Oliver Schönrock** I'm still playing with speed settings on Procerus but there doesn't appear to be any resonance for the large moves I've been doing so far, Tonight I'll feed some actual gcode with dense infill into it so see how it behaves then. It's clear the dual steppers are performing better than a single stepper, otherwise 20000mm/s/s acceleration would cause all kinds of lost steps and general havoc. I still suspect that I'll dial it back to at most 9000mm/s/s for the purposes of avoiding too much deflection and vibration in the hot end and carriage. **+Brook Drumm** also tells me he's used twinned steppers for some of his projects and I'm sure if there were problems with that set-up he's have found them by now.


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/17XY8k2FH4d) &mdash; content and formatting may not be reliable*
