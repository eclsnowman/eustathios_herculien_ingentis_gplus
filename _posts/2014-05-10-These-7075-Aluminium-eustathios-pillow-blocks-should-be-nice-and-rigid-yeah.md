---
layout: post
title: "These 7075 Aluminium eustathios pillow blocks should be nice and rigid, yeah?"
date: May 10, 2014 08:21
category: "Show and Tell"
author: Jarred Baines
---
These 7075 Aluminium eustathios pillow blocks should be nice and rigid, yeah? :-)

Sandblasted the non-functional faces and they look great!



![images/d910afa294c0c11e9922f431aea0a900.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d910afa294c0c11e9922f431aea0a900.jpeg)
![images/0e1397efabaacdc5dba03fcb628a0822.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0e1397efabaacdc5dba03fcb628a0822.jpeg)

**Jarred Baines**

---
---
**Daniel Fielding** *May 10, 2014 08:35*

Nice looks good


---
**Riley Porter (ril3y)** *May 10, 2014 11:41*

Holy crap where did u get those


---
**Eric Lien** *May 10, 2014 12:53*

I notice you put the bearing in from the other side. Was that for was ease of machining?﻿  BTW looks great.


---
**Eric Lien** *May 10, 2014 13:00*

If you make other parts in aluminum I would be interested in purchasing a set, or if you wanted to share your machining friendly model files I know a guy who would help me out.﻿


---
**Jason Smith (Birds Of Paradise FPV)** *May 10, 2014 14:10*

Wow that's cool.


---
**Jarred Baines** *May 10, 2014 14:55*

Made at my dad's CNC workshop **+Riley Porter**. The idea of putting the bearing on the outside was actually to support the rod at the extremities (thinking this would help align them that minuscule-amount more...) But thinking about it now I've also got a slightly longer length of rod between the bearings now, so the rod will be able to bend that minuscule-amount more also >_< lol



I'm sure I won't notice that though, it's like 4-5mm difference.



I think I did sketch these up in inventor format, although on those CNC machines it's much easier to work with 2D DXF files and use the CAM package (surfCAM) to manage heights. I'll see what models I have for them tomorrow and I'll report back, anything I do have is up for grabs for sure! :-)



 #WarmFuzzyGivingBackFeeling  



I'm going to go through your solidworks models and pick out good pieces to CNC machine **+Eric Lien** so once I have a list of parts I'm proposing to make I'll let you know and since I'll be setting the machine and CAM stuff up for myself anyways I wouldn't mind knocking a few extras up for cheap if you're interested... All more complex components will be printed though.


---
**Eric Lien** *May 10, 2014 15:18*

**+Jarred Baines** count me in on a set. I love my printer... But CNC tolerances sure are nice.﻿



Also let me know if you want any of the fillets rolled back on the models. I always put them in last on the design tree... But being a foundry guy I am a sucker for fillets.


---
**Jarred Baines** *May 11, 2014 14:04*

Fillets 'n stuff are not a problem ;-)



Ill keep you posted mate


---
**ThantiK** *May 19, 2014 00:54*

rofl; I <i>JUST</i> designed this exact thing in OpenSCAD.  Today.  2 hours ago...  I am so dumb.  I could have just checked here but NOOOOOO!


---
**Jarred Baines** *May 20, 2014 13:22*

Ha ha **+Anthony Morris** :-p



Theres so many ingentis variant designs now! No point re-inventing the re-invented wheel ay!


---
**ThantiK** *May 20, 2014 17:02*

Let me know when you do the machined versions, I might be in for <i>quite</i> an order of them.  At least one order personally, but if it takes a while I might be in for a large set...considering starting up my own company.


---
**Eric Lien** *May 20, 2014 18:56*

**+Anthony Morris**  I might want to see the bearing bore moved back to the other side of the block.  Then the bearing is held between the pulley and the mount. Nothing holds the bearing in along the length but friction as shown here.


---
**ThantiK** *May 20, 2014 21:26*

**+Eric Lien** on mine I'm using the little ingentis knobs, so that's what holds the bearing for me. 


---
**Eric Lien** *May 20, 2014 22:21*

**+Anthony Morris** makes sense.


---
**Jarred Baines** *May 23, 2014 14:01*

Mines going to have a (possibly) polycarbonate case which will do the same thing... You won't get the bearings out without a press tho - nice press-fit ;-)



But I think ideally they should be placed on the inside, less length between the bearings = less flex in the shaft... Was a bit of an oversight. ..



Perhaps we all should start up a business? Great minds and skillset here! On more than one occasion I've considered speaking to **+Tim Rastall** about a business proposition...



I'll let you all know if I get onto this, life is a bit hectic at the moment! 


---
**ThantiK** *May 23, 2014 14:06*

**+Jarred Baines**, often thought about starting my own company making machines like this.  But there are so many at this point in time, and I've learned a lot being employed at DeltaMaker about how hard it really is to get production started.  Though it doesn't help that the guy who started the company doesn't actually help with production at all...


---
**Daniel Fielding** *June 01, 2014 04:56*

Hi Jarred, could I grab a set of whatever you make please, I really like the idea of machined parts.


---
**Jarred Baines** *June 05, 2014 00:12*

**+Daniel fielding** **+Anthony Morris** **+Eric Lien** 



If I get the chance (looking good) I will make up a set of x/y blocks and the carriage, since I'm doing it for myself the cost will simply be the material + "mates rates" for machining (ie no up-front setup costs that would normally be included).



Wondering what extruder carriage design you all think would be best suited? The Eustathios ones are mega-complicated - which doesn't matter when you're printing but will take too long to be 'worth it' to machine in my CNC...



I like the V4 Ingentis carriage or D-Rob's carriage, I am leaning toward D-Rob's only because I like how it clamps the hot-ends around the groove and then bolts to the carriage - seems 'tough'...



Any thoughts?



Belt clamping I will have to come up with something too as machining teeth in a groove like the eustathios design will be near impossible.



Anyone else want in on this? I can only offer 'reasonable costs' as far as pricing at this stage and I will only charge what's necessary (not out to make profit from this, just giving back the way I can)


---
**ThantiK** *June 05, 2014 00:55*

I'm using the Kraken myself, so none of the normal carriages really apply to me.  I saw a really nice one the other day but no files have been posted yet.


---
**Daniel Fielding** *June 05, 2014 01:15*

Any carriage that will accept a e3d hotend I will probably go with 2 v6. 


---
**Eric Lien** *June 05, 2014 02:43*

**+Daniel fielding** I am with you on the qty:2 v6 e3d. And I would be in for machined parts. I would just need an idea on cost before signing up.


---
**ThantiK** *June 05, 2014 03:01*

That's what I wanna know as well; cost.  Talking about $100 total here?  Less?  More?


---
**Jarred Baines** *June 05, 2014 04:27*

No commitment necessary until I get a firmer idea of what is going to happen... Just wanted an idea of how many I will be making so I can see what sort of jigs / fixtures I will need to make to make things easier.



Will be making mine to suit e3d V5, I'm assuming that they wouldn't change the mount on us for the V6?



Kraken carriage just sounds like an easier version of the e3d mount carriage **+Anthony Morris** , I think we could squeeze one in for you ;-) I'll contact you when the time comes.


---
**Daniel Fielding** *June 05, 2014 05:10*

I think overall length of v6 is shorter


---
**Jarred Baines** *June 05, 2014 10:40*

It definitely is shorter, one of the benefits - but I'll check to confirm that the mount is the same (j-head I believe?) before I go making any ;-)﻿


---
*Imported from [Google+](https://plus.google.com/+JarredBaines/posts/c2r4Fr3Z8E4) &mdash; content and formatting may not be reliable*
