---
layout: post
title: "I have Walter's mini space invader carriage with two 24V blower fans and I just realized the pins on the Azteeg X5 mine v3 only has two fan outputs"
date: October 12, 2016 22:13
category: "Build Logs"
author: Sean B
---
I have Walter's mini space invader carriage with two 24V blower fans and I just realized the pins on the Azteeg X5 mine v3 only has two fan outputs.



I have one input going to the hotend and I wired the twoblower 24V fans in parallel on the other output.  The azteeg has a note that high amperage fans shouldn't be used and I think mine apply since they are 0.16A.



I tried running them and they are extremely low speed, my voltmeter screen is damaged but it appears to be quite a low voltage.  



Any suggestions?  Are there alternatives to running the blower fans?



([http://www.robotdigg.com/product/516/24V-5015-Turbine-Fan](http://www.robotdigg.com/product/516/24V-5015-Turbine-Fan)) 





**Sean B**

---
---
**Eric Lien** *October 12, 2016 22:27*

Fan MOSFETs say 250ma

![images/d771b37a75b5a9766a72e238f469515d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d771b37a75b5a9766a72e238f469515d.png)


---
**Eric Lien** *October 12, 2016 22:40*

You could always try and find some spare gpio pins on the x5 mini (maybe ask Roy) and map those output pins as the part cooling fan to drive something like this:  [amazon.com - Amazon.com: Estone IRF520 MOS FET Driver Module for Arduino New: Industrial & Scientific](https://www.amazon.com/dp/B00KNK8IRO/ref=cm_sw_r_cp_apa_KTR.xbPFJTXM4)


---
**Sean B** *October 12, 2016 23:01*

Thanks Eric, can't believe I miss that on their website...  The wiring diagram is nebulous.  I'll look into using some spare gpio pins with a mostfet. 

 


---
**Eric Lien** *October 12, 2016 23:04*

**+Sean B** it wasn't directly on the shop page, it was in the supporting links lower on the page: [panucattdevices.freshdesk.com - X5 mini V3 Install guide : Panucatt Devices](http://panucattdevices.freshdesk.com/support/solutions/articles/1000233346-x5-mini-v3-install-guide)


---
**jerryflyguy** *October 12, 2016 23:44*

**+Sean B** according to Roy there are pins in the EXP1 header that can be used for I/O. I've not figured out which ones yet as the wiring diagram doesn't note them as either 'I' or 'O' but there is supposed to be a couple usable pins there


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/KpLVg1w2KQR) &mdash; content and formatting may not be reliable*
