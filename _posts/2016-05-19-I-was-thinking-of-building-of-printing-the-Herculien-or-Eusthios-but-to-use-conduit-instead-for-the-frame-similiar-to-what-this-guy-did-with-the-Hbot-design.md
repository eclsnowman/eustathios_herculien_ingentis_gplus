---
layout: post
title: "I was thinking of building of printing the Herculien or Eusthios but to use conduit instead for the frame, similiar to what this guy did with the Hbot design"
date: May 19, 2016 14:26
category: "Mods and User Customizations"
author: Dovid Teitelbaum
---
I was thinking of building of printing the Herculien or Eusthios but to use conduit instead for the frame, similiar to what this guy did with the Hbot design. Any idea or suggestions welcome?





**Dovid Teitelbaum**

---
---
**Daniel F** *May 19, 2016 14:43*

2020 extrusions are so handy to fix things, it's just much more convenient to put a nut in the slot and screw parts on it. I built my 1st printer with square tubes and I had to drill extra holes all the time. I would go for the well thought out "original" frame design. There is still much room for modifications...


---
**Dovid Teitelbaum** *May 19, 2016 14:50*

Whats the total cost of 2020? conduit would cost me under $20 total.


---
**Daniel F** *May 19, 2016 14:58*

hard to say what it costs in the us, I'm based in Europe  and it cost me around 150$ cut to leght incl. shipping. It really depends what your goal is, all the other (quality) parts have it's price as well so I think the overall mix/quality of parts should be from the same category. If costs is an issue, why not use cheaper parts for the frame, as long as it is precise and rigid, it should work.


---
**Eric Lien** *May 19, 2016 16:56*

**+Makeralot** has a pretty good deal setup for us. The Eustathios Frame It is just slightly different in that you will need to 6mm bolts and tee nuts. But should work very good. The HercuLien frame they sell looks to use the standard 4.2mm center hole, so the 5mm tap in the center should work like normal.



[http://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207/](http://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207/)



[http://www.makeralot.com/aluminum-extrusion-kit-for-herculien-2020-2080-v-slot-p215/](http://www.makeralot.com/aluminum-extrusion-kit-for-herculien-2020-2080-v-slot-p215/)




---
**Ted Huntington** *May 19, 2016 17:05*

cool idea, thanks for sharing


---
**Dovid Teitelbaum** *May 19, 2016 17:58*

Seems like a nice percentage off the total price for me to give it a shot. Can I ask what parts are connected to the actual frame so I can see what type of modifications or maybe fond something already made. 


---
**Peter Kikta** *May 19, 2016 20:11*

I payed 60€ inc cutting and shipping (Eustathios), and I think it is definitely worth the extra money


---
**Eric Lien** *May 19, 2016 21:01*

**+Dovid Teitelbaum** Almost every part of both printers attaches to the frame except the central carriage. It would be an ambitious project to make a cross rod gantry printer with conduit frame. I think you would be the first. But it would be fun to see. Keep us updated if you follow that path. I would enjoy watching the design phase.


---
*Imported from [Google+](https://plus.google.com/+DovidTeitelbaum/posts/M6WAVNLDw6V) &mdash; content and formatting may not be reliable*
