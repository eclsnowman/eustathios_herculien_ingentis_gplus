---
layout: post
title: "A bunch more photos, sadly no progress"
date: April 23, 2015 23:58
category: "Discussion"
author: Ben Delarre
---
A bunch more photos, sadly no progress.



The smooth rods now fit into the clamps perfectly, no problem there at all.



However....if i place the bottom z mounts in the correct measured positions then I can barely turn the leadscrews at all. I have to loosen one side which pops back a little further and then the screws move fine.



However, I still get binding when trying to drive from the motor so I'm guessing its still too tight.



At this point I'm at a loss, spent the whole day on this today and no progress, pulled it apart and back together again more times than I can count.



Anyone who has built the V2 z-axis got any tips?



Otherwise I think it might be time to give up and see if new plastic parts would help. Anyone up for printing me a set of Z axis parts for the Eustathios V2, including the motor mount in ABS?



I'm happy to pay whatever is necessary. I'm in Los Angeles, so someone in California would be best as I'd rather not wait for postage.



![images/eb7f407d27e1d066f4d2ce0626b0c56d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eb7f407d27e1d066f4d2ce0626b0c56d.jpeg)
![images/0fba3ea05a3653e2add2e3cfcfad1b0f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0fba3ea05a3653e2add2e3cfcfad1b0f.jpeg)
![images/a2918c88902ebe683c9c3df29375118c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a2918c88902ebe683c9c3df29375118c.jpeg)
![images/1dceb46323eb247cece3bc90d374d6cb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1dceb46323eb247cece3bc90d374d6cb.jpeg)
![images/4b2c031163fac19a72d9ed52a7397e20.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4b2c031163fac19a72d9ed52a7397e20.jpeg)
![images/370d732d5d9d0e13a6797c2a2cc20399.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/370d732d5d9d0e13a6797c2a2cc20399.jpeg)
![images/96a6734d863dd3ba18bfe8e4b45d81fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/96a6734d863dd3ba18bfe8e4b45d81fa.jpeg)

**Ben Delarre**

---
---
**Isaac Arciaga** *April 24, 2015 01:59*

**+Ben Delarre** I'm in Cerritos. I can help ya out.


---
**Jason Smith (Birds Of Paradise FPV)** *April 24, 2015 02:22*

Stupid question: Does it move freely if you remove the bed? If needed, you could add spacers (washers) to space the bed away from the supports. 


---
**Ben Delarre** *April 24, 2015 03:33*

**+Jason Smith** yes it does. its almost definitely bad tolerances on my printed parts. I managed to improve it substantially by sanding down one of the top clamps and it now runs ok-ish. There's some rubbing noises and an odd periodic donking sound that I think is the leadscrews rubbing on the bearings. The leadscrews also show a pronounced wobble at the top which I guess means they are not properly lined up at all. I don't think I can sand them all down enough to get that right so its new part time!



**+Isaac Arciaga** I'm in Redondo Beach, so not far at all. When do you think you would be able to print them all off? Happy to pay you for your time, effort, filament and wear and tear, I just want my printer working! :-)


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/Gd3dwUy2UHt) &mdash; content and formatting may not be reliable*
