---
layout: post
title: "Hey Guys, Just wanted to share a couple of my recent prints"
date: December 05, 2016 21:32
category: "Show and Tell"
author: Sean B
---
Hey Guys,



Just wanted to share a couple of my recent prints.  It has taken a great deal of dialing in, especially with this being my first experimentation with bowden and PETG (used to use ABS exclusively).  I was printing some parts for a friends D-bot and noticed I had considerable ghosting and ripples throughout the part.  Based on Zane's post about acceleration I dialed my acceleration down, I initially set it up for 2000mm/s^2 but need to double check that this is still accurate.  If my acceleration is low, any other culprits?



![images/058796503406a4014401151c225001c6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/058796503406a4014401151c225001c6.jpeg)
![images/99c88cf07f2dfd6ffb2d610ad7f89bbb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/99c88cf07f2dfd6ffb2d610ad7f89bbb.jpeg)
![images/59b1c835385d37d8e2dcd30744547f43.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/59b1c835385d37d8e2dcd30744547f43.jpeg)
![images/fab166f3f3c971d4832c36aa9842111b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fab166f3f3c971d4832c36aa9842111b.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-th-Jt8Syrv8/WEXc8P4zNdI/AAAAAAAAaJI/I4HM1xhHGRkk4mJWlyEJ_ldpBu4BySMyQCJoC/s0/VID_20161204_231033.mp4**
![images/5eafd854d01f70394629d478cd71e214.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5eafd854d01f70394629d478cd71e214.jpeg)
![images/0b18d3d973ef7714b41d023542f7f7eb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0b18d3d973ef7714b41d023542f7f7eb.jpeg)
![images/2d10a3fddc1d1586e097a833e5a3f0fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2d10a3fddc1d1586e097a833e5a3f0fa.jpeg)

**Sean B**

---
---
**Zane Baird** *December 05, 2016 21:54*

Just out of curiosity, what controller are you using? If it's smoothie-based I'd update to the newest firmware (or at least one where accel is calculated each step). I eliminated a lot of ghosting by updating my firmware when this update was released. Additionally, make sure your alignment is spot-on. This will accentuate the rippling effect.


---
**Eric Lien** *December 05, 2016 21:57*

With that amount of ripple, perhaps the hot end is loose in the carriage? Also I might say loose belts on the long belts along the side?



Lastly I think with the continuous ripple along the side, you have a belt rubbing on the side of a pulley flange somewhere. When the teeth of a GT2 belt rub on the pulley flange it tends to give that signature continuous ripple. Check the closed loop belts at the motor. If that belt is too tight, or the mount not strong enough it will deflect the mount, causing the motor pulley axis to go at a slight angle which tends send the belt to one side of the pulley and causing rub.


---
**Eric Lien** *December 05, 2016 23:31*

As Zane said, disconnect the motor closed loop belts and confirm you don't have any chatter/binding moving the carriage by hand. 


---
**Sean B** *December 06, 2016 03:25*

Eric, I think you might be right about the belt on a pulley.  I slowed my speeds down also and I don't see anything like before.  I still seem to have not quite circular holes though.

![images/d8880d1bb9fdcc4d2181d6466a876aa6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d8880d1bb9fdcc4d2181d6466a876aa6.jpeg)


---
**Sean B** *December 06, 2016 03:25*

![images/a69c27159532e07b846e70af32d5f1b9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a69c27159532e07b846e70af32d5f1b9.jpeg)


---
**Zane Baird** *December 06, 2016 04:23*

Looks like backlash go me at this point. Especially with the shape on those holes.... You sure you're screws are all tightened and you aren't getting a bit of flex in one of your joints?


---
**Sean B** *December 06, 2016 12:23*

**+Zane Baird** I'll go through tonight and double check all the pulleys.  Thanks


---
**Eric Lien** *December 06, 2016 13:25*

**+Sean B** just double-checking (because during troubleshooting make no assumptions), you have those small shims between the bearing and the pulleys on the side shafts correct? In the past several people have forgotten those. Otherwise the pulleys can rub on the bearings face. 


---
**Sean B** *December 06, 2016 13:27*

**+Eric Lien**​ Yes, I printed out plastic ones


---
**jerryflyguy** *December 06, 2016 15:58*

**+Sean B** check for cracked bearing holders. I found one which was printed 'upright' instead of on its 'back'. It had split through the center of the bearing between layers. It was just enough to give a tiny bit of play and make printed holes out of round.


---
**Sean B** *December 08, 2016 23:41*

So I went through all the screws and they were all tight.  I did adjust some of the pulleys to reduce and chatter or rubbing, I also slowed down my speeds and am relatively satisfied with the finish.  I am more concerned about the non-cricular holes...I closely inspected the bearing mounts and did find one tiny crack; however it isn't through the whole piece and doesn't appear to have any flex so I highly doubt this is the source of my uneven holes.  I snugged the belts in case there was any play, didn't seem to be overly loose but we'll see.  I am printing something right now, but at first glance the hole shape looks exactly the same.  



I am at a loss, I have no idea what is going on... I do have a coat hanger inserted into my carriage to act as a wire support could this be an issue if it is putting a small force in one direction?  Maybe if the amperage on the motors isn't high enough?  I feel like I am grasping at straws at this point... 


---
**Eric Lien** *December 08, 2016 23:51*

**+Sean B** motion seemed smooth with the drive motors disengaged?


---
**Sean B** *December 09, 2016 01:23*

yeah I thought so, but I'll disconnect the motors this weekend and double check.  I think one direction is a little tougher.  


---
**Eric Lien** *December 09, 2016 02:35*

One trick. Is the printer sitting stable on all 4 feet? Try shimming (one at a time) under each of the four feet. See if a shim under one foot makes the motion smoother. If so you may have a slight misalignment tension in the frame. 


---
**Sean B** *December 09, 2016 02:41*

It is on there basement floor... It doesn't rock, but then again the floor isn't completely flat.  Good point, I'll try and get it on a flatter surface and play with some shims.


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/a7iYDktRepn) &mdash; content and formatting may not be reliable*
