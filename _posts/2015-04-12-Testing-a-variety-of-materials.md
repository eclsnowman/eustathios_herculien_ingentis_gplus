---
layout: post
title: "Testing a variety of materials"
date: April 12, 2015 04:35
category: "Show and Tell"
author: Derek Schuetz
---
Testing a variety of materials

![images/f0406ee3f18f2861c9ad5b347b9f2b74.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f0406ee3f18f2861c9ad5b347b9f2b74.jpeg)



**Derek Schuetz**

---
---
**Eric Lien** *April 12, 2015 05:00*

If you can get that flexible filament to run in HercuLien you are a better man than me. I took a few stabs at it... But no luck. It is stiffer than ninjaflex and has some cool characteristics (cut off a section and pull it). It is bendable but not stretchable, it has memory.


---
**Derek Schuetz** *April 12, 2015 05:19*

I figured that would be the hardest task but I figured for that price why not 


---
**Владислав Зимин** *April 12, 2015 09:55*

счастливый :)


---
**Mike Thornbury** *April 12, 2015 13:03*

Price? What price? They are giving it away :) 



I pay that for plain ABS or PLA, then have to pay freight, which is usually around the same again.



I budget on US$50/kg for normal stuff. Ninja flex and such $100+



I'm so over 'cheap' Asia and their 'low' prices :(


---
**Joe Spanier** *April 12, 2015 16:17*

I wish they did those prices on their website. I have some of the petg a friend grabbed for me an I love it. 


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/QuLtK1yVxEi) &mdash; content and formatting may not be reliable*
