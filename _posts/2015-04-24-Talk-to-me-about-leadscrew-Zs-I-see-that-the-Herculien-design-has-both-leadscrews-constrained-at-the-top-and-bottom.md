---
layout: post
title: "Talk to me about leadscrew Z's I see that the Herculien design has both leadscrews constrained at the top and bottom"
date: April 24, 2015 11:54
category: "Discussion"
author: Mike Miller
---
Talk to me about leadscrew Z's



I see that the Herculien design has both leadscrews constrained at the top and bottom. Is the Leadscrew nut floating? Is there any Z-banding as a result of constraining both the top and bottom of the screw?



I'm direct mounting the motors to screws, as that's what I can machine, and granted, nothing's mounted yet (still sussing everything out before taking my only printer offline)...but just bolting down the shaft of the motor and the leadscrew shows a significant departure from true. ;)



Naturally, I can suspend everything loosely in place, then get it all true and perpendicular, but wanted to tap the hive mind first. What I DON'T want to end up with, when this is all said and done, is vertical banding issues. ;)





**Mike Miller**

---
---
**Mutley3D** *April 24, 2015 16:02*

I use a constrained lead screw on my other machines, and will do when i build my herculien, but key for me is a lead nut holder that acts as a Z axis isolator. Straight screws are also important of course.


---
**James Rivera** *April 24, 2015 17:20*

I think it depends a lot on how straight your screws are and how parallel they are to the straight shafts. I can tell you that when I added the Super-Z upgrade (2' Z) to my 1st Printrbot+ the screws were almost shaped like bananas. The design called for captive tops but I took them off because it was clearly flexing the 12mm smooth rods judging by how easily perceptible it was in the prints. Basically, the smooth rods are supposed to be doing the job of keeping it in a straight line, not the lead screws.


---
**Mike Miller** *April 24, 2015 19:23*

My plan at this point is to have the nuts push the bed upwards, with the weight of the bed allowing it to fall downwards, then the leadscrews can pretty much do whatever they want to. The nuts are free floating (mostly because I ordered the screws and nuts a size too small. d'oh.)


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/ANG9XVts6vA) &mdash; content and formatting may not be reliable*
