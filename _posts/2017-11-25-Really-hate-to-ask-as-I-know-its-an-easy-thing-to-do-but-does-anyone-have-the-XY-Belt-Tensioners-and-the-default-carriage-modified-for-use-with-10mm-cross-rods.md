---
layout: post
title: "Really hate to ask as I know it's an easy thing to do, but does anyone have the XY Belt Tensioners and the default carriage modified for use with 10mm cross rods?"
date: November 25, 2017 15:45
category: "Mods and User Customizations"
author: Ryan Fiske
---
Really hate to ask as I know it's an easy thing to do, but does anyone have the XY Belt Tensioners and the default carriage modified for use with 10mm cross rods?





**Ryan Fiske**

---
---
**jerryflyguy** *November 25, 2017 15:55*

The belt tensioners are built in. The bushing holes in the carriage will fit 8mm or 10mm bushings 


---
**Ryan Fiske** *November 25, 2017 16:06*

Woops sorry you're correct I forgot that the OD on the bronze bushings is the same!



However for the cross rod holes in the XY belt tensioner pieces I think those are a snug fit for 8mm rods right?**+jerryflyguy** 


---
**Eric Lien** *November 25, 2017 16:53*

**+Walter Hsiao**​ has an 8mm and 10mm version. Plus his is a more elegant/clean design. I don't have the same knack for design as several of the guys that built the machine after me. I have a very utilitarian design sense. It takes real talent to break it down to it's simplest components and have smooth arcs between features like others have come up with. 



[https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/walterhsiao/Belt%20Tensioner](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/walterhsiao/Belt%20Tensioner)






---
**Ryan Fiske** *November 26, 2017 02:27*

Thanks Eric, I have printed out Walter's and was just looking to print a set of yours as well but maybe I'll just stick with Walter's. Do you know if it's acceptable to use the self aligning bushings in walter's parts? I haven't gotten them in yet so I can't do a fit check but it looks like it'll work out. I read Walter's page on them that he had made them for graphite bushings so I wasn't exactly sure.


---
**Eric Lien** *November 26, 2017 02:56*

**+Ryan Fiske** i believe he maintained the standard ID, then printed his TPU sleeve to fit the difference. Also if I recall he eventually moved to the self alignment type anyway. A dimension check between his parts and mine could tell you pretty quick if his is sized for the self alignment type.


---
**Nicole Pabst** *November 30, 2017 12:52*

yes you're right ^_^


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/aG6XQcgton6) &mdash; content and formatting may not be reliable*
