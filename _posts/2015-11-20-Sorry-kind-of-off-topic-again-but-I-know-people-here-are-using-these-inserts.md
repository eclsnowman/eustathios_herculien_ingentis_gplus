---
layout: post
title: "Sorry, kind of off topic again but I know people here are using these inserts"
date: November 20, 2015 16:56
category: "Discussion"
author: Chris Brent
---
Sorry, kind of off topic again but I know people here are using these inserts. So I ordered:

[http://www.mcmaster.com/#94180a331/=zw6v4x](http://www.mcmaster.com/#94180a331/=zw6v4x)

Just wanted to confirm that the ID of holes I want to use them in should be 5.1mm (i.e. the A dimension on the diagram)? Or are you realy supposed to taper the hole?





**Chris Brent**

---
---
**Bud Hammerton** *November 20, 2015 17:03*

I use 5.1 or 5.2 the little bit of excess plastic will get pushed out of the way by the heat. I also stick a screw in it before inserting, just in case my hole is a bit undersized. Any excess plastic in the hole will act as a thread lock, sort of like nylon insert lock nuts. Once its all cooled, remove the screw. it will end up being a very clean installation.


---
**Joe Spanier** *November 20, 2015 18:09*

I usually use the upper end number just because the IDs on a print are always a bit undersize. You may want to print a couple tests to see what work best for you. You can always get the insert back by heating it a bit and quickly threading a bolt in and popping it out. 


---
*Imported from [Google+](https://plus.google.com/+ChrisBrent/posts/ZZkA36WyjNC) &mdash; content and formatting may not be reliable*
