---
layout: post
title: "Just a little pic to show where my printer is at"
date: April 23, 2014 12:40
category: "Show and Tell"
author: Brian Bland
---
Just a little pic to show where my printer is at.  Added space at the bottom for electronics and Z axis drive.  I am liking the belt drive a lot better than the Spectra drive.  Still trying to decide on hotends.  May wait on the V6 E3D hotends.

![images/081e16d18fb70f8bba9e6bc3701a3924.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/081e16d18fb70f8bba9e6bc3701a3924.jpeg)



**Brian Bland**

---
---
**ThantiK** *April 23, 2014 12:42*

Where are the files for those X/Y ends (because I'm moving to a belt system)


---
**Brian Bland** *April 23, 2014 12:47*

[https://www.youmagine.com/designs/eustathios](https://www.youmagine.com/designs/eustathios)



They are from the Eustathios.  I modified the X and Y bearing holders so the shaft would go all the way through.﻿



You need the X and Y bearing holders from the Eustathios for the belts to line up with the X/Y ends.


---
**Mike Miller** *April 23, 2014 12:53*

Saaay, that's nice looking orange ABS! :D


---
**Brian Bland** *April 23, 2014 12:59*

Yes it is.  My printers always seem to end up multi-colored.


---
**Riley Porter (ril3y)** *April 23, 2014 22:24*

Sweet **+Jason Smith** did you see this?


---
**Jason Smith (Birds Of Paradise FPV)** *April 23, 2014 22:56*

Yeah, it's looking good! How are the belt tensioners working out? Are you using 10mm misumi pulleys, robotdigg, or a combination of both?


---
**Brian Bland** *April 24, 2014 00:45*

X/Y pulleys are all Robotdigg.  Z pulleys will be combination of both Robotdigg and Misumi.  Did you put flats on the 10mm rods for pulley set screws?  If so how did you align the x/y ends?


---
**ThantiK** *April 24, 2014 00:53*

No need to put flats, really.  Shouldn't be <i>that</i> much force on the pulleys.


---
**Jason Smith (Birds Of Paradise FPV)** *April 24, 2014 01:00*

Correct, I've had no issues despite not having flats on the shafts. 


---
**Brian Bland** *April 24, 2014 01:21*

I was hoping flats were not needed.  Would be a pain aligning the x/y ends.


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/1BH7wUhYvEy) &mdash; content and formatting may not be reliable*
