---
layout: post
title: "Is there some particular reason the to use the Azteeg X5 GT over the Smoothieboard for the HercuLien?"
date: November 03, 2017 18:48
category: "Deviations from Norm"
author: Daniel Sixta
---
Is there some particular reason the to use the Azteeg X5 GT over the Smoothieboard for the HercuLien? Does have more power or is there a reason we really need the modular drivers?





**Daniel Sixta**

---
---
**Eric Lien** *November 08, 2017 12:59*

I just prefer the offerings by Panucatt. Roy has been very supportive for me on these projects and for others. But I know **+Zane Baird**​ uses a Smoothieboard on his HercuLien.


---
**Eric Lien** *November 08, 2017 13:01*

Btw sorry for the late reply, your post got caught in the spam filter. The spam filter is so agressive it basically catches any new persons first post. I try and keep up looking through the filter... But I am working out of town and have limited time right now.


---
**Zane Baird** *November 08, 2017 13:08*

The Smoothieboard has done fine on my Herculien, but modular drivers are nice if you want to upgrade in the future. I have a Smoothieboard that predates V1 and I'm stuck with 16x microstepping and all the MOSFETs came without back EMF protection. The new drivers use 32x microstepping but the large MOSFETs still require you to solder in your own diodes for protection. I've been extremely happy with Panucatt's X5 mini on my delta and its small footprint is great if you only need to run one extruder.



Another minor point, the Smoothieboard will also require you to solder in a separate 5V regulator if you want to power it without a USB connection.


---
**Arthur Wolf** *November 08, 2017 13:52*

I'll mention it because I know it matters to some : buying a Smoothieboard supports the development of both the smoothie firmware, hardware, and documentation.

The azteeg hardware is nice. We get a lot of complaints from users not getting answers to their requests for support though ( seems to be a periodic thing ), and the community has to do it in their place unfortunately.



**+Zane Baird** The diodes protect when wiring in fans or motors, but the big mosfets are for heated beds and hotends, which do not require diodes.


---
**Zane Baird** *November 08, 2017 14:07*

**+Arthur Wolf** I realize that the large FETs are typically used for hotends and heated beds but some users deviate from the pre-set designations for their projects.



I've had 2 Smoothieboards and they've served me well, so I didn't mean to negate that and I've been happy to support Smoothie development. But any product develops over time and addresses issues which a potential buyer might like to know about, even if they are minor.


---
**Arthur Wolf** *November 08, 2017 14:09*

**+Zane Baird** Didn't think you were negating anything, just curious why you need diodes on the big mosfet. We chose not to add the big mosfet diodes because we have thousands of users, and reported use cases for them are extremely rare, but we certainly like to know when users have new needs.


---
**Zane Baird** *November 08, 2017 14:19*

**+Arthur Wolf** I use them for external fans and at one point had one hooked up to a step-down transformer for some lower voltage LED control (ran out of small FETs). In the case of the transformer the diode protection was a definite requirement, but I understand that is outside most people's applications for the larger FETs


---
*Imported from [Google+](https://plus.google.com/108959349382939279759/posts/dGdBJvKREBv) &mdash; content and formatting may not be reliable*
