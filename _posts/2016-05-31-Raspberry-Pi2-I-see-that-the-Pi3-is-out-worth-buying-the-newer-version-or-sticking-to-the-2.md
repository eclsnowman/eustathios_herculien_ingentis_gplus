---
layout: post
title: "Raspberry Pi2- I see that the Pi3 is out, worth buying the newer version or sticking to the '2'?"
date: May 31, 2016 17:51
category: "Discussion"
author: jerryflyguy
---
Raspberry Pi2- I see that the Pi3 is out, worth buying the newer version or sticking to the '2'? I've no clue about them, just sitting down to watch **+Thomas Sanladerer** how-to video





**jerryflyguy**

---
---
**Pieter Swart** *May 31, 2016 18:04*

Even the original Pi1 runs Octoprint without a problem. The only advantage I can think of is that it has WiFi built in, but as some have mentioned, there's no antenna, so depending on where you place it, you might struggle getting a good signal.(ie, placing it inside the aluminium frame might have some impact)


---
**Eric Lien** *May 31, 2016 18:16*

Since there is no price difference go with the Pi3. I am running the PI3 now on mine and you can feed the difference in page performance.


---
**jerryflyguy** *May 31, 2016 18:19*

Thanks guys, up here in Canada the Pi3 is ~$10 cheaper than the 2.



Is it absolutely necessary to have a Pi w/ the Azteeg X5 or more of a convenience to have remote access etc?  


---
**Pieter Swart** *May 31, 2016 18:35*

If you don't mind connecting it to your PC, then no, however, it really improves the whole experience and the printer often does not sit in a convenient location. I would even choose this over the LCD and Rotary encoder.


---
**jerryflyguy** *May 31, 2016 18:39*

**+Pieter Swart** its most likely going in my heated garage/shop. I've already bot the Viki LCD most likely will buy a Pi also. As I understand it though, w/ an SD card you can print 'unplugged' using the Azteeg X5? The Pi just gives you the remote ability to skip the SD card transfer and put in a webcam if you choose, correct?


---
**Pieter Swart** *May 31, 2016 18:52*

You get a bit more. Just like pronterface or the machine controls on Simplify3d, you get an interface on Octoprint to control the motors of the printer. I've always had to connect my printers to a PC to calibrate because the LCD unit was simply to slow and tedious. 



Now I can sit next to my printer with my tabled and do the same thing that I used to do on the PC. Also, Octoprint can use the Cura engine directly on the Pi so if you properly configure it, it will do the slicing as well. I'm not sure about the speed of the slicing though since I haven't configured it yet. 



I'm still stuck with an 8bit controller so I'm not sure what the LCD offers you on the 32bit controllers. I can say that even though I'm not really using the LCD, it is useful to tweak the more advance settings that's specific to the Marlin firmware that I'm running...


---
**jerryflyguy** *May 31, 2016 19:14*

**+Pieter Swart** ok that makes sense, I'm not sure what all movement control is possible w/ the X5 & smoothieware, I guess worst case I have to connect a PC to it for the initial setup if I skimp on the Pi. 



I just added up my totals thus far.. Burning through my build budget pretty fast.. Things like shipping and exchange are always more than you estimate. The Pi might have to be a add-on in a month or two once I've got something to show for my current purchases. We shall see.. :)


---
**Pieter Swart** *May 31, 2016 19:16*

You can definitely get it later. I only started using octoprint this month and I've been playing with 3d printers for a few years now﻿


---
**Eric Lien** *May 31, 2016 19:40*

**+jerryflyguy** yeah the Pi can surely wait. It is just nice to have down the road. I can push gcode to it, control it, and monitor it remotely via a webcam plugged into the Pi via my phone from bed, or from work. So it's a luxury, by no means a necessity.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/XvwUMKjW4VQ) &mdash; content and formatting may not be reliable*
