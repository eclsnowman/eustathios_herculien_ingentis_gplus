---
layout: post
title: "Just saw this on AliExpress. Looks like a good deal for the money ($285+free s/h to USA!), and it is an Ultimaker-ish style gantry design similar to Eustathios"
date: January 19, 2016 19:24
category: "Discussion"
author: James Rivera
---
Just saw this on AliExpress. Looks like a good deal for the money ($285+free s/h to USA!), and it is an Ultimaker-ish style gantry design similar to Eustathios. Thoughts?





**James Rivera**

---
---
**Bruce Lunde** *January 19, 2016 19:43*

Even if not super mechanically, it might make a good base for a smaller unit. It seems to include the hotend and ramps/motor drivers.


---
**Chris Brent** *January 19, 2016 19:46*

Yeah looks like it might be a cheap way to get a frame and rods. I have a dislike of the cantilevered Z stage, but that could be fixed. The horizontal rod holders look a lot like the Euth ones. 46 sold and five 5 star "reviews", someone on the internet must have one?


---
**James Rivera** *January 19, 2016 19:54*

**+Bruce Lunde** What is mechanically wrong with it? It looks pretty good to me.


---
**James Rivera** *January 19, 2016 19:56*

**+Chris Brent** The cantilevered Z stage has been used successfully in many 3D printers (e.g the Ultimaker), so I'm fine with that.


---
**Chris Brent** *January 19, 2016 20:36*

Fair enough, you just need to do it "right" I guess. Most of my biases come from my old Makerbot which did a lot "wrong" :P The comments mention the Z has a lot of backlash, but that's due to the lead screws not the cantilever. When the platform is that big, why not support both sides though?


---
**Øystein Krog** *January 19, 2016 20:42*

This looks like an absolutely amazing deal.


---
**Bruce Lunde** *January 19, 2016 20:47*

**+James Rivera** Sorry, I did not mean to infer there was anything wrong with it, I was surmising if you bought it and if it was not super solid, it would be a good start....


---
**James Rivera** *January 19, 2016 20:56*

**+Chris Brent** I agree that support on both sides would be better, but I think this may be more than adequate.


---
**James Rivera** *January 19, 2016 20:59*

**+Bruce Lunde** No apology necessary! I was just worried you had spotted some flaw I didn't see. I don't really even need this, but it I've wanted to build an aluminum extrusion based Ultimaker style gantry 3D printer for quite some time, which is why I've been lurking in this (awesome) group.


---
**Eric Lien** *January 19, 2016 21:01*

Great deal if it is built well. No heated bed, but they have an add-on for $10. Power supply is small, so the heated bed requires an additional power supply. It has no cross supports up top, and this style gantry does benefit from additional bracing up there. But that's just a few pieces of extrusion and some angle brackets to add. Also I agree I am not a huge fan of cantilevered beds. But since it is smaller than a Eustathios it might be OK. I think it could be a good deal, but then again to me it really depends on the quality of the parts. Some of the Chinese electronics are just fine... while others are a fire just waiting to happen. If anyone gets one let us know what you think. 


---
**Ted Huntington** *January 19, 2016 22:34*

great deal= thanks for sharing it - the heated bed I would just buy from alirubber and use a 120V relay. The one they provide is an  MK2b 12/24.


---
**James Rivera** *January 20, 2016 01:26*

I just pulled the trigger on it. Seems too good to pass up, and I'm tired of hearing myself complain that I wanted this style of printer but keep running into "not enough time to get all of the parts together". Fingers crossed hoping that it is as good as it looks.


---
**Bruce Lunde** *January 20, 2016 01:32*

I will be interested in how it turns out, keep us posted!


---
**Mike Miller** *January 20, 2016 01:34*

I like it, I'm partial to this design, and darned it, my IGUS printer is down...don't need three printers, though.But man that's cheap. 


---
**Chris Brent** *January 20, 2016 04:06*

If I wasn't still in the tinkering with my delta stage then I'd been onto this too! (Plus explaining to my wife why we need a third printer might be a stretch :) Please let us know how it goes.


---
**James Rivera** *January 20, 2016 19:51*

**+Nathan Walkner** I sure hope not, because I ordered one. But, if so, they can expect a credit card dispute from Capital One, the bank for the Visa card I used to buy it. Conversely, if it arrives intact and as advertised, I will post a nice comment, because good behavior should be rewarded, too.


---
**Ted Huntington** *January 20, 2016 19:57*

I'm interested in hearing more details- it looks like they use a threaded rod for the Z axis- they didn't bother with 2 upper brackets but it would be nice to attach a spool holder on it. I wonder what the extruder is.


---
**James Rivera** *January 20, 2016 20:15*

**+Ted Huntington** Yeah, the extruder is a bit of a wild card, but I knew that before I paid for it (parts list just says, "nozzle"). I suspect it will be a cheap j-head, but as long as it works that's ok with me. I do not expect champagne for the price of beer. Also, I have an unopened E3D v6 I can throw in it, perhaps converting to a dual-extruder. If it is really bad, I also have a spare Ubis hot end. So basically, a dual extruder upgrade is highly likely. :)


---
**Ted Huntington** *January 20, 2016 22:01*

**+James Rivera** I am referring to the cold part of the extruder- the part that moves the filament- I usually call the hot part the "hot end"- but this is a common confusion - sorry about that- but the hot end- I know I will be charged with heresy for this, but I find the E3D V6 clone parts to be good enough for basic printing- I use the ptfe throat because I mostly print with PLA- but I find assembling them myself is a better solution than buying them already assembled.


---
**James Rivera** *January 23, 2016 19:03*

**+Ted Huntington** I have a genuine E3Dv6 I purchased along with my OpenBuilds OX CNC from [http://smw3d.com](http://smw3d.com), who I'm confident actually sells the genuine article. But, I mostly got it so I could print high temperature filament (I've been sitting on a spool of Taulman 618 nylon for well over a year), but some assembly required, so I've put that on hold for other things. I'll get to it eventually.


---
**Ted Huntington** *January 23, 2016 21:09*

**+James Rivera**

Probably the original E3Dv6 is better than the $10 clones, in particular at higher speeds and volumes. There are now 2 clone E3Dv6 throats (heat breaks) on aliexpress - 1 the all metal which works fine for ABS- and probably for Nylon- since there is no PTFE in it- but I've never tried Nylone- only ABS which works consistently- and then a second throat with a PTFE which is needed for PLA- I could not get PLA to extrude after the initial push through without a PTFE tube in the all-metal clone version- but I've read that other people have trouble with PLA and the all-metal original too.


---
**James Rivera** *February 04, 2016 01:52*

It arrived today. :)


---
**Mike Miller** *February 04, 2016 01:59*

And? AAAAND? Don't leave us in suspense!


---
**Eric Lien** *February 04, 2016 02:04*

Alright... We need an unboxing and first impression video :)


---
**James Rivera** *February 04, 2016 02:24*

**+Eric Lien**, **+Mike Miller** I just posted photos from the unboxing. It may be a while before I actually get around to putting the whole thing together though.


---
**Ted Huntington** *February 04, 2016 06:32*

Looks neatly packaged- and I notice that they are using a smaller watt power supply- in particular if using a alirubber heating pad- a large supply is not necessary. I think these z-moving-only bed designs are going to ultimately replace the y-moving ones.



That was pretty fast shipping. It looks like the design is pretty similar to the Eustathios Spider V2- I wonder if they use 10mm perimeter rails and 8mm carriage rails.



A lot of printed parts have been replaced with laser cut acrylic- which is probably a lot faster to produce.



Is that a geared reduced extruder?



Just plain bushings- not self-aligning- let us know if getting the carriage to move freely is no problem. There is a set screw adjustment there which looks complicated.



The z-limit is interesting- they run the rail up higher and it must just be hand adjusted.



nice the way they avoided having to machine the aluminum bed (other than 4 drill holes).


---
**James Rivera** *May 06, 2017 20:47*

**+Ted Huntington** I didn't see your question until now: no, that is not a gear-reduced extruder. It is just a hobbed gear on the stepper shaft.


---
**James Rivera** *May 06, 2017 20:49*

BTW, while it was a little frustrating to get setup and tweaked properly, I have been using it for quite a while now without having to adjust anything. I don't even have a bed leveling sensor and I haven't had to adjust it at all after I spent (a non-trivial amount of) time getting it level.


---
**Ted Huntington** *May 07, 2017 04:55*

oh ok good to read. Too bad that printer is no longer available. I found a similar one here: [https://www.aliexpress.com/item/3D-printer-ultimaker-2-cross-shaft-structure-Diy-Kit/32796573258.html](https://www.aliexpress.com/item/3D-printer-ultimaker-2-cross-shaft-structure-Diy-Kit/32796573258.html) but it is $400 and looks possibly like acrylic. This one opts for putting the X-axis motor on the carriage itself: [https://www.aliexpress.com/item/S5-large-size-metal-fuselage-High-Quality-Precision-DIY-3D-printer-kit-with-LCD-Ultimaker2-structure/32789780180.html](https://www.aliexpress.com/item/S5-large-size-metal-fuselage-High-Quality-Precision-DIY-3D-printer-kit-with-LCD-Ultimaker2-structure/32789780180.html)

[aliexpress.com - 3D printer ultimaker 2 cross shaft structure Diy Kit](https://www.aliexpress.com/item/3D-printer-ultimaker-2-cross-shaft-structure-Diy-Kit/32796573258.html?spm=2114.01010208.3.42.oibwDr&ws_ab_test=searchweb0_0,searchweb201602_4_10152_10065_10151_10130_10068_436_10136_10157_10137_10060_10138_10155_10062_10156_10154_10056_10055_10054_10059_10099_10103_10102_10096_10147_10052_10053_10107_10050_10142_10051_10084_10083_10119_10080_10082_10081_10178_10110_10111_10112_10113_10114_10181_10037_10183_10182_10185_10032_10078_10079_10077_10073_10070_10123_10120-436_10183_10120,searchweb201603_4,ppcSwitch_7&btsid=a64f4b26-0ef9-444d-ac85-90f3dfb46192&algo_expid=7468a3a8-421f-4acb-99ac-d7942cf8a965-5&algo_pvid=7468a3a8-421f-4acb-99ac-d7942cf8a965)


---
**James Rivera** *May 07, 2017 18:57*

That one is quite a bit more expensive. Check out this post I made yesterday:





[plus.google.com - They just keep getting cheaper! Less than $300 (~$340 with shipping from Chin...](https://plus.google.com/u/0/+JamesRivera1/posts/fqg8wwhDcMF)


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/NtaFbCnBUi5) &mdash; content and formatting may not be reliable*
