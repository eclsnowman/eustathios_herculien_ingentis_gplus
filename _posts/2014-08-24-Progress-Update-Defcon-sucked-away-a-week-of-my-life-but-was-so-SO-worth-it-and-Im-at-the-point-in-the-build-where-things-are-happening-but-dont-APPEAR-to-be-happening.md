---
layout: post
title: "Progress Update Defcon sucked away a week of my life (but was so SO worth it), and I'm at the point in the build where things are happening, but don't APPEAR to be happening"
date: August 24, 2014 14:34
category: "Show and Tell"
author: Mike Miller
---
Progress Update



Defcon sucked away a week of my life (but was so SO worth it), and I'm at the point in the build where things are happening, but don't APPEAR to be happening. 



Those of you playing the home game might think 'man, that build platform looks a little janky'...and you'd be right! I'm having envelope issues with the Dual Y-axis motors and the built plate that are complicating things. 



So, between the time I took pictures, and started this post, I've decided to go Eustathios for XY...doing so immediately simplifies electrical runs (everything is in the base of the printer) and I'm back up to an 11"x11"x14.5"" envelope. 



![images/d9380c4295af1e5006a9677352bfd908.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d9380c4295af1e5006a9677352bfd908.jpeg)
![images/aaf651a8b46d00c6872d5f49ab78ee9b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/aaf651a8b46d00c6872d5f49ab78ee9b.jpeg)
![images/47cad1a767a174d8d9dccfa310c9a811.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/47cad1a767a174d8d9dccfa310c9a811.jpeg)
![images/8e62aa9b9020836965db9064b8be04d7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8e62aa9b9020836965db9064b8be04d7.jpeg)
![images/461388abf6b4cc4b30ee1ed68aaaa231.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/461388abf6b4cc4b30ee1ed68aaaa231.jpeg)
![images/61fc0411fbce6377379bbb5ca246238b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/61fc0411fbce6377379bbb5ca246238b.jpeg)

**Mike Miller**

---
---
**Mike Miller** *August 24, 2014 20:44*

This carriage was also an interference fit for the bushings. It's pretty good, but the outside points need to be constrained from twisting (which GT2 belts oughta do) to keep everything square. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/BffnxcXBSLb) &mdash; content and formatting may not be reliable*
