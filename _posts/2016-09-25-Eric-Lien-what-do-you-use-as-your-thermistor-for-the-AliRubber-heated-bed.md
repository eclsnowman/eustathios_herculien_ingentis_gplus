---
layout: post
title: "Eric Lien what do you use as your thermistor for the AliRubber heated bed?"
date: September 25, 2016 03:56
category: "Discussion"
author: jerryflyguy
---
**+Eric Lien** what do you use as your thermistor for the AliRubber heated bed?





**jerryflyguy**

---
---
**Eric Lien** *September 25, 2016 04:00*

What settings in the config?


---
**jerryflyguy** *September 25, 2016 04:01*

**+Eric Lien** yeah, looking at smoothies site they give a list of possible thermistors, no idea which to pick.


---
**jerryflyguy** *September 25, 2016 04:02*

Doing a config setup for the first time in my life.. Looks easy when others do it but not so easy when I'm on the pointy end of the spear ☺️


---
**Eric Lien** *September 25, 2016 04:12*

You can do:



temperature_control.bed.beta 3950 # or set the beta value



(Less accurate, but fine for a heated bed)



Or



temperature_control.bed.coefficients         0.0006454337947,0.0002239473005,0.000000087     #Alirubber 800W 120V



The last one was calculated by **+Zane Baird**​​​ via the Steinhart Hart coefficients method: [http://smoothieware.org/steinharthart﻿](http://smoothieware.org/steinharthart%EF%BB%BF)

[smoothieware.org - Steinharthart - Smoothie Project](http://smoothieware.org/steinharthart)


---
**jerryflyguy** *September 25, 2016 04:14*

**+Eric Lien** k. Thanks!


---
**jerryflyguy** *September 25, 2016 04:29*

I pulled the V3 config off Panucatt's site, that the best starting point or should I be going to the smoothie site or?? (Might as well ask before I get too far into this one) I'm seeing a lot of things that look different from the old V2 one on the Github for the Eustathios.


---
**jerryflyguy** *September 25, 2016 04:40*

**+Eric Lien** do I '#' comment out the temperature_control.bed.beta and "...control.bed.thermistor" and add in a line w/ your noted "temperature.control.bed.coefficients" 



?? 


---
**Eric Lien** *September 25, 2016 04:46*

**+jerryflyguy**​ use the one from the smoothieware site: [github.com - Smoothieware](https://github.com/Smoothieware/Smoothieware/tree/edge/ConfigSamples/AzteegX5Mini/Version3)



And you are correct, if you use the second one, comment out the other line in the fresh config and add the new line "copy/paste"



Yes the new smoothieware configs differ a bunch from my old one. They made a lot of great improvements recently and so some formatting changed.




---
**jerryflyguy** *September 25, 2016 05:09*

**+Eric Lien** all the Viki2 stuff off the old config just gets copied across? Same pins and what not?


---
**Eric Lien** *September 25, 2016 05:47*

**+jerryflyguy**​ nope. But it is in the new one on the smoothieware github. Just need to uncomment it.


---
**jerryflyguy** *September 25, 2016 06:46*

Disregard that one.. Found the problem (panel enable was false)



Is there a better way to shut down the system than just pulling the 110v off the 24v power supply? I've got a switch there but it just feels harsh to shut it off that way


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/DpFaX5dqav9) &mdash; content and formatting may not be reliable*
