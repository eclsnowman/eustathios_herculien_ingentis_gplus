---
layout: post
title: "Here are some photos of the cracking"
date: February 13, 2018 15:01
category: "Show and Tell"
author: Ryan Fiske
---
Here are some photos of the cracking. **+Dennis P** was asking me for some pictures and thought the rest of you might be interested too.



![images/633484fa4f2dec0fa915b38e13b9a370.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/633484fa4f2dec0fa915b38e13b9a370.jpeg)
![images/c78e995b0cfc6c390e656701ae6540f5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c78e995b0cfc6c390e656701ae6540f5.jpeg)
![images/d3ad29b68bc9bdb7799a1e43b548c70e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d3ad29b68bc9bdb7799a1e43b548c70e.jpeg)

**Ryan Fiske**

---
---
**Michaël Memeteau** *February 13, 2018 15:45*

Due to the location, I can't help thinking of the Bearing Gate...

[https://e3d-online.com/blog/2018/01/22/bearinggate/](https://e3d-online.com/blog/2018/01/22/bearinggate/)




---
**Eric Lien** *February 13, 2018 16:09*

Looks like the clear abs Walter printed with for his parts. I never tried that material, but it looks like there is a combination of stress, low strength plastic, and undersized holes. Also when softening the bushing pockets with acetone, it takes a lot of applications with the qtip to get more than just the superficial surface to soften. Also it takes a long time to cure back out before you will want to put any stess on the ABS. 



I think unless you are enclosing the printer PLA or PETG might be a better candidate. I have an odd preference for ABS... but then again I have a large enclosed printer, and I am old school enough when it was one of the only two options to choose from :) 


---
**Alex Broadchief** *February 13, 2018 18:13*

We modified this as the price of the selfcentering bushings is horrible here. We took 30mm Graphite-Bushings (aliexpress) holded in a self printed TPU-Sleeve and it works flawless since a year now (heavy duty printer) without cracking or other up to real 500mm/s.


---
**Ryan Fiske** *February 13, 2018 18:42*

**+Eric Lien** up it's the translucent black abs. Very cool color, but I'm thinking that I likely was printing them undersized. 


---
**Ryan Fiske** *February 13, 2018 18:43*

Does anyone have experience using 3D Hubs or another online 3d printing service? Thinking about getting mine printed through a service.


---
**James Rivera** *February 13, 2018 19:09*

I recently used some transparent black PLA (from Hatchbox) for the first time. It seems terribly brittle and the filament has broken several times while printing. I have bought several spools from Hatchbox and this is the first one that was bad, and looking at yours I have to wonder if this is inherent in this color filament? As in, does the lack of colorant make it more brittle? Related anecdotal evidence: I've tried some clear ("natural") PLA before (bargain stuff from Printrbot) and it was similarly awful.


---
**Walter Hsiao** *February 13, 2018 19:45*

Yeah, I've found the transparent ABS filaments crack under stress and  behave very differently from the opaque ABS filaments.  I just added a couple of example pictures to a brief blog post I put up a while back - [http://thrinter.com/part-failure-1-crazing-cracking/](http://thrinter.com/part-failure-1-crazing-cracking/)



My rod holders printed out of the same plastic are still holding up, but the z-rod clamps cracked fairly quickly.


---
**Ryan Fiske** *February 13, 2018 20:02*

Thanks for the input **+James Rivera** **+Walter Hsiao**. I think I need to rebuild anyways and spend much more time getting the frame right, so I'll try to get some new parts printed and start from scratch.


---
**Dennis P** *February 13, 2018 23:56*

WOW! That looks like classic hoop stress. How tight were the bores that you put the bearings in? In reflection, I 'goosed' my bores with a 1/2" diam sanding drum on the dremel to clean up the supports after printing. I wished I had a sharp 5/8 reamer to run down them to make them straight and to size. I used one company brokered through 3DHubs. It was pricey. Then I discovered my library had one for supervised public use. 


---
**Dennis P** *February 14, 2018 00:19*

**+Michaël Memeteau** unfortunately, we learned the lesson of overstress due to poor design many many years ago...one example [en.wikipedia.org - Great Molasses Flood - Wikipedia](https://en.wikipedia.org/wiki/Great_Molasses_Flood)




---
**Julian Dirks** *February 14, 2018 02:39*

Yikes that looks hellish!  That aside some good info here for me as I'm going to be printing parts soon.  I've noticed my 2 rolls of transparent PLA are really brittle as well so ordered opaque PETG.  If you print in PETG acetone isn't going to work so do you need to soften the part with ethyl acetate or has PETG got enough flex to just pop the bushings in? 


---
**Dennis P** *February 14, 2018 05:04*

**+Julian Dirks** these bearings holders are **+Walter Hsiao**'s remixes. They printed best for me in this orientation, so that the bores for the bearings were in the Z direction. **+Alex Broadchief**'s comment should be taken to heart- check your shipping costs on those bearings. 

There are only 2 makers of these types of bushings apparently- spyraflo makes the sintered bronze version sold through bearing distributors like [sdi-sp.com](http://sdi-sp.com) and [mcmaster.com](http://mcmaster.com), and an Igus acetal spherical bearing version that is similar. You might want to consider the TPU shell version of plain bearings. 

I have also seen orings used to cage the bearings circe clip grooves in a bore with the same effect. 

I know that Water did similar TPU sleeving of graphite plugged bearings based on his photos before he ended up with the self aligning type- hoping that he might comment further.     

![images/6568d29c5ef97b4ed8cf0c504d0c3a37.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6568d29c5ef97b4ed8cf0c504d0c3a37.png)


---
**Walter Hsiao** *February 14, 2018 06:34*

I'm still using the longer graphite bushings with a ninjaflex sleeve on the rod holders (1 each), they seem to work well there since you don't have to worry about alignment and they are much cheaper.



![images/bc48ca180e30ca2945fa12af1220562c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bc48ca180e30ca2945fa12af1220562c.jpeg)


---
**Walter Hsiao** *February 14, 2018 06:35*

For the carriage I'm using the self aligning bushings.  I did initially use the graphite bushings for the carriage, but it was difficult to get them aligned, it was much easier with the self aligning ones.

![images/30780207193b4dcd150dbfd01f6fffde.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/30780207193b4dcd150dbfd01f6fffde.jpeg)


---
**Julian Dirks** *February 14, 2018 07:10*

Thanks for the advice **+Dennis P** I have ordered the bushings as per the BOM along with 10mm rods all around ex Misumi.  There are enough stories in here of bent rods and mismatched bearings to convince me that it might be false economy going cheap when you consider the cost of freight down to NZ with either option and the hassle/cost/delay if there are any issues.



**+Walter Hsiao** Have been reading your blog etc.  Great info there.  Your prints and designs are something to aspire to. 


---
**Michaël Memeteau** *February 14, 2018 09:08*

**+Julian Dirks** Just try heat gun or simple lighter, that should work just fine... That's what I did on my Black Panther with red PETG.


---
**Eric Lien** *February 14, 2018 13:25*

**+Michaël Memeteau** one thing to be careful of is overheating the bronze bushings in order to push them in. Better to heat the plastic.



I made the mistake of heating the bushings with a pen torch when I first built my Eustathios. The bushings are an outer housing, and into that housing the bronze sleeve is epoxy potted creating the spherical joint.



Excess heat cracked the epoxy, which ruined the spherical joint, hence the bearing.


---
**Michaël Memeteau** *February 14, 2018 14:10*

**+Eric Lien**  I was thinking of heating the PETG only as a matter of fact (this is what I did to fit the IGUS spherical bearing).


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/CsTN4kwnruw) &mdash; content and formatting may not be reliable*
