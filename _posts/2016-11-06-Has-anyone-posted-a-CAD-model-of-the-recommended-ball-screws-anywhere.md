---
layout: post
title: "Has anyone posted a CAD model of the recommended ball screws anywhere?"
date: November 06, 2016 20:35
category: "Deviations from Norm"
author: Jeff DeMaagd
---
Has anyone posted a CAD model of the recommended ball screws anywhere?





**Jeff DeMaagd**

---
---
**Eric Lien** *November 06, 2016 21:22*

Just added a few thing for you to the github. Should get you going: [https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/walterhsiao/1204%20Ballscrew%20Bed%20Support/Eustathios%20Ball%20Screw%20Model%20Files](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/walterhsiao/1204%20Ballscrew%20Bed%20Support/Eustathios%20Ball%20Screw%20Model%20Files)

[github.com - Eustathios-Spider-V2](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/walterhsiao/1204%20Ballscrew%20Bed%20Support/Eustathios%20Ball%20Screw%20Model%20Files)


---
**Eric Lien** *November 06, 2016 21:27*

I think you will notice the ball screw model is slightly shorter. That's because on the Eustahtios Spider V1 (and V1.5 which is what I call mine because its half converted into a V2) was before I dropped the floor down to gain extra Z. I noted this in the PDF print in that directory. But if you can open the solidworks 2015 file you can change the length to whatever you need.


---
**Jeff DeMaagd** *November 06, 2016 21:40*

Thank you. The Golmart listing doesn't show or describe the end thread. Is that a problem? Or do you just send them the PDF to tell them what you want?


---
**Eric Lien** *November 06, 2016 22:40*

I added it later. You can use it as they have it listed with a 8mm ID collar on the bottom.  But I like using a nylock nut on the bottom instead.


---
**Eric Lien** *November 06, 2016 22:40*

Yeah just send them the PDF, price shouldn't change.


---
*Imported from [Google+](https://plus.google.com/+JeffDeMaagd/posts/AYUqy9gV7jT) &mdash; content and formatting may not be reliable*
