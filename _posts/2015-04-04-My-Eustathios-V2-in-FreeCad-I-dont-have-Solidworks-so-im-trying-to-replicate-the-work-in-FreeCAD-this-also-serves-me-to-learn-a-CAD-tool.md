---
layout: post
title: "My Eustathios V2 in FreeCad, I don't have Solidworks so i'm trying to replicate the work in FreeCAD, this also serves me to learn a CAD tool"
date: April 04, 2015 17:34
category: "Show and Tell"
author: Ricardo Rodrigues
---
My Eustathios V2 in FreeCad, I don't have Solidworks so i'm trying to replicate the work in FreeCAD, this also serves me to learn a CAD tool.



For the moment now just the bearing holder.. ;)

![images/bb2eb21f0102836db4739cafc4415f62.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bb2eb21f0102836db4739cafc4415f62.jpeg)



**Ricardo Rodrigues**

---
---
**ThantiK** *April 04, 2015 18:19*

I <i>love</i> the ideology behind FreeCAD, and I try it every now and again to see if it's worth using - every time it's just frustrated me to no end.  But I keep going back to it every year or so to give it another shot.  :D


---
**Eric Lien** *April 04, 2015 18:43*

Looking good. Keep up the great work.


---
**Eric Duprey** *April 04, 2015 19:23*

**+Ricardo Rodrigues**​​ Have you seen onshape? [https://www.onshape.com/](https://www.onshape.com/) It seems they use the github model, limited free private projects, unlimited free public projects. Apparently made by some of the original creators of Solidworks and can import solidworks files. Not free software or open source, unfortunately.﻿


---
**Ricardo Rodrigues** *April 04, 2015 19:24*

I'm sure that it still comes up short in many things, but I think it is usable.


---
**Ricardo Rodrigues** *April 04, 2015 19:29*

Yes, **+Eric Duprey**, I even have a invitation, but I'm trying to give a try on to FreeCAD for the ideology, like **+ThantiK** said.



I like Onshape but FreeCAD seemed more attractive to me.


---
**James Rivera** *April 04, 2015 22:17*

**+ThantiK**​ my thoughts exactly. I'm still trying to use it, but my needs are (usually) single use and I wind up just creating a quick Sketchup model.


---
**James Rivera** *April 04, 2015 22:19*

**+Eric Duprey**​ I will try OnShape again sometime soon. My first try was frustrating, but they are still pretty new, so I will try them again later.


---
*Imported from [Google+](https://plus.google.com/+RicardoRodriguesPT/posts/hGg7f8znWS1) &mdash; content and formatting may not be reliable*
