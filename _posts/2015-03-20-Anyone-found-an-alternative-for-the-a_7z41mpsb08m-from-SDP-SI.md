---
layout: post
title: "Anyone found an alternative for the a_7z41mpsb08m from SDP/SI?"
date: March 20, 2015 03:49
category: "Discussion"
author: Ben Delarre
---
Anyone found an alternative for the a_7z41mpsb08m from SDP/SI? (The sleeved bearings for the eustathios carriage).



Guess I'll have to wait till they're back in stock unless there's a drop in replacement out there?





**Ben Delarre**

---
---
**Erik Scott** *March 20, 2015 04:25*

I use LM8UU bearings for the carriage. Which carriage are you using?


---
**Ben Delarre** *March 20, 2015 04:28*

I printed the most recent eustathios v2 carriage. I would prefer bushings to be honest,  in my experience bearings have either a)  eaten into my smooth rods or b)  left nasty oil over my hands everytime I touch the thing. 


---
**Eric Lien** *March 20, 2015 04:54*

**+Ben Delarre** [http://www.ultibots.com/8mm-bronze-bearing-4-pack/](http://www.ultibots.com/8mm-bronze-bearing-4-pack/)



Ultobots are if I remember a father/son group. I like everything I have bought from them.


---
**Ben Delarre** *March 20, 2015 04:55*

Excellent thanks Eric! 


---
**Eric Lien** *March 20, 2015 04:55*

They have 10mm ones also.


---
**Isaac Arciaga** *March 20, 2015 05:19*

Also LulzBot [https://www.lulzbot.com/products/8mm-bronze-bushing-pack-4](https://www.lulzbot.com/products/8mm-bronze-bushing-pack-4)


---
**Eric Lien** *March 20, 2015 06:09*

**+Isaac Arciaga** nice find.


---
**Dennis P** *November 28, 2017 04:31*

I know that this is a little stale... those are SDP/SI units. 

[shop.sdp-si.com - Product &#x7c; Designatronics Store &#x7c; Stock Drive Products/Sterling Instrument](http://shop.sdp-si.com/catalog/product/?id=A_7Z41MPSB08M)

They are about $3/each in 2017 prices...




---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/8mRMc7myoav) &mdash; content and formatting may not be reliable*
