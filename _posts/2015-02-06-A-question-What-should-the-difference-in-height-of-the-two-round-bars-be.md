---
layout: post
title: "A question. What should the difference in height of the two round bars be?"
date: February 06, 2015 17:38
category: "Discussion"
author: Richard Mitchell
---
A question. What should the difference in height of the two round bars be?



I'm going through ground down M5 nuts and printed frame braces at a prodigious rate.



![images/db35f949574986c328e078283c75202f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/db35f949574986c328e078283c75202f.jpeg)
![images/49df27d626e194ba98c17aaea8cd0bd7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/49df27d626e194ba98c17aaea8cd0bd7.jpeg)
![images/33366ac5f13e4f245f4c8a3000e4ab5d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/33366ac5f13e4f245f4c8a3000e4ab5d.jpeg)

**Richard Mitchell**

---
---
**Eric Lien** *February 06, 2015 17:42*

14mm


---
**Richard Mitchell** *February 06, 2015 17:44*

Is that the gap or the distance between centres?


---
**Mike Miller** *February 06, 2015 20:24*

I kept all sides loose, then added the carriage, and kept moving and snugging until the carriage was moving smoothly, and the bars were parallel with the horizontal support structure. 


---
**Richard Mitchell** *February 06, 2015 20:28*

I had thought that to just wanted to make sure I got the ballpark right to design the rod carriages. Working backwards from a hot end carriage is a plan I can live with. 100 M5x12 so far and I've run out again. Should have worked out how many beforehand but I'm enjoying the journey.


---
**Jim Wilson** *February 06, 2015 20:39*

This spacer might help: [https://drive.google.com/folderview?id=0B1rU7sHY9d8qVnp2WWxxZTFCNVU&usp=sharing](https://drive.google.com/folderview?id=0B1rU7sHY9d8qVnp2WWxxZTFCNVU&usp=sharing)﻿ via **+Eric Lien**​


---
**Eric Lien** *February 06, 2015 22:17*

**+Richard Mitchell** center to center.


---
**hon po** *February 07, 2015 12:23*

Vertical spacing for rotating rods is 20mm, and 14mm for non rotating rods. I change bearing holder on one axis to align the rods base on the frame. Will post it for anyone want to try.



**+Richard Mitchell** I see that you use non self-aligning bushing. Interested to know how this work out for you. Keep us posted.


---
**Eric Lien** *February 07, 2015 16:23*

**+hon po** I forgot I was thinking of the central rods. Thanks.


---
**Richard Mitchell** *February 07, 2015 16:51*

I'm also using 8mm rods for both rotating and non rotating because the whole design is smaller. Hopefully I can knock another centimetre or two off the dimensions when I can do motion testing.



I don't suppose there's a design I can base the xy blocks on for 8mm rods and bronze bushes anywhere?


---
**Eric Lien** *February 07, 2015 17:46*

**+Richard Mitchell**​ 8mm bronze self align bushings have the same OD as the 10mm. No changes needed for the printed parts.


---
**Eric Lien** *February 07, 2015 17:47*

Ok. I see you are using plain bushings.


---
**Richard Mitchell** *February 07, 2015 18:26*

Just trying to use the simplest cheapest parts I can, after all I have a 3D printer so I can do rapid prototyping :)


---
*Imported from [Google+](https://plus.google.com/111547506541230089782/posts/agaqTAMLTTh) &mdash; content and formatting may not be reliable*
