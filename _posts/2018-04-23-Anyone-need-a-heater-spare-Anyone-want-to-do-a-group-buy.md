---
layout: post
title: "Anyone need a heater/spare? Anyone want to do a group buy?"
date: April 23, 2018 02:30
category: "Discussion"
author: Dennis P
---
Anyone need a heater/spare? Anyone want to do a group buy?



I screwed up and put a divit in my heater. I can not get past 60C... I am going to order another one from Alirubber just in case.... 



They quoted me  ~$25/each for the heater depending on 'options'  but the killer is shipping- $25 by DHL. 



Eric suggested getting them without thermistors and just hole in the middle so that you take the actual temp of the aluminum heat spreader with the thermistor of you choice.  



Anyone interested? We could combine orders and save on shipping.. 







**Dennis P**

---
---
**William Rilk** *June 29, 2018 05:48*

Just now gathering materials for a herc build so I'll be ordering a heater or two myself here pretty soon.  Any mention of how big the thermistor hole should be?  I was thinking around 15-20 mm, dead center.


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/ad7vpg7nscs) &mdash; content and formatting may not be reliable*
