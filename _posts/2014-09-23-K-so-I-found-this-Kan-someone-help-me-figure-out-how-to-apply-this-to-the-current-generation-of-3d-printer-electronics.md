---
layout: post
title: "K so I found this. Kan someone help me figure out how to apply this to the current generation of 3d printer electronics?"
date: September 23, 2014 04:57
category: "Discussion"
author: D Rob
---
[https://github.com/danithebest91/ServoStrap/blob/master/Arduino_stepper_motor_emulator_v1.0.1_direct_port_manipulation.pde](https://github.com/danithebest91/ServoStrap/blob/master/Arduino_stepper_motor_emulator_v1.0.1_direct_port_manipulation.pde) K so I found this. Kan someone help me figure out how to apply this to the current generation of 3d printer electronics? ramps or ultiboard, or marlin in general. I am trying, but my arduino programming skills are non existent!﻿





**D Rob**

---
---
**Wayne Friedt** *September 23, 2014 06:41*

Wow this really cool. I now on my TODO list for later. 


---
**Miguel Sánchez** *September 23, 2014 08:25*

I have reviewed the code: You need to use direct port manipulation code and most likely you will need to tune the KP,KD,KI values by sending them through a serial port to the board (you may use SoftwareSerial to send data to each motor driver using a different Mega pin).


---
**Kalani Hausman** *September 26, 2014 00:11*

I would look at the cost for servos with enough motive force to replace stepper motors, to check that the range of motion (limiting build area) and torque (limiting hotend mass and build transfer rates) are advantageous at or near the same price point. Almost any form of robotic limb can serve to transit the build volume, we just see the same thee formats of 3D printers (Cartesian, Delta. and Polar) due to the cost-ratios of the more complex systems.


---
**D Rob** *September 26, 2014 03:50*

**+Kalani Hausman** if you look at my profile you will see my new bot. I want to use 24v or 12v dc brushless motors with high res optical disc encoders like or from standard 2d printers. My bot is using 1204 sfu c5 ground ball screws the translation of motion in the form of a highly efficient screw will handle most of the torque issues while adding resolution. The higher acceleration rates and speeds of a dc will bring this machine into more realistic velocities. 


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/ZVQBBmTxytA) &mdash; content and formatting may not be reliable*
