---
layout: post
title: "Heated bed wiring. On the silicone heated bed, should the wires come out the rear of the bed or one of the sides?"
date: September 19, 2016 03:51
category: "Discussion"
author: jerryflyguy
---
Heated bed wiring.



On the silicone heated bed, should the wires come out the rear of the bed or one of the sides? Rear seems to make the most sense but.... Wouldn't be the first time I'd thought about things sideways! 😬





**jerryflyguy**

---
---
**Eric Lien** *September 19, 2016 04:44*

HercuLien or Eustathios? I forget which one you built. 


---
**jerryflyguy** *September 19, 2016 05:04*

**+Eric Lien** sorry, Eustathios V2 (I keep forgetting to post that 😬)


---
**Eric Lien** *September 19, 2016 05:11*

I would come out the back. In the bottom cover plate I had a cutout for the wires near the corner (where the SSR and PSU mount).


---
**jerryflyguy** *September 19, 2016 05:23*

Awesome, thanks! It's coming together, get my new Misumi rods this Wednesday (I hope) and then I think it's just assembly from there


---
**Eric Lien** *September 19, 2016 05:34*

**+jerryflyguy**​​ can't wait to see your first print :)



As always, ask questions as they come up. Many here have been through this process before and can help... And many more might have similar questions but haven't asked.





![images/8646356638ec6c23d8cf5e76bb720df7.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8646356638ec6c23d8cf5e76bb720df7.gif)


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/Jqrpo2NagJN) &mdash; content and formatting may not be reliable*
