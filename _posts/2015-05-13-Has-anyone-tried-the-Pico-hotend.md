---
layout: post
title: "Has anyone tried the Pico hotend?"
date: May 13, 2015 13:34
category: "Discussion"
author: Eirikur Sigbjörnsson
---
Has anyone tried the Pico hotend?



[https://www.b3innovations.com/](https://www.b3innovations.com/)





**Eirikur Sigbjörnsson**

---
---
**Jeff DeMaagd** *May 13, 2015 15:52*

I have a couple I've been running for about a week with ABS and HIPS.



PLA seems to be the most problematic, though thats only going by online discussions.



I would strongly suggest making a good fan duct to blow on the fins rather than the odd fin they recommend.


---
**Jeff DeMaagd** *May 13, 2015 18:54*

I forgot to add, its been working very well for me. But I don't have more than 60 hours of run time on them yet.


---
**Eirikur Sigbjörnsson** *May 13, 2015 19:49*

I might opt for those instead of E3D for my Herculien build.


---
**Jeff DeMaagd** *May 13, 2015 20:51*

I'd post a photo of my Pico setup to this thread  but G+ isn't letting me.


---
*Imported from [Google+](https://plus.google.com/118262882256504121671/posts/KmFWGEU2xJw) &mdash; content and formatting may not be reliable*
