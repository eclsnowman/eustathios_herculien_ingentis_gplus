---
layout: post
title: "Hey all, I need a little help figuring out how to assemble the z-axis"
date: April 23, 2015 02:34
category: "Discussion"
author: Ben Delarre
---
Hey all,



I need a little help figuring out how to assemble the z-axis.



I tried attaching the z-axis bed supports to the bed extrusions, screwing the z axis lead screws into the bronze nuts, and then inserting them into the z axis supports. This sort of worked, but i soon discovered that the entire axis is somewhat skewed.



Its clearly leaning to one side, I managed to force the smooth rods into the clamps but I think the whole mechanism is rather tight now.



Any suggestions as to why the z is skewed?





**Ben Delarre**

---
---
**Mike Miller** *April 23, 2015 02:36*

Hard to say without pics, but want ZERO effort movement. Either fiddle with it now, or  later...after the wear causes you to buy new bronze and you have to figure out why the motor is skipping steps. 


---
**Ben Delarre** *April 23, 2015 02:38*

Yeah, definite needs fixing.



Any suggestions on the correct order and steps to assemble the z from scratch, perhaps i've done something backwards?


---
**Mike Miller** *April 23, 2015 02:41*

No clue (pics would help :) ). While this isn't a kit, lots of us have been successful at making it...keep at it, it'll come to you. 


---
**Isaac Arciaga** *April 23, 2015 03:04*

**+Ben Delarre** If I'm understanding your description correctly, your Z axis is binding when manually jogging the axis up and down?


---
**Ben Delarre** *April 23, 2015 03:10*

I've posted some pics in the other post. **+Isaac Arciaga**​ it's more an issue of the smooth rods and screws not being square with the clamps I think resulting in it all being off. 


---
**Erik Scott** *April 23, 2015 03:16*

Are you using version 1 or 2 of the z-axis?



First of all, make sure the z-bed supports are centered on the z-bed frame. you want the bearings to be dead center on both sides. If you're using version 1, make sure you leave everything loose. Do what you did before, but allow the smooth rod supports and z screw mounts to slide along the aluminum extrusions. Screw the bed all the way down to the bottom most position. Make a mark half way along the bottom horizontal aluminum extrusion and position the lower smooth rod holder there and tighten. Do the same at the top. Then do the other side. Then do the Z-screw mounts. this constrains everything in the right order, and by having the bed all the way at the bottom when you do this, you ensure you get the z-screw aligned as best as possible. 



If you can post some pics, that would be great!


---
**Eric Lien** *April 23, 2015 15:00*

I would also check to make sure the tolerance stack up of the parts isn't misaligning things. The bottom mounts look like they are slightly over extruded. This pushes the rods in towards the center and may cause misalignment and binding since the bearings on the bed are fixed by the combined length of the assembly.


---
**Erik Scott** *April 23, 2015 15:03*

If you don't mind PLA, I could print and send you some replacement parts. My machine is pretty well calibrated. Let me know, and maybe we could work something out. 


---
**Ben Delarre** *April 23, 2015 15:08*

It's quite possible that my mounts are over extruded.  The printer I used for them was not in great shape. I will try again today and if that fails look to getting a better set made. **+Erik Scott**​​ I might take you up on that. 


---
**Erik Scott** *April 23, 2015 15:23*

**+Ben Delarre**​ I'm looking to get my aluminum bed cut (I'm printing on acrylic and bluetape at the moment) and if you know anything about where I could get that done, we'll have a deal. 


---
**Ben Delarre** *April 23, 2015 15:49*

**+Erik Scott** I'm probably going to attempt to cut the aluminum bed myself on my Shapeoko, but it'll be a few weeks before I get to that. Just moved house and the CNC enclosure is still in bits in the garage. I'll let you know if I'm successful!


---
**Eric Lien** *April 23, 2015 17:32*

**+Ben Delarre** if it is just over extruded you could just use a rat tail file and test fitting. Just take a measurement of the rods in the mounts (no bed installed), then compare that to the spacing of the bearings on the bed to see if there is a difference. That will tell you if there is an interference... And how much must be removed.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/XLRU23iotAT) &mdash; content and formatting may not be reliable*
