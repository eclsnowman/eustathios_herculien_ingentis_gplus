---
layout: post
title: "My first actual 3d model where dimension and placement matter"
date: November 12, 2015 04:47
category: "Discussion"
author: Jim Stone
---
My first actual 3d model where dimension and placement matter.



i have this feeling i screwed up . i think i may have the pi mounts sideways. and the card when in the slot. may go out of bounds.



[http://www.thingiverse.com/thing:1126452](http://www.thingiverse.com/thing:1126452)





**Jim Stone**

---
---
**Bud Hammerton** *November 12, 2015 12:25*

Congrats on your first design.



A suggestion, if you think your model may need some tweaking check the "Work in Progress" box on the model upload page of Thingiverse. It will let others know there could be changes. Also as a side. If you also include your source files (not just the exported STL file) it will be much easier for others to make modifications for their specific needs


---
**Jim Stone** *November 12, 2015 17:49*

Source files are straight down pictures from google or tech drawings.



if someone has models of the Pi B ( not the b+) could they send them my way. the tech drawing wasnt clear on the orientation and i want the memory slot and usb facing the long sides and not the short sides.


---
**Bud Hammerton** *November 13, 2015 08:13*

**+Jim Stone** I am not sure you understood what I meant by source. I mean the native format of the CAD program you used to create them, not just the STL files.



And , what format do you want the rPiB models in?


---
**Bud Hammerton** *November 13, 2015 08:20*

[https://grabcad.com/library/raspberry-pi-model-b](https://grabcad.com/library/raspberry-pi-model-b)


---
**Jim Stone** *November 13, 2015 08:23*

oh... i used fusion to do it. and i think i only saved it as STL =/


---
**Bud Hammerton** *November 13, 2015 08:29*

Fusion 360 is a good choice, fairly robust and it saves to the cloud. A big plus is that it is free for Education and small Entrepreneurs. It can also export 3D solid formats of IGES, STEP, SMT, SAT and their own native F3D. When I post on Thingiverse or YouMagine, especially for a single use item, I also post a couple of 3D solid file formats. I don't want to revisit my models if I don't need to tweak them for myself. The 3D solid model files makes it easier for someone else to do their own tweaking without bothering me to do it for them.


---
**dstevens lv** *November 15, 2015 01:33*

**+Jim Stone** Your Fusion account should still have the .f3d files linked to it online.  To provide editable source you can export the object as a .f3d file or share it on A360.  Export in STEP, IGES, etc creates what is called a "dumb solid" and while it can be edited the formats lose the parametric assets of the model.  


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/8gKChde47EL) &mdash; content and formatting may not be reliable*
