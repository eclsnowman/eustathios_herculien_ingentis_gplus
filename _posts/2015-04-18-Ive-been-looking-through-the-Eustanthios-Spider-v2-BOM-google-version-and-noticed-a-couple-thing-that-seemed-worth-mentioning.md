---
layout: post
title: "I've been looking through the Eustanthios Spider v2 BOM (google version) and noticed a couple thing that seemed worth mentioning"
date: April 18, 2015 22:52
category: "Discussion"
author: Walter Hsiao
---
I've been looking through the Eustanthios Spider v2 BOM (google version) and noticed a couple thing that seemed worth  mentioning.



I think the name and link for bars for the bed (HFSB5-2020-355-AH177_5) is supposed to end in a 177.5 instead of 177_5.  Also, the description lists it at 562mm in length instead of 355.



The BOM shows and links to LM10UU bearings, but the description mentions a 55mm length and looks like the parts are sized for the longer LM10LUU?



There are two entries for M3x10mm button cap screws (91239A115 and 94500A223) in the sum of parts by vendor, not sure if that was intentional, the only difference I can see is one is black oxide, the other is stainless.



If anyone is ordering screws from Trimcraftaviationrc, they're adding M5x10mm and M5x12mm button socket cap screws (should be online Monday).



Thanks to everyone who contributed to these printers, instructions, and BOMs, it's all really helpful.  I'm looking into building a Eustanthios Spider v2 once I figure out how to order everything.





**Walter Hsiao**

---
---
**Seth Messer** *April 19, 2015 02:14*

Thanks so much Walter. I'll get that updated this weekend.


---
**Walter Hsiao** *April 19, 2015 04:16*

You're welcome, thank you for helping to improve and maintain the BOM. One more thing I noticed is the list of printed parts seems to be missing the "Z Endstop Mount V2"


---
**Gus Montoya** *April 19, 2015 04:44*

**+Seth Messer**  Wow the guy is thorough.   :)


---
*Imported from [Google+](https://plus.google.com/+WalterHsiao/posts/HtQqVnc5YQw) &mdash; content and formatting may not be reliable*
