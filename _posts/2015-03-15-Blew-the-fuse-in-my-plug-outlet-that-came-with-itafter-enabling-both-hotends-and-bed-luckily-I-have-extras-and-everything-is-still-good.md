---
layout: post
title: "Blew the fuse in my plug outlet that came with it...after enabling both hotends and bed, luckily I have extras and everything is still good"
date: March 15, 2015 08:16
category: "Show and Tell"
author: Derek Schuetz
---
Blew the fuse in my plug outlet that came with it...after enabling both hotends and bed, luckily I have extras and everything is still good. Other then that I am 2 small belts from printing(calibrating). Dam China post and sdp-si I want to finish this thing





**Derek Schuetz**

---
---
**Daniel Salinas** *March 16, 2015 13:22*

Love the #DAM


---
**Daniel Salinas** *March 16, 2015 13:23*

What is the fuse rated?  I should probably stock up


---
**Derek Schuetz** *March 16, 2015 14:14*

There suppose to be rated 10a I stressed tested the machine again with a new fuse and no issues think it was just a shity Chinese fuse that came with the outlet


---
**Gus Montoya** *March 17, 2015 06:27*

Wow, sounds like your really really close to finishing. Keep us posted :)


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/GSGDcTcTHrZ) &mdash; content and formatting may not be reliable*
