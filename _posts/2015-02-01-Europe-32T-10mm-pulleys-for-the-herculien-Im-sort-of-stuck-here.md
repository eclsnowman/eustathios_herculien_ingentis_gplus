---
layout: post
title: "Europe + 32T/10mm pulleys for the herculien Im sort of stuck here..."
date: February 01, 2015 12:50
category: "Discussion"
author: Jo Miller
---
Europe + 32T/10mm pulleys for the herculien



I´m sort of stuck here...  i´v tried ordering them through Robotdig, but they will be in stock again by the end of March (!!), Mitsume Europe wont sell to private customers.   any alternatives ?﻿





**Jo Miller**

---
---
**Eric Lien** *February 01, 2015 14:15*

Maybe contact these guys and see if they will make customs?



[http://www.aliexpress.com/store/group/2GT-3GT-5GT-Pulleys/702327_254857481/2.html](http://www.aliexpress.com/store/group/2GT-3GT-5GT-Pulleys/702327_254857481/2.html)


---
**Jo Miller** *February 01, 2015 14:41*

thanks Eric, I´ll give them an @ tommorrow, might be of interrest for more new potentional herculin-builders   :-)


---
**Brandon Satterfield** *February 01, 2015 17:43*

Looking like I'll a large handful of these tomorrow, if your in the US may be able to get them to you a little quicker. 


---
**Isaac Arciaga** *February 01, 2015 19:06*

**+Brandon Satterfield** You will be stocking these? 5mm/8mm/10mm bore sizes?


---
**Jo Miller** *February 01, 2015 20:03*

 

+Brandon    well, it seems to be an european issue, i `m located in Hamburg/germany 

,with or without flange  .﻿


---
**Isaac Arciaga** *February 01, 2015 20:15*

If **+Brandon Satterfield** has the non-shouldered pulleys I will buy immediately :)


---
**Brandon Satterfield** *February 01, 2015 20:46*

Hey guys I will be able to confirm tomorrow, but I believe it will be a standard pulley. 


---
**Eric Lien** *February 01, 2015 21:48*

**+Brandon Satterfield**​ he's that man to scratch your reprap itch. Soon my BOM can be one line with a link to your site ;)﻿


---
**Brandon Satterfield** *February 01, 2015 23:29*

**+Eric Lien** I vote for a Herculien in every household!!! 


---
*Imported from [Google+](https://plus.google.com/103341289176473342280/posts/86797hb6A6w) &mdash; content and formatting may not be reliable*
