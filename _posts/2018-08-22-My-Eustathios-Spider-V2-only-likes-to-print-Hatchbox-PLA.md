---
layout: post
title: "My Eustathios Spider V2 only likes to print Hatchbox PLA"
date: August 22, 2018 02:35
category: "Discussion"
author: Brandon Cramer
---
My Eustathios Spider V2 only likes to print Hatchbox PLA. 



Do you have any tips on which setting to change in Simplify3D when trying other filaments? If the extruder temp isn’t near 250 degrees it will push the tubing that holds the filament out of the extruder and makes a big pile of filament spaghetti. This is with at least one Bowden clip in place. 





![images/691b91ab9eedb6493f698b7b2a9c489b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/691b91ab9eedb6493f698b7b2a9c489b.jpeg)



**Brandon Cramer**

---
---
**Dennis P** *August 22, 2018 03:27*

Have you double checked you actual temps vs. what the machine thinks its temp is? 250 for anything sounds high! I print PLA at 180-185 and PETG at 230 without a problem on all my pritners. 


---
**Brandon Cramer** *August 22, 2018 03:38*

**+Dennis P** I have never printed anything under 200 since I built this. It has always been 220 or higher. I’m not sure how I would check the temperature. 


---
**Rob Povey** *August 22, 2018 04:54*

Use a thermocouple, you can buy a meter with one for under $20. FWIW all my E3D hotends read very high with the recommended firmware settings, some are off by as much as 25C.


---
**Brandon Cramer** *August 22, 2018 07:06*

Would this work? After I use it once I can use it for food. 



ThermoPro+ TP18 Digital Food Cooking Thermocouple Thermometer Ultra Fast Instant Read Meat Thermometer with Touchable Button for Kitchen BBQ Grill Smoker [amazon.com - Amazon.com: ThermoPro+ TP18 Digital Food Cooking Thermocouple Thermometer Ultra Fast Instant Read Meat Thermometer with Touchable Button for Kitchen BBQ Grill Smoker: Kitchen & Dining](https://www.amazon.com/dp/B0793MSG7S/ref=cm_sw_r_cp_api_vRqFBbXAQVP0G)


---
**Eric Lien** *August 22, 2018 13:09*

Do you still have the HercuStruder, or have you moved to the Bondtech?


---
**Dennis P** *August 22, 2018 16:42*

the thermistor's are notoriously inaccurate but if the controller thinks its 250 when its really 200, who cares. as long as you know its +50, you can account for it. 

i don think the thermopens go hot enough. remember 200C is almost 400F! Do you have multimeter with thermocouple temp option? the thermo couples are ~$10.  a dedicated thermometer is handy when you need it-  

this was the 1st cheapest thing that came up - [https://www.amazon.com/Tigervivi-Digital-Industrial-Thermometer-Thermocouple/dp/B01NBH62UO/ref=sr_1_18?ie=UTF8&qid=1534954603&sr=8-18&keywords=thermocouple+thermometer](https://www.amazon.com/Tigervivi-Digital-Industrial-Thermometer-Thermocouple/dp/B01NBH62UO/ref=sr_1_18?ie=UTF8&qid=1534954603&sr=8-18&keywords=thermocouple+thermometer)

there is also old school temp-crayon from welding supply house-[http://www.markal.com/temperature-indicators/thermomelt/](http://www.markal.com/temperature-indicators/thermomelt/)

but that is pretty much go/nogo at the requisite temp, but i offer it just in case.   


---
**Dennis P** *August 22, 2018 16:46*

I use the same M3 threaded barrel style NTC3960 thermistor in my E3D knock off and it is within a couple of degrees. I use the same one in all my printers now for consistency. 

[https://gulfcoast-robotics.com/products/2017-version-hex-screw-in-m3-ntc3950-100k-thermistor-3d-printer-extruder-hotend](https://gulfcoast-robotics.com/products/2017-version-hex-screw-in-m3-ntc3950-100k-thermistor-3d-printer-extruder-hotend) 


---
**Brandon Cramer** *August 22, 2018 22:42*

**+Eric Lien** I guess you can call it the stock one. Which Bondtech model would you recommend. Seems like there are quite a few different models. 


---
**Eric Lien** *August 23, 2018 02:01*

I think either the QR, or the BMG would be good. But if you are just pushing out the Bowden tube from the extruder nut trap on the "Hercustruder"... perhaps an upgrade to the nut trap on the extruder is all that's required. I know someone replaced my nut trap design with a pnuematic fitting adapter that bolts in it's place. I can't seem to find the design now. But modeling a new top peice that incorporates a proper 4mm barbed pnuematic fitting on top might be all that's required.


---
**Dennis P** *August 23, 2018 02:07*

**+Eric Lien** I just drilled out the nut trap and ran a tap into the hole for the bowden fitting


---
**Brandon Cramer** *August 23, 2018 02:07*

It’s pushing the tube out the top of the E3D V6. I have the nut trap on the Hercustruder.


---
**Brandon Cramer** *August 23, 2018 02:16*

I may have to try out the Bondtech BMG. Looks affordable and great performance. 


---
**Dennis P** *August 23, 2018 02:39*

**+Brandon Cramer** I made a Saint Flint Extruder using a Mk10 drive gear and have been really happy with it. Initially, the gear that I used did not have sharp enough teeth so I didn't work well. But once I got a new gear, it can really move filament! It was designed by list member +Michael Memeteau and a bunch of the quad rap folks like it. It might not be as nice as a Bondtech, and I have great respect for theBondtech folks, but the SaintFlint extruder is a step above the single sided gear ones. 

[thingiverse.com - Saintflint Extruder by mmemetea](https://www.thingiverse.com/thing:979113) 


---
**Eric Lien** *August 23, 2018 02:44*

Yeah, but if the failure happens on the e3d hotend side try cutting the teflon bowden tube back a tiny amount to give it a clean end. And use a bowden fitting clip to keep tension on the fitting once clamped) The clip keeps the fitting from getting slack on the tube, hence keeping the barbs from loosening on the tube.



[https://www.thingiverse.com/thing:2798864](https://www.thingiverse.com/thing:2798864)



Or 



[https://www.thingiverse.com/thing:2478545](https://www.thingiverse.com/thing:2478545)




---
**Eric Lien** *August 23, 2018 02:47*

Glad to know my old Hercustruder design is still winning the battle against the hotend. If its pushing out the fitting on the hotend... Then its not a slipping extruder issue. So the extruder is not the first item to correct. Once we get the tube staying in the hotend, then we tackle the next thing to fail.


---
**Dennis P** *August 23, 2018 19:38*

Here is my model of the Hercstruder nut trap adapted to 1/8"-BSPP threads to mate up to the threaded E3D Bowden fittings. I can remodel it with M5 or M6 if you need.  



EDIT: I print this with 0.1 or 0.15 layer heights, 3 perimeter loops and SLOW. takes about 1/2 hour+/1, No supports.

  

HERC_bowden-1_8_BSPP.stl

and here is the OPenSCAD source

[https://drive.google.com/open?id=1ZOODqsA9969_dr1XeMHZ9CL2doNqYCyg](https://drive.google.com/open?id=1ZOODqsA9969_dr1XeMHZ9CL2doNqYCyg)

[drive.google.com - HERC_bowden-1_8_BSPP.stl](https://drive.google.com/open?id=1KKQ8ntoBsGB7gD2SwEuC28s8tBcCw7Rd)


---
**Stefano Pagani (Stef_FPV)** *August 27, 2018 23:54*

I had a saint flint then I went to a bondtech. Awesome upgrade it’s really the gearing the saintflint is a great design though


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/6cerzKkE1q8) &mdash; content and formatting may not be reliable*
