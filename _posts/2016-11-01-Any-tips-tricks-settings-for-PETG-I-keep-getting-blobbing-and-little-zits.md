---
layout: post
title: "Any tips/tricks/settings for PETG? I keep getting blobbing and little zits"
date: November 01, 2016 02:22
category: "Discussion"
author: Sean B
---
Any tips/tricks/settings for PETG?  I keep getting blobbing and little zits. I know I need to fiddle with the retract and maybe wipe coast.  Does anyone have suggestions?



I am using Walter's carriage with no blower fan, and E3d V6.



I am running mmemetea's Saint Flint Extruder with an MK7 drive. [https://www.youmagine.com/designs/saintflint-extruder](https://www.youmagine.com/designs/saintflint-extruder)   This thing rocks and seems to be quite capable of driving filament without grinding.





**Sean B**

---
---
**Greg Nutt** *November 01, 2016 04:29*

I think you need to under extrude a little with PETG.  I think I under extruded at 0.9. 


---
**Stephanie A** *November 01, 2016 05:06*

It oozes. Fast and long retraction helps.


---
**Daniel Seiler** *November 01, 2016 06:21*

Second the under extrusion. For me, slow retraction (like 15-20 mm/s) with eSUN PETG gave significant improvements in print quality.  Saint Flint Extruder FTW!


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/XQyTY6m5EeL) &mdash; content and formatting may not be reliable*
