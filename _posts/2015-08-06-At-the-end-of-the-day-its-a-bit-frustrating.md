---
layout: post
title: "At the end of the day it's a bit frustrating..."
date: August 06, 2015 17:12
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
At the end of the day it's a bit frustrating... well maybe more than a bit. But: I'm getting closer to the cause. The PETG first printed well in continues extrusion (that vase i posted) but then it failed again with the smaller nozzle while printing the little Marvin. This brought me to investigate a bit more in combination with retraction. I changed filaments as i didn't trust the PETG to a cheap PLA that i already have for quite a while. It wasn't that complicated to print so far though i hadn't printed it for quite some while.



I had the same stuff happening quite quickly. I got the print stopped before the bondtech killed the filament feeding due to its raw power. I tried pushing the filament through the E3D by hand (without the bowden tube of course) and nearly had no chance to push it through. When i pulled it out it looked like one of the pieces on the photo. I dismounted the E3D to check/clean it.



It wasn't really dirty but i found out that the measures doesn't fit the ones on the drawings from the website. The heatbrake normally should have an inner diameter of 2mm on the 1,75mm V6. Mine has exactly 2mm on the inside of the heatbreak just below that 4,2mm part where the tube comes in but the in the lower part it has around 2.15mm. I don't know if that is the reason but when mounted this part directly connects to the nozzle which again is 2mm.



For my understanding this could be the problem as there's a room right above the nozzle in the "hot zone" that is wider than the rest. I'm not too much of an expert when it comes to extruders but i think this maybe the reason for filament to clog. Looks like this does never happen on continous extrusion but it happens especially when retracting a lot (like on pieces with lots of smaller parts).



Does anyone have an idea? If you have a spare heatbreak lying around - how do yours look? I have a second one that has even 2,2mm on the bottom side. Maybe **+Sanjay Mortimer** can jump in an leave a word on this? 



Anyway, i have cleaned everything up, put it together again and gave it another try with the handpushing and i could easily reproduce it. On first push i can push through quite easily and everything extrudes well. When pulling back around 3-4mm, letting it sit for a second and then push in again i need about 10 times the force to get it pushed again, after waiting a bit longer its getting even harder. This seems to be related to some filaments but as they are not that exotic and already printed well on other extruders i have a hard time thinking that it's the filaments fault. At the end it's quite usual filament like the Woodfill, eSun PETG, Colorfabb CF20 and that stuff.



After dismounting and mounting everything again and again i think i'm giving up for today.

![images/b3a6940fffd99ed75a9ebb40e2e70b60.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b3a6940fffd99ed75a9ebb40e2e70b60.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**Oliver Seiler** *August 06, 2015 18:06*

Interesting. I haven't got access to my printer at the moment and can't measure the heatbreak, but I can confirm that I hardly ever had issues with continuous prints (vase mode), and mainly during normal prints with lots of retractions. 


---
**Eric Lien** *August 06, 2015 19:21*

The hot zone is by design slightly larger if I recall. **+Sanjay Mortimer**​ did a PowerPoint about it at MRRF if I recall. I would have to do some searching to find it.


---
**Maxim Melcher** *August 06, 2015 19:22*

I think blame is low melting temperature of PETG. E3D has a short transition zone without PTFE - coating (between heater and heat sink). That's the problem when retract.


---
**Sanjay Mortimer** *August 07, 2015 10:23*

The slight deviation in heatbreak geometry is normal and to be expected, it's because of the finishing process we use to get the inside of the heatbreak just right - a small amount of material is removed in the process.



The material is a molten liquid at the point where it moves from heatbreak to nozzle, so that shouldn't pose an issue.



What are your retraction settings like? If you fill in the following form it will give me all the info I need to see what might be going on. [https://docs.google.com/a/e3d-online.com/forms/d/1VN8SXarv6KRbPEBnVJ9yqIGPIxZ-0fo40N4VlTWjytM/viewform?usp=send_form](https://docs.google.com/a/e3d-online.com/forms/d/1VN8SXarv6KRbPEBnVJ9yqIGPIxZ-0fo40N4VlTWjytM/viewform?usp=send_form)



If it's not working, we'll sort it out. We always do!


---
**Frank “Helmi” Helmschrott** *August 07, 2015 10:33*

**+Sanjay Mortimer** thanks for jumping in. Good to hear that the tolerance at the heatbreak is the default one. I've filled out the form (which is <i>really</i> long) and sent it. Additionally to keep people updated here too: I normally retract about 6mm at 50-80mm/s with this extruder. I've tried 4 and 5 too here but that didn't change anything. A while ago i tried shorter retraction but got oozing. 



My plan was when i get back to the shop today to try shorter retraction again and see if reliability will get better then without looking too much for oozing at the moment. 



Apart from that kindly awaiting your answer.


---
**Sanjay Mortimer** *August 07, 2015 10:58*

Retraction is pretty high, and likely your source of issues. You might look at around 3mm on that tube length and tweak from there.



Also your form indicates that your hotend cooling fan is controlled by software, not hardwired to the PSU for always-on cooling. This can be an issue if it's rigged up as a normal "print fan". The feature that turns on the cooling fan when the hotend is above X temperature is pretty good, but a lot of people have the number a bit too high - I would use no more than 40C.



Woodfill and CF20 take a bit more pushing and slightly higher temps than their parent materials, but should work without any issue really. It's possible that there is some residual gunk in there, which woodfill is quite bad for. You might try some cleaning filament or a nylon warm-pull as such: [http://bukobot.com/nozzle-cleaning](http://bukobot.com/nozzle-cleaning) is all else fails.



I know the form is long, but the system is complex! All the questions there relate to something that we've seen cause issues in the past. Thanks for taking the time to fill it in, it really is useful. We collect and aggregate this data and it helps us find common issues, and track how each version and batch is doing in the wild.


---
**Frank “Helmi” Helmschrott** *August 07, 2015 11:03*

Thanks again, **+Sanjay Mortimer** - your support is highly appreciated.



Re: Retraction i will definitely try this today.



Re: Fan. It's hooked up as hotend cooling fan in Repetier and get's fully powered as soon as the hotend heater is on or the temperature is above 50°C. So basically it runs at full speed from the time i switch it on until it cools back down to below 50°C. It's just to keep the machine less noisy when it just sits their powered after a longer print.



Re: Cleaning. I regulary burn the nozzles to clean them as i change filaments quite often. That has prooven to be a good thing on my other printers and hotends too and worked well with a V5 E3D in the past. I also did this with the heat break to be sure that isn't an issue here.



No really worries about the lenght of the form. Good support is always appreciated and i love to take the time to fill such a long form as long as it helps clearing my issues or helping to improve the product.



I'll keep you updated with the results of my tests later today.


---
**Sanjay Mortimer** *August 07, 2015 11:28*

Ach! Don't be burning out nozzles and especially not heatbreaks to try and clean them.



Most plastics, especially PLA have some pretty gnarly degradation products. The decomposed material can form some really nasty thin coating deposits on the inside of the heatbreak that greatly increase adhesion to the walls.



Definitely consider some cleaning filament as an alternative. I was surprised when I tested it to find that it's not actually a gimmick and really does remove a bunch of crud that even a nylon warm pull won't get out.


---
**Frank “Helmi” Helmschrott** *August 07, 2015 13:03*

Hmm that's interesting. Like i said my experience so far was quite good with burning. I will definitely test that cleaning filament and also the nylon warm pull (have some nylon here). I also thought that this cleaning filament stuff is just a gimmick.


---
**Eric Lien** *August 07, 2015 13:47*

I use nylon pulls between every material type switch(ABS-to-PLA, PLA-to-PETG, Etc). I find it is the best way to keep it "CLEAN" without a burn out. It is amazing the stuff you will see come out of a "already cleaned" hot end when you do a nylon pull. 


---
**Frank “Helmi” Helmschrott** *August 07, 2015 13:58*

**+Eric Lien** **+Sanjay Mortimer**  how warm do you do that pull? by hand? any special tips?


---
**Eric Lien** *August 07, 2015 14:12*

I load the nylon heat up to normal Nylon temps, extrude a few mm, then quickly drop to 110C. Also while the temp drops I push down slightly on the nylon by hand to keep the heatbreak and nozzle full. Once it levels off at 110C do the pull. 



Rinse and repeat until clean.


---
**Frank “Helmi” Helmschrott** *August 07, 2015 18:53*

As it looks now the smaller retract has solved the biggest issues. At least the eSun PETG prints now without blockage. I have given up on the cheap PLA for the moment and will continue to try other Filaments. Thanks so far **+Sanjay Mortimer** for helping on this. 3mm still is not enough retraction on for example the Colorfabb PLA but i think it'll work on ABS and other stuff.



**+Eric Lien** ineed impressive what the nylon pull brought out from a freshly "cleaned" nozzle.



The eSun PETG still doesn't print well on smaller extrusion parts and is problematic with retracts there. I'll open a new thread with a pic showing the problems. I think it maybe related to this special filament but maybe it's just a sign that things are still not perfect.


---
**Eric Lien** *August 07, 2015 19:44*

**+Frank Helmschrott** its odd you cannot run longer retracts. I retract 6.75mm @ 80mm/s on my Bowden for PETG at 255C and an E3D V6 with zero issues. But then again my Bowden is 950mm, so I really need it. Also the actual functional retract is much lower at the hot end. I bet at least 4 of that retract is just taking up slack in the Bowden system.


---
**Frank “Helmi” Helmschrott** *August 07, 2015 19:46*

**+Eric Lien** does this refer to the eSun PETG? The thing is i haven't seen all this problems before with 6mm retract. This only seems to happen to some filaments. I'm just trying to fiddle that out so it won't be a big problem setting different printing profiles for these filaments.


---
**Eric Lien** *August 07, 2015 20:10*

**+Frank Helmschrott** Yes this is Esun PETG, I have used the Magenta, Blue, Clear, and Black all with similar settings.


---
**Frank “Helmi” Helmschrott** *September 28, 2015 16:09*

Time for an update after quite some testing. I didn't want to post that too early but after printing quite some hours since solving this problem i can easily say it is solved. 



It looks like the problem has been a faulty heat break on the E3D hotend i was using. Luckily enough i'm a big fan of E3D hotends and so had another one in my shop - changed the heat breaks and didn't see this problem coming up ever again.



**+Sanjay Mortimer** ever seen this? Anything i could do to investigate or repair the - what it seems - faulty heat break? Any way i could use this again?



Apart from that thanks to anyone trying to help - that really cost me some hours, nerves and filament - luckily it is solved now and printing reliability has improved a lot since then.


---
**Sanjay Mortimer** *September 28, 2015 17:10*

**+Frank Helmschrott** I'd love to get that bad heatbreak back from you to analyse, and of course send you a new one along with some extra free bits for your trouble.



If you could get in touch through the support email system, or the contact form on our website we'll sort everything out for you.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/UaesHiWTYdP) &mdash; content and formatting may not be reliable*
