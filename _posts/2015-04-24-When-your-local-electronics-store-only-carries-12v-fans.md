---
layout: post
title: "When your local electronics store only carries 12v fans..."
date: April 24, 2015 03:57
category: "Show and Tell"
author: Derek Schuetz
---
When your local electronics store only carries 12v fans...

![images/30aa6b9b7cf3d3cfcd9362df100c00de.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/30aa6b9b7cf3d3cfcd9362df100c00de.jpeg)



**Derek Schuetz**

---
---
**Oliver Seiler** *April 24, 2015 04:39*

I've done the very same thing, but for a much larger fan pointed at my driver ics. I've got a 7812  cable tied into the path of the fan. 


---
**Miguel Sánchez** *April 24, 2015 06:17*

two of them in series seems another possible choice


---
**Isaac Arciaga** *April 24, 2015 06:51*

Yep, 2 wired in series would work fine while waiting for 24v fans. **+SMW3D**, Amazon and Makers Tool Works has'em


---
**Jeff DeMaagd** *April 24, 2015 12:48*

That's legit, but be careful touching the regulator when using it with some fans. Putting the heat sink in the air flow probably helps though,



I want to draw up a board that does better, it would have a switching converter plus FET gates so the controller can PWM the reduced voltage.﻿


---
**Whosa whatsis** *April 24, 2015 13:30*

If you're going to try two fans wired in serial, make sure that their identical. They won't get 12V each if they have different resistances.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/WgdizmhVH4K) &mdash; content and formatting may not be reliable*
