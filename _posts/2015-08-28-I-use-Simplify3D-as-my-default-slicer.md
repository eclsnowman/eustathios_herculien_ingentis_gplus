---
layout: post
title: "I use Simplify3D as my default slicer"
date: August 28, 2015 20:47
category: "Show and Tell"
author: Eric Lien
---
I use Simplify3D as my default slicer. But for the Cura users out there I made an STL of the bed for Eustathios. It can be used in Cura similar to the way the Ultimaker bed displays when you select that as your printer. To be honest it is just a waste of graphics resources... but it looks cool. 



You define to use the STL inside the:

C:\Program Files (x86)\Cura_15.06.03\resources\settings\"YOURPRINTER".json



Here is the STL: [https://drive.google.com/file/d/0B1rU7sHY9d8qZmpvVHF4WnFXaDg/view?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qZmpvVHF4WnFXaDg/view?usp=sharing)



![images/90356e3566a164004cf257bec4ecec8c.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/90356e3566a164004cf257bec4ecec8c.png)
![images/000af84bb352cf0de841e3115622192e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/000af84bb352cf0de841e3115622192e.png)
![images/7ef393549cb4897558d7d4ee6c2bb403.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7ef393549cb4897558d7d4ee6c2bb403.png)

**Eric Lien**

---
---
**Eric Lien** *August 28, 2015 22:52*

I should also mention you put the stl file in the folder called:



C:\Program Files (x86)\Cura_15.06.03\resources\meshes\



... I think... I am away from my computer﻿


---
**Joe Spanier** *August 29, 2015 00:17*

You use it in simplify too?


---
**Eric Lien** *August 29, 2015 01:39*

**+Joe Spanier** I didn't know you could in simplify3d.


---
**Joe Spanier** *August 29, 2015 03:07*

In the new 3 you can in you machine definition. I have a lulzbot bed in mine


---
**Jeff DeMaagd** *August 29, 2015 13:48*

I think Simplify has a place to put a machine STL but I haven't tried yet.﻿ it's in the machine manual configuration panel.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/SR1KAyuMgyP) &mdash; content and formatting may not be reliable*
