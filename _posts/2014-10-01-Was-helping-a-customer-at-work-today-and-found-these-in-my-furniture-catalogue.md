---
layout: post
title: "Was helping a customer at work today and found these in my furniture catalogue"
date: October 01, 2014 04:53
category: "Show and Tell"
author: D Rob
---
Was helping a customer at work today and found these in my furniture catalogue. They are 16inch³ outside measurements 406.4mm³. I figured there may be some interest. I can get them, for those who would like, in the 5 colors shown for $60 US plus shipping. You can drill holes you need and modify as needed. It even has a base plate already there. Just add an electronics cabinet underneath and your hardware and electronics and away you go. Easy peasy. I'm taking to you **+Eric Lien**, **+Shauki Bagdadi**, **+Mike Miller**, **+Nuker Bot**, **+ThantiK**, and anyone else looking for quick cheap frames.

![images/4cfb5dea28a600030a3cefb88cf0a27e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4cfb5dea28a600030a3cefb88cf0a27e.png)



**D Rob**

---
---
**Eric Lien** *October 01, 2014 06:20*

I cannot tell from the pictures. It looks a little thin?


---
**D Rob** *October 01, 2014 06:23*

These make stacking easy. If you'll notice the top protrudes and the base of the one on top fits snuggly around it. It appears to be held together by 16 screws. The uprights are angle but the top and bottom look to be square tubing. I will probably bring some in on the next order. It's good to be manager... Until people start bitching that is ;)


---
**D Rob** *October 01, 2014 06:29*

**+Eric Lien** After I bring in some at least one. I'll do a video. But with a few holes drilled and panels bolted on. This will stiffen right up any way. Base square with base plate, and upper square grand will be rigid. And panels should handle the rest. Leave as is add panels and mount electronics on back panel and I see a print farm being easy to manage space wise.


---
**D Rob** *October 01, 2014 06:29*

The thickness brings solid doodle to mind.


---
**Nuker Bot (NukerBot 3D Printing)** *October 01, 2014 06:44*

I need to make a print farm


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/MhaYDmsWshR) &mdash; content and formatting may not be reliable*
