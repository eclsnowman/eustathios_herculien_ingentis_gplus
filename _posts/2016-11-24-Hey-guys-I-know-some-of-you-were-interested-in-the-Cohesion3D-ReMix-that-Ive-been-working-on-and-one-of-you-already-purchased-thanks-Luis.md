---
layout: post
title: "Hey guys. I know some of you were interested in the Cohesion3D ReMix that I've been working on (and one of you already purchased, thanks Luis)"
date: November 24, 2016 03:37
category: "Discussion"
author: Ray Kholodovsky (Cohesion3D)
---
Hey guys.  I know some of you were interested in the Cohesion3D ReMix that I've been working on (and one of you already purchased, thanks Luis).



It's a 32 bit board that runs Smoothie and has 6 stepper drivers, a 20 amp heated bed mosfet,  direct connections for the RepRapDiscount Smart Controller, and protected endstops for running inductive sensors at 12/24v.  I made this because of the growing popularity of multi-extrusion and the triple input capability of something like the Diamond hotend, yet there didn't seem to be any available 32 bit boards that did 6 drivers natively.  Plus, the motion performance of Smoothie is awesome. I hope that you builders find this a good fit for your machines and check it out.  I'd love to see some more big printers with multiple heads out there.  

On my site [cohesion3d.com](http://cohesion3d.com) you'll find the ReMix available for sale, and also the Mini which is a 4 driver toned down version of this for a single extruder printer for pre-order.  **+Eric Lien** has been testing it in his Delta with success ([https://plus.google.com/u/0/+EricLiensMind/posts/6f1LmCBtJx4?sfc=true](https://plus.google.com/u/0/+EricLiensMind/posts/6f1LmCBtJx4?sfc=true)), and he's been working extremely hard to help me with CAD Models of the boards, mechanical diagrams, case designs, and the list goes on.  

Thanks for the support and happy holidays to all!



![images/aeac17bdb896835bb4e9afaf07a62213.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/aeac17bdb896835bb4e9afaf07a62213.jpeg)
![images/5a827bfe86c1cc0b32ff95d4c0a4dff5.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5a827bfe86c1cc0b32ff95d4c0a4dff5.png)

**Ray Kholodovsky (Cohesion3D)**

---


---
*Imported from [Google+](https://plus.google.com/+RayKholodovsky/posts/UdqcVDbVpFs) &mdash; content and formatting may not be reliable*
