---
layout: post
title: "I have started to receive material for my Herculien build"
date: February 27, 2015 18:03
category: "Discussion"
author: Mikael Sjöberg
---
I have started to receive material for my Herculien build. The mdf insulator board was CNC:ed yesterday . The Aluminium frame , Alu healing bed, stepper engines, and some other funny stuffs is ordered . I know there has been a discussion about the cost for the frame and that its probably  overengineered . This is My first 3Dprinter  and I have choosen  to make it very similar to Erics basis design . I do like overengineered stuff :)

I choosed to use 2 pcs of 20x40 Alu frame instead of 1 pcs 20x80 since the cost difference was very big from the supplier. For me the Alu frame cost was the half by doing this . 

This is really a Fantastic forum and its very inspring to see all cool progress and home built solutions !







![images/3ed74b66642b1b11d78f520302ef73fe.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3ed74b66642b1b11d78f520302ef73fe.jpeg)
![images/384de84bf2adfcd8260f920cccb2b48f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/384de84bf2adfcd8260f920cccb2b48f.jpeg)

**Mikael Sjöberg**

---
---
**Dat Chu** *February 27, 2015 18:53*

Awesome build. Great to see your share. I was trying to find the dimension of the mdf insulator board yesterday but couldn't find it. Can you give me some hints please?


---
**Mikael Sjöberg** *February 27, 2015 19:58*

I haven't checked it yet but the outer dimensions are 454x464 (mm)


---
**Eric Lien** *February 27, 2015 21:18*

**+Dat Chu** if you look in the drawings folder on github it should be there.


---
**Eric Lien** *February 27, 2015 21:25*

[https://github.com/eclsnowman/HercuLien/blob/master/Drawings/MDF_Insulator_Board.PDF](https://github.com/eclsnowman/HercuLien/blob/master/Drawings/MDF_Insulator_Board.PDF)


---
**Dat Chu** *February 27, 2015 21:37*

I must have had a stupid moment last night. Too tired when I was browsing through :(. Thank you **+Eric Lien** and **+Mikael Sjöberg** .


---
**Eric Lien** *February 27, 2015 23:21*

Also this might help: [https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Heated_Bed_Assembly.PDF?raw=true](https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Heated_Bed_Assembly.PDF?raw=true)


---
**Dat Chu** *February 28, 2015 00:45*

Awesome. Thanks **+Eric Lien** . Now let me find a way to export this to dxf something that can be used with Cambam.


---
*Imported from [Google+](https://plus.google.com/118217975155696261814/posts/RH2VLYZajow) &mdash; content and formatting may not be reliable*
