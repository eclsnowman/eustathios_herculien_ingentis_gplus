---
layout: post
title: "Is there any reasonable way to independently control more then two fans from an Azteeg X5 mini v3?"
date: February 26, 2017 01:29
category: "Discussion"
author: Matthew Kelch
---
Is there any reasonable way to independently control more then two fans from an Azteeg X5 mini v3?





**Matthew Kelch**

---
---
**jerryflyguy** *February 26, 2017 01:55*

There is a couple more I/o on the unused plug beside the VIKI plug, but you'd have to use them via a relay or something. Don't think they'd drive a fan directly. Not sure of the porting on those pins either, although I think I've seen a diagram somewhere. 



Roy has been very helpful to me when I had questions. Shoot him an email, I'm sure he'd be able to help!


---
*Imported from [Google+](https://plus.google.com/+MatthewKelch/posts/eoYDwcjdSa8) &mdash; content and formatting may not be reliable*
