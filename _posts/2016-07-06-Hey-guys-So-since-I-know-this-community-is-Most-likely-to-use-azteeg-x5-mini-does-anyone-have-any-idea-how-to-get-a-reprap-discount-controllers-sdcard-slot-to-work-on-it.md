---
layout: post
title: "Hey guys So since I know this community is Most likely to use azteeg x5 mini does anyone have any idea how to get a reprap discount controllers sdcard slot to work on it"
date: July 06, 2016 02:16
category: "Discussion"
author: Derek Schuetz
---
Hey guys



So since I know this community is Most likely to use azteeg x5 mini does anyone have any idea how to get a reprap discount controllers sdcard slot to work on it. I have the display fully working and it recognizes when I inserted a card but it never shows any data on he card just blank





**Derek Schuetz**

---
---
**Ted Huntington** *July 06, 2016 03:36*

I don't know about azteeg but you can use RAMPS with that LCD controller. For sure you are using a .gcode format with a .gcode file extension?


---
**Derek Schuetz** *July 06, 2016 03:51*

**+Ted Huntington** ya it worked on my ramps board fine


---
**Matt Wils** *July 06, 2016 03:54*

Check with the folks @ Smoothieware. 


---
**Derek Schuetz** *July 06, 2016 04:00*

Already tried that still waiting on a reply he just linked me to the smoothie guide which tells me nothing


---
**Jeff DeMaagd** *July 06, 2016 04:13*

I seem to recall them saying once that using the smart controller's slot would conflict with the on-board slot. You might have to settle for one or the other.


---
**Maxime Favre** *July 06, 2016 04:19*

YOu have the SD pins lines in your config and are they wired in the right place ? 

There's a guide for the x5 mini 1.1. You don't need the adapter but this page  shows your where is what: [http://smoothieware.org/panel#toc8](http://smoothieware.org/panel#toc8)




---
**Derek Schuetz** *July 06, 2016 04:38*

**+Jeff DeMaagd** f I'm what I can tell it works Roy from panucatt sent me something also but I can't decipher it.


---
**Derek Schuetz** *July 06, 2016 04:38*

**+Maxime Favre** I can't find a breakdown of the adapter if I could then I could just follow the pins like I did for the 1.0 guide but the 1.0 guide leaves out the SD card


---
**Matt Wils** *July 06, 2016 04:50*

I'll send some pix and my config setting once I get home. That's if someone doesn't provide it by then. 


---
**Matt Wils** *July 06, 2016 04:52*

Are you using a GLCD shield?


---
**Matt Wils** *July 06, 2016 04:54*

Also, how did you format you SD card?


---
**Derek Schuetz** *July 06, 2016 04:54*

**+Matt Wils** No shield Just wires to pins. There's 2 pins I can't use that I'm using for my laser pwm control and laser fire


---
**Derek Schuetz** *July 06, 2016 04:54*

**+Matt Wils** format? I just am using the same card from my ramps that worked with the same fikes


---
**Matt Wils** *July 06, 2016 05:03*

Are you on a MAC/PC/Linux? Format your SD to Fat32


---
**Derek Schuetz** *July 06, 2016 05:04*

**+Matt Wils** PC and I have never heard of Fat32


---
**Matt Wils** *July 06, 2016 05:07*

Which 2 Pins are you using for PWM and Fire? Example 2.4 etc. Check your config or post your config setting on this forum. 


---
**Derek Schuetz** *July 06, 2016 05:08*

[https://www.dropbox.com/s/hhms32qixbwpc0g/config.txt?dl=0](https://www.dropbox.com/s/hhms32qixbwpc0g/config.txt?dl=0)


---
**Derek Schuetz** *July 06, 2016 05:11*

**+Matt Wils** I could also not have all the wires I need hooked up from the LCD I tried everyone to a pin and nothing worked


---
**Matt Wils** *July 06, 2016 05:39*

Have you test fired your setup?


---
**Matt Wils** *July 06, 2016 05:41*

When you relocate pin/wire your config needs to be matched. 


---
**Derek Schuetz** *July 06, 2016 05:41*

Everything works fine except as card reader


---
**Matt Wils** *July 06, 2016 05:42*

Please post pix of wire setup. 


---
**Matt Wils** *July 06, 2016 05:46*

Ok, place SD in you PC and right click, select format. Choose Fat32. Hit enter. 


---
**Derek Schuetz** *July 06, 2016 05:52*

**+Matt Wils** np change. dont have wires going to the pins SD_Mosi, SD_Miso, and SD_SCK because i cant find anytnign in the config file about them so maybe thats my issue


---
**Derek Schuetz** *July 06, 2016 06:04*

**+Matt Wils** okni hooked up all wires as shown in the image 

but i have no idea what a spi channel is



[https://imgur.com/a/R3Ex6](https://imgur.com/a/R3Ex6)


---
**Derek Schuetz** *July 06, 2016 06:13*

**+Matt Wils** check pic for pin designation for the display


---
**Matt Wils** *July 06, 2016 06:39*

I'll get back with you.  I Should be home in 2hr. 


---
**Derek Schuetz** *July 06, 2016 13:21*

**+Matt Wils** ok


---
**Matt Wils** *July 06, 2016 14:33*

# Panel See [[smoothieware.org](http://smoothieware.org)]

panel.enable                                 true              # set to true to enable the panel code



# Example for reprap discount GLCD

# on glcd EXP1 is to left and EXP2 is to right, pin 1 is bottom left, pin 2 is top left etc.

# +5v is EXP1 pin 10, Gnd is EXP1 pin 9



panel.lcd                                   reprap_discount_glcd 	       #

panel.spi_channel                           0                 # OK spi channel to use  ; GLCD EXP1 Pins 3,5 (MOSI, SCLK)

panel.spi_cs_pin                            SSEL1             # OK spi chip select     ; GLCD EXP1 Pin 4

panel.encoder_a_pin                         1.27!^            # OK encoder pin         ; GLCD EXP2 Pin 3

panel.encoder_b_pin                         1.25!^            # OK encoder pin         ; GLCD EXP2 Pin 5

panel.click_button_pin                      3.26!^            # OK click button        ; GLCD EXP1 Pin 2

panel.buzz_pin                              1.30              # OK pin for buzzer      ; GLCD EXP1 Pin 1

#panel.button_pause_pin                      1.23!^            # kill/pause          ; GLCD EXP2 Pin 8 either

#panel.back_button_pin                       1.23!^            # back button         ; GLCD EXP2 Pin 8 or

kill_button_enable                          true              # OK set to true to enable a kill button

kill_button_pin                             1.23^             # OK kill button pin. default is same as pause button 2.12 (Add ^ for external buttons)



panel.contrast                               18               # OK override contrast setting (default is 18)

panel.encoder_resolution                     2                # OK override number of clicks to move 1 item (default is 2)

panel.menu_offset                            1                # OK here controls how sensitive the menu is. some panels will need 1



panel.alpha_jog_feedrate                     6000             # x jogging feedrate in mm/min

panel.beta_jog_feedrate                      6000             # y jogging feedrate in mm/min

panel.gamma_jog_feedrate                     3000             # z jogging feedrate in mm/min



panel.hotend_temperature                     242              # temp to set hotend when preheat is selected

panel.bed_temperature                        100              # temp to set bed when preheat is selected



# setup for external sd card on the RRD GLCD which shares the onboard sdcard SPI port

panel.external_sd                            false            # set to true if there is an extrernal sdcard on the panel

panel.external_sd.spi_channel                1                # set spi channel the sdcard is on

panel.external_sd.spi_cs_pin                 0.28             # set spi chip select for the sdcard (or any spare pin)

panel.external_sd.sdcd_pin                   0.27!^           # sd detect signal (set to nc if no sdcard detect) (or any spare pin)


---
**Matt Wils** *July 06, 2016 14:35*

This is what  I utilized/followed [http://forums.reprap.org/read.php?13,486023](http://forums.reprap.org/read.php?13,486023)

Also dont forget to set panel.external_sd true


---
**Matt Wils** *July 06, 2016 14:38*

There are many variable/options, so there isn't a wrong or right way.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/44SmcaJaJ3N) &mdash; content and formatting may not be reliable*
