---
layout: post
title: "Hi, Anyone knows where I could get 2020 extrusion in Canada ?"
date: October 24, 2014 13:30
category: "Discussion"
author: Jean-Francois Couture
---
Hi,



Anyone knows where I could get 2020 extrusion in Canada ? looks like its pretty hard to find.. 



Thanks !





**Jean-Francois Couture**

---
---
**Aaron Kartash** *October 24, 2014 13:32*

Last time I looked I had a hard time finding any that sold in small quantity's, You're best bet is to order from China.


---
**Denis Seguin** *October 24, 2014 13:32*

It possible to buy in Mirabel, quebec

[http://www.faztek.ca/fr/](http://www.faztek.ca/fr/)


---
**Denis Seguin** *October 24, 2014 13:38*

It have 2 another compagnie

[http://rptmotion.com/](http://rptmotion.com/)

[http://dicsa.biz/index_fr.htm](http://dicsa.biz/index_fr.htm)


---
**Jean-Francois Couture** *October 24, 2014 13:56*

**+Aaron Kartash** Yeah.. 10 meters would cost a arm and a leg ordering from China I would bet.


---
**Jean-Francois Couture** *October 24, 2014 14:02*

**+Denis Seguin** Thanks, there is not much information on there website, I'll have to contact them. Anyone of you ordered som 2020 to make 3D printers ? (and cost)


---
**Aaron Kartash** *October 24, 2014 14:14*

**+Jean-Francois Couture** aslong as you're not ordering really long lengths it's not that bad. Let us know what it costs from that company.


---
**Shachar Weis** *October 27, 2014 13:59*

Hello fellow Canadian! If you do manage to source 2020 locally, it would be great if you could share where and how much.



Thanks.


---
**Jean-Francois Couture** *October 27, 2014 14:23*

I did not call yet the places mentioned above. I was approached by a fellow repraper that lives in the US so we could look at the shipping..



turns out that on a average, it would cost arount US20$/1meter to send. (based on a 7 lbs box from USPS, contaning  7 x 1 meter segments). Not counting duties and money conversions.



Too bad because he has some great stuff on his website at great prices.



Another place would be Misumi. but I guess it would be around the same price.. except, they pay for duty imports.. so that's a plus


---
**Denis Seguin** *October 28, 2014 01:01*

Jean-Francois Couture

Email to contact,

sales@faztek.ca

dpolis@dicsa.biz

ventes@rptmotion.com


---
**William Frick** *October 28, 2014 15:00*

I recall open beam had links to a canadian hobby shop that sold that variety of extrusion and accessories including nema 17 motor mounting plates.


---
**Jean-Francois Couture** *October 28, 2014 17:26*

**+William Frick** I think you're right. This is what I found (did a quick search) [https://solarbotics.com/product/52600/](https://solarbotics.com/product/52600/) they are based in Alberta. 10$CAD / Meter


---
**Jean-Francois Couture** *October 28, 2014 17:29*

spoke to soon, these look to be 15/15 not 20/20 extrusions


---
**Denis Seguin** *October 28, 2014 19:31*

1010 extrusion (1" x 1")

Is 32$ for 8feet at Faztek 



If you some feet I'm live in Laval 


---
**Jean-Francois Couture** *October 28, 2014 19:48*

Thanks,



I guess they dont have 2020 extrusion ?



I was goin to write to them.

i've already written to rpmotion. waiting for a feedback.


---
**Denis Seguin** *October 29, 2014 04:16*

I think Yes, is 2in x 2in, on faztek

I buy conner bracket for 2020 but I don't ask the price.



I buy, 1010 and 1020



rpmotion is more expensive



Regards


---
**Aaron Kartash** *October 29, 2014 04:32*

$132us for 10m 2020 in 1m lengths [http://www.aliexpress.com/item/Aluminum-Profile-Aluminum-Extrusion-Profile-2020-20-series-20-20-length-1-meter-1m-L-1000mm/2048229294.html](http://www.aliexpress.com/item/Aluminum-Profile-Aluminum-Extrusion-Profile-2020-20-series-20-20-length-1-meter-1m-L-1000mm/2048229294.html)


---
**Jean-Francois Couture** *October 29, 2014 11:42*

**+Denis Seguin** ok, When I say 2020, I mean metric, not SAE. 2"x2" is a bit to big to make a standard sized 3D printer :-)


---
**Jean-Francois Couture** *October 29, 2014 11:46*

**+Aaron Kartash** That's actually not that bad.. i'd be curious to see if there are dutie fees on that shipping.. my guess would be yes.


---
**Aaron Kartash** *October 29, 2014 12:09*

It's possible you might get hit with duties, but it's possible that you won't it's kinda a gamble from china, If you select a shipping method that goes through canada post the chances are less, and even if you do get hit it's a hell of a lot cheaper then UPS/Fedex/etc. Canada post charges like $10 fee + taxes UPS takes your house, car, 2 arms and a leg.


---
**Jean-Francois Couture** *October 29, 2014 12:34*

**+Aaron Kartash** There is a way to have UPS and Canada post rates for duties.. people are just not aware of the procedure. I told how to do it to my girlfriend when she imported some goods from the US (a big value) and it worked.


---
**Denis Seguin** *October 29, 2014 22:33*

Jean francois

Make sure when you order  have in mm

Because I buy 1010 on some supply it have 1" x 1" and another is 10mm and 10mm




---
**Shachar Weis** *November 07, 2014 20:01*

Hey all, I found a great source for 2020 in Canada.

This ebay seller: [http://www.ebay.com/usr/electronsurf](http://www.ebay.com/usr/electronsurf)

Contact him and he will cut the needed lengths. He was cheaper than any other quote I got, and the 2020's arrived nice and clean cut.


---
**Jean-Francois Couture** *November 07, 2014 20:46*

**+Shachar Weis** Thanks, I've added to my following list. 


---
*Imported from [Google+](https://plus.google.com/105576148076542448710/posts/iXjE99BvTtx) &mdash; content and formatting may not be reliable*
