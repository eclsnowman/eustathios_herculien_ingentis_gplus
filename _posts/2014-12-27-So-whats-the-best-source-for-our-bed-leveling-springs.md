---
layout: post
title: "So what's the best source for our bed leveling springs?"
date: December 27, 2014 21:20
category: "Discussion"
author: Tony White
---
So what's the best source for our bed leveling springs?





**Tony White**

---
---
**Alex Benyuk** *December 27, 2014 21:30*

this is where I've got mine from, great quality springs [http://www.aliexpress.com/snapshot/6262524155.html?orderId=63972901585482](http://www.aliexpress.com/snapshot/6262524155.html?orderId=63972901585482)


---
**Daniel F** *December 27, 2014 21:49*

Bought 1mm spring steel wire and made them myself with an accu drilling machine. Functional but not as nice as the ones from ali. Advantage of DIY is flexibility, I also made some smaller ones for my extruder from the same wire.


---
**Eric Lien** *December 27, 2014 21:57*

**+Anthony White**​ I bought a hand full from a local surplus store 3cents each. Where are you located, I will send you some.


---
**Miguel Sánchez** *December 27, 2014 22:05*

clothes' pegs are a good source of cheap springs from the dollar shop


---
**Tony White** *December 27, 2014 22:21*

I'm in Los Angeles, but have rigged up a solution with some doubled up weaker compression springs that will work for now!


---
**Eric Lien** *December 27, 2014 22:36*

**+Anthony White** private message me your address and I will drop them in the mail.


---
**James Rivera** *December 27, 2014 23:02*

I found some at the local Ace Hardware store, which makes me think Home Depot or Lowe's might have some, too.



EDIT: Yep. Not as cheap as Eric Lien's offer of free, but if you just want to run to the store to get one now...



[http://www.homedepot.com/p/Unbranded-Zinc-Plated-Compression-Springs-6-Pack-16087/202045468](http://www.homedepot.com/p/Unbranded-Zinc-Plated-Compression-Springs-6-Pack-16087/202045468)


---
**Jim Squirrel** *December 28, 2014 00:25*

i recycled some springs from an old heatsick for a dell desktop


---
*Imported from [Google+](https://plus.google.com/+AnthonyWhiteMechE/posts/2MxRFApemqE) &mdash; content and formatting may not be reliable*
