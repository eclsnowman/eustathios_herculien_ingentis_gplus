---
layout: post
title: "hey guys .. im looking for the printhead on this video"
date: November 16, 2016 23:43
category: "Discussion"
author: Eduardo Ribeiro
---
hey guys .. im looking for the printhead on this video. Does anyone know where to find the stl ????




{% include youtubePlayer.html id=watch %}
[https://www.youtube.com/watch?feature=player_embedded&v=aucE49ZBXx0](https://www.youtube.com/watch?feature=player_embedded&v=aucE49ZBXx0)





**Eduardo Ribeiro**

---
---
**Eric Lien** *November 16, 2016 23:54*

Look at the Ingentis by **+Tim Rastall**​ : [youmagine.com - Ingentis - A Tantillus Variant](https://www.youmagine.com/designs/ingentis-a-tantillus-variant)


---
**Jason Smith (Birds Of Paradise FPV)** *November 17, 2016 00:33*

Here you go, but that carriage is kind of a piece of crap.  I wouldn't trust the guy who designed it :)

[https://github.com/jasonsmit4/Eustathios/commit/932ef91786a7c1657b8da373d78776833d0fcabf](https://github.com/jasonsmit4/Eustathios/commit/932ef91786a7c1657b8da373d78776833d0fcabf)

[github.com - Added STL Files · jasonsmit4/Eustathios@932ef91](https://github.com/jasonsmit4/Eustathios/commit/932ef91786a7c1657b8da373d78776833d0fcabf)


---
**Eduardo Ribeiro** *November 17, 2016 01:06*

Thanks ... I'll take a better look on that Jason 


---
**Eric Lien** *November 17, 2016 02:07*

**+Eduardo Ribeiro** don't listen to **+Jason Smith**​, if it wasn't for the hard work of him and many other talented designers we wouldn't have this amazing community :)



Eduardo: if you want a little inspiration on the many versions and parts available for these type of printers, here are a few resources worth reading over (I am always inspired by all the talented people involved in these family of printers).



1.) Tantillus by Sublime

2.) Tslot Tantillus by Goopyplastic (aka **+Brad Hill**​ who went on to design the LittleRP SLA printer)

3.) Ingentis by **+Tim Rastall**​

4.) Eustathios by **+Jason Smith**​

5.) Eustathios Spider 1, 2, and HercuLien by me



Also many other variants like:



A.) Kitten printer by Patrick ([https://github.com/woolfepr/Printer-Kitten](https://github.com/woolfepr/Printer-Kitten))

B.) ShauLienKi by **+Shauki B**​

C.) Aluminum Eustathios beast by **+Matt Miller**​ (sorry I forget the name of it)

D.) Ingentilere by **+Mike Miller**​

E.) Open18 by **+Joe Spanier**​

F.) Mods and oversized version by **+Ashley Webster**​

G.) Mods by **+Walter Hsiao**​

H.) Mods by **+Bud Hammerton**​

I.) Mods by **+Frank Helmschrott**​



And so many more (sorry if I missed anyone, but typing this from my phone while trying to keep my 3 and 5 year old boys in bed). But a look through the "deviations from the norm" section for inspiration from our unbelievably creative members.



Sorry for the long rant/post, every once and a while I just like to throw out a brief history lesson of all the creative members that have brought us to where we are today. Without all of you this wonderful hobby I fell in love with would have likely been a fleeting one, instead of the constant joy it has become. Thank you all.




---
**Eduardo Ribeiro** *November 17, 2016 22:53*

Actually i'm still not sure what mechanism  on x and y i'll follow but thank you very much for the advices.  


---
*Imported from [Google+](https://plus.google.com/105312930138124598385/posts/87WmhHAnNgy) &mdash; content and formatting may not be reliable*
