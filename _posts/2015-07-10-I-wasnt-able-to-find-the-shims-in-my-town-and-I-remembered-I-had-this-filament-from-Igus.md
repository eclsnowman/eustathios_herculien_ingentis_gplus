---
layout: post
title: "I wasn't able to find the shims in my town and I remembered I had this filament from Igus"
date: July 10, 2015 20:41
category: "Show and Tell"
author: Gunnar Meyers
---
I wasn't able to find the shims in my town and I remembered I had this filament from Igus. It prints pretty easy and moves super smooth on the rods. 



![images/9d6390c4d6d79b5c3673155c991df07e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9d6390c4d6d79b5c3673155c991df07e.jpeg)
![images/7dbaec647e7db0f004bfba4dbff5e8b5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7dbaec647e7db0f004bfba4dbff5e8b5.jpeg)

**Gunnar Meyers**

---
---
**Eric Lien** *July 10, 2015 21:36*

 Nice


---
**Frank “Helmi” Helmschrott** *July 12, 2015 06:55*

great idea - i too have some igus filament samples flying around in the basement. should give this a try.


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/XN1rWfArNER) &mdash; content and formatting may not be reliable*
