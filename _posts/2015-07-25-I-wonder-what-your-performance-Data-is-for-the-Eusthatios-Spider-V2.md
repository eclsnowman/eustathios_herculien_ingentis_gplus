---
layout: post
title: "I wonder what your performance Data is for the Eusthatios Spider V2?"
date: July 25, 2015 14:28
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
I wonder what your performance Data is for the Eusthatios Spider V2? What acceleration values are you using on X and Y and what are your usual printing speeds?



Would be interesting to see how you guys are speeding.





**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *July 25, 2015 14:55*

I am not at my PC, but do you have simplify3d? If so I will send you some of my factory files to check out. If others could post some too that would be very informative.


---
**Frank “Helmi” Helmschrott** *July 25, 2015 15:02*

Always taking them of course, Eric (i have S3D) but would also be great just to compare these simple numbers.


---
**Frank “Helmi” Helmschrott** *August 01, 2015 10:52*

**+Eric Lien** is your offer still valid to send me your s3d factory files?


---
**Eric Lien** *August 02, 2015 11:54*

**+Frank Helmschrott** of course. I forgot. I am at my parents cabin now. But I will send some when I get back. 


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/JTc35ExHwrN) &mdash; content and formatting may not be reliable*
