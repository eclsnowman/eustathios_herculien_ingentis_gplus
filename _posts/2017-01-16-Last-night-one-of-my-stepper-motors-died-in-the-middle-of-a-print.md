---
layout: post
title: "Last night one of my stepper motors died in the middle of a print"
date: January 16, 2017 06:38
category: "Discussion"
author: Oliver Seiler
---
Last night one of my stepper motors died in the middle of a print. The back bearing had gotten completely stuck and when I took it out it was full of brown-ish powder that looked a little like rust, but I think it might have been dried grease.

I replaced the bearing (625Z) with the same from another stepper and it seems to be doing just fine now.

Any idea what might be causing this? They get warm during prints, but not hot. Also I have mounted heat sinks to the back. I've been running them @1.5A

[http://www.robotdigg.com/product/29/Nema17-60mm-1.5A-high-torque-stepper-motor](http://www.robotdigg.com/product/29/Nema17-60mm-1.5A-high-torque-stepper-motor)





**Oliver Seiler**

---
---
**Greg Nutt** *January 16, 2017 11:54*

Probably was rust.  We're all the balls present in the bearing?


---
**Eric Lien** *January 16, 2017 13:13*

You should be able to run lower than 1.5A.



1.5A is the maximum the motor is rated for, but nothing says that much is required. I would slowly start dialing the current back until you start seeing missed steps. Then bump it up above that failure threshold by a safety margin.



I think I am around 1.3 amps or so.


---
**Zane Baird** *January 16, 2017 14:13*

On my herculien I'm currently running the X and Y motors at 0.6 amps. Cuts back on the noise and I haven't noticed any missed steps or ill-effects compared to higher currents




---
**Eric Lien** *January 16, 2017 15:01*

**+Zane Baird** 0.6A per phase, or 0.6A Max Current?



If the motors work all the way down to 0.6A total... then holy cow :)


---
**Zane Baird** *January 16, 2017 16:33*

**+Eric Lien** This is setting the alpha_current and beta_current at 0.6 in the smoothie config. I'm assuming that's the summed current from both phases, but I'm not entirely sure...


---
**Eric Lien** *January 16, 2017 17:48*

Well it looks like I am sending them more current than required. I will play around over the next few days and see how it goes.


---
**Oliver Seiler** *January 16, 2017 20:05*

Ok, I might not need 1.5A, but as long as the motors don't get hot there shouldn't be an impact on the bearing, right?

I don't mind the noise and rather make sure there aren't any missed steps during long prints. I did have some skipped steps after I first finished my build and could probably reduce the current now that everything has been run in nicely.

**+Greg Nutt** I don't think it was rust, as there was just this brown powder but the surfaces of the balls, bearing and internals of the motor were all clean.


---
**Hakan Evirgen** *January 18, 2017 10:51*

AFAIK most printers work with current in the area of 0.6 - 0.9. If you need more current then either you have big printer (thus moving more mass) or a problem (e. g. not enough lubrication etc).


---
**Oliver Seiler** *January 19, 2017 06:07*

Good to know **+Hakan Evirgen**




---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/Snd6zGEmf71) &mdash; content and formatting may not be reliable*
