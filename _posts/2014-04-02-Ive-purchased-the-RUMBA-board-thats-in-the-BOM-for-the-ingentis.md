---
layout: post
title: "I've purchased the RUMBA board that's in the BOM for the ingentis..."
date: April 02, 2014 12:22
category: "Discussion"
author: Jarred Baines
---
I've purchased the RUMBA board that's in the BOM for the ingentis... It says it is "universal voltage" and can take 12-35V input which is regulated to 12V for the fan / lights etc and 5v for the processor etc.



I'm also going to buy 2x E3D hot ends for it. And bought the standard motors from the BOM as well...



Since the RUMBA itself will take 24V and still work, can I hook the E3D hotend up to 24V? How is this done?



Also...



If I buy a 24V HBP and hook it up, is it possible to run the E3D's at 12V using the rumba board (with jumpers or something?) Anyone know a good tutorial / have any advice on running the motors at higher voltage also? From what I understand this is all desirable (higher voltage).



I understand a bit about what I'm doing with a 12V setup, but I'm out of my depth with mixing voltages.



Any help greatly appreciated! I have read about the board on the wiki page but, it doesn't tell me how to do what I want to.





**Jarred Baines**

---
---
**Eric Lien** *April 02, 2014 17:11*

I would like to know also. I have an azteeg x3 that has expansion outputs (LEDs and fans) marked on the wiring diagram as 12v so I assumed that took the 24 input I am using and stepped down to 12v for the LEDs I used... Nope I burnt out the LEDs because it drives them at input voltage... But the 5v works as it should. :(


---
**Martin Bleakley** *April 02, 2014 19:28*

Once thing you should really get is an SSR for the heatbed as the rumbas don't handle over 10-15A though to the heatbed, esp if your using like a 500W alirubber 300mmx300mm heatpad


---
**D Rob** *April 02, 2014 22:17*

Just get 40w 24v heater cartridges for your e3ds


---
**Jarred Baines** *April 03, 2014 11:01*

After my experiences with my reprappro HPB, I will only ever use relays... I have to manually control my heated bed now because I burnt out the connector on the board and am so bad at soldering I can't fix it! (luckily my HBP maxes out at about 100, so, no problem with manual control)



Thanks for the tips blokes!



How about for the motors? If I were to get a 24V HBP and cartridges, then what's involved in using the motors at 24v? any tuning / parameters that need to be set in firmware / on the hardware? or just plug'n'play 24v?


---
**D Rob** *April 03, 2014 15:22*

You'll need to adjust the current on the steppers drivers like any new stepper drivers


---
**D Rob** *April 03, 2014 15:24*

**+Tim Rastall** I don't have a rumba so none of this is first hand could you step in?﻿


---
**Tim Rastall** *April 03, 2014 17:47*

All of the above,  I cooked the mosfet on my first RUMBA and use a relay now for my second with my Alirubber heaterpad. 


---
*Imported from [Google+](https://plus.google.com/+JarredBaines/posts/NBJX4fp7hE8) &mdash; content and formatting may not be reliable*
