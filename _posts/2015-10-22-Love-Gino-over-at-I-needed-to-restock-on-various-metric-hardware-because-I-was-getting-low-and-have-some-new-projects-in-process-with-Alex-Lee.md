---
layout: post
title: "Love Gino over at . I needed to restock on various metric hardware because I was getting low, and have some new projects in process with Alex Lee ."
date: October 22, 2015 21:49
category: "Show and Tell"
author: Eric Lien
---
Love Gino over at [trimcraftaviationrc.com](http://trimcraftaviationrc.com). I needed to restock on various metric hardware because I was getting low, and have some new projects in process with **+Alex Lee**​.  Gino helped me out and added some bolts to the site that I needed but weren't listed, and shipped the next day. They are just over in Wisconsin, so via Spee-Dee delivery I had the parts the following day and only $2.99 flat rate shipping.



Also got some brass thermal set inserts from aliexpress for cheap. I love cheap hardware. Even If I never use it all its nice to know I have it on hand If I get the creative bug and want to start building.



![images/a8f7d2b8901239a9c2b466c477808f25.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a8f7d2b8901239a9c2b466c477808f25.jpeg)
![images/3b09825cd19850574781ab415c27fdc9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3b09825cd19850574781ab415c27fdc9.jpeg)
![images/7d8dcc05d620b2494754e3c2484bbc36.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7d8dcc05d620b2494754e3c2484bbc36.jpeg)
![images/0e37f2457efb47806b1ce0736e2d90cb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0e37f2457efb47806b1ce0736e2d90cb.jpeg)
![images/cf2f99535abd2392acc2a52f1b484747.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cf2f99535abd2392acc2a52f1b484747.jpeg)

**Eric Lien**

---
---
**Igor Kolesnik** *October 22, 2015 22:07*

I was completely amazed with the service at [trimcraftaviationrc.com](http://trimcraftaviationrc.com). Good prices, responsive support. Will definitely buy for my next projects only from Gino. They also added some screw to their site for me. Thanks Gino


---
**Chris Brent** *October 22, 2015 23:29*

Wait, what? A delta? I just built a Kossel (party so I can print a Euth) but now I want to know what a DeltaLien would be like :P


---
**Eric Lien** *October 22, 2015 23:41*

**+Chris Brent** this one is all **+Alex Lee**​. I am not saying much about it until he releases it. But he makes some dang nice printers. His Talos Cartesian, and this Delta uses lots of extrusion and CNC cut aluminum plates. He builds them just how I like them... Overbuilt ;)


---
**Igor Kolesnik** *October 22, 2015 23:43*

Will be waiting for details. Overbuild is always better than underbuild. Better safe than sorry


---
**Alex Lee** *October 23, 2015 00:05*

**+Eric Lien** 100% me besides half the more complicated designs... **+Igor Kolesnik** I've been learning Solidworks with Eric's guidance and I just started working on the delta's modeling, it may take a while since I'm such a newbie at Solidworks, but once I'm done I'll release it at our Talos3D group (shameless plug)


---
**Chris Brent** *October 23, 2015 02:31*

Joining that group now... great just what I need, another printer to build! :P


---
**Frank “Helmi” Helmschrott** *October 23, 2015 05:17*

**+Eric Lien** where did you get the brass inserts at Ali? Any recommendations? Always looking to get resources for that.



Also looking forward to see the Delta you and **+Alex Lee** are building. Will look out for that group instantly :)


---
**Eric Lien** *October 23, 2015 05:38*

**+Frank Helmschrott** sorry it was eBay: [http://pages.ebay.com/link/?nav=item.view&id=281707783298&alt=web](http://pages.ebay.com/link/?nav=item.view&id=281707783298&alt=web) 



[http://pages.ebay.com/link/?nav=item.view&id=271948562112&alt=web](http://pages.ebay.com/link/?nav=item.view&id=271948562112&alt=web) 



My t-nuts which haven't arrived yet was on aliexpress.


---
**James Rivera** *October 23, 2015 22:39*

I've only bought from them once ([trimcraftaviationrc.com](http://trimcraftaviationrc.com)), but it was a good experience and a good price.


---
**Eric Lien** *October 27, 2015 03:33*

Looks like Gino saw the post :)



Contacted me about the thermal inserts. Maybe we can get these one the site too, they are really top notch.


---
**James Rivera** *October 27, 2015 05:25*

Incidentally, I needed some long M3 bolts that the local hardware stores (Home Depot, Loews) do not carry, so I placed an order with them on Sunday.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/2Vdjzs944jv) &mdash; content and formatting may not be reliable*
