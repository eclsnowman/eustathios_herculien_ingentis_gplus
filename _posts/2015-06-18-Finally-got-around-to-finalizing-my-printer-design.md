---
layout: post
title: "Finally got around to finalizing my printer design"
date: June 18, 2015 20:23
category: "Discussion"
author: Russell Hummerick
---
Finally got around to finalizing my printer design. Time to order some parts.



Print Area 12x12x18

PCduino with Adafruit motor shields to control the printer.

![images/7b48c16be29978c98d63822fcd92938f.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7b48c16be29978c98d63822fcd92938f.png)



**Russell Hummerick**

---
---
**Mike Kelly (Mike Make)** *June 18, 2015 22:04*

I like it. I like it a lot


---
**Vic Catalasan** *June 18, 2015 22:16*

I like it but would prefer the bed to lower during prints because it is harder to adjust a gantry that is moving than a stationary gantry. I also prefer not to use roller bearings riding on two separate extrusions, binding can be an issue unless the extrusions are perfectly paralleled and all 4 vertical  supports will need to be perfectly aligned. This is all my opinion anyways.


---
**Russell Hummerick** *June 18, 2015 23:05*

I am designing this to be used with my cosplay business I am starting. It will be used to make some large prints so I wanted to keep the print bed fixed.


---
**Øystein Krog** *June 19, 2015 15:18*

Interesting, why did you go with a moving gantry?


---
**Russell Hummerick** *June 20, 2015 00:12*

Ordered most of the parts today.


---
**Frank “Helmi” Helmschrott** *June 27, 2015 10:34*

looks really interesting - keen to see more.


---
**Russell Hummerick** *June 27, 2015 13:06*

I'm going with the moving because this will me making very large prints. 


---
*Imported from [Google+](https://plus.google.com/100843029189377920683/posts/Xex5k1jNnjU) &mdash; content and formatting may not be reliable*
