---
layout: post
title: "Shot glass in 3 minutes! This is about as fast as I've been able to print with the ."
date: December 10, 2014 02:01
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
Shot glass in 3 minutes!  This is about as fast as I've been able to print with the #Eustathios. I set it for a spiralized 10 sided cylinder with Cura at 200mm/s. Initial temp was 240, which was then increased to 270 mid-first layer. Layer height was .2mm. The limiting factor seems to be two-fold. The Rambo board has trouble pushing g-code fast enough if I use cylinders with a larger number of sides. At these speeds, I'm having trouble maintaining hotend temp with a 24v 14a power supply.  Only downside is that I don't think it's a regulation shot glass. 

![images/a268281023bf737a523d2e7985e35e28.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a268281023bf737a523d2e7985e35e28.gif)



**Jason Smith (Birds Of Paradise FPV)**

---
---
**Wayne Friedt** *December 10, 2014 02:16*

What would you need to do to maintain temp, possibly run dual heaters?


---
**Eric Lien** *December 10, 2014 02:42*

I have my best luck high speed printing with ABS, but looking at your bed temps you are doing this with PLA. I can never maintain cooling in PLA, at least never with any overhangs or sharp corners. I may have to convert to your carriage.



I have seen the same gcode limitation at high speed on my azteeg at 200 to 250 depending on geometry. As soon as I can run tinyg on my printer I think we can push things much further.



Glad to see you posting still from time to time. You really designed something special with Eustathios.




---
**hon po** *December 10, 2014 02:43*

Strange that the temp graph shows the hotend temp start to rise 20s before the print end.



If you power the heated bed from the same supply, try a print with the heated bed off could verify if it's a supply issue.


---
**Eric Lien** *December 10, 2014 02:43*

Also I see you added a heated bed since your last posts. How are you liking it so far?


---
**Jason Smith (Birds Of Paradise FPV)** *December 10, 2014 03:20*

This is one of the very first prints with the heated bed. It's not the neatest looking thing I've ever built, but I'm liking it so far. I've just been using a quick shot of hairspray on the glass, and it seems to work well. I am running the bed on the same supply as the hotend, but I ran into the same limitation in the past before the bed was installed. I know the cooling fan is causing a good portion of the temp drop. I'm actually running a previous version of the carriage which didn't do as good of a job at directing the air away from the heater block. I agree that the TinyG will be able to push things faster/smoother. Does anyone know if any slicers out there support arc generation (G2 or G3)? It sure would be nice if every curve wasn't broken into tiny line segments. 


---
**Eric Lien** *December 10, 2014 04:05*

**+Jason Smith**​ I think with 32bit controllers becoming more popular arcs will be coming to slicers eventually.



For the bed try a 1/8" thick heat spreader as the base to the springs, with the mat adheared below. You can insulate below the mat to help direct all the heat into the spreader. Works great for me.﻿


---
**Brandon Satterfield** *December 10, 2014 04:34*

WHAT not regulation size. Humph. Looks great flying like that! 



I'm curious if you guys think the smoothie could run it? 32 bit..


---
**Mike Thornbury** *December 10, 2014 11:29*

STL is all made up of triangles, there are no splines to work with to make an arc from.


---
**Tim Rastall** *January 16, 2015 23:52*

Hey **+Jason Smith**​ what were your acceleration values for this print? 


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/KEK14Zi88rM) &mdash; content and formatting may not be reliable*
