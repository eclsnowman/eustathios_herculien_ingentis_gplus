---
layout: post
title: "Just wanted to some honey comb fun for a part I am designing for heavy industrial use"
date: February 18, 2018 18:47
category: "Show and Tell"
author: Sean B
---
Just wanted to some honey comb fun for a part I am designing for heavy industrial use.  First in PLA, and a final version in taulman 910.  Any advice on printing nylon?  All I know is that it needs to be dry and about 260c.  


**Video content missing for image https://lh3.googleusercontent.com/-kymbdWZQZs8/WonKLsuMx0I/AAAAAAAAgmc/ve6clRa6HIwszPhU_BSSCNhRycqftpOxwCJoC/s0/VID_20180218_125655.mp4**
![images/7acc1d30b1365b7dd1ae448cfb93ec0b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7acc1d30b1365b7dd1ae448cfb93ec0b.jpeg)



**Sean B**

---
---
**Oliver Seiler** *February 18, 2018 19:53*

Nylon doesn't stick well to PEI sheets, should you have one. PVA glue stick on glass works well for me. Make sure it's dry and start slow. I'm usually using 245 degrees and 60 degrees for the bed. 


---
**Sean B** *February 18, 2018 19:55*

**+Oliver Seiler** I had read that as well, can I put glue stick directly on the PEI or painters tape?


---
**Oliver Seiler** *February 18, 2018 19:57*

Probably. I have managed to print small parts directly on PEI, but it barely sticks and larger parts warp off. I didn't want to contaminate my PEI sheet with PVA and just used a spare glass. 


---
**Gus Montoya** *February 19, 2018 01:00*

good advice for future use :)




---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/QZVmGktq3e9) &mdash; content and formatting may not be reliable*
