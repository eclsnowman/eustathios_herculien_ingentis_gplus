---
layout: post
title: "Does anyone have a dxf of the MDF insulator board, or the measurements of the two levels of the cuts?"
date: August 08, 2015 19:12
category: "Discussion"
author: Bruce Lunde
---
Does anyone have a dxf of the MDF insulator board, or the measurements of the two levels of the cuts?  I cannot seem to get edrawings installed my my pc, and am ready (finally) to cut this out. I received my aluminum plate yesterday, so am almost complete on this build, after a bit of a delay.





**Bruce Lunde**

---
---
**Bruce Lunde** *August 08, 2015 19:15*

For the HercuLien 


---
**Rick Sollie** *August 08, 2015 20:52*

If you ordered the Ali rubber heat pad it comes (or can be ordered) with a double sided 3m tape which you can attach directly to the aluminum plate.


---
**Eric Lien** *August 08, 2015 20:55*

[https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Drawings/MDF_Insulator_Board.PDF](https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Drawings/MDF_Insulator_Board.PDF)





[https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Drawings/MDF_Insulator_Board.DXF](https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Drawings/MDF_Insulator_Board.DXF)


---
**Bruce Lunde** *August 08, 2015 21:04*

Thank-you both for your quick responses! Off to the CNC table for me!


---
**Eric Lien** *August 08, 2015 21:10*

Glad to be of service, just help out other people when you can and then the circle is complete :)


---
**Bruce Lunde** *August 08, 2015 21:12*

As soon as I am printing, I will do the pay it forward, like **+Alex Lee** did for me! I also try to answer questions, when I know enough to help.


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/G9SVELPmpCv) &mdash; content and formatting may not be reliable*
