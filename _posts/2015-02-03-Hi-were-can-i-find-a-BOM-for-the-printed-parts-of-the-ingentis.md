---
layout: post
title: "Hi, were can i find a BOM for the printed parts of the ingentis?"
date: February 03, 2015 19:14
category: "Discussion"
author: Patrick Liesens
---
Hi, were can i find a BOM for the printed parts of the ingentis? How many of what part should be printed.





**Patrick Liesens**

---
---
**James Rivera** *February 03, 2015 19:30*

The root page for this community has links to the sources, but you're right--I don't see any way to tell how many of each printed part you will need other than to look at the photograph and guessing. Maybe **+Tim Rastall** already has a list somewhere?


---
**Tim Rastall** *February 03, 2015 19:34*

The you machine page has the parts qualities I think.  However I recommend building a Eustathios as its an iteration beyond ingentis. You can easily chop and change their Z axis if you prefer belted Z. 


---
**David Heddle** *February 03, 2015 20:11*

Here is a sheet I made up when I was printing mine.

This was before I changed to the direct drive for xy

[https://docs.google.com/spreadsheets/d/1hLnJ6RHLBs6PbHADeaKGqiFbT8FweBb7Btyvsxf_01s/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1hLnJ6RHLBs6PbHADeaKGqiFbT8FweBb7Btyvsxf_01s/edit?usp=sharing)


---
**Tim Rastall** *February 03, 2015 21:12*

**+Oliver Schönrock** nothing wrong with it except the lack of planetary gearbox.  Layer consistency is as good as I've seen. 


---
**Patrick Liesens** *February 04, 2015 19:34*

**+David Heddle** Thanks for that list. Just what i was looking for.


---
*Imported from [Google+](https://plus.google.com/113289096255842909244/posts/9ijoEK6zbeQ) &mdash; content and formatting may not be reliable*
