---
layout: post
title: "Hello all I am new and have a question"
date: September 29, 2014 03:38
category: "Discussion"
author: Gus Montoya
---
Hello all



   I am new and have a question. How thick do the linear shafts need to be on  a 3 ft L x 2.5 ft W x 2.5 ft H frame ?  I am making this for large build haha. I am assuming 10 mm is thick enough...but not sure. Please all constructive critique is welcome.





**Gus Montoya**

---
---
**D Rob** *September 29, 2014 03:48*

10mm should be sufficient. Check the .stl hole sizes first though, you may need customized parts. I have a 16x16 frame and use 10mm. You may want to investigate 12mm though﻿.


---
**Stephanie A** *September 29, 2014 06:14*

There are online linear motion calculators that will determine the deflection based on all of the variables (speed, weight, size, distance)


---
**Wayne Friedt** *September 29, 2014 06:41*

What would that online calculator be called?


---
**D Rob** *September 29, 2014 14:24*

**+Stephanie S**, calculators can be good to help improve educated guesses but can't include, practically, sone unknowns like the hardness/temper of the rods he'll use. And hardness will dictate the flexibility of the rod. A high carbon hardened rod will flex less than a mild steel rod for example.


---
**Dale Dunn** *September 29, 2014 15:16*

**+D Rob** , elastic modulus is pretty much the same for most all steels (except stainless), with only a few percent difference. Hardness describes the relative deformability of the surface. Heat treatment also has a negligible impact on elasticity.



**+Gus Montoya** , what you need to look for are beam calculators or tables of beam formulas. You can use them to calculate the static sag from weight and the flex from acceleration. You will have to look up some of the variables to fill in, and that will also automatically capture the specific properties of the material used, as **+D Rob** was talking about. The linear motion calculators **+Stephanie S** mentioned will help you find some of the load to put into the beam equations and may so some beam calculations for you. That all depends on what calculators you find (usually on a large manufacturer's site).



Be aware that there is no calculator or tool that will take just a few inputs and give you valid results for an Ingentis, unless someone has put together a spreadsheet. I didn't when I ran the numbers for mine. I was looking at the rigidity of the mechanism specifically, in order to maximize acceleration without causing significant artifacts in the prints. Based on what I remember coming up with, 10mm may be rigid enough for the rotating shafts (you'll want to check me against my memory), but for the travelling beams, solid steel, round beams will be heavy and flexible. You'll have to limit your acceleration more than you may like. I posted some calculations for this back in the spring in regard to the Quadrap. I'll try to find them. Edit: I found it in the comments here: [https://plus.google.com/116889746506579771100/posts/c1BbZU2eigq](https://plus.google.com/116889746506579771100/posts/c1BbZU2eigq)


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/KSbzQwjtVUB) &mdash; content and formatting may not be reliable*
