---
layout: post
title: "Hi everybody! I am watching the site for a while now and finally I took the decision to built an eustathios!"
date: April 02, 2016 13:24
category: "Build Logs"
author: Dimitrios Tzioutzias
---
Hi everybody! 

I am watching the site for a while now and finally I took the decision to built an eustathios! For the last month I was gathering the materials needed!  The printed parts were made on my prusa i3. Most probably I am going to use lead screws for z axis instead of ballscrews. This is going to be a later upgrade for my eustathios.I am trying to keep the cost as low as practicable. Any advice  is always welcome!  More to come soon! 



![images/4bdabc8b6e7a0f2f3a159c24c323f15e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4bdabc8b6e7a0f2f3a159c24c323f15e.jpeg)
![images/b55c28cd05ccce1cc0854d557746dd0d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b55c28cd05ccce1cc0854d557746dd0d.jpeg)
![images/5741c9c1f98032b3a956c6441250dfa0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5741c9c1f98032b3a956c6441250dfa0.jpeg)
![images/34d3d30959fbb1107b4afc06739cc12d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/34d3d30959fbb1107b4afc06739cc12d.jpeg)
![images/a04816de54fc89fef85f94881d3fe568.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a04816de54fc89fef85f94881d3fe568.jpeg)

**Dimitrios Tzioutzias**

---
---
**Eric Lien** *April 02, 2016 13:40*

Looking great. And the lead scews will be just fine. I know **+Zane Baird**​ uses them on his HercuLien and his prints look amazing. Glad to have another great member to the community **+Dimitrios Tzioutzias**​. Can't wait to watch your progress.


---
**Dimitrios Tzioutzias** *April 02, 2016 13:50*

**+Eric Lien** Thank you and the community for the great design and for all the files you gave publicly! I have also the rods but i have to cut them in correct length, I bought them 500mm. For the heated bed I ordered 350x350 220v 800watt, so the aluminum bed need to be a little bigger than the design. I already have an mks sbase smothie clone just waiting for the steppers for x,y,z and the geared one for the extruder from robbotdig.


---
**Eric Lien** *April 02, 2016 14:30*

**+Dimitrios Tzioutzias** So will your printer be slightly larger... or just the bed?



I see you are planning to use the HercuStruder. I always had good luck with it. But later on, an upgrade I might recommend is the Mechanical Kit for the Bondtech. Since you already have the geared stepper all you will need is the housing and gear set. Martin did such a great job on the design. Mine is good, but his is so much better I find it hard to not recommend it to everyone, even over my own design.


---
**Eric Lien** *April 02, 2016 14:45*

Also when giving thank you's on the design I want remind people my design on the Eustathios Spider V1 and V2 was iterative from the amazing work of **+Jason Smith**​​, **+Tim Rastall**​​, **+Brad Hill**​​ and many others who came before me. Nothing I did could have been possible without all their amazing open source work.﻿


---
**Dimitrios Tzioutzias** *April 02, 2016 15:15*

The basic size  of the bed will be the same 400x360, I ve just changed the 27.5mm dimension of your design to 20mm so I can fit the rubber heater. I will take a look for the bondtech! ﻿


---
*Imported from [Google+](https://plus.google.com/108355995361667474020/posts/e8MsJUojhbU) &mdash; content and formatting may not be reliable*
