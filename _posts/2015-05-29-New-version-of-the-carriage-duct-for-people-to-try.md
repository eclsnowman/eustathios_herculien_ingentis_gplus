---
layout: post
title: "New version of the carriage duct for people to try"
date: May 29, 2015 02:39
category: "Show and Tell"
author: Eric Lien
---
New version of the carriage duct for people to try. Let me know what you think (nice thing is it is an easy swap).



[https://drive.google.com/file/d/0B1rU7sHY9d8qbmNvSE1EbWVibXM/view?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qbmNvSE1EbWVibXM/view?usp=sharing)



I like this one more because the air flow seems to be more directed at the tip of the nozzle. Pictures of the old vs the new are included for comparison.﻿



Note: Support for the inner face of the duct is built in since it is very hard to support in a slicer. You may need a little knife action to remove it.﻿



![images/b16d72d6f8c34556285815d27d6e1ba2.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b16d72d6f8c34556285815d27d6e1ba2.png)
![images/cff6026c5384679f7761600d680f8d77.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cff6026c5384679f7761600d680f8d77.png)
![images/6501dddcce986ab056b6535be7bbcdbd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6501dddcce986ab056b6535be7bbcdbd.jpeg)
![images/81daa30c7336407c5547d409dc87cfec.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/81daa30c7336407c5547d409dc87cfec.jpeg)
![images/05f71dffd867151c81c9501041a94bcc.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/05f71dffd867151c81c9501041a94bcc.png)
![images/27eff271a366f58b4eb5072cb3d0bfab.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/27eff271a366f58b4eb5072cb3d0bfab.png)
![images/dff8daeafbca7274e9662de672734e46.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dff8daeafbca7274e9662de672734e46.jpeg)
![images/a14163c0d551dbed4b612f302d0b9641.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a14163c0d551dbed4b612f302d0b9641.jpeg)
![images/0afad14d39c7cf78fee2d8e011fde6dc.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0afad14d39c7cf78fee2d8e011fde6dc.png)
![images/aab94154b4f31e02b4c942ff3d40fda3.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/aab94154b4f31e02b4c942ff3d40fda3.png)

**Eric Lien**

---
---
**Jim Squirrel** *May 29, 2015 02:50*

so purdy


---
**Ricardo de Sena** *May 29, 2015 03:07*

Good job!


---
**X Copter** *May 29, 2015 03:10*

Thats awesome! Can't wait to build mine!

Whats the point of the ring around the hotend?


---
**Eric Lien** *May 29, 2015 04:05*

**+X Copter** cooling for PLA and other materials that require active cooling.


---
**Jim Squirrel** *May 29, 2015 04:19*

it also adds an AWESOME visual enhancement to any printer 


---
**X Copter** *May 29, 2015 05:11*

Oh I see. 

Sorry I should have looked at the rest of the photos haha

That design is awesome! Do you have any tips for when I scale mine down?


---
**Eric Lien** *May 29, 2015 06:12*

To scale it down just take the extrusion length and rods and shorten them by the difference. But you will need to go part by part and confirm. I recommend using one of the free cad software mentioned in another post (designspark, fusion360, etc.)



It is good to learn them, you will need CAD to go beyond printing trinkets anyway. Thats what I did. 



One note is scaling it down will do little to change the cost. Is there a reason you want it smaller.


---
**Eric Lien** *May 29, 2015 06:17*

After some testing I noticed I will need to tweak the air flow path some more. I think it affects the hot end temp too much when the cooling turns on. 


---
**X Copter** *May 29, 2015 07:23*

I know how to use inventor fusion and sketchup. Been using them for over a year now (I design a lot of parts for my mini quads). 

I would like to scale it down so I don't have to use a solid state relay and I don't want to mess around with 230v (we can't get 120 here in nz). It will also make start up times quicker and besides, I rarely print parts larger than 100x100mm. 



That cooling method is absolutely awesome!


---
**Isaac Arciaga** *May 29, 2015 07:58*

**+Eric Lien** have you tried PID tuning with your fan on? 


---
**Carl Hicks** *May 29, 2015 09:05*

Nice work. 


---
**X Copter** *May 29, 2015 11:53*

**+Eric Lien** 

What's the exact Max build volume that the current eustathios is capable of? Just so I know how much to take off all the parts :)


---
**Eric Lien** *May 29, 2015 12:40*

**+X Copter** 285x285x295 I believe... Maybe a tad more.


---
**Eric Lien** *May 29, 2015 12:42*

**+Isaac Arciaga**​ I haven't... How will that effect PID with the fan off. I had to hand tune mine. Autotuning the hot end via Smoothieware was really unstable for me.﻿


---
**X Copter** *May 30, 2015 00:55*

Thanks! Will begin ordering parts next week!


---
**Erik Scott** *May 30, 2015 20:08*

Yeah, I noticed there's going to be a lot of cooling right on the nozzle and heater-block when I saw the cross section. Glad you made it compatible with the existing carriage. Makes experimenting easy. 


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/WT79ruM7StQ) &mdash; content and formatting may not be reliable*
