---
layout: post
title: "Working on some Eustathios text to put on my printer and I figured I'd ask some opinions"
date: September 05, 2015 19:39
category: "Discussion"
author: Erik Scott
---
Working on some Eustathios text to put on my printer and I figured I'd ask some opinions. Here are my current thoughts, in no real particular order. 



1 represents a the base text I would be working from. Each letter was drawn on a 5x4 skewed grid (with the exception of the T, the vertical stroke of which had to be centered between the 2nd and 3rd columns), with curves added to the corners to accentuate the letters. While it's pretty simple and nice on its own, the A and the O have enclosed regions, creating issues when it comes to laser cutting. From here, I wanted to find interesting ways to create a sort of stylistic stencil typeface. 



2 was accomplished by simply cutting all the letters at the 2nd row from the bottom. Unfortunately, this makes the letters all a bit top-heavy and the A retains an enclosed region. 



3 is a sort of mix between 2 and 4. I split letters horizontally where I felt they made sense. It's a bit more abstract (the S and E for instance)



4 is like 2, but split horizontally at the 2nd row from the top. This doesn't leave any enclosed regions, but unfortunately it makes the U (and the H, though it no longer looks like an H) appear to have an umlaut. It also resembles the ESPN logo a bit too much in my opinion. 



5 is a variation of all of the above. All are (failry generously) cut horizontally with the exception of the U to avoid the umlaut issue. The only problem here is the cut is now 2 columns wide, whereas all prior cuts were only a single row or column wide. 



6 is actually one my original designs I sketched on paper before I turned to the computer. I abandoned attempting to maintain only horizontal cuts and just made cuts where I thought they should be. I can't work without rules however, so in this case I wouldn't allow any regions of negative space to be surrounded on 3 sides. (compare the previous As to the #6 A) I did this for consistency and to avoid some fragility in the laser-cut piece. I also removed the header and footer from the I to make things a bit more compact. 



Number 7 is very similar to number 6 with a few letter changes. I changed the S around a bit with 2 half-width cuts at the center. I also took out a cut from the H and reversed the side of the vertical cut in the A. 



Anyway, why am I asking a bunch of "makers" about their opinions on a graphic design project? Well, I'm going to be adding this to the design files of my Eustathios build, and I want something that people are happy with. I'm also not so certain about what I want either, and it's always good to get some opinions. So let me know which version you like best, and even which letters in particular you like. Currently, i'm looking at number 6 with maybe the A and the H from 7.  

![images/e69660a11840facd2e7d9cc22c5cb5ed.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e69660a11840facd2e7d9cc22c5cb5ed.png)



**Erik Scott**

---
---
**Michaël Memeteau** *September 05, 2015 19:45*

Like best 1. and 4. for legibility. Maybe looking at your proposed mix 6. + A,H from 7. could make me change my mind.


---
**Erik Scott** *September 05, 2015 19:47*

Yup, I know what you mean. Again, 1, isn't really an option unless I can get clever about attaching things to the light diffuser behind it (yes, I want the letters to glow blue). 


---
**Michaël Memeteau** *September 05, 2015 19:53*

Maybe worth trying having a thin layer (0.6 mm) as an outline and the remaining thicker to improve the glowing... Or even have a smoother transition towards the center. 


---
**Erik Scott** *September 05, 2015 20:06*

Hard to explain what I had in mind. It's going to be a glossy opaque black front plate, with a frosted acrylic diffuser behind it. In the holes of the letters I want to put transparent blue acrylic. If I glue the black bits from the holes in the A and O onto the diffuser using the blue letters as guides, I could probably get away with the #1. However, I think it's a bit boring compared to the other options. 


---
**Eric Lien** *September 05, 2015 20:13*

My preference is 4, a good balance of the continuity of the horizontal line but still very legible.


---
**Dave Hylands** *September 05, 2015 20:50*

I'd also be inclined to take the 0 and O from 6, and the A (minus the horizontal line) from 6 or 7, and leave the rest as in 1.



I also think that 4 with a thinner horizontal line would be interesting.


---
**Erik Scott** *September 05, 2015 21:10*

Something like this: [http://imgur.com/v0Vvq89](http://imgur.com/v0Vvq89)
That's interesting. Not sure what I think about only 2 letters having cuts. The A also looks like a large lowercase N, at least to me. 



I'm not sure how I'd shrink down the horizontal line in 4 without changing the structure and pattern of the letters. I could simply lower the tops a bit, though that will mess up the spacing. 


---
**Dave Hylands** *September 05, 2015 21:13*

Actually for the A I was thinking of just the vertical cut and not the horizontal one.


---
**Erik Scott** *September 05, 2015 21:25*

Ah, so more like this? [http://i.imgur.com/rpvmfOE.png](http://i.imgur.com/rpvmfOE.png)



I also make the horizontal cut in #4 thinner. 


---
**Dave Hylands** *September 05, 2015 21:57*

I like both of those, perhaps leaning towards the horizontal line one. Of course, others will have their own opinion


---
**Sébastien Plante** *September 06, 2015 01:57*

4 or 6


---
**Rick Sollie** *September 06, 2015 12:03*

3 looks pretty cool. 7 is nice but looks like its version 2.8


---
**Riley Porter (ril3y)** *September 08, 2015 01:33*

**+Jason Smith**​ what do you like?


---
**Jason Smith (Birds Of Paradise FPV)** *September 08, 2015 01:47*

**+Riley Porter** I personally like #4.


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/6qTuN72TTVC) &mdash; content and formatting may not be reliable*
