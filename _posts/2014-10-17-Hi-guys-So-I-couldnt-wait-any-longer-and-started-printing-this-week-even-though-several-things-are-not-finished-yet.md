---
layout: post
title: "Hi guys So, I couldn't wait any longer and started printing this week even though several things are not finished yet"
date: October 17, 2014 13:54
category: "Show and Tell"
author: Pieter Swart
---
Hi guys

So, I couldn't wait any longer and started printing this week even though several things are not finished yet. The two big issues that I'm struggling with are the dribbling of the hot-end (although I see I can minimise it a lot if I use Slic3r) and backlash? My circles don't look too good. Probably covered somewhere before, but how do I figure out which belts need some attention? The  picture of the circles taken from the top is with the orientation from the front of the printer.




**Video content missing for image https://lh4.googleusercontent.com/-H10Oh4faNL8/VEEdXbpF65I/AAAAAAAAAb8/hxswS-ALty4/s0/20141016_201920_Android.mp4.gif**
![images/697c0f6031a5dc6ad8dd76bb020b04f0.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/697c0f6031a5dc6ad8dd76bb020b04f0.gif)
![images/3150f233c74b182b207fa879a1364f86.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3150f233c74b182b207fa879a1364f86.jpeg)
![images/0095befddbc5a52382828799b3ef4205.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0095befddbc5a52382828799b3ef4205.jpeg)
![images/58a39a506bcb5693cf0f64eabaa46e10.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/58a39a506bcb5693cf0f64eabaa46e10.jpeg)
![images/04a56cb19477aba4c69def34f142de78.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/04a56cb19477aba4c69def34f142de78.jpeg)
![images/45a27ca07acd12910e2e522a6d3cf4ab.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/45a27ca07acd12910e2e522a6d3cf4ab.jpeg)

**Pieter Swart**

---


---
*Imported from [Google+](https://plus.google.com/117269052061351663459/posts/6dn55S73iiN) &mdash; content and formatting may not be reliable*
