---
layout: post
title: "I've been getting some excess filament when printing"
date: August 21, 2015 00:44
category: "Discussion"
author: Brandon Cramer
---
I've been getting some excess filament when printing. About 5 hours into printing the gauntlet it got caught up and shifted the entire print over 1/4 inch. :(



There were some strings (technical name?) hanging between the sides of the gauntlet that I think probably need addressing. 



The cable chain seems to be starting off a little rough in the picture. 

![images/4dbca686cb58c15c99f5ae2ffeb7909c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4dbca686cb58c15c99f5ae2ffeb7909c.jpeg)



**Brandon Cramer**

---
---
**Mutley3D** *August 21, 2015 00:51*

that looks like over extruding, or close to the bed, but id bet on esteps too high, try turning down by 10% just to see what difference you get, or raise z height a nats knob. You might even need to do both


---
**ThantiK** *August 21, 2015 00:58*

That skirt thickness around the edge looks about right, so my money is on esteps being too high as well, or filament diameter being incorrect.


---
**Eric Lien** *August 21, 2015 01:13*

Ever since I switched to this method my prints have been ridiculously accurate and extrusion / surfaces are vastly improved: 
{% include youtubePlayer.html id=cnjE5udkNEA %}
[https://youtu.be/cnjE5udkNEA](https://youtu.be/cnjE5udkNEA)



Each roll just gets its own extrusion multiplier marked on it with tape and a marker, it takes almost no time.


---
**Frank “Helmi” Helmschrott** *August 21, 2015 06:23*

I wouldn't touch esteps if it's already been okay before and i guess you already did a calibration on esteps?



Then like Eric said calibration per filament role would be important. If you use an extruder like Slic3r their are quite some options to setup your profiles right for extrusion - with S3D i didn't have too much luck with that so far but that's maybe just on me :)


---
**Roland Barenbrug** *August 21, 2015 11:20*

It's the same for every printer. My Prusa E3D started to print a lot better once the esteps were set correct (i.e. 1 cm of fillament is really 1 cm), and I started to check the fillament diameter (3.1 mm is not equal to 3.0mm). Further make sure nozzle parameters are set correctly in in slicer software, e.g. slicr3r. Then you get into the next step of setting retraction right.


---
**Brandon Cramer** *August 21, 2015 16:29*

My esteps were off by 3mm. I followed the youtube video **+Eric Lien**  and I set my extrusion multiplier to 0.96. 



I printed this 20mm hollow cube and wow!! Looks really good! The block however is:



X: 19.97mm

Y:19.92mm

Z:19.72mm



Close enough or should I adjust the steps some more?


---
**Mutley3D** *August 21, 2015 16:43*

I bet if you printed the same object again youll get slightly different +- 0.05 without changing anything with ref to X and Y. Loss of height on the Z will be in part to do with Z height setting and the squish onto the bed


---
**Brandon Cramer** *August 21, 2015 20:44*

I thought this was an issue with my Printrbot Simple Metal, but since it just happened on my Eustathios Spider V2 I think it's caused be Repetier Host....



I was printing perfectly fine and when I went to start my next print job it acted like the printer has lost its mind and where the Z starting point should be. When I Z home it was pressing the plate down. So I have seen this on both printers now. Anyone else experience this? 



The good thing about it sliding across the glass and taking the blue tape with it, was that it motivated me to get the hair spray and print directly on the glass. :)


---
**Eric Lien** *August 21, 2015 21:22*

**+Brandon Cramer** have you calibrated Z height with everything at temp. The bed and hot end grow when up to temp shrinking the distance.


---
**Brandon Cramer** *August 21, 2015 21:24*

**+Eric Lien** It was printing good. I didn't change anything that I'm aware of. 



What exactly is the process of calibrating the Z height?


---
**Brandon Cramer** *August 21, 2015 21:45*

How much hairspray are you putting on the glass? I sprayed it three times but my test shark print peeled up pretty easy. I'm spraying it again but with quite a bit more this time. I'm using Auqa Net Extra Super Hold. Round two here we go....


---
**Eric Lien** *August 21, 2015 22:32*

**+Brandon Cramer**​​ I just recently tested 3D Eez by **+argas231**​​​. It works great with ABS and a heated bed. I am going to move away from hairspray moving forward.﻿


---
**Brandon Cramer** *August 21, 2015 23:46*

It hasn't been a good day for my 3D printing. I guess I don't understand the Z calibration. Also when I home everything, I get different levels every time I hit home Z? Once I use the piece of paper to make sure everything is the same on all 4 corners the print head comes out and is like 1mm higher than normal, or it drags across the glass. 


---
**Brandon Cramer** *August 21, 2015 23:51*

Clicking home all has two different outcomes. One drives the nozzle into the glass and the other is where I slide the paper under the nozzle while it is heated up. Frustrating!!!


---
**Eric Lien** *August 22, 2015 00:00*

Is your Z endstops loose? Also check that the bed seems stable if you wiggle it. Lastly, I use shielded cables on my endstops to minimize noise.


---
**argas231** *August 22, 2015 00:17*

Hello..  I am Argas231    Tony Gaston  Creator of 3d EeZ...[www.3D-EeZ.com](http://www.3D-EeZ.com)     Eric has just mentioned 3D EeZ to you      I just thought you might like to see more about it


---
**Brandon Cramer** *August 22, 2015 01:19*

**+Eric Lien** I've checked the end stop and frame. It's strange that every time I hit "home all" it alternates between those two different outcomes. The wiring is shielded. 


---
**Frank “Helmi” Helmschrott** *August 22, 2015 05:34*

**+Brandon Cramer** your z-home position is always determined by the firmware. the host software can't change it. if you hit the home button  for all axis in Repetier Host it sends G28 to the printer which makes the firmware move axis by axis to the home position. 



The printerbot does have auto leveling while your Eusthatios doesn't, right?



It sounds a bit like as you would have construction issues anywhere. can you do a video of that different homing processes on the Eusthatios? One where it happens that you end up in different positions?




---
**Brandon Cramer** *August 22, 2015 06:12*

**+Frank Helmschrott** I'm not exactly sure what's going on but I'm thinking on Monday I might try printing something and when it goes to print I will pause the job and calibrate it from the exact spot it is at that time. Not sure what else to do. I don't know maybe Monday I will walk in and it will start working. I could record a video of this happening but like I said, when you home all it drives the nozzle into the glass and when I home all the second time it goes to the spot where I calibrated it. Hitting home all again will just repeat this process. Once I start the print job it starts like 1mm off the glass! I don't see anything physically wrong and find it strange that it could be if it does the same exact thing every other time I hit home all.



You're right my Printrbot has auto leveling and my Eustathios doesn't.  



The only other thing I can think of is something I did this morning caused my issue. All I did was adjust esteps and filament multiplier. Not sure how any of this would cause this issue. 



**+argas231** I will be ordering this to try it out. Thanks for the link. I tried to search for it but I couldn't get find the correct site. 


---
**Frank “Helmi” Helmschrott** *August 22, 2015 06:43*

**+Brandon Cramer** i think it would be useful to approach the issue step by step. When doing a "home all" (G28) there's not much that comes into place:



- Homing direction per axis

- Endstop switches



Depending on your firmware (i think you're using Smoothieware which i don't know too much of) you may also have additional settings like values for a move back when checking the home position or something like that.



As you're getting good positions every now and then i wouldn't worry about the firmware settings too much. I would check:



- Is G28 always the command that is getting used (when homing manually via the Host software it should be but also in your starting G-Code?)

- Are the endstops mounted well and can't be moved by hand? 

- Do the endstops all feel well in their switching position or may one be broken and switch differently every time you drive into it?

- May your bed be mounted unreliably? Does it come back out of the springs if you press it in?


---
**Frank “Helmi” Helmschrott** *August 22, 2015 06:46*

You could also post your home/endstop settings from the smoothieware so we could look if there are unusual values. I was just reviewing the documentation for that ([http://smoothieware.org/endstops](http://smoothieware.org/endstops))


---
**Brandon Cramer** *August 22, 2015 17:29*

Here is the Azteeg X5 Mini config file:



[https://www.dropbox.com/s/vu49w203ehtlqx1/X5%20Mini%20Config.txt?dl=0](https://www.dropbox.com/s/vu49w203ehtlqx1/X5%20Mini%20Config.txt?dl=0)



I slapped (installed) Repetier Host on my desktop computer this morning and started up Eustathios Spider V2 and guess what? It's printing like a dream. I have no idea why, but I'm glad it's working. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/VHtYLMGnXEB) &mdash; content and formatting may not be reliable*
