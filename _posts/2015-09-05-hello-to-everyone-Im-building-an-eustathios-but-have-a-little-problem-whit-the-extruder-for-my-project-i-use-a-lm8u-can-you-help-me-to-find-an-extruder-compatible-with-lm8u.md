---
layout: post
title: "hello to everyone, I'm building an eustathios but have a little problem whit the extruder, for my project i use a lm8u , can you help me to find an extruder compatible with lm8u?"
date: September 05, 2015 06:07
category: "Discussion"
author: Roberto Viglione
---
hello to everyone, I'm building an eustathios but have a little problem whit the extruder, for my project i use a lm8u , can you help me to find an extruder compatible with lm8u? 





**Roberto Viglione**

---
---
**Frank “Helmi” Helmschrott** *September 05, 2015 06:37*

I'm not sure i got you right - are you building an Eusthatios Spider V2? And you are using LM8UU on the inner cross rods? You could always modify the holes of **+Eric Lien** 's Extruder. There are Files in some formats that you can import to the CAD of your choice. Maybe use onshape or **+Autodesk Fusion 360** (both free) and change the wholes diameter to your choice.



May i ask why you are using LM8uu instead of the bronze bushings?


---
**Eric Lien** *September 05, 2015 14:53*

I thick **+Isaac Arciaga**​ or **+Jason Smith**​ have the carriage in the lm8luu version.


---
**Ishaan Gov** *September 05, 2015 15:32*

**+Frank Helmschrott**, IMHO, I'd prefer bearings to bushings generally because of the preload in the bearings, leading to less backlash (please correct me if I'm mistaken); I'm getting some misumi LMU8s and misumi 8mm shaft for the flying gantries on my machine


---
**Jason Smith (Birds Of Paradise FPV)** *September 05, 2015 16:24*

**+Eric Lien** **+Roberto Viglione** **+Isaac Arciaga** Here you go!  This is the carriage I use:

[https://drive.google.com/file/d/0B2629YCI5h_wUkNkdzRCU1ZsQVE/view?usp=sharing](https://drive.google.com/file/d/0B2629YCI5h_wUkNkdzRCU1ZsQVE/view?usp=sharing)


---
**Roberto Viglione** *September 12, 2015 06:02*

thank's you !! :D


---
*Imported from [Google+](https://plus.google.com/107727214917983809932/posts/Q6HoThrBhv3) &mdash; content and formatting may not be reliable*
