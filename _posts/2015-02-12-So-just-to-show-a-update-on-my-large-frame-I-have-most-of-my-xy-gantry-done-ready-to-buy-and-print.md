---
layout: post
title: "So just to show a update on my large frame, I have most of my xy gantry done ready to buy and print"
date: February 12, 2015 21:58
category: "Deviations from Norm"
author: Ryan Jennings
---
So just to show a update on my large frame, I have most of my xy gantry done ready to buy and print.  



Using 10mm rod with 14mm z spacing to allow use of the gantry carriages designed by **+Eric Lien** 



Unfortunately this is giving my a XY movement area of only 560mm x 560mm.  



I would love to hit the magical 600mm movement area if anyone has some ideas

![images/6e4cb6bb8f536e2716b8928ce031ebf6.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6e4cb6bb8f536e2716b8928ce031ebf6.png)



**Ryan Jennings**

---
---
**Tim Rastall** *February 12, 2015 22:01*

Eeek. How long for are those 10m shafts going to be?


---
**Ryan Jennings** *February 12, 2015 22:31*

The long rods are 688mm and using a solid aluminum coupler to the stepper shaft. Perhaps a bearing on that end would be smart also


---
**Eric Lien** *February 13, 2015 00:04*

I think you will want the larger bearing on the end. Putting that load directly in the steppers small bearing could be bad news.



The side rods have to stay really true or you will see issues. That's why I use the small closed loop gt2 belts to couple the motor and shaft. Also it allows you to give the stepper some reduction for added torque. I run 20 tooth on the stepper and 32 on the shaft.﻿


---
**Ryan Jennings** *February 13, 2015 00:19*

Hm that does make sense, I might be able to switch to that style with minimal work to my existing design work.  Move motors to different plate, and put bearing on plates, then i can move the rods closer to the frame to get a bit more space.



Would a worm gear be a possible option instead of a small gear?



I will admit i do not know how to calculate the steps in firmware yet based on pulleys so i will have to figure that out.  



I had considered doing everything in 12mm instead of 10 as i dont know if there is flex over the distance. main reason i had not yet was to save time and reuse the existing designs for the herculien 



Thanks for the ideas guys.


---
**Eric Lien** *February 13, 2015 01:33*

Also on that size bot I would think about linear bearing on the center carriage. Any rod deflection could yield chatter on the center carriage. Bearings are more forgiving than bushings is that regard.﻿



The side shafts would have to remain bushings. Unless you try scaling up Tims new design.


---
**Ryan Jennings** *February 13, 2015 01:53*

Only reason i was looking at bushings was to reuse your carriage file, with bearings it would basically be a overhaul of your file but should still be doable.  While i do not mind designing from scratch I am excited to get it going so trying to be as quick as possible



my goal is to get the XY printing this weekend and get the misumi parts ordered monday for that


---
**Eric Lien** *February 13, 2015 02:27*

**+Ryan Jennings** It should be doable with bushings. If not, nice thing is you are only out $9 in bushings and some abs ;)


---
**Mike Kelly (Mike Make)** *February 13, 2015 17:04*

Yeah those are really long spans for the gantry. I'd suggest doing some calculations and seeing the amount of deflection you get. 10mm might suffice but that's a very  large span.


---
*Imported from [Google+](https://plus.google.com/100961646236572561479/posts/SWnfoauAGKe) &mdash; content and formatting may not be reliable*
