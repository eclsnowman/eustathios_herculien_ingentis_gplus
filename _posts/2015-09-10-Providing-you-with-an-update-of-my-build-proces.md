---
layout: post
title: "Providing you with an update of my build proces"
date: September 10, 2015 08:22
category: "Show and Tell"
author: Roland Barenbrug
---
Providing you with an update of my build proces. Most of the parts have been printed by my Prusa I3 (takes some time but size is not the issue), smooth rods have been delivered, 2020 frame has been cut by Motedis (Germany) and is on its way, lots of small parts have been ordered at RobotDIgg. As soon as the frame parts arrive, assembly can start. Not sure yet on the bronze bushings from a sourcing point of view. Any ideas for European sourcing?

![images/b226678dbaa331b269213078f1643dab.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b226678dbaa331b269213078f1643dab.jpeg)



**Roland Barenbrug**

---
---
**Daniel F** *September 10, 2015 08:47*

Got my bushings from lulzbot, shipped from european warehouse (GB). Unfortunately the 8mm ones are out of stock. The 10mm version is  available: [https://www.lulzbot.com/products/10mm-bronze-bushing-4-pack](https://www.lulzbot.com/products/10mm-bronze-bushing-4-pack)


---
**Daniel F** *September 10, 2015 08:51*

European sourcing is difficult but Ultibots USA has both of them in stock, 8 and 10mm, shipping from us is 16$:

http://www.ultibots.com/8mm-bronze-bearing-4-pack-a-7z41mpsb08m/


---
**Frank “Helmi” Helmschrott** *September 10, 2015 12:02*

**+Roland Barenbrug** I'm also located in germany. I've used the Robotodigg graphite bushings for the outer rods. I also used them for the inner rods but had trouble getting proper alignment and smooth movement. I switched them for shorter bronze bushings i had laying around but without the self aligning part around them. I used self printed filaflex sleeves from **+Walter Hsiao** but wasn't lucky so far (see my thread where i searched for stronger steppers). I'm currently trying LM8UU on the inner rods and I'm quite happy so far. They seem to be the better solution for the rod crossing and well... Ultimaker also uses them there - probably for a reason :)


---
**Eric Lien** *September 10, 2015 17:01*

**+Roland Barenbrug**  Even on a a large printer it takes a long time to print all the parts :)



I similar to you use a good brim to ensure my parts stay flat. Removal of the brim is easy with a razor and a small jewelers file. Warped parts drive me nuts, so I would rather have some post processing than look at a part for years than I know I could have done better on.



Great job, the parts look very nice.


---
**Roland Barenbrug** *September 10, 2015 18:46*

**+Eric Lien** indeed printing a brim avoids a lot of problems hence this has become a standard procedure. Concerning the time I'm not to worried as printing does not require any attention once started.

Regarding quality I woulde grade it as a A-. Prints show show kind of wobble and I haven't found the cause for it. As far as I can seen it's not caused by the Z-axis.


---
**Roland Barenbrug** *September 10, 2015 18:48*

**+Frank Helmschrott** **+Daniel F** guess I will order the 10mm bushings for the outer rods and use some LM88UU bearings on the inner rods. Thnx for your shared experience and suggestion


---
**Roland Barenbrug** *September 10, 2015 19:00*

**+Frank Helmschrott** BTW, Netherlands (close to Germany ;-))


---
**Eric Lien** *September 10, 2015 19:00*

**+Roland Barenbrug** I like using the LM8LUU (long 8mm linear bearing) over using two LM8UU in line. There is less chance of misalignment and binding.


---
**Ted Huntington** *September 10, 2015 20:26*

I bought the bushings from ultibots, sdpsi is out too- although they have 8mm and 10mm teflon filled acetal bushings. I may try to transition to the many low cost bushings available on ebay and aliexpress, because they probably are just as good, always readily available, and low cost- like here: [http://www.ebay.com/itm/10pcs-SF-1self-lubricating-composite-bearing-bushing-sleeve-8mm-10mm-12mm-/290649239307?hash=item43ac0b6f0b](http://www.ebay.com/itm/10pcs-SF-1self-lubricating-composite-bearing-bushing-sleeve-8mm-10mm-12mm-/290649239307?hash=item43ac0b6f0b) or here: [http://www.ebay.com/itm/10Pcs-SF-1-0808-Self-Lubricating-Composite-Bearing-Bushing-Sleeve-8-10-8mm-/390649560542?hash=item5af4873dde](http://www.ebay.com/itm/10Pcs-SF-1-0808-Self-Lubricating-Composite-Bearing-Bushing-Sleeve-8-10-8mm-/390649560542?hash=item5af4873dde) - there area also appareantly graphite oiless brass bearings here: [http://www.ebay.com/itm/1Pc-8-12-8mm-JDB-Graphite-Lubricating-Brass-Bearing-Bushing-Sleeve-Oilless-/370885281295?hash=item565a7c460f](http://www.ebay.com/itm/1Pc-8-12-8mm-JDB-Graphite-Lubricating-Brass-Bearing-Bushing-Sleeve-Oilless-/370885281295?hash=item565a7c460f) - I have never tried any of these, but perhaps they would be a low cost equivalent substitute?


---
**Eric Lien** *September 10, 2015 20:54*

**+Ted Huntington** The big problem is none of them are misalignment bushings. Our printed parts are only so accurate. And Bushings are notoriously bad when side loaded or mis-aligned.The SDPSI ones have the center bronze section potted into the outer ring so it acts as a 15deg misalignment spherical joint. This adjustment is what helps the carriage run easily within the tolerances our printers hold.



I think the standard bushings would work better with a machined carriage. Or like **+Walter Hsiao**  did print a rubber sleave to allow the bushings to self align.


---
**Ted Huntington** *September 10, 2015 21:31*

**+Eric Lien**

oh! I wasn't aware that misalignment was an important issue- in that case the regular low cost bushings must not be an option. I searched but couldn't find any low cost press fit bushings anywhere on ebay or aliexpress that seem to be mass produced and fit the bill. What do you think of the press fit teflon acetal bushings A 7Z44MPSD08MAF and A 7Z44MPSD10MAF? the only apparent difference is the max speed- which is why I passed on them- but for 3D printing maybe the difference in max speed isn't a major problem.


---
**Eric Lien** *September 10, 2015 22:06*

The problem with acetyl/steel is the higher coefficient of static friction compared to bronze/steel.


---
**Ted Huntington** *September 10, 2015 22:24*

**+Eric Lien**  yeah acetal bushings ultimately could add too much friction- good thing I didn't get those


---
**Frank “Helmi” Helmschrott** *September 11, 2015 13:08*

I didn't hear anything of acetal bushings so far but after googling i think that's the same principle and therefore the same problem as with **+igus Inc.** bushings on this printer. Apart from the higher friction that basically isn't a problem they are even more sensible to misalignment than the standard bronze bushings. Probably (linear) ball bearings are just the right thing to use in this place. I haven't had a single case of lost steps since even with my not so powerful motors since i put them in place.



I agree with **+Eric Lien** the LM8_L_UU (the long version) would probably be the best. I'll probably get myself some of them.


---
**Roland Barenbrug** *September 11, 2015 16:20*

**+Eric Lien** **+Ted Huntington** **+Frank Helmschrott** **+Daniel F**  summarizing all information I get the impression that the best solution (taking care of misalignment and minimal friction) for now consists of the bronze bushings on the outer rods and LM8LUU lineair bearings on the inner rods. Correct?


---
**Frank “Helmi” Helmschrott** *September 11, 2015 16:23*

I would sign that with that little bit of experience i now have with the Eusthatios (~4 weeks, some rebuilds). I though have to add i still use Rubber sleeves (self printed from Filaflex/Ninjaflex) over the outer bushings as the Robotdigg ones don't forgive any misalignment that might always be there due to missing printing accuracy.


---
**Eric Lien** *September 11, 2015 16:42*

**+Frank Helmschrott** yeah the outer rods are less prone to binding due to the distance between the bushing, the other constraints, and the fact that the cross rod has some room for flex in the mating hole. But I still prefer the oil bronze versus graphite bronce to eliminate dust and the chance for misalignment of two stiff bushings on one shaft.


---
*Imported from [Google+](https://plus.google.com/118296832015849309457/posts/4nF4bgjDm5a) &mdash; content and formatting may not be reliable*
