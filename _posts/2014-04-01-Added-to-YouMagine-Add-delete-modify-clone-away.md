---
layout: post
title: "Added to YouMagine! Add, delete, modify, clone away!"
date: April 01, 2014 01:08
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
Added  #Eustathios  to YouMagine!  Add, delete, modify, clone away!





**Jason Smith (Birds Of Paradise FPV)**

---
---
**Denys Dmytriyenko (denix)** *April 01, 2014 01:48*

Awesome! Do you plan on detailing the build process?


---
**Eric Lien** *April 05, 2014 00:02*

**+Jason Smith** I noticed the Eustathios Carriage A 1 V1.0 had stl errors. I corrected it between rhino and netfab cloud. Do you want the corrected file? The existing errors cause one of the e3d fan holes to close over and zip tie passage errors when sliced in cura and slic3r.


---
**Jason Smith (Birds Of Paradise FPV)** *April 05, 2014 00:15*

**+Eric Lien**, thanks for the heads-up. I remember having to clean things up prior to printing my carriage, but must not have retraced those steps properly when exporting for youmagine. If you'd be willing share the file with me, I'd really appreciate it!


---
**Eric Lien** *April 05, 2014 00:52*

**+Jason Smith** [https://drive.google.com/file/d/0B1rU7sHY9d8qVUpBcXdNQ0NCcDQ/edit?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qVUpBcXdNQ0NCcDQ/edit?usp=sharing)


---
**Jason Smith (Birds Of Paradise FPV)** *April 05, 2014 02:39*

**+Eric Lien**, Thanks!  I updated the files.


---
**Eric Lien** *April 06, 2014 03:02*

**+Jason Smith** Are you using one LM10LUU bearing per side on Z? I am going to pickup the last remaining mechanical items this weekend. Also do you have a suggestion for sourcing a selection of m5 bolts and other items for the build. I am trying to source them up front. All the hardware runs for expensive metric bolts on my COREXY build killed my budget.


---
**Jason Smith (Birds Of Paradise FPV)** *April 06, 2014 04:35*

@Eric Lien, if you have time, you should be able to source most of the hardware from aliexpress or eBay. I ended up buying a good portion of mine from

McMaster.com, which cost quite a bit more, but shipped very quickly from the states. Yes, I used 1 lm10luu per side for the z. They fit perfectly in the bed mounts. 


---
**Eric Lien** *April 06, 2014 06:21*

**+Jason Smith** thanks.


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/PRKMTSMetEJ) &mdash; content and formatting may not be reliable*
