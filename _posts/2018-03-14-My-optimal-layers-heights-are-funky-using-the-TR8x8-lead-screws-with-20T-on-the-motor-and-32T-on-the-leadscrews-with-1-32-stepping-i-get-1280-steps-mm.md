---
layout: post
title: "My optimal layers heights are funky- using the TR8x8 lead screws, with 20T on the motor and 32T on the leadscrews, with 1/32 stepping, i get 1280 steps/mm"
date: March 14, 2018 17:53
category: "Discussion"
author: Dennis P
---
My optimal layers heights are funky- using the TR8x8 lead screws, with 20T on the motor and 32T on the leadscrews, with 1/32 stepping, i get 1280 steps/mm. 



This is what i get for optimal layer heights with the Prusa Calculator-  0.192 and 0.256. Not easy numbers to remember. Playing around with other combinations, I think I need to change either my pulleys or my leadscrews. 



To those who used the trapezoidal leadscrews screws, what pitch and what pulleys did you use? 



 

![images/99116cb795414197097216df3fe308c2.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/99116cb795414197097216df3fe308c2.png)



**Dennis P**

---
---
**Eric Lien** *March 14, 2018 20:55*

Yeah, a motor pulley of 16 tooth would do the trick. I just dislike them a little due to the extra wear that bend radius puts on the Z drive belt. Another option is to change the Z driven pulleys (the 32 tooth)... but you run into  diameter limitations against the inner frame of the extrusion if you go too big.

![images/32b08a0663313340809599afd328bba1.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/32b08a0663313340809599afd328bba1.png)


---
**Ryan Fiske** *March 15, 2018 00:57*

Dennis, I am not sure if my issue was solely related to this, but I encourage you to see if it is possibly your issue. I calculated out my optimal layer heights, and the banding I was experiencing is gone. I also happened to tighten up my hotend, so there's a good chance that might have been it all along :)


---
**Walter Hsiao** *March 15, 2018 03:49*

If you're running 1280 steps/mm @ 32x microstepping that should be equivalent to 40 full steps per mm (1280/32) or 8 full steps per 0.2mm layer.  So I don't think you need to change anything, unless I'm missing something.



I think a 16 tooth pulley should give you a 0.02mm full step length, which is a bit easier to work with then your current 0.025mm step length.  I use a 16 tooth pulley on my z-axis and the belt seems to be holding up ok so far.


---
**Walter Hsiao** *March 15, 2018 04:34*

If the motor is attached directly to the lead screw, then that sounds right.  But with a 20 to 32 gear reduction (20T on the motor, 32T on the lead screw), that should increase the resolution by 32/20.  25 * 32/20 = 40.


---
**Dennis P** *March 15, 2018 04:54*

+Walter Hsiao I am really confused now. I think that calculator is not working in the way we are expecting it to. 

I agree with your math, and came up with the same thing, but wanted to double check, so went to Prusa site believing that it was vetted and 'infallible'. 



I put the values in the calculator to be consistent and thought nothing of it. I trusted the results because, well, its Prusa's site and why question it. 



[imgur.com - Imgur](https://imgur.com/a/ghNyl)






---
**Walter Hsiao** *March 15, 2018 05:07*

Yeah, I think the gear ratio is swapped too in the optimal layer height portion of the calculator too, assuming it's supposed to be consistent across the different sections.  It told me to put a 64T pulley on my z-motor to get my desired step length instead of the 16T pulley I'm using.


---
**Dennis P** *March 15, 2018 05:12*

**+Walter Hsiao** our comments are chriss-crossing and I deleted my comment and made a new post ... your math checks my original. I will stick with it and ignore Prusa's. 


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/4Jtw3pF7cfV) &mdash; content and formatting may not be reliable*
