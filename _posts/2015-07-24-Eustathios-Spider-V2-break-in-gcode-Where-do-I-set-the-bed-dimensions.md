---
layout: post
title: "Eustathios Spider V2 break in gcode.... Where do I set the bed dimensions?"
date: July 24, 2015 21:40
category: "Discussion"
author: Brandon Cramer
---
Eustathios Spider V2 break in gcode.... Where do I set the bed dimensions? Is this in Octopi or in the smoothie config file or both?



When I run the break in gcode it grinds the belt. I have all my home end stops working. When I click home in Octopi, it goes home for X,Y, and Z. I'm guessing I don't have the bed dimensions set correctly. What are the exact bed dimensions? Are my endstops possibly not in the correct place causing gcode to reach their limits and grind the belt? 



Just a few questions I have.... in a Yoda voice. 





**Brandon Cramer**

---
---
**Eric Lien** *July 24, 2015 22:02*

Yes you need to set bed size in the config file. Send me your current config file and I will take a look at it.


---
**Brandon Cramer** *July 24, 2015 22:39*

Thanks!



[https://www.dropbox.com/s/x48sglsb1wsy1ke/config.txt?dl=0](https://www.dropbox.com/s/x48sglsb1wsy1ke/config.txt?dl=0)


---
**Brandon Cramer** *July 24, 2015 23:37*

Well, at least the Z BreakInCode works :-) Kind of nice to see it moving back and forth. There is a lot more work to do, but I like it! 


---
**Eric Lien** *July 25, 2015 00:48*

I don't see anything wrong with the config. You will need to calibrate the extruder for the HercuStruder, the value in there is for the Bondtech. At 1/16 microstepping Hercustruder will be around 580 steps/mm



What do you have your microstepping set at on the board? Look at this diagram: [http://files.panucatt.com/datasheets/x5mini_wiring_v1_1.pdf](http://files.panucatt.com/datasheets/x5mini_wiring_v1_1.pdf), the jumper setting matrix is at the bottom of page1.



It may be traveling too far if the microstepping is set differently, hence it is traveling too far? Also which break-in g-code are you running? I just wanted to make sure it is not the one from HercuLien which is a larger bed.


---
**Brandon Cramer** *July 25, 2015 01:15*

**+Eric Lien** I think you might be onto something there. I will take a look at it Monday since my printer is at work. The micro stepping thing confuses me. How do I figure out what setting I need it on? 


---
**Brandon Cramer** *July 25, 2015 01:16*

They are all on 1/32 right now I think. Not sure which setting is right. 


---
**Eric Lien** *July 25, 2015 02:38*

**+Brandon Cramer** micro stepping is a preference really. More steps per mm is good in theory, but that of course means more calcs for the processor. So for example on HercuLien even though my drivers can do 1/32 I run at 1/16 do I don't run into stalling at high speeds.


---
**Isaac Arciaga** *July 25, 2015 04:00*

**+Eric Lien** from you experience, at what speeds have you noticed stalling using 1/32 with the X5? I was thinking of purchasing one because of the micro stepping. 


---
**Eric Lien** *July 25, 2015 04:23*

**+Isaac Arciaga** I have an X3 on HercuLien, so the same processor as a ramps. I would say above 175mm/s maybe 200mm/s it would jitter. So not the sort of speeds you normally would use. But I used to use those sort of speeds for infill and inner perimeters for prototype foundry patters back when I still worked at the foundry.


---
**Eric Lien** *July 25, 2015 04:24*

I have never seen stalling on the X5 yet.﻿


---
**Frank “Helmi” Helmschrott** *July 25, 2015 06:03*

with a solid firmware (I don't know much about smoothie ware) it shouldn't lead to any problems going too fast.



With AVR boards (everything Arduino based like RAMPS, RUMBA, Azteek X3 etc) with 1/32 the end is near at 125mm/s on the X/Y axis of Eusthatios. With Smoothieboard/Aztek X5 you shouldn't have any problems. I'm going 300-400 mm/s without bigger problems with 1/128. You may see problems based on the drivers and the power they bring into the motors (I had these with the the TI drivers with 1/32 before) but the steps calculation should be fine. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/7wkzBCygESE) &mdash; content and formatting may not be reliable*
