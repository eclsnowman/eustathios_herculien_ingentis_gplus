---
layout: post
title: "My current bed setup is pei on a sheet of glass, over a very bent aluminum plate"
date: August 15, 2018 12:37
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
My current bed setup is pei on a sheet of glass, over a very bent aluminum plate.



I will be remixing Walters bed design with a pei spring steel sheet and magnets.



The idea is to get something like the MK3, with auto leveling and perfect attachment.



Any thoughts?





Also all my belts squeak now as they rub on the pulley side walls. Is this due to cheap robotdigg belts?



![images/090cd5f7c4b00c407cfb0b6eed797ced.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/090cd5f7c4b00c407cfb0b6eed797ced.jpeg)
![images/a22d6a87266e7f6c5a2696d2c5f91f0c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a22d6a87266e7f6c5a2696d2c5f91f0c.jpeg)

**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *August 17, 2018 13:06*

Machined High Temp Magnets into an aluminum heat spreader, and two alignment pins at the back.



For the belts, are you sure the pulleys are in line with the belt path. Do you have the proper shims between the side pulleys and the side bearings.


---
**Stefano Pagani (Stef_FPV)** *August 17, 2018 14:08*

**+Eric Lien** they do have the shims, it could be my pla parts slowly melting :/ I plan to rebuild it soon with petg parts


---
**Dennis P** *October 22, 2018 16:20*

Any progress on this? I am trying to figure out weirdness to me bed leveling.






---
**Stefano Pagani (Stef_FPV)** *October 22, 2018 21:27*

**+Dennis P** You can get big beds off alibaba I dont have any time to work on this and honestly, everything has been working fine so far.


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/NNQ7vE7oeLD) &mdash; content and formatting may not be reliable*
