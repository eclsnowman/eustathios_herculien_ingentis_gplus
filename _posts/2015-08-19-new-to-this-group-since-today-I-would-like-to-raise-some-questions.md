---
layout: post
title: "new to this group since today, I would like to raise some questions"
date: August 19, 2015 20:49
category: "Discussion"
author: Roland Barenbrug
---
new to this group since today, I would like to raise some questions. RUnning rather happily my self build Prusa I3, it's time to consider a next step in building 3D printers. Eustathios seems the way to go because of size, the extruded frame foundation (living in Holland, sourcing from Germany is doable) and an active community. Having read the documentation I could find I wondering what kind of stepper motors are required (not mentioned in BOM). I presume it's NEMA17 but considering size of the printer I imagine a minimum torque is required. Next to that when it comes to printed parts, what material has delivered the best results. Candidates that come into my mind are PLA (not sure for parts close to hot end), ABS (toxic in printing), PETG (no experience yet but according to various reports it should be easy in printing and extremely strong).



Thnx for your support.





**Roland Barenbrug**

---
---
**Brandon Cramer** *August 19, 2015 21:00*

Are you building the Eustathios or Eustathios Spider V2?



Eustathios Spider V2:

(3) Nema 17 - 60mm 1.5A 

(1) Nema 17 - 40mm Stepper w/5.18 Planetary Reduction. 



I printed all my pieces using PLA. Seems to be working well. 


---
**Roland Barenbrug** *August 19, 2015 21:58*

Hi, thnx for the quick answer. Haven;t decided yet on Spider or not. What would be the essential difference between the two?


---
**Eric Lien** *August 19, 2015 23:18*

The Spider is essentially the original Eustathios by **+Jason Smith**​, with a few tiny alterations, and a detailed model and BOM.



Spider V2 has a larger Z length, and some modifications from my HercuLien design.


---
*Imported from [Google+](https://plus.google.com/118296832015849309457/posts/Rt5zvsj5zDJ) &mdash; content and formatting may not be reliable*
