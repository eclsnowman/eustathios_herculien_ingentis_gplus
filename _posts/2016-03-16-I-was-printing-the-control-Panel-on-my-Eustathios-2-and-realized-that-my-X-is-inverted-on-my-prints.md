---
layout: post
title: "I was printing the control Panel on my Eustathios 2 and realized that my X is inverted on my prints"
date: March 16, 2016 02:29
category: "Discussion"
author: Rick Sollie
---
I was printing the control Panel on my Eustathios 2 and realized that my X is inverted on my prints. This wasn't apparent when doing simple tests and non-engineering pieces.  I'm using RAMPs 1.4 board and running Marlin and have not been able to get this corrected.  



I've checked and my hardware is mounted according to Eric's Solid works model, with both x & y at a single corner. Both X and Y are connected as MIN.  Any suggestions?







**Rick Sollie**

---
---
**Stephanie A** *March 16, 2016 02:32*

Check the wiring to the stepper. You can reverse directions in the firmware. Configuration.h


---
**Tomek Brzezinski** *March 16, 2016 02:44*

You can also reverse wiring on most stepper config but not all. E.g make wires 1234 reconnect as 4321. Always change stepper wires without power. Don't unplug steppers if there's power in the system


---
**Ted Huntington** *March 16, 2016 03:01*

I'm using Ramps 1.4 on my Eustathios and here is the relevant part of my configuration.h:

// For Inverting Stepper Enable Pins (Active Low) use 0, Non Inverting (Active High) use 1

// :{0:'Low',1:'High'}

#define X_ENABLE_ON 0

#define Y_ENABLE_ON 0

#define Z_ENABLE_ON 0

#define E_ENABLE_ON 0 // For all extruders



// Disables axis when it's not being used.

// WARNING: When motors turn off there is a chance of losing position accuracy!

#define DISABLE_X false

#define DISABLE_Y false

#define DISABLE_Z false



//@section extruder



#define DISABLE_E false // For all extruders

#define DISABLE_INACTIVE_EXTRUDER true //disable only inactive extruders and keep active extruder enabled



//@section machine



// Invert the stepper direction. Change (or reverse the motor connector) if an axis goes the wrong way.

#define INVERT_X_DIR false

#define INVERT_Y_DIR true

#define INVERT_Z_DIR true //tphfalse



//@section extruder



// For direct drive extruder v9 set to true, for geared extruder set to false.

#define INVERT_E0_DIR true//tphfalse

#define INVERT_E1_DIR false

#define INVERT_E2_DIR false

#define INVERT_E3_DIR false



//@section homing



// ENDSTOP SETTINGS:

// Sets direction of endstops when homing; 1=MAX, -1=MIN

// :[-1,1]

#define X_HOME_DIR -1

#define Y_HOME_DIR -1

#define Z_HOME_DIR -1



// Travel limits after homing (units are in mm)

#define X_MIN_POS 0

#define Y_MIN_POS 0

#define Z_MIN_POS 0

#define X_MAX_POS 300//200

#define Y_MAX_POS 300//200

#define Z_MAX_POS 280



  #define XY_TRAVEL_SPEED 8000 



#define HOMING_FEEDRATE {80*60,80*60,15*60,0}

#define DEFAULT_AXIS_STEPS_PER_UNIT   {80,80,1280,486}

#define DEFAULT_MAX_FEEDRATE          {300,300,3,1000} 

#define DEFAULT_MAX_ACCELERATION      {800,800,100,10000}

#define DEFAULT_ACCELERATION          800

#define DEFAULT_RETRACT_ACCELERATION  3000    // E acceleration in mm/s^2 for retracts

#define DEFAULT_TRAVEL_ACCELERATION   800

#define DEFAULT_XYJERK                10.0

#define DEFAULT_ZJERK                 0.4


---
**ThantiK** *March 16, 2016 04:53*

The print head should home to the front left of your machine if everything is done correctly.  If it doesn't, flip the direction of the stepper motor (either wires, or through config) and move the endstop.


---
**Rick Sollie** *March 16, 2016 11:34*

**+Ted Huntington** thanks. When I get home I will compare with my configuration.h.  I did try reversing the motor, but ran into a problem with the homing switch.


---
**Rick Sollie** *March 18, 2016 10:17*

Found the solution. The X and Y motors were reversed as well as the x and Y endstops. Prints pretty darn  good now.


---
**Roland Barenbrug** *April 01, 2016 10:26*

**+Rick Sollie** can you share your Marlin configuration. Using Marlin with my Prusa works like a dream. With my Eustathios (in progress) I get the impression it is to slow create speed in the X and Y axis. Using 1.8 degree steppers and 1/16 micro stepping.


---
**Rick Sollie** *April 05, 2016 00:05*

I use .9 degree steppers  



#define SERIAL_PORT 0

#define BAUDRATE 250000

#ifndef MOTHERBOARD

  #define MOTHERBOARD BOARD_RAMPS_13_EFB

#endif

#define EXTRUDERS 1



#define TEMP_SENSOR_0 5

//e3dv6

#define TEMP_SENSOR_BED 7

#define MAX_REDUNDANT_TEMP_SENSOR_DIFF 10



// Actual temperature must be close to target for this long before M109 returns success

#define TEMP_RESIDENCY_TIME 10  // (seconds)

#define TEMP_HYSTERESIS 3       // (degC) range of +/- temperatures considered "close" to the target one

#define TEMP_WINDOW     1       // (degC) Window around target to start the residency timer x degC early.



// Otherwise this would lead to the heater being powered on all the time.

#define HEATER_0_MINTEMP 5

#define BED_MINTEMP 5

#define HEATER_0_MAXTEMP 250

#define BED_MAXTEMP 150

#define PIDTEMP

#define BANG_MAX 255 // limits current to nozzle while in bang-bang mode; 255=full current



#define ENDSTOPPULLUPS // Comment this out (using // at the start of the line) to disable the endstop pullup resistors



#ifndef ENDSTOPPULLUPS

  // fine endstop settings: Individual pullups. will be ignored if ENDSTOPPULLUPS is defined

  // #define ENDSTOPPULLUP_XMAX

  // #define ENDSTOPPULLUP_YMAX

  // #define ENDSTOPPULLUP_ZMAX

  // #define ENDSTOPPULLUP_XMIN

  // #define ENDSTOPPULLUP_YMIN

  // #define ENDSTOPPULLUP_ZMIN

#endif



#ifdef ENDSTOPPULLUPS

  #define ENDSTOPPULLUP_XMAX

  #define ENDSTOPPULLUP_YMAX

  #define ENDSTOPPULLUP_ZMAX

  #define ENDSTOPPULLUP_XMIN

  #define ENDSTOPPULLUP_YMIN

  #define ENDSTOPPULLUP_ZMIN

#endif



// The pullups are needed if you directly connect a mechanical endswitch between the signal and ground pins.

const bool X_MIN_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.

const bool Y_MIN_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.

const bool Z_MIN_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.

const bool X_MAX_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.

const bool Y_MAX_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.

const bool Z_MAX_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.

#define DISABLE_MAX_ENDSTOPS



#if defined(COREXY) && !defined(DISABLE_MAX_ENDSTOPS)

  #define DISABLE_MAX_ENDSTOPS

#endif

// For Inverting Stepper Enable Pins (Active Low) use 0, Non Inverting (Active High) use 1

#define X_ENABLE_ON 0

#define Y_ENABLE_ON 0

#define Z_ENABLE_ON 0

#define E_ENABLE_ON 0 // For all extruders



// Disables axis when it's not being used.

#define DISABLE_X false

#define DISABLE_Y false

#define DISABLE_Z false

#define DISABLE_E false // For all extruders

#define DISABLE_INACTIVE_EXTRUDER true //disable only inactive extruders and keep active extruder enabled



#define INVERT_X_DIR false    // for Mendel set to false, for Orca set to true

#define INVERT_Y_DIR false    // for Mendel set to true, for Orca set to false

#define INVERT_Z_DIR false     // for Mendel set to false, for Orcfdefine INVERT_X_DIRa set to true

#define INVERT_E0_DIR false   // for direct drive extruder v9 set to true, for geared extruder set to false

#define INVERT_E1_DIR false    // for direct drive extruder v9 set to true, for geared extruder set to false

#define INVERT_E2_DIR false   // for direct drive extruder v9 set to true, for geared extruder set to false



// ENDSTOP SETTINGS:

// Sets direction of endstops when homing; 1=MAX, -1=MIN

#define X_HOME_DIR -1

#define Y_HOME_DIR -1

#define Z_HOME_DIR -1



#define min_software_endstops true // If true, axis won't move to coordinates less than HOME_POS.

#define max_software_endstops true  // If true, axis won't move to coordinates greater than the defined lengths below.



// Travel limits after homing

#define X_MAX_POS 280

#define X_MIN_POS 0

#define Y_MAX_POS 280

#define Y_MIN_POS 0

#define Z_MAX_POS 580

#define Z_MIN_POS 0



#define X_MAX_LENGTH (X_MAX_POS - X_MIN_POS)

#define Y_MAX_LENGTH (Y_MAX_POS - Y_MIN_POS)

#define Z_MAX_LENGTH (Z_MAX_POS - Z_MIN_POS)



//Manual homing switch locations:

#define MANUAL_X_HOME_POS 0

#define MANUAL_Y_HOME_POS 0

#define MANUAL_Z_HOME_POS 0



#define NUM_AXIS 4 // The axis order in all axis related arrays is X, Y, Z, E

#define HOMING_FEEDRATE {50*60, 50*60, 4*60, 10}  // set the homing speeds (mm/min)



#define DEFAULT_AXIS_STEPS_PER_UNIT   {320,  320,  800, 944}  // default steps per unit for Ultimaker

#define DEFAULT_MAX_FEEDRATE          {500, 500, 5, 25}    // (mm/sec)

#define DEFAULT_MAX_ACCELERATION      {3000,3000,500,3000}    // X, Y, Z, E maximum start speed for accelerated moves. E default values are

#define DEFAULT_ACCELERATION          2000    // X, Y, Z and E max acceleration in mm/s^2 for printing moves

#define DEFAULT_RETRACT_ACCELERATION  2000   // X, Y, Z and E max acceleration in mm/s^2 for retracts



#define DEFAULT_XYJERK                20.0    // (mm/sec)

#define DEFAULT_ZJERK                 0.4     // (mm/sec)

#define DEFAULT_EJERK                 5.0    // (mm/sec)


---
**Roland Barenbrug** *April 05, 2016 07:48*

**+Rick Sollie**, thnx a lot. is almost equal to my configuration. Btw, using 0.9 steppers as well. Will first realign the gantry to make sure friction becomes as low as possible. Otherwise move to 1.8 60mm steppers that should bring a higher torque.


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/hmKiuM2VKaT) &mdash; content and formatting may not be reliable*
