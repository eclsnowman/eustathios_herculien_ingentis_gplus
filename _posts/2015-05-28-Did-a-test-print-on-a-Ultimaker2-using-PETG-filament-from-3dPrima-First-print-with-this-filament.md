---
layout: post
title: "Did a test print on a Ultimaker2 using PETG filament from 3dPrima First print with this filament"
date: May 28, 2015 14:30
category: "Show and Tell"
author: Eirikur Sigbjörnsson
---
Did a test print on a Ultimaker2 using PETG filament from 3dPrima



First print with this filament. Used 235°c and 110°c on bed (material is listed for 230-250°c). Minimal supports used, no raft and 60% infill



By far the best results Ive got printing this part. A very solid piece.. No warps even with this high infill %, and only slight flattening on the bottom probably because the bed is too hot (ABS settings used). I probably can also lower the heat on the printhead, I just wanted to stay within the reccomended parameters for the first print.



link to the materal:

[http://www.3dprima.com/en/filaments-for-3d-printers/special-filament-3mm/3d-prima-premium-petg-filament-3mm-1-kg-spool-blue.html](http://www.3dprima.com/en/filaments-for-3d-printers/special-filament-3mm/3d-prima-premium-petg-filament-3mm-1-kg-spool-blue.html)



Probably will use this rather than ABS to print my parts for my Herculien build



![images/282e9490dd284e522696488c9738cfb2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/282e9490dd284e522696488c9738cfb2.jpeg)
![images/def1c7dfa439441c0bb4a7f4a7dadbe7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/def1c7dfa439441c0bb4a7f4a7dadbe7.jpeg)
![images/881cf31f13724d7032cc756094e06719.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/881cf31f13724d7032cc756094e06719.jpeg)

**Eirikur Sigbjörnsson**

---
---
**Eric Lien** *May 28, 2015 16:34*

Looks very similar to some blue microcenter (esun) PETG I have been printing recently. I am a big fan of PETG now... But it sure does make a mess of the outside of the nozzle if it comes in contact with strings.


---
**Gústav K Gústavsson** *May 28, 2015 19:06*

Talking about making a mess, after Eiríkur left I was printing some cubes with this material which we have used to tune in heating and bed temp. Is also printing at more speed. Lowered the bed to 90 degrees. Tried 220 degrees head temp, didn't go well (reel said 230-250) tried 230 and seemed to be Ok until infill it was not solid and was curling up, tuned to 235 and got a lot better. Left for an hour... And this was what the result was when I got back.   Well if I could figure how to send picture in a comment on iPad..... No.... Have to send picture in a separate post.??


---
**Eirikur Sigbjörnsson** *May 28, 2015 19:49*

Just before the printer started my print i ran a wet tissue over the glass. Makes the material stick much better


---
**Øystein Krog** *May 28, 2015 21:42*

Do you guys think PETG is viable as an ABS replacement for heat-sensitive stuff (extruder mount etc.)?

I've come to really hate ABS warping/shrinking on large parts.


---
**Eirikur Sigbjörnsson** *May 28, 2015 22:26*

You need more heat to print with it so I  think it should work. I used 225°c for the ABS but needed 235°c for the PETG on the same printer. At least there is a good reason to try it.


---
**Gústav K Gústavsson** *May 28, 2015 23:36*

Have to agree with Eiríkur, when the bed is heating up I always wipe it with a tissue with tap water. Prints which failed and needed raft stick like hell. Didn't do that on the last print and it failed because the part got loose. Mind you this is Icelandic tap water with all the volcanic minerals so result may be different for you. But Hips looks promising, will try more tomorrow and do some measurement regarding shrinking.


---
**Gústav K Gústavsson** *May 28, 2015 23:39*

Ok thats PETG not Hips in the last post. Nearly midnight here and I must be getting sleepy.﻿


---
**Gústav K Gústavsson** *May 29, 2015 00:07*

If Eiríkur lets me, his material i'm squandering.


---
*Imported from [Google+](https://plus.google.com/118262882256504121671/posts/BrM4sXUSj8W) &mdash; content and formatting may not be reliable*
