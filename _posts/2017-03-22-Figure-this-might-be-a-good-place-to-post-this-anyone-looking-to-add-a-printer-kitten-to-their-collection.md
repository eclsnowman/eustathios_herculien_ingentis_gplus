---
layout: post
title: "Figure this might be a good place to post this, anyone looking to add a printer kitten to their collection?"
date: March 22, 2017 23:31
category: "Discussion"
author: Brad Hill
---
Figure this might be a good place to post this, anyone looking to add a printer kitten to their collection? I had Patrick build one up for me but I'm just swimming in printers. Only changes made have been swapping out the titan for bondtech mini, changing the firmware steps and printing out the gold benchy pictured. Would love it if someone had a use for it :) I'm happy to let it go for a good price, was thinking $495. I'm not sure what the state of the project is, but the github stuff is all still there. It's the printer from this post: [https://plus.google.com/105660857457447815256/posts/aT1mzr7Knmj](https://plus.google.com/105660857457447815256/posts/aT1mzr7Knmj)



Thanks! Brad



![images/05a967e2c7f89111e7f1a6fab1427625.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/05a967e2c7f89111e7f1a6fab1427625.jpeg)
![images/1fef6af7bd1174af0f8a49c61209092f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1fef6af7bd1174af0f8a49c61209092f.jpeg)

**Brad Hill**

---
---
**Eric Lien** *March 23, 2017 02:24*

I might be interested. Not that my wife would let me buy another one (she thinks 5 is too many). But just because I saw it in person last year and I know how high the quality is. You coming to MRRF?


---
**Ray Kholodovsky (Cohesion3D)** *March 23, 2017 03:21*

On a similar tangent, do we know if Patrick is coming to MRRF? I came across this design a few days ago and would love to see it in person. 


---
**Zane Baird** *March 23, 2017 13:13*

I do love this printer. It's built like a rock and the print quality is some of the best I've seen


---
**Brad Hill** *March 23, 2017 16:48*

**+Eric Lien** sadly not this year, hoping for next year, my Culver's withdrawals are starting to get worse! **+Ray Kholodovsky** not that I know of, I think that due to the lack of generating good sales they have archived the project for the most part. Maybe it's the Tantillus curse! more than likely it's just hard to sell a smaller printer when most consumers are only looking at $ divided by print area.


---
**Ray Kholodovsky (Cohesion3D)** *March 23, 2017 16:51*

**+Brad Hill** ironically the reason this caught my eye is that it seemed a simpler and more affordable implementation of DICE 


---
**Patrick Woolfenden** *March 23, 2017 18:17*

**+Brad Hill** **+Eric Lien** **+Ray Kholodovsky** Hey guys. I'm not going to MRRF this year unfortunately. Trying to make money selling the kitten wasn't making the most sense when you take into account the time it takes to make each one and the cost of the components. Plus it became difficult to find buyers because of the high cost. Matt and I also got pretty busy with other projects ect. That being said, I still think the kitten is a great printer for someone looking for a project and a small portable PLA printer. It’s definitely a solid little printer. I’ve carried it onto airplanes many times. [Printerkitten.com](http://Printerkitten.com) is down, but we plan to keep the github up and will get the pictures working again for the assembly instructions soon.


---
**Patrick Woolfenden** *March 23, 2017 18:20*

**+Brad Hill** I hope you find a good home for it. That 3dBenchy looks great. You’ve really got it dialed in. If anyone is wondering, $495 is a great price. Well under the BOM cost if you were to build one yourself.


---
**Eric Lien** *March 23, 2017 18:58*

**+Patrick Woolfenden** I can attest to what you are saying. I reviewed the BOM in detail. All high quality components. You did a great job.


---
**Amit Patel** *March 25, 2017 10:50*

**+Brad Hill** if you still have the printer for sale and **+Eric Lien** is no longer interested I'll go ahead and purchase it if you don't mind shipping to Florida


---
**Eric Lien** *March 25, 2017 16:50*

**+Amit Patel** please do. I don't need another, just didn't want to see it go to waste. It's an exception printer, you will be very happy.


---
**Brad Hill** *March 25, 2017 17:47*

**+Amit Patel**​ Florida shipping no problem, shoot me an email unsped@[gmail.com](http://gmail.com) 


---
**Amit Patel** *March 26, 2017 04:41*

Thx **+Brad Hill** sent u an email


---
*Imported from [Google+](https://plus.google.com/117781012580355961928/posts/SXmzyGdcrYK) &mdash; content and formatting may not be reliable*
