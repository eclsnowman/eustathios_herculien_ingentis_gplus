---
layout: post
title: "One thing i have found is working with this Extrusion is a pain in the tail pipe"
date: April 23, 2014 11:22
category: "Show and Tell"
author: Wayne Friedt
---
One thing i have found is working with this Extrusion is a pain in the tail pipe.



![images/442812ed713c22e6bbab9f4492dbd6e9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/442812ed713c22e6bbab9f4492dbd6e9.jpeg)
![images/037a94fb5bcb8cfe97408dfd1c63a8d7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/037a94fb5bcb8cfe97408dfd1c63a8d7.jpeg)
![images/ea39b976fd4e2b9d1f160974ea1c30cf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ea39b976fd4e2b9d1f160974ea1c30cf.jpeg)
![images/67516a8836dfd72c79d8c241afcfe2db.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/67516a8836dfd72c79d8c241afcfe2db.jpeg)
![images/6ab5b0c305af3700ab4685e74c025499.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6ab5b0c305af3700ab4685e74c025499.jpeg)

**Wayne Friedt**

---
---
**Lynn Roth** *April 23, 2014 11:39*

**+Wayne Friedt** - what issues are you having? I haven't worked with Al extrusion before, but I got some to start working on a new printer....


---
**Carlton Dodd** *April 23, 2014 11:58*

I thought extrusion t-slot was supposed to be easier than other material options. 

Is it just that it's hard to square up? 


---
**Mike Miller** *April 23, 2014 12:01*

Man, that thing's a MONSTER! Is the frame solid?


---
**Wayne Friedt** *April 23, 2014 12:14*

**+Lynn Roth** It is mainly the little 4nn screws and the TEE nuts. These are turn left and they will come out and turn right and they will Tighten down. Thats the plan of how they work but it is hard to get them to lock. Have to unscrew the screw to the almost fall off point before the may or may not catch to tighten. If you go to far they may come unscrewed and slide out. 


---
**Wayne Friedt** *April 23, 2014 12:16*

**+Carlton Dodd** Ya that's what i thought. Its just the screws are hard to get to tighten. I don't know if all extrusion is this way, or the new stuff we been seeing on Kickstarter but this kind is not easy to get tightened up. Need 4 hands. At least for me. 


---
**Wayne Friedt** *April 23, 2014 12:18*

**+Mike Miller** Ya is big. 24x24x24 To big really but its not for me.  Not solid yet. I hope later it will be


---
**Mike Miller** *April 23, 2014 12:44*

Looks like it needs gussets or diagonal braces...


---
**Wayne Friedt** *April 23, 2014 12:48*

Yes but i am not to that point yet.


---
**Wayne Friedt** *April 23, 2014 13:04*

**+Eric Lien** You see how the belts are not aligned straight, one on each side. Is that going to be a problem?


---
**Liam Jackson** *April 23, 2014 14:08*

I used normal square nuts and slid them in rather than those fiddly ones. Had a few that were too thick to slide in but most fit fine! 


---
**Eric Lien** *April 23, 2014 14:57*

**+Wayne Friedt** I think there may be tension issues. The optimal length and tension would change (I Think) depending on the position of the gantry back and forth in x and Y since things are not aligned. Also the force of the belt being so high on the y axis carriages may lead to issues because the fulcrum they have down to the x rods. The rods don't engage much into the carriage (mine either) so when you get good/high tension on the belts the carriage will want to tip down.


---
**Matt Miller** *April 23, 2014 16:00*

**+Wayne Friedt**  Your belts must be absolutely parallel on X and Y - I would expect racking due to cosine error once they're under significant tension.  And a machine this size should really be moved up to something like HFS5-4040 for the pillars and main members.  That much mass, that far off the ground is going to be an issue at even modest speeds.  Seriously - reinforce the frame to semi-ludicrous amount, especially if you're going with printed gusssets.  Strongly consider aluminum gussets.  And the belt bath can probably be fixed by moving to a larger toothed gear on the Y-end and then pushing it's position away from the center of the Y-axis.  **+Eric Lien**   is absolutely correct in pointing towards an XY axis that occupies the same plane as the belts.  You should check this thread: [http://forums.reprap.org/read.php?4,297740](http://forums.reprap.org/read.php?4,297740)



I've been working on a similar coreXY machine in CAD and the refinements I've had to make for alignment issues to maximize the XY print envelope have been pretty ugly.  


---
**Eric Lien** *April 23, 2014 17:43*

**+Wayne Friedt** I see the cause now. You have only have 1 corner idler. So at least one of the belts (coming off the stepper to the idler, or off the corner idler to the cart idler) will then have to be misaligned.﻿


---
**D Rob** *April 23, 2014 19:53*

**+MISUMI USA** has some tee nuts that slide in so they must be placed before covering the channel end and they are cheaper than the drop-ins


---
**Matt Miller** *April 23, 2014 20:04*

**+MISUMI USA**  HNKK5 are the pre-assembly nuts, and buy more than 1 bag - quantity discounts start @2 $20 for 1 bag, $30 for 2...



And you can buy the post-assembly HNTA5's in bulk - $33 for 100 


---
**Wayne Friedt** *April 23, 2014 22:40*

**+Liam Jackson** I can see some benefits using square nuts. If you needed to add a nut to a closed run would the only way to do so is to disassemble the frame to a point they could be slid it?


---
**Wayne Friedt** *April 23, 2014 22:40*

**+D Rob** Thanks.


---
**Wayne Friedt** *April 23, 2014 22:43*

**+Mike Miller** Thanks for the input,


---
**Wayne Friedt** *April 23, 2014 22:47*

**+Eric Lien** I think i am going to fix the idler issue, add in 2 more to square up the belts and see how things look after that. I am not sure what to do about the belts being so much higher above the rods. I may just go with that for now and see how that turns out as-well. Thanks much.


---
**Eric Lien** *April 23, 2014 23:25*

**+Wayne Friedt** its a great looking printer. And I am sure it will end up running well. Love the size.



How much of the rod is inside the y carriages? That will be the thing that determines if it flexes I think. I think I didn't have mine engage enough. Also I should have had it clamp with a bolt instead of press fit into the carriage.﻿


---
**Wayne Friedt** *April 24, 2014 04:19*

**+Eric Lien** The X rods go all the way through the side carriages. 


---
**Liam Jackson** *April 24, 2014 07:39*

**+Wayne Friedt** yeah that's the only pain, or you can use the fiddly ones to add upgrades, I planned for upgrades so have two spare nuts in each section. Some of my upgrades just use printed clips anyway (spool holder, RPi). 


---
**Wayne Friedt** *April 24, 2014 07:45*

**+Liam Jackson** Good idea.


---
*Imported from [Google+](https://plus.google.com/+WayneFriedt/posts/BKvF9JbR61j) &mdash; content and formatting may not be reliable*
