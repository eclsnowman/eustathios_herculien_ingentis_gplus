---
layout: post
title: "Eric Lien did you decide on a new home for the BOM for Eustathios V2 (and HercuLien for that matter)?"
date: March 27, 2015 18:36
category: "Discussion"
author: Seth Messer
---
**+Eric Lien** did you decide on a new home for the BOM for Eustathios V2 (and HercuLien for that matter)? I ask because I checked to see if there were updates to the Eustathios V2 repo and saw this commit: [https://github.com/eclsnowman/Eustathios-Spider-V2/commit/019a181e6f23987f14db8ea5f950ce1d6d5c3ecc](https://github.com/eclsnowman/Eustathios-Spider-V2/commit/019a181e6f23987f14db8ea5f950ce1d6d5c3ecc)



Would you be able to share exactly which extrusion descriptions were changed? I ask because my Misumi order is on its way and I'm curious if I purchased the wrong things and need to return/re-order parts based on the changes you made.



Thanks!





**Seth Messer**

---
---
**Isaac Arciaga** *March 27, 2015 18:56*

**+Seth Messer** The only difference in the extrusions for V2 is that the 4 verticals have the holes drilled 80mm from the bottom end. V1 has the holes at 100mm. the "AP80" at the end of the part number indicates this.



If I missed something hopefully someone else can chime in.





*Edit: The V1 verticals will still work with V2. You just don't gain the potential 20mm on Z.


---
**Seth Messer** *March 27, 2015 19:06*

**+Isaac Arciaga** I can just drill at the 80mm mark too, though, right?


---
**Eric Lien** *March 27, 2015 22:01*

If you ordered off the v2 BOM you are good. The part numbers were correct, but I transposed two numbers on one of the descriptions. That is what I corrected in that commit.



I will be looking into making the BOM a google doc, but I am taking a little break for a few weeks. My family (wife in particular) has been very understanding of my time commitment to these designs. But the weather is getting nice, so I will be trying to spend more quality time with the family in the coming weeks.


---
**Seth Messer** *March 27, 2015 22:25*

understood completely! i sent you the BOM as a google doc btw.


---
**Eric Lien** *March 27, 2015 22:48*

**+Seth Messer** any chance you added the product links? ;)


---
**Seth Messer** *March 27, 2015 22:51*

Hah. Doing that for robot digg even as we speak. But had to leave for a couple hours. Will finish it up tonight. If I submit a PR for adding th link to GH, will you be good with merging it this wweekend?


---
**Eric Lien** *March 27, 2015 23:19*

**+Seth Messer** I will, but I will want to review it. The BOM is kinda sensitive since a mistake could cost people money. Trust me I really worry about that when I release my files. I would hate to screw up to the tune of hundreds of dollars for another individual.



Also looking at the google doc it looks like the pivot table page summarizing the parts by vendor did not fair well. If I convert the BOM to google docs that will take some heavy review.


---
**Seth Messer** *March 28, 2015 04:05*

**+Eric Lien** Understood completely on that. So, I'd love it if you could verify the 4 comments I put on the google doc version. I updated all of the robotdigg items with correct part numbers and links. i changed one of the robotdigg parts (6900 bearings) to amazon and a separate part number and link.


---
**Seth Messer** *March 28, 2015 04:05*

A few of those comments are needing some details to put the final part number and link. /cc **+Eric Lien** 


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/d5KskGDcLkP) &mdash; content and formatting may not be reliable*
