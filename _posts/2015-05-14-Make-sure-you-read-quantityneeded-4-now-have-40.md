---
layout: post
title: "Make sure you read quantity...needed 4 now have 40...."
date: May 14, 2015 23:24
category: "Show and Tell"
author: Derek Schuetz
---
Make sure you read quantity...needed 4 now have 40....

![images/e4a2eb635a4a14d76ef6aea66ff93d28.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e4a2eb635a4a14d76ef6aea66ff93d28.jpeg)



**Derek Schuetz**

---
---
**Dat Chu** *May 14, 2015 23:49*

Are those lmu10? I am sure someone on here will be able to use them. 


---
**Derek Schuetz** *May 14, 2015 23:52*

yep


---
**Eric Lien** *May 15, 2015 00:06*

I see a new project in your future.


---
**Eric Lien** *May 15, 2015 00:08*

**+Derek Schuetz**​ BTW what are the lm10uu bearings for? I hope not the Eustathios, cause those are lm10luu. The extra l makes it the long version (55mm I think).﻿


---
**Derek Schuetz** *May 15, 2015 00:19*

Well shit...**+Eric Lien** could I use 2 of these instead of 1 of the long versions? There 30mm each so an extra 5 mm


---
**Gus Montoya** *May 15, 2015 01:00*

@Derek Schuetz , Eric mentioned that won't work. Bummer.


---
**Gus Montoya** *May 15, 2015 01:00*

@Derek Schuetz , Eric mentioned that won't work. Bummer.


---
**Eric Lien** *May 15, 2015 01:00*

**+Derek Schuetz** my guess is two would just cause binding from misalignments. But you can always test it.


---
**Derek Schuetz** *May 15, 2015 01:10*

ok i just ordered the correct one from a local CNC store....now what will i do with 40 LM8UU bearing...


---
**Eric Lien** *May 15, 2015 01:14*

Design a 10 axis robot😉


---
**Gus Montoya** *May 15, 2015 04:04*

Or you can build some smaller 3d machines like the pursa and sell them. Or a mini eustathios. I'm sure you will sell.


---
**Mike Miller** *May 15, 2015 12:40*

And the funny thing is: 40 isn't crazily more expensive than 4.  Part price from US store: $4/piece x 4 + $5 shipping = $21....Part price from Ali Express: $0.75 X 40  + $10 shipping = $40



I was looking at Acme Rod and needed 4...The price for 10 was less than twice the price for 4 ($200 shipped vs. $120 shipped)....But I didn't need 10, and have no desire to go into business as a 3D printer part supplier. 


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/ixrfrsG5juv) &mdash; content and formatting may not be reliable*
