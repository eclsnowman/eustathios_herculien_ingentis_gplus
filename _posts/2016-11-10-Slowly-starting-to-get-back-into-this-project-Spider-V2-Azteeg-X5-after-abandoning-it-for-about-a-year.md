---
layout: post
title: "Slowly starting to get back into this project (Spider V2, Azteeg X5) after abandoning it for about a year"
date: November 10, 2016 15:39
category: "Discussion"
author: Bryan Weaver
---
Slowly starting to get back into this project (Spider V2, Azteeg X5) after abandoning it for about a year.  I got everything built mechanically, but when it came time to set up the controller and get everything working on the software side, I completely lost focus and shut it down.  Anyway, I'm back on it now. Time to get this thing printing.



First question (don't worry, there will be many more to come), what microstepping settings have worked best for you guys?  I've done a little searching and it seems like most people are using 1/16 for the extruder, but i'm having a hard time finding anything for X, Y, and Z.  I'm sure it varies depending on who you ask, just looking for a place to start.





**Bryan Weaver**

---
---
**Zane Baird** *November 10, 2016 15:56*

1/16 is pretty good all around. I run my delta on an Azteeg X5 at 1/32 and its insanely quiet. But that doesn't require the same amount of torque from the motors as on the Spider. I'd try both 1/16 and 1/32 and see how performance changes. Having a quite running printer is quite amazing though...




---
**Eric Lien** *November 10, 2016 16:09*

I have tended to run my Z at lower microstepping (atleast on HercuLien because of the heavier bed). The pulley reduction mixed with the screw reduction makes the steps/mm so high if you go with higher microstep on Z.


---
**Bryan Weaver** *November 11, 2016 17:14*

**+Eric Lien** Now I'm trying to wrap my head around steps/mm calculations, feels like i'm taking crazy pills.  In the config file, steps/mm is set to 160 for X and Y, which appears to be dead on for my printer.  



Where i'm getting lost on the calculations is the gear reduction from  the 20-tooth pulley on the stepper to the 32-tooth pulley.  The reprap step calculator tells me a stepper with 200 steps per revolution with 1/16 microstepping and a 20-tooth pulley works out to 80 steps/mm.  If that's the case, wouldn't the other pulley need to be 40-tooth to double the steps/mm and end up at 160 steps/mm?


---
**Eric Lien** *November 11, 2016 18:18*

The two 32 tooth pulleys cancel each other. You need to change the ratio between the first and second (driven and drive) 32tooth pulleys to increase ratio in the mechanical system. It's difficult to wrap your head around. But if you write out the formula long hand you can see the numerator and denominator of how the two(2)- 32 tooth pulleys drive the steps/mm.


---
**Bryan Weaver** *November 11, 2016 19:37*

So that means steps/mm is only being determined by the 20-tooth pulley on the stepper?  If that's the case, does that mean i'm actually running 1/32 microstepping instead of 1/16 like I thought, to end up at 160 steps/mm?  Do I need to double-check my jumpers?


---
**Eric Lien** *November 11, 2016 20:13*

Yes you are running at 1.8deg steppers at 1/32 microstepping (or using 0.9deg steppers at 1/16 microstepping).


---
*Imported from [Google+](https://plus.google.com/111820797809026464429/posts/K3F8GT5TKwv) &mdash; content and formatting may not be reliable*
