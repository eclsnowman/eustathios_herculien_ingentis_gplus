---
layout: post
title: "I haven't posted in a while. Thought I would share my Iteration Bin"
date: September 16, 2014 15:55
category: "Show and Tell"
author: Eric Lien
---
I haven't posted in a while. Thought I would share my Iteration Bin. Some are items I should have measured twice printed once, some are design changes based on how it performed, and some are the change from corexy to #HercuLien. But all of them are a learning experience. 



Anyone know If I can just put these in single sort recycling?

![images/3486b961b7feb69c2ce280611fa06346.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3486b961b7feb69c2ce280611fa06346.jpeg)



**Eric Lien**

---
---
**Mike Miller** *September 16, 2014 16:21*

Is it just me, or does Orange seem to be a great color to print? I've had really good luck with <b>cough*Makerbot*cough</b> Orange. 


---
**Eric Lien** *September 16, 2014 16:54*

**+Mike Miller** this is actually pushplastic red abs. They recently adjusted the color addatives to make it more vibrant. The sample they sent me is a much more true red after printing.



I have recently been printing the Inland brand $15 spools I got from Microcenter. The Inland abs smell is much worse than pushplastics, but Inland printing temps are lower and filament consistency is up there with the best I've used (so far anyway).


---
**Brandon Satterfield** *September 16, 2014 17:40*

**+Eric Lien** I found a not so responsible way of getting rid of my "test prints", through the years there's been a number of them. :-)

I let my 9 yo son bring his friends over and they dig through the box and their imaginations take charge. 

He now has friends that come over and the first thing they ask, "can we dig through your dad's 3d printed box?" Ha


---
**Eric Lien** *September 16, 2014 20:14*

**+Brandon Satterfield** I know what you mean. I have printed elaborate articulating toys for them... Only to have them play with a failed x/y gantry piece instead. 


---
**Brandon Satterfield** *September 17, 2014 02:39*

Hahaha. 


---
**Jean-Francois Couture** *September 17, 2014 12:05*

I too have a trash can to keep the failed parts.. I hope in the near future to make a machine to refill the empty spools laying around my office !  


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/X7wCGZzT49n) &mdash; content and formatting may not be reliable*
