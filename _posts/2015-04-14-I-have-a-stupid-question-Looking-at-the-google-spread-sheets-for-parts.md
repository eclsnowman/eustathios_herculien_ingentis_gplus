---
layout: post
title: "I have a stupid question. Looking at the google spread sheets for parts"
date: April 14, 2015 17:16
category: "Discussion"
author: Gus Montoya
---
I have a stupid question. Looking at the google spread sheets for parts. I want to be clear that I don't order parts twice. In the picture I have provided. Is 3.1and 3.2 the same ? And also what is the difference between XY_Axis_A and XY_Axis_A (Driven Version/Configuration)? 

![images/a9a7efc055546fb01c341268f0a1a23a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a9a7efc055546fb01c341268f0a1a23a.jpeg)



**Gus Montoya**

---
---
**Eric Lien** *April 14, 2015 17:54*

One has a pass through bearing block for the longer rod to go through out to the stepper for driving the axis.﻿ If you look at the descriptions it should make sense (I think).﻿


---
**Gus Montoya** *April 14, 2015 18:12*

Thank you **+Eric Lien** for your response. Ok, I'll look at the linear rods I have already to determine what I need. 


---
**Gus Montoya** *April 14, 2015 19:01*

Sorry I just realized that both linear rod length's are the same. So this difference is dependent on builder preference? 


---
**Eric Lien** *April 14, 2015 20:22*

**+Gus Montoya** no the rod lengths are not the same. I think you need to take a little time and study the BOM in detail. Also look at the summary tab (a pivot table of the sub-assemblies), that makes things a little more clear.


---
**Gus Montoya** *April 14, 2015 20:39*

I'll print it all out, I guess my eyes are getting too old for monitors. Heh


---
**Eric Lien** *April 14, 2015 22:30*

**+Gus Montoya** I hope I didn't sound too short I was at work. But since several of the cut parts can be a little pricey, it would be best to familiarize yourself fully with the BOM and the model before ordering to avoid any costly mistakes. There is several free formats like edrawings viewer and 3d-pdf which would be good to look around the model when you have questions.


---
**Gus Montoya** *April 14, 2015 23:08*

Not at all **+Eric Lien**, I'll study the BOM.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/fAkBQrTSvYi) &mdash; content and formatting may not be reliable*
