---
layout: post
title: "Does anyone know a trick how to order at Misumi as an private Person?"
date: December 05, 2014 13:16
category: "Discussion"
author: Florian Schütte
---
Does anyone know a trick how to order at Misumi as an private Person?





**Florian Schütte**

---
---
**ThantiK** *December 05, 2014 13:18*

Just order. You don't need to be a company. 


---
**Florian Schütte** *December 05, 2014 13:19*

On phone and mail i got an other information from them :(

Then i will try and error :)


---
**Florian Schütte** *December 05, 2014 13:21*

My be they are more narrow minded when it comes to this here in germany (misumi europe).


---
**Miguel Sánchez** *December 05, 2014 14:32*

**+Florian Schütte** you may use Motedis.de instead ( much cheaper, provided you want aluminium extrusions)


---
**Florian Schütte** *December 05, 2014 15:10*

The only reason for me to order at misumi are the 32T 10mm pulleys without collar.


---
**Miguel Sánchez** *December 05, 2014 15:13*

I was afraid that could be the case. You can get the ones from robotdigg, get rid of the collar and glue them to the rods (dirty job I know).


---
**Florian Schütte** *December 05, 2014 15:16*

That is one way i also thought about. I allready ordered at robotdigg. I think i will give it a try. Im tired of searching this pulleys :)


---
**D Rob** *December 05, 2014 17:27*

Claim you are a business. ﻿ let you conscience be your guide.


---
**Eric Lien** *December 05, 2014 17:42*

**+D Rob** yup. I am Lien3D to many places ;)


---
**Mike Miller** *December 05, 2014 20:02*

I'm using the robotdigg pullies unmachined, they take up a nontrivial amount of envelope. :(


---
**D Rob** *December 05, 2014 20:08*

The perk of screws is there are pockets in each corner that are unusable that are around the right size for my pulleys.


---
**Miguel Sánchez** *December 05, 2014 20:09*

I guess a metal saw, a 10mm rod and a hand drill can be used to split the collar from the pulley. My guess is the pulley can be superglued to the rod, but I haven't done that myself.


---
**D Rob** *December 05, 2014 20:13*

Clamp on a small section of 10mm rod with the set screw. Lock that in the drill press. Use a hack saw like a cut off tool. Done!


---
**Eric Lien** *December 05, 2014 22:13*

**+Mike Miller**​ true but the motor mounts would require being redesigned because the belt would be in the wrong spot by the distance of the collar.﻿


---
**Mike Miller** *December 06, 2014 04:44*

**+Eric Lien** That wouldn't bother me much. I'm really not all that precise. It's also why I've got plywood holding down the motors/controllers/power supply...easy to re-drill and move things around. 


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/22mEWNJLduP) &mdash; content and formatting may not be reliable*
