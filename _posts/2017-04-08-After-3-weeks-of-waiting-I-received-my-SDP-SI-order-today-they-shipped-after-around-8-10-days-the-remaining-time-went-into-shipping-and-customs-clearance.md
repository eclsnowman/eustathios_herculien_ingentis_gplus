---
layout: post
title: "After 3 weeks of waiting I received my SDP/SI order today (they shipped after around 8-10 days, the remaining time went into shipping and customs clearance)"
date: April 08, 2017 16:17
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
After 3 weeks of waiting I received my SDP/SI order today (they shipped after around 8-10 days, the remaining time went into shipping and customs clearance). Quite an expensive stunt. Their intl. shipping is expensive, given the small package and weight so i paid a little under $100 for everything including import tax. I ordered 10 x 10mm and 6 x 8mm to have a few in spare.



After a first test on some spare rods I'm impressed to see that their tolerance is around the same as with the IGUS that I use on the carriage (can only say for the 8mm of course). They seem to be as sensible to misalignment as the chinese graphite bushings but have the self aligning outer ring of course. 



If this wasn't soo much work and I had so little time I'd really like to see a 1by1 comparison between the SDP/SI and the Igus. They do cost about the same (though at least in Germany as Igus is german, they are cheaper and easier to source)﻿



![images/1e2271d04094d303d4861b883f42008a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1e2271d04094d303d4861b883f42008a.jpeg)



**Frank “Helmi” Helmschrott**

---


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/MKnugQX7caa) &mdash; content and formatting may not be reliable*
