---
layout: post
title: "Quick video of Techne printing snowflakes @80/100mm/s"
date: January 01, 2015 09:32
category: "Discussion"
author: Jarred Baines
---
Quick video of Techne printing snowflakes @80/100mm/s.



She works beautifully when everything's going well but when there are curling parts / high blobs the carriage snags and looses steps.



I'm not new to 3D printing in any way, these blobs should not cause lost steps, I just don't have the torque necessary to overcome them.



The machine is mechanically free and glides easily without motors attached. Motors have no trouble printing these snowflakes at something like 1500-2000mm/s^2 acceleration, but it just lacks torque.



I am using 1.7A .9deg steppers (ingentis BOM) and DRV8825's (found the large drivers I had were causing problems). Motors are connected directly to the shaft and I'm using 32t pulleys to move the carriage. Used a multimeter to set the Vref on the pololu's to .8v giving 1.6A if I know what I'm doing... (I may not)



Do you guys have speed at the expense of torque? or you do have enough to overcome small blobs?



I do suppose my setup is EXTREMELY rigid, perhaps too rigid - the carriage and all connecting components are aluminium, so they don't have any 'give', and my table is supported by some fairly heavy (12mm) springs. On my old reprap mendel if there was a 'snag' the table leveling springs would 'give' and the table would drop to allow the head to pass over the blob.



Just looking for peoples thoughts, I feel like I should be able to achieve enough torque to deal with snags if I can print at speeds like this? even at 50% or less of these speeds the snags happen, I'm convinced it's not purely an acceleration / speed thing.





**Jarred Baines**

---
---
**Mark “MARKSE” Emery** *January 01, 2015 10:02*

I've seen blobs form when the extrusion rate is slightly too high when printing fast at 0.1 layer heights. It's as if pressure builds up in the head then "blows" leaving a tiny blob. That blob then grows as layers are printed over it. Try backing off the extrusion rate ever so slightly.


---
**Eric Lien** *January 01, 2015 12:13*

I had the same issues with 0.9deg steppers, dvr8825 drivers, 1/16 microstep, and 32tooth stepper pullies.



I found I have far less catch issues by swapping to a 20 tooth stepper pulley (leaving the 32 on the rod of course).



Next step would be ditch the 0.9deg wanati steppers for some higher torque 1.8deg kysan or maybe the 60mm robotdigg steppers.﻿


---
**Mike Miller** *January 01, 2015 16:14*

I thought Vref was supposed to be 1.2v? 


---
**Jarred Baines** *January 02, 2015 09:25*

Thanks for the input guys! I will try a few things out - I don't think over extrusion is the be-all-and-end-all answer as an overhang that curves up slightly is just as much of a problem and the quality is extremely good when it doesn't snag...



I never liked the idea of having a ratio but I may make up some brackets to test with some 20t pulleys I have... Will need belts too tho >_<



I've also been avoiding softer springs... The Mendel's bed would "wander" on those springs and give slight errors... I like the idea of a fixed bed really, but as you say, SOMETHINGS gotta give... Probably worth softening them a bit...



Cheers guys ;-)


---
**Jarred Baines** *October 10, 2015 14:58*

So - revisiting this post since someone just shared it elsewhere and reminded me that it existed...



I solved the problem the easy and expensive way... Nema 23 motors... They're kicking butt - even with my drv8825 drivers... I have some Wantai DQ420MA drivers I can hook up to give them the full-grunt they deserve but at this point they have solved the problem and I can print with small curl-ups and no worries and travel at 250-300mm/s - that will do just fine :-)


---
**Eric Lien** *October 10, 2015 16:53*

**+Jarred Baines** wow 300mm/s. I concede defeat, your crown is available at the reception desk ;) now you gotta show a video, just use a high speed camera so its not just a blur ;)


---
**Jarred Baines** *October 25, 2015 13:54*

Ha ha :-)



Nema 23s directly coupled to the shafts. And 32 tooth pulleys, give me 64mm per rev, works out to 100 steps per mm at 1/32 microstepping. I really have to get those 1/128 microstep drivers tho, my bot is Aluminium almost exclusively so its super rigid and the only gripe I have is that I can see "step" marks on my prints!﻿


---
*Imported from [Google+](https://plus.google.com/+JarredBaines/posts/Uodquhvk1i3) &mdash; content and formatting may not be reliable*
