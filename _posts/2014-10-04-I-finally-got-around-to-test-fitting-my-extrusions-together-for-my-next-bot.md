---
layout: post
title: "I finally got around to test fitting my extrusions together for my next bot"
date: October 04, 2014 09:06
category: "Show and Tell"
author: James Rivera
---
I finally got around to test fitting my extrusions together for my next bot.  They are 800mm & 450mm long (Misumi HFS5-2020-800 & HFS5-2020-450).  For an easy visual size comparison I put my Printrbot Plus v2 inside it.



It feels pretty darn rigid, too, and it should get better once I put the rest of the corner brackets on it.  But I will probably want to order 2-4 more 450mm pieces (or 2x 450mm and 2x 410mm?) to make it a little bit stiffer.



I have to say, I had soooo much fun putting this together!  I felt like a kid with a freshly opened gift of an erector set!  Which makes complete sense, because aluminum extrusions are like the adult version of it.  Good times...

![images/9cc8ebb4a68d66249df17d7a49c9fa6a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9cc8ebb4a68d66249df17d7a49c9fa6a.jpeg)



**James Rivera**

---
---
**Eric Lien** *October 04, 2014 12:13*

Nice.


---
**Eric Lien** *October 04, 2014 15:02*

Is there a reason you didn't butt the horizontals to the verticals? You can avoid the angle brackets that way. The ends need to be square to work well. It looks like you did it on the horizontals to connect to another. You can gain 20mm in X and Y this way.



Just an idea.


---
**Brandon Satterfield** *October 04, 2014 15:14*

HUGE! Assuming it's printer you planning on piecing a life size Eiffel Tower together? ;-)


---
**James Rivera** *October 04, 2014 17:38*

**+Eric Lien** Yes. That reason is: newb. :)



This is my first foray into extrusions, so the quantity and size were chosen semi-randomly. This is also why I didn't put all the brackets on yet, and why I posted here—I knew somebody would suggest an improvement such as you have done—thanks!  (it was also late and I was dying to just hurry up and try it out)



Initially, I wanted a frame to hold a rework of my v1 Printrbot Plus (not shown, it has the Super-Z upgrade and is taller) into an Ultimaker-ish variant, with the key change being to have either a stationary build platform, or to only have it move vertically, like the Ultimaker.  Also, I bought these before joining the Ingentis community, so I only bought enough for just the box itself thinking that would be enough (newb!).  As I said above, I will probably want to order 2-4 more pieces to make it a little bit stiffer.



Also, I bought the ends untapped. I later bought a tap & die set to enable the possibility of blind joints, which sounds like what you are suggesting.



BTW, I've never used a tap & die before (always wanted to!), so while I've read about it and watched some YouTube vids, any tips from experienced folks might be helpful.


---
**James Rivera** *October 04, 2014 17:45*

**+Brandon Satterfield** LOL! I actually tried to print a huge Eiffel tower on my taller Printrbot.  It failed miserably the first time (broke loose from the bed after a few hours), the 2nd attempt had an extruder jam (I was still learning), and the 3rd attempt failed around the first observation deck.  I'm still not 100% sure why, but in hindsight I think I might have never told the system it was taller than 200mm (slicer software? M564 S0?). Oh well, live and learn.


---
**Mike Miller** *October 04, 2014 20:42*

You'll want to use a cutting lubricant...if you can slowly turn the tap, or the part, under power, you may have a more fun time of it...just don't over-power the tap....if you snap it, you'll lose the tap AND the part. 



My printer isn't near as tall, and am REALLY curious to see how my printing habits change. I have a lathe that can turn 3 foot parts, but spend 99% of my time within 3 inches of the chuck. I expect the printer will be similar. 


---
**James Rivera** *October 04, 2014 21:37*

**+Mike Miller** I've heard that is necessary, and that some people have used motor oil for this. Which cutting lubricant do you/would you use for tapping aluminum?



Also, I already have a tall Printrbot (about 23 inches print height) and like you suspect, not including my (ridiculous) Eiffel tower attempts, I have stayed pretty much confined to regular sized (read: small) prints.



But, the reality is its unreliability with very large prints makes me loathe to try them.  In fact, reliability issues have made it so I don't even like to print a large plate of low height parts.



So part of the reason for building it from hand-selected parts is to (hopefully) increase the precision and reliability of even the small prints to the point where I might actually consider printing large objects.



EDIT: my unreliable printer is not the Printrbot shown, but my other one. The one shown has been very reliable since I added the machined aluminum (super flat!) bed.


---
**Mike Miller** *October 05, 2014 02:57*

You're really just looking for lubrication. Motor oil, wd-40, 3-in-1 oil will do it. And periodically take a nylon brush to the tap,otherwise the aluminum will gall.




---
**Paul Sieradzki** *January 12, 2015 16:11*

How is the rigidity at that size? I see you're using brackets everywhere, which probably helps a lot.. any additional bracing needed?


---
**Paul Sieradzki** *January 12, 2015 16:14*

Also **+Eric Lien**, do you suggest using the Misumi option of having the ends of extrusions tapped and then having countersunk holes drilled through the verticals? If so, I can see that working for only one of the horizontal axes (x or y) since doing both would mean a screw would have to intersect another.. ideas?


---
**Eric Lien** *January 12, 2015 18:12*

**+Paul Sieradzki** the screws do to pass through. The button head rides in the t-slot. The hole provides Allen wrench access only.


---
**Paul Sieradzki** *January 12, 2015 18:14*

Ahhhhh. Thanks!



Follow-up question: what is your experience with the need for anti-backlash nuts for the z-axis? I'm looking to build a big printer like yours, so the platform will be heavy. Are there ever quick back/forth motion in z?


---
**Eric Lien** *January 12, 2015 18:24*

**+Paul Sieradzki** Gravity works pretty well on the z-bed for me without backlash nuts. But if you used z-lift in the slicer and quick Z accelerations/moves it may come in handy. I don't use z-lift.



For more detail on the button head screw discussion, see detail E in the HercuLien frame print: [https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Herculien_Frame.PDF](https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Herculien_Frame.PDF)


---
**James Rivera** *January 12, 2015 19:45*

**+Paul Sieradzki** listen to **+Eric Lien**. He knows this stuff inside out and backwards.


---
**Mike Miller** *January 12, 2015 19:52*

Upside down, too.


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/9nCDJzNSfYL) &mdash; content and formatting may not be reliable*
