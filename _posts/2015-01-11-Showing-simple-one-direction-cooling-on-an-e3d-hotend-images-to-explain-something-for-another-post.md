---
layout: post
title: "Showing simple one-direction cooling on an e3d hotend - images to explain something for another post"
date: January 11, 2015 05:06
category: "Discussion"
author: Jim Wilson
---
Showing simple one-direction cooling on an e3d hotend﻿ - images to explain something for another post.



![images/3f6c1024d29417dc4c6b67310806b625.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3f6c1024d29417dc4c6b67310806b625.jpeg)
![images/dd9028111869fe74670c0834373fb6c1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dd9028111869fe74670c0834373fb6c1.jpeg)
![images/34f86bd8e2707dd963875ce0625ccaa0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/34f86bd8e2707dd963875ce0625ccaa0.jpeg)
![images/af3c48274bef6075ce051672782008af.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/af3c48274bef6075ce051672782008af.jpeg)

**Jim Wilson**

---
---
**Jim Wilson** *January 11, 2015 05:08*

Sorry, this was in relation to another discussion about the Eustathios hot end cooling, this isn't a Eustathios.﻿


---
**Jim Wilson** *January 11, 2015 05:16*

[https://plus.google.com/109092260040411784841/posts/VZQNccsWhqL](https://plus.google.com/109092260040411784841/posts/VZQNccsWhqL)


---
*Imported from [Google+](https://plus.google.com/101778058628996936791/posts/RBpZa7z8TTn) &mdash; content and formatting may not be reliable*
