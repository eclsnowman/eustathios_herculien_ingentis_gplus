---
layout: post
title: "E3D hotend socks are out! Already ordered 9!"
date: August 31, 2016 20:34
category: "Show and Tell"
author: Stefano Pagani (Stef_FPV)
---
[http://www.filastruder.com/products/silicone-socks-3-pack](http://www.filastruder.com/products/silicone-socks-3-pack)



E3D hotend socks are out! Already ordered 9!





**Stefano Pagani (Stef_FPV)**

---
---
**Zane Baird** *August 31, 2016 20:54*

If I had any of the new heater blocks you can bet I would be ordering some too. Alas, I'll have to wait until I break a hotend, build another printer, or add another modular effector to my delta. Sigh...


---
**Sean B** *August 31, 2016 20:58*

Just ordered mine :-)  fortunately I ordered the new E3D after the cartridge change.


---
**Luis Pacheco** *August 31, 2016 23:15*

So how exactly would you know which cartridge one would have?


---
**Sean B** *August 31, 2016 23:42*

A couple of months ago they changed the hot end to a cartridge thermistor (instead of screw)  that should tell you.  Also the design of the aluminum block is slightly different as a result.




---
**Luis Pacheco** *August 31, 2016 23:44*

I'll have to check the one I bought, I got it a while back though (maybe a year or more so it's probably the older style).


---
**Sean B** *August 31, 2016 23:45*

**+Luis Pacheco** If it wasn't ordered in the past 2 months, it's the old style.




---
**Luis Pacheco** *August 31, 2016 23:48*

Then yes, definitely old style! Might have to check into an upgrade kit then, I've been wanting to change my nozzle to a hardened steel one anyways.﻿ Appreciate the info **+Sean B**​


---
**Jim Stone** *September 01, 2016 02:42*

Does anyone have any comments on the accuracy of the new thermosistor?



 im thinkin of buying a second e3d soon...and if i do i would have to get a new block for my current. because i dont think firmware allows two different therm tables for nozzles.



Secondary nozzle would be ONLY for exotic/Hard/abrasive filaments.



but thats a bit in the monthish future. need to secure the bondtech first.


---
**Jeff DeMaagd** *September 01, 2016 22:20*

**+Jim Stone** the thermistor is the same as before but potted in a metal cartridge. The new cartridge probably has better thermal contact, so you probably get slightly better response.


---
**Jim Stone** *September 01, 2016 22:20*

Noice


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/2HvaU6qavbm) &mdash; content and formatting may not be reliable*
