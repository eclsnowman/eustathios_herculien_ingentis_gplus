---
layout: post
title: "Received the Misumi extrusion yesterday and the Bondtech extruder parts a week ago"
date: March 27, 2015 01:07
category: "Show and Tell"
author: Brent ONeill
---
Received the Misumi extrusion yesterday and the Bondtech extruder parts a week ago. **+Ashley Webster** has the Azteeg controllers and the various motion related components from RobotDigg. Looks like we're  sparking-up the frame soon, just need to figure out where to build these beasts ;) . Many many thanks to the creators and contributors of these beautiful machines. I hope to give-back somehow, and even though we are printing our own parts to use on these builds, maybe I can provide a print-forward for a fellow Canadian enthusiast thereby saving freight costs from the US. Cheers and good journeys all!



![images/a6bc803ecbbfa47bfe1f21dc9b7ff0bd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a6bc803ecbbfa47bfe1f21dc9b7ff0bd.jpeg)
![images/6b10e861349a610c95f480f66e59a7b8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6b10e861349a610c95f480f66e59a7b8.jpeg)

**Brent ONeill**

---
---
**Brandon Cramer** *March 27, 2015 01:55*

Very nice! Can't wait to order my Misumi extrusion parts. 


---
**Brent ONeill** *March 27, 2015 02:33*

Those machined parts are some of the best quality I've seen in a looooong time....kudos **+Martin Bondéus** 


---
*Imported from [Google+](https://plus.google.com/117063078975595625522/posts/GE3mYdcDuDV) &mdash; content and formatting may not be reliable*
