---
layout: post
title: "Hey guys, I'm trying to get an order together"
date: March 05, 2014 06:34
category: "Discussion"
author: D Rob
---
Hey guys, I'm trying to get an order together. 10mm bore 2mm pitch 6mm wide 32 tooth gt2 pulleys. We have found a custom pulley source and these are perfect for an ingentis belt conversion. Min order is 50 and we wanted to see who all wants some. If you are interested let me know. If you use 10mm rod for z axle 12 will make a full conversion. 10 if not. I'll be handling the order. So if your interested pm me your address to figure out the shipping. Pulleys will cost 2.30 ea plus post. Just send me your quantity address and you can send me what your total is via pay pay once all orders are placed I'll order and send everyone their pulleys. If you have a preferred shipping method let me know. I'm getting 12 **+Anthony Morris** 10 **+Tim Rastall** 10  **+Jim Squirrel** 4 so we are up to 36 72% of the min. 





**D Rob**

---
---
**Brian Bland** *March 05, 2014 06:44*

10 for me.  Sent you a PM.


---
**Tim Rastall** *March 05, 2014 06:56*

Thanks for doing this dude! 


---
**D Rob** *March 05, 2014 06:57*

**+Tim Rastall**  It needs to be done. And we are all working together. Its the spirit of open source and the 3d printing community. We are a community and helping each other is what we do.


---
**D Rob** *March 05, 2014 07:01*

**+Jim Squirrel** **+Anthony Morris**  **+Tim Rastall**  can you pm me your email mailing address preferred shipping method and quantity. Im creating an order list


---
**D Rob** *March 05, 2014 07:06*

oh yeah shipping in my area is limited to fed ex and usps envelope


---
**Tim Rastall** *March 05, 2014 07:28*

USPS envelope fine for me. 


---
**Eric Lien** *March 05, 2014 07:45*

I am in. PM sent. Goodbye corexy, hello effecient build to footprint ratio!


---
**Daniel Fielding** *March 05, 2014 11:20*

Why not I say, I'll be in for 10. PM coming your way.


---
**Dale Dunn** *March 05, 2014 12:54*

2mm pitch. These are GT2 profile?


---
**Brian Bland** *March 05, 2014 12:58*

**+Dale Dunn** Yes they are GT2.  **+D Rob** did they give you a time frame on getting the pulleys once ordered?﻿


---
**Dale Dunn** *March 05, 2014 13:14*

OK, sign me up for 12, but I plan to try getting everything done with 5.


---
**Carlton Dodd** *March 05, 2014 13:27*

I'm down for 15 (spares for losing).  PM sent.

I'm starting from scratch, but this should be enough, right?


---
**Nick Adams** *March 05, 2014 16:12*

I'll take 15 as well. PM coming your way.


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/ZBxgcNn5Ebh) &mdash; content and formatting may not be reliable*
