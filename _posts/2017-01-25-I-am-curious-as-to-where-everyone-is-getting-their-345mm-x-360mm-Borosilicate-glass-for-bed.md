---
layout: post
title: "I am curious as to where everyone is getting their 345mm x 360mm Borosilicate glass for bed?"
date: January 25, 2017 02:08
category: "Build Logs"
author: Pete LaDuke
---
I am curious as to where everyone is getting their 345mm x 360mm Borosilicate glass for bed?  I have not been able to find a piece even close to the size.  Comments? Sources?   





Thanks in advance to any help or pointers here.





**Pete LaDuke**

---
---
**Oliver Seiler** *January 25, 2017 02:21*

I wasn't able to source borosilicate glass either and have been using plain 4mm float glass from a local supplier and that's been working fine for many months. 


---
**Pete LaDuke** *January 25, 2017 02:31*

Thanks Oliver!  I was wondering if I may have to go that route, I just haven't used anything but Boro glass in the past.   



Are there any limitations or other comments on it?  I was looking to go with a PEI sheet anyhow, so it may not matter.   Just didn't want to change if I didn't need to.


---
**Oliver Seiler** *January 25, 2017 02:52*

I added a PEI sheet a few months ago and it's the best improvement I made to my Eustathios in a while. Just get one. 

My build stack is now 4mm aluminium, 4mm float glass and then the PEI sheet. Works really well for a range of filaments. 


---
**Eric Lien** *January 25, 2017 03:18*

Standard window glass in all my printers. Zero issues. Only time I had issues but in the past was when I thought tempered glass would be better... I was wrong. Tempered glass is designed so it is naturally in tension (so if something hits it the tension can fight the force of the impact). Standard window glass or annealed glass is not in tension so handles the thermal stresses much better.



And I agree with **+Oliver Seiler**​ heat spreader below + glass for flatness on the spreader + PEI on top for adhesion has been a winning combination for me.


---
**Pete LaDuke** *January 25, 2017 03:19*

Awesome, thanks for your insights.   I have a PEI sheet ready to go for my Eustathios, but still working through my build.  My build took a set back as I have to fix a different machine that has turned ugly.   Still need a few components to keep the Eusthathios build moving forward.  Hopefully I'll get it back in gear again soon.


---
**Oliver Seiler** *January 25, 2017 03:21*

Reminds me that I had first tried tempered glass and it wasn't flat (the tempering process warped it). Stay away from it. 


---
**Pete LaDuke** *January 25, 2017 03:21*

Thanks for for feedback Eric!   I'll go with some standard glass.


---
**Eric Lien** *January 25, 2017 03:24*

Here is what I did: [https://plus.google.com/+EricLiensMind/posts/cSZcrhYp6LL](https://plus.google.com/+EricLiensMind/posts/cSZcrhYp6LL)


---
**Eric Lien** *January 25, 2017 03:26*

I added the foil tape between the glass and PEI Incase I ever wanted to add an inductive probe down the road. The foil actually can be sensed by and inductive probe :)


---
**Daniel F** *January 25, 2017 07:13*

I use a 30x30cm mirror tile (from ikea or hw store) works great. I heat it up to 90° for ABS and never had an issue with it.


---
*Imported from [Google+](https://plus.google.com/100658478011121421875/posts/ZHDeK5uktsD) &mdash; content and formatting may not be reliable*
