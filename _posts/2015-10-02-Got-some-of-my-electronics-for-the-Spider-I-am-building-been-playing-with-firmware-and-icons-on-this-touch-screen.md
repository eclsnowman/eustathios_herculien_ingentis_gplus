---
layout: post
title: "Got some of my electronics for the Spider I am building, been playing with firmware and icons on this touch screen"
date: October 02, 2015 20:07
category: "Show and Tell"
author: Bud Hammerton
---
Got some of my electronics for the Spider I am building, been playing with firmware and icons on this touch screen. I sure wished that Makerbase would release source code. But alas that is likely never going to happen. So I can only change a very few things that a HEX editor will allow me to do without breaking the current firmware.



All the icons I used were stolen from the web and put in a very large (lots of layers) PSD file, I just pick the image layer I want, select the text layer that goes with it, then "Save as ..." to get the correct icon I need. Willing to share the PSD for those tempted to try the MKS-TFT

![images/266d3ef4b41a48583ce416dfc38ccfbd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/266d3ef4b41a48583ce416dfc38ccfbd.jpeg)



**Bud Hammerton**

---
---
**Bud Hammerton** *October 02, 2015 20:37*

How do I add additional photos to this post?


---
**CkTBrD** *October 04, 2015 03:12*

I think it would be best to put a Dropbox link in the post it something of that nature rather than upload a bunch of photos which you can't save anyway (well at least from my phone I can't) 




---
**Bud Hammerton** *October 05, 2015 15:24*

I already have them on my Google Drive, guess I could make them shareable and then link to them. I should likely start a Eustathios album in my Photos.


---
**Bud Hammerton** *October 05, 2015 15:56*

OneDrive Album

[http://1drv.ms/1MUjcXQ](http://1drv.ms/1MUjcXQ)


---
*Imported from [Google+](https://plus.google.com/+BudHammerton/posts/bGjsqTLr8vk) &mdash; content and formatting may not be reliable*
