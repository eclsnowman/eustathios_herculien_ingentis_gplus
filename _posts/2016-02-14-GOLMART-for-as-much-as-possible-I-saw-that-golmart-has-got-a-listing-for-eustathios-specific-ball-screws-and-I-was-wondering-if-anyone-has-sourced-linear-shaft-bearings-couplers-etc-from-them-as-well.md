---
layout: post
title: "GOLMART for as much as possible? I saw that golmart has got a listing for eustathios specific ball screws and I was wondering if anyone has sourced linear shaft, bearings, couplers etc from them as well?"
date: February 14, 2016 03:53
category: "Discussion"
author: Ben Marks
---
GOLMART for as much as possible?



I saw that golmart has got a listing for eustathios specific ball screws and I was wondering if anyone has sourced linear shaft, bearings, couplers etc from them as well?  If their quality is acceptable I'd like to get as many hard parts as I can from them.





**Ben Marks**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 14, 2016 04:19*

In general, since I haven't built this specific printer, I use RDB Bearing... For my shafts and bearings and flex couplers or transmission cnc if you need rigid couplers (and they have great leadscrews)


---
**Jim Stone** *February 14, 2016 04:29*

misumi is good too.


---
**Ben Marks** *February 14, 2016 04:44*

**+Ray Kholodovsky** that transmission cnc shop has great prices.



**+Jim Stone** Misumi is always my fallback if I decide to swing for the fences budget wise.


---
**Eric Lien** *February 14, 2016 05:11*

For the linear shafts I really do recommend MISUMI due to the tolerance. I've heard of people who spec the proper tolerance from Chinese suppliers and you get a mixed bag of whether or not you have the proper +/-.  You would want to double check against the misumi tolerance listing for the part in the BOM. I am out right now and so I cannot confirm.


---
**Ray Kholodovsky (Cohesion3D)** *February 14, 2016 06:35*

**+Ben Marks** yeah, I originally avoided them because the shipping wasn't free and seemed expensive, but then I realized that even with, say, $8 shipping the leadscrews were cheaper than the next comparable vendor with free shipping (aka shipping factored in). Those were some great quality tr8's btw, also got a hollow Jeka 8 stepper and couplers from them and all came very quickly. 


---
**Ben Marks** *February 15, 2016 15:30*

**+Eric Lien**

When you say tolerances are you mainly talking about straightness or the diameter consistency?


---
**Eric Lien** *February 15, 2016 16:20*

These are the misumi tolerance for g6 tolerance: 



[http://us.misumi-ec.com/pdf/fa/2012/p1_0097.pdf](http://us.misumi-ec.com/pdf/fa/2012/p1_0097.pdf)



[http://us.misumi-ec.com/vona2/detail/110302634310/?HissuCode=SFJ10-%5B20-800%2F1%5D&PNSearch=SFJ10-%5B20-800%2F1%5D&searchFlow=suggest2products&Keyword=SFJ10-](http://us.misumi-ec.com/vona2/detail/110302634310/?HissuCode=SFJ10-%5B20-800%2F1%5D&PNSearch=SFJ10-%5B20-800%2F1%5D&searchFlow=suggest2products&Keyword=SFJ10-)


---
*Imported from [Google+](https://plus.google.com/110401891851536186752/posts/UxfCbJTqimX) &mdash; content and formatting may not be reliable*
