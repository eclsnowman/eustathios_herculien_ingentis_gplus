---
layout: post
title: "this is what an entire Herculien build looks like after disassembly and arrangement into grouped components"
date: January 23, 2016 05:01
category: "Show and Tell"
author: Zane Baird
---
this is what an entire Herculien build looks like after disassembly and arrangement into grouped components. If anyone is interested I'd be happy to document my rebuild with pictures and comments as I go through the the process from individual components, to alignment, break-in, and tuning. 

![images/d6b2b22d16bff4bf6ef6a31719d8c534.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d6b2b22d16bff4bf6ef6a31719d8c534.jpeg)



**Zane Baird**

---
---
**Jim Stone** *January 23, 2016 05:11*

That would be a much appreciated documentation.



During my first go thru of install I found a few things in the current documentation that should be worded or done differently



Mainly the cutting of the gantry belts >.< if you cut to 1m like it says you will have a bad time. Lol



That and no documentation on when to put the panels in. Or the best plan of attack to put them in. 



I figured out the top cover. Now just to figure out the rest.



Minus the door  the door is self explanatory. 


---
**Jim Stone** *January 23, 2016 05:11*

Ooh alignment too. I'm still not wired and would love a different way to check


---
**Zane Baird** *January 23, 2016 05:31*

**+Jim Stone** As I was taking it apart I was devising my strategy for re-assembly. I think I have it figured out now. But **+Eric Lien** has done it more times than I, so his insights into my future posts should shed some light on it all. I'm approaching it with new knowledge now, so I think I can provide useful comments and suggestions  as to the approach. 



I also had the same experience with belt cutting previously. Luckily I cut one and tested before I cut them all


---
**Ted Huntington** *January 23, 2016 06:46*

Yes, I for one would love to see any documentation and pictures from the rebuild.


---
**James Rivera** *January 23, 2016 07:29*

Documentation on how to order the <b>exact</b> parts is what kept me from building one.﻿


---
**Mike Miller** *January 23, 2016 14:24*

Someone is Very Close to having all you need to sell these as a kit. Whether that's you, or the guy that takes the time with Eric's BOM and you documentation ... 


---
**Jeff DeMaagd** *January 24, 2016 03:58*

I gather Herculien is a very expensive machine. Something like $2k+ for just the parts, especially if you buy them to spec. It's also not a beginner machine to build.


---
**James Rivera** *January 24, 2016 04:30*

I was going to build the Eustathios, which is (a little) less.


---
**Zane Baird** *January 24, 2016 04:44*

**+Jeff DeMaagd** I think the average cost for the Herculien is around $1.5k (which is why I built it over a period of 6 months on a student budget). While I haven't built a eustathios, I can forsee alignment being a bit more finicky than the Herculien. Either printer requires some machining capabilities. For the Herculien, the bed insulator is the hardest but can be done with a router by hand. In all honesty, with a good 1kW heater you may even get away without it by insulating the leveling screws from the aluminum extrusions. 


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/NrouqdGpjFj) &mdash; content and formatting may not be reliable*
