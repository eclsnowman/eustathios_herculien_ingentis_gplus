---
layout: post
title: "Anyone have a supplier that has 1/4\" eccentric spacers in stock?"
date: January 29, 2015 00:06
category: "Discussion"
author: Daniel Salinas
---
Anyone have a supplier that has 1/4" eccentric spacers in stock?  I need 4 for my Herculien build and don't see anyone online that has any in stock.





**Daniel Salinas**

---
---
**Eric Lien** *January 29, 2015 00:50*

Maybe here: [https://www.inventables.com/technologies/eccentric-spacer](https://www.inventables.com/technologies/eccentric-spacer)


---
**Eric Lien** *January 29, 2015 00:55*

Looks like the 25195-03 would give even better support.


---
**Daniel Salinas** *January 29, 2015 05:36*

I ordered some of those and these. [http://store.amberspyglass.co.uk/eccentric-spacer.html](http://store.amberspyglass.co.uk/eccentric-spacer.html) we'll see what works best.


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/QAsK1iaud74) &mdash; content and formatting may not be reliable*
