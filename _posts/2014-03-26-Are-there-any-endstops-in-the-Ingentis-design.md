---
layout: post
title: "Are there any endstops in the Ingentis design?"
date: March 26, 2014 11:30
category: "Discussion"
author: Mike Miller
---
Are there any endstops in the Ingentis design? I'm sure not seeing any in the videos people have posted...





**Mike Miller**

---
---
**Jason Smith (Birds Of Paradise FPV)** *March 26, 2014 11:40*

I designed endstop mounts for the #Eustathios which could likely also be used for the  #Ingentis .  They're just flat pieces which can be printed or milled and mount directly to the horizontal extrusion.  Files here: [https://github.com/jasonsmit4/Eustathios](https://github.com/jasonsmit4/Eustathios)


---
**David Heddle** *March 26, 2014 13:40*

They can be exported from the v4 stp file that is on [youmagine.com](http://youmagine.com).  I have them printed so will have the files somewhere give me a shout if you want me to send them to you.


---
**Dale Dunn** *March 26, 2014 13:42*

They're in the published models (V4 STEP, anyway), but I haven't been paying attention to whether anyone has implemented them.


---
**Tim Rastall** *March 26, 2014 19:40*

Ah,  yes.  Tbh the ones in the steps are a bit clunky.  I'd intended to design a universal one that would work for either axis and accommodate for a number of endstops.  The Z endstop is similarly clunky . ﻿


---
**Dale Dunn** *March 26, 2014 20:07*

Some of the Ingentis variants on Youmagine have some nice end stops.


---
**Riley Porter (ril3y)** *March 28, 2014 21:15*

FYI autodesk 123 online webapp thing allows to import stp files and u can export stls. I did this with the #shapeoko files 2 days ago. Its perfect if u do not have autodesk inventor!


---
**Riley Porter (ril3y)** *March 29, 2014 03:46*

I meant autodesk fusion. 


---
**Jarred Baines** *March 29, 2014 22:32*

**+Riley Porter** you legend :-) I'll be trying that!


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/ZoHPVmvgEYb) &mdash; content and formatting may not be reliable*
