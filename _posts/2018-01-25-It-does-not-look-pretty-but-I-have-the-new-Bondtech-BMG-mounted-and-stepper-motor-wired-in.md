---
layout: post
title: "It does not look pretty, but I have the new Bondtech BMG mounted and stepper motor wired in"
date: January 25, 2018 03:58
category: "Show and Tell"
author: Bruce Lunde
---
It does not look pretty, but I have the new Bondtech BMG mounted and stepper motor wired in. I Think this is the smoothest unit I have experienced to date. I need to figure out the steps to put into the Smoothieboard configuration, but the second picture shows a nice long run of melted plastic, so I am excited.



![images/59564584ba14221e2068b29241882dc3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/59564584ba14221e2068b29241882dc3.jpeg)
![images/1e397f1dcd71d236d533fafdc8668561.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1e397f1dcd71d236d533fafdc8668561.jpeg)

**Bruce Lunde**

---
---
**Eric Lien** *January 25, 2018 06:26*

I like the sheet metal mount. Look forward to seeing it get to printing again :)


---
**Eric Lien** *January 25, 2018 06:28*

BMG is 415steps/mm if you are at 1/16 microstep on the extruder. 830steps if you are at 1/32 microstep.


---
**Eric Lien** *January 25, 2018 06:29*

Details about half way down the page: [http://www.bondtech.se/en/faq/](http://www.bondtech.se/en/faq/)


---
**Bruce Lunde** *January 25, 2018 19:18*

**+Eric Lien** I will print  the Bondtech mount once i get everything going.  It's nice to have a bending tool to make the mount quickly with the sheet metal.




---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/athEbnyG4kz) &mdash; content and formatting may not be reliable*
