---
layout: post
title: "I ordered stepper motors from Robotdigg for my Herculien from the BOM link but I am not sure its the correct one, here is the link it is a 6 pin motor?"
date: May 15, 2015 02:44
category: "Discussion"
author: Vic Catalasan
---
I ordered stepper motors from  Robotdigg for my Herculien from the BOM link but I am not sure its the correct one, here is the link



[http://www.robotdigg.com/product/29/Nema17-60mm-1.5A-high-torque-stepper-motor](http://www.robotdigg.com/product/29/Nema17-60mm-1.5A-high-torque-stepper-motor)



it is  a 6 pin motor? I thought they should be 4 pin?



Also the same for the extruder motor



[http://www.robotdigg.com/product/103/Nema17-40mm-Stepper-Gearmotor](http://www.robotdigg.com/product/103/Nema17-40mm-Stepper-Gearmotor)





**Vic Catalasan**

---
---
**Mike Miller** *May 15, 2015 02:46*

6 or 4, it can be made to work. IIRC there are two, three leg circuits, you drop the middle wire and only use the four outside leads. Google it. 


---
**Vic Catalasan** *May 15, 2015 02:49*

Okay thanks will look how its done. Is it true however that I am only using part of the coil in the motor?


---
**Mike Miller** *May 15, 2015 02:59*

It looks like this: [http://www.osmtec.com/images/4-and-6-wire.png](http://www.osmtec.com/images/4-and-6-wire.png)


---
**Sébastien Plante** *May 15, 2015 03:07*

I think the middle leg is to provide feedback to driver that allow it.﻿



(not true, see bellow)


---
**Eric Lien** *May 15, 2015 03:24*

It is just a standard plug interface some steppers use. They supply a 4wire cable with the stepper. Also they can make custom length cables for you too if you ask them.﻿


---
**Whosa whatsis** *May 15, 2015 03:47*

The #2 and #5 pins are intended for the center taps required to run the motors in Unipolar mode, but they are not connected to anything in any of the RobotDigg motors I've checked.



The RobotDigg motors are also odd in that the #3 and #4 pins are reversed. The cable that comes with it switches the pins back to the correct order (it's wired 1-3-2-4), but without modification, that motor won't work with any other cable, and vice versa.


---
**ThantiK** *May 15, 2015 04:12*

We used to use these in some of the DeltaMaker printers.  They're not the strongest, but they're passable.


---
**Eric Lien** *May 15, 2015 04:20*

**+ThantiK** the 60mm ones? They are torque monsters on mine. Dang near nema 23 motor ranges.


---
**ThantiK** *May 15, 2015 06:11*

**+Eric Lien**, I was just going by the picture.  Looks like the description is for the larger (deeper) versions.  The deeper ones are great.  Been a long day.


---
**Vic Catalasan** *May 15, 2015 07:35*

Anyone have a source for the wire harness?


---
**Eric Lien** *May 15, 2015 09:41*

**+Vic Catalasan** [http://www.robotdigg.com/product/192/Extra-cables-w/-Dupont-Connector-for-RobotDigg-stepper-motors-in-lengths](http://www.robotdigg.com/product/192/Extra-cables-w/-Dupont-Connector-for-RobotDigg-stepper-motors-in-lengths)




---
**Whosa whatsis** *May 15, 2015 15:05*

**+Vic Catalasan** the cables should have come with the motor, and like I said, their motors aren't compatible (without modification) with cables from anywhere else.


---
**Vic Catalasan** *May 15, 2015 16:06*

Thanks Eric, I have nearly everything now to start my build. Just waiting from a local friend on the printed templates. His printer must be really slow because he started printing before I started ordering... LOL 


---
**Vic Catalasan** *May 15, 2015 16:07*

**+Whosa whatsis** Unfortunately I did not see in the package


---
**Eric Lien** *May 15, 2015 16:47*

What I did it take the templates, print them in sections and paste them together. Thats why I have them subdivided with grid lines like they are (makes them easy to reassemble).



I look forward to seeing your progress.


---
**Gústav K Gústavsson** *May 16, 2015 00:57*

The motors came with a 4 to 6 pin cable on my friends order. He is Building a extended HercuLien, mostly following erics design... With everything electronic addon possible.


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/UmUCc9pxVNc) &mdash; content and formatting may not be reliable*
