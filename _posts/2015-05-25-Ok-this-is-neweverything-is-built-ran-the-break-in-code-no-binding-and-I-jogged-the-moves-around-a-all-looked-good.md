---
layout: post
title: "Ok this is new...everything is built ran the break in code no binding and I jogged the moves around a all looked good"
date: May 25, 2015 00:08
category: "Discussion"
author: Derek Schuetz
---
Ok this is new...everything is built ran the break in code no binding and I jogged the moves around a all looked good. so I wanted to try a small test cube . All of a sudden I hear horrific binding noises. I watched it a little and noticed it was only during slow movements in x axis. So I did some more test... Any fast movements will move just fine but if I home it its binding up but not skipping steps: so I realigned everything and I'm still having the issue..:



Any thoughts?





**Derek Schuetz**

---
---
**Gus Montoya** *May 25, 2015 00:12*

It's peculiar that it occurs only at that specific movement. Did you check that the rods were straight? I found one (luckily I over ordered) that was slightly bent. Has to take a straight ruler and gauge tool to check engine deck straightness.


---
**Isaac Arciaga** *May 25, 2015 00:35*

**+Derek Schuetz** is it something that can be captured on video?


---
**Eric Lien** *May 25, 2015 01:06*

**+Derek Schuetz**​​ disconnect the motor pulleys. Then move the carriage by hand. If it doesn't move smooth as butter... Then you need to play with alignment more. It is a finicky process. But when it's right you will know. Thats why I made the corners combined in HercuLien, if the parts are printed correctly there is no alignment required. But since Eustathios has so many degrees of freedom on the gantry that require  alignment the process takes a while.﻿


---
**Derek Schuetz** *May 25, 2015 01:41*

You mean disconnect the small closed loop belts or loosen up all the set screws and move it


---
**Eric Lien** *May 25, 2015 02:19*

**+Derek Schuetz** either would work. Just don't force it with the belts hooked up. Motors are generators when run in reverse. Its a good way to burn out your board.


---
**Gus Montoya** *May 25, 2015 02:36*

Or easier disconnect the motors, then move by hand. 


---
**Walter Hsiao** *May 25, 2015 02:42*

The alignment brackets worked well for me, but I was able to reduce friction a bit more by loosening all the bearing holders, aligning the 4 top bearing with the frame top and securing them there (after making sure the top beams were aligned).  I then pushed the extruder to each corner and secured the lower bearing holder wherever it ended up.  I went around in a circle and loosened and retightened each lower bearing at least twice.  Don't know if the same thing will work for others, my setup is a bit different, but it's fairly easy to try.


---
**Eric Lien** *May 25, 2015 02:49*

**+Walter Hsiao** that is the best method without printing the alignment tools I made. But to be honest, the carriage to corner method produces the best results. It just might take a few times around. And once you tension belts (closed loop to motors, or main axis open loops to shaft pulleys) things can get torqued slightly and require readjustments. 


---
**Derek Schuetz** *May 25, 2015 02:52*

well i got it down to very minor binding but its weird it only shows up slow novements


---
**Eric Lien** *May 25, 2015 03:11*

**+Derek Schuetz** bushings tend to be like that, they have a static friction to overcome. But it should not cause steppers to skip. Maybe up the current. Also what microsteps are you using. I use 1/16, I think **+Jason Smith**​ was using 1/8 if I recall. People say micro stepping doesn't reduce torque... But my experiences seem different. Also run the break in code several times. These printers age like a fine wine... They get better (the smoothness of the motion) along the way.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/7KPfk1Mjive) &mdash; content and formatting may not be reliable*
