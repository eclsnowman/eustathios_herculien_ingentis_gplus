---
layout: post
title: "What have we here? Guys I'll package everything this weekend and try to so them Monday"
date: April 12, 2014 05:46
category: "Show and Tell"
author: D Rob
---
What have we here? Guys I'll package everything this weekend and try to so them Monday.



![images/b4399940d35f9b03ac121fe37300d5e6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b4399940d35f9b03ac121fe37300d5e6.jpeg)
![images/38d59ff4872524fdf2dea8bdd8012abd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/38d59ff4872524fdf2dea8bdd8012abd.jpeg)
![images/c8231381a18397296217293586e8a949.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c8231381a18397296217293586e8a949.jpeg)
![images/8e9a978e7d0a1aa89e9e829ce944c97c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8e9a978e7d0a1aa89e9e829ce944c97c.jpeg)
![images/55e20766f34943c523f6b0ced9382fdb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/55e20766f34943c523f6b0ced9382fdb.jpeg)
![images/22ef0d113d9b3caad845b5c9c2c9dc25.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/22ef0d113d9b3caad845b5c9c2c9dc25.jpeg)

**D Rob**

---
---
**Daniel Fielding** *April 12, 2014 05:47*

Don't forget to tell me if I owe you more


---
**Brian Bland** *April 12, 2014 06:16*

That was fast.  


---
**Tim Rastall** *April 12, 2014 06:22*

Yay! 


---
**Carlton Dodd** *April 12, 2014 13:46*

Woot!  They look pretty good.  Thanks for doing this.


---
**D Rob** *April 12, 2014 14:47*

Thanks to everyone who bought in. Together we made these available to everyone at an affordable price on [http://robotdigg.com.](http://robotdigg.com.)﻿


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/fVAstt1su5S) &mdash; content and formatting may not be reliable*
