---
layout: post
title: "Do you guys think a Go Pro would last within the build chamber of the herculien?"
date: November 07, 2015 22:16
category: "Discussion"
author: Jim Stone
---
Do you guys think a Go Pro would last within the build chamber of the herculien? it gets what ...65c ambient?





**Jim Stone**

---
---
**Eric Lien** *November 07, 2015 22:20*

Around 55C with the bed at 110C


---
**Jim Stone** *November 07, 2015 22:23*

oh hell. the gopro should have no issues then


---
**Sébastien Plante** *November 07, 2015 22:24*

they said up to 82c :)


---
**Dave Hylands** *November 07, 2015 23:09*

Most consumer electronics components are only rated to 40C. GoPro has this on their support site (for original HD Hero): [https://gopro.com/support/articles/hot-cold-operating-temperatures-camera](https://gopro.com/support/articles/hot-cold-operating-temperatures-camera) which says 125F or 51C and this for the HERO3+ [https://gopro.com/support/articles/hero3plus-camera-operating-temperatures](https://gopro.com/support/articles/hero3plus-camera-operating-temperatures) (82C or 180F)


---
**Jim Stone** *November 08, 2015 02:38*

hmmm so it sounds like mounting a go pro to the bed may be a bad idea :( dang it. i wanted timelapse


---
**Eric Lien** *November 08, 2015 02:57*

Use a raspberry pi and a webcam (octoprint). It's awesome, gives you remote control capabilities, and timelapse all in one. And you don't have to tie up a pc to run the printer.


---
**Jim Stone** *November 08, 2015 03:02*

yeah i knew about that. was just gonna use a go pro. cause i thought it would have been more robust. :( but it looks like hte chamber kills electronics i guess.



unless .... do you run the webcam internally **+Eric Lien** ?


---
**Eric Lien** *November 08, 2015 03:10*

**+Jim Stone** Logitech c270 with a Chinese cellphone wide-angle lense. 2 years+ now. Zero issues.


---
**Eric Lien** *November 08, 2015 03:10*

[http://i.imgur.com/bib0wzy.jpg](http://i.imgur.com/bib0wzy.jpg)


---
**Eric Lien** *November 08, 2015 03:11*

[http://i.imgur.com/cMU4uWZ.png](http://i.imgur.com/cMU4uWZ.png)


---
**Florian Schütte** *November 09, 2015 17:27*

**+Eric Lien** How did you mount the Lens?


---
**Eric Lien** *November 09, 2015 17:29*

**+Florian Schütte** It has a metal ring with adhesive on one side. I stuck the ring to the webcam, then the lens has a magnetic ring which holds it on.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/AJUfjtvENDN) &mdash; content and formatting may not be reliable*
