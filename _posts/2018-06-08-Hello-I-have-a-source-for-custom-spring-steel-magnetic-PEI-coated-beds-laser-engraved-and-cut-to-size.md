---
layout: post
title: "Hello! I have a source for custom spring steel magnetic PEI coated beds, laser engraved and cut to size"
date: June 08, 2018 12:40
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hello! I have a source for custom spring steel magnetic PEI coated beds, laser engraved and cut to size. Also magnetic.



Price is around $50



UPDATE:

[https://goo.gl/forms/zuOTrR5DGKvkKIjs1](https://goo.gl/forms/zuOTrR5DGKvkKIjs1)



Please fill out google form for bed order, all details are private.



Will order within a week. Will provide shipping time estimate​ as well.





**Stefano Pagani (Stef_FPV)**

---
---
**wes jackson** *June 08, 2018 13:05*

I'd be down. What is engraved on it?


---
**Zane Baird** *June 08, 2018 13:24*

If they can be any custom size I would be in for 2 of different dimensions


---
**Eric Lien** *June 08, 2018 13:59*

Count me in.


---
**Ryan Fiske** *June 08, 2018 14:00*

Count on me as well!


---
**Stefano Pagani (Stef_FPV)** *June 08, 2018 14:01*

**+wes jackson** I was going to design somthing similar to the prusia mk3 design. Just a grid to help with measurements no names or anything


---
**Stefano Pagani (Stef_FPV)** *June 08, 2018 14:02*

I will make a straw poll for sizes. I was going to order just for the eustathios, but if there is demand for the herc I can get that in too. Poll will be up tonight! 


---
**Stefano Pagani (Stef_FPV)** *June 08, 2018 14:05*

**+Zane Baird** they are made to order so the more of one size the cheaper. I could get a special one for you. Will make a poll soon 


---
**Brandon Cramer** *June 08, 2018 14:48*

I’d be interested. Printrbot Simple Metal and Eustathios Spider V2.


---
**Ted Huntington** *June 08, 2018 17:19*

definitely sounds like a good idea, surprised PEI coated steel beds are not on aliexpress already.


---
**Glenn West** *June 08, 2018 18:30*

Love one for mk3


---
**Neil Merchant** *June 08, 2018 20:58*

I'd be down for one too, Eustathios.


---
**Antonio “3DZillait” Ranaldi** *June 09, 2018 09:17*

qualche foto




---
**Zane Baird** *June 09, 2018 15:55*

**+Stefano Pagani** For the custom sized beds, do you expect the cost to be the same?


---
**Stefano Pagani (Stef_FPV)** *June 10, 2018 01:18*

Everyone please do the google form !


---
**Stefano Pagani (Stef_FPV)** *June 10, 2018 01:19*

**+Zane Baird** may be slightly more. $60? If you send dxf I can have them quote it


---
**Eric Lien** *June 10, 2018 01:23*

Should we design in some alignment pin locations.


---
**Zane Baird** *June 10, 2018 02:15*

**+Stefano Pagani** to add on to **+Eric Lien**, what are the tolerance of the cuts? Water jet or laser?


---
**Stefano Pagani (Stef_FPV)** *June 10, 2018 04:09*

**+Zane Baird** will inquire with seller


---
**Stefano Pagani (Stef_FPV)** *June 10, 2018 04:09*

**+Eric Lien** I would say so. Very busy this weekend but will work on it Monday and Tuesday 


---
**Ryan Fiske** *June 11, 2018 16:47*

Thinking about getting a plate sized to fit Walters bed layout, would that effect pricing significantly?


---
**Ryan Fiske** *July 06, 2018 16:55*

**+Stefano Pagani** any update on the order?


---
**Stefano Pagani (Stef_FPV)** *July 06, 2018 16:57*

**+Ryan Fiske** going to order soon, sorry for the delay. Looking at where to put magnets/pins


---
**Ryan Fiske** *July 06, 2018 17:13*

**+Stefano Pagani** no worries! I was merely curious, thanks for letting me know! I appreciate you putting in the work for this.


---
**Stefano Pagani (Stef_FPV)** *July 06, 2018 17:32*

**+Ryan Fiske** no problem :) I’m trying to get a cad model for another printer I have so I can order them all in one


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/XPiauBsC27Z) &mdash; content and formatting may not be reliable*
