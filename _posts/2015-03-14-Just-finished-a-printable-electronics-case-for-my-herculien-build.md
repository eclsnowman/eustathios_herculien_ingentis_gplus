---
layout: post
title: "Just finished a printable electronics case for my herculien build"
date: March 14, 2015 18:58
category: "Discussion"
author: Daniel Salinas
---
Just finished a printable electronics case for my herculien build. I'm pretty happy with the results. 



![images/513f1af1d0d872c9c97f8af4d832547c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/513f1af1d0d872c9c97f8af4d832547c.jpeg)
![images/5126d6bc99269afae743d1520646140a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5126d6bc99269afae743d1520646140a.jpeg)

**Daniel Salinas**

---
---
**Eric Lien** *March 14, 2015 19:54*

Lots of cooling power there. Great job.


---
**Daniel Salinas** *March 14, 2015 20:06*

I had a couple prints on my RoBo 3D fail because of thermal protection so I figured two 40mm fans sucking in cold air and 1 80mm fan pulling it out over the controller should be more than enough to keep things cool. I put the vents in there to allow the raspi to get a passive airflow over it caused by the vacuum effect the larger fan will create. And it looks cool too. 


---
**Daniel Salinas** *March 14, 2015 20:37*

I've also made a raspi camera mount to attach to the case too. I just need play with the design a bit. 


---
**Daniel Salinas** *March 15, 2015 04:24*

I'm waiting on a delivery of more black and red slot covers to pack all the wiring away. I'm a little obsessive about neatness and I have max limit switches installed too so I have a few extra wires to hide but the build is coming along nicely. The documentation is pretty complete for everything mechanical. I need to add the photos to the last section and then once I have everything wired up I'll start documenting the last few parts like mounting the hotends and the fans to the xy carriage. There will be no electronics documentation since that's all very well documented by the reprap folks. **+Eric Lien**​ should have the finalized build doc by next week sometime for anyone who wants to take a look at it. You can notify me or Eric if you find any bugs in the doc and one of us can get it cleaned up. 


---
**Eric Lien** *March 15, 2015 05:00*

**+Daniel Salinas** thanks so much for all your hard work.


---
**Gus Montoya** *March 17, 2015 06:29*

**+Daniel Salinas** Your Herculien looks almost exactly as Eric's,. Nice :)  Whats your eta for first print? So excited for you .


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/Br2hkigLBJj) &mdash; content and formatting may not be reliable*
