---
layout: post
title: "thought I was being smart, but I ended up cracking the case, yet not messing up the bushing as before"
date: May 17, 2015 03:25
category: "Discussion"
author: Gus Montoya
---
thought I was being smart, but I ended up cracking the case, yet not messing up the bushing as before. I hope super glue saves the day.

![images/856fc0fd21b4f77d87a9bb4dae2fafeb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/856fc0fd21b4f77d87a9bb4dae2fafeb.jpeg)



**Gus Montoya**

---
---
**Derek Schuetz** *May 17, 2015 04:10*

try using some acetone around the bushings those parts are abs, or gently heat up the part with a heat gun then press in


---
**James Rivera** *May 17, 2015 04:20*

Or epoxy?


---
**Eric Lien** *May 17, 2015 04:48*

**+Derek Schuetz**​ careful with heat. The bronze is potted into the outer  bushing housing. It can break down and crumble with too much heat (ask me how I know ;). Also heat pushes the oil impregnated into the bronze out. They will look wet after heating... Thats the bronze loosing the oil.



Printing the carriage in ABS and acetone to soften the hole is the best way to go.﻿


---
**Mykyta Yurtyn** *May 17, 2015 06:49*

My part broke at exact same spot. I used abs pipe glue (essentially acetone with abs) to seal the crack. My concern though that the crack will throw it out of alignment. Still waiting to hook up motors to see how well it moves.  

How hot does it get around there? I'm wondering if i can reprint it with PETG. 


---
**Gus Montoya** *May 17, 2015 07:14*

I tried heating it, and it didn't help. The other bushing holes show stress also (but no cracking). I'll glue it and I'm sure it'll be fine. Considering how poor I assembled this one, I'll print out a new set of parts and reassemble with new bushings. I hate having an ugly machine.


---
**Isaac Arciaga** *May 17, 2015 09:02*

Use sand paper to adjust/enlarge the holes. The holes for the bushings should be adjusted large enough for you to be able to insert the bushings with a firm push using 1 of your thumbs.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/anaJ8Ub5v8n) &mdash; content and formatting may not be reliable*
