---
layout: post
title: "We need to find a way to add this tablet support thing :-) It does look nice"
date: February 24, 2015 18:47
category: "Discussion"
author: Dat Chu
---
We need to find a way to add this tablet support thing :-) It does look nice.





**Dat Chu**

---
---
**Ubaldo Sanchez** *February 24, 2015 18:59*

I was considering doing that with an old nook color and a pi. Repetier server gives you most of the options to control the printer, so it would probably work out OK. Figured I could step it up by using a nexus 7 or just installing a touch screen on the pi. 


---
**Dat Chu** *February 24, 2015 19:07*

Nexus 7 is nice and a good upgrade. There is a reprap Lcd thing posted recently. 


---
**Daniel Salinas** *February 24, 2015 21:26*

MatterControl has a new tablet to be a one stop controller for printers but I find that Octoprint has been my goto tool for printing and I like to slice on something that has a little more control than a tablet app offers me.



Also is it just me that finds the size of that printer for the small build platform it has to be slightly amusing?


---
**Dat Chu** *February 24, 2015 21:35*

It's not only you. Plus, it costs some pretty money too. I am also using the octoprint toolchain. But I wish there is a plug-in to cura and craftunique to send the gcode directly to octoprint. Having to manually upload it is kindda of a pain. 


---
**Eric Lien** *February 24, 2015 23:06*

I run simplify3d on a Windows 8 tablet on my Eustathios. It works great and I use chrome remote desktop to control it from anywhere.


---
**Jeff DeMaagd** *February 25, 2015 01:38*

The MatterControl tablet is a more than bit out of hand, IMO. It's basically an $80 tablet sans battery, with control software, packaged as a $300 3D printer controller. Buying a TW700 + Simplify 3D is $100 cheaper and you get a spare license left over. Or you run the MatterControl x86 software. Or any other x86 software.


---
**Daniel Salinas** *February 25, 2015 02:21*

I started with mattercontrol when I bought my RoBo3D but I have since moved to Simplify3D and I feel like they really nailed it with this software. I don't think there is any setting left unexposed. 


---
**Daniel Salinas** *February 25, 2015 02:22*

**+Eric Lien**​ I do something similar but its an 27" iMac that I remote into. :-)


---
**Eric Bessette** *February 25, 2015 02:26*

I like the wire/tube guides, looks slick.  I laughed when I saw the small print plate too. 


---
**Daniel Salinas** *February 25, 2015 16:21*

That thing looks like its the size of a herculien with a 200x180 X/Y build plate. Will anyone actually <b>buy</b> one???


---
**Dat Chu** *February 25, 2015 18:24*

People bought Makerbot 5th gen :|. I guess someone will buy this.


---
**Daniel Salinas** *February 25, 2015 20:02*

PT Barnum said there's a sucker born every minute...


---
**Jeff DeMaagd** *February 25, 2015 20:32*

Did anyone catch that nozzle spacing? Yow!



Something is seriously wrong with that machine's overall space efficiency. I think it could be 300mm cubed working volume, if not more, with that frame.


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/NU1CeBPFsrv) &mdash; content and formatting may not be reliable*
