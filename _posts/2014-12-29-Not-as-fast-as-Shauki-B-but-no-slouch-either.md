---
layout: post
title: "Not as fast as Shauki B , but no slouch either"
date: December 29, 2014 02:30
category: "Show and Tell"
author: Mike Miller
---
Not as fast as **+Shauki B**, but no slouch either. 

![images/c648aeeda09e94f4da1565d8d85a1c26.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c648aeeda09e94f4da1565d8d85a1c26.gif)



**Mike Miller**

---
---
**Eric Lien** *December 29, 2014 02:33*

Looks like your cooling duct had a little blunt force trauma ;)


---
**Mike Miller** *December 29, 2014 02:54*

It did...it's a <i>touch</i> too long...combined with a little adhesion problem. I'll iterate on it a little more...eventually. 


---
**James Rivera** *December 29, 2014 06:29*

Throw some duct tape on it. I think that's the 3D printer equivalent of, "Rub some dirt on it".  ;)


---
**James Rivera** *December 29, 2014 06:31*

Hey--that's your garolite bed, right?  I know you've got blue tape here, but are you printing nylon?


---
**Mike Miller** *December 29, 2014 13:30*

I wish...I'm currently still working on ABS print strength and quality. That part in the video is a Bic Lighter flintwheel extruder...hoping my surface quality issues are extruder related. 


---
**Miguel Sánchez** *December 29, 2014 15:16*

From 0:28 till the end I can only hear the sound but video freezes in both my cellphone and my laptop (though for the sound of it, it seems where the fastest moves are happening)


---
**Mike Miller** *December 29, 2014 15:41*

Uf da, blame my iPhone. I'll use a real camera, next time I go for land speed records.


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/BJjxwJfB8Si) &mdash; content and formatting may not be reliable*
