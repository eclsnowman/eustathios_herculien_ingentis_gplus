---
layout: post
title: "I have purchase the Azteeg X3 controller, what firmware is recommended or preferred for the Herculien?"
date: May 09, 2015 22:28
category: "Discussion"
author: Vic Catalasan
---
I have purchase the Azteeg X3 controller, what firmware is recommended or preferred for the Herculien? is there a sample configuration file I can view for setting things up?  Thanks!





**Vic Catalasan**

---
---
**Eric Lien** *May 09, 2015 22:50*

I use marlin: [https://github.com/eclsnowman/HercuLien/tree/master/Marlin%20Firmware](https://github.com/eclsnowman/HercuLien/tree/master/Marlin%20Firmware)


---
**Eric Lien** *May 09, 2015 22:52*

It is a little older version now. But that folder is ready to flash. If you use a Viki LCD you will need the adder library for arduino [https://github.com/lincomatic/LiquidTWI2](https://github.com/lincomatic/LiquidTWI2)


---
**Eric Lien** *May 09, 2015 22:53*

I am not sure about the viki2, I use smoothieware on an azteeg x5 mini for that. On my Eustathios.


---
**Eric Lien** *May 10, 2015 15:34*

For my Eustathios V1 I have the config file here: [https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Smoothieware/Eustathios%20V1%20Config%20File](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Smoothieware/Eustathios%20V1%20Config%20File)


---
**Eric Lien** *May 10, 2015 15:35*

**+Eric LeFort** I used the edge build of smoothieware that was new at the time. But to be honest smoothieware is REALLY easy to config. Nothing like Marlin configs which can take some time.


---
**Daniel Salinas** *May 11, 2015 19:05*

Vic I had really good luck with Marlin using the development branch directly from github.  They've made some great improvements to Marlin that haven't been "released" yet.  I update my herculien's firmware weekly to keep the current firmware on it.


---
**Eric Lien** *May 11, 2015 22:44*

**+Daniel Salinas** I would love to see your latest updates on the firmware.


---
**Vic Catalasan** *May 11, 2015 23:13*

Thanks for the help... there's quite a bit of information to digest so am glad I have time to chew through them as parts for the build slowly arrive. 


---
**Vic Catalasan** *May 12, 2015 22:45*

Eric I was getting compile error and I did not not know where to start so I downloaded the standard Marlin file and was able to connect to the Azteeg, will probably take bits and pieces of the settings and move them over as I learn more... I turned on the fan and the hot end light goes on... so will need some more reading... 


---
**Eric Lien** *May 12, 2015 23:56*

**+Vic Catalasan** do you have the needed Arduino library in the arduino folder for the LCD?


---
**Vic Catalasan** *May 13, 2015 08:10*

**+Eric Lien** Thank a bunch! Got the Herculien setting to run. Had to learn how to actually use Arduino properly. Will play with some board functions. 


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/FBaf1koWbdP) &mdash; content and formatting may not be reliable*
