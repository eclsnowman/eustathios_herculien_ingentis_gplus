---
layout: post
title: "I found an easy way to set the bar heights on Eustathios using the spacers I posted earlier"
date: February 07, 2015 00:07
category: "Show and Tell"
author: Eric Lien
---
I found an easy way to set the bar heights on Eustathios using the spacers I posted earlier. Some fine tuning may be required based on the accuracy of your printed carriage or spacers as well as how the bushings (or bearings in some cases) settle in your carriage. But it gives a consistent alignment to start from.﻿



Here are the files again for anyone who missed the first post: [https://drive.google.com/folderview?id=0B1rU7sHY9d8qVnp2WWxxZTFCNVU&usp=sharing](https://drive.google.com/folderview?id=0B1rU7sHY9d8qVnp2WWxxZTFCNVU&usp=sharing)



![images/b4fd7cf2691d309990a4b47d03f9c6ea.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b4fd7cf2691d309990a4b47d03f9c6ea.jpeg)
![images/de205a841f934b182685fce06e744b3a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/de205a841f934b182685fce06e744b3a.jpeg)

**Eric Lien**

---
---
**Mikael Sjöberg** *February 07, 2015 06:18*

Nice, a simple and practical solution !


---
**Richard Mitchell** *February 07, 2015 06:35*

I saw the spacer design the other day but didn't think of using it with the printer inverted. Genius.


---
**Erik Scott** *February 10, 2015 21:50*

Are your rods spaced the same as the original sketch up design? Your bearing mounts seem to be positioned differently. ﻿I ask because I'd love to take advantage of these alignment guides. 


---
**Eric Lien** *February 10, 2015 23:38*

**+Erik Scott** yes the rods are spaced 14mm apart center to center. This is the same spacing and location as the original Eustathios.


---
**Gus Montoya** *February 18, 2015 05:23*

Hi Eric,  I have  already ordered my frame from Misumi, it should arrive by next week or so. But I am finding difficulty figuring out the pulleys,  bearings, linear shafts, lead screws etc. Is it safe to order based on @Jason Smith Bom spreadsheet? It's exactly what you have correct?


---
**Eric Lien** *February 18, 2015 07:02*

**+Gus Montoya** If you don't mind waiting a little bit I will have a more detailed BOM soon. Jasons list is great, but for example I would replace line 7 for GPA32GT2060-A-P5 with these: 20 tooth 5mm bore from [http://www.smw3d.com/gt-pulleys/](http://www.smw3d.com/gt-pulleys/)



Also you can get the 32tooth 10mm bore from the same place. 



I am getting close if you can spare to wait. I promise, less than 1 week and I should have V2 models, BOM, and files more formalized.


---
**Gus Montoya** *February 18, 2015 08:26*

NP Eric, I will be more patient. Sorry for the constant hagging :/ 


---
**Gus Montoya** *June 23, 2015 08:35*

Hi Eric 

   I have the files and I imported them into solid works. I tried using the measuring tool but for some reason it is not working. I want to varify that the two alignment tools are the correct size. Do you have the dimensions?


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/Bso1ocs77iv) &mdash; content and formatting may not be reliable*
