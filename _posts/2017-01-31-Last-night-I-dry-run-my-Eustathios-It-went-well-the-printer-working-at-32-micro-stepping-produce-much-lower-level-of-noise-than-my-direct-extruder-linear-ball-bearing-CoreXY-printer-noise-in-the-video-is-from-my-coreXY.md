---
layout: post
title: "Last night I dry run my Eustathios, It went well, the printer working at 32 micro stepping, produce much lower level of noise than my direct extruder linear ball bearing CoreXY printer( noise in the video is from my coreXY"
date: January 31, 2017 17:40
category: "Build Logs"
author: larry huang
---
Last night I dry run my Eustathios, It went well, the printer working at 32 micro stepping, produce much  lower level of  noise than my direct extruder linear ball bearing CoreXY printer( noise in the video is from my coreXY printer at a few meter away). I use lower spec step motor on X and Y  axis, they run cold at 200mm/s travel rate. And I use light switch on the Z endstop, the well planned aluminum extrusion frame design really make life easier.﻿



![images/77b97aae13537f672383f36ddd2a5b24.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/77b97aae13537f672383f36ddd2a5b24.jpeg)
![images/99b5e89327eea285688f8671cf678421.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/99b5e89327eea285688f8671cf678421.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-H6UutcjcKcs/WJDMG1Xl9kI/AAAAAAAAA2U/O6OAML-jMGANQ7r63AgSZisz-F6niyz4gCJoC/s0/VID_20170201_010522%2525281%252529.mp4**
![images/e23b3cea2de3ccae2b65656a62bbe937.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e23b3cea2de3ccae2b65656a62bbe937.jpeg)

**larry huang**

---
---
**Eric Lien** *February 03, 2017 13:04*

Just found your post in the g+ community spam filter for some reason. Sorry for the delay in getting it out of spam. I got behind a few days on checking the spam folder.


---
**Eric Lien** *February 03, 2017 13:04*

Looking good.


---
**larry huang** *February 03, 2017 19:04*

Thanks,  I'll look for what might be the cause.


---
*Imported from [Google+](https://plus.google.com/104789216831460398100/posts/1eHFxF9zy4i) &mdash; content and formatting may not be reliable*
