---
layout: post
title: "I keep seeing that people are using Spectra Line, but what I want to know is what size line?"
date: March 13, 2014 06:51
category: "Discussion"
author: Jason Barnett
---
I keep seeing that people are using Spectra Line, but what I want to know is what size line? 100LB? 5LB? What is the right balance of strength and flexibility?





**Jason Barnett**

---
---
**Tim Rastall** *March 13, 2014 06:52*

120lb for me.


---
**Mike Miller** *March 13, 2014 16:12*

100 Lb for my 3DR


---
**Jason Barnett** *March 13, 2014 19:54*

Thank you for the info. Now, off to see if I can find it in lengths of less than 500 meters! :)


---
**Mike Miller** *March 14, 2014 03:21*

ebay. (I can't say as it was my idea, someone else told me.)


---
**Brian Bland** *March 14, 2014 03:24*

How much do you want?  I have 100lb.  Cover shipping and I will send what you need.


---
**Jason Barnett** *March 14, 2014 14:59*

**+Brian Bland** That would be awesome! I'll contact you directly to make arrangements. Thank you!


---
*Imported from [Google+](https://plus.google.com/108834075676294261589/posts/gYv6mJL9Ecw) &mdash; content and formatting may not be reliable*
