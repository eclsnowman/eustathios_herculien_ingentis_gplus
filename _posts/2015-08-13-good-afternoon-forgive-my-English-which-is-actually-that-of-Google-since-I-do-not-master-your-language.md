---
layout: post
title: "good afternoon, forgive my English which is actually that of Google, since I do not master your language"
date: August 13, 2015 18:58
category: "Discussion"
author: rafael lozano
---
good afternoon, forgive my English which is actually that of Google, since I do not master your language. Just to thank you for your work, which I used to fabricarme a new 3d printer. I miss some modifications to use diameter 10 and already becomes limited.



![images/bc4696ace15057adae9d12d2b5e6ec51.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bc4696ace15057adae9d12d2b5e6ec51.jpeg)
![images/338e77aecf6d9906aac6d9f47398f466.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/338e77aecf6d9906aac6d9f47398f466.jpeg)
![images/1d72dc6db275b12c5d816a3643d95b9d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1d72dc6db275b12c5d816a3643d95b9d.jpeg)
![images/ddc9f6fac1b1765f20331048d2d8ce22.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ddc9f6fac1b1765f20331048d2d8ce22.jpeg)
![images/40a3064c9df3a1d36a850d007824c32b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/40a3064c9df3a1d36a850d007824c32b.jpeg)
![images/2f46cd59f79036a9691f6a057a9aea46.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2f46cd59f79036a9691f6a057a9aea46.jpeg)
![images/49202287e0eca20693f7bd01ad0e8fbc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/49202287e0eca20693f7bd01ad0e8fbc.jpeg)
![images/e10ff147801997444d3e9fb867d9523a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e10ff147801997444d3e9fb867d9523a.jpeg)

**rafael lozano**

---
---
**Daniel F** *August 14, 2015 06:18*

Congratulation on your build, it looks good.  The only potential issue I see is your Z axis, I dont think these tubes can suport load. Did you fix the threaded rods at the top (hard to see in the pictures)?

I also use 10mm threaded rods but fixed them at the bottom acootding Erics spider v2 design. ﻿


---
**Selim MERIC** *August 17, 2015 07:24*

I need some design for Z axis so could you share design files.. or STL


---
**rafael lozano** *August 22, 2015 23:21*

**+Selim MERIC**   sorry for not responding earlier today I finally managed to post it on github

[https://github.com/rafaelloz/mod-eustathios.git](https://github.com/rafaelloz/mod-eustathios.git)


---
*Imported from [Google+](https://plus.google.com/114113589997084082680/posts/WoGstfTmynz) &mdash; content and formatting may not be reliable*
