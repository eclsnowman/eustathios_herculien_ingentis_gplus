---
layout: post
title: "I have a 300x300x6 mm piece of borosilicate?"
date: August 08, 2014 17:36
category: "Discussion"
author: Øystein Krog
---
I have a 300x300x6 mm piece of borosilicate? glass from a broken bathroom scale that I am thinking of using as a print surface/bed.

It's quite heavy, is that something I should be concerned about?

I'm not sure how flat it is, any ideas for a good way to find out?





**Øystein Krog**

---
---
**D Rob** *August 09, 2014 00:27*

What type of printer is the bed the z? if so you'll be fine. If it is the y you will lose acceleration and speed potential as well as possibly have driver and stepper heat/current issues


---
**D Rob** *August 09, 2014 00:29*

As to flatness lady a straight edge on it on its' side at several angles look for a gap. Window glass after getting hot will flatten out to the surface it is on


---
**Øystein Krog** *August 09, 2014 10:11*

**+D Rob**  This will be for an ingentis/eustatihos-style printer, which is why I am even considering it at all :)

Thanks for the tip about straightness, I guess the "several angles" part is pretty important.

I'm pretty sure this borosilicate as it is really tough, is this "melting" effect really that significant though?


---
**D Rob** *August 09, 2014 10:22*

**+Øystein Krog**  on window glass it is. when I take a pane off hot is takes a convex shape. if I put it on after cooling that way I have to heat it to completely before clipping it back down or it will shatter.


---
**Øystein Krog** *August 09, 2014 10:25*

Wow, I had no idea. I guess the same effect may apply for boro glass then. That's good in a way I guess:P


---
**D Rob** *August 09, 2014 11:13*

+Øystein Krog deffinitel. window glass after proper heating for abs levels out my slightly unlevel alu bed﻿


---
**D Rob** *August 18, 2014 10:44*

All the the kids are using inductive sensors for z probes. With that stuff being 6mm on aluminum plate you'll probably not be able to do this


---
*Imported from [Google+](https://plus.google.com/+ØysteinKrog/posts/6c3122Zsg3f) &mdash; content and formatting may not be reliable*
