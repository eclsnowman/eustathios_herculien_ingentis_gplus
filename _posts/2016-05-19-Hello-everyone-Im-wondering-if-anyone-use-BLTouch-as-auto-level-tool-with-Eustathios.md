---
layout: post
title: "Hello , everyone. I'm wondering if anyone use BLTouch as auto level tool with Eustathios?"
date: May 19, 2016 19:42
category: "Build Logs"
author: Botio Kuo
---
Hello , everyone. I'm wondering if anyone use BLTouch as auto level tool with Eustathios? And also what changes I need to do if I want to use BLTouch ? I'm thinking about using Cartesian Flying Extruder with BLTouch. Is it possible ? or I don't need auto level system with Eustathios ? Sorry for so many questions...





**Botio Kuo**

---
---
**Maxime Favre** *May 19, 2016 20:29*

Honestly auto bed leveling is more a complication of a problem than a solution. If you take the time to set your bed right it will cost you less time and money in the end.




---
**Eric Lien** *May 19, 2016 20:58*

The repeatability of my HercuLien and Eustathios beds are so good that I almost never need to make any adjustments. That being said dramatic changes in print temps can effect things. The thermal expansion of aluminum is actually very high. So  a bed at 35C and hot end at 200C will have a different seperation distance than a 110C bed and 245C hot end. 



A BLtouch bed probe could compensate for bed deviation. But not the linear growth of the hot end at various temps.



What I have instead is get the bed very very well trammed and use that as your reference. Then build in Z offset start gcodes into your slicer recipes. It takes a little trial and error to find the correct valves at differnt temps. But once it is done and you have found the thermal expansion offsets, you are good to go no matter the temps or materials.


---
**Botio Kuo** *May 19, 2016 23:46*

**+Eric Lien** Compare to the newest 3d printer design, I still think Eustathios is the best for me. I think you are right ... It doesn't need auto level. :)


---
**Botio Kuo** *May 20, 2016 03:05*

**+Eric Lien** Which one I should buy ? 

OMRON G3NA-220B-DC5-24 ?

or OMRON G3NA-220B-AC100-120? 



They are all SSR


---
**Eric Lien** *May 20, 2016 05:08*

Since you want to trigger with a DC signal in to switch the AC for the bed you want:



Omron G3NA-220B-DC5-24 Solid State Relay, Zero Cross Function, Yellow Indicator, Phototriac Coupler Isolation, 20 A Rated Load Current, 24 to 240 VAC Rated Load Voltage, 5 to 24 VDC Input Voltage [https://www.amazon.com/dp/B003B2Z0N6/ref=cm_sw_r_cp_apa_RSPpxbF9JKCSM](https://www.amazon.com/dp/B003B2Z0N6/ref=cm_sw_r_cp_apa_RSPpxbF9JKCSM)


---
**Botio Kuo** *May 20, 2016 05:14*

**+Eric Lien** Thank you so much, you are the best


---
**Eric Lien** *May 20, 2016 06:00*

**+Botio Kuo** thanks. I try to be helpful where I can. Just pay it forward once you have the knowledge :)


---
**Botio Kuo** *May 21, 2016 08:47*

**+Eric Lien** Hi, one more question about the bearing on BOM 10x22x6 Rubber Sealed Bearing 6900-2RS (10 Units)- sum of parts by vender tab said the number is 8. It meant 8 packs I need to order ? Because 1 pack has 10 units ... so I just confused ... thanks


---
**Maxime Favre** *May 21, 2016 08:56*

8 parts, buy 1 pack :)


---
**Botio Kuo** *May 21, 2016 17:50*

sorry, I have a question. I didn't see bondtech extruder on BOM sheet. But I need to buy one right? Thanks


---
**Eric Lien** *May 21, 2016 18:47*

Yeah I have the one that I designed in the bill of materials for those interested. But a Bondtech QR is better by far and would be the way I would recommend you go.



**+Martin Bondéus**​ has done such great work refining his design. There's no way I can compete with that. That's why I own four of them and a mini :-)


---
**Botio Kuo** *May 22, 2016 00:45*

**+Eric Lien** hi, so u meant I need four of Bondtech QR for eustathios ? Thx 


---
**Eric Lien** *May 22, 2016 02:37*

**+Botio Kuo** no, I just own 4 for all my printers. Long story short get a Bondtech:)


---
**Botio Kuo** *May 22, 2016 03:52*

**+Eric Lien** hi, so I only need one?! 


---
**Eric Lien** *May 22, 2016 03:59*

**+Botio Kuo** unless you want to mount a dual extruder carriage. But yes a default Eustathios is a single extruder, so you will need only one.


---
**Botio Kuo** *May 22, 2016 04:08*

**+Eric Lien** haha thank you, idk eustathios can have two extruder. 


---
**Eric Lien** *May 22, 2016 04:22*

**+Botio Kuo**​ HercuLien and Eustathios carriage parts are interchangeable. But of course the HercuLien carriage is larger so you would loose build space in X/Y. I almost never use my dual extruder. Single extruder with a slicer that can generate quality support wins hands down in my opinion.﻿


---
**Botio Kuo** *May 22, 2016 07:31*

**+Eric Lien** sorry ask again, about the cable for QR, which one I should choice ? standard one ? Thanks [http://shop.bondtech.se/ec/extruders/bondtech-qr-175-universal.html](http://shop.bondtech.se/ec/extruders/bondtech-qr-175-universal.html) 


---
**Eric Lien** *May 22, 2016 12:01*

**+Botio Kuo** yes the 2510 standard end should be good. But that will depend on the board you buy of course. For example I always prefer a board with screw terminals. But that's just my preference coming from the industrial equipment side of things.


---
**Botio Kuo** *May 23, 2016 02:35*

**+Eric Lien** Hi Eric, if I buy QR extruder, do I still need 8mm_Shaft_Hobb_Gear ? I don't want to buy something I don't need for saving money. Thank you so much


---
**Eric Lien** *May 23, 2016 03:05*

No that and the motor will no longer be needed.


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/NJaUQAWGyUr) &mdash; content and formatting may not be reliable*
