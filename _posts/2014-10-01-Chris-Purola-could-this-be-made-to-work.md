---
layout: post
title: "Chris Purola could this be made to work?"
date: October 01, 2014 23:14
category: "Discussion"
author: D Rob
---
[http://www.ebay.com/itm/L293D-Motor-Drive-Shield-Expansion-Board-For-Arduino-Duemilanove-Mega-UNO-/221541254774?pt=LH_DefaultDomain_0&hash=item3394e34676](http://www.ebay.com/itm/L293D-Motor-Drive-Shield-Expansion-Board-For-Arduino-Duemilanove-Mega-UNO-/221541254774?pt=LH_DefaultDomain_0&hash=item3394e34676)

**+Chris Purola** could this be made to work? Anyone else have an opinion?





**D Rob**

---
---
**Chris Purola (Chorca)** *October 01, 2014 23:20*

The H-bridges on that board are fairly low-current, only 0.6A apiece. It would work for a smaller motor. The biggest hurdle for me would be the software and doing PID. The video you posted awhile back had someone who had used the arduino PID library to handle that along with some hardware interrupts for high speed encoder processing. At that point it's just how fast you can run the motors and how well the PID tuning works.

The H-bridges can be scaled up using MOSFETS, which it appears is all that Gecko does, there is a set of TO220 packages on the rear heatsink.


---
**D Rob** *October 01, 2014 23:28*

Have you seen any open or cheap alternatives to the gecko?


---
**D Rob** *October 01, 2014 23:30*

Could there be a PID autotune algorithm, similar to tuning a hot end or heated bed, that uses the encoder feedback in the same way thermistors give feedback ? 


---
**Chris Purola (Chorca)** *October 01, 2014 23:36*

That would be AMAZING and awesome. From what I've read, some high-end commercial drives have that feature. See: 
{% include youtubePlayer.html id=EeyWN5bhexA %}
[Mitsubishi Electric Servo Auto tuning](https://www.youtube.com/watch?v=EeyWN5bhexA)


---
**Miguel Sánchez** *October 02, 2014 07:48*

Like in the hotend auto-tuning, you have a closed feedback loop here due to the enconder, so once you set up your system (belts and carriages are added, you'll need to set the PID values for each axis too. I do not see why it could not be automated (something like M303 XYZE)


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/68GL8gz1dzt) &mdash; content and formatting may not be reliable*
