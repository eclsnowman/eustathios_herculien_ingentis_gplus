---
layout: post
title: "Can someone explain how I need to hook up the wiring and filament tubing?"
date: July 31, 2015 20:52
category: "Discussion"
author: Brandon Cramer
---
Can someone explain how I need to hook up the wiring and filament tubing? I have this 8 position connector that goes in the mix somewhere. Do I terminate all my wiring from the extruder to it and then run the jumper wire down to the bottom and send those wires where they need to go.



What is going to hold all of this up while the carriage is moving around? 





**Brandon Cramer**

---
---
**Eric Lien** *July 31, 2015 21:30*

2 wires for the hot end, 2 wires for the thermistor, 2 wires for the part cooling fan, 2 wires for the hotend cooling fan. 8 in total.


---
**Eric Lien** *July 31, 2015 21:31*

This post has pictures of the carriage: [https://plus.google.com/+EricLiensMind/posts/WT79ruM7StQ](https://plus.google.com/+EricLiensMind/posts/WT79ruM7StQ)


---
**Eric Lien** *July 31, 2015 21:36*

I put wire loom over the wiring, and use a hanger wire to give the bundle structure going up about 60% of the way. That keeps the curve nice and gentle, but also holds it up and out of the way.


---
**Eric Lien** *July 31, 2015 21:39*

[http://imgur.com/Nanv7pI](http://imgur.com/Nanv7pI)


---
**Brandon Cramer** *July 31, 2015 22:16*

Thanks **+Eric Lien**  Do you have that same photo only showing the entire wiring going up over the printer. Seem like will just lay down on me. 


---
**Eric Lien** *July 31, 2015 22:21*

My finger in that photo is pointing to the hanger wire. It goes down into the vertical corner extrusion. That's what gives it the structure going vertical.﻿


---
**Eric Lien** *July 31, 2015 22:22*

**+Brandon Cramer** I don't, I won't be home this weekend. I am driving to the cabin now.


---
**Brandon Cramer** *July 31, 2015 22:23*

**+Eric Lien**   Ah! I see that now. No worries. Thanks for the help. Have a good weekend!!!


---
**Eric Lien** *July 31, 2015 22:41*

**+Brandon Cramer** thanks man. I need a little get away.


---
**Brandon Cramer** *July 31, 2015 23:13*

What do you think? 



[https://www.dropbox.com/s/pluk2zd672h49md/2015-07-31%2016.11.34.jpg?dl=0](https://www.dropbox.com/s/pluk2zd672h49md/2015-07-31%2016.11.34.jpg?dl=0)


---
**Brandon Cramer** *July 31, 2015 23:14*

I really need to find a black fan! That white one is hideous!!! 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/ihjkW2EzdKe) &mdash; content and formatting may not be reliable*
