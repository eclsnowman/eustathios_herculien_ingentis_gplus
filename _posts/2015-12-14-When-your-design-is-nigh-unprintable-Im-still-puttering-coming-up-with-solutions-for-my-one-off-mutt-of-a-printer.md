---
layout: post
title: "When your design is nigh-unprintable. I'm still puttering, coming up with solutions for my one-off mutt of a printer"
date: December 14, 2015 13:07
category: "Deviations from Norm"
author: Mike Miller
---
When your design is nigh-unprintable. 



I'm still puttering, coming up with solutions for my one-off mutt of a printer. The bed has two, narrow-ish extrusions going the width of the printer as the base, and I wanted three arms to support the build plate. 



(yeah, LOTS of caveats here, including the fact that  Garolite is non-structural...it'll be clipped to a 1/4" glass plate, and my problem would be solved by, you know, breaking out a drill and a bolt)



I wanted a piece that would clip over the back extrusion...three would allow for pretty much any size bed, with lots and lots of 'do whatever you want'. 



But the part I'm imagining really can't be printed without a whole lot of support, which, you know, kinda messes things up from a snap-to-fit standpont. 



![images/96b905db31d561493e8b450d7762ee7e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/96b905db31d561493e8b450d7762ee7e.jpeg)
![images/0c75f00b0a1bd4799d4099d331a96ece.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0c75f00b0a1bd4799d4099d331a96ece.png)
![images/c05bd662afc34d0d9ff13aaf083b0da2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c05bd662afc34d0d9ff13aaf083b0da2.jpeg)

**Mike Miller**

---
---
**Mike Miller** *December 14, 2015 17:58*

Mostly from an assembly standpoint, I have to disassemble all I've done to this point. :P


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/HuVkTHSkzA2) &mdash; content and formatting may not be reliable*
