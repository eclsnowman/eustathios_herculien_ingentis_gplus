---
layout: post
title: "Hi Folks. Just wanted to apologise for making promises to share files or designs and then failing to do so - I'm super busy at work right now and I had my CAD PC died so I couldn't get Rhino fired up to export the relevant"
date: May 16, 2014 02:03
category: "Discussion"
author: Tim Rastall
---
Hi Folks. Just wanted to apologise for making promises to share files or designs and then failing to do so - I'm super busy at work right now and I had my CAD PC died so I couldn't get Rhino fired up to export the relevant files. I've invested in a grunty 15" Macbook pro with VMware so I've got Rhino going again. As soon as I get the chance I'll export all of my finished or work in progress designs to IGES/STEP and put them on Google drive. 

**+Brian Bland** **+Eric Lien** **+Jarred Baines** 





**Tim Rastall**

---
---
**Eric Lien** *May 16, 2014 02:51*

No rush. I would hate to think how long it would take me to get back online after a crash. My cad station is also my server and has 12 x 2tb hard drives in raid 6 and 4 ssd's in raid 10. The initialization alone would kill me while waiting. 


---
**Mike Brown** *May 16, 2014 03:00*

So are the build instructions still good on the website or are there updated ones?  I'm excited for this thing.


---
**Tim Rastall** *May 16, 2014 07:12*

**+Eric Lien** jesus Eric.  Apart from the crazy costs,  how much storage do you need? What are you doing with such a beast? 


---
**Ricardo de Sena** *May 16, 2014 07:27*

Very good, I want to start over and I'm always in search of new features derived from Tantillus embrace.


---
**Tim Rastall** *May 16, 2014 07:54*

**+Mike Brown** the build instructions for Ingentis are still valid but I'd recommend the Eustathios design **+Eric Lien** is following as it builds on the core concept of Ingentis with several improvements.  The Ingentis belt drive produces lovely consistent layers,  so I can't quite recommend lead screws  instead but they have the advantage of not letting the bed drop if the motor cuts out. ﻿


---
**Eric Lien** *May 16, 2014 12:04*

**+Tim Rastall** I am the storage center for friends & family through crashplan (free peer to peer). Then I payed $130 for 4 years of crashplan hosted so it all backs up off site. I am kind of a collector so I have everything I ever downloaded since 1998. And I am a sucker for solid backups. 



I have my server in raid, backup critical info to an onsite NAS, then redundant backup off site. You might describe me as neurotic on backup :)﻿


---
**ThantiK** *May 16, 2014 20:37*

Heh, I actually downloaded a pirated version of Solidworks last night, and turns out it won't install on a Non-SP1 pirated version of Windows 7...hah!  Was gonna shorten those printed pieces, but instead I just borrowed some bolts from DM


---
**George Salgueiro** *May 19, 2014 20:55*

Take your time.


---
**Jarred Baines** *May 20, 2014 13:15*

Pfft - not good enough **+Tim Rastall**!



Nah no pressure mate, so busy right now it takes me a week or so to even read my G+ notifications! 



Sorry to hear bout the laptop - I'm really getting nervous about my NAS at home which has everything I've ever done on it, it's probably time to take a page from **+Eric Lien**'s book!



Good luck with the rescue operation Tim! 


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/7k5Qf3djbL8) &mdash; content and formatting may not be reliable*
