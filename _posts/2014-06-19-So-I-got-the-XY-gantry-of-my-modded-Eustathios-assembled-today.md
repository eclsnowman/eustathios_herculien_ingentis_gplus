---
layout: post
title: "So I got the XY gantry of my modded Eustathios assembled today"
date: June 19, 2014 08:39
category: "Show and Tell"
author: Erik Scott
---
So I got the XY gantry of my modded Eustathios assembled today. Things are beginning to move pretty quickly now that I have nearly all the parts. 



I know my Z-axis modification was a bit controversial when I posed the CAD images earlier. I don't have the bed on the leadscrews yet, but so far just from moving it up and down I don't get any friction causing it to tilt forward and back, which is promising. I think it's going to work out. 



The XY gantry isn't quite as smooth as I'd like. I think it'll get smother as things begin to settle and those bronze bearings self-align. In any case, I'm fairly confident the steppers will be able to handle it. I redesigned the bearing holders to be printed without support material and less plastic in the style of **+Jarred Baines**'s beautiful CNC machined ones. Pretty happy with how those came out.  



The XY Carriage is again my own design and probably a bit over-engineered. I'm doing a dual extruder setup as the water soluble PVA as support material really appeals to me. Plus, it gives me more flexibility in general. The E3D hotends just slide on in the grove mount and are held in with a custom machined aluminum bracket attached under the main carriage block. The carriage slides on 4 LM8UU bearings, which, as I said is probably overkill, but still preferable in my opinion. I have mounting provisions on the back of the carriage block for 2 40mm cooling fans for the plastic as it's extruded. In my experience with other printers, this is just about required for good prints with overhangs. 



For the extruder itself, I'm going with this one from thingiverse: [http://www.thingiverse.com/thing:185005](http://www.thingiverse.com/thing:185005)

I'll probably re-design it in the future, but it'll work for now. I don't like the idea of using something from thingiverse. 



I will likely be adding a heated bed in the future, but for now I think I've complicated things enough. 



If anyone is interested in my CAD files, I'd be happy to share them. I have all the bits relevant to my modifications made and it can compliment **+Eric Lien**'s CAD work. 



Anyway, enjoy the pictures, and I apologize for making this text so long. I just want to make sure my thoughts and process are documented for others' benefit. 



![images/a0c147bc24b1c95552cc54f7d2119bb6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a0c147bc24b1c95552cc54f7d2119bb6.jpeg)
![images/65a413e9e3a0ec5bd9c3ed65ef75da62.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/65a413e9e3a0ec5bd9c3ed65ef75da62.jpeg)
![images/19a62bf8d607cb7040a459b09aa19a17.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/19a62bf8d607cb7040a459b09aa19a17.jpeg)
![images/e0140e7e55229842970f429a83b88f87.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e0140e7e55229842970f429a83b88f87.jpeg)
![images/03efdaa5a27ffaf7377a0b7ee680cc8f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/03efdaa5a27ffaf7377a0b7ee680cc8f.jpeg)
![images/18fbef849155ec5520a04485ce364f4d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/18fbef849155ec5520a04485ce364f4d.jpeg)
![images/056f9960a5512f4ca071e41e69e69945.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/056f9960a5512f4ca071e41e69e69945.jpeg)
![images/b0482612dc69ac7916e141d7aa460c5a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b0482612dc69ac7916e141d7aa460c5a.jpeg)
![images/375d8d44247847f46364257a5eecb398.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/375d8d44247847f46364257a5eecb398.jpeg)
![images/6a487b432aa85c377b64e60decd37f3f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6a487b432aa85c377b64e60decd37f3f.jpeg)
![images/07bb8752ac68d87b66eff02d52f9c8cc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/07bb8752ac68d87b66eff02d52f9c8cc.jpeg)
![images/c2465158b63218e8519e0f4261068624.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c2465158b63218e8519e0f4261068624.jpeg)
![images/c216d30410091f443c7a9e2d8cdcdbb0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c216d30410091f443c7a9e2d8cdcdbb0.jpeg)
![images/a976ea4cff681b204f09e26ea20f708e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a976ea4cff681b204f09e26ea20f708e.jpeg)
![images/f63225fcc005c8565a8453c48a4edde0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f63225fcc005c8565a8453c48a4edde0.jpeg)

**Erik Scott**

---
---
**Tim Rastall** *June 19, 2014 09:22*

Super awesome.


---
**Eric Lien** *June 19, 2014 10:51*

Looking good.


---
**Wayne Friedt** *June 19, 2014 14:53*

I like the fact that you have 2 sliders per side for the Z.


---
**Eric Lien** *June 19, 2014 17:43*

One thing I have found is the rods need to be very precise in level and square for smooth motion. I ended up printing a jig for the corners based on the measurements of my actual center carriage spacing. Then I also found over tightening the bolt on the rod end slides caused undue binding. I loosened them up and instead used loctite so the would not loosen from vibration.



Lastly per Tim I ran it for an hour cycling around the perimeter and crossing over the center.



Now my printer can be moved around with one finger and minimal pressure.﻿


---
**Erik Scott** *June 19, 2014 21:15*

I ran into the same issue with the M5 screw on the X and Y sliders. I just left them tight enough to stay in place. 



Could you explain this jig a bit more? I feel it would be worth my time to get this as smooth as possible and I'm sure others would be interested. 



And yes, I anticipate it loosening up over time. 


---
**Eric Lien** *June 19, 2014 21:37*

What I did was measure the distance between rod bushings on the assembled center carriage. And then I printed a spacer to support the rods near the flange bearings at that proper distance. Then I tighten down the flanges and removed the jig.﻿


---
**Jarred Baines** *June 21, 2014 10:03*

A simple spacer block held up against the frame and touching the z rod should achieve the same thing if you have something the right size. 



I'm intrigued about your Z axis setup, if you do put the cad files up post a link on the forum so I can have a squiz ;-)



Looks great so far!


---
**Erik Scott** *June 21, 2014 20:19*

The spacer idea would probably be my best bet. 



I'll see if I can get those CAD files to you. Might have to finally figure out GitHub. They're all SolidWorks 2013 files, but I can convert the assembly to a STEP for other people. 


---
**Erik Scott** *June 21, 2014 21:16*

I have uploaded all my files to GitHub here: [https://github.com/erikscott128/Eustathios-EWS-Mod](https://github.com/erikscott128/Eustathios-EWS-Mod)


---
**Jarred Baines** *June 23, 2014 09:16*

You da man ;-)



Cheers Erik!


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/F4YxHXuedU7) &mdash; content and formatting may not be reliable*
