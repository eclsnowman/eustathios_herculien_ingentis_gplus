---
layout: post
title: "Is anyone having any black Friday deals on electronics?"
date: November 28, 2014 05:53
category: "Discussion"
author: Seth Mott
---
Is anyone having any black Friday deals on electronics?  I am just starting a eustathios build and need a full set of electronics, LCD, power supply, stepper motors, ect.





**Seth Mott**

---
---
**Brian Bland** *November 28, 2014 06:05*

Lulzbot is having Cyber Monday sale on Ramps, Mega and Drivers


---
**Matt Miller** *November 28, 2014 06:07*

[https://www.lulzbot.com/blog/lulzbot-2014-cyber-monday-sale?pk_campaign=sale-alert_phplist&pk_kwd=cyber-monday-blog-link](https://www.lulzbot.com/blog/lulzbot-2014-cyber-monday-sale?pk_campaign=sale-alert_phplist&pk_kwd=cyber-monday-blog-link)


---
**R K** *November 28, 2014 13:48*

Sainsmart has 20% off orders over 100$, and free express shipping upgrade over 100$. I picked up two full ramps kits, a bunch of mechanical endstops, and a heated bed for ~105.



I think makerstoolworks has 10% of kits / extruders and 20% off everything else - so 5 high-torque motors are <100$. Unfortunately shipping isn't free so the discount will probably end up going mostly towards that as far as savings. 10% off an E3D V6 might be kind of nice.



Filastruder has a 10% off code 'Nice2MeetU' you can use for an E3D as well. 



15% off SeeMeCNC until tomorrow, 10% after that. Random bits/bobs, more valuable for the filament IMHO.



RepRapDiscount.com has 'BlackFriday2014' coupon code 20% off deals



Pololu has some 10% off stuff as well depending on how much you buy.


---
*Imported from [Google+](https://plus.google.com/108197211464469447407/posts/W1of8DQ7sNU) &mdash; content and formatting may not be reliable*
