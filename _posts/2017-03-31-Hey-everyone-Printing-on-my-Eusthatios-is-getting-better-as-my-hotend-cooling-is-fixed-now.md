---
layout: post
title: "Hey everyone. Printing on my Eusthatios is getting better as my hotend cooling is fixed now"
date: March 31, 2017 14:34
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
Hey everyone. Printing on my Eusthatios is getting better as my hotend cooling is fixed now. I'm actually quite happy in terms of reliability and general printing but on closer look and on brighter materials like this white PLA I do see some marks. They seem to be running around the whole object so they shouldn't be due to some play in my carriage. 



Any ideas how i may fight them? 



I'm using the ball screw spindles on Z like **+Walter Hsiao** does.

![images/5a5d7ca04b603138c3907061b1a2743f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5a5d7ca04b603138c3907061b1a2743f.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**larry huang** *March 31, 2017 14:39*

Is it related to under extrusion?  Are you using a bowden setup?


---
**Frank “Helmi” Helmschrott** *March 31, 2017 14:53*

Why do you think under extrusion? Guess not. 



And yes, bowden (it's an Eusthatios) with a Bondtech QR as feeder which is basically highly reliable. 


---
**Eric Lien** *March 31, 2017 14:54*

One thing I will say is that white filament is probably the one I like least. The Zinc oxide pigment that is used has flow characteristics that I do not particularly like. I can print one part in any variety of colors, then run that exact gcode on white filament and see inferior results. If you ever get white filament nailed down let me know. I would very much like to steal your settings. I can get close, but never perfect results with white.


---
**Frank “Helmi” Helmschrott** *March 31, 2017 14:58*

Oh interesting. I was always thinking that the white one is just the least forgiving one when it comes to hide irregularities. I have to try that out on the Prusa i3 mk2 that is close to perfect on other filaments and how it looks there.



Didn't come to my mind that this may be related to ingredients in the filament. Will try some yellowish filament I still have around next and see how things looks with that. Good hint.



I wanted to add to the data that I'm retracting with 60mm/s and currently as far as 5mm which works well without stringing and other marks on the place of the retracts so I guess it shouldn't be the cause for anything. 




---
**Ryan Carlyle** *March 31, 2017 15:59*

White and black filament require higher pigment loading than other colors. As a consequence, they have legitimately different extrusion behavior. White is unforgiving, black is forgiving. High pigment load = more masterbatch pellets = more polystyrene contamination in the PLA = messed up polymer properties. (Unless your vendor gets special PLA masterbatch pellets, which to my understanding is rare. I think E3D does? Don't quote me on that.)



I've also seen some weird variation within white filament that points to manufacturing problems, even from good vendors. For example, looking at white printed parts under blacklight from some spools, I've seen stripes of glowy sections, indicating some of the pigment is UV-fluorescent (ie modified "high brightness" white pigment) while the majority isn't. That suggests inconsistent pellet sourcing to me. 


---
**Eric Lien** *March 31, 2017 16:11*

**+Ryan Carlyle** Yeah I always have better luck with Natural Off White filaments than vibrant Bright White (high colorant loaded) filaments. When I first started printing I was fighting extrusion issues on a spool of Bright White filament for a month. Just couldn't get it dialed in (a real confidence buster). I then switched to a spool of red with identical settings and had a picture perfect print. That's when I decided, to only use white if explicitly necessary.  



And yeah black can be hit or miss too. I have actually had the best luck with Esun Black filament actually. Which is nice because it is inexpensive and has a really rich black (not that off black, brown tint like you can sometimes get). 


---
**Frank “Helmi” Helmschrott** *March 31, 2017 17:41*

Thanks for the clearification **+Ryan Carlyle** that's very interesting. 



Regarding Black filament I nearly only had good stuff so far apart from some "smart ABS" from Orbitech (German manufacturer) but everything else was quite good. I had especially good experiences with Extrudr mf-PETG (from Austria) and YPi3D (house brand of a german online shop) - i don't know how good they are available over in the US.


---
**Whosa whatsis** *March 31, 2017 21:43*

I think that white filament also usually doesn't get enough rounds of mixing, so the pigment (and the masterbatch plastic that it comes in with) is not adequately mixed, which means that the melting properties are not consistent. Add to this the fact that the opaque white surface creates harder shadows than any other color and you have a recipe for crappy-looking prints.



A white that is translucent will be better on both counts, and translucent filaments probably get more rounds of mixing at the factory because inconsistent color mixing will be more obvious.


---
**larry huang** *April 01, 2017 15:09*

**+Frank Helmschrott**  Now I think "under extrusion " was misleading,  I thought it's not related to the 3 axis, maybe the word should be irregular extrusion.  Direct setup will show less  Z axis artifact. I tends to suspect the pressure on the filament inside the bowden tube could be the cause.



I use bowden setup Eustathios (extruder hot zone not optimized) to test print this ABS filament in the first object without retract,  at constant speed,  then I use the same filament to print this egg shape container, it looks terrible. And on the replacement Z support, the artifact is  less obvious.

![images/05e577b747de41b933e84bcfb957d3da.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/05e577b747de41b933e84bcfb957d3da.jpeg)


---
**larry huang** *April 01, 2017 15:25*

In my case, I think greater push force may not solve the under extrusion problem, when filament is unable to pass through the nozzle fast enough ( after retraction usually) ,  a filament plug appear in the extruder hot zone,  and energy stored in the filament will later ruin the perfect print by extrude at a faster rate when possible.


---
**Frank “Helmi” Helmschrott** *April 01, 2017 16:49*

Thanks for explaining this in Detail, Larry. You may be right, this sounds like a reasonable explanation. 



In the meantime I had printed some stuff in yellow PLA (Colorfabb) which definitely looks better but still shows some of the same issues. The fact that it isn't as visible as on the white filament may be a mix between the white issues described above and the general issue like **+larry huang** described it.



I will try to switch to some different Filament than PLA next to see if there's differences depending on the material.


---
**Frank “Helmi” Helmschrott** *April 02, 2017 18:15*

So I did some more printing over the weekend. A few facts came out with that:



- The white definitely is more problematic or at least optically less forgiving than other colors. I printed a yellow Evil Ducky that looks quite nice though does show some of the irregularities still.



- Printed the same Startrooper head than above in a whitish ApolloX from FormFutura (which is basically ASA filament). It's more of a beige-white than bright white. This looks better but still shows the same issues.



- I printed the Head with bright white PLA on the Prusa i3 mk2 which also doesn't look as good as other filaments on the Prusa do but still looks quite a bit better than the print that came from the Eusthatios.



These are the things I still have to investigate as possible issues:



- X/Y carriage bushing by Igus do have some bigger tolerance as they should have. I need to find some good testing parts to find out if they may be an issue.



- Extrusion maybe not as good as it could be due to the bowden setup. Basically I'm very happy with the Bondtech extruder and the E3D v6 hotend but possibly there are just some specialities to the bowden setup that I can't really come around. My bowden lenght is already in an area where it can't really be so much shorter without causing problems. Maybe trying 3mm filaments could be a way to improve here. At least most of the commercial bowden printers (Ultimaker 1,2,3, BCN3D Sigma) do use 3mm filaments for their bowdens. I don't like 3mm filament at all but for a bowden it might be the better solution. Another option would be to try direct extrusion. The new E3D Titan Aero would be a light weight alternative to bowden though still much more mass of course.



- Maybe a Z-Axis problem. Can't imagine as my z-axis is quite rigid with the ball screws. But probably the last thing to investigate if everything else doesn't work.



I have to add that this all of course is now fine tuning on a very high level. The actual results are quite good but as there's still room to improve... why not try it :)


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/etqbSXQENZh) &mdash; content and formatting may not be reliable*
