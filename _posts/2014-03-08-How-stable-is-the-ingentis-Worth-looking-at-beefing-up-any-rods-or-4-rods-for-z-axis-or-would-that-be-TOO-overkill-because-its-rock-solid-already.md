---
layout: post
title: "How stable is the ingentis? Worth looking at beefing up any rods or 4 rods for z axis or would that be TOO overkill because its rock-solid already?"
date: March 08, 2014 17:45
category: "Discussion"
author: Jarred Baines
---
How stable is the ingentis? Worth looking at beefing up any rods or 4 rods for z axis or would that be TOO overkill because its rock-solid already?



I fear instability, coming from a Mendel ;-)





**Jarred Baines**

---
---
**ThantiK** *March 08, 2014 18:04*

I'd say it's overkill.  Think of the i3 design.  1 smooth rod on either side to hold the entire X gantry and that thing has a moving motor on it.


---
**Tim Rastall** *March 08, 2014 18:13*

It wouldn't hurt to use 12mm Z shafts and bearings. But not really necessary. Might  be worth considering aluminium, hollow 10mm xy shafts to reduce moving weight.


---
**ThantiK** *March 08, 2014 18:23*

**+Tim Rastall** I'd be interested in seeing that work.  I'd be too worried that the aluminum rods would bend, or wear away over time with the brass bushings over them.


---
**Tim Rastall** *March 08, 2014 18:57*

**+Anthony Morris** sorry,  I meant with igus bushings or similar. 


---
**Eric Lien** *March 08, 2014 19:24*

The ones I have on my current printer (1/2 " PCB liner ceramic coated [http://www.essentracomponents.com/sku/PBC-1052](http://www.essentracomponents.com/sku/PBC-1052) ) work like a dream. But you can't use roller bearings on them. They won't flex. I will be using these on my ingentis variant potentially. But I will have to turn the ends down for the pulleys I just got in the group buy.﻿


---
*Imported from [Google+](https://plus.google.com/+JarredBaines/posts/Y2YNgenUhdi) &mdash; content and formatting may not be reliable*
