---
layout: post
title: "Heres a noisy video of a speed test on the Procerus I did tonight"
date: January 12, 2015 08:29
category: "Show and Tell"
author: Tim Rastall
---
Heres a noisy video of a speed test on the Procerus I did tonight.

I had the acceleration up to 16000mm/s but I don't expect it will result in very nice prints.


**Video content missing for image https://lh3.googleusercontent.com/-kmasMVZ0I-U/VLOBUichjNI/AAAAAAAAVVg/JATAv4-s3y8/s0/PROCERUS%252BMOVEMENT%252BTEST.mp4.gif**
![images/10e35f0ce198274ff65e56665df42769.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/10e35f0ce198274ff65e56665df42769.gif)



**Tim Rastall**

---
---
**Tim Rastall** *January 12, 2015 10:06*

**+Oliver Schönrock** Not really done a direct comparison but not by any great amount. The Z axis design I'm using won't be affected by the layshaft gantry and the space above the carriage is occupied by the top of the hot end - I'm actually considering using a geared nema 11 and direct drive rather than a bowden system.

Very high acceleration can result in print artefacts because the head is vibrating from the sudden directional changes - until I push some plastic out on this one, I won't know the limits...


---
**Tim Rastall** *January 13, 2015 00:06*

**+Oliver Schönrock** Yeah. I think it's becuse the external x/y shafts are touching as they cross. Going to try to resolve it this evening. The video makes it sound worse than it does in reality.


---
**Tim Rastall** *January 13, 2015 08:42*

Found the source of the noise. The inner collars of the layshaft bearings are rubbing against the aluminium bracket they're supported by - need to take them off and insert a shim to give them enough space. 


---
**Tim Rastall** *January 13, 2015 09:05*

**+Oliver Schönrock** Remember the Eustathios looses vertical space with all the electronics and steppers at the base of the bot.

One of the reasons I went with 2 steppers was that it was symmetrical :). Should be straight forward to put an idler in instead of the second stepper.

In my experience you rarely see a design choice that doesn't have a knock on impact. My modus operadi for the Procerus was to build something that had the same symetrical gantry style as the Ultimaker but without the rotating support shafts and with the motors directly driving the x y movement. one thing I haven't pointed out is these choices also make the bot lower cost than Eustathios ( I think); even with 2 extra nemas (which are pretty cheap) there are only 4 x 8mm bore GT2 pulleys and 4 x 5mm bore GT2 pulleys, 4 x 608ZZ.  Eustathios has 10 x 10mm bore and 2 x 5mm. Plus, no closed loop belts, no pricey supper straight shafts etc etc. The Z axis will also only use belts, so no lead screws either.

Anyway, point being that extrusions are cheap so the price for an extra 160mm (4 x40mm) of 2020 is bugger :).


---
**Tim Rastall** *January 13, 2015 17:49*

**+Oliver Schönrock** for the Z axis.  I'm going for 4 corner support and a planetary gearbox to prevent the bed from dropping.  My current Ingentis has always use belts on Z,  and I've never had any discernable Z artifacts (apart from of some that were due to thermal expansion of the bed).


---
**Tim Rastall** *January 13, 2015 19:43*

**+Oliver Schönrock** not quite.  I'll draw a picture. 


---
**Tim Rastall** *January 14, 2015 00:19*

**+Oliver Schönrock** [http://i.imgur.com/Jbqk32v.jpg](http://i.imgur.com/Jbqk32v.jpg)

Does that make sense? That would just be one side of the bed with it mirrored on the other side. on each side one of the bearings will be replaced with a GT2 pulley and the 2 pulleys would be joined by a drive shaft. The shaft would then be driven by a geared nema via a closed loop belt..


---
**Tim Rastall** *January 14, 2015 09:08*

**+Oliver Schönrock**

WRT to why 4 corner support. I'm going to try using the 4 vertical extrusions as linear shafts with spring loaded flanged bearings on the corners of the bed (probably deserves another picture). The only way I can see that approach will work is if each corner is kept level by the belts so. In theory, with the belts supporting the bed in the vertical axis, the bearings only have to keep the bed aligned in the XY plane. I'm 50/50 on whether it will work or not, so the fall back is to use linear shafts on each corner.

And no I haven't fixed the bearings, went to the pub instead - All printers and no play make Homer something something.


---
**Tim Rastall** *January 14, 2015 09:26*

All good questions. 

I actually want the bed to be as heavy as practical. What I've learned from using z belts on the Ingentis is that belts give realyy good layer consistency - lead screws are just not machined accurately enough and for low layer values, there is enough play in the mountings, lead nuts etc to get enough margin of error to effect print quality. However if the bed is heavy and is prevented from dropping by a belt, even a tiny movement in the belt directly translates to an accurate Z position change - does that make sense?

The big down side of the Z belt is that as soon as nema motor power is cut - BANG! bed drops like a lead Zeppelin, that is why I will use a geared nema this time as they don't like been driven backwards. I'm also happy to take all the ratio I can get to maximise the Z movement accuracy - the fastest it ever needs to move is during a z move to prevent the head from catching during a travel move. With smoothie I can drive the motor plenty fast enough to achieve that, even with a 15:1 ratio.


---
**Tim Rastall** *January 14, 2015 20:35*

**+Oliver Schönrock** Layshafts are 8mm  aluminium tube from local hardware store.

Correct that I'm referring to the 'budget lead screws' rather than high grade tradezoidal ones. Although part of the issue can lie with the linear mechanics and stability of the bed - If you only use a centrally located pair of shafts/extrusions (a la Ingentis, Eustathios and Herculien), for Z linear rails, any force imposed by the hot end as it deposits material can result in minor fluctuations in bed position (in theory), with this effect being more apparent, the further from the center line you go. 4 corner support should be inherently more stable.

A have to say that  (at least in part I'm trying this design as it's never been done before (as far as I can tell).


---
**Tim Rastall** *January 15, 2015 09:22*

2mm


---
**Sean McDonald** *January 24, 2015 20:23*

**+Tim Rastall** Hi. I just built a Rostock Max Delta. Great quality prints coming off it, but looking at larger bed size, so I have been reading this thread with interest. As regards the 4 corner support, the QU-BD Revolution XL 3D printer (https://www.youtube.com/watch?v=D2XjIyyq0x0) uses bearings on vertical rods in each corner of the bed. Its design looks interesting to me. Any thoughts?


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/VW6AKrHShfa) &mdash; content and formatting may not be reliable*
