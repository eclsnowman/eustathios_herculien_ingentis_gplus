---
layout: post
title: "Beautiful work as always by Walter Hsiao"
date: June 22, 2015 23:06
category: "Show and Tell"
author: Eric Lien
---
Beautiful work as always by **+Walter Hsiao**​





**Eric Lien**

---
---
**Brent ONeill** *June 22, 2015 23:35*

nice sleek design! kudos **+Walter Hsiao** 


---
**Gus Montoya** *June 23, 2015 00:11*

Jesus, how did you get such clean lines? WOW!


---
**Isaac Arciaga** *June 23, 2015 01:46*

Nice looking prnt **+Walter Hsiao**  I was showing some people how perfect your prints are. May I ask what your typical print speeds are on your RigidBot to achieve that quality?


---
**Walter Hsiao** *June 23, 2015 04:58*

Thanks!

**+Isaac Arciaga** For normal plastics, it's almost always 40-50mm/sec, 0.15mm layers, with outer perimeters at 50%. Acceleration is usually at 2000.


---
**Frank “Helmi” Helmschrott** *June 27, 2015 10:40*

**+Walter Hsiao** is just a pro when it comes to printable parts design. and printing. and photographing prints :-) <b>thumbsup</b> 


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/YExfk4DqYBF) &mdash; content and formatting may not be reliable*
