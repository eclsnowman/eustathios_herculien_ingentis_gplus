---
layout: post
title: "It looks like the Eustathios Bearing B part at is a bit broken"
date: February 23, 2015 17:33
category: "Discussion"
author: Eric Bessette
---
It looks like the Eustathios Bearing B part at [https://github.com/jasonsmit4/Eustathios/blob/master/STL/Eustathios%20Bearing%20Holder%20B%20-%204%20-%20v1.0.stl](https://github.com/jasonsmit4/Eustathios/blob/master/STL/Eustathios%20Bearing%20Holder%20B%20-%204%20-%20v1.0.stl) is a bit broken.  Does any have an updated STL file?





**Eric Bessette**

---
---
**Oliver Seiler** *February 23, 2015 17:53*

Check Eric's updated Spider version or put it through Netfabb

[https://github.com/eclsnowman/Lien3D_Eustathios_Spider/tree/master/Other%203D%20Formats/STL](https://github.com/eclsnowman/Lien3D_Eustathios_Spider/tree/master/Other%203D%20Formats/STL)

[https://netfabb.azurewebsites.net/](https://netfabb.azurewebsites.net/)


---
**Eric Bessette** *February 23, 2015 19:56*

NetFabb worked, thanks!


---
*Imported from [Google+](https://plus.google.com/106288190144199995561/posts/ARVLSymA8fX) &mdash; content and formatting may not be reliable*
