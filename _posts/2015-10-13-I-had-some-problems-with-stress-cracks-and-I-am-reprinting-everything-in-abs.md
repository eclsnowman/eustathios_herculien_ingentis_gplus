---
layout: post
title: "I had some problems with stress cracks and I am reprinting everything in abs"
date: October 13, 2015 02:07
category: "Discussion"
author: Rick Sollie
---
I had some problems with stress cracks and I am reprinting everything in abs. Now that I have access to a printer that will print ABS again.  I ran into the same problem Ashley did, removing the brass bushings. I dunked the whole thing in boiling water for 20 seconds and it was surprizingly easy to twist with plyers and get them out. I tested and they slide okay, but did I ruin the bushings? Did it exude the oils? I can't tell just by looking at them.

![images/7703cebc63d41bb049dce0b464976656.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7703cebc63d41bb049dce0b464976656.jpeg)



**Rick Sollie**

---
---
**Igor Kolesnik** *October 13, 2015 02:14*

You should be able to restore the lubricant in it. Just dump them in oil. I would try thick grease and put the container in the hot water. Just dry them first


---
**Eric Lien** *October 13, 2015 02:27*

Be careful with heat. The inner section is epoxy potted into the outer housing. The epoxy is what forms the spherical joint. I learned the hard way once not to heat them with a torch to press them in. The epoxy all cracked and fell out.


---
**Igor Kolesnik** *October 13, 2015 02:37*

Warm water should not damage the epoxy.


---
**Eric Lien** *October 13, 2015 03:18*

But might deposit plastic residue on the inside bronze surface when boiled. So I think a light cleaning of the inside, and a re-oil job will do the trick.


---
**Rick Sollie** *October 13, 2015 16:14*

Thanks guys! I thing I will try the heat gun on the next set.


---
**Eric Moy** *October 13, 2015 17:11*

Be careful with a heat gun. Boiling water will only go to 100c whereas a heat gun gets much hotter. There are 2 stages to damage. If you reach the glass transition temperature, the epoxy will actually yield/bend permanently. If you heat it with a torch, you will probably break down the epoxy. It's a thermoset, so you can't really reflow it, it breaks down at high heat. Glass transition is just a weird relaxation/mobility of the bonds. Hope that helps


---
**Rick Sollie** *October 14, 2015 23:13*

**+Eric Moy** thanks for the information. I was jut on the way to the garage to look for it.


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/bcWhqsgTzqG) &mdash; content and formatting may not be reliable*
