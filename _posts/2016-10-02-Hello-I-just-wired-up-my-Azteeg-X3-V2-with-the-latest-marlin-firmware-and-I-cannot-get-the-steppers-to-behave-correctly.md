---
layout: post
title: "Hello! I just wired up my Azteeg X3 V2 with the latest marlin firmware, and I cannot get the steppers to behave correctly"
date: October 02, 2016 20:05
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hello! I just wired up my Azteeg X3 V2 with the latest marlin firmware, and I cannot get the steppers to behave correctly. They are set to 1.5A (2X vref - .75 V) They only move a step backward and move forwards loudly and with little torque. They have great holding torque, though. See attached vids


{% include youtubePlayer.html id=v9GjXS_3KYQ %}
[https://youtu.be/v9GjXS_3KYQ](https://youtu.be/v9GjXS_3KYQ)





**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *October 02, 2016 20:19*

I think only one of the coils is working. Check the wiring to make sure both coils show continuity back at the board with a multimeter.


---
**Eric Lien** *October 02, 2016 20:20*

Also the max current is 1.5A, but you don't need to run at max. Try 1.2A to start. Go up if you loose steps... And down if you have excessive heat at the stepper or driver.


---
**Stefano Pagani (Stef_FPV)** *October 02, 2016 20:21*

Ok, Thanks! I'll go check now and turn down the current. I turned it up because I thought they had trouble moving the carriage.


---
**Stefano Pagani (Stef_FPV)** *October 02, 2016 20:27*

**+Eric Lien** I checked the connections, all good. I forgot to set the steps per mm


---
**Stefano Pagani (Stef_FPV)** *October 03, 2016 12:42*

Hey, anyone know if there is anything I need to change in the firmware to make this work? They move forward a bit loud (with little torque) and sometimes don't at all. They never move backwards though, they just move a step back no matter how many mm's I say.


---
**Eric Lien** *October 03, 2016 23:04*

**+Stefano Pagani** any chance it is looking for limit switch input? Maybe you could home, or make it think it homed... Then jog both directions?


---
**Stefano Pagani (Stef_FPV)** *October 04, 2016 01:19*

Oh, it could be that, ill go buy some switches :)




---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/K8BTGj5ZyeH) &mdash; content and formatting may not be reliable*
