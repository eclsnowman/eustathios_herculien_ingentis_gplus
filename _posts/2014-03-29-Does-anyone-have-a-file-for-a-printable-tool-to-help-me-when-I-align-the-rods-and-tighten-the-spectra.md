---
layout: post
title: "Does anyone have a file for a printable tool to help me when I align the rods and tighten the spectra"
date: March 29, 2014 17:16
category: "Discussion"
author: David Heddle
---
Does anyone have a file for a printable tool to help me when I align the rods and tighten the spectra. I am sure I remember reading about it but cant find it anywhere now I am looking for it.



I am not entirely sure how it works but if it makes my life easier I am all for it!





**David Heddle**

---
---
**Tim Rastall** *March 30, 2014 17:23*

I have some clamps that keep the shafts parallel,  I post the design once I get a spare moment. 


---
**David Heddle** *March 30, 2014 19:33*

Not at home for a couple of days, so no rush.


---
*Imported from [Google+](https://plus.google.com/+DavidHeddle/posts/6xkUAosr3wp) &mdash; content and formatting may not be reliable*
