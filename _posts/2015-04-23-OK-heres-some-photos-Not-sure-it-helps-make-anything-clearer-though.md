---
layout: post
title: "OK here's some photos. Not sure it helps make anything clearer though"
date: April 23, 2015 03:08
category: "Discussion"
author: Ben Delarre
---
OK here's some photos.  Not sure it helps make anything clearer though. You can see in the photos that the left side smooth rod fits into the clamps perfectly,  while the right side is a good 2mm too far over. 



Only way I can get the fight side in is to move the top clamp back so the whole axis is twisted which is obviously wrong. 



It feels like the bed is too wide but I have checked it against the solidworks model and it is spot on and cut by misumi anyway. 



I was also wondering if perhaps the lm10uu bearing in either or both sides is not square resulting in the whole axis being tilted to the right. 



I don't know.  Its close but it's just not coming together right. 



Any suggestions? 



![images/b2d7c2fd75129abd3e30a58778e7b963.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b2d7c2fd75129abd3e30a58778e7b963.jpeg)
![images/9e60c5cfeafa3d528f8b610f7bb151a3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9e60c5cfeafa3d528f8b610f7bb151a3.jpeg)
![images/de1f24502754a635fc6e8bdda5355255.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/de1f24502754a635fc6e8bdda5355255.jpeg)
![images/9dd569b4a8294cb9a5f3834d98816473.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9dd569b4a8294cb9a5f3834d98816473.jpeg)
![images/34d7e3a4d9691d1604c4430e23aa9d8c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/34d7e3a4d9691d1604c4430e23aa9d8c.jpeg)
![images/382c059864397483f4b87a93b16eb36d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/382c059864397483f4b87a93b16eb36d.jpeg)
![images/624ef0590a6104e78a75d41fb4a28680.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/624ef0590a6104e78a75d41fb4a28680.jpeg)
![images/4a15f7809a786a824a36edbe1ae8c400.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4a15f7809a786a824a36edbe1ae8c400.jpeg)
![images/6631a7830a91a2cbe4a51c9700073178.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6631a7830a91a2cbe4a51c9700073178.jpeg)
![images/a64deab8eb5c1c9620a10677c51f624c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a64deab8eb5c1c9620a10677c51f624c.jpeg)
![images/88f713371e3b34e0899ce90c60355c27.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/88f713371e3b34e0899ce90c60355c27.jpeg)

**Ben Delarre**

---
---
**Isaac Arciaga** *April 23, 2015 03:23*

**+Ben Delarre** this should help aligning the Z rods. It's a simple Z alignment tool to ensure the spacing between the 10mm rod and the lead screw are correct throughout the axis. The two rods are spaced 40mm at their center. Photo here: [http://i.imgur.com/34r12jo.jpg](http://i.imgur.com/34r12jo.jpg) and download here: [https://drive.google.com/file/d/0B3_UjSUEy0i2LTFpOG9qMUlnQXM/view?usp=sharing](https://drive.google.com/file/d/0B3_UjSUEy0i2LTFpOG9qMUlnQXM/view?usp=sharing)


---
**Erik Scott** *April 23, 2015 03:26*

Leave both the top and bottom mounts loose until you get everything in place. Also, make sure you're frame is as square as possible, and the center aluminum extrusion in the Z-bed assembly is the correct length (321mm if I remember correctly. You should also make marks at the halfway points along the aluminum extrusions where you attach the smooth rod supports. These extrusions are 425mm long, so make a mark at the 212.5mm point. 



I'm not too familiar with the v2 z-axis. 



I notice you chose a different print orientation for the upper smooth-rod mounts. I printed mine with the mounting surface on the print bed. I wonder if the printer you used printed a little large and the mounts are a bit too far inboard. It shouldn't make THAT much of a difference. 



Do check your frame to make sure your horizontal extrusions are all 425mm in length. 


---
**Isaac Arciaga** *April 23, 2015 03:28*

**+Ben Delarre** I just remembered my leadscrews are 10mm, not the 12mm the BoM calls for. I can quickly make a 12mm version if you need one.

 Also what **+Erik Scott** mentioned above. It's vital that the entire frame is square.


---
**Ben Delarre** *April 23, 2015 03:43*

This is a v2 eustathios. 



I've double checked the extrusion and they are all the correct length. The frame seems square and the xy axis moves perfectly so I think that that backs that up. 



I'll try your method **+Erik Scott**​ sounds like a good plan.



I don't currently have access to another 3d printer so I will try without the z alignment guides and see if I can get it working before going to find a printer. 


---
**Ben Delarre** *April 23, 2015 19:49*

Ok I'm having a hell of a time with this. Spent a few hours on it this morning. Got a little further.



The smooth rods now go in without issue, a little sanding on the upper clamps and getting the z-axis bed frame together more accurately helped immensely.



I have since managed to get it a lot smoother, and even got some moves out of it, but can't get a full travel at all. It ends up binding near the top no matter what I do.



I've tried loosening the top clamps and then moving the z up and down to try and square the top clamps, but I can't seem to find a position that is smooth along the whole axis.



The rods are all misumi cromed ones and seem perfectly straight so I'm pretty sure the hardware is not at fault.



Any suggestions? 


---
**Ben Delarre** *April 23, 2015 20:03*

I just noticed....my lead screws are not square with my smooth rods. God knows how...but the leadscrews tilt inwards from bottom to top. Quite significantly so too.


---
**Erik Scott** *April 23, 2015 20:09*

Is there any warpage in the lower combined z-screw/rod holder? That would tend to make them converge at the top. ﻿



I don't know too much about the v2 z screw holder, but you may want to check the holes where the bearings sit to make sure there isn't any extra plastic or whatever tilting the bearings and screw. 


---
**Ben Delarre** *April 23, 2015 20:12*

**+Erik Scott** possibly.....damn i think i'm going to have to get new z-axis plastic parts.


---
**Ben Delarre** *April 23, 2015 20:22*

Hmm, on more careful inspection it appears the leadscrews wobble when rotated in the z-axis bearing holders. I figured 'damn they must be bent', but they're perfectly flat on the table. So...I guess my bearings in the z-axis bearing blocks are off somehow?


---
**Isaac Arciaga** *April 24, 2015 07:47*

**+Ben Delarre** I just looked through your photos. The photo of the bottom of the lead screw and pulley stood out to me. What I noticed was the end thread of the lead screw is resting on top of the pulley. Can you raise the lead screw maybe .5mm away from the pulley so the threads don't touch? I'm assuming you are using Misumi Lead Screws with the required Step.



Also, I would remove the bed frame and disengage the belt during this alignment process so you can rotate each leadscrew freely by hand and easily determine what and where things need to be adjusted. Once you have each rod and lead screw aligned and moving freely, lower each Bed Support all the way down, then attach the bed frame and re-engage the belt.


---
**Ben Delarre** *April 24, 2015 13:46*

**+Isaac Arciaga**​ I will give thd raising of the leadscrew a go today. But I don't think I can attach the bed after putting the support onto the screw as the bronze nut blocks two of the mounting screws. 


---
**Isaac Arciaga** *April 24, 2015 17:52*

**+Ben Delarre** Are you using the same Misumi Lead Screw nut called for in the V2 BoM as well as the V2 printed Z Bed Support? There should be ample room to tighten that particular bolt down when looking at Eric's cad drawing. [http://i.imgur.com/qHYHj8y.png](http://i.imgur.com/qHYHj8y.png)

Otherwise, you can tighten down the hard to reach bolt by unbolting both lead screw nuts from the Bed Support and then lift the bed up to tighten it then drop it back down and bolt the lead screw nuts back on.



Your prints look ok to me. I'm not convinced (yet) that it's causing your issue.


---
**Ben Delarre** *April 24, 2015 18:06*

**+Isaac Arciaga** hmm...looks like I printed an older version of the bed support, the holes have been placed differently and definitely block. Not to worry though as you say could always remove the nuts afterwards, or just live with 3 mounting screws which is probably enough tbh. Alternatively I may drill out an additional hole.



Did you have much wobble in your leadscrews when in their mounts? I've just recorded a video which is uploading now that I'll post in a bit, but with the screws in place, mounted in the bottom bearing holders and screwed through the bed support, I can move the leadscrew top back and forth almost an inch. There seems to be loads of play in the bearings. Double checked they are 608zz's from VBZ, but they definitely have some slop in them when mounting over the 8mm turned part of the screw.



With this much movement, its actually impossible to move the bed supports up and down without holding them square to the frame, otherwise when turning the leadscrew they shift one way or the other and cause binding with the bearing. It works when the bed is in place obviously since this holds it square, but standalone like this its not easy to move them up and down.


---
**Isaac Arciaga** *April 24, 2015 18:33*

**+Ben Delarre** Which Bed Support did you print? Was it the version Eric originally shared when he released the V4 Carriage?


---
**Ben Delarre** *April 24, 2015 18:33*

Don't think so.  I believe it was the one from the v2 solidworks files. 


---
**Ben Delarre** *April 24, 2015 21:45*

**+Isaac Arciaga** I got it working!



In a fit of rage I took the entire thing apart, frame and all and rebuilt from the ground up.



XY still to be done, but the Z is now in and I have full travel up and down without binding at 500mm/min. Good enough, and no bad noises.



Thanks all!


---
**Isaac Arciaga** *April 25, 2015 03:53*

**+Ben Delarre** Awesome! Glad it all worked out!


---
**Ben Delarre** *April 25, 2015 03:54*

Got the xy up now.  But it's caused a donking noise again in the z.  A little more tuning I guess. Still.... Almost there! 


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/aUQ48tZK3VM) &mdash; content and formatting may not be reliable*
