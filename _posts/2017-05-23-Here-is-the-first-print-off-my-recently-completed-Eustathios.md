---
layout: post
title: "Here is the first print off my recently completed Eustathios"
date: May 23, 2017 15:47
category: "Build Logs"
author: Brad Vaughan
---
Here is the first print off my recently completed Eustathios.  Pardon the length (too long) and production quality (not my forte).  Thanks again to **+Eric Lien** and this community in general.




{% include youtubePlayer.html id=Ln3poqPSv6E %}
[https://youtu.be/Ln3poqPSv6E](https://youtu.be/Ln3poqPSv6E)





**Brad Vaughan**

---
---
**Eric Lien** *May 24, 2017 12:04*

Video looks great. And very nice job on the printer.



Congratulations, and welcome to the club!


---
*Imported from [Google+](https://plus.google.com/109037736098991448813/posts/N1kh6NkN2KA) &mdash; content and formatting may not be reliable*
