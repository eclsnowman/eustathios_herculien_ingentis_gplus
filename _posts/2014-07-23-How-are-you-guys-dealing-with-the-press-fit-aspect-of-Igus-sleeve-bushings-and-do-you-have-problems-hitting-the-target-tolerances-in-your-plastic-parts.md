---
layout: post
title: "How are you guys dealing with the press-fit aspect of Igus sleeve bushings and do you have problems hitting the target tolerances in your plastic parts?"
date: July 23, 2014 20:42
category: "Discussion"
author: John Lewin
---
How are you guys dealing with the press-fit aspect of Igus sleeve bushings and do you have problems hitting the target tolerances in your plastic parts?



As **+Mike Miller**  mentioned in a post earlier today,  Igus bushings ship oversized. The Igus documentation consistently states that they require a press-fit install to achieve their target tolerance ([http://www.igus.com/wpck/3691/iglidur_toleranzen](http://www.igus.com/wpck/3691/iglidur_toleranzen) or the "Installation Tolerances" section of the technical data sheets)



I knew this might be a challenge with plastic parts before I ordered a set of M250 sleeve bushings but after weeks of trying to make them work, I've given up and ordered bronze bushings. In my experience, I'd either have too much slop and they would allow rocking and binding or I'd over compress them and end up with too much friction. I assume the problem is related to the use of plastic rather than metal housings and is caused by a lack of rigidity in my plastic parts, exceeding the tolerance when drilling the bushing pockets and/or an inability to create the ideal pocket chamfer. In the end, they just seemed to introduce a new problem, where I'm spending tons of time trying to get an ideal fit, and ultimately never quite getting there.



Since I was worried about the press-fit aspect going into it, I also ordered some of their clip bearings and had slightly better results because I could easily remove the bearing and revise the pocket dimensions but I won't be using them in my next build either.



I was quite excited about using their products as they are cost effective, well regarded, readily available and technically more advanced. I'll probably try them again in the future and I'm looking forward to hearing what you guys are doing to achieve a "to spec" fit and to learn if my experience was just an issue with my tools, technique or setup.





**John Lewin**

---
---
**Mike Miller** *July 23, 2014 20:57*

Keep in mind that the loads on a printer are TINY. If a bushing is true to the axis of movement and properly constrained, you'll still get smooth movement and near infinite service life. 


---
**Eric Lien** *July 23, 2014 22:29*

If the carriage is abs this is what I did. Soften the carriage holes with acetone. Insert the rod into the hole. Slide the bushings onto the rod. Push the bushings unto the hole (should now be easy with the softened abs so there is no fear of cracking the carriage). Put the rod into a hand drill. Spin the rod which aligns the bushings. Then allow everything to setup.



You now have bushings essentially perfectly aligned and well seated into the carriage.﻿


---
**Eric Lien** *July 24, 2014 10:48*

Perhaps. Good point.


---
*Imported from [Google+](https://plus.google.com/+JohnLewin/posts/dgvj6DvN2db) &mdash; content and formatting may not be reliable*
