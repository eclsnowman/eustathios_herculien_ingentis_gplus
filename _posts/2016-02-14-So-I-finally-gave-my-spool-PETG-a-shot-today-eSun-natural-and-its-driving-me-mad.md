---
layout: post
title: "So, I finally gave my spool PETG a shot today (eSun, natural), and it's driving me mad"
date: February 14, 2016 05:25
category: "Discussion"
author: Erik Scott
---
So, I finally gave my spool PETG a shot today (eSun, natural), and it's driving me mad. I'm having the exact same problem I was having with PLA a while back, and I can't seem to figure out how to fix it. (In fact, I'm not even sure how/if I fixed it when I was running PLA.)



Anyway, since I suck at describing what happens, I decided to record a video instead. Of course, I'm printing a Benchy Boat, which is everyone's go-to torture-test object. You can see that large globs appear in the first layer when it's printing the test, almost as if the extruder is pushing out too much plastic. I put my fingers on the filament as it enters the extruder to feel what's happening to the filiment, and it certainly feels like it suddenly pushes out a lot more than it retracted just before. at also sounds and feels like it's slipping, skipping steps, or grinding. You can hear it in the video.



If anyone would be so kind as to look though my settings and find any areas I could improve, that would be fantastic. I'm using a brand-new Bondtech QR extruder, which I have calibrated using the filiment measuring technique and confirmed with the thin-wall cube test. The settings are based on the ones found here, though admittedly, those are not for an Eustathios. [http://forum.makergear.com/viewtopic.php?f=11&t=2593](http://forum.makergear.com/viewtopic.php?f=11&t=2593)



I'm also printing on plain glass as I still need to get some hairspray, and gluestick just didn't seem to be working. The part would actually move around on the bed, which was really annoying. 



The other issue is temperature (second video). When I have the fan going, I can't get the hotend to maintain temperature. I have it set for 250 for the first layer, and 245 for the rest. However, the temp will drop to about 230 when layer 1 is done. It also barely maintains 250. I've run PID autotune, and I've increased the proportional and tripped the integral values. doesn't help much. However, because I get the correct temperature on the first layer, when the globbing happens, I don't think this is the issue, though I think it will be an issue down the road. 



So, any help is much appreciated! 



![images/3958582c74b328c67376eef8f3187275.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3958582c74b328c67376eef8f3187275.gif)
![images/c633c0e8b8b1efd727c3eb6f3f26ec26.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c633c0e8b8b1efd727c3eb6f3f26ec26.gif)
![images/706bfa8ccc8b0b7f512a558b45f4ce27.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/706bfa8ccc8b0b7f512a558b45f4ce27.jpeg)
![images/3dd011dda6996510bceebba368c57556.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3dd011dda6996510bceebba368c57556.jpeg)
![images/e5392c6fed23ab5f3637b119634337d3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e5392c6fed23ab5f3637b119634337d3.jpeg)
![images/bbb185eadf3c3a5d9ab63454be96b151.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bbb185eadf3c3a5d9ab63454be96b151.jpeg)
![images/5c281fb228ac3370dde18e73b0906131.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5c281fb228ac3370dde18e73b0906131.jpeg)
![images/c5fb2c9c1b807ce364d13ec618f51bcb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c5fb2c9c1b807ce364d13ec618f51bcb.jpeg)
![images/9e09af0e75e871b95bca74291df969ab.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9e09af0e75e871b95bca74291df969ab.jpeg)
![images/20e8bb317273e3aff4835065e9a0550a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/20e8bb317273e3aff4835065e9a0550a.jpeg)
![images/9cabbb9426b205cf26ee90acdf299f93.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9cabbb9426b205cf26ee90acdf299f93.jpeg)
![images/fe71ff5aea5988a5a52c845c8efe5c03.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fe71ff5aea5988a5a52c845c8efe5c03.jpeg)
![images/1607128ca9a42bbc8c29c27877225ad3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1607128ca9a42bbc8c29c27877225ad3.jpeg)
![images/a6253844707fbb7ddcc23f780790c613.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a6253844707fbb7ddcc23f780790c613.jpeg)
![images/4ce25808011314288579c058fde1fb6b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4ce25808011314288579c058fde1fb6b.jpeg)

**Erik Scott**

---
---
**Ben Marks** *February 14, 2016 05:51*

I have my current printer dialed in well for Inland PETG which I believe is the same as eSun.  My prints are coming out absolutely perfect at the moment.



- E3D v6, 0.40 nozzle, 0.20 layer height

- 250/90 hotend/bed for whole print

- .98 extrusion factor compared to ABS

- 50mm/s movement

- purple glue stick on glass for bed adhesion

- no fan but my ambient is pretty cool



I can send you my slic3r profile for these prints if you want.


---
**Ben Marks** *February 14, 2016 05:53*

After watching the video again, turn speed down to 40mm/s and keep temp at 250.  Do 50% movement for first layer. 


---
**Oliver Seiler** *February 14, 2016 07:24*

In terms of the extruder not holding the temperature I had the same problem with both of my E3DV6. The heat cartridges were just too weak and delivering less power than specified.

I tested them by hooking them up directly with my PSU (i.e. full power), turning the fan on and they wouldn't get above 220-230 degrees.

Check the resistance of the heat cartridge - mine was higher than it should have been. I complained with E3D and they sent me new ones straight away. In the end I went for a higher wattage heater cartridge from AliExpress - the PWM takes care of that. No issues ever since.﻿


---
**Oliver Seiler** *February 14, 2016 07:29*

Try to set coasting to 0.1-0.2 and extra restart distance to -0.1.

I'm not sure if that will take care of your problem, but should at least make it less of a problem...


---
**argas231** *February 14, 2016 12:35*

Okay guys...    Water is a real issue...   Many of these materials we try to print with Like PETG and ABS are Hygroscopic    they love to soak it up... However when they do..   They do no9t Mold very well...   and many other things...     You can dry it out in a Hot Air oven    keep in below 160 Degrees...   But if you keep in no Higher than Human Body temp   you can dry it   just takes a bit longer...   Do not exceed 160 degrees    because much higher and you can encounter serious burn issues working with it.     Invest in a Dehumidifier in your Printing room...   keep your Plastic Dry...   and you will be surprised how much better your Printing with Become



   And Yes    a little drying goers a long ways...


---
**Eric Lien** *February 14, 2016 15:17*

First layer looks too close to the bed. From what I can tell that isn't 0.25mm. Also take the time to tune a single wall cube to get the extrusion multiplier. PETG tends to have die swell, and benefits for being slightly under extruded more than over extruded (since it tends to ball up on the nozzle).



 Also my temps that works great is 70C bed, 260C hot end. If you are having trouble maintaining temp try PID tuning, or a 40W cartridge since the 25W supplied by E3D is insufficient IMHO.


---
**Erik Scott** *February 14, 2016 17:18*

I have the 40W heater cartage from E3D. I did PID autotune and it wasn't sufficient to maintain temps once the fan turned on. Is there an M command to just check to make sure the firmware has the right PID values? 



I print in the basement, but it is dehumidified. It's REALLY cold in New England right now, so the ambient temp in the basement is only about 15C. 



If I print any higher, it balls up on the nozzle, like you said. It seems to really like to do that, even more so than PLA. 



I'm going to try playing with the first layer a bit. increasing the height a bit pr decreasing layer height so it extrudes a bit less. 


---
**Eric Lien** *February 14, 2016 18:01*

I think something must be off, with a 40 watt heater I can easily reach 290 C and maintain it. Also with PETG you don't really need the cooling fan, trying to run it without part cooling and see if that helps you maintain temp.


---
**Eric Lien** *February 14, 2016 18:06*

Can you confirm resistance of your heater cartridge? And confirm the voltage?


---
**Erik Scott** *February 14, 2016 19:25*

So, here's the thing. I can't get my heater up to 260 with the fan on and the z axis at zero. There's too much cooling happening right there, and the cool air just builds up. Damn it, Eric, your cooling duct is just too good :D



I've noticed the nozzle gets noticeably closer to the bed when heated, so I'm going to have to level it with things heated up. 



I'm going to proceed without active cooling. Hopefully the cool ambient temperature will do what the cooling would do otherwise. 



Not sure what the proper way to test the heater cartage resistance and voltage is, so I just unplugged the powerpole connectors I'm using and stuck the multimeter ends in there. The resistance is 20 ohms, and the voltage is 24.2. The voltage is right, but the resistance isn't. I'm sure I ordered the right one. Well, this is annoying. Now I have to take the damn thing apart...


---
**Erik Scott** *February 14, 2016 19:30*

The heater cartage is red, so it should be a 40W heater. I have another old heater carriage that's only 5 ohms, but that might be too low for what I'm doing. 


---
**Eric Lien** *February 14, 2016 19:43*

You want 14.4 ohms for 40W at 24v, R = (V^2/P), so (24*24)/40 = 14.4ohms


---
**Erik Scott** *February 14, 2016 20:18*

Yup, i know. I just didn't know I had a bad heater cartridge. 



I'm still having issues with the extrusion of the first layer. I corrected the layer height so It's barely putting it down. I'm still getting the globbies. I feel the plastic going into the extruder and it's obvious it's extruding a lot more than it just retracted. It's grinding and skipping, and there's white plastic in the extruder teeth. I'm just about ready to throw this damn thing at the wall.


---
**Jim Squirrel** *February 14, 2016 21:05*

I've had my fair share of heater catridge failure and extrusion issues. take a break then go through it again. You'll get it. If not I'll gladly give your printer a new home :) Good Luck.




---
**Erik Scott** *February 15, 2016 02:36*

**+Jim Squirrel** Yeah. I don't know what's up with this. It's a red heater cartridge, but it's the resistance of one of the 25W ones. That's annoying. Going to see if I can find the old order number and complain. Certainly not giving up on the printer, though. 



I've played around a bit more with my settings. Plain glass isn't working so well for adhesion (I was really hoping it would, but I kinda expected it wouldn't) and blue tape was even worse, so I'm going to get some hairspray or give the gluestick another go. I think I figured out the globbing/over-extrusion issue.  I had "retract on wipe" enabled in the advanced tab. In the case of the Benchy boat, there are some really small features on that first layer, and I think it had to make a really quick wipe movement and it didn't retract as much as it thought, and then extruded a whole bunch. Once I disabled this setting, I didn't get globs, though other issues persisted. If anyone wants to give this a go themselves with my settings, please let me know how it turns out and if you get the same results. I'm thinking it might be a bug in S3D. 



Anyway, I'll have to give it another go next weekend. In the meantime, I'm going to order some hairspray and better parts. 


---
**Gus Montoya** *May 22, 2016 06:31*

 Hi Erik, nice to see the build plate working for you. It looks so nice )


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/CkWCy5qgQnD) &mdash; content and formatting may not be reliable*
