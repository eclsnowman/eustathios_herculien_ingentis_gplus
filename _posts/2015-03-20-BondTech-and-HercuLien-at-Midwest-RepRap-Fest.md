---
layout: post
title: "BondTech and HercuLien at Midwest RepRap Fest"
date: March 20, 2015 23:52
category: "Show and Tell"
author: Jeff DeMaagd
---
BondTech and HercuLien at Midwest RepRap Fest

![images/1390a01dbad8df6bc60c9b7a83930a1b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1390a01dbad8df6bc60c9b7a83930a1b.jpeg)



**Jeff DeMaagd**

---
---
**Isaac Arciaga** *March 21, 2015 00:22*

I spy an Eustathios as well.


---
**Jeff DeMaagd** *March 21, 2015 00:27*

Yes.﻿ I can't add a photo to this thread for that though, seems too late.


---
**Gus Montoya** *March 21, 2015 02:11*

YEAH!  Way to represent @Eric Lien. @Jeff  Please Please Please post tons of pictures.


---
**Jeff DeMaagd** *March 21, 2015 03:26*

I'll probably post other photos to the 3D Printing group as I haven't seen any other Eusts or Hercs there. Someone has a couple of Tantillus. I guess the E3D guys just got here, so maybe a photo of their booth tomorrow since this group's machines seem designed around their products.


---
*Imported from [Google+](https://plus.google.com/+JeffDeMaagd/posts/NfvGPmjEdAq) &mdash; content and formatting may not be reliable*
