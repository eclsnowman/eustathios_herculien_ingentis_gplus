---
layout: post
title: "So I discovered the awesome Eustathios design a couple months ago, bravo to the people who designed it"
date: March 04, 2016 22:34
category: "Mods and User Customizations"
author: Cullen Parish
---
So I discovered the awesome Eustathios design a couple months ago, bravo to the people who designed it.  It seems like the best design I have found online.  My original intention was to modify it slightly to use larger aluminum extrusion for more rigidity, and reduce part count a bit (mainly by eliminating the second drive belt from the X and Y axis).  However, I was having a hard time modifying the files (I use Catia for work) and ended up redrawing and modifying pretty much everything but the carriage.  I intend to use one Z-axis motor ,and one ball or lead screw and one 8mm rod and linear bearing on each side of the build plate. So the two lead/ ballscrews will be one a single belt.  The X and Y axis motors are in a configuration on saw here a couple weeks ago, but can't seem to find again.  It is missing a pair of idler pulleys below each X/Y motor.  Of course the belts are not included either because I have no idea how to model them.  I intend to use graphite impregnated bronze bushings on the X and Y slides, and the self aligning bushings per the Eustathios design in the carriage.  The build volume is 300x300x300.  

     I guess I am just throwing this out there to see if the people here with more experience than me could point  out any problems or pitfalls with what I have designed.  



![images/2c248bb376e55f24ebd46d0294dc5962.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2c248bb376e55f24ebd46d0294dc5962.gif)
![images/2ba3dee70bef25e47d2ebd2cdcf4335c.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2ba3dee70bef25e47d2ebd2cdcf4335c.gif)
![images/208d73031c6427da684cc0d47287f57d.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/208d73031c6427da684cc0d47287f57d.gif)
![images/a68c54adcbad70a62b9daec20536f8dc.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a68c54adcbad70a62b9daec20536f8dc.gif)
![images/9d5547353b9144250eb3f42d93a891e8.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9d5547353b9144250eb3f42d93a891e8.gif)

**Cullen Parish**

---
---
**Ted Huntington** *March 04, 2016 23:47*

One difference is that (like a low-cost Eust-like printer from China posted earlier) you did away with the  two cross bars that hold the Z shafts- which probably saves a little money and seems a little cleaner.


---
**Jeff DeMaagd** *March 05, 2016 06:19*

I'm pretty sure the "second" drive belts for X and Y are necessary with this mechanism type. The leverage of the carriage so far from the axis slide, I just don't think you're going to have a good time.


---
**Jeff DeMaagd** *March 05, 2016 06:22*

When I designed a machine, I just drew flat belts - without the teeth. At least makes a good stand-in.


---
**Cullen Parish** *March 05, 2016 18:19*

I think I wasn't clear, the two X or Y axis rods are connected at each end.  I eliminated a third belt (or coupler) to attach the motor.


---
**Jeff DeMaagd** *March 06, 2016 01:18*

OK, I thought Eustathios Spider v2 did away with that relay belt.


---
**Cullen Parish** *March 06, 2016 01:41*

I got the idea for the belt configuration from a post on this group a couple weeks ago, but can't seem to find it.  I think the spyder v2 does still have the short belt [https://github.com/eclsnowman/Eustathios-Spider-V2/blob/master/Documentation/Pictures/Screen%20Shot%2003-08-15%20at%2004.16%20PM.PNG](https://github.com/eclsnowman/Eustathios-Spider-V2/blob/master/Documentation/Pictures/Screen%20Shot%2003-08-15%20at%2004.16%20PM.PNG)


---
**Eric Lien** *March 06, 2016 06:23*

**+Cullen Parish** yeah that was the post by **+Shauki B**​ but all his posts seem to be gone now. Not sure why I can't see them anymore.




---
*Imported from [Google+](https://plus.google.com/113316559025663406512/posts/Zt3kjLoPvmL) &mdash; content and formatting may not be reliable*
