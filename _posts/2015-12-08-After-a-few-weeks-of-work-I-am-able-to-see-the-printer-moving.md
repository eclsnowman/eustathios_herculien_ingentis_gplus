---
layout: post
title: "After a few weeks of work, I am able to see the printer moving"
date: December 08, 2015 17:23
category: "Show and Tell"
author: Claudio Volpi
---
After a few weeks of work, I am able to see the printer moving.

Thank you Eric for your fantastic work


**Video content missing for image https://lh3.googleusercontent.com/-TPZHc5_sxGc/VmcSCl6ck7I/AAAAAAAATAg/b5y297zIwp4/s0/WP_20151208_14_36_21_Pro.mp4.gif**
![images/40e81f2c02d0f21b16c08b2ed5a0e43b.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/40e81f2c02d0f21b16c08b2ed5a0e43b.gif)



**Claudio Volpi**

---
---
**Eric Lien** *December 08, 2015 17:45*

Great job. And great to see another printer up and running.


---
*Imported from [Google+](https://plus.google.com/+ClaudioVolpi22/posts/MJs3WhA2fX8) &mdash; content and formatting may not be reliable*
