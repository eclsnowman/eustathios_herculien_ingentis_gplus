---
layout: post
title: "Shared on March 11, 2014 04:56...\n"
date: March 11, 2014 04:56
category: "Show and Tell"
author: D Rob
---
[http://www.servocity.com/html/vertical_shaft_worm_drive_gear.html#.Ux6XE_ldWVE](http://www.servocity.com/html/vertical_shaft_worm_drive_gear.html#.Ux6XE_ldWVE)





**D Rob**

---
---
**Liam Jackson** *March 11, 2014 08:55*

There also worm drive stepper gearboxes on aliexpress 


---
**Eric Moy** *March 11, 2014 09:43*

Ah nice, you'll still get striping, but the wobble will be way less﻿


---
**Mike Miller** *March 11, 2014 12:10*

Is it a servo, or a dumb motor? 


---
**Dale Dunn** *March 11, 2014 12:22*

Nice find. Replace the aluminum motor mount with a printed NEMA 17 mount. It looks like the worm gear mounts to a hub. Perhaps one could print a hub to adapt it to a 10mm shaft.



BTW, what is the ideal ratio we would want for the best compromise between layer height accuracy and Z axis speed?


---
**D Rob** *March 11, 2014 13:42*

**+Dale Dunn**  My thought is this z steps/mm should probably be set 1/2 of the steps/mm that give max capable resolution with current tech. So we can hopefully reach higher res, as tech improves, without having to change the drive only extruders and hotends and whatnot. What is the highest res print on a reprap ya'll have seen to date? Once we know that we can do the maths to figure stepps/mm


---
**Dale Dunn** *March 11, 2014 14:49*

I've heard of people experimenting with layer heights as low as 0.05, but I'm sure we want more resolution that that, just to be able to adjust first layer height. If I was trying for layers that low, I might want full steps somewhere around 0.005. That would be my guess, but I'm not sure what the more experienced users would say. Whether we can make an Ingentis platform flat enough for that is a separate issue.


---
**Mike Miller** *March 11, 2014 15:06*

Keep in mind as the layer height halves, the data pushed, and number of passes more than doubles. 


---
**Mike Miller** *March 11, 2014 15:11*

This was also on their site...bet you could print an equivalent, and the gearing would be enough to prevent a fall [http://www.servocity.com/html/gearmotor_gearbox.html#.Ux8nbtDnbqA](http://www.servocity.com/html/gearmotor_gearbox.html#.Ux8nbtDnbqA)


---
**Dale Dunn** *March 11, 2014 15:52*

I don't think a NEMA 17 stepper with the coils unpowered has enough torque to prevent a fall, even with a high reduction ratio. The worm gear is different because the asymmetric friction in the system makes it impossible to move the input shaft by turning the output shaft (for common worm helix angles).


---
**Mike Miller** *March 11, 2014 16:02*

yeah, but if it were connected to a leadscrew...


---
**Liam Jackson** *March 11, 2014 16:03*

**+Dale Dunn** **+D Rob** 5 microns per step was my thoughts, but I don't know how to work that out as a gear ratio, doesn't it depend on the circumference of the rod that the line is wrapped around or the belt pulley used? 


---
**Dale Dunn** *March 11, 2014 16:15*

**+Liam Jackson**  , yes it will depend on what the belt or line is wrapped around.



**+Mike Miller**  A leadscrew is indeed a different matter. But they provide a high steps/mm already, unless you're considering a multiple start ACME (trapezoidal) screw (which may not resist a fall). M6x1 provides 400 full steps per mm, 0.0025 per step. M8x1.25 gives 0.003125 mm per step. Both are a bit slow on a Mendel, but I don't know if the Ingentis platform is too heavy to lift with fewer steps/mm. So, I'm very curious what Tim is using now. I don't know what all pulleys he's using, so I can't calculate it myself.


---
**Mike Miller** *March 11, 2014 17:27*

Yikes...."I started Zeroing the bed last Tuesday, it's halfway there"


---
**D Rob** *March 12, 2014 00:02*

well the math would follow something like this I believe: to travel the full distance of  circumference of the pulley/shaft will take one full rotation on the axle. normally this is divided by the number of steps for one rotation (200 on 1.8 and 400 on .9 steppers) so spectra on a 10mm rod travels 31.42mm for each full rotation (c=2*pi*r) this equals (in a 1:1)  0.1571mm per step. now find the desired worm ratio lets use 10:1 so now we divide by 10 to get our new steps/mm 0.01571


---
**Dale Dunn** *March 12, 2014 01:34*

That sounds about right, especially since you remember the correct number of steps for a normal stepper. Oops.



Those of us who want to use belts have a much different situation. 32 GT2 teeth per revolution is 64mm/rev. (64 mm/rev)/10(200 steps/rev)=0.032 mm/step. Not so good.



If I still want 0.005 mm/step, I need a 64:1 ratio. That might be an awkwardly large worm wheel if printed. If I hob a plastic worm wheel with 64 teeth and use M6x1.0 threaded rod as the screw, I get a pitch diameter of ~Ø20. That sounds feasible, but it might wear quickly. ~2kg held by 0.5 mm deep ABS tooth doesn't give me warm fuzzy.


---
**D Rob** *March 12, 2014 05:15*

**+Dale Dunn** sorry for the brain dead moment. We forgot to divide by 32 for micro stepping. Or 16 or 8 depending on the jumper setup


---
**D Rob** *March 12, 2014 05:16*

.032/32=.001 way better


---
**D Rob** *March 12, 2014 05:16*

With 10:1 worm


---
**D Rob** *March 12, 2014 05:18*

BTW spectra and 8mm rod would give better resolution than 10 but the math isn't as easy to do in the head


---
**Dale Dunn** *March 12, 2014 12:27*

All the relationships are mathematically linear, so going from 10 to 8 is just a factor of 0.8 difference.



The problem with microstepping is that the stepper is not able to hold a microstep position as well as a full step position. This leads to a cyclical error in layer heights and banding in the print. So we need layer heights to be increments of a full step, even if the actual layers don't fall on a full step. Which is an illustration that the position error when using microstepping is significant and different from the whole step error. Depending on the load, the actual shaft position can sag as far as halfway to the next whole step position without losing position entirely. So we can't set bed elevation within 0.001 by using 1/32 microstepping. The position error could be as large as 0.016, depending on which microstep we're attempting to hold. (If we operate on the verge of losing steps. We probably run at something more like 80% of that.) Microstepping smooths things and makes the system quieter, but does not improve accuracy by the same factor as the microstepping fraction. 


---
**Liam Jackson** *March 12, 2014 12:37*

You can get 20 tooth GT2 pulleys for 8mm shafts, what that would do to a belt holding up a heated bed I have no idea, but it helps the numbers a lot :-D ﻿



Agreed that we should not rely on microsteps, full step per 5 microns is my vote! 


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/TbeRJ2xvj46) &mdash; content and formatting may not be reliable*
