---
layout: post
title: "New member here. I am collecting parts for a Eustathios build"
date: January 28, 2018 14:50
category: "Discussion"
author: John Gadbois
---
New member here.  



I am collecting parts for a Eustathios build.  Had planned on a D-bot and had ordered most of the parts when I came across the Eustathios.  I realized that a large volume printer would not be practical unless it was faster. So I chose Ultimaker kinematics which led to Eustathios.



I will be modifying the frame slightly, using 2040 instead of 2020 in some areas as that is what I bought for the D-bot. I also have a 300x300 aluminum plate and 120v heater to incorporate.



Does anyone have any tips for getting a flat square end on extrusion cuts?  I have a 12” compound miter saw and aluminum cutting blade.



Also,  when looking through the posts I am having difficulty determining whether some of the posts refers to HercuLien or Eustathios. It would be helpful if posters could indicate which printer they are referring to. 



Thanks for any help.











**John Gadbois**

---
---
**Eric Lien** *January 29, 2018 01:50*

Some printers are not as sensitive to frame misalignment. The HercuLien and Eustathios are critical to having frame cut to correct dimensions and square. I usually order mine cut to length from Misumi for these printers.



Perhaps someone who has cut thier own extrusion can chime in.


---
**Dennis P** *February 06, 2018 06:50*

A well tuned chop saw and non-ferrous blade with a few drops of either Alumatap or wd40 as lube. mt blades are 'older' Delta negative hook blades. 


---
*Imported from [Google+](https://plus.google.com/110366941702450753861/posts/ey6JcGCaPti) &mdash; content and formatting may not be reliable*
