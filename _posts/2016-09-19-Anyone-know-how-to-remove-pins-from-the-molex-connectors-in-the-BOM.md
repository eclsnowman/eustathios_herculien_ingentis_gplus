---
layout: post
title: "Anyone know how to remove pins from the molex connectors in the BOM?"
date: September 19, 2016 01:04
category: "Discussion"
author: Sean B
---
Anyone know how to remove pins from the molex connectors in the BOM?  It is a Microfit 3.0 8 pin connector.  One of my wires snapped after installing, followed by a second when trying to remove the first...



Should I just order a new connector?  ﻿





**Sean B**

---
---
**Oliver Seiler** *September 19, 2016 01:23*

I usually use a new one. There'a a tool you can buy to remove the pins, but its horrendously expensive. 

I'm using 3 connectors now (fan, heater+temp, blower) to reduce the impact and be able to swap out parts easier.


---
**Eric Lien** *September 19, 2016 02:43*

**+Oliver Seiler** is right, I would try a new connection. If they are breaking perhaps you are crimping too hard. Also make sure you strain relief it somehow. Even just using large heat shrink tubing over the bundle and the end of the connector to give it some stiffness as it enters the connection can help.


---
**Zane Baird** *September 19, 2016 04:00*

For anyone else reading this, I would also recommend buying 3x the connections you think you might need. With shipping, its worth the extra couple $ for piece of mind. Some other advice: 1) cut your wires longer than you think you'll need them before you crimp the connectors on. You'll want room for error here. 2) test your crimps with a mulitmeter and by pulling on them by hand to make sure they are secure. Once you plug them into the housing its still rough to get them out with the right tool. DO NOT insert them into the connector unless you're sure you have made a quality crimp. 3) make sure and use a quality crimping tool to save yourself the hassle. 



I know I've vented my frustrations to **+Eric Lien** about these connectors before, but they are definitely still my recommendation as well. They are in spec for the power load and are small enough to work well for the application better than any other connector I've used. Because of these I can switch out end effectors in my delta in a matter of seconds and have no worry about the current load causing me grief


---
**Tomek Brzezinski** *September 19, 2016 04:06*

I use some combination of hot glue or heatshrink for strain relief, or hot glue with heatshrink. The hot glue is good at binding the wires together to share loads and bridging the housing and the wires for secure relief.



A word of caution on pulling crimps too hard- you can easily cause damage in that process. I do a light tug sometimes, but prefer to inspect the crimp under microscope, especially once I have the settings down for a given crimp and crimper (usually I don't have the standard crimper for a given crimp, given the OEM crimpers are super pricey.)




---
**Jeff DeMaagd** *September 19, 2016 04:36*

I guess the cost has gone up a bit since I bought the ejector tool. It's easier to snip off and start again in my opinion. [digikey.com - WM4561TR-ND](http://www.digikey.com/product-search/en?mpart=0011030043&vendor=900)


---
**Sean B** *September 19, 2016 11:02*

Omg that is pricey.  I'll just purchase another connector.  Thanks everyone!


---
**Gústav K Gústavsson** *September 20, 2016 00:03*

Hummm.... computer cable MOD needle tool/ PIN remover / Pin Extractor for sata/molex/24PIN/8PIN/6PIN 4pcs/set

[aliexpress.com - computer cable MOD needle tool/ PIN remover / Pin Extractor for sata/molex/24PIN/8PIN/6PIN 4pcs/set](http://s.aliexpress.com/7nqIBFBN)


---
**Gústav K Gústavsson** *September 20, 2016 00:10*

Don't know if it will do the job but I used to have fine steel strips which did the job nicely but I seem to have misplaced them when I moved last time :(


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/Ydmaxqooing) &mdash; content and formatting may not be reliable*
