---
layout: post
title: "For the heating pad I see Eric changed from 500W for the Eustathios Spider 2 to 800W for the Herculien"
date: August 10, 2015 03:20
category: "Discussion"
author: Ted Huntington
---
For the heating pad I see Eric changed from 500W for the Eustathios Spider 2 to 800W for the Herculien. What is the best wattage heating pad at 120V that people have found? As far as I know Marlin firmware just passes the full voltage (unpulsed) to the heating pad. Sivia at Alirubber said a 120V pad has a resistance of 20ohm, so 120/20=6A  120Vx6A=720W.





**Ted Huntington**

---
---
**Eric Lien** *August 10, 2015 03:24*

The HercuLien is 800W because it is almost 17" x17" and 1/4" thick. Eustathios is 500W because it is smaller and 1/8" aluminum. In fact I still run my Eustathios on a 400w 24V bed. But I upped it to 500W 120v via an SSR on Eustathios V2 because I like 120v beds so much better.


---
**Eric Lien** *August 10, 2015 03:24*

You just spec what you want, and they make it custom.


---
**Ted Huntington** *August 10, 2015 03:25*

How are you calculating wattage? They have "Wattage

Customer specified(Normal maximum of 5 watts of 5 watts per square inch)" - is that 5 watts/sq inch or 25watts/sq inch?


---
**Ted Huntington** *August 10, 2015 04:08*

I talked more with Sivia and they confirmed that the resistance of the pad just depends on wattage and voltage so for example, no matter what size, for 120V 500W, R=V*V/P, 120x120/500 = 28.8ohm, so 120V/28.8 =4Amps- no matter what size pad- it will start at room temperature with 4A (heating presumably increases the resistance). In addition, they only make 1 ply (1.5mm) or two ply (3.0mm) which is more expensive.


---
**Vic Catalasan** *August 10, 2015 07:56*

If you decide running 500+ Watt bed heater I would  recommend running it on SSR 120V, this would be less taxing on your power supply. The relay can take a lot more abuse than the power supply that is also powering your hot end and steppers. Relays are a lot cheaper than buying higher amp power supplies.  


---
**Ted Huntington** *August 10, 2015 15:26*

oh definitely- I use the cheap SSR-25 from ebay- they are only a few dollars.


---
**Ishaan Gov** *August 10, 2015 15:41*

The other nice thing with running beds off of mains, is that a power supply of equivalent wattage draw up to 1.5 times the power of the actual heater, due to the efficiency of the supply and other losses. That means in the US, if you have at ~350W power supply driving your electronics (drawing 7A from the mains), then you can only run a 350W heated bed @ 24V (24V supply also draws 7A from the mains); therefore, bumping up the bed to mains voltage allows you to more than double your bed power, while still not overloading your house circuit


---
**Ted Huntington** *August 10, 2015 17:09*

one thing I found is that on a 15A circuit- heating up my bed at 120V and my treadmill can't run at the same time or it blows the fuse. It's time to up the standard house circuit limit to 20A I think.


---
**Jeff DeMaagd** *August 11, 2015 03:21*

Unless you buy a crappy PSU or bought a linear supply, you should be getting 80% or better efficiency, so to power a 200W bed, you shouldn't be using 300W, but rather, 250W or less. That said, going from mains is far more efficient than that.﻿ the biggest down side is its more places that can go wrong safety-wise in handling mains voltage, more connections that can get wired poorly.


---
**Ishaan Gov** *August 11, 2015 04:06*

**+Jeff DeMaagd**, some of the NES-350 power supplies that I have (the proper meanwell ones) have typical current draws of 7A at 115V, which means ~800W for a 350W power supply; that may be accounting for some power factor discrepancies or some other things; spec'd efficiency for the supplies are in the 80% range


---
**Jeff DeMaagd** *August 11, 2015 04:25*

Is that measured, or just reading from the label, which is really a worst-case scenario?


---
**Ishaan Gov** *August 11, 2015 04:33*

**+Jeff DeMaagd**, that was a typical data sheet spec


---
**Jeff DeMaagd** *August 11, 2015 04:36*

Take a measurement. Data sheet numbers mean specific things, and I'm pretty sure it's not what you think they mean.


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/jQgAd8F34kb) &mdash; content and formatting may not be reliable*
