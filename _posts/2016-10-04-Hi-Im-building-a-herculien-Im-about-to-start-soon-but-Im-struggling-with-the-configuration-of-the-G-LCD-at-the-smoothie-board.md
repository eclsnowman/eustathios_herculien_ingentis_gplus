---
layout: post
title: "Hi Im building a herculien. I'm about to start soon but I'm struggling with the configuration of the G LCD at the smoothie board ."
date: October 04, 2016 11:19
category: "Discussion"
author: Mikael Sjöberg
---
Hi

Im building a herculien. I'm about to start soon but I'm struggling with the configuration of the G LCD  at the smoothie board .



The resolution of the glcd is also poor. How can I increase that? 



At the moment I have this written in the configfile :





# config settings

panel.enable                          true              # set to true to enable the panel code

panel.lcd                             reprap_discount_glcd     # set type of panel

panel.spi_channel                     0                 # spi channel to use  ; GLCD EXP1 Pins 3,5 (MOSI, SCLK)

panel.spi_cs_pin                      0.16              # spi chip select     ; GLCD EXP1 Pin 4

panel.encoder_a_pin                   3.25!^            # encoder pin         ; GLCD EXP2 Pin 3

panel.encoder_b_pin                   3.26!^            # encoder pin         ; GLCD EXP2 Pin 5

panel.click_button_pin                1.30!^            # click button        ; GLCD EXP1 Pin 2

panel.buzz_pin                        1.31              # pin for buzzer      ; GLCD EXP1 Pin 1

panel.back_button_pin                 2.11!^            # 2.11 menu back      ; GLCD EXP2 Pin 8

#added panel.encoder_resolution with value of 4

panel.encoder_resolution                    4



# setup for external sd card on the GLCD which uses the onboard sdcard SPI port

panel.external_sd                     true              # set to true if there is an extrernal sdcard on the panel

panel.external_sd.spi_channel         1                 # set spi channel the sdcard is on

panel.external_sd.spi_cs_pin          0.28              # set spi chip select for the sdcard (or any spare pin)

panel.external_sd.sdcd_pin            0.27!^            # sd detect signal (set to nc if no sdcard detect) (or any spare pin)



custom_menu.power_on.enable              true              #

[custom_menu.power_on.name](http://custom_menu.power_on.name)                Power_on          #

custom_menu.power_on.command             M80               #



custom_menu.power_off.enable             true              #

[custom_menu.power_off.name](http://custom_menu.power_off.name)               Power_off         #

custom_menu.power_off.command            M81               #



custom_menu.filament_change_c.enable               true                                                   #

[custom_menu.filament_change_c.name](http://custom_menu.filament_change_c.name)                 Change Filament                                        #

custom_menu.filament_change_c.command              G91|G1 Z0.6 F12000|G90|G1 X0 Y0|G91|G1 Z-0.6|G90|M25   #



custom_menu.filament_change_r.enable               true              #

[custom_menu.filament_change_r.name](http://custom_menu.filament_change_r.name)                 Resume            #

custom_menu.filament_change_r.command              M24               #





What else do I need ?







![images/d8e4faed418b795cb2ebab862e4e330f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d8e4faed418b795cb2ebab862e4e330f.jpeg)



**Mikael Sjöberg**

---
---
**jerryflyguy** *October 04, 2016 14:17*

I'll have to look it up but there is a panel.contrast call (I set mine to 8) and that makes it much more legible


---
**jerryflyguy** *October 04, 2016 14:18*

Are you using the config file off the smoothie github?


---
**Philipp Tessenow** *October 04, 2016 14:32*

As I recall panel.contrast doesn't work for all LCD panels. 

There is poti on the back of the LCD as well. Are you using just USB or a PSU to power the unit?


---
**jerryflyguy** *October 04, 2016 14:37*

It's worth a shot before adjusting a physical pot, I didn't see it in his list of config calls. If it doesn't help (easy fix) then moving to a larger PS or adjusting physical pots makes sense as potential issues.


---
**Eric Lien** *October 04, 2016 14:43*

It depends on where you sourced the glcd display. Looks like some just need the pot adjusted... And some are actually incorrect requiring a resistor be soldered in for the pot to work. Look here for more details: [http://forums.reprap.org/read.php?13,448609,page=1](http://forums.reprap.org/read.php?13,448609,page=1)


---
**Mikael Sjöberg** *October 04, 2016 16:41*

Thanks all for your Quick help, I will dig into it and get back to you How it proceeds.


---
**Mikael Sjöberg** *October 04, 2016 17:43*

**+jerryflyguy** Yes, the one available in the herculien download . But as I see it, that one doesnt include the lcd screen. So i have used the codes in the smoothieboard 3D printing manuals 


---
**jerryflyguy** *October 04, 2016 17:55*

**+Mikael Sjöberg** I found when setting up mine (Azteeg) that starting with the latest config was a big help. From there, adding code etc was much simpler where needed. I did use the Viki2 LCD so that may give different results between systems. I'm also much less experienced than the others posting here. 😉 


---
**Mikael Sjöberg** *October 04, 2016 18:00*

**+jerryflyguy** sorry I was wrong . I have used the configfile that was already on the smootiheboard 

And added the codes in the manual . Is there a configfile available at GitHub ?


---
**Mikael Sjöberg** *October 04, 2016 18:02*

**+jerryflyguy** well then we are at the same basic level . I'm really interested in the 3D technique, but I'm not a pro in electrics and codes :) 


---
**Mikael Sjöberg** *October 04, 2016 18:21*

Well I found a nice configfile in the github for herculien. That one is made for azteeg, is it possible to adapt it for a smooothieboard ? 




---
**jerryflyguy** *October 04, 2016 19:12*

**+Mikael Sjöberg** I'd assume they would be similar, I'd guess that all the pinouts for the smoothie would be different from the Azteeg though.


---
**Jeff DeMaagd** *October 04, 2016 20:19*

That you got the LCD displaying information means you got it right. Fixing the contrast is just dialing a micro pot somewhere on the LCD board.


---
**Mikael Sjöberg** *October 04, 2016 20:28*

**+Jeff DeMaagd** ok! Thanks !

Then maybe I shall keep the codes I have now . I can move the x, y and z axis . I can start and stop the fans , I can heat up the bed and the hotend . 

I have the extruder config left . I have to check if that's already prepared in the file and if I have the correct port written in the file 


---
**Gústav K Gústavsson** *October 06, 2016 01:56*

There is pot on the display board to adjust the contrast, just adjust it slowly to get the contrast you want.


---
*Imported from [Google+](https://plus.google.com/118217975155696261814/posts/9cCNq6rhzXr) &mdash; content and formatting may not be reliable*
