---
layout: post
title: "Lady's and gentlemen, I present to you my new hotend!"
date: November 07, 2014 03:18
category: "Show and Tell"
author: D Rob
---
Lady's and gentlemen, I present to you my new hotend! As of yet untested, but will be soon. 



This uses x2 **+E3D-Online** v6 heater blocks, nozzles, kraken heat breaks, and my Hydra heatsink. HAIL HYDRA! This is my answer to all of the lost space from using traditional dual hotend setups. But for those not needing, or not ready for the Kraken. There is a cut that  I have not made yet. This will be the heat break clamp. It is the same concept for the v6 heater cartridge clamp. 1x M3 screw per hotend. Level the bed with only one hotend installed then insert the other one loose. Home. Then put a feeler gauge under it (piece of paper or what you used to level the bed. ) then tighten. Done! I made this with a drill press, vise, hacksaw, calipers (for measuring and etching cut lines), a wood saw to widen the cuts, and a needle file to clean the cuts. It's made from 1x1 aluminum square stock cut 1 ⅛" long. It will have active cooling from 1 or 2 fans. Not sure yet if it needs 2. I have designs for. 3mm also and for some other experimental cooling techniques.



![images/e07e9743d9013517cad4dd67d78a46ba.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e07e9743d9013517cad4dd67d78a46ba.jpeg)
![images/f046d668e358a99252ba9b76d13c19f2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f046d668e358a99252ba9b76d13c19f2.jpeg)
![images/86af60fd783a4544a1185e02e9099195.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/86af60fd783a4544a1185e02e9099195.jpeg)
![images/37a06e9037e81e1c240e71022dbe0e9b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/37a06e9037e81e1c240e71022dbe0e9b.jpeg)
![images/ea3de2c0ec331429b9649da4137728f5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ea3de2c0ec331429b9649da4137728f5.jpeg)
![images/f3b3aabc3b8217c2819b81a2d93f5d30.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f3b3aabc3b8217c2819b81a2d93f5d30.jpeg)
![images/5cc427aee26d49faaa1fd799a03fc935.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5cc427aee26d49faaa1fd799a03fc935.jpeg)
![images/958a8ee340aba11b48fabadb17a74c02.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/958a8ee340aba11b48fabadb17a74c02.jpeg)
![images/20ba79d7018a9e445b32af76a37d3117.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/20ba79d7018a9e445b32af76a37d3117.jpeg)
![images/1b97fb9ef65541ced7339d8ac910a08c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1b97fb9ef65541ced7339d8ac910a08c.jpeg)
![images/a0f19eb956aba87f40243894ca29d0bd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a0f19eb956aba87f40243894ca29d0bd.jpeg)
![images/3e9bfb0f9405bb0c26d7c0295c748475.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3e9bfb0f9405bb0c26d7c0295c748475.jpeg)
![images/e5f7945d2af130d88798db7e0fb9e0bb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e5f7945d2af130d88798db7e0fb9e0bb.jpeg)
![images/bb7f0ffa13f5c1e36772219fff3f1ee0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bb7f0ffa13f5c1e36772219fff3f1ee0.jpeg)

**D Rob**

---
---
**Brandon Satterfield** *November 07, 2014 03:30*

**+D Rob** gnarly idea man! I like it! Do post how testing goes, let me know if you need parts or help.


---
**Mike Miller** *November 07, 2014 03:48*

Someone get this man a mill! And a Lathe!


---
**D Rob** *November 07, 2014 04:04*

I have a Taig lathe. Out of order. Yes someone please give me a mill lol!  Based on this imagine what I could do with some tools. But this is just the prototype. If it works as expected I will outsource production and sell them. I posted the how to pics so others can build and test while I work out the kinks. I'm working on going to college and still working full time. So time is short until school starts. Then i can get a part time job. Gi Bill will handle the rest. Sucks being a 31yo freshman lol! But EE here I come.


---
**Daniel F** *November 07, 2014 07:36*

looks great, too bad I decided to go with 3mm when I started to build my printer. Kraken is only for 1.75 as far as I could see. Although 3mm should be possible with m6 threads in the block and "regular" e3dv6 heat pipes.


---
**D Rob** *November 07, 2014 08:13*

I have plans for a 3mm heat break and a SW file for one. Should be easy to lathe. There is also a heat sink already designed. 


---
**Nicolas Arias** *November 07, 2014 10:58*

Its ok if i make one?


---
**Carlton Dodd** *November 07, 2014 13:01*

**+D Rob** Yay education!  I was 48, and about to retire from the USAF when I finally finished my Bachelor's degree. Well worth it!  


---
**D Rob** *November 07, 2014 14:26*

**+Nicolas Arias** of course


---
**Brandon Satterfield** *November 07, 2014 14:38*

**+D Rob** did my BSME at 30 working full time with a family. Admire anyone who takes on this challenge! Noted, it took me 7 years with life as a background. It will not be the easiest thing, but completely worth it. 


---
**D Rob** *November 07, 2014 18:59*

**+Daniel F** threads are a no go. Makes leveling with heater blocks impossible.


---
**D Rob** *November 08, 2014 18:47*

I'd like to note to those with a band saw that a stop plate for depth of cuts and some distance jigs of some sort would make short work of this. I like this design because you don't have to have a mill. Yes one is nice, but not necessary


---
**Mike Miller** *November 08, 2014 19:06*

It's all about using what you've got, especially if you're tool constrained. 


---
**D Rob** *November 08, 2014 19:37*

**+Tim Rastall** father of ingentis this is designed with our builds in mind so... what do you think?


---
**Tim Rastall** *November 09, 2014 19:39*

I like it. Principal concerns would be the heatsinks ability to dissipate. A single v6 sink has way way more surface area than this one, and even they seem to struggle to keep the break cool enough to prevent pla sticking. Suggest you put a thermistor hole right between the heat break holes to see what sort of temp you get at the bottom of the block. Aggressive cooling and a duct to push air over the breaks could be enough to overcome this though.


---
**D Rob** *November 09, 2014 23:36*

**+Tim Rastall** I plan on having 2x 30mm fans on opposite sides I just havent decided whether both face in, both out or 1 in and 1 out. Both in may interfere with air current. Both out may draw enough air in between them and across and out. 1 in 1 out (blowing the same direction will increase flow but how much? these things i need to test I have an extra ramps so will build a test rig before it ever mounts to a printer


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/gkSeV7qQi9z) &mdash; content and formatting may not be reliable*
