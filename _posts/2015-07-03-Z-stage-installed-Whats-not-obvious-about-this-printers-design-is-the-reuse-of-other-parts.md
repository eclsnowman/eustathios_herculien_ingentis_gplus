---
layout: post
title: "Z-stage installed. What's not obvious about this printer's design is the reuse of other parts"
date: July 03, 2015 17:20
category: "Deviations from Norm"
author: Mike Miller
---
Z-stage installed.  #ingentilere  What's not obvious about this printer's design is the reuse of other parts. It's using the motor mounts and carriage from **+Eric Lien**, and the Rod holders are designed to use his motor mount for the Z-stage. 



The frame is held together using Button Head Screws, the same screws are used to support the external shaft mounts (You've gotta wedge a screwdriver in while tightening the nuts to keep them from spinning.) but the shaft mounts are simple to print. 



The Z stage consists of two Rod retainers, the Stage lift, and the bed supports. What <i>isn't</i> obvious, is that it can be duplicated and mirrored for a larger printer with a heavier stage. The Rod retainers are on opposite sides of the extrusion so that the top has clearance for restraining the leadscrew if it needs it. (There's a notch in the lift that will require some backlash adjustments in the firmware, but that's done to decouple the leadscrew from the lift. 



Currently there are 6 types of printed parts unique to this design. You calibrate your printer so that the parts are to your spec and you just tell it to go to town. 8 of one kind, 4 of another, two each of this and that part. 



I've been hoping to keep oddball screw size count down, but these printers don't really lend themselves to that. You've still gotta buy some t-nuts and a buncha hex-head screws.  I also like the printer at this point as you can see the mechanism...adding the electronics just makes everything look yucky. 



![images/443c3c7c38fe434a461f14d89057dc64.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/443c3c7c38fe434a461f14d89057dc64.jpeg)
![images/7bc03f499dec8a52df98f7e53c760da2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7bc03f499dec8a52df98f7e53c760da2.jpeg)
![images/39b875da68f97944c0bb6151f37d636d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/39b875da68f97944c0bb6151f37d636d.jpeg)

**Mike Miller**

---
---
**Rick Sollie** *July 03, 2015 21:46*

Interesting design. Looks like you are pushing anything that will take up printing real estate outside the frame.


---
**Mike Miller** *July 03, 2015 21:54*

I am. After its running well, I'll take a look at the carriage, but my CADCAM skills are a little weak and there are some great designs already out there. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/WVepYS8u986) &mdash; content and formatting may not be reliable*
