---
layout: post
title: "After struggling with PETG on an E3D Volcano I decided to revisit the idea of a dual V6/Volcano carriage for the Herculien"
date: November 06, 2015 00:15
category: "Show and Tell"
author: Zane Baird
---
After struggling with PETG on an E3D Volcano I decided to revisit the idea of a dual V6/Volcano carriage for the Herculien. Waiting on some brass inserts (no more captive nuts) before I can test it. Oh, and I need to buy a V6 now...



The plan is to mount the hot ends using threaded brass inserts and some M3 screws to hold it in place. Should allow for adjustments of nozzle heights over 1-2 mm as well as easy removal of the hot ends and access to the bowden pushfit connectors. Designed to print without supports.



![images/3da04b7fd3b23b8456398d4118f57aa1.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3da04b7fd3b23b8456398d4118f57aa1.png)
![images/f0917779441cc8b4e477472d1487f2f9.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f0917779441cc8b4e477472d1487f2f9.png)
![images/d114af62f6371805de1e4eae42043486.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d114af62f6371805de1e4eae42043486.png)
![images/fac4707df2ee78ca010883b560398f90.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fac4707df2ee78ca010883b560398f90.png)
![images/bd9df254dd1d44ebc2050018a1b7a0c2.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bd9df254dd1d44ebc2050018a1b7a0c2.png)

**Zane Baird**

---
---
**Jim Stone** *November 06, 2015 00:17*

Sexy carriage. That's what stopped me from getting a volcano. Cause I don't always need mega size


---
**Eric Lien** *November 06, 2015 01:25*

Beautiful work. BTW what is the slot on the top.


---
**Ishaan Gov** *November 06, 2015 03:50*

Just a thought, have you considered using an E3D Chimera with two volcano heads, one with a small nozzle diameter (0.4) and the other with a larger diameter (0.8)? That is, only if your problems stem from the larger diameter nozzle as the source of your oozing problems


---
**Zane Baird** *November 06, 2015 05:09*

**+Eric Lien** the slots are to zip tie the wires for electronics. I will post an update when I install it


---
**Zane Baird** *November 06, 2015 05:11*

**+Ishaan Gov** The benefit of the Volcano is the extended melt zone, not just larger nozzles. This allows for much faster speed with traditional nozzle sizes. However, it's a lot more work to dial in the right settings


---
**Hakan Evirgen** *November 06, 2015 09:42*

**+Zane Baird** if you want buy V6 hotends, I have two to sell here: one is not even assembled and other has been used just for some test prints (Benchy and 0.5mm cube). I do not need them as I switched to Chimera.


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/aPhpWmxCyBh) &mdash; content and formatting may not be reliable*
