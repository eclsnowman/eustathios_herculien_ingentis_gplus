---
layout: post
title: "Completed my frame a couple weeks ago and am really struggling with getting the carriage to move freely"
date: January 13, 2018 22:59
category: "Discussion"
author: Ryan Fiske
---
Completed my frame a couple weeks ago and am really struggling with getting the carriage to move freely. As in it takes a whole hand to push it around and often with another hand on the frame. Very stiff! I went through the great gantry alignment video and still things are quite tight. I understand that this might just mean I need to take things apart and perhaps try again, but I was curious if anyone here could offer some tips or tricks that I could try prior to doing so.



Also to note: Almost all the bronze bushings slide pretty easily when I was moving them individually before assembly along the rods, so I'm hoping my issues aren't from that in particular.





**Ryan Fiske**

---
---
**Eric Lien** *January 13, 2018 23:39*

Are the X/Y Drive motors attached? If so remove the belt to the drive motor for that axis.



Have you confirmed the cross rods are running parallel to the drive rod, and perpendicular to the guide rod for that axis. (note the guide rod for one axis is the drive rod for the other).



Is it harder to move towards the sides than towards the center (that usually means you need to adjust the side bearing supports to match the C-C spacing of the center carriage, since printed parts are imperfect).



Are the side bearing blocks oversized. If so they can cause issues where you will never be able to get the rods far enough apart since they hit the frame supports (see attached image). If they are oversized take a file/razor to them, or you can lower the lower side extrusion a little to give them breathing room. I really need to adjust the design to allow for a tolerance here. The model and design assume the parts are perfect. This of course causes issues when they are not.

![images/32ff92ac4bb6b840fff5f3f1b5408cf9.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/32ff92ac4bb6b840fff5f3f1b5408cf9.png)


---
**Ryan Fiske** *January 14, 2018 00:27*

Eric thanks for the questions! I'll try better to characterize the issues and report back


---
**Dennis P** *January 24, 2018 17:33*

i am starting to assemble my printer and noticed the same thing. if i assemble the bearings in the holders in pairs using a rod or shoulder bolt as a pilot mandrel they have almost no friction as compared to putting them into the bores unguided. 


---
**Ryan Fiske** *January 24, 2018 17:58*

So I took a quick measurement of my bearing holders and they all appear to be less than 45mm, so hopefully this weekend I can start to check parallel and perpendicularity. Wish I had more time to devote to this!


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/USoVcYEVtbr) &mdash; content and formatting may not be reliable*
