---
layout: post
title: "I've just finished my biggest print ever, a 1.3kg CF reinforced PETG model, for a client"
date: March 02, 2018 07:29
category: "Discussion"
author: Oliver Seiler
---
I've just finished my biggest print ever, a 1.3kg CF reinforced PETG model, for a client. I had to use the whole 300x300mm of the build plate and used a Volcano with a 1mm nozzle, 0.4mm layers and 50% infill, and took 2 full days to finish. I was shit scared to be honest, especially when I realised towards the end that I had forgotten some additional supports (and quickly squeezed some EPP foam into the nut holes, which worked a treat to support the layers above!).

Anyway I'm again and again amazed at the reliability and versatility of the Eustathios. This is my 2nd printer and I've got no desire to replace it with anything else (what do you HercuLien owners do???).

Heads up and massive thanks to everyone who contributed to this awesome beast. I've learned so much, finished some crazy jobs from 3DHubs, done some parts for the local movie industry and supported a number of start ups prototyping their products, always pushing the boundaries of my skills. It's been an amazing ride so far and I hope it goes on much longer...





**Oliver Seiler**

---
---
**Oliver Seiler** *March 02, 2018 07:30*

Sorry, can't post any photos publicly, because it's a client's print.


---
**Eric Lien** *March 02, 2018 12:40*

I am happy it is serving you so well. I have had similar experiences. It is not the easiest printer to build and break in. But once you do it's a great workhorse.



All my biggest prints on my HercuLien were for foundry prototype patterns. I designed that printer around those needs originally. But I no longer work at the Foundry, so HercuLien doesn't get as much use as it once did.


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/dou9rvLMEdh) &mdash; content and formatting may not be reliable*
