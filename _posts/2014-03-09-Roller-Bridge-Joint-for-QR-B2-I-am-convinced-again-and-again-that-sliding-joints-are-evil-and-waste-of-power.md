---
layout: post
title: "Roller Bridge Joint for QR B2 I am convinced again and again that sliding joints are evil and waste of power"
date: March 09, 2014 04:59
category: "Deviations from Norm"
author: Tim Rastall
---
<b>Roller Bridge Joint for QR B2</b>

I am convinced again and again that sliding joints are evil and waste of power. Rolling joints rock and roll! To this bridge-joint you can attach whatever extruder(s) and/or hot-end(s) you like: The bill of materials: One piece of plastic 30x30x10mm (can be...





**Tim Rastall**

---
---
**D Rob** *March 09, 2014 05:10*

my concern is the tolerance for accuracy in extrusions, and is aluminum going to hold up to the abuse of hardened steel bearings?


---
**Tim Rastall** *March 09, 2014 05:14*

**+D Rob** yeah,  my thoughts too,  allthough it's been pointed out that this idea would work with shafts if you used 3 bearings arranged in a triangle around the shaft. 


---
**Tim Rastall** *March 09, 2014 05:15*

Aluminium extrusions like this are normally pretty straight ime. 


---
**D Rob** *March 09, 2014 05:33*

anyone thought about steel channels? with the right set up (one bearing in the channel and one on both sides and bottom it would have a tighter form factor rigidity and look pretty cool too) lol


---
**William Frick** *March 09, 2014 06:24*

If the block was nylon or delrin it would have even less friction. There is no adjustment for wear in this assembly though. **+Shauki Bagdadi**  does seem to have mastered the ( M/m)*(1/$)  equation though !


---
**Tim Rastall** *March 09, 2014 06:33*

**+William Frick** yeah,  I thought spring loading one side somehow might be worth considering.

It really should be Dr **+Shauki Bagdadi** as he's a PhD in mech eng,  which shows in his ability to macguyver these solutions into existence. 


---
**Tim Rastall** *March 09, 2014 06:35*

I thought a UHMW-PE block would make sense as it's pretty close in its friction coefficient to Teflon and is super hard wearing. 


---
**William Frick** *March 09, 2014 06:37*

**+Tim Rastall** Delrin is UHMW (variety of)


---
**Tim Rastall** *March 09, 2014 06:39*

**+William Frick** good point.  I'm sure I used to know that :) 


---
**Tim Rastall** *March 09, 2014 07:52*

How about UHMW-PE strips you slide into a corresponding slot in the printed part? They could then be replaced too. 


---
**William Frick** *March 09, 2014 12:41*

Really hard to successfully clamp as it will squirt out of vice jaws if not perfectly square !


---
**Eric Moy** *March 09, 2014 14:33*

**+Shauki Bagdadi** , if they is any bending moment (moment on the carriage that would result in a transferred bending moment to the beam that is) on the carriage while it is moving, I could see the hardened steel eventually yielding the aluminum as it is only supported by 2 point loads at the site of the force couple, which would be the tangency of the bearings.



Any preload put between the bearings and the rail would be added to the applied force couple, and I'm thinking over time, the aluminum will yield and your preload will decrease, allowing further slop and deflection.



Of course you could find the balanced of lowered preload and acceptable deflection.



Then again, I'm probably over thinking the crap out of this, as the deflections in the elastic region are probably insignificant compared to errors in extruder beads, so you're probably loading the aluminum far below any plastic deflection.



Sorry, it's hard to switch back and forth from normal world and metrology world, where you chase microns.



Either way, I love reading your work, obviously gets my brain churning :-)


---
**Eric Moy** *March 12, 2014 01:54*

**+Shauki Bagdadi** , that would be an interesting test, unfortunately, I don't have my octopi setup correctly to record timelapse... for that matter, I don't have a carriage designed to ride on my misumi rail, but I do have plenty of 608 bearings.



I'm honestly, planning on using the LM8UU's out of my defunct Eventorbot. Need to get my money's worth out of the thing. since my extruder motor finally gave up, I haven't been able to print anything at home for some time :o/


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/8ZmKAXfWphz) &mdash; content and formatting may not be reliable*
