---
layout: post
title: "Endstops seem optional these days. I'm seeing several of you with moving printers with no real endstops in sight"
date: June 22, 2014 14:45
category: "Discussion"
author: Mike Miller
---
Endstops seem optional these days. 



I'm seeing several of you with moving printers with no real endstops in sight. ;)



I get that you can set the nozzle pretty much anywhere and say 'here's 0,0'  but how do you consistently set Z=0? I hope it's not manual jogging and eyesight, my vision's declining!





**Mike Miller**

---
---
**Jason Smith (Birds Of Paradise FPV)** *June 22, 2014 14:51*

The #Eustathios uses endstops. I have no idea how you'd get an accurate z position without them. 


---
**ThantiK** *June 22, 2014 14:53*

Great eyesight (20/15) here.  Manual jogging all the way.


---
**Shachar Weis** *June 22, 2014 15:44*

No endstops no worries. Less things to go wrong :)


---
**Tim Rastall** *June 22, 2014 20:33*

I use min endstops on all axis. 


---
**Eric Lien** *June 22, 2014 21:11*

They are so cheap I don't see why not to use them. I actually had a print recently that could have used max endstops too. Not sure what happened but it was printing fine then just shot off into the back right corner for no apparent reason. No skipped steps.... Just took off into no mans land and made one hell of a racket.


---
**Andreas Thorn** *June 23, 2014 11:02*

I always start/stop the print at a set Z. Got it in start- and end.gcode. Calibrate the bed once in 4 locations using a dial gauge and print for weeks/months with no problem. Never got around to mounting end stops.


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/GFTbrgcaEum) &mdash; content and formatting may not be reliable*
