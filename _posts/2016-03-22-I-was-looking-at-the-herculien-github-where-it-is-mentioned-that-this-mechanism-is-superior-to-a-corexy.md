---
layout: post
title: "I was looking at the herculien github where it is mentioned that this mechanism is superior to a corexy"
date: March 22, 2016 22:09
category: "Mods and User Customizations"
author: ekaggrat singh kalsi
---
I was looking at the herculien github where it is mentioned that this mechanism is superior to a corexy. Why is that so. I am intending to build a 600x600 x600 mm monster and i need to decide on the direction of mechanism to go. Any advice?





**ekaggrat singh kalsi**

---
---
**Eric Lien** *March 22, 2016 22:19*

For that size printer I wouldn't recommend going to a crossrod gantry of the Ultimaker or Herculien/Eustathios style. Corexy or perhaps Quadrap style will probably scale better at that size. Also **+Jeff DeMaagd**​​ has a printer that performed better than most small printers I have seen. And it has a massive build area: [https://m.flickr.com/#/photos/jabella/25812195362/in/set-72157665553307120/](https://m.flickr.com/#/photos/jabella/25812195362/in/set-72157665553307120/)﻿


---
**ekaggrat singh kalsi** *March 22, 2016 22:28*

I didnt mean using rods, but to choose between the cross mechanism and the corexy. Both can done using plates or misumi sections and linear guides. Just want to know the disadvantages of corexy. Thanks.


---
**Eric Lien** *March 22, 2016 22:34*

My corexy was a bear to keep tuned. Balanced belt tensions was critical. And the long belts required for a 600x600 corexy might cause some belt stretch backlash or at minimum harmonic nodes along the long horizontal belt paths.


---
**Paul de Groot** *March 22, 2016 23:53*

I was in a similar position as you a few months ago.  I decided to go for a core xy for my 500x500 printer because of the scaling issue for a cross bar. It's all about innovation. You need to solve those barriers. Once you overcome those corexy might be out performing a cross bar solution.  We need more people to drive innovation and not just copying a design. Unfortunately not everyone is born with an engineering brain 😕


---
**Ryan Carlyle** *March 23, 2016 00:40*

CoreXY is a very scaling-insensitive design. All you need to do aside from standard frame scaling is make the X bridge stiffer and use heavier-duty belts and bigger motors as the size scale increases. (Ignoring the Z stage design, which is basically identical to those used with cross gantries.)


---
**Ryan Carlyle** *March 23, 2016 00:42*

Jetguy has a 1.2 meter cube CoreXY (four ballscrew Z stage) that makes incredible prints. I personally think it's silly to lift the bed at that size, you should lift the entire gantry instead, but that's a design trade-off. 


---
**Jo Miller** *March 23, 2016 00:51*

Corex XY needs much   longer belts for the XY ,  so - I wouldent go for any  standart  6mm belts ,  you´ll need , steel enforced 8 to 10 mm  belts , if not,  you´ll see a lot of ghosting in your prints


---
**Jeff DeMaagd** *March 23, 2016 17:03*

If you're looking to build, look up whether Gigabot has an open hardware version first, they're a 600mm machine. A giant machine just isn't easy do do from scratch, and scaling up a small design has many pitfalls too.


---
**Ryan Carlyle** *March 23, 2016 17:18*

One last comment on the belts -- CoreXY requires the same total belt length per usable size as a UM type cross gantry. It's just in two long runs instead of four shorter runs. And when you factor in the 45 degree kinematic rotation effect on the idler pulleys, you get a sqrt(2):1 = 1.41x "gear reduction" between belt motion and gantry motion inherent to the gantry design. So [double belt length / sqrt(2) mechanical advantage] = sqrt(2) times or 41% more nozzle motion from belt stretch than an equivalent-size UM gantry. That difference disappears entirely if you use additional belts or cables to synchronize sides rather than UM-style torsion rods. Which is basically mandatory at large size scales, if you don't want double the XY motors. Which means the CoreXY has minimal belt stretch disadvantage vs a cross gantry, if any.


---
**Miguel Barroso** *March 24, 2016 14:53*

For large printers, you may get better scaling with some kind of CNC structure. Have you checked the MPCNC? it has similarities with the UM cross Gantry,  it's quite cheap to build and has been used as a large scale 3D printer with sucess: [http://www.thingiverse.com/make:172500](http://www.thingiverse.com/make:172500) 

Check also this big comercial setup from 3DP, that looks like a CNC, and prints huge objects: 
{% include youtubePlayer.html id=ZbWSUPgKgTI %}
[https://www.youtube.com/watch?v=ZbWSUPgKgTI](https://www.youtube.com/watch?v=ZbWSUPgKgTI)


---
**ekaggrat singh kalsi** *March 25, 2016 01:40*

i think the choice of gantry comes down to availability of belts.. If 8mm belts and pulleys are hard to find then the only way is ulitmaker gantry with 6mm i guess. any body has links to good 8/10mm s2m/gt2/s3m belts and pulleys. the usual aliexpress stuff is horrible at this scale.


---
**Eric Lien** *March 25, 2016 02:07*

I have had great luck with everything I have purchased from Robotdigg. They have gt3: [http://www.robotdigg.com/product/597/3GT+9mm+wide+open+ended+belt](http://www.robotdigg.com/product/597/3GT+9mm+wide+open+ended+belt)


---
**Eric Lien** *March 25, 2016 02:11*

And GT2: [http://www.robotdigg.com/product/359/9mm+wide+open+end+GT2+belt](http://www.robotdigg.com/product/359/9mm+wide+open+end+GT2+belt)


---
**Ryan Carlyle** *March 25, 2016 02:13*

SDP-SI sells actual licensed Gates GT2/GT3 belts. Fiberglass reinforcement so stretch is negligible. They have a 2.4m 3mm pitch 9mm wide GT2 loop belt that you can cut open for a ~500x500mm usable build area, for example. 


---
*Imported from [Google+](https://plus.google.com/+ekaggrat/posts/bt8kXNYW9KK) &mdash; content and formatting may not be reliable*
