---
layout: post
title: "Been working on this Ingentis/Eustathios/Herculien Variant for some time now, and now that high school is coming to a close, I can start building it"
date: May 31, 2015 21:25
category: "Show and Tell"
author: Ishaan Gov
---
Been working on this Ingentis/Eustathios/Herculien Variant for some time now, and now that high school is coming to a close, I can start building it. 

Hats off to **+Eric Lien** for being an inspiration for the build

Shooting for an all-aluminum construction but still trying to keep the printhead light

Feel free to comment on things that need to be improved, would appreciate the feedback!



![images/4d1ab81a1a574afad928f0e7db16c9b6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4d1ab81a1a574afad928f0e7db16c9b6.jpeg)
![images/fba303ca335432425d5ffcf5ba31aba3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fba303ca335432425d5ffcf5ba31aba3.jpeg)
![images/0debaa0025b28072abc35f0c9f9ddaa1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0debaa0025b28072abc35f0c9f9ddaa1.jpeg)
![images/5988577b0362752bb5862a75ad662b9a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5988577b0362752bb5862a75ad662b9a.jpeg)
![images/36c337867bc9786cfa318d180456f270.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/36c337867bc9786cfa318d180456f270.jpeg)
![images/d84cf0bb50e920d383fd4be864a1a444.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d84cf0bb50e920d383fd4be864a1a444.jpeg)
![images/b5352bbd9b8496a1b70bc46708e41019.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b5352bbd9b8496a1b70bc46708e41019.jpeg)
![images/1b8c9de68053b114bfa8220f9a2079f2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1b8c9de68053b114bfa8220f9a2079f2.jpeg)
![images/bd990f1361c576b63636534d0204d720.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bd990f1361c576b63636534d0204d720.jpeg)
![images/4125fc6c8bf66529121c0b3f5db7b06d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4125fc6c8bf66529121c0b3f5db7b06d.jpeg)
![images/b99ef61cd01abf022bedced5964c88e4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b99ef61cd01abf022bedced5964c88e4.jpeg)
![images/b2065d5b10420f5eae14d3e983e87f89.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b2065d5b10420f5eae14d3e983e87f89.jpeg)
![images/bfda1f80db3abe98e293d14a7cdaed09.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bfda1f80db3abe98e293d14a7cdaed09.jpeg)
![images/97e0ce231d80fbc4c1afa43d87824264.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/97e0ce231d80fbc4c1afa43d87824264.jpeg)
![images/0c8b328537b6211cb1bf630f97dd7b53.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0c8b328537b6211cb1bf630f97dd7b53.jpeg)

**Ishaan Gov**

---
---
**Eric Lien** *May 31, 2015 22:18*

Love it.


---
**Mike Kelly (Mike Make)** *June 01, 2015 16:46*

Looks like a great design


---
**Brent ONeill** *June 02, 2015 20:31*

Really liking your aluminum components!


---
**Ishaan Gov** *June 02, 2015 20:43*

Thanks for the support, everyone!

I can release the source files and Marlin fork after I get everything up and running﻿


---
**Brent ONeill** *June 02, 2015 22:32*

Wondering if you would release some of the aluminum gantry component drawings early so I may have a closer look and provide some feedback. I'm very close to starting the machining processes for a Herculien Variant. Cheers and thanks for sharing!


---
**Ishaan Gov** *June 03, 2015 00:22*

**+Brent ONeill**​, definitely can do, I'll make some drawings in inventor of the printhead/gantry and I can upload the inventor files when I can get back to my computer


---
**Brent ONeill** *June 03, 2015 02:26*

**+Ishaan Gov** , excellent...looking forward to seeing your work in detail. I've been messing with the design of machined aluminum components in 6061...like machining butter, but without the urge to throw a few shrimp on the part ;)


---
*Imported from [Google+](https://plus.google.com/113675942849856761371/posts/fPm4NqVuJzZ) &mdash; content and formatting may not be reliable*
