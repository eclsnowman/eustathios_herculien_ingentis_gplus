---
layout: post
title: "Today during a quick test print I started noticing the typical smell of smoking electronics and had a quick look under my Eustathios only to see some sparks coming off the (bed heating) SSR connectors"
date: August 30, 2015 07:44
category: "Discussion"
author: Oliver Seiler
---
Today during a quick test print I started noticing the typical smell of smoking electronics and had a quick look under my Eustathios only to see some sparks coming off the (bed heating) SSR connectors. Turned out that one of the terminal screws on the load side had loosened and the cable barely touched the terminal, which started emitting tiny metal sparks. I was really surprised for this to happen as I remember tightening these screws quite strongly. I had used a DC SSR on 24V and luckily it survived the whole incident.

So, just a little heads up for everyone to check these screws are solid and maybe use some thread locker - obviously the vibrations during printing can get quite strong.





**Oliver Seiler**

---
---
**Miguel Sánchez** *August 30, 2015 07:49*

Any bad contact will show increased resistance and due to the current involved will heat up quickly. That is why higher voltage is better as it will require less current for the same amount of power (so you can use thinner wires too).



Make sure your screw terminal was not damaged because of the heat and replace it if it was.


---
**Stephen Baird** *August 30, 2015 08:11*

Are your wires crimped into ferrules, or bare into the screw terminal? 


---
**Oliver Seiler** *August 30, 2015 08:28*

The wires are soldered into ring terminal connectors. The issue was the screw that had loosened almost completely (was about to fall out).


---
**Eric Lien** *August 30, 2015 09:09*

Glad you are OK. Yeah regular checks of my electrical is key. I have kids in the house.


---
**Oliver Seiler** *August 30, 2015 09:14*

No worries, **+Eric Lien** the printer is located in my shed away from the house and I've got connected smoke detectors. But good reminder, especially if you let it run overnight or when not at home.


---
**Eric Moy** *August 31, 2015 14:17*

Since your design uses screw terminals, the thermal cycling can "pump"  the threads loose. Instead of just thread locker, try using some type of springed washer, ie, Belleville, wave, or even split lock. This will keep preload on your contact while the shaft of the screw elongates. Also bear in mind that heat makes loctite flow. Good luck


---
**Oliver Seiler** *August 31, 2015 19:34*

Good point **+Eric Moy**. My SSD sits on a large aluminium cooler and is somewhat overrated, so it does not really warm up during use, but I'll keep an eye on it.


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/hBk4tWWLCka) &mdash; content and formatting may not be reliable*
