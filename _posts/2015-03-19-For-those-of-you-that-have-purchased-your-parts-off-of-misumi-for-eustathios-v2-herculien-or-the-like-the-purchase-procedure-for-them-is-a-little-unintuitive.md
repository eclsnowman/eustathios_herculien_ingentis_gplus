---
layout: post
title: "For those of you that have purchased your parts off of misumi (for eustathios v2, herculien, or the like), the purchase procedure for them is a little unintuitive"
date: March 19, 2015 04:06
category: "Discussion"
author: Seth Messer
---
For those of you that have purchased your parts off of misumi (for eustathios v2, herculien, or the like), the purchase procedure for them is a little unintuitive. it wants to charge me freight, yet shipping costs show $0? i'm a little weary of unforeseen freight charges (the total for parts for eustathios v2, JUST on misumi is at $485.06 as it is!)



thoughts, suggestions, guidance, jeers?



thanks!

-seth





**Seth Messer**

---
---
**Eric Lien** *March 19, 2015 04:14*

They do prepay and add at time of shipping. The rates are good.


---
**Seth Messer** *March 19, 2015 04:16*

thanks **+Eric Lien** .. looks like only for UPS? (no biggy on FedEx vs. UPS, just interesting that prepay is for UPS only).


---
**James Rivera** *March 19, 2015 04:24*

I thought they were expensive. But the packaging was excellent. :-/


---
**Jeff DeMaagd** *March 19, 2015 04:42*

Misumi's site takes a lot of exploration and getting used to. For a product line that's largely custom make to order, it's very impressive.



Their shipping costs are pretty reasonable, though I do wish they offered postal service for smaller items.


---
**Gus Montoya** *March 19, 2015 05:04*

**+Seth Messer**  can you share your misumi order with me?


---
**Derek Schuetz** *March 19, 2015 05:23*

Shipping is $50-$60


---
**Eric Bessette** *March 19, 2015 05:32*

I got free shipping on an order of over $600.  Another order of about $100 (most nuts and bolts) had just over $9 in shipping.



I agree their shipping setup is not ideal.  I mean they know the weight of the order, so they should be able to calculate the shipping costs.  Or at minimum the maximum the shipping could cost.


---
**Seth Messer** *March 19, 2015 13:28*

**+Gus Montoya** [https://dl.dropboxusercontent.com/u/81794/misumi_eustathios_v2_parts.csv](https://dl.dropboxusercontent.com/u/81794/misumi_eustathios_v2_parts.csv)



i'm not sure how you are wanting it shared.. that's an export directly from their site from my saved cart/parts.


---
**Seth Messer** *March 19, 2015 14:03*

The order has been placed, it never asked me to pay the "prepay" on freight, so I have no idea if/when it asks. :/ I know you guys say "just do it", but it just feels strange ordering from them. Oh well. <b>fingers crossed</b>


---
**Gus Montoya** *March 19, 2015 15:45*

That's funny, when I inputed the V1 part numbers. It asked for freight and I got a final price. I guess something has changed. **+Seth Messer**  Can you email me your parts order to me? gus.montoya@gmail.com  Blur or erase any personal information. I don't need that.


---
**Seth Messer** *March 19, 2015 15:46*

**+Gus Montoya** that csv file i linked is exactly what exported out of my cart. i'm not sure what you're looking for other than that. misumi should let you import the csv.


---
**Gus Montoya** *March 19, 2015 15:47*

**+Seth Messer**  Sorry I didn't see it at first. But I have it now. Thank you :)


---
**Derek Schuetz** *March 19, 2015 15:48*

did you order all your parts cut or do you plan on cutting yourself? if you buy materials not cut to order you can pay shipping upfront because the system has weight already in the system and can calculate it. (this is based on my orders through misumi)


---
**Seth Messer** *March 19, 2015 15:50*

**+Gus Montoya** np! glad that's what you were after. **+Derek Schuetz** i ordered it per the part numbers in **+Eric Lien**'s V2 BOM and the part numbers looked to be set up to automatically set the mm length for the extrusions. so i'm assuming they are cutting them to that length. i'm sure i'll still have to tap the holes for screws on the face of the extrusions.


---
**Eric Lien** *March 19, 2015 16:19*

**+Seth Messer** they are already tapped.


---
**Chris Brent** *March 19, 2015 16:22*

It would be so nice if Misumi had a BOM feature where you could just share a part number for an entire machine. They're so close with the way they let you create part numbers for custom cuts and drill holes that it's a little frustrating that this doesn't exist.


---
**Jeff DeMaagd** *March 19, 2015 16:24*

Yeah. If you can get to the "order slip" page, you can just copy in the part numbers & add quantities, row by row. Not much more to paste in a long list of part number & quantities in one shot.


---
**Gus Montoya** *March 19, 2015 16:27*

Yeah doing that now. 


---
**Eric Lien** *March 19, 2015 16:40*

Anybody want to update the V2 BOM with pricing ;) I have been swapped at work and to be honest I need to start catching up on some sleep.﻿



For nuts and bolts I get what I can from [http://www.trimcraftaviationrc.com/](http://www.trimcraftaviationrc.com/)



The prices are a lot better and also all stainless.


---
**Seth Messer** *March 19, 2015 16:40*

ended up being $42.60 for shipping/freight. not bad at all.


---
**Seth Messer** *March 19, 2015 16:41*

yeah.. i can do that this weekend for the misumi orders. my only concern is, changes to BOM will not show up like normal code diffs. e.g. folks won't know what changed in the BOM unless they give it a good scrutinization. excel docs will just be diffed as binary files. :/


---
**Eric Lien** *March 19, 2015 16:47*

**+Seth Messer** you are a code guy. Do you think I should convert it to another format? Like google docs maybe? That at least has versioning.


---
**Seth Messer** *March 19, 2015 16:48*

**+Eric Lien** yeah, google docs would be better. something more geared towards versioning of binary data.


---
**Gus Montoya** *March 19, 2015 16:56*

Finished my purchase, lets see what I get. Crossing fingers. 


---
**James Rivera** *March 19, 2015 22:50*

I second **+Eric Lien**'s recommendation for  [http://www.trimcraftaviationrc.com](http://www.trimcraftaviationrc.com). I bought a bunch of nuts and bolts there and am happy with my purchase.


---
**Eric Lien** *March 19, 2015 23:45*

**+James Rivera** yeah I bought ones I didn't even need cause the price was so low :)


---
**Seth Messer** *March 19, 2015 23:46*

so did i pay exponentially too high by using misumi for the ones marked as available at misumi? e.g. 485.xx before the 42.xx freight charge. is that about expected **+Eric Lien** ?


---
**Eric Lien** *March 20, 2015 01:15*

**+Seth Messer** misumi bolts? I don't follow. If you mean for the extrusion I think your total is about right.


---
**Seth Messer** *March 20, 2015 01:24*

**+Eric Lien** i was referring to everything marked with "misumi" on the V2 BOM .. e.g., extrusions, shafts, nuts, etc. that's good then if 400-500 sounds about right (pre-shipping).


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/WqcDRiRxvkK) &mdash; content and formatting may not be reliable*
