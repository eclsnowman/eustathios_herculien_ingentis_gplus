---
layout: post
title: "I am building the Herculien but thinking of changing the Z-axis and platform using the Eustathios design, anyone do this?"
date: May 07, 2015 22:29
category: "Discussion"
author: Vic Catalasan
---
I am building the Herculien but thinking of changing the Z-axis and platform using the Eustathios design, anyone do this? 





**Vic Catalasan**

---
---
**Eric Lien** *May 07, 2015 23:32*

I don't think so yet. But there is no reason it couldn't work. But the bed is larger, so you will have more cantilever forces on the bed toward the edges.


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/iXZDsr5LbaE) &mdash; content and formatting may not be reliable*
