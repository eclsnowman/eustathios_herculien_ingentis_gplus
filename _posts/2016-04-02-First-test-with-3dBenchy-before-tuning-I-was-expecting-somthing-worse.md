---
layout: post
title: "First test with 3dBenchy before tuning, I was expecting somthing worse"
date: April 02, 2016 21:00
category: "Build Logs"
author: Maxime Favre
---
First test with 3dBenchy before tuning, I was expecting somthing worse.

Retraction need some work.



There's still few things to do:

- plasma cut panels

- real spool holder (better than just a ghetto screw)

- check perpendicularity

- flying extruder

- ... 



![images/fe53d7c624bf76104fdf0cf824ab7f5e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fe53d7c624bf76104fdf0cf824ab7f5e.jpeg)
![images/0664aa76aff336822e415c603566156f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0664aa76aff336822e415c603566156f.jpeg)
![images/d3a7bed160c41063270653d72b7560be.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d3a7bed160c41063270653d72b7560be.jpeg)
![images/97bc8e95a498282c737ed4d9827f4ba5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/97bc8e95a498282c737ed4d9827f4ba5.jpeg)
![images/9ed59ef14de793ca04a2d636abb5d421.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9ed59ef14de793ca04a2d636abb5d421.jpeg)

**Maxime Favre**

---
---
**Eric Lien** *April 02, 2016 22:21*

Looking great, I love the USB port moved up front.


---
**James Rivera** *April 03, 2016 01:37*

(looks at this printer and 1st try Benchy boat, then looks at own printer) (cries)  ;)


---
**Zane Baird** *April 03, 2016 02:15*

Looks like one beautiful printer. I can only hope that one day I will be able to achieve that kind of perfection in my wire management.


---
**Maxime Favre** *April 03, 2016 12:33*

**+Zane Baird** It's professional deformation ;) It's really not that hard, just take some time thinking of wire routing. You can do great things using simple cable ties.


---
**Roland Barenbrug** *April 03, 2016 21:02*

Wow, looks great. What did you use as headbed? Still need to decide for my own on the heatbed (sandwich)


---
**Maxime Favre** *April 04, 2016 05:54*

Thanks ! Regarding the bed, from the bottom: heatpad 300*300 230v 500w from robotdigg (it dont have the 468mp tape so you should go with the aliexpress one linked in BOM), 6mm alu plate plasma cut and for now a ghetto 300x300 Ikea mirror until I find better. I will probably switch for a boro glass and PEI sheet but I failed to find a correct size glass.




---
**Hakan Evirgen** *April 07, 2016 12:41*

Smoothieboard and Viki2 LCD. Did you connect it in a way that you can use all features of Viki2? I had to deactivate onboard LEDs in order to do that.


---
**Maxime Favre** *April 07, 2016 13:12*

**+Hakan Evirgen**​ viki2 need 2 more pins than smoothieboard spares. I didn't usé the buzzer and used the pin of a spare MOSFET for the seco﻿nd led




---
**Hakan Evirgen** *April 07, 2016 14:41*

**+Maxime Favre** when you disable onboard LEDs then you have enough pins available. I am using all MOSFETS as well as all features of Viki2 in that way.


---
**Roland Barenbrug** *April 10, 2016 20:55*

**+Maxime Favre**, thnx for clarifying this. Feel not completely confident with a 230V heatpad. What do you use to switch the heatpad on/off. Solid State Switch?


---
**Maxime Favre** *April 11, 2016 04:33*

**+Roland Barenbrug** yep


---
**Daniel F** *April 11, 2016 08:19*

Good to see one more Eustathios, especially one from CH. Didn't noticed you're from Switzerland at first sight althoug I was impressed with your wiring--indeed it looks very swiss. I had to make a break with my build but I plan to continue soon--you set the level quite high if I look at your test print.


---
**Maxime Favre** *April 11, 2016 08:36*

**+Daniel F** Thanks ! We're are you from ? **+Roland Barenbrug** regarding 230VAC heater with the SSR is fine, just don't use a relay, it will not last long with the PWM. With 230V going further in the printer than just the power supply, I highly recommand to make sure the structure and bed support are correctly grounded in case of failure. On mine I still need to add a ground wire to the bed support.


---
**Roland Barenbrug** *April 11, 2016 09:41*

**+Maxime Favre** . Thnx, would be my idea too.


---
**Daniel F** *April 11, 2016 10:59*

**+Maxime Favre** Bern

  **+Roland Barenbrug** OMRON G3NA-210B or 220B are good choices, look for genunine ones (can be found used on ebay), they run cool. Ground wire to the build plate and frame is a good idea--you don't want the phase on the frame in case something goes wrong with the slicone pad


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/dzifpH3qPs7) &mdash; content and formatting may not be reliable*
