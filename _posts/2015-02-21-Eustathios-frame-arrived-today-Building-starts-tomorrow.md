---
layout: post
title: "Eustathios frame arrived today. Building starts tomorrow :)"
date: February 21, 2015 06:04
category: "Show and Tell"
author: Isaac Arciaga
---
Eustathios frame arrived today. Building starts tomorrow :)

![images/fcfdfc793b3ccb63f4a6e4e4baaaa987.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fcfdfc793b3ccb63f4a6e4e4baaaa987.jpeg)



**Isaac Arciaga**

---
---
**James Rivera** *February 21, 2015 06:34*

Did you order it pre-drilled & tapped?!? If so, what are the magic part numbers? I would have already done it if Misumi didn't make it so difficult!


---
**Eric Lien** *February 21, 2015 06:48*

The part numbers listed in Jason's BOM are predrilled. But if you want screw on feet you need to tap them. You can also print some feet that slide over the extrusion.


---
**Isaac Arciaga** *February 21, 2015 06:52*

These are actually vslot (OpenBuilds) extrusions. A friend of mine did the cutting, tapping, drilling for me with precision and accuracy. I'm very happy.


---
**Isaac Arciaga** *February 21, 2015 07:54*

**+Alex Lee** that's the kitchen table!!


---
**Eric Lien** *February 21, 2015 14:21*

Take pictures of your process


---
**Dat Chu** *February 21, 2015 14:45*

Still need my extrusion done. 😯


---
**Isaac Arciaga** *February 21, 2015 17:40*

I'll do my best to gather photos of the build. I'm going to have to use the t-nuts from OpenBuilds for this. The t-nuts and post install t-nuts I intended to use are still in China :(. I'm going to have to pre-load the extrusions and pray I don't miss one.


---
**James Rivera** *February 21, 2015 18:14*

**+Isaac Arciaga** if they already left China (as in, before the Chinese Lunar New Year, which is like Christmas & Thanksgiving rolled up and injected with steroids) then they might just be stuck in transit. There is a strike at the shipping ports in California.


---
**Eric Lien** *February 21, 2015 18:14*

**+Isaac Arciaga** yup. I would put money on you missing one, or one too many. But please prove me wrong ;) ... I have had mine apart a lot due to this


---
**Isaac Arciaga** *February 21, 2015 19:55*

**+Eric Lien** you are right. I will miss one. I think I'll just go ahead and purchase post install t-nuts from Misumi today and do the entire build with them. I'm already over budget anyways :)



Using decent post install t nuts for the entire build might be worth considering in the BoM to save a lot of time and grief.


---
**Eric Lien** *February 21, 2015 20:29*

**+Isaac Arciaga** I like the strength of standard t-nuts better. I will try to make assembly prints with the locations, and also include some spare drop in nuts in the BOM for sanity sake.


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/GydvQ35GERj) &mdash; content and formatting may not be reliable*
