---
layout: post
title: "Hello. Now that my eustathios is my main printer for my 3D Hub, I want to go much faster than 60mm/s"
date: June 04, 2018 19:00
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hello. Now that my eustathios is my main printer for my 3D Hub, I want to go much faster than 60mm/s.



What are your thoughts on carbon cross rods? Thinking square with pulleys or round with igus? 



Will make super light carbon 3d printed carriage. Walter's is too heavy for 80-100mm/s



Already have a bond-tech with slightly faster gearing.





**Stefano Pagani (Stef_FPV)**

---
---
**Zane Baird** *June 04, 2018 21:13*

My worry would be the straightness of the carbon rods. The rods in the BOM are spec'd to L/100x0.01mm (where L is the length of the rod). Looking up carbon rods I found one spec at 2mm/1000mm which going to cause a lot of binding. 



One option to increase the max speed would be to use 9mm wide belts so they exhibited less stretching during fast accel. 



If you want to save a couple grams you could also use the Mosquito hotend ([https://www.sliceengineering.com/shop/mosquito-hot-end](https://www.sliceengineering.com/shop/mosquito-hot-end)) but I haven't heard any wide adoption yet as it was only debuted at MRRF this year

[sliceengineering.com - PREORDER - Mosquito Hot End](https://www.sliceengineering.com/shop/mosquito-hot-end)


---
**Dennis P** *June 04, 2018 22:32*

FYI: From another list source, the Mosquito's are delayed, again. 

What material are you printing? Perhaps the Merlin will do what you need? 

Speed: have you tried any benchmarks to see if it will make a measurable difference in time? If you are making lots of 1-2 smaller parts it may not be worth it. If you are making any quantity of 2-12 hour parts, yes, go for it. , 

[reprap.org - Merlin Hotend - RepRap](https://reprap.org/wiki/Merlin_Hotend) 


---
**Stefano Pagani (Stef_FPV)** *June 05, 2018 12:56*

**+Dennis P** I am printing a ton of parts. Usually 5-20 per job



**+Zane Baird** that’s a great idea with the belts. I will probably try that with a lighter carriage for now.



I see what you are talking about with the rods. I think square with pulleys won’t bind.



There’s a rep rap out there with carbon rods just forgot the name :(


---
**John Gadbois** *June 08, 2018 20:54*

What issues are you seeing when you run faster than 60mm/s?


---
**Stefano Pagani (Stef_FPV)** *June 08, 2018 20:55*

**+John Gadbois** just less accurate, ringing etc. also kinda want to build the fastest eusthstios :p


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/SjSryhwB8Vf) &mdash; content and formatting may not be reliable*
