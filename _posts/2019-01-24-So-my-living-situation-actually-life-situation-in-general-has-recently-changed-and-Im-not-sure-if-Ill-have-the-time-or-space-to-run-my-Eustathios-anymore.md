---
layout: post
title: "So my living situation, actually life situation in general has recently changed and I'm not sure if I'll have the time or space to run my Eustathios anymore"
date: January 24, 2019 20:09
category: "Discussion"
author: Ryan Fiske
---
So my living situation, actually life situation in general has recently changed and I'm not sure if I'll have the time or space to run my Eustathios anymore. If there's anyone in the local area that would be interested, I'd be willing to let it go for cheap. Haven't thought much on price yet. I'll try to grab a picture of it soon but it does work quite well. 

![images/f767d908dda41c2463112b24dd35253f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f767d908dda41c2463112b24dd35253f.jpeg)



**Ryan Fiske**

---
---
**Stefano Pagani (Stef_FPV)** *January 24, 2019 22:19*

hey, I am in Mass I would be interested!


---
**ThantiK** *January 25, 2019 01:41*

Life caught up to me and I was never able to finish building mine, lol.


---
**Eric Lien** *January 25, 2019 01:49*

Sorry to hear you need to let it go. But I hope it finds a happy home.


---
**Ryan Fiske** *January 25, 2019 17:01*

**+Stefano Pagani** it would think shipping would be cost prohibitive, but we could definitely take a look!


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/fKbSoZRbZwV) &mdash; content and formatting may not be reliable*
