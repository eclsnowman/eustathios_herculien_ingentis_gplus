---
layout: post
title: "Is anyone willing to accept payment for assistance in getting my printer to work?"
date: October 15, 2015 00:55
category: "Discussion"
author: Gus Montoya
---
Is anyone willing to accept payment for assistance in getting my printer to work? I have skype and laptop with a camera. My issue is software in nature. Maybe a few mechanical issues but now I do not know. You must have a smoothie board 5XC working (preferably with a Vicki V2 LCD).





**Gus Montoya**

---
---
**Zane Baird** *October 15, 2015 01:14*

**+Gus Montoya**, no payment necessary or accepted, but I'd be more than willing to help. I don't have an LCD running currently, but I'll help in any way I can


---
**nick durrett** *October 15, 2015 01:35*

Good on ya **+Zane Baird**​! It's awesome seeing people so willing to help one another


---
**Zane Baird** *October 15, 2015 01:37*

**+nick durrett** the only way I learned anything about 3D printing was through forums and user groups such as this. I feel I have a duty to pass on what I've learned after what everyone here has taught me.


---
**Gus Montoya** *October 15, 2015 03:38*

@Zane Baird, I will communicate with you tomorrow and share all I have. I work grave yard in an ER.  You don't know how much this help means to me.


---
**Zane Baird** *October 15, 2015 04:17*

**+Gus Montoya** The necessary info in order: 1) A picture of your smoothieboard with connections, labeled to the best of your ability; 2) your config file as is;  3) a description of the problems you are having thus far; 4(  host software (pronterface, repetier, etc.). Message me a link to a .zip or a cloud folder with all of this is the best option


---
**Mutley3D** *October 15, 2015 04:59*

**+Gus Montoya** in bullet points, what are the issues you think you are having?


---
**Chris Brent** *October 16, 2015 16:10*

Do a Google Hangout! Easier for people to pop and and help where they can.


---
**Gus Montoya** *October 27, 2015 05:13*

**+Mutley3D**   

* I am not sure of my wiring for the smoothie board

      - my print bed warmer pad is not heating

      - my hot end is not getting hot

      - not sure if my thermestat is working but then again nothing is heating         up.

      - the motor are wired up but not moving per the manual commands.             (ex: if I tell Y to move on repetier X moves.) 

     - the fans are not working.

* Viki 2.0 LCD is not working. I am unable to finish the wiring to the smoothie board.

* My config file is not correct, I am hoping that someone might have a config file that will get me started in the correct direction.


---
**Mutley3D** *October 27, 2015 05:34*

**+Gus Montoya** it sounds like all your primary issues are related to your control electronics (smoothie). The best advice i can offer at this point is two fold. 1) Take a look at the smoothie website if you havent already (im sure you have) but look at it again. Also get your voltmeter out and check you are getting main power to the smoothie. If you breath on or hold onto, or blow a hairdryer over the thermistors, you should be able to see a temp change occur) and also motors wont run without main power (hence why i suggest this is the first thing you check) and then after that...2) jump into the  #smoothie  channel on IRC and the guys (including the original devs) in there will be the best available to advise and walk you through on a live step by step basis to get you up and running or identify the faults. To me it sounds like you have a single issue that is causing all the above - ie a power outage


---
**Mutley3D** *October 27, 2015 05:37*

Ah i re-read and see you have motor movement. Its a pretty basic and safe assumption that if X motor moves when you command the Y motor to move, you may well have the wires in the wrong connector :) Im also not sure if you need to "home" the motor before it will operate correctly. Sounds like you do have some power into the smoothie afterall, so id jup straight to point 2 and head for the IRC channel.


---
**Gus Montoya** *October 27, 2015 06:03*

irc?


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/23z3D6pwGTW) &mdash; content and formatting may not be reliable*
