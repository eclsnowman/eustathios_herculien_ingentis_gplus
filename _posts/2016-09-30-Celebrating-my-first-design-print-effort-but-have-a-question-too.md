---
layout: post
title: "Celebrating my first design /print effort, but have a question too"
date: September 30, 2016 13:47
category: "Discussion"
author: Bruce Lunde
---
Celebrating my first design /print effort, but have a question too. I designed this meter mount with openSCAD, ran it through slic3r, and printed on my HercuLien in one session. Happy with the smoothieboard too, and the new web interface.



What is the advice of the group on feed issues.  Not sure why, but as I am learning to design parts and slice and print, I have to watch my filiment constantly, as it seems to have a tendency to get tied up as it unrolls. I had to free two snags during my print last night to "save" it, as i could hear the crunching of the filiment when it got stuck.



![images/67fbb047318c2f6c81154ec1b4682445.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/67fbb047318c2f6c81154ec1b4682445.gif)
![images/b4005500096e5d51638c95115ff995f1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b4005500096e5d51638c95115ff995f1.jpeg)
![images/0c3b61a51abbb9ecf45fb4cc01fc1469.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0c3b61a51abbb9ecf45fb4cc01fc1469.jpeg)
![images/76dcb3e8bd7a2b7c96f8f41519efe98b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/76dcb3e8bd7a2b7c96f8f41519efe98b.jpeg)

**Bruce Lunde**

---
---
**Eric Lien** *September 30, 2016 14:30*

Is the filament tangled on the roll? Or is the extruder jamming? Or is the roll not rolling freely on the filament spool holder?



That info will help us with troubleshooting.


---
**Bruce Lunde** *September 30, 2016 14:35*

**+Eric Lien** It tangles on the roll, almost like it somehow gets under other wraps but has never been un-spooled.  I guess I need to order another roll to compare too. Extruder works fine until it hits a snag, but then starts eating the part that is under the feed gear.




---
**Eric Lien** *September 30, 2016 15:36*

**+Bruce Lunde** yeah, sounds like a bad spool. I forget, are you using the HercuStruder, or did you upgrade to the Bondtech? The HercuStruder is good... But if you want the best I would suggest the Bondtech QR as a future upgrade. I think **+Martin Bondéus**​ has an upgrade kit since you already have the motor, you would just need the gears/hardware and the housing. I printed my own housings for a long time, but recently got the SLA nylon housings Martin provides. They are a thing of beauty... And far more accurate and rigid than FDM ABS parts.


---
**Bruce Lunde** *September 30, 2016 18:23*

**+Eric Lien** Yes HercuStruder with a volcano hot end. I will look into the BondTech QR,  Also thinking about a second extruder, so I would have a large volume hotend (volcano) with1.2 mm , and a finer hotend  like a .3 always available.


---
**Bruce Lunde** *October 01, 2016 15:59*

Successfully completed this print and mounted the meter. So much fun to have a working printer!  Thanks again to all for helping out a newbie!



![images/147ab1df9854cb86be40251b69457f03.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/147ab1df9854cb86be40251b69457f03.jpeg)


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/TqRfdy2h6CN) &mdash; content and formatting may not be reliable*
