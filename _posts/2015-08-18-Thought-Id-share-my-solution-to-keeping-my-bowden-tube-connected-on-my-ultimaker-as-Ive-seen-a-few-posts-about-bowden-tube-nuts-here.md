---
layout: post
title: "Thought I'd share my solution to keeping my bowden tube connected on my ultimaker, as I've seen a few posts about bowden tube nuts here"
date: August 18, 2015 11:37
category: "Show and Tell"
author: Jeff Heath
---
Thought I'd share my solution to keeping my bowden tube connected on my ultimaker, as I've seen a few posts about bowden tube nuts here.  Instead of threading the outside, I heated up the end of my bowden tube,shoved it over a pair of needle nose pliers to expand it, then threaded it over the top of a threaded PEEK tube I machined to replace all the push fittings and clips in the hotend assembly.  I've been running it this way for the past 2+ years and it hasn't come off once.  Just thought it might help someone else keep their bowden in place.

![images/1d9fc2ba737b26b92a63890cc4c75118.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1d9fc2ba737b26b92a63890cc4c75118.jpeg)



**Jeff Heath**

---


---
*Imported from [Google+](https://plus.google.com/105415486539145588112/posts/UmHhphAZ6vc) &mdash; content and formatting may not be reliable*
