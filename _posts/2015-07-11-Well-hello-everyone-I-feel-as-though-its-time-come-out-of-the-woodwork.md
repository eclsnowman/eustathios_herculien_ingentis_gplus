---
layout: post
title: "Well hello everyone, I feel as though it's time come out of the woodwork"
date: July 11, 2015 01:08
category: "Discussion"
author: Tim Pizzino
---
Well hello everyone, I feel as though it's time come out of the woodwork. I've been lurking for quite some time following everyone's build while I myself gathered the parts for my own build. Now that I'm finally nearing the completion of my printer,  I'm looking forward to interacting with the community while printing out some functional projects once it's done. 



I was also pushed to post since I feel as though I've run into my first real question. Namely, after my initial assembly I ran the break in code and I am getting some vibrations on the carriage that I feel are a bit extreme even for first assembly. I have since found a fundamental error in my initial buildup that has the printer in pieces again but from what I've read so far, the alignment of the linear rods must have been off. However what I haven't been able to find is a method for ensuring that they are square to each other. I should have things together again this weekend and will give it another go but it would appreciated if anyone had some tips. 



![images/66e22341c5229610b108285989084da0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/66e22341c5229610b108285989084da0.jpeg)
![images/d09b9dd16867223ef3b9da98b213e603.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d09b9dd16867223ef3b9da98b213e603.jpeg)

**Tim Pizzino**

---
---
**Eric Lien** *July 11, 2015 01:48*

The first crucial part is good printed parts and a squared frame ( check the frame closely, with a square in each plane of each corner, then measure corner to corner. Once that's done you can really tighten down the frame and begin with the gantry. 



For the gantry make sure the side and center carriages move smooth on the rod by themselves. If not you are out of alignment between the two self aligning bushings. Once all parts move freely by themselves its time to move on the assembly. 



You probably know by now it is easiest to assemble the center cross bar and carriage assembly first, then slide the side rods in from the outside of the frame into the side carriages which are already on the center cross rods (put the gt2 belt in to the side carriages now too to save some headache before the frame is in the way). Don't forget the 10mm shim washers between the side pulleys and the corner radial bearings ( this is a common mistake). 



Now complete the circle on the side gt2 open loop belts back to the belt tensioner. Put light tension on the belts. The pulleys at the corners should still be loose on the side shafts which means each end of the cross rods can move in dependant of each other. Put a spacer between on of the side rods and a the cross rod (anything will work just make sure it is the same at each end if the cross rod). Tighten the corner pulleys for the two belts connected to that cross rod. And boom... The belt now holds the cross rod parallel to that side rod. Do the same for the other cross rod and you should be squared and paralleled.



Hope that helps. ﻿


---
**Eric Lien** *July 11, 2015 01:52*

Also make sure the side shafts are not rubbing on the frame with the end of the rods, and that the side corner pulleys are pressed tight into the shim, and the shim tight into the corner radian bearings.


---
**Eric Lien** *July 11, 2015 01:56*

Also make sure the table the frame sits in is flat. If you have binding try a shim under one of the corners of your frame, see if it helps. Try all four corners. If one of the corners having a shim helps... Then you might want to think about leveling feet for your frame so the table doesn't put a torque into the frame as it sits on the uneven table.﻿


---
**Tim Pizzino** *July 11, 2015 04:15*

Wow, this is great Eric. I appreciate it, and will use this during my reassembly this weekend. I'll report back with results. Thanks! 


---
**Eric Lien** *July 11, 2015 04:36*

**+Tim Pizzino** no problem. Glad to help. And once you have things moving I would love to see the follow-up posts.


---
**Vic Catalasan** *July 11, 2015 07:49*

This is better than tech support and he did not even sell you a 3D printer! 


---
**Eric Lien** *July 11, 2015 11:28*

**+Vic Catalasan** thanks. I am hoping once there are enough of us out there the tech support will be self sustaining. Until then I know my fair share.... I helps that I got things wrong bunches of times before I got it right.


---
**Daniel Salinas** *July 14, 2015 14:44*

Alignment was by far the most difficult thing to get right on my herculien build. I had pretty bad chatter in my XY gantry and it turned out one of my XY pulleys was not tight enough and it pulled out of alignment really bad. 


---
**Tim Pizzino** *July 14, 2015 16:02*

**+Daniel Salinas** Good to know, I'll add tightening the pulley set screws to the checklist. I definitely underestimated the amount of precision required when assembling the bushings and gantry assembly as a whole. Second time around though things are looking much better and I hope to have the motors back on for testing by the end of the week!


---
*Imported from [Google+](https://plus.google.com/102199124418837682786/posts/cN1u5hD3uzi) &mdash; content and formatting may not be reliable*
