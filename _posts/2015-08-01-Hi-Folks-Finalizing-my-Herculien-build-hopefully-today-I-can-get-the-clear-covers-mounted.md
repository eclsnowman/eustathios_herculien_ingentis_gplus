---
layout: post
title: "Hi Folks, Finalizing my Herculien build, hopefully today I can get the clear covers mounted"
date: August 01, 2015 00:52
category: "Show and Tell"
author: Vic Catalasan
---
Hi Folks, Finalizing my Herculien build, hopefully today I can get the clear covers mounted. Here is the Azteeg X3 box, front Viki 2 mount on my Herculien. Printing now using a nylon version of the spool holder. Nylon is so nice and strong, I kept breaking them with PLA and ABS prints. I could not find the 12 mm and 25 mm holder so I modified the 50mm version and uploaded them to [youmagine.com](http://youmagine.com).  As you can see my Bondtech is pulling hard on that spool  netting and no signs of giving up. I think I may have to make another printer part so it can replace that blue tape.  Also I am redoing the power receptacle since the power supply will be mounted outside the box since I have opted not to cut any of the acrylic covers.

![images/0a2dfeb0bd1dbe0c787482258cee04db.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0a2dfeb0bd1dbe0c787482258cee04db.jpeg)



**Vic Catalasan**

---
---
**Vic Catalasan** *August 01, 2015 01:38*

I am thinking about replacing the carriage with a single Eustathios V2 carriage, do you guys think a nylon bridge filament can handle the heat or should I stick with ABS? 


---
**Eric Lien** *August 01, 2015 02:05*

Nylon should work fine. Love the build an enclosure. Great job all around.


---
**Frank “Helmi” Helmschrott** *August 01, 2015 19:38*

My Carriage is completely printed from HDGlass (PETG) - including the Fan duct. Works well until now, so Nylon should work even better.


---
**Vic Catalasan** *August 01, 2015 20:55*

Thanks guys will make with nylon. 


---
**Vic Catalasan** *August 05, 2015 18:52*

Well the top portion printed nice with nylon but that bottom air blower is too hard to print with nylon. It basically catches itself on the inner wall of the blower because of softness of nylon when printing. I am going to reprint with ABS and also will print the V4 hot end clamp version 


---
**Vic Catalasan** *August 05, 2015 18:58*

**+Eric Lien** I think we need to make the holes on the bushing bearing a bit larger. The average folks will have a hard time pushing the bearings through. I have plenty of tools in my shop but for those who do not will have a tough time.


---
**Eric Lien** *August 05, 2015 20:10*

**+Vic Catalasan** Do you mean on the carriage? If so I have them under sized .1mm so I can soften the ABS hole with acetone on a q-tip and press in the bushing for a firm fit. The ABS then re-solidifies making a perfect fit and the bushing then is essentially glued in.



Have you calibrated your extrusion multiplier for that spool via the single perimeter cube method yet? If not many people over extrude and parts will tend to be overstuffed causing inside diameters to come in undersized. To do this print a single wall 20x20 cube with 1 or two bottoms, 1 perimeter, no infill, and no tops. Then measure your wall width once the part cools. Compare that to your extrusion width in the slicer. For example I always tell the slicer my filament is 1.75mm exactly, but I print several of these cubes dialing it in, then write this extrusion multiplier on the side of the spool.



Also have you calibrated your shrink factor for the plastic yet. Many people forget this step, but thermoplastics will see contraction from printing temp down to ambient. For PLA this value is low, but for Nylons and ABS this value is much higher. I apply a shrink factor of 1.006 to all ABS parts because I have found this to result in dimensionally accurate parts with 95% of the ABS brand I use. This takes a while to find out for materials, and is also geometry dependant, infill dependant, perimeter dependant, etc. So it is a bit of trial and error to find values that work for you.



This contraction is the source of what most people call warp... but is actually the part contracting towards center-line. The outside edges cool sooner than the main thermal mass, so the outside shrinks before the inside. When this happens the bottom on the build plate tends to crown. Things like brim and hold down pads try to fight this mechanically, and heated beds and enclosures try to fight this thermodynamicaly by keeping things a more similar temp and allowing them to cool uniformly once the print completes. 


---
**Vic Catalasan** *August 06, 2015 06:55*

**+Eric Lien** Thanks for those tips, will try the calibration test. Will definitely try the 1.006 scale factor.



 


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/iaDMbVN2xhe) &mdash; content and formatting may not be reliable*
