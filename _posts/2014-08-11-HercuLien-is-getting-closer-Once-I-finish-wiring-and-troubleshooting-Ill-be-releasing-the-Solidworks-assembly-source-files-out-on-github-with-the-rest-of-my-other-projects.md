---
layout: post
title: "HercuLien is getting closer. Once I finish wiring and troubleshooting I'll be releasing the Solidworks assembly source files out on github with the rest of my other projects"
date: August 11, 2014 15:10
category: "Deviations from Norm"
author: Eric Lien
---
HercuLien is getting closer. Once I finish wiring and troubleshooting I'll be releasing the Solidworks assembly source files out on github with the rest of my other projects.

![images/f388367c2a6a61c6e7ebd6e43e9ec966.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f388367c2a6a61c6e7ebd6e43e9ec966.jpeg)



**Eric Lien**

---
---
**Ricardo de Sena** *August 11, 2014 15:36*

Very good!!!


---
**Maxim Melcher** *August 11, 2014 15:44*

Colosseum! ;)


---
**Eric Lien** *August 11, 2014 17:44*

**+Shauki Bagdadi** I will send them both to the octagon to see who reigns supreme.


---
**Eric Lien** *August 15, 2014 17:07*

**+Shauki Bagdadi** I see your point. But in my experience I have seen no I'll effects.


---
**Bruce Wattendorf** *August 24, 2014 19:12*

Eric, Can I get a copy of the E3D mount? I am looking to try it on my Ultimaker since I have 8mm cross rods.  


---
**Eric Lien** *August 24, 2014 20:56*

**+Bruce Wattendorf** yes. I will be releasing all the files soon.


---
**Bruce Wattendorf** *August 24, 2014 22:09*

Sweet thanks!! Eric

Have you thought about adding a inductive prox sensor for the Z height??


---
**Eric Lien** *August 24, 2014 22:46*

**+Bruce Wattendorf** to be honest I have never seen the need. I level my bed every 4-6 weeks. My take is a solid build makes auto leveling unnecessary.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/KFJAaVKaWHq) &mdash; content and formatting may not be reliable*
