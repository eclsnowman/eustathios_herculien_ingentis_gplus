---
layout: post
title: "Well, my pile is starting to grow!"
date: October 28, 2018 00:04
category: "Build Logs"
author: William Rilk
---
Well, my pile is starting to grow!  I'll be putting my order in to misumi in a couple weeks.  Trying to scratch all the big ticket items off the list right now.

![images/e1705d9245f6b8b0f49b4d5ba9f2c5e5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e1705d9245f6b8b0f49b4d5ba9f2c5e5.jpeg)



**William Rilk**

---
---
**Eric Lien** *October 29, 2018 03:35*

Glad to hear you have everything coming together. Please remember to post progress pictures along the way. And ask any questions you run into. I know G+ is winding down... But we still have an active community for now.


---
*Imported from [Google+](https://plus.google.com/100191047182984055447/posts/K2jrRaPZfjh) &mdash; content and formatting may not be reliable*
