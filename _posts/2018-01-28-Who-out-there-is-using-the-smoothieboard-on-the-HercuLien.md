---
layout: post
title: "Who out there is using the smoothieboard on the HercuLien?"
date: January 28, 2018 23:19
category: "Discussion"
author: Bruce Lunde
---
Who out there is using the smoothieboard on the HercuLien?  I worked with **+Dennis P** and have made several solid steps forward, but somewhere I am still missing something with the Z height.  I looked back in the discussion history for the proper calculations, and tried twice with the Triffid Hunter Calibration Guide,  and tried the setting for gamma with the results, but I still cannot get it right, so I am asking what your setting is for gamma:



alpha_steps_per_mm                           80               # Steps per mm for alpha ( X ) stepper

beta_steps_per_mm                            80               # Steps per mm for beta ( Y ) stepper

gamma_steps_per_mm                           3200             # Steps per mm for gamma ( Z ) stepper



This is my last setting, but it is wrong. I doubled the 1600 because initially it seemed to be half the proper height.  I may have calculated it wrong too. Each time I calculated I tried a print, but all have not gotten me to 50mm on the test cube for the height.



I do have the standard BOM parts, 12 mm lead screws (2mm pitch), with 36 tooth on them, and the 20 tooth on the stepper motor. Standard motor with 1.8 degree step as well. 







**Bruce Lunde**

---
---
**wes jackson** *January 28, 2018 23:48*

[https://www.prusaprinters.org/calculator/#stepspermmlead](https://www.prusaprinters.org/calculator/#stepspermmlead)



Great way to get the right ammount here


---
**wes jackson** *January 28, 2018 23:50*

According to the calculator, 3200 is correct :/


---
**Eric Lien** *January 29, 2018 01:22*

It all depends on what microstep you run on z



Lets say you run 1/8 microstep on Z (I usually run Z at less since resolution is way overkill already.



((200steps*8micro)/(20/36))/2mm = 1440


---
**Eric Lien** *January 29, 2018 01:53*

2880 at 1/16 microstep


---
**Bruce Lunde** *January 29, 2018 02:14*

**+Eric Lien**  Thanks for that, that matches what i got from the [prusaprinters.org/Calculator](http://prusaprinters.org/Calculator); I just put that in and started a new print.  I was doing those calulations on paper, Dennis pointed out the on-line one.  


---
**Bruce Lunde** *January 29, 2018 02:28*

**+wes jackson** Thanks for that link!




---
**Bruce Lunde** *January 29, 2018 02:56*

![images/11b1da135d1c5eed790e5e3260e6691e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/11b1da135d1c5eed790e5e3260e6691e.png)


---
**Bruce Lunde** *January 29, 2018 03:41*

Success.

![images/28a07d9ee6bb1f0e32849d126938b354.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/28a07d9ee6bb1f0e32849d126938b354.jpeg)


---
**Bruce Lunde** *January 29, 2018 03:43*

**+Eric Lien** **+Dennis P** **+wes jackson**  Thanks for helping an old man learn, lol!


---
**Eric Lien** *January 29, 2018 14:37*

**+Bruce Lunde**​ looking much better, but I see a distinct z wobble at almost exactly a 2mm pitch. Next thing you should review is alignment of the z axis throughout it's range. Looks like leadscrews misalignment is introducing an eccentric wobble into the bed. 


---
**Bruce Lunde** *January 30, 2018 14:49*

+Eric Lien I will take a look at that. I know I still have a couple problems to solve, based on prints last night, maybe retraction based on the "fuzziness" of these edges?

![images/20d833075e9eec4f6f73c16ce4620b7b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/20d833075e9eec4f6f73c16ce4620b7b.jpeg)


---
**Bruce Lunde** *January 30, 2018 14:49*

![images/37fe061f042faad4aeee0cb2c86869cd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/37fe061f042faad4aeee0cb2c86869cd.jpeg)


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/HkRtiUU2cGT) &mdash; content and formatting may not be reliable*
