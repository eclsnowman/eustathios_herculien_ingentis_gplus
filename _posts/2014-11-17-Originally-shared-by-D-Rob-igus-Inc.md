---
layout: post
title: "Originally shared by D Rob igus Inc"
date: November 17, 2014 03:17
category: "Show and Tell"
author: D Rob
---
<b>Originally shared by D Rob</b>



**+igus Inc.**​ 1050 lead screw's with antibacklash nuts get a new x/y end! Upgrading the UL-T-Slot mkIII. MORE TO COME SOON ! This is the test fit which fit perfectly. I left openings for adjusting the backlash. Works better than i had hoped!. I just hope the pitch isn't too aggressive or I'll have top slap 40 tooth pulleys on the drive for x and y



![images/7665509ad50e113c1a4568507958d3c0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7665509ad50e113c1a4568507958d3c0.jpeg)
![images/d4db8ee35df2559a5fa81224c8bf2fb1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d4db8ee35df2559a5fa81224c8bf2fb1.jpeg)
![images/4b39cc74b25ec3a314c598d02ddaa175.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4b39cc74b25ec3a314c598d02ddaa175.jpeg)
![images/99a7a0979bc975815c28e11ff273773a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/99a7a0979bc975815c28e11ff273773a.jpeg)
![images/b31340b58dd9f8dc109435cd39b99bc2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b31340b58dd9f8dc109435cd39b99bc2.jpeg)

**D Rob**

---
---
**Mike Thornbury** *November 17, 2014 04:36*

Thats a really steep pitch on your screw - why did you choose that?


---
**D Rob** *November 17, 2014 05:15*

It was the only thing faster than what I'd use on z. I have 1204 on the bot now. Great for machining speeds. Have to use high microstepping and still not as fast as I'd like. So I'm trying these. I can always gear it down via the pulley that interfaces the motor while leaving everything else in place. Better than ball screw's because they are self lubing and cheaper too.


---
**R K** *November 26, 2014 19:03*

Just to confirm - you looked to using a similar threaded screw + lock nut for X and Y instead of linear motion guided by pulley?


---
**D Rob** *November 26, 2014 19:14*

Yes. They are ages 50mm pitch 10mm od


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/287dDRso91M) &mdash; content and formatting may not be reliable*
