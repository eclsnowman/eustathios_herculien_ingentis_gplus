---
layout: post
title: "Im stuck with the Herculien Z axis,it wont print above 1440 steps per mm So far I managed to print a PLA test-cube 20 x 20 x 20 MM : measuring the result gives me 19.58 x 19.30 x 7.7 mm Gamma steps per mm 1440 All"
date: September 28, 2015 18:41
category: "Discussion"
author: Jo Miller
---
I´m stuck with the Herculien Z axis,it wont print above 1440 steps per mm



So far I managed to print a PLA test-cube 20 x 20 x 20 MM : measuring the result gives me 19.58 x 19.30 x 7.7 mm

Gamma steps per mm 1440



All ingredients of my Z axis are identical to the original Herculien BOM:  Pololu A4988 16bit :vref set on 0.68

20 teeth pully on motor / 36 teeth pully on Missumi Leadscrew : TR 12-2  pitch 2 mm/revulotion.



According to the prusa calculator this gives me gamma steps/mm  2880 .



But: the strange thing is: if I go above 1440 stepps/mm. The motor+ pulleys make a harsh kind of noise when homing and after print start, each layer is only moved about 1/10 of the steps it should move. sometimes it even goes back (reverse) and forward from layer to layer. ????



I´m an absolut newbie to smoothieware, but not to repetier Firmware , (this my third reprap printer). what am i overlooking ?

 too much vref ?

faulty firmware ? 

wrong repetier host version ? (V1.5.6 )



by the way: if I manually jog 20 mm directly through the AZSMZ Mini Jog-wheel with the  gamma steps/mm correctly  set on 2880mm,  it moves excactly 20 mm without the stepper motor giving any harsh noises, (but if I print directly from the SD card, it´s all wrong again), the same goes for printing through Repetier :  manual control with 2880 steps works fine, but printing only works with 1440 stepss/mm  , anything above goes wrong.



1:How can you change the Pololu A4988 from 16 to 8 bit ?, my 32 Bit board (AZSMZ Mini) does not offer any   jumpers for such a change (as on a ramps 1.4 board)



2: what influence has that smoothie-parameter "gamma_max_rate " ?   I have the alpha & Betta values on 30000 but the Gamma rate on 800  (refering to the Marlin original file) ,  but I dont know what this parameter actually stands for.





Thanks in advance:





**Jo Miller**

---
---
**Miguel Sánchez** *September 28, 2015 19:11*

what speed do you use for z-axis moves? it seems that above 1440 steps/mm the number of steps/second is too much for your motor/driver (if that is the case slowing down the move might help).


---
**Eric Lien** *September 28, 2015 20:37*

Yes set max Z velocity and acceleration down to a lower number. As things break in it may be able to be increased. Also confirm the lead screws are perfectly vertical. One thing most people miss on assembling HercuLien is the lower lead mounts Do Not get pushed back to the vertical v-slot 20x80. They are spaced out by design to allow degrees of freedom for alignment.﻿


---
**Jo Miller** *September 29, 2015 20:14*

@Miguel Sánchez + Eric Lien



Yes, that was it ! , THANKS ! 

Gamma speed was too high on the Z-axis , so I reduced it to 500 instead of 3000 ,    however still don´t know  what influence gamma max-rate (200) might have   on gamma-speed


---
**Ruwan Janapriya** *March 06, 2016 03:55*

Hi **+Jo Miller**, I'm trying to get my AZSMZ mini board working, once firmware installed and motors connected, I could only move X and Y. Z never moves although the motor gets power and held in place. Do you have an idea on this? Thanks.


---
*Imported from [Google+](https://plus.google.com/103341289176473342280/posts/fvetgN48TBb) &mdash; content and formatting may not be reliable*
