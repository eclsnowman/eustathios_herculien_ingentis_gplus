---
layout: post
title: "I have two major questions. 1) Does anyone have experience printing ultra high temperature filament?"
date: February 24, 2015 00:28
category: "Discussion"
author: Gus Montoya
---
I have two major questions. 1) Does anyone have experience printing ultra high temperature filament? I am trying to figure out how I can make an air intake plenum for my 190e cosworth without it melting onto the motor through desert trips. 2) Also anyone can direct me to a DIY 3d scanner that's moderate in cost and well documented? I'm doing my own research but still not satisfied in what I have found.





**Gus Montoya**

---
---
**Dat Chu** *February 24, 2015 00:54*

Search for bq ciclop 3d scanner. ﻿


---
**Gus Montoya** *February 24, 2015 01:01*

Thanks **+Dat Chu**, I'll look into it. :)

BTW: do you have a spreadsheet of all the people making the heat pad order? How is payment going to be done? 


---
**Jeff DeMaagd** *February 24, 2015 04:14*

I take it ABS won't do for high temperature? Maybe an ABS nylon blend? polycarbonate? Above that and you're starting to get into pretty exotic filaments.


---
**Mike Miller** *February 24, 2015 13:11*

You need a materials sheet describing the mechanical abilities of the material you want to use...e.g. Taulman 618 is reasonably high-temp resistant (prints at 230), but it's <i>flexible</i> in one or two perimeter prints...you don't want your intake deforming, it's sub-optimal for performance. It's GREAT from an intra layer adhesion and strength standpoint, it's machinable, and I absolutely cannot speak to it's ability to survive the environment you want.



Why do I bring it up then? Most 'plastic' intakes these days (and for the last 12-15 years) has been made out of Nylon 66, and 618 is Nylon, but the numbers are important, and I dunno enough about it. 



Just scanning the outside of a representative intake won't help you with internal ribbing and structures, and while Taulman does a GREAT job of actually testing the mechanical properties of their materials, I'm not finding anything that speaks to temperature of the environment, post print. 



Validation of use in an automotive environment (that can swing from -30F to well over 200F) is a nontrivial exercise. The worst that can happen is that you're stuck in the middle of the Austrailian outback with a puddle of goo where your intake used to be...but how many of us are ever in that position?


---
**Jeff DeMaagd** *February 24, 2015 13:26*

PPSF and Ultem 1010 are two materials a Fortus machine can build, and I'm guessing it's going to be a hardcore RepRap that does the same. One filamet manufacturer is planning to introduce PPSF, but the timeline isn't known. PPSF I understood half of Stratasys' price, but sill at a huge sticker shock to anyone not familiar with it. It will require a hot end that can handle higher than 300C.﻿


---
**Kalani Hausman** *February 24, 2015 19:12*

Nylon is good for higher-temp extruded material. It can also be colored as you wish using RIT-type clothing dye. Try the Slid3r build for a cartesian format, or the 3DR if you prefer delta printers if you want a quick build.


---
**Gus Montoya** *February 25, 2015 00:13*

Thanks everyone for the input, It's given me alot to research.


---
**Mike Thornbury** *February 26, 2015 18:25*

Heat shield and cold air diversion. And don't sit idling too long :)


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/KNFdfxDquam) &mdash; content and formatting may not be reliable*
