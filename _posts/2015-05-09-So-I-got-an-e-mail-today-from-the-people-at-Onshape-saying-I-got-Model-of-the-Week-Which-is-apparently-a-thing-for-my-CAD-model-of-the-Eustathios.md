---
layout: post
title: "So, I got an e-mail today from the people at Onshape saying I got Model of the Week (Which is apparently a thing) for my CAD model of the Eustathios"
date: May 09, 2015 19:08
category: "Discussion"
author: Erik Scott
---
So, I got an e-mail today from the people at Onshape saying I got Model of the Week (Which is apparently a thing) for my CAD model of the Eustathios. I'm having a real hard time accepting this, as most of the work is not my own, and so, **+Eric Lien** , **+Jason Smith** , **+Tim Rastall** and anyone else who's made significant contributions to the Eustathios/Ingentis family of printers, please let me know if there's anything you'd like me to do in return. 





**Erik Scott**

---
---
**Jason Smith (Birds Of Paradise FPV)** *May 09, 2015 19:16*

Cool man. I think that's awesome!  Thanks for all the hard work. 


---
**Eric Lien** *May 09, 2015 19:31*

Great job. No problems here. Just make attributions if you would, and congratulations. Have others used the collaborative features yet?


---
**Erik Scott** *May 09, 2015 20:34*

Jason And Eric are mentioned in the description of the file. And I haven't used any collaborative features, though the model is open to the public. I need to explicitly add people for it to work. 


---
**Tim Rastall** *May 09, 2015 22:17*

No problems from my side. Good work Erik . 


---
**Erik Scott** *May 09, 2015 22:47*

I'd really like to add you guys to the project. If you send me your e-mails, I'll go right ahead and do it! 


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/ezuG6tSyJ4f) &mdash; content and formatting may not be reliable*
