---
layout: post
title: "Does anyone have a link or Google Doc for the Eustathios Spyder 2 BOM?"
date: July 22, 2015 19:40
category: "Discussion"
author: Seth Mott
---
Does anyone have a link or Google Doc for the Eustathios Spyder 2 BOM?  I cant get the one in the Github to open.





**Seth Mott**

---
---
**Gus Montoya** *July 22, 2015 21:39*

You sure? I was able to open it just fine on my crappy 20 yr old computer. [https://github.com/eclsnowman/Eustathios-Spider-V2/blob/master/Documentation/BOM/Eustathios%20Spider%20V2%20BOM.xlsx](https://github.com/eclsnowman/Eustathios-Spider-V2/blob/master/Documentation/BOM/Eustathios%20Spider%20V2%20BOM.xlsx)


---
**hon po** *July 23, 2015 02:17*

**+Seth Mott** You could upload it to your own google drive and open it in the browser without installing office.



BTW, any update on your smartcore adventure?


---
**Seth Mott** *July 23, 2015 02:18*

I tried that and google drive says it was corrupt.. My Smartcore is printing OK, not using MakiBox parts yet though


---
**hon po** *July 23, 2015 02:44*

I could open the BOM. May be you right click the xlsx link and saved the html file? You should left click the xlsx and then click the raw button.



How about print quality? Your last post on reprap forum talking about quality problem. May be update with some photo?


---
**Seth Mott** *July 23, 2015 12:18*

I got the BOM to open, I don't know what I was doing wrong before haha.



The Smartcore is doing pretty good.  I got a PEI sheet for the bed and it is pretty amazing.  I think my print quality is not where I would expect it to be because my bed is too unlevel which causes the autolevel to have to work harder than it should.  This causes issue because my bed shakes slightly when moving up and down due to vibrations from all the motors, especially during fast moves.  I  just need to figure out a way to stiffen the bed.  I did switch to leadscrew for the z-axis and I think it is an improvement over the old belt.  PEI Bed: [http://i.imgur.com/zwgyvGzl.jpg](http://i.imgur.com/zwgyvGzl.jpg)  Latest print quality: [http://i.imgur.com/EAh8paVl.jpg](http://i.imgur.com/EAh8paVl.jpg)


---
*Imported from [Google+](https://plus.google.com/108197211464469447407/posts/HA3iegtGdg6) &mdash; content and formatting may not be reliable*
