---
layout: post
title: "Regarding the config for Smoothieboard, should we use the one on the github for spider V2 or directly from Smoothie?"
date: September 01, 2016 21:10
category: "Discussion"
author: Sean B
---
Regarding the config for Smoothieboard, should we use the one on the github for spider V2 or directly from Smoothie?  The reason I ask is because the V2 config is a year old, and I am using the X5 Mini V2 Azteeg board.  Are there significant differences in the config?



On a side note Octoprint couldn't identify the Azteeg, any suggestions on default port or baud rate to use?



Is there a way to adjust the voltage to the stepper motors?  I am running the 5984 drivers from Panucatt.  When I moved the motors through the Viki display the x and y seemed choppy and had a hard time moving the carriage; however the Z was smooth as butter.  I set the steps to 1/16 for Z and 1/32 for X and Y. 



Should the stepper voltage be increased for X and Y?  I am only familiar with manual pot adjustment.





**Sean B**

---
---
**Eric Lien** *September 01, 2016 21:38*

You will want to use the latest smoothieware and the latest x5 mini v2 config (but I think you have the v3, not v2 like you said above).


---
**Sean B** *September 01, 2016 22:27*

**+Eric Lien** Thanks Eric, I meant V3, you're correct.  Is there anything in the config specific to the spider V2?


---
**Eric Lien** *September 02, 2016 13:48*

I recommend starting with a fresh configuration. It is an easy edit.


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/UY2a3vuQv7o) &mdash; content and formatting may not be reliable*
