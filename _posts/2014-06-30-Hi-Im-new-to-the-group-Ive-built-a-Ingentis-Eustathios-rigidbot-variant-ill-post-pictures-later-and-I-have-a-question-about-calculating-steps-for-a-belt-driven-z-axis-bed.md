---
layout: post
title: "Hi, I'm new to the group. I've built a Ingentis/Eustathios/rigidbot variant (i'll post pictures later) and I have a question about calculating steps for a belt driven z-axis bed.."
date: June 30, 2014 13:32
category: "Discussion"
author: Jean-Francois Couture
---
Hi, I'm new to the group. I've built a Ingentis/Eustathios/rigidbot variant (i'll post pictures later) and I have a question about calculating steps for a belt driven z-axis bed.. how did you guys do it ? I have a 1.8 degrees nema17 motor with a 36 tooth gear connected on a 90 tooth pulley that drives a 8mm shaft that have the belts on the end (they're each on a 36 tooth/8mm bore). i'm look for the formula that can give me the number I can put in the marlin firmware for the steps. 



Any pointers are appreciated. Thanks !





**Jean-Francois Couture**

---
---
**Mike Miller** *June 30, 2014 13:39*

Request 10mm travel, measure actual travel, change Z-steps, repeat


---
**Eric Lien** *June 30, 2014 13:44*

Distance between teeth = 2mm



We need what micro steps you are using to finish the calculations.


---
**Jean-Francois Couture** *June 30, 2014 14:13*

**+Mike Miller** Yes, I did that at first but i feel that using a ruler is not precise enough.. maybe its me ? hehe


---
**Jean-Francois Couture** *June 30, 2014 14:16*

**+Eric Lien** Well, if you mean the motor rotations then its a 1.8 deg motor so 200 steps per rotations.. but, now that I think of it, you must mean on the step driver... its set to 1/16th.. should I use a 1/32th to have finer travels ?


---
**Mike Miller** *June 30, 2014 14:19*

**+Jean-Francois Couture** You may want to invest in a pair of digital calipers, they've gotten cheap enough. [http://www.harborfreight.com/6-inch-digital-caliper-47257.html](http://www.harborfreight.com/6-inch-digital-caliper-47257.html) (understanding harbor freight might not ship to your part of the world, alternatives should be available) 


---
**Matt Miller** *June 30, 2014 14:29*

Your gearing is 2.5:1  Your 36T pulley turns 2.5 times for every turn of your 90T pulley.



The circumference of your M8 rod is ~ 25.132741323 mm.



Therefore, for 1 revolution of the M8 rod your stepper must make 2.5 revolutions.  



So your ratio is 10.05309649mm/rev, which now needs to be divided up into microsteps.



1x microstepping with a 200 step motor yields 0.05026548245mm/step, or 19.8943678894 steps/mm.  Simply multiply this number by your microstepping rate.



At 16x that's 318.3098862304 steps/mm



And +1 for digital calipers.  They are not optional in this process.


---
**Mike Miller** *June 30, 2014 14:36*

And based in significant figures (and pi being a bitch) 318.31 should get you really close. :D﻿


---
**Jean-Francois Couture** *June 30, 2014 14:38*

**+Mike Miller** Yeah, sorry, I said ruler but ment caliper( I have 3 of them)  The problem is, I'm unable to get that golden number (10mm, like you said, I actually tried that exact number) and I tought that by going with the formula, I could get closer to the right movement :-)


---
**Jean-Francois Couture** *June 30, 2014 14:41*

**+Matt Miller** **+Mike Miller** : That is great.. see, for now, I have "110" set in Marlin.. something tells me that i'm WAY off or, i'm on the wrong micro step on the driver (the rumba board is king of weird to setup on the driver site of things).  


---
**Jean-Francois Couture** *June 30, 2014 15:49*

Thanks alot, i'll try my best to the final steps and send photos of the printer your way :-)



Cheers !


---
**Mike Miller** *June 30, 2014 16:04*

Making a thin walled 20mm cube with low infill will give you something to work with...keep in mind that over extrusion can skew measurements some. 


---
**Jean-Francois Couture** *June 30, 2014 16:32*

Yes, I use the 20mm_cube.stl file (fount on thingeverse) to compare results.


---
**James Rivera** *June 30, 2014 17:45*

**+Jean-Francois Couture** you may already have your answer, but for future reference, this page might help:



[http://calculator.josefprusa.cz/](http://calculator.josefprusa.cz/)


---
**James Rivera** *June 30, 2014 17:49*

...and a detailed description of the process can be found here:



[http://reprap.org/wiki/Triffid_Hunter's_Calibration_Guide](http://reprap.org/wiki/Triffid_Hunter's_Calibration_Guide)


---
**Jean-Francois Couture** *June 30, 2014 17:53*

**+James Rivera** Hi, Thanks. I know about the calculator (I've done printers with lead screws (turns per mm), no problem), what i did not know is how to do it with belts. :-)


---
**Robert Burns** *June 30, 2014 18:28*

get as close to 10mm as you can, then switch to 100mm, that will give you more room to see the differences. 


---
**Jean-Francois Couture** *June 30, 2014 18:47*

hey, that's a great idea.. thanks, will do :)


---
**Eric Lien** *June 30, 2014 21:16*

Just for clarification is this the setup:



Nema17 w/ 36 tooth -> shaft w/ 90 tooth input and 36 tooth output?






---
**Jean-Francois Couture** *June 30, 2014 22:17*

**+Eric Lien** Yes, that is right.. both belts are on a 36T/8mm(the shaft) pulleys. For now, looks like I have it dialed it at 112.1 in the Z-Steps in marlin.


---
**Jason Smith (Birds Of Paradise FPV)** *June 30, 2014 22:22*

What the heck.  I'll throw my guess in as well.  Assuming the belt pitch is 2mm, I would say it's (36 teeth/rev / 90 teeth * 36 teeth * 2mm/tooth = 28.8 mm/rev).  This would be .144 mm/step at 1x microstepping, .018 mm/step at 8x, and .009 mm/step at 16x. This would result in 6.9444 steps/mm at 1x, 55.5555 steps/mm at 8x, and 111.1111 steps/mm at 16x.

Unless I'm misunderstanding how the pulleys and belts are actually configured, I don't believe the diameter (or circumference) of the m8 shaft factors in at all. 

With the 112.1 number you gave, I assume that you're running 16x microsteps and just happen to be close to the correct number of 111.111...


---
**Eric Lien** *June 30, 2014 22:27*

If what I typed above is the case here is your steps per mm for the gt2 belt connected to the bed.



((360deg/rotation)/(1.8deg/step))*(16microstep/step) = 3200 microstep/rotation of the stepper



Now gear reduction:



36tooth/90tooth = 0.4 ratio motor to shaft



Output is a 36 tooth pulley so one motor rotation is:



(36tooth/rotation)<b>(2mm/tooth)</b>(0.4 ratio)= 28.8mm travel per rotation at the stepper



So (3200microsteps/rotation)/(28.8mm/rotation)= 111.11111... microsteps/mm



It would be better to get this to be an integer number to avoid rounding errors.



Someone should double check my work because I did this in my  head while driving.﻿


---
**Eric Lien** *June 30, 2014 22:30*

**+Jason Smith** you beat me to it.


---
*Imported from [Google+](https://plus.google.com/105576148076542448710/posts/24JxfNXxppr) &mdash; content and formatting may not be reliable*
