---
layout: post
title: "Can you all talk to the fans and fan positions/ adapters you have on your Herculien around the extruder?"
date: February 03, 2018 20:56
category: "Discussion"
author: Bruce Lunde
---
Can  you all talk to the fans and fan positions/ adapters you have on your Herculien around the extruder?  I currently have the fan that came with the E3D Volcano, and it runs constantly.  I have a second fan that is on the carriage, but I have it disabled because it seemed to be messing up my extruder. Any relevant pictures would be appreciated.



 Also, when I got the plastic parts to build my printer I received a part that does not seem to fit any of my fans, but I could modify now that I am printing decent parts.



 I do recall an older thread that I will revist when **+Eric Lien** advised on how to hook up the fan(s) so they could be controlled to turn on later in the print cycle.

![images/26b81bae5232e924411eb4cd6e6b6bdb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/26b81bae5232e924411eb4cd6e6b6bdb.jpeg)



**Bruce Lunde**

---
---
**Dennis P** *February 03, 2018 21:14*

Can you post a picture of your carriage? 



That part looks like the blower duct. It would fit over the discharge end of the fan and direct the airflow at the plastic your just extruded. 



Look at this picture and zoom into the middle at he carriage- you will see the duct on the fan. 



[https://raw.githubusercontent.com/eclsnowman/HercuLien/master/Photos/Render2.PNG](https://raw.githubusercontent.com/eclsnowman/HercuLien/master/Photos/Render2.PNG)



Traditionally, the cooling fan on the hotend should run at all times. This is the one that blows on the cooling fins of the hotend. 



The part cooling fan(s) should be the one controlled by the Smoothieboard.  These would be the ones that are directed by the blower duct. 



As for WHEN the part cooling fan and how fast it blows is controlled in the slicer. Here is a small snip of how it might look in Cura (Cura right?)



![images/b74129becd8a9e4a5f81e7938cf7f1a9.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b74129becd8a9e4a5f81e7938cf7f1a9.png)


---
**Bruce Lunde** *February 03, 2018 21:34*

**+Dennis P** Side view of my carriage

![images/79ae56245b3379621e2e27ae2b3c8bff.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/79ae56245b3379621e2e27ae2b3c8bff.jpeg)


---
**Dennis P** *February 03, 2018 22:03*

**+Bruce Lunde** this is the fan duct on the fan (mirror image of dual extruder carriage but you should get the idea)

![images/ed448c19ebedf541204fd4fed3761e85.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ed448c19ebedf541204fd4fed3761e85.png)


---
**Dennis P** *February 03, 2018 22:07*

**+Bruce Lunde** i am pretty sure your duct goes here like this. and i noted the fans. check their wiring to make sure they are connected to the right terminals. 

![images/1b3696a97c822c03aaf3bfbf4394ef97.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1b3696a97c822c03aaf3bfbf4394ef97.png)


---
**Eric Lien** *February 04, 2018 00:13*

You can setup the hotend heatsink fans to come on when the hotend reaches a specific temp (normally around 50C). If you look at my old Marlin configs, or my Smoothie configs you will see it in there.



For the part cooling fans (and the duct you see in the pictures) that is a separate output on the controller. It really depends on what board you used and how you set it up. For example if you set the heatsink fan off main 24v supply then they will always be on. I instead used a spare mosfet output, so it could be turn off when the hotend isn't at temp to save some life on the fans.


---
**Bruce Lunde** *February 04, 2018 00:43*

**+Dennis P**  Thanks I will print one that fits my fan and get it into place.  I found the setting you show in my Cura, so I will get those setup!



**+Eric Lien**, since I bought the 5XC I have the extra pin to use. I like the idea of having it off during warm up!


---
**Dennis P** *February 04, 2018 03:50*

It does make sense to control the fans that way, but the fewer number of cats you try to herd right now the better!

 


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/VjHvFNEQ5A1) &mdash; content and formatting may not be reliable*
