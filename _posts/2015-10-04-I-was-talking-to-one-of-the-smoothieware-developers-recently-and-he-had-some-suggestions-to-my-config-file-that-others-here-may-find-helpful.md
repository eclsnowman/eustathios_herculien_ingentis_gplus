---
layout: post
title: "I was talking to one of the smoothieware developers recently and he had some suggestions to my config file that others here may find helpful"
date: October 04, 2015 06:43
category: "Discussion"
author: Walter Hsiao
---
I was talking to one of the smoothieware developers recently and he had some suggestions to my config file that others here may find helpful.



set "z_junction_deviation = 0"

Adding this line causes the z-axis motion to be calculated independently of x and y, which allows the z-axis to accelerate and decelerate correctly while printing.  In my case, this has stopped the loud clunking sounds from my z-axis moves.  I increased tension on my z-axis belt after this change.  You may also want to configure "z_acceleration" after changing this parameter.



comment out "#mm_per_line_segment"

Should be commented out for cartesian bots, otherwise it will break up straight line moves.  I'm not sure it makes any difference with printing, but it should reduce the amount of processing and it avoids motion artifacts at low acceleration. 



axis max_speed and actuator max_rate

I had different settings on my z axis for these which was limiting it to the slower speed (they should be identical on cartesian bots), I set the z_max_speed and gamma_max_rate both to 2000mm/min and my z-axis moves much faster than before (it was previously moving around 8mm/sec, should be around 30mm/sec now).



For those who are using an older version of smoothieware, there have been recent improvements to pid tuning and a max temp safety check was added that are worth upgrading to (added in the last month or two).  I've been running with the new pid code with the autotune settings since it was checked in and my hotend has been much more stable since then.  I'm running off the edge branch (I don't know if the changes have propagated to the stable version).  If you do upgrade you'll probably need to run autotune and update the pid settings.  I haven't tried it yet, but the max temp check should halt the machine if the specified temperature is reached (enable temperature_control.hotend.max_temp).



PSA: if you want to encourage further smoothieware development you can donate at [http://smoothieware.org/](http://smoothieware.org/).





**Walter Hsiao**

---
---
**Jo Miller** *October 04, 2015 07:32*

thanks for the z axis  info  !


---
**Eric Lien** *October 04, 2015 09:53*

Thank you for the detailed post. This is great info. I can't wait to try some of these changes.


---
*Imported from [Google+](https://plus.google.com/+WalterHsiao/posts/f6QUqFjiCty) &mdash; content and formatting may not be reliable*
