---
layout: post
title: "So I'm sure this is a long shot but does anyone have a HercuLien they would want to sell?"
date: February 01, 2016 20:10
category: "Discussion"
author: Nathan Fisher
---
So I'm sure this is a long shot but does anyone have a HercuLien they would want to sell? Interested in anything to save me time, partial or complete. Im having a hard time finding the time to research, print, and build one and I need another printer ASAP. Thanks!!



What's the fastest anyone has built one?





**Nathan Fisher**

---
---
**Jim Stone** *February 01, 2016 22:05*

That is one HEAVY set of parts to ship. Like incredibly


---
**Nathan Fisher** *February 01, 2016 22:09*

True, pick up would work too! Any idea what a complete HercuLien weighs?


---
**Jim Stone** *February 01, 2016 22:14*

I personally can't move mine on my own and it's the full featured enclosed version. If I had to guess. Over 50lb 


---
**Jo Miller** *February 02, 2016 22:42*

Building : 2 hours a day  over month or so


---
*Imported from [Google+](https://plus.google.com/113760767936457744939/posts/RNRGzFgnFKD) &mdash; content and formatting may not be reliable*
