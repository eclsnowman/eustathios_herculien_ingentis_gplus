---
layout: post
title: "Things are getting serious, great job on designing this printer Eric Lien and same for the addons of Walter Hsiao ."
date: March 02, 2016 20:35
category: "Build Logs"
author: Maxime Favre
---
Things are getting serious, great job on designing this printer **+Eric Lien** and same for the addons of **+Walter Hsiao** . It's a pleasure to build it.

Thanks to you  my dining table is now full of 3d-thing-crap ;)



![images/bebac1d0c3960b3c22c65c18b0457ae4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bebac1d0c3960b3c22c65c18b0457ae4.jpeg)
![images/6b6d952561c8bfa1ea7b07ba05304b12.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6b6d952561c8bfa1ea7b07ba05304b12.jpeg)
![images/c48ff759836c173d7cd9855e630cb298.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c48ff759836c173d7cd9855e630cb298.jpeg)
![images/233c13fb96ad4b3a2031d28f5a4071f0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/233c13fb96ad4b3a2031d28f5a4071f0.jpeg)

**Maxime Favre**

---
---
**Brandon Cramer** *March 02, 2016 21:03*

Looks good. Is this the Eustathios Spider V2? 


---
**Eric Lien** *March 02, 2016 21:22*

Thanks man. And I love the Walter upgrade/mods. I have to get around to implementing them eventually. Mine is still a V1.5 :)


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/hWsT4pCpzVq) &mdash; content and formatting may not be reliable*
