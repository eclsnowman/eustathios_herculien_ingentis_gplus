---
layout: post
title: "does the stock dual carriage work with other hotends like jheads?"
date: October 27, 2015 03:23
category: "Discussion"
author: Jim Stone
---
does the stock dual carriage work with other hotends like jheads? or the pico? they both have groovemounts. like the e3d. they just dont have the tube clip to my knowledge.



im just trying to make this as dummy proof as possible :P





**Jim Stone**

---
---
**Jeff DeMaagd** *October 27, 2015 03:56*

E3D made the odd decision of using a taller groove than anyone else, so you might need to make adjustments to the model.﻿ The barrel cooling might need similar adjustments.


---
**Jim Stone** *October 27, 2015 04:07*

but the regular one fits this version right? with the weird plastic nub for holding the tube? [http://e3d-online.com/image/cache/data/v6/C_v6-1.75-U-3-1000x1000.jpg](http://e3d-online.com/image/cache/data/v6/C_v6-1.75-U-3-1000x1000.jpg)



im just still weary as hell from using e3d again after my 3mm direct was nothing but jams.


---
**Jim Stone** *October 27, 2015 04:11*

like there is this version

[http://e3d-online.com/E3D-v6/Full-Kit?product_id=380](http://e3d-online.com/E3D-v6/Full-Kit?product_id=380)



and this one

[http://e3d-online.com/E3D-v6/Full-Kit?product_id=381](http://e3d-online.com/E3D-v6/Full-Kit?product_id=381)



they both have everything the same in the box apparently so i have NO idea what the difference is.


---
**Chris Brent** *October 27, 2015 04:58*

The Bowden add on is just a few feet of PTFE tubing and a fitting for the cold end. Other than that they're identical, the direct drive uses the same end fitting on the hot end.


---
**Jim Stone** *October 27, 2015 05:01*

but which one fits in the carriage?


---
**Eric Lien** *October 27, 2015 05:04*

**+Jim Stone** the universal v6 but don't install the black fitting clamp. The ptfe tube from the extruder fits straight from the extruder down into the carriage and continues down into the hot end.


---
**Jim Stone** *October 27, 2015 05:08*

theyre both labelled as universal. one is just "direct" the other is bowden add on


---
**Eric Lien** *October 27, 2015 12:25*

**+Jim Stone** does this help: [http://m.imgur.com/fLjdCt8](http://m.imgur.com/fLjdCt8)


---
**Eric Lien** *October 27, 2015 12:36*

Get the one that looks like this and remove the top black pnuematic fitting clip, since I use captive 4mm nut to retain the bowden tube. 



[http://www.smw3d.com/e3d-hotend-v6-1-75mm/](http://www.smw3d.com/e3d-hotend-v6-1-75mm/)


---
**Eric Lien** *October 27, 2015 12:38*

Otherwise a new head piece could be designed and printed that integrates one of these inserted in a matched hole with a soldering iron. Then you wouldn't need the captive nut.



[http://www.smw3d.com/e3d-embedded-bowden-coupling/](http://www.smw3d.com/e3d-embedded-bowden-coupling/)﻿


---
**Jim Stone** *October 27, 2015 15:47*

that 3dimage was the best help ever actually. thank you!


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/6iqkh4QEfrL) &mdash; content and formatting may not be reliable*
