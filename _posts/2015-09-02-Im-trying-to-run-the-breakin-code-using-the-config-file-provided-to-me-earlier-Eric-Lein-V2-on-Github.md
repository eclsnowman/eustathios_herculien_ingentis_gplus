---
layout: post
title: "I'm trying to run the breakin code using the config file provided to me earlier (Eric Lein V2 on Github)"
date: September 02, 2015 09:35
category: "Discussion"
author: Gus Montoya
---
I'm trying to run the breakin code using the config file provided to me earlier (Eric Lein V2 on Github). I erased what I had and saved Eric's code as the config file on the memory on the smoothie board. The memory is in the board and I tested that the printer was online. All axis move. Now I'm trying to run the break in code and I'm not finding success. I made sure I had the recommended software installed per: [http://reprap.org/wiki/Printrun](http://reprap.org/wiki/Printrun)  and I do. But I get errors via the command console and the pronterface GUI software. I tried several command lines on the command console with the same effect. 

    My question is - how do I run the break in code. I must be trying the wrong way. 

![images/4331b4888df411040b3fd906ffc99320.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4331b4888df411040b3fd906ffc99320.jpeg)



**Gus Montoya**

---
---
**Gus Montoya** *September 02, 2015 09:37*

I also followed the instructions on the smoothie website but no success: [http://smoothieware.org/player](http://smoothieware.org/player)


---
**Frank “Helmi” Helmschrott** *September 02, 2015 11:03*

I can't tell anything about Pronterface/Printrun but maybe it could be a good idea ot use Repetier Host instead http://www.repetier.com


---
**Gus Montoya** *September 02, 2015 11:11*

I'll try it after I get some rest. 


---
**Eric Lien** *September 02, 2015 12:17*

My config is for an azteeg x5 mini, not a smoothieboard. So that might have something to do with it?


---
**Gústav K Gústavsson** *September 02, 2015 18:38*

Is your baud rate the same on the smoothieboard/software. If not it sometimes appear that the printer is online but he does not respond to commands or he hiccups. But I'm not familiar with smoothie board. (But serial communication is part of my knowledge ;-) )


---
**Roland Barenbrug** *September 02, 2015 19:52*

Hi, please check your Pronterface installation. Just checked the Break-in code with my Pronterface. Even without a printer connected Pronterface returns some sensible information after loading the file (e.g. size of object and estimated print time). In otherwords not related to X5 or Smoothie.


---
**Gus Montoya** *September 02, 2015 20:00*

Can anyone provide a working config file for the smoothieboard 5XC?


---
**Roland Barenbrug** *September 03, 2015 09:12*

Other route: could you connect your smoothie board to your PC/Laptop and press the Pronterface connect button. This should show some messages from your Smoothieboard (if properly connected).


---
**Bruce Lunde** *September 03, 2015 12:40*

I have not done anything but connect and run pronterface to move all the motors, but I did not have to change anything on the 5xc to get started.  I have not printed as yet, so I cannot advise any adjustments to the default code.


---
**Bruce Lunde** *September 03, 2015 12:50*

IF you need to reload the original configuration it should be on the SD card, if you overwrote it, I think there is a copy here: [https://github.com/Smoothieware/Smoothieware/tree/edge/ConfigSamples](https://github.com/Smoothieware/Smoothieware/tree/edge/ConfigSamples)


---
**Gus Montoya** *September 03, 2015 14:41*

**+Bruce Lunde**  Did you run the break in code?


---
**Bruce Lunde** *September 03, 2015 20:15*

No, I am still building.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/DR2pdAnRwXZ) &mdash; content and formatting may not be reliable*
