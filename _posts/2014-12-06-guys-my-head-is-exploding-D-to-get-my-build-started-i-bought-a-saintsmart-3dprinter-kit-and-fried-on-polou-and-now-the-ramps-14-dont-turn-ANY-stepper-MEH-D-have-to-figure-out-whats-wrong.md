---
layout: post
title: "guys my head is exploding :D to get my build started i bought a saintsmart 3dprinter kit and fried on polou and now the ramps 1.4 dont turn ANY stepper MEH :D have to figure out whats wrong..."
date: December 06, 2014 10:24
category: "Discussion"
author: janosch simon
---
guys my head is exploding :D to get my build started i bought a saintsmart 3dprinter kit and fried on polou and now the ramps 1.4 dont turn ANY stepper MEH :D have to figure out whats wrong... i want to build an Ingentis and what would you guys suggest as an easy and stable controller? Azteeg? Smoothie? Rambo? TinyG i see that some of you use the TinyG but on reprap i cant find a Marlin firmware? but is here a tinyG ingentis firmware available? 

cheers janosch





**janosch simon**

---
---
**Florian Schütte** *December 06, 2014 10:41*

I think some controller with an Atmel ATMEGA128/2560 (Arduino+Ramps, Megatronics...) would be an easy starting point for beginners, because the marlin FW is not hard to understand and to modify. There is a big community behind it and there are many SW/HW plugins.

I will start my build with a megatronicsV2 and pololu drivers (i have the Boards lying around). Big goal is to use a rPi or some other "small real PC"  like BeagleBoneBlack in the future (also lying around from an old printer project + replicape and additional developed cape for display and some other addon features :D ), so the printer can do everything on his own (download models, sliceing etc.) and e.g. can be easily integrated to a network. 


---
**Pieter Swart** *December 06, 2014 10:54*

I had a horrible experience with an Azteeg X3. Not only was it missing a header, critical to the working of the controller, but was also causing my USB ports to malfunction and disconnect. I could also not power the controller from USB. Roy from panucatt advised me to solder in a replacement header and assured me that the controller was tested before it was shipped out. He also said that he would replace the controller but then stopped communicating with me altogether. a few months later I took the controller to a friend who identified the USB socket as the culprit. It wasn't properly soldered and could have damaged my notebook. After he fixed the board, I was able to use it again (and I could even power it from USB). From my experience, I don't think they are really testing those boards (you're either lucky or your not). What also counted against me was that I bought the board, but didn't use or test it immediately.


---
**Mike Thornbury** *December 06, 2014 12:33*

The Smoothie and TinyG use their own firmware - you won't find anything else to load on them. I have a couple of TinyG, but wouldn't recommend them for 3D printing - they don't have the ports/FETs for heated bed, etc. Smoothie is a definite plus - but they are pricey. You won't go wrong with one of those, except for your wallet :) Saying that, it's about 80 Euro if you are happy to solder your own connectors.



For my 3D printers, I use RAMPS. I wouldn't ever recommend anything from Sainsmart. There are good Chinese manufacturers (like Elecrow - [elecrow.com](http://elecrow.com)) but Sainsmart don't make it into that category.



I've bought five of these - they haven't arrived yet, so I can't say what they are like, but at $52 for the lot, even if I only get to use half the components, its still a bargain: [http://www.aliexpress.com/item/Reprap-Ramps-1-4-Kit-With-Mega-2560-r3-Heatbed-mk2b-12864-LCD-Controller-DRV8825-Mechanical/1956892171.html](http://www.aliexpress.com/item/Reprap-Ramps-1-4-Kit-With-Mega-2560-r3-Heatbed-mk2b-12864-LCD-Controller-DRV8825-Mechanical/1956892171.html)



The DCDuino is a half-decent Arduino clone. The rest of the parts are stock Chinese bits, but the drivers are Ti 8825s, which are (IMO) much better than the A4988s


---
**Eric Lien** *December 06, 2014 13:49*

**+Pieter Swart** wow. Sorry to hear about your azteeg. I have had good luck with both of mine. But I agree Roy is terrible at communication.


---
**janosch simon** *December 06, 2014 13:57*

thx guys cause i also have now a dislike on saintsmart i bought the kit from Sintron paid 54€ for ramps arduino and 5 polou [http://www.amazon.de/gp/product/B00K6ZRKZ2?psc=1&redirect=true&ref_=oh_aui_detailpage_o00_s00](http://www.amazon.de/gp/product/B00K6ZRKZ2?psc=1&redirect=true&ref_=oh_aui_detailpage_o00_s00) hope that this runs :D and thx for the tips that tinyg has no ports for heatbed :-O


---
**Pieter Swart** *December 06, 2014 17:03*

Thanks **+Eric Lien** . I do like his designs though. Still need to find the time to test out a Viki display unit that I bought from him.


---
**R K** *December 10, 2014 02:35*

Out of curiosity, how did the ramps + steppers blow out? I picked up a sainsmart kit during black friday, and am curious if there are things to look out for


---
*Imported from [Google+](https://plus.google.com/113803124995753345088/posts/FFppQ95sibU) &mdash; content and formatting may not be reliable*
