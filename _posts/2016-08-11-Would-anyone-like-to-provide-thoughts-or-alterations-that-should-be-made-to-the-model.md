---
layout: post
title: "Would anyone like to provide thoughts or alterations that should be made to the model?"
date: August 11, 2016 20:29
category: "Show and Tell"
author: Sean B
---
Would anyone like to provide thoughts or alterations that should be made to the model?  Building on Eric's stacked azteeg x5 mini/Raspberry pi combo, I included holes for a 5mm button head to mount directly to the extrusion.  The images are missing the additional peg support but you get the idea.  Also, these images are upside down.



![images/35b4ee410522a573ab42517fcd535a3e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/35b4ee410522a573ab42517fcd535a3e.png)
![images/ddcc1d72e44fd05bcfb72ac06a6323ce.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ddcc1d72e44fd05bcfb72ac06a6323ce.png)
![images/31ad071689ed37fb32745da2596dc89e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/31ad071689ed37fb32745da2596dc89e.png)
![images/8431160ed06684d3f8e6270be5041757.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8431160ed06684d3f8e6270be5041757.png)
![images/bcdb5d96663849bf294c9ddb7a29dbb1.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bcdb5d96663849bf294c9ddb7a29dbb1.png)
![images/12dde4bb79dc28dce32d1ad0252780c3.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/12dde4bb79dc28dce32d1ad0252780c3.png)

**Sean B**

---
---
**Eric Lien** *August 11, 2016 20:46*

Looks good. I guess the real test will be to print one and see how stable it feels. 


---
**Sean B** *August 11, 2016 20:48*

Thanks, I'll try and print it up this weekend in ABS.  I really wanted to align the ethernet ports but it looked like more work than it was worth


---
**Ray Kholodovsky (Cohesion3D)** *August 11, 2016 21:08*

I shall coin this, the stack-teeg :) 


---
**Jeff DeMaagd** *August 12, 2016 04:20*

It's neat how the holes are close enough. I wonder if the Pi's GPIO serial port can control the smoothieboard using the x5's serial debug port. That way no cable leaves the frame and has to re-enter.


---
**Eric Lien** *August 12, 2016 05:07*

**+Jeff DeMaagd** I run my HercuLien now on Tx, Rx pins. It works great between the Azteeg and the pi3. Only issue is for some reason it can have trouble establishing initial serial communication sometimes via octoprint. But once connected it has been rock stable.


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/BomPnmfzQ8s) &mdash; content and formatting may not be reliable*
