---
layout: post
title: "Printed this small blower a few days ago, this eustathios is awesome !"
date: June 08, 2016 18:19
category: "Show and Tell"
author: Maxime Favre
---
Printed this small blower a few days ago, this eustathios is awesome ! The grey filament is new, still need to tune some stuff. The fan is almost perfect.



![images/34b6ee81c2a7d29570758fa5c03493cc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/34b6ee81c2a7d29570758fa5c03493cc.jpeg)
![images/02194d515ebfc73979b9987beba72fd1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/02194d515ebfc73979b9987beba72fd1.jpeg)

**Maxime Favre**

---
---
**Roland Barenbrug** *June 08, 2016 19:06*

**+Maxime Favre** what do you use as hot end (E3D (normal or Volcano, nozzle size) and what do you use as cold end (Bondtech?)


---
**Maxime Favre** *June 08, 2016 19:18*

Yes, E3D normal 0.4 and bondtech


---
**Eric Lien** *June 08, 2016 19:32*

**+Maxime Favre** it's a winning combination :)


---
**Jason Smith (Birds Of Paradise FPV)** *June 08, 2016 19:52*

That model looks nice.  Is it open source/available for download?


---
**Maxime Favre** *June 08, 2016 19:56*

Here you go [https://drive.google.com/open?id=0B3rOLUu_uwqNaV81OVVqUmNoSHc](https://drive.google.com/open?id=0B3rOLUu_uwqNaV81OVVqUmNoSHc)


---
**Roland Barenbrug** *June 08, 2016 20:11*

OK, good to know. I've been using an E3D normal with 0.4 nozzle on my Prusa I3 with direct drive for 2 years and am really happy with it. The Volcano with 0.6 nozzle and bowden drive isn't getting close in printing quality. Considering changing the Volcano for a normal E3D and change the 0.6 for a 0.4 nozzle.


---
**Jason Smith (Birds Of Paradise FPV)** *June 08, 2016 20:17*

**+Roland Barenbrug** E3D also sells a .4 for the volcano. That's what I use, and it's a significant improvement over the .6, though I'm not sure if it's as good quality wise as a regular E3D. Might be worth a shot. 


---
**Eric Lien** *June 08, 2016 20:21*

**+Jason Smith** Yeah, I use a 0.4mm Volcano on my HercuLien printer now. No problems with quality. With tuning it is getting just as good as my V6 Standard E3D did. [https://goo.gl/photos/MTQ2tVS6imc515ca7](https://goo.gl/photos/MTQ2tVS6imc515ca7)


---
**Roland Barenbrug** *June 08, 2016 20:49*

**+Jason Smith** **+Eric Lien** OK, good to know. WiIl get myself a .4 nozzle for Volcano. [reprapworld.com](http://reprapworld.com) is near to next door for me.


---
**Nils Hesse** *June 08, 2016 22:34*

Nice job! How well is the blower working? Is it louder than a non-printed version would be?


---
**Morgaine Fowle (de la faye)** *June 09, 2016 03:55*

Beautiful print! 


---
**Roland Barenbrug** *June 10, 2016 09:48*

Interesting discussion yesterday with [reprapword.com](http://reprapword.com). They use Volcano + .6 nozzle on most of their production machines without no issues. Printed parts look great. So we moved discussion to fillament. Been using so far ESun. Decided to give REAL fillament a try. First prints look promissing and differerent league.


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/VFF4xTXQvo2) &mdash; content and formatting may not be reliable*
