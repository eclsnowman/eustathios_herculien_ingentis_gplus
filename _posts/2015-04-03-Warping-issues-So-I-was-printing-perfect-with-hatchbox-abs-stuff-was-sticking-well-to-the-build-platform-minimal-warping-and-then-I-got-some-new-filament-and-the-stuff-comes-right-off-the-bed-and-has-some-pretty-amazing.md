---
layout: post
title: "Warping issues? So I was printing perfect with hatchbox abs stuff was sticking well to the build platform minimal warping and then I got some new filament and the stuff comes right off the bed and has some pretty amazing"
date: April 03, 2015 07:13
category: "Discussion"
author: Derek Schuetz
---
Warping issues?

So I was printing perfect with hatchbox abs stuff was sticking well to the build platform minimal warping and then I got some new filament and the stuff comes right off the bed and has some pretty amazing warping properties. Anyone have suggestions to get this stuff to stick down?





**Derek Schuetz**

---
---
**Daniel F** *April 03, 2015 08:13*

You can use a raft. I try to place the object in a way the print does not get too high. Abs juice ( abs acetone mix) on kapton also helps. You can also place thin "discs" in the corners of your object to improve adhesion.


---
**Eric Lien** *April 03, 2015 12:58*

I got some microcenter filament once that just wouldn't stick. I used disks at corners (I almost always do this for good measure because removal of it is trivial and really helps). I tried brim. I tried upping my extrusion multiplier. I tried extra first layer smoosh. I tried changing infill direction and shell amounts. I tried kapton. I tried hairspray (which is now my default for abs). Etc.



I ended up taking it back. The replacement roll printed fine. So sometimes bad filament is just bad.


---
**Derek Schuetz** *April 03, 2015 13:16*

**+Eric Lien** exactly the brand it is Eric same issues to point. The new roll you get work out?


---
**Eric Lien** *April 03, 2015 14:36*

**+Derek Schuetz**​ yes. There are two brands they carry. Inland, and toner plastic (labeled microcenter brand).



Inland seems to have better dimensional stability. Toner plastics seems to stick better and have better mechanical properties.﻿


---
**Derek Schuetz** *April 03, 2015 15:24*

Ya I got the micro enter labeled one. Tolerance is awesome but I'm having perfect first layer on all types of bed adhesion things and it slides off like nothing is there. I'll try the silver roll out maybe it's just the black


---
**Eric Lien** *April 03, 2015 17:09*

Black is usually the cheapest ingredients. They can hide a lot of junk in it.


---
**Eric Lien** *April 03, 2015 17:34*

I like the inland brand for black ABS.


---
**Eric Lien** *April 03, 2015 21:05*

I am also liking the red hips they have now. Similar to abs. But lower bed temps. Good overhangs, smooth finish. Very nice.


---
**Derek Schuetz** *April 03, 2015 21:31*

**+Eric Lien** Is hips ok to use as printer parts?


---
**Daniel Salinas** *April 04, 2015 13:05*

Hatchbox is very consistent for me so I stick with that. Hope you have better luck with the new roll. 


---
**Derek Schuetz** *April 04, 2015 22:41*

well i figured it out. went down to 225C went to a .8 nozzle and now im fine 


---
**Eric Lien** *April 04, 2015 23:13*

**+Derek Schuetz** i think it should be. But it seems like interlayer adhesion could be better. I am testing now with 3x0.5 perimeters to validate it.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/arGXAXQE6BS) &mdash; content and formatting may not be reliable*
