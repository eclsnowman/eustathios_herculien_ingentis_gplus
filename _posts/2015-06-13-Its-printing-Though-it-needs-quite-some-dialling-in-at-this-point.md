---
layout: post
title: "Its printing! Though it needs quite some dialling in at this point"
date: June 13, 2015 05:56
category: "Discussion"
author: Ben Delarre
---
Its printing! Though it needs quite some dialling in at this point.



Lots and lots of stringing, and the top of the benchy got destroyed near the end, not sure quite why. I suspect the massive build up of stringing resulted in the nozzle knocking it off.



This is with an E3D v6, 0.4mm nozzle with a Bondtech v2 extruder.



Current retract is 3mm at 60mm/s, printed started at 190 and moved down to 180, but still stringing.



Any suggestions?



![images/501d2f15717ef616bd47427aefb8f676.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/501d2f15717ef616bd47427aefb8f676.jpeg)
![images/69133c7655dc1c6ad6dd423ba346428f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/69133c7655dc1c6ad6dd423ba346428f.jpeg)
![images/d3fb6e354bc6d3f64520c82eb6449717.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d3fb6e354bc6d3f64520c82eb6449717.jpeg)
![images/b95c5eee55c4f05dcb2c1c4e3fcaeff7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b95c5eee55c4f05dcb2c1c4e3fcaeff7.jpeg)
![images/c6f06bddda3fceecf5be04d6a38c8896.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c6f06bddda3fceecf5be04d6a38c8896.jpeg)
![images/b3a6715b26b687b18c5841f28324a9a7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b3a6715b26b687b18c5841f28324a9a7.jpeg)

**Ben Delarre**

---
---
**Eric Lien** *June 13, 2015 06:24*

I have found more retract helps stringing more than lowering temps. Like I said in the other post, try 6mm. ﻿higher temps also help limit the back pressure that leads to stringing. I print at 205+ for PLA (sometime up to 225 at high speeds and large parts).﻿


---
**Mutley3D** *June 13, 2015 15:38*

Ben, not bad for a first print at all. Perhaps not as much dialling in as you might think :)

1. I see slight but irregular Z artifacting - check Prusa Z height calculator for setting a precise layer height for your chosen layer height. [http://prusaprinters.org/calculator/](http://prusaprinters.org/calculator/) << scroll down toward bottom of page

2. Decrease retract speed and increase retract length (for the stringing) since temps seem reasonable although a bit higher might help as **+Eric Lien** suggests

3. Also some ringing/ripple is evident on short straight areas, try decreasing X and Y axis accelerations to reduce this if you wish

The caveat...belts and nozzle are assumed as tight - also check pulley grub screws are tight tight :)


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/DWR78CcTdvx) &mdash; content and formatting may not be reliable*
