---
layout: post
title: "I think I just witnessed the single most rare print failure"
date: August 06, 2016 03:07
category: "Show and Tell"
author: Stefano Pagani (Stef_FPV)
---
I think I just witnessed the single most rare print failure. Partway through the print, the Y shifted just enough so that the female threads printed over the male threads. It was spaghetti for about one CM then printed perfectly and the threads worked! To top it all off, the printer had a foot of filament left!



![images/d61556297f902aa2b6b7e8e7bf6751fb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d61556297f902aa2b6b7e8e7bf6751fb.jpeg)
![images/9857268f843c3d04d8e0319eb6171caf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9857268f843c3d04d8e0319eb6171caf.jpeg)
![images/cec5624d2d2c6a47b68b15438f04cc1e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cec5624d2d2c6a47b68b15438f04cc1e.jpeg)
![images/d99c1918d62dd2d5dd36fd3c62e264ff.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d99c1918d62dd2d5dd36fd3c62e264ff.jpeg)

**Stefano Pagani (Stef_FPV)**

---
---
**Sean B** *August 06, 2016 04:50*

I think you must pay homage to the Spaghetti Monster.


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/XPYTK7cc38m) &mdash; content and formatting may not be reliable*
