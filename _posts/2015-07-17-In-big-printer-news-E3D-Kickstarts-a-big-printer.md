---
layout: post
title: "In big printer news: E3D Kickstarts a big printer!"
date: July 17, 2015 20:11
category: "Discussion"
author: Chris Brent
---
In big printer news:

E3D Kickstarts a big printer!

[https://www.kickstarter.com/projects/e3dbigbox/the-e3d-bigbox-3d-printer](https://www.kickstarter.com/projects/e3dbigbox/the-e3d-bigbox-3d-printer)





**Chris Brent**

---
---
**Erik Scott** *July 17, 2015 22:41*

I quite like this actually. Glad it will be open-sourced. 


---
**Gus Montoya** *July 17, 2015 23:08*

Hmm, looks cool. Something about all plastic frame. Makes it disposable and temporary.


---
**Ricardo Rodrigues** *July 18, 2015 11:45*

I would like better if it would have a metal box, but nevertheless it should great comming from those guys. 


---
*Imported from [Google+](https://plus.google.com/+ChrisBrent/posts/EyD7pXdUvNk) &mdash; content and formatting may not be reliable*
