---
layout: post
title: "Hey guys! Has anyone put a BL touch on their Eustathios warped plate and I really don't want to have to deal with leveling anymore"
date: December 06, 2018 17:06
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hey guys! Has anyone put a BL touch on their Eustathios warped plate and I really don't want to have to deal with leveling anymore.



Also wondering about Acceleration and Jerk values. I am losing steps past 70mm/s and want to get a really reliable printer.



Thanks!





**Stefano Pagani (Stef_FPV)**

---
---
**Daniel F** *December 06, 2018 18:16*

Hi Stefano,

No sensor on my Eustathios, it hasn't been necessary so far, the cast and milled 6mm aluminium plate with glass on top is flat and I only have to adjust it after a nozzle change.

Acceleration in x and y direction is set to 5000mm/s^2 for printing and 6000mm/s^2 for traveling

Z is 300mm/s^2

Jerk 20



Board is RADDS Arduino due, Steppers are RAPS128 with THB6128 chip. Although I can print @100mm/s, I usually do so at 60mm/s.

Max speed with Arduino due with 128 microsteps in my setup  (16 teeth pulley on stepper, 32 teeth on rods) is 125 mm/s.

SW is Repetier 1.0



I never loose steps anymore since I changed to quality drivers and properly adjusted the current so the motors don't overheat.








---
**Eric Lien** *December 06, 2018 22:14*

I have found lost steps to occur more from nozzle catches than from high speeds. Unfortunately the central carriage using bushings makes it far less compliant to catches than linear bearings would. Catches will cause a bind on the busing and skipping steps.



To confirm, Make a test gcode that is really fast, but doesn't actually print filament. Start at a known position, Move around a bunch at high speeds, small moves, fast moves, etc. Return to the known location. My  guess is if you measured start to end positions you would find no missed steps.



Also you can turn down acceleration. I found with testing, 2000 is more than enough to maximize print speeds.


---
**Roland Barenbrug** *December 06, 2018 22:49*

Same here with a setup very similar to **+Daniel F** (DUE + RADDS + Repetier). Lost step are always caused by a colision between hotend and filament. Must admit that this part of dialing-in, i.e. extrusion related,  is the most difficult part. May **+Daniel F** or **+Eric Lien** can share their settings relevant to this. Running here an E3D Volcano with 0.4mm nozzle.


---
**Eric Lien** *December 06, 2018 23:28*

My main thing is not running too fast on  small prints that have a tendancy to curl up because you can't cool the layer in time before the next one starts. I also print outside perimeter to inside perimeter on parts that have a tendancy to curl (like overhangs). This tends to fight curling because the second or third perimeter pass is inside. I Z-hop on travel moves atleast 2x layer height.



I used to print fast on all things. Now I have more printer capacity than I need, so I slow down. 100mm/s is great, but if I have such a large print that printing that fast is needed... then I go to a bigger nozzle and slow back down.



I spend a lot of time dialing in my extrusion muiltiplier. Over extrusion is a sure fire way to cause a nozzle catch if you have fast travels. I set non-travel moves at 160mm/s. SO if you hit a blob at that speed you are certainly going to skip... expecially in PLA.



Once I have the steps/mm dialed in, then I run my extrusion multiplier around .98 down to .96 depending on the filament. This tends to lead to perfect top layers for me... but with no over extrusion to catch on.


---
**Stefano Pagani (Stef_FPV)** *December 07, 2018 02:12*

Thank you all!! I'll be trying all the things and getting back to you


---
**Dennis P** *December 07, 2018 18:11*

**+Stefano Pagani** I have the Duet/dc42  IR sensor, and I am running using it on Mega/RAMPS & Marlin. This is 7x7 mesh on backpainted 4mm glass on Mic6 plate with 3 point supports- 

[i.imgur.com](https://i.imgur.com/ysda6rC.png)

I have found that it really is not worth the auto bed leveling/meshing. 

After all the time I have put into it, I am pretty much back to relying on a single, dumb microswitch for Z stop and I get as good, if not better prints with no fuss. 

  


---
**Stefano Pagani (Stef_FPV)** *December 09, 2018 20:58*

Thanks guys! I am going to skip the autolevel, my bondtech case (printed) fell apart so I am printing a better one. It wasnt retracting much as the gears were not meshing well.



I am going to rebuild my plate to be like walters design and use better aluminum.






---
**Daniel F** *December 10, 2018 07:31*

**+Stefano Pagani**, [aluminium-online-shop.de - Alu-Shop - Artikelgruppe: Alu Gussplatten feingefräst – Aluminium-Online-Shop.de](https://www.aluminium-online-shop.de/de/shop-aluminium-kleinstmengen/Feingefraeste-Gussplatten-_-26/index.html)

 I don't know where you're located and if they deliver to your location. Just to give you an idea, cast aluminium milled surface, cut measurement, 6mm thick, 290x290mm is around 30€.


---
**Stefano Pagani (Stef_FPV)** *December 10, 2018 15:57*

**+Daniel F** Awesome! I am in the US so I will probably get it from McMaster


---
**Eric Lien** *December 12, 2018 00:18*

**+Stefano Pagani** I like these guys:



[https://www.midweststeelsupply.com/store/castaluminumplateatp5](https://www.midweststeelsupply.com/store/castaluminumplateatp5)

[midweststeelsupply.com - Cast Aluminum Tool & Jig Plate (ATP 5)](https://www.midweststeelsupply.com/store/castaluminumplateatp5)


---
**Stefano Pagani (Stef_FPV)** *December 12, 2018 01:42*

**+Eric Lien** thanks!




---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/ZrGVF8A7HZL) &mdash; content and formatting may not be reliable*
