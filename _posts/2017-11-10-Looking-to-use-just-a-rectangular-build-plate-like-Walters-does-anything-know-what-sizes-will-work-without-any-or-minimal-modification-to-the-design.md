---
layout: post
title: "Looking to use just a rectangular build plate like Walter's, does anything know what sizes will work without any (or minimal) modification to the design?"
date: November 10, 2017 16:01
category: "Discussion"
author: Ryan Fiske
---
Looking to use just a rectangular build plate like Walter's, does anything know what sizes will work without any (or minimal) modification to the design?



I saw Walter has the bed mounts but there isn't any info on what size will fit.





**Ryan Fiske**

---
---
**Sean B** *November 10, 2017 23:18*

I use Walters bed mounts, just measured mine.  16"x14.375.  ordered from midweststeelsupply.com.



Cast Aluminum Tool & Jig Plate (ATP 5) (ALI14)

Size: 1/4

Quantity: 1

Width: 14.2500

Length: 15.7500

Price: $32.99

Item Total: $32.99




---
**Ryan Fiske** *November 11, 2017 03:40*

Thank you very much Sean!


---
**Ryan Fiske** *November 15, 2017 16:13*

**+Sean B** are you using a silicone heater? What dimensions did you use? Want sure if I'd have to deviate from the one listed in the BOM


---
**Sean B** *November 15, 2017 18:39*

**+Ryan Fiske** I believe it was from the BOM, 320x320 120V 500W Heated_Bed.  Contact Sivia at 	梁菌菌 <s@alirubber.com>.




---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/gbdjHSGnv8e) &mdash; content and formatting may not be reliable*
