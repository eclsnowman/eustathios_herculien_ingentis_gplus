---
layout: post
title: "Just found this.."
date: March 13, 2014 08:09
category: "Deviations from Norm"
author: Tim Rastall
---
Just found this..





**Tim Rastall**

---
---
**Jarred Baines** *March 13, 2014 08:19*

Loving the Z - looks tough!


---
**Martin Bleakley** *March 13, 2014 08:28*

Yep its very solid :D


---
**Martin Bleakley** *March 13, 2014 08:30*

I need to take more pictures of it but here is a video of it running


{% include youtubePlayer.html id=L3h6Sp-ZfNs %}
[XMaker printing](https://www.youtube.com/watch?v=L3h6Sp-ZfNs)



I have recently changed from an airtripper to the bulldog its running really well atm


---
**Daniel Fielding** *March 13, 2014 08:56*

Any chance of showing a longer video of the whole setup.


---
**Martin Bleakley** *March 13, 2014 08:59*

I will work on taking one this weekend


---
**D Rob** *March 13, 2014 14:10*

Love it man. Awesome work


---
**Martin Bleakley** *March 15, 2014 08:08*

I took some more video for you guys as requested


{% include youtubePlayer.html id=AUOYJMHpzPw %}
[http://youtu.be/AUOYJMHpzPw](http://youtu.be/AUOYJMHpzPw)


---
**Daniel Fielding** *March 15, 2014 08:10*

Nice, that's pretty much how I was gonna do my z axis.


---
**Martin Bleakley** *March 15, 2014 08:17*

the 1204 ballscrew is really smooth, you can also replace the z bed printed parts with some ballnut housings [http://www.aliexpress.com/snapshot/6028607331.html](http://www.aliexpress.com/snapshot/6028607331.html) and some linear pillow blocks [http://www.aliexpress.com/snapshot/6027880874.html](http://www.aliexpress.com/snapshot/6027880874.html), which im in the process of doing


---
**Martin Bleakley** *March 15, 2014 08:19*

the z is using a continuous belt from [http://robotdigg.com/product/114/1220mm-endless-gt2-belt-in-6mm-width](http://robotdigg.com/product/114/1220mm-endless-gt2-belt-in-6mm-width) and 608zz bearings in the guide


---
**Martin Bleakley** *March 15, 2014 09:14*

**+Daniel fielding**


{% include youtubePlayer.html id=4lMv-3nuEDE %}
[http://youtu.be/4lMv-3nuEDE](http://youtu.be/4lMv-3nuEDE)


---
**Jarred Baines** *March 15, 2014 12:50*

Do you have a link to the actual ballscrews you bought **+Martin Bleakley**? (Please ;-) )


---
**Daniel Fielding** *March 15, 2014 13:16*

**+Martin Bleakley** pretty sure I already have 2 of those belts :)


---
**Martin Bleakley** *March 15, 2014 19:44*

**+Jarred Baines**

[http://www.aliexpress.com/item/1204-Ball-Screw-SFU1204-L-400mm-Rolled-Ballscrew-with-single-Ballnut-for-CNC-parts/1581347647.html](http://www.aliexpress.com/item/1204-Ball-Screw-SFU1204-L-400mm-Rolled-Ballscrew-with-single-Ballnut-for-CNC-parts/1581347647.html)


---
**D Rob** *March 16, 2014 02:52*

even better: [http://www.aliexpress.com/item/1set-1605-ballscrew-1pcs-End-machined-SFU1605-Ball-Screw-L-500mm-1pc-1605-BallScrew-Nut-for/1588972708.html](http://www.aliexpress.com/item/1set-1605-ballscrew-1pcs-End-machined-SFU1605-Ball-Screw-L-500mm-1pc-1605-BallScrew-Nut-for/1588972708.html)


---
**D Rob** *March 16, 2014 04:19*

they quoted me the same price even with the ends turned down to a custom diameter. I asked to turn both ends to 10mm  one tide 20mm long the other 10 so I can put pulleys on the longer end under the build chamber base plate


---
**D Rob** *March 16, 2014 04:22*

I sent them a ms paint drawing with dimensions. the email is :                               golmart_li <golmart_li@yeah.net> 



their response was: the unit price is USD19.34 , for 2sets, the shipping cost is USD28.6 by UPS, so the total price is USD67.28, the lead time is about 3-5days. if you have any question ,please let me know ,thanks.


---
**Martin Bleakley** *March 16, 2014 06:48*

I went with the 1204 as it was 4mm raised per rev, using a 36 tooth gt2 pully on the 1204 and a 16tooth on the stepper it also gives you a 1:2 ratio gives you around 1600 -  1800 esteps , using a 1605 will mean 5mm pre rev or around 1280 esteps


---
**Martin Bleakley** *March 16, 2014 06:50*

a handy little cal is [http://calculator.josefprusa.cz/](http://calculator.josefprusa.cz/)


---
**Jarred Baines** *March 16, 2014 21:47*

**+Martin Bleakley** - Did you make any special requests on the leadscrew (like **+D Rob** has, asking for the ends to be turned down) or just bought it "off the shelf"?



I'll probably go the 4mm lead for the reason's you've listed / divides into a wider array of z heights more evenly.


---
**D Rob** *March 16, 2014 23:06*

My custom was for 12mm as well


---
**Jarred Baines** *March 16, 2014 23:27*

What material are they?

I was thinking of just ordering them and turning them down myself if necessary (I have a serious lathe and carbide / CBN tooling)


---
**Martin Bleakley** *March 17, 2014 01:24*

**+Jarred Baines**

I had it machined here in Auckland , in hind sight I would of had them do it at the factory, as it ended up costing me 80 and that was the cheapest that I could find here


---
**Martin Bleakley** *March 17, 2014 01:26*

**+Jarred Baines**

Chrome plated steel. however they have a ballnut and if your machining them you need to secure in place else you have a ton of small ball bearings loose all over the place


---
**Tim Rastall** *March 17, 2014 01:35*

**+Martin Bleakley** I have tonne of small bearings in a jar because no one ever gave me that advice before I took apart a ball screw :)


---
**Jarred Baines** *March 17, 2014 02:36*

ha ha ha :-) Yep! I'll hold onto that tip boys, cheers ;-)



No worries machining them here then, I'll do that as then I can fine-tune lengths and fits ;-)


---
**Martin Bleakley** *March 17, 2014 02:36*

hah yeah I made sure I knew what I was doing before pulling things apart


---
**Tim Rastall** *March 17, 2014 02:42*

**+Martin Bleakley** In fairness I was very careful and it had to be done as there was some contamination in the race but those things are a nightmare to put back together - one day I will get around to it. Something to note with ballscrews in fact is that the cheaper ones often only have one silicone seal to protect the race and if a piece of plastic or metal get's past it, it can make the movement rough.


---
**Martin Bleakley** *March 17, 2014 02:50*

Ive been lucky with mine not getting crap in it and it was wrapped well before it was machined


---
**Jarred Baines** *March 17, 2014 12:24*

**+Martin Bleakley** - Are you able to upload an entire assembly to youmagine (or link here) I'm interested in your xy setup now as well as the Z... They're 'blocklike' but look tough and, on closer look, they seem like a really great design! Have you managed to have the belts drive point on the x/y ends the same height as the rods AND the rods of the ends and the middle shaft also at the same height? (It looks that way from pictures).



When you get around to it of course ;-)


---
**Tim Rastall** *March 17, 2014 19:45*

**+Jarred Baines** from what I can make out.  I looks like the belts are aligned above or below the outer shafts so the distance between shafts would be constrained by the size of the pulleys driving the belts.  Looks like 20 tooth gt2 pulleys to me,  I assume Martin had those custom machined to 10mm bore.﻿


---
**D Rob** *March 17, 2014 21:23*

**+Tim Rastall** I asked for 20 tooth from robotdigg but they said the smallest they could do is 32 in a standard. Ive looked ad the wall thicknesses on a 20 tooth (eyeballed it) and it doesnt look like they have enough meat for a 10mm bore


---
**Tim Rastall** *March 17, 2014 21:41*

Good point. The ones **+Martin Bleakley**  is using look smaller than 32/36 and it looks like he's using 10mm shafts.... Hopefully he can comment.


---
**Jarred Baines** *March 17, 2014 22:35*

You guys would have prefered 20t to 32t? 



I understand you can fit stuff easier with 20t but wouldn't 32t be better for tracking the belt?


---
**D Rob** *March 17, 2014 22:38*

ultimaker uses 20t


---
**Jarred Baines** *March 17, 2014 23:18*

Ultimaker's also scaled down 33% from your machines though... It's fitting that it uses a 20t pulley and we're using 30+ in that sense...



I'm not the most learned when it comes to toothed pulleys and the advantages / disadvantages of a smaller / larger pulley, except for the more obvious points, I don't know which is preferable... 20T gives more torque and smaller footprint, 32T gives more speed, less resolution and is available with bigger bores...



:-/



I'm going to use 10mm shafts on the outside, was going to use 32T pulleys simply because I saw you ordering a bunch rob (robotdigg are making it a standard item I think: [http://www.aliexpress.com/item/GT2-Pulley-32-Teeth-10mm-Bore/1704280978.html](http://www.aliexpress.com/item/GT2-Pulley-32-Teeth-10mm-Bore/1704280978.html))



But if 20T is ABSOLUTELY (not maybe, absolutely) good enough, I'd just go with them to reduce the footprint and fit in my custom X/Y setup easier.



Perhaps **+Martin Bleakley** can vouch for 20T pulleys once he's back on ;-)


---
**D Rob** *March 17, 2014 23:32*

those benefits/disadvantages are really more dependent on the pulley to pulley ratio. if I use 32t in my stepper and 32t on mr rods then its a 1:1 no matter how you slice it. however if I change the pulley on my motor to sat 20 then the pulleys on the rods only turn 5/8 of a rotation for every full rotation of the stepper pulley. its the same as doing gear ratios. if 20t would fit it would be ideal for the outside. then you could play with ratios to get faster speeds/lower res or slower/more intricate moves


---
**Jarred Baines** *March 17, 2014 23:45*

Sorry... brainfart on my end - you're 100% correct.



I'm thinking of direct-drive motor anyways, so it HAS to be 1:1 between my pulleys. So given that fact, there's probably 2 main concerns;



The SIZE of the pulley (for footprint / design purposes) and the OD of the pulley (as I understand it, a larger OD has less tendancy to slip and places less stress on the teeth as it spreads the load over more teeth, therefor it is possibly better in demanding situations also)



ugh... wish I had more experience with belts... I'm so used to leadscrews and pneumatics on the machinery I've been involved with...


---
**D Rob** *March 18, 2014 00:01*

what we need is the middle ground where limited to no possibility of slipping for the forces at work and as small a footprint as possible. basically the bang for the buck er... space


---
**Martin Bleakley** *March 18, 2014 03:14*

I had the 1204 machined down to 8mm, and used a 32tooth  pulley from robotdigg, and the reason i used a 16tooth on the stepper was so that i had a little more refined control when micro-stepping or doing a full step hence the 2:1 but that was just my personal preference, if your wanting 1:1 I would use 32tooth on all items as your after good tooth contact and you do not need to have a solid transition due to the smaller pulley


---
**Jarred Baines** *March 18, 2014 04:08*

400 steps per rev (Igentis stock motors)

4mm lead per rev (your leadscrew)

=.01 resolution... to me that is PERFECT - you can pick any multiple of .01mm and be on a full-step...



Of course, you can be on any multiple of .005 now **+Martin Bleakley** ;-)



But given this info I will probably go for all 32T, since that means only 1 type of pulley for the whole machine :-)


---
**Tim Rastall** *March 18, 2014 04:41*

**+Martin Bleakley** are you using 1/32 microstepping? 


---
**Jarred Baines** *March 18, 2014 06:49*

*calculations above may be incorrect - I only think I know how steps / rev works


---
**D Rob** *March 18, 2014 07:01*

If the motors are .9 degree/step then yes 400/rev if they are standard 1.8 then only 200. Then the 2:1 because of the pulley setup. With  .9 degree steppers a 1:1 could be maintained or minimum layer height halved by continuing to use 16t w/32t﻿


---
**Martin Bleakley** *March 18, 2014 07:11*

tim im using 1/16 and im using the rumba electronics [http://www.reprapdiscount.com/electronics/54-rumba-board-incl-6-a4988-driver.html](http://www.reprapdiscount.com/electronics/54-rumba-board-incl-6-a4988-driver.html) the steppers are [http://www.aliexpress.com/item/5pcs-NEMA17-78-Oz-in-CNC-stepper-motor-stepping-motor-1-8A/523595194.html](http://www.aliexpress.com/item/5pcs-NEMA17-78-Oz-in-CNC-stepper-motor-stepping-motor-1-8A/523595194.html)


---
**BoonKuey Lee** *March 25, 2014 09:17*

what about 36 teeth on the motor (5mm bore) and leadscrew (8mm bore)? this would be a 1:1 ratio. This is off  stock, just that I have to drill the bore to 10mm.


---
**Martin Bleakley** *March 25, 2014 19:59*

If that's what your wanting to do then go with it, ratios are more a persons personal preference than anything


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/Vwyh9tqY1Ke) &mdash; content and formatting may not be reliable*
