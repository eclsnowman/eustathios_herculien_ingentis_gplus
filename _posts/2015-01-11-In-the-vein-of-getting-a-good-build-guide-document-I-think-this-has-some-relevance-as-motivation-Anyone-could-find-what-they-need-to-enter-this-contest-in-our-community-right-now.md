---
layout: post
title: "In the vein of getting a good build guide document, I think this has some relevance: (as motivation.) Anyone could find what they need to enter this contest in our community right now...."
date: January 11, 2015 19:09
category: "Discussion"
author: Tony White
---
In the vein of getting a good build guide document, I think this has some relevance:

[http://www.reddit.com/r/3Dprinting/comments/2rzjwc/misumi_usa_contest/](http://www.reddit.com/r/3Dprinting/comments/2rzjwc/misumi_usa_contest/)

(as motivation.)



Anyone could find  what they need to enter this contest in our community right now....





**Tony White**

---
---
**James Rivera** *January 11, 2015 20:17*

If **+Eric Lien** posts the Herculien, I expect to see the word ,"amazeballs" used. :D


---
**Eric Lien** *January 11, 2015 20:38*

Thanks for the heads up guys.


---
**Eric Lien** *January 11, 2015 22:07*

I would be happy if I lost to someone who used my Eustathios model and finally made the BOM I have been procrastinating on. Jason's BOM is great, but really just a major component starting point. But if anybody does the full soup to nuts (bolts/belts/pullies/wiring/etc) version of Eustathios will get my vote).



P.S. if you use my solidworks model check the bolt length. I used solidworks toolbox and if you change the bolt length it doesn't always change the part name. That's why on HercuLien I used McMaster Carr bolt models.﻿


---
**Tim Rastall** *January 12, 2015 00:52*

I've just gone through an exercise in speccing the Procerus using Misumi and it works out about 4 times more costly than direct from China :(. Nema17s are $50 each!


---
**Eric Lien** *January 12, 2015 01:06*

**+Tim Rastall** I would say each supplier has their place. For extrusion (even precision precut/drilled) they aren't bad. It is likely since they actually make the extrusions. But for other items they are only resellers. 


---
**Joe Spanier** *January 12, 2015 03:34*

Apparently they are resellers too for extrusion. We tried to do a big group buy with our makerspace and they have to check with their "supplier". Whether thats in-house or not I'm not sure but didn't sound like it. 


---
*Imported from [Google+](https://plus.google.com/+AnthonyWhiteMechE/posts/hPnnZf3FSTG) &mdash; content and formatting may not be reliable*
