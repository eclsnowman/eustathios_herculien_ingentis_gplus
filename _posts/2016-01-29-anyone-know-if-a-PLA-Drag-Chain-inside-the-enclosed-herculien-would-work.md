---
layout: post
title: "anyone know if a PLA Drag Chain inside the enclosed herculien would work?"
date: January 29, 2016 05:06
category: "Discussion"
author: Jim Stone
---
anyone know if a PLA Drag Chain inside the enclosed herculien would work? or would it get too warm in there for PLA when printing high bed temp items





**Jim Stone**

---
---
**Eric Lien** *January 29, 2016 05:14*

Mine is PLA


---
**Jim Stone** *January 29, 2016 05:21*

Good to know thank you


---
**Jim Stone** *January 29, 2016 05:36*

is one drag chain on thinggiverse better than another?


---
**Eric Lien** *January 29, 2016 06:12*

Not really. It will really just depend on how you want to route them. Some can bend in two directions like the one I have on github. The others are designed to only bend in one direction. There is also a nice parametric one that you can customize that I used on the Talos3D Spartan. It is nice because you can specify it for your exact needs.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/U68Y8qAyqi4) &mdash; content and formatting may not be reliable*
