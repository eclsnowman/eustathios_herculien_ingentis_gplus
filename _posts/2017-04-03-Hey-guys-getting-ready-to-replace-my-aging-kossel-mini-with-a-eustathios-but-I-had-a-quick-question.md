---
layout: post
title: "Hey guys, getting ready to replace my aging kossel mini with a eustathios, but I had a quick question"
date: April 03, 2017 04:06
category: "Mods and User Customizations"
author: Ryan Fiske
---
Hey guys, getting ready to replace my aging kossel mini with a eustathios, but I had a quick question. Has anyone been successful downsizing this build to something work perhaps about a 200x200mm print area? Looking for something that's between a kitten printer and the eustathios size. Thanks guys! Really love the active community around this design!





**Ryan Fiske**

---
---
**Eric Lien** *April 07, 2017 02:19*

Sorry, I just found this post got caught in the G+ group spam filter. I think the design could certainly be shrunk down. It would essentially make it Ultimaker sized.


---
**Ryan Fiske** *April 16, 2017 15:44*

Eric thanks for the reply! For reducing size to this, do you think this would be as simple as reducing the lengths for the extruded aluminum, smooth rods, and Z screws? Or is this something that's going to require some real thought?


---
**Eric Lien** *April 16, 2017 16:43*

The bed would be the only thing that needs attention, that and how the electronics mount underneath because of the closed loop belt (perhaps just mount the PSU on the back). 



The rest should be able to scale linearly.


---
**Ryan Fiske** *April 16, 2017 16:48*

Awesome, if I attempt it, I'll be sure to report back though it might be some time!


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/JgfsQbDtFM8) &mdash; content and formatting may not be reliable*
