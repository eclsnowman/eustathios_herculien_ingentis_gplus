---
layout: post
title: "Hey guys, sitting down to order my steppers and want your thoughts on a good choice"
date: December 22, 2014 00:10
category: "Discussion"
author: Rick Sollie
---
Hey guys, sitting down to order my steppers and want your thoughts on  a good choice.  Should I go 1.8 or .9 step, if I'm using a DRV8825 Stepper Driver on a RAMPS board is the .9 even necessary considering the DRV8825 does 1/32 steps.  Whats the minimum power (oz/in) needed?   thx





**Rick Sollie**

---
---
**SalahEddine Redjeb** *December 22, 2014 01:51*

it's not 1.8 or 0.9 steps, its in degrees (an angle for rotation of the motors), the less is the angle the higher is your resolution, i personally use nema's with 1.8°, i think it should be enough even though m machine is not complete yet.

I am not aware of the torque required to run perfectly this kind of machines, but i have 4.6kg/cm rated motors, which hopefully will be okay.


---
**Florian Schütte** *December 23, 2014 20:40*

I ordered this model together with my pulleys and some other stuff

[http://www.robotdigg.com/product/241/0.9%C2%B0-Step-Angle-Nema17-48mm-High-Torque-Stepper-Motor](http://www.robotdigg.com/product/241/0.9%C2%B0-Step-Angle-Nema17-48mm-High-Torque-Stepper-Motor)

not tested yet


---
**Rick Sollie** *December 23, 2014 20:53*

I placed my order for those as well.  We'll see how well it works in the end. Thanks for the info.


---
**SalahEddine Redjeb** *December 23, 2014 21:00*

should be more than enough


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/ULbqG5wZy7Z) &mdash; content and formatting may not be reliable*
