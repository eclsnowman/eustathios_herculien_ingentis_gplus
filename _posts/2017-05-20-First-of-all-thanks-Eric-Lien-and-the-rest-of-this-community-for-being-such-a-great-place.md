---
layout: post
title: "First of all, thanks Eric Lien , and the rest of this community for being such a great place"
date: May 20, 2017 05:16
category: "Discussion"
author: Brad Vaughan
---
First of all, thanks **+Eric Lien**, and the rest of this community for being such a great place.  I've been a passive participant for a few months, but I've learned a lot just reading.  That said, I have run into an issue on my first Eustathios build...



Has anyone had issues with the Azteeg X5 GT (not mini) and the heat bed output not working?  The thermistor seems to be working (it's reading 25, which is reasonable for room temp this time of year in my office), but it never heats up.  It's not providing any voltage (measured with a meter), despite the Viki LCD displaying the bed heating icon.



It's the first time I've had it hooked up.  X, Y, Z endstops and motors work (been running the break-in G-code without issue).  Bad FET/board?



The bed temp portion of my config is below.



# Bed temperature control configuration

temperature_control.bed.enable               true

temperature_control.bed.max_pwm              255

temperature_control.bed.thermistor_pin       0.23

temperature_control.bed.heater_pin           2.7

#temperature_control.bed.thermistor          Honeywell100K

temperature_control.bed.beta                 3950

temperature_control.bed.set_m_code           140

temperature_control.bed.set_and_wait_m_code  190

temperature_control.bed.designator           B

#temperature_control.bed.bang_bang           true

#temperature_control.bed.hysteresis          2.0

temperature_control.bed.p_factor             295.8

temperature_control.bed.i_factor             34.301

temperature_control.bed.d_factor             638





**Brad Vaughan**

---
---
**Brad Vaughan** *May 20, 2017 19:39*

I've hooked up the thermistor for the hotend, and it reads the same temp as the bed thermistor.  When setting the temp for the hotend, the red light on the board lights up indicating the FET is in use, and I get voltage on pin 2.4 as expected.



I also tried changing the bed control to bang_bang, but that didn't help (no voltage, no FET light turns on).



Starting to think it's the bed FET/board that's the issue.


---
**Eric Lien** *May 21, 2017 14:42*

Sorry this got caught in the spam filter. I will take a look at my config in a little bit to compare. But if you get identical output on the hotend and bed, and pins are configured right, then I agree you might have a bad board.


---
**Eric Lien** *May 21, 2017 14:43*

**+Roy Cortes**​


---
**Roy Cortes** *May 22, 2017 18:45*

Most of the time the Mosfets get shorted when damaged(RED led ON all the time), so if not the case then it could be the chip that drives the mosfets. Here are some things to try.



First make sure that you don't have an override file in your SD card. Second make sure there are no double entries in your config file (like using pin 2.7 in several instances).



Other than that I am suspecting something happened to the driver chip, particularly the line for the heatbed if the hotend works. Email me at support@panucatt.com for replacement.


---
**Brad Vaughan** *May 22, 2017 18:58*

Thanks for the response, Roy.  I have a ticket (#502) logged in FreshDesk.



I believe I've already checked for duplicate lines referencing pin 2.7, but I'll double-check again at home this evening.



There are no other config-related files on the microSD card besides the one I'm actively using.  I'll post the file in its entirety this evening for you to review if that will help.


---
**Eric Lien** *May 22, 2017 23:45*

**+Roy Cortes** Thanks for helping out. You have always provided amazing support for me, even helping me through my own missteps and config typos, and even late into the night when I know you have things you would rather be doing :)



Your great boards and support are the reason I keep coming back, and I really can't recommend your boards enough to anybody looking for the highest quality boards out there.



Thanks again for your continued support in our community.


---
**Brad Vaughan** *May 24, 2017 19:19*

I will echo **+Eric Lien**.  **+Roy Cortes** has been responsive throughout the troubleshooting process and is shipping a new board.  I'm amazed at the specs and features on these things, so when you couple that with great customer service, it's hard to find a better supplier.


---
*Imported from [Google+](https://plus.google.com/109037736098991448813/posts/fXv1ec9RKsh) &mdash; content and formatting may not be reliable*
