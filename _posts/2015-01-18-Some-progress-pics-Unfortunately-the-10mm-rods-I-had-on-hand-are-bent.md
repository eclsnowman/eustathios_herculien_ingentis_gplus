---
layout: post
title: "Some progress pics. Unfortunately, the 10mm rods I had on hand are bent"
date: January 18, 2015 01:23
category: "Show and Tell"
author: Matt Miller
---
Some progress pics.



Unfortunately, the 10mm rods I had on hand are bent.  Luckily, I have some more to try tomorrow.  



Cutting new Y-axis plates tonight and tomorrow morning.



Z Axis is nearly complete.  Just need some M6 fender washers to finish off the lifter assemblies and cut/mount the front and rear bed supports.



![images/6fa9d9fff7c90c647e39c5fd7f5ca81f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6fa9d9fff7c90c647e39c5fd7f5ca81f.jpeg)
![images/6ef152b9ef2d938cbb56a019399857a3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6ef152b9ef2d938cbb56a019399857a3.jpeg)
![images/d07b5cc54449e10012ab9c4299765b43.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d07b5cc54449e10012ab9c4299765b43.jpeg)
![images/96dc2c7a9f8a8253379abf3ac6eb27fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/96dc2c7a9f8a8253379abf3ac6eb27fa.jpeg)

**Matt Miller**

---
---
**ThantiK** *January 18, 2015 01:35*

I think this is the issue that **+Tim Rastall** was getting pissed off about.  Virtually unpossible to find rods that are completely straight, right tim?



I know for a fact that one of my rods is bent ever so slightly.  But t hat was my fault because it's so easy to just pick this machine up by the edge of the frame that I ended up picking it up and wrapping my hand around the bar to pick it up.


---
**Tim Rastall** *January 18, 2015 02:15*

**+ThantiK** correct.  They all have some run out. It's just a question of finding ones with very little. Also,  this: How to straighten bent 8mm metal shaft accurately…: 
{% include youtubePlayer.html id=mRtIxG2co5w %}
[http://youtu.be/mRtIxG2co5w](http://youtu.be/mRtIxG2co5w)


---
**Matt Miller** *January 18, 2015 02:49*

**+Tim Rastall** great video!  I now have an idea of what to do.  Thanks!


---
**Eric Lien** *January 18, 2015 04:58*

Every rod I have received from misumi has been straight as an arrow. Guest I have lucked out.



**+Matt Miller**​ hell of a machine. And I thought HercuLien was overbuilt ;)



You could drive a truck over that thing.


---
**Matt Miller** *January 18, 2015 05:40*

If I can't get the 10mm McMaster shafts straight I'm gonna have to go the misumi route.  But it'll be worth the time and headaches.



And thanks for the compliments about the frame!  The Herculien is an awesome machine - your prints are some of the best I've seen.  Hoping to come close to that level of quality with this machine.  I do have concerns about frame twist - those spindly little legs give me agita.  But I guess I won't know until I fire it up!  :D


---
**Mike Thornbury** *January 18, 2015 07:09*

Is there a particular reason for 10mm shafts? They are 35% heavier than 8mm shafts which will detract from the acceleration and top speed. Then there's the additional cost of bearings, etc.



Does it give you any advantage?


---
**Tim Rastall** *January 18, 2015 07:31*

**+Mike Thornbury** , The 10mm shafts are used on the outer part of the frame to support the gantry. They only rotate in place so for all that there is some consequence of the additional rotational inertia, it wasn't of great concern. It's my fault they are 10mm though as I was scaling up the original Tantillus (that used 8m shafts for a build area of about 150x150mm). I figured for 300x300 I'd better increase the shaft diameter in order to minimise any deflection, particularly as they rotate and need to be dead straight. I stuck with 8mm for the moving cross members to minimise moving mass as much as possible.

Edit - this of course assumes Eric went with 10mm as it was the defacto for Ingentis and Eustathios rather than deciding on it independently.

Oh, and this problem with shafts that aren't perfectly straight is one of the reasons I've not used the same mechanical design on my new bot; the ultimaker/tantillus/ingentis mechanics don't scale very nicely, as you need to scale the radius of the outer shafts as your build volume grows, which is not only costly but as the shafts get longer the implications of even minor imperfections in shaft straightness are exacerbated in the resultant run-out at the shafts midpoint.


---
**Mike Thornbury** *January 18, 2015 07:37*

Gotcha! Thanks for the explanation. I (again) want to try some lightweight composite tubes instead of steel, and I just happen to have a bunch of 8mm steel shafts lying around that I can use for the gantry supports... interestinger and interestinger :)


---
*Imported from [Google+](https://plus.google.com/+MattMiller_akhlut/posts/2Gc3yrneDwA) &mdash; content and formatting may not be reliable*
