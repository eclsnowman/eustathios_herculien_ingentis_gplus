---
layout: post
title: "Got this today for the Spider V2 build"
date: June 16, 2015 04:37
category: "Discussion"
author: Brandon Cramer
---
Got this today for the Spider V2 build. I got so excited when I saw the dimensions!!!

![images/55040d59918e6c2ba78db5bd8743e103.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/55040d59918e6c2ba78db5bd8743e103.jpeg)



**Brandon Cramer**

---
---
**Brandon Cramer** *June 16, 2015 04:38*

I tossed my sunglasses on there to have something to compare the size to. 


---
**Gus Montoya** *June 16, 2015 05:07*

Yeah looks nice when new, but smells super bad. I washed it with soap and water and dried it. But it still stinks bad (but not nearly as much). Anyway to get the smell off?


---
**Christian Werner** *June 16, 2015 07:17*

Alirubber, I suppose?


---
**Ben Delarre** *June 16, 2015 14:34*

Mine stopped stinking after about a week sat in the garage. I have not powered it up yet though. 


---
**Brandon Cramer** *June 16, 2015 15:01*

**+Christian Werner** Yes. 



I haven't noticed the smell I guess. When does it start smelling? 


---
**Ben Delarre** *June 16, 2015 15:18*

I guess it depends on how well cured the rubber is that's all the smell is. 


---
**Mike Thornbury** *June 17, 2015 02:14*

Turn it on for ten hours or so, should burn off all the volatiles.


---
**Richard Teske** *June 18, 2015 17:39*

Can someone tell me how much such silicon heater costs? For an 300mm x 300mm heated bed.


---
**Brandon Cramer** *June 18, 2015 17:43*

You can email sivialiang@alirubber.com.cn and ask her. I paid $50.00 for the 320mmx320mm.


---
**Ben Delarre** *June 18, 2015 17:45*

I paid 55.50$ delivered, 320x320 500W heater with two thermistors embedded.


---
**Richard Teske** *June 18, 2015 18:24*

thanks for the fast answers :)


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/8mN8bCZMNgA) &mdash; content and formatting may not be reliable*
