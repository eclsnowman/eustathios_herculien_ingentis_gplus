---
layout: post
title: "Marlin firmware issues, am new at Arduino and trying to troubleshoot some problems"
date: May 16, 2015 17:22
category: "Discussion"
author: Vic Catalasan
---
Marlin firmware issues, am new at Arduino and trying to troubleshoot some problems. When I use the standard marlin firmware I can control the steppers and hot ends using the Repetier or Matter Control host, however when I load the Azteed X3  v2 board with the Herculien firmware I cannot get anything to function on the board, connecting to the printer gives no error. Is there something I may need to set? I have yet to install the LCD panel, I am guessing that might be it.



Any help is appreciated! Thanks,





**Vic Catalasan**

---
---
**Vic Catalasan** *May 16, 2015 22:08*

Okay all good, I commented the Viki defind setting and got the Herc firmware working. pre-break in codes running now on a test table


---
**Eric Lien** *May 17, 2015 04:52*

**+Vic Catalasan**​​ sorry. Just saw the post. Thats what I would have suggested. Good job troubleshooting.



Also I know others have firmware versions based off the updated Marlin code. Perhaps we should get a repository going of firmware options (old marlin, new Marlin, Smoothieware, etc).﻿



**+Derek Schuetz**​​ **+Daniel Salinas**​​ **+Mike Thornbury**​ **+Isaac Arciaga**​ what are your thoughts?﻿


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/H3zeieuDLYR) &mdash; content and formatting may not be reliable*
