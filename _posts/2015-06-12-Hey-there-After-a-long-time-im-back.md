---
layout: post
title: "Hey there. After a long time i'm back"
date: June 12, 2015 00:30
category: "Discussion"
author: Florian Schütte
---
Hey there. After a long time i'm back. Had some ups and downs with my eustathios the last months. but nothing i can't solve. But now i'm a little bit clueless. The printer seems to have problems with diagonal movements. I can not find any fault in the mechanics. Some ideas? Am i blind?

![images/872d05bd2a9af8f9bab28ee876c385bb.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/872d05bd2a9af8f9bab28ee876c385bb.png)



**Florian Schütte**

---
---
**Jim Wilson** *June 12, 2015 00:32*

Multiple slicers all show the same problem?


---
**Florian Schütte** *June 12, 2015 00:44*

Tested with Repetier/Cura and Simplify3D


---
**James Rivera** *June 12, 2015 00:57*

Could this be a polyhole problem?

http://hydraraptor.blogspot.com/2011/02/polyholes.html


---
**Jeff DeMaagd** *June 12, 2015 01:11*

Notice how the outer perimeter of the circle below has flats on the top, bottom and sides. I had something similar once. I think you might be experiencing some backlash issues, meaning some play somewhere in your x and y axes.﻿


---
**Florian Schütte** *June 12, 2015 01:16*

**+James Rivera**  i dont think this is the problem. The problem occurs after reassembling the mechanics. Changed linear ball bearings to	friction bearing. I have aligned the 10mm rods to each other and the 8mm rods in parallel to them. belt tension seems to be ok too. 


---
**Florian Schütte** *June 12, 2015 01:20*

Oh and i tested some other rings. same problem. rectangles have nice outline dimensions but same problem with spaces when printing diagonal infill.


---
**ThantiK** *June 12, 2015 02:12*

This is 100% a backlash issue. ﻿ **+Jeff DeMaagd**​ hit the nail on the head. 


---
**James Rivera** *June 12, 2015 02:28*

**+Florian Schütte** Considering the fact you have just reassembled it, I have to agree with **+Jeff DeMaagd** and **+ThantiK**.


---
**Jason Perkes** *June 12, 2015 02:37*

Grub screws on pulleys should be pretty tight onto rods Given the consistency of the defect, id be looking at the individual pulleys on the motors (pulley grub screw flapping on a motor shaft flat?) , or the secondary pulley from the motor to driven axis shaft, as opposed to one of the opposing pulley pairs for each axis (if that makes sense?).


---
**Ivans Nabereznihs** *June 12, 2015 16:01*

You can try to make notches on the shaft using a dremels cut off disc where touch grub screws to avoid turning pulleys.


---
**James Rivera** *June 12, 2015 17:33*

You need to be careful to not get the metal shavings inside the stepper motor when adding a flat surface to the stepper motor shaft. I had to do this on my old Printrbot LC+ and found these directions which explain how (and why) to do it.

[http://www.printrbottalk.com/forum/viewtopic.php?f=6&t=441](http://www.printrbottalk.com/forum/viewtopic.php?f=6&t=441)


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/SyWzEq4Tm59) &mdash; content and formatting may not be reliable*
