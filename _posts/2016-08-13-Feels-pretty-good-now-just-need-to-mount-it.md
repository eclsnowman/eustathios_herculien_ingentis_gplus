---
layout: post
title: "Feels pretty good, now just need to mount it"
date: August 13, 2016 19:33
category: "Show and Tell"
author: Sean B
---
Feels pretty good, now just need to mount it.  I wish I made the mounting tabs thicker.  We'll see if it holds up.



![images/c79ac13b6212ea9c76a2a6b42e77df90.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c79ac13b6212ea9c76a2a6b42e77df90.jpeg)
![images/6625fa3259f26ac0fe3029bf86880b74.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6625fa3259f26ac0fe3029bf86880b74.jpeg)
![images/9974d9ffae45b87c049be6ecfbe59643.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9974d9ffae45b87c049be6ecfbe59643.jpeg)
![images/d0aebb1f67763af2aeb0d04116c209d7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d0aebb1f67763af2aeb0d04116c209d7.jpeg)

**Sean B**

---
---
**Eric Lien** *August 13, 2016 19:49*

Use a large washer to distribute the force. You should be fine.



Nice work.


---
**Stefano Pagani (Stef_FPV)** *August 22, 2016 03:22*

You used a button head with a socket head screw. That disturbs me.


---
**Sean B** *August 23, 2016 18:38*

**+Stefano Pagani** Haha, yes it is mildly annoying, but I needed 35 mm screws to reach through and that was all I had available.  Desperate times call for socket head and button head. :-)




---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/QiYtT7sUZ3f) &mdash; content and formatting may not be reliable*
