---
layout: post
title: "working on getting all the parts collected to make the eustathios v2, and there is one part i just cant seem to find"
date: December 28, 2017 21:37
category: "Discussion"
author: wes jackson
---
working on getting all the parts collected to make the eustathios v2, and there is one part i just cant seem to find. 7Z41MPSB08M 8mm self-lubricated bearing. ultibots is sold out for a while, sdp-si is sold out, QBC is also sold out. can any pressfit self-lube 8mm bearing work? or do you guys recommend a different one altogether.





**wes jackson**

---
---
**Eric Lien** *December 29, 2017 04:39*

McMaster PN 2032N24


---
**Eric Lien** *December 29, 2017 04:42*

Any bushing will work. But the self alignment feature is a key trick. The bronze bushing is essentially in a spherical joint in these bushings. So it can makeup for imperfections in frame assembly and your printed parts.


---
**Dennis P** *December 29, 2017 05:11*

I ordered mine today from McMaster-Carr and they are 1-3 weeks out, which is not good if SDP is out since I am pretty sure they are the only one who make them. I will try Isostatic and Oillite as backup.  


---
**Eric Lien** *December 29, 2017 15:35*

One nice thing is if they supply McMaster... Then they have a vested interest in getting them supplied first. McMaster is a big-carrot-big-stick scenario for these suppliers. So they like to make them happy.


---
**Dennis P** *January 05, 2018 16:31*

**+wes jackson** fyi: McMaster updated my order to be shipped by 1/11/18 for 4-8mm & 8-10mm. the watched pot never boils... 


---
**wes jackson** *January 11, 2018 06:56*

looks like ultibots are back in stock!


---
*Imported from [Google+](https://plus.google.com/109780842416628726318/posts/PhEiehJf8JB) &mdash; content and formatting may not be reliable*
