---
layout: post
title: "Hey everyone. I though I'll leave a short note and some pics/vids of my bigger maintenance I did during the last days"
date: November 08, 2016 14:59
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
Hey everyone. I though I'll leave a short note and some pics/vids of my bigger maintenance I did during the last days. If you remember I posted a few weeks ago that my heated bed died and I had to replace it. As I already had some more maintenance in mind I did that now and did even more than I initially thought.



Here's what I changed:



- New heated bed (again 250V/500W this time from alirubber) without built in thermistor but a hole in the center instead where I placed my own into the 5mm aluminum bed.



- New carriage/hotend holder completely inspired from **+Walter Hsiao** 's Space Invader Mini. I did a complete redraw as i need some things changed and i also opted to go with a magnetic hotend holder. It's not on the pics but I can show more details if anyone wants. It's my personal special edition for my changed rod distance and LM8UU ball bearings instead of bushings in the carriage. 



- I added some LED strips to the case that are software switched. They give a great lightning though it flickers a bit due to the usage of the PWM-based mosfet output. It's especially great when controlling the printer via the network to have good lightning for the webcam.



- I added a ARM-based Cubietruck board that I had left and so installed the Repetier Server I had previously running on an external machine completely to the case. It features WIfi, too. So the only connection to the printer is the 220V mains plug now. This is great improvement.



- I finally completely attached **+Mutley3D** 's Printbite bed to the new aluminum bed and it works great so far. I had an older one attached with clamps but having it attached with some glue (the new one came with a 3M-like glue layer) is awesome. Some pics are still with the bare or foiled aluminum bed, the ligher beige colored piece is the new permanent print plate.



- With the new carriage I also changed wiring. All the wiring is now going to the back where it goes down to the "basement" through some aluminum pipe and some printed parts. Also a big improvement in terms of clean installation. Also handling the teflon/bowden tube is much easier  that way. I think I will still add some guide pieces to that but it already runs great now though it's a bit wobbly but this doesn't affect extrusion at all so far.



- I also added a new filament spool that I found in this group - I will add the name of the guy who did it later, I seem to have missed writing that down before writing this post - sorry for that.



I guess that's it (this post got much longer than I thought. Please let me know if you have any questions.



![images/5d26df6e509c7649a62ce1e78ebc8bd2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5d26df6e509c7649a62ce1e78ebc8bd2.jpeg)
![images/43f6d6021861fd904c7177d3ed277051.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/43f6d6021861fd904c7177d3ed277051.jpeg)
![images/c087b52f2cf72830e31368458c62e9e8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c087b52f2cf72830e31368458c62e9e8.jpeg)
![images/6fce52379de5a56b62e93df7576d5ecd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6fce52379de5a56b62e93df7576d5ecd.jpeg)
![images/d1a8d7d3ed150187854419edf7caa79b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d1a8d7d3ed150187854419edf7caa79b.jpeg)
![images/c3a9fd3895c46b09c4095eca1cd7ae00.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c3a9fd3895c46b09c4095eca1cd7ae00.jpeg)
![images/2d4b91d441a5f61c0c5cfedfa3ae3343.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2d4b91d441a5f61c0c5cfedfa3ae3343.jpeg)

**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *November 08, 2016 15:09*

Looks amazing. Any chance you could perform a pull request to the github with the relevant files and a few pictures into the user mods and community upgrades area?


---
**Mutley3D** *November 08, 2016 15:23*

Awesome build the **+Frank Helmschrott** loveing the wiring almost as much as that last picture ;)


---
**Frank “Helmi” Helmschrott** *November 08, 2016 15:23*

I'll do my best but that will surely take some time to pack everything together.


---
**Maxime Favre** *November 08, 2016 16:18*

Nice update ! Interested about the re-design of the carriage ;)


---
**Ted Huntington** *November 08, 2016 17:21*

That is really interesting that you used a cubietruck for RepetierHost server- that is a really low cost and isolated solution- I currently have RepetierHost on the main computer that I do other work on- and sometimes a print will get ruined because I accidentally pull out the USB cable or crash the software with other software I am running. What is the printer controller board you are using that the cubietruck talks to via wifi?


---
**Frank “Helmi” Helmschrott** *November 08, 2016 17:28*

I'm using a RADDS board which sits on an Arduino Due. A 32bit Arm solution with the RAPS stepper drivers (1/128 steps) - quite a smooth and quiet though powerful solution. (For info visit [doku.radds.org - English Archive - RADDS Electronics for 3D Printer](http://doku.radds.org/en/)). The board is running Repetier Firmware so things connect very well. With the latest version of the server and the firmware there's also quite a nice percentage bar on the display that shows the state of the print.



I don't know if you know the Repetier Server well but you could just continue using Repetier Host and connect it to the server. This way you can use the Host as you ever did before but you can quit it after starting a print as the server will handle everything. Quite a nice solution. Also it cares about the webcam I attached to the printer (totally forgot to show this too - should add that later). This is the other USB cable connected to the Cubietruck. Btw for low cost you could also use a Rapsberry Pi which is much cheaper, not exactly that powerful but that doesn't matter yet (though it may matter when the Repetier slicer is on the server too). Also the Pi is overall a bit of an easier setup as the cubietruck's software isn't that frequently updated and not as good documented. Also there's a downloadable Pi image for the Repetier Server with everything done already.


---
**Ted Huntington** *November 08, 2016 19:25*

very cool - thanks for explaining all that. I wish the RADDS was an all-in-one solution, but not a big deal- I guess RADDS is like a 32-bit RAMPS. Yeah I'm sure that a Banana Pro and other cheap ARM boards could work too- the Raspberry Pi is probably the easiest and problem-free implementation. I am not too familiar with RepetierHost server, but I presume that a single PCB and instance can serve 2 or more 3D printers via WIFI- and that is really cool if true. 


---
**Frank “Helmi” Helmschrott** *November 09, 2016 08:01*

**+Ted Huntington** what do you mean by All-in-one? One Board? Well actually that doesn't make a real difference after you've put it under the hood anywhere on the bottom of your Eusthatios. Personally I do prefer slim, compact solutions too but in this case it was just technically the best and to be honest: There's enough room in this printer and once screwed in you don't see a difference. And yes it is kind of a 32-bit RAMPS but it's also well packed with features.



To be clear about the Repetier Toolchain:



Repetier Host: Windows/Mac/Linux Desktop tool to slice prints (with Slic3r or Cura) and control a printer/print.



Repetier Server: Background Daemon with Web Interface runs on Windows, Mac and Linux (and Rapsberry Pi, Cubietruck etc.) currently to control a print/printer, later also for slicing. Can be connected to from a Desktop Host



Repetier Firmware: Well the firmware. All other tools can connect to other Firmware (Marlin, Smoothieware, etc.) too but Repetier Firmware has some features which makes it work a bit better with Host/Server.



The Server can indeed server multiple printers - actually it is made for that. It can even connect and auto detect multiple Webcams from the newest versions so you can setup each printer with a webcam for remote control. 



I selected to go with a single instance of server for each of my two printers. The Eusthatios got the cubietruck inside, the Prusa i3mk2 got a Raspberry Pi including Pi cam.


---
**Zane Baird** *January 06, 2017 19:49*

I'm very curious about your magnetic hotend holder. I was mulling this idea over myself the other night as I'm working through the design of a new carriage for the Herculien that would allow for a rapid change of hotends.


---
**Frank “Helmi” Helmschrott** *January 06, 2017 19:59*

To be honest I'm unsure and I'll probably not do it again. It's not bad but i think it's missing the perfect mount for a hotend although the magnets are quite good. Whenever there's rapdid infill movement i think it doesn't withstand the forces a 100%. I really can't tell for sure but as I will definitely have to redraw the carriage i'll most probably go with a screw mount again. Even though I plant to have the hotend swappable it won't happen too often so screwing is okay for me.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/ZCLFZ7t5qX9) &mdash; content and formatting may not be reliable*
