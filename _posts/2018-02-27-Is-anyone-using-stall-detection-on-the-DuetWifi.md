---
layout: post
title: "Is anyone using stall detection on the DuetWifi?"
date: February 27, 2018 01:22
category: "Discussion"
author: Oliver Seiler
---
Is anyone using stall detection on the DuetWifi?

I've just set it up for logging only and tweaked it to not get any false alerts.



I'm using 

M915 P0:1 S3 F1 H400 R1



[https://duet3d.dozuki.com/Wiki/Stall_detection_and_sensorless_homing](https://duet3d.dozuki.com/Wiki/Stall_detection_and_sensorless_homing)



Update: I just had it trigger on a print and it saved the day. It works really well (using R2, which pauses the print, R3 will re-home and continue)





**Oliver Seiler**

---
---
**Ryan Fiske** *February 27, 2018 01:58*

This was one feature that I really liked in the prusa mk3, interested to see how useful it is in practice!


---
**Jeff DeMaagd** *February 27, 2018 04:22*

I have mine dialed up to flag and log suspected stalls, actual stalls haven’t happened. Word is sensorless homing works within a specific range of belt tension, so it might require some tuning. I think my cars might have too much drag for it to work.


---
**Oliver Seiler** *February 27, 2018 06:02*

I'm not wanting to use it for homing, just stall detection during printing. I do print a lot of PET CF and that tends to build up on the nozzle and sometimes catches...


---
**Jeff DeMaagd** *February 27, 2018 12:50*

What nozzle do you use? I find that Micro-Swiss nozzles are pretty good at resisting PETG booger buildup.


---
**Oliver Seiler** *February 27, 2018 19:04*

I'm using E3D nozzles. Normal PET(G) is totally fine, it's only XT-CF that builds up (on a hardened 0.4 nozzle)


---
**Oliver Seiler** *February 27, 2018 19:04*

Would be interesting to see if the Olsson Ruby is any better with that....




---
**Jeff DeMaagd** *February 27, 2018 19:37*

Ruby might be OK. The thing is partly the geometry and Ruby's tip seems an OK shape and size. I just consider the E3D style geometry to be too blunt, a finer tip seems to resist boogers.


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/ZerXjbkYv1E) &mdash; content and formatting may not be reliable*
