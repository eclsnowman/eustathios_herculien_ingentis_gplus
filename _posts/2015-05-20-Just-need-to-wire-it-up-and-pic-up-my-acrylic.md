---
layout: post
title: "Just need to wire it up and pic up my acrylic"
date: May 20, 2015 16:40
category: "Show and Tell"
author: Derek Schuetz
---
Just need to wire it up and pic up my acrylic 

![images/46e508362f6f454db6acb994a489ae2a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/46e508362f6f454db6acb994a489ae2a.jpeg)



**Derek Schuetz**

---
---
**Bruce Lunde** *May 20, 2015 17:25*

Looks great!


---
**Eric Lien** *May 20, 2015 17:26*

NightRider, black on black. It's the Eustathios ninja edition ;)


---
**Gus Montoya** *May 20, 2015 17:47*

Nice, I still need my build plate and wiring. 


---
**Derek Schuetz** *May 20, 2015 19:01*

wiring is so dam time consuming


---
**Isaac Arciaga** *May 20, 2015 19:29*

That looks sexy. The black lead screws are a nice touch too.


---
**Gus Montoya** *May 23, 2015 08:22*

Can you email me how much your acrylic came out to. I was quoted $250. I fell the its way too much. That was the cheapest quote I could get.


---
**Derek Schuetz** *May 23, 2015 09:06*

$200, which includes the sides


---
**Derek Schuetz** *May 23, 2015 09:07*

I'm cool with the guy though 


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/X9AmFJd2tkZ) &mdash; content and formatting may not be reliable*
