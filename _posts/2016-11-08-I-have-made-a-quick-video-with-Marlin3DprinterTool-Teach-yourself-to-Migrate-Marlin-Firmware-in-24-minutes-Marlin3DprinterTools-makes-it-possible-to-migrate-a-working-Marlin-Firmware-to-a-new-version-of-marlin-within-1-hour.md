---
layout: post
title: "I have made a quick video with Marlin3DprinterTool \"Teach yourself to Migrate Marlin Firmware in 24 minutes\" Marlin3DprinterTools makes it possible to migrate a working Marlin Firmware to a new version of marlin within 1 hour"
date: November 08, 2016 16:35
category: "Show and Tell"
author: Johnny Lindén
---
I have made a quick video with Marlin3DprinterTool

"Teach yourself to Migrate Marlin Firmware in 24 minutes"



Marlin3DprinterTools makes it possible to migrate a working Marlin Firmware to a new version of marlin within 1 hour. The video is shorter but shows what kind of help the tool gives.

New version of Marlin have obsolite features and some new. The tool only helps find the same features in the two different versions. You still have to implement the new features and even translate old featurenames to new names. Most of the features are the same and the tool gives you possibility to find all features that was activated and check the values









**Johnny Lindén**

---
---
**Abrar Ashfaq** *November 08, 2016 17:01*

Wow...


---
**Markus Granberg** *November 08, 2016 17:25*

Bra jobbat!!


---
**Johnny Lindén** *November 08, 2016 21:10*

Tack **+Markus Granberg** 


---
*Imported from [Google+](https://plus.google.com/102707506187095306417/posts/PPT6M7mseSS) &mdash; content and formatting may not be reliable*
