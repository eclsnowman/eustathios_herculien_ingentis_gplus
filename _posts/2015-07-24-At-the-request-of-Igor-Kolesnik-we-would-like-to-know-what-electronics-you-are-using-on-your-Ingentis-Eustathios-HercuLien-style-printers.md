---
layout: post
title: "At the request of Igor Kolesnik we would like to know what electronics you are using on your Ingentis / Eustathios / HercuLien style printers"
date: July 24, 2015 14:18
category: "Discussion"
author: Eric Lien
---
At the request of **+Igor Kolesnik**  we would like to know what electronics you are using on your Ingentis / Eustathios / HercuLien style printers. I personally go with Panucatt Devices controllers like the Azteeg X3, and X5 Mini because my personal belief is Roy makes the best quality boards out there. But then again I have not tried everything, and peoples mileage may vary from my own.



If you use different controllers please put your opinions in the comments below about what you like, and what you think could be improved.



For example I love my Azteeg X5 Mini, but it could really benefit from more IO like the X3 has (Though I hear Roy has something in the works).





**Eric Lien**

---
---
**Mike Thornbury** *July 24, 2015 14:43*

I don't have any of the printers you mentioned -I'm working on my own design, but in my experience of 3D printers and CNC routers, it doesn't get much better than the Smoothieboard.



Just today I've been pissing around with an MKS/Marlin implementation and I can confidently say "No more!" I fkn hate Marlin and the other Arduino-based controller implementations.



Such a kludge to configure and tune. I have RAMPS boards, Sanguinolos, Melzi, TinyG, Arduino hacks, including Due-based and nothing comes close to Smoothieware for ease of integration and operation.



But, I did just win another one, so I <3 Arthur Wolf, so I am probably biased.﻿



Second place would go to TinyG, but as they only have 4 drivers and don't have integrated FETs, they aren't as useful for 3D printers as they are for CNC


---
**Eric Lien** *July 24, 2015 15:02*

**+Mike Thornbury** That is one reason I really like the X5 mini, because it is smoothie based. But in my opinion the X5 Mini is a better board hardware wise. I like the TI drivers better, It is way more compact, and the integration with the Viki2 panel is simple and a great looking package.



I agree I think Marlin on 8bit 2560 controllers are outdated now, though once the firmware is setup they run fine. They just cannot go fast enough for my taste :)


---
**Alex Lee** *July 24, 2015 15:24*

I just ordered a few of these boards [http://www.geeetech.com/gt2560-3d-printer-controller-board-p-915.html](http://www.geeetech.com/gt2560-3d-printer-controller-board-p-915.html).



I have high hopes for it, the items i'm a bit excited about:



- Cheap - $40

- Accepts 12 or 24V

- 4 PWM outputs (you can do dual extrusion + part fan right out of the box, if you have a single extruder you can set the extruder fan to turn off when the hotend reaches a lower temperature, it was a tip given to me by **+Isaac Arciaga** )

- 3 12V outputs (independent of input voltage)

- FETs with heatsinks

- Built in LCD ports

- Replaceable SMD fuses

- Protected USB port



Thank you to **+Mandy Ellifritz** for letting me know about this board.


---
**Eric Lien** *July 24, 2015 15:41*

**+Alex Lee** So long as the quality is there, that is a great deal! I have heard mixed reviews about Geeetech. Some people say they are great, some people said the board contacts were all dry solder joints (which has me concerned for people using higher wattage heated beds without an SSR).



But looking over the board I like the layout much more than a ramps. Let us know what you think about the quality.



Thanks for the heads up.



Also it is very nice to see you back on G+, we have missed you :)


---
**Alex Lee** *July 24, 2015 15:43*

**+Eric Lien** Awww shucks... LOL!


---
**Mike Thornbury** *July 24, 2015 15:46*

**+Eric Lien** I would like to try an Azteeg board, but they are just too expensive. By the time I add a display and shipping, they are like $350 ea.



I do like the screw connectors, though. I have made my dislike of the pin connectors of the Smoothie fairly well known :)


---
**Eric Lien** *July 24, 2015 15:51*

**+Mike Thornbury** I keep forgetting you are doing this hobby from the boonies. When shipping is twice the cost of an item that really kills the savings of ordering online.



And I couldn't agree more on the connectors. Screw terminals all the way, you will never see flimsy plastic clips in an industrial control panel (like a PLC panel). So I figure if screw terminals are what industry chooses, then they are the right choice for me.


---
**Frank “Helmi” Helmschrott** *July 24, 2015 15:54*

I also already tried loads of electronics in the past. I've been mostly gone with the RUMBA board lately as it offered a shit load of features and great flexibility. What i came across now is RADDS - it's a bit RAMPS like so an additional board but not to the Arduino Mega but to the DUE (ARM based). 



Regarding **+Mike Thornbury** 's complaints about configuration of Marlin i can completely follow - probably you should have tried Repetier instead. It has a Web configurator for the Firmware ([http://www.repetier.com/firmware/v092/](http://www.repetier.com/firmware/v092/)) and most of the settings are EEPROM based and editable on the fly while the machine is running (even while it's printing). It is editable through Repetier Host or web based if you're running Repetier Server (which may run on your local machine, a Raspberry Pi or any other Computer). Unfortunately Information is a bit split across several not so easy overlookable sites ([http://www.repetier.com](http://www.repetier.com), [http://www.repetier-server.com/](http://www.repetier-server.com/) etc) but at the end it's only one product from one vendor and not hundreds of forks like with Marlin.



Regardin the RADDS electronics i'm additionally using their RAPS drivers which are just insane. I just started using them but i haven't seen any drivers (and i've tried them all) that are as silent and as powerful as these 1/128 micro stepping drivers are. Combined with the 0,9°/400 Steps/rev Motors i'm using this is really great action currently. I still have to fine tune extrusion but will post some videos as soon as it's running like i want it. I'll also post some more information about my setup when i'm done.


---
**Mike Thornbury** *July 24, 2015 16:06*

Thanks Frank. I'm not stuck with the concept or  complexity of configuring the Marlin, it's just such a chore. Having to recompile and uplload using ICSP every time you make a minor change is a drag.  I build unix servers from scratch, I can cope with a printer controller. :)


---
**Frank “Helmi” Helmschrott** *July 24, 2015 16:18*

oh well i think i understood you right, that's why i pointed out the EEPROM feature in Repetier. That helps set nearly all "tuning relevant" settings on the fly. But of course you're right, the smoothieware configuration is quite easy too.


---
**Mike Miller** *July 24, 2015 16:45*

RAMPS on one, RUMBA on the other. 


---
**Eirikur Sigbjörnsson** *July 24, 2015 18:19*

My plan is (or were) to use an Aakar Brainboard which I supported on Indiegogo. I'm begging to think they won't deliver though, since the communication is zero. If not, then I'll switch to Azteeg Pro


---
**Brandon Satterfield** *July 24, 2015 18:21*

Ramps, Rambo, smoothie. In that order.


---
**Daniel F** *July 24, 2015 19:15*

I use a RADDS with arduino due and Repetier. RAPS 128 stepper drivers. Although not the cheapest electronics, I like it a lot and the quality is much better than cheap ramps and geetech stepsticks.      The RAPS drivers are really silent and reliable.


---
**Eric Lien** *July 24, 2015 19:15*

**+Brandon Satterfield** a man of many hats... I like it.


---
**Eric Lien** *July 24, 2015 19:17*

**+Eirikur Sigbjörnsson** I haven't seen many posts from you recently. Is your HercuLien variant still coming along?


---
**Joe Spanier** *July 24, 2015 21:05*

Cramps! Running machinekit


---
**Eirikur Sigbjörnsson** *July 24, 2015 23:53*

**+Eric Lien**  Yes I'm mostly done sketching the variant up. Just few loose ends I need to tie up (and one big decision to make). Ive already started ordering parts, but still have some of the  larger orders to complete. The plan is to have this up and running some time before the end of the year :)

The plan is to finish the drawings in Onshape and then I'll give you access to the project so you can look things over and give me your opinions (you do need to have an Onshape account and a powerful computer, this thing is a heavy load especially the main assembly :)). After this is all done I will turn the Onshape document public so everyone has access. By the way, this project is way bigger than the 100mb storage that Onshape will allow for free accounts after their beta program finishes.


---
**Eric Lien** *July 25, 2015 00:15*

**+Eirikur Sigbjörnsson**​​ I have an onshape account, I just haven't tried it much yet. But it looks intuitive.



Nice to know about the size limit. 



And I think my GPU is up to the task. But one of those new Titian cards to replace my old card sure would be nice with my big printer assemblies. I guess I need to stop spending so much on printers and I could have it. ;)﻿


---
**Joe Spanier** *July 25, 2015 00:16*

That must be a hell of an assembly file to "need" a titan lol


---
**Eric Lien** *July 25, 2015 00:31*

**+Joe Spanier** need vs want. My wife says I convince myself I need more stuff than I do.



But it does get a little gittery, because all the bolts in my models have the threads modeled also.


---
**Eirikur Sigbjörnsson** *July 25, 2015 01:40*

Well I'm driving this on an old 5xx twinfrozer card, but I do have 32 GB and I'm using the Intel anniversary pentium overclocked to 4.6 gb


---
**Marcos Duque Cesar** *July 25, 2015 06:56*

CRAMPS and RAMPS


---
**Jo Miller** *August 06, 2015 05:36*

AZSMZ mini board  with X5 mini Firmware  +  DRV8825 stepper drivers


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/fiiS2LCFNDY) &mdash; content and formatting may not be reliable*
