---
layout: post
title: "Have any of you all attempted to make a paste extruder?"
date: October 14, 2015 13:27
category: "Discussion"
author: kaley garner
---
Have any of you all attempted to make a paste extruder?



 Im wondering whats involved with editing the firmware temperature settings or other things to look out for. Any advice would be greatly appreciated. :)





**kaley garner**

---
---
**Miguel Sánchez** *October 14, 2015 16:21*

I did. Marlin can be pleased using either a fixed 47k resistor replacing the thermistor or by removing it and setting to 0 the lower limit on Configuration.h HEATER_0_MINTEMP


---
**kaley garner** *October 14, 2015 17:13*

Cool thanks Miguel, did you remove all of the thermistor components or just change the firmware settings and leave the components attached?


---
**Miguel Sánchez** *October 14, 2015 17:16*

If you leave the thermistor attached then there is no error condition happening. If you remove the hotend thermistor then Marlin will stop working as it detects a MINTEMP condition. It is to prevent it that you would set HEATER_0_MINTEMP to 0. Bed heater will not give you trouble whether it is kept or removed.


---
**kaley garner** *October 14, 2015 17:17*

Awesome, ill give it a try!


---
**Michaël Memeteau** *October 14, 2015 17:56*

- Allowing cold extrusion should suffice (M302);

- According to the nature of the paste you want to extrude you may want to look at a moineau pump ([http://www.thingiverse.com/thing:28237](http://www.thingiverse.com/thing:28237) for example);

- Have a look also at this blog, even for the sake of it, it worth the reading. It utterly documented and informative

([https://0x7d.com/2014/10/3d-printed-precision-paste-dispenser/](https://0x7d.com/2014/10/3d-printed-precision-paste-dispenser/))


---
**Ian Hoff** *October 14, 2015 19:32*

This one get recommended in  #reprap  a bit



[http://www.thingiverse.com/thing:26343](http://www.thingiverse.com/thing:26343)



An air pressure based extruder for chocolate, sugar and etc. If I am remembering my history this is basically the isomalt printers that were being used to study vasculature and bio printing.


---
*Imported from [Google+](https://plus.google.com/104940700298630363140/posts/KfnkmJoTnR3) &mdash; content and formatting may not be reliable*
