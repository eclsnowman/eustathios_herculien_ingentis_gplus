---
layout: post
title: "Hey guys where is the best place to pickup the self aligning bearings now?"
date: June 16, 2014 17:52
category: "Discussion"
author: Joe Spanier
---
Hey guys where is the best place to pickup the self aligning bearings now? Last time I ordered from SPD-SI they charged me like 20$ shipping for 10 bearings, I wasn't a fan. lol





**Joe Spanier**

---
---
**Eric Lien** *June 16, 2014 19:15*

Ultibots


---
**John Lewin** *June 16, 2014 20:29*

Likewise, I probably won't order from them again as I spent $15 on shipping for $19 dollars in parts. Which is made worse by them computing the shipping price as they ship rather than as you order. USPS would have been cheaper and faster and the experience certainly wasn't a good one. **+Stock Drive Products | Sterling Instrument | A Designatronics Company**


---
**Stock Drive Products | Sterling Instrument | A Designatronics Company** *June 20, 2014 12:49*

Greetings **+Joe Spanier**  and  **+John Lewin**! We are sorry that you did not have the best shopping experience on the SDP/SI eStore. We ship most orders by UPS. Shipping cost is based on weight and we charge only what UPS charges us to ship your package - absolutely no handling fees. We are working on a major overhaul of our eStore beginning with realtime UPS shipping quotes prior to check out, which will be released very soon. We appreciate your business and hope we can continue to supply you with the components you need for your future projects. 


---
**John Lewin** *June 20, 2014 14:28*

Good to see you guys are listening and have some improvements on the way. The real rub though is my package weighed almost nothing, seemed to ship slower than USPS  and shipping costs were nearly as much as the actual product. You'd do better to prevent small orders like mine than to produce disgruntled customers that regret their purchase. Right away I need more of the parts I ordered and due to shipping I will be shopping around rather than coming back.


---
**Joe Spanier** *June 20, 2014 15:56*

What he said^.  My order was for 10 1/2" acetal sleeve bearings and my shipping was almost 20$  the box weighed more then the bearings. I also would like to order more as the bearings are great. But I can't afford 50$ for bearings when I can get similar igus bearings with free shipping and same bearing cost. 


---
**John Lewin** *June 20, 2014 20:34*

Since I'm only buying Igus bearings, this free shipping option sounds great. I've looked around but so far haven't found a better option than SDP/SI other than directly from Igus. **+Joe Spanier**  - care to share your source?


---
**Joe Spanier** *June 20, 2014 21:03*

I get free overnight shipping through my work account with Grainger. Sorry that's not one your likely to get in on. 


---
*Imported from [Google+](https://plus.google.com/+JoeSpanier/posts/KfTYkeMXRAD) &mdash; content and formatting may not be reliable*
