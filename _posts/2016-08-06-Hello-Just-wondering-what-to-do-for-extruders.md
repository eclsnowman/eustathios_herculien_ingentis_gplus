---
layout: post
title: "Hello! Just wondering what to do for extruders"
date: August 06, 2016 03:01
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hello! Just wondering what to do for extruders. I'm using a Chimera. I will want to upgrade to a bondtech in the future, but $310 is too much for me to spend on two extruders. I'll be playing with all different types of filament and I want something cheep that won't jam. Should I just go with the hercustruder? I kinda forgot to order the parts for it though, so shipping may be a bit much. I also want to use the same motor (and preferably drive gear) of the bondtech so I can upgrade just one extruder at a time. Thanks! - Stefano





**Stefano Pagani (Stef_FPV)**

---
---
**Zane Baird** *August 06, 2016 03:38*

The last place you should cut costs is on an extruder (in my opinion). It dictates print quality more than anything. Stick with one extruder for now. Don't dive into dual extrusion until you have your print quality absolutely perfect. 


---
**Michaël Memeteau** *August 06, 2016 11:05*

Can't agree more with the comment from **+Zane Baird**. Give the #Saintflint a try (two in this case). It's the closest you'll get to a Bontech for almost nearly zero buck. It's not only me saying so: [http://www.bajdi.com/3d-printer-extruders/](http://www.bajdi.com/3d-printer-extruders/)


---
**Stefano Pagani (Stef_FPV)** *August 06, 2016 18:07*

Ok, I'll still use the Chimera, I'll just leave one nozzle off for now. Thanks for the advice!




---
**Stefano Pagani (Stef_FPV)** *August 06, 2016 18:11*

Wow the saintflint is an amazing design! I think I'll go with that


---
**Michaël Memeteau** *August 06, 2016 19:40*

One more trick: once the filament is in place in the PTFE, you can use hot-glue to maintain the screw at the end of each PTFE section in place in the two halves of the Saintflint. That should turn the loading of filament easier...


---
**Mutley3D** *September 11, 2016 16:03*

**+Stefano Pagani** I have just seen this post about your dual filament predicament. How are you getting along on your quest for drives? Not sure if it will interest you but I have just launched a dual direct drive Flex3Drive specifically for the Chimera Cylops hotend.


---
**Stefano Pagani (Stef_FPV)** *October 04, 2016 01:51*

**+Mutley3D** I saw your product, it looks amazing! I think I will stick with Bowden for now, but next year I will purchase your's, I want to do some 100% infill clear prints later, but for now I just want speed. (Plus I have no more budget)


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/WhXiuc6eJkM) &mdash; content and formatting may not be reliable*
