---
layout: post
title: "Looks like I managed to solve my long lasting problem by just adding some thermal grease"
date: March 20, 2017 18:31
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Looks like I managed to solve my long lasting problem by just adding some thermal grease. I'm a bit buffled how big the difference is but all that glogging/jamming I had before seems to be gone. At least with the first test which is the PLA that caused me the biggest problems so far. This print isn't perfect yet but that needs just a little bit of temp tuning I guess. Amazing to see that the new carriage seems to work better than I thought without the extrusion chaos.



Thanks for all your input guy. That motivated me enough to just add another few hours into testing and un/remounting the hotend.

![images/cac5c90d990ef1fb9084e567bd27d735.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cac5c90d990ef1fb9084e567bd27d735.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**Øystein Krog** *March 20, 2017 20:08*

What kind of thermal grease did you use?

I've been trying to find one rated for the high temperature.


---
**Frank “Helmi” Helmschrott** *March 20, 2017 20:15*

To be honest just one I had around. Don't know how it is rated but what could possibly happening apart from this drying out :)



So far nothing happened. Will see how the first longer print goes that I will have running the next 12 hours or so.


---
**Øystein Krog** *March 20, 2017 20:56*

Hehe, I did the same a while ago and so far it's been fine


---
**James Rivera** *March 20, 2017 21:35*

Nice looking Benchy boat!


---
**Jeff DeMaagd** *March 21, 2017 03:44*

For the upper end of the heat break, it doesn't need to be anything special, normal CPU thermal paste is fine. For anything touching the heater block, you can use automotive anti-seize lubricant. The kind I use is good for several hundred degrees Celsius. I use a copper-based one that is rated for over 900˚C. Use it sparingly and run your initial warm-up in a well-ventilated area.


---
**Frank “Helmi” Helmschrott** *March 21, 2017 05:42*

Using thermal grease on the lower end doesn't seem logical to me, should it? 


---
**Jeff DeMaagd** *March 21, 2017 12:03*

Not for thermal but to prevent parts from fusing together from heat and corrosion.


---
**Frank “Helmi” Helmschrott** *March 21, 2017 12:07*

hmm ok, that hasn't been a problem for me in the past but thanks for the hint.




---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/ejw4CdNV1H8) &mdash; content and formatting may not be reliable*
