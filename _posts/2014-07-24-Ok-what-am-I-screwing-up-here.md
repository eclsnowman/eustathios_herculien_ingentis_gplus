---
layout: post
title: "Ok - what am I screwing up here?"
date: July 24, 2014 05:37
category: "Discussion"
author: Tony White
---
Ok - what am I screwing up here? The belt can slot nicely in on one end, but how is the other end then attached?



Also, do I have something flipped around the wrong direction in the second pic? - the belt interferes with the carriage...



![images/faecdc4f1ca16bf6445382ffabe8924e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/faecdc4f1ca16bf6445382ffabe8924e.jpeg)
![images/e13a12cf7ab6c008f72103a0922c2149.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e13a12cf7ab6c008f72103a0922c2149.jpeg)

**Tony White**

---
---
**ThantiK** *July 24, 2014 05:39*

Looks like you didn't print the mirrored version.  There are two different XY holders.  You should have a pair of each.


---
**Tony White** *July 24, 2014 05:40*

I've got 2 pairs - maybe I didn't pair them with each other appropriately. That might fi the interference issue - what about the belt termination?


---
**ThantiK** *July 24, 2014 05:46*

There is a separate printable block that has teeth. You slot belt in, then attach with screws if I remember correctly. 


---
**Mike Smith** *July 24, 2014 16:37*

I believe **+ThantiK .** is correct. But the belt end you're holding goes into the slot that the other belt end is clamped in then the belt end that would be on the right in your picture goes into the "floating Block" that gets bolted to the carrier. 


---
**ThantiK** *July 24, 2014 18:34*

**+Mike Smith** I'm actually thinking of redoing mine, and jacking the belt retainer design from your Y carriage on the i3.  Seems like it would be much more elegant.


---
**Mike Smith** *July 24, 2014 19:20*

**+ThantiK .** I was thinking the same thing. and maybe making some sort of tensioner that would push the bearing block into the corner of the frame...might also make it easier to align the smooth rods.


---
*Imported from [Google+](https://plus.google.com/+AnthonyWhiteMechE/posts/Nzc4pL2ZyZA) &mdash; content and formatting may not be reliable*
