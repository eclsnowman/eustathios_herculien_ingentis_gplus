---
layout: post
title: "Eric I need your expertise, hope you can help"
date: January 22, 2017 08:26
category: "Discussion"
author: Brandoan (Rex)
---
Eric I need your expertise, hope you can help. I have designed a core xy system but I am having trouble with layer shifting and print surface ossicillation at speeds beyond 50mm/s ... my system uses two separate belt lengths on different planes .. so not a single belt that crosses in the back. I also wonder what should my belt tension should be. Also what are the best tips you could offer to me to help me double check my design. I will post some pictures shortly for review. Thanks very much for any help you or anyone else could provide.



-Rex





**Brandoan (Rex)**

---
---
**Eric Lien** *January 22, 2017 19:17*

Let's start with pictures of the design, then pictures of the prints. That will give the group a frame of reference to begin troubleshooting.


---
**Eric Lien** *January 22, 2017 19:18*

You can insert pictures in comments now on G+ so just add them into this thread.


---
**Eric Lien** *January 25, 2017 05:40*

**+Brandon D** ? Just making sure you saw my replies?


---
**Brandoan (Rex)** *March 16, 2017 06:52*

![images/ddad7ce6d192dfa1c9344111738ee86b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ddad7ce6d192dfa1c9344111738ee86b.jpeg)


---
**Brandoan (Rex)** *March 16, 2017 06:53*

![images/1968dde7efa377049e5e37b2e07ff659.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1968dde7efa377049e5e37b2e07ff659.jpeg)


---
**Brandoan (Rex)** *March 16, 2017 06:54*

![images/b7232463730db27030b5a3fa369e5b5a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b7232463730db27030b5a3fa369e5b5a.jpeg)


---
**Brandoan (Rex)** *March 16, 2017 06:55*

![images/e90668ab2a2170081fa50f4f707760c6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e90668ab2a2170081fa50f4f707760c6.jpeg)


---
**Brandoan (Rex)** *March 16, 2017 06:56*

![images/63eafb19032b0d66b8a0d7d2a705089d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/63eafb19032b0d66b8a0d7d2a705089d.jpeg)


---
**Brandoan (Rex)** *March 16, 2017 07:03*

Thanks Eric for any help you can offer... Just can't get this to run fast and diagonal moves make a giant racket. I have push the carriage to the rear and loosened everything to get the alignment of the X Axis right but to no avail... I feel like this CORE XY should be killing it.. but I am stuck at low speeds. Also what is the easiest way to get my belt tensions uniform between the two belts - I bought one of these deflection belt tensioners but it doesn't seem to work for long belted systems like CORE XY... Thanks Eric for any help you can offer.




---
**Brandoan (Rex)** *March 17, 2017 05:47*

**+Eric Lien** Hope these give you enough insight into my project?


---
**Eric Lien** *March 17, 2017 14:10*

My first thoughts for noise is resonance of the large flat panels. That coupled with any misalignment of the side roads causing chatter.


---
**Brandoan (Rex)** *March 18, 2017 01:06*

Hey you wanna do a video conferemce


---
**Eric Lien** *March 18, 2017 01:18*

I could do it, but this weekend is really booked up. Can we do it next week?




---
**Brandoan (Rex)** *March 18, 2017 01:57*

In one of your videos you talk about a break in script


---
**Brandoan (Rex)** *March 18, 2017 01:57*

Can you link me to that


---
**Brandoan (Rex)** *March 18, 2017 05:30*

Eric we have a .012 inch differential on our Y rods front to back. We get the whale sounding noises from our igus bearings a lot and we get a lot of noise when moving in diagonal directions... The x rods are out of whack by about .016 inches... We are running the breaking script and attempting to shim.. Any suggestions? 


---
**Brandoan (Rex)** *March 18, 2017 05:39*

Let me know what day next week works for you, thanks Eric.


---
**Brandoan (Rex)** *March 22, 2017 18:17*

Hey Eric can you do a video conference today?!? We are available




---
**Brandoan (Rex)** *March 22, 2017 18:20*

**+Eric Lien** Hey Eric do you have time to consult we are between a rock and a hard place? I have a short window to fix this issue and have people depending on me. I am very willing to pay you for your time. Me and my two colleagues are stuck on our issue.




---
**Eric Lien** *March 22, 2017 19:39*

**+Brandoan** unfortunately until next week I am just slammed. No need to pay me, I enjoy the challenge as my hobby. But between work, two projects for guys in the community, and two young kids my time is spread so thin it's transparent.


---
**Brandoan (Rex)** *March 22, 2017 23:15*

Does Monday or the weekend work?




---
**Brandoan (Rex)** *March 22, 2017 23:21*

I can send you a meeting invite if you can tell me your email or post here for you to join?


---
**Eric Lien** *March 23, 2017 02:20*

**+Brandoan** this weekend I will be at MRRF in Goshen Indiana. If you came out to that we could talk printers till the cows come home. But otherwise I could do a late night next week. Hit me up on Google Hangouts. 


---
**Brandoan (Rex)** *March 23, 2017 18:17*

Perfect I will hit you up Tuesday Night, thanks Eric.. Have fun in Goshen. I appreciate your communication a lot.






---
**Brandoan (Rex)** *March 30, 2017 17:13*

**+Eric Lien** Hope you had a good trip any chance we can schedule some thing for Monday?


---
**Eric Lien** *March 30, 2017 18:01*

Sure, hit me up on Hangouts, lets plan on Monday at 9pm Central (That way it is after I get the kids to bed).




---
**Brandoan (Rex)** *April 06, 2017 21:03*

Eric here is exactly the design we are trying to validate for great CORE XY motion...

![images/31a44a60388971c4837f088bf78e4481.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/31a44a60388971c4837f088bf78e4481.jpeg)


---
**Brandoan (Rex)** *April 06, 2017 21:03*

![images/8225beece0d1744c75ac84081ec94323.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8225beece0d1744c75ac84081ec94323.png)


---
**Brandoan (Rex)** *April 06, 2017 21:03*

![images/8b6542e4ccd608af90347cae303c8509.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8b6542e4ccd608af90347cae303c8509.png)


---
**Brandoan (Rex)** *April 06, 2017 21:04*

![images/0febeb497bc047359baa536ad5ac72fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0febeb497bc047359baa536ad5ac72fa.jpeg)


---
**Brandoan (Rex)** *April 06, 2017 21:13*

A couple quick questions ... We have set this up so we can test attaching the belts to the center, one on top and one on bottom of the print carriage on each side but we can also configure this setup to connect the belts to the left and right sides of the carriage one on top and one on bottom. The pictures depict the centerline configuration. What are your thoughts on this. Also we are using 59oz inch capable holding torque motors with .9 degree step motors that are micro-stepped at 1/16th with a Trinamic 2100 stepper... Should we be using stronger motors? The build area is 12x12x12 and the total length of each belt is about 8 feet. We are using a 32 tooth drive pulley and our pulley stacks that act as idlers are F625ZZ bearings (5x16x5mm).. We have been using IGUS Bearings and solid anodized aluminium rods. 



What tips could you offer us to ensure our components fit together perfectly and don't cause any binding on the rods... 



Look forward to your response. Also I would love to do a video session tomorrow night if you available.



-Brandon  


---
**Brandoan (Rex)** *April 06, 2017 21:14*

**+Eric Lien**  If you can do tomorrow night at 9pm great if not whatever you propose we will be happy to work with.


---
**Eric Lien** *April 06, 2017 21:49*

**+Brandoan** tonight would work for me, tomorrow I am heading out of town. I was around on Monday but never saw a Hangouts request to chat.


---
*Imported from [Google+](https://plus.google.com/116174312368035630667/posts/MuFF7T2HSNn) &mdash; content and formatting may not be reliable*
