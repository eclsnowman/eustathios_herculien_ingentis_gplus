---
layout: post
title: "Alright, you've waited long enough is officially a plotter"
date: September 27, 2014 15:51
category: "Show and Tell"
author: Mike Miller
---
Alright, you've waited long enough  #IGentUS  is officially a plotter. Using **+MISUMI USA** extrusions and **+igus Inc.**   #Drylin   bushings, it's making lines and turning circles. 



Straight lines and round circles.



Each flat of the big nut in the video is exactly 13.5 cm wide, and the circle measures 10.35cm in one direction and 10.45 cm in the other. At this stage of the build, I'll take that kinda precision. ( 0.9% ) 


**Video content missing for image https://lh4.googleusercontent.com/-NmTIGxggJLU/VCbc7iqf2bI/AAAAAAAAH3E/kJXQliIHQgU/s0/Plotter%252521.mp4.gif**
![images/7d4051c164ee42dee554b1061f156d42.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7d4051c164ee42dee554b1061f156d42.gif)



**Mike Miller**

---
---
**Mike Miller** *September 27, 2014 16:12*

Only grease on the rods is dust. :) I suspect it works well enough as the GT2 belts provide rigidity, and with the bushing being slightly oversized to the rod, theres a very small contact patch on each bushing.


---
**Mike Miller** *September 27, 2014 16:21*

I mentioned that in the video..it's not symmetrical because there's a thread running down the center of the object. It's being plotted accurately to the model. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/LEx4fzmU2hk) &mdash; content and formatting may not be reliable*
