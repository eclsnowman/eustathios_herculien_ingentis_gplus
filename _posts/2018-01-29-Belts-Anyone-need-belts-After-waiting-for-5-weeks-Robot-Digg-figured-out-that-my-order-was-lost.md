---
layout: post
title: "Belts! Anyone need belts? After waiting for 5 weeks, Robot Digg figured out that my order was 'lost'..."
date: January 29, 2018 19:06
category: "Discussion"
author: Dennis P
---
Belts! Anyone need belts?



After waiting for 5 weeks, Robot Digg figured out that my order was 'lost'... so I am resourcing a few things. 



A regional US supplier for belts I found is reasonable if you consider how they are made- the endless best are extruded like a giant piece of macaroni. Then the tube is sheared at the proper width of the belt that you order. B&B in LaPorte, IN. So the little bit extra goes to help the local economy I guess. 2-3 day lead time to be safe. 



[https://www.bbman.com/catalog/category/timing-belt](https://www.bbman.com/catalog/category/timing-belt)



Thing is that they have a minimum order of $35. The 186mm belts are ~$4, the 986mm belt ~$7.  So to hit the minimum,  its basically 2+ sets of belts.  I did not check the prices for the belt sizes for the HercuLien, but I would assume on the same order.  



Anyone interested in a spare belt or 2? Otherwise I will order enough and take my chances and ebay them... 



Dennis





**Dennis P**

---
---
**Dennis P** *January 29, 2018 21:09*

Anyone tried skiving and cementing a GT2 belt? Anyone wanna be a Beta tester with me? 



I figured with extra belting, all I have to do is skive & glue to get me something to test our electronics with while I wait and figure things out.



[https://drive.google.com/open?id=1hasJ1TsWFN8U_CUVEkZir9sRTVL-NjLF](https://drive.google.com/open?id=1hasJ1TsWFN8U_CUVEkZir9sRTVL-NjLF) 


---
**Eric Lien** *January 30, 2018 03:02*

Do they make gt2 belts, or just MXL.


---
**Dennis P** *January 30, 2018 03:54*

it looks like they make a lot of timing belts, and only timing belts. imperial and metric. pulley stock, tensionsers, idlers,  ploy-v... MXL, XL, L, H, XH, XXH pitches, powerhouse and powerhouse HTD, GT2, 3, 5, 8 &^ 14... i mean a LOT of belts. when i called, the rep explained to me why the minimum and the process. the minimum cover the guy walking out to the floor,  pulling the belt/tube stock and then cutting to size.  


---
**Jeff DeMaagd** *January 30, 2018 04:21*

I will have to keep them in mind. I’ve seen them before in my search for unusual belts but I didn’t understand them.


---
**Dennis P** *January 30, 2018 06:24*

The skiving experiment seems to have worked well enough that I can turn the motors and move things. The skiving part was easy, sanding drum on a dremel made short work of it all. I used super glue to join the ends and that joint is a little stiff. Alignment is critical, so print out the guide plate and clamp it carefully in the plate to align the teeth. 


---
**Dennis P** *January 31, 2018 01:28*

Skiving attempt V2.0 worked much better- its critical that you test fit everything i the alignment block before you glue. i used superglue gel pen, but i think any thicker glue will work. I also sprayed a little kitchen spray on the block to release in case of glue leakage. So I have 2 hort belts for X & Y to test mechanics out. 


---
**Dennis P** *February 04, 2018 08:43*

Skiving Jig Version 2.0

![images/24aa8d883b28e2428659ec174f25eccc.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/24aa8d883b28e2428659ec174f25eccc.png)


---
**Dennis P** *February 04, 2018 08:43*

Gluing Jig V2.0

![images/70cd2ee594035616bb23aee7955bfa71.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/70cd2ee594035616bb23aee7955bfa71.png)


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/8DxBtVH8Ggf) &mdash; content and formatting may not be reliable*
