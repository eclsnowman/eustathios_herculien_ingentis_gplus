---
layout: post
title: "Now it is my turn to show off"
date: August 26, 2015 21:59
category: "Show and Tell"
author: Igor Kolesnik
---
Now it is my turn to show off. Finally something is moving. Running Break In gcode on 3k. Smooooth)))) 


**Video content missing for image https://lh3.googleusercontent.com/-C4Gsy56PkJg/Vd42t4XP3fI/AAAAAAAABvo/7CEYS34j0HY/s0/VID_20150826_174350.mp4.gif**
![images/ebd7869b6144ca833ede49a23d964473.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ebd7869b6144ca833ede49a23d964473.gif)



**Igor Kolesnik**

---
---
**Eric Lien** *August 26, 2015 23:12*

Alright! Another Eustathios is born :)



You work quickly! And I sure do envy all you guys with ball screw Z axis. Wish I had done that.


---
**Igor Kolesnik** *August 26, 2015 23:21*

I am def getting the azteeg. I will make it a beast. This drivers can handle only 1/16 and I want it quiet. But first I am getting bondtech


---
**Eric Lien** *August 27, 2015 00:45*

**+Igor Kolesnik** which controller are you using now?


---
**Igor Kolesnik** *August 27, 2015 01:56*

RAMBo. It is fine for now but my inner perfectionist says that it can be better﻿


---
**Eric Lien** *August 27, 2015 04:21*

Yeah Rambo is for sure a nice board. But when you get going really fast you can get stutters on 8bit boards. So smoothieware is nice for the speed demon in me :)


---
**Hendrik Wiese** *August 27, 2015 05:56*

Am I the only who can't watch the video? "An error occurred, please try again later"


---
**Øystein Krog** *August 27, 2015 17:38*

**+Hendrik Wiese** It works for me, but generally speaking I've often had problems with videos here.


---
*Imported from [Google+](https://plus.google.com/+IgorKolesnik/posts/AknLLrPGSz7) &mdash; content and formatting may not be reliable*
