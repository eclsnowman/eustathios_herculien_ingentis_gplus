---
layout: post
title: "Anyone know whats about with item 20.20 on the Spider V2 BOM (#90545A009)?"
date: April 22, 2016 18:51
category: "Discussion"
author: Sean B
---
Anyone know whats about with item 20.20 on the Spider V2 BOM (#90545A009)?  It has no description, and according to McMaster Carr is a Titanium Hex Nut, 8-32 Thread Size, 3/8" Width, 1/8" Height.  It's under the HercuStruder section.







**Sean B**

---
---
**Brandon Cramer** *April 22, 2016 19:10*

Under the description I have on my Eustathios Spider V2 BOM, it shows: 8-32 Titanium Hex Nut. 


---
**Sean B** *April 22, 2016 19:11*

Is that accurate?  What would we need a titanium 8-32 nut?




---
**Jason Smith (Birds Of Paradise FPV)** *April 22, 2016 19:21*

My guess would be that it's used to thread onto the Bowden tube to attach to the hercustruder, but I'm not positive about that. 


---
**Eric Lien** *April 22, 2016 19:32*

No need for titanium. And go with a 4mm nut. It holds better. Sorry for the BOM issue.


---
**Erik Scott** *April 22, 2016 19:55*

If you don't like the idea of threading the Bowden tube into a nut, you can use a different method. I created an adapter for the attachment that comes with the Bowden e3d hotends. You may be able to find the old post somewhere in this community. If you can't, but are still interested, I can get you the files when I get home from work.



I still used the nut and threaded tube on the bottom section where the filament enters the extruder, but I didn't mind marring a piece of scrap tube. Can't remember which I used for that, an M4 or an 8-32 nut, as I have both types kicking around.


---
**Jim Stone** *April 22, 2016 21:35*

**+Erik Scott**

 this thing? [https://www.dropbox.com/s/z153yzn1jmseqhl/Eustathios%20-%20Extruder%20Bowden%20Push-fit%20Adapter.stl?dl=0](https://www.dropbox.com/s/z153yzn1jmseqhl/Eustathios%20-%20Extruder%20Bowden%20Push-fit%20Adapter.stl?dl=0)


---
**Erik Scott** *April 22, 2016 23:03*

**+Jim Stone** Yup, that's the one. 


---
**Jim Stone** *April 22, 2016 23:04*

I use it on my herculien. Much preferred over the nut  and since we buy the e3d anyways we might as well use it


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/bSq4AiyWFNZ) &mdash; content and formatting may not be reliable*
