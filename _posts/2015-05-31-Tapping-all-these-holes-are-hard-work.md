---
layout: post
title: "Tapping all these holes are hard work"
date: May 31, 2015 19:51
category: "Discussion"
author: Dat Chu
---
Tapping all these holes are hard work. 😣 The hole drilling is also very time consuming. Good thing the drill guide was very well designed. It would have taken much more time measuring. 



I wish there is a service for cutting aluminum and tapping them so I just can get these predone. The fun part is the wiring up. ☺ Soon. 

![images/ddbcd95400b50f45be47708bf618dda9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ddbcd95400b50f45be47708bf618dda9.jpeg)



**Dat Chu**

---
---
**Mutley3D** *May 31, 2015 19:55*

I usually put my tap into a drill and use a drop of cutting oil prior to tapping each hole.


---
**Dat Chu** *May 31, 2015 20:01*

What cutting oil do you use? I am using honing oil solution and it works well with tap bit in the drill but I am not sure if that is the best. 


---
**Eric Lien** *May 31, 2015 20:05*

Misumi can do it, but with all the holes the cost goes up a bit. 


---
**Eric Lien** *May 31, 2015 20:06*

**+Dat Chu** great to see this thing taking shape.


---
**Dat Chu** *May 31, 2015 20:18*

Me too. I am glad I finally find some time to start on this. 


---
**Isaac Arciaga** *May 31, 2015 20:21*

**+Dat Chu**​ Tap Magic is what I use for a tapping lubricant. Harbor freight also sells tapping oil that has  good reviews.


---
**Eric Lien** *May 31, 2015 20:25*

The hard holes are where the 10mm rods go through the frame. They are half on an internal rib. I recommend a drill press, and an end mill bit, or a hole saw. Bit walk is a bear with a standard 1/2" tapered tip bit.﻿


---
**Dat Chu** *May 31, 2015 20:34*

Yeah, it looks like I need to finally get my own drill press. My only problem is I have no work bench in the garage for the drill press. Now I have to get recommendations for a work bench in the garage.


---
**Eric Lien** *May 31, 2015 20:39*

I have this bench, and I love it: [http://www.samsclub.com/sams/ultra-heavy-duty-12-drawer-rolling-workbench/prod1480010.ip?navAction=](http://www.samsclub.com/sams/ultra-heavy-duty-12-drawer-rolling-workbench/prod1480010.ip?navAction=)


---
**Dat Chu** *May 31, 2015 21:18*

That looks awesome. I am thinking of getting this one since I don't need the drawers [http://www.costco.com/NewAge-Workbench-with-Bamboo-Work-Surface.product.100159806.html](http://www.costco.com/NewAge-Workbench-with-Bamboo-Work-Surface.product.100159806.html)


---
**Dave Hylands** *June 01, 2015 01:03*

You need the drawers. You just don't know it yet.


---
**Mike Thornbury** *June 01, 2015 01:22*

Make your workbench. Two sheets of ply and a top sheet of mdf and a tube of glue. You will be able to afford a nice end vice and still come out less than half the cost of that 'workbench'.



As to tapping holes, don't bother, just buy self-tapping bolts. I tapped about five holes in my aluminium extrusion life, then went self-tapping and have never had a problem in the hundreds and hundreds of ends I have tapped, since.


---
**Isaac Arciaga** *June 01, 2015 02:50*

Here's a popular one. You can beat it up and not feel bad about it. Paint/Stain it if you want to make it look prettier. [http://www.homedepot.com/p/Unbranded-Fold-Out-Wood-Workbench-Common-72-in-Actual-20-0-in-x-72-0-in-WKBNCH72X22/203083493](http://www.homedepot.com/p/Unbranded-Fold-Out-Wood-Workbench-Common-72-in-Actual-20-0-in-x-72-0-in-WKBNCH72X22/203083493)



I agree with **+Mike Thornbury** the money you save, you can put towards tools.


---
**Joe Spanier** *June 01, 2015 13:03*

If you want to throw up a really quick bench these things are great [http://www.menards.com/main/building-materials/work-supports-workbenches/2x4basics-reg-workbench-legs/p-2364482-c-19492.htm](http://www.menards.com/main/building-materials/work-supports-workbenches/2x4basics-reg-workbench-legs/p-2364482-c-19492.htm)

I got a set when I wanted to do a quick center bench. Took less than an hour to get it all together with one person and it was super square. Better than any bench ive done with 2x4 legs.


---
**Mikael Sjöberg** *June 02, 2015 16:38*

Maybee you are already Done, but I recommend to dip the threading pin in ethanol before tapping. This works perfect When tapping with a metal tool in aluminium . I did the tappering manualy in the beginning but then I started using a screwdriver and drilled very slowly . This together with the ethanol made it work very smooth ! 


---
**Dat Chu** *June 02, 2015 16:51*

Ethanol? Isn't that stuff bad?


---
**Mikael Sjöberg** *June 02, 2015 17:37*

Technical alcohol, no, it Works really good 


---
**Mikael Sjöberg** *June 02, 2015 17:39*

Try one and see what happens :)


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/S7Mxuc4EAsA) &mdash; content and formatting may not be reliable*
