---
layout: post
title: "Getting close to installing my Eustathios V4 Carriage Mod"
date: January 22, 2015 02:19
category: "Show and Tell"
author: Eric Lien
---
Getting close to installing my Eustathios V4 Carriage Mod. New bushings will be here Friday. Also prints with the new Bondtech Extruder Beta version are going well (closeup in the photos). I still have the tinniest z ribbing going on. But I will look into that later. The new bed supports move the bed up so it can reach the shortened height of the new carriage. Wish me luck ;)



![images/e1b556476908ca5b1c2606969e5c9487.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e1b556476908ca5b1c2606969e5c9487.jpeg)
![images/c12282912e29c2e2768a582b05114c3b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c12282912e29c2e2768a582b05114c3b.jpeg)
![images/a15db1a62ccca2aebbddf6107f5b64b1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a15db1a62ccca2aebbddf6107f5b64b1.jpeg)
![images/53b0cb0220f3a5158bb9ebac6d0bd8b6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/53b0cb0220f3a5158bb9ebac6d0bd8b6.jpeg)
![images/430b5e9459647aa90c94341949de7836.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/430b5e9459647aa90c94341949de7836.jpeg)
![images/0d85d323f77405b5ff567d8e9b7e301c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0d85d323f77405b5ff567d8e9b7e301c.jpeg)
![images/e75b22cb27db14d7868a1d2ee253168c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e75b22cb27db14d7868a1d2ee253168c.jpeg)
![images/1808b9656504736607f840032052c1b0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1808b9656504736607f840032052c1b0.jpeg)
![images/b300f87de10db1c9c00750bb3c88bbb8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b300f87de10db1c9c00750bb3c88bbb8.jpeg)

**Eric Lien**

---
---
**Jason Smith (Birds Of Paradise FPV)** *January 22, 2015 02:21*

The carriage looks awesome. I'm going to have to switch to that once I save up the motivation to switch it out. Let us know how it works out. 


---
**Bruce Lunde** *January 22, 2015 02:29*

Wow, just Wow, that looks so good!


---
**Eric Lien** *January 22, 2015 02:45*

**+Jason Smith** will do.


---
**Eric Lien** *January 22, 2015 11:57*

**+Oliver Schönrock**​ right now spacing is for Eustathios. It might work for HercuLien. I forget if the rod spacing is the same. If not it would be easy enough to change.﻿


---
**Eric Lien** *January 22, 2015 18:12*

**+Oliver Schönrock** yes.


---
**Jean-Francois Couture** *January 23, 2015 14:26*

**+Eric Lien** Did you dip your parts in acetone vapors ? they look so shiny !


---
**Eric Lien** *January 23, 2015 16:48*

**+Jean-Francois Couture** yes on the carriage, no on the bed support bracket (since it is PLA for rigidity).


---
**Jean-Francois Couture** *January 23, 2015 17:40*

**+Eric Lien** Yes I forgot to mention it but I was talking about the carriage.. but the bed support does look nice.. one thing tough, since its PLA aren't you worried that it will soften with the bed being near ? 


---
**Eric Lien** *January 23, 2015 19:03*

**+Jean-Francois Couture** I've been running it for a long time now and haven't seen any problem yet. It's not in direct contact with anything hot only in proximity which is fine.


---
**Eric Lien** *January 23, 2015 19:04*

**+Jean-Francois Couture** I would be more concerned if I enclosed this printer though. But right now it is my PLA printer and open air.


---
**Jean-Francois Couture** *January 24, 2015 03:53*

oh ok, not the Herculien on this then :)



Good luck with the mods !


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/CWZ6wuZtEb8) &mdash; content and formatting may not be reliable*
