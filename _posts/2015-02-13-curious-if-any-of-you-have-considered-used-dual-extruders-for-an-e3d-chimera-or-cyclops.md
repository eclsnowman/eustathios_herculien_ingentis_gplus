---
layout: post
title: "curious if any of you have considered/used dual extruders for an e3d chimera or cyclops.."
date: February 13, 2015 00:02
category: "Discussion"
author: Seth Messer
---
curious if any of you have considered/used dual extruders for an e3d chimera or cyclops.. if so, how'd that go? dual extrusion and the work involved to get it all dialed in worth it?





**Seth Messer**

---
---
**Eric Lien** *February 13, 2015 02:32*

My opinion is dual extrusion isn't worth the hassle.


---
**Tim Rastall** *February 13, 2015 03:22*

Samesies,  at least for support material.  Snap away supports work just fine with simplify3d.

I might try a combination of volcano for infill and V6 for perimeters at some point.


---
**Derek Schuetz** *February 13, 2015 05:12*

**+Eric Lien** whats the hassle with dual extrusion?


---
**Eric Lien** *February 13, 2015 12:00*

**+Derek Schuetz** ooze of second nozzle, z alignment if second nozzle, xy alignment of second nozzle, retraction and purge calibration of two nozzles.



I wanted dual extrusion on HercuLien for support material. But using simply3d I don't really need it. So to be honest I never use it.


---
**Mike Kelly (Mike Make)** *February 13, 2015 17:08*

I use dual extrusion and much prefer it. I've been enjoying the cyclops a great deal but it does have it's own nuances and difficulties involved with it. That said it eliminates a great deal of the usual criticisms and complaints of dual extrusions. 



HIPS is a great support material and breaks away cleanly with minimal post processing. Then there's the whole realm of multiple colors/material mixtures etc. 



About to try the chimera and see how much it simplifies the process as compared to my old Dual E3D v6 and v5 carriages. 


---
**Joe Spanier** *February 13, 2015 19:19*

So I have my legends pack sitting next to my printer waitng to be bolted on. **+Mike Kelly** Do HIPS and ABS play nice together in the cyclops? Thats the combo Im really looking forward too especially with the changes placed in slic3r 1.2.6 for soluble support materials. 



As far as nozzle XY calibration **+LulzBot** put this awesome little site together for their Dual extruder setup. The site details setting up their extruder but if you read through it there is a link to download a calibration square .amf file and a web app that you put your current offset and your measured values in to give you back a proper tool offset. Took me 2 trys to get a dead on xy calibration. 



Web App: [http://devel.lulzbot.com/TAZ/accessories/dual_extruder/dual_extruder-1.0/Calibration/calibration_calculator.html](http://devel.lulzbot.com/TAZ/accessories/dual_extruder/dual_extruder-1.0/Calibration/calibration_calculator.html)



Calibration Site: [https://ohai-kit.alephobjects.com/project/dual_extruder_calibration/](https://ohai-kit.alephobjects.com/project/dual_extruder_calibration/)



Even though this is Lulzbot specific the AMF would work for any printer as would the webapp. 


---
**Eric Lien** *February 13, 2015 20:06*

**+Joe Spanier** bookmarking these. Thanks.


---
**Mike Kelly (Mike Make)** *February 13, 2015 20:24*

**+Joe Spanier** No problems that I've noticed. Seems like the HIPS doesn't purge the ABS out quite as nicely so you get a little color bleed right at the start. 



I've had some issues, but i'm 99% sure they're related to my extruder and not the hot end. Really need to design a replacement to my EZStruders like the Hercustruder. Having a bit of creators block on it deciding what exactly I want to do. I also like UniquePrototypings extruder but they won't give up the source files. That's a rant for another day. 


---
**Seth Messer** *February 13, 2015 20:50*

so, coming from a beginner here.. let's say i have a bondtech v2 extruder, i'd likely want to get a second bondtech v2 extruder to go dual extrusion, correct? would you go with a different extruder? i reckon since martin sells the geared/nonplastic parts for his extruder i could just buy that and print the plastic parts whenever i get single extrusion working.



looking forward to see more variants with dual+ extrusion setup. :D


---
**Mike Kelly (Mike Make)** *February 13, 2015 21:13*

The Bondtech v2 would be a great extruder. Running 2 of them is all the better.



In an ideal world you'd use identical extruders because, to my knowledge, there's not an easy way to set the Esteps per mm for E0 and E1. I might be wrong on that but in general it simplifies the process just to use identical extruders.



A geared extruder is always a good idea. EIther the Greg's wade or a geared stepper motor etc. 


---
**Seth Messer** *February 14, 2015 03:12*

guys, you weren't joking about Simplify3D's support capabilities. I just watched some extremely impressive print result videos with the supports coming cleanly off in some really precarious areas. I don't anticipate needing to mix colors or material types, especially if supports are handled this well, so maybe i'll just go with an e3d v6 or volcano and call it a day. save money and get used to the printer, etc without mucking around with multiple extruders my first go round.


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/UecRJ4YYX7G) &mdash; content and formatting may not be reliable*
