---
layout: post
title: "Hi again ;) I recieved the lasts parts for completing the flying extruder mod from Walter Hsiao Looks good, prints good !"
date: April 11, 2016 16:55
category: "Build Logs"
author: Maxime Favre
---
Hi again ;)



I recieved the lasts parts for completing the flying extruder mod from **+Walter Hsiao** 

Looks good, prints good !

Still a bit shaky during infill because the dampening bungee cord is missing.

I found some ikea handle who fits quite good.



Meanwhile my flotilla is slowly growing ;)

Is there something I can do for the small joint on perimeters start/end in the third picture ? It looks like it's missing some matter at the beginning of the perimeter and th'ere's a little blop at the end. It occur one every layer even if retraction is needed or not.



I can put them all in one corner but I think I can do something before.

Infill still need some fine tuning, working on it.



![images/20bc0ae7e1270135984b656a8d5fbd87.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/20bc0ae7e1270135984b656a8d5fbd87.jpeg)
![images/4b96b0308a24a8e93dde0eb45b995e2f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4b96b0308a24a8e93dde0eb45b995e2f.jpeg)
![images/4b0279c0be4bb9ffc74b864904ca20e6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4b0279c0be4bb9ffc74b864904ca20e6.jpeg)
![images/77bd75629e571408a70e706e5945174a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/77bd75629e571408a70e706e5945174a.jpeg)

**Maxime Favre**

---
---
**Michaël Memeteau** *April 11, 2016 17:15*

Pristine! **+Maxime Favre**, after such a teaser you should treat us with a video... ;) High quality job from +Walter Hsaio (as usual).


---
**Eric Lien** *April 11, 2016 17:19*

Which Slicer are you using? If you use simplify3D there is the coast to end, and extra restart distance variables which are handy for tuning blobs at ends of perimeters or gaps at the start of new lines (extra restart distance can be made positive or negative to help in case you are seeing blobs vs gaps after a retract.


---
**Eric Lien** *April 11, 2016 17:19*

Looking great BTW :)


---
**Maxime Favre** *April 11, 2016 17:23*

Yes I'm on simplify3d. I printing right now with this setting modified to see the difference  between pos and neg value, I keep you updated.

I'll do a video ;)


---
**Eric Moy** *April 12, 2016 20:28*

Looks pretty cool. What are the benefits of adding this arm? Smaller linear stage rods?


---
**Botio Kuo** *May 19, 2016 19:57*

**+Maxime Favre** Hi, I want to build flying extruder and the Arm system like your, could you tell me how could I get the information for the build and material list? Thanks


---
**Maxime Favre** *May 19, 2016 20:22*

You can find the articulations parts on Walter hsiao's thingiverse page. I can provide stl for the extruder mount and the supports for the vertical extrusion. Read walter's post about the mod: [http://thrinter.com/cartesian-flying-extruder/](http://thrinter.com/cartesian-flying-extruder/)

With the pictures you should be able to build it. If you have any questions feel free to ask!


---
**Botio Kuo** *May 19, 2016 20:25*

**+Maxime Favre** Thank you for reply so quick. Could you sent me the stl files for the extruder mount and the supports for the vertical extrusion, pls ? supremeoape@gmail.com Thank you again. And I will read the post from Walter. 


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/jdwfKiusLM8) &mdash; content and formatting may not be reliable*
