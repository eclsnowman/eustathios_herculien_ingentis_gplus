---
layout: post
title: "here is the Ul-T-Slot z. It's spectra driven at the current design"
date: March 07, 2014 05:00
category: "Show and Tell"
author: D Rob
---
here is the Ul-T-Slot z. It's spectra driven at the current design.

![images/fb72c2ccb971ae6c8f42b5bf324e5f22.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fb72c2ccb971ae6c8f42b5bf324e5f22.jpeg)



**D Rob**

---
---
**Mike Miller** *March 09, 2014 03:03*

How's that printed wormscrew working for ya?


---
**D Rob** *March 09, 2014 03:50*

well... truth be told, I have been working 2 jobs lately and havent even gotten it printing. I made some game changes and have just finally gotten everything. I swapped from ramps so didnt bother to finish until I got my Ultiboard 1.5.7 and 24v 17a power supply. jacobsonline nichrome ,switching regulator to replace 7812 to avoid overheating at 24v. new extrusions to fix an eyesore (and possible problems in my frame design) the electronics box was an afterthought when I noticed I had an 8in z if I kept the z drive inside the build chamber. All the changes started when I was drilling a hole in the plexi for the heat bed cables and killed the power supply beneath. In my defense I put a plastic plate between but the brand new bit saw this as a minor distraction as I killed a 12v 48.7a powersupply :( so i wanted 24 volt upgrade so I went ultiboard.)


---
**Mike Miller** *March 14, 2014 12:53*

No judgement here. I blew a RUMBA by delivering 5V instead of 12V...


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/VXsbE5trFkX) &mdash; content and formatting may not be reliable*
