---
layout: post
title: "So, I've set anyone who's name I recognise to moderator, please add categories as you see fit but don't go crazy(I reserve the right to delete them if they displease me, mwahhahhaha) ."
date: March 05, 2014 05:15
category: "Discussion"
author: Tim Rastall
---
So,  I've set anyone who's name I recognise to moderator,  please add categories as you see fit but don't go crazy(I reserve the right to delete them if they displease me,  mwahhahhaha) . I've promoted **+D Rob** to owner as his Ultislot machine is more of a close cousin of Ingentis and he's implemented a number of improvements worth considering. ﻿





**Tim Rastall**

---
---
**Carlton Dodd** *March 05, 2014 05:25*

Perhaps we should add links to whatever original plans/BOM exist in the "about" section (where "Spread the word" is now)?


---
**Tim Rastall** *March 05, 2014 05:31*

Yes! I am terribly time limited right now as ive just started a new job so I'm keen for people to take control of this thing so it can evolve without me constraining it.


---
**David Heddle** *March 05, 2014 05:50*

I stuck the links up, had them all in my favourites anyway. I will leave the about text to someone else as I am useless with words!


---
**D Rob** *March 05, 2014 06:21*

**+Tim Rastall** I'm touched. :') lol thanks


---
**Eric Lien** *March 05, 2014 07:13*

I will be watching this thread closely. My corexy is giving me enough headaches that it will likely converted over once I get enough parts printed. 


---
**Wayne Friedt** *March 05, 2014 07:43*

GRrr Eric. Convert to what. Your about at the top of the heap now. Whats it doing.


---
**Eric Moy** *March 05, 2014 10:57*

**+Eric Lien** , that's a shame, I just got my MISUMI frame and am finally starting up my core xy design. But my contingent design is a standard Cartesian, as I already have all the linear bearings and rails from my defunct eventorbot﻿


---
**Eric Lien** *March 05, 2014 14:38*

**+Wayne Friedt** **+Eric Moy** random shelf's on the parts. Its like a shift but not. Only on one edge of the part. Not visible in the gcode or model. It happens regardless of speed. I may try repetier firmware first to make sure it's not corexy implementation in marlin due to the double steps needed for Cartesian to Corexy transforms. But I cannot get repetier firmware to work with my Viki LCD. All black bars on the display.


---
**Mike Miller** *March 06, 2014 02:01*

**+D Rob** do ya have a writeup somewhere on your improvements?


---
**D Rob** *March 09, 2014 06:36*

No not yet. tearing it down for some improvements and will be replacing some extrusion. Full tear down and rebuild starts tomorrow. I plan on videoing the whole build process and then I'm going to put my vids up as an Ul-T-Slot build guide﻿


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/QGPXBGKeL8C) &mdash; content and formatting may not be reliable*
