---
layout: post
title: "Another Eustathios growing up. Now waiting for stepper motors, pulies, belts, etc to arrive"
date: September 13, 2015 16:29
category: "Show and Tell"
author: Roland Barenbrug
---
Another Eustathios growing up. Now waiting for stepper motors, pulies, belts, etc to arrive. Ordered the bronze bushings at Lulzbot. Using the 2020 frame offered by Motedis.com, and the quick fasteners. This avoids the need the throughholes in the frames. You can see them in all corners of the top bars. **+Frank Helmschrott** thnx for suggesting this supplier.



![images/12be5fc0bf50273c7bff8e239d9b373c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/12be5fc0bf50273c7bff8e239d9b373c.jpeg)
![images/f06ae12f8000b51506e42782dd75b1c2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f06ae12f8000b51506e42782dd75b1c2.jpeg)
![images/4ab1b373c1cc299a9d115601815b11fb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4ab1b373c1cc299a9d115601815b11fb.jpeg)

**Roland Barenbrug**

---
---
**Eric Lien** *September 13, 2015 16:36*

Looks great. Hope you take some pictures and notes as you build. One of these days I need to put together actual assembly instructions.


---
**Roland Barenbrug** *September 13, 2015 19:29*

Before I forget, first built the left side of the printer (flat on the floor), next the right side, and finally interconnected the left and right sides with the horizontal bars. In more detail, first building the left side, fastening all to make sure it would not fall apart when lifting. The right side build in the same way, flat on the floor, but mirrorred. Next attached the horizontal bars the the left side (still on the floor so the horizontal bars are temporariliy vertical). Finally took the right side, lift, turn up side down and make it land on the upwards pointing horizontal bars.

During assembly I immediately attached the printed parts to the bars, essentially to make sure I would have sufficient knots inserted in the bars.

As a support I had made 3 prints on paper of the included 3D pfd document.

I have to check some issues with length of bolts. The bolts used to fasten the corner brackets (triangular shapes) should probably be 8mm (10mm being too long). Further the 16mm bolts to fasten the bed leveling mounts seem too short (had to use 20mm bolts).


---
*Imported from [Google+](https://plus.google.com/118296832015849309457/posts/VWeSxYe2jMH) &mdash; content and formatting may not be reliable*
