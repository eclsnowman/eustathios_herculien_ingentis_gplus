---
layout: post
title: "Alternate Z bracket- I goofed when I ordered belts"
date: February 08, 2018 04:56
category: "Deviations from Norm"
author: Dennis P
---
Alternate Z bracket- I goofed when I ordered belts. My frame is stretched 30mm, and I really need a belt ~1060mm in length. I ordered and got 976mm.  TOO SHORT! Not many options... Its not long enough to even get around the motor or pulley. 

I decided to make use of an additional crossbar and put the tensioner on a revised Z motor mount. Z Motor rotated 45 degrees to get the tensioner closer to the spindle. I shoujld have also added a second mounting boss on the open side. 

I will put the model and .stl in my Community Mods folde in case anyone else ends up with similar need. 

![images/16897f14ffe79c7a7d4d43867384f9bb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/16897f14ffe79c7a7d4d43867384f9bb.jpeg)



**Dennis P**

---
---
**Øystein Krog** *February 08, 2018 14:31*

Shorter belt is always better, so this is a nice improvement :)


---
**Dennis P** *February 08, 2018 17:51*

this is how it came about. the tension adjust can be made moving it left to right. but you need another cross member to make it work. Not all the elements are in the picture but the pulleys are in the right place.

![images/9d33c0fe4928e1624b227c3cc88b27ec.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9d33c0fe4928e1624b227c3cc88b27ec.png)


---
**James Rivera** *February 09, 2018 21:00*

Good solution. Just a thought: if you move the idler a little more to the right, then you could loop the belt a little more around the pulley, increasing the number of teeth in contact with the belt. Obviously not necessary, since yours works. Just a suggestion.


---
**Bruce Lunde** *February 11, 2018 00:39*

Nice engineering, good to have a working 3D printer to build yours!


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/5knLkb9Tg9h) &mdash; content and formatting may not be reliable*
