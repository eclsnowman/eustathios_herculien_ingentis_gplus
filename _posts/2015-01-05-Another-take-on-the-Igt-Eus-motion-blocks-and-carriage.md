---
layout: post
title: "Another take on the Igt/Eus motion blocks and carriage"
date: January 05, 2015 18:16
category: "Show and Tell"
author: Matt Miller
---
Another take on the Igt/Eus motion blocks and carriage.  I took the exact opposite approach to **+ThantiK** and decided to fillet almost none of the things.



![images/2dad7ccbf6a01560ebe835bb04fbcb38.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2dad7ccbf6a01560ebe835bb04fbcb38.png)
![images/985cd7777b125f3c818b5e918c293798.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/985cd7777b125f3c818b5e918c293798.png)

**Matt Miller**

---
---
**Tony White** *January 06, 2015 15:35*

fillets in a machined part don't usually make sense. But besides looking good and saving a hair of material, fillets in a printed part can also slightly to moderately increase strength (reduced stress concentrations and increased surface area for layer adhesion.) When the cost is only a few clicks in your modeling program, why not fillet?


---
**Matt Miller** *January 06, 2015 15:57*

Originally that was the plan - machine the XY blocks and carriage from aluminum like the rest of the machine.  This proved to be unfeasible at my level of expertise.  So I beefed the design up and went with plastic.    I could add some fillets, but they'd be in the belt path.  


---
*Imported from [Google+](https://plus.google.com/+MattMiller_akhlut/posts/3zqu754UxYx) &mdash; content and formatting may not be reliable*
