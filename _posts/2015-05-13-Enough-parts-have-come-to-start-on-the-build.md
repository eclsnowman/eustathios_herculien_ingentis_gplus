---
layout: post
title: "Enough parts have come to start on the build"
date: May 13, 2015 06:54
category: "Discussion"
author: Gus Montoya
---
Enough parts have come to start on the build. I'm still missing some wire, and the viki 2. 

![images/3cf64c43eaf57fa50d5b0e51b25c90bc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3cf64c43eaf57fa50d5b0e51b25c90bc.jpeg)



**Gus Montoya**

---
---
**Gus Montoya** *May 13, 2015 06:54*

crappy pic from very old phone.


---
**Godwin Bangera** *May 13, 2015 07:32*

congrats .. hope your entire build will be logged here :)


---
**Eric Lien** *May 13, 2015 12:13*

Good luck. Do you still plan to record video.


---
**Gus Montoya** *May 13, 2015 18:16*

I'm not decided yet, I don't have a good enough camera to document by that method (initial picture of this post as proof), I'll give it a take with the camera and see how it goes. I'm really not good at media.


---
**Eric Lien** *May 13, 2015 18:46*

**+Gus Montoya** no problem just wondering.


---
**Gus Montoya** *May 13, 2015 19:03*

**+Eric Lien** I can make a pdf manual, but I don't know what software would be good enough for that. Maybe word with some crappy pictures. 


---
**Jim Squirrel** *May 13, 2015 19:12*

libreoffice exports directly to pdf [https://www.libreoffice.org/](https://www.libreoffice.org/)


---
**Gus Montoya** *May 13, 2015 19:30*

I was thinking more along the lines of this, where anyone can go to the site and either print it out direct or follow along. I wonder if we should start our own wiki or if their is one we can peg our builds to. Anyone will be able to create an account and upload video's etc I think we can take these machines to a new level with the proper site to host, build instructions and file distribution. Or if all of you prefer a small niche community is fine by me. I can also setup my own mini file server to run on my desk 24/7 which won't consume a whole lot of power. Please let me know what this community would like to do.



[http://reprap.org/wiki/Wallace_Build_Manual](http://reprap.org/wiki/Wallace_Build_Manual)


---
**Eric Lien** *May 13, 2015 19:55*

**+Gus Montoya** I would prefer you take notes during the build as you run into questions during assembly. Then take pictures and notes of those steps. What I don't have a good grasp on is what questions people (who aren't me and didn't spend months modeling it) run into. I guess what I am saying is what does someone who is not me run into when building. 



Once the resources and notes are collected a manual can be made. I like the github model like **+Daniel Salinas**​ did. It is editable, already hosted, and in the location.



I know **+Seth Messer**​ is also working on this. And different people will have different questions.


---
**Gus Montoya** *May 13, 2015 20:00*

no worries, I'll document as I go along. I misunderstood the request to document as making an official one.


---
**Daniel Salinas** *May 14, 2015 02:23*

yeah I won't say my writeup is perfect.  please submit any revisions as you go along.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/UiBG4MB6Drd) &mdash; content and formatting may not be reliable*
