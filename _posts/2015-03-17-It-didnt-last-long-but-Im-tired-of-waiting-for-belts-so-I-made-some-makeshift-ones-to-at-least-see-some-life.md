---
layout: post
title: "It didn't last long but I'm tired of waiting for belts so I made some makeshift ones to at least see some life"
date: March 17, 2015 20:23
category: "Show and Tell"
author: Derek Schuetz
---
It didn't last long but I'm tired of waiting for belts so I made some makeshift ones to at least see some life. 

![images/293f6975601a2d8a1e4b3d7ceb6b1aa2.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/293f6975601a2d8a1e4b3d7ceb6b1aa2.gif)



**Derek Schuetz**

---
---
**Владислав Зимин** *March 17, 2015 21:12*

о нет... вертикальное видео D:


---
**Derek Schuetz** *March 17, 2015 21:56*

Ya I decided to be one of those people 


---
**Eric Lien** *March 17, 2015 23:15*

If thats my break in gcode looks like you are running 32microsteps. I have mine at 16 because 32 limited my top speeds due to stuttering. Hence why the travel is so small. If you stick with 32 steps then you will need to double the steps/mm in my marlin.



Great to see another HercuLien moving.


---
**Derek Schuetz** *March 17, 2015 23:49*

Oh that explains that....so I either remove a jumper or double the steps?


---
**Eric Lien** *March 18, 2015 00:23*

Yup. I had trouble over 150mm/s with 32microsteps. Sometimes even slower if there is a lot of short sections like circular parts, or If I designed the part with a lot of fillets.


---
**Derek Schuetz** *March 18, 2015 00:31*

Those are just printer moves right not printing? 150mm/sec seems so fast to be reliably printing at 


---
**Eric Lien** *March 18, 2015 01:57*

It depends on the size part. I print at 150+ regularly when doing really big parts. For a 20x20 cube... Yeah 150 would rattle some bolts loose ;)


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/RyWoLPjucLw) &mdash; content and formatting may not be reliable*
