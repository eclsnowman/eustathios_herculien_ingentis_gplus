---
layout: post
title: "Mr Lien, being pretty awesome. I'll forgive him for murdering my surname"
date: February 28, 2015 05:43
category: "Discussion"
author: Tim Rastall
---
Mr Lien,  being pretty awesome.  I'll forgive him for murdering my surname. 



<b>Originally shared by Eric Lien</b>



Sorry for the shameless self promotion, but I had great fun this afternoon. I was interviewed about my HercuLien open source 3d printer by Michael Balzer on All Things 3D.



Interview with Eric Lien, and the 'Herculien': 
{% include youtubePlayer.html id=FePEONtdPpU %}
[http://youtu.be/FePEONtdPpU](http://youtu.be/FePEONtdPpU)





**Tim Rastall**

---
---
**All Things 3D** *February 28, 2015 08:26*

Hey thanks Gus, help me out on not being so "dry."  FYI, I normally have a co-host Chris Kopack who is normally the lively one and I produce, and play technical director. Sorry guys, will work on it in the future.  In any case, love to have you guys on as well, I looked at your G+ pages and love what you guys are doing. Tim, great job on the "Hulk"


---
**Gus Montoya** *February 28, 2015 09:01*

I'm glad you did not take my critic in a bad way. As I have mentioned before I tend to be a bit straight forward but not due to deliberate lack of respect or sensitivity. You had various points of information I enjoyed learning about. Keep up the good work. Over all the video was very informative. I'll be a loyal viewer going forward. :)


---
**Eric Lien** *February 28, 2015 13:55*

**+Tim Rastall**​​​ I knew I screwed it up as soon as I said it. I should have backtracked, and corrected myself. 



Heck I am still not sure I am pronouncing Eustathios correctly.



I guess there is a reason I went into physics in college, not public speaking ;)﻿


---
**Daniel Salinas** *February 28, 2015 18:11*

I'm famous now too! hahaha


---
**Eric Lien** *February 28, 2015 19:06*

**+Daniel Salinas** I have to get some show notes comments over to Michael with links to everyone I mentioned and some links to products like the Alirubber heated beds, etc. I will make sure to link to your great work on the documentation.


---
**Tim Rastall** *February 28, 2015 19:28*

**+Eric Lien** no worries mate. I work with a lot of Japanese guys and you should hear the variations on my surname they come up with :) 


---
**Tim Rastall** *February 28, 2015 19:35*

**+All Things 3D** Thanks!  I'll get around to finishing it and releasing for printing in the next few months,  new printer needs to be done first though. If I could provide some feedback on the interviews -  I think you need to guide it more and have a bit more structure to what you intend to achieve, otherwise you inevitably end up rambling and digressing. No different from public speaking in that regards, in my experience. Just my 2. 


---
**All Things 3D** *February 28, 2015 21:19*

Your feedback is welcome, but surprisingly you are the first to say this and most are surprised of the amount of effort I put into providing a structure and discussion questions before the interviews.  I will agree that this was not as tight as I liked to be, and there is some rambling, but I normally give myself more than an hour to prepare for these things, but through crossed wires on my part, Eric actually had taken time off from work to do this interview so I accommodated instead moving it to the next Friday.  In any case, I do not do this as profession even though I have pedagogy and public speaking background, I thought what the heck, Chris and I enjoy 3D stuff, why not do a podcast (now YouTube channel) talking with other like minded people.  So with my video production experience, I put together an adhoc studio and feel we have come a long way from single Zoom H2 in the middle of the floor on a tripod stand.  This was our 100th YouTube episode.



Your bio says you work at NEC.  I am assuming you are an executive or someone in public relations.  As mentioned in the private message, I welcome the opportunity for you, Eric, Martin and the other great innovators to take part in the roundtable discussion about the RepRap community, its past & future, and how companies like MakerBot have "borrowed" from it build their 3D empires.  I am looking to schedule this on the 25th of March at a time convenient to your timezone in New Zealand.



Again thanks Tim for the feedback, and agree we need to keep things tighter and moving along quicker.


---
**Eric Lien** *February 28, 2015 21:58*

I tend to pull the best laid plans off the rails. Ask the people in my morning engineering meetings.



You did an admirable job wrangling in my ramblings. I am much better at talking for hours around a fireplace with scotch in hand and friends nearby solving the worlds problems than I am at concise scripted dialogue. I get distracted by shiny things. 



Oh look a squirrel....


---
**Tim Rastall** *February 28, 2015 23:51*

**+All Things 3D** Fair enough and I've not had a chance to see any of your other interviews (yet) so I'm sure this was an outlier on the bell curve. I run the technical delivery team in NEC NZ - combination of technical SME, project director, international liaison and commercial management. Very little PR though (thankfully):). Anyway, I'd be happy to talk with you whenever you have a free slot - hopefully my new machine will be working in the near future and it has a number of features you may be interested in: smoothieware brain, belt driven z axis, almost entirely metallic using off the shelf parts, e3d volcano hot end, dual nema motors for all axis etc etc. Also, I didn't get a PM from you that i can see...


---
**Chris Brent** *March 02, 2015 17:45*

With a bit of editing this would be a great intro the the Ingentis, HercuLien and Eustathios projects!


---
**Jean-Francois Couture** *March 05, 2015 14:47*

**+Eric Lien** great interview.. always cool to see people talking about there work ahd 3D printing in general ! :)


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/dGU2tNHxD4n) &mdash; content and formatting may not be reliable*
