---
layout: post
title: "I made a low profile spool holder with parts who where laying around"
date: May 17, 2016 17:29
category: "Mods and User Customizations"
author: Maxime Favre
---
I made a low profile spool holder with parts who where laying around.

It works well with all the spools I have, from standard and cardboards ones and smaller like taulmann and filaflex.



Uploaded here with source files if anyone is interested:

[http://www.thingiverse.com/thing:1570166/#files](http://www.thingiverse.com/thing:1570166/#files)



![images/0f899fc2304d842f4d867c38f0a14b55.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0f899fc2304d842f4d867c38f0a14b55.jpeg)
![images/808383997d4ba9d899ff8dcd89333bcc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/808383997d4ba9d899ff8dcd89333bcc.jpeg)

**Maxime Favre**

---
---
**Eric Lien** *May 17, 2016 18:19*

I made one similar a while ago but I love the spring idea for easy change of the upper idlers. Going to have to steal that one :-)


---
**Sven Eric Nielsen** *May 17, 2016 18:42*

Seriously, this is probably the smartest solution I've seen so far!


---
**Dave M** *May 17, 2016 22:37*

Beautiful design!


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/b5PfGWD7yuD) &mdash; content and formatting may not be reliable*
