---
layout: post
title: "Aquarium airpump is great for cooling. I think Schorsch printer uses it too"
date: July 12, 2014 07:42
category: "Discussion"
author: karabas3
---
Aquarium airpump  is great for cooling. I think Schorsch printer uses it too [https://pp.vk.me/c620323/v620323705/c873/xirxVrIIjc4.jpg](https://pp.vk.me/c620323/v620323705/c873/xirxVrIIjc4.jpg)



![images/41a8355f3fa447bde7eb90965aa0bfe5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/41a8355f3fa447bde7eb90965aa0bfe5.jpeg)
![images/2ace3f36b218e5d852501de116532225.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2ace3f36b218e5d852501de116532225.jpeg)
![images/4e765a73dba1b0eca0b323573a9684f9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4e765a73dba1b0eca0b323573a9684f9.jpeg)

**karabas3**

---
---
**Liam Jackson** *July 12, 2014 08:30*

That's a nice idea, Is it not a little noisy? 


---
**karabas3** *July 12, 2014 08:44*

No, my e3d fan is louder. It is loud if only without tubing


---
**Jarek Szczepański** *July 14, 2014 18:10*

thought about the same idea :)


---
**karabas3** *July 14, 2014 18:45*

Just do it! About 5 russian forum members did it for themselves in  last 2 weeks. All are happy. One connected via relay to fan output. One via DIY PID  regulator


---
*Imported from [Google+](https://plus.google.com/115164543335947782806/posts/icJyHXGtcPv) &mdash; content and formatting may not be reliable*
