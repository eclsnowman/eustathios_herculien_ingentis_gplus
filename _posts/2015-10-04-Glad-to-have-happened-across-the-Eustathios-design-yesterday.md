---
layout: post
title: "Glad to have happened across the Eustathios design yesterday"
date: October 04, 2015 19:59
category: "Discussion"
author: Matthew Kelch
---
Glad to have happened across the Eustathios design yesterday.  I've recently been thinking about buying an Ultimaker 2, but the pricing is higher than I think is reasonable for it's build volume. I currently have a Printrbot Simple Metal with the Matrix Precision volume upgrades (8"x8"x10").  It's a nice printer, but also very limited in terms of speed and noise.  I'll probably keep it around specifically for dealing with things like flexible filaments.



I've started pricing out a Eustathios Spider v2 build.  Looking at the BoM it's a bit overwhelming, but I'm sure I'll be able to figure it out.



Ordering linear rails from Misumi get's really expensive really quickly.  Anyone have any tips on the cheapest source to pick those up from?





**Matthew Kelch**

---
---
**Igor Kolesnik** *October 04, 2015 20:14*

I just finished the initial build of Spider V2. I'd suggest you not to make my mistake. Misumi rods are worth every penny and I really regret spending money on cheaper rods from local source. Don't need chromed once, this way you can save some coins. Robotdigg is a good source of small things. In any case you will end up with machine better and cheaper than UM2  ﻿


---
**Igor Kolesnik** *October 04, 2015 20:16*

Also go with ball screws that are better and cheaper than lead screws from misumi


---
**Eric Lien** *October 04, 2015 20:55*

Yeah, I still need to update the BOM to include the lead screws spec'd from golmart on aliexpress. I should also see if they would consider creating a part number for them with the proper end machining and ball nuts in the proper orientation.


---
**Matthew Kelch** *October 04, 2015 21:20*

Thanks for the recommendation!


---
*Imported from [Google+](https://plus.google.com/+MatthewKelch/posts/HPayqpufP23) &mdash; content and formatting may not be reliable*
