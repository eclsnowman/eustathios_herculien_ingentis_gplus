---
layout: post
title: "I've never really participated in social media on my 3D printing adventures"
date: March 02, 2017 19:03
category: "Show and Tell"
author: Jules Ouellette
---
I've never really participated in social media on my 3D printing adventures. So this is a bit new to me. I've built about 6 3D printers some got disassembled and built(are being built) into new printers.



I've attached a gallery of all the printers I've built/upgraded. Currently I'm finishing the large delta that is in the album. A few print quality issues and calibration issues left to sort out. I'm also <b>still</b> in progress on a printed case Tantillus (it's a very challenging build).



I really like my square tubing SmartRap Core printer with dual lead screws but I find that the build area is too small and it's very loud. Though less loud than the wooden box version I had built previously. It's fast and the print quality is good and even prints 90A flexible filament well.



I thought the next printer I should tackle is a Eustathios clone. My goal is to build it out of easily found AliExpress parts. I plan on using good ol, RAMPS 1.4 and one of the many Raspberry Pi B's I have hanging around with Repetier Server. (It still has issues compared to Octoprint, but is much nicer to do certain things with). I've used this combo on all my printers, what's nice about Repetier Server is that I can control all of the Repetier Server instances from a central Repetier Server instance and off load all of the gcode rendering to that same central instance. I will probably never use the slicing capability as slicing is an art and I like the control Simplify3D has. Cura drives me nuts, and Slic3r is buggy and does weird things.



For the extruder I will probably start with the Modicum extruder which I love, but move to the Titan Extruder as it's much smaller and fits nicer into the frame. I am not a fan of direct drive (non geared) extruders, they do not produce nice quality prints (IMO).



The build plate I can get locally for fairly cheap, but the downside is that they use a shear which usually requires a trip to the anvil afterwards to straighten out a bit. 



Anyway tl;dr and I hope to post some updates in the coming weeks!









**Jules Ouellette**

---
---
**Eric Lien** *March 05, 2017 14:56*

I am excited that you are interested in building a Eustathios. As you prepare please utilize the group for a sounding board with any questions. We have an amazing group here with so much knowledge and willingness to help.


---
*Imported from [Google+](https://plus.google.com/+JulesOuellette/posts/VuRciDeJrHe) &mdash; content and formatting may not be reliable*
