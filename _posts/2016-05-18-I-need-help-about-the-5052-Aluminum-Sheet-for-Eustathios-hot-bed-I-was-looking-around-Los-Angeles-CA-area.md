---
layout: post
title: "I need help about the 5052 Aluminum Sheet for Eustathios hot bed, I was looking around Los Angeles CA area"
date: May 18, 2016 07:02
category: "Build Logs"
author: Botio Kuo
---
I need help about the 5052 Aluminum Sheet for Eustathios hot bed, I was looking around Los Angeles CA area. And they didn't accept for just cutting one sheet for me. Or the price is too high and I can't accept. So if anyone can cut one and ship to CA for me, I'll really appreciate it.(if the price is affordable for me.) Thank you ... And I think I will buy AZSMZ Mini Ver2.1 + AZSMZ 12864 LCD + 4 x DRV8825 instead of Azteeg X5 for my board.





**Botio Kuo**

---
---
**Makeralot** *May 18, 2016 08:19*

Do you mind shipping from China?


---
**Eric Lien** *May 18, 2016 11:21*

If you go with the **+Walter Hsiao**​ style bed it can be square with just four(4) holes drilled in. 



To be honest the square bed and extrusion at the front and back of the bed to allow for misalignment tolerance is growing on me. The issue with the current v2 design is it doesn't take into account stack tolerance. So you can end up with side pressure on the lead screws which can introduce z wobble.


---
**Ted Huntington** *May 18, 2016 15:51*

Botio you can do what I did and just have the metal company cut the aluminum to size (I used IMS in Orange County, CA), and then use a round file to file away a semi-circular inset/hole on the edge of the aluminum plate where the Z rods intersect with it- it took about a good 30 minutes to file away the aluminum, but it works :)


---
**Derek Schuetz** *May 18, 2016 16:08*

If your in orange county or near I can refer you to someone who can do all your cutting


---
**Brandon Satterfield** *May 18, 2016 16:09*

Happy to assist.

Know Eric has the files up but if you'll send the DXF, material (5052) and thickness I can get SMW3D to cut and ship from Houston, TX. I'll check it out and shoot a paypal invoice to you.. Basically for material... 



Let me know if interested. 


---
**Botio Kuo** *May 18, 2016 16:38*

**+Brandon Satterfield**  OMG!! That's will be really great!!! How could I sent you the information  pls? And could you tell me how much with the shipment pls (not need very precisely) ~~ Thank you 


---
**Botio Kuo** *May 18, 2016 16:39*

**+Derek Schuetz** Hi,Thank you for help. That's a way I can do too. Could you tell me the info pls? My email is supremeoape@gmail.com. Thanks


---
**Botio Kuo** *May 18, 2016 16:44*

**+Ted Huntington** Hi, Thank you for your reply. I think Orange County is a good place I may need to go. And your way is so smart. :)


---
**Brandon Satterfield** *May 18, 2016 16:46*

Shot you over an email. Looks like you may have a local solution, which is awesome! 


---
**Daniel F** *May 18, 2016 16:52*

Azsmz mini works well on my corexy. Quality is ok for the price. I changed the polyfuses with 1808 smd fuseholder plus fuses, I run it with 24V, heated bed through omron ssr. Buil plate can be square, mine is 31x31cm, silicon heater is 29x29cm, 450W. Bed is mounted with 3 screws and springs. Build area is around 290mm in all directions.


---
**Botio Kuo** *May 18, 2016 17:00*

**+Daniel F** Hi Daniel, I think about D-Bot Corexy before. And I think the design of Eustathios is sexier and nice than it. So I almost down for printing out all 3D print parts for it. haha. And thank you.


---
**Daniel F** *May 18, 2016 20:40*

**+Botio Kuo** Agree with you, I built my corexy before the eustathios, actually I printed  the eustathios withe the corexy. I just replaced my old ramps of the coreyx with an azsmz mini, coming from repetier host, smoothieware is much easier and straight forward to setup.


---
**Botio Kuo** *May 19, 2016 15:39*

**+Daniel F** woops, I find another board called "MKS SBASE V1.2" it's 32bit and could run with touch screen ... so now I buy this one as final decision. haha


---
**Daniel F** *May 19, 2016 16:06*

Difference is mks has onboard drivers, I think drv8825 (clones) with 32 micro steps, whereas the azsmz mini has sockets for stepsticks. I use 128 micro steps raps128 drivers.


---
**Botio Kuo** *May 19, 2016 16:12*

**+Daniel F** so 128 micro steps is much better ?!! OH...no... I paid already...


---
**Daniel F** *May 19, 2016 16:47*

Not really, just more quiet, but they are expensive 15 to 18 euro per pice that you need to add on top of the board. You would probably have to get them shipped from Europe, I'm not sure if there is an US distributor. And with azsmz mini, there are no dip switches to adjust micro steps, if you need to change the drivers default, you need to wire it directly on the driver. 


---
**Botio Kuo** *May 19, 2016 18:03*

**+Daniel F** It sounds too professional... I'm not expert for this. haha So I think MSK is fine for me. 


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/dfQJEdFCB3t) &mdash; content and formatting may not be reliable*
