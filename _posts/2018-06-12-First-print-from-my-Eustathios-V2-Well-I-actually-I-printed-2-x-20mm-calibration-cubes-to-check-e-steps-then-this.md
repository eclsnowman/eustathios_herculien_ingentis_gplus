---
layout: post
title: "First print from my Eustathios V2! Well, I actually I printed 2 x 20mm calibration cubes to check e steps then this"
date: June 12, 2018 10:30
category: "Build Logs"
author: Julian Dirks
---
First print from my Eustathios V2!  Well, I actually I printed 2 x 20mm calibration cubes to check e steps then this. Printed with 18 month old PLA on the spare "build tak" that came with my Wanhao taped to the bed with no bed heating and no optimisation of settings at all.  I couldn't be happier!  I've still got a number of things to finish and I'll post some pics and thoughts once I get everything done.  



Credit where it is due.  A huge thanks to **+Eric Lien** (and those before him)who made this even a realistic option for someone like me. Thanks to **+Walter Hsiao** for his designs and also **+Maxime Favre** might recognise a few things too. And **+Oliver Seiler** for rapid fire advice from this part of the world.





![images/bc3af113fbd5da2081068922d6f3e49d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bc3af113fbd5da2081068922d6f3e49d.jpeg)



**Julian Dirks**

---
---
**Eric Lien** *June 12, 2018 10:41*

Congratulations. Looking great. I always love to see first prints. I look forward to see as you dial everything in and learn your new machine. Please keep us posted of your progress. 


---
**Oliver Seiler** *June 16, 2018 00:05*

Well done, looks great :-)


---
*Imported from [Google+](https://plus.google.com/113795478307151372873/posts/YDrwreV4dnb) &mdash; content and formatting may not be reliable*
