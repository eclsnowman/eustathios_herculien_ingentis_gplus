---
layout: post
title: "Hey, I started this hobby with a Folgertech Kossel, I didn't really know where I wanted to take it, I really enjoyed the tinkering"
date: March 26, 2016 00:20
category: "Discussion"
author: Jaimie Fryer
---
Hey, I started this hobby with a Folgertech Kossel, I didn't really know where I wanted to take it, I really enjoyed the tinkering. These days however as I've learned more I want to spend more time learning and printing CAD/CAM designs and less time making my printer work.

It takes seemingly endless calibration, some guesswork to make this Delta give quality prints, from time to time I'd achieved that but maintenance and forced upgrades (bad kit parts) forced me back into endless hours of calibration.



I want to recycle what I have into a smaller Eustathios Spider V2 (320 * 320 * 348 in mm). I'm still not great at CAD but I've started to play with a frame that will give me the most volume with the parts I have.



This gives me a few questions though. Which parts will I have to change to fit into a smaller volume? Are there any obvious problems that may come from doing this? What is the smallest distance that I can make this section? (see image)

![images/01ac60e6795cfb0e7a87719005385748.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/01ac60e6795cfb0e7a87719005385748.png)



**Jaimie Fryer**

---
---
**Eric Lien** *March 26, 2016 00:26*

If you want to test fit into your model fusion360 can import the X_T files on the github very well. If you have questions about fit... Confirming via the model is the best way. That's why I 100% modeled mine before building. It leaves less up to chance.


---
**Eric Lien** *March 26, 2016 00:28*

Great model by the way. Keep up the great work.


---
**Joe Spanier** *March 26, 2016 00:48*

Should pull the solidworks in too. Sometimes the assembly goes a little nuts but it's not bad to put back together


---
**James Rivera** *March 26, 2016 03:56*

Change the words "Folgertech Kossel" with "Printrbot LC+ v1" and the 1st paragraph is <b>exactly</b> me. Do it. You will be very pleased with this design.


---
**Jaimie Fryer** *March 26, 2016 15:02*

Another question I have is, do you think I could use this for the extruder? I'd like to print flexible filaments. 

[http://www.thingiverse.com/thing:1102900](http://www.thingiverse.com/thing:1102900)


---
**Jaimie Fryer** *March 26, 2016 15:03*

**+Joe Spanier** thanks, that really helped. It's basically cut and paste now.


---
**Erik Scott** *March 26, 2016 18:49*

You shouldn't have to change much when it comes to the 3d printed parts, as they aren't really dependent upon the lengths of the other metal pieces (Rods and extrusions and such). You just need to make sure you make them long enough to handle the desired build area. Modeling everything in CAD first is indeed the best way to go. 


---
**Jaimie Fryer** *March 26, 2016 21:43*

[https://www.dropbox.com/s/zotayqu5jq7uf6r/Screenshot%202016-03-26%2021.42.41.png?dl=0](https://www.dropbox.com/s/zotayqu5jq7uf6r/Screenshot%202016-03-26%2021.42.41.png?dl=0)



Looks good to me. Now I need the printed parts, maybe I can send someone a roll of ABS?


---
**Eric Lien** *March 26, 2016 23:02*

**+Jaimie Fryer** looking great


---
**Dovid Teitelbaum** *May 13, 2016 18:33*

Thanks for the add. I have recently built a Pegasus 12" and really want to convert it to a ultimaker type design. I was recommended one of these printers. I want to keep the size so as not to spend extra money. Can someone help me choose a design that would work. I would like to use it for abs as well. Thanks


---
*Imported from [Google+](https://plus.google.com/+JaimieFryer/posts/XoFGSFqwfg5) &mdash; content and formatting may not be reliable*
