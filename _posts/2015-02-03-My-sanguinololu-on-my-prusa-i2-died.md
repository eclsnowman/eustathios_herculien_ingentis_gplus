---
layout: post
title: "My sanguinololu on my prusa i2 died!"
date: February 03, 2015 01:51
category: "Discussion"
author: Ethan Hall
---
My sanguinololu on my prusa i2 died! I can no longer print the remainder of the parts for my ingentis. After seeing the print it forward campaign I was wondering if I could hop on that list? Or if someone could just do me a big favor and print out the remainder of the parts? Any help is appreciated. I plan on starting fresh with my ingentis with a new board.

![images/388a02106e13836cb371ee5b57af6482.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/388a02106e13836cb371ee5b57af6482.jpeg)



**Ethan Hall**

---
---
**James Rivera** *February 03, 2015 02:09*

You're going to need a board for the Ingentis. Why not hook it up temporarily to your Prusa to get the print done, then move it to the Ingentis when it is ready? In the meantime, you'd have a working 3D printer.


---
**Ethan Hall** *February 03, 2015 02:11*

Getting the beast up and running was difficult the first time and I'd rather just start fresh and try not too waste to much time on a machine that ultimately will be a pile of parts once I'm done with the new one.


---
**Daniel Salinas** *February 03, 2015 02:46*

You can definitely get your name on the "Print it forward" list, there's a google doc around here somewhere.  If you're in a hurry though I'd suggest picking up a new board for your ingentis and using it to get your prusa back up.


---
**Joe Spanier** *February 04, 2015 02:15*

I would too. But not for getting your parts done. But for the experience. Nothing makes you learn something better then repetition and if you have already wired it once and flashed the board all the works been done just transfer flash and go!  Takes me about 30 minutes to transfer to a new ramps board now if something goes wrong cuz I've just done it. 


---
**Ethan Hall** *February 04, 2015 02:22*

Okay! I'm ordering a new board but I'm switching to ramps 1.4 purely because of the price difference between it and say a better board such as an azteeg x5 or a smoothieboard.


---
**Joe Spanier** *February 04, 2015 05:01*

That's a perfect intermediate board and easy to setup. Good luck and feel free to ask questions. 


---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/UfsXthp5GZv) &mdash; content and formatting may not be reliable*
