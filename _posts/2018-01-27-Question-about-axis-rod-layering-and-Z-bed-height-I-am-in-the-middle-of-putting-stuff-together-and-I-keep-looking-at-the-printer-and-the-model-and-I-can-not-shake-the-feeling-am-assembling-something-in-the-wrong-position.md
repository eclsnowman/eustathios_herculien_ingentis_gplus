---
layout: post
title: "Question about axis rod layering and Z bed height- I am in the middle of putting stuff together and I keep looking at the printer and the model and I can not shake the feeling am assembling something in the wrong position"
date: January 27, 2018 00:44
category: "Discussion"
author: Dennis P
---
Question about axis rod layering and Z bed height- I am in the middle of putting stuff together and I keep looking at the printer and the model and I can not shake the feeling am assembling something in the wrong position. 



Does the layering of the 10mm rods matter as long as the carriage rods line up?  Do the kinematics matter if the X rods are on top of the Y rods or vice versa?  Any suggestions about which axis to align with the bed support rails?



The bed and Z brackets.... the Z bracket hits the extrusion on the top before it bottoms out against the bearing housing bracket. Seeing how everything else fits together so far, it surprised me. Loosing 20mm of travel seems like a lot to give up. 



Thanks in advance



Dennis

  





**Dennis P**

---
---
**Eric Lien** *January 27, 2018 01:04*

Can you provide a few pictures to get a better visual on what you are referencing? I am having trouble following by your description.


---
**Dennis P** *January 27, 2018 01:30*

**+Eric Lien** 

![images/6a38fb98ec445ca1efc70696c8fd00be.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6a38fb98ec445ca1efc70696c8fd00be.jpeg)


---
**Dennis P** *January 27, 2018 01:32*

![images/3a992e804b87fc9793837a17d92a94d0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3a992e804b87fc9793837a17d92a94d0.jpeg)


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/aJZ9wzhQK8W) &mdash; content and formatting may not be reliable*
