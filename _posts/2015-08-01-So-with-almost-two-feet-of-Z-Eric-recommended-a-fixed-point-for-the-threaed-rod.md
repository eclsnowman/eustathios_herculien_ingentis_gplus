---
layout: post
title: "So with almost two feet of Z, Eric recommended a fixed point for the threaed rod"
date: August 01, 2015 00:42
category: "Discussion"
author: Rick Sollie
---
So with almost two feet of Z, Eric recommended a fixed point for the threaed rod. Had to stop at Familab and get lessons on using a lathe to turn down the threads.



For last minute changes like this I highly recommend a 10 piece bag of post insertion nuts. I forgot who gave me advice its awesome advice.



So naturally I get most of it together and forget to put the belt on.... Which brings me to the question. Can you run a GT2 32 tooth pulley instead of a 20tooth? 



![images/88ba5c9ae7d5d32808b9f31864e39d6e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/88ba5c9ae7d5d32808b9f31864e39d6e.jpeg)
![images/9c40633b260c7409b0796ceb45397646.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9c40633b260c7409b0796ceb45397646.jpeg)

**Rick Sollie**

---
---
**Eric Lien** *August 01, 2015 01:40*

Looks great. Where did you want to put the 32tooth? I do recommend 20tooth on the motors for the reduction it provides. 32tooth ever where else.


---
**ThantiK** *August 01, 2015 01:56*

**+Rick Sollie**, when you get your machine done, mind printing me some parts?  I've been ABS-less for a while now, and need to reprint some parts so I can get rid of PLA.


---
**Erik Scott** *August 01, 2015 02:37*

I'm putting a 32-tooth pulley on the Z-motor so that I get nice even steps for normal layer heights. I'm doing 20 on X and Z though. 



I've always heard it's best to keep the lead screws unconstrained at the top, but if they're precision machined and straight enough, it shouldn't matter. Hopefully everything works out.



What are you planning on printing that requires that Z-height? 


---
**Eric Lien** *August 01, 2015 02:51*

**+Erik Scott**​ the biggest deal is even steps for Z (no micro steps), so if you use layer height consistent with even stepper steps you get the same result (I use the prusa calculator for this). Also on Z I like the reduction for added torque to allow rapid Z moves. But 32 tooth is likely fine.﻿


---
**Erik Scott** *August 01, 2015 02:53*

I have 32 tooth now, and it works fine. I just want to be able to do nice even 0.2 or 0.1 mm layer heights, and not have to remember some long decimal. 


---
**Rick Sollie** *August 01, 2015 03:04*

**+Eric Lien**

 The 32 tooth is on the motor on the Z axis. 


---
**Rick Sollie** *August 01, 2015 03:05*

**+ThantiK**

 I only have two words for you Thantik,  What Color ;-)


---
**Rick Sollie** *August 01, 2015 03:08*

**+Erik Scott**

Thanks for the insight. I went to a model Rock launch and thought it would be cool to make a 'Rocket of the Future' 50's style. I plan to print it during Maker Fair Orlando.  I'll post picture when it happens. (but the truth is I had the extrusion before I started building and though, build it big and I don't have to cut as much)


---
**Ian Hoff** *August 02, 2015 05:52*

The over-constraint comment refers to systems where a wobble in the z gets translated into xy wobble. 

Here is an article from reprap magazine 2 years ago. This is where that information is derived but may or may not affect the herculien design I havent gotten into it far enough yet to know for myself.



[http://reprapmagazine.com/issues/1/](http://reprapmagazine.com/issues/1/)



Sorry, I dont have a cleaner link to it the article is on page 29


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/NLMN2yhitsn) &mdash; content and formatting may not be reliable*
