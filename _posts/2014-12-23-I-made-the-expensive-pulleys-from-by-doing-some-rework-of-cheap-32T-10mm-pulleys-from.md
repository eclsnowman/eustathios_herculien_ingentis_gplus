---
layout: post
title: "I made the expensive pulleys from by doing some rework of cheap 32T/10mm pulleys from ."
date: December 23, 2014 20:37
category: "Show and Tell"
author: Florian Schütte
---
I made the expensive pulleys from #misumi by doing some rework of cheap 32T/10mm pulleys from #robotdigg . I milled off the collar and drilled M3 threads between the teeth.

I hope it will work like expected. But some parts didn't made their way to my postbox before christmas. So it may take some time to test the pulleys.



![images/3216e81193fbee4bf48565b35e4cb0a3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3216e81193fbee4bf48565b35e4cb0a3.jpeg)
![images/e84ec09c17e0e1c40c431001bed4ebd7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e84ec09c17e0e1c40c431001bed4ebd7.jpeg)
![images/b9de77623390f5552d477139d1118b8b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b9de77623390f5552d477139d1118b8b.jpeg)
![images/b2075c28afb8c6b373bf9e303ca47276.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b2075c28afb8c6b373bf9e303ca47276.jpeg)

**Florian Schütte**

---
---
**Rick Sollie** *December 23, 2014 20:48*

I was planning on trying this myself. Let us know how the tests go.


---
**SalahEddine Redjeb** *December 23, 2014 20:58*

well, looks good so far, please update whenever possible


---
**Maxim Melcher** *December 23, 2014 23:23*

Great work! 


---
**Mike Thornbury** *December 24, 2014 00:38*

Did you need 32T for the speed? I have about 55 25T 3mm HTD sprockets if they are any use. Aircraft-grade alloy, US made, will sell for only a couple of dollars each. only 5mm bore, though.



EDIT: A couple of our dollars, which is about a pound/euro.


---
**Eric Lien** *December 24, 2014 01:05*

Great work.


---
**ThantiK** *January 04, 2015 17:30*

Is this how the misumi pulleys are made as far as set screw placement?  Almost seems like it would be better to put the set screw directly in the middle of a single tooth, so you're only losing 1, vs losing the partial of 2.


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/cXZeSYrNcVR) &mdash; content and formatting may not be reliable*
