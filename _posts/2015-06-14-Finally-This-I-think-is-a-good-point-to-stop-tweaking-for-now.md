---
layout: post
title: "Finally! This I think is a good point to stop tweaking for now"
date: June 14, 2015 23:11
category: "Discussion"
author: Ben Delarre
---
Finally! This I think is a good point to stop tweaking for now.



Turns out I had to tighten the x axis motor belt, I had completely forgot about the motor belts, and was busily tightening all the other belts on the machine but not thinking about that one at all.



Tighter belts and the combination of retraction negative restart distance of 0.3 mm and the 0.2mm coasting distance have cleaned everything up considerably.-



Thanks all! And let my stupidity be a lesson to everyone else :-)﻿



![images/609374c50de3af9542d1d14b231064ff.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/609374c50de3af9542d1d14b231064ff.jpeg)
![images/b710aaf1f8a1edf599bbd62d146b5a64.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b710aaf1f8a1edf599bbd62d146b5a64.jpeg)
![images/ba09b376a5c0a4ac84f194b374e2bae3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ba09b376a5c0a4ac84f194b374e2bae3.jpeg)
![images/d2ab502abc19b42b12bf43dc1c023576.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d2ab502abc19b42b12bf43dc1c023576.jpeg)
![images/ab420fd24941ef1a35f4f4b2672d3582.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ab420fd24941ef1a35f4f4b2672d3582.jpeg)

**Ben Delarre**

---
---
**Eric Lien** *June 14, 2015 23:47*

Tiny bit more tuning on retract, coast, and negative restart and you will be there. But great looking results.﻿


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/F8FuSfYm8uB) &mdash; content and formatting may not be reliable*
