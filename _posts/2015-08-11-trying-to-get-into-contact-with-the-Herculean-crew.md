---
layout: post
title: "trying to get into contact with the Herculean crew...."
date: August 11, 2015 17:42
category: "Deviations from Norm"
author: argas231
---
trying to get into contact with the Herculean crew.... about testing 3D EeZ





**argas231**

---
---
**Eric Lien** *August 11, 2015 22:09*

Hey, I think you already shipped me some on Sunday. As soon as I get a chance to test it I will put my review up here for the group.


---
**Frank “Helmi” Helmschrott** *August 11, 2015 22:27*

what's that 3D EeZ? Never heard of it.


---
**argas231** *August 11, 2015 23:27*

sorry    [www.3D-EeZ.com](http://www.3D-EeZ.com)    Richard Horne  Blogged about it...    I was hoping to send you some   so that you coulkd test it to see if you might want to bundle it with your 3-D Printer...    Mike Paysoin of Makers Tool Works  loves it   and as such has Bundled it with his 3-D Printers as well as he now Distributes it on his website...     Sorry    but then   I guess I am still trying to make a go of things..  Heck   the Navy and NASA are running test on it now...   


---
**Eric Lien** *August 11, 2015 23:31*

**+argas231** I will definitely test it. I have been using hairspray exclusively for years now. One thing to note is the HercuLien is not a commercial product. Just an open source labor of love. But if the stuff works I will evangelize it for you. For some reason people seem to value my opinions. 


---
**argas231** *August 11, 2015 23:31*

By the way     we are also Partnered with  Cinter Design in London UK   



Chris Verbick



Managing Director



Cinter



The Crypt, St Mary Magdalene Church, Holloway Road, London, N7 8LT



tel : +44 20 7607 0704

mob : +44 7927 179 384



[www.cinterdesign.com](http://www.cinterdesign.com)



Perhaps    I will contact him and have him send you some samples...



Tony G   


---
**Øystein Krog** *August 11, 2015 23:32*

I am currently using it on another printer and I am very happy with it. Can't find hairspray that works.


---
**argas231** *August 11, 2015 23:40*

Thank you...  Glad it helps


---
**Isaac Arciaga** *August 11, 2015 23:46*

**+Eric Lien** I think you'll like it. When you receive your sample I suggest you do not coat the entire build area of your Herc/Eustathios. Try a 150x150mm square first on the center with 2-3 thin coats so you get a feel on how it lays down. I've been printing ABS dead center with the same 3 coats 53 times now on one printer and over 30 times on another. 


---
**Eric Lien** *August 12, 2015 00:19*

**+Isaac Arciaga** thanks for the tips. 


---
**Brandon Satterfield** *August 12, 2015 03:21*

**+argas231** we do value **+Eric Lien** 's opinion, he's an amazing maker and one hell of a guy!



I'm interested in this product as well. 


---
**Prabhdial Singh** *August 12, 2015 22:45*

**+argas231**​​ i just want to find out about shipping to canada, how much it'd cost and do i order on USA page or international??

Thank you﻿


---
**argas231** *August 12, 2015 23:07*

well    you would think because we as so close together it would be the US     Not correct...  I sent a Liter to Canada   cost me $44...   But from London  it was around $12...  so    yes   Order from the Euro Button if you live outside the US or Its Territories     Thank you   that was a great Question    Chris at the London office will ship it the same day...  


---
**Mike Thornbury** *August 14, 2015 07:21*

**+argas231** have you got a distributor in australia/nz/SE Asia?


---
**argas231** *August 14, 2015 12:15*

we do ship to Australia and asia from our London Office...    But we are open to discussion pon the subject


---
*Imported from [Google+](https://plus.google.com/118416530699505149662/posts/YXtLU4mGw9D) &mdash; content and formatting may not be reliable*
