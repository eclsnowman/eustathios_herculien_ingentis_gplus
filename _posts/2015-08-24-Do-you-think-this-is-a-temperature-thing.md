---
layout: post
title: "Do you think this is a temperature thing?"
date: August 24, 2015 21:58
category: "Discussion"
author: Brandon Cramer
---
Do you think this is a temperature thing? The print comes out awesome but has this all over it. The previous print job that is not in this picture was without an extrusion multiplier but still had this stringy stuff all over it. This one is with extrusion multiplier set to 0.96. My temp is currently set to 215C using Printrbot White Filament. 



I guess now that I have typed all this, it could be the fact that I'm printing without the fan to cool the PLA filament?

![images/35d6c16f9f225096aff323e32094e4b7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/35d6c16f9f225096aff323e32094e4b7.jpeg)



**Brandon Cramer**

---
---
**Jeff DeMaagd** *August 24, 2015 22:28*

215 might even be a bit high for PLA. Might be not enough retraction. PLA benefits from a part cooling fan, but I don't know how much that affects this problem. It might.


---
**Eric Lien** *August 24, 2015 22:56*

More retract, or try a higher temp to minimize back pressure. Remember as you go faster the hot end temp does not mean the filament ever actually makes it to that temp. Think of putting you hand quickly into a flame. It never gets to the temp of the flame. It is a combination of Delta T  ( temp difference between plastic and hot end) and time.


---
**Sal42na 68** *August 25, 2015 01:17*

Need more retraction 


---
**Sal42na 68** *August 25, 2015 01:18*

Mine is set for 7mm


---
**Frank “Helmi” Helmschrott** *August 25, 2015 05:43*

Definitely not a cooling issue, i'd also go for a higher temperature, maybe some more retraction.


---
**Roland Barenbrug** *August 25, 2015 07:32*

Retraction would be first thought. What hotend have you installed, as e.g. E3D does require some but not a lot of retraction? Next to that wondering how the object has been sliced (slic3r settings ?) and why there are that many crossings from the upper to to lower part.


---
**Brandon Cramer** *August 25, 2015 17:23*

I'm trying it with 5mm of retraction. I can post my slic3r setting on here. I have some questions about speeding up my prints. Standby...


---
**Brandon Cramer** *August 25, 2015 17:54*

Here is what I have currently for slic3r settings. I'm not sure what setting I need to adjust to get this Eustathios Spider V2 printing faster. I set the retraction to 5mm and so far it looks good. (Not sure what happens when you have too much retraction)



[https://www.dropbox.com/s/x5u7wvl4exz0maa/Slic3r_config_bundle08252015.ini?dl=0](https://www.dropbox.com/s/x5u7wvl4exz0maa/Slic3r_config_bundle08252015.ini?dl=0)


---
**Eric Lien** *August 25, 2015 18:42*

I run some materials up to 7 or even 8mm retract. But my bowden tube is very long, so the majority of that is slack, not actual functional retract distance.


---
**Roland Barenbrug** *August 25, 2015 18:42*

Hi wouldn't be concerned with speeding up until you have perfect prints. Having checked your slic3r settings, speeds are at the safe side. Would suggest to stay there for a while. Looking at the slic3r settings I haven't seen anything alarming explaining the results achieved. Can I suggest to do some basic calibration first as in [http://reprap.org/wiki/Calibration](http://reprap.org/wiki/Calibration) ? At least the layer height, infill and temperature control should deliver perfect results. Advantage of these calibrations is that they use small objects hence spending just a bit of fillament.

Next could you post (dropbox) the stl file of the object you intend to print. Could check how it is being sliced in this part of the world.


---
**Brandon Cramer** *August 25, 2015 20:45*

Done with hairspray.... Got my 3D-EEZ film applied to my glass. Starting a 20 hour print job for one part of the Rorsch X1 Airsoft Rifle. This time I made sure I have enough filament to finish the print.... :)



I uploaded the file below to [https://modelrepair.azurewebsites.net/](https://modelrepair.azurewebsites.net/) since it said it wasn't manifold. Is this generally a good idea?



Rorsch Z1 Airsoft Rifle gear box bottom w joint_fixed:



[https://www.dropbox.com/s/5vxpsohf46a41qi/rorsch%20x1%20Gear%20box%20Bottom%20w%20joint_fixed.STL?dl=0](https://www.dropbox.com/s/5vxpsohf46a41qi/rorsch%20x1%20Gear%20box%20Bottom%20w%20joint_fixed.STL?dl=0)


---
**Eric Lien** *August 25, 2015 20:50*

Yeah, Running Netfab is always a good idea if you didn't make the model yourself. There is a lot of modeling software out there that can make sloppy models which cause printers issues.


---
**Roland Barenbrug** *August 26, 2015 07:48*

So I did a double check and compared with my own settings. You could give it a try with the following settings changed: tick "avoid_crossing_perimeters",  this will make your hotend travel nicer with respect to the object being printed at the price of a bit longer travel.


---
**Roland Barenbrug** *September 01, 2015 21:39*

Brandon, just checking. Has this been solved?


---
**Brandon Cramer** *September 01, 2015 21:41*

Yes. Setting the retraction to 5mm helped a lot! 


---
**Roland Barenbrug** *September 02, 2015 08:14*

Great.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/7BJbbYKBSfB) &mdash; content and formatting may not be reliable*
