---
layout: post
title: "I haven't been paying attention...there's a gantry that's used with a v5 e3d, and has two fans for the hot end and the part being printed, I've seen it on one of out Ingent/Eust/Quad/Things, but I've never seen a link to an"
date: July 12, 2014 01:36
category: "Discussion"
author: Mike Miller
---
I haven't been paying attention...there's a gantry that's used with a v5 e3d, and has two fans for the hot end and the part being printed, I've seen it on one of out Ingent/Eust/Quad/Things, but I've never seen a link to an STL



Any thoughts? The two headed gantry that came with the ingentis parts isn't going to work for my needs. 





**Mike Miller**

---
---
**Eric Lien** *July 12, 2014 01:47*

There is the Eustathios original but Jason modified it for single and using roller bearings . I have it somewhere, I will hunt it down. Bad thing is there is no part cooling fan location. 


---
**Mike Miller** *July 12, 2014 02:17*

I saw one that retained the hot end with zip ties, it was cubic with dual bearings in each axis (what I'm really looking for, but I could of sworn it had fans on both axes, again to cool the hot end and the part being printed...


---
**D Rob** *July 12, 2014 04:55*

if you get me some specs and a sketch I'll model something


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/cXPWHFZnK1m) &mdash; content and formatting may not be reliable*
