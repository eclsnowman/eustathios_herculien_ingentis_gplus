---
layout: post
title: "So I am playing with a fun new (unreleased) controller and some drivers from Roy Cortes at Panucatt"
date: May 24, 2016 04:32
category: "Show and Tell"
author: Eric Lien
---
So I am playing with a fun new (unreleased) controller and some drivers from **+Roy Cortes**​​​ at Panucatt. Dual extruder capable smoothieware based unit. I have been waiting to try this thing for a while. Just my luck. I get everything installed and now I have to travel for work next week. Guess I need to get some energy drinks and do my initial testing and tuning before I go.



My Panucatt azteeg x3 has served me so well. It was sad to pull it off its home of so many years. But I am certain just like Roy's other boards this new board will be a bomb proof workhorse. And his new driver design/footprint is just amazing.



My old front mounted hinged controller box and Viki LCD was just a little bit too tight a fit for the new board. So I designed a new box for the side of HercuLien which mounts the board above a Raspberry Pi3 running Octoprint and a new viki2 mount for the front.











﻿



![images/c46419c64c0923765df9a3f4118ad307.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c46419c64c0923765df9a3f4118ad307.jpeg)
![images/41d914ebb721fa6fe492f47103d73f53.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/41d914ebb721fa6fe492f47103d73f53.jpeg)
![images/9f711cf0379dce6f6ffc320d48e9afe8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9f711cf0379dce6f6ffc320d48e9afe8.jpeg)
![images/6ff6247bef0659086af49bb7630ad699.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6ff6247bef0659086af49bb7630ad699.jpeg)
![images/f00f0f5ff9cea5571874e65aa5db4594.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f00f0f5ff9cea5571874e65aa5db4594.jpeg)

**Eric Lien**

---
---
**Derek Schuetz** *May 24, 2016 04:58*

I just wish he'd get his other x5 in stock


---
**Eric Lien** *May 24, 2016 11:16*

**+Derek Schuetz** I know he is working hard at getting them back in stock. I think there was a component supply issue.


---
**Zane Baird** *May 24, 2016 12:46*

Nice and clean wiring. Can't wait to see how your Herculien performs with a 32bit controller.


---
**Jeff DeMaagd** *May 24, 2016 12:47*

Looks interesting though analog trim pots on the drivers seem like a step back.


---
**Jeremy Marvin** *May 24, 2016 13:15*

**+Eric Lien** Whats the current rating on the drivers?  I'm collecting parts for my build and my motors are rated for up to 3 amps.


---
**Eric Lien** *May 24, 2016 13:17*

**+Jeff DeMaagd** that is just there for option to use manual. They have digital trim pots too. It depends on if you enable it via a jumper on/off.


---
**Eric Lien** *May 24, 2016 13:19*

**+Jeremy Marvin** I have some drivers from Roy that are 4amp capable too :)



Those are going on my SMW3D R7 CNC soon!


---
**Eric Lien** *May 24, 2016 13:26*

**+Zane Baird** I know, after going to smoothieware on my Eustathios and Talos3D Tria Delta I am sold that 32bit is the only way to go for new builds. I have just been waiting for Roy to build a dual extrusion capable board. IMHO Panucatt boards are hands down the best ones out there. And the selection of drivers he is prototyping right now will knock your socks off. He is really covering all the use cases.



Did you notice the cool network daughter board he added :) time to finally play with smoothieware network interface in addition to my standard choice of Octoprint. Gotta love having options!


---
**Jeremy Marvin** *May 24, 2016 13:41*

**+Eric Lien** Awesome!  Do you have links or anything to these?  I've been searching. a lot!




---
**Eric Lien** *May 24, 2016 13:49*

**+Jeremy Marvin**​​ his first released "Bigfoot" driver is here: [http://www.panucatt.com/product_p/bsd109a.htm](http://www.panucatt.com/product_p/bsd109a.htm)



It does require one if his newer form factor boards due to the size and pin configuration. The only available version of those boards is currently his GRBL based Gradus M1 Pro designed for CNC machines: [http://www.panucatt.com/product_p/gm1pro.htm](http://www.panucatt.com/product_p/gm1pro.htm)



But he has plenty of new goodies in the pipeline :)﻿


---
**Jeremy Marvin** *May 24, 2016 14:26*

**+Eric Lien** At least something is in the works!  I too could use a Dual Extruder option, but really want 3 amps for my XYZ.


---
**jerryflyguy** *May 27, 2016 05:06*

**+Eric Lien** any kind of timeline on when the new board is made available to the public? I'm looking to order components but kinda stuck due to the issues w/ the X5 Mini V2 not being available.



Your opinion, do I find a cheap RAMPS 1.4 unit (Amazon has some kits for $~50)  until they start producing again or?


---
**Eric Lien** *May 27, 2016 05:19*

I would wait. I think it could be pretty soon.


---
**jerryflyguy** *May 27, 2016 05:31*

**+Eric Lien** thanks Eric, much appreciated. 


---
**jerryflyguy** *May 27, 2016 17:50*

**+Eric Lien** I see today that they now show a Azteeg X5 Mini V3 under pre-order. They have modular drives! Wondering which stepper drive looks the best?

[http://www.panucatt.com/mobile/Product.aspx?id=37857](http://www.panucatt.com/mobile/Product.aspx?id=37857)


---
**Eric Lien** *May 27, 2016 18:03*

**+jerryflyguy** See... I told you to wait :)



Both the SD6128 and SD5984 are great drivers. I have been testing them both since I got the other board running. The main difference that I can see for you is SD6128 = 1/128 microstep max, and the SD5984 = 1/32 microstep max. Both are much more silent operation than the SD8825, and run nice and cool. Also both are digipot enabled. I tried using SD6128 at 1/128 microstepping. But to be honest it is massive overkill at the resolutions our mechanical side can actually maintain and I actually had missed steps. I think the missed steps were from the processor not being able to keep up with the step count at the high speed I was testing. Since the SD6128 are a $6 premium each... I would go with the SD5984 right now. They perform great in my initial testing.


---
**jerryflyguy** *May 27, 2016 18:07*

**+Eric Lien** yes you did! :)



Noise wise there is no difference between the two? The $6 premium is not my first concern.. Can you 1/64 micro step the SD6128 and get a 'best of both' ? Certainly don't want missed steps, had that on the mill I built, drove me crazy on super long projects (80hr machine jobs).


---
**Eric Lien** *May 27, 2016 18:51*

**+jerryflyguy** certainly can. I ran my SD6128 drivers originally on the X3 using Marlin at 1/32. They run well there too.


---
**Botio Kuo** *May 29, 2016 00:38*

**+Eric Lien** Hi Eric, could you give me the black cover box stl file, pls? I want to make a same one. thx


---
**Eric Lien** *May 29, 2016 01:43*

**+Botio Kuo** I will when I get back. But note that the electronics mount underneath on Eustathios. Also this is a different controller than the X5 mini v3. So it will need to be adjusted.


---
**Blake Dunham** *June 02, 2016 03:44*

**+Eric Lien** Hello Eric. Has Roy said anything about when his smoothie powered dual extruder board would be released? I am torn on whether I should go with the x3/x3 pro with some SD6128 drivers, or hold out for this mystery board. 


---
**Eric Lien** *June 02, 2016 04:15*

The X3 performed on my printer amazingly for years. You won't go wrong with it. 



That said, if it were me and wanted to push the printer to its limits... I would wait.  This smoothieware based controller made by **+Roy Cortes**​ is a thing of beauty. But release dates are not for me to discuss. Roy has mentioned a few tidbits about what he is thinking... but that information should come from Roy, not me.



I will say though, this controller and his driver's are giving me some of the best looking prints I have ever produced on my HercuLien. And that is with much tuning still to go.


---
**Blake Dunham** *June 02, 2016 04:17*

Okay. I was just curious. I'm starting an internship with NSK international and I'm building them a new printer for their prototyping lab. I have the x5 mini v2 for my printer and its been amazing. Thanks for the info though!


---
**Eric Lien** *June 02, 2016 04:35*

**+Blake Dunham** no problem. Also perhaps hit up Roy at sales@panucatt.com and see what he has to say. He is an amazing guy, totally friendly and helpful. 


---
**Blake Dunham** *June 02, 2016 04:46*

**+Eric Lien** also, is it possible to go higher than 1/32 microstepping with the x3?


---
**Eric Lien** *June 02, 2016 06:10*

**+Blake Dunham**​​ not reliably. You will start taxing the printer processor at higher print speeds. That's why the 32bit smoothieware controller Roy is making is so nice.﻿ You can finally use those higher microstep drivers.﻿


---
**Blake Dunham** *June 15, 2016 19:42*

**+Eric Lien** I emailed roy about getting a board. He said he should be able to send me a prototype board to start playing around with. Did roy happen to give you a spec sheet for the new board that I could look at?


---
**Eric Lien** *June 15, 2016 21:53*

**+Blake Dunham** nope no spec sheet. That's what testing prototypes is all about :-)


---
**Blake Dunham** *June 15, 2016 21:54*

**+Eric Lien**​ Well spec sheet as in basic input voltages and such


---
**Eric Lien** *June 15, 2016 21:56*

I run at 24v, I know it works with that :)


---
**Blake Dunham** *June 16, 2016 15:44*

**+Eric Lien** Can you give what overall size of the board is and the mounting hole spacing?


---
**Blake Dunham** *July 12, 2016 15:33*

**+Eric Lien** Hey Eric, Roy sent me over a prototype board, were you able to get the led's and buzzer for the control panel working?


---
**Eric Lien** *July 12, 2016 15:49*

**+Blake Dunham** To be honest... no. But I didn't spend much time on trying. Most of the time I control my printers from Octoprint anyway. But I always like to have an LCD for those... OH CRAP moments when a reset button is handy :)


---
**Blake Dunham** *July 13, 2016 11:56*

Yeah I always make sure to have the reset button exposed and somewhere easy to get to. Everything else on the board seems to be working fine except for the lcd. The lights for the button flash constantly and it looks like it isn't displaying a line of text for some reason.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/6fymRigPWuj) &mdash; content and formatting may not be reliable*
