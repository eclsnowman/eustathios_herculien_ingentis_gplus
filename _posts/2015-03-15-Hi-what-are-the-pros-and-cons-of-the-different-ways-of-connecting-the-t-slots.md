---
layout: post
title: "Hi, what are the pros and cons of the different ways of connecting the t-slots?"
date: March 15, 2015 05:00
category: "Discussion"
author: Daithí Ó Corráin
---
Hi, what are the pros and cons of the different ways of connecting the t-slots? I've been looking at gussets, tapping the ends and 3d-printed braces. I'm looking for accuracy in getting them square. Are any of the above adjustable or do you have to get it spot-on with the install?

Also, for driving the XY axes is there any downside to direct-drive through a shaft coupler as opposed to belt-driven?

Thanks!





**Daithí Ó Corráin**

---
---
**Jeff DeMaagd** *March 15, 2015 12:34*

Gussets are more expensive than the angle brackets, but it seems they can give you a more rigid machine.



I really don't know if direct coupler or belt is better. Belts let me put the motor where I want it, direct drive means motors sticking out the machine's side. But if you're enclosing, that keeps the motor out of the enclosure heat.


---
*Imported from [Google+](https://plus.google.com/104923267981938346235/posts/ckvxKH1E5iW) &mdash; content and formatting may not be reliable*
