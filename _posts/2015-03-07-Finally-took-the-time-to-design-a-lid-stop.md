---
layout: post
title: "Finally took the time to design a lid stop"
date: March 07, 2015 08:25
category: "Show and Tell"
author: Eric Lien
---
Finally took the time to design a lid stop. Prints as one piece with the peg captive in the slot. The slot side uses a 5mmx16mm bolt which just bottoms out on the extrusion allowing the bolt to act as a fixed pivot. Then just adjust the ped side to open to your desired amount.



STL: [https://drive.google.com/file/d/0B1rU7sHY9d8qS0thVnF2d196SG8/view?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qS0thVnF2d196SG8/view?usp=sharing)



![images/ab1a5ce32ed2134ac3f2994a2ebafc27.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ab1a5ce32ed2134ac3f2994a2ebafc27.png)
![images/c059597ec25f949d2e47fbe4580c16c6.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c059597ec25f949d2e47fbe4580c16c6.png)
![images/b484f42051a15ff9aac0b33498ca50d9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b484f42051a15ff9aac0b33498ca50d9.jpeg)

**Eric Lien**

---
---
**Jeff Kes** *March 08, 2015 02:54*

That's really cool!


---
**Kyle Wade** *March 09, 2015 03:28*

simple and effective  nice work !


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/74F64TcG66k) &mdash; content and formatting may not be reliable*
