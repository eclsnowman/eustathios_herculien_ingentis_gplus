---
layout: post
title: "E3D V6 temp sensor. Thinking I'll upgrade my hot end to the new screw retained temp cartridge"
date: September 08, 2016 20:20
category: "Discussion"
author: jerryflyguy
---
E3D V6 temp sensor. Thinking I'll upgrade my hot end to the new screw retained temp cartridge. Is anyone using the PT100 system provided by E3D? I'm using the new Azteeg x5 V3 smoothie board



This was the kit I was looking at [http://spool3d.ca/genuine-e3d-pt100-total-upgrade-kit/](http://spool3d.ca/genuine-e3d-pt100-total-upgrade-kit/)

Anyone used this vs the standard 100k thermistor? 



May hold off until they have the new silicone socks in stock.



Thoughts?

J





**jerryflyguy**

---
---
**Ray Kholodovsky (Cohesion3D)** *September 08, 2016 21:11*

Smoothie doesn't yet support the PT100.  Whether you can get it to work thru the adapter board, is a tricky question. Last I recall you can't just plug it into a thermistor port because they don't need the resistor and capacitor circuit. So maybe if you went as far as to desolder those...


---
**jerryflyguy** *September 08, 2016 23:48*

**+Ray Kholodovsky** ugh.. I think I'll pass if that's the case.. 100K will have to be 'good-enough' 



I had it in my head that the add-on board would translate the signal out of the PT100 to basically duplicate the 100K output but more accurately? I'm a layman when it comes to this detailed/specific stuff


---
**Jim Stone** *September 09, 2016 01:00*

They arent a genuine reseller ...or atleast the website says not.




---
**jerryflyguy** *September 09, 2016 01:51*

Yeah I noticed they aren't listed on E3D's site, not sure if it's a blatant lie or just not updated on E3D's site. They sure claim to sell genuine parts but.. Would confirm w/E3d before I bot from them.. Doesn't appear like it's worth doing though as the kit isn't compatible w/ my Azteeg x5 


---
**Eric Lien** *September 09, 2016 02:51*

Here is a good discussion on it: [github.com - Does Smoothieware support RTD temperature sensors? · Issue #683 · Smoothieware/Smoothieware](https://github.com/Smoothieware/Smoothieware/issues/683)


---
**jerryflyguy** *September 09, 2016 03:04*

Thanks **+Eric Lien** , looks like it's a no-go presently. I'm ok w/ that will still probably upgrade the block, but still use the 100K when I do. Many other things to handle before that point 😉


---
**Jeff DeMaagd** *September 09, 2016 12:16*

Smoothieware supports thermocouples with a digital thermocouple adapter. So if you need 300°+ temps they're an option worth testing. I own the parts but haven't run it yet.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/HNzHcWivycM) &mdash; content and formatting may not be reliable*
