---
layout: post
title: "Hey Guys, Any Ideas what would be causing this?"
date: August 13, 2015 18:00
category: "Discussion"
author: kaley garner
---
Hey Guys, Any Ideas what would be causing this? Model appears fine in S3D preview and then when it is sliced....looks like this.

![images/47b537909878e6262b68b9317fbe9c82.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/47b537909878e6262b68b9317fbe9c82.jpeg)



**kaley garner**

---
---
**Frank “Helmi” Helmschrott** *August 13, 2015 20:29*

Looks like the Stl file is broken and s3d isn't able to repair it. can you hand us the Stl so we could check?


---
**Erik Scott** *August 13, 2015 20:29*

Make sure all the model's normals are correct (pointed outwards). Also make sure there are no holes in the mesh and no floating verts or faces not attached to the main model. You can download Blender to take a look and clean it up. 


---
**Liam Jackson** *August 13, 2015 20:45*

You can try running your stl through netfab, they have a free cloud repair service [https://netfabb.azurewebsites.net](https://netfabb.azurewebsites.net)


---
**Sal42na 68** *August 13, 2015 21:35*

Netfab then slice with cura


---
**Erik Scott** *August 13, 2015 21:39*

You could use netfab as well, but it's not going to give you the control a mesh modeling program would. Blender has a built-in addon that will check for 3D print issues as well. 


---
**kaley garner** *August 13, 2015 22:08*

Thanks so much for your help guys. It's a file I was playing with off of thingiverse here:[http://www.thingiverse.com/thing:852939](http://www.thingiverse.com/thing:852939) 



I did run it through netfabb, and fixed the holes but still no luck.... :/


---
**Ian Hoff** *August 14, 2015 02:15*

I got identical results from slic3r at .25 and .1 layer heights that model is just too thin at those parts to generate  proper code.

[http://i.imgur.com/19CIGrV.png](http://i.imgur.com/19CIGrV.png)



I then enabled slic3rs "detect thin walls" which gave me something printable. if S3d has something similar that should fix it.

[http://i.imgur.com/Y0jJjsC.png](http://i.imgur.com/Y0jJjsC.png)


---
**Erik Scott** *August 14, 2015 02:26*

Yesh, this model has a lot of issues. Many separate parts, faces facing the wrong way... yeah, not really suitable for printing. 


---
**Sal42na 68** *August 14, 2015 02:37*

Try cura for slicer


---
**Jeff DeMaagd** *August 14, 2015 02:53*

**+Sal Fortuna** Cura shows the same problem. I suspect the walls are way too thin for the slicing strategy that S3D and Cura use.


---
**Erik Scott** *August 14, 2015 03:36*

Walls are too thin. Normals are inverted. The thing isn't a single watertight mesh. The mesh itself is in many pieces. Some of this the slicer can handle, but not the wall thickness. Judging by the picture, it's the wall thickness that's giving you grief. idk if you had a specific size in mind, but if you made it larger it might make the walls thick enough to handle. 


---
*Imported from [Google+](https://plus.google.com/104940700298630363140/posts/Nov8VWWd6iQ) &mdash; content and formatting may not be reliable*
