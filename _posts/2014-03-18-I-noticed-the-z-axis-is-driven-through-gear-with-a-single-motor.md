---
layout: post
title: "I noticed the z-axis is driven through gear with a single motor"
date: March 18, 2014 04:26
category: "Discussion"
author: BoonKuey Lee
---
I noticed the z-axis is driven through gear with a single motor. Can the design driven via 2 motors with threaded rod attached via coupler, like most reprap printers?



Also, the bed is supported via one or both sides with the z rods? I have also seen single and double smooth rods on each side for the Z-axis.



What are the advantages and disadvantages of each design?





**BoonKuey Lee**

---
---
**Jarred Baines** *March 18, 2014 07:22*

4 smooth rods are just gonna give u overkill stability. If that sounds good to you then go with the 4 rods :-) the xmaker is the design with the z leadscrews, I think all others have belts, but it is also the one with double rods. The leadscrews / belts are attached on both sides of the z stage to move it smoothly.



You can hook up the leadscrews or belts to two steppers just like a Mendel design, same wiring, in the end this is still a reprap and the electronics could be used on or even taken from a Mendel design.



I like the idea of twin steppers on the z stage, to me it means that the stage is more likely to be driven from both sides simultaneously, whereas a belt driven design could only have more slop (although this may be undetectable).



But I have had issues on my Mendel where one stepper has stalled and the other hasn't, twisting the stage out of square and would probably break something if I weren't watching it... The leadscrews should be MUCH smoother than my m5 rods though so it may never happen in this design but... there's a few pros and cons for ya ;-)


---
**Eric Moy** *March 18, 2014 11:04*

Do not use 4 smooth rods, it'll be severely overconstrained. Use only 2 rods and single threaded rod. Otherwise, and misalignment, including inevitable kinks and imperfections in threaded rod, will cause the z axis to bind


---
**Jarred Baines** *March 18, 2014 13:36*

They're ballscrews - no reason to have only one - and the 4 posts is working fine, its possibly not necessary, but it hasn't caused binding...


---
**BoonKuey Lee** *March 19, 2014 00:22*

I am using 8mm rod and leadscrew that's why I am thinking of 4 rods. 



I will use 2 motors as well. 


---
**Jarred Baines** *March 19, 2014 00:24*

I don't see any problems there ;-)


---
*Imported from [Google+](https://plus.google.com/105655952080130147525/posts/jnHpQYWi2MS) &mdash; content and formatting may not be reliable*
