---
layout: post
title: "Alrighty, I've made some good progress this weekend finally, had the gantry moving pretty nicely, not quite one finger smooth but close"
date: February 04, 2018 00:08
category: "Discussion"
author: Ryan Fiske
---
Alrighty, I've made some good progress this weekend finally, had the gantry moving pretty nicely, not quite one finger smooth but close. Put the motors on and did some movements and got lots of friction in all directions. Loosened up some of the bearing holders and moved them around and got even closer! However, I am still getting friction in the X axis, but only one direction, the other direction is smooth. Any idea what causes this particular issue?





**Ryan Fiske**

---
---
**Eric Lien** *February 04, 2018 01:16*

Have you run the break-in gcode for a while yet. When you do things tend to settle into equilibrium pretty well. You are in the home stretch. This is the last tricky hurtle, and one of the most patience testing portions of the build. But once you get through it, its pretty smooth sailing.


---
**Ryan Fiske** *February 04, 2018 04:21*

Thanks again **+Eric Lien** for the advice. I'll run it and see what happens! I noticed that the stock code has current set for 1.5A. I used to run about 0.4 on my Kossel, is 1.5 necessary to push this printer generally speaking?


---
**Eric Lien** *February 04, 2018 13:24*

**+Ryan Fiske** no, you can run way less. 1.5 is max motor current. I need to change that.


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/hNLD77Gd9ZP) &mdash; content and formatting may not be reliable*
