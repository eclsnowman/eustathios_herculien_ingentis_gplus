---
layout: post
title: "Poly-V Ribbed Belt"
date: October 16, 2015 15:30
category: "Discussion"
author: Lee Shawn (3DP2GO)
---
Poly-V Ribbed Belt.

![images/0524db8d1c8700155ce9b96930223c97.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0524db8d1c8700155ce9b96930223c97.jpeg)



**Lee Shawn (3DP2GO)**

---
---
**Michaël Memeteau** *October 16, 2015 18:40*

What's the catch?


---
**Jeff DeMaagd** *October 16, 2015 22:46*

Wouldn't that increase the drag on the drive system to expend energy flexing a thicker belt?


---
**Eric Lien** *October 18, 2015 01:49*

Might be good with matched groved pullies as exterior idlers for alignment on long runs?


---
*Imported from [Google+](https://plus.google.com/112626826206243668100/posts/bqFjrnaSEbd) &mdash; content and formatting may not be reliable*
