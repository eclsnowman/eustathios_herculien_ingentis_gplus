---
layout: post
title: "Coming together and hit yet another snag"
date: July 09, 2015 02:24
category: "Discussion"
author: Rick Sollie
---
Coming together and hit yet another snag. Everything was sliding very smoothly individually but after assembly its rather snug. Obviously its alignment somewhere. 



My thought was to loosen all the pulleys and use Eric's alignment tools (8mm to 10mm spacer) on first the x then the y.



Any other suggestions? 

![images/639877cb3ee58db5624cf9e165581aa4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/639877cb3ee58db5624cf9e165581aa4.jpeg)



**Rick Sollie**

---
---
**Eric Lien** *July 09, 2015 02:51*

Break-in is the key to theses machines. If you think about the compound tolerance involved in the assembly it is quite extensive. Run the break-in gcode several times. Then tweak and adjust the corners bit by bit. When it is right... You will know. It is kind of an Ah Ha moment. At that point things will work without any tweaking for a long long time.


---
**ThantiK** *July 09, 2015 02:57*

The big thing is to make sure the rods are parallel; also, unplug your motors -- If they're bare wire and the wires are touching, they will electrically brake, making it seem harder to rotate.


---
**Mike Miller** *July 09, 2015 03:02*

I'll set one side high, leaving the other three corners loose, then wal the carriage down each axis, then finally the far corner. #IGentUS is a smoooth mammajamma... #ingentilire still needs a little tweaking. 


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/Wb879b8wsn5) &mdash; content and formatting may not be reliable*
