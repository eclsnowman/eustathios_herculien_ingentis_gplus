---
layout: post
title: "Suddenly I'm losing steps on my printer"
date: October 15, 2016 19:42
category: "Discussion"
author: jerryflyguy
---
Suddenly I'm losing steps on my printer. It seems to always go one direction on the list steps. When I abort the print it sits and makes an odd ticking/thumping noise in the motors.



Running the Azteeg x5 v3 w/ sd8825 drivers.



2000 accel, 1.5a current. I upped this to 2.0 but no change.



Video of it here 
{% include youtubePlayer.html id=E-3mVODVQW4 %}
[https://youtu.be/E-3mVODVQW4](https://youtu.be/E-3mVODVQW4)





**jerryflyguy**

---
---
**Jeff DeMaagd** *October 15, 2016 19:51*

Maybe cooked the motor driver. I've never gotten rated current out of a driver module like that.


---
**Miguel Sánchez** *October 15, 2016 19:51*

1.5a might be too much current, not for the motor but for the driver, that might be entering ocassionally into thermal shutdown. 



Do you have a fan blowing on your drivers?


---
**Roland Barenbrug** *October 15, 2016 20:23*

Have you put a fan on top of your stepper drivers? If not, it's likely they become hot and disable themselves as a measure of protection.


---
**Eric Lien** *October 15, 2016 20:25*

The stepper only can handle 1.5A Max. I run lower.


---
**Eric Lien** *October 15, 2016 20:34*

Yes, I don't have it modeled in, but I run a fan blowing over the drivers in from the side, and then ducted over the drivers. I can almost guarantee you are seeing thermal throttling on the drivers if you don't have a fan. The TI drivers run hot.


---
**jerryflyguy** *October 16, 2016 03:13*

Thanks everyone, so is it a matter of the drives cycling themselves on and off (to save themselves from over-temp & letting out the magic smoke), which is fixed w/ a simple fan or is it more likely they are cooked and will need replacement? I'd planned to print some kind of duct to improve what cooling is available from the sinks once they are mounted up under the base plate. I suppose it's worth trying a simple fan on the drives as they are, see if that helps. Doesn't it list 2.5a as rated current for these 8825's? I did notice they were pretty warm at 1.5a but figured if the literature said they needed to have the sinks on them if running more than 1.0a, that 1.5 would be ok w/out a fan(initially anyway).



Will drop the current back to 1.5a and put some kind of airflow on them & report back.



**+Eric Lien** what current do you run your X/Y at? Am I pumping more than I need (@ 1.5a) to run 2000 accel and 60mm/s?


---
**Eric Lien** *October 16, 2016 04:08*

**+jerryflyguy**​ the drivers are likely fine... The steppers though if you over current them will degrade the windings. The max rated current for the stepper is 1.5A. I still have an old analog pot driver on my Eustathios. I think I am around .65 vref which is 1.3A.


---
**jerryflyguy** *October 16, 2016 06:55*

**+Eric Lien** ok, I'll put a fan on it tomorrow and see what it does. I think I've got the 2a steppers from smw3d so I think (fingers crossed) I'm ok current wise for the motors. They've only ran a a handful of minutes at 2a. I'll for sure dial them back, will drop to 1.3a and see if that helps temp wise. 



Thanks!


---
**Eric Lien** *October 16, 2016 13:16*

**+jerryflyguy** ah, Ok. I thought you had the ones from Robotdigg in the BOM.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/MJhj6yY5ZgK) &mdash; content and formatting may not be reliable*
