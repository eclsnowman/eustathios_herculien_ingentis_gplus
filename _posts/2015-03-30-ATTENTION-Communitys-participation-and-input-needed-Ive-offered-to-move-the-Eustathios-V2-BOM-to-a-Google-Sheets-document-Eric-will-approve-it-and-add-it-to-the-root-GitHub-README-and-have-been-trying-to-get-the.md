---
layout: post
title: "ATTENTION: Community's participation and input needed. I've offered to move the Eustathios V2 BOM to a Google Sheets document (Eric will approve it and add it to the root GitHub README), and have been trying to get the"
date: March 30, 2015 01:26
category: "Discussion"
author: Seth Messer
---
ATTENTION: Community's participation and input needed.



I've offered to move the Eustathios V2 BOM to a Google Sheets document (Eric will approve it and add it to the root GitHub README), and have been trying to get the product numbers corrected as well as links to the actual product pages on the various sites (have only completed most of Robotdigg's parts so far).



For reference, here is the Google Sheets document at its current state:

[https://docs.google.com/spreadsheets/d/1ATz5AoIUtASowBtlXsOb8FVt8YLd0A-wu1EWHHHPDnA](https://docs.google.com/spreadsheets/d/1ATz5AoIUtASowBtlXsOb8FVt8YLd0A-wu1EWHHHPDnA)



Those of you that have purchased from either the HercuLien, or the Eustathios V1 or V2 BOMs (/cc **+Eric Lien** **+Jason Smith** **+Dat Chu** **+Derek Schuetz** **+Daniel Salinas** **+Isaac Arciaga** **+Oliver Seiler**) , I've got a few items I'm having troubles finding. Could y'all confirm the following?



Amazon:

=============================

* G3N_220B_SSR (Omron Solid State Relay), which one? 

[https://www.google.com/search?q=site%3Aamazon.com+G3N_220B_SSR&oq=site%3Aamazon.com+G3N_220B_SSR&aqs=chrome..69i57j69i58.3328j0j7&sourceid=chrome&es_sm=119&ie=UTF-8](https://www.google.com/search?q=site%3Aamazon.com+G3N_220B_SSR&oq=site%3Aamazon.com+G3N_220B_SSR&aqs=chrome..69i57j69i58.3328j0j7&sourceid=chrome&es_sm=119&ie=UTF-8)



Robotdigg:

=============================

* Micro Switch Button (NO / NC Limit Switch), is this the one?

[http://www.robotdigg.com/product/154/Microswitch-Board-w/-Lead-Wires-for-3D-Printers](http://www.robotdigg.com/product/154/Microswitch-Board-w/-Lead-Wires-for-3D-Printers)



* Nema17-40mm-Stepper-Gearmotor (40mm Long NEMA 17 Stepper w/ 5.18 Planetary Reduction), is this the correct one?

[http://www.robotdigg.com/product/103/Nema17-40mm-Stepper-Gearmotor](http://www.robotdigg.com/product/103/Nema17-40mm-Stepper-Gearmotor)



Those are all I've got questions on so far.



Would <b>love</b> if you guys/gals that have already purchased these parts could chime and and confirm for me.



Thanks!

Seth





**Seth Messer**

---
---
**Oliver Seiler** *March 30, 2015 02:05*

Good effort Seth. As I'm based in NZ I'm cut off many US vendors, or shipping is prohibitively expensive. I got many of my parts via AliExpress and could provide some alternative links (though shipping might be more expensive for those in the US).

As to the parts in questions:

Steppers: [http://www.robotdigg.com/product/241/0.9%C2%B0-Step-Angle-Nema17-48mm-Stepper-Motor](http://www.robotdigg.com/product/241/0.9%C2%B0-Step-Angle-Nema17-48mm-Stepper-Motor)

End stop switches: [http://e3d-online.com/Electrical/Components/Endstop-Microswitches-Omron
GT2](http://e3d-online.com/Electrical/Components/Endstop-Microswitches-Omron%0AGT2) belt: [http://www.aliexpress.com/item/Hot-sale-20meter-GT2-6mm-open-timing-belt-width-6mm-GT2-belt/1625932969.html](http://www.aliexpress.com/item/Hot-sale-20meter-GT2-6mm-open-timing-belt-width-6mm-GT2-belt/1625932969.html)


---
**Isaac Arciaga** *March 30, 2015 02:06*

**+Seth Messer** I can confirm the geared stepper you linked from RobotDigg. A good reputable name brand alternate for that motor available from a US vendor here [http://www.tridprinting.com/Electronics/](http://www.tridprinting.com/Electronics/) it's the Kysan 1040229.  **+SMW3D** also has the 32t pullies (even without shoulders) here [http://www.smw3d.com/gt-pulleys-gt2-and-gt3/](http://www.smw3d.com/gt-pulleys-gt2-and-gt3/) in case it's not mentioned in the V2 BoM.



I'll try to make some time tonight before bed to take a look at the V2 BoM and pitch in on adding US vendors.


---
**Seth Messer** *March 30, 2015 02:07*

**+Oliver Seiler** Huge thanks! I'd love to at least get enough detail/description into each line item so that all countries can have enough info to at least know what to search for.


---
**Seth Messer** *March 30, 2015 02:08*

Thanks much **+Isaac Arciaga**!


---
**Isaac Arciaga** *March 30, 2015 02:35*

**+Seth Messer** no sir, Thank You!


---
**Eric Lien** *March 30, 2015 03:50*

**+Eric LeFort** the formatting is a little confusing at first. The qty per sub assembly column shows how many are required for that assembly. The total to the right is the qty x number of that subassembly (since the printer may require multiples of that sub assembly). Unfortunately the pivot table (sheet 2) that summarizes the totals didnt translate well in google docs. That pivot table by vendor was my attempt to make the ordering sheet where the links would exist.


---
**Eric Lien** *March 30, 2015 03:54*

**+Oliver Seiler** I have had some issues with the 0.9deg steppers. The added steps required forced me to lower my micro stepping on my 8bit controller because it would stutter at high speeds. With smoothieware it is much less of an issue, so if people plan on using 0.9deg steppers I recommend the higher speed controllers like the azteeg x5 mini.


---
**Mykyta Yurtyn** *March 30, 2015 04:15*

**+Seth Messer** are 5972K164 FastEddy bearings (line 49 on vendor pivot) same 10x22x6 as 6900 (line 33)?


---
**Mykyta Yurtyn** *March 30, 2015 04:20*

**+Seth Messer** **+Eric Lien** If you want to get more alt. sources for extrusions, I ordered mine here: [http://openbuildspartstore.com/black-v-slot-linear-rail/](http://openbuildspartstore.com/black-v-slot-linear-rail/) They are not cut or drilled, same as smw3d. Prices are also the same with better materials availability and a little better shipping cost.


---
**Oliver Seiler** *March 30, 2015 08:17*

**+Eric Lien** good to know. I've got these from my old printer where they work fine with a RAMPS board, but I haven't been pushing for speed. My  Eustathios will run Smoothieware ;)


---
**Seth Messer** *March 30, 2015 12:04*

**+Eric Lien** thanks for the heads up on the pivot table and what you had intended. I'll get that cleaned up too!


---
**Seth Messer** *March 30, 2015 13:36*

**+Mykyta Yurtyn** yes sir, these, on fasteddy ([http://www.fasteddybearings.com/10x22x6-rubber-sealed-bearing-6900-2rs/](http://www.fasteddybearings.com/10x22x6-rubber-sealed-bearing-6900-2rs/)) are the same as the ones I have on the Google Sheets, purchasable via Amazon link.


---
**Seth Messer** *March 30, 2015 13:50*

**+Mykyta Yurtyn** the amazon link for the 6900 bearings has been switched to the 10 pack at FastEddy.


---
**Seth Messer** *March 30, 2015 14:47*

Good news, <b>almost</b> all of the parts have been verified and have links, and I believe **+Eric Lien** went ahead and added a cleaner pivot table tab, labeled as "Sum of Parts by Vendor" at the bottom. Please note, most of the McMaster-Carr parts are sold as packs of xx. The quantity Eric has specified in the BOM is for individual pieces, not quantity of packages. I'll get the last few links done throughout the rest of the day.



I'm still unsure on the Solid State Relay, as to exactly the one we should order..


---
**Eric Lien** *March 30, 2015 15:38*

I linked the SSR last night. Also, Most of the hardware can be purchased tons cheaper elsewhere. But McMaster makes everything really easy since they supply Solidworks Native Models.



Trimcraft Aviation ( [http://www.trimcraftaviationrc.com/](http://www.trimcraftaviationrc.com/)) is the best quality/price I have found in the states. Plus they are Stainless Steel. Unfortunately they only carry about 80% of the hardware needed.


---
**Seth Messer** *March 30, 2015 15:48*

**+Eric Lien** I saw you had linked the SSR, but it was a broken amazon link :/ i ended up just buying from your commented ebay link.. 


---
**Eric Lien** *March 30, 2015 16:49*

**+Seth Messer** link should be good now.


---
**Eric Lien** *March 30, 2015 18:11*

Unfortunately someone did some drag down copies (likely to quickly add the links for similar parts).



It borked up the assembly numbering and color schemes in the process.



Looks like it could be a painful fix. 


---
**Seth Messer** *March 30, 2015 18:20*

Sigh, "someone" would either be you or me, and I doubt it was you. I did copy/paste (contents ONLY!) for the McMaster-Carr parts numbers that matched. I did not alter quantities. I'll go back through it and double check everything from the original xls.


---
**Seth Messer** *March 30, 2015 19:27*

**+Eric Lien** Looking back at what I did, I'm certain i was copying/pasting the value (link text/url) from same Part Number cells. I see what happened though.. I sorted a specific column, I believe Part Number, to make it easier to see what had links and what didn't. That's the only thing I can think of because there is one revision from your first change and mine at 9:44am CST.



Will keep you posted.


---
**Seth Messer** *March 30, 2015 19:32*

I'm retracting viewing privileges from the general public and until I can get that column back to good.. and NEVER sort on part number column ever again.



Sorry all, for the inconvenience.


---
**Eric Lien** *March 30, 2015 20:28*

**+Seth Messer** I hope you didn't take offense. But yes if you do a sort, then drag copy it unfortunately brings more than the values along for the ride. But everything can be fixed. No big worries.


---
**Seth Messer** *March 30, 2015 20:29*

**+Eric Lien** no offense taken at all! i was just bummed to have caused you grief with this. was wanting to do this to help you out and get it to a point where people can see what's changed if more changes are necessary. that said. i fixed it all. :) i still haven't filled in the "openbuild" vendored items with their links, but i believe that's all that's left. please give it a once over if you don't mind.


---
**Eric Lien** *March 31, 2015 00:41*

**+Seth Messer**​ looks great. I will try to add some details and alternative sources and pricing. Also I will add links to the github stl's for the printed parts (that seemed to help people for the HercuLien BOM). Now If I could only find a good and inexpensive source for CNC acrylic and aluminum parts for people. If anyone has a suggestion I am all ears.﻿


---
**Gus Montoya** *March 31, 2015 01:32*

**+Seth Messer**   So the parts I already ordered were not the correct one?

I'm confused now. 


---
**Bruce Lunde** *March 31, 2015 01:39*

**+Seth Messer** my invoice for the geared step motor shows this detail:

Nema17 40mm Stepper Gearmotor	GSM002	$30.00	0.45KG


---
**Eric Lien** *March 31, 2015 02:05*

**+Gus Montoya** what changed?


---
**Seth Messer** *March 31, 2015 03:29*

**+Gus Montoya** no. You're fine, you likely haven't used the Google Sheets document yet and were using the original V2 excel document on github. ﻿


---
**Gus Montoya** *March 31, 2015 04:46*

Ok, tomorrow my 2nd order of 2 will arrive. So I am crossing my fingers that I will be able to at least have the frame up. 


---
**Brandon Cramer** *March 31, 2015 05:57*

Is there going to be a HercuLien Google Sheet coming soon? 



I haven't used Google Sheets. I assume only a select few will be able to make changes to it? 


---
**Eric Lien** *March 31, 2015 06:21*

**+Brandon Cramer** I will look into adding this in the future. 


---
**Brandon Cramer** *March 31, 2015 06:24*

Thanks **+Eric Lien**. On the HercuLien bom , is the excel spreadsheet the same as the PDF? The PDF shows V2. Almost done printing all the parts for the HercuLien. :)


---
**Eric Lien** *March 31, 2015 06:34*

**+Brandon Cramer** I think so. I made some formatting changes a while back after some design changes. The excel is the master, use that first.


---
**Rick Sollie** *April 01, 2015 23:59*

**+Eric Lien**

 So is this complete enough to start ordering?  I have to run a comparison between this an V1 as I already ordered some the parts.

Thanks!


---
**Seth Messer** *April 02, 2015 00:18*

**+Rick Sollie** you can use either the google sheets version or the xls version on **+Eric Lien**'s github for eustathios v2. i've ordered all but my misumi parts from the google sheets doc.


---
**Gus Montoya** *April 02, 2015 03:19*

2nd order came in, and it's not correct. Will have to go back to look to varify. Anyone can tell me if returns to misumi is easy or painful?


---
**Eric Lien** *April 02, 2015 03:36*

**+Gus Montoya** what was incorrect?


---
**Gus Montoya** *April 02, 2015 05:18*

**+Eric Lien**  I think it's my fault for messing up the order. I need to recheck, but at this time due to my recent circumstance I'll be returning everything. I hope returns are not painful and long for misumi.


---
**Eric Lien** *April 02, 2015 16:35*

Since misumi parts are predrilled and cut to length I doubt you can return them.


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/3vPpixypATm) &mdash; content and formatting may not be reliable*
