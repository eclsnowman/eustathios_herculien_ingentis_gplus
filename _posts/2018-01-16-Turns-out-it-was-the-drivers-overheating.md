---
layout: post
title: "Turns out it was the drivers overheating"
date: January 16, 2018 01:52
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Turns out it was the drivers overheating.



So it’s doing small prints perfectly but the second layer is waaaaay to high up on larger ones...



Using an X5 GT SD2224 drivers.



My steps per mm are correct.



Need to resolve tonight, any feedback is helpful, thanks!

![images/8ce026202cd83f84a140645e820d69ba.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8ce026202cd83f84a140645e820d69ba.jpeg)



**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *January 16, 2018 01:59*

Any chance your z is drifting down? Am I correct that the first layer is perfect... But on the second layer it is too far from the first layer?


---
**Stefano Pagani (Stef_FPV)** *January 16, 2018 02:22*

**+Eric Lien** Yes you are, the same part works just fine when only printing one of them, but when I put 4 it fails. Maybe something with s3d?




---
**Dennis P** *January 16, 2018 02:26*

Do you have gcode offset values plugged into S3D?

What is the behavior of the print in the preview? 

Have you tried another slicer? 


---
**Stefano Pagani (Stef_FPV)** *January 16, 2018 02:41*

**+Dennis P** Preview is fine, have not tried another slicer as I don't have time to reconfigure right now. Its weird because it only happens on large prints.


---
**Stefano Pagani (Stef_FPV)** *January 16, 2018 03:58*

It actually happened on the first layer this time, but on one of the 4 parts. (not bed leveling issue)



Could it be a problem with the micro-stepping and the pitch of the ballscrew?


---
**Eric Lien** *January 16, 2018 04:33*

Maybe your current on Z isn't enough... And its slipping. Maybe a loose z pulley somewhere? 


---
**Michaël Memeteau** *January 16, 2018 09:12*

I remember Thomas Sanladerer having issues with a large bed that was deformed by the dilatation and had symptoms very similar to what you're describing...


---
**Stefano Pagani (Stef_FPV)** *January 16, 2018 13:13*

I think it’s the drivers overheating and lowering the v ref. I don’t have a fan on em :p



Because it did it on the first layer on one when I printed it right after the other one. I think it’s slipping during z hop.



Thanks for all the advice


---
**Dennis P** *January 17, 2018 15:47*

Do you know how hot the drivers were getting before they started being flaky?   Were you using heatsinks? 


---
**Stefano Pagani (Stef_FPV)** *January 18, 2018 01:38*

**+Dennis P** Yes, was running at 1A, not that hot, about 60C by touch.


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/cfU3mKHVe3t) &mdash; content and formatting may not be reliable*
