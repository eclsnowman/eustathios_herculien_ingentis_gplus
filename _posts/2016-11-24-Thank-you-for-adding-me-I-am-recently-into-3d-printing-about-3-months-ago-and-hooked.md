---
layout: post
title: "Thank you for adding me! I am recently into 3d printing, about 3 months ago, and hooked"
date: November 24, 2016 18:44
category: "Discussion"
author: Carter Calhoun
---
Thank you for adding me!  



I am recently into 3d printing, about 3 months ago, and hooked.  Built a Prusa I3 clone kit, then a Chinese Delta kit, and now looking for more of a challenge, a new gantry style and something that I like to look at even when it's not printing.  

 

I nearly settled on the C-Bot (D-Bot), but, didn't really like the idea of the V-slot rail being part of a moving gantry (just didn't seem 'elegant' enough). **+Eric Lien** create the fantastic HercuLien and Spider V2 - exactly what I am looking for.  



I have been reviewing posts for the past few days and am excited to begin the build.  This is a great community!  I am in SoCal, and so nearby  **+Ted Huntington**, **+Botio Kuo**, and **+Derek Schuetz**, from what I have learned from all of your posts thus far.  I would love to meet up or speak with any of you to discuss your experiences in the build.



I will begin sourcing the parts for the Spider V2 build based on the BOM, and look forward to this project.   Best wishes to you all.





**Carter Calhoun**

---
---
**Luis Pacheco** *November 24, 2016 18:56*

It's so easy to get hooked on builds! I'm rebuilding a small printer I built almost 4 years ago, bought a Chinese Delta kit on singles day so I'm building that, and I'm in the process of building a HercuLien. Welcome to the community, it's a great one with tons of help!


---
**Eric Lien** *November 24, 2016 19:04*

**+Carter Calhoun**​ glad to hear you are excited about the build. It would be great if you could meet up with a few of our members. Seeing a printer in person is worth a thousand pictures in posts.



As you go through the BOM there are a few things to note. The controllers available now are much better than two years ago when I built mine. Go with some form of smoothieware based board there are many great ones to choose from from Panucatt and now C3D.



I would go with ball screws over lead screws. The cost is actually cheaper than misumi and has several benefits. GolMart on AliExpress will custom cut them for you, and for Eustathios they actually have one specifically for it thanks to **+Walter Hsiao**​.



Look forward to seeing you join the club.


---
**Carter Calhoun** *November 24, 2016 19:23*

Hello **+Luis Pacheco**, pleased to meet you and thanks for saying hello.



  **+Eric Lien** thank you for the swift add, your feedback here, your expertise and all around demeanor - can't tell you enough that this Thanksgiving I am grateful for what has been created here by all of these talented minds!



I have been following your feedback on Ray's boards, and read up on your recent posts about them.  I am going to research more in depth and then opt for a good smoothie board - So, check on that!  I also have been trying to keep track of the ball/lead screw dialogue, so I am grateful for your answer to clear this up.



If this seems right, my plan of action is to: 

1) order from the BOM(minus the lead screws), 

2) study **+Dan Huntington**'s Assembly guide and also the 3D model, 3) read through this Community as much as I can, and then 

4)begin assembly 



Does that plan of attack sound about right? 



Naturally, I am eager to begin, and so I want to order parts for the Spider V2 build this weekend.  It seems like the BOM links for Misumi extrusions already include all of the properly drilled holes, taps, lengths..If so, that's AWESOME?!?  That part of the build seems to be the trickiest based on my skills (especially reviewing the excellent HercuLien Assembly guide and 99 steps of tapping, drilling, measuring).


---
**Ray Kholodovsky (Cohesion3D)** *November 24, 2016 19:29*

Welcome **+Carter Calhoun** 

I will second ball screws wherever possible, I like them in general :)

I'm happy to answer any questions you may have about the Cohesion3D Boards, there's info available at [cohesion3d.com](http://cohesion3d.com) and that's the place when you're ready to purchase. 

Happy holidays![cohesion3d.com - Cohesion3D: Powerful Motion Control](http://cohesion3d.com)


---
**Ted Huntington** *November 24, 2016 19:34*

Welcome aboard Carter! Great to see another SoCal 3D printing fan. The assembly instructions I made could definitely use more details and steps- but hopefully will serve as a good guide in preparing to assemble a Eustathios Spider 2. Having assembled one I think it would be take about 20% of the original time- because once you have done one, you can see how easy it is. I only recently realized that Walter Hsiao put an extra aluminum extrusion on the bottom to connect the power supply and PCB- I totally would have done that had I known about it (I connected mine on the exterior to a vertical extrusion). One key is the aluminum sheet- I just used a file to carve out 4 semicircles - but I think Walter did a workaround- possibly you can just run the springs on the inside of the extrusion- and you would lose a few cms in Y. Enjoy the build and keep up updated with your progress!


---
**Eric Lien** *November 24, 2016 20:32*

**+Carter Calhoun** another note to save money, don't order hardware from McMaster. Order it from [trimcraftaviationrc.com - Trimcraft Aviation RC](https://www.trimcraftaviationrc.com). they have amazing prices, and have been super helpful. They should have almost ever nut/bolt needed for the build. If not and you contact them, they can probably get it for you.



And for t-slot nuts there are great prices on AliExpress. Buy more than you need, and get some drop-in style in case you get into a situation you need to add some but can't install it without having to disassemble the printer to get it in.



([http://s.aliexpress.com/QZFRJRrI](http://s.aliexpress.com/QZFRJRrI))

([http://s.aliexpress.com/IZJnayEn](http://s.aliexpress.com/IZJnayEn))

([http://s.aliexpress.com/IF3M7feM)](http://s.aliexpress.com/IF3M7feM%29)


---
**Ray Kholodovsky (Cohesion3D)** *November 24, 2016 20:48*

Seconding trimcraft. They even beat bolt depot's prices which is slightly better than McMaster. 


---
**Jo Miller** *November 26, 2016 09:00*

funny ,  I am in the end phase of building a  1 meter long D-Bot /herculien  Frankensteiner.     But this based on the joyfull experience of building (and learning) a  Herculien Printer a year ago.      I also had my D-Bot doubts about the idea of the V-slot rail being part of a moving gantry , but its seems to work smoothly. (so far)  



But my Herculien (and its  style of x/y gantry) still is my nr one choice printer  ! 


---
**Eric Lien** *November 26, 2016 13:37*

V-wheels and v-slot are a great motion platform. I think you will be very happy with the D-Bot style system as well.


---
*Imported from [Google+](https://plus.google.com/104205816672263226409/posts/Wb9C9VdjJKb) &mdash; content and formatting may not be reliable*
