---
layout: post
title: "What do you all think about the MK8 extruder?"
date: October 18, 2015 05:23
category: "Discussion"
author: Ted Huntington
---
What do you all think about the MK8 extruder? They are derived from Makerbot and feed filament directly into the hot end (using a convenient spring release lever). So if one requires the extruder to not be located on the hot end then this is obviously of no use. They drive directly from the step motor (without any gear reduction) using a standard hobbed gear that sells on Ebay for <$2. Here are two variations:

[http://www.thingiverse.com/thing:28241](http://www.thingiverse.com/thing:28241)

[http://www.thingiverse.com/thing:147705](http://www.thingiverse.com/thing:147705)



I wonder if pushing filament directly from the gear shaft without reduction provides enough force, but apparently it must because so many people use them. Another plus is that it is an extremely simple design.





**Ted Huntington**

---
---
**Frank “Helmi” Helmschrott** *October 18, 2015 06:03*

Well it is what it is: A direct drive extruders (there are others out there) with a spring load. I don't know if Makerbot was the first to introduce them but they are kind of widely used. They only work with 1,75mm filament as they wouldn't have enough force to push 3mm filament reliably and they have their limitations. One surely is speed and another one is materials that are harder to push.



If you're looking for a cheap solution that is probably a good way to go with. You can also do bowden with a direct drive but the same limitations apply here.I have used this one from Robotdigg ([http://robotdigg.com/product/70/MK7+Molded+Drive+Block](http://robotdigg.com/product/70/MK7+Molded+Drive+Block)) with this drive gear ([http://robotdigg.com/product/73/RobotDigg+Filament+Drive+Gear](http://robotdigg.com/product/73/RobotDigg+Filament+Drive+Gear)) for quite a while and it worked quite ok. It is even simpler and cheaper but has no spring loader. This makes it reliably stable but forces you to retract to unload. Still great.



At the end: If you're looking for more reliability, strength and power i'd vote for other solutions then these.


---
**Eric Lien** *October 18, 2015 13:04*

I have run almost every extruder out there, and I will admit almost any will move filament under good conditions. The problem is of course the outliers. Fast speeds, narrow ideal temps for filament, variation in filament diameter. In my opinion, there is only one extruder to get (so long as money will allow for the purchase). That is the BondTech extruder by **+Martin Bondéus**​​​. 



In the years since I started using it I have never had a filament feed issue. Not a single one. And since consistent, repeatable filament feed is just about the most important component to quality prints... I can't imagine using anything else now.﻿



Also if you want to save money he offers just the hardware, and you can print the housing and buy the stepper yourself.﻿


---
**Ted Huntington** *October 18, 2015 19:52*

Yeah that was another concern- that can a hobbed gear connected directly to the stepper shaft, like the MK8 design, keep up with fast speed (>=60mm/s) printing - I have doubts but have never tried one. It isn't expensive to try out so maybe I will try the mk8. The BondTech extruder seems like a good design and extruder, but I can't justify paying $120 or even $60 for the kit (w/o motor) when there are other good enough options- like the lower-cost Wade's and Hercustruder :) I just found a dual extruder with two included hot ends on robotdigg for $60 ([http://www.robotdigg.com/product/243/MK7+Dual+Stepextruder](http://www.robotdigg.com/product/243/MK7+Dual+Stepextruder)) - the prices for 3D printing components from China are becoming so low now. It's interesting that the BondTech is the only dual bearing/hobb extruder I've seen so far- I am somewhat surprised that others have not picked up on that idea- but I haven't searched extensively for any. It's great that it is open source- the main problem I think is the cost of the dual metal gears. Thanks for the info!


---
**Ian Hoff** *October 18, 2015 20:47*

A directly connected motor like the ones we typically use will get a max speed of 6-10 rotations per second, so if your mk8 has an effective diameter of ~7-10mm, making the circumference ~23-31mm, then 60mms is achievable with some torque remaining.That's ~ 1/2-1/3 the max motor speed.



I use this site for calculating max motor speed

[http://www.daycounter.com/Calculators/Stepper-Motor-Calculator.phtml](http://www.daycounter.com/Calculators/Stepper-Motor-Calculator.phtml)



As well as this chart as a general guideline for the max step rates a given MCU can achieve. (non Cartesian planner solutions like delta and polar bots tend to blow this up)



[http://reprap.org/wiki/Step_rates](http://reprap.org/wiki/Step_rates)


---
**Ted Huntington** *October 18, 2015 23:07*

Nice to see that someone has done the math here ;) You know I don't doubt that the MK8 direct connection to the motor shaft would be fast enough to feed filament at high speeds (>60mm/s)- after all the Wade's extruder is gear reduced- it goes slower. I guess the worry is about torque- without being gear reduced - does the stepper still have enough torque? If yes, it seems like a waste of money and time to use Wade's big and small gear (why did Wade think gear reduction was needed?), or a gear reduced stepper motor (like the Hercustruder and BondTech extruder). I'm definitely going to at least try the MK8 spring release extruder and see if it works for the simple things I print.


---
**Ian Hoff** *October 19, 2015 02:05*

gear reduction is for pretty much 1 reason, more power.



On a 3mm bowden a direct drive motor of the nema 17 variety just struggles unless you spent a lot of money on a triple stack, and reprap grew up out of cheap. this is probably one of the most common answers in reprap. "It was done that way because it was the cheapest way that works."



On a direct drive weight becomes an issue so people will use small motors and without gearing these can struggle or fail.


---
**Ted Huntington** *October 19, 2015 04:09*

That is so funny: "RepRap grew up out of cheap."" ahaha- still it's a good logic. Similarly, If some 3D (reprap/Prusa) printer part is now mass produced in China, then you know it has become a standard. If you can't find it on Ebay or Aliexpress in mass quantity and low cost then it's not standard.



Yeah, thankfully I use only 1.75mm filament. I am going to try the MK8 with a NEMA17- only for 1.75mm. But it's not a big deal just to add 2 gears to add more torque either.


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/7wUyb6ThgqE) &mdash; content and formatting may not be reliable*
