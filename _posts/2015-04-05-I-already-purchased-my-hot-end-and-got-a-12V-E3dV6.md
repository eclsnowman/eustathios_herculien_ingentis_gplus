---
layout: post
title: "I already purchased my hot end and got a 12V E3dV6"
date: April 05, 2015 01:33
category: "Discussion"
author: Rick Sollie
---
I already purchased my hot end and got a 12V E3dV6.  I choose the 12volt as the RAMPS board was 12v.  I checked the build requirements and see 24V.  Can I just go the 12V route?  I only see the fans (from amazon) that would need to be changed.  The SSR looks good.





**Rick Sollie**

---
---
**Joe Spanier** *April 05, 2015 02:19*

the main difference is your motors will run much smoother at 24v but other then that you would be fine. The e3d change over is just as easy, a 24v heater and fan. Personally, 24v is worth the effort.


---
**Bruce Lunde** *April 05, 2015 02:36*

I did 12v I think it will be okay.  Still waiting on smoothieboard (ordered last month) to see how it runs though.


---
**ThantiK** *April 05, 2015 02:38*

I certainly prefer 24v now.


---
**Daniel F** *April 05, 2015 05:53*

You can just get a 40W heater and a 3010 fan for 24V from ali/ebay, doesn't cost a lot.


---
**ThantiK** *April 05, 2015 05:56*

**+Daniel F**, it's not quite as simple as that.  You missed the part where his RAMPS board has 12v rated caps.  I'm betting they're also surface-mount, which means replacing them is practically impossible as well.



If he's got through-hole caps on his RAMPS board, he can replace them with higher rated caps and some small modification though.


---
**Daniel F** *April 05, 2015 06:07*

Ups. You're right **+ThantiK**, didn't think about that. Depends on the board, some have caps with 16v some with 35v. I got one with 35V, but I had to ask for the voltage rating of the caps. You also need to remove a diode. Arduino needs seperste power.I Then replaced the fuses with automotive fuses. The quality of cheap ramps 1.4 ($7.5) is not the best but mine works for 6 months now.

And of coarse the PSU also needs to be 24V.


---
**Jeff DeMaagd** *April 05, 2015 12:54*

Also, check the thermal fuse if you haven't already replaced it with a normal fuse. I understand cheap RAMPS are built with 16V fuses.


---
**Joe Spanier** *April 05, 2015 13:49*

Oh when I read it, it didn't sound like he had a board yet. That changes things I guess


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/jNQm7eFfsxg) &mdash; content and formatting may not be reliable*
