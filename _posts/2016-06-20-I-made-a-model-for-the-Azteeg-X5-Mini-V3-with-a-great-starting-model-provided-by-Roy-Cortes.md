---
layout: post
title: "I made a model for the Azteeg X5 Mini V3 (with a great starting model provided by Roy Cortes )"
date: June 20, 2016 05:07
category: "Show and Tell"
author: Eric Lien
---
I made a model for the Azteeg X5 Mini V3 (with a great starting model provided by **+Roy Cortes**). I did it so I could update the mount for the Eustathios Spider V2. I made a new electronics mount in two configurations (side by side and over under) along with the raspberry Pi for octoprint.



Let me know which one looks best and I will modify the side panel to line up with the new mount and ports.



I added the controller model in several formats, as well as STLs, and Solidworks files for the mounts. It was a part of the latest commit on Github.



[https://github.com/eclsnowman/Eustathios-Spider-V2/commit/d6b591430df8d1bcb712915e22e6668606d15f9a](https://github.com/eclsnowman/Eustathios-Spider-V2/commit/d6b591430df8d1bcb712915e22e6668606d15f9a)







![images/5d303d77df8ffc74b5ddd2a91c99f4ec.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5d303d77df8ffc74b5ddd2a91c99f4ec.png)
![images/6aa62b5aafc1254426d4b1b394e5bbb0.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6aa62b5aafc1254426d4b1b394e5bbb0.png)
![images/0914716afa9545c5ad38e6d55ba06577.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0914716afa9545c5ad38e6d55ba06577.png)
![images/469427d2b8caad35f9df0e30e2d60f28.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/469427d2b8caad35f9df0e30e2d60f28.png)
![images/698c2766980ad87edca6720b9f32c16c.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/698c2766980ad87edca6720b9f32c16c.png)
![images/06d1fcc87c08c7c0412996455bc1ab38.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/06d1fcc87c08c7c0412996455bc1ab38.png)
![images/d60b3ba265050ab2440898c41d786cfd.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d60b3ba265050ab2440898c41d786cfd.png)

**Eric Lien**

---
---
**Eric Lien** *June 20, 2016 06:12*

**+Alex Skoruppa**​ 70.68mm : [https://goo.gl/photos/v9NTsYrbaEFUBnni8](https://goo.gl/photos/v9NTsYrbaEFUBnni8)﻿, the spacer between the boards is just over 20mm.


---
**Matthew Kelch** *June 20, 2016 12:27*

Stupid question -- what's the point of ethernet on the v3?  


---
**Matthew Kelch** *June 20, 2016 12:33*

I guess more specifically I'm asking, what does the web interface look like and what does/doesn't it do that octoprint does?  Is that something built in to smoothieware?


---
**Eric Lien** *June 20, 2016 13:31*

**+Matthew Kelch** Here is the standard Web-UI Info: 



[http://smoothieware.org/install-web-interface](http://smoothieware.org/install-web-interface)


---
**jerryflyguy** *June 20, 2016 14:28*

I'm still liking the side by side variety. Is there a simplification reason why stacking them would be easier/faster/better? There isn't a shield type connection, is there? I just like the wider/thinner layout as i'd have access to everything, it's not like it's tight for space under there! :)


---
**Eric Lien** *June 20, 2016 14:57*

**+jerryflyguy**​ there is no shield, I just figured it might help keep things more compact and allow cabling to be kept neat. Also it might be helpful for people who plan to use this setup in another printer without such excess of space.﻿


---
**jerryflyguy** *June 20, 2016 14:59*

**+Eric Lien** yeah for sure it's more compact.  No doubt it's simply preference on either the HercuLien or Eustathios


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/MGFqpS2XZKU) &mdash; content and formatting may not be reliable*
