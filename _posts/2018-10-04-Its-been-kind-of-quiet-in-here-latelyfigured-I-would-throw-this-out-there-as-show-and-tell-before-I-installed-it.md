---
layout: post
title: "Its been kind of quiet in here lately...figured I would throw this out there as show and tell before I installed it"
date: October 04, 2018 04:45
category: "Show and Tell"
author: Dennis P
---
Its been kind of quiet in here lately...figured I would throw this out there as show and tell before I installed it. Its a lot easier to see uninstalled. 



 I have been working on  incorporating 30x30 fans into **+Walter Hsiao** 's Mini Space Invaders carriage. I have been fighting nozzle and heatbreak issues and its been quite painful to disassemble things to the point I can get the hotend out. (I am pretty sure the issue was cartridge heater power issues: I am running 72mm motors instead of 60's and a 50W heater on a RAMPS/Mega board. The poor RAMPS cant feed everything enough power via the polyfuses. Once I put the catridge heater on its own mosfet, things have been better).



Its a work in progress, one thing I want to tweak is the bore at the bottom for the radiator. The radiator cants towards the 'front' a touch even though its clamped tights at the top. I have backed off the bottom retaining screw, but I think that bottom fin is not bearing well at the bottom of the block. You can see this in the pic of the bottom. I see the solution as either being a couple of setscews to center the radiator, put in a couple of 'pads' to bear the side of the radiator on or shrink the hole entirely.

  

Other mods: slightly protruded (0.25mm) and recessed (0.5mm) the magnets for the part cooling fan duct so the duct registers a little bit better and keys into place a bit. It still breaks away if it hits an obstruction. Counterbored the busing holes so that they are flush to the sides, mounting surface for David Crockers Differential IR sensor (which is growing on me now that I have figured out how to use it). M3 threaded inserts for the **+Daniel F** rear fan bracket/cable support. 



I forced the screw in the top hole instead of grinding it down and the bore was tight so I ended up cracking it a bit. 

I am happy to share the file with anyone in the meantime until I get it finally tweaked and posted.  



Thanks again to **+Walter Hsiao** for providing the original model and designs and **+Eric Lien** for this little corner of the interwebs. 







**Dennis P**

---
---
**Dennis P** *October 04, 2018 04:48*

![images/3c3648dd23076e00aca30adf52a5e2c9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3c3648dd23076e00aca30adf52a5e2c9.jpeg)


---
**Michaël Memeteau** *October 04, 2018 09:48*

Hi **+Dennis P** I like what you're doing with the place (it sure helps to stand on the shoulder of a giant as well). 

I feel that the airflow directed on heat transition zone where it's the most effective and critical is maybe too restricted in this case. There's a gradient and we need to ensure that the transition from hot to cold end is as quick as possible. Regular radial fans aren't great at overcoming obstacles and back-pressure will build up if the way isn't free enough. I can try to help. Would mind sharing the CAD file? 


---
**Dennis P** *October 04, 2018 15:57*

**+Michaël Memeteau** do you mean the fan on the hot end heat sink? That one is unchanged from the original design, its a 40x40.

The airflow is focused through the fins and then exits out the ports on the 2 sides.  

Regardless, I would be grateful for the critique and any input. I put a 3d DXF up and link below. 

Walters original design is here: [https://www.thingiverse.com/thing:1322641](https://www.thingiverse.com/thing:1322641)



[drive.google.com - djp_mini_block_v2.dxf](https://drive.google.com/open?id=1LtJGSXocOIHPsdGl8qi51DLUsrSN1Lmk)


---
**Michaël Memeteau** *October 04, 2018 16:05*

Great... No chance to get a STEP file?


---
**Michaël Memeteau** *October 04, 2018 18:57*

3D DXF isn't supported by Onshape, but IGES will do just fine.


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/PeHyTUNhwBa) &mdash; content and formatting may not be reliable*
