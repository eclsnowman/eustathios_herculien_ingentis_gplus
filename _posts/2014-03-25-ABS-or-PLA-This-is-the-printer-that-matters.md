---
layout: post
title: "ABS or PLA? This is the printer that matters"
date: March 25, 2014 09:50
category: "Discussion"
author: Mike Miller
---
ABS or PLA? 



This is the printer that matters. I'm taking everything learned in the 3DR build and applying it here. Heated build plate, auto leveling, hiding the power supply and guts, potential for multiple extrusion...



So, considering it'll have a heated build plate, and potentially side covers to cut down on drafts would seem to point towards ABS...however, I'm using an e3d, which keeps pretty darned cool, and the build plate will have an airgap and a buncha aluminum between the heat and the plastic parts.



Does it need to be ABS, or would PLA suffice?





**Mike Miller**

---
---
**Carlton Dodd** *March 25, 2014 12:31*

The E3D keeps cool by shedding the heat efficently.  Combine that and a heated bed inside even a semi-enclosed space and it's going to heat up quickly.

Personally, if "This is the printer that matters," I wouldn't be using the word "suffice".  Go with the ABS (heck, even Nylon!).


---
**Mike Miller** *March 25, 2014 12:52*

Yeah, I'll be going ABS. I'll print nylon parts on my own time with it, if it comes to that, which I doubt. 


---
**Jarred Baines** *March 26, 2014 00:04*

Does the flexibility of nylon have a negative effect? I've only printed thin nylon parts so not sure how rigid it is / good for printer components (vs abs which is prone to contraction / warping issues)



Nylon vs abs, which one?


---
**ThantiK** *March 30, 2014 22:41*

Don't print Nylon for structural parts, for the same reason you wouldn't do them in PLA.  Nylon has a <i>very</i> low (lower than PLA) heat deflection temp.


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/aNdNiPLNwUd) &mdash; content and formatting may not be reliable*
