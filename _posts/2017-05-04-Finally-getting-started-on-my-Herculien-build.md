---
layout: post
title: "Finally getting started on my Herculien build"
date: May 04, 2017 13:54
category: "Build Logs"
author: James Ochs
---
Finally getting started on my Herculien build.  I was planning on building one ~2 years ago, then got sidetracked by a large format printer kickstarter that turned out pretty much as you'd expect. After a few iterations of redesign on that printer, I've finally had to admit I should have stuck with the herc ;)  As you can see I'll have plenty of help.



I do have a couple of questions, 



First does anyone have any experience using a RADDS board and due (which I have in hand.)   I haven't used the RADDS board yet so I don't have any experience with it so far.  I'm debating using that instead of the Azteeg X5 GT, since I've already got one.



Second, I'm going to try to print the parts on my existing printer, but the print quality isn't very good, and there's something that I've never been able to quite work out with the dimensions.  The design has a fixed bed, and the cantilever on the carriage supports causes the head to not move in a plane... manual mesh leveling results in some "interesting" measurements... Is there any one that would be willing to print a set of herculien parts for me and ship to the US for $?



I'm sure I will have more questions as I get started building ;)







![images/e4e8e95c2bd272a8d1485c01807cac53.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e4e8e95c2bd272a8d1485c01807cac53.jpeg)



**James Ochs**

---
---
**Eric Lien** *May 06, 2017 02:02*

Sorry your post got caught in the group spam filter for some reason. Glad to hear you are getting to start your build.



I think **+Frank Helmschrott**​ ran a Radds board, but I could be remembering wrong.



I hope you can find someone to print you the parts. I used to do that for free to aspiring builders when things first started. But after 20+ kits printed for people and only a handful of people ever finishing their build... I guess I just got disheartened. So I decided not to print "print it forward" kits anymore, since the "forward" part never seemed to come about.



But I hope someone can come forward for you if they have some open print time available. Just note it is no small feat to print all the parts needed for this build. So if someone can, make sure you take care of them in return.



Keep us updated on your progress, and always feel free to ask questions. We have a great community, with so much knowledge packed into the brain trust of our little corner of the interwebs :)


---
**James Ochs** *May 06, 2017 03:36*

Thanks for springing me from the spam jail ;) 



No worries on the printed parts.  I'm going to try to print them on my existing printer first and see how that goes with the first few parts.  I'm mostly concerned with the corner pieces and the carriage, so those may be all I need to find a printer for.  I'm just trying to figure out my backup options, if I can't find someone here, I'll get them done commercially.  



I think I've got the entire BOM at this point with the exception of new hotends (i'm leaning toward the chimera) and printed parts.    I'm going to give the RADDS a try before dropping the cash on the X5 GT, I was just hoping to get some feedback if there are any issues with the RADDS.


---
**Frank “Helmi” Helmschrott** *May 06, 2017 07:38*

Hey James, I indeed do use the Eusthatios with a RADDS 1.5 and i'm quite happy. I'm using the Dev-Version of the Repetier Firmware which has board config files/settings for RADDS. A great combo.


---
**James Ochs** *May 06, 2017 12:12*

Thanks for the information Frank!  That helps a lot.  What drivers are you using with it?


---
**Frank “Helmi” Helmschrott** *May 06, 2017 13:03*

The RAPS128 that fit them well. But I guess any other drivers will work too. Those are definitely great, very quiet and smooth.


---
**Daniel F** *May 06, 2017 20:28*

I also run the RADDS/ArduinoDue with RAPS128 on the Eustathios and can confirm it runs very smooth and quiet.


---
**James Ochs** *May 06, 2017 22:38*

Ahhh ok, sounds like I should get the raps  drivers, I've got 8825s now.  What microstepping are you guys running them at?


---
**Stefano Pagani (Stef_FPV)** *May 15, 2017 15:00*

**+James Ochs** What printer name? I backed the Rhino. Piece of crap....


---
**James Ochs** *May 15, 2017 15:23*

it was the cobblebot, but their original design was pretty bad (and a lot of the parts were garbage), so one of the other backers came up with a new design that was a little better.  I think the issues really revolved around having a fixed bed and no good way to stabilize the carraige in all three dimensions at that size using just wheels on the frame extrusions.  It winds up making the hotend move in Z depending where its mass is located.


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/CHFiQX42nCJ) &mdash; content and formatting may not be reliable*
