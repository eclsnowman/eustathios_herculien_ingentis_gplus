---
layout: post
title: "Edit: Taken! I have a whole set of Herculien parts I don't think I'll be using"
date: May 29, 2015 02:03
category: "Discussion"
author: Chris Okuda
---
Edit: Taken!



I have a whole set of Herculien parts I don't think I'll be using. Who  is next in line? You pay shipping and have them for free. They are white ABS, 35% infill, 6 perimeters, and very strong!





**Chris Okuda**

---
---
**Eric Lien** *May 29, 2015 02:50*

Very cool of you to offer **+Chris M.**​​



I say first come first served... Unfortunately print it forward kind of died on the vine (at least in the formal list way). I just haven't had time to keep up on it with my new job.



I might suggest the first person to show they have purchased some parts to build one gets it.﻿


---
**Vic Catalasan** *May 29, 2015 04:28*

I would like to use it and when my printer is done will make a set for our group. I have all my parts and electronics ready to go.


---
**Chris Okuda** *May 29, 2015 09:22*

**+Vic Catalasan** Sounds good, I responded to your message on gchat just now!


---
**Chris Okuda** *May 29, 2015 09:25*

**+Eric Lien** No worries, it seems that the print it forward has kinda taken on a life of its own now, I see people printing stuff all the time here. That's one reason why I even thought to give the parts away now that I don't need them!


---
**Luis Pacheco** *May 29, 2015 14:29*

Guess I'm too late for this one. **+Vic Catalasan** Let me know once you get yours going I'll pay you for some parts! All I'm waiting is for printed parts to finish my printer.


---
**Vic Catalasan** *May 29, 2015 15:53*

**+Luis Pacheco** You got it!  I will forward the parts to you. 


---
**Vic Catalasan** *July 31, 2015 18:18*

Hi Luis, I am close to being able to print, are you still waiting for the Herculien printed parts? 



Once again, I want to thank Chris M. for the printed parts.


---
**Luis Pacheco** *July 31, 2015 18:58*

**+Vic Catalasan** I would love some printed parts! I somewhat built my printer but the parts are not only in PLA, they are also not the best and have slight cracking to some. Would love to swap them out for some decent parts. I could buy the plastic and send it your way or we could work something else out.


---
**Vic Catalasan** *August 01, 2015 01:04*

When I finish,  you can send me a mail label. I will have it in Yellow or White ABS?



Are you able to print now with what you have? 


---
**Luis Pacheco** *August 03, 2015 20:57*

**+Vic Catalasan**​ I will be trying to tonight to see if I can print. The xy don't move very smoothly and I'm assuming it's due to poor prints and cracking. 


---
*Imported from [Google+](https://plus.google.com/106419408152490638700/posts/1GFGRieS1Mf) &mdash; content and formatting may not be reliable*
