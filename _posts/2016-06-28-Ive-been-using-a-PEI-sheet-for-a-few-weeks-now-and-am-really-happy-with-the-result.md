---
layout: post
title: "I've been using a PEI sheet for a few weeks now and am really happy with the result"
date: June 28, 2016 20:50
category: "Discussion"
author: Oliver Seiler
---
I've been using a PEI sheet for a few weeks now and am really happy with the result. Prints seem to stick better the longer I use it, to the point that I'm struggling sometimes to remove objects. 

What are you using to remove items without damaging the PEI sheet?





**Oliver Seiler**

---
---
**Martin Bogomolni (MB)** *June 28, 2016 21:16*

A can of "cold air."   Sometimes I remove my build platform and put it in the fridge for a couple minutes to get the part to "pop" off ( I have a removable build plate of PEI on borosilicate glass )


---
**Eric Lien** *June 28, 2016 21:51*

With PEI and certain materials I set my nozzle higher. Essentially don't need any of the standard squish you need on glass and bed coating. Also this tool is great: 



3D Print Removal Tool [https://www.amazon.com/dp/B00VB1U886/ref=cm_sw_r_cp_apa_gfVCxb3DGRQ1G](https://www.amazon.com/dp/B00VB1U886/ref=cm_sw_r_cp_apa_gfVCxb3DGRQ1G)


---
**Tony White** *June 29, 2016 05:08*

What certain materials **+Eric Lien**​?


---
**Oliver Seiler** *June 29, 2016 06:48*

Thanks everyone. I'm trying to avoid having to carry the glass with PEI around the house to put it in the fridge (which probably isn't large enough anyway). Have you tried cold spray **+Martin Bogomolni** ? I'd be concerned that it might crack the glass :(

**+Eric Lien** how does the PEI stand up against this tool in terms of scratches? Otherwise that looks good and I'll have to find a way to get one as Amazon unfortunately doesn't ship to NZ :(

Generally PETG seems to stick much more than PLA and as Weic said, some extra distance with the nozzle helps most of the time.


---
**Eric Lien** *June 29, 2016 16:06*

**+Anthony White** some PLA, Some PETG, and if you ever use TPU or Ninjaflex apply some glue stick or the darn stuff will fuse to the bed. In fact I almost always have the nozzle higher now. Except ABS I still squish a little so I put an offset in my S3D profile to compensate so I never have to touch the bed.


---
**Angel Espiritu** *June 29, 2016 17:31*

You might be printing your first layer too slow. When you print it too slow (<40-50%), prints get stuck. Increase the speed.


---
**Chris Purola (Chorca)** *June 30, 2016 17:24*

I have issues getting stuff to stick to PEI :(


---
**Angel Espiritu** *June 30, 2016 18:15*

**+Chris Purola** Clean the surface with isopropyl and make sure the bed is trammed. I think it's better to be too far than too close to the bed. lol. I have since switched back to regular boro glass w/ wolfbite or PVAc.


---
**Chris Purola (Chorca)** *June 30, 2016 18:47*

**+Angel Espiritu** I clean with 99% isopropyl and have it pretty close, heated too, PLA just peels right up. Even further away it peels off. Using the rough side of the PEI


---
**Oliver Seiler** *June 30, 2016 22:16*

**+Chris Purola** I had the same issue first. For me PLA sticks less than e.g. PET. I've cleaned my PEI with isopropyl and sanded it very carefully a tiny bit with 1500 sand paper. That made it stick very well.


---
**Vic Catalasan** *June 30, 2016 23:54*

Like Eric, raise tiny bit. I also lowered my bed temp by 10 degree after the PEI has been used. Also wait till about 50-60 degree before removing. Sometimes I turn my bed fan on center during cooling stage. I also use the same removal tool its simply awesome. 



PEI = Best thing that ever happened to my 3D printer


---
**Christian Gooch** *July 02, 2016 20:58*

Another option for the print removal tool is an oil paint spatula/ knife... Some look very similar to the one linked above 


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/M4LFwsrHe1n) &mdash; content and formatting may not be reliable*
