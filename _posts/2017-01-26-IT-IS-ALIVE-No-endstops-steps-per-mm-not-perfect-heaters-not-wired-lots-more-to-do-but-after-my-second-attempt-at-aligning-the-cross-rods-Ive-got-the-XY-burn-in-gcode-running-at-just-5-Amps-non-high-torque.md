---
layout: post
title: "IT. IS. ALIVE..... No endstops, steps per mm not perfect, heaters not wired, lots more to do - but after my second attempt at aligning the cross rods, I've got the XY burn-in gcode running at just .5 Amps (non-high torque"
date: January 26, 2017 08:54
category: "Build Logs"
author: Benjamin Liedblad
---
IT. IS.  ALIVE.....



No endstops, steps per mm not perfect, heaters not wired, lots more to do - but after my second attempt at aligning the cross rods, I've got the XY burn-in gcode running at just .5 Amps (non-high torque steppers)!



Loudest part is the 24V power supply fan. 



Hearing some slight vibration on the longer moves... probably normal?



Anyone have input on what to use for the bottom plate? I could use plywood and cut it myself, but was hoping to have the machine look a little more classy. Would prefer if I could craft at home rather than order custom. 



I have also seen several pictures of builds here which have multi-pin connectors to detach the hot-end/fans from the cables.



What are y'all using there, and do you double up on pins for heater element?

![images/fdb45a141501adcf2e5a36e1fe921ee1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fdb45a141501adcf2e5a36e1fe921ee1.jpeg)



**Benjamin Liedblad**

---
---
**Zane Baird** *January 26, 2017 13:57*

The molex microfit 3.0 connectors are rated at high enough amperage to use on the heaters. I use the 2 row, 8 position connectors (2 pins each for heater, thermistor, hotend fan, and part cooling fan) and buy the cut-strip contacts. The Molex part numbers are below



Female pins (for receptacle): 46235-5001 (Cut Strip)

Male pins (for plug): 43031-0002 (Cut Strip)

Receptacle: 43025-0800

Plug: 43020-0801



You can use different plugs depending on your need, but the part number I listed is probably the simplest.



These pins can be a real pain to crimp without a proper crimping tool so buy extra. I would also highly recommend the linked crimper from amazon. Works like a charm on all the molex pins I tried it on.





[amazon.com - HT-225D Full Cycle Ratchet Crimping Tool with interchangeable die set HT-225D - Crimpers - Amazon.com](https://www.amazon.com/gp/product/B007JLN93S/ref=oh_aui_detailpage_o02_s01?ie=UTF8&psc=1)


---
**jerryflyguy** *January 26, 2017 15:20*

Looking good! I used Molex connectors from Digikey. Don't have the numbers in front of me but can get the part numbers if you like. Looks like **+Zane Baird** has some details too. Lots of options in this area, just pick what works for you! 😉


---
**Neil Merchant** *January 26, 2017 21:40*

Awesome! I had some vibrations on some of my movements as well - but with a few tries aligning the cross rods and a few hours of printing most of my vibrations have gone away. I'm also looking for a solution for my bottom plate - I'd like to hand-cut some acrylic or something, but I'll likely end up using a thin sheet of MDF and painting it myself. Definitely post here if you think of something - I'm not totally in love with either solution.


---
*Imported from [Google+](https://plus.google.com/111192213763748051453/posts/i7L3o9hK9pT) &mdash; content and formatting may not be reliable*
