---
layout: post
title: "I don't understand why my pieces are not becoming flush"
date: June 12, 2015 01:07
category: "Discussion"
author: Gus Montoya
---
I don't understand why my pieces are not becoming flush. Should I be using washers? Alignment is horrible all around.



![images/f7391b58eb0a20e3b667dbb0b9f26a52.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f7391b58eb0a20e3b667dbb0b9f26a52.jpeg)
![images/3cbcc7e89517ced11a76c5abb521d7a6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3cbcc7e89517ced11a76c5abb521d7a6.jpeg)

**Gus Montoya**

---
---
**Erik Scott** *June 12, 2015 01:10*

Screws are too long. You can bias them with washers or use the correct 12mm M5 screw. 


---
**Erik Scott** *June 12, 2015 01:12*

For clarification, if you do use washers, they should go between the screw-head and the plastic part, not between the plastic part and the aluminum extrusion. 


---
**Gus Montoya** *June 12, 2015 01:21*

I think something else is wrong. I took a picture of the wrong screw. I am using M5 x 12mm screw.


---
**Derek Schuetz** *June 12, 2015 01:22*

That seems like too long of a screw mine barely had enough thread to grab the tnut 


---
**Erik Scott** *June 12, 2015 01:37*

Are you sure? You could try 10mm screws, but I would first make sure you're using the screw you think you are, and that your part is dimensionally accurate. 


---
**Richard Earl** *June 12, 2015 02:27*

**+Gus Montoya**​ if you have tightened the screws then they are to long, or from your picture the top one is longer than the bottom, if all the printed parts are of the same dimension then I would think you should be generally fine for alignment once you have them mounted correctly. Having said that I am no expert and I will defer to others with greater experience. 


---
**Gus Montoya** *June 12, 2015 04:43*

I'll remeasure them later. 


---
**Gus Montoya** *June 12, 2015 04:47*

I found the problem, it was the screw (deformed)  and when cleaning up the part I carved into it too much. Duh....


---
**Gus Montoya** *June 12, 2015 04:49*

That's what happens when trying to do things in a rush right before work.


---
**Richard Earl** *June 12, 2015 14:27*

Glad to hear that you got it sorted out. 


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/TKpLyDF4UAX) &mdash; content and formatting may not be reliable*
