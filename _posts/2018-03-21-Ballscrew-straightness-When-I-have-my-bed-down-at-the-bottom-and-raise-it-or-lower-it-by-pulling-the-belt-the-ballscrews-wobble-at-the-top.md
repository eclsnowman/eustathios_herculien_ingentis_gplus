---
layout: post
title: "Ballscrew straightness. When I have my bed down at the bottom and raise it or lower it by pulling the belt the ballscrews wobble at the top"
date: March 21, 2018 19:58
category: "Discussion"
author: Julian Dirks
---
Ballscrew straightness.  When I have my bed down at the bottom and raise it or lower it by pulling the belt the ballscrews wobble at the top.  On one side 0.2mm and on the other 1.5mm. I don't have huge expectations from a $20 chinese ballscrew (Golmart) and freight is more than the cost of the item so hardly going to send them back, but I presume this will cause artifacts in my prints?



Have been reading straightening methods here and in other forums so think I will try that but I am wondering how straight do they need to be?



Do yours wobble at all?  How much wobble is acceptable?





**Julian Dirks**

---
---
**Oliver Seiler** *March 21, 2018 20:21*

I'd think that 1.5mm is likely going to cause a few issues, though the z rods will constrain it when the bed is up. Have you tried reseting the ballscrew in the bearing at the bottom to ensure it's stitting straight?


---
**Julian Dirks** *March 21, 2018 21:22*

Hi Oliver. I dropped the bottom mount off completely and wound a heavy rubber band round the top of the leadscrew so it wouldn't drop out then let the leadcrew fall by it's own weight constrained only by the ball nut.  At the start of the drop (ball nut at the bottom, ball screw all above the nut) the screw wobbles a lot then as it falls it gets to a point where the top stops wobbling then the section that is out the bottom of the nut starts wobbling.  Presumably that is the bend point.  So it's hopefully a pretty simple bend to get out.



Do yours wobble at all?


---
**Oliver Seiler** *March 21, 2018 21:32*

Ok if that's the case I'd ask the vendor to provide a replacement. I had to do that for the rods a couple of times, before I received straight ones.

My ballscrews do wobble a tiny little bit on the top, but you have to look closely to notice it. I've never measured the deviation as prints turn out ok for what I need.


---
**Eric Lien** *March 21, 2018 22:31*

**+Julian Dirks** i would certainly hit them up for replacements. Many from the group have bought from them. So I think they have a vested interest in keeping the group happy.


---
**j.r. Ewing** *March 22, 2018 07:46*

Straightening a ball screw is fun. Does it have center drills in each end? Support on centers find high spot push rinse realest  down to zero plus springback how big of screw diameter  I’m seeing these 12mm linear bearings cheeap and  wheels are turning.  How much backlash is in it? Just curious


---
**Julian Dirks** *March 22, 2018 08:49*

**+Eric Lien** I took your advice and contacted Golmart.  i got a very quick and pleasant response from them asking for some more info and they said if it really is bent they will send me a new one.  

**+j.r. Ewing** with the bed low the top moves by 0.2mm/180deg turn on one ball screw and over 1.5 - 2mm on the other.  They said that 0.2mm is normal.



I'll let you know what happens but really impressed with their customer service so far.


---
**Eric Lien** *March 22, 2018 11:46*

Quick question, are you sure all your printed parts (rod holders, bed holders, Lower bearing blocks, etc) are dimensionally accurate? If not the bed system can be over constrained due to tolerance stack up. I had to shave a few parts, and shim others for perfect alignment. In fact eventually i plan to redesign the bed assembly to allow a degree of freedom in the assembly width to allow the stackup to be aligned. As things are now things have to be perfect to get perfect travel on the bed (which is poor design I will admit).


---
**Julian Dirks** *March 22, 2018 22:17*

Hi.  Haven't measured anything specifically but my trusty Wanhao i3 is good with calibration cubes and prints nicely - even better since I put the BMG and E3Dv6 that are desitined for this printer on it   :  )



I printed all the parts in quality PETG (Atomic) and haven't seen any warping.  



Everything seems to have gone together really well.  I've assembled the gantry now including belts & pulleys and your bondtech direct mount. After watching your video and a bit of alignment tweaking/belt tensioning etc I have it moving smoothly by pinkie power before powering up any motors.



None of that means I don't have any alignment issues in Z I guess but the screw is definitely bent:  I put a heavy rubber band round the top of the ball screw (to stop it falling out) and took the bottom mount off completely so the ball screw was only constrained by the nut and it could fall/rotate through the nut under it's own weight.  With the ball screw wound all the way up it wobbles as it drops but the portion sticking out the bottom of the nut doesn't wobble, then at a point half way or so down the top starts running perfectly true and the portion below the nut starts wobbling.  Presumably the is the point where the bend passes through the nut?



I want to make this printer as good as I can though so if there are any specific dimension checks or movement tests etc to get it as true as I can I would love to hear them. 





![images/dd6a5b5bba9ba2a7c4526f26f2b7f4fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dd6a5b5bba9ba2a7c4526f26f2b7f4fa.jpeg)


---
**Julian Dirks** *March 25, 2018 20:34*

Update.  Golmart agreed to send me a new ballscrew.  Cathy Li at Golmart was very pleasant to deal with and it was great to see them stand behind their product, which I guess is why so many people here use them.  I'll surely buy from them again in the future.


---
*Imported from [Google+](https://plus.google.com/113795478307151372873/posts/GYj184NxTAd) &mdash; content and formatting may not be reliable*
