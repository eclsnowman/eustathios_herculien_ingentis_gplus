---
layout: post
title: "On the Eustathios what is the vertical distance between the y and x axis?"
date: January 23, 2015 13:09
category: "Discussion"
author: Keith Dibling
---
On the Eustathios what is the vertical distance between the y and x axis?





**Keith Dibling**

---
---
**Eric Lien** *January 24, 2015 00:27*

**+Keith Dibling**  14mm vertical distance between the rods on both Eustathios and HercuLien. So carriages are interchangeable. They would just need the proper misalignment bushing installed for the shafts used on that machine. Eustathios uses 8mm carriage rods, and HercuLien uses 10mm rods.


---
**Keith Dibling** *January 24, 2015 09:00*

Thanks Eric



I'm just trying to set up x & y whilst awaiting bearings 


---
*Imported from [Google+](https://plus.google.com/103631495948845103844/posts/5kfcYJWvS41) &mdash; content and formatting may not be reliable*
