---
layout: post
title: "What inductive sensors is everyone using? comment below with links please!"
date: November 27, 2016 05:03
category: "Discussion"
author: Jim Stone
---
What inductive sensors is everyone using? comment below with links please! :D



i am most likely switching from the bltouch as its repeatability is not as advertised after 3 different units. the best repeatability with a bltouch has been 0.04 which ...is unacceptable.





**Jim Stone**

---
---
**Jim Stone** *November 27, 2016 05:40*

it is when the machine its going on is a herculien hehehehehe.


---
**Eric Lien** *November 27, 2016 05:47*

**+Jim Stone** we can get something modeled up. To be honest HercuLien carriage is due for a refresh anyway.


---
**Jim Stone** *November 27, 2016 05:54*

well as long as whatever probe its remodeled to take is easily accessable. ie amazon :P



also **+Eric Lien**  i noticed you use an inductive as well. a tube screw type.


---
**Eirikur Sigbjörnsson** *November 27, 2016 09:14*

Is there any difference between the blue and the green version of the SN-04?


---
**Jim Stone** *November 27, 2016 16:19*

**+Eirikur Sigbjörnsson** Im not sure if there is. however reviews from each steer me off about their actual trigger distance that people are getting.


---
**Benjamin Liedblad** *November 28, 2016 17:28*

Not inductive, but I'm going to test this guy's idea out once my printer is operational...

[hackaday.io - Super Simple Bed Auto Leveling](https://hackaday.io/project/8179-super-simple-bed-auto-leveling)



I'm thinking I can use conductive paint if I'm printing on glass, or just touch the aluminum build plate.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/6TcfwwRqB4t) &mdash; content and formatting may not be reliable*
