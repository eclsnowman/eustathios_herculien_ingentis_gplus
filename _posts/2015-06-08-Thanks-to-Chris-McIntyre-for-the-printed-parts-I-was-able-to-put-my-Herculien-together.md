---
layout: post
title: "Thanks to Chris McIntyre for the printed parts I was able to put my Herculien together"
date: June 08, 2015 23:13
category: "Show and Tell"
author: Vic Catalasan
---
Thanks to Chris McIntyre for the printed parts I was able to put my Herculien together. This thing barely fits through my bedroom door, hopefully when finished I can manage to take it out. 



I am not sure it is needed but I am using shield cables for the steppers as I did on my DIY CNC machine. 



Anyone interested in the printed templates please let me know and I can send them to you, it will get you started? I used my CNC machine to make the holes instead so they are not used. Next is to work on the Z gantry.



![images/dac3c8119e72cd931c6e7f50a7c86d98.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dac3c8119e72cd931c6e7f50a7c86d98.jpeg)
![images/aeec6861e90cfdaad0e5afaf7f10de36.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/aeec6861e90cfdaad0e5afaf7f10de36.jpeg)
![images/3b0facf4101843dde83b90df604440bc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3b0facf4101843dde83b90df604440bc.jpeg)
![images/0a838bcaf0871447c5ef06c281d16ebe.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0a838bcaf0871447c5ef06c281d16ebe.jpeg)

**Vic Catalasan**

---
---
**Dat Chu** *June 08, 2015 23:16*

Very nice. How did you use cnc machines for making holes? 


---
**Vic Catalasan** *June 08, 2015 23:25*

I am using Mach3 software to control my CNC machine and in the program there is a drilling pattern wizard, I set Mach3 to mm and set 4 holes at 20mm apart. The side bottom extrusions I used 2 x 4 holes. On the rod holes I used measurements from the documents using a 1/2 mill as suggested. I am building two machines so it would have been a big chore doing all by hand. 


---
**Vic Catalasan** *June 08, 2015 23:28*

I have the CNC process posted with pictures


---
**Dat Chu** *June 08, 2015 23:34*

I see. Very nice. Thank you for the explanation.


---
**Vic Catalasan** *June 08, 2015 23:44*

Thanks! 



I think when I get it running good, will make some mods to keep everything 'inside the box'. I am pretty sure I will be breaking something off when it gets to my shop


---
**Eric Lien** *June 09, 2015 01:02*

**+Vic Catalasan** looking great, keep us posted.



The only reason I tried to keep things out of the enclosure is for heat. It gets 55C+ in the summer time in the enclosure passively from the heated bed.


---
**Bruce Lunde** *June 09, 2015 01:04*

Really nice work. I used mach3 myself to do the holes; two at once?  You are a gutsy man!


---
**Vic Catalasan** *June 09, 2015 05:53*

**+Eric Lien** Thanks for that info, 55C is toasty!


---
**Vic Catalasan** *June 09, 2015 05:59*

**+Bruce Lunde** Well I always try my first attempt on wood first and check measurement. The 2 x 4 holes was fun to watch and yes I still get nervous if the system goes hay wire and ruins my part.


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/Fm4FYKAcBHy) &mdash; content and formatting may not be reliable*
