---
layout: post
title: "Anyone ever had a file which wouldn't 'play'?"
date: October 15, 2016 01:59
category: "Discussion"
author: jerryflyguy
---
Anyone ever had a file which wouldn't 'play'?  I've had it happen a couple times where it just doesn't do anything. Doesn't start the heat up or timer etc?



Azteeg x5 v3





**jerryflyguy**

---
---
**Zane Baird** *October 15, 2016 02:21*

Does your controller freeze and require a reset? If so, then yes. But only when my bed was preheated above the set temp and not with the new firmware version. 


---
**jerryflyguy** *October 15, 2016 02:24*

**+Zane Baird** nope, I can go back in and re select the file but it doesn't give you the 'abort' function. Just reverts back to the 'watch' screen and is just static, no action. No heat up, no time count. I'm wondering if it's taking time to spool the data or? The last file was ~33mb


---
**jerryflyguy** *October 15, 2016 02:50*

**+Ashley Webster** what do-ya-know.. that worked. Seems weird that a 10-15 character name is an issue?


---
**Jeff DeMaagd** *October 15, 2016 04:13*

I just make it short so it's easier to type. It might be worth doing an "ls /sd" to see if it's a naming issue.


---
**Arthur Wolf** *October 15, 2016 08:10*

What is the exact filename ? Also, yes, **+Jeff DeMaagd** is right, the result of "ls /sd/" would be helpful.

If all else fails, it's possible the SD card got corrupted ( do you always "safely eject" it in the menus after changing something on it ? ) and formatting it then pasting the files back in would solve it.


---
**jerryflyguy** *October 15, 2016 18:36*

**+Arthur Wolf** the actual file name was

Fan_No_Supports.gcode



I'm not sure what "is/sd/" means?



I've always used the eject on SD card. It was formatted from day one.



I simply changed the file name to "F.gcode" and it ran. 


---
**Arthur Wolf** *October 15, 2016 18:39*

Well maybe a special character crept in the filename or something ...

ls /sd/ is a command you can send to your somothieboard via your host software, a serial terminal or the web interface


---
**jerryflyguy** *October 15, 2016 18:42*

**+Arthur Wolf** just tried is/sd/ and get unknown syntax? I've tried every permutation I can think of (including the "") but same result, what am I missing?


---
**Arthur Wolf** *October 15, 2016 18:44*

**+jerryflyguy** using Pronterface ? do "@ls/sd/"


---
**Jeff DeMaagd** *October 15, 2016 19:46*

L not I, space between the s and /. Computer commands are extremely specific.


---
**Jeff DeMaagd** *October 15, 2016 19:47*

ls is a command to find the contents of a folder, /sd/ is the name of the folder we want it to check.


---
**jerryflyguy** *October 16, 2016 03:02*

Thanks for the clarity, I'll try again tomorrow. Will post the results


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/7omZSZiiAYT) &mdash; content and formatting may not be reliable*
