---
layout: post
title: "Look what was in my mail box"
date: April 17, 2014 01:13
category: "Show and Tell"
author: Jim Squirrel
---
Look what was in my mail box. Now I need to order more and belts. Mass drop motors haven't shipped yet so I have plenty of wait time. 

![images/86d36e4ce748976d872eed09fba38e9b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/86d36e4ce748976d872eed09fba38e9b.jpeg)



**Jim Squirrel**

---
---
**Eric Lien** *April 17, 2014 01:17*

Got mine today. Really great quality.


---
**ThantiK** *April 17, 2014 01:27*

Still haven't got mine yet, was too lazy to type out the tracking number on the picture that was sent to me.  hah!



What belt lengths are needed for A) the Ingentis style rod corners and B) the Tantillus corners? -- I guess it doesn't matter if the belt attachments allow for adjustment with an open-loop belt, 'eh?


---
**D Rob** *April 17, 2014 01:35*

the corners I used with 1010 and the motors mounted on top can use 280mm I have 320mm because I wanted a little extra and plan on using a spring loaded tensioner. kinda like the belt tensioners on a riding lawn mower but the corners I have and therefore the plexi panels I have and the carriage will now support these on the inside to replace the spectra. I will use these (just 2 ) for the drive instead of herringbone gears. Build 2 and 3 will have modified corner brackets and carriage to compensate for the pulley diameters.


---
**Dale Dunn** *April 17, 2014 02:28*

I was as lazy as **+Anthony Morris** , but then I got curious. So I typed it in. When Google took me to the USPS I thought, great, their tracking is useless until... hey it's delivered. Honey, where's today's mail?



They look great. Thanks for organizing this, Dustin.


---
**D Rob** *April 17, 2014 02:52*

**+Dale Dunn** no problem. i was glad to do it. they are pretty nice huh. probably the most accurate i've seen in chinese pulleys.and way better than sdp-si prices 


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/YEUqdaD88LA) &mdash; content and formatting may not be reliable*
