---
layout: post
title: "I am new here and got my first 3D printer a few months ago (Robo3D) I decided I am going to build a HercuLein because of the dual extruders"
date: July 26, 2015 20:30
category: "Discussion"
author: Bud Hammerton
---
I am new here and got my first 3D printer a few months ago (Robo3D) I decided I am going to build a HercuLein because of the dual extruders. It will be a very slow build, so everyone that says their build is slow will be surprised at just how fast they put their together compared to mine. I am in the process of printing out the printable parts and see that there are a few that I can't find in the pictures. I am wondering if every STL needs to be printed. I am actually printing two sets while I am perfecting my Slic3r profiles for the Robo. The first set is about 1/5 of the way through in ABS, the second set will be in PETG. PETG is my preferred filament, but I need the practice in ABS.



Also, I would like to know if there are some files that aren't manifold, as error messages pop up on occasion when loading into Slic3r. I have also figured out that not all parts are saved in an orientation conducive to printing. I am using the parts from a downloaded .ZIP file direct from the GITHUB source as of the beginning of this week.



Thanks for this wonderful design, and I hope to learn a lot from this community.

Bud





**Bud Hammerton**

---
---
**Eric Lien** *July 26, 2015 21:24*

I think all models are manifold, solidworks normally does a very good job. 



Yes several parts are modeled in assembly mode in solidworks. I never bothered to rotate them since it is so easy in the slicer.



For the qtys I recommend you look at the BOM, that will tell you what is needed. Other parts are just accessories or mods.


---
**Eric Lien** *July 27, 2015 01:01*

**+Ashley Webster** I found this out recently. Funny that I print it at 255C, but it deflects much lower than that temp. I guess ABS is still king for high temp applications.


---
**Bud Hammerton** *July 27, 2015 04:27*

I was actually planning on doing all the high temp stuff (adjacent to the hotend) in Alloy 910. PETG for all the non critical pieces. Not sure how hot it will get if enclosed on all sides, but don't think it would be any hotter than the bed temp. Of course the bed would take forever to heat the entire chamber. At the moment I am still printing out parts in ABS. 


---
**Bud Hammerton** *July 27, 2015 12:59*

**+Ashley Webster** I will of course.



I do have a question for everyone. So far all the structural parts I have printed are 100% infill. When it comes to the Extruder head, has anyone printed anything other than 100% infill?


---
**Eric Lien** *July 27, 2015 13:44*

I am usually at 30% maybe 40% max on most of my parts. More perimeters create more strength-to-weight than high infill.


---
**Bud Hammerton** *July 27, 2015 15:17*

Thanks **+Eric Lien** I do understand that particular concept. I was just asking if anyone had done it with your design. Weight is really not my biggest concern. It's not like the old days of LAN parties playing Doom or Quake where you had to carry your PC from place to place. I have never heard of a LAN 3D Print party! I am going to print the bigger parts with 40% infill and see how that works out. I am about a third the way down the list of printed parts. Will likely print a few things that were speced as purchasable also. The Hoffman Q-Line enclosure has a STEP file available as well as some of the handles and other miscellaneous parts from McMaster-Carr.


---
**Eric Lien** *July 27, 2015 15:32*

**+Bud Hammerton** Sounds good. Yes I have models for the printable handles on the Github. Also the enclosure is just what I used, but a local surplus house had the for $5 each... so considerably cheaper than others could get it for. Really any enclosure which can fit the components will do,



As to weight one advantage of this x/y cross gantry is the low moving mass which allows for the fast printing people like without vibrations due to the inertia of the part during hard direction changes. So on all parts that are not in motion you are correct weight is no issue. But for all moving components the weight increases though small will have an effect as you push your machine harder.



Best of luck.


---
*Imported from [Google+](https://plus.google.com/+BudHammerton/posts/4CjEDthgryM) &mdash; content and formatting may not be reliable*
