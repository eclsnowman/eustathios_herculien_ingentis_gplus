---
layout: post
title: "So not exactly 3d printing but I thought I would share with you guys"
date: June 22, 2014 20:58
category: "Discussion"
author: Riley Porter (ril3y)
---
So not exactly 3d printing but I thought I would share with you guys.  This is an example of the UI (IN BROWSER) called chillipeppr using  #TinyG  (full disclosure I am one of the  #tinyg  creators) and how easy it is to extend it!  TinyG is coming to a 3d printer near you very soon.  However this is going to be a really good talk  and I think everyone will pick up something from it.  So its short notice (2 mins!) but if you have some time on a lazy Sunday give it a go!



<b>Originally shared by Riley Porter (ril3y)</b>



This hangout is geared towards people wanting to extend the Chillipeppr UI for the TinyG CNC Motion controller.  This hangout will show you how to create a new widget, subscribe to events and to publish events.  This is the defacto hello world example for extending Chillipeppr to work on your TinyG use case.





**Riley Porter (ril3y)**

---


---
*Imported from [Google+](https://plus.google.com/+RileyPorter/posts/BCnnL2pRcfj) &mdash; content and formatting may not be reliable*
