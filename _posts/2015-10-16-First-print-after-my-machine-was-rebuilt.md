---
layout: post
title: "First print after my machine was rebuilt"
date: October 16, 2015 18:14
category: "Show and Tell"
author: Igor Kolesnik
---
First print after my machine was rebuilt. Some kinks are still there but now I know that if I slow down it will be perfect. 0.192 mm height, 40 inner, 35 outer, 50 infill, 3 mm retraction on 50mm/s. Bed 60. Sliced with Cura.

![images/158a58f8935cd00864bb0aa61bff2cd7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/158a58f8935cd00864bb0aa61bff2cd7.jpeg)



**Igor Kolesnik**

---
---
**Eric Lien** *October 16, 2015 19:24*

Looking great. Can't wait to see some printing vids. I always have trouble with that top loop. It's a bear unless you print it at a snail's pace. I never liked going slow on my printer ;)﻿


---
**Igor Kolesnik** *October 16, 2015 21:21*

It was intended to print fast. I will be posting my progress regularly with comments and suggestions. Any experience worth sharing. Plus as soon as I will get a bed cut, I will start printing ABS for the next person in line for Print Forward. My bed is now a retrofitted PCB Mk2a with aluminum heat spreader. It didn't like ABS at all.  


---
**Igor Kolesnik** *October 16, 2015 21:27*

**+Eric Lien** can you share the experience on how you tuned the bed.l? Did you use PID? RepRap does not recommend PID with SSRs from what I understood 


---
**Eric Lien** *October 16, 2015 23:35*

**+Igor Kolesnik** I used PID with SSR on all my printers. No issues here. Just use a high quality SSR like the omron in my BOM.


---
*Imported from [Google+](https://plus.google.com/+IgorKolesnik/posts/EohGE1mVRxM) &mdash; content and formatting may not be reliable*
