---
layout: post
title: "The orders for tonight on 32tooth 2mm gt2 with the un-findable (nearly) 10mm bore pulleys ends at 10:00pm central standard time"
date: March 05, 2014 19:31
category: "Discussion"
author: D Rob
---
The orders for tonight on 32tooth 2mm gt2 with the un-findable (nearly) 10mm bore pulleys ends at 10:00pm central standard time. The shipping domestically (USPS First Class) is handled as below. Cost for each is $2.30 us.





10 oz to australia costs $14.90 +$1.05 for tracking

for usps domestic shipping costs $1.05 for tracking + $2.32 for first 3oz and $0.18 per additional oz up to 13 Plus the padded envelopes I havent priced yet so im charging $4.10 as if all are 13oz and $1.00 for the envelope. plus the qnty ordered + their shipping divided evenly





**D Rob**

---
---
**Eric Lien** *March 05, 2014 19:41*

Do these have single or double set screw?﻿


---
**D Rob** *March 05, 2014 19:42*

i believe double but I will double check


---
**Tim Rastall** *March 05, 2014 19:43*

**+Eric Lien** if they are like the 8mm bore ones,  they use 2


---
**Carlton Dodd** *March 05, 2014 22:29*

PayPal sent.


---
**Daniel Fielding** *March 06, 2014 11:37*

Would 10 oz be enough for 10 pulleys? Not sure what to expect. Would $45 sound right to Australia for 10 pulleys?


---
**D Rob** *March 06, 2014 12:48*

Daniel we can try for that but if it comes a little difference you can PayPal the difference after the post office gets it on the scale but yes it should be close


---
**BoonKuey Lee** *March 11, 2014 02:22*

btw. can I use 36 tooth instead of 32 tooth?


---
**D Rob** *March 11, 2014 02:44*

I dont see why not. but The smaller the outer diameter of the pulley the less chance that you will have to modify things like the carriage. or in my case the corner bearing holders. If the pulley is too large it will not allow the rods to keep thes same distance from each other. this will use more plastic for parts modified (the carriage) which is no biggie. but also eat into the z height. I haven't measured yet so i really don't know if the 32 will fit yet 


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/Jk23zkcQ9Q8) &mdash; content and formatting may not be reliable*
