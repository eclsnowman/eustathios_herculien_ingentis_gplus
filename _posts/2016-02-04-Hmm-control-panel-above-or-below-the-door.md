---
layout: post
title: "Hmm control panel above or below the door"
date: February 04, 2016 22:03
category: "Show and Tell"
author: Jim Stone
---
Hmm control panel above or below the door. I'm thinking above

![images/18db658ebb062ffb81706542060ea122.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/18db658ebb062ffb81706542060ea122.jpeg)



**Jim Stone**

---
---
**Eric Lien** *February 04, 2016 23:22*

Ahh the wire wrangling... My least favorite part of printer building. No matter how much time I take wiring I am never happy enough with the results. But by then the wires are to short to reroute. The best piece of advice I can give is leave em a little long and use gradual bends so you maintain some slack :)


---
**Jim Stone** *February 05, 2016 00:06*

So...control panel up.... Got it XD


---
**Eric Lien** *February 05, 2016 00:34*

Mine is out front on hinges. I think it really comes down to preference, and where you will have it located.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/1SCab39xXtH) &mdash; content and formatting may not be reliable*
