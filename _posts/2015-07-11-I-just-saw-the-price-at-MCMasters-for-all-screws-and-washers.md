---
layout: post
title: "I just saw the price at MCMasters for all screws and washers"
date: July 11, 2015 00:12
category: "Discussion"
author: Igor Kolesnik
---
I just saw the price at MCMasters for all screws and washers. Gave me a thought. Since most of parts are coming in x100 I can resell leftovers by their price + shipping to whoever will be interested. parts will be sorted and marked by eustathios v2 BOM





**Igor Kolesnik**

---
---
**Eric Lien** *July 11, 2015 00:16*

Use [http://www.trimcraftaviationrc.com/](http://www.trimcraftaviationrc.com/) for everything you can. They are great, hardware is stainless at a fraction of the price, and qtys are more realistic. 


---
**Igor Kolesnik** *July 11, 2015 00:17*

Thanks. BTW are there any builders from Canada?


---
**Eric Lien** *July 11, 2015 00:38*

**+Igor Kolesnik** there are a few actually. But my memory is terrible. **+Ashley Webster**​ is in Vancouver if I recall. I myself am not far, I live in Minnesota.


---
**Gus Montoya** *July 11, 2015 00:54*

**+Igor Kolesnik**  Also a quick search for industrial hardware in your area will be suficient. I found one not too far from me that has same price as trimcraftaciationrc. Hope their is one near you.


---
**Brent ONeill** *July 11, 2015 02:35*

**+Igor Kolesnik** , I'm in Vancouver and stock many fasteners, washers nuts etc in metric as well as imperial sizes. My prices are well below any other distributors for lower than 100 quantities. Send me a list of what you need for your build, I'll give you a quote. I distribute every type of fastener available.


---
**Eric Lien** *July 11, 2015 02:41*

**+Brent ONeill** any chance you want to work up a quote of the fasteners from the BOM for HercuLien and Eustathios. If you can give a single item number price they can buy at a good price and they get the batch, I will change the BOM over to you as the supplier.


---
**Brent ONeill** *July 11, 2015 03:15*

**+Eric Lien** , I've been planning on helping the DIY community with lower prices. Summer is here and I've been working on a webstore to service the DIY's on the west coast. The website [www.technologysalad.com](http://www.technologysalad.com) will soon be open to all. I currently stock a huge range of machine screws, soc head capscrews, setscrews, button head capscrews, Gr.5 hex head capscrews, washers, nuts, lockwashers etc etc.


---
**Mike Miller** *July 11, 2015 15:02*

I'm lucky enough to have a Fastenal just down the street, but you're right, I'd hade to be the guy building this printer buying 5 bolts at a time at BigBoxStore prices. 


---
**Brent ONeill** *July 11, 2015 19:32*

It's great you have a distributor so close **+Mike Miller** , especially if you have the cashola to pay for the convenience. I'm a distributor similar to Fastenal, I purchase from the same fastener manufacturers. I don't have the huge volumes of stock on hand, or the overhead of a warehouse so I'm able to offer the same product at lower prices...for quantities under 500 of each size, I'm able provide very attractive pricing thus helping the small businesses, prototyping shops, hobbyists and DIYs on the west coast.


---
*Imported from [Google+](https://plus.google.com/+IgorKolesnik/posts/Xga6Rw4AhSp) &mdash; content and formatting may not be reliable*
