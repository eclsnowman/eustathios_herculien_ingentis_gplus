---
layout: post
title: "Im so happy Its working :) Thanks to continuosly support from Martin Bondeus its now working ."
date: October 23, 2016 06:05
category: "Discussion"
author: Mikael Sjöberg
---
Im so happy

Its working :) Thanks to continuosly support from  Martin Bondeus its now working .

The print result is not perfect but Thats the next gate to pass :)



![images/80e05f27b6429bbec2b97b61807723b8.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/80e05f27b6429bbec2b97b61807723b8.gif)



**Mikael Sjöberg**

---
---
**jerryflyguy** *October 23, 2016 06:40*

Looking good!


---
**Eric Lien** *October 23, 2016 12:48*

Congratulations. It's always awesome to see another printer come into existence. Make sure to ask questions here in the group if you run into issues. We have lots of great minds with experience tuning these style of printers.


---
**Mikael Sjöberg** *October 23, 2016 13:48*

![images/78c7db9052646b02fc5380c3f3d819bf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/78c7db9052646b02fc5380c3f3d819bf.jpeg)


---
**Mikael Sjöberg** *October 23, 2016 13:50*

**+Eric Lien** yes i will I haven't solved my Glcd screen completely yet . I cannot scroll in the panel . I guess I'm missing some codes in the configfile 


---
**Tobias Rodewi** *October 24, 2016 07:15*

Hi Mikael!



By the look of you name I guess you are swedish... 

Did you find a swedish distrubutor for the alu profiles?



//Tobias


---
**Mikael Sjöberg** *October 24, 2016 17:37*

**+Tobias Rodewi** yes Im swedish 

I bought My Aluprofiles from motedis in germany 

My recommendation is if you use them , they are really good , then better place one big order then many small to reduce freight costs 


---
*Imported from [Google+](https://plus.google.com/118217975155696261814/posts/BLbNJNatrhc) &mdash; content and formatting may not be reliable*
