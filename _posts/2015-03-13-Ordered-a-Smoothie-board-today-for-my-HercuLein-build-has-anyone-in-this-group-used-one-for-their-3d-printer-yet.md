---
layout: post
title: "Ordered a Smoothie board today for my HercuLein build, has anyone in this group used one for their 3d printer yet?"
date: March 13, 2015 01:51
category: "Discussion"
author: Bruce Lunde
---
Ordered a Smoothie board today for my HercuLein build, has anyone in this group used one for their 3d printer yet?





**Bruce Lunde**

---
---
**Eric Lien** *March 13, 2015 01:59*

I am using an azteeg x5 mini on my eustathios. It is a great board. I assume smoothieboard with the extra outputs would be great as well. I think **+Marc McDonald**​ is using a smoothie board.


---
**Dat Chu** *March 13, 2015 02:41*

Not yet but I bought mine from that group buy on deltabot list as well.


---
**Bruce Lunde** *March 13, 2015 03:44*

Thanks for the responses, I figured I should check in case I run into configuration problems, since I am new to 3d printing.


---
**Oliver Seiler** *March 13, 2015 04:39*

I've ordered one as well for my Eustathios in the making


---
**Chad Nuxoll** *March 13, 2015 14:52*

I just got done configuring mine and it was one of the easier ones to configure that I have worked with. 


---
**Bruce Lunde** *March 13, 2015 17:44*

That is great to hear! I may be posting questions to the group, as this is all new to me.


---
**Seth Messer** *March 16, 2015 01:39*

Just to add to the list, I too have purchased a smoothieboard 5xc.


---
**Bruce Lunde** *March 16, 2015 13:10*

**+Chad Nuxoll** Did you use the information on the Smoothie web page for 3d printer to do the config?


---
**Chad Nuxoll** *March 16, 2015 22:29*

**+Bruce Lunde**​ I did use the info from the smoothie Web page and it worked great for me. 

[http://smoothieware.org/3d-printer-guide](http://smoothieware.org/3d-printer-guide)


---
**Gus Montoya** *March 17, 2015 15:41*

I found the discount code, and am wondering if buying a relay, or FDTI Platinum would benefit me? Not sure if the code is multi-use.


---
**Dat Chu** *March 17, 2015 16:05*

Code is multi-use. It applies to the entire order. If you need those elements in the future, why not? The code won't last long.


---
**Gus Montoya** *March 17, 2015 16:35*

**+Dat Chu** that's the question. I'm not sure if I ever will. Since I am new to the board, I don't know exactly what those extra parts are for.


---
**Dat Chu** *March 17, 2015 16:37*

The only two things that you are likely to use would be the voltage regulator and the LCD. Those two should make your printer much better. With the coupon, the price is attractive. The rest can always be bought later when you need it.


---
**Gus Montoya** *March 17, 2015 19:56*

**+Dat Chu** Do you mean the smoothieboard GLCD shield and switching regulator DC to DC 5v? I don't see an LCD persay.


---
**Dat Chu** *March 17, 2015 19:57*

Yes


---
**Gus Montoya** *March 17, 2015 20:03*

glcd is sold out :(


---
**Dat Chu** *March 17, 2015 20:12*

They don't allow backstocking?


---
**Gus Montoya** *March 17, 2015 20:57*

Apparently not , Not sure if I'll need it if I want to do the same tablet  mod as @eric lien .


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/BbWo1YpjFtr) &mdash; content and formatting may not be reliable*
