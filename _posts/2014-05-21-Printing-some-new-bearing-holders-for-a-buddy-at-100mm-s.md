---
layout: post
title: "Printing some new bearing holders for a buddy at 100mm/s"
date: May 21, 2014 02:13
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
Printing some new #Eustathios bearing holders for a buddy at 100mm/s.

Also, doing a test print at 160mm/s. Note the slight decrease in quality of the cylinder on the right vs the left due to the higher speed.



![images/7931c20f7c27197baa1054e0969f7097.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7931c20f7c27197baa1054e0969f7097.gif)
![images/29a5aaafc9fb0f0ad0c45310496befc5.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/29a5aaafc9fb0f0ad0c45310496befc5.gif)
![images/75de5ff5005109d5fb40b90dff6a5751.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/75de5ff5005109d5fb40b90dff6a5751.jpeg)

**Jason Smith (Birds Of Paradise FPV)**

---
---
**Eric Lien** *May 21, 2014 03:18*

I am so excited for my motors to arrive from China. I ordered them 5+weeks ago from aliexpress. Ugh. But they just hit the mainland in san fransisco :)



Anyway mind sharing your marlin settings? I would help a lot on getting some tried and tested setting to JumpStart calibration.



Also what extruder are you using? Airtripper?


---
**Jason Smith (Birds Of Paradise FPV)** *May 21, 2014 03:33*

I (and everyone else here, I'm sure) knows the feeling of waiting on parts :)  I ordered many of my original parts from ali right before the Chinese new year.  Some of them took 2+ months to hit the states...

You can find my marlin settings here.  I'm sure there's room for improvement:

[https://drive.google.com/file/d/0B2629YCI5h_wSWtRelhlOUx1MGM/edit?usp=sharing](https://drive.google.com/file/d/0B2629YCI5h_wSWtRelhlOUx1MGM/edit?usp=sharing)

Yes, I'm using the airtripper.  Good luck, and keep us posted on your build.


---
**Eric Lien** *May 21, 2014 05:21*

Excited about these acceleration values: {9000,9000,90,10000}. :)﻿



And thanks.


---
**Riley Porter (ril3y)** *May 21, 2014 11:13*

**+Shauki Bagdadi** I think its actually his hugely overpowered fan that is cooling the print too fast.  


---
**Maxim Melcher** *May 21, 2014 17:59*

**+Shauki Bagdadi** [http://www.dr-henschke.de/waerme_filament.html](http://www.dr-henschke.de/waerme_filament.html)


---
**Maxim Melcher** *May 21, 2014 21:14*

Аглицким так себе,  немецкий вполне себе,  русский - ну ни фига себе :)


---
**Maxim Melcher** *May 21, 2014 21:20*

**+Shauki Bagdadi** он же [http://www.dr-henschke.de/advance.html](http://www.dr-henschke.de/advance.html) 


---
**Jason Smith (Birds Of Paradise FPV)** *May 21, 2014 21:38*

**+Shauki Bagdadi** I re-printed with 20° higher temperature, and the result was much closer to the original print.  Thanks for the tip!


---
**Riley Porter (ril3y)** *May 21, 2014 21:46*

Sweet


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/VmVtwV8U8aR) &mdash; content and formatting may not be reliable*
