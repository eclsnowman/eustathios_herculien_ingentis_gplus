---
layout: post
title: "Just a fyi for anyone building a Herculien, trimcraftaviationrc has added some new items since the BOM was last updated"
date: June 09, 2015 02:32
category: "Discussion"
author: Walter Hsiao
---
Just a fyi for anyone building a Herculien, trimcraftaviationrc has added some new items since the BOM was last updated.  I think the following are are equivalent to the mcmaster parts, but cheaper, at least if you're looking at stainless steel versions.  When I ordered parts for my Eustathios, I pointed out the printer BOMs and he was nice enough to add a few more items to his inventory.



3mm oversized flat washer (Din 9021) - [http://www.trimcraftaviationrc.com/index.php?_a=product&product_id=458](http://www.trimcraftaviationrc.com/index.php?_a=product&product_id=458)



5mm oversized flat washer (DIN 9021) - [http://www.trimcraftaviationrc.com/index.php?_a=product&product_id=459](http://www.trimcraftaviationrc.com/index.php?_a=product&product_id=459)



5mm x 10mm button head screw - [http://www.trimcraftaviationrc.com/index.php?_a=product&product_id=456](http://www.trimcraftaviationrc.com/index.php?_a=product&product_id=456)



5mm x 12mm SST socket head cap screw - [http://www.trimcraftaviationrc.com/index.php?_a=product&product_id=451](http://www.trimcraftaviationrc.com/index.php?_a=product&product_id=451)



5mm wing nut (DIN 315) - [http://www.trimcraftaviationrc.com/index.php?_a=product&product_id=455](http://www.trimcraftaviationrc.com/index.php?_a=product&product_id=455)





**Walter Hsiao**

---
---
**Eric Lien** *June 09, 2015 03:50*

Awesome. Those guys are great.


---
**Walter Hsiao** *June 09, 2015 06:56*

Yeah, I was so happy to find out about them, thanks! Between the ridiculously low prices and $3 shipping, they're great for someone who has an irrational desire to replace all the hardware with stainless versions.  I think I've placed 4 orders and thought I had all the M3, M4, and M5 screws I could possibly want, but it looks like I already need more.


---
*Imported from [Google+](https://plus.google.com/+WalterHsiao/posts/Thtareu7h65) &mdash; content and formatting may not be reliable*
