---
layout: post
title: "Hey all just an FYI that I adapted Eric Lien's original Eusththios Spider 2 carriage for 2 M8 linear bearings: This is an alternative carriage for a Eustathios Spider 2 3D printer that uses m8 linear bearings instead of"
date: April 29, 2018 06:31
category: "Deviations from Norm"
author: Ted Huntington
---
Hey all just an FYI that I adapted Eric Lien's original Eusththios Spider 2 carriage for 2 M8 linear bearings: [https://www.youmagine.com/designs/alternative-eustathios-3d-printer-carriage-uses-m8-linear-bearings](https://www.youmagine.com/designs/alternative-eustathios-3d-printer-carriage-uses-m8-linear-bearings)



This is an alternative carriage for a Eustathios Spider 2 3D printer that uses m8 linear bearings instead of bushings. This can reduce friction, cost, and wait for parts. But the original carriage works fine too (so I have not actually used this carriage yet- if anyone does please let me hear your comments). Note that this carriage was reduced from the stl of the original in Blender since there were not Blender compatible files available (and I mostly 3D model using Blender), so has some backward polygons and other rough spots, but prints fine. This is the version with supports already in the model. I was able to trim a few millimeters off the original carriage to gain a little extra printing space. Unfortunately I removed the lower cover part attachments which I do not need for most of the printing I do.









**Ted Huntington**

---
---
**Eric Lien** *April 29, 2018 13:20*

Looking good. I like the clamshell spring design for holding in the bearings.


---
**Ted Huntington** *April 29, 2018 20:52*

**+Eric Lien** Thanks I just copied the idea from a RepRap I bought from China- it works very well - surprisingly simple


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/SUowun7tztF) &mdash; content and formatting may not be reliable*
