---
layout: post
title: "Phew. That took a while but all the parts are ordered / claimed with respective vendors"
date: January 30, 2015 06:03
category: "Discussion"
author: Dat Chu
---
Phew. That took a while but all the parts are ordered / claimed with respective vendors. Now I need the extrusions cut to the right length and then the <s>LEGO</s> assembly begin.



Needless to say, I am feeling excited.





**Dat Chu**

---
---
**Eric Lien** *January 30, 2015 06:57*

I am excited too. Keep in touch with questions.


---
**Dat Chu** *January 30, 2015 07:01*

I watched the video by Thomas Sanladerer. I think I am gonna go with the Rambo from ReprapElectro. [http://reprapelectro.com/product/rambo/](http://reprapelectro.com/product/rambo/) It works with Marlin too.


---
**Dat Chu** *January 30, 2015 07:04*

I think the one **+Brandon Satterfield** has in his store is the same one made by ReprapElectro. Brandon is truly my MakerCrack supplier.


---
**Miguel Sánchez** *January 30, 2015 07:42*

There is a smaller unit now [http://shop.prusa3d.com/en/electronics/49-rambo-mini.html](http://shop.prusa3d.com/en/electronics/49-rambo-mini.html)


---
**Brandon Satterfield** *January 30, 2015 11:54*

Thanks **+Dat Chu** I'm pretty stoked about your builds too man! Looking forward to seeing you get this together! 


---
**Paul Sieradzki** *January 30, 2015 14:06*

**+Dat Chu** Not sure if you are willing to cut your own extrusions, but if you are I can sell you more than enough to make your build and have spares in case of an error for really cheap. I'm moving so can't go forward with my plans for a big build, and I'd love to see the materials used for a build! :)


---
**Dat Chu** *January 30, 2015 14:15*

**+Paul Sieradzki**​ I have already bought the v-slot extrusions. 😢


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/UgeHghcEXdj) &mdash; content and formatting may not be reliable*
