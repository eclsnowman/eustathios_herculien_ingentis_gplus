---
layout: post
title: "The first automated moves on my new printer, which I am calling \"Machigai\" ."
date: July 21, 2014 06:14
category: "Deviations from Norm"
author: Jason Barnett
---
The first automated moves on my new printer, which I am calling "Machigai" . Machigai is Japanese for "Mistake", as I have made many during the design of this printer. The frame and basic design is from the Ingentis, but I am using linear rails for all axes. Inspiration was taken from many places to design the mounts, but they are of my own design, so when they fail, I have no one else to blame.



Only X and Y at the moment, but it seems to be working well. I am borrowing the board from my Cupcake here, just for testing purposes.



Can't wait to get my Azteeg X5 next month, by which time I should have the rest of the printer designed and working. Still lots of work to be done, but that's where the fun is.. :)


**Video content missing for image https://lh5.googleusercontent.com/-A3SzLZ7E7Iw/U8ytybunfdI/AAAAAAAAFR8/ISfkmzJmU1k/s0/VID_20140720_195037.mp4.gif**
![images/7ad998496885559c11988138d523ec82.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7ad998496885559c11988138d523ec82.gif)



**Jason Barnett**

---
---
**Daniel Fielding** *July 21, 2014 06:33*

Looks good very much like what I want to do with makerslide but yours is not a design just in your head :)


---
**Jason Barnett** *July 21, 2014 06:56*

**+Daniel fielding** I almost wish it were! I have a pile of parts that I have printed and found that they didn't work like I expected or I figured out a better style. I should have thought it out a bit better to begin with... Hence the name :)

 I think the makerslide is probably a better way to go than my route. these rails were reasonably cheap but I only got 4 of the 6 that I bought, to work reasonably.


---
**Daniel Fielding** *July 21, 2014 06:59*

My biggest problem has been availability of makerslide in Australia but now it's funds.


---
**Jason Barnett** *July 21, 2014 07:03*

**+Daniel fielding** I understand that feeling. I started buying parts for this about 8 months ago and am finally getting them put together. I am still finding other parts that I wish I had bought already and are now on my wishlist for later.


---
**Pieter Swart** *July 22, 2014 05:21*

Jason, this looks really good. Where did you buy the linear rails from? Likewise, I'm sitting with a lot of stuff that didn't work out, and even more on my todo list. My biggest pain so far has been to get reliable straight shafts. The guys at Makers Toolworks actually told me that they are moving over to rails completely on their printers because of similar reasons. 


---
**Jason Barnett** *July 22, 2014 19:48*

**+Pieter Swart** I got them from RobotDigg.com, though I do NOT recommend buying them. At $58 dollars for 3 rails, I was not expecting much, but got even less. I ordered two sets of 3 rails and was only able to get four rails to work to an acceptable level. I actually had to remove some of the bearings to get the cars to move without binding.

RobotDigg, did give me a refund on one set, so I am happy with that, but it did take a bit to get them to respond. 

Once this printer is fully functional, I intend to try printing replacement cars for the rails, using the IGUS filament and see if I can get that to work better. If so, then it might be cheaper to buy decent quality rails and just print the cars for them... I hope... :)



Here is the link, just in case you still want them... [http://www.robotdigg.com/product/164/China-Made-MGN-12H-L400-hardened-Steel-Rail-and-Carriage-for-Kossel](http://www.robotdigg.com/product/164/China-Made-MGN-12H-L400-hardened-Steel-Rail-and-Carriage-for-Kossel)


---
**Pieter Swart** *July 22, 2014 20:24*

Thanks **+Jason Barnett**. I assume they buy those from a third party. I had a similar issue with their 8mm shafts. Keep us updated on your progress. I would love to see how this turns out!


---
*Imported from [Google+](https://plus.google.com/108834075676294261589/posts/CBHsBWyMCPh) &mdash; content and formatting may not be reliable*
