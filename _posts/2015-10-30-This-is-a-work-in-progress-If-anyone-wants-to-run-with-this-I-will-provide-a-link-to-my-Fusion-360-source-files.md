---
layout: post
title: "This is a work in progress. If anyone wants to run with this I will provide a link to my Fusion 360 source files"
date: October 30, 2015 16:52
category: "Deviations from Norm"
author: Bud Hammerton
---
This is a work in progress. If anyone wants to run with this I will provide a link to my Fusion 360 source files. The model timeline is a mess since it was try this, then that, then figuring out something I did near the beginning wasn't going to work, etc.



I still need to do a bit of touch up and create a fan duct set for the Volcano. There is currently no way to fasten any wiring.



I tried to get this extruder to be the physically smallest it can be without compromising rigidity. Just a note, I haven't printed this yet as I ran out of orange translucent filament.



Some key features: Uses the Robotdigg 8x12x30 Graphite/Bronze Self-Lubricating Bearings. Single bearing in the X and Y axes. Brass heat-set threaded inserts as there are no high torque screw settings. Uses the stock E3D 30 x 30 x 10 mm fan. Parts fans are 40 x 40 mm.



Of course this idea was stolen from the Chinese Ultimaker 2 extruder posted a few days ago and made to work with the Eustathios.



![images/2b3ee47ce3567c8c98357335e62cb14f.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2b3ee47ce3567c8c98357335e62cb14f.png)
![images/fbf76d29b3b672a2867247ea0c914c51.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fbf76d29b3b672a2867247ea0c914c51.png)
![images/4a0673226ddf6a336b474221b319a146.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4a0673226ddf6a336b474221b319a146.png)
![images/44103e33101bfbd0cfd975b65f8a240f.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/44103e33101bfbd0cfd975b65f8a240f.png)
![images/6c75593cd8af638a3dccb4a09f75fb79.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6c75593cd8af638a3dccb4a09f75fb79.png)

**Bud Hammerton**

---
---
**Eric Lien** *October 30, 2015 16:55*

Looks great. Nice work.


---
**Bud Hammerton** *October 30, 2015 18:04*

Shaing the WIP design at this link: [http://a360.co/1LGQutg](http://a360.co/1LGQutg)


---
**Jim Stone** *October 30, 2015 21:55*

ugh i wish i knew how to use these programs D: i dont even know how to make those screws to be in the models....let alone a simple hollow square D:


---
**Eric Lien** *October 30, 2015 22:25*

**+Jim Stone** I didn't know before I started either. I started into 3dprinting as an excuse to learn solidworks. Lots of great videos and tutorial out there. And between onshape, educational fusion360, designspark and others there is no $ barrier to entry. Just time my friend just time.


---
**Erik Scott** *October 30, 2015 23:58*

Nice. Pretty compact. Just worried with the bearings being so short, there might be a bit of play or possibly some binding. Could be no issue though. 



How do you plan on getting the long brass bearings in there? Are they just pressed in? 



Nice render, btw. You've even got subsurface scattering in there! Looking forward to the day our parts look that nice coming off the printer. 


---
**Bud Hammerton** *October 31, 2015 03:12*

The whole point of single bearings is to minimize any binding caused by misalignment. I would actually think this is a better way to go than with bearings spaced apart. This is why self-aligning bearings are specified. A single bearing doesn't need to be self-aligning. Yes the bearings are simply pressed in, Can't actually try this out until Monday, when I get some bearings from Robotdigg.



Fusion 360 allows ray-tracing or just plain old screen captures. The first image has ray tracing on. The only issue is that it pegs every core on my I7 at 95-100% CPU, so I left it to run overnight.


---
**Erik Scott** *October 31, 2015 03:44*

No GPU rendering?  That sucks. I don't care for rendering packages don't give me control, but then again, I approach that from more of an artistic side. 



Any way, I know what you mean about the single bearing. I guess I was just more concerned as there's a but more room for misalignment and, at least in my mind, I could see some potential binding. It all depends on how tight the tolerances are on the bearing and rod. There's also some more potential for movement of the nozzle. It's hard to explain what I mean exactly. Maybe tomorrow I'll make a picture. 



I don't want to sound really critical of what you've done. I think it may be really good, and I'm looking forward to hearing about the results. 


---
**Bud Hammerton** *October 31, 2015 04:00*

Well it is a CAD program, after all, so I just let it run overnight if I want it to look pretty. Otherwise it's just screen caps.



Regarding mis-alignment, unless your source printer is really messed up there likely shouldn't be much of an issue. If it binds with a single bearing, it will bind just as effectively with two.



Nozzle movement is my biggest concern. My current printer is a Robo3D and the Greg's Wade Extruder does not hold the hotend rigidly, you can see it and feel it move if you touch it. I have it in there pretty tight also. This was one reason why I am designing something simpler, Eric did a great job on his carriage but my gut tells me it is more prone to hotend wiggle than something that firmly grasps the top of the heatsink. No zip ties here!  In my case the concern is magnified by the fact that I will be using a Volcano, which has a 10 mm longer lever arm than the standard E3Dv6.


---
**Erik Scott** *October 31, 2015 04:19*

I modified Eric's carriage to clamp the hotend, much like you have here. You should be able to find the post somewhere in this community. 



By spacing the bearings, small differences in the inner diameter of the bearings and the outer diameter of the rod result in smaller angular changes. This is the point I am trying to make. Here's a quick pic to show the difference. [http://i.imgur.com/77DYQ8q.png](http://i.imgur.com/77DYQ8q.png)

It's obviously exaggerated, but I think you can see why I'm a bit concerned. 


---
**Bud Hammerton** *October 31, 2015 04:37*

Erik, I do understand what you are saying, but whether its 1° or 4° it would still bind. But tolerances are just not that tight. So even a minor misalignment will be easily overcome during the break-in period. Common practice is to run through a break in period when you seat new bearings in any type of mechanical device to account for these small errors.



We are dealing with printed parts, bigger parts have a greater chance of misalignment than smaller ones. It could always be CNC milled is it is of real concern.



BTW, I did the same thing you did originally, but did not like to final result, this is why I decided to move past the original hotend carriage. I did see your post and link to the files. I am not familiar enough with the web based software you used  to download anything but an STL, so I gave up and did my own version.


---
**Bud Hammerton** *October 31, 2015 13:56*

**+Erik Scott** I have given much more thought to your concerns, after sleeping on it. I can no longer see where your concerns are based. The flaw in your diagram is that you actually illustrated a problem that could not exist with two bearings and a bearing block that is printed from the same source. If one plane is off 4° in 30 mm then guess how much it will be off in 60 mm (4° if you didn't want to guess). Printers do not get more accurate just because the printed object is bigger. If it's going to bind because of misalignment chances are that the longer length will bind more since you have actually moved the planes further apart. This is fundamental geometry and has very little to do with whether or not this is a viable design.



With regard to hot end movement, I have now printed a test piece and clamped a Volcano in the carriage. With even a minimum of holding torque on the clamp the tip of the nozzle does not move in any direction. I am confident that barring something stupid on my part (like the cooling fan stopping/non-working) it will hold the hotend rigidly.



Multiple bearing arrangements always need to be concerned with alignment, they are generally used to provide additional load that a single bearing cannot sustain, not to compensate for alignment issues. A single bearing design should and is sufficient for the loads the hotend/carriage places on them.


---
**Eric Lien** *October 31, 2015 14:24*

I think the proofs in the pudding guys. Let's give her a whirl. Real world testing always trumps theory in my book. I have had some great ideas on paper that never panned out in reality, and stuff that I thought would never work that ended up performing great.﻿


---
**Eric Lien** *October 31, 2015 14:29*

BTW my concern would be less with misalignment since it is a single bearing. I would be more concerned with the location of the force of the cross rods to the assymetry of the carriage. I run into this on my carriage. Since the cross rod C-C spacing does not allow the bearings to directly overlap due to the bearing OD the torque forces due to cantilever are different in positive x vs negative x. I can actually see this during fast travel moves as micro chatter/resonance. As you bring the bearing contact area inwards towards the hot end center of mass this will amplify.


---
**Bud Hammerton** *October 31, 2015 16:01*

**+Erik Scott**  Rendering: I don't believe Fusion 360 does GPU assisted ray tracing, but you can use Autodesk's servers and do cloud rendering. Final renders take about 20-30 minutes and your PC didn't have to do any work. You have unlimited space to save renders, but can only request 16 cloud-based renders per month. The quick rendering that Autodesk uses isn't bad for illustration purposes, so I don't usually turn ray-trace on.



It is not as complete as SolidWorks, but it is free for educational and non-commercial use and that's a good thing.



My mistake, it's not 16 requests per month, it is 16 renders per request, so if you want multiple views in the same request the max is 16. By the way, the render above took overnight on my PC, but the cloud based one took less than 5 minutes.


---
**Erik Scott** *October 31, 2015 16:44*

**+Eric Lien** that force offset is a contributor, but the fact that one rod is below the other will always create some moment that could cause a slight tilt in the carriage. But I think I'm blowing it out of proportion with all this talk. Give it a shot and we can discuss the results later. 



I too am running into this chatter on my carriage. I haven't worked on the printer in a couple weeks as I needed a break. I'm thinking about making yet another carriage with LM8UU bearings. 


---
**Bud Hammerton** *October 31, 2015 16:56*

**+Erik Scott**  This could easily be modified to accept LM8UU, but you might want to go LM8LUU so the bearings are longer.



Also looking at +Ashley Webster 's XY Axis parts, the bearing holder could be made open, say at 270° to allow a bit of play if needed as well as ease of insertion. That is one reason I shared the  Fusion 360 file directly, so others could adapt it, if they so desire.


---
**Erik Scott** *October 31, 2015 22:11*

I don't like press-fit for the LMXUU bearings as it is very dependent on the printer's tolerances. I want to make some sort of way to clamp them in there, like the z-axis linear bearings. 



Any yes, LM8LUU bearings would be better for the very reasons I mentioned before. 


---
**Bud Hammerton** *November 01, 2015 00:31*

**+Erik Scott** as I mentioned, it could easily be adapted to a 270° open block with maybe something like zip ties to hold it tight.



Printer tolerance is what the user will accept. I always felt it funny that many go to all the trouble of precisely modeling it in a CAD program, but when printed not enough care is taken to setup and if its off a few percent, it's okay. As long as it fits together. I just don't get it. I have even gotten feedback on stuff I posted in repositories of people who have printed a model and then complain that it doesn't fit when they never really bothered to make sure their printer is accurate. A 3 cm cube should be 3 cm not 2.99 cm.



I am looking forward to see what you come up with. I am not anti LMxUU bearings just not in love with them either.



I should have my remaining few missing gantry parts on Monday and I will test it all out. If it doesn't work as expected or desired, easy enough to modify and print over.


---
*Imported from [Google+](https://plus.google.com/+BudHammerton/posts/BEbhgpJWcWr) &mdash; content and formatting may not be reliable*
