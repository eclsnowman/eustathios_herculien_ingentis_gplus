---
layout: post
title: "Huh. Originally shared by null Want to see the BOM that took the cake in our contest?"
date: February 20, 2015 05:20
category: "Show and Tell"
author: Tim Rastall
---
Huh. 



<b>Originally shared by null</b>



Want to see the #3Dprinter BOM that took the cake in our #Reddit contest? Check out the Alumimaker 2! Pics and STLs available too.





**Tim Rastall**

---
---
**Dat Chu** *February 20, 2015 05:45*

His design look very very similar. Oh well.


---
**Eric Lien** *February 20, 2015 07:30*

**+Tim Rastall**​ kinda felt the same way... Huh.


---
**Tim Rastall** *February 20, 2015 07:49*

It's basically an ultimaker 1 with extrusions.  Not a copy of ingentis or Eustathios or herculien. Same ultimaker carriage. Same belt set up etc etc. 


---
**Jim Wilson** *February 20, 2015 12:43*

It even has the same two z rods on one side like the Ultimaker and it looks like their EXACT carriage... Is it different at all other than size?﻿


---
**Robert Burns** *February 20, 2015 14:16*

the big difference was he submitted it (I won 3rd place BTW) 


---
**James Rivera** *February 20, 2015 18:50*

It uses direct drive on the X & Y axes instead of a short belt, so it is a little bit different than an Ultimaker.


---
**Tim Rastall** *February 20, 2015 19:30*

**+James Rivera** good point,  missed that. 


---
**Brad Hopper** *February 20, 2015 20:27*

The rules said it did not have to be an original BOM regardless. I asked for any info Misumi might provide on their selection criteria and they didn't want to reveal. I suspect it might be down to the terrific photo gallery. 


---
**Seth Messer** *February 21, 2015 02:16*

to be honest, this was a build I was going to chase after before digging deeper into this community and deciding on the eustathios. no denying the success of the UM and UM2 (that price though.. yikes)


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/PBpnYYyWoxt) &mdash; content and formatting may not be reliable*
