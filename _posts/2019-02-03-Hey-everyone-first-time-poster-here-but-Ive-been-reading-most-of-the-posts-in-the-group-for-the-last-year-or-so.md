---
layout: post
title: "Hey everyone, first time poster here but Ive been reading most of the posts in the group for the last year or so"
date: February 03, 2019 07:12
category: "Discussion"
author: Ásgeir Ögmundarson
---
Hey everyone, first time poster here but I’ve been reading most of the posts in the group for the last year or so. I’ve designed a printer in Solidworks that is very much based on Eustathios V1 and Herculien, with a couple of twists.



The goals I have with this printer is to make it as cheap as possible on parts not essential for achieving the highest possible quality prints (within reason), so I’m constructing the frame out of some square steel tubing I found and some wood. Another goal is to make everything as neat as possible, hiding everything possible (wires, belts, motors, etc). That’s where the current working name came from: Houdini.



I had the idea to do a Eustathios V1 style XY gantry and trying to hide the vertical drive belts within the square steel tubes. The only unusual thing I have to do is having to drill out a couple of 20T GT2 pulleys to make the ID=10mm (according to my Solidworks testing, this should be okay).



One modification made is a little cut to the top of the Z gantry plate to allow it to go a little higher as Walter’s mini space invaders carriage has the tip of the nozzle at about 2 cm higher than the ones on Eric’s dual carriage.



One way of minimizing cost and problems was making as many things required in standard sizes that are readily available. This includes making the V-Slot extrusions of length 500mm and using TR8X8 leads screws of length 500mm.



I have access to a local fablab that has an X-Carve CNC Mill and a drill press, among other things. I’ll try to use these things as much as possible to make sure everything is cut and drilled to specs.



The deciding factor in XY size are four 10mm rods I pulled from some printers being thrown out at my office that have a length of 503mm. With my design I had to make some minor modifications to the heated bed. I will be using an E3D hotend, Voron Mobius2 extruder (money saving experiment to avoid buying a planetary stepper) and probably an Azteeg X5 GT controller.



I have added some screenshots of my design and some interesting parts I’ve mentioned above. I have yet to add all screws, end stops, controller, power plug, and a few small things. This design also makes it very easy to enclose it, I'll probably do that some day.



I have some questions I was hoping you guys could help me with:



1. Why are 8 eccentric spacers in the V slot assembly in the BOM for Herculien? The Solidworks model has 4 aluminium spacers and 4 eccentric spacers



2. I have a bunch of Nema17 48mm (and smaller) steppers I pulled from the office printers. I am planning to use these for the X&Y drives. I was wondering if I should also use these for a dual stepper setup on Z or if I should spring for a 60mm high torque stepper for Z (considering the high shipping cost of the stepper from robotdigg)?



3. I will be printing the parts in PLA to start with as I have access to free printing in PLA. I have then planned on replacing these with PETG parts printed on this printer once completed. I have the bronze self aligning bushings and I have read some posts here about it being not so nice to remove these after installed. I saw a post about someone creating clamshell belt tighteners, I was wondering if you guys thought it would be feasible for me to design something similar for easy removal of my bushings from the PLA belt tighteners and carriage once I have printed the PETG parts?



4. I had the idea to make the 2020 cross beams for the heated bed (e.g.) 4mm shorter for a degree of freedom (pictured), what do you think?



5. Would there be any reason for not using nyloc nuts instead of standard nuts everywhere possible when securing parts you would expect that you wouldn’t be moving or adjusting much or at all after the initial dialing in (e.g. on my XY gantry bearing holders)? I’m just thinking about trying to prevent things needlessly coming loose over time. I do know that after removing nyloc nuts you can’t use them again, so I would have to replace them after switching out the PLA parts with PETG.



Thanks in advance, I really think this is a great community full of knowledge, it’s a damn shame G+ is being pulled down, hopefully there can be a migration to another platform, e.g. a new subreddit or something.



If you have any feedback, ideas, questions or anything, please fire away!



![images/ea8b827f23f2b2f75dfd227bf66728c2.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ea8b827f23f2b2f75dfd227bf66728c2.png)
![images/26e4586f4ee5ef5d4e28ea7a48aee49d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/26e4586f4ee5ef5d4e28ea7a48aee49d.png)
![images/82e75710d68fad6a5e6e0e948aef424f.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/82e75710d68fad6a5e6e0e948aef424f.png)
![images/d3f6d53a6f3862f9e74c8f9e3d8e8e17.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d3f6d53a6f3862f9e74c8f9e3d8e8e17.png)
![images/edf69991811987eb7aed9993f405d7a8.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/edf69991811987eb7aed9993f405d7a8.png)
![images/e2005a0cd1239bc619120bc283991fa3.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e2005a0cd1239bc619120bc283991fa3.png)
![images/443833c80914d1257ddb949f1f5b82e0.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/443833c80914d1257ddb949f1f5b82e0.png)

**Ásgeir Ögmundarson**

---
---
**Eric Lien** *February 03, 2019 18:18*

Sorry your post got caught in the G+ spam filter for about 10hrs. I try to check it frequently. I really wish they had a notification for community owners for those things.



Anyway to try and answer some questions:



1.) Yes you are right. Only 4 eccentric are needed on a HercuLien style Z-Axis and bed. The two can be fixed so long as your drill marks are on point for the fixed ones. Infact I had always thought about switching to a taller side angle, and going to three wheels (since only three is perfectly constrained). But it has worked for me, so I never changed it.



2.) Z actually doesn't need the torque as much as X/Y since it has so much mechanical reduction. I know people who used smaller steppers without issues. all around. I have a habit of over building things.



3.) I have pulled out my bushings before. If you are willing to sacrifice the printed part... it is no issue. And even saving the part it is doable.



4.) That is a good idea WHen I first designed this printer I did not know as much about proper constraints. by making it 4mm shorter you would have a lot of play for alignment. Probably more than needed. But it should be just fine.



5.) Nyloc nuts should be fine, and you should even be able to reuse them. The nylon does deform, but you are only using it as a friction source to keep screws from backing out. That should still function if removed and reinstalled (withing a reasonable number of times.



I am excited to see you build, keep us updated and if when you get things finalized, please share the model so others could follow suit. Thats the great part about this community. The open sharing of ideas. And if I can make a suggestions. Go with a Duet board. Reprap firmware has many nice features and that would be my go to board nowadays. I love Panucatt boards, but Reprap firmware seems to be where the action is now.



_[https://plus.google.com/photos/..._](https://plus.google.com/photos/..._)


---
**Eric Lien** *February 03, 2019 18:28*

Also for the Z axis, use tr8x8 or a golmart ballscrew, not the misumi lead-screws in the BOMS (they cost much more, and add little value).



And for the extruder, I cannot recommend the Bondtech BMG enough. It is great and has wonderful retraction performance.


---
**Michael K Johnson** *February 03, 2019 19:32*

**+Eric Lien** Why wouldn't you recommend to use TR8*2 single-start instead of TR8*8 quad-start?


---
**Eric Lien** *February 03, 2019 19:35*

**+Michael K Johnson** If you use a fast Z-hop I find Tr8x2 to be too slow. Tr8x8 has plenty of z resolution still, especially on HercuLien where I built in a mechanical gear reduction before it got to the leadscrew, combine that with a 1/32 microstep and you have far more resolution than required in Z.


---
**Eirikur Sigbjörnsson** *February 03, 2019 20:33*

You might want to consider going for a flex plate system instead of using a glass plate (both Eustathios and Herculien are designed with glass plate in mind and it seems your design also uses glass). Yes it will up the cost, but in the long run it will make things lot easier for you. 


---
**Ásgeir Ögmundarson** *February 03, 2019 20:48*

**+Eric Lien** Thanks for the reply, I really appreciate it. Here are some comments to your comments:



1) I really like the idea of switching to three wheels, it both reduces part count and the need for super accurate drilling on the fixed side. I think I may have to give that a try, thanks for the idea.



2) Well that certainly relieves some headache for me, I'll at least try using 48mm steppers on all axis' and see if that gives me any issues.



3) Another load off my mind, I won't care about breaking the PLA parts so I'm happy I don't need to design clamshell holders or struggle to remove the bushings.



4) 4mm was a good number for me as it took the extrusion from 484mm to 480mm, so that's why I landed on that :) I only learned about proper constraints from reading all of the posts on the group, so I definitely got this idea from reading some posts by yourself and others. I think it's going to help me a lot when I actually put this thing together that I am designing everything so that it is adjustable for alignment purposes, especially since I don't have the possibility of just sliding things around a little in  extrusions slots.



5) Sounds great, I think I'll try to use nyloc nuts for most or all places then, thanks.



It's definitely on my agenda to put everything up on Github and share it here once completed, I do love the idea of open source and as a software engineer I especially love that I've found a hobby that incorporates  that way of thinking.



Thanks for the suggestion on Duet, I have been going back and forth a lot between Azteeg X5 GT, Duet Wifi and Radds (or actually making my own RADDS with wifi, I have an Arduino Due lying around and most of the required hardware for building a prototype, but I'm not so sure I want to rely on my own creation when I eventually leave the thing running when I'm not there). The only "issue" with the Duet Wifi is the price point, it would end up costing quite a bit more than other solutions, but maybe I'll bite the bullet and get it anyway.



Regarding the Z drive I will definitely use an off-the-shelf tr8x8 as they are dirt cheap on ali express and look like they will do a good job.



I have a Bondtech extruder as my backup plan in case my experiment with the Voron Mobius2 doesn't yield satisfactory results. It's clear from all I've read that Bondtech is really great, if money was no object, I would definitely go for that from the get go.


---
**Ásgeir Ögmundarson** *February 03, 2019 20:52*

**+Eirikur Sigbjörnsson** I did look at bit into PEI at some point, but I saw that Eric is using PEI on top of his glass so I figured I could always add it later. What type/brand are you using? Do you have yours directly on an aluminum heat spreader?


---
**Eirikur Sigbjörnsson** *February 03, 2019 21:28*

**+Ásgeir Ögmundarson** I am using PEI on my Prusa printers (with flex plates) and it works fine with most materials, and maybe too well with PETG. However for some jobs I like to use Nylon and PEI has not worked for me for that type of material. I have two types of flex plates which I am installing on my JGAurora A5. One is called Ziflex and the other is called Wham Bam. On this printer I am installing the magnetic part directly on the aluminum heat spreader. I had one made with a water jet at a local supplier (in Hafnarfjordur), but I found out that suppliers here are not used to supply plates with the fine tolerance needed for the bed so I imported a aluminum heat spreder with 220v heater from Germany. I tried out the bed design from Eric on a custom made printer of mine and found also out that getting good glass plate locally  is also a problem :) Beside I had trouble with the corner fasteners for the glass plate they tend to soften up if you are heating the bed to ABS levels.



One of the reason I am recommending using flex plate is because it's a major pain to replace the PEI sheet if it gets damaged (for instance the best way to do this on the Prusa is to freeze the print plate before peeling the sheet of and without a flex plate that means removing the print bed from the printer.


---
**Eric Lien** *February 03, 2019 22:10*

**+Eirikur Sigbjörnsson** I second a flexplate system. Use the aluminum tooling plate as the heat spreader for even heating due to the aluminums thermal mass. Then use a magnet system embedded inside the aluminum spreader to hold down a spring steel plate with PEI or buildtack on the steel. I figure you can get pockets for "High Temp N42SH" bar magnets milled into the aluminum. If not milled pockets for bar style magnets you can get round magnets and drill holes which the round magnets drop into. The N42SH magnets are good for high temp, and are a standard spec when you are working your "Google Fu" to source them near you.


---
**Eirikur Sigbjörnsson** *February 03, 2019 22:43*

The flex systems I have are two part. A magnetic base with a glue sheet attached and then stainless steel print plates. Since the base part is a permanent fixture its fine for me to glue it directly on the aluminum heat spreader.


---
**Ásgeir Ögmundarson** *February 04, 2019 01:32*

**+Eirikur Sigbjörnsson** "I imported a aluminum heat spreder with 220v heater from Germany"



Do you have a link for that?


---
**Julian Dirks** *February 04, 2019 01:36*

**+Eirikur Sigbjörnsson** +1 for flex 

plate. I got a spring steel sheet with gekotek on it and have that directly on my aluminium spreader. Was intending to use magnets but have just been using bull dog clips and it’s working really well.


---
**Ásgeir Ögmundarson** *February 04, 2019 06:51*

One question regarding the spring steel sheets, as they are very thin, do they not tend to warp? 


---
**Julian Dirks** *February 04, 2019 07:16*

**+Ásgeir Ögmundarson** the only time mine has warped was when it took it off when it was still very hot. I initially thought I had wrecked it but as it cooled fully it popped back flat again. I let it cool a bit now and with gekotek the parts come off easily anyway. I only need 2 clips to hold it down but I have a 10mm cast aluminium build plate and a 600w silicon heater so very even heating! I’ll put magnets in at some stage.


---
**Ásgeir Ögmundarson** *February 05, 2019 08:54*

**+Eric Lien** I noticed that you use 36T pulleys on the Herculien leadscrews as opposed to 32T on Eustathios, is that due to the Herculien leadscrews having diameter 12mm and being functionally different from the Eustathios tr8x8 ones or is there some other reason?



Can I ask what you wrote to AliRubber when ordering the heated bed for the Herculien? If I remember correctly I've seen you/others talk about having a hole in the middle for the thermistor, having two thermistors for redundancy, asking for longer cables, etc. I was hoping I could get some help with that so I don't end up ordering something that is not ideal.



Thanks!


---
**Eirikur Sigbjörnsson** *February 05, 2019 12:09*

**+Ásgeir Ögmundarson** I got my aluminum plate from [clever3d.de - clever3d.de/](https://clever3d.de/) and Frank also sells the heater so you can just order the heater from him too. Send him an email because Iceland is probably not listed on the webpage. He will custom make the size you want and the drill the holes you need in the plate.


---
**Eric Lien** *February 05, 2019 13:19*

**+Ásgeir Ögmundarson** yes put a whole in the heater pad center so the thermistor can be potted into the plate, but also replaced if it goes bad.


---
**Eric Lien** *February 05, 2019 13:22*

For the z, try to make the pulleys an even multiple if possible (18/36) or (16/32) or (20/40). Makes the optimal layer math for full steps easier.


---
*Imported from [Google+](https://plus.google.com/117390690533383986024/posts/NUVJ8dPTCJh) &mdash; content and formatting may not be reliable*
