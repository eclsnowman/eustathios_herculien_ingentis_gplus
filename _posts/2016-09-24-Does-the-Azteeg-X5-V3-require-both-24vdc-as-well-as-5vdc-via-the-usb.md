---
layout: post
title: "Does the Azteeg X5 V3 require both 24vdc as well as 5vdc via the usb ?"
date: September 24, 2016 06:22
category: "Discussion"
author: jerryflyguy
---
Does the Azteeg X5 V3 require both 24vdc as well as 5vdc via the usb ? Or does it just need the 12-24vdc?



To connect the Raspberry Pi, do I just need a small USB TO USB(micro) cable? (Micro end goes into the x5)



 





**jerryflyguy**

---
---
**Eric Lien** *September 24, 2016 06:27*

The x5 generates it's own 5V.



Yeah the easiest way is to connect is the pi USB port via a short high quality cable into the Azteeg usb micro input.



Power the Pi via a (2A+) wall adapter.


---
**Eric Lien** *September 24, 2016 06:30*

Put the jumper so the x5 powers via its own onboard 5V, not the Pi usb. That way you always have the ability to run the printer via SD/LCD even if the PI is shutoff.


---
**jerryflyguy** *September 24, 2016 06:34*

**+Eric Lien** is that the "input selector" pins? Jumper the "int" w/ the middle pin?


---
**Eric Lien** *September 24, 2016 06:58*

INT is internal power.

USB is... Well you can guess.



Yes run the jumper to power via "INT"


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/gGi2T2ha8W1) &mdash; content and formatting may not be reliable*
