---
layout: post
title: "The Sinclair ZX10 of the printing age?"
date: April 14, 2014 01:39
category: "Discussion"
author: Mike Miller
---
The Sinclair ZX10 of the printing age?

(You whippersnappers might not know, but when Ataris and Commodores were going for $400 to $800, a nutbar from the UK brought out a personal computer for $100. [http://en.wikipedia.org/wiki/ZX81](http://en.wikipedia.org/wiki/ZX81))



The m3d making records on kickstarter seems like it's cut from the same cloth. One of the things I hadn't seen yet in the hype was a plain document explaining their goals and methods. There's definitely some interesting stuff here, but like the $100 Personal Computer, it's build volume is a bit smaller than I'm interested in at this time.)



Here's the whitepaper: [http://www.printm3d.com/M3D-The_Micro-How_We_Did_It.pdf](http://www.printm3d.com/M3D-The_Micro-How_We_Did_It.pdf)





**Mike Miller**

---
---
**Kalani Hausman** *July 12, 2014 08:23*

One of my fist computers was a self-built Timex/Sinclair ZX-81. I wrote about it with a comparison to the marvelous RasPis (another UK product) here: [http://www.stemulate.org/2013/03/17/raspberry-pi-brings-memories-zx81/](http://www.stemulate.org/2013/03/17/raspberry-pi-brings-memories-zx81/)


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/fQ6gjygSSQC) &mdash; content and formatting may not be reliable*
