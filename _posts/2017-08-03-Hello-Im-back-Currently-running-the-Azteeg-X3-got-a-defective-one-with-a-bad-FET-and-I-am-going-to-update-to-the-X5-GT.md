---
layout: post
title: "Hello, I'm back! Currently running the Azteeg X3, got a defective one with a bad FET and I am going to update to the X5 GT"
date: August 03, 2017 22:43
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hello, I'm back! Currently running the Azteeg X3, got a defective one with a bad FET and I am going to update to the X5 GT. What drivers to you guys recommend, looking for maximum print quality at 80mm/s to 120mm/s on a Spyder V2 with a Chimera.



Ordering tomorrow, got a huge 3D hubs order (7KG!) and need to get printing ASAP.



I was thinking about the Bigfoot BSD2660 or the SD8825



Let me know what you think, thanks!





**Stefano Pagani (Stef_FPV)**

---
---
**Oliver Seiler** *August 04, 2017 07:39*

Sorry, can't help you with your drivers (I moved to the DuetWifi and can highly recommend it), but it's good to see another Eustathios on 3DHubs!




---
**jerryflyguy** *August 04, 2017 20:25*

I'm using the Bigfoot drivers (BSD2660) which have been good so far. I made the switch over earlier this summer, haven't been using it a whole lot since (just how my summer is) but like it so far!


---
**Daniel F** *August 05, 2017 11:40*

Raps128


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/7LtuERobFxa) &mdash; content and formatting may not be reliable*
