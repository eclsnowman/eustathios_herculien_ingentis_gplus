---
layout: post
title: "To those that have started their Herculien build, how close are you to being done?"
date: March 02, 2015 02:07
category: "Discussion"
author: Gus Montoya
---
To those that have started their Herculien build, how close are you to being done? It's exciting to see some running builds. I'm still waiting to start, so I keep happy viewing others builds. Also I'm self learning solidworks. What a powerful and yet complicated software. I hope I grasp it sooner then later.





**Gus Montoya**

---
---
**Dat Chu** *March 02, 2015 02:16*

Quite far from being done. I am working around a baby schedule so ....


---
**James Rivera** *March 02, 2015 02:57*

I wish I had SolidWorks (or Autocad), but I simply do not have $4k+ to blow on this hobby. ﻿


---
**Derek Schuetz** *March 02, 2015 05:00*

I'm just waiting on silicone bed and just have to hook up electronics very close to done


---
**Bruce Lunde** *March 02, 2015 15:02*

I've got lots of parts yet to arrive, and I need the weather to change to get in the shop to start the build.


---
**Gus Montoya** *March 02, 2015 22:07*

**+James Rivera** I can hook you up with solidworks, message me personally. **+Bruce Lunde**  Yeah feel for you. Your getting the winter of your life time. **+Derek Schuetz**  Oh man, that must feel so good. :)


---
**Derek Schuetz** *March 02, 2015 23:43*

**+Gus Montoya** do you have anymore solid works hook ups?


---
**Bruce Lunde** *March 04, 2015 05:06*

Finally got to go to the shop tonight, but next three nights will be too cold...sigh..waiting for warmer weather.


---
**Gus Montoya** *March 04, 2015 05:27*

**+Bruce Lunde**  Your post was beautiful. Weather is horrible, might help to pry to the 3d gods lol


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/CYXVtQuNMVQ) &mdash; content and formatting may not be reliable*
