---
layout: post
title: "Looking through the documentation, I can't find anything on the length of the bowden tube"
date: September 07, 2015 14:35
category: "Discussion"
author: Rick Sollie
---
Looking through the documentation, I can't find anything on the length of the bowden tube.  Can someone supply the length?





**Rick Sollie**

---
---
**Eric Lien** *September 07, 2015 15:03*

Mine is 900mm on my Eustathios. It gives a nice gentle arc reaching to all the corners.


---
**Eric Lien** *September 07, 2015 15:12*

Also I use a short length of coat hanger wire around 2/3 of the way UP the arc to give it structure and keep it vertical. The wire fits down into the corner vertical extrusion center hole and pivots in the hole. I bent a tiny feature into the wire so it doesn't fall down too far into the extrusion.


---
**Eric Lien** *September 07, 2015 15:14*

I use loose Velcro strips to bundle the wires, Bowden tube and hanger-wire so the stay together, but don't fight each other or make the bundle too stiff.﻿


---
**Zane Baird** *September 07, 2015 21:44*

I am currently using a piece of 1/4" OD, 1/8" PE tubing to keep the arc in the tubing and wires. I've wrapped it all together with spiral cable wrap. It has been working great so far. It keeps a very clean arc, and it slides right into the t-slot of the 20x20mm extrusion: [http://imgur.com/a/NmeVA](http://imgur.com/a/NmeVA)



If it looks strange, that's because it is... The 30mm fan on my E3D died and I improvised with a 4020 blower. 



**+Eric Lien** What are the general ranges for the retraction speed and distance on your Herculien (Bondtech and Hercustruder if you know off the top of your head)?  I'm currently using 6.5mm @50mm/s and it's not quite enough. I'm wondering if I need to address the hysteresis before going to longer retractions (probably...)





 


---
**Eric Lien** *September 07, 2015 22:38*

**+Zane Baird** I use 80mm/s retract speed. And length is filament dependant. PETG is almost 8mm, and PLA around 7mm, and ABS is around 6.5mm. But again those all vary depending on my temps, speeds, brand, and color (pigments have an effect).


---
**Zane Baird** *September 07, 2015 22:50*

**+Eric Lien**  I was a bit hesitant about going that fast with retraction. I'll experiment with some higher speeds. What is your extruder acceleration? I'm printing with the E3D Volcano and it's been a bit more difficult to dial in that what I've been used to before. PLA in particular has been giving me problems as increasing retraction has led to the hotend jamming. ABS prints are coming out great though. 


---
**Rick Sollie** *September 07, 2015 23:44*

**+Eric Lien**

 Thanks for the info!


---
**Eric Lien** *September 07, 2015 23:44*

**+Zane Baird**​ I have found going a little hotter can help lower retracts. It makes it so there is less back pressure to overcome. It sounds counter intuitive... But give it a whirl.﻿



See this post: [https://plus.google.com/+EricLiensMind/posts/QNugHpSXEwk](https://plus.google.com/+EricLiensMind/posts/QNugHpSXEwk)


---
**Zane Baird** *September 07, 2015 23:49*

**+Eric Lien** I've found the same to be true, except for PLA. Expansion in the heat brake gives me more troubles than it's worth. I definitely prefer printing Nylon and PETg, now that I have some experience on how to dial things in. 


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/fPHHq5ccKfL) &mdash; content and formatting may not be reliable*
