---
layout: post
title: "So we have over 1000+ Members in our fun little group now"
date: November 28, 2016 15:29
category: "Discussion"
author: Eric Lien
---
So we have over 1000+ Members in our fun little group now. Which I guess means it is not a little group any more. With that in mind I wanted to do something I have thought about for a long time. I want to get a list going of all the builders. Some times I lose track of just how many people have actually built one of these style printers. 



If you have some time and would be willing to add your build to the list I would appreciate it. And if you know someone how has built one, but is not a part of the group, can you add them or poke them to add theirs to the list.



The list includes a spot for Other, so if you made a mod/variant please add it as well. And if you have links to your modded files (github, thingiverse, youmagine, etc) please add them at the end. I am hoping this can be a neat resource to see the spread and scope of our fun little group :)



[https://docs.google.com/spreadsheets/d/1MvEpomn6QGNBD2iqSPwBLvhStv1acxAJ1NK_ktYEiOA/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1MvEpomn6QGNBD2iqSPwBLvhStv1acxAJ1NK_ktYEiOA/edit?usp=sharing)﻿





**Eric Lien**

---
---
**Matt Miller** *November 28, 2016 16:22*

updated!



can this be stickied?


---
**Eric Lien** *November 28, 2016 16:31*

**+Matt Miller** Thanks man. I appreciate it. And I am glad your "Boxy The Robot" links are included there too. I think others will find them useful. You made some stout upgrades, and there are those among us that like things "Built Like A Brick Sh!thouse" :)


---
**Matt Miller** *November 28, 2016 16:41*

It started as a thought experiment and grew from there...



So much of it was drawn in inventor fusion.  I want to refactor the entire thing in fusion360 and make it completely parametric and 100% compatible with herculien (rod spacing is slightly different).



#someday


---
**Sven Eric Nielsen** *November 28, 2016 18:16*

**+Eric Lien**​

Even though I don't own such a printer (and actually I don't plan to build one) I think it's an impressive example for a open source work and this brings it to the next step. 

This printer is a very interesting and nice evolution step of the reprap printer idea and I really like to see you and all the others continuously working on it to improve it. 



Well done everybody! 


---
**Roland Barenbrug** *November 28, 2016 19:24*

Updated


---
**Stefano Pagani (Stef_FPV)** *December 07, 2016 04:24*

**+Matt Miller** that would be amazing I'm going to start on that soon for the spider V2 and make some changes with the mounting of the electronics and bed. I want to have a top removable panel and he boards mounted to the frame. And Walters bed design ( or something like it)


---
**Stefano Pagani (Stef_FPV)** *December 07, 2016 17:33*

**+Matt Miller** I've already started :) Ill invite you to the project, but the fusion servers went down :( I'm gonna try to make a 'V3', with more curves and and nicer design with build in cable management. My idea is with a bottom hatch for the electronics, the adjustable bed design the **+Walter Hsiao** made that Eric talked about in the stream. Carriages the have a part thats always on the rods and the hotend attaches to. Anyone have any more ideas?


---
**Eric Lien** *December 07, 2016 22:14*

**+Stefano Pagani** I will love to see what you come up with. That's one of the best things about our community is all the people putting on their own spin and making the printers their own.


---
**Bruce Lunde** *December 20, 2016 16:00*

updated.


---
**Batbold Jamts** *January 11, 2017 18:33*

Hello,i'm from germany. can you buy Spider v2 as Kit. Thank you


---
**Eric Lien** *January 11, 2017 21:04*

No, it's not a commercially available printer. But you can source all the components via the BOM to build one.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/Dz2h22GHX4m) &mdash; content and formatting may not be reliable*
