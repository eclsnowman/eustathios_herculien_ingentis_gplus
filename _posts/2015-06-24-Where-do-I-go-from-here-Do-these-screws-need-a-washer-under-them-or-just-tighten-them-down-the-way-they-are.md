---
layout: post
title: "Where do I go from here? Do these screws need a washer under them or just tighten them down the way they are?"
date: June 24, 2015 21:36
category: "Discussion"
author: Brandon Cramer
---
Where do I go from here? Do these screws need a washer under them or just tighten them down the way they are? Also what do I need the shims for? 

![images/db950cac60148a8f0075f10f01be0a5e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/db950cac60148a8f0075f10f01be0a5e.jpeg)



**Brandon Cramer**

---
---
**Eric Lien** *June 24, 2015 21:51*

The bolts should be m5x10mm and go on the inside of the tslot on the other side of the extrusion. The through hole is to allow an Allen wrench to pass through to the socket head of the bolt to tighten.



The 10mm ID shim washers are to go between the corner pulley and corner bearings since there is no shoulder on the flat side of the pulley. So the shim washer is a very thin washer so the pulley face gets spaced off the bearing just slightly to avoid rubbing.


---
**Eric Lien** *June 24, 2015 21:54*

Hear is a picture from the HercuLien documentation showing the button head sliding inside the extrusion: [https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Photos/IMG_20150214_123942.jpg?raw=true](https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Photos/IMG_20150214_123942.jpg?raw=true)


---
**Vic Catalasan** *June 24, 2015 22:23*

Cute that is a nice looking dogg crib!


---
**Walter Hsiao** *June 25, 2015 02:10*

I'm pretty sure you have to remove the cute dog before proceeding :)  If you haven't installed the eDrawings viewer (it's free) to open Eric's Solidworks model (SLDASM file), you might want to give it a try.  You can look around and see where every part goes, down to the screw, washer, and t-nut, the amount of detail is amazing.  I found it useful to go through sections of the printer, selecting and hiding parts until you have almost nothing left (it's also a good way to see exactly how many t-nuts to put in each extrusion).  There's a parts tree on the right, which can be used to search for parts by name when you don't know where a part belongs.



[http://www.edrawingsviewer.com/ed/download.htm](http://www.edrawingsviewer.com/ed/download.htm)


---
**Brandon Cramer** *June 25, 2015 16:34*

**+Eric Lien** Thanks. I didn't think that was right, but I forgot about the screw sliding down in the tslot. I now understand about the two holes in the extrusion so I can tighten up the screws once they are in place. 



**+Walter Hsiao** edrawings viewer is totally new to me. So are we talking about the components option. That seems like it will be helpful. 



Now if I just had time to work on it.....



**+Ashley Webster** That print took 10 years to complete. Looks good though.... :)


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/XDDna9Zs6Kz) &mdash; content and formatting may not be reliable*
