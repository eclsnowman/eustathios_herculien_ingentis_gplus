---
layout: post
title: "What type of material are people printing the Herculien?"
date: March 18, 2015 14:38
category: "Discussion"
author: Jeff T
---
What type of material are people printing the Herculien? Because of the enclosure it’s most likely ABS for the high glass transition point but what infill % are people using?





**Jeff T**

---
---
**Владислав Зимин** *March 18, 2015 14:57*

Abs 100%


---
**Eric Lien** *March 18, 2015 16:57*

I tend to use 3 or 4 perimeters depending on the parts strength needs, 20 to 30% infill in abs, 0.164 layers (which gives an even number of z steps for the HercuLien).﻿



More than that tends to be overkill. I don't usually go more than 40% infill. Perimeters provide far more strength than infill.


---
*Imported from [Google+](https://plus.google.com/103527135201379694785/posts/Q8mVPzCJWFA) &mdash; content and formatting may not be reliable*
