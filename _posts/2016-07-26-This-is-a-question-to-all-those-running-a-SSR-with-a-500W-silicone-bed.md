---
layout: post
title: "This is a question to all those running a SSR with a 500W silicone bed"
date: July 26, 2016 01:10
category: "Discussion"
author: Sean B
---
This is a question to all those running a SSR with a 500W silicone bed.  Do you find that the SSR heats up enough to warrant a heatsink?  



Also, do you find that your bed temps are accurate?  Would it make sense to mount a thermistor directly into the aluminum build plate?





**Sean B**

---
---
**Eric Lien** *July 26, 2016 01:27*

If you used the SSR specified in the BOM heat will not be an issue. Those cheap fotek SSR are another issue. The fotek SSR is over spec's and under built. But a genuine omron SSR will serve you very well for years to come.



A thermistor directly against or (even better) potted into a recess in the heated bed is ideal. But I have been using my heated bed for a long time with the thermistor in the pad and it works well too.


---
**Ben Delarre** *July 26, 2016 01:42*

I can confirm the ssr recommended by Eric is great. My cheap fotek ssr wouldn't even heat up the bed. 


---
**Daniel F** *July 26, 2016 06:45*

My Omron SSR stays quite cool. It's screwed to the frame which acts as an. additional heat sink.


---
**Michaël Memeteau** *July 26, 2016 19:00*

Second that... Had to replace my first SSR that was a cheap Fotek one!


---
**Sean B** *July 27, 2016 01:05*

Damn... admittedly I did go the cheap route and purchased a no name SSR from Robotdigg.  I'll probably use it to get me up and running (if it works) and substitute an omron down the road.


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/Cyq7ogVAawX) &mdash; content and formatting may not be reliable*
