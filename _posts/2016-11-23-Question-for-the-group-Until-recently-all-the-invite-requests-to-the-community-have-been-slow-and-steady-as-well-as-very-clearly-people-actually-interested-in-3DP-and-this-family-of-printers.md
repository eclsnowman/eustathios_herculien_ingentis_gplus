---
layout: post
title: "Question for the group. Until recently all the invite requests to the community have been slow and steady, as well as very clearly people actually interested in 3DP and this family of printers"
date: November 23, 2016 02:33
category: "Discussion"
author: Eric Lien
---
Question for the group. Until recently all the invite requests to the community have been slow and steady, as well as very clearly people actually interested in 3DP and this family of printers. But in the last month I have been seeing 30+ a day requests for joining. Most are Middle Eastern or Asian profiles with zero posts, or posts with nothing to do with anything maker at all. I like to be open and inviting... But also want to avoid spam posts or fake community member counts.



Anyone who is a moderator for other invite based communities... How do you handle it? It is almost becoming a part time job to administrator the requests because I pull up the profile of each requested invitee. I don't want to accidentally reject a request by someone interested in joining that just hasn't historically/actively posted about printing. And I don't want to reject someone just because they usually post in their native language because there might be something lost in translation and I mistakenly see it as potential spammers. But I also want to keep the community focused and clean.



Any suggestions from the community on how you think this should be handled is welcome.





**Eric Lien**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 23, 2016 02:53*

Ok, so the 3DP community is open (not invite based).  We get a lot of spam.  Here's the trend/ most common qualifying criteria:

The account is blank, or has many unrelated spamlike posts of the same thing.  To paraphrase what you said, the account has not made any contribution in general other than reposting youtube or other site links.  However, since we're not filtering entering users, just flagged spam posts, I can see your concern about deferring non-malicious accounts.  I also think something like 20% of registered accounts fit the "throwaway account" criteria but are dormant and don't post spam.

I like the google groups setup which is to moderate the first post. Doesn't seem to be a way to do that here.

**+ThantiK** want to rant for a bit?


---
**Ray Kholodovsky (Cohesion3D)** *November 23, 2016 02:55*

Also, congrats are in order.  I think what you described is a milestone.


---
**ThantiK** *November 23, 2016 03:23*

If it's indian, arabic, hindu, etc - I auto ban.  I don't even look at the content any more -- I used to.  G+ is not a popular social site in those areas, and I haven't seen a single legitimate post from those 3 languages at all.  If they don't have a content picture, and their profile is basically blank...I simply don't trust it and will ban.  I actually go through the 3D printing members page and just randomly ban anything that looks even slightly suspicious.  Nobody's complained so far.  If they're legitimate, they'll message (or try to message) one of the moderators.  It's getting really bad on the 3D Printing side too.



G+ doesn't give us shit for moderation tools, so there's nothing much else that we can do about it. :(



German names, I usually end up approving.  Names with pictures I check.  People with posts, I'm almost guaranteed to approve, etc.  I know it's super discriminatory, but in the early days of the 3D printing community, I actually had teachers bitch me out hardcore because their kids from school went on the page only to find porn that wasn't quickly removed.  I'd rather be harsh, and have false positives, than be too lenient, and have people not want to participate because it looks like a garbage dump.


---
**Eric Czeladka (corethan)** *November 23, 2016 12:15*

Hi everobody. I'm a french geek who is interested In 3d. I found your community and decided to follow it because it is an important and rich source of informations. I don't post. I only read the post. I don't want to bother you. If you think i don't have to stay because i don't have the kind of profile which you are looking After... Don't hesitate to mp me.

Best regards.


---
**Eric Lien** *November 23, 2016 13:18*

**+Eric Czeladka** a case like yours is exactly why I try so hard to get it right.


---
**Luis Pacheco** *November 23, 2016 13:40*

I will say that this has been one of the better G+ communities I've been a part of. I've joined a few others and it's been a cluster of spam and posts that don't really help the community. I have been meaning to say that on here, this community has been and still is one of the cleanest and most helpful ones I've seen! **+Eric Lien**


---
**Eric Lien** *November 23, 2016 14:04*

**+Luis Pacheco** thanks, that means a lot.


---
**Jo Miller** *November 24, 2016 00:00*

this is one of the best communities on google.  




---
**Eric Lien** *November 24, 2016 01:14*

**+Jo Miller** it is because of all our amazing community members :)


---
**Pete LaDuke** *November 24, 2016 03:41*

I'm one of the others who seldomly posts but do really enjoy this G+ Community.  I am in the process of building an Eustatios printer partially because of it.   Keep up the great work, believe it or not, it is noticed.   Thanks again for your efforts.


---
**Eric Lien** *November 24, 2016 04:05*

Thanks **+Pete LaDuke**​, it means a lot to me and the other hard working mods and members. I am excited to hear we have another printer in the works. I know you said you don't post much. But while you build please always feel free to ask questions here if you run into problems, and share your progress if you can. The build posts are my favorite, and it helps motivate others to know feel more confident to take on the build.



Also as I mentioned in regards to questions, if you ask... The replies people post help you but also likely help others who might have been afraid to ask. The rising tide of our joint knowledge raises all the makers boats :)


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/SqiaqVEmPJr) &mdash; content and formatting may not be reliable*
