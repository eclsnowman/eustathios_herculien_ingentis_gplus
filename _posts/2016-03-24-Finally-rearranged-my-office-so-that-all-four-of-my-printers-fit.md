---
layout: post
title: "Finally rearranged my office so that all four of my printers fit"
date: March 24, 2016 13:02
category: "Discussion"
author: Eric Lien
---
Finally rearranged my office so that all four of my printers fit. Until recently I had my Eustathios in the basement and hadn't printed with it in months until taking it out to MRRF. But I am glad I made room. I forgot how much fun this printer is.



Printed the **+Whosa whatsis**​ Arc Gimbal he just posted to: [https://www.youmagine.com/designs/arc-gimbal](https://www.youmagine.com/designs/arc-gimbal)



Great model, my kids were facinated by the motion.





**Eric Lien**

---
---
**Ben Delarre** *March 24, 2016 16:58*

I printed this myself last night in red esun petg. Almost perfect,  the outer rings all popped loose with a bit of twisting but the innermost one the cone snapped. Still works but not as smooth as I'd like. 



Any tips on how to tune for this kind of thing? Reduce extrusion multiplier? (already kinda low tbh) 


---
**Eric Lien** *March 24, 2016 19:28*

**+Ben Delarre** I just used my default PLA settings. But since there is a very low step-over angle created by the radius of the edge of the rings it will be prone to lifting at those edges. I used PEI on the bed so it sticks pretty well. Plus I added support at those edges to help hold them down (not sure that actually helped though). I think the big thing is make sure you run at a speed where cooling can keep up to limit curling and I think you will be set.


---
**Ben Delarre** *March 24, 2016 20:25*

Hmm don't think it was a bed curling issue, the inner rings cone stuck to the outer ring a little too well. You could be right on cooling though,  and I know petg is sometimes a little hard to remove support from since it bonds to itself so well. Maybe not the best material for this type of print in place object. 


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/8Xi3kAhNdi5) &mdash; content and formatting may not be reliable*
