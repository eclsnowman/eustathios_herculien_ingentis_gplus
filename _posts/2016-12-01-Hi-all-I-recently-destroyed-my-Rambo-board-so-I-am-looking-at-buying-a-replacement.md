---
layout: post
title: "Hi all, I recently destroyed my Rambo board so I am looking at buying a replacement"
date: December 01, 2016 09:44
category: "Discussion"
author: Adam Morgan
---
Hi all,



I recently destroyed my Rambo board so I am looking at buying a replacement. I have my eyes fixed on the Azteeg X5 GT which is due out next week. I just wanted to get some opinions on what stepper drivers to go for? 



My Rambo currently has 1/16, but my printer bot has 1/32 microstepping but I have seen the SD6128 stepper driver which gives 1/128 microstepping.



I just wanted to find out if people think its worth going to 1/128 or whether just to get 1/32?



Cheers!





**Adam Morgan**

---
---
**Eric Lien** *December 01, 2016 14:12*

I am loving the SD6128 which recently got installed on my Delta. They are nearly silent, though for the extruder they are overkill because if you use the Bondtech extruder because 1/128 microstepping makes steps/mm overkill because it is already gear reduced.



The SD5984 is another great option. And likely what I would use for the geared extruder. Similarly the Z axis because of belt reduction and the lead screws doesn't need any more microstepping. In fact I only run 1/8 microstepping on my HercuLien and Eustathios because I always run my layer heights as an even multiple of full steps anyway (see optimal layer heights calculator here: [http://prusaprinters.org/calculator/](http://prusaprinters.org/calculator/))

[prusaprinters.org - RepRap Calculator - Prusa Printers](http://prusaprinters.org/calculator/)


---
**Adam Morgan** *December 01, 2016 15:07*

ah wicked, i will go for the SD6128 then, do you know if you can alter the microsteps on each stepper driver? i.e if i but 5 SD6128s then set 2 to 1/128 and then reduce the rest? Only wondering because I only got the option or having all of them in one type when buying the board.


---
**Eric Lien** *December 01, 2016 15:14*

Yes each driver has its own set of jumpers for microstepping


---
**Eric Lien** *December 01, 2016 15:18*

I will say I have heard great things about the BSD2660 divers too (using the trinamic 2660). **+Roy Cortes**​ you have any input on the 2660 drivers for a printer application?


---
**Ryan Carlyle** *December 01, 2016 15:54*

The Duet Wifi has 2660s, they're nice. 



With 6128s, you will need to consider the steps pulse generation frequency limit of your controller. Even 32bit controllers may have issues at 1/128 unless you're going slow. Off the top of my head, 50khz is a good limit for Smoothie (but don't quote me on that). So take your desired mm/s, and multiple by your planned steps/mm, and see if that puts you over 50,000 steps/sec. If so, drop the microstepping level. 


---
**Adam Morgan** *December 01, 2016 15:59*

well i dont really have a desired mm/s yet. i kinda burned out my board before i really pushed the limits of its speed so not sure waht to aim for. Essentially I was looking for a board with the potential to grow to save me money long term.



The board im looking at is: ([http://www.panucatt.com/azteeg_X5_GT_reprap_3d_printer_controller_p/ax5gt.htm](http://www.panucatt.com/azteeg_X5_GT_reprap_3d_printer_controller_p/ax5gt.htm)) it comes with the SD6128 which is why I was considering it. 



Otherwise can someone recommend a good 32bit board? I have read a lot of people just going with the Smoothieboard, but there was something that put me off (cant remember what at the moment tho)

[panucatt.com - panucatt.com](http://www.panucatt.com/azteeg_X5_GT_reprap_3d_printer_controller_p/ax5gt.htm)


---
**Ryan Carlyle** *December 01, 2016 16:36*

My opinion, the Duet Wifi is the best controller on the market right now. The Azteeg x5 line is also good. 


---
**Adam Morgan** *December 01, 2016 16:55*

Just looking at the Duet Wifi and its a bloody impressive board. Very tempted with that. 



The only thing thats got me wondering is it used reprap Firmware, any advantages to using smoothieboards firmware vs reprap? I tried converting from Marlin to RepRap but didnt get very far, so curious how it runs.


---
**Ryan Carlyle** *December 01, 2016 17:24*

Smoothie and RepRapFirmware are both "modern" firmwares -- one firmware build for all printers, with a text config file to set up your specific printer. In my view they're pretty equivalent for ease of use. (Which is to say, vastly better than Marlin or any other Arduino IDE based firmware.)


---
*Imported from [Google+](https://plus.google.com/117208540149253709671/posts/V39ayVtRZZq) &mdash; content and formatting may not be reliable*
