---
layout: post
title: "Hi, it's much easier to get flanged self-lubricating bronze sleeve bushings from robotdigg for the xy blocks"
date: April 15, 2015 17:20
category: "Discussion"
author: Daithí Ó Corráin
---
Hi, it's much easier to get flanged self-lubricating bronze sleeve bushings from robotdigg for the xy blocks. Would they be suitable replacements for the sintered bushings in the BOM?





**Daithí Ó Corráin**

---
---
**Eric Lien** *April 15, 2015 17:36*

It is possible, but with printer tolerance you can likely get misalignment. You could print oversized and epoxy pot them in while on the rod to allow for proper alignment. But I would go with the self aligning kind myself.


---
**Jeff Kes** *April 15, 2015 17:38*

I believe that the ones Eric is using are self-aligning also as these are not. Most other printers do use these kind or the linear bearings.


---
**Daniel F** *April 15, 2015 17:44*

The self aligning bushings are not so easy to get outside the US. I also considered Chinese bearings but finally went for the ones in the BOM. Ordered mine from Lulzbot and they shipped from UK stock (i'm based in Switzerland).


---
**Isaac Arciaga** *April 15, 2015 22:29*

**+Daniel F** Igus has their alternative to the bronze self aligning bushings. [http://www.igus.com/wpck/3777/igubal_Gelenklager](http://www.igus.com/wpck/3777/igubal_Gelenklager) I was thinking about using them myself.


---
**Daithí Ó Corráin** *April 16, 2015 03:28*

Western Australia seems especially hard to source parts but there is an Igus distributor here. Thanks! How does the plastic compare to brass for performance, I'm thinking possibly higher friction? I was also looking at the Selfgraphite linear bearings: [http://www.robotdigg.com/product/175/Selfgraphite-8*12*30mm-Linear-Bearing](http://www.robotdigg.com/product/175/Selfgraphite-8*12*30mm-Linear-Bearing) Obviously would need a solution simlar to Eric's above but may be better performance-wise?


---
**Tim Rastall** *April 16, 2015 18:33*

The problem is they aren't self aligning, as **+Eric Lien**​ says the danger is they will misalign once installed. 


---
**Daithí Ó Corráin** *April 18, 2015 11:27*

The Igus spherical bearings are expensive. 10 bucks a piece, 8 total makes 80 dollars for them alone! I might redraw the XY block to fit the above graphite bearing but are they suitable for axial and radial motion? They're called linear bearings... Any idea would it be okay to encase the holes in that bearing?


---
*Imported from [Google+](https://plus.google.com/104923267981938346235/posts/RFFpsFqv4hy) &mdash; content and formatting may not be reliable*
