---
layout: post
title: "Completely and totally untouched. The delta could never come close to completing this"
date: October 13, 2014 22:33
category: "Show and Tell"
author: Mike Miller
---
Completely and totally untouched. The delta could never come close to completing this. It'll be even better when I throw active cooling on it. 



![images/bacfa2229343adafb0bbbd0c47e05191.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bacfa2229343adafb0bbbd0c47e05191.jpeg)
![images/4b1eb4355a149f36dbf3f19f01d7d050.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4b1eb4355a149f36dbf3f19f01d7d050.jpeg)

**Mike Miller**

---
---
**Eric Lien** *October 13, 2014 23:57*

Its hard to tell. How did the cylinder pillars come out. Looks a Tilly bit flat on one side from the back shot.



But great job. Not bad on the arch. I still have never perfected that part of the torched test.


---
**ThantiK** *October 14, 2014 00:03*

All the circles look a little flat on that side.  Check for loose pulleys/belts in that axis that travels across there (X?)


---
**Eric Lien** *October 14, 2014 01:08*

**+ThantiK** With my experience using composite bearings I do worry a little. The difference in the friction at rest (or slow speeds) to the friction in motion creates the slip/stick issues that **+Shauki Bagdadi** saw, and also what I experienced on my corexy. It is most apparent on circles because there are two direction changes per axis on a circle.  So that is 4 times there is a zero velocity when steppers need to break a static friction force. Any miss alignment in the axis seem to compound the issue because side loading increases the effect.



But I hope it's just belts, and that I am totally wrong. 


---
**Mike Miller** *October 14, 2014 01:15*

They're all over the place between 5.8 and 6.2 mm. Can one expect them to be round and 6mm on a fully tuned printer? 


---
**ThantiK** *October 14, 2014 01:16*

**+Mike Miller**, easily.  The UltiMaker uses this same gantry setup and achieves near perfect prints.


---
**Mike Miller** *October 14, 2014 01:43*

Well aren't THEY SPECIAL! :P



maybe after I opt to change bushings? Then again, I still have improvements to make. 


---
**ThantiK** *October 14, 2014 02:29*

Also, just noticed your Apple is a fire bender.


---
**Mike Miller** *October 14, 2014 03:49*

Only when chrome is running. Otherwise it's nice and cool. ;)


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/Bbku3DYkkr6) &mdash; content and formatting may not be reliable*
