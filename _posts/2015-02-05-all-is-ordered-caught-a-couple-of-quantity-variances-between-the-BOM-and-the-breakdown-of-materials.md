---
layout: post
title: "all is ordered! caught a couple of quantity variances between the BOM and the breakdown of materials"
date: February 05, 2015 20:09
category: "Show and Tell"
author: Derek Schuetz
---
all is ordered! caught a couple of quantity variances between the BOM and the breakdown of materials. now i just have to play the waiting game as all 800+ parts come from around the world. thanks for all the help!





**Derek Schuetz**

---
---
**Isaac Arciaga** *February 05, 2015 20:31*

Grats! I'm just about done myself with the Eustathios BoM. Now I can sleep better until everything arrives!


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/cKsNpwK1mNU) &mdash; content and formatting may not be reliable*
