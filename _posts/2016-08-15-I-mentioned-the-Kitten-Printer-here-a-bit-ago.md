---
layout: post
title: "I mentioned the Kitten Printer here a bit ago"
date: August 15, 2016 22:52
category: "Show and Tell"
author: Alexander Larson
---
I mentioned the Kitten Printer here a bit ago. Just got this kit in. Small form factor and well designed. The parts in the kit were printed on another Kitten.



I have no affiliation with the developer other than the fact that I ordered a kit for my students to assemble and use. 



[http://printerkitten.com/](http://printerkitten.com/) 

[https://github.com/woolfepr/Printer-Kitten](https://github.com/woolfepr/Printer-Kitten)











[http://imgur.com/a/tzkhy](http://imgur.com/a/tzkhy)









**Alexander Larson**

---
---
**Sébastien Plante** *August 15, 2016 23:02*

100mm x 100mm... and I always find my 170mm tooo small :o


---
**Alexander Larson** *August 15, 2016 23:08*

Agreed but the small bed helps for the intended use. This is in a school setting so it keeps print times down and machines are able to get more parts through if we constrain size a bit. We can work projects around the size.



The goal is to get like 12 of these on a rack for daily use and then a couple of larger format printers. (looking at Herculien) for big stuff. We have a Stratasys 1200SST as well but its hard to justify the cost of material on it.﻿


---
**Eric Lien** *August 16, 2016 03:12*

**+Alex Larson**​ so glad you decided to go with the kitten printer. It is a gorgeous piece of engineering, and the print quality I saw it produce out at MRRF was exceptional. 



Also glad to hear a HercuLien might be on the horizon. If you move forward building one let me know. I will give any help I can provide. So cool to see our awesome community working it's way into STEM programs like yours. It hopefully means the next generation of makers are on the way :)


---
**Mike Kelly (Mike Make)** *August 19, 2016 20:46*

I'm really curious how well it lasts over time


---
**Alexander Larson** *August 19, 2016 21:10*

Just curious do you think it will have challenges in the Long Haul?


---
**Eric Lien** *August 19, 2016 22:52*

It's Bowden and the forces are so low I see this printer having great service life for years to come. But that's just my 2¢


---
**Alexander Larson** *August 19, 2016 22:57*

That was my thought too. I was just curious to what the counterpoint would be.


---
**Mike Kelly (Mike Make)** *August 22, 2016 18:13*

Unless they changed something their linear motion is all handled by 3d printed components. I believe it was either PLA or PETg. Neither are traditionally used as plastic bushings, so I'm not really aware as to how well those hold up over time. As it was it's super rigid, but I feel like once the bushings wear down a bit the issues will start appearing.


---
**Alexander Larson** *August 22, 2016 18:30*

Not on the hot end. The XY travel there is on a misumi bearing the outside rails do use PLA printed bearings with misumi rods. The bearings will wear but that is the in design intent. The bearing surface will wear into the rail irregularities. The moving mass being as low as it is will also have minimal friction in total. I have not done any math on it vs the materials wear rates but I imagine even if there is an issue, drawing a replacement with a linear bearing wont be too difficult.


---
*Imported from [Google+](https://plus.google.com/117084726865589765120/posts/ePdL4kSq1J3) &mdash; content and formatting may not be reliable*
