---
layout: post
title: "You don't realize how big the herculien build surface is until you hold it..."
date: February 08, 2015 00:13
category: "Show and Tell"
author: Derek Schuetz
---
You don't realize how big the herculien build surface is until you hold it...

![images/e3441d51209d62c972652eca748f525c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e3441d51209d62c972652eca748f525c.jpeg)



**Derek Schuetz**

---
---
**Eric Lien** *February 08, 2015 01:49*

Looks like you got someone to CNC it for you? Looks great. I did mine by hand with a bunch of router passes and straight edges. What a mess that was.



FYI if you paint it like I did make sure to prime it first with a sealer primer. I took a shortcut and didn't prime it. That stuff sucks up paint like a sponge if you don't seal it.﻿


---
**Dat Chu** *February 08, 2015 03:39*

Totally forgot that I need this made. 


---
**Eric Lien** *February 08, 2015 04:35*

Something tells me an Ox could do this in it's sleep.


---
**Mikael Sjöberg** *February 08, 2015 05:59*

Looks really good ! And what a big area :) 


---
**Derek Schuetz** *February 08, 2015 07:46*

Ya I had it CNCed by a local shop along with my acrylic cut. I just sprayed a black ename on it but didn't seal it =/


---
**Eric Lien** *February 08, 2015 17:01*

Please note the offset pigtail. I don't know if I ever let people know to order this way from alirubber.﻿ That way the pigtail avoids the screw at the back holding the aluminum to the MDF.


---
**Eric Lien** *February 08, 2015 17:06*

[http://imgur.com/r3bQX0f](http://imgur.com/r3bQX0f)


---
**Derek Schuetz** *February 08, 2015 17:25*

you have to request that?


---
**Eric Lien** *February 08, 2015 18:17*

Yes. Otherwise it comes straight out the back. 


---
**Derek Schuetz** *February 08, 2015 18:30*

Well I hope I can edit my alirunber order 


---
**Eric Lien** *February 08, 2015 18:35*

Sorry. I asked them If I could make a PN for reference. But they never got back to me so I forgot.


---
**Eric Lien** *February 08, 2015 19:12*

Otherwise you will likely be able to sneak the pigtail over there without issue. Might just need a tiny bit of material removal at the corner of the low pocket.


---
**Daniel Salinas** *February 09, 2015 22:37*

I've been trying to get them to reply to me.  still no answer.


---
**Derek Schuetz** *February 09, 2015 22:48*

Ya they start at 4pm my time and I usually get 3-4 responses and then don't hear from then


---
**Daniel Salinas** *February 09, 2015 23:03*

No they haven't responded to me in a week. I've asked for updates on my quote


---
**Daniel Salinas** *February 09, 2015 23:27*

**+Eric Lien** do the thermistors go through the pad or are they just adhered to the underside of the pad?


---
**Daniel Salinas** *February 09, 2015 23:30*

**+Derek Schuetz** can you get your contact to CNC more of those mdf boards?  If so, I'd be interested in outsourcing it to him.


---
**Daniel Salinas** *February 09, 2015 23:34*

Sadly I don't own a CNC machine nor a router so I'm looking at doing this by hand and I don't want to do that.


---
**Eric Lien** *February 09, 2015 23:47*

**+Daniel Salinas** it was a royal mess when I did it by hand.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/KWhWzRM8z3P) &mdash; content and formatting may not be reliable*
