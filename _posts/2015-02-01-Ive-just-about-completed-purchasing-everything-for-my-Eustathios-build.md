---
layout: post
title: "I've just about completed purchasing everything for my Eustathios build"
date: February 01, 2015 07:21
category: "Discussion"
author: Isaac Arciaga
---
I've just about completed purchasing everything for my Eustathios build. What's left is the aluminum build/heat plate. Can someone point me the direction where I can have this custom cut to the shape of the current design? I plan on clipping a 300mmx300mm piece of borosilicate on top of it. What thickness aluminum should I go with that wouldn't sag under ABS temps but also not take an eternity for it to get to temp?



I'm in Southern Cali (LA/OC area) if anyone knows a local place to refer me to I would greatly appreciate it!





**Isaac Arciaga**

---
---
**Mike Thornbury** *February 01, 2015 10:40*

Aluminium is unlikely to sag at anything under 1200F



My plate is 3mm.


---
**Eric Lien** *February 01, 2015 14:05*

This is the hardest part to source. I use 1/8" plate. I lucked out and had a friend with a CNC. Perhaps an opportunity for someone in the OX group to make some money? **+Alex Lee**​​ or **+Brandon Satterfield**​​ what do you guys think?﻿


---
**Brandon Satterfield** *February 01, 2015 14:36*

Should have my second OX complete end of next week. Be used for something like this. Customers have to come first, so been working on it for a long time. 



Didn't get to break down the Herculien cuts yesterday.



Sorry **+Isaac Arciaga** could get you taken care of on the rods, had a small chop saw to do these hardened steel rods. It's down. 


---
**James Rivera** *February 01, 2015 16:50*

Not a PC right now to check, but [onlinemetals.com](http://onlinemetals.com) might work.


---
**Isaac Arciaga** *February 01, 2015 19:16*

**+Eric Lien** thank you. I checked out your Herc BoM and bookmarked the place you got it from. **+Brandon Satterfield** No problem! It was worth a shot to ask anyways :) Thanks for getting back to me so quickly. **+James Rivera** Thanks for the link. I will check them out!



I did find [www.industrialmetalsupply.com](http://www.industrialmetalsupply.com) It looks like they cut to order and do custom if u give a cut file. They have two locations local to me. I should be able to find out tomorrow what something like this will cost but finding someone with a CNC i'm guessing would be less painful to the wallet.


---
**Brandon Satterfield** *February 01, 2015 20:47*

Less painful would be to build your own sir...:-)


---
**Paul Sieradzki** *February 01, 2015 22:12*

I'm based in NYC and a lot of hackerspaces/makerspaces have laser cutters. AlphaOne Labs and Resistor have a decent member base, perhaps you could reach out to some people there (I'm not a member) and see if they'd be willing to leverage their membership for a couple of bucks to help a fellow out?


---
**Isaac Arciaga** *February 02, 2015 05:03*

At the moment this is the brick wall/road block. :(


---
**Mike Thornbury** *February 02, 2015 08:07*

FWIW, cutting 3mm / 1/8" aluminium plate is pretty easy with a circular saw, drop saw or table saw, with a stock 'wood' blade. Just give it a spray of lube, take it slow and steady.



I lent someone mt 10" drop saw last week and needed to cut some plate today, so ventured my Makita portable table saw -it worked fine, I used my circular saw in the past for extrusion, but you are better off with something easier to keep in a straight line.



I've got a 10" 110 tooth blade on the table saw that makes a beautiful job of alloy.



Judging by how easily it deals with 3mm, I would venture 5mm too, maybe even 6mm.


---
**Isaac Arciaga** *February 02, 2015 08:25*

**+Mike Thornbury** unfortunately I don't own those types of power tools. I'm not one who is confident with a circular saw :) I'm more than willing to pay someone for their time and materials for this one.


---
**Brandon Satterfield** *February 02, 2015 13:01*

Hey **+Alex Lee** you think your tuned in good enough? This guys in your state.


---
**Isaac Arciaga** *February 02, 2015 17:02*

**+Alex Lee** I can certainly source the sheet myself! IMS is a 30min drive for me. Are you in Southern California? I just submitted a quote at IMS for 6061. I should hear back soon.

 


---
**Isaac Arciaga** *February 02, 2015 18:34*

**+Alex Lee** Awesome! I'm in Cerritos. Will I need to purchase a sheet larger than the actual cut for this to work?


---
**Brandon Satterfield** *February 02, 2015 18:34*

Making!!!


---
**Isaac Arciaga** *February 02, 2015 20:35*

**+Alex Lee** I purchased 16"x17"x1/8" 6061-t4 aluminum from IMS in Irvine. I'll probably pick it up from their store this Weds/Thurs. I'm trying to figure out how to email you from here with no luck. (I'm new to G+).


---
**James Rivera** *February 02, 2015 21:39*

**+Isaac Arciaga** I think you can use Hangouts to send a message. It is remarkably non-obvious. Hover over the name, then select the little button at the bottom of the floating dialog (below the Add button).


---
**Isaac Arciaga** *February 02, 2015 21:44*

**+James Rivera** Thank you sir.


---
**Isaac Arciaga** *February 03, 2015 00:26*

**+Alex Lee** Likewise!


---
**Isaac Arciaga** *February 03, 2015 03:33*

**+Eric Lien** and **+Brandon Satterfield**  thanks a bunch for connecting me with **+Alex Lee** ! Brandon, I'm eyeballing the OX kit on your site :)


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/EZebhRoLMCK) &mdash; content and formatting may not be reliable*
