---
layout: post
title: "First parts arrived today, the printer is still running"
date: February 24, 2016 20:35
category: "Build Logs"
author: Maxime Favre
---
First parts arrived today, the printer is still running

![images/e14b59dad50cd06ae1b42eb35ae4fdfb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e14b59dad50cd06ae1b42eb35ae4fdfb.jpeg)



**Maxime Favre**

---
---
**Matthew Kelch** *February 24, 2016 21:15*

Interesting extruder, this is the first time I've seen one. Anyone used one before?


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2016 21:33*

**+Matthew Kelch** it's called Bondtech, it's fairly popular.  


---
**Ariel Yahni (UniKpty)** *February 24, 2016 22:08*

**+Ray Kholodovsky**​ is it as good as they say it it? 


---
**Ray Kholodovsky (Cohesion3D)** *February 24, 2016 22:11*

**+Ariel Yahni** No idea, don't have one.  But between Tom's video and some community chatter the general consensus seems to be "awesome, but overkill". 


---
**Alex Lee** *February 24, 2016 22:56*

**+Ariel Yahni** I have the Bondtech extruder running on all 4 of my machines and I'd have a hard time going back to a single geared extruder. When printing with a Volcano at 1mm layers, the Bondtech handles it with no skips.


---
**Eric Lien** *February 24, 2016 23:41*

**+Ariel Yahni**​ in my opinion, they are even better than people say. I have run them since the alpha test. All I can say is I have not jammed a Bondtech yet in over 40kg of printing on my 4 printers. So I guess if you asked me, I would never use anything else. The extruders are like a freaking metronome :)



﻿


---
**Alex Lee** *February 24, 2016 23:43*

**+Eric Lien** My extruder is so well tuned I can play Beethoven with it...


---
**Ariel Yahni (UniKpty)** *February 24, 2016 23:50*

How about retraction? I assume you all run them as bowden? 


---
**Alex Lee** *February 24, 2016 23:51*

**+Ariel Yahni** I have it running on a bowden and a direct drive, works like a champ either way.


---
**Ray Kholodovsky (Cohesion3D)** *February 25, 2016 00:50*

A rotary metronome... Now there's an idea!


---
**Ariel Yahni (UniKpty)** *February 25, 2016 01:24*

**+Ray Kholodovsky**​ I like that. Let's put it next to the PAH


---
**Ray Kholodovsky (Cohesion3D)** *February 25, 2016 01:41*

**+Ariel Yahni** all set: [https://vine.co/v/hvYIdvQeZpK](https://vine.co/v/hvYIdvQeZpK)


---
**Ariel Yahni (UniKpty)** *February 25, 2016 01:44*

OMGGGGg that's really cool


---
**Ray Kholodovsky (Cohesion3D)** *February 25, 2016 02:07*

For reference, the PAH: [https://plus.google.com/+ArielYahni/posts/V9oEgvijXfB](https://plus.google.com/+ArielYahni/posts/V9oEgvijXfB)


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/N273JvMAa9h) &mdash; content and formatting may not be reliable*
