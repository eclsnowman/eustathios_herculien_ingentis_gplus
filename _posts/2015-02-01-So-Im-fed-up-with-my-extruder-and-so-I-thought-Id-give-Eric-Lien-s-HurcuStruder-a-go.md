---
layout: post
title: "So, I'm fed up with my extruder and so I thought I'd give Eric Lien 's HurcuStruder a go"
date: February 01, 2015 03:53
category: "Discussion"
author: Erik Scott
---
So, I'm fed up with my extruder and so I thought I'd give **+Eric Lien**'s HurcuStruder a go. I'm a little wary of threading the main Teflon tube into a nut, so I wanted to find a way to use the push-lock solution that came with my e3d v6s. Eric's design is really nice and allows you to easily change the attachment method. So, I quickly CAD-ed up a new attachment piece. Thought others might like to try this option, so I uploaded it here: here: [https://www.dropbox.com/sh/0rifioj0v93qr86/AABfBk4VqVJRDbmDhZz_0M8Ca?dl=0](https://www.dropbox.com/sh/0rifioj0v93qr86/AABfBk4VqVJRDbmDhZz_0M8Ca?dl=0)



And now, for the obligatory stupid question: I'm looking to use a planetary geared stepper for the extruder as that's really going to give me the added benefit over my current extruder. Would people be willing to share which stepper they used and where they got it? There are a couple options out there that I've seen, but I'm not familiar with any of the suppliers. 

![images/e87db6a153f59e523f6751b204950dbe.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e87db6a153f59e523f6751b204950dbe.png)



**Erik Scott**

---
---
**Eric Lien** *February 01, 2015 05:08*

Here's what I use: [http://www.robotdigg.com/product/103/Nema17-40mm-Stepper-Gearmotor](http://www.robotdigg.com/product/103/Nema17-40mm-Stepper-Gearmotor)


---
**Eric Lien** *February 01, 2015 05:09*

I know others who like this one: [http://m.omc-stepperonline.com/item_detail/40-Gear-Ratio-5-1-Planetary-Gearbox-High-Torque-Nema-17-Stepper-17HS19-1684S-PG5#!detail](http://m.omc-stepperonline.com/item_detail/40-Gear-Ratio-5-1-Planetary-Gearbox-High-Torque-Nema-17-Stepper-17HS19-1684S-PG5#!detail)


---
**Erik Scott** *February 01, 2015 05:26*

Thanks for that! I'll have a look. Any think I should know about the hurcustruder before I go ahead and change something or do something totally stupid? I'm most concerned about idler force and how to dial that in. 


---
**Eric Lien** *February 01, 2015 05:39*

My best suggestion, is get a bondtech extruder. It is the best part of my printer. Martin is taking preorders for the net run. I loved my Hercustruder, gave my a lot of hassle free printing. But if you want totally hassle free, get his. I can't imaging printing with anything else now.


---
**Erik Scott** *February 01, 2015 05:41*

Didn't realize he was taking pre-orders again. Thanks, I'll look into it. I really liked the initial design. 


---
**Eric Lien** *February 01, 2015 05:43*

Nice thing is he sells hardware only kits. So if you got a geared stepper, and could print your own housing, you would only need the gears and bearings.


---
**Mike Thornbury** *February 01, 2015 10:42*

[http://www.omc-stepperonline.com/gear-ratio-51-planetary-gearbox-with-nema-17-stepper-motor-17hs130404spg5-p-140.html](http://www.omc-stepperonline.com/gear-ratio-51-planetary-gearbox-with-nema-17-stepper-motor-17hs130404spg5-p-140.html)


---
**Mike Thornbury** *February 01, 2015 10:45*

I'll second Eric on that - because I am in a crappy place, shipping-wise, Martin just sent me the housings and gears - arrived pretty smartly. Awesome kit. I have both the FDM and extruder printed versions and they are both great.


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/DA7Ex1XcaFL) &mdash; content and formatting may not be reliable*
