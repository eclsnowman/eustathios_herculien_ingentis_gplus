---
layout: post
title: "Hey everybody. I was wondering if the g3d smart lcd controllers work with the azteeg x3 v2"
date: June 21, 2014 23:36
category: "Discussion"
author: D Rob
---
Hey everybody. I was wondering if the g3d smart lcd controllers work with the azteeg x3 v2. Or what lcd controller is good for it?





**D Rob**

---
---
**ThantiK** *June 21, 2014 23:51*

Panucatt sells the ViKi LCD which is I2C - it's expensive but it's pretty much guaranteed to work.  They also sell normal LCDs so I'm guessing any will work, but might require a pinout change or something minor.


---
**Chet Wyatt** *June 22, 2014 00:27*

And I just ordered a Viki and X3 Pro from Panucatt Thursday night... I already have them. Cali to NY 2 days, now that's service :)


---
**Eric Lien** *June 22, 2014 01:02*

I saw a guy out at MRRF that had the smart LCD hooked up to the Azteeg x3 v1. He said all the pins are there... Just not well documented.


---
**D Rob** *June 22, 2014 01:19*

anyone got the x3 pro pinout? especially for ext1 and ext2


---
**Chet Wyatt** *June 22, 2014 04:09*

[http://files.panucatt.com/datasheets/x3pro_pins.pdf](http://files.panucatt.com/datasheets/x3pro_pins.pdf)


---
**D Rob** *June 22, 2014 04:31*

**+Chet Wyatt** Vielen dank! 


---
**Chet Wyatt** *June 22, 2014 11:48*

Kein Problem


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/8Fcr8xTPwVR) &mdash; content and formatting may not be reliable*
