---
layout: post
title: "I think this is the Alumimaker 2 design that won that Misumi contest a month ago"
date: March 21, 2015 20:49
category: "Show and Tell"
author: Jeff DeMaagd
---
I think this is the Alumimaker 2 design that won that Misumi contest a month ago.



![images/0dd9467af10454b41d64fc5c318e1297.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0dd9467af10454b41d64fc5c318e1297.jpeg)
![images/dae2a162959c1c55aeb1f8c5dc4dee26.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dae2a162959c1c55aeb1f8c5dc4dee26.jpeg)

**Jeff DeMaagd**

---
---
**Seth Messer** *March 21, 2015 21:26*

this is probably a naive statement by me, but it looks like they just grabbed the eustathios design..


---
**Gus Montoya** *March 21, 2015 23:14*

I don't like it, coexy never were at the top of the list in an engineering aspect.


---
**Jeff DeMaagd** *March 21, 2015 23:20*

This isn't a corexy machine.


---
**Gus Montoya** *March 21, 2015 23:22*

Oh? Hah even 4 eyes on a small smartphone can't see very well. 


---
**Mutley3D** *March 22, 2015 00:25*

Good lord, its the guts of a UM2 transplanted into an extrusion based frame lock stock and print head.

 So much for thinking out of the box ;)


---
**Isaac Arciaga** *March 22, 2015 01:17*

**+Seth Messer** It's actually the opposite. The Eust is derived from this particular design. **+Mutley3D** My thoughts exactly! But hey, it prob half the cost of the real deal heh.


---
**Sébastien Plante** *March 22, 2015 01:23*

woah, I just noticed that the Eustatios wasn't CoreXY... always think it was! 


---
**Mutley3D** *March 22, 2015 02:32*

**+Isaac Arciaga** No, i mean it IS Actually the full guts ripped out of a UM2. The print head appears to be an original spec item, check the base ali parts of the print head, these are not available apart from Ultimaker, also the belt clamps are UM2 original injection moulded pieces. They have even kept the black spacers on the ends of the shafts that position the pulleys, and used the LED strips lol. Id say this would cost the price of a UM2 + it's shipping cost + cost of extrusions.


---
**Mutley3D** *March 22, 2015 02:43*

Not sure I could do this and pass it off as my design in a competition.  I may be wrong though, happy to eat my hat :) Afterall they didnt use the UM2 fan assembly on the print head, and they did remount the motors for direct drive. Maybe that was the whole objective to transplant the UM2.


---
**Jeff DeMaagd** *March 22, 2015 02:49*

Yes, it does use a lot of UM2 parts, which seems a bit of an odd choice. The BOM for this machine was supposedly $1300. I expect UM2 BOM is less just because of economies of scale. Those extrusions & hardware to assemble the extrusion frame costs a lot more than the UM2 panels, but 6mm sandwich panel is harder to find, and it requires a CNC router to cut the panels.


---
**Mutley3D** *March 22, 2015 02:54*

yea and you dont get those parts unless you buy a UM2 lol, its bizzare. I cant see Ultimaker selling those parts as a partial kit either.


---
**Robert Burns** *March 22, 2015 04:06*

Its no more a rip from the UM than the rest of the designs from this group...kinda funny these stones being thrown.


---
**Mutley3D** *March 22, 2015 12:26*

**+Robert Burns**  actually not throwing stones, and it is quite a different rip from the rest, designs in this group are iterations. Throwing stones was not the intent.


---
**Eric Lien** *March 22, 2015 12:33*

Everyone play nice. It is well built, and someone without a printer could build one (I think, or at least get things functional to print remaining parts).



Nice looking printer.


---
**Mutley3D** *March 22, 2015 12:55*

Agreed its not bad looking, well presented. Apologies if my tone seemed harsh or if anyone was offended. Was simply stating what i could see. Creating an extrusion based equivalent of a UM2 is not a bad way to go, thats why i like this group as it is close to that theme


---
*Imported from [Google+](https://plus.google.com/+JeffDeMaagd/posts/DgiKSmfatqy) &mdash; content and formatting may not be reliable*
