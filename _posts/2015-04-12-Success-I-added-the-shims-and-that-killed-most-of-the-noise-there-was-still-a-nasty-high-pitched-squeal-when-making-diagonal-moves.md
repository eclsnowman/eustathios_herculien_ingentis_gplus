---
layout: post
title: "Success! I added the shims and that killed most of the noise, there was still a nasty high pitched squeal when making diagonal moves"
date: April 12, 2015 18:13
category: "Discussion"
author: Ben Delarre
---
Success!



I added the shims and that killed most of the noise, there was still a nasty high pitched squeal when making diagonal moves.



So I repeated my earlier alignment procedure. Moved the carriage to the middle, loosed both the bearing blocks in one corner, made sure they could move up and down and then slid the carriage into the corner and locked up the bearing blocks. Repeated for all corners.



After that things started running perfectly, no grinding noises no high pitched squeals, just stepper motor whine and belt noise.



Got to say I love the noise this thing makes when changing directions. :-)


**Video content missing for image https://lh3.googleusercontent.com/-jx-GMmP9Ubs/VSq1Tg_dsOI/AAAAAAAAEWk/L0LaCVSERWg/s0/VID_20150412_105317.mp4.gif**
![images/4f1ab1ee4bb0d87884a125d748b14e3c.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4f1ab1ee4bb0d87884a125d748b14e3c.gif)



**Ben Delarre**

---
---
**Eric Lien** *April 12, 2015 18:24*

Sounds good. Wait till you get this thing really cooking on a print with 150+ mm/s non-print moves. It is really a fun printer to watch at high speeds. So happy to see another printer join the family;)


---
**Eric Lien** *April 12, 2015 18:27*

The only tricky bit left is the bed. When you install it take time making sure everything moves smooth on the rods before mounting the lead screws nuts to the bed. There are several stacked tolerances so things might need a little finesse.



Great work.


---
**James Rivera** *April 12, 2015 18:31*

Are the shims printable parts?


---
**Eric Lien** *April 12, 2015 18:39*

**+James Rivera** they could be. But it is just a 10mm bearing shim. So I just bought them myself.


---
**Ben Delarre** *April 12, 2015 19:12*

**+Eric Lien**​ yeah I love watching it already.  I think the z axis might have to wait till after my move now sadly, still trying to disassemble the enclosure I built for my shapeoko! 


---
**Jason Perkes** *April 12, 2015 20:32*

**+Ben Delarre** These machines can be very hypnotic, especially when they sing!


---
**Ben Delarre** *April 12, 2015 20:55*

**+Jason Perkes** yeah, the full XY gantry is far more hypnotic than the X gantry with moving bed model on the office Prusa. Can't wait to get this one finished!


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/WTwgsGhH1ua) &mdash; content and formatting may not be reliable*
