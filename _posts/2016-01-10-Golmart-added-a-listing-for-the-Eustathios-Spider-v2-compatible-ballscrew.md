---
layout: post
title: "Golmart added a listing for the Eustathios Spider v2 compatible ballscrew"
date: January 10, 2016 10:54
category: "Deviations from Norm"
author: Walter Hsiao
---
Golmart added a listing for the Eustathios Spider v2 compatible ballscrew.  Hopefully I got the diagram right, if anyone sees anything wrong with it, please let me know and I'll try to get it fixed before someone orders one.



[http://www.aliexpress.com/store/product/Best-quality-1pc-Ball-screw-SFU1204-L425mm-1pc-RM1204-Ballscrew-Ballnut-standard-processing-for-CNC-BK10/920371_32592258963.html](http://www.aliexpress.com/store/product/Best-quality-1pc-Ball-screw-SFU1204-L425mm-1pc-RM1204-Ballscrew-Ballnut-standard-processing-for-CNC-BK10/920371_32592258963.html)﻿

![images/42fda95dd1ed6ebf5d3c2ae5c5e99da4.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/42fda95dd1ed6ebf5d3c2ae5c5e99da4.png)



**Walter Hsiao**

---
---
**Eric Lien** *January 10, 2016 13:31*

Thanks so much Walter. Time to get this added to the BOM and switch some mounts.


---
**Eric Lien** *January 10, 2016 13:44*

And this helps lower the build cost too.


---
**Joe Spanier** *January 10, 2016 16:07*

Wow that's a good price


---
**Jim Stone** *January 10, 2016 16:51*

Are both ends not 8mm...or one end pulley sized or something D: I can't remember I don't have it in front of me


---
**Eric Lien** *January 10, 2016 17:10*

This is for Eustathios, HercuLien would need a separate one.


---
**Jim Stone** *January 10, 2016 17:11*

Oh :( got all excited there lol


---
**Eric Lien** *January 10, 2016 17:33*

**+Jim Stone** but I will try to setup the HercuLien too. **+Walter Hsiao**​ can you PM me who you contacted to setup the one for Eustathios?


---
**Jim Stone** *January 10, 2016 19:00*

I would much appreciate this.



I got frustrated with Ali rubber so I went elsewhere.



And I went to misumi for my lead screw and nut because of the Ali rubber experience.



Now I have slop in my misumi piece and it hasn't even been used yet lol.



Yay for language barriers


---
**Joe Spanier** *January 10, 2016 21:02*

**+Jim Stone**​what issues did you have with Alirubber


---
**Walter Hsiao** *January 10, 2016 21:02*

**+Eric Lien** You're welcome!  I just contacted Cathy Li through the contact link in one of their Aliexpress postings (leftmost column, under service center) with the diagram and request, got a message saying they'll have it up later that day, then a message with the link soon after that.



As for AliRubber, my first attempt to contact them through the website was taking forever (it took a week to get the first response).  I ended up contacting daisyhuang@alirubber.com.cn directly (who has been mentioned here a few times).  That was much faster, took a day or two to get all the details sorted out, about four days to make it, and a week to ship via DHL.


---
**Jim Stone** *January 10, 2016 21:03*

Was unable to define if I was speaking to the right shop as it was my first time. That and they didn't seem too sure.



Just threw me off.



aliexpress for me as a first time user was confusing. if i had direct links thats is more helpful.


---
**Jake Shi** *January 11, 2016 21:40*

+Eric Lien I'm planning to build a eustathios spider v2 with ball screws. But I only find SFU1204/DFU1204 type like this one, which has a lead of 4mm instead of 2mm (equivalent to 0.2mm pitch). Is that going to be a big sacrifice over Z-direction resolution? Should I use 0.9degree stepper motor to compensate that? Thanks!


---
**Eric Lien** *January 11, 2016 22:50*

**+Shengjun Shi** no, you will be just fine... But 0.9deg steppers will definitely increase resolution.


---
**Daniel Moore** *January 18, 2019 09:26*

for a stock eustathios spider v2 build these are the correct dimensions for the ball screw?


---
**Eric Lien** *January 19, 2019 01:38*

Yes


---
**Daniel Moore** *January 19, 2019 02:31*

**+Eric Lien** sweet thank you about to place orders to build the eustathios spider v2

just crossing the t's and dotting the i's


---
**Eric Lien** *January 19, 2019 02:49*

**+Daniel Moore** When you place your order (X2 mind you since that is the price for one) tell them to make sure to orient the nut as shown in the picture. Otherwise you will need to reverse it. It is not impossible, but you need to print this to allow you to flip the ball nut without the balls falling out:



[myminifactory.com - 1204 Ball Nut Inverter](https://www.myminifactory.com/object/3d-print-1204-ball-nut-inverter-18548)


---
**Daniel Moore** *January 19, 2019 03:35*

**+Eric Lien** Awesome thank you for the tip i will make sure i put that in the notes


---
*Imported from [Google+](https://plus.google.com/+WalterHsiao/posts/P6729uvnK9N) &mdash; content and formatting may not be reliable*
