---
layout: post
title: "Originally shared by D Rob The Hydra with its shroudless direct bolt on fans"
date: November 22, 2014 07:24
category: "Show and Tell"
author: D Rob
---
<b>Originally shared by D Rob</b>



The Hydra with its shroudless direct bolt on fans. The fans just hit the base of the rod clamps on the x/y ends :/ changing print area to 12 x 11 ⅝. I'm not happy. The STL will be fixed by shaving off 2mm and I'll file these down so that I don't have to reprint and disassemble.



![images/dfec37945371f116c0a639dd5c0d27f1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dfec37945371f116c0a639dd5c0d27f1.jpeg)
![images/a8f534f50f32c79197e16f777800334c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a8f534f50f32c79197e16f777800334c.jpeg)
![images/5d5a7f82a6433b0dec865a82e8d14626.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5d5a7f82a6433b0dec865a82e8d14626.jpeg)
![images/573189c2ad47a20c624818c7edabd392.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/573189c2ad47a20c624818c7edabd392.jpeg)
![images/cb462c7a99a7751106900309a664f3e1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cb462c7a99a7751106900309a664f3e1.jpeg)
![images/e9bc7e4790b0384ed3c23a93affa81f7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e9bc7e4790b0384ed3c23a93affa81f7.jpeg)
![images/44c17c5ba5f13d9605d3499a1a94e9c4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/44c17c5ba5f13d9605d3499a1a94e9c4.jpeg)
![images/dff5dadbd9179bd620d478157360a93a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dff5dadbd9179bd620d478157360a93a.jpeg)

**D Rob**

---
---
**ThantiK** *January 06, 2015 22:49*

Hey **+D Rob**, how's this coming along by the way?  Very interested in knowing if you're running into the same problem that **+Tim Rastall** was having with non-straight rods and the rotation causing problems.


---
**D Rob** *January 06, 2015 22:52*

Haven't had time to work on it. I've moved and am starting college Monday. Hopefully soon though. I see no visible bend while rotating manually though.


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/fkTeaEHghZk) &mdash; content and formatting may not be reliable*
