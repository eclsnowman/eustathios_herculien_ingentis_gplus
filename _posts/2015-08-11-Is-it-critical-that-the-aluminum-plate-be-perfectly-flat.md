---
layout: post
title: "Is it critical that the aluminum plate be perfectly flat?"
date: August 11, 2015 16:43
category: "Discussion"
author: Rick Sollie
---
Is it critical that the aluminum plate be perfectly flat? I didn't check that it was flat upon receiving but after it was cut on the water jet I noticed that it has a slight bend in it.





**Rick Sollie**

---
---
**Chris Brent** *August 11, 2015 17:02*

Yes, you'll always have adhesion issues if it's not flat. Some firmware's can take multiple measurements and try and compensate for it while printing, but you'll never get prints with a flat base. And if you can see that it has a bend I'd guess it's too bent. 


---
**Rick Sollie** *August 11, 2015 17:07*

Bend is about 2mm. I was curious as the glass plate wouldn't flex and worst case would just be some cool spots on the glass.


---
**Eric Lien** *August 11, 2015 17:49*

If you clamp the glass to a bent plate when you heat it it can crack... Ask me how I know ;)



But 5 minutes, a rubber mallot, a quality straight edge and some 2x4 standoffs fixed mine. I was impressed with how quick this method fixed the crown.


---
**Rick Sollie** *August 11, 2015 17:52*

**+Eric Lien** I will try this. I like the idea of making my problems go away with a mallet ;)


---
**Isaac Arciaga** *August 11, 2015 17:52*

**+Rick Sollie** unless you chose cast aluminum like Mic6 or ATP-5, chances are you will have a bow to it. Rolled aluminum sheet like 6061 isn't as dimensionally stable. I initially had my bed made of 6061 aluminum sheet. The bow on it was way too much. I ended up having **+Jeff DeMaagd** water jet an ATP-5 bed for me. Maybe he can chime in.


---
**Igor Kolesnik** *August 11, 2015 18:57*

It shouldn'be a huge issue if you are going to use glass. Othewise you can try to straighten it


---
**Jeff DeMaagd** *August 11, 2015 19:47*

I had a couple extra made when I commissioned a set of plates in ATP-5 for a different project. Because it's cast and milled, it's already very flat and it won't distort with changes in temperature. I couldn't find a way to post pics to this thread, so please check my latest post on my own feed.





[https://plus.google.com/+JeffDeMaagd/posts/9WA1YLAjSKe](https://plus.google.com/+JeffDeMaagd/posts/9WA1YLAjSKe)


---
**Vic Catalasan** *August 11, 2015 21:11*

**+Rick Sollie** +1 on mallet


---
**Eric Lien** *August 11, 2015 21:22*

My mallet influenced 6061 has stayed flat for over a year now. I think peoples biggest issue with bowing on plates has more to do with thermodynamics. If the heater doesn't go to the edges then the center is hotter than the edge when heated. So thermal expansion dictates it will naturally crown. Also not letting the bed reach thermal equilibrium before printing means your bed will still be changing as you print. Lastly, insulation in the underside of the bed has done wonders for my dimensional stability of the bed.﻿


---
**Jeff DeMaagd** *August 12, 2015 03:59*

Well, when I had dimensional stability in mind, I meant different bed temps change how the bed is shaped. On my current machine, it's definitely repeatable at a given temp, but when switching materials that need a different temp, I have to adjust my Z a bit because the bed bowed slightly more or less.﻿ I hope to fix that on the machine I'm building.


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/M1RBTVVgBfu) &mdash; content and formatting may not be reliable*
