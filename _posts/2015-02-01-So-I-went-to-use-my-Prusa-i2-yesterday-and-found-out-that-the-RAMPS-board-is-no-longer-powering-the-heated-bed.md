---
layout: post
title: "So I went to use my Prusa i2 yesterday and found out that the RAMPS board is no longer powering the heated bed"
date: February 01, 2015 03:20
category: "Discussion"
author: Todd Kruger
---
So I went to use my Prusa i2 yesterday and found out that the RAMPS board is no longer powering the heated bed. I have plenty of replacement boards here but instead of replacing it I think it's about time I put my thoughts/ideas into action and build a Ingentis/HercuLien style printer.



So what is the largest size that people have tried with this design style? I'm looking at 1000mm*1000mm+ bed size with a fully heated enclosure using IR Ceramic Plates from a BGA Rework Station instead of a heated bed. Has anyone considered/tried this before? Seems that it should be a better way of doing it compared to the Hot Air heating system I have seen used on the Stratasys systems. Thoughts?





**Todd Kruger**

---
---
**Erik Scott** *February 01, 2015 03:41*

Don't know what you plan to print that's that large, but sure, I'd say go for it! Might want to beef up some of those rods (8mm rods for the carriage might be a bit thin). You'll definitely want to have a talk with **+Eric Lien**, as he seems to be the 3d printing guru around here. 


---
**Todd Kruger** *February 01, 2015 03:47*

I did do some basic calculations for the X & Y axis rods based on using the Kraken that i've got laying around. Could go with 10mm rods and was calculated to have a maximum bend of 0.05mm with a static load but not sure about when it is in motion. The more I think about it the bigger I want to make the new printer just to say I have done it.


---
**Tim Rastall** *February 01, 2015 05:23*

**+Todd Kruger** I dont think you'll have much luck scaling to that sort of size with Ultimaker mechanics (on which these bots are based). 

My experience of developing and building the Ingentis taught me the larger the xy span,  the more you will struggle with noticeable run-out on the shafts and that will ruin your print quality. For 1000mm bed,  you'll need shafts at least 1100mm long and they'd need to be at least 12mm in diameter (haven't got time to do the maths but definitely more than 10mm). These will be very hard to source with tolerances for straightness to be useful

So, one way to avoid this issue is to remove the shaft rotation and use fixed shafts or vslot instead.  I'm working in something that will scale much more readily but it's still an early prototype. . **+Oliver Schönrock**​ is running with a variation of the same idea as well but he's still on the drawing board I believe.

So, if your feeling adventurous you're welcome to join in our development of the idea.  Search the community for 'Procerus' to get an idea of what I'm on about.




---
**Eric Lien** *February 01, 2015 05:25*

I use 10mm on Herculien, but my span is nowhere near that size. I wonder if extrusions and rollers would make better cross bars at those lengths. Also the printer **+Tim Rastall**​ is working on might scale better than HercuLien or Eustathios. Lastly look at the work by **+Shauki B**​ on his quadrap. 



The problem with Ingentis, Eustathios, or Herculien style printer at that size is obtaining smooth rods with ample straightness across that span.


---
**Todd Kruger** *February 01, 2015 05:42*

Thanks for all the replys/info. At the moment the idea was to use fixed smooth rods with lead screws for the movement. I have used makerslide etc on routers before so I did consider that way instead of hardened shafts but from my experience the precision of extruded aluminium is worse then hardened rods. 


---
**Tim Rastall** *February 01, 2015 23:25*

**+Todd Kruger** ah,  cool.  I find I were going that large I'd be looking at linear slides or supported linear shafts like this: 

[http://www.automationtechnologiesinc.com/wp-content/uploads/2012/01/Supported_Linear_Shaft-1024x1024.jpg](http://www.automationtechnologiesinc.com/wp-content/uploads/2012/01/Supported_Linear_Shaft-1024x1024.jpg)

You'll be well positioned to succeed with this design if you're familiar with cnc builds as it sounds like you'll end up with a machine suitable for milling anyway. Have you considered motor sizes and what sort of linear system you'll use for the cross members?


---
**Todd Kruger** *February 02, 2015 02:11*

I have been considering using linear slides or similar for the outside X & Y axis and then smaller (12mm or so) hardened shafts for the part the carriage travels along. I'll have to do more calculations on it but that's the way I'm leaning atm. The major problem that I can see is keeping the weight down while keeping the rigidity up.


---
**Eric Lien** *February 02, 2015 02:47*

I wonder how a bowden will perform with that long of a path.


---
**Tim Rastall** *February 02, 2015 02:55*

**+Eric Lien** I'd say that given the likely mass of a 1m span x/y gantry, using direct drive will have little additional impact on the performance of the bot, particularly if it's begin driven by lead/ball screws.


---
**Todd Kruger** *February 02, 2015 02:59*

Only problem I see with direct drive is the fact that I want to get the heated build area up close to the glass transition temp of each material that I'm printing with so far example it would be close to 150c for Polycarbonate. I will have to work out an extruder that would work in that temperature range. A lot easier if running a bowden and keeping all the important parts outside the enclosure.


---
*Imported from [Google+](https://plus.google.com/+ToddKruger/posts/BzkLwEyPxYN) &mdash; content and formatting may not be reliable*
