---
layout: post
title: "My printer with kracken installed"
date: July 06, 2014 02:28
category: "Show and Tell"
author: Jim Squirrel
---
My printer with kracken installed. 



![images/35a440ec2d33f1d26cd82c33ff83d99a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/35a440ec2d33f1d26cd82c33ff83d99a.jpeg)
![images/1ca6d19aab5f753515c1ec03fe19d983.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1ca6d19aab5f753515c1ec03fe19d983.jpeg)

**Jim Squirrel**

---
---
**Jim Squirrel** *July 06, 2014 03:09*

I now have funds to finish my printer. I'm still on the fence on kraken or two 1.75 v6 e3d. For electronics i want to jump 32bit but the hotend is going to control which i choose. Also i need fasteners my prusa kit included all so Please give me suggestions for fastners. On ebay i found a seller selling kits of 3mm 4mm screws for rc cars utilmachines has a good selection but no bulk kit.  So suggestions please!


---
**Jim Squirrel** *July 06, 2014 03:20*

With this printer it's primary purpose it going to be printing prosthetic parts for the [http://enablingthefuture.org/](http://enablingthefuture.org/).


---
**Eric Lien** *July 06, 2014 03:34*

**+Jim Squirrel** mcmaster for fasteners. I had mixed results buying fasteners from aliexpress because 5 weeks is a long time to wait for $7 in bolts.


---
**Jim Squirrel** *July 06, 2014 03:51*

That's where i was going for the m8 threaded rod for Zstage. i also pondered on the post that **+Tim Rastall** about Syncromesh . i am a few

 pulleys shy of being belt ready.


---
**James Rivera** *July 06, 2014 03:52*

LOL!  Nice Kraken you have "installed" there. :)


---
**Joe Spanier** *July 06, 2014 04:36*

Or fastenal if you have one near you. That's where I've been sourcing all mine. 


---
**Jeremie Francois** *July 06, 2014 08:14*

Oh and it dual prints metal and glass at the same time, wow :D


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/DiHWDiTLbH9) &mdash; content and formatting may not be reliable*
