---
layout: post
title: "UPDATE: Roy has sent me a replacement board for free :) I really don't know whats so great about these Azteeg boards anymore"
date: December 05, 2017 23:38
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
UPDATE: Roy has sent me a replacement board for free :)



I really don't know whats so great about these Azteeg boards anymore.



I first got the Azteeg X3 and received a defective unit with the HBED always on. Numerous emails to the support line had no reply.



I wanted to give them another chance as I have heard many people love them, so I bought the X5 GT. Both the X and Z endstops trigger together, even with the stock config.



At this point, I will probably just buy a smoothie, but I can't afford one because of all these boards I bought.



It's the only thing holding my printer back right now.



I don't mean to trash the Azteeg brand that **+roy cortes** has created, but there is next to none in terms of customer support.



I will mention that he has been very generous to me in offering to replace a driver I blew up, but I have gotten no more communications from him.

 



If anyone knows how to contact him (other than email) please lmk, I want another shot on the X5GT before I switch.



![images/64e31b75910d6f40d111037f6d7c6f27.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/64e31b75910d6f40d111037f6d7c6f27.jpeg)



**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *December 06, 2017 00:04*

Without the hardware in front of me I cannot tell if it is hardware or configuration/wiring. I will say in all the boards I have bought or tested by Roy I have never had a single issue. But that is not to say you have not had a hardware issue. 



Have you tried using one of the other endstops... and just reconfigured smoothie config to use the Xmax as the Xmin (if you truly have pins that are shorting together). It is not a perfect solution, but knowing how to make that pin assignment change is important to learn. Then you can have roy take care of the hardware RMA. But it won't keep you from printing.


---
**Gus Montoya** *December 06, 2017 00:18*

Following...still looking at my options on mobo and features I want on my printer....


---
**Stefano Pagani (Stef_FPV)** *December 06, 2017 00:43*

**+Eric Lien** thanks for the advice, it is not the pins shorting together, nor the wiring for the end stops. I will try again with the config, will upload the herculien config as a test. All the pin assignments seemed fine, thats the weird part.


---
**Ryan Fiske** *December 06, 2017 01:52*

I want to vent here since I haven't ever found a good opportunity before to do so, but I too have experienced zero customer support from Panucatt. I bought an Azteeg X3 Mini v1.1 back in the day, shot some questions to them and got no response. Shot another question after that board had mysteriously died, once again no response. I replaced the board with the X3 mini v3 and it's still running on my Kossel, and once again I emailed them a question about pins. Not a peep. At this point I'll obviously run this thing and hopefully get plenty of life out of it, but I'm really disappointed in them for their lack of support and wouldn't likely recommend them to anyone looking to purchase a new 3d printer controller board.


---
**Stefano Pagani (Stef_FPV)** *December 06, 2017 02:02*

The problem I am getting with the endstops is that both Z and Y trigger Pin P1.25 (probe menu)




---
**Eric Lien** *December 06, 2017 03:54*

I am sorry you ran into issues. But it looks like Roy is getting you taken care of. I know at times he can be a little hard to reach. One thing to keep in mind when working with smaller companies tied to hobby markets like 3d printing is they are usually a company of a few people at most. I know several companies in 3dp that are just one person. So they don't have the resources like an Amazon support center. I know that is no excuse for not getting back in a timely manner. But just something to keep in mind when some companies don't get back quickly. Niche market companies often have one or two people who pull duties as warehouse, design, support, marketing, r&d, fabrication, accounting, etc. 



That being said, Roy has always taken care of me with support. So thats why I have his products in my BOM. I hope his new board does the trick for you, and you get up and printing quickly.


---
**Gus Montoya** *December 06, 2017 05:16*

**+Stefano Pagani** Keep us posted. I wouldn't mind having a similar board as **+Eric Lien** since he has the files to make it run correctly already. Decreases headaches but diminishes problem solving and thus learning. hmmm


---
**Arthur Wolf** *December 06, 2017 10:03*

About Smoothie prices, if you know how to do simple SMT soldering work, we can send you a smoothieboard with something broken on it with a huge discount, and you can then fix it yourself. It'd be a way to work around your problem. wolf.arthur@gmail.com if interrested.


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/AXn3e8M49P6) &mdash; content and formatting may not be reliable*
