---
layout: post
title: "leave a comment"
date: June 15, 2015 19:40
category: "Show and Tell"
author: E. Wadsager
---
leave a comment





**E. Wadsager**

---
---
**Isaac Arciaga** *June 15, 2015 19:47*

nice. which MK extruder is that?


---
**E. Wadsager** *June 15, 2015 20:03*

mk8 sintron


---
**Eric Lien** *June 15, 2015 21:18*

Alright, direct drive Eustathios!


---
**Vic Catalasan** *June 15, 2015 22:32*

Very nice build and well though out


---
**Ben Delarre** *June 16, 2015 00:59*

Woo! Very interested in a direct drive carriage 


---
**E. Wadsager** *June 16, 2015 15:23*

Now i consider upgrading: 

*print adapters between aluminium block and bushings, so i can replace the lm10uu bearings . 

*Only use one belt driven motor for z axis instead of 2 

*Maybe bigger heatbed. 



Btw i'm pretty impressed by the mk8 extruder.


---
**Isaac Arciaga** *June 16, 2015 17:58*

**+E. Wadsager** Your variant of the Eustathios is very impressive! I would like to tinker with the MK* extruder idea on my build. Was there anything special you had to do to adapt the extruder to the bearing blocks? Also, how much Z did you lose if any?


---
**E. Wadsager** *June 17, 2015 20:57*

As you can see i have not used any printed parts. I used the basic idea about how the yx axis works and designed the frame to be most economic so i had almost none material waste and the wood was very cheap. So i still have about 270mm z travel (haven't measured).

the reason why i did use a mk8 extruder is i bought a complete 3d printer set. i did think about the best way to mount it and zip ties was easy and works great.


---
**Richard Teske** *June 18, 2015 18:28*

Do you have any problems with the rotation of the linear rods, because you are using linear bearings?


---
**E. Wadsager** *June 18, 2015 18:51*

haven't had any problems yet. but my plan is print some adapters/spacers so i can use bushings instead. 



what kind of bushing do you use? 


---
**Richard Teske** *June 18, 2015 18:55*

At the moment I'm planning my build. Would it be possible to push those linear bearings out and push some sintered bushing in?

Because i like the way you use as less printed parts as possible if even none :)

with something like these:

[http://www.aliexpress.com/item/JDB-101430-oilless-impregnated-graphite-brass-bushing-straight-copper-type-solid-self-lubricant-Embedded-bronze-Bearing/32235102431.html?spm=2114.32010308.4.244.PBQ2mI](http://www.aliexpress.com/item/JDB-101430-oilless-impregnated-graphite-brass-bushing-straight-copper-type-solid-self-lubricant-Embedded-bronze-Bearing/32235102431.html?spm=2114.32010308.4.244.PBQ2mI)


---
**E. Wadsager** *June 18, 2015 19:41*

It is just lm10uu bearings inside which are hold back by a snap ring. you can remove them by hand. 



could one of these bushings work? 

[http://www.ebay.com/itm/10Pcs-Plain-Oilless-Bearing-Sleeves-Composite-Bushing-10mm-x-12mm-x-10mm-/310873869007?pt=LH_DefaultDomain_0&hash=item486186cacf](http://www.ebay.com/itm/10Pcs-Plain-Oilless-Bearing-Sleeves-Composite-Bushing-10mm-x-12mm-x-10mm-/310873869007?pt=LH_DefaultDomain_0&hash=item486186cacf)



or 

[http://www.ebay.com/itm/10-Pcs-SF-1-Self-Lubricating-Oilless-Bearing-Bushing-10mm-x-12mm-x10mm-Bronze-/161550274128?pt=LH_DefaultDomain_0&hash=item259d258e50](http://www.ebay.com/itm/10-Pcs-SF-1-Self-Lubricating-Oilless-Bearing-Bushing-10mm-x-12mm-x10mm-Bronze-/161550274128?pt=LH_DefaultDomain_0&hash=item259d258e50)



or print these in pla. 

[https://www.thingiverse.com/thing:24990](https://www.thingiverse.com/thing:24990)



my plan is to use 2 bushings in each block. 


---
**Richard Teske** *June 18, 2015 20:01*

Those printed bushings look also interesting. I think you would need some bushings with an outer diameter of 19mm to fit them in the sc10uu.


---
**E. Wadsager** *June 18, 2015 20:20*

yes that lm10uu size. the printed should fit. 

for the other bushings i would print a spacer/ adapter to make them fit the 19x29mm


---
**Richard Teske** *June 18, 2015 20:21*

Then this should work all right. So why not? Would be great if you could test this :) 


---
**E. Wadsager** *June 18, 2015 20:28*

i cant see any bushings in the Eustathios BOM. does any one know which one is used?


---
**Richard Teske** *June 18, 2015 20:40*

I think you could use those I posted earlier. They are also 30mm long.


---
*Imported from [Google+](https://plus.google.com/103157635215674778495/posts/3WgsdZ9x6uC) &mdash; content and formatting may not be reliable*
