---
layout: post
title: "Hi all, I'm back live and kicking"
date: November 20, 2017 22:21
category: "Discussion"
author: Gus Montoya
---
Hi all, I'm back live and kicking. Going to take this new build's nice and slow. Yes building both the Herculien AND Eustathios. Got the proper equipment to build and not under a time constraint. My first question: is PETG a proper plastic to print the parts? I really want the printer to last the high speeds I really really hope to get to. The extremely slow Creality CR-10 is driving me insane. But all be it a good printer. What is the preferred material to print the plastic parts? 





**Gus Montoya**

---
---
**Eric Lien** *November 20, 2017 22:54*

I still have a soft spot for ABS, but for printability on an open air printer like the CR-10... go with a good quality PETG, and make sure it is dry. PETG should be able to handle the temps and forces very well. I know all of the parts in  **+Zane Baird** 's printer is PETG and holding up well. Also I recommend going with the printed parts upgrades by Zane on the HercuLien, and several by **+Walter Hsiao** on the Eustathios.



I haven't done much to maintain the Github in a while. Mostly because the number of people interested in taking on the builds seems to have tailed off. But I think that is to be expected. Back when I designed these there were more people building from scratch. I think more people buy a printer than build one right now. Also there are lots of good designs out there by others. 


---
**Gus Montoya** *November 21, 2017 00:37*

I hear ya, hey life is busy. I fell in love with this design because it has it all and in my eyes it's timeless. Very robust and  industrial. The new "user friendly" printers are nothing special. Cheap, but they do work. It fits the bill for people that are enameled with 3d printing but don't want to put the mental gears into it. I've kept track with some designs and boy I like the good work by many. I'll hopefully be posting some updates soon. WOW feeling nostalgic. haha


---
**Eric Lien** *November 21, 2017 00:54*

**+Gus Montoya** glad you are back at it. I look forward to frequent posts with your progress :)


---
**Zane Baird** *November 21, 2017 01:28*

**+Gus Montoya** I've been very happy with PETg for printer parts and its all I use for that purpose any more. If you go with my parts for the gantry I would not recommend ABS as it doesn't hold the expanding M3 threaded inserts very well. However, you could always modify the parts to accept captive nuts instead or open the holes up and use heat-set inserts and threadlocker for the hardware


---
**Gus Montoya** *November 21, 2017 01:37*

Hi Zane! Long time haha   I 'm trying to stay away from ABS. I don't want to deal with the fumes, maybe later when I get to that point in the build to have it nicely sealed and vented. I'll take the parts as they are :) Although I am still waiting on Sonny Mounicou to upload his linear rail design. I had rust issues on the rods on my first printer. Not happy about that. 


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/K1YTzeZpEYm) &mdash; content and formatting may not be reliable*
