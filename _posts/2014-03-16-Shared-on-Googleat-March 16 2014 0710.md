---
layout: post
title: "Shared on March 16, 2014 07:10...\n"
date: March 16, 2014 07:10
category: "Show and Tell"
author: D Rob
---




![images/c224ea8ae067232d481fb897b006b01a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c224ea8ae067232d481fb897b006b01a.jpeg)
![images/a392416c09e23ca8843345cc8681e496.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a392416c09e23ca8843345cc8681e496.jpeg)
![images/f83c92182cd3cb27c7e7e1f5c84645cf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f83c92182cd3cb27c7e7e1f5c84645cf.jpeg)

**D Rob**

---
---
**D Rob** *March 16, 2014 07:25*

everyone else is making their take so I thought I would too. This will be universal to spectra or belts. the accompanying back plate will have slots that line up with the 4 bolt holes. I intend to make my back plate use guitar tuners. This slotted design will make guitar tuners with spectra much easier to fine tune using jigs. Will make fine tuning with belts easier also


---
**D Rob** *March 16, 2014 07:27*

oh yeah this design uses 14mm od bronze bushings from **+MISUMI USA** free150 they are 25mm long with 10mm id they will be friction fit but if need be I will add a set screw


---
**Jarred Baines** *March 16, 2014 12:11*

I like it rob ;-) spot on!


---
**Ricardo de Sena** *March 16, 2014 14:37*

liked.


---
**Mike Kelly (Mike Make)** *March 16, 2014 17:07*

Hide your planes! :)


---
**Dale Dunn** *March 16, 2014 19:11*

Acetone vapor bath in SolidWorks!


---
**D Rob** *March 16, 2014 19:22*

**+Dale Dunn** I glad you caught it.    :-) that was what I was going for


---
**Tim Rastall** *March 16, 2014 20:22*

Nice.  What about print orientation? You really want that bushing hole oriented perpendicular to the bed ime.

Also,  I've stopped vapour smooting printer parts as the Acetone seems to weaken them and it screws up the dimensional accuracy of holes. 


---
**Jarred Baines** *March 16, 2014 21:55*

I find parts come up STRONGER by giving them a quick brush with acetone, especially around 'shear points' and parts that are likely to delaminate easily... the acetone gently melts the surface and instead of being more "thready" (scientific term) it becomes "one"...



I've recently done it to my new mendel X ends, tried one set and they split when I inserted the rods... brushed the second set (twice actually) and the weak points just go away. I can't see it working well on everything though, but on some parts it seems stronger afterward. Especially for single-walled (decorative) prints, really enhances the layer bonding IME ;-)



Does seem like it might be hard to print, but all of our parts having important holes at 90deg to one-another are going to be hard to print... I don't see a way around it except breaking the part into 2 separate pieces which isn't good practice either...


---
**D Rob** *March 16, 2014 23:04*

I too have given up on vapour smoothing. I now use a quick acetone bath with a tooth brush and a small condiment bowl of acetone. This leave the critical holes alone (if you are careful:-) ) and when the entire part isn't permeated the acetone evaporates well enough to strengthen the outside shell while leaving the inner bonding alone. I believe the weakening has to do with infill erosion or degradation. A 100% part probably would not do this. I plan on using support on bed only and acetone bath to pretty it up


---
**Jarred Baines** *March 16, 2014 23:40*

There is a second piece to this I imagine?


---
**D Rob** *March 17, 2014 04:32*

Yes the slot in the back is for a tongue and groove connection. There will be a plate. 2 actually depending on what you choose. One for belts and another for spectra. The plate will have a guitar tuning peg on the spectra version. And a belt clamp on the other type. The plate will have slots that the bolts go through (not round holes) so you can fine tune using a jig then tighten the plate in place.﻿


---
**Jarred Baines** *March 17, 2014 06:30*

Too cool... Let me know when you have those done, I'm very interested ;-)



I would design so that the spectra / belt is as close to the linear rail's 'plane' as possible... Then look into motor/pulley positioning to suit that perfect setup ;-)



I didn't think about it until yesterday, but this is actually the same way the **+Ultimaker**  is designed, with that sort of pre-testing done for us I'd say this can be called a "well proven" design :-)



Too... Cool... :-)


---
**D Rob** *March 17, 2014 07:31*

Unfortunately the belt/spectra position will be determined by the pulley/rod that the spectra belt I'd being driven by.    :/


---
**Jarred Baines** *March 17, 2014 07:44*

I'm going to work on the position of the drive rods soon... I'm sure we can work something out, been super busy lately...


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/KRwXoEWVZUc) &mdash; content and formatting may not be reliable*
