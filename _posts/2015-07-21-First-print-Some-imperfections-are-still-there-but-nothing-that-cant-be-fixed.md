---
layout: post
title: "First print. Some imperfections are still there but nothing that can't be fixed"
date: July 21, 2015 12:00
category: "Show and Tell"
author: Igor Kolesnik
---
First print. Some imperfections are still there but nothing that can't be fixed. Red PLA. Dimentions are spot on. Today I'm going to try printing a new extruder

![images/7e517cb229cd5cb5e2486685b516f6da.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7e517cb229cd5cb5e2486685b516f6da.jpeg)



**Igor Kolesnik**

---
---
**Eric Lien** *July 21, 2015 14:25*

Looking good.


---
**Vic Catalasan** *July 21, 2015 16:44*

Very nice, what print settings did you use? temp, layer, speed etc 


---
**Igor Kolesnik** *July 21, 2015 16:48*

.1mm layer hight, 200 C, PLA, 40% infill and 4mm shell. That was a slow print, 2 hours on 40 mm/s with 30mm/s on the outer layer


---
**Vic Catalasan** *July 21, 2015 16:56*

Thanks for that info, it looks great and what machine did you build? Was this done on Slic3r or Cura? 



I will definitely have to slow my prints, thin out my layer and have more patience. Initially I was thinking 20 minutes on that part you printed. 


---
**Igor Kolesnik** *July 21, 2015 17:13*

I wanted to print so badly so I decided to assemble it from what I have. Eventually it will be spider V2. But for now you might find some hot glue in strategic places. Model was sliced with Cura.


---
**Gus Montoya** *July 21, 2015 22:21*

Hello **+Igor Kolesnik** , can you take a picture of your printer? I'm really curious how you hot glued it. :)


---
**Igor Kolesnik** *July 21, 2015 22:27*

**+Gus Montoya** Sure but have your heart pills nearby. 


---
**Gus Montoya** *July 21, 2015 23:03*

? really? Now I gotta see hahaha


---
**Eric Lien** *July 21, 2015 23:49*

**+Gus Montoya** [https://plus.google.com/110771739287710638686/posts/jBFc1b4kK3n](https://plus.google.com/110771739287710638686/posts/jBFc1b4kK3n)


---
**Vic Catalasan** *July 22, 2015 06:55*

Wow! MacGyver you are!


---
*Imported from [Google+](https://plus.google.com/+IgorKolesnik/posts/FhgV9zJTDSW) &mdash; content and formatting may not be reliable*
