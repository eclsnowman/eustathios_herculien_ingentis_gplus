---
layout: post
title: "Eric Lien 's V4 carriage modded for LM8UU linear bearings"
date: February 11, 2015 22:59
category: "Show and Tell"
author: Isaac Arciaga
---
**+Eric Lien** 's V4 carriage modded for LM8UU linear bearings. Printed with Carbon Fiber ABS from 3DXTech. I might do a live stream tonight with a full plate of Eustathios parts with this material.



I'll share the file once I verify fitment.

![images/2e324bf23f8e1c081abdf8e7f4896576.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2e324bf23f8e1c081abdf8e7f4896576.jpeg)



**Isaac Arciaga**

---
---
**Eric Lien** *February 11, 2015 23:15*

 beautiful print


---
**Dat Chu** *February 12, 2015 00:05*

Carbon fiber? Wait. I want some. 


---
**Ryan Jennings** *February 12, 2015 00:45*

Impressive looking. I bought a roll to test out now!


---
**Eric Lien** *February 12, 2015 00:56*

**+Ryan Jennings** I just bought two.


---
**Ryan Jennings** *February 12, 2015 00:57*

Showing off huh :-) 


---
**Joe Kachmar** *February 12, 2015 01:00*

**+Isaac Arciaga**  Have you used any of 3DXTech's other filaments? I've been wanting to try their MG94 ABS, since they're the only vendors I've seen selling that plastic in filament form.


---
**Eric Lien** *February 12, 2015 01:25*

**+Ryan Jennings** nope, but hopefully I will be with some of the prints from it ;)


---
**Isaac Arciaga** *February 12, 2015 01:45*

**+Joe Kachmar** I haven't yet but Sabic is good stuff.


---
**Tim Rastall** *February 12, 2015 04:42*

Huh,  I've clearly not been paying enough attention.  Anyone tried their nanotube?! filament?


---
**Isaac Arciaga** *February 12, 2015 07:20*

**+Tim Rastall** I know someone that uses it at work. They love it. Just don't use it on a PEI bed. You won't be able to take the print off.


---
**Seth Messer** *February 12, 2015 20:15*

**+Isaac Arciaga** apologies if this is not the right venue for this question, but, would you be billing to print out all of the latest Eustathios parts (even better with Eric's updated parts/additions too)? If so, what would your rate/charge be? I don't think CF would be necessary compared to ABS, but I'm so new to this, that maybe CF would be better to pay extra for.



Thanks much!


---
**Isaac Arciaga** *February 14, 2015 08:40*

Hi **+Seth Messer** sorry for the late reply. I was planning on joining in on the Print It Forward Program after completing my build. I have just started working on my own parts.



To the Mods, is there a list for a PIF so Seth can be submitted to the queue?


---
**Seth Messer** *February 15, 2015 05:44*

Thanks much **+Isaac Arciaga**. I believe someone added me to a list that **+Eric Lien** had started. I'm really stoked. Soon enough I can join you guys. In the meantime, could you give me some idea of how much I can expect to pay for the printed parts I'll need? Would help with keeping the budget in check to some extent. Thanks so much. 


---
**Isaac Arciaga** *February 15, 2015 06:14*

Hi **+Seth Messer** . My understanding is the recipient pays for shipping. That's it :). Hopefully someone can chime in here to correct me if i'm wrong.



With that said, I have 6lbs of ABS set aside to assist in the program when **+Eric Lien** releases the V2 variant (easier BoM) of the Eustathios. After my build is completed of course.



No pressure Eric!


---
**Eric Lien** *February 15, 2015 06:23*

**+Isaac Arciaga**​ I am up working on it. 


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/TyfS3KU8cBp) &mdash; content and formatting may not be reliable*
