---
layout: post
title: "I cannot express how happy I am"
date: January 25, 2019 01:42
category: "Discussion"
author: Eric Lien
---
I cannot express how happy I am. **+Michael K Johnson** did me and this community a great favor. As we all know G+ is slowly approaching it's "use by date". And I don't trust that google will gracefully sunset G+ with static content pages to retain the years of knowledge, sweat, community we have nurtured in our corner of the interwebs. So Michael who is ...lets be frank... a computer ninja, helped make a backup of this community so it can be hosted out on Gitlabs. It started with a json and image export from [https://gplus-exporter.friendsplus.me](https://gplus-exporter.friendsplus.me) , then Michael worked his magic to convert it into usable data we could host remotely.



So now even if Google gives us the old middle digit in April, we will not lose the historic record of what our group brought to the table in the history of the 3DP community. Michael, I cannot thank you enough.



Now it is not a perfect 1:1 G+ backup solution. One of the advantages with G+ was the ubiquity with which you could share. Google photo albums, youtube links, embedded post images, links to outside content. It does not all perfectly display. But DANG is it an amazing start. I have some things to learn to try and get the remainder of the videos and some other content wrapped up under the backup umbrella. 



But I thought you should all know it is out there. Take a look, and if any other computer ninjas have some ideas on the remainder of the content backup... please chime in with ideas.



[https://eclsnowman.gitlab.io/eustathios_herculien_ingentis_gplus/](https://eclsnowman.gitlab.io/eustathios_herculien_ingentis_gplus/)





**Eric Lien**

---
---
**Michael K Johnson** *January 25, 2019 01:51*

If anyone wants to search the content or browse it locally to make browsing faster, you'll need to have ruby and jekyll installed, but then:



git clone [gitlab.com - Eric Lien / Eustathios_HercuLien_Ingentis_Gplus](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus.git)

cd eustathios_herculien_ingentis_gplus

jekyll serve



[https://jekyllrb.com/docs/installation/](https://jekyllrb.com/docs/installation/) to get to instructions for jekyll



If you want to help archive pictures and videos from the about 160 posts that are missing pictures and videos, we can provide information on how to make a merge request to that repository that adds them.



I haven't built any of these printers, but as I construct my own unique design inside the tronxy x5s frame I'm re-using, I'll be glad to have this archive of useful information!


---
**Scott Hess** *January 25, 2019 01:57*

This is great!  Of my Google+ groups, this was the one I most thought I'd miss, because someday I suspect I'll break down and start ordering parts :-).


---
**Ryan Fiske** *January 25, 2019 03:30*

**+Michael K Johnson** thanks for your hard work!


---
**Bruce Lunde** *January 25, 2019 03:41*

This is awesome, as I learn slowly and being able to continue to look up information is fantastic!


---
**Michael K Johnson** *January 25, 2019 03:58*

[gitlab.com - README.md · master · Eric Lien / Eustathios_HercuLien_Ingentis_Gplus](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/blob/master/README.md) now has instructions on how to help contribute missing pictures and videos.


---
**Michaël Memeteau** *January 25, 2019 08:52*

Fantastic! Thank you so much **+Michael K Johnson**...


---
**Dennis P** *January 25, 2019 16:09*

Thank you all for your efforts! I am an RTFM kind of guy and the forum is the first place I go and look. 


---
**Carter Calhoun** *January 25, 2019 18:39*

Thank. you SO much Computer Ninja **+Michael K Johnson** !!


---
**James Rivera** *January 27, 2019 07:17*

Awesome!


---
**Michael K Johnson** *January 29, 2019 23:27*

Thanks to the Friends+Me Google+Exporter author for sharing documentation with me, we now have bold, italic, and strikethrough formatting preserved in the output.


---
**Michael K Johnson** *January 30, 2019 23:51*

Google finally, after months of unconscionable delay, has told us: they will be <b>deleting</b> all the G+ content, including images and videos. Starting <b>2 April.</b>



There are nearly 200 missing images/videos from this community in the backup archive. Help now if you care to save them. I'm hoping that updates to the export tool allow us to save some of the missing comments, but I'm not expecting that missing images and videos will show up automatically. See the instructions above to help, and ask if you need help following the instructions.



[https://cloud.google.com/blog/products/g-suite/what-you-need-to-know-about-the-sunset-of-consumer-google-plus-on-april-second](https://cloud.google.com/blog/products/g-suite/what-you-need-to-know-about-the-sunset-of-consumer-google-plus-on-april-second)

[cloud.google.com - What you need to know about the sunset of consumer Google+ on April 2 &#x7c; Google Cloud Blog](https://cloud.google.com/blog/products/g-suite/what-you-need-to-know-about-the-sunset-of-consumer-google-plus-on-april-second)


---
**Eric Lien** *January 31, 2019 01:43*

**+Michael K Johnson** this is great (about the italics and formatting). Once I get through some of these on site installations I have going right now I hope to dig into manually downloading the missing files unless there is another method that comes up from the group in the interim.


---
**Eric Lien** *February 01, 2019 01:55*

Will this help us potentially?![images/fb81896fef754c9583f4deac7ce17717.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fb81896fef754c9583f4deac7ce17717.jpeg)


---
**Michael K Johnson** *February 02, 2019 12:21*

**+Eric Lien** Maybe! We can see what they give us now, and in March (wow that's cutting it close) we'll see what else it gives us. But I wouldn't count on it. It will probably work for only some of the images or videos. It is likely to give us the text of the missing comments if F+M G+E doesn't first fix the problem with incomplete comment streams.



Unless google go out of their way to make it difficult, it won't be tremendously hard to add code to parse that data.


---
**Michael K Johnson** *February 02, 2019 23:25*

The next release of F+M G+E is expected to solve the missing comment problem, as well as recognizing and fixing one of the common sources of missing images in this community! Yay!



Meanwhile, I'm also checking out the current content of a community takeout, which Google promises will improve in March (that's cutting it close...) — it may or may not help fill in any gaps by then.



If anyone is willing to help improve the ugly formatting by experimenting with Jekyll themes, that could be nice. I don't see one that mimics the Google+ experience, but I haven't looked much at all.


---
**Michael K Johnson** *February 03, 2019 19:30*

The current takeout for a community is 99.75% useless, and it's hard to tell what Google will see fit to release in late March, a few days before they unceremoniously pull the plug.



Meanwhile, the new F+M G+E 1.7.7 release seems to have done a good job of getting the missing comments, and also found a few (7) more pictures. That's the good news.



Getting through all the comments showed lots more missing images. We now have 191 missing images thanks to bringing in the missing comments, and there are still 75 videos missing from the archive. It will be up to the community to fill those in.


---
**Eric Lien** *February 03, 2019 19:33*

**+Michael K Johnson** Can a list of the missing urls be generated? I will manually download pictures and videos tonight.


---
**Michael K Johnson** *February 03, 2019 19:43*

The [gitlab.com - README.md · master · Eric Lien / Eustathios_HercuLien_Ingentis_Gplus](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/blob/master/README.md) file has instructions for how to fix the missing images and videos.


---
**Eric Lien** *February 03, 2019 22:24*

I am liking the look and feel of wikifactory so far. Time will tell but they are building a platform for migrating G+ communities and I like the vibe I am seeing over there so far. Seems much more inline with the types of people that drew me to G+. I am not sold yet... but so far that is my front runner to move the Eustathios/HercuLien for the future posts.



[https://wikifactory.com/@carolportugal/stories/wikifactory-forum-for-digital-fabrication-communities-on-google](https://wikifactory.com/@carolportugal/stories/wikifactory-forum-for-digital-fabrication-communities-on-google)



[https://wikifactory.com/@eclsnowman](https://wikifactory.com/@eclsnowman)






---
**Michael K Johnson** *February 03, 2019 22:40*

Looks interesting. Given that community takeout has no post content, I wonder whether it is aspirational or if they are screen scraping like F+M G+E?  Also, will they provide content to community owners so that it's not out of the frying pan and into the fire?


---
**Eric Lien** *February 03, 2019 22:52*

**+Michael K Johnson** very true. I might just be trading masters. But I am looking for a system ideally with a low barrier to to user entry, public viewability, and ease of content addition. Like I said, I am not sold yet... But so far, I like the interface and projects I saw posted.


---
**Michael K Johnson** *February 03, 2019 23:00*

Yeah, a gitlab pages site isn't exactly a low barrier to entry. But it is worth asking them about data portability. I note they are in EU and interpretations of GDPR are rampant and diverse, and as a commercial entity they might have to react in unexpected ways as case law develops.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/bpXUtQFv5nJ) &mdash; content and formatting may not be reliable*
