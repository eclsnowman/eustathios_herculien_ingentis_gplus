---
layout: post
title: "This keeps happening after rebuilding my Eustathios Spider V2 and installing a new Azteeg X5 Mini WIFI with SD2224 drivers"
date: January 24, 2018 22:10
category: "Discussion"
author: Brandon Cramer
---
This keeps happening after rebuilding my Eustathios Spider V2 and installing a new Azteeg X5 Mini WIFI with SD2224 drivers. It shifts on every print job. 









![images/bb9c41ba5dc34e23e3f85bebbfbb6188.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bb9c41ba5dc34e23e3f85bebbfbb6188.jpeg)



**Brandon Cramer**

---
---
**Eric Lien** *January 24, 2018 22:29*

Think the drivers are overheating? What is your current set at in firmware? Do you have cooling on the drivers (trinamic are known for running hot). 


---
**Brandon Cramer** *January 24, 2018 22:39*

alpha_current                                    0.65

beta_current                                      0.65

gamma_current                                0.75



I forget the math. Is that 2 * .65 = 1.3 for X and Y and 1.5 for Z.






---
**Carter Calhoun** *January 24, 2018 22:41*

I'm assuming you've observed this happening?  Not visibly getting caught on anything?  All your set screws on pulleys are tight?


---
**Brandon Cramer** *January 24, 2018 22:53*

**+Carter Calhoun** Yes I watch it shifting on each layer practically. 



**+Eric Lien** I'm going to try 1.0 for XYZ. As far as cooling. What's that? I need to get that hooked back up I guess.

 


---
**Carter Calhoun** *January 24, 2018 23:04*

Eric's asking if you have forced air (fan) on those drivers.  If not, that's most likely. 



Looks like it's only shifting on one axis, is that correct?  If so, maybe try swapping your Z stepper driver with your skipping axis and see what happens.


---
**Eric Lien** *January 25, 2018 00:03*

The trinamic are actually setup on the digipot so you put actual current value in firmware. So if you put in 0.65 in the smoothie config you are getting 0.65A. The 2xVref is for if you have digipot disabled and are using the manual pot via the screw adjustments. In that case you measure the voltage and 2xVoltage measured in the solder pad = approximate current.


---
**Eric Lien** *January 25, 2018 00:04*

Yes you want cooling running over the drivers.


---
**Brandon Cramer** *January 25, 2018 00:57*

**+Eric Lien** Thanks! That could be causing me some trouble running them at .65A. I set them all to 1.0A. 



I need to print the Azteeg X5 Mini V3 electronics package stl so I can mount it properly. After that I will get the fan setup. Is it enough to just have the fan mounted and blowing air over the board? 



It's almost 5:00 so I will give it a try tomorrow.  


---
**Eric Lien** *January 25, 2018 01:07*

**+Brandon Cramer** ideally you would want a ducted fan forcing it directly over the fins... But some airflow is better than none so if you get the 40mm fan hooked up similar to the GitHub design, you should be fine.


---
**Brandon Cramer** *January 25, 2018 17:43*

Looks good at 1A. Too bad I need the fan. This thing would be near silent with the Azteeg X5mini WIFI and SD2224 drivers.

[dropbox.com - File Jan 25, 9 37 06 AM.mov](https://www.dropbox.com/s/5w9ksjlt5zul4af/File%20Jan%2025%2C%209%2037%2006%20AM.mov?dl=0)


---
**Carter Calhoun** *January 25, 2018 17:44*

Cool!  Fan could be pretty small I think, 40mm probably?  That's not loud at all. Mine is hooked up such that it only comes on when the steppers are active, which at least then when idle your machine would be silent ;)


---
**Brandon Cramer** *January 25, 2018 19:17*

Well. It started to work. I don't think my Z axis is moving at all. 



I made some changes via the X5mini WIFI interface. Does this override the config.txt file? Do I need to reboot after making changes?



So what is the actual current to the steppers:



alpha_current 1.0 ##                         0.65                # X stepper motor current 

beta_current 1.00 #                          0.65                # Y stepper motor current

gamma_current 1.5 ##                     0.75                # Z stepper motor current



Are they set at 1.0 and 1.5 or .65 and .75?



I'm asking because my Z axis just whines when I try to move it up or down. If I home Z it seems to work sometimes. 



I'm not sure what this is either:



z_axis_max_speed 300 # #                300                 # mm/min ###was 15000###



gamma_max_rate                               30000               # mm/min actuator max speed ###was 15000###


---
**Carter Calhoun** *January 25, 2018 19:29*

Hey Brandon, take my input with a grain of salt, I've never used the X5mini or anything other than RAMPS for that matter, but,  I have noticed in certain cases that when I make changes to the configuration, a reboot needs to take place before it is implemented. A simple and fast thing to test.



On first look,  that z_axis_max_speed info seemed strange, but then again Eric is going to know that best.


---
**Brandon Cramer** *January 25, 2018 19:49*

I don't get it. I can move the Z around in the EPS3D for Smoothware web page off the X5 Mini but not in Simplify3d. I can home Z in Simplify3d and EPS3D web page. 


---
**Eric Lien** *January 25, 2018 19:56*

are you sure your steps/mm are set correctly for Z?


---
**Brandon Cramer** *January 25, 2018 20:02*

I have the Z set for 1/32. I think in the past I have always used 1/16 which is what I started with. Do I need to set this somewhere else for 1/32?




---
**Brandon Cramer** *January 25, 2018 20:10*

![images/e622c933937057312324d71975d6ac32.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e622c933937057312324d71975d6ac32.jpeg)


---
**Brandon Cramer** *January 25, 2018 20:11*

![images/be171fff2d0b3515c0770040bf44ef41.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/be171fff2d0b3515c0770040bf44ef41.jpeg)


---
**Carter Calhoun** *January 25, 2018 20:14*

**+Brandon Cramer** If you are running 1/32, when you were previously running 1/16, then you would double your steps per mm in your configuration.


---
**Eric Lien** *January 25, 2018 20:20*

you set the microstepping via the jumpers on the board under the driver. It is set via the  details here: [http://panucattdevices.freshdesk.com/support/solutions/articles/1000258055-sd2224-drivers](http://panucattdevices.freshdesk.com/support/solutions/articles/1000258055-sd2224-drivers)




---
**Eric Lien** *January 25, 2018 20:22*

Note For the SD2224 jumpers MS1 and MS2 set the microstepping, and MS3 sets if it is in stealth chop or spreadcycle modes. Details in the link above.


---
**Brandon Cramer** *January 25, 2018 20:40*

**+Eric Lien**  That is where I have the microstepping set to 1/32. I've tried both stealth chop and spreadcycle modes. 



Do I need to reboot after each change of the configuration?


---
**Eric Lien** *January 25, 2018 20:42*

**+Brandon Cramer** yes reset after each change to the configuration. Also changes via the ESP interface are likely written to config_override. But perhaps **+Roy Cortes**  can confirm. I am not near my printer to test if the changes are in config or config_override. If you have changes in config, but there is a config_override... then config_override wins.


---
**Brandon Cramer** *January 25, 2018 21:03*

Here is the config file. When I swapped Z and Y my Y axis had the same issue being on the Z axis pins. 





[dropbox.com - config.txt](https://www.dropbox.com/s/kkthiedqgbsao3b/config.txt?dl=0)


---
**Brandon Cramer** *January 25, 2018 21:06*

gamma_current 1.0



I don't know if these two settings could cause me trouble:



z_axis_max_speed                             15000

gamma_max_rate                              15000


---
**Brandon Cramer** *January 25, 2018 21:12*

Maybe this is my problem?



gamma_steps_per_mm                           5120


---
**Brandon Cramer** *January 25, 2018 22:39*

5120 is what it should be for 1/32 micro stepping. 2560 for 1/16 micro stepping. I can home Z no problem, but I can't move Z up or down with Simplify3D. 


---
**Brandon Cramer** *January 26, 2018 06:04*

I had to set z axis max speed to 300 and it appears to be working better although my first print failed but I ran out of time to try another print. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/V2mcp7iRvZU) &mdash; content and formatting may not be reliable*
