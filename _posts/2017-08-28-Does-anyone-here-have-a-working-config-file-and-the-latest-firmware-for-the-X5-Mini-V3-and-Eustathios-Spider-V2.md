---
layout: post
title: "Does anyone here have a working config file and the latest firmware for the X5 Mini V3 and Eustathios Spider V2?"
date: August 28, 2017 17:08
category: "Discussion"
author: Brandon Cramer
---
Does anyone here have a working config file and the latest firmware for the X5 Mini V3 and Eustathios Spider V2? 





**Brandon Cramer**

---
---
**Brandon Cramer** *August 28, 2017 21:18*

I think I'm getting somewhere. I didn't have the panel.enable set to true. 



How do I set the stepper motor voltage using firmware? When I check the Vref using a voltmeter on my SD5984 drivers it's showing 0.50 vdc.

[dropbox.com - config.txt](https://www.dropbox.com/s/7hrkj2457grwsck/config.txt?dl=0)


---
**Brandon Cramer** *August 29, 2017 16:04*

**+Alex Skoruppa** I saw that in the manual. There aren't any pins to put a jumper on the SD5984 in the place where there would be a jumper. 



I have the steppers set to 1.5 amps but I'm not sure why I wouldn't have it at the vref? Is there another spot for setting the current or do I need to adjust the screw manually to get the current I need. Since this is digipot I'm assuming I don't need to adjust it manually. 



I wish Panucatt support was more help. They don't even respond to trouble tickets!!! **+Roy Cortes** 






---
**Sean B** *September 02, 2017 13:58*

**+Brandon Cramer** I have the Azteeg X5 Mini V3 and although I haven't measured the Vref on the board itelf, any time I adjusted the digipots there was a difference in motor torque.  They also heat up much quicker when running for any amount of time at 1.5A.  Make sure you have active cooling on the Azteeg or it will go into thermal shutdown.  I can provide my config if you want, but I am not sure if you need it.  Let me know.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/dboVaBTZY6a) &mdash; content and formatting may not be reliable*
