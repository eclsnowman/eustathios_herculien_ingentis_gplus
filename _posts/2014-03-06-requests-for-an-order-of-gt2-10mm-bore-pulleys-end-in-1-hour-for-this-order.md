---
layout: post
title: "requests for an order of gt2 10mm bore pulleys end in 1 hour for this order"
date: March 06, 2014 02:55
category: "Discussion"
author: D Rob
---
requests for an order of gt2  10mm bore pulleys end in 1 hour for this order. Next order will have to wait until we have a large enough order again





**D Rob**

---
---
**Eric Lien** *March 06, 2014 02:57*

Any chance we could bulk buy the gt2 belt also and save some $.


---
**D Rob** *March 06, 2014 03:21*

Ebay has the stuff pretty cheap. I am in need of some closed loop belts though. I'll check but we'll probablt come cheaper on the bay.


---
**D Rob** *March 06, 2014 03:26*

I see plenty of closed loops on their site but no cut to length


---
**Tim Rastall** *March 06, 2014 03:47*

They definitely do open lengths. 


---
**D Rob** *March 06, 2014 04:41*

Can you send me a link to them **+Tim Rastall**


---
**Brian Bland** *March 06, 2014 10:19*

[http://www.robotdigg.com/product/10/Open-Ended-6mm-Width-GT2-Belt](http://www.robotdigg.com/product/10/Open-Ended-6mm-Width-GT2-Belt)


---
**D Rob** *March 06, 2014 12:48*

Thanks Brian


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/ZQzAjqDww9t) &mdash; content and formatting may not be reliable*
