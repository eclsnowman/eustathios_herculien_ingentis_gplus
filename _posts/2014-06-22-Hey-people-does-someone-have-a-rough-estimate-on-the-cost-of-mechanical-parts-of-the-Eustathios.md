---
layout: post
title: "Hey people, does someone have a rough estimate on the cost of mechanical parts of the Eustathios?"
date: June 22, 2014 15:48
category: "Discussion"
author: Shachar Weis
---
Hey people,

does someone have a rough estimate on the cost of mechanical parts of the Eustathios? Just the extrusions/rods/belts/nuts/bearings/ect. Not including motors/hotend/electronics/printed parts. Thanks. 



PS, I should mention I'm in Canada.





**Shachar Weis**

---
---
**Jason Smith (Birds Of Paradise FPV)** *June 22, 2014 15:51*

 Check out the BOM. I'm sure a lot of the parts can be found for cheaper, but this should give you an idea. 

[https://docs.google.com/spreadsheet/lv?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing](https://docs.google.com/spreadsheet/lv?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing)


---
**Shachar Weis** *June 22, 2014 15:53*

Thanks! I didn't know that the BOM included prices. 


---
**Jim Squirrel** *June 22, 2014 22:50*

It's lacking the screws not used on aluminum extrusions. Aside from that very good bom


---
**Kalani Hausman** *June 28, 2014 02:32*

Very nice to include the costs in your BOM. Thank you!


---
*Imported from [Google+](https://plus.google.com/117479393665221551027/posts/RSRHNb66rMD) &mdash; content and formatting may not be reliable*
