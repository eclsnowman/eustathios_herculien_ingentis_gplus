---
layout: post
title: "Other than build volume and cost what would be the advantages of my initially building the HercuLien over the Eustathios design?"
date: January 30, 2016 21:40
category: "Discussion"
author: Robert Clayton
---
Other than build volume and cost what would be the advantages of my initially building the HercuLien over the Eustathios design? Is the accuracy and precision the same? Can they use the same filament materials?  It seems most of the recent discussions involve the Eustathios model. Also, would ABS be the preferable material for the printed components?





**Robert Clayton**

---
---
**Tomek Brzezinski** *January 30, 2016 21:56*

Just answering what I can: ABS is generally much preferred over PLA (assuming you only have those options) for 3Dprinter parts. That's because in certain conditions you can get close to the 60C softening point for PLA. But if it's a warpy hard-to-print part, or a rubbing part, PLA might be better.  


---
**Eric Lien** *January 30, 2016 22:08*

The main advantages to HercuLien are build volume, enclosed for better ABS results, and dual extrusion. Other than that Eustathios Spider V2 is every bit as good as HercuLien and has more users who have built it.



HercuLien is more printer volume that 99% of people need, but when you need it it's great to have the option. 



Hope that helps some.


---
**Robert Clayton** *January 30, 2016 22:19*

I suppose one could build an enclosure to surround the Eustathios. Would the ABS ability be similar then? Additionally, would it be best for the temperature inside the enclosure to be maintained at a specific level?


---
**Eric Lien** *January 30, 2016 23:43*

My HercuLien maintains 50-55C with the bed at 110C. So that's perfect for me.


---
**Jim Stone** *January 30, 2016 23:48*

when you can print large....you wont be going back....


---
**Tomek Brzezinski** *January 30, 2016 23:52*

Allowing a build temperature closer to 70-75c allows for better  abs printing.  Now many people do that but for larger print areas and the structurally improved qualities of abs, I find it worthwhile.  But your cold end might have trouble staying cold enough for pla,  so you might want at least some way to cool things down inside for pla prints.  Like exhaust or just removing paneling some.  But those reasons are why I prefer printed structural parts to be abs


---
**Robert Clayton** *January 31, 2016 01:58*

I know it's the Engineer in me but where are the temperature regulated cases and the water cooled cold ends?


---
**Eric Lien** *January 31, 2016 02:22*

**+Robert Clayton** build it and they will come :)


---
*Imported from [Google+](https://plus.google.com/103318213942990361542/posts/M9bFKQy1dY5) &mdash; content and formatting may not be reliable*
