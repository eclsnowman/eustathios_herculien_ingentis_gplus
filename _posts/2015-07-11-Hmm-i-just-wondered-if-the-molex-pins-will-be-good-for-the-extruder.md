---
layout: post
title: "Hmm i just wondered if the molex pins will be good for the extruder!?"
date: July 11, 2015 12:26
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Hmm i just wondered if the molex pins will be good for the extruder!? We've got 8 pins so there's no chance of doubling them for the extruder. I couldn't find any specifications that tell - just anything else but nothing about current max.



How did you guys solve this?





**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *July 11, 2015 13:33*

I don't totally follow. Do you mean for doing direct drive?


---
**Eric Lien** *July 11, 2015 13:36*

I have run 4 pin molex computer power plugs for years at my steppers for quick disconnect (before I found the Robotdigg steppers that have the plug designed into the stepper.﻿)


---
**Eric Lien** *July 11, 2015 13:40*

[http://imgur.com/P9SkbVi](http://imgur.com/P9SkbVi)


---
**Frank “Helmi” Helmschrott** *July 11, 2015 13:41*

Hmm no i was talking about Bowden. There's 2 wires for the hotend , 2 thermistor, 2 Cooling Fan, 2 Extruder Fan. Is the hotend ok with only two Pins?


---
**Eric Lien** *July 11, 2015 14:18*

Oh. It has been for me. I run 24v 40watt heater. It also holds up fine to my 24v 65watt heater from Watlo.


---
**Frank “Helmi” Helmschrott** *July 11, 2015 14:20*

alright, thanks - i was just curious about that. I also have a 24V/40W for the E3D (though they delivered a 25W only with the 24V version of the hotend)


---
**Eric Lien** *July 11, 2015 14:22*

"Molex’s Micro-Fit™ product family consists of 3.00mm pitch connectors with a maximum current rating of 5.0A; available in multiple circuit sizes and cable lengths for power/signal, blind-mating, wire-to-wire, wire-to-board and board-to-board and some cable assembly applications"


---
**Eric Lien** *July 11, 2015 14:23*

So 5amp for the small connector in the default carriage.


---
**Eric Lien** *July 11, 2015 14:24*

Wire size must be correct of course.


---
**Eric Lien** *July 11, 2015 14:26*

**+Frank Helmschrott** I have no idea why they switched to 25watt. I run 40 and still at high temp filament can fight keeping temp stable when the head is really traveling fast.


---
**Frank “Helmi” Helmschrott** *July 11, 2015 14:28*

**+Eric Lien** i see similar issues on other printers too. Also 25W is really too low.Very keen to see how it develops on the Eusthatios. We're coming close slowly.


---
**Mike Miller** *July 11, 2015 14:53*

I've used high amp, RC car connectors, but honestly, they seem to be a lot of overkill if everything is set up right. We're not building hundreds of thousands of units with clogging nozzles. The cost of sourcing male and female connectors, crimp connectors, and having enough on- hand, vs just having a long wire and a dozen zip ties when something needs swapping... If you have to replace a component, you STILL have to crimp/solder in the connector to the new component, so it's not like you're even saving a lot of time. 


---
**Eric Lien** *July 11, 2015 15:51*

**+Mike Miller**​ I have to disagree with you. I upgrade my printer all the time with new ideas. Modular wiring is the only way to go in my opinion. But that's just my 2cents and personal preference.﻿


---
**Frank “Helmi” Helmschrott** *July 11, 2015 16:18*

I'm completely with **+Eric Lien** regarding modularity but i think in this case i'll bring the molex connection from the extruder carriage to the upper frame. For one i have more possibilities to secure the whole cable pack with the frame and therefore relieve the connectors from the movements of the extruder. Secondly the distance between the extruder and the electronics is quite long and this way i could use some more preconfigured cables that i have lying around here.


---
**Mike Miller** *July 11, 2015 16:29*

The stuff I'm changing is pretty much...the nozzle. The heater cartridge is effectively glued in place, and I haven't had a failed thermistor yet. I'm using these, but honestly, I've had it apart once since final troubleshooting assembly. 


---
**Mike Miller** *July 11, 2015 17:29*

So the Google ate my link...I'm using these : [https://en.m.wikipedia.org/wiki/Anderson_Powerpole](https://en.m.wikipedia.org/wiki/Anderson_Powerpole)


---
**Jeff DeMaagd** *July 11, 2015 17:40*

The "computer plug" is Mini-Fit Jr., and that's probably great for heaters. I wouldn't go Micro-Fit for the heater. I'm trying to find a usage guide from Molex. But it's good to have extra current margin.


---
**Frank “Helmi” Helmschrott** *July 12, 2015 07:01*

With 5A max current the margin should indeed be good enough. with a 40W heater at 24V this is more than 50% margin.


---
**Jeff DeMaagd** *July 12, 2015 14:52*

Assuming you use the correct pins and you're using them to how the data sheet specifies. They offer the pins in several different metals and coatings. Also, there are deratings that might need to be applied based on the conditions it's used in. Two adjacent high current pins in a connector lowers the max current. Higher ambient temperature does that too.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/7Sy9HZQpq7p) &mdash; content and formatting may not be reliable*
