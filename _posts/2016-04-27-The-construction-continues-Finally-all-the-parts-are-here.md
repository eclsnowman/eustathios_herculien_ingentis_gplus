---
layout: post
title: "The construction continues! Finally all the parts are here!"
date: April 27, 2016 11:34
category: "Build Logs"
author: Dimitrios Tzioutzias
---
The construction continues!



Finally all the parts are here! For z axes I am going to use goldmart's 1204 ballscrews. One question, most of my printed parts are made with PLA. I am thinking to use ABS for the parts that are near the bed like the bed leveling mounts and ball screws/shaft mount. Do you think I can use PLA for those parts or the temp emitted by the bed is going to melt them? 

This is the link of the rubber heater, that's the last part  I am waiting! [http://www.aliexpress.com/snapshot/7531642535.html?orderId=74297574740434](http://www.aliexpress.com/snapshot/7531642535.html?orderId=74297574740434) 









![images/140ec069124194189407dac7b3db8436.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/140ec069124194189407dac7b3db8436.jpeg)
![images/3776d6b5a5ce3481191053188d8239bf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3776d6b5a5ce3481191053188d8239bf.jpeg)

**Dimitrios Tzioutzias**

---
---
**Michaël Memeteau** *April 27, 2016 11:47*

If the parts are not in direct contact with part that are heating with the bed by conduction, it's gonna be fine. I didn't have any problem so far and found the PLA to be stiffer than ABS.




---
**Eric Lien** *April 27, 2016 11:48*

You are probably fine in PLA for those parts, but the part cooling duct and central carriage would be best in ABS if you can manage it. I think **+Jason Smith**​ has PLA for all of his parts if I recall correctly. He designed the Original Eustathios. So if anyone knows for sure it would be him :)


---
**Jason Smith (Birds Of Paradise FPV)** *April 27, 2016 11:55*

**+Eric Lien**   I was originally running PLA for everything, but later added a heated bed and enclosed the printer.  I was able to run the printer that way long enough to re-print all the parts in ABS, but just barely.  Towards the end, I had one sad looking, saggy printer with lots of tolerance/alignment issues :)  I definitely would recommend ABS/PETG for anything that will be near the bed, and would at least bootstrap yourself a new set of ABS/PETG parts as soon as you can once it's running.


---
**Maxime Favre** *April 27, 2016 11:57*

PLA everywhere here too exept fan duct in PET. Until now everything is fine. It will probably reprint the carriage and duc in ABS.


---
**Jason Smith (Birds Of Paradise FPV)** *April 27, 2016 11:59*

Interestingly, my cooling fan died in the middle of a print last week, and my ABS fan duct deformed fairly significantly due to the heat.


---
**Dimitrios Tzioutzias** *April 27, 2016 12:05*

Initially I will print only the duct and the carriage in ABS because my prusa i3 can reach only 110c and it takes 30 min or so to do it! Later on I will print them in ABS when eustathios is ready. Thanks for the advises!!! Also I had to change a little bit the design of the carriages because I couldnt find self aligned bushings like the one's in BOM. I will post a photo of what I finally used!


---
*Imported from [Google+](https://plus.google.com/108355995361667474020/posts/CcZ6pAHLJaL) &mdash; content and formatting may not be reliable*
