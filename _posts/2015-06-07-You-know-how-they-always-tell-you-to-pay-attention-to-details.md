---
layout: post
title: "You know how they always tell you to pay attention to details?"
date: June 07, 2015 01:25
category: "Show and Tell"
author: Bruce Lunde
---
You know how they always tell you to pay attention to details? Can you spot what is wrong in picture 1? Picture two shows the assembly in right, but the cause of why I turned it  90 degrees in the first place. I had two components upside down, so it seemed like I had to turn it.  You may laugh, I laughed at myself!



![images/1b10b17eed567578122c5f6a0e7520cb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1b10b17eed567578122c5f6a0e7520cb.jpeg)
![images/2547d0240a5058555bdc6e0b8e4383b2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2547d0240a5058555bdc6e0b8e4383b2.jpeg)
![images/6c1d16bb894aaf7a919933b1e8c04213.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6c1d16bb894aaf7a919933b1e8c04213.jpeg)
![images/0524c82eedd00c35872538f5037c077e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0524c82eedd00c35872538f5037c077e.jpeg)
![images/eac63b82649e1661a036c0778c67aa3f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eac63b82649e1661a036c0778c67aa3f.jpeg)

**Bruce Lunde**

---
---
**Eric Lien** *June 07, 2015 01:56*

Looking great. Yeah I saw it right away. You mentioned the motion seemed stiff in another post. Are you getting things dialed in now?


---
**Eric Lien** *June 07, 2015 01:59*

Also just wanted to make sure you have the thin 10mm ID shims between the corner pulleys and the bearings. Some people forget those the first time and the pulley flange rubs on the bearing.﻿


---
**Bruce Lunde** *June 07, 2015 06:54*

Oops, I did miss those.


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/CizZ4bNSeCf) &mdash; content and formatting may not be reliable*
