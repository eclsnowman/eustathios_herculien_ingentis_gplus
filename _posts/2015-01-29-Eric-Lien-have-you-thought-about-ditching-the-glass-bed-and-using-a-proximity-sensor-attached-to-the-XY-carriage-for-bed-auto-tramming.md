---
layout: post
title: "Eric Lien have you thought about ditching the glass bed and using a proximity sensor attached to the XY carriage for bed auto tramming?"
date: January 29, 2015 16:19
category: "Discussion"
author: Daniel Salinas
---
**+Eric Lien** have you thought about ditching the glass bed and using a proximity sensor attached to the XY carriage for bed auto tramming?  I've never printed directly on aluminum but my initial research turns up that it's not bad.  Just curious.





**Daniel Salinas**

---
---
**Miguel Sánchez** *January 29, 2015 16:22*

I do print directly on aluminium heated bed (which does not stick at all) with hairspray on it. It works pretty good.


---
**Bruce Wattendorf** *January 29, 2015 16:38*

I have a 1/4 Steel plate on my Ultimaker origional with glass but I only print with PLA. the Sensor is about 4 times the trigger height as aluminum.  The only thing is the printer is twice as heavy. 


---
**Eric Lien** *January 29, 2015 16:54*

I haven't leveled my bed in two months. I feel letting the bed come up to temp and reach equilibrium, and using a properly ridged system make auto tramming unnecessary. To be honest I feel auto tram is a bandaid for pour design.



As far as printing without glass, I feel glass is less prone to damage with a head crash, easier to clean, less prone to warping, and down right just plain better. Combine that with the fact it can be easily removed, easily and cheaply replaced I see no benifit to printing on aluminum.



Just my 2cents.


---
**Daniel Salinas** *January 29, 2015 18:33*

cool, I noticed that the bed was adjustable and I have a ton of issues with my Robo3D with leveling.  I read that the Herculien/Ingentis/Eustathios beds were rock solid so I thought I'd ask.  If you don't have to level the bed more than once a month then I'd say it's probably not worth adding the auto-tramming into it.


---
**Eric Lien** *January 29, 2015 18:54*

The big thing is to allow things to equalize. Also changes in bed or head temp may require adjustment due to the thermal expansion coefficient of the parts: [http://www.engineeringtoolbox.com/linear-expansion-coefficients-d_95.html](http://www.engineeringtoolbox.com/linear-expansion-coefficients-d_95.html)


---
**James Rivera** *January 29, 2015 20:13*

My experience echoes **+Eric Lien**'s. Once I upgraded my Printrbot+ v2's bed to a machined aluminum plate (with glass on top) and manually trammed it (after heating everything up) I have not had to recalibrate it. And I'm fairly certain the Ingentis/Eustathios/HercuLien designs are all much more rigid, so they are even less likely to need it than my bot. In fact, this is why I have been so lazy about not implementing the induction sensor I bought for it months ago--it simply hasn't needed it. That being said, I do think it is a nifty add-on for countering minor aberrations, so I still plan to put it on there eventually (or on my next build).


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/fLFUReZGfcK) &mdash; content and formatting may not be reliable*
