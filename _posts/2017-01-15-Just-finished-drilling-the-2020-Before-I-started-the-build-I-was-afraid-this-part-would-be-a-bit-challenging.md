---
layout: post
title: "Just finished drilling the 2020. Before I started the build I was afraid this part would be a bit challenging"
date: January 15, 2017 09:09
category: "Show and Tell"
author: larry huang
---
Just finished drilling the 2020. Before I started the build I was afraid this part would be a bit challenging.  I used 6mm drill bit, two 6mm bearing, (four will be ideal), and a printed Guide. I had to pressing the bearing down so that they won't pop out when working, should find a way  to fix the bearing along Z axis.



![images/fd596e8372d931acd03b98d06bd5459e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fd596e8372d931acd03b98d06bd5459e.jpeg)
![images/69bfb2c7c06ca5193aac27632f5e8d77.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/69bfb2c7c06ca5193aac27632f5e8d77.jpeg)

**larry huang**

---
---
**Eric Lien** *January 16, 2017 15:45*

Great idea, well done. I used printed drill guides for HercuLien, but never thought about a bearing as the axis. Very smart.


---
**larry huang** *January 17, 2017 08:03*

Thank you. I like your printer design more when I began the build, the idea of securing the 2020 like this make the assemble a lot easier, fixing the bearing holder into place was very straight forward. 


---
*Imported from [Google+](https://plus.google.com/104789216831460398100/posts/3yf9iWVbX49) &mdash; content and formatting may not be reliable*
