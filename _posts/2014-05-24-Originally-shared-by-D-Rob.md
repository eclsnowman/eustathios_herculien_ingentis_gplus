---
layout: post
title: "Originally shared by D Rob"
date: May 24, 2014 23:15
category: "Deviations from Norm"
author: D Rob
---
<b>Originally shared by D Rob</b>

![images/46b302b2aae6034575c5e73cfb283086.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/46b302b2aae6034575c5e73cfb283086.png)



**D Rob**

---
---
**George Salgueiro** *May 24, 2014 23:21*

That is not the first time. They already have sued affinia. 


---
**D Rob** *May 24, 2014 23:25*

**+George Salgueiro**  true but in the persona of Stratysys. We have to do something before it happens to anyone else!


---
**Matt Miller** *May 25, 2014 00:22*

Honestly, this doesn't go far enough.  This issue has to be pushed into the faces of "maker" companies who partner with #takerbot.  I'm looking at you **+Adafruit Industries** and **+MAKE** and that's just the start.  We need to show that #takerbot is a pariah in the maker community.  



We need to hurt them where it counts - in their pocketbook. Affecting the bottom line of a company has a funny way getting their attention.



If nothing is done to resolve this I can see world maker faire turning into a shitstorm of protests against MBI and its business partners.



Only way out for #takerbot is to patent these things and then hand the patents over to the EFF or place them in the public domain so that no one holds these patents.  The probability of this happening is about zero though.

 

Oh, and screw **+Thingiverse** .  You upload it and they own it.  Says so in their ToS.  Don't believe the for a second that #takerbot has  a bone of "good faith" in its body.  


---
**George Salgueiro** *May 25, 2014 03:37*

Stop #takerbot #makerbot #patent 


---
**Lucas Carnevale** *July 17, 2014 06:42*

matt miller has the right angle on this. either find a way to hit their pockets or it wont even swing


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/auKzvKZnj4Y) &mdash; content and formatting may not be reliable*
