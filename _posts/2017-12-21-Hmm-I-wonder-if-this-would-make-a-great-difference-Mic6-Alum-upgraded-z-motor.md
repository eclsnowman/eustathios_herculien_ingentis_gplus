---
layout: post
title: "Hmm. I wonder if this would make a great difference Mic6 Alum upgraded z motor"
date: December 21, 2017 00:37
category: "Discussion"
author: Jim Stone
---
Hmm. I wonder if this would make a great difference



Mic6 Alum



upgraded z motor.



already using ball screws.



its basically the MDF insulator but in aluminum to be printed upon. and the insulator made smaller.



MDF no longer an influence on the thin aluminum 



Mic6 is the most even expanding



4 corners changed to SHCS from FH.



Should make for a more stable bed. being now that the MDF wont expand and contract all weird.



ive got/had 2 aluminum beds for the herc both have been influenced by the MDF insulator.



if the hammer doesnt work....get the bigger hammer. 



i present. the bigger hammer.



also looking at either using smoothie or upgrading to trinamic drivers ( the ones in the prusa?) for the z axis i may need a higher amp driver. due to the possibly more torquey motor needed



i will be needing a little help in choosing a stepper. as i dont know if holding torque ratings translate well to the rotational torque. or if the holding torque is 100% different.



as i understand it. holding torque is the force required to move the motor shaft when it is idle at a position. ie the force needed to make it skip a step



oh and the engraving is at the rear for the edge where the silicon bed wires come out. and i figured why not engrave a line for the "printable" area too.



may mill out a recess later for a bed material like printbite etc. so it lats flush. not 100% on that idea. as it means sticking with a adhesion medium. and i dont like the idea of being confined.



May also remove the "safe zone" engraving and try to figure out where a "prime area" could be



i bet this looks like rambling. wow this type window is tiny!



![images/22cafa0a6489814f4da2f0655c398161.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/22cafa0a6489814f4da2f0655c398161.jpeg)
![images/9c8b96c33578cc521f7305f1c98a48aa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9c8b96c33578cc521f7305f1c98a48aa.jpeg)

**Jim Stone**

---
---
**Jim Stone** *December 21, 2017 00:39*

this IS the print bed /heat spreader btw. no glass.



MDF on the under side where that pocket is. just to mitigate some radiant heat directly downwards.


---
**Eric Lien** *December 21, 2017 01:30*

I have my springs acting on the aluminum only (not the mdf). The mdf is screwed to the bed in two places... But where the springs act, it passes through an oversized hole in the mdf.


---
**Jim Stone** *December 21, 2017 01:34*

The pdf doesn't show those oversized holes on the 4 corners. It shows a counter bore. Which makes a sandwich of plate>nut>mdf>then idk what on the mdf side. Washer>spring>extrusion>wingnut ?

Even in that stack the mdf influences the aluminum :(






---
**Stefano Pagani (Stef_FPV)** *January 07, 2018 16:36*

The trinamic drives are so quiet! I love them


---
**Stefano Pagani (Stef_FPV)** *January 26, 2018 23:29*

You could have done way less extrudes...


---
**Jim Stone** *January 27, 2018 00:14*

Probably. But I'm not an aficionado at the software.


---
**Eric Lien** *January 27, 2018 00:52*

**+Jim Stone** I think the end result looked great. When you are new to software, it's about getting it to work for you. At first it fights you, later it helps you more quickly. Along the way you learn the path of least resistance. Don't worry about taking a less efficient way to the finish line at first.  Proficiency and speed come with time. 


---
**Jim Stone** *January 27, 2018 00:54*

yeah. it took the better part of 2 days to finish that XD



and i still think the pocket for the silicone heater is small




---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/i1m9bRJ1Uzo) &mdash; content and formatting may not be reliable*
