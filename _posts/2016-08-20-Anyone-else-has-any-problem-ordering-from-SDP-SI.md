---
layout: post
title: "Anyone else has any problem ordering from SDP-SI?"
date: August 20, 2016 18:24
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Anyone else has any problem ordering from SDP-SI? I placed an order last month and I haven't received any order confirmation or a shipping date. I also haven't been billed...





**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *August 20, 2016 18:35*

If it's for the bushings they can be bought from Ultibots and Lulzbot as well. 


---
**Stefano Pagani (Stef_FPV)** *August 20, 2016 20:08*

Thanks! I'll wait for now, I don't want to have $50 in bearings laying around unused.


---
**Eric Lien** *August 20, 2016 21:15*

If I were you I would contact them on Monday to find out the status of your order.


---
**Sean B** *August 21, 2016 03:27*

i had no problem ordering from them and received a confirmation email shortly after placing the order.


---
**Stefano Pagani (Stef_FPV)** *August 21, 2016 23:20*

Yeah I sent an email in sat I'll check on Monday. Weird about the email though


---
**Joe Spanier** *August 22, 2016 02:50*

Only problem I've ever had is being charged 20$ for chipping 10 bearings in a box the size of a baseball


---
**Stefano Pagani (Stef_FPV)** *August 26, 2016 17:52*

They said they had to restock the parts, but they were in stock weeks ago they just forgot to ship!


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/JUHfDCE4n7m) &mdash; content and formatting may not be reliable*
