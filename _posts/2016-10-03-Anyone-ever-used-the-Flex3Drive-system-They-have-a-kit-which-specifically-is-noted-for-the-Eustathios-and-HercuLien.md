---
layout: post
title: "Anyone ever used the Flex3Drive system. They have a kit which specifically is noted for the Eustathios and HercuLien?"
date: October 03, 2016 16:00
category: "Discussion"
author: jerryflyguy
---
Anyone ever used the Flex3Drive system. They have a kit which specifically is noted for the Eustathios and HercuLien? 









**jerryflyguy**

---
---
**Eric Lien** *October 03, 2016 16:09*

It would need a carriage designed for it to make it work. I would help in the design, but am swamp at work.


---
**jerryflyguy** *October 03, 2016 16:33*

**+Eric Lien** the kit comes w/ a carriage (or so it seems?)

[flex3drive.com - Flex3Drive – 4th Gen’ Micro for XY Gantry](https://flex3drive.com/product/flex3drive-4th-gen-micro-for-xy-gantry/)


---
**Eric Lien** *October 03, 2016 18:45*

**+jerryflyguy** I didn't know **+Mutley3D** had created the carriage already. NICE!


---
**Mutley3D** *October 03, 2016 18:57*

**+Eric Lien** **+jerryflyguy** it is an advanced work in progress :) CAD is the easy part, other life skills suck! :)

Core development of all the extruder variants (single and duals) was the hard part, but that side of things is 100% complete now.


---
**Mutley3D** *October 03, 2016 19:02*

In a few days some heavy work will be in full swing producing rapid results. Currently just clearing a backlog of orders that stacked up during the transition of the business and time taken by a last minute opportunity to exhibit at last weeks TCT show at the NEC in Birmingham UK.


---
**Jim Stone** *October 04, 2016 04:23*

omg. so youve got flex shaft that is that long and WORKS at that length? YAHOOOOOO so much closer to being able to use all my filaments on one machine!


---
**Mutley3D** *October 04, 2016 05:17*

**+Jim Stone** people tend to over estimate the length of the shaft that will be required. There are also a few tricks that can be used to optimise shaft length by positioning of the motor to minimise required length further. Ultimately the gear reduction of 40:1 reduces load to such a point that less than 1Ncm (0.01Nm) of torque is required to be transmitted along the shaft.


---
**Jim Stone** *October 04, 2016 05:35*

When I get the $ this is gonna run on my rig....if I can ever get my carriage apart lol.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/LmkQBWqRcq8) &mdash; content and formatting may not be reliable*
