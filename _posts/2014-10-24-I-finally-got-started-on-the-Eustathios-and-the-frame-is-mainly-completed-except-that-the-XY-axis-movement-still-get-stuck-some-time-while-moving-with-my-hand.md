---
layout: post
title: "I finally got started on the Eustathios and the frame is mainly completed except that the XY axis movement still get stuck some time while moving with my hand"
date: October 24, 2014 05:35
category: "Show and Tell"
author: BoonKuey Lee
---
I finally got started on the Eustathios and the frame is mainly completed except that  the XY axis movement still get stuck some time while moving with my hand. This is without the 2GT belt yet.



I am now working on the heated bed and before I purchase any of the items,  appreciate some feedback on the following

- the aluminium heated bed. 340x340mm aluminium T6061 4mm thick

- Alirubber silicon heated  300x300mm, 12v

- another thin aluminium 3mm plate on the Z axis bed ( I wanted to have spring connecting the aluminium plates so that I can adjust the bed level)



Is there any thing wrong on the layer of aluminium plates?  Most importantly, how do I power the heated bed. I plan to use utilmaker board  V1.57, 12V.  Is there enough amp to power the heated bed.



Also, anyone has a sample of the marlin firmware configuration file? I am familiar with Prusa i3 running RAMPS board with marlin.  But, I am not sure of the configuration for Eustathios.



![images/daf2d457f3bb2f2ebb5f289b1ba7804f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/daf2d457f3bb2f2ebb5f289b1ba7804f.jpeg)
![images/f92359081d8cb182ae6a58bafb4cc630.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f92359081d8cb182ae6a58bafb4cc630.jpeg)

**BoonKuey Lee**

---
---
**Eric Lien** *October 24, 2014 06:58*

Why do you need the lower plate?


---
**Daniel F** *October 24, 2014 07:08*

Congratulations on your build, it looks really stable.

Aluminium plates should be fine, I use one 6mm plate (350x350mm) for my heated bed. I would recommend to go 24V for such a big heated bed if you want to connect it directly to a pwm output of your board, as you need at least 300W to reach 100°C. My silicon heater measures 330x330mm and is rated 350W and it takes about 10min to reach 90°C and 15 min to reach 100°C (measured with the built in thermistor, it's less on the surface). If I could start over, I would go for a 400W heater pad. It also helps to isolate the lower side of the bed with rockwool or some other heat resistant material, it could be sandwiched between the two aluminium plates. with 12V you will get very high currents which might be a problem with your hardware. I use an arduino/ramps 1.4 with 24V and a 600W PSU.

The bed is connected to the heat bed PWM output .The Mosfet on my ramps can cope with the high current but I had to replace the fuses. I also got a problem with a screw terminal that burnt, because of bad contact/high current.

If you want to use 12V, a solid state relay could be a solution.


---
**Wayne Friedt** *October 24, 2014 09:23*

Yes a 24v heatbed or an AC powered one with a SSR may be even better depending on where you plan to use it.


---
**Mike Miller** *October 24, 2014 09:25*

You can eliminate one of the plates...the build documents inclue stl's for parts that attach to the ends of the H-frame and provide mounting points for capscrews and springs that then support the build plate. I'll share a photo with you. 


---
**BoonKuey Lee** *October 24, 2014 10:11*

**+Eric Lien** the second plate is to attach to the Frame and then it is connected to the bed so that i can adjust the bed level (see attached) I agree with **+Mike Miller** I should use printed part to attach to the H frame.



**+Mike Miller** I will go with a AC powered bed, what is the SSR? Where can I find more info?  How do I PID the bed with external AC?  I like to use 12V to power board.



Any info on the Marlin firmware configuration?


---
**BoonKuey Lee** *October 24, 2014 10:43*

**+Daniel F** thanks. Where can I find SSR info to wire it up. I will go for AC powered. 


---
**Daniel F** *October 24, 2014 11:22*

I think you need to look for Zero crossing SSR, something like this: [http://www.ebay.ch/itm/Panel-Mount-Solid-State-Relay-SSR-4-32VDC-0-380VAC-10A-Load-with-LED-Zero-Cross-/301337738453?pt=UK_BOI_Electrical_Components_Supplies_ET&hash=item462920fcd5](http://www.ebay.ch/itm/Panel-Mount-Solid-State-Relay-SSR-4-32VDC-0-380VAC-10A-Load-with-LED-Zero-Cross-/301337738453?pt=UK_BOI_Electrical_Components_Supplies_ET&hash=item462920fcd5)

With SSR You can't use PWM, use "bang-bang", see here: [http://forums.reprap.org/read.php?219,196602,page=2](http://forums.reprap.org/read.php?219,196602,page=2)

And be aware that if you use 110V or 240V AC for the heater there is a risk: if a wire gets loose and touches the frame it could kill you. For that reason you might consider to use a DC SSR with the 12V from your board on the input side and an isolated DC source of 24-50V DC on the load side. This is more expensive as you need a second power supply but it's safer.


---
**Mike Miller** *October 24, 2014 11:25*

I used a 12v Automotive relay. It ticks a little, but has worked just fine with the default settings in Marlin. 


---
**Eric Lien** *October 24, 2014 11:40*

**+BoonKuey Lee** I don't think you want a conductor like aluminum on the underside of the heater. If anything you want an insulator. Get a heater pad with adhesive backing. Stick it to the underside of your heat spreader (1/8" thick aluminum). The heat spreader is what will connect to the frame for leveling and also distribute the heat more evenly into the glass print surface.


---
**Mike Miller** *October 24, 2014 11:59*

While it's not perfect, and I plan on replacing it with a more precise piece of aluminum, the diamond plate is working surprisingly well. I use blue tape directly on it, no glass. At 100C, the tape peels off surprisingly well (and I've gotten really good at not getting burned.)


---
**Jean-Francois Couture** *October 24, 2014 12:48*

**+BoonKuey Lee**  I use a kapton incapsulated in silicon to heat my bed. its a 500W/120V best of all, you can order custom made size. mine is 230mmX300mm. its got a thermistor in the middle if the pad. look for Keenovo on ebay. Send him a email with your specs and he will send you a quote. I ordered 2 of them.



SSR = Solid state relay. basically, they are made with electronics and no moving parts (like a standard relay) look at it like a standard hard drive VS a SSD.



If you use a DC heatpad, you need a DC-DC SSR, else if using main's a DC-AC is required.


---
**Øystein Krog** *October 24, 2014 18:22*

I use a "SSR-40 DD DC-DC 40A" solid-state relay for my heatbed:

[http://www.ebay.com/itm/251525260933?_trksid=p2060778.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/251525260933?_trksid=p2060778.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)



It is also available in higher amperage-rating, e.g :

[http://www.ebay.com/itm/Solid-State-Relay-SSR-80A-DC3-32V-Control-DC5-220V-DC-Control-DC-Free-Shipping-/121030519651?pt=LH_DefaultDomain_0&hash=item1c2dfb2b63](http://www.ebay.com/itm/Solid-State-Relay-SSR-80A-DC3-32V-Control-DC5-220V-DC-Control-DC-Free-Shipping-/121030519651?pt=LH_DefaultDomain_0&hash=item1c2dfb2b63)



I run a normal MK2B pcb-bed at 30V, helps a lot with heat-up time.

It works very well in both bangbang and PID mode with my ramps board.



You might need a heatsink for large heatbeds, but with mine the relay does not even really get hot at all.


---
**Jean-Francois Couture** *October 24, 2014 18:26*

**+Øystein Krog** at 30V, with a 200W PCB rating, you're pumping 6.66amps. if the mosfet in the SSR is rated to 40Amps, its not breaking a sweat.. thats why its not hot (i case you did not know) ;-)


---
**Øystein Krog** *October 24, 2014 18:42*

**+Jean-Francois Couture** Hehe indeed.

I feed 30V to the 24 input on the MK2B, the measured resistance on mine is 4.8Ohms, so it's only ~166W).



166W is still a bit though and since there are lots of tales of fakes from china I was a bit worried, but this model seems to good.


---
*Imported from [Google+](https://plus.google.com/105655952080130147525/posts/BJtUP661cDn) &mdash; content and formatting may not be reliable*
