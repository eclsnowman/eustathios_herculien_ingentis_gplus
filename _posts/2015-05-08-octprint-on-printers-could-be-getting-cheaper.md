---
layout: post
title: "octprint on printers could be getting cheaper"
date: May 08, 2015 00:47
category: "Discussion"
author: Derek Schuetz
---
octprint on printers could be getting cheaper



[http://kck.st/1H3QfZj](http://kck.st/1H3QfZj)





**Derek Schuetz**

---
---
**Dat Chu** *May 08, 2015 12:30*

Has anyone used octoprint on wifi? I am a bit wary of shaky connection. 


---
**Liam Jackson** *May 08, 2015 13:42*

**+Dat Chu** works fine. The network connection is just for uploading files and pressing print so I don't see where the problem is? 


---
**Dat Chu** *May 08, 2015 15:43*

I see. I have only used wired connection with my Pi. Any one has inputs on processing speed of this allwinner soc vs the one on a pi 2?


---
**Liam Jackson** *May 08, 2015 15:54*

**+Dat Chu** the Pi 2 is much faster. But without the HDMI output board CHIP is more in the price range of the model A Pi, which it should be faster than. The Model A Pi will be better for video/camera stuff and have better quality software though. 


---
**Dat Chu** *May 08, 2015 16:00*

I think a chip would be a good board for gardening, telemetry and other projects that do not require a hdmi output. I m still deciding what I want before putting down the money. 


---
**Dat Chu** *May 08, 2015 16:02*

I also don't know if having to rewrite software for it is worth it. 


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/Y8fedSGYiF7) &mdash; content and formatting may not be reliable*
