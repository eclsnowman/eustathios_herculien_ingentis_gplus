---
layout: post
title: "Herculien: Z wobble/Ribbing. have tried re aligning the rods"
date: February 17, 2016 23:35
category: "Discussion"
author: Jim Stone
---
Herculien: Z wobble/Ribbing.



have tried re aligning the rods.



re squared the z vertical extrusions.



enabling PID on the bed ( didnt do anything didint bring them closeer...or farther)



im at a loss here



the printed parts when placed against the z lead screws seem to match perfectly for where the ribbing is.





**Jim Stone**

---
---
**Eric Lien** *February 18, 2016 00:08*

Did you ever tighten the lower lead screw mount forcing the plastic piece up against the vertical the vslot. If so it may have bent lead screws. The gap there is the part most people seem to miss and cause binding.  If you bind it or force it laterally it may bend them. 



Unplug the Z motor and see how difficult it is to send it up and down by hand. You should be able to move it very easily.


---
**Ted Huntington** *February 18, 2016 00:16*

Yeah that idea to unplug the motor and try it manually is the best one. Could be that your frame is not square- that happened to me until I realized that I had to add a screw through every hole in the extrusion- in particular the screws that go into the threaded holes. Make sure all parts are tightly fastened.


---
**Jim Stone** *February 18, 2016 00:31*

yeah i had them initially tighened right to the extrusion months ago =/

fack



anyone got a lead on golmart screws yet? i dont wanna buy from misumi again. was overly expensive.


---
**Eric Lien** *February 18, 2016 00:42*

**+Jim Stone** I can make up a print tonight so you can send it to them with the dimensions you'll need


---
**Jim Stone** *February 18, 2016 00:45*

thank you!


---
**Jim Stone** *February 18, 2016 04:52*

**+Eric Lien** and as per your bent idea. you may be right.



when pulling on the belt to either raise or lower with the motor not connected ( well unplugged motor side)



it will go loose tight loose tight loose tight. not really tight. but enough to know it had grabbed.



but that is in the top few inches. the rest of it feels smooth and mostly without effort.


---
**Eric Lien** *February 18, 2016 05:20*

Take off the top support and try again. You may just need to thin or shim the upper bearing mounts.


---
**Eric Lien** *February 18, 2016 05:21*

There's a lot of stacked tolerances on the lead screws so everything has to be pretty precise. I had to do this my first time around on the upper supports to get them perfectly aligned


---
**Eric Lien** *February 18, 2016 05:22*

It can actually run without the upper supports at all, I just use them so I can use faster z movements down.


---
**Eric Lien** *February 18, 2016 14:48*

**+Jim Stone** [http://i.imgur.com/tGOkEt2.png](http://i.imgur.com/tGOkEt2.png)



Please note there might need to be a few modifications to incorporate the ball nut along with the bed supports. I will have to investigate further.




---
**Jo Miller** *February 18, 2016 20:48*

I kept the delrinnuts "flying", on the aluminium- L-frame, with the two bolts of the Delrin Nut just loose attached ; the bed is heavy enough for that,  and no z-wobbling


---
**Jim Stone** *February 19, 2016 02:46*

No Change when upper supports removed.



"flying" nut idea used. no change.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/VudAf7RQyWN) &mdash; content and formatting may not be reliable*
