---
layout: post
title: "Michal Memeteau as requested ;) I'm way better at photography than video but it will do the job"
date: April 11, 2016 18:23
category: "Build Logs"
author: Maxime Favre
---
**+Michaël Memeteau** as requested ;)

I'm way better at photography than video but it will do the job. I forgot to shut down the radio, it going to spare you the bzzzzzbzzzzbzzzz thing :D

Sorry for the shaky table

![images/b1a1051663bbcb9b0c94a4e85f6ff212.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b1a1051663bbcb9b0c94a4e85f6ff212.gif)



**Maxime Favre**

---
---
**Maxime Favre** *April 11, 2016 18:33*

This particular setup is described here : [http://thrinter.com/cartesian-flying-extruder/](http://thrinter.com/cartesian-flying-extruder/)


---
**Bruce Lunde** *April 11, 2016 18:54*

The pivoting arm is Brilliant!


---
**Tomek Brzezinski** *April 11, 2016 19:17*

That looks super cool! Do you mind me asking:

(1) So how long of a bowden tube is this still? Meaning, the distance from extruder to hot end.

(2) Is the basic idea that those two aluminum pieces don't have a lot of inertia and the extruder doesn't need to be rigidly supported because it's not directly coupled? I.e, if it were rigidly connected to the hot end movements it would require more rigid supports to bear the moments from the various motions of the hot end? 



All this said, am I correct that the power requirements are still the same  or similar for moving the mass of the extruder? It's simply decoupled from effecting the behavior of the hot end



Thanks!

-Tomek


---
**Maxime Favre** *April 11, 2016 19:28*

Yes it's pretty cool to see it works 

1: The tube is 90mm

2: the main goal is to minimize inertia while placing the extruder the closest to the hot end.

Rigidity is not needed, it would be almost similar than fixing the extruder directly on the carriage with a little loss of weight due to the arm. The vertical tube act as a damper, the bungie cords limits infill and travel quick moves (see link above).

The arm is slightly preloaded to pull the carriage up.



All credits about this awesome thing goes to **+Walter Hsiao**  ;)


---
**Eric Lien** *April 11, 2016 19:44*

So cool. All of Walters photos look great. But seeing it in motion is a whole other animal. I will be watching this several times. It is mesmerizing :)


---
**Tomek Brzezinski** *April 11, 2016 19:45*

Thanks!



Sounds great :).  While it technically adds weight in a sense as you mention by the arm, by having that decoupled from the rigid sections of the hot end make me think it could be useful.  It doesn't seem to actually minimize inertia, insofar as you do mostly move the extruder everywhere the hot ends go, but it does change the nature/quality of it, and spread the load out a bit. And for very small movements it could eliminate the inertia. Maybe I'm wrong in my impression. 


---
**Maxime Favre** *April 11, 2016 19:52*

No I think you're right. There is inertia but since the arm follow and is quite dampened, it's not a big deal for the motors. It's something between bowden and direct.


---
**Sean B** *April 12, 2016 02:56*

Awesome video it's nice to see Walter's design in action!


---
**James Rivera** *April 13, 2016 17:34*

I'm no ME, but my $.02: the arm reduces the downward force of the added weight of the moving extruder, so this is good for preventing the smooth shafts of the cross gantry from flexing due to the additional weight (as compared to a more traditional bowden tube extruder setup for this type of gantry).  However, the <i>inertia is actually </i><i>increased</i>, because of the additional mass of the extruder <b>and</b> the arms, because their mass is being moved around, too.  Basically, it seems good for preventing downward flex, but at the expense of increased inertia. Still, it is probably the best way to go if you want to be able to print with flexible filament (which generally does not like bowden tube setups, or so I've read).


---
**Jim Stone** *April 14, 2016 21:37*

**+James Rivera**

That is exactly Correct. the x and y are still moving that mass. so the only thing it has stopped is downward force on the rods.



it is still moving the same amount of mass + the extra arms. as if the extruder were mounted to the carriage.



IMO does nothing but waste material. i would use an extruder mounted to the frame of the printer and run a bowden tube if you wanted to reduce the inertia imparted on the carriage.



Or you can have hte best of both worlds and run a Fl3xdrive which is essentially mounting the motor to the printer frame. and from there running a supported flex shaft to a sepcially designed extruder mech.



this method doesnt require a bowden tube. allows 3mm filament AND flexible filament easily. and it chops the extruder motor weight out of the equation.



part mentioned here [http://mutley3d.com/Flex3Drive-1/](http://mutley3d.com/Flex3Drive-1/)



he also has a material called printbite. rather awesome stuff!


---
**Eric Lien** *April 15, 2016 00:16*

I think the XY motors easily handle the extruder mass. Decoupling the extruder dampens the acceleration/force of the extra mass of the extruder. So it minimizes the ringing that would be caused by the extra head mass. I know it works, just look at the prints by **+Walter Hsiao**​. They are likely the cleanest prints I have seen from a Eustathios.


---
**Daniel F** *April 15, 2016 13:33*

Too bad I plan to build an enclosure around the Eustathios, the "elbow" would hit that enclosure. Maybe I try to fix the bowden at the top, this way lenght can be reduced to half (about 40cm). **+Walter Hsiao** mentioned that he was not happy with this solution but maybe it works with 3mm filament.


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/CJ8D3ELUGHA) &mdash; content and formatting may not be reliable*
