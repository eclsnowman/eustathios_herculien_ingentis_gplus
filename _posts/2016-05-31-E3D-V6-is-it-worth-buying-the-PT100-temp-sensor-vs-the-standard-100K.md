---
layout: post
title: "E3D V6- is it worth buying the PT100 temp sensor vs the standard 100K?"
date: May 31, 2016 06:19
category: "Discussion"
author: jerryflyguy
---
E3D V6- is it worth buying the PT100 temp sensor vs the standard 100K? If I'm using a Azteeg X5-Mini V3, I assume I'd also have to buy the PT100 Amplifier board as well?



Worth the extra money or naa? If I end up buying it later the shipping will double the cost.. So if it's worth getting.. Might as well do it up front.



[I'm just about to the end of my parts/ordering questions.. I swear :) ] 





**jerryflyguy**

---
---
**Pieter Swart** *May 31, 2016 08:53*

It's probably only worth buying if you're going to print above 300 degrees Celsius. If you can, try to get a stainless steel nozzle as well since some of the exotic filaments will eat through the standard brass nozzle.


---
**Jeff DeMaagd** *May 31, 2016 11:22*

The PT100 sensor is more rugged and can go above 300°C. You can go a very long ways with a standard thermistor though. I don't think I've ever damaged a thermistor on an E3D yet.


---
**jerryflyguy** *May 31, 2016 14:02*

Thanks guys, that answers my question.. For now I'll stick w/ the standard 100k. Do they normally ship w/ the fire-sleeve or do you buy that separate?


---
**Pieter Swart** *May 31, 2016 14:13*

When I bought my hot-ends from E3D, the sleaving came with the units. You probably need to buy the sleaving if your ordering loose parts.


---
**jerryflyguy** *May 31, 2016 14:14*

**+Pieter Swart** noted! I'll just ask Brandon before I order! ;)


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/55V9eFkHpiM) &mdash; content and formatting may not be reliable*
