---
layout: post
title: "So I'm finally at a point where I have a working derivative/custom Eust"
date: September 01, 2017 04:35
category: "Show and Tell"
author: Sonny Mounicou
---
So I'm finally at a point where I have a working derivative/custom Eust.  I'll have her printing tomorrow.  But I thought I'd post some pics tonight.  This one has rails for x/y/z.  It uses igus bearings for the rods.  The bearing supports, the carriage, and the x/y ends are all custom designed CNCed aluminum.  The bed is a druckerplate.  On the carriage, I'm using a titan extruder with a volcano nozzle.  It has a BLTouch mounted behind it.  I still need to design endstops, a fan duct, and increment the x/y ends one more time.  Oh, and I need to do something with wiring, its a mess.



![images/4eaa08b2efe77055ad2c398711ed8344.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4eaa08b2efe77055ad2c398711ed8344.jpeg)
![images/7db6e3170901fa73f3d7811e29a6b875.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7db6e3170901fa73f3d7811e29a6b875.jpeg)
![images/60162975a233971e1cc60eed698622db.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/60162975a233971e1cc60eed698622db.jpeg)
![images/3a6b6d94ead9a746b61d430497bd3301.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3a6b6d94ead9a746b61d430497bd3301.jpeg)

**Sonny Mounicou**

---
---
**Eric Lien** *September 01, 2017 05:17*

Looking very cool. One thing I will mention is that belt path at the back will need to get straightened to be parallel with the axis. Otherwise you will fight cosine error creating differential belt tension depending on where the carriage is located along the rod.

![images/939dd22efba24991d2c8384185681bf0.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/939dd22efba24991d2c8384185681bf0.png)


---
**Sonny Mounicou** *September 01, 2017 05:23*

Yeah.  Thats why I need to redo those ends.  I'm debating on flipping the rails sideways to get more Z.  What do you think?


---
**Ryan Carlyle** *September 01, 2017 19:14*

Neat build, thanks for sharing. I've been wondering why you don't see more cross gantries with rails like that. 



Don't rotate the rails 90 degrees, if that's what you meant. That'll make them pretty floppy. 


---
**Jeff DeMaagd** *September 02, 2017 18:27*

**+Ryan Carlyle** I think because crossing gantries went out of fashion before the style of rails got popular. I decoupled the rotational and linear on mine so all linear motion is on square rails. It works very well but it requires more rails than CoreXY.


---
**Alejo Cain** *September 03, 2017 01:35*

good job


---
**Aliou Djigue** *September 19, 2017 20:29*

Cool


---
**Gus Montoya** *November 12, 2017 07:08*

please share :)




---
*Imported from [Google+](https://plus.google.com/113710966260079086437/posts/Y6GtRbwk22v) &mdash; content and formatting may not be reliable*
