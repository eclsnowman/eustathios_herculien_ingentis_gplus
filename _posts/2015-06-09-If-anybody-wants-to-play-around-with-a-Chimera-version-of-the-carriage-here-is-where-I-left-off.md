---
layout: post
title: "If anybody wants to play around with a Chimera version of the carriage here is where I left off"
date: June 09, 2015 04:38
category: "Deviations from Norm"
author: Eric Lien
---
If anybody wants to play around with a Chimera version of the carriage here is where I left off. It is not my best work, and the part cooling duct looks like a bird beak... but it should work just fine. With some TLC it could be a pretty compact dual extrusion carriage for either HercuLien or Eustathios. It could work for Cylops too... but it would need removal of the beak on the part cooling duct.



**+Walter Hsiao**  could make something much prettier and less utilitarian than mine... I am sure of it. Every modification he has made so far to Eustathios is brilliant. So that's why I am throwing it out there, hoping someone better than me can get inspired and do something cool.



Anyway, I have STL, Solidworks, Step, and X_T here: [https://drive.google.com/folderview?id=0B1rU7sHY9d8qTjZHcjUwaVhDd1E&usp=sharing](https://drive.google.com/folderview?id=0B1rU7sHY9d8qTjZHcjUwaVhDd1E&usp=sharing)



Have Fun :)



![images/da53eeb7405c7aca9a673f0906174ae7.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/da53eeb7405c7aca9a673f0906174ae7.png)
![images/332133de8985317089aa388f7d9191cc.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/332133de8985317089aa388f7d9191cc.png)
![images/a9c9fdb8ae5000240007ef3977c702d5.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a9c9fdb8ae5000240007ef3977c702d5.png)
![images/8e03b01c85b959c37ba184e5993a3909.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8e03b01c85b959c37ba184e5993a3909.png)
![images/78dad5cde1a50412c95c32b865c2667e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/78dad5cde1a50412c95c32b865c2667e.png)
![images/2bb243f0510a761c585fc092a28ca522.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2bb243f0510a761c585fc092a28ca522.png)

**Eric Lien**

---
---
**Walter Hsiao** *June 09, 2015 09:20*

Thanks Eric!  You did all the hard work already, I've just been tweaking around the edges.  And doing so is so much easier with all the designs and files you've provided.



I probably won't do anything with a Chimera for a while since I don't have one and haven't really found dual extrusion to be something I want to do.  There's a lot of things I like about the Chimera though so I may play with one at some point.


---
**Mike Kelly (Mike Make)** *June 09, 2015 22:32*

That chimera looks nice on there :)


---
**Daniel Salinas** *June 10, 2015 18:10*

Omg I'm so excited. I was just about to put the eusta carriage on my herc. So glad I waited. 


---
**Daniel Salinas** *June 10, 2015 18:11*

Now I just need to order a chimera


---
**Gus Montoya** *June 10, 2015 18:11*

I wonder if this will work with e3d's cyclops hotend?


---
**Eric Lien** *June 10, 2015 18:12*

**+Daniel Salinas** should help gain a little more usable space on the HercuLien.


---
**Eric Lien** *June 10, 2015 20:54*

**+Gus Montoya** It would, but the part cooling duct would need some modifications.


---
**Stefano Pagani (Stef_FPV)** *June 24, 2016 17:08*

Anyone done anything with the Chimera and Volcano? I'm going to use it with a .4 Volcano and a .8 or 1, don't want things to get too leaky. +Eric Lien Does S3D support using one extruder for infill and another for shell?


---
**Stefano Pagani (Stef_FPV)** *July 11, 2016 02:13*

**+Walter Hsiao** Could you send me the file you have been working on? I'll improve it a bit more (if it needs be) and use it for my chimera.


---
**Brandon Cramer** *January 16, 2018 19:03*

This is an old post about dual extrusion... But I'm curious if anyone is doing this on a Eustathios Spider V2? What are the advantages of the chimera versus the cyclops? Would the X5 Mini be capable of doing dual extrusion? 


---
**Stefano Pagani (Stef_FPV)** *January 17, 2018 00:48*

**+Brandon Cramer** I think you can get a stepper driver shield for it.



I ran a chimera on it but then switch to a V6 because I didn't want all the mess of wiring and complications while building.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/RKJhQbBKPkz) &mdash; content and formatting may not be reliable*
