---
layout: post
title: "A couple of pics. Mounting the X-Y steppers below in the electronics area will allow open ended belts to be used"
date: May 30, 2014 03:41
category: "Show and Tell"
author: Brian Bland
---
A couple of pics.  Mounting the X-Y steppers below in the electronics area will allow open ended belts to be used.  Also a pic of the Kraken carriage.  Still not happy with it.



![images/4b6e3dd914182ee6fd286d0bcd2af1e9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4b6e3dd914182ee6fd286d0bcd2af1e9.jpeg)
![images/83e410cc9581a6a05cfb9540c31cff71.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/83e410cc9581a6a05cfb9540c31cff71.jpeg)

**Brian Bland**

---
---
**Chet Wyatt** *May 30, 2014 04:11*

So this is something I can use my over-sized can of Rem Oil for ;)


---
**Ethan Hall** *May 30, 2014 11:24*

By the looks of it that is a ingentis with belts for the x/y. Is that correct?


---
**Brian Bland** *May 30, 2014 16:27*

**+Ethan Hall** I started out building an Ingentis and switched over to the belt drive.  I am sure it will change even more by the time it starts printing.


---
**ThantiK** *May 30, 2014 21:41*

Honestly, those belt-drive ends could be much better too.  Someone just slapped large rectangular cubes onto the ends and called it a day.  I feel like we could get a better rod clamping system and belt attachment if someone had the time to work on them.


---
**Brian Bland** *May 30, 2014 21:44*

**+Tim Rastall** posted some he drew up, but I haven't had time to look at them yet.


---
**ThantiK** *June 05, 2014 01:31*

Did you post the files for these anywhere?


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/Dc3BuuCbUyr) &mdash; content and formatting may not be reliable*
