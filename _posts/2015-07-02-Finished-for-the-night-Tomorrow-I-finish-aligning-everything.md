---
layout: post
title: "Finished for the night. Tomorrow I finish aligning everything"
date: July 02, 2015 02:48
category: "Discussion"
author: Rick Sollie
---
Finished for the night. Tomorrow I finish aligning everything.



 Where can I find the assembly instructions on the V2 Spider? Or the ones for the Herculein(the gantry instructions should be similar...I hope)



Thx

![images/15953cfcac17a20f5d5cf7077196bff3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/15953cfcac17a20f5d5cf7077196bff3.jpeg)



**Rick Sollie**

---
---
**Eric Lien** *July 02, 2015 10:21*

None are written yet. But I would love some help in this regard.


---
**Rick Sollie** *July 02, 2015 23:59*

**+Eric Lien**

Even thought mine is a variant, I could create instructions as I go.  From the drawings I can't tell if the 8mm rods supporting the hotend have to be put in the same time as the 10mm x&y rods.  Can they be inserted later by slightly spinning the  support arms


---
**Eric Lien** *July 03, 2015 00:23*

On Eustathios the side carriage 8mm holes are deeper than needed. So the cross rods can be pushed into one side deeper than needed, then the other side rotated in, then the rod pulled back.


---
**Rick Sollie** *July 03, 2015 03:35*

**+Eric Lien**

Excellent, thanks for the info.


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/SVhjV3VHtKz) &mdash; content and formatting may not be reliable*
