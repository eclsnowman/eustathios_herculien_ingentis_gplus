---
layout: post
title: "A few Eustathios Spider V2 questions.... What are you wiring the 24V fan that cools the equipment to?"
date: August 05, 2015 19:03
category: "Discussion"
author: Brandon Cramer
---
A few Eustathios Spider V2 questions....



What are you wiring the 24V fan that cools the equipment to? Does this run the entire time the printer has power? 



There will be more questions soon... :)





**Brandon Cramer**

---
---
**Godwin Bangera** *August 05, 2015 20:14*

Electronics will def need active cooling, running all the time, best is to run one 80mm or a little bigger fan direct 110v for this ... which board are you running ?



i have found the XY axis and extruder steppers motors as well run peacefully if you mount a 40mm fan as well and cool them every few mins or keep them at a 60 -70% speed once a couple of layers are done.. i would especially in enclosed heated chamber situations,  ...

stepper motors can have an alu heat sink/or copper plates attached which help in keeping it cool ... i use heatsinks with fans for those steppers just as a precaution (i run my printers round the clock, with this setup :)  .. i also read somewhere you could use heatsinks with/without peltier modules .... so there will be no sound ..



pls try and look at Kühling & Kühling printer especially the pics .. they run a water cooling system on all their stepper motors, since they a use a controlled heated chamber which reaches close to to 70 - 80 degree C. (i think)



hope this helps


---
**Eric Lien** *August 05, 2015 20:36*

can you confirm what you mean by equipment? Board, hotend-heatsink, etc.


---
**Brandon Cramer** *August 05, 2015 20:37*

Equipment as in Raspberry Pi and Azteeg X5 mini. 


---
**Brandon Cramer** *August 05, 2015 20:39*

[https://www.dropbox.com/s/ri7fkimsgmnpk0z/Fan.JPG?dl=0](https://www.dropbox.com/s/ri7fkimsgmnpk0z/Fan.JPG?dl=0)


---
**Eric Lien** *August 05, 2015 20:49*

Yes run that all the time the printer is on. Just wire it directly to the 24V supply. Also I always meant to design a duct to direct the air down onto the X5 board, I just never got around to it. If anyone want to help a guy out and model that up I would appreciate it :)


---
**Brandon Cramer** *August 05, 2015 21:09*

**+Eric Lien**  I got it wired up and running now. :) I just wired it off the Azteeg X5 Mini Input.



One of these days I need to spend some time and learn how to design stuff so I can print it. 


---
**Erik Scott** *August 06, 2015 00:55*

**+Eric Lien** I was thinking the same thing. I'm on it. Going to be adding a mount for a cable carrying chain to the bed frame as well. Stand by. 


---
**Erik Scott** *August 06, 2015 02:08*

**+Eric Lien** Here's what I came up with: [http://i.imgur.com/KqwFSWR.png](http://i.imgur.com/KqwFSWR.png) 

Thoughts?


---
**Eric Lien** *August 06, 2015 02:28*

**+Erik Scott** oh Man. That looks great. You have a shot of it in assembly from a side view. Looks like it divides the air up to spread across the drivers?


---
**Brandon Cramer** *August 06, 2015 02:29*

That's awesome!!! Can't wait to print it and test it out!!!


---
**Erik Scott** *August 06, 2015 02:33*

**+Eric Lien** I wasn't thinking about the drivers so much as just getting air across the whole board. As long as air is moving, things will improve. But yes, that's the general idea. The air is split into two channels.



Here's the final assembly: [http://i.imgur.com/GkJbgDu.jpg](http://i.imgur.com/GkJbgDu.jpg)


---
**Eric Lien** *August 06, 2015 02:34*

Man that looks great. Awesome job. I am going to come to you for modeling now. You put me to shame.


---
**Erik Scott** *August 06, 2015 02:36*

I'd be happy to. I'm going to make a few more tweaks, sleep on it, and then I'll send out the final version. Glad you like it! 


---
**Frank “Helmi” Helmschrott** *August 06, 2015 04:39*

**+Erik Scott** good job - looks great.



**+Brandon Cramer** If the fan needs to be constantly on is mostly depending on your electronics. I'm using the RADDS board with the Arduino Due that has enough Power outputs to switch additional Fans. My Board cooling is automatically switching on when the motors are powered. This is the time when the drivers are producing heat and where's need to cool down. My E3D fan is activating as soon as the Hotend is powered or is warmer than 50°C so it runs when the hotend first switches on until it cools back down to below 50°C. This makes the printer nearly "sleep" when it not prints. I only need to work out a good solution for the PSU, something like a self-switching-mechanism would be good to switch the printer off after a long print :)


---
**Brandon Cramer** *August 06, 2015 04:44*

**+Frank Helmschrott** This fan is 24v so the only place to hook it up will be where there is constant 24v. There isn't a place to put it on Azteeg X5 mini that is 24v except the input. 


---
**Frank “Helmi” Helmschrott** *August 06, 2015 04:47*

**+Brandon Cramer** oh, ok - it don't know the Azteeg X5 mini so i didn't know how many powered outputs there are. The RADDS has 6 of them which can be used for Extruders, Heatbed and/or Fans [http://doku.radds.org/dokumentation/radds/-](http://doku.radds.org/dokumentation/radds/-) i also think you need to have Repetier Firmware running as the others don't yet support additional Fan control besides via GCODE but i may be wrong on that.


---
**Vic Catalasan** *August 06, 2015 07:05*

There is a setting in Marlin with a pin assignment that will turn a PWM fan on when there is stepper movement and will shut off after a period of time. Works well for me so far. Really do not need to run fan all the time since drivers do not heat when not holding or moving steppers or anything on the board.


---
**Frank “Helmi” Helmschrott** *August 06, 2015 07:49*

Oh, didn't know Marlin also supports this. Sounds like the same as in Repetier. Only switches the fan on when the motors are powered and if they unpower automatically after a timeout that is to be set in the EEPROM settings the fan turns off again. Comfortable feature.


---
**Vic Catalasan** *August 06, 2015 08:47*

At first I placed the fan on/off command on my start and stop Gcode in my pronterface now that it is in my boards firmware no more worries. Either way it works. You really only need a bit of airflow so running the fan on a PWM pin will let you set specific fan speed which is nice in reducing noise. 


---
**Brandon Cramer** *August 06, 2015 17:10*

The wire harness coming off of my Viki 2.0 LCD, where does this SDCD wire go? I don't see a pin labeled 2.13 or SDCD on my Azteeg X5 Mini. 



[https://www.dropbox.com/s/r6fsyd08l78h13q/2015-08-06%2010.04.30.jpg?dl=0](https://www.dropbox.com/s/r6fsyd08l78h13q/2015-08-06%2010.04.30.jpg?dl=0)


---
**Brandon Cramer** *August 07, 2015 04:28*

**+Erik Scott** Did you get a good night sleep? Do you think I could print that design and see if it works?


---
**Erik Scott** *August 07, 2015 04:33*

It's on the printer as we speak, but I'm off to bed now. Tomorrow evening I'll make a post with the models and some pics. You're farther along on your build, so I'd be interested in your input on how well it works. 


---
**Brandon Cramer** *August 07, 2015 04:36*

Sounds good. I appreciate you designing something for cooling the Azteeg X5 Mini. I'm sure it will work well. 


---
**Erik Scott** *August 07, 2015 13:39*

I checked it this morning. I got a lot of issues with the overhang at the top. I'm going to slow it down, add some support, and try again. I'll keep you posted. 


---
**Brandon Cramer** *August 11, 2015 06:05*

**+Erik Scott** I got that air duct printed out. Works good. Thank you!!! How is the cable chain coming along? I would like to put on the heated bed but need something that looks good to put the wiring in.



**+Eric Lien** I don't know if this is a limitation of Octoprint but in Repetier Host I can extrude at different speeds. I have my extruder motor set at 1/32 micro stepping. Not sure if that is right. 



Before the filament enters the extruder motor, do you use the Bowden tubing and if so do you tie it to anything? It doesn't seem right to let it just sit in mid air. 



On the fans. In Repetier Host I can set speeds for my print job. It doesn't seem like that is going to be possible with the Azteeg X5 mini. Am I missing something here? The other day when I had my main cooling duct fan running (white fan in the picture of my printer) and the fan for the extruder it didn't seem like it could get up to PLA operating temperatures. Will I be able to turn these fans on and off with the button in Octoprint? 



I think that is all. :)






---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/Sj199JPYpaW) &mdash; content and formatting may not be reliable*
