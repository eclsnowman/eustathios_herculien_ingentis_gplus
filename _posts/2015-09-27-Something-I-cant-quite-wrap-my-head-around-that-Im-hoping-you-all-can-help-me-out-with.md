---
layout: post
title: "Something I can't quite wrap my head around that I'm hoping you all can help me out with..."
date: September 27, 2015 21:35
category: "Discussion"
author: Zane Baird
---
Something I can't quite wrap my head around that I'm hoping you all can help me out with... Currently I'm using 4 start lead screws with a 2 mm pitch for an effective pitch of 8 mm (4*2mm). Using the Prusa calculator here ([http://prusaprinters.org/calculator/](http://prusaprinters.org/calculator/)) I get a 0.072mm step size for the 20 tooth motor drive gear coupled with 36 teeth gears driving the lead screws. While this seems like low resolution, it produces phenomenal prints. Especially considering that I'm using a Volcano hotend, so <100 micron layer heights were not my aim (however, 0.072 I know I can do at full steps on the z axis) , but fast printing was. Anyways, intuitively I would think that a 20 tooth gear on my motor with larger (>36 teeth) on the z-axis would mean more steps/mm that the 36 tooth gear, but the calculator says otherwise. What am I missing here? I really can't make sense of this, especially when I look at the gearing on my  bicycle... Can someone help me make sense of this?





**Zane Baird**

---
---
**Eric Lien** *September 27, 2015 21:52*

First number should be your motor pulley, second your lead pulley.  If I use that on the prusa calculator it works as I would expect increasing the lead pulley give more steps/mm.


---
**Walter Hsiao** *September 27, 2015 22:38*

The leadscrew steps per mm works as expected for me, but I've been inverting the gear ratio for the optimal layer height section, otherwise it doesn't make sense to me either.  Of course I may be doing this all wrong so I'm curious about this as well. 


---
**Whosa whatsis** *September 27, 2015 22:47*

BTW, a note on terminology, the <i>pitch</i> of a screw is the axial distance from one thread to the next. The axial distance to the next pass of the SAME thread, which is also how far a nut will travel along the screw for each 360 degrees of rotation, is called the <i>lead</i>. Pitch * starts = lead, so the screw has a pitch of 2mm, and a lead of 8mm. For single-start screws, pitch and lead are the same.


---
**Zane Baird** *September 27, 2015 23:31*

**+Eric Lien** I understand the terminology, but increasing the lead pulley tooth count gives me less steps/mm which is not intuitive to me. I would think that increasing that with the same motor pulley count would have the opposite effect. 


---
**Eric Lien** *September 28, 2015 00:05*

what I am saying is that it works correctly for me: [http://imgur.com/a/SFYBl](http://imgur.com/a/SFYBl)



higher number in the second box (lead pulley box) increases things correctly.


---
**Walter Hsiao** *September 28, 2015 04:07*

I assumed Zane was looking at the "Optimal layer height for your Z axis" section?  With that one increasing the second box increases the step length when I use it,  The steps per millimeter calculation works as I'd expect too.



[https://goo.gl/photos/6MnsrkxkuERXWH7x9](https://goo.gl/photos/6MnsrkxkuERXWH7x9)


---
**Zane Baird** *September 28, 2015 04:33*

**+Walter Hsiao** **+Eric Lien**  That's what I was referring to, the optimal height for the z-axis. It is confusing because it has worked perfectly for my 20 to 36 teeth motor to lead gear combo that I have currently, but increasing the lead gear tooth count acts counter-intuitively


---
**Whosa whatsis** *September 28, 2015 04:35*

That sounds right. Fewer steps per millimeter means that your distance per step increases (these numbers are inversely proportional). If your distance per step increases, you need to move in larger increments, and your optimal layer heights get further apart.


---
**Eric Lien** *September 28, 2015 05:38*

**+Walter Hsiao** yeah something is weird there. That is backwards on the optimal layer height calculator. A greater gear reduction should yield finer resolution on the Optimal layer height (full steps) calculator. They must have the gear ratio reversed on that one.


---
**Eric Moy** *September 28, 2015 09:32*

I hope I understand what you wrote correctly, but **+Whosa whatsis**​ hit the nail on the head, you don't need to calculate an effective pitch. Your pitch is 2mm period. Every time the lead screw turns 360 degrees your bed will travel 2mm regardless of how many starts. The number of starts just allows you 4 different clockings for any set height, which you don't care about. Hope that helps. That factor of 4 should definitely speed up your z axis in the right direction


---
**Zane Baird** *September 28, 2015 12:22*

**+Eric Moy** My question was not related to determining the axial distance traveled per turn as I know this is 8 mm. The definition of pitch is the axial distance from the crest of one thread to the next. With a 4 start lead screw this is still the pitch (2 mm), but the lead is 8 mm. My original question was in relation to the calculator on the last part of the linked page (full step layer height). On that calculator the gearing does not behave as expected when increasing tooth count for the lead screw gear.


---
**Walter Hsiao** *September 28, 2015 17:51*

I find it easier to calculate full step length directly from steps per mm, (number_of_microsteps / steps_per_mm = step_length).  Then I make sure to choose layer heights that are a multiple of the full step length.  That way you don't have to think about gear ratios or thread pitch.



My printers z-axis has 3200 steps per mm, which is 3200/16 = 200 full steps per mm, which is 1/200 = 0.005mm/full step.  My preferred layer height of 0.125mm results in .125/.005 = 25 full steps per layer.  I gave up a bit of speed on the z axis to get a z step length of 0.005, it makes it easy to pick layer heights without using a calculator, and lets me use layer heights of 1/10, 1/8, and 1/5 mm.  I don't exclusively use layer heights that divide evenly from 1mm, but I prefer them.


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/WR4UX37Cszh) &mdash; content and formatting may not be reliable*
