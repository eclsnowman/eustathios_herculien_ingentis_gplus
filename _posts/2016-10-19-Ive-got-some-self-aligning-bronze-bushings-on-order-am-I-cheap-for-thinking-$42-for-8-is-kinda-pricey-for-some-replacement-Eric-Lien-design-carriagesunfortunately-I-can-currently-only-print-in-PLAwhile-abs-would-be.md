---
layout: post
title: "I've got some self aligning bronze bushings on order (am I cheap for thinking $42 for 8 is kinda pricey?) for some replacement Eric Lien design carriages...unfortunately, I can currently only print in PLA...while abs would be"
date: October 19, 2016 12:41
category: "Discussion"
author: Mike Miller
---
I've got some self aligning bronze bushings on order (am I cheap for thinking $42 for 8 is kinda pricey?) for some replacement **+Eric Lien** design carriages...unfortunately, I can currently only print in PLA...while abs would be the ideal material, the printer I have available doesn't have a heated print bed. 



Once installed, how hard would it be to recover the bushings for re-use on an abs carriage? 





**Mike Miller**

---
---
**Maxime Favre** *October 19, 2016 13:57*

My printer is running with only PLA parts except the fan duct in petg since about 8 months without any problems. Like you I couldn't print ABS parts. The bushings can be removed quite easily by gently heating the PLA parts assuming you didn't do some heavy hammering to put them in place ;)


---
**Mike Miller** *October 19, 2016 14:00*

Cool...it's at this point I notice my cold-bed working printer has 8mm rods...doubling the cost for the 10mm  bushings..at least until I build another printer. I swear 40% of the cost of these things is waste due to ordering mistakes. 


---
**Oliver Seiler** *October 19, 2016 18:12*

Like **+Maxime Favre** I've been running a PLA carriage with a PETG duct for an extended time without any issues. Or you could print the whole carriage in PETG


---
**Roland Barenbrug** *October 29, 2016 13:40*

Printed all in PLA. Cut the old parts in 2 pieces and it's easy to push the bushings out in the inside out direction.




---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/UqvRMoC8Wwx) &mdash; content and formatting may not be reliable*
