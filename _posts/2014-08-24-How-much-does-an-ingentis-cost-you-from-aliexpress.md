---
layout: post
title: "How much does an ingentis cost you from aliexpress?"
date: August 24, 2014 11:16
category: "Deviations from Norm"
author: Matt Kraemer
---
How much does an ingentis cost you from aliexpress? Particularly, how much does 2020 extrusion and fasteners add to the price? **+Tim Rastall**



I'm working on a bot that copies your mechanics but in a heavily insulated mdf frame... total cost $20. (Not counting cutting costs on the new wood router)





**Matt Kraemer**

---
---
**Mike Miller** *August 24, 2014 12:12*

Mdf is cheap, and susceptible to warping due to moisture...but lots of printers have been made from it...mostly because it's cheap. 


---
**Joe Spanier** *August 24, 2014 13:15*

Keep it painted or don't get it wet it doesn't warp. It moves a bit with humidity but overall if you treat MDF right it will be stable and strong for pretty much ever. Leave your printer in the rain though.... Lol


---
**Matt Kraemer** *August 24, 2014 18:33*

**+Shauki Bagdadi** , in what way are your mechanics divergent? ﻿



My understanding is that your machines are also derivatives of ingentis? So don't be jealous. My new design is going to be using resin cast and wood routered parts for ease of production and minimal cost. I'm not trying to make something that anyone can make.


---
**Tim Rastall** *August 24, 2014 20:48*

**+Matt Kraemer** hmmm,  i think ive lost track of the total cost what with moving to gt2 pulleys etc but the extrusions and fasteners would amount to about $110 nzd including shipping. big costs kick in with the electronics and heaters and whatnot.

FYI.  My new bot is using lm10luu and lm12luu bearings on non rotating shafts which means I don't need as many belts or motors protruding from the frame to achieve direct drive. Or have to worry about getting self aligning bushings to align :). Also, not a single printed part. All made of extrusions and aluminum plates.


---
**Matt Kraemer** *August 24, 2014 22:16*

**+Tim Rastall** interesting. That's quite a bit cheaper than I expected. 


---
**Tim Rastall** *August 25, 2014 06:29*

I followed and took inspiration from the Quadrap blog as I was building ingentis if I recall correctly. There are lessons, advantages and disadvantages for both designs. **+Shauki Bagdadi**s robots are very much descendants of Quadrap and not ingentis I think.


---
**X Copter** *May 26, 2015 08:49*

**+Tim Rastall** Are you in NZ too?? Where did you source your extrusions and what not?


---
**Tim Rastall** *May 26, 2015 10:02*

**+X Copter** yup.  Let me introduce you to **+Oliver Seiler**​ (also in nz) who has most recently sourced some extrusions from China.


---
**X Copter** *May 26, 2015 10:08*

Oh cool O.o

I really really like this design but I'm not a fan of the build volume. It's a bit big for me (slow heat up times would annoy me). How hard would it be to redesign it so it's around a 225mmx225mm bed?


---
**Tim Rastall** *May 26, 2015 10:28*

Very straight forward for x and Y.  Less so for Z.  For X and Y,  you just need shorter extrusions and shafts. 


---
**X Copter** *May 26, 2015 10:29*

Yeah, it's the bed that worries me. Why would it be hard to shorten the z axis?


---
*Imported from [Google+](https://plus.google.com/115849100560366423247/posts/FNSZg5Fvpys) &mdash; content and formatting may not be reliable*
