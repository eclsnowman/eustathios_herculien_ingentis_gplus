---
layout: post
title: "Hey guys, I've been fighting ball screws"
date: August 28, 2016 16:55
category: "Discussion"
author: Brandon Satterfield
---
Hey guys, I've been fighting ball screws. Most request I get for them come from this group. Through Eric Lien I learned that I cannot chuck on these in a lathe with a 3 or 4 jaw, so I ordered 5C collets. After researching I found I had to anneal them, then manually grind off the threads to remove the interrupted cut. One gentleman has been pretty patient with me, and after holding for a while I got the right feeds and speeds and made a set for him. The cuts were accurate and beautiful. 



Post this I laid them up in my machined V blocks and started checking run out. There was a .1mm run out noted on all ends. I spent hours trying to figure out why. The 12mm 5C collet I ordered has a flaw, the center is .05mm off set. Hard to put into words how upset I was. 8 hours of my time, holding a builder up, an expensive 5C collet and 3 trashed ball screws later, my blood pressure was not in a healthy zone.



I want to get this gentleman a set or at least reference him to a provider that can ensure the same quality I would expect in a production part. Where are those of you that are making the ball screw hack getting yours from? Hate outsourcing, but I do not want to hold this gentleman's build up any longer while I fight with the collet manufacturer. 







**Brandon Satterfield**

---
---
**Mike Miller** *August 28, 2016 17:08*

I can't help, but I can feel your pain. 


---
**Eric Lien** *August 28, 2016 17:09*

[http://www.aliexpress.com/store/product/Best-quality-1pc-Ball-screw-SFU1204-L425mm-1pc-RM1204-Ballscrew-Ballnut-standard-processing-for-CNC-BK10/920371_32592258963.html](http://www.aliexpress.com/store/product/Best-quality-1pc-Ball-screw-SFU1204-L425mm-1pc-RM1204-Ballscrew-Ballnut-standard-processing-for-CNC-BK10/920371_32592258963.html)




---
**Isaac Arciaga** *August 28, 2016 17:56*

**+Ryan Jennings** where did you purchase yours?


---
**jerryflyguy** *August 28, 2016 18:36*

**+Brandon Satterfield** I bot mine from you then got a local shop to machine them. Ballscrews are difficult w/ the outer hardness. They are typically like butter under the hardening. In larger screws taking a deeper first cut is the trick. But w/ units as small as this, taking the deeper first cut becomes a delicate balance.  


---
**Ryan Jennings** *August 28, 2016 21:44*

**+Isaac Arciaga** the link **+Eric Lien**​ posted is where i got mine


---
**Brandon Satterfield** *August 29, 2016 13:07*

Thanks guys, I linked the gentleman to this post. Hopefully he can use these links to get up and running till I can get the things hammered out. 

Agreed on the annealing it made a world of difference in the amount I could take out per pass. The whole set up is on CNC now, first attempts were on a manual lathe. Annealing to temp and allowing to air cool made for precise and beautiful cuts.... Well till I found the stupid issue with the collet. What's awesome is the bottom of the collet box had a QC check paper not filled out. Sometimes we just don't win. 


---
*Imported from [Google+](https://plus.google.com/111137178782983474427/posts/hpqFWLKqBTG) &mdash; content and formatting may not be reliable*
