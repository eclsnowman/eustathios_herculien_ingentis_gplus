---
layout: post
title: "Can someone please help me convert a pdf of the ultimaker electronics 2.1.1 into a gerber compatible format?"
date: March 17, 2014 22:32
category: "Discussion"
author: D Rob
---
Can someone please help me convert a pdf of the ultimaker electronics 2.1.1 into a gerber compatible format? I cant use pdf 2 gerb as it doesnt support 4 layer pcb. I want to maybe get a crowd fund going and get some pcbs. I'm really keen on building my first pcb from scratch and I'love this to be the board. If you cant help but know someone who can pass this along. I found a service for this type of thing but they want over 1000usd for the gerbers





**D Rob**

---
---
**Riley Porter (ril3y)** *March 17, 2014 22:34*

I am not sure why you would want pdfs if you were going to build your own pcbs?



However, This I think will do what you want.

[http://sourceforge.net/projects/gerbv/](http://sourceforge.net/projects/gerbv/)


---
**D Rob** *March 17, 2014 22:36*

they posted the board on github as a pdf. I need gerbers to get the boards made


---
**Riley Porter (ril3y)** *March 17, 2014 22:39*

Oh what is their license? Perhaps to stop people from cloning? Not sure of another reason they would do pdfs.


---
**D Rob** *March 17, 2014 22:43*

[http://creativecommons.org/licenses/by-nc/3.0](http://creativecommons.org/licenses/by-nc/3.0)


---
**D Rob** *March 17, 2014 22:44*

[https://github.com/Ultimaker/Ultimaker2](https://github.com/Ultimaker/Ultimaker2)


---
**Eric Lien** *March 18, 2014 06:02*

Does This Help At All? I Converted The Board To DXF from PDF:



[https://drive.google.com/file/d/0B1rU7sHY9d8qb2NwdVRJaHFHRVk/edit?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qb2NwdVRJaHFHRVk/edit?usp=sharing)


---
**D Rob** *March 18, 2014 06:20*

idk yet but ill definitely explore this avenue. thanks man!


---
**D Rob** *March 18, 2014 06:31*


{% include youtubePlayer.html id=OYGJ-N3mrsc %}
[Convert PDF to PCB Gerber data - Avoid Raster PDF files](https://www.youtube.com/watch?v=OYGJ-N3mrsc) maybe what I need


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/4FRbnXsReQC) &mdash; content and formatting may not be reliable*
