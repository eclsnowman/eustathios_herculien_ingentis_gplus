---
layout: post
title: "OK, I've put off ordering parts long enough and I need to get a new printer started"
date: December 18, 2016 19:17
category: "Discussion"
author: Nathan Fisher
---
OK, I've put off ordering parts long enough and I need to get a new printer started. My only hesitation is that I want to go as large as possible but I'm not sure what the limits of this style of printer would be. Maybe i'm better off looking at a different design?  Any suggestions? 





**Nathan Fisher**

---
---
**Eric Lien** *December 18, 2016 23:08*

Describe how big you want to go and we can better address which avenue might be best to proceed with.


---
**Nathan Fisher** *December 19, 2016 01:00*

Well I'm thinking maybe it's a good idea to first build one just a bit larger then work on going larger. Maybe try for 18"x18" on X and Y at first. But I would eventually like to go as large possible and still fit through a standard doorway. Not sure what would be practical. 


---
**Nathan Fisher** *December 19, 2016 03:28*

24x24x24 would be awesome. Right now i run 2 tweaked Robo R1 printers. One 14.5"x9"x8"  So maybe replacing them with a normal sized herculien and then working on something larger would make the most sense. So many ways to go about this!


---
**Eric Lien** *December 19, 2016 04:51*

Yeah I would say much more than a few inch increase in HercuLien on the stock 10mm cross rods would be ill advised. For larger spans you will want cross beams with a greater cross section since your maximum deflection is a function of the span length squared. If you are going to scale up a lot, look at the #quadrap G+ group. Something like the Black Pather QR should scale up further than HercuLien.



Just remember component quality and calibration time for larger printers is non-linear with smaller printers. Anybody can make an 200x200x200mm printer work well. It is an order of magnitude more difficult to scale up something to print at 0.1mm layer heights on printers that span 2ftx2ft. Errors become compounded over such distances.


---
**Nathan Fisher** *December 19, 2016 05:04*

Good advice.  Would it make sense to step up to 12mm rails? Maybe building a normal Herculien is the best starting point and then i can figure out what to do different to go bigger. 


---
**Eric Lien** *December 19, 2016 07:06*

**+nathan fisher** I might go down that route first. Going big is not for the faint of heart :)



Also I have been splitting prints into smaller chunks and doing post print assembly recently. The cost of failure goes up as parts get massive... As does print time.


---
**Nathan Fisher** *December 22, 2016 05:08*

Well i think i'm going to try to build on your idea and see what i can do.  I've modeled up the frame with 1.5"x1.5" and 3"x1.5" 80/20 extrusion. Outside dimensions of 27x27x24 but may go taller.  Going to machine everything i can, use 12mm rods. Maybe linear rails if it makes sense. we'll see. Should be interesting.

![images/becf6a61412dd72ef299ea7558fb1e1a.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/becf6a61412dd72ef299ea7558fb1e1a.png)


---
**Nathan Fisher** *December 23, 2016 13:09*

Not quite as pretty as yours but it's a start. I may need to find a new community to join now but any input on using rods like you used but swapping in 2 600mm rails like this? Still may move the rod bearings back to the inside corner but won't have access to catia until Tuesday. 

![images/21c525f4a81d8808a8c3de5224298d68.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/21c525f4a81d8808a8c3de5224298d68.png)


---
*Imported from [Google+](https://plus.google.com/113760767936457744939/posts/G2oSFfAddP3) &mdash; content and formatting may not be reliable*
