---
layout: post
title: "How do you guys tune your axis positions and movement of X and Y axis when building your printer?"
date: July 16, 2015 06:08
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
How do you guys tune your axis positions and movement of X and Y axis when building your printer? I just wonder as i want to make sure X/Y movement is as good as possible before starting to give it a go with the motors? For now i think it's a bit strong to move by hand but i don't have a real comparison. I've built a classic Ultimaker some 2 years ago and think it was about the same back then, but well... i thought i could improve it a bit possibly.





**Frank “Helmi” Helmschrott**

---
---
**Øystein Krog** *July 16, 2015 09:13*

I don't tune X, Y and Z movement, I calculate it directly from the mechanics.

[https://github.com/oysteinkrog/Marlin/blob/config_i3_working/Marlin/Configuration.h](https://github.com/oysteinkrog/Marlin/blob/config_i3_working/Marlin/Configuration.h)


---
**Eric Lien** *July 16, 2015 10:59*

It should move very freely by hand once aligned properly and broken in. But running the break-in code helps so things settle in. After running the break-in code check for binding when the carriage goes to the corners of it's motion and feel by hand for any binding, then adjust as needed. It takes a while, but when it is right you will know by feel after some practice. Once everything is aligned and tightened you should never have to mess with it again.


---
**Frank “Helmi” Helmschrott** *July 16, 2015 11:04*

**+Øystein Krog** sorry for beeing unprecise here - with tuning i was talking about the alignment, not the steps/mm - you're of course right with that, though i won't use Marlin :)



**+Eric Lien** thanks, Mate - will follow that and recheck everything. Frame should be near-to-perfect aligned already but will double check the rods.


---
**Øystein Krog** *July 16, 2015 11:05*

**+Frank Helmschrott** Ah, my bad actually, I did not see this was in this community:P


---
**Eric Lien** *July 16, 2015 11:18*

**+Frank Helmschrott**​ yeah C-C on the side rods compared to what the carriage actually printed at (printed parts are inherently imperfect dimensionally), and squaring the cross rods to the side rods is your main objective. If those are right... You will have smooth motion. Also confirm the side rod pulleys aren't rubbing on the corner bearings. Most people forget the 10mm ID shim washers between the pulley and the bearing the first time through. The Robotdigg pulleys have no shoulder on that side of the pulley, so without the shim... They can rub.﻿


---
**Frank “Helmi” Helmschrott** *July 16, 2015 11:22*

**+Eric Lien** i think they're spaced a bit but i will check that when i'm in the shop later today. Thanks for pointing that out.


---
**Eric Lien** *July 16, 2015 12:29*

Yah, you need the pulleys up tight to the bearing so the rod doesn't slide back and forth. The pulley acts as a collar. That's why the shim is needed (a thin one, 1mm thick or less). Also make sure the end of the rod doesn't rub on the inside face of the plastic corner bearings mount.﻿


---
**Mike Miller** *July 16, 2015 15:14*

Also, movement is difficult, by hand, if you don't have the outside belts and pull ices installed...they're REALLY necessary to maintain perpendicularity. 


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/KRkJTfNohKU) &mdash; content and formatting may not be reliable*
