---
layout: post
title: "MISUMI USA are you able to tell me why you can't ship to Australia?"
date: March 14, 2014 23:27
category: "Discussion"
author: Daniel Fielding
---
**+MISUMI USA** are you able to tell me why you can't ship to Australia? :(





**Daniel Fielding**

---
---
**Tim Rastall** *March 14, 2014 23:30*

**+Daniel fielding** in my experience many US distributors don't ship overseas. In this case I thinks because there's an Apac division of MISUMI. Or you can use reship.Com like I do. 


---
**Daniel Fielding** *March 14, 2014 23:32*

What of sort money are we talking for [reship.com](http://reship.com) and have you found it easy to use


---
**Wayne Friedt** *March 14, 2014 23:47*

I had never head of this. Glad i have now tho as i am currently living in The Philippines and have had some trouble with shipping here.


---
**Tim Rastall** *March 15, 2014 08:52*

**+Daniel fielding** you can get quotes for the shipping.  Their overheads are pretty low.  They will also consolidate packages,  remove invoices etc. 


---
*Imported from [Google+](https://plus.google.com/110967506247660788897/posts/ZRWkJsHT2nE) &mdash; content and formatting may not be reliable*
