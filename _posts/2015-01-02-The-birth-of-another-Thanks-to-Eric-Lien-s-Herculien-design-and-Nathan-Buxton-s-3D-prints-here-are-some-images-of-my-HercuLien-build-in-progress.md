---
layout: post
title: "The birth of another :) Thanks to Eric Lien 's Herculien design and Nathan Buxton 's 3D prints, here are some images of my HercuLien build in progress"
date: January 02, 2015 20:13
category: "Show and Tell"
author: Marc McDonald
---
<b>The birth of another  </b>#HercuLien<b>   :)</b>

Thanks to **+Eric Lien**'s Herculien design and **+Nathan Buxton**'s 3D prints, here are some images of my HercuLien build in progress. I missed some bearings and a belt or two from orders so progress has slowed. I plan to post some photos of completed machine. This machine will use the  #smoothieboard  and  #OctoPrint . 



![images/998f5b77964a992e098b758e86fb74ba.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/998f5b77964a992e098b758e86fb74ba.jpeg)
![images/700be3fa0c81f78f10067aaf04fd5070.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/700be3fa0c81f78f10067aaf04fd5070.jpeg)
![images/f26367d77d632e5b72c1119699016888.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f26367d77d632e5b72c1119699016888.jpeg)
![images/2bbe9b0571b76582af1a49c3f51f58ee.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2bbe9b0571b76582af1a49c3f51f58ee.jpeg)
![images/56241c9d51d19e3959aeb13e06f7aa62.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/56241c9d51d19e3959aeb13e06f7aa62.jpeg)
![images/cae17c87047978aaf03660aa15e07ce7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cae17c87047978aaf03660aa15e07ce7.jpeg)

**Marc McDonald**

---
---
**Eric Lien** *January 02, 2015 21:08*

Great work. It looks clean :)



Now I'm jealous. If I works well You might get me to convert to smoothie to see If I can improve high speed printing :)


---
**Eric Lien** *January 02, 2015 21:10*

Let me know if you can improve the part ducts. I was never happy with my design (the shadowed side from the blower needs better cooling).


---
**Brad Hopper** *January 02, 2015 21:29*

Looks very nice! I wonder what is the large chamber on top for? Does it print up there or only in the lower half?


---
**Jim Wilson** *January 02, 2015 21:49*

I've seen this done to trap heat in the past, but that IS a large attic going on there.﻿


---
**Eric Lien** *January 02, 2015 23:35*

I think I had it modeled at 12 inches high to allow for capturing heat and also for cabling and Bowden tube travel. With a long bowden tube like this you want this gentle an arc as possible.


---
**Eric Lien** *January 03, 2015 00:39*

These pictures are great. To be honest I am worried you have a better looking version of my printer than I do.


---
**Marc McDonald** *January 03, 2015 00:50*

**+Eric Lien** **+Brad Hopper** **+Jim Wilson** Thanks for all the comments, I will post more as build progress continues.

looking at the pictures, the picture showing the Top was taken at an angle and seems to make the top look higher than 12 inches.


---
**Tim Rastall** *January 03, 2015 09:21*

**+Eric Lien** That is just the way it goes - second iterations and all that.


---
**Eric Lien** *January 03, 2015 19:47*

**+Marc McDonald**​ I just noticed you mimicked my mistake of the four extra tee nuts on the z-bed drive 20x80 horizontal base extrusion. I had put them in the wrong row to act as the nuts for the two sliding tensioner idlers. I have been meaning to remove them from mine, but I did the same as you and just put bolts in to hold them still so they don't rattle. 



Sorry ;)


---
**Eric Lien** *January 03, 2015 22:15*

**+Oliver Schönrock** yes it is vslot on the z-guides. It works well and the chamber never gets over 50C from the passive heat from the heated bed so delrin works fine. But openbuilds does have PC wheels which might help ridgidity.



Another improvement that could be made is a plate on the outside of the bolts so the wheels are not cantelievered on the bolts... But then you can run into interference issues with the main box frame and that plate.



I can apply a fair amount of force to the bed without deflection, so it performs very well as is. But spacing the wheels apart more could improve that even further.



Hope that helps.


---
**Eric Lien** *January 07, 2015 17:18*

**+Oliver Schönrock**​ I am not running a smoothie board, but with the additional network jack it can run a basic webserver for jogging and other tasks.


---
**Marc McDonald** *January 20, 2015 06:41*

**+Eric Lien** **+Oliver Schönrock** The  #HercuLien   is operational using the  #smoothieboard  and  #octoprint   and the first printed parts in ABS look good. Next will be some tuning and increase of print speed.



I have a  #bondtech  extruder installed and operational and am very impressed with the quality and operation. Will definitely consider a second unit when production units are available, at this time I have the Hercstruder on the second hotend.


---
**Marc McDonald** *January 20, 2015 07:25*

**+Oliver Schönrock** Octoprint on the Pi as I have a few Pi available and do not want to add the networking tasks to the smoothie.


---
**Eric Lien** *January 20, 2015 12:43*

**+Marc McDonald** Great news. keep us posted.


---
**Nathan Buxton** *January 21, 2015 00:05*

Awesome news! Did you have any other troubles with the parts? I'm thinking of building a HercuLien myself now.


---
**Eric Lien** *January 21, 2015 00:11*

**+Marc McDonald**​ do you think I should drop the height of the lid at all?


---
**Marc McDonald** *January 21, 2015 01:32*

**+Eric Lien** At this time I am not using the lid, that will be next after I have resolved a small vibration in the Y axis(does not seem to affect the print quality at this time), the x axis works great. I plan to add some type of arch support for the boden/control/power cables to the inside of the lid, I have not verified if the height can be reduced at this time.  



One item I would consider changing is to install the side walls outside of the frame so they can be easily removed so you can gain access to all parts of the  #herculien  as it can be challenging with the current design. This may also reduce some vibration of the side panels that I have noticed.


---
**Marc McDonald** *January 21, 2015 01:44*

**+Nathan Buxton** at this time the double carriage E3DV6 seems have had too much extrusion that resulted in a slight misalignment of the Y axis bushings causing the small vibration I observe. I removed the over extrusion using a CNC but may not have got the alignment exactly correct when I flipped the part in the CNC for the other side of the Y axis. I will see when it is replaced (waiting for new bushings to arrive) if this resolves the small vibration. You need to watch the extrusion amount on all bushing cavities.


---
**Eric Lien** *January 21, 2015 02:28*

**+Marc McDonald**​​ I know what you mean about accessibility. I thought about outside panels since that is how my corexy was. But I was torn because the drive motors ride on the outside. Also in retrospect thicker panels would be better.



For the carriage bushings that's why I print in abs. Easy clean out with a file, or just wipe the hole with a qtip soaked in acetone. That softens it to allow the bushing to press in easy. Plus it has the effect if glueing in the bushing.



For vibration did you run a few hours if break in gcode? For these bushing style machines it is almost a must. Also after installing the center carriage bushings guide the rod through and align the bushings since they have misalignment capacity of 5deg built in. Then put the rod in a drill and spin it while the sliding the carriage up an down. That seats everything nicely. Do the same for all the rod ends.



For arch support I have a small section of thin flexible rod (coat hanger) where the cables enter the chamber. Drill a small diameter hole larger than the hanger wire in the top of the main frame at the center of where the cable entry cutout is in the lid. If you put a small triple 45 bend in the coat hanger and a washer it cannot fall too far into the hole and pivots nicely. Guide the cable in a gentle arc terminating the hanger rod a few inches from the top inside of the lid and before the apex of the cable arch. That allows the cables to reach anywhere they want... But holds the midpoint of the cable in the air.﻿


---
**Nathan Buxton** *January 21, 2015 03:18*

Very interesting stuff. **+Marc McDonald** if you'd like me to reprint that part for you I'd be happy to do that for you at no cost. I no longer have Red, though. I have dialed my extrusion steps/mm by 0.2mm compared to how it was before. I've also got a finer nozzle (.35mm) as opposed to the 0.5mm I had before. Maybe it will print better? I have noticed, though, with parts so big, that the differential cooling rates of the plastic REALLY do start to take effect. So just the less extrusion might not solve the problem entirely. I've also adjusted some other calibration values on my machine since I printed your parts. Not sure if it's better or worse, but it's printing. :)


---
**Marc McDonald** *January 21, 2015 03:26*

**+Eric Lien** Thanks for the information. The panels rattle slightly 3mm used with the spacer plastic (have added a few pieces of clear plastic to reduce the rattle - work well).



I been running the the break in code for several hours over a few days and noticed improvement each time I ran it. I will continue to run it a few more hours in between my tuning sessions. Now it is down to a very small vibration on the Y axis, X axis is great (no vibration). 



Nathan printed all the parts in ABS for me but seemed to have more extrusion in the bushing/bearing locations. There was far too much over extrusion in the double carriage to soften and push in the bushing so I CNC bored them out to a close fit. I have new part ready (waiting for another set of bushings to arrive) and will try the acetone soften method.



I did attempt a similar approach (flexible fibreglass rod) but removed in favour of an elastic band support while I work on improving the Y axis. I will try your mounting method next.


---
**Eric Lien** *January 21, 2015 03:52*

I am loving the feedback, keep it coming and don't be shy. I want the criticism more than accolades. I want to make it better, stronger, faster ;)


---
**Marc McDonald** *January 21, 2015 04:19*

**+Nathan Buxton** Thanks for all the information, I see that calibration and adjustments can make a big difference in part print quality. At this time I have a replacement part already printed and will see when the new bushings show up, will try the Acetone bushing install method on it. I am printing a modified spool holder at this time and will see how it turns out.


---
**Marc McDonald** *January 27, 2015 04:45*

**+Eric Lien** Have you considered using bearing inner race spacers on the X, Y and Z bearing / GT2 timing pulley assemblies?



I see issues with the timing pulleys  pressing up against both inner and outer bearing races and attempting to rotate the entire bearing in the bearing support. This seems to cause some of the vibrations I have reported earlier. I am currently modifying the bearing mountings (X, Y and Z) to add the space for the inner race spacers to keep the timing pulley in the same locations and out of contact with the outer bearing race.



Was there a reason the Z bearing holder base was designed with a small space between the Z rail and holder? I was planning on removing that space and enlarging the Z bottom bearing holder.


---
**Eric Lien** *January 27, 2015 04:55*

**+Marc McDonald** now that you mention it... Shoot I did have to buy super slim precision shims to space the pulley out from the bearing. I got them locally and forgot to add it to the BOM. 


---
**Tim Rastall** *January 27, 2015 04:57*

**+Marc McDonald**​ **+Eric Lien**​Those shims could quite easily be printed eh? 


---
**Eric Lien** *January 27, 2015 04:57*

I spaced it to allow for adjustment. At the time my printer was less tuned (it was a corexy) so I didn't want to get into a situation where I could get close enough and hence cause binding.


---
**Eric Lien** *January 27, 2015 05:51*

**+Tim Rastall** that why your the master and I am the student. Why I never thought of that floors me.


---
**Tim Rastall** *January 27, 2015 06:17*

**+Eric Lien** I've just thad a slight head start on you. Your work is awesome and you've clearly got a greater aptitude for finishing stuff than I do, hence the success of the Herculien, super Kudos to you man :) 


---
**Eric Lien** *January 27, 2015 07:23*

**+Tim Rastall** yeah, but your new bot looks like a screamer. I have some catching up to do. I can see your tail lights... But just barely ;)


---
**Jo Miller** *February 01, 2015 09:43*

For those surching for the V-slot wheels and extrusions in europe , my tip  : dont go for the UK-/france based suppliers, their shipping costs are redicilous, go for [http://ratrig.com/](http://ratrig.com/)

in portugal, they produce the V-Slot Linear guide rail system themselves.


---
*Imported from [Google+](https://plus.google.com/118118158840729615568/posts/g34prRENmJf) &mdash; content and formatting may not be reliable*
