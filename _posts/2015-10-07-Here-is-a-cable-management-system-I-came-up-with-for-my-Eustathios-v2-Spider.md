---
layout: post
title: "Here is a cable management system I came up with for my Eustathios v2 Spider"
date: October 07, 2015 02:42
category: "Show and Tell"
author: Rick Sollie
---
Here is a cable management system I came up with for my Eustathios v2 Spider.



Hack, modify, print, enjoy!





**Rick Sollie**

---
---
**Brandon Cramer** *October 07, 2015 04:01*

I like it!!


---
**Oliver Seiler** *October 07, 2015 05:09*

Nice!


---
**Bud Hammerton** *October 07, 2015 06:47*

Can you add source files to your YouMagine posting? 


---
**Rick Sollie** *October 07, 2015 10:28*

**+Bud Hammerton** I put this together in Tinkercad, which only outputs the stl/obj file.


---
**Bud Hammerton** *October 07, 2015 14:29*

Okay, then. I was looking for something like this, but STLs are just too hard for me to work with. I'll see what I can do with this to convert it to a solid part. Thanks for all the effort and the design.



Update: I recreated this and exported to STEP and IGES, take them for anyone else asking for CAD files. [http://1drv.ms/1QZgtx2](http://1drv.ms/1QZgtx2)



This was a model I didn't know I needed until you posted, now I can see how it could simplify and clean up wire management.


---
**Erik Scott** *October 07, 2015 18:38*

I like this a lot! I was trying to think of how I might make something like this. My ideas weren't nearly as simple. 


---
**Rick Sollie** *October 07, 2015 19:00*

**+Bud Hammerton** thanks for making that available. I seriously need to learn a proper cad program ;)


---
**Rick Sollie** *October 07, 2015 19:02*

**+Erik Scott** Thanks. I'm still working on something to transition from horizontal to vertical extrusion, more to come.


---
**Erik Scott** *October 07, 2015 19:19*

**+Rick Sollie** make versions with a 45deg cut in the top, rather than horizontal, and don't carry the clip bits up to the cut. 


---
**Erik Scott** *October 07, 2015 22:13*

And since I suck at explaining things, I decided to go ahead and make a set: [http://i.imgur.com/sedSiDY.png](http://i.imgur.com/sedSiDY.png). **+Rick Sollie** what do you think?


---
**Rick Sollie** *October 08, 2015 11:16*

**+Erik Scott** that will do the trick!


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/CRxCeSoEH1N) &mdash; content and formatting may not be reliable*
