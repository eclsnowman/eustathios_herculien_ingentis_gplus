---
layout: post
title: "That feeling you get when you realize you've left the map and the only way through is to roll your own"
date: June 01, 2015 23:54
category: "Discussion"
author: Mike Miller
---
That feeling you get when you realize you've left the map and the only way through is to roll your own. 



Now, I've gotta learn to ream out 8mm holes to 10mm and keep them wobble free...off to buy a 10mm reamer...

![images/534477c23ce3ab965a01d46217ad748e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/534477c23ce3ab965a01d46217ad748e.jpeg)



**Mike Miller**

---
---
**Eric Lien** *June 03, 2015 00:10*

I am having a little trouble wrapping my head around what I am seeing... But I like it ;)


---
**Mike Miller** *June 03, 2015 00:58*

I still need two top cross bars for the top of the Z, but I wanted independently adjustable X and Y heights, with as much of the interior devoted to build area as possible. It's using button-head screws to hold everything together. 


---
**Vic Catalasan** *June 03, 2015 16:22*

Whats the reasoning on the two belts instead of just one? 


---
**Mike Miller** *June 03, 2015 18:48*

O.o cuz...that's the way I've always done it... (Runs off to see about just using one belt)


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/dZEi2Kv3JAG) &mdash; content and formatting may not be reliable*
