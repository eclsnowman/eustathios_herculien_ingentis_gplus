---
layout: post
title: "So I have decided to build an ingentis"
date: November 29, 2014 19:32
category: "Discussion"
author: Ethan Hall
---
So I have decided to build an ingentis. I was wondering of there was any major performance differences between the original ingentis and it variants (herculien, eustathios, etc)





**Ethan Hall**

---
---
**Eric Lien** *November 29, 2014 21:57*

Original ingentis is great, Eustathios made improvements in my opinion with gt2 belts and lead screws.



HercuLien is a scaled up variant for larger builds. I use it to make prototype patterns for the foundry I work at. Its a cheap way to verify dimensions after solidification as well as verify our real work soundness results back to magmasoft solidification software prior to CNC cutting production tooling. It is my design and probably over built... But that's how I like things. 



All are great machines.


---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/4nkYLzNDn5y) &mdash; content and formatting may not be reliable*
