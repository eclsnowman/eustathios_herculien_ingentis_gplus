---
layout: post
title: "Eric Lien 's v4 Carriage and e3d's Lite6 Extruder"
date: May 15, 2015 01:44
category: "Discussion"
author: Mike Miller
---
**+Eric Lien**'s v4 Carriage and e3d's Lite6 Extruder. I think I might bump the model up 5% and reprint. It got this close with just a little work with a drill bit on the carriage...it's nearly a drop-in replacement. 



The Lite6 is an interesting little extruder. There's no stainless heat-break, and it's good to 250C...which is pretty much all I've ever heated my e3d to for t-glase and Taulman 618. 



Looks like an excellent compromise for my son's ABS/PLA printer. 



![images/4818ef69a7c89f360e2e7dff6bd954dc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4818ef69a7c89f360e2e7dff6bd954dc.jpeg)
![images/ae293d34e0557eb9e7205a665263d662.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ae293d34e0557eb9e7205a665263d662.jpeg)
![images/70e5cf35f27187ce70cafc46547f3bd2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/70e5cf35f27187ce70cafc46547f3bd2.jpeg)
![images/24be393c7906f6a2bfa52eab594adffd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/24be393c7906f6a2bfa52eab594adffd.jpeg)

**Mike Miller**

---
---
**hon po** *May 15, 2015 02:09*

Never saw a Lite6, but the heatsink is stainless steel according to their webpage. So the heatbreak is still there. The price point is great. My only concern is the PTFE tube could be forced out if pressure build up inside the hotend.


---
**Mike Miller** *May 15, 2015 02:25*

You're right. The whole thing's stainless. I just saw one material and didn't think to evaluate further....interesting as I'd expect a stainless heat sink to be more expensive...but must be cheaper to machine overall. 


---
**Jeff DeMaagd** *May 15, 2015 13:59*

I didn't realize the Lite6 heat sink was stainless either. The material is costlier per unit volume (I think) and generally slower and harder to machine. But the material switch might have been necessary to reduce part count.


---
**hon po** *May 15, 2015 14:22*

I also believe major cost goes to machining the heatbreak with smooth internal wall. Which is not required for Lite6, as the filament don't touch the heatbreak.


---
**Mike Miller** *May 15, 2015 14:29*

Agreed. There's some smart engineering here, **+Sanjay Mortimer** has a lot to be proud of. 


---
**Brad Hopper** *May 16, 2015 14:50*

But stainless will be a far less efficient heat sink than Al, but its probably good enough.


---
**Mike Miller** *May 16, 2015 16:06*

At that point, I don't think your looking for head rejection, so much as keeping it isolated to the heat block...stainless is a crappy thermal conductor...and in this case, that's good. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/7QyVEzQEwvR) &mdash; content and formatting may not be reliable*
