---
layout: post
title: "Idler designs .5, 1 and 2...I'll incorporate guides and call it done"
date: September 21, 2014 16:06
category: "Deviations from Norm"
author: Mike Miller
---
Idler designs .5, 1 and 2...I'll incorporate guides and call it done. 

![images/742fcf67c67c52e2092f58a4b22d9f2d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/742fcf67c67c52e2092f58a4b22d9f2d.jpeg)



**Mike Miller**

---
---
**ThantiK** *September 21, 2014 16:54*

Man, I'm so close on mine.  I've got like 3 parts to print and I'll have my Z stage set up...


---
**Eric Lien** *September 21, 2014 18:35*

**+ThantiK** I look forward to seeing yours. It has been in the works for a long time.


---
**ThantiK** *September 21, 2014 21:18*

**+Eric Lien**, only because I've been so busy doing other things and no willpower to work on it. :D


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/CA1ER5HevX1) &mdash; content and formatting may not be reliable*
