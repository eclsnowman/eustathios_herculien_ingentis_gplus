---
layout: post
title: "Hello, a few of my bushings are very very stiff"
date: September 11, 2016 03:54
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hello, a few of my bushings are very very stiff. One even scratches the rod as it moves. I am using the Misumi rods and a few work really well, but that cross rod is slightly tilted, haven't tried with the carriage yet...



Oh, and it looks like the pulley connecting the rods is rubbing every 1/2 turn on the bearing holder. Will that just wear out?





**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *September 11, 2016 04:23*

For the pulley there need to be shims between the pulley and the bearing to space the pulley out. Very thin shims are all that is required. They are in the BOM so if you ordered off of that you should have them.


---
**Eric Lien** *September 11, 2016 04:26*

Not sure what you mean by tilted? Can you make a video and upload to show us the problem? Also the bronze bushing can't scratch the hardened rods. Bronze is too soft to scratch it.


---
**Eric Lien** *September 11, 2016 05:37*

BTW Here is where the shims go: [imgur.com - Imgur: The most awesome images on the Internet](http://imgur.com/a/42cST)



(You can see it in blue)


---
**Stefano Pagani (Stef_FPV)** *September 11, 2016 17:17*

**+Eric Lien** Ohhhh, that makes sense. I fixed the tilted problem. Basically, the cross rod was pushed all the way into one mount/holder on one side and very little on the other so it was at a slight angle. All fixed now.




---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/EX8Dm3Sx5LF) &mdash; content and formatting may not be reliable*
