---
layout: post
title: "I'm trying to battle the z-ribbing that I've pretty much always had on my Eustathios, V2 and I've just had a thought that I would love someone here to explain to me"
date: March 16, 2016 20:51
category: "Discussion"
author: Ben Delarre
---
I'm trying to battle the z-ribbing that I've pretty much always had on my Eustathios, V2 and I've just had a thought that I would love someone here to explain to me.



Z-ribbing is where your layer height is incompatible with your z-axis full steps, this results in a small cumulative error that occurs as your z axis steps through your layer heights. This error creates thicker and thinner layers in a regular pattern as you progress through Z. This is supposedly easily solved by picking a layer height that matches the full steps of your z-axis.



On the Eustathios with 16:32 gear ratios and 2mm pitch leadscrew this results in layer heights of 0.1920 or 0.2080. I've tried these, and 0.2 to no avail, and its definitely ribbing and not wobble since its not shifting the layer but making the layers fatter or thinner as we go through Z. Its also not filament diameter issue since that would change depending on the length of filament extruded in the layer and no matter what size object I print the ribbing stays the same frequency.



So, this got me to thinking. The idea that you should always be on a full step for every layer is sound, the motors are much more accurate on full steps and will hold positions reliably there. But we zero at the top of our axis on Eustathios. This means that when we get to Z=0 we've moved the motors to get there and there's no guarantee we're actually at a full step when we zero out at all. So could we be introducing this issue in a way that would mean we're constantly on a microstep and hence not having fully accurate steps per layer no matter what we do?



If this is the case how can we correct it?



![images/dd349bccfcf0e1a822900ae20c5f66a6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dd349bccfcf0e1a822900ae20c5f66a6.jpeg)
![images/106fe8077d539d1edeb3a1a1e042ac0a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/106fe8077d539d1edeb3a1a1e042ac0a.jpeg)
![images/aac8bd2afe6cb75ae1496d89f70c02d1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/aac8bd2afe6cb75ae1496d89f70c02d1.jpeg)
![images/03853da24adf7f2c5fb51ebd6d83011e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/03853da24adf7f2c5fb51ebd6d83011e.jpeg)

**Ben Delarre**

---
---
**Miguel Sánchez** *March 16, 2016 21:08*

You can work without micro-stepping, a bit radical but ... you asked for it.



However if the problem is due to micro-stepping and not stopping at full steps, should not change the frequency when you change the layer height?


---
**Ben Delarre** *March 16, 2016 21:09*

Sadly I'm on a smoothieboard so I can't, at least not without soldering on the board and I've yet to find any instructions about how to accomplish it anyway.


---
**Igor Kolesnik** *March 16, 2016 21:10*

When printer homes gets in between steps, it can't hold it ther so the error will be only in first layer. On the next z change it will skip to the next step up or down, depending of the direction so error will not be accumulated. At least this is how I understand this issue. Also as I know z-ribbing is an issue caused by the bend z shaft or leadcrew that creates misalignment in horizontal layers creating waves on the side of the part. Correct me somebody if I am wrong﻿


---
**Ben Delarre** *March 16, 2016 21:12*

Thats definitely possible, I'm not sure if the stepper will fall down to the full step or not while engaged though. Could potentially fix this by homing to zero, then disengaging the motors for a while to let it settle to a full step, then re-engaging them. That could be something worth trying.



What you're describing in terms of z-shaft or leadscrew bending causing misalignment in layers is what I've heard referred to as Z-banding, and you can determine this by seeing the layers shift from side to side. What I'm referring to here though is Z-ribbing which you can see on all sides of the part as layers get thicker and thinner.


---
**Miguel Sánchez** *March 16, 2016 21:13*

**+Ben Delarre** Smoothieboard also has some headers for each steppers signals, so you might want to try with an external driver to see if that effect goes away without micro-stepping on your motor (I am not convinced though).


---
**Eric Lien** *March 16, 2016 21:16*

One issue I've always been meaning to address with Eustathios is that the distance X between the two z axis rods and lead-screws is fixed.  So if it is not perfectly aligned and it will put a side load on the ball screw. Ideally those would be allowed to be aligned maybe making one of the extrusions longer so I can mount to the underside.


---
**Miguel Sánchez** *March 16, 2016 21:18*

**+Eric Lien** Latest Prusa i3 parts have changed the hex hole for the Z nuts for a channel that holds the nut but allows side play [https://github.com/prusa3d/Orignal-Prusa-i3/blob/master/Printed-Parts/stl/x-end-idler.stl](https://github.com/prusa3d/Orignal-Prusa-i3/blob/master/Printed-Parts/stl/x-end-idler.stl)


---
**Miguel Sánchez** *March 16, 2016 21:21*

Now that I think about it, what **+Eric Lien** mentions would cause a repeatable pattern with no scale change for different layer heights, right?


---
**Ben Delarre** *March 16, 2016 21:23*

Hmm, side load on the leadscrew? I'm not sure I understand enough about the acme profile to make a judgement on that. Can anyone confirm that side loading an acme leadscrew will result in a repeating pattern of variation in layer height?


---
**Eric Lien** *March 16, 2016 22:11*

I'm more referring to the stack tolerance of everything bolting together setting up perfect alignment of the rods which is unlikely. Mine is pretty good but depending upon the thickness of the printed parts and how everything bolts together and how accurately the frame extrusions were cut could potentially put the lead screw in a side-loaded condition. So then the lead screw nut will not rest fully down on the threads but will be riding on the side of the threads which might oscillate back and forth as the lead screws rotate. 


---
**Ben Delarre** *March 16, 2016 23:00*

Hmm. That would explain why the banding is at the leadscrew pitch as well as why it doesn't change with layer height.



What would be the change in the design to fix this? Separating the leadscrew nut mount from the z rod bearing mount would make it easy to align the top and bottom of the rod and leadscrew along the z axis,  but across x there would still be some potential for misalignment.


---
**Ryan Carlyle** *March 16, 2016 23:05*

Full-step-multiple increments between layers are what you care about, not necessarily only doing layers on full steps. The microstep angle error is very consistent -- if microstep X is off by Y degrees, then all microsteps X+n*[full_step] will also be off by Y degrees. Angle error doesn't cause print flaws if it's always the same angle error for every layer.  


---
**Ben Delarre** *March 16, 2016 23:09*

That makes sense **+Ryan Carlyle** thanks for the explanation. I guess then its most likely the issue Eric has laid out.


---
**Ryan Carlyle** *March 16, 2016 23:11*

Yeah, I don't think this is microstep angles. See **+Whosa whatsis** post here: [https://www.evernote.com/shard/s211/sh/701c36c4-ddd5-4669-a482-953d8924c71d/1ef992988295487c](https://www.evernote.com/shard/s211/sh/701c36c4-ddd5-4669-a482-953d8924c71d/1ef992988295487c)


---
**Ben Delarre** *March 16, 2016 23:13*

Yeah that post was what started me thinking about this.



I think this might actually qualify as a new entry, its not Z wobble, but its inconsistent Z movement based on the geometry of the screw and the fact that the nut is riding predominantly on one side of the screw.


---
**Ben Delarre** *March 16, 2016 23:15*

**+Eric Lien** if I widened the Z nut mount holes to allow some horizontal adjustment along X for the nut position on the mount would that alleviate this?



Make the holes a bit more slot like, loosen off the nut mount screws, let it fall back into its natural position and then tighten them up?


---
**Eric Lien** *March 16, 2016 23:42*

I'll try and model up some ideas over the next couple of days and throw them out for the group to look at


---
**Ben Delarre** *March 16, 2016 23:43*

Well for my specific issue it may have just been my idiocy when setting the machine up originally.



When I first set it up I was having lots of problems with z binding on full length travel. So I shimmed the left side z-rod mount and it seemed to fix it. BUT, I just removed the shims, can still get full length travel without binding, and it appears to have fixed the z banding!



Best test cube print ever.


---
**Whosa whatsis** *March 17, 2016 00:09*

This doesn't look like what I was referring to when I wrote about Z ribbing. When that occurs, you'll have several layers that look pretty much the same, then one that is bulging or inset, as if it was over- or under-extruded. It happens due to rounding errors in the number of steps per layer, or because. Depending on the rigidity of your mechanisms, this may take a layer or two to get back to "normal", but it shouldn't look this sinusoidal. I suppose if you had really poor microstep positioning accuracy AND your layer height was not an integral number of microsteps (particularly if it was off by one microstep, like 17 or 31 microsteps, or something like that) you might get something like this, but I don't think microstep position error is quite that sinusoidal either (**+Ryan Carlyle** would know that).



This looks like a form of classic Z wobble, but it could be one of those situations where instead of wobbling in circles around the Z axis or side-to-side (in the X/Y plane), it actually wobbles vertically. This is more rare, but I've seen it happen. If, for instance, you have a threaded rod with the end cut-off crooked and then shoved into a flexible coupler so that it's directly up against the motor shaft AND the rod is at a bit of an angle going to the nut, that uneven end will press up against the motor shaft and stretch the coupler more in one position and less 180 degrees from that position. This would result in a sinusoidal patterns of over- and under-compressed layers with a pitch matching the lead of the screw (which is the same as the pitch for single-start screws, but different for multi-start screws like most of the acme/trapezoidal ones people use). There are also some devices that are designed to isolate Z wobble in the X/Y direction that end up translating it into a (usually smaller) wobble in the Z direction.


---
**Ben Delarre** *March 17, 2016 00:17*

Well since Eustathios does not use a coupler for the leadscrews (driven via belt, riding on a bearing), and its clearly not z ribbing as you described, or classical z-wobble then I think this turned out to be exactly as Eric described. The smooth rod was forcing the nut with a side load that was causing the z axis to ride mostly on one side of the screw. So the bed wasn't falling down as far when the driving edge of the screw was at the other side. Fixing the alignment of the z-rod has made this completely disappear, only artifacts I'm now seeing are really minor and probably due to small filament diameter variation.


---
**Whosa whatsis** *March 17, 2016 00:20*

Side-load can do it too, if it has a cyclical component. Driving it through a belt with an eccentric pulley could do it too. Those were just a couple of examples of how vertical Z wobble can occur.


---
**Ben Delarre** *March 17, 2016 00:21*

Can you add a section on vertical z wobble to your evernote page? Really helpful document and could save a bunch of people some headscratching calling some of these issues out a little. I had no idea it would present this way and have been chasing z-wobble for ages :-)


---
**Whosa whatsis** *March 17, 2016 00:52*

Yeah, might find some downtime for that on my trip to MRRF.



I might want to use one of your pictures (with credit) if that's alright.


---
**Ben Delarre** *March 17, 2016 00:53*

Go for it, use anything you like.



I'm jealous of all you guys headed out to MRRF, have fun!


---
**Jim Stone** *March 18, 2016 08:40*

I've got a similar problem before it was directly related to the z axis. Now that I've swapped it out for a new bit it's become more...erratic and less patterned than it was. With a herculien



part on the right is an old part. and the ribs or pattern is exactly 2mm OC



part on left is with new screw and its more erratic. i dont have my large benchy near me, but it shows it more.



[http://imgur.com/WNdXnir](http://imgur.com/WNdXnir)


---
**Whosa whatsis** *March 18, 2016 17:15*

**+Jim Stone**​ the one on the left looks like it could just be spool tension, but that's a Bowden machine, isn't it? Bowden extruders shouldn't have that problem unless the tension is causing the entire frame to flex


---
**Jim Stone** *March 18, 2016 18:02*

**+Whosa whatsis**​ yes. It's a herculien. With a bond tech extruder and an e3d. Using atomic filament abs


---
**Whosa whatsis** *March 18, 2016 18:03*

Yeah, so unless your frame is flexing, that's not it.


---
**Jim Stone** *March 18, 2016 21:50*

yeah i doubt the whole flexing thing. its a pretty solid frame.

question. would my table have anything to do with it?


---
**Eric Lien** *March 19, 2016 01:39*

The verticals on HercuLien are only 20x20 and I have seen that they can flex if the 4 feet are level. I've actually put levelers on my feet now to make sure that I've got it set on the table in a way that isn't putting the frame and tension.


---
**Whosa whatsis** *March 21, 2016 01:18*

Added a section on vertical Z wobble (mostly reworked from what I wrote here, plus a couple more potential causes I noticed while looking at the variety of machines a MRRF): [https://www.evernote.com/l/ANNwHDbE3dVGaaSClT2JJMcdHvmSmIKVSHw](https://www.evernote.com/l/ANNwHDbE3dVGaaSClT2JJMcdHvmSmIKVSHw)


---
**Ryan Carlyle** *March 21, 2016 01:31*

**+Whosa whatsis** Random note in the article... I don't know if I'd recommend HDPE feed tubes, they can have quite a lot of friction if there's much curvature in the tube. I did some drag math (capstan equation) on tube materials a while back and basically everything but PTFE and nylon will add a lot of drag. 


---
**Whosa whatsis** *March 21, 2016 01:48*

**+Ryan Carlyle** They're certainly not as good as PTFE, but empirically, the larger-diameter tubes I recommend don't add noticeably more friction than the tighter PTFE tubes used for bowden tubes. If they fit the filament nearly as tightly as PTFE tubes generally do, I would agree completely. Because they cost virtually nothing and are far easier to find locally, I think they're still worth mentioning.



The stiffness of these tubes is actually a bigger concern, depending on how long they are and how they are routed. I've actually seen one of these tubes break off the filament at the extruder when someone tried to use one that was too short for the job.


---
**Ryan Carlyle** *March 21, 2016 02:14*

**+Whosa whatsis** Has anybody ever collected any hard data to your knowledge on drag force vs tube ID? Seems to me like poor straightness in the filament (ie mild bends where it crosses lower wraps) would be the only effect causing increased drag until you got a REALLY tight fit. 



Man, I need to make a filament force sensing jig. 


---
**Whosa whatsis** *March 21, 2016 02:25*

Not that I know of, but friction is proportional to the normal force, and it seems like you could calculate that by modeling the tube and filament as two springs pushing against one another...


---
**Ryan Carlyle** *March 21, 2016 02:57*

Cylinder-in-cylinder physics is pretty much bread and butter stuff at my day job, but it took me a LOT of thinking to figure out any reason why tube ID would affect drag friction in the absence of filament kinking. If you assume line contact between the filament and tube, there should be ZERO impact of either filament OD or tube ID. Ideal line contact is diameter-neutral. Likewise if you assume a flat (planar) contact cross-section, as would be used in the typical Capstan Equation workup. Sliding friction of a cylinder on a plate will be approximately diameter-neutral, since Coulomb friction is a function of normal force rather than contact area or pressure. The only time the diameters should come into play is when the contact cross-section becomes large enough to cover a meaningful amount of arc curvature. At that point, there is an additional "wedging" action due to the horizontal force components from the contact on either side of the filament. That's the only thing I can think of (in addition to filament bending/kinking effects). 


---
**Whosa whatsis** *March 21, 2016 03:06*

That fits my empirical impressions. There's also the fact that the difference in diameters will be subtracted from a term in the spring force equation (after all, there would be no force contributing to friction except from gravity if the tube's ID was large enough to be effectively infinite so that the differences in their bend radii no longer matters).


---
**Whosa whatsis** *March 21, 2016 03:17*

Oh, and you also need to consider the second-order characteristics of the shapes. If one of your cylinders is being pulled and the other is being pushed (they reverse roles in a Bowden tube vs. A feed tube), the one being pushed will try to compress axially and it will want to buckle, adding additional side forces. This is why you need a reasonably stiff tube.


---
**Ryan Carlyle** *March 21, 2016 04:02*

What do you mean when you say there would be no force contributing to friction if the tube ID were infinite? No additional force? I'm not sure if this is what you meant, but the primary friction force is coming from the normal force applied by filament tension (or compression) as it wraps around the large-scale tube curvature. An "infinite ID tube" is just filament wrapped around a cylinder. That's classic capstan friction. 


---
**Whosa whatsis** *March 21, 2016 04:07*

I mean that if the ID of the tube is effectively infinite, pulling the filament through the tube is no different from dragging it across the floor, in which case, the only force contributing to friction is from gravity. The filament's bend radius will only attempt to conform to the tube's bend radius if the filament is flexible enough to collapse under its own weight (plus whatever amount of force is pulling it taught, which depends on the resistance at the other end).


---
**Ryan Carlyle** *March 21, 2016 15:06*

But you mean aside from the capstan friction from the tube's large-scale curvature, right? That's seriously where most of the drag comes from. Try pulling filament through a straight 4x2 tube and compare to the same tube with a couple loops in it. 


---
**Whosa whatsis** *March 21, 2016 15:32*

Does that still apply to a filament that is too stiff to conform to that curvature? The tube should have a larger natural bend radius than the filament. It seems to me that until the tension is sufficient to make the bend radii match at the contact surfaces, you will instead have point contacts, and the angular term of the capstan equation will be essentially zero.


---
**Ryan Carlyle** *March 21, 2016 15:51*

Dunno about 3mm filament, but it doesn't take much tension (maybe a half pound at most) to get 1.75mm filament to reasonably conform to any reasonable tube curvature. The exact contact shape doesn't matter, it's the normal force at the circumferential contact required to react against the tension vectors at the free ends of the line that generates the capstan friction. (Again, Coulomb friction only depends on normal force, not contact area or pressure.)



I think line stiffness can only add to the capstan friction. If you imagine the filament curvature naturally matches the large-scale bend of the tube, that's equivalent to a non-stiff line and only capstan friction will occur. If you then make the filament curvature different from the tube (tighter or looser), there is still the same normal force due to tension at the free ends of the line, but it also has an additional force component of the filament and tube trying to deflect each other. 


---
**Whosa whatsis** *March 21, 2016 16:23*

Half a pound sounds like a lot to me. The ideal spool holder will have nearly zero dynamic friction, only enough to keep the filament from unspooling itself due to the bending force of wrapping it around the spool.



If you just shove a stick through the center of the spool, then yes, I can see how capstan friction would come into play, at least with 1.75.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/Ek2vFbsLKfA) &mdash; content and formatting may not be reliable*
