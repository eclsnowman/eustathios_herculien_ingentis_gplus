---
layout: post
title: "Where do you guys source aluminum plate?"
date: February 24, 2016 03:54
category: "Discussion"
author: Mike Miller
---
Where do you guys source aluminum plate? The Changes to IGentUS (which needs a new name, there's no IGUS there anymore)...make the old envelope too small. 



So, aluminum, head pad, bakelite, glass...but you can pay crazy amounts for aluminum greater than 12" on a side, I'm needing 15.5" (390mm) square to really take advantage of the improvements. 





**Mike Miller**

---
---
**Eric Lien** *February 24, 2016 04:00*

[http://i.imgur.com/dRJ4TVk.png](http://i.imgur.com/dRJ4TVk.png)


---
**Eric Lien** *February 24, 2016 04:02*

But I recommend you go with something other than shear cut unless you want to be flattening it back by hand. Just ask **+Zane Baird**.


---
**Zane Baird** *February 24, 2016 04:25*

True story **+Eric Lien**... Pay more and get a truly flat piece. Trust me, it's worth the extra cash 


---
**Alex Lee** *February 24, 2016 04:47*

**+Eric Lien** I kept clicking the links and buttons on the image. I'm not a very smart man...


---
**Jim Stone** *February 24, 2016 05:48*

hey that looks like the place i may have bought mine from... i cant remember lol...but yeah it aint flat.


---
**Ted Huntington** *February 24, 2016 06:32*

I was able to pick up a piece of aluminum sheet for about $20 at IMS- there are probably metal suppliers around you- they cut it with a sheer for a small fee- but I then just used a circular file to grind away the four places where the rails and ball screws are.  But it could even be cut using a hand grinder with an abrasive saw.


---
**Jim Stone** *February 24, 2016 21:05*

ah. here is where i ordered mine from [http://www.onlinemetals.com/](http://www.onlinemetals.com/)

beware something is leaving my bed with weird ripples.


---
**Eric Lien** *February 24, 2016 22:11*

**+Jim Stone** Do you use glass over the bed? Even though we have aluminum beds my recommendation is still to go over that with glass. My intention with this design was for the aluminum to act as the heat spreader / thermal mass. And the glass to provide flatness. So if the bed is not perfect it should still work ok (presuming the glass in not being pulled out of flatness when clamped down).


---
**Jim Stone** *February 25, 2016 05:03*

yeah im using some good borosilicate too.


---
**Maxime Favre** *February 25, 2016 08:21*

**+Alex Lee** I click'ed too ;)


---
**Mike Miller** *February 28, 2016 15:46*

Just to circle back, I ended up getting 1/4" Tooling plate from Amazon for $54. It was $9 more than the 'non-tolerance CNC cut' metal from discount steel. 



Good thing I upgraded the Z-stage...between this, the heater and the glass, the build platform's gettin' kinda heavy!


---
**Eric Lien** *February 28, 2016 15:52*

**+Mike Miller** got a link?


---
**Mike Miller** *February 28, 2016 15:58*

[http://www.amazon.com/TEMCo-Aluminum-Tooling-Sheet-Plate/dp/B00ZS0YPLC/ref=sr_1_1?s=industrial&ie=UTF8&qid=1456675079&sr=1-1&keywords=Aluminum+1%2F4+plate16](http://www.amazon.com/TEMCo-Aluminum-Tooling-Sheet-Plate/dp/B00ZS0YPLC/ref=sr_1_1?s=industrial&ie=UTF8&qid=1456675079&sr=1-1&keywords=Aluminum+1%2F4+plate16)


---
**Mike Miller** *February 28, 2016 16:00*

Humph, doesn't look like mic 6, but it IS tooling plate, which is held to higher tolerance than sheet stock. 


---
**Mike Miller** *March 06, 2016 14:43*

BTW, it's a thing of beauty...and the dual leadscrew Z-lift has NO problem moving it, plus the glass, plus the bakelite+heaterpad...I'll be attempting to remove the pad from the bakelite and attaching it to the aluminum...I predict I'll be ordering another pad. 




---
**Eric Lien** *March 06, 2016 15:06*

Glad to hear it.


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/XhZNUxpSTcD) &mdash; content and formatting may not be reliable*
