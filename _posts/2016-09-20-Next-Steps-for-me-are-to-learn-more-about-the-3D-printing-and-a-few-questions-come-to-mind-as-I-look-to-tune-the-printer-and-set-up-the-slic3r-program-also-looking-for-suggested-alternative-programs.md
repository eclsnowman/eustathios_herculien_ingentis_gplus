---
layout: post
title: "Next Steps for me are to learn more about the 3D printing and a few questions come to mind as I look to tune the printer and set up the slic3r program (also looking for suggested alternative programs);"
date: September 20, 2016 16:53
category: "Build Logs"
author: Bruce Lunde
---
Next Steps for me are to learn more about the 3D printing and a few questions come to mind as I look to tune the printer and set up the slic3r program (also looking for suggested alternative programs);  



This is my first print with 'guessed' setting based on the build [HercuLien, SmoothieBoard, and E3D Volcano with a 1.2mm nozzle];

I know that I need to tighten up some belts and some mechanical adjustments, but I could use advice in the slic3r configuration.  At this point I do not completely understand the relationship of layer height to the other settings? I set up the filament tab to 1.75, left the multiplier at 1; set the extruder temp to 240 and bed temp to 110, [using ABS] ;

I used the default layer height [I think at .3]

I used the default speeds;



Any other thoughts based on this result pictured?



![images/fa4cd1ce147caf9d7ce13dc0e1022666.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fa4cd1ce147caf9d7ce13dc0e1022666.jpeg)



**Bruce Lunde**

---
---
**Stephen Baird** *September 20, 2016 17:13*

First, it looks like your extruder settings are incorrect in firmware, like the steps per mm is too low. That sort of texture is indicative of significant underextrusion which could be caused by a clogged or partially clogged hot end, but in a brand new printer I would guess incorrect extruder steps per mm.



You can try calculating the correct steps value using the Prusa calc ([prusaprinters.org - RepRap Calculator - Prusa Printers](http://prusaprinters.org/calculator/)) or try my favorite method: Cut a straw at precisely 100mm, then slit it down the middle so you can easily push it onto your filament. Put the straw onto the filament so one end is hard up against the extruder where the filament enters, then make a sharpie mark on the filament where the straw ends. Heat your hot end up and tell the printer to extrude 100mm, make another mark at the point where the filament enters the extruder, then measure the distance between your two marks.



Subtract the distance measured from 100 (if the mark hasn't entered the extruder) or add 100 to the distance (if the mark has entered the extruder) then divide this value by 100 and divide your existing e-steps per mm value by that resulting value and put this final result in as your new e-steps per mm. Repeat the process to make sure it's actually moving 100mm of filament when you tell it to, and now your e-steps value should be good.



As for the interplay of those settings in the slicer:



The filament diameter and filament multiplier are used by the slicer to calculate how much filament needs to be extruded to produce the desired shapes. You want to set the filament diameter by measuring your filament at a few points over maybe an arm span's worth of length and averaging them out. You can then print a single walled cube and compare the actual measurement of the wall's thickness to the expected wall thickness based on your settings in the slicer and modify the multiplier as necessary based on the ratio of those two values.



That's a bit more advanced, though, first you need to get those e-steps set correctly.



As for layer height, that tells your printer how far to move the z-axis down after each layer. Where that comes into play with temperatures is that a thicker layer means your printer is extruding more plastic per unit time and so may need a higher temperature to perform consistently. In general, though, you want to just play around with your temperature settings starting from the manufacturer's recommended temperature until you find the sweet spot for that material. It'll vary a bit from material to material, manufacturer to manufacturer, and even hot end to hot end, and in the end experience is what will help you get the temperatures really dialed in.



It seems like you're at a good starting point for temps, though.


---
**Eric Lien** *September 20, 2016 17:31*

First step is calibrate the extruder. Start calibration by free air extruding. Mark the filament from the top of the extruder with a sharpie, feed 100mm into the air (no Bowden tube, no hotend, just into the air with no restrictions). Measure the amount the extruder feed out. It should be 100mm. (Good walk through here: [reprap.org - Triffid Hunter's Calibration Guide - RepRapWiki](http://reprap.org/wiki/Triffid_Hunter%27s_Calibration_Guide)).



Once you tune for distance, then you calibrate for extrusion width. Print a hollow cube with two perimeters and no top infill. If you know the anticipated extrusion width, the wall thickness when you measure it should be 2x the extrusion width. Adjust extrusion multiplier until that dimension come in (averaging your measurements on the 4 walls of the cube). You can continue to a 3 perimeter cube if you want to dial in even more.



Finally print a large flat object at 100% infill. Watch the layer lines. They should just touch making a glass smooth top solid surface. If you get raised edges, you are likely over extruding. If you get gaps, you are likely under extruding.



If you get those three items dialed in, you are 90% of the way there as far as calibration goes. The remaining items are getting that first layer height dialed in, and then tuning you hotend temp in line with your print speed and melt rate.


---
**Tomek Brzezinski** *September 20, 2016 17:40*

Eric and Stephen have great troubleshooting advice.



A while back I found a lot of good resources on the soliforum forum, for calibrating a solidoodle. It's pretty mucht he same stuff.  



But, I may even save the responses as a quick-answer to this ever-common question.  :) I'm clearly lazier


---
**Eric Lien** *September 20, 2016 17:41*

Thanks man :)


---
**Tomek Brzezinski** *September 20, 2016 18:49*

**+Nathan Walkner** , small note that is obvious when noted: But 3mm vs 1.75mm offers a difference of roughly 3X, not the <2X that you'd expect from a simple ratio. 



Totally funny how it does default to 3mm when it seems newest printers mostly use 1.75mm. I wonder if that will ever change :). Earlier repraps (for quite a while) used 3mm welding rod, i believe, and other 3mm plastic stocks that were totally meant for other purposes.


---
**Bruce Lunde** *September 20, 2016 19:14*

Thanks  everyone.  I am getting started on the extruder calibration now.


---
**Tomek Brzezinski** *September 20, 2016 19:31*

**+Nathan Walkner** also I did not mean to imply you didn't realize the 3X vs <2X business I mentioned, just that I feel like it's a relevant disclaimer in that context


---
**Bruce Lunde** *September 21, 2016 22:22*

Thank-you to everyone, I am printing much better today thanks to  all the suggestions.  Still some fine tuning to do, but works better than the last try, and my granddaughter is very happy with her new toy!

![images/5be0d3e6e114e10c250bb7bd325f7324.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5be0d3e6e114e10c250bb7bd325f7324.jpeg)


---
**Eric Lien** *September 21, 2016 23:47*

Great first print after tuning. Pretty soon you will be putting us all to shame :)



Happy printing!


---
**Wilmer Gaona** *October 21, 2016 14:47*

**+Richard Horne** has an excellent blog. In this post, he shows a detailed procedure to extruder calibration: [http://richrap.blogspot.mx/2012/01/slic3r-is-nicer-part-1-settings-and.html](http://richrap.blogspot.mx/2012/01/slic3r-is-nicer-part-1-settings-and.html)

[richrap.blogspot.mx - Slic3r is Nicer - Part 1 - Settings and Extruder Calibration](http://richrap.blogspot.mx/2012/01/slic3r-is-nicer-part-1-settings-and.html)


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/2oNxZKPMEVN) &mdash; content and formatting may not be reliable*
