---
layout: post
title: "I didn't have enough outputs on my printer controller after switching to the Azteeg X5 mini to drive my led lighting strip"
date: June 06, 2015 15:02
category: "Show and Tell"
author: Eric Lien
---
I didn't have enough outputs on my printer controller after switching to the Azteeg X5 mini to drive my led lighting strip. So I am using octoprint system:actions to run the led light strip via the raspberry pi GPIO and a sainsmart 2channel 5v relay board. In the album I have a wiring diagram of how I connected the relay, and videos of the file edits and operation. Below is a quick description of what you need to do on the raspberry pi and octoprint to get this working:





***You must install WiringPi to utilize the commands I used

***Install instructions and support for WiringPi can be found at [http://wiringpi.com/download-and-install/](http://wiringpi.com/download-and-install/)

***To install WiringPi SSH into to octoprint pi server (I use putty from windows)

***Use the following commands in the terminal to install WiringPi



git clone git://[http://git.drogon.net/wiringPi](http://git.drogon.net/wiringPi)

cd wiringPi

git pull origin

./build







***Now you must tell the PI how the GPIO pins will be used (we need them as outputs)

***Add text below to the end of /etc/init.d/octoprint

***Edit it by SSH to the pi and type: sudo nano /etc/init.d/octoprint

***CTRL-V to scroll all the way to the end

***Type the text below, then CTRL-X to exit and save when exiting



#Run export GPIO Raspberry



/usr/local/bin/gpio export 17 out

/usr/local/bin/gpio export 18 out

/usr/local/bin/gpio -g write 17 1

/usr/local/bin/gpio -g write 18 1









***We need to add custom actions in Octoprint to turn the relays on and off via GPIO

***Add this to the System->Actions area of config.yaml  

***Edit it by SSH to pi and type: sudo nano ~/.octoprint/config.yaml

***CTRL-V to scroll down until you see the actions

***Type the text below to add the new actions, then CTRL-X to exit and save when exiting



  - action: Relay1On

    command: gpio -g write 17 0

    name: Turn on Relay1

  - action: Relay1Off

    command: gpio -g write 17 1

    name: Turn off Relay1

  - action: Relay2On

    command: gpio -g write 18 0

    name: Turn on Relay2

  - action: Relay2Off

    command: gpio -g write 18 1

    name: Turn off Relay2

﻿





**Eric Lien**

---


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/Z5aD7RxJcj3) &mdash; content and formatting may not be reliable*
