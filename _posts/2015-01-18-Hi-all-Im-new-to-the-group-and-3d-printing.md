---
layout: post
title: "Hi all. I'm new to the group and 3d printing"
date: January 18, 2015 20:19
category: "Discussion"
author: Daniel Salinas
---
Hi all. I'm new to the group and 3d printing. I bought a Robo3d on the cheap and have been immediately hooked. I'm currently printing a full set of herculien parts as the RoBo was just meant to bootstrap me to a bigger printer and to have as a spare working printer should I break the herculien I'm building. Given that the robo3d is very Mendel like any tips for getting these abs parts right for the herculien. I was having delaminating issues but fixed that by slowing the print down and lowering the hot end temps but I'm still getting some warping where the bushing holes aren't true round. I'm sorry for asking this here but the same questions in Mendel boards usually turn into "why not build a bigger Mendel?"







**Daniel Salinas**

---
---
**Miguel Sánchez** *January 18, 2015 21:13*

Lowering temperature does not help improving layer adhesion, raising it is what is recommended. The usual suspects are air drafts and low temperature and excessive layer height. But other times bad quality filament is the cause. 


---
**Daniel Salinas** *January 18, 2015 21:17*

Thanks. I don't have an enclosure for this printer so it is most likely drafts. I have a space heater blowing near the printer but not on it now and layers seem to be sticking pretty good. Also rafting the models has reduced the warping on the lower layers on this next print. I'm busy going through the herculien BOM and ordering hard parts now. So excited!


---
**Miguel Sánchez** *January 18, 2015 21:19*

And lowering the speed helps too as you already mentioned. 


---
**Eric Lien** *January 19, 2015 00:31*

For abs I run at 235c hotend temps, and the bed at 110c. Slowing things down will help adhesion. Also if you are having troubles print 1 part at a time. The longer print times of multi-part prints means a longer time that the bottom of you part is at a dissimilar temp to the top. Also last resort would be put a big cardboard box over the whole printer. It will eliminate drafts, and if you preheat your bed for a while before starting the print it will act like a passively heated chamber.



Best of luck.


---
**Daniel Salinas** *January 19, 2015 21:18*

I thought about the cardboard box, maybe lined with aluminum foil to keep the heat in.  I went out and bought solid insulated foam with thermal barrier  to make a box around the printer.  Hopefully this will reduce drafts and keep things warm enough.


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/deYJ9BaswzX) &mdash; content and formatting may not be reliable*
