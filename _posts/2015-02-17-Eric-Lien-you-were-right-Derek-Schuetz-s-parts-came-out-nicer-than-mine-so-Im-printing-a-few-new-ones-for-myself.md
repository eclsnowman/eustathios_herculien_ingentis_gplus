---
layout: post
title: "Eric Lien you were right. Derek Schuetz 's parts came out nicer than mine so I'm printing a few new ones for myself"
date: February 17, 2015 14:28
category: "Discussion"
author: Daniel Salinas
---
**+Eric Lien** you were right.  **+Derek Schuetz**'s parts came out nicer than mine so I'm printing a few new ones for myself. :D





**Daniel Salinas**

---
---
**Derek Schuetz** *February 17, 2015 15:09*

Thanks **+Daniel Salinas**


---
**Eric Lien** *February 17, 2015 17:50*

Never hurts to have spare parts. If you need any stl's adjusted for your printers output (like bearing holes) let me know.


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/C6RLHTLsHaB) &mdash; content and formatting may not be reliable*
