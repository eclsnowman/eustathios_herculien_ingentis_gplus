---
layout: post
title: "My build will start shortly, received these printed parts today - Thank you to Alex Lee !"
date: February 25, 2015 16:09
category: "Show and Tell"
author: Bruce Lunde
---
My build will start shortly, received these printed parts today - Thank you to **+Alex Lee**​! Mitsumi  order due Friday!

![images/46dd7d03baa1d47ca47ea5676959b98f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/46dd7d03baa1d47ca47ea5676959b98f.jpeg)



**Bruce Lunde**

---
---
**Ricardo Rodrigues** *February 25, 2015 16:25*

Lucky you, they seem really nice detail.


---
**Chris Brent** *February 25, 2015 16:56*

Yes, amazing looking prints! Which printer were they done on?


---
**Bruce Lunde** *February 25, 2015 16:59*

**+Alex Lee**  will have to answer, I know he has at least two printers, maybe more! They look great, I look forward to printing that quality soon!


---
**Isaac Arciaga** *February 25, 2015 20:36*

**+Bruce Lunde** congrats! I hope a pictorial of your build is in the works! **+Alex Lee** it's all about patience and practice my friend.The quality of our prints didn't happen over night. Get to know your machine. BE THE MACHINE!! ;)


---
**Gus Montoya** *February 26, 2015 01:35*

Oh WOW! I'm excited for you **+Bruce Lunde**. That bumps me up to 3# now :)


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/Jca3QpU1ufK) &mdash; content and formatting may not be reliable*
