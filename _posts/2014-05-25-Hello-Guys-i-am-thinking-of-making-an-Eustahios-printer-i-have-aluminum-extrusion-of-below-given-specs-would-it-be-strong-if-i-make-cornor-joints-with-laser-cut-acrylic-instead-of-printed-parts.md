---
layout: post
title: "Hello Guys i am thinking of making an Eustahios printer i have aluminum extrusion of below given specs would it be strong if i make cornor joints with laser cut acrylic instead of printed parts"
date: May 25, 2014 15:52
category: "Discussion"
author: Bijil Baji
---
Hello Guys i am thinking of making an Eustahios printer i have aluminum extrusion of below given specs would it be strong if i make cornor joints with laser cut acrylic instead of printed parts

![images/5853a8d7b66f8f7058bc0d4454b0e725.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5853a8d7b66f8f7058bc0d4454b0e725.jpeg)



**Bijil Baji**

---
---
**Riley Porter (ril3y)** *May 25, 2014 15:56*

It prolly would be.  However the Eustahios has quite a bit of mass.  So I am not sure.  I would get the corner aluminum connectors and put them in place where possible.  I too like the idea of laser cut bearing mounts much better.  **+Jason Smith** makes it look easy to align the rods, however I did not do as well with my plotter bot design.  

[https://www.flickr.com/photos/rileyporter/13988054029/](https://www.flickr.com/photos/rileyporter/13988054029/)

Let me know if you end up doing it.


---
**Bijil Baji** *May 25, 2014 15:58*

**+Riley Porter** Sure


---
**Riley Porter (ril3y)** *May 25, 2014 16:01*

I should note that **+Jason Smith** came over and "fixed" my rods so my machine would move much smoother!


---
**Eric Lien** *May 25, 2014 17:45*

**+Jason Smith** I think I have my rods level. But would you mind letting me know your tricks. I put the carriage in the center then leveled all mine with a caliper to the frame. Then moved the the carriage in each corner loosened the bottom rod bearing block bolts to let it relax to its preferred location, then tightened it up.﻿


---
**Jason Smith (Birds Of Paradise FPV)** *May 25, 2014 18:16*

**+Eric Lien**, the method you describe here is the best that I've found.  It definitely takes some time to break a machine in before it reaches optimal smoothness.  I should add that as long as the carriage travels smoothly, you have make up for any slight unlevelness with the bed adjustment.


---
**Jason Smith (Birds Of Paradise FPV)** *May 25, 2014 18:26*

**+Bijil Baji**, I think most folks using the aluminum extrusion for these frames have gone away from any printed or laser cut parts for the corners.  Someone (I think **+Tim Rastall** maybe, can't find it right now) made a video showing how the extrusion can be fastened at the corners by tapping the ends of the intersecting pieces and sliding the head of the bolt into the channel of the perpendicular extrusion.  Between this method and the commonly available 90 degree brackets, that should be all you need.


---
**Bijil Baji** *May 25, 2014 18:46*

**+Jason Smith**  ok it would be great if i could see how to assemble the problem with extrusion i have is that it has a large square  hole in the middle so mounting screw would be difficult


---
**D Rob** *May 26, 2014 00:06*

**+Bijil Baji** I see now. you could print an insert thread it and epoxy the insert into the square hole. or if you have a piece to test it on you could see if you could run a tap into it and get enough threat on the 4 points to be suffifient. But why such large extrusion 40x40 is twice what most use. I have a machine with sae 1010 and it is too big 1"x1" I now use 2020 


---
**Tim Rastall** *May 26, 2014 00:33*

Unless you are looking to make a huge machine,  4040 would be, a waste.  A hundred bucks will get you enough 2020 for a 300x300 build volume. 


---
**Jarred Baines** *June 05, 2014 02:38*

I say:



If you have the funds for it and the patience / skills to re-design parts for 4040 - Go for it!



I see nothing wrong with over-engineering parts, it means you will be better placed if you decide to make it bigger, or beef up other components...



Might be a waste if you're building a stock-standard ingentis build with 4040, but if you plan on tinkering and upgrading it's not "crazy" ;-)


---
**Bijil Baji** *June 07, 2014 03:02*

**+Jarred Baines** I have almost half a ton of 4040 extrusion in my store so funds and meterial is not a problem. If you could suggest me a good full design link for and ingentis design as a start??


---
**Eric Lien** *June 07, 2014 03:24*

You could try my Solidworks version of **+Jason Smith** ’s Eustathios. Its like ingentis but more belts and motors inside the frame footprint. [https://github.com/eclsnowman/Lien3D_Eustathios_Spider](https://github.com/eclsnowman/Lien3D_Eustathios_Spider)



There are also step files, sketch up, and other formats. But as I make updates I do not keep the other versions as current.



Or Jason's files [https://github.com/jasonsmit4/Eustathios](https://github.com/jasonsmit4/Eustathios)



Otherwise **+Tim Rastall** posted his in this community a few posts back.﻿


---
*Imported from [Google+](https://plus.google.com/+BijilBaji/posts/eoAqJUMYTvB) &mdash; content and formatting may not be reliable*
