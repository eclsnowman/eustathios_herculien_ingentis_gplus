---
layout: post
title: "10 boxes just arrived from Misumi today"
date: February 04, 2015 17:22
category: "Discussion"
author: Daniel Salinas
---
10 boxes just arrived from Misumi today.  This is happening!  or maybe not.  Last night my Robo3D decided it was gonna break.  I wonder if anyone here can help me figure out whats up with it.  I don't think it's a stl/slicing issue but rather a power supply issue.  Or a stepper controller issue.  The Z axis isn't consistently moving up and down at the right steps.  All this was dialed in perfectly and printing wonderfully for over a week.  Now if I tell the Z axis to move up or down it will start moving and then start stuttering.  It's not binding and it's not humming like it's got too low current but rather moving in short bursts.  I swapped out my arduino mega and ramps board for a spare and it still happens.  Reset all my in memory settings to default configurations from Robo3D and it still happens.  The only 3 things that haven't been swapped out are the steppers, the stepper controller and the power supply.  I need to get a meter on it to see if current is right but my gut tells me the cheap ass power supply isn't supplying enough current.  I overnighted a new one today so I guess tomorrow I'll be able to eliminate the PS.  I have a spare 8825 controller so I can also swap that out.  The robo has 2 steppers on z axis using one controller so that is why I think it's a stepper controller or PS issue.  Thoughts?





**Daniel Salinas**

---
---
**D Rob** *February 04, 2015 17:24*

Plug the z steppers into x or y. Does it still do this? This should rule out or confirm Motor/driver issues


---
**Daniel Salinas** *February 04, 2015 17:35*

**+Derek Schuetz** this is gonna delay my start of your prints but the abs arrived this morning.  If you find someone that can print sooner let me know and I can put that box in the mail to them but because I need the Robo3D up and running asap for my own stuff, rest assured I'm spending all the time I can on fixing it.


---
**Mutley3D** *February 04, 2015 17:44*

Try lubricating the Z screws with a couple of drops of light oil, might be dry/binding. I know you mention nothing binding, but its hard to determine if Z screws are binding, which could appear as though theres a stepper driver or voltage issue.


---
**Mike Kelly (Mike Make)** *February 04, 2015 18:23*

Lubrication is a great idea. Though it might also be a cooling issue. If the stepper driver is overheating it will behave similarly to what you're describing. Make sure there's a fan that's providing sufficient cooling.



Head over to [forums.robo3dprinter.com](http://forums.robo3dprinter.com) if you need more assistance. 


---
**Daniel Salinas** *February 04, 2015 18:31*

Theres definitely a fan on the stepper driver and I did lube the Z screws.  I think I've narrowed it to PS but I'll be sure once I get a meter on it.  Hint 1. power supply fan doesn't come on.  It tries but makes a sad noise.  Hint 2. The 12v led strips that are on the same ps are dimming considerably when the z axis kicks in.  I think I've got low current coming from the power supply.  When I get off work I'll confirm with my meter.


---
**James Rivera** *February 04, 2015 22:11*

Definitely sounds like the power supply is dying.


---
**Derek Schuetz** *February 04, 2015 23:19*

**+Daniel Salinas** just keep me updated on how its going. i know the frustration of that. I just finished ordering everything and its beginning to trickle in already. Misumi order isn't schedule till the 10th ti ship, and i wouldn't need the printed parts till then


---
**Daniel Salinas** *February 05, 2015 18:22*

I completely cleaned the Z axis lead screws and lubed them and things seemed to move a little better but still stuttered.  Put in a new PS and blamo! Prints are working great.


---
**Mutley3D** *February 05, 2015 18:28*

**+Daniel Salinas** Interesting, good to know and glad to hear you've got the fix :)


---
**Derek Schuetz** *February 05, 2015 18:31*

Woot!


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/Y3fwpeTuqd8) &mdash; content and formatting may not be reliable*
