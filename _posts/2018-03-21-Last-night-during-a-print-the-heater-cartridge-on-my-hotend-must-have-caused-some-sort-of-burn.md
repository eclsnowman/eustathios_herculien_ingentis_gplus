---
layout: post
title: "Last night during a print the heater cartridge on my hotend must have caused some sort of burn"
date: March 21, 2018 20:18
category: "Show and Tell"
author: Oliver Seiler
---
Last night during a print the heater cartridge on my hotend must have caused some sort of burn. Interestingly it kept working and even finished the print.

I found traces of ash on the print bed, as well as embedded in the print.

Just a wee reminder to be careful.



![images/de85cf92217774d3773bb98091ce6dc2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/de85cf92217774d3773bb98091ce6dc2.jpeg)
![images/70334d38fc5f7e81012bc7eed7e37937.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/70334d38fc5f7e81012bc7eed7e37937.jpeg)

**Oliver Seiler**

---
---
**Ryan Fiske** *March 21, 2018 20:34*

Yikes! Do you think the wires going into the cartridge shorted out? That always struck me as a very likely failure point.


---
**Oliver Seiler** *March 21, 2018 20:43*

I don't think the wires shortened, cause that would have made it stop heating (and kill the FET/fuse on the controller), and also they didn't touch. One of the wires came off though when I took it apart, so it might have been that one cable was weak and overheated because of that.




---
**Dennis P** *March 21, 2018 21:33*

Super Scary!  Glad it wasn't worse. How much damage to the printer? What material were you printing? I often worry about leaving them unattended like being away or out of the house or asleep, seems like many of these components are of questionable provenance and no UL or similar ratings. 

 


---
**Oliver Seiler** *March 21, 2018 21:41*

I was printing PET at 255 degrees. The heater block (with fittings) and heat break is toast, but the thread on it was already wearing out, so luckily I had a replacement sitting here. 

Overall little damage for what it could have been, it seems that the cable isolation has some fire retardant in it which prevented them from catching fire.

Time to check the smoke detectors and making sure there's nothing combustible above the printer/cartridge.


---
**Scott Hess** *March 21, 2018 21:58*

I've been wondering if there's any way to teach OctoPrint (or comparable) to identify issues like this from the video stream.  Not only flames, but also filament balls and the like.  Or even when a big nest of filament twigs is just sliding back and forth with the head.  Then it could ping my phone with an image and a clean way to cut my losses.



I also have been wondering if an enclosed print bed would help in any way by limiting the available oxygen.  [Which just made me think you could use a sensor in the enclosure to detect oxygen and CO2 ratio, hmmmm.]


---
**Dennis P** *March 21, 2018 22:03*

**+Scott Hess** how bad does your heater smell when under power? Mine is still new and has that 'new' smell still. 

**+Oliver Seiler** did you have any insulation on the heatblock? like the silicone socks or kapton/mineral wool paper? since you said threads on heat break were wearing, i wonder if it got loose and you sprung a molten plastic leak. ???




---
**Scott Hess** *March 21, 2018 22:08*

**+Dennis P** I've not built one of these, yet, just lurking.  My Monoprice mid-range device has maybe 70 hours on it, and when printing PLA it just smells like there's something hot around, but it doesn't smell like burning.


---
**Oliver Seiler** *March 21, 2018 22:14*

**+Dennis P** No insulation on the heat block and the nozzle was fastly secured (and no gunk on the top of the block where it would also leak). I had a hard time getting it out of the hot heater block and the heatbreak broke when I tried to unscrew it.

All this happened shortly after I left the room and it carried on printing fine for about 4 hours. One of the 2 parts printed is totally fine, just the one in the photo has all the ash embedded.


---
**Ryan Carlyle** *March 23, 2018 01:53*

There's a fair chance that printing PET kept your house from burning down. It's moderately flame-retardant (self-extinguishing upon removal of heat source). ABS or PLA could have ignited. Burning plastic has more energy per kg than gasoline... 


---
**Oliver Seiler** *March 23, 2018 02:04*

**+Ryan Carlyle** I know, one of the reasons that my printer sits in a shed and not in the house and I've connected smoke detectors that all sound when one goes off.

There wasn't much combusible material in proximity (small printed part plus the carriage made from PET) and a small fire on the print bed probably wouldn't have spread further, but you're right, every reason to be careful.

I've spent 10 years in the fire service and have seen what can go wrong - not from 3d printers, but plenty of other stuff.


---
**Ryan Carlyle** *March 23, 2018 02:21*

**+Oliver Seiler** Exactly why I refuse to mount spools of filament directly above the printer... have to consider flammables nearby!


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/P4WiyYFXVPp) &mdash; content and formatting may not be reliable*
