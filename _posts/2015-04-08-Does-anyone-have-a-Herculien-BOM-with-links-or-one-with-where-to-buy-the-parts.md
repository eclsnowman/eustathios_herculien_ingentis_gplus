---
layout: post
title: "Does anyone have a Herculien BOM with links or one with where to buy the parts?"
date: April 08, 2015 23:17
category: "Discussion"
author: Gunnar Meyers
---
Does anyone have a Herculien BOM with links or one with where to buy the parts?





**Gunnar Meyers**

---
---
**Seth Messer** *April 08, 2015 23:23*

Hey there **+Gunnar Meyers** the links to the right (if you're viewing on the Google+ website) has a link to the HercuLien related files, including the BOM on GitHub.  If for some reason you can't view those links, then here you go:  [https://github.com/eclsnowman/HercuLien/tree/master/BOM](https://github.com/eclsnowman/HercuLien/tree/master/BOM)


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/jjAHuZSk4DX) &mdash; content and formatting may not be reliable*
