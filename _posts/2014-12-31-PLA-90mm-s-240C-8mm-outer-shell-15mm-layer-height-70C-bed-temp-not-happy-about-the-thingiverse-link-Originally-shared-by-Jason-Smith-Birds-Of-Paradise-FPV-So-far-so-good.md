---
layout: post
title: "PLA, 90mm/s, 240C, .8mm outer shell, .15mm layer height, 70C bed temp, - not happy about the thingiverse link :) Originally shared by Jason Smith (Birds Of Paradise FPV) So far so good"
date: December 31, 2014 01:56
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
PLA, 90mm/s, 240C, .8mm outer shell, .15mm layer height, 70C bed temp, [http://www.thingiverse.com/thing:264769](http://www.thingiverse.com/thing:264769) - not happy about the thingiverse link :)



<b>Originally shared by Jason Smith (Birds Of Paradise FPV)</b>



So far so good.  This is a long print...11hrs.  Hopefully it won't be too "stuck" together when it's done.

![images/18c665994a357ce69c68667d20e39f5b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/18c665994a357ce69c68667d20e39f5b.jpeg)



**Jason Smith (Birds Of Paradise FPV)**

---
---
**Eric Lien** *December 31, 2014 02:17*

I have trouble cooling pla for gear prints like this at those speeds .. Always ends up fused.



Great job.


---
**Nick Parker** *December 31, 2014 03:38*

Bit of a silly design though no?



Unless you have a spider driving the planets, the internal bearings don't do anything do they?


---
**Nick Parker** *December 31, 2014 04:08*

Yup didn't look, prefer not to give thingiverse traffic.



That's a slick design though. Makes me want to drive both inputs on it and build a hybrid ~something~.


---
**Mark “MARKSE” Emery** *December 31, 2014 08:19*

You could always use Cura to split the design up so the parts have more spacing during printing. A trick I was told by the good folk here only days ago..


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/XZ8fHdK5g5U) &mdash; content and formatting may not be reliable*
