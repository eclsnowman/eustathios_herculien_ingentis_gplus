---
layout: post
title: "After a long time away I finally got it finished"
date: October 29, 2015 02:21
category: "Deviations from Norm"
author: Matt Miller
---
After a long time away I finally got it finished.  Break-in gcode ran with zero problems.  Test prints tomorrow, then some calibration.  



Still have some housekeeping to do (networking, Octoprint, swap out 5V blowers for 12V, etc) but that'll come along in the next few days.



Overall, very pleased.  Thanks for the inspiration **+Eric Lien**! 





**Matt Miller**

---
---
**Brandon Satterfield** *October 29, 2015 02:57*

Awesome!


---
**Eric Lien** *October 29, 2015 11:12*

Oh man. That looks amazing. Love all the machined upgrades. It's a beast. Hope you would be willing to share some of the files, and some close-up pictures. 


---
**Matt Miller** *October 29, 2015 13:25*

It's pretty disorganized right now, but the files are here:

[https://drive.google.com/folderview?id=0B6yPgtXXa9tXZXZON21YSC1rMlk&usp=sharing](https://drive.google.com/folderview?id=0B6yPgtXXa9tXZXZON21YSC1rMlk&usp=sharing)



I'll organize it and toss it up on github this weekend.


---
**Eric Lien** *October 29, 2015 13:35*

**+Matt Miller** Thanks Matt. Can't wait to look over the folder for good ideas to steal :)


---
**Matt Miller** *October 29, 2015 13:42*

Steal away!  :D


---
**Anthony Webb** *October 29, 2015 16:55*

This is really cool, excited to see more footage of it in action and the repo.


---
**Mike Mills** *November 25, 2015 01:08*

Just when I thought I couldn't get more excited about this build...


---
*Imported from [Google+](https://plus.google.com/+MattMiller_akhlut/posts/3mFxwxKNxHH) &mdash; content and formatting may not be reliable*
