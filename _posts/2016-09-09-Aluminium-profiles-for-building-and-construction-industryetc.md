---
layout: post
title: "Aluminium profiles for building and construction, industry.......etc"
date: September 09, 2016 05:16
category: "Show and Tell"
author: David Pan
---
Aluminium profiles for building and construction, industry.......etc

![images/254f1dba88de7dc3b6abe911a7b6dd4c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/254f1dba88de7dc3b6abe911a7b6dd4c.jpeg)



**David Pan**

---
---
**Eric Lien** *September 09, 2016 05:29*

**+David Pan**​ thank you for the post. If you have something specific that you can provide to the community please let us know. But if you joined the community just post as a general advertisement that is unfortunately not allowed.


---
**David Pan** *September 09, 2016 05:38*

**+Eric Lien** thank you Eric, I can understand your point very well, not only for advertisement, will update more specific to the community very soon, thank you. 


---
**Eric Lien** *September 09, 2016 12:36*

**+David Pan** sounds good. Thanks.


---
*Imported from [Google+](https://plus.google.com/103120662790760188089/posts/6okuArVdRjC) &mdash; content and formatting may not be reliable*
