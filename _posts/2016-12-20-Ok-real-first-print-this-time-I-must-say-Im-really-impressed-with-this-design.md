---
layout: post
title: "Ok, real first print this time. I must say, I'm really impressed with this design"
date: December 20, 2016 08:38
category: "Build Logs"
author: Neil Merchant
---
Ok, real first print this time.



I must say, I'm really impressed with this design. I haven't spent much time tuning yet, but from the first print I'm getting passable parts. 



The vibration I mentioned before accounts for some of the striations in the extrusion, but with some further inspection I uncovered another likely culprit - one of my bearing holders is cracked! Hopefully replacing that part and working through my alignment a few more times will help. My circles also aren't perfectly circular, but with any luck the cracked bearing holder will account for that as well.



It feels like it's been a pretty long road working on this thing whenever I can sneak a day off work, but I must say - it's incredibly rewarding to see it up and printing for the first time. Of course, once I get it all up and running perfectly we'll have to see about adding that flying extruder or maybe a chimera... The build process will never end! ;)



![images/3f062769aa5407a74047569382cc78ba.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3f062769aa5407a74047569382cc78ba.jpeg)
![images/b3fab09b98696f8db4d9ff1a57854500.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b3fab09b98696f8db4d9ff1a57854500.jpeg)
![images/0e2fdc102418a8c4570fb17d7540e15c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0e2fdc102418a8c4570fb17d7540e15c.jpeg)

**Neil Merchant**

---
---
**Eric Lien** *December 21, 2016 02:13*

Great looking first print. Keep us updated as you work through the break-in tuning. It really helps others to learn from people's troubleshooting even more than the successes.


---
*Imported from [Google+](https://plus.google.com/105839137113604539428/posts/AuHR2nGX7ja) &mdash; content and formatting may not be reliable*
