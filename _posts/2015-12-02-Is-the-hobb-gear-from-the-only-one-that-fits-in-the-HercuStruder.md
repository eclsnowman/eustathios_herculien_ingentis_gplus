---
layout: post
title: "Is the hobb gear from the only one that fits in the HercuStruder?"
date: December 02, 2015 17:16
category: "Discussion"
author: Florian Schütte
---
Is the hobb gear from [makerstoolworks.com](http://makerstoolworks.com) the only one that fits in the HercuStruder? I ordered some at RobotDigg, but they don't fit :(





**Florian Schütte**

---
---
**Florian Schütte** *December 02, 2015 17:17*

BTW it's not the diameter. They are to tall.


---
**Eric Lien** *December 02, 2015 17:44*

Others may fit, but I used the makertoolworks one... So it's the only one I can guarantee myself that it works.


---
**Florian Schütte** *December 02, 2015 17:47*

May you can tell me the dimensions of the makertoolworks one (height, OD)? I'm not really willed to pay $30 shipping for it. Want to look for other sources.

 


---
**Eric Lien** *December 02, 2015 20:00*

Hope this helps: [http://imgur.com/a/FIokV](http://imgur.com/a/FIokV)


---
**Florian Schütte** *December 02, 2015 20:14*

This helps a lot. Turns out this are the standard MK7 hobb gear dimensions.


---
**Joe Spanier** *December 03, 2015 02:57*

I'm using the ones from utibots and a another mk8 drive wheel from some other random place and the hobb goblins from E3D. 


---
**Florian Schütte** *December 03, 2015 11:42*

I had a second look at robotdigg product page for the hobb gear. They sell them as MK7 Gear, but they dont fit, because they don't have the dimensions mentioned on product page (and needed to be MK7 compatible). They also messed up some other items. I'm very dissappointed about my **+Robot Digg** order this time.




---
**Bud Hammerton** *December 03, 2015 15:15*

I was going to suggest the HobbGoblin also. But are you sure the RobotDigg doesn't fit? That is what I have on mine and unless they changed it in the last month it fits perfect. MK7 with 8mm bore.



Did you go direct to RobotDigg or through their AliExpress store? This information is on the specific page:

MK7 Filament Drive Gear

Height 11mm

Diameter: 12mm

Bore Size: 5mm or 8mm

For 1.75mm 3d printing filament.


---
**Florian Schütte** *December 03, 2015 15:44*

**+Bud Hammerton**​ My two gears i ordered are 13mm in height. So they do not fit. Also the Extruder is designed for 12.7mm diameter. Isn't that an issue for you? 


---
**Bud Hammerton** *December 03, 2015 18:49*

**+Florian Schütte** I wish I could tell you that I had issues also, but I can't. It fits perfectly and there is wiggle room to perfectly adjust the hobbed gear to align it precisely with the filament guide holes. I have not taken it apart to see if the part you describe differs from the part I got. All I can say is that it fits on my HercuStruder without any modifications.



I did some quick measurements, the one I got from them is 12.7 diameter, it is 12 mm diameter hobbed area, the length is 11 mm, exactly as the description they provide.  Are you sure you ordered part number MK7DG ([http://www.robotdigg.com/product/390](http://www.robotdigg.com/product/390))?


---
**Florian Schütte** *December 03, 2015 18:58*

A simple way will be to change the model to fit. But i'm not able do get it done in Fusion360. And sketchup is even more a mess. Managed to give it the extra 2mm in height, but i fail on repositioning the motor mountig holes (0,35mm nearer to the filament because OD is 0,7mm less). All the rounded edges will mess up the sketchup model.


---
**Florian Schütte** *December 03, 2015 19:00*

**+Bud Hammerton** I kow they write 11mm height in the produkt description. But they are 13mm. **+Robot Digg** did not answer to my questions about that.




---
**Bud Hammerton** *December 03, 2015 19:41*

The only thing I can say then is that they changed the item since I ordered mine on October 15th, It is exactly as the description and fits in the Hercustruder. I feel your pain and truly hope you can work it out with Robotdigg. I too have found them extremely slow in responding to emails.


---
**Eric Lien** *December 03, 2015 22:07*

**+Florian Schütte** give me the dimensions you want changed and I will change it tonight when I get back to the hotel in Solidworks.


---
**Florian Schütte** *December 04, 2015 04:12*

Thakns **+Eric Lien**. I found an easy way to get the extra 2mm. I lowerd the base and made the hole for the gear 2 mm deeper. Printed out and it fits and works. The only thing im not happy with is the smaller diameter of the gear which leeds to nearly no space between bearing and filament leads. I fiddled around a little bit in fusion 360 to bring the mortor mounts and shaft hole 0.35mm nearer to the filament lead, but i'm not happy with the result. Didn't manage to adjust the radius of the "gear cage". But it seems to work, so for now i'm happy with that.


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/94YrTNeUAJY) &mdash; content and formatting may not be reliable*
