---
layout: post
title: "One of the variables in the current design that needs work is the Z-axis...sure, it currently produces fine results, but a loss of power has a platform crash in the Z direction...which is sub-optimal"
date: March 05, 2014 15:28
category: "Deviations from Norm"
author: Mike Miller
---
One of the variables in the current design that needs work is the Z-axis...sure, it currently produces fine results, but a loss of power has a platform crash in the Z direction...which is sub-optimal. Springs can limit the damage but that seems, generally, like something that can be improved upon. (a 2am power failure should not result in a heart attack)



Case in point: Including a rotary damper (like, but not specifially this: [http://www.mcmaster.com/#6597k8/=qynk6b](http://www.mcmaster.com/#6597k8/=qynk6b)) would make power failure a graceful failure, as would using Leadscrews in the Z axis (expensive, with cantilever issues), or a Worm Gear (expensive, untested)



Originally I wanted a topic to handle the Z axis as it seems that's the component of the printer that needs the most development, but really, a category that listed all deviations from the originating design could use thier own category.





**Mike Miller**

---
---
**Mark Hindess** *March 05, 2014 15:35*

Would shorting the motor coils on power failure (e.g. with a relay) create enough hold to prevent damage?


---
**D Rob** *March 05, 2014 15:36*

my answer to this is a worm drive. manufactured ones are expensive and hard to match a set for our needs. I have a set I printed and soon as my other life allows I will give some results on how well this approach works


---
**Mike Miller** *March 05, 2014 15:45*

I'm not to where I can produce gears yet...I could see having a bronze worm and a printed gear, I'm not sure having both printed would be a strong enough solution, and there's a good chance the layers of the printed gears might cause artifacts. 



The diy acetal nuts I posted about in the 3d printer group are a possibility, with the added cost of two acme leadscrews or a leadscrew and additional rail(s)﻿


---
**Mike Miller** *March 05, 2014 15:48*

**+Mark Hindess** what happens to the RAMP in an unintentional shorting situation? The motor could hold things in place (or provide enough drag to slowly lower the platform), but I wouldn't want it to potentially harm the controller. 


---
**Mike Miller** *March 05, 2014 15:50*

Another option would be to have a fixed bed and move the extruder in the Z direction, I'm sure that raises a host of other issues. (Including doubling the vertical envelope .)


---
**Mark Hindess** *March 05, 2014 16:21*

**+Mike Miller** I've not thought it through but it should be possible to have the relay switch from ramps to shorted so you wouldn't end up with a short at the driver.


---
**Dale Dunn** *March 05, 2014 16:23*

I've been thinking about this for quite a while, here and in the RepRap formus ([http://forums.reprap.org/read.php?279,222917,page=4](http://forums.reprap.org/read.php?279,222917,page=4)).  I particularly like this mockup by acrux: [http://forums.reprap.org/file.php?279,file=19863,filename=z_axis.jpg](http://forums.reprap.org/file.php?279,file=19863,filename=z_axis.jpg).



I don't really trust that method of driving filament or cable though. This arrangement could be done with a toothed belt driven by a worm gear connected to one of the top pulleys. There is a method for making worm gear sets for use with threaded rods: [http://www.astronomyasylum.com/gears.html](http://www.astronomyasylum.com/gears.html)



Another way to use Spectra or cable would be to connect it to nut running on a screw along one of the straight runs. The belt would isolate the bed from wobble, but you would need to find a way to keep the nut (and cable) from turning around the screw.



I like the idea of making a worm gear set, but I'm leaning toward the screw and belt idea as the easiest to implement.


---
**William Frick** *March 05, 2014 16:35*

The issue is stopping the bed from falling on power loss. Why not a brake solenoid energized through a relay or controlled output. Power fail > brake applied ...


---
**Mike Miller** *March 05, 2014 16:55*

**+William Frick** I like the way you're thinking...we could have air brakes, like on Semi Tractor Trailers. :D


---
**D Rob** *March 05, 2014 16:56*

a worm gear only transfer motion to the axle when the worm turns. the worm wheel cant turn the worm


---
**D Rob** *March 05, 2014 17:10*

Even with "air breaks" as **+Tim Rastall** rastall pointed out many moons ago the steppers have a timeout in firmware that would need to be changed. ergo if you leveled your bed and waited x amount of minutes for it to heat or any reason and it surpasses the set time the motors will go slack and drop the bed. though it would be cushioned it would still move. a spring on the rods is an easier and cheaper solution. If you want the bed to remain stationary a mechanical change needs to be made. I have thought about a ratcheting system but that wont work for variable z heights. I honestly believe the most elegant solution is a worm and wheel set up. The weight of the platform holds a constant back pressure and if the z isnt required to make fast movements backlash would be mitigated


---
**Whosa whatsis** *March 05, 2014 17:43*

The relay idea could potentially damage your stepper driver if it malfunctioned, even if it was wired so that the driver sees open-circuit when it is activated instead of a short, but you could to the same thing mechanically with a solenoid mounted so that its return spring brakes the Z axis when power is removed.


---
**William Frick** *March 05, 2014 17:54*

**+Whosa whatsis** That is what I was thinking, as the issue is a Z axis fall on power fail or some other fault.


---
**Whosa whatsis** *March 05, 2014 18:01*

**+William Frick** Ah, missed your comment, sorry.



Anyway, you wouldn't need a controlled output if you just want it brake on power loss. Just wire it directly to the power supply and the return spring will activate the breaking mechanism when power is lost. If you want it to brake any time the Z stepper is deactivated, even without power loss (like on reset), you could use a FET wired to the Z stepper enable pin.


---
**William Frick** *March 05, 2014 18:10*

**+Whosa whatsis** You could do both I suppose ... set brake on power fail, or fault detect of z-axis stepper.  PSU to one side of coil, ground return thru a controlled FET.  Either way when the relay/solenoid de-energizes > brakes applied.


---
**Ben Malcheski** *March 05, 2014 18:59*

I haven't started to build an Ingentis yet, but when I do I plan on using lead screws or possibly a single lead screw solution. I like many things about the design in general and the basic frame system was more or less what I had stewing in my head, but I have never been a fan of of the belts on the Z.



If people insist on using belts then I think the cheapest and simplest solution would be a counter weight system. Yes, it will change the tension in the belts during a print but this probably only matters with <b>very</b> large parts.


---
**D Rob** *March 05, 2014 19:39*

counter weights will eat into your build volume to outside footprint ratio king of defeats the purpose of such a space conservative design. lead screws can be good but can cause artifacts and wobble ymmv. and with the reduction of a worm gear Ibelieve max resolution can be achieved. the better the worm setup the bettes your results. 


---
**Tim Rastall** *March 05, 2014 20:00*

Great to see this discussion happening,  aside from a general unease about using  spectra on X and Y axis (mostly irrational as it works fine) the Z axis has never really been completed to my satisfaction.  I have a design for a large worm drive that I will print at some point soon,  it's scaled and has a pitch that should allow for good print quality. Aside from that,  I also have lead screws on the way from misumi,  I'm of a mind to try them too.  

The belts do give a very smooth and consistent layer height,  no Z ribbing or any of that malarkey and I'm glad I tried it.  I'd also be keen to see someone try rack an pinion,  the Z axis is the only viable candidate for this as it's performance won't be particularly effected by having the Nema mounted on the bed to drive the pinion, of course,  this would suffer the same fate as belts if power was cut.  Incidentally,  tats, exactly what happens to alfinia UP! Printers. 


---
**Ben Malcheski** *March 05, 2014 20:11*

**+D Rob** I'm pretty sure a weight of the proper form factor could be worked in between the bed frame and main frame of the printer.



In regard to z wobble with screws there are many solutions to that such as flex couplers and not over constraining them. I have also been playing with the idea of using smaller diameter lead screws and 10 mm linear rod or one of the linear rail/carriage systems. If you keep the linear motion components stiffer and the screws less rigid then you have less to worry about when it comes to the lead screws affecting quality.


---
**Mike Miller** *March 05, 2014 21:58*

Unless it's a chunk of lead ziptied to the belt, there will probably also be additional cost in one or more bearing slides and bearings for the conterweight to ride along. 



I had a thought where the bed could be lifted using filament on pullies, and the whole affair being driven by a leadscrew/motor mounted flat on the bottom. Loss of power would not cause a drop, but tuning might take a little effort. 


---
**Ben Malcheski** *March 05, 2014 22:30*

It wouldn't need to ride on a slide at all. I'm not sure how much the bed assembly weighs but it may not be practical in terms of volume to fit a piece of lead I suppose.


---
**Jarred Baines** *March 05, 2014 23:56*

I suggested this a little while back but not on such an active thread!



What about a pawl? Like a ratchet action on the Z gear? This would limit you to only moving Z in one direction with a basic setup, although you could install a solenoid to hold the pawl 'up' and if power is lost that solenoid (from my limited understanding) should disengage and drop the pawl 'down' to lock the z axis gear in place and prevent any further movement?



Only issue with the solenoid method vs the non-solenoid method is, if it doesn't lock back in quick enough, the gear may be spinning too fast and the pawl will have trouble engaging, at least with the first method, despite only having one direction of movement, you should not be able to crash the bed (or move it downward) without manual intervention.


---
**Jarred Baines** *March 06, 2014 00:42*

OR as an alternative to the worm-wheel method, and easy-to-print design, what about a planetary gearbox? Or something derived from Emmet's harmonic drive? [http://www.thingiverse.com/thing:219779](http://www.thingiverse.com/thing:219779)



They're meant to be able to hold position even when the drive forces are removed... which exactly describes our goal!


---
**Mike Miller** *March 06, 2014 01:57*

Both good possibilities, **+Jarred Baines**...I was additionally thinking of centrifugal clutches. They should be available in R/C sizes, and would act as a brake if one side is held stationary, yet not engage if movement was slow. 


---
**D Rob** *March 06, 2014 02:10*

as i mentioned above a ratcheting design would limit z layer height to the lock positions unless it only engaged when not printing ie a power loss. but this still doesn't solve the problem of a timed motor release. either the motor would be locked for prolonged times after activation or drop. if it dropped onto a ratcheted design chances are it is going to drop some and if this is a filament pause or the like you've lost position and the print will fail


---
**William Frick** *March 06, 2014 02:30*

**+D Rob** If the ratchet was on a gear identical to the z motors step then there would be no [measurable] loss. 


---
**D Rob** *March 06, 2014 02:48*

true but you have a better chance to print a worm gear set than a 1.8 degree ratchet/ solenoid set up


---
**Jarred Baines** *March 06, 2014 02:58*

In my experience... Loss of power means loss of print... Or at LEAST loss of position - I saved a print once by re-homing and manually setting the Z position where I thought it should be, but that wasn't a great result either.



What do you mean when you say "a timed motor release"?



Filament pause, as in "a pause in the program so you can change filament"? If this was the only issue, it is fixable in firmware - Z needs to stay locked (along with X and Y) and you need a code to unlock E.



The issue we CAN'T fix with firmware is the "loss of power = crash" issue.



A pawl would likely fix that. It doesn't need to positively lock at each layer height, it just needs to stop the <b>BANG</b> encountered at power-loss.



and **+Mike Miller** I was just returning to suggest that, or even a sprag clutch, overcoming the issue of the pawl 'locking into' a tooth (and the possibility of it NOT locking into a tooth!) The centrifugal clutch has been standardized for seat belts, seems like it could work here too, it's pretty much an identical situation.



This is all if you REALLY want to not use screws... I'd just use screws, problem solved. But this is good exercise for the brain ;-)


---
**D Rob** *March 06, 2014 03:20*

I meant if the filament stripped and you paused the print to fix the issue. or the like. if you stop the motors and let it stand for a few minutes they go slack. yes there is a firmware adjustment that **+Tim Rastall** found a way to work around. but that means keeping the motors engaged for prolonged periods . If all anyone wants is crash protection do what tim does put springs on the rods


---
**Jarred Baines** *March 06, 2014 03:54*

?



I'm sorry, in this design, the Z motors should be ALWAYS engaged during printing... if they ever disengage, the stage drops... there's nothing mechanical stopping the Z stage from falling, it's PURELY the z motors that hold the load for the entire print (if it works as I imagine... I haven't had time to get nitty-gritty and study this machine yet!)



So what you're asking of the motors while "pausing for a filament or other issue" is no more of a burden on them than printing is. They're performing the EXACT same function under a 'pause' as they are under a 'printing' scenario.



They shouldn't "go slack" either - what goes slack? do you mean the motor disengages completely? or it begins losing steps? or are you saying the belts go slack somehow?


---
**Mike Miller** *March 06, 2014 03:58*

It's a failure-mode...if the printer loses power, then things drop. I'd prefer it to do so gracefully (as less than 9.8 m/s^2)


---
**D Rob** *March 06, 2014 04:39*

Come on 32ft/s²  over a1 ft distance isn't that bad lol


---
**Tim Rastall** *March 06, 2014 10:05*

SO, Sublime posted on the Ingentis thread in reprap forums about a fluid friction brake in response to this discussion. He's anti google so won't post directly. Anyway, as usual he's come up with a very clever solution.

[http://forums.reprap.org/read.php?279,222917,320475#msg-320475](http://forums.reprap.org/read.php?279,222917,320475#msg-320475)


---
**Eric Moy** *March 06, 2014 10:55*

**+Tim Rastall** , that is pretty much the insides of most rotary dampers. I'm glad to see this  discussion so active. Here's my two cents.

Any use of worm gears out rack and pinion will give the same artifacts in ribbing as a lead screw, the worm gear more so, since a worm is almost the same thing as a lead screw. The period of the rubbing will be different though.



As for braking the the z stage in case of power failure or axis timeout, in servo systems, brakes are applied to motors when not actuated to prevent overheating. The same can be done with steppers. There would need to be code added to apply the brake between moves, and it would obviously be a brake that engages under no power, disengaged under power. In marlin, just set the z axis to not be pureed between moves. Hmmm, actually, without adding code, like **+Whosa whatsis** said, you can use the motor enable pin to trip the brake. When it's not enabled, the brake engages. And when I say brake, it's generally attached directly to the motor shaft, preferably a motor with a shaft sticking out both ends. Brakes a are commercially available for large Nema sizes, not sure about 17 or 23


---
**Mike Miller** *March 06, 2014 11:11*

**+D Rob** It is at 2am!


---
**Eric Moy** *March 06, 2014 11:38*

[http://www.aliexpress.com/item/Brake-Stepper-motor-57mm-stepper-motor-57HB56-03-DC24V/1558064665.html](http://www.aliexpress.com/item/Brake-Stepper-motor-57mm-stepper-motor-57HB56-03-DC24V/1558064665.html)



This is similar to what I described.  I'm just not sure whether this is a power off brake, as there's no data on the part.  Just realized that most steppers with brakes are $200+, yikes.


---
**Wayne Friedt** *March 06, 2014 14:53*

**+Ben Malcheski**  I am actually using a 3mm screw for the Z in one of my bots. Stepps/mm are 6400. I have a solid coupler and use a " slop nut " to take care of any wobble. 


---
**D Rob** *March 06, 2014 19:16*

**+Jarred Baines** by goes slack I mean the motor is on a timed disengagement cycle after a command is given. If I move a motor via software, and there is a pause after, it stays engaged for a short period. Then I can move the shaft by hand. If I pause a print for whatever reason and leave the room the stage will fall if I exceed that axis' timeout period. True this can be changed in firmware. Or as I have done on my machine using a worm gear (printed) the z only moves when I tell it to. Power or no power pause or no pause


---
**D Rob** *March 06, 2014 19:18*

**+Mike Miller** huh?


---
**Jarred Baines** *March 07, 2014 00:51*

**+Eric Moy** I think that unit REQUIRES power to 'brake'...



If the power goes off, so does the brake... So it wouldn't have any effect on power outtage, the Z stage would just drop...



**+D Rob** oh, ok - I have mine setup differently, no timeout... To me it's important that the position gets held indefinitely until I want it to not be held... My firmware originally "unlocked Z" while it was not "in motion" - so that small Z tweaks could be made inbetween layers...



I see this as "Small amounts of error can accumulate between layers"... If I'm doing some crazy-fast prints and the bot is shaking a little I don't want to lose position!


---
**Eric Moy** *March 07, 2014 01:03*

**+Jarred Baines** , not sure, but I was looking foe power off fail safe brakes, which usually have large magnets, so you're probably right. But honestly, if the power goes out, I can't see how your print will be saved anyhow, in which case the springs would suffice.  The brake is good for any timeout issues.


---
**D Rob** *March 07, 2014 01:33*

voila brake [http://ecx.images-amazon.com/images/I/41IbJ1hTNOL._SY300_.jpg](http://ecx.images-amazon.com/images/I/41IbJ1hTNOL._SY300_.jpg)


---
**Jarred Baines** *March 07, 2014 02:08*

Lol @ **+D Rob** - I get a "translate" button under your post :-P



Merci!


---
**D Rob** *March 07, 2014 02:18*

me too for voila


---
**D Rob** *March 07, 2014 02:18*

see what I mean


---
**Mike Miller** *March 07, 2014 02:30*

**+D Rob** A platform crash due to a printer fault or loss of power would be kinda noisy in a quiet house in the middle of the night. 



Granted, my current experiences are of a printer with several rough edges left, a properly behaved printer is probably a lot quieter. 


---
**D Rob** *March 07, 2014 02:37*

yes i can see shot up walls over a loud crash in the night lol will probably drop at the end of prints if there is no custom g code to lower it at the end too


---
**Jarred Baines** *March 07, 2014 04:12*

Yeah, even with springs I don't think I'd be satisfied...



I do like sublime's idea, but I think that also will limit Z speeds, not a big problem, but if you attempt to raise or lower Z faster than that device will allow, it will cause pain somewhere else (belt slippage, losing steps etc).



It basically just adds friction / resistance to the setup... Solves one problem while possibly introducing another.



Do people consider Z-jumps necessary? I for one DO think they are necessary, especially on a bowden setup.


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/HHEGExE29Ax) &mdash; content and formatting may not be reliable*
