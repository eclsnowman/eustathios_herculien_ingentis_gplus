---
layout: post
title: "What is everyone printing the Herculien parts out of ABS, PLA, or other?"
date: May 30, 2017 17:55
category: "Discussion"
author: Jeff Kes
---
What is everyone printing the Herculien parts out of ABS, PLA, or other?  I have been playing with some eSun PLA+ and seems really good results.



Thanks in advance.





**Jeff Kes**

---
---
**James Ochs** *May 30, 2017 18:07*

I've been doing all of my parts using esun PETG.  It's been a little tough to get dialed in, but once it is the parts come out pretty nice, and they are more heat resistant and stronger than PLA.


---
**Carter Calhoun** *May 30, 2017 18:22*

I used eSun PLA+ for my Eustathios.  It seems fine to me, sufficiently strong.  I didn't have faith that I could get PETG right, lol.  I haven't printed more than 100 or so hours, but no issues due to printed parts.


---
**Eric Lien** *May 30, 2017 18:27*

If you use the panels on HercuLien (i.e. making it enclosed), then avoid PLA. International chamber temp can passively reach 55C when the bed is at ABS temps. At 55C the PLA will deform under load. So PETG or ABS are the preference for anything inside the enclosure. Or at least for anything inside that experience any loading.


---
**Jeff Kes** *May 30, 2017 19:12*

Now i see what you are talking about as the distortion temp for PLA is between 52 to 56 and ABS is at 78.  But I see PETG is only at 64 is that high enough? But I think ABS is a good option but I see PETG has a higher elongation.

  

![images/04719362795e5c6a21bde7996f2ea90b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/04719362795e5c6a21bde7996f2ea90b.jpeg)


---
**Eric Lien** *May 30, 2017 19:31*

**+Jeff Kes** I have all ABS parts. But I know **+Zane Baird**​ has been using PETG parts without issues.


---
**Jeff Kes** *May 30, 2017 19:34*

I guess I need to work on my ABS printing quality.  It has been good but I need to get a better enclosure. Thanks for the feedback.




---
*Imported from [Google+](https://plus.google.com/+JeffKes/posts/ctHSWZYbRJc) &mdash; content and formatting may not be reliable*
