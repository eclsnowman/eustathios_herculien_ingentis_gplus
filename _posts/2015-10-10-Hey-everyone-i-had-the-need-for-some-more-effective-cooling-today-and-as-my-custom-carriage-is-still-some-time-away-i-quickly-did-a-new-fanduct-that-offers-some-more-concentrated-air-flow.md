---
layout: post
title: "Hey everyone, i had the need for some more effective cooling today and as my custom carriage is still some time away i quickly did a new fanduct that offers some more concentrated air flow"
date: October 10, 2015 16:07
category: "Deviations from Norm"
author: Frank “Helmi” Helmschrott
---
Hey everyone,



i had the need for some more effective cooling today and as my custom carriage is still some time away i quickly did a new fanduct that offers some more concentrated air flow. It might not fit your needs but it does work quite good for me so i thought i'd share it.



See some render pictures attached - the duct is just the yellow part obviously :)



The stl file is here: [http://hel.me/eusthatios_duct](http://hel.me/eusthatios_duct) and here's he Step-File which should hopefully work in your favourite 3D CAD: [http://hel.me/Euthatios_Duct_step](http://hel.me/Euthatios_Duct_step) - let me know if you need anything else.



Regarding the print I did mine in Colorfabb XT CF 20 (PETG with Carbon fibre) and printed it upright without support (there's just one small bridge that is a bit of a challenge, everything else is quite easy to print. 



The hole on the left works with the screwhole in the default carriage but works the other way around i just attached it with a slightly smaller screw, therefore the hole in the duct is only 2mm.



![images/e54032838fb1545e1aa8f9a320ef843d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e54032838fb1545e1aa8f9a320ef843d.png)
![images/d2e9c640795d5b8b4f95c5fe9df28eee.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d2e9c640795d5b8b4f95c5fe9df28eee.png)
![images/8d6591d6ae367dfd4c06d83886217d9d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8d6591d6ae367dfd4c06d83886217d9d.png)

**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *October 10, 2015 16:33*

Looks great. Mind sharing the step?


---
**Frank “Helmi” Helmschrott** *October 10, 2015 16:35*

of course not - added it to the top post.


---
**Eric Lien** *October 10, 2015 16:55*

**+Frank Helmschrott** thanks. I am gonna print this today to test on a tricky long span bridge part I have been fighting :)


---
**Frank “Helmi” Helmschrott** *October 10, 2015 18:05*

Great, **+Eric Lien** - looking forward to hear if it worked for you too.


---
**Bud Hammerton** *October 11, 2015 18:00*

**+Frank Helmschrott** I took your design and modified it for the E3D Volcano. I am really surprised that more Eustathios/HercuLien builders aren't using the Volcano. It is posted here: [http://www.thingiverse.com/thing:1066144](http://www.thingiverse.com/thing:1066144) (STEP and IGES files included). I haven't printed this yet.


---
**Frank “Helmi” Helmschrott** *October 11, 2015 18:06*

**+Bud Hammerton** i have the volcano upgrade set lying here but didn't have a need to use it until now. What are you printing primarily that gives you a need for it? Maybe i'm not printing big enough all day :)


---
**Bud Hammerton** *October 11, 2015 23:50*

Theory is that with a larger hot zone, you could technically print faster. Not always about bigger.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/Jssk4TeiZxG) &mdash; content and formatting may not be reliable*
