---
layout: post
title: "While looking around for bits I came across these little beauties - V623ZZ v-groove sealed bearings"
date: November 09, 2014 14:41
category: "Deviations from Norm"
author: Mike Thornbury
---
While looking around for bits I came across these little beauties - V623ZZ v-groove sealed bearings. Only 30c each: [http://www.aliexpress.com/item/10pcs-V623ZZ-V-groove-roller-wheel-ball-bearings-3-12-4-mm-embroidery-machine-pulley-bearing/32226374792.html](http://www.aliexpress.com/item/10pcs-V623ZZ-V-groove-roller-wheel-ball-bearings-3-12-4-mm-embroidery-machine-pulley-bearing/32226374792.html)



3mm ID, so perfect for M3 screws,  with a 12mm x 4mm pulley.



By my estimation, there's 14 pulleys on a Eustathios - so for $4.20, you get steel and a sealed bearing inner. Can't print better than that!



 #FlatSli3DR   #Sli3DR  





**Mike Thornbury**

---


---
*Imported from [Google+](https://plus.google.com/101708620681849403392/posts/HeYysNAxKqP) &mdash; content and formatting may not be reliable*
