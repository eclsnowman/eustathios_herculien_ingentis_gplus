---
layout: post
title: "Hey guys! Would anyone here be able to assist in making a mount for my BLTouch to mount onto my Herculien Carriage?"
date: September 16, 2016 17:51
category: "Discussion"
author: Jim Stone
---
Hey guys!



Would anyone here be able to assist in making a mount for my BLTouch to mount onto my Herculien Carriage? my model skills are not good enough yet to model onto things yet



It will have to mount to the existing structure. and not be a whole new carriage. ( i cant remove my carriage without fudging things USE THE MISUMI RODS GUYS! dont use drill rod!)



attached is CAD Diagram of the BLTouch with all of its dimensions

as well as a GRABCAD Link

[https://grabcad.com/library/bltouch-sensor-for-3d-printer-1](https://grabcad.com/library/bltouch-sensor-for-3d-printer-1)



as for the carriage. i am using "Double Carriage_E3DV6" from the herculien GITHub



i am currently only using the Left most hotend spot. the one that would be closest to 0,0 when home



Any and all help appreciated!

![images/b29af87180d1002f3da961ba7d38961f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b29af87180d1002f3da961ba7d38961f.jpeg)



**Jim Stone**

---
---
**Jim Stone** *September 16, 2016 17:53*

**+Mutley3D** **+Eric Lien** **+Zane Baird** 


---
**Eric Lien** *September 16, 2016 18:54*

I'll see what I can come up with. But give me a week or so I'm pretty busy right now


---
**Jim Stone** *September 16, 2016 20:48*

Thank you!


---
**Eric Lien** *September 16, 2016 21:01*

**+Jim Stone** Something like this work? The problem is there is only 2mm below the housing to the start of the BLTouch probe. So threading in 3mm button screws from the bottom don't leach much meat to thread. You could screw it, but I might add super glue to be safe between the adapter and the BLTouch along the flange.



[https://drive.google.com/drive/folders/0B1rU7sHY9d8qR0JWajYxMHRaRlk?usp=sharing](https://drive.google.com/drive/folders/0B1rU7sHY9d8qR0JWajYxMHRaRlk?usp=sharing)


---
**Eric Lien** *September 16, 2016 21:02*

![images/d4961222b71119c5d2e19c43ea8289c2.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d4961222b71119c5d2e19c43ea8289c2.png)


---
**Eric Lien** *September 16, 2016 21:02*

![images/62d06b4429ffa9c8a590b12b9d9ca227.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/62d06b4429ffa9c8a590b12b9d9ca227.png)


---
**Eric Lien** *September 16, 2016 21:03*

![images/05a26913a3cf9339bf61248217aa816f.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/05a26913a3cf9339bf61248217aa816f.png)


---
**Jim Stone** *September 16, 2016 21:06*

That could DEFINATELY work for now. until i get a second Hotend ( it will be a while)



I totally forgot about the whole hotend adapter type stuff!



and as for threading. i can just use nuts and washers no?

to solve the meat problem


---
**Eric Lien** *September 16, 2016 21:18*

**+Jim Stone** give it a whirl, let me know how it goes.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/fWhKY1aiqAp) &mdash; content and formatting may not be reliable*
