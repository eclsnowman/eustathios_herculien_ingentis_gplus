---
layout: post
title: "Probably a dumb question, but how is the door stop supposed to work on the Herculien?"
date: October 10, 2015 16:13
category: "Discussion"
author: Zane Baird
---
Probably a dumb question, but how is the door stop supposed to work on the Herculien?





**Zane Baird**

---
---
**Eric Lien** *October 10, 2015 16:31*

Don't use my lid stop. Not strong enough. But the door stops get magnets glued in: 



[http://imgur.com/pKDNz1y](http://imgur.com/pKDNz1y)

[http://imgur.com/LfwxGCA](http://imgur.com/LfwxGCA)

[http://imgur.com/5IthGh6](http://imgur.com/5IthGh6)

[http://imgur.com/rqE7Bhj](http://imgur.com/rqE7Bhj)

[http://imgur.com/EPV1xjO](http://imgur.com/EPV1xjO)




---
**Zane Baird** *October 10, 2015 17:39*

**+Eric Lien** Thanks, that makes sense with the magnets. Think the lid stop would be strong enough in the carbon filled PETg? I have about 500 grams, but I'm waiting to run it through my Volcano until I can get a hardened nozzle in a size smaller than 0.8mm


---
**Eric Lien** *October 10, 2015 17:55*

**+Zane Baird** I think if it was straight, not a curve it would work well. The curve unloads when in tension.


---
**Zane Baird** *October 10, 2015 18:18*

**+Eric Lien** I see... Up to this point I'm running it without a lid so it's not a big deal, but I'll have to draw something up when I install the lid. However, my first priority is finding time to finish my design of a dual carriage that fits an E3D v6 and Volcano on the same setup. One for small prints, one for large drafts.


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/e4zPsFSLm7x) &mdash; content and formatting may not be reliable*
