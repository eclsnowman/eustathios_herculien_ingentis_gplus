---
layout: post
title: "Looking forward to building a Eustathios Sprider V2"
date: May 27, 2016 03:10
category: "Discussion"
author: jerryflyguy
---
Looking forward to building a Eustathios Sprider V2. 



Working through the BOM and have a few questions. Several of the nuts are the same size nut but different materials (Stainless & zinc plated) can they be all the same?



Most of the McMaster part numbers are 50 or 100pc packages, I assume the quantities on the pivot table are single pc counts?



SMW3D has a Nema 17 steppers but it's 1.68a not 1.5, does it matter? 



I'm trying to compress & consolidation my order list down to as few a suppliers as possible as most will be shipped internationally.









**jerryflyguy**

---
---
**Jeff DeMaagd** *May 27, 2016 03:55*

The current rating difference isn't so bad, but torque rating is important. Your limiting factor is your controller driver, so make sure the torque is at least as good as the one that is on the bom.


---
**Eric Lien** *May 27, 2016 03:59*

For the model I just grabbed bolts from McMaster for the model. If you can, get your hardware here: [http://www.trimcraftaviationrc.com/](http://www.trimcraftaviationrc.com/)



So much cheaper, great help and service. If they don't have a bolt, chances are they can get it in a day or two.﻿


---
**jerryflyguy** *May 27, 2016 04:09*

**+Eric Lien** thanks, do they also do the springs, bearings and collars etc?


---
**Eric Lien** *May 27, 2016 04:54*

Not that I know of. Just hardware I think


---
**jerryflyguy** *June 08, 2016 06:20*

**+Eric Lien** just to recap on my fastener materials question, it doesn't matter if the bolts & nuts are stainless or black steel? There is some of both in the BOM, I'm doing a 'Black Edition' so all black hardware is my preference if it doesn't cause issues?


---
**Jeff DeMaagd** *June 08, 2016 11:11*

I'm going to guess you don't want to use ordinary black oxide if it touches aluminum parts. There are black coatings for stainless steel.


---
**jerryflyguy** *June 08, 2016 12:51*

**+Jeff DeMaagd** whys that? Is it some kind of galvanic reaction? 


---
**Eric Lien** *June 08, 2016 13:10*

**+jerryflyguy** I went will all stainless hardware from trimcraft when I built mine. And Jeff is correct there is reactions between black oxide bolts and aluminum oxide from the extrusion. But in the past I have used both types of bolts and never had any real issues. I understand the desire to keep the black on black Knightrider color scheme. But my personal opinion is since 95% of the fasteners go unseen... I would save the money and go with trimcraft aviation.


---
**jerryflyguy** *June 08, 2016 15:53*

Ok thanks **+Eric Lien** being as I'm in Canada I've op'd to go w/ my local Fastenal store, they will pull a kit for me w/ the exact quantities I require of the various sizes. Saves a bunch of shipping, duties etc. 



So should I switch everything to be stainless then?


---
**Eric Lien** *June 08, 2016 17:42*

**+jerryflyguy** you will be fine. If you use them in an aluminum engine block that is one thing (high heat expedites a reaction). At ambient temps you are just fine.


---
**jerryflyguy** *June 08, 2016 17:47*

Thanks it's great to have such a helpful group, makes the process go that much easier! 


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/HzgXcCexoJR) &mdash; content and formatting may not be reliable*
