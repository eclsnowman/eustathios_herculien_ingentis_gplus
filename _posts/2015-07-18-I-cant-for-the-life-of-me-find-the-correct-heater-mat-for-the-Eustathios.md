---
layout: post
title: "I can't for the life of me find the correct heater mat for the Eustathios"
date: July 18, 2015 23:38
category: "Discussion"
author: Erik Scott
---
I can't for the life of me find the correct heater mat for the Eustathios. The closest I could find is this: [http://www.aliexpress.com/item/Custom-Silicone-Hot-Bed-For-3D-Printer-320x320MM/32319957576.html](http://www.aliexpress.com/item/Custom-Silicone-Hot-Bed-For-3D-Printer-320x320MM/32319957576.html) but It doesn't seem to have the thermistors. Anyone mind pointing me in the right direction? 





**Erik Scott**

---
---
**Dat Chu** *July 18, 2015 23:51*

You can contact Ali rubber directly and ask for a custom quote. They will do whatever you ask for. 


---
**Eric Lien** *July 19, 2015 00:56*

[http://m.alirubber.en.alibaba.com/](http://m.alirubber.en.alibaba.com/)


---
**Eric Lien** *July 19, 2015 01:02*

I just sent you a private message with contact info for Daisy at Alirubber.


---
**Frank “Helmi” Helmschrott** *July 19, 2015 04:37*

You can also contact Derek from Robotdigg (skype: robotdigg) and ask him for a quote. He made mine to the exact dimensions and power I wanted. I asked him to put it in the shop for other Eusthatios builders and he said he did but i can't yet find it.


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/2Ln9knDc68T) &mdash; content and formatting may not be reliable*
