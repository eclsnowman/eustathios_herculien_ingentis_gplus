---
layout: post
title: "Hey, we made this with a lot of effort and passion, will be great to get some diffusion, so if you like our product, please help us to spread the word"
date: May 08, 2015 14:34
category: "Show and Tell"
author: Mariano Ronchi
---
Hey, we made this with a lot of effort and passion, will be great to get some diffusion, so if you like our product, please help us to spread the word. For more info go to [www.drotix.com](http://www.drotix.com)! Thanks!

![images/4b2811fc5eccf49892f7c84a187a1352.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4b2811fc5eccf49892f7c84a187a1352.jpeg)



**Mariano Ronchi**

---
---
**Carlton Dodd** *May 08, 2015 19:43*

So, it's obviously a Raspberry Pi with a touchscreen, a case, and some custom software. 

It looks like it might be okay for some. I, personally, prefer open-source hardware and software, and making things myself. 


---
**Mariano Ronchi** *May 08, 2015 20:04*

You should know that not everyone is comfortable putting together the hardware and then setting and programming them, on the other side, makers communities help to create new features and solve problems! Yes, it's a RPI2+Touchscreen so it's Open Hardware, regarding Software will be licensed under Creative commons BY-NC...


---
**SalahEddine Redjeb** *May 08, 2015 22:02*

"You should know" sounds a little bit aggressive here, and you are stating the obvious, before finally giving a piece of info...

Work out your commercial skills, you will be losing money speaking to people in that manner.

Now another thing, your site doesn't display well on my lg g2, you might need to work on that.

What do you mean by certified camera  ? 

All that said, good job making that this simple, any way to see real pictures of the product operating  ? 


---
**Carlton Dodd** *May 08, 2015 22:20*

**+SalahEddine Redjeb**

You said what I was going to!


---
**Mariano Ronchi** *May 08, 2015 22:33*

**+SalahEddine Redjeb** sorry if I sounded aggressive, it was never my intention to do so. I was trying to say that many people prefer to buy a closed product and other people like you and I prefer to make our own devices. We want and need both profiles, the first to sell them a product and the others and most important to create a community whose contributions can enrich the product. So then again sorry if anyone was assaulted by my words.


---
*Imported from [Google+](https://plus.google.com/112915425903512115595/posts/U4AsDKsBMDa) &mdash; content and formatting may not be reliable*
