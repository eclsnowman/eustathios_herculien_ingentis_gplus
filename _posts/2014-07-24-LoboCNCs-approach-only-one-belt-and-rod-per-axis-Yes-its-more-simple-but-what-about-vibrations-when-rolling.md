---
layout: post
title: "LoboCNC's approach.: only one belt and rod per axis Yes it's more simple but what about vibrations when rolling..."
date: July 24, 2014 17:49
category: "Discussion"
author: karabas3
---
LoboCNC's approach.: only one belt and rod per axis [http://forums.reprap.org/read.php?1,386926](http://forums.reprap.org/read.php?1,386926) Yes it's more simple but what about vibrations when rolling...





**karabas3**

---
---
**James Rivera** *July 24, 2014 18:35*

Good point. And any wear on those parts could make it worse or perhaps add eccentricity to the gear. Also, the Z-axis design uses 3 lead screws and no smooth rods. I have to wonder if the straightness of the lead screws alone will be enough to prevent z-ribbing. I'm also not so sure I like the U-bearings he uses at the corner of the build platform against the square aluminum tubes, because it just seems to add more reliance on the straightness of something that probably does not have a high straightness specification. I heard a weird ticking noise in his youtube video when the z-axis moved, too.


---
*Imported from [Google+](https://plus.google.com/115164543335947782806/posts/ZqUug8pPsrg) &mdash; content and formatting may not be reliable*
