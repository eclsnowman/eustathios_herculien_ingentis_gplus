---
layout: post
title: "Cleaned up, re-trammed, printing nylon, and back to using IGUS on the carriage"
date: February 17, 2015 16:06
category: "Show and Tell"
author: Mike Miller
---
Cleaned up, re-trammed, printing nylon, and back to using IGUS on the carriage. 



![images/4650479ab18fb2db9abcdf97f80e1638.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4650479ab18fb2db9abcdf97f80e1638.jpeg)
![images/4a4b85748731b69d386c6ed3da0201bd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4a4b85748731b69d386c6ed3da0201bd.jpeg)
![images/99ee9946630c9ca6bff9dffe2424a0f7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/99ee9946630c9ca6bff9dffe2424a0f7.jpeg)

**Mike Miller**

---


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/SPYKQVoTQAv) &mdash; content and formatting may not be reliable*
