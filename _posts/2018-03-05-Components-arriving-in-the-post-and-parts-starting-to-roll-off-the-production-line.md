---
layout: post
title: "Components arriving in the post and parts starting to roll off the production line..."
date: March 05, 2018 22:58
category: "Build Logs"
author: Julian Dirks
---
Components arriving in the post and parts starting to roll off the production line...

![images/1eb21aaf0b92cb9402653833dee59064.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1eb21aaf0b92cb9402653833dee59064.jpeg)



**Julian Dirks**

---
---
**Gus Montoya** *March 07, 2018 04:19*

Looking nice, my efforts have stalled. Concentrating on my CNC machine which seems to be stubborn as all hell not to work. 


---
**Julian Dirks** *March 07, 2018 05:13*

Thanks. First time printing PETG so haven’t really got it dialled in  but I’m really impressed with it as a material. Super strong and no warping whatsoever. 


---
*Imported from [Google+](https://plus.google.com/113795478307151372873/posts/Vq4YzX9ggAv) &mdash; content and formatting may not be reliable*
