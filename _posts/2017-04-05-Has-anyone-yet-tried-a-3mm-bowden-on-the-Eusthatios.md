---
layout: post
title: "Has anyone yet tried a 3mm bowden on the Eusthatios?"
date: April 05, 2017 13:02
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Has anyone yet tried a 3mm bowden on the Eusthatios? I wonder what it's like? Pros and Cons?



I do have a 3mm E3D bowden here and also the Bondtech QR in 3mm from a stil-to-be-done printer project. My Carriage would make it relatively easy to swap the 1.75mm to a  3mm I just wonder what you guys think before I do it. 



There must be a reason why +Ultimaker and +BCN3D Technologies use it in favor of a 1.75mm bowden. Maybe extrusion would be more stable? Retracts could be more difficult as you would probably have more filament mass in the hot zone, right?



What are your thoughts?





**Frank “Helmi” Helmschrott**

---
---
**Zane Baird** *April 05, 2017 13:19*

The only real positive I can see from it is being able to feed flexible materials more reliably in a Bowden system. Otherwise I think many manufacturers just stick with 3mm because it's what they used in the past and they don't want to make their longstanding customers switch out any filament they have in stock. Its my understanding that  3mm was cheaper to manufacture, but with so many working with 1.75mm now I think that idea has gone out the window.



1.75mm wins with thermal transfer as well, so large nozzles and layer heights can be reallized


---
**Shane Graber** *April 05, 2017 13:35*

The Ultimaker 2 that we have at work uses a 3mm bowden and I've had zero problems with it.  I'd say try it.  :)


---
**Jeff DeMaagd** *April 05, 2017 13:46*

Oh I didn't realize BCN was bowden. I only know of six brands that still use 3mm (or 2.85mm), and two of those are large format brands, the other large format brands might be as well. All of four "desktop" brands that I can find are 2.85/3, everyone else seems to be 1.75mm. I don't do much bowden so I can't comment on that, other than it seems the larger would be easier to run as bowden.


---
**Eric Lien** *April 05, 2017 14:38*

From my understanding you get better "Volumetric Resolution" by using 1.75mm. Because for a specified length of filament you have less volume in that length length of 1.75mm than in 3mm. Steps/mm of a 1.75 bondtech = 476.55 per the manual, and 3mm = 492.45.



But by volume that 1mm length of 3mm(2.85 actually) filament is more than 2.5x the volume of 1.75mm. This decreases your effective extrusion resolution.



Again this is theory, and does not take into account other items that effect printing performance. For example the effectiveness of the grip on the larger diameter, the retraction performance of the filament, etc. 2.85mm filament should be less compressible/elastic due to the larger cross section. Also there is the surface area contact of the melt zone vs the filament cross section to consider. 2.85mm filament can be more difficult to melt at high speeds since the cross section is larger.



Long story short, I don't think 3mm inheriently does any better. But it will make printing flexibles possible. And I am always open to being proved wrong. So I would love to see you test if you decide to go that route.


---
**Shane Graber** *April 05, 2017 14:41*

Honestly I was thinking about going the 3mm route for mine.  I have a LOT of 3mm filament sitting around that I have to do something with!  lol




---
**Daniel F** *April 05, 2017 14:58*

I use a 3mm bowden (40cm) setup with my Eustathios and I'm happy with it. Can't compare with 1.75, as I've never tried it.


---
**larry huang** *April 05, 2017 16:23*

I haven't try 3mm filament before, I doubt how much real different it will make. Besides , it won't be as good as direct extruder at both printing flexible filament and retraction performance.  I think there are still other improvement can be done on software and hardware to improve print quality.


---
**Markus Granberg** *April 05, 2017 16:43*

One nice thing with my direct e3d titan set-up. It's quick to alter between 3 and 1.75mm


---
**Frank “Helmi” Helmschrott** *April 05, 2017 18:47*

EDIT: I just noticed that I already saw your post 9 weeks ago ;-)



Any experiences yet? Anything to tell?




---
**Markus Granberg** *April 05, 2017 19:39*

If your talking to me, It's working great! If you can go direct drive I see no point in a bowden setup. I will not go back.. I got a blower fan added to  my carrage now to.




---
**Frank “Helmi” Helmschrott** *April 05, 2017 19:48*

sorry for not tagging you right **+Markus Granberg** - i was indeed talking to you.



Good to hear it is working for you. I was thinking the weight maybe a problem. Have to check how the aero setup could fit in without taking too much space.



Looks like you are using a thin pancake motor. I wonder which size you would need at least for a working setup with enough power.


---
**jerryflyguy** *April 06, 2017 04:31*

I'm interested to try E3D's new Titan direct extruder.. hoping it might make the added weight of direct drive less of an issue. I use a Titan via Bowden right now and it's worked very well for me. 


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/jmDrkbznimm) &mdash; content and formatting may not be reliable*
