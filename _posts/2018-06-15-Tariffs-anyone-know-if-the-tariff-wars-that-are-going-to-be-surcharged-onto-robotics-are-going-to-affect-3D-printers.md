---
layout: post
title: "Tariffs- anyone know if the tariff wars that are going to be surcharged onto robotics are going to affect 3D printers?"
date: June 15, 2018 23:53
category: "Discussion"
author: Dennis P
---
Tariffs- anyone know if the tariff wars that are going to be surcharged onto robotics are going to affect  3D printers?





**Dennis P**

---
---
**Brandon Satterfield** *June 16, 2018 01:10*

Yep.


---
**jinx OI** *June 16, 2018 08:38*

it may in the US, and those that export  from the US gonna feel it...  those of us in Europe gonna be happy me thinks  


---
**Scott Hess** *June 16, 2018 16:09*

I'm expecting to see a lot of randomness.  Like an official retail transaction (say a Monoprice printer) is probably going to get hit, because they can't afford to take chances, and they're an easy to find target.  But ordering bits and pieces from some random seller on Aliexpress?  I suspect a lot of those will just fly under the radar.


---
**Jeff DeMaagd** *June 19, 2018 03:56*

If you’re in the US, you’re not charged duties if your import shipment is worth less than $200.


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/Dr9P3FRENRZ) &mdash; content and formatting may not be reliable*
