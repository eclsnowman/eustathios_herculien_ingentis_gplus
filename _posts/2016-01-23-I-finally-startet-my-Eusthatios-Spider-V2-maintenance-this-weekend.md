---
layout: post
title: "I finally startet my Eusthatios Spider V2 maintenance this weekend"
date: January 23, 2016 14:28
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
I finally startet my Eusthatios Spider V2 maintenance this weekend. First step was the biggest: replace the carriage with a new one that suits two crossing LM8LUU bearings. 



Thankfully **+Eric Lien** modified the X/Y-ends for me a while ago so that the rods have a slightly bigger distance to make room for the 15mm OD bearings to cross each other.



Just doing the first print and quality looks even better than before but that maybe just due to this special print. I made some mistakes when reaming the x/y ends which will probably let me replace everything again in a while but i noticed that only after everything was built in and as it runs for now I'll leave it :)



 Next step is to replace my Dibond cover plates to fit the display that i only had taped down so far and also a new 80mm Case fan.



sorry for the bad imagery - i didn't realize i could take some more photos during the build process so just some finished ones for now.



![images/9733c5b7988e7ccd8ba9d036903f093d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9733c5b7988e7ccd8ba9d036903f093d.jpeg)
![images/b773ebb29a3ef7ccd8e25b8df818315c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b773ebb29a3ef7ccd8e25b8df818315c.jpeg)
![images/7be3d98a83c1b8acb553ff06aeeb7d45.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7be3d98a83c1b8acb553ff06aeeb7d45.jpeg)
![images/d80b5e89b91b3899dcc47eb28d9f67a0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d80b5e89b91b3899dcc47eb28d9f67a0.jpeg)

**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *January 23, 2016 15:13*

Can't wait to hear how you like it long term. If it works well please do a push to the GitHub into the user modifications area. That way other people can use it to.


---
**Frank “Helmi” Helmschrott** *January 23, 2016 18:36*

Sure I will, Eric. Was going to publish that anyway as soon as it's a bit more tested.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/2Wqz9RjeJ8C) &mdash; content and formatting may not be reliable*
