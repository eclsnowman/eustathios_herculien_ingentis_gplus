---
layout: post
title: "Alignment is set I think, (and Eric Lien those washers made a difference!), but what is the secret to getting the belts into these slots?"
date: June 09, 2015 02:02
category: "Discussion"
author: Bruce Lunde
---
Alignment is set I think, (and **+Eric Lien**​ those washers made a difference!), but what is the secret to getting the belts into these slots? Do I cut off some of the knobby part?

![images/b88b4b8f02d42c2c8eb814e27349f38c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b88b4b8f02d42c2c8eb814e27349f38c.jpeg)



**Bruce Lunde**

---
---
**Dat Chu** *June 09, 2015 02:13*

Washer, which washer? Can I have a pix of the washer of super secret magical ability? :)


---
**Gus Montoya** *June 09, 2015 02:13*

Well if you used washers, then that means I should have used washers. But where exactly?


---
**Erik Scott** *June 09, 2015 02:21*

Looks like that piece was either over-extruded or the nozzle was too low and the bottom layer is squished out, creating a sort of flange around the part. You'll have to cut off that flange, but if the part was so over extruded that you can't even get the belt in the teeth, it looks like you may need new parts. 



You can help persuade the belt to slip into the teeth by using something flat to push it into the groove. 


---
**Derek Schuetz** *June 09, 2015 02:53*

I had a lot more clearance when I built my herculien


---
**Eric Lien** *June 09, 2015 03:44*

The bad news is you put the belt on the rod end before assembling the inner cross gantry. And you may need to do a tiny bit it exacto work where the part met the build plate... But not much, the belt between the teeth is pretty thin.


---
**Eric Lien** *June 09, 2015 03:45*

With the cross rods off you can tip up the rod end and belt install is easy.


---
**Eric Lien** *June 09, 2015 03:47*

**+Dat Chu**​ the washer is just a 10mm ID shim washer between the corner bearings and the corner pulley. The pulleys have no shoulder on that side so they rub on the bearings without it.﻿


---
**Eric Lien** *June 09, 2015 03:48*

**+Derek Schuetz** that clearance Bruce has looks correct to me, I made it very close to maximize build area.


---
**Bruce Lunde** *June 09, 2015 04:12*

Thanks for all the help, I will give that a try tomorrow. Darn, I knew I dropped a screw, now I see it!


---
**Chris Brent** *June 09, 2015 17:56*

I was about to ask if you knew about the screw hiding out in there :)


---
**Vic Catalasan** *June 09, 2015 21:25*

I managed to install the belt after installing the gantry, not easy and if I had to do again I would place the belt before assembly. I used a credit card cut in strip to help push the belt into the groove


---
**Bruce Lunde** *June 10, 2015 00:41*

I hope to try tonight when I get home from work.


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/EDcujaDvd9T) &mdash; content and formatting may not be reliable*
