---
layout: post
title: "I've been thinking a lot about the carriage for my printer and came across this modular design"
date: January 20, 2015 06:17
category: "Discussion"
author: Richard Mitchell
---
I've been thinking a lot about the carriage for my printer and came across this modular design. There's already a couple of attachments for an e3d hotend and a flexdrive direct drive.



What do people think?





**Richard Mitchell**

---
---
**Ubaldo Sanchez** *January 20, 2015 06:20*

I'm currently using the merlin hotend on my prusa i3 and love it despite its fairly major flaws. In any case, whenever I build a eustathios, that's probably the carriage I'll look into considering that I have and really like the hexagon and merlin hotends. Thanks for the find!


---
**James Rivera** *January 20, 2015 06:29*

Nice! I had been thinking of something similar to RichRap's design (the whole extruder gets swapped out), but this is cool, too. However, it lacks fan mounts--both for the cold end (to prevent jams) and for the hot end (part cooling for printing with PLA).


---
*Imported from [Google+](https://plus.google.com/111547506541230089782/posts/L8wVdcFGmPC) &mdash; content and formatting may not be reliable*
