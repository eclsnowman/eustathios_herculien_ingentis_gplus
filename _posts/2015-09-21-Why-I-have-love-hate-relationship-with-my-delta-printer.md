---
layout: post
title: "Why I have love-hate relationship with my delta printer..."
date: September 21, 2015 23:40
category: "Show and Tell"
author: Ian Hoff
---
Why I have love-hate relationship with my delta printer...



I printed most of a set of Herculien parts before I realized I had done something stupid.



When changing delta radius to get the motion flat again I neglected to adjust the diagonal rod offset. The result is a set of 99% sized parts.



My bowden is not completely  solid/tuned either as you can see by the poor restarts near the screw holes. Making the thinner under extruded appearance on the smallest parts.



*Time to order more filament... 



A friend of mine runs [atomicfilament.com](http://atomicfilament.com) and he makes high specification PLA and ABS (+/-.04mm) filaments.



I have tried a couple filament suppliers and this stuff is just solid.

I used to be  protopardigm believer, now I know atomic is the bomb ;)



Different colors print at the same temps or very very close. Strong layer bonding when printing at the right temps, beautiful colors



*sorry for the sales pitch but I love this guys stuff





**Ian Hoff**

---


---
*Imported from [Google+](https://plus.google.com/106725640305554524389/posts/SDZD3a6vxJo) &mdash; content and formatting may not be reliable*
