---
layout: post
title: "Got the cable chain installed and cleaned up some wiring while I try to figure out warping issue"
date: April 04, 2015 19:01
category: "Show and Tell"
author: Derek Schuetz
---
Got the cable chain installed and cleaned up some wiring while I try to figure out warping issue.

![images/42a165417357285cadf2c05eac3a5265.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/42a165417357285cadf2c05eac3a5265.gif)



**Derek Schuetz**

---
---
**Brandon Cramer** *April 04, 2015 19:11*

Wow! I'm so jealous! Can't wait to get started with ordering the parts for this! Looking good!!


---
**Seth Messer** *April 04, 2015 23:58*

yeah Derek, that looks incredible. loving the hinges.


---
**Mike Thornbury** *April 05, 2015 02:20*

Very pro! I like the z-axis drive. 


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/ZLPvm6Tomwd) &mdash; content and formatting may not be reliable*
