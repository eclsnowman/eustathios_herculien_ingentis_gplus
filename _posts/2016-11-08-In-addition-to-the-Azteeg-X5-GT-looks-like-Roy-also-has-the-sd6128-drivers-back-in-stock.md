---
layout: post
title: "In addition to the Azteeg X5 GT, looks like Roy also has the sd6128 drivers back in stock"
date: November 08, 2016 00:27
category: "Discussion"
author: Eric Lien
---
In addition to the Azteeg X5 GT, looks like Roy also has the sd6128 drivers back in stock. These drivers are super smooth and quiet.



[http://www.panucatt.com/product_p/sd6128.htm](http://www.panucatt.com/product_p/sd6128.htm)



He also has a few more drivers coming out soon (look under the Driver's and Bigfoot Driver's drop-downs under the X5 GT page to see what's coming.﻿





**Eric Lien**

---


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/ecagwBWyaG1) &mdash; content and formatting may not be reliable*
