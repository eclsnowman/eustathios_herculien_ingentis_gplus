---
layout: post
title: "Oh yeah. My first personal shop machine purchase is putting into used"
date: June 17, 2015 23:38
category: "Discussion"
author: Dat Chu
---
Oh yeah. My first personal shop machine purchase is putting into used. These holes are cleanly made. 

![images/65071210a7f177d84fb36b6e86e92356.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/65071210a7f177d84fb36b6e86e92356.jpeg)



**Dat Chu**

---
---
**Mike Thornbury** *June 18, 2015 00:20*

You can't beat a clean hole...﻿ 0.o


---
**Gus Montoya** *June 18, 2015 00:36*

**+Mike Thornbury**  HAHAHA   **+Dat Chu**   Wow, I hope your going to use that to warrant such a purchase. Very nice machine. What brand is it?


---
**Eric Lien** *June 18, 2015 00:44*

Great job. I did mine with a standard spiral bit... And it walked like crazy. Took me 2 hours to drill qty:4 half inch holes. Glad to see someone smarter than me using the right tool for the job!


---
**Dat Chu** *June 18, 2015 00:53*

I got this one [http://www.amazon.com/gp/product/B003LSSS0W?psc=1&redirect=true&ref_=oh_aui_detailpage_o03_s00](http://www.amazon.com/gp/product/B003LSSS0W?psc=1&redirect=true&ref_=oh_aui_detailpage_o03_s00) Amazon has it for $15 off for Father's Day. I thought, worst case scenario, I spent some money to learn how to use a drill-press. 



With a 1/2" end mill (per the instruction on github), making the hole after hand-drill the pilot hole was pretty easy.


---
**Gus Montoya** *June 18, 2015 01:05*

Oh yeah, that'll last you a good long time. :)


---
**Mike Thornbury** *June 18, 2015 01:27*

**+Eric Lien**  , I had the same problem - tool supply here in the third world is poor - what is the 'right tool'? I will bite the bullet and ship it from the US.


---
**Eric Lien** *June 18, 2015 01:42*

**+Mike Thornbury** 1/2" end mill bit is ideal (or similar clearance metric bit), or a hole saw bit could work too.


---
**Daniel Fielding** *June 18, 2015 01:44*

I love that the clamp still has the card on it from the shop


---
**Mike Thornbury** *June 18, 2015 02:23*

**+Eric Lien**  You mean a router bit? Like I have for my CNC machine?



I was thinking of using one of my smaller, cheaper Forstner bits for making the 'clearance' hole, down to the bottom of the v-groove, then using a normal metal bit for the final hole through the extrusion.



I have one that is just right for fitting an M5 cap-head bolt - 8mm Forstner like this one: [http://www.woodcraft.com/images/Products/200/12E33.jpg](http://www.woodcraft.com/images/Products/200/12E33.jpg)


---
**Eric Lien** *June 18, 2015 02:40*

I meant an end mill bit like you would use machining: [http://static.grainger.com/rp/s/is/image/Grainger/14M956_AW01?$mdmain$](http://static.grainger.com/rp/s/is/image/Grainger/14M956_AW01?$mdmain$)



Or a hole saw bit like this: [http://images.lowes.com/product/converted/885911/885911105958lg.jpg](http://images.lowes.com/product/converted/885911/885911105958lg.jpg)


---
**Mike Thornbury** *June 18, 2015 04:18*

Yep, that's the same as some of my CNC bits.



Thanks.


---
**Mike Miller** *June 18, 2015 11:59*

My only thought is that I'd clamp it in two locations...keeps it from pivoting if the clamping load is inadequate, and makes sure it's flat with the build plate....other than that, it looks awesome.



(And you'll want a vice at some point...drill presses love to take parts and fling them into the dark places of your work area.)


---
**Dat Chu** *June 18, 2015 13:03*

**+Mike Miller**​ I am getting two drill press clamps. I got myself a vice but it was too small. So I now have to get another. 😥


---
**Mike Miller** *June 18, 2015 13:44*

A problem ALL Tool Fetishists have. :D


---
**Chris Brent** *June 18, 2015 17:51*

I think you mean, "I got myself a vice but it was too small for this job. I know it will be useful for something else though" :)


---
**Dat Chu** *June 18, 2015 18:01*

Amazon told me that it costs me $7 to send it back so I am keeping it for my electronic work on my desk. Should have checked the size on these. 


---
**Dat Chu** *June 19, 2015 03:30*

The funny thing is I bought two more drill press clamp then look at the documentation just to realize I only need to drill 4 holes :|. Meh, it's father day coming up, time to buy some tools anyway :p


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/3Ku4QtX9YR3) &mdash; content and formatting may not be reliable*
