---
layout: post
title: "Huh. Just happened across my google plus page today and thought i'd see if this group was still going"
date: May 14, 2017 09:40
category: "Discussion"
author: Tim Rastall
---
Huh. Just happened across my google plus page today and thought i'd see if this group was still going. Nearly 9000 members! Nice work **+Eric Lien** :)





**Tim Rastall**

---
---
**Eric Lien** *May 14, 2017 10:53*

Thank you **+Tim Rastall**​. Though I will admit, since I opened up the community from approve to join and switched to approve to post we obviously got inflated numbers. But overall I think it helps exposure, and moderating the invites was becoming problematic. 



I am glad to see you. I hope your new endevors are keeping you busy and life is good. 


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/jMVaCewU9Dg) &mdash; content and formatting may not be reliable*
