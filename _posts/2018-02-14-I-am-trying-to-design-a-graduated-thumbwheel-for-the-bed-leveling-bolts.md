---
layout: post
title: "I am trying to design a graduated thumbwheel for the bed leveling bolts"
date: February 14, 2018 17:30
category: "Deviations from Norm"
author: Dennis P
---
I am trying to design a graduated thumbwheel for the bed leveling bolts. I find it most convenient on my Di3 to have the calibrated nuts to adjust the bed. 



The thing is the 0.8mm pitch of the M5 screws are making it fussy. At the size it wants to be ~32-36mm diameter, the divisions can get crowded for 0.8/8=0.1mm resolution. 



0.5mm pitch for fine thread screws would be perfect, but surprisingly, I can not find flat head M5-0.5 fine thread screws long enough to make the assembly. 



So if you were confronted by these three options, which ones do you find most intuitive? Each major division is 0.1mm resolution.  





![images/034b3ee12fdc4c6379aeb0a015346840.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/034b3ee12fdc4c6379aeb0a015346840.png)



**Dennis P**

---
---
**Daniel F** *February 14, 2018 18:29*

The most right one


---
**Scott Hess** *February 14, 2018 19:26*

I can see why you might like to have the ticks to indicate how far you're adjusting, but is there any value to having the numbers?  I mean, like would you ever actually be able to say that you turn something to "1", rather than "drop .5mm"?



I guess in addition to tick marks, I'd probably most want a directional indicator for "up".  Numbers could do that, of course, I just notice every time I adjust my bed, I have to think through how the threading and directions impact the bed height :-).



Lastly, I'd be somewhat surprised if the middle picture's tick marks actually worked for you.  You might just print a sample of each, just to see if they work.  It probably would work with a laser cutter, though, which gives me an idea ...


---
**Dennis P** *February 14, 2018 20:28*

The numbers generally indicate 0.1mm change in height since 1 complete revolution is 0.8mm, 1/8 rev => 0.1mm. The minor divisions are 0.02mm each.  



<<shhh, I am supposed to be watching a webinar>> Here are my test prints so far... I am not loving either one so far.. the middle one is too busy (granted, I bumped up the diameter after I printed it). I don't think the paint fill will work well. The left one is a little nicer... **+Scott Hess** I like your idea of the + and -. and I too have to think it through everytime I need to adjust my bed.  There is real estate on the bed holders that can be used to give the + and - direction arrows too if not on the wheels. 





![images/69e8935e7da067807e26325ebe9353fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/69e8935e7da067807e26325ebe9353fa.jpeg)


---
**Julian Dirks** *February 14, 2018 22:01*

I would go with the design on the left (of the designs not the printed ones) because if I understand correctly it has the finest control (0.2mm height change per quarter revolution of the wheel).  I also struggle with wheel direction, mainly because every since I upgraded the carriage on my Wanhao (V2.1) I basically only have to relevel it when I change bed temp significantly.  



If you are going to the trouble of having a graduated wheel would a stationary pointer would be worth adding?



How stable is bed level on Eustathios?


---
**Eric Lien** *February 14, 2018 23:17*

**+Julian Dirks** i haven't leveled mine in almost a year... If that gives you an idea :)


---
**Julian Dirks** *February 14, 2018 23:27*

That's a good thing to hear when I'm in the "bleeding cash" stage of the build!  : )


---
**Eric Lien** *February 14, 2018 23:37*

**+Julian Dirks** yeah, these are not the cheapest printers to build. And not the easiest to build. But my opinion is the net results after getting everything installed is worth it for a high speed large build volume printer. Then again, I might be a little biased :)


---
**Dennis P** *February 16, 2018 20:03*

**+Walter Hsiao** can you help us understand these things that are in your square bed mount photo? What is the wire sticking out and what is the square cut washer object below it? THANKS!

![images/6aaa6e95590e95c8086e1df7e4141ddd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6aaa6e95590e95c8086e1df7e4141ddd.jpeg)


---
**Eric Lien** *February 16, 2018 22:22*

**+Dennis P** Looks to me like that is t-slot nut under there (the stamped ones like openbuilds uses. And, to me it looks like that is a ground jumper (otherwise the bed is isolated with mains feed which is bad news). I wouldn't run a mains bed without some path to ground. Since the bed is on plastic mounts, you need another path to ground... that hopefully isn't through you :)


---
**Dennis P** *February 17, 2018 23:22*

I put source and print files on github... you can change the +/- orientation by editing the scad file. Direction is subjective by the user, my convention might not be the same as yours. 

![images/b4839b6ac2a924b5372645bf744b5b0a.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b4839b6ac2a924b5372645bf744b5b0a.png)


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/JZFpgyx42JR) &mdash; content and formatting may not be reliable*
