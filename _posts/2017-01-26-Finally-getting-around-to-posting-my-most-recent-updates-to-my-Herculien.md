---
layout: post
title: "Finally getting around to posting my most recent updates to my Herculien"
date: January 26, 2017 00:12
category: "Mods and User Customizations"
author: Zane Baird
---
Finally getting around to posting my most recent updates to my Herculien. I've made 2 major updates: 



(1) Simple (and effective) belt tensioners that allow for the carriage to be swapped in under 5 minutes. Additionally, the belt tensioners only require 2 screws and threaded brass inserts (McMaster 94510A240)  for fastening hardware. Belt tension is adjusted by tightening a screw and the expansion-fit nature of the inserts makes it  effectively a lock nut. The carriage can be changed easily as the belt tensionser provide adequate room to rotate out of the way once the cross rods are removed. 



(2) A single-extruder carriage utilizing two 10x14x30mm self-graphite bushings from robotdigg as well as an inductive probe for automated bed leveling. The carriage is compact enough to open up the build volume to 380x380x340mm.



All of my new parts are printed in PETg as this seems to hold up the best ( the pink ABS tensioners cracked when tightening the set screw that held the cross rods.) I haven't had any issues with heat and I'm currently in the middle of a 4 hour ABS print.



I haven't got around to compiling all the files just yet, but when I do I will push them to the github under the community mods folder for everyone else to try.



![images/ae731f017a4b84f840be968039d2da94.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ae731f017a4b84f840be968039d2da94.jpeg)
![images/f2d1900ed53cb473904f39caaf7c7133.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f2d1900ed53cb473904f39caaf7c7133.jpeg)
![images/1a7871f6aeba3686d45ece60f8f34dd2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1a7871f6aeba3686d45ece60f8f34dd2.jpeg)
![images/61cd007ae15991bc2780f3b1352f89b2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/61cd007ae15991bc2780f3b1352f89b2.jpeg)
![images/5afec4c0af4c6ced3c0debf87354952c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5afec4c0af4c6ced3c0debf87354952c.jpeg)

**Zane Baird**

---
---
**jerryflyguy** *January 26, 2017 15:22*

Looks nice! I like the wire connection/holder part! I need something on my Eustathios like that.. like it better than the zip-tie method


---
**Zane Baird** *January 26, 2017 15:29*

**+jerryflyguy** I'm going to modify this carriage to accept the pressfit bushing that are on the carriages. That version should be cross-compatible with the Eustathios (the belt tensioners already are)


---
**Eric Lien** *January 26, 2017 15:49*

As always **+Zane Baird**, you take a design and make it look much more elegant than I can. The side carriages will be going on my HercuLien for sure... though perhaps in a more modest color scheme :)


---
**jerryflyguy** *January 26, 2017 15:58*

**+Zane Baird** like **+Eric Lien** I'll be putting it on my Eustathios at some point here. Currently working on enclosing the entire printer so I can put a fume capture system on it, then I can print ABS/ASA in the house.


---
**Jim Stone** *January 26, 2017 21:43*

for the carriage. will i be able to use the same bushing as in the original Herculien carraige?

does the print fan nozzle use the dimensions for the fan mentioned in the BOM for the herculien as well?

this may be the carriage i use. as i dont see the space invader style having a probe spot :)


---
**Zane Baird** *January 26, 2017 23:17*

**+Jim Stone** the bushings are different but I'm going to make a mod that uses the same as the BOM. The fan is not the same though. It's a 40x40x20 blower in this carriage. I'll make a post when I get the final versions up


---
**Frank “Helmi” Helmschrott** *April 04, 2017 15:17*

just found this a bit older post. **+Zane Baird** wonder how it's working out for you. I started with the graphite self lub bushings from Robotdigg on the Eusthatios. They are still running on the outer carriage and are doing a great job though I wasn't that happy using two of them in a row (8mm ones) in the center. I found they are too easily blocking on only a little misalignment or forces that they don't like.



Wonder how this works for you.


---
**Zane Baird** *April 04, 2017 15:36*

**+Frank Helmschrott** I have since modified the carriage to use the self-aligning bushings from SDP/SI instead of the self-graphite bushing from robotdigg. My conclusions were very much along the lines of yours: the self-graphite bushings are very sensitive to misalignment and allowed for more play than was desired. Once in alignment they worked fairly well, but It really wasn't worth the trouble for me.


---
**jerryflyguy** *April 06, 2017 04:27*

**+Zane Baird** have you posted the files for these parts?


---
**Zane Baird** *April 07, 2017 00:09*

**+jerryflyguy** I just pushed the commits so they should be up relatively soon. I haven't put together a short BOM with the part numbers for the brass inserts and screws used, but shoot me a message if you can't figure it out




---
**jerryflyguy** *April 07, 2017 16:41*

**+Zane Baird** awesome, I'll go have a look! 

Thanks btw!! 😉


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/791g9qhk3Nm) &mdash; content and formatting may not be reliable*
