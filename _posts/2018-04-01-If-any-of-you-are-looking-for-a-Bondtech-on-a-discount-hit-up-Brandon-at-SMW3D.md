---
layout: post
title: "If any of you are looking for a Bondtech on a discount hit up Brandon at SMW3D"
date: April 01, 2018 04:05
category: "Show and Tell"
author: Eric Lien
---
If any of you are looking for a Bondtech on a discount hit up Brandon at SMW3D. He and I worked on a printer a while back. Looks like he is going a different direction on the extruders (Bondtech BMG versus the big dog QR's). If you are using a bowden setup on HercuLien or Eustatios, and looking to save a few sheckles on a Bondtech QR, I think these would serve you well.



<b>Originally shared by Brandon Satterfield</b>



I am switching to BMG extruders I have these two used QRs I’ll add a new stepper motor wire for each. Anyone interested? 120.00 each I’ll ship in the US on my dollar.

![images/51f93cba10cd84d07e33988e9e10bb0f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/51f93cba10cd84d07e33988e9e10bb0f.jpeg)



**Eric Lien**

---


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/ZwCpXyZKNbG) &mdash; content and formatting may not be reliable*
