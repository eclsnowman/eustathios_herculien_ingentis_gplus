---
layout: post
title: "We have an Ultimaker 2 and work"
date: January 12, 2017 07:47
category: "Discussion"
author: Christopher Bray
---
We have an Ultimaker 2 and work. I am itching to have my own 3D printer at home.



What are the differences between the HercuLien and the Eustathios?



I like the full frame and Ultimaker style XY movement. I also like how the bed isn't cantilevered. And it looks like booth Z screws are driven off of the same stepper motor with a timing belt.



Am I crazy to try and build one of these as my first 3D printer build?







**Christopher Bray**

---
---
**Eric Lien** *January 13, 2017 04:17*

Not crazy, just ambitious :)


---
**Eric Lien** *January 13, 2017 04:18*

There's an amazing community of people here to help you through the process if you do decide to go ahead with the build. In my opinion we have one of the best and most helpful groups out there.


---
**jerryflyguy** *January 22, 2017 14:54*

This was my first printer, it was rather straight forward. I guess it all depends on your skill? Having your work machine which cam print you the parts you need will be super handy. That was the hardest part for my build (getting printed parts). I'd recommend Misumi for the pre-cut and drilled/tapped 2020 kit and linear rods. Way faster (and in my case; more accurate) than I could do it.. and reasonably priced. The trick is get it all organized and only order once! 



Like **+Eric Lien** says, great community here, you'll be well supported. I know I was! 



Good luck! 🤓


---
*Imported from [Google+](https://plus.google.com/102349946554733022759/posts/i9HbDppQoY8) &mdash; content and formatting may not be reliable*
