---
layout: post
title: "I'm seeing a bit of an oddity and wondering if anyone else has seen this?"
date: November 12, 2016 19:56
category: "Discussion"
author: jerryflyguy
---
I'm seeing a bit of an oddity and wondering if anyone else has seen this? I have my suspicions but will wait to see what others respond with.



I'm watching the temps on my hotend and they are varying wildly. Temp is set at 210 but it's cycling from 187 to 218 or so? Jumping big numbers of degrees per second.



I've got the upgraded thermistor cartridge and a 25w heater. I've run the PID tune a few times in the past month so I'm sure the tuning is good? 



Thoughts? I don't think I'm seeing true temp variations? 





**jerryflyguy**

---
---
**Tomek Brzezinski** *November 12, 2016 20:09*

Can you confirm you have the right thermistor enabled for your hot end? Typically I dont see more than 4C change in second, so wildly fluctuated makes me thing bad data or bad interpretation of data. I'd also look at the thermistor if it's shorting or wires look damaged


---
**Tomek Brzezinski** *November 12, 2016 20:09*

Right thermistor meaning, when Marlin is installed you have set the right thermistor type in the definitions. 


---
**jerryflyguy** *November 12, 2016 20:16*

Yes, it was working for ~2 months, with about 1Deg variation, suddenly it's jumping all over the place 


---
**Sean B** *November 12, 2016 20:16*

Check thermistor connection.  Did you do PID time under commodities you normally would run?  Did you put the results of your running in the config?  Smoothie or Marlin?


---
**Tomek Brzezinski** *November 12, 2016 20:20*

Working for 2months then not working- definitively replace the thermistor as first pass potential solution. I'd also check the resistance on the heater cartridge if you want to confirm it hasn't partially burnt out.


---
**Sean B** *November 12, 2016 20:21*

**+Tomek Brzezinski** I doubt the heater cartridge is involved if he is seeing large swings in temps.  


---
**Miguel Sánchez** *November 12, 2016 21:57*

Thermistor cable contact might be failing. Do you experience these  variations while printing only or with the printhead still too?



Jumping big numbers suggest transitory connection failure, not bad enough to trigger MINTEMP though.


---
**Tomek Brzezinski** *November 13, 2016 17:44*

**+Sean B** I agree the heater cartridge is likely not involved, but the low cost of checking resistance vs not is mostly why I'd consider it. Cartridges can have partial shorts that behave weirdly too, so maybe pulling nonconstant load for a given signal (which would screw up PID.) First thing to do is check all the thermistor cabling, including connectors and the extension wire from hot end back to motion control board, and also probably easy enough to replace the thermistor as well (but at least all those cables and connectors.) 


---
**jerryflyguy** *November 14, 2016 14:37*

Thanks everyone, I'm leaning towards a connection issue. I don't think it can be the heater, the rate of change is just too large a magnitude for a 25w (30deg swing/sec) it's always down from the set temp. It might be a degree or two higher then drops ~20-30 for a second then back to the set temp. I put some better constraints on the wiring and will see if that helps, (first look at it seems like it might have) and will report back. My impression has been that the thermistor failure mode is usually pretty abrupt and one & done? 


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/AjaKpQd2HbJ) &mdash; content and formatting may not be reliable*
