---
layout: post
title: "Greetings to all from Chile, I have a question , some of you have done this printer profiles 30x30 but what happens is that in Chile do not sell 20x20 ."
date: November 12, 2014 01:06
category: "Discussion"
author: i-MaKe Chile
---
Greetings to all from Chile, I have a question , some of you have done this printer profiles 30x30 but what happens is that in Chile do not sell 20x20 .





**i-MaKe Chile**

---
---
**D Rob** *November 12, 2014 02:24*

What is available? Imperial? If si I've done 10/10 which is 1 inch by 1 inch


---
**i-MaKe Chile** *November 12, 2014 04:15*

30x30 milimeters


---
**D Rob** *November 12, 2014 04:35*

How big is the center hole? and is it circular? I've seen some weird holes. Using square tubing like **+Shauki Bagdadi** may be your best route. 3030 is heavy and the tnuts and whatnot get expensive. You could even rivet together angle. And bolt the x/y plates on if you use the flat style I'm using now. Check my pics of the UL-T-Slot to see what I mean.if you have means to print now, I would print inserts for the tubing for building the frame. like t's and angles. These could be attached with bolts. Would even lighten the build. Like this. [https://www.shapeways.com/wordpress/wp-content/uploads/2010/10/shapeways3dprintedfurniturecomponent2.jpg](https://www.shapeways.com/wordpress/wp-content/uploads/2010/10/shapeways3dprintedfurniturecomponent2.jpg)


---
*Imported from [Google+](https://plus.google.com/111211451008574183794/posts/h78zysRjghD) &mdash; content and formatting may not be reliable*
