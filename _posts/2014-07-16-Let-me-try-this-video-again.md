---
layout: post
title: "Let me try this video again"
date: July 16, 2014 23:47
category: "Show and Tell"
author: Brian Bland
---
Let me try this video again.


**Video content missing for image https://lh3.googleusercontent.com/-s4Om7nf1EE8/U8cO3gjleCI/AAAAAAAADKo/OkY59F7Zv80/s0/20140712_185043.mp4.gif**
![images/f48ca73acd5075754b4da2d3121a6c55.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f48ca73acd5075754b4da2d3121a6c55.gif)



**Brian Bland**

---
---
**Ricardo de Sena** *July 17, 2014 00:28*

Wonderfull machine.


---
**Jean-Francois Couture** *July 17, 2014 00:46*

geat looking.. those are 8mm rods on the X/Y and 10mm on the drives right ?


---
**Jim Squirrel** *July 17, 2014 00:57*

Very nice!


---
**Eric Lien** *July 17, 2014 01:49*

Z stage looks nice.


---
**ThantiK** *July 20, 2014 07:03*

Finally got around to playing with this today myself; The barbs on my Kraken are a bit larger than these must be, so I've gotta modify it so that the sides near the barbs are a little wider.


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/Bq5X34EygPc) &mdash; content and formatting may not be reliable*
