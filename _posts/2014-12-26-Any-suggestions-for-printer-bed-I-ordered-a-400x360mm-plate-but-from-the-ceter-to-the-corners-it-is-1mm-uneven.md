---
layout: post
title: "Any suggestions for printer bed? I ordered a 400x360mm plate but from the ceter to the corners it is 1mm uneven"
date: December 26, 2014 17:47
category: "Discussion"
author: Florian Schütte
---
Any suggestions for printer bed? I ordered a 400x360mm plate but from the ceter to the corners it is 1mm uneven. Very bad. Now i am afraid of ordering anything new because i don't know if i will get a even plate. I don't want to waste more money. Milled plates are a bit expensive (in comparsion to the plate i ordered). I thought about glass. But my glass-man said it will also not be even.





**Florian Schütte**

---
---
**Eric Lien** *December 26, 2014 18:40*

If it's thin you can straighten it. I have a cheap 1/8 heater spreader. It was warped as heck. And hour with a rubber mallet, a few beers, and a straight edge took care of that.


---
**Eric Lien** *December 26, 2014 18:41*

I still recommend glass on top. My $2 window glass is perfectly flat.


---
**Florian Schütte** *December 26, 2014 18:51*

It is 5mm thick. I thought about installing the silicone heater to see if it gets better when the plate is hot. 

I also thought about a rubber mallet.


---
**Mike Miller** *December 26, 2014 20:12*

I'm learning about garolite...it's cheap Light, and stiff, but the blue tape adhesive didn't want to come off... Still learning. 


---
**James Rivera** *December 26, 2014 20:27*

I'll second **+Eric Lien**'s comment on window glass. I've been using it on my 1st Printrbot for almost 2 years and it has been a huge improvement over just printing directly the warped PCB heater.


---
**James Rivera** *December 26, 2014 20:28*

**+Mike Miller** good to know. I recently bought a 12x12 inch piece of garolite that I have not done anything with yet. But when (if?) I find the time, I'll avoid putting blue tape on it. 


---
**Mike Miller** *December 26, 2014 20:34*

I wouldn't take my word as gospel. ;) It was easy to work with with a dremel to fit to my build area, the heat pad stuck WELL to it, and I wanna think alcohol was working well on the mess of blue tape adhesive and elmer's stick glue mess I made. Second layer of blue tap and all is well again. I'd bet goo gone or equivalent would work just fine. Blue tape seems to last longer on Garolite than it did in Aluminum. 


---
**Daniel F** *December 26, 2014 21:53*

My bed was also not entirely flat, I managed to get it almost flat, there is around 0.3 mm difference (size is 350x350mm). I put a 3mm mirror tile on top, fixed with clips. The mirror is totally flat, I manage to print large objects like a boomerang. If you need a flat bed from aluminium you can get a cast and milled board like this one: [https://www.aluminium-online-shop.de](https://www.aluminium-online-shop.de), look for "ALLU GUSSPLATTEN FEINGEFRÄST"﻿

Not cheap, but no mallet...


---
**James Rivera** *December 27, 2014 00:08*

I ended up just buying my 2nd Printrbot's aluminum bed from Printrbot.com, but I had considered this one, too (they are in the US, and have a Seattle office, which would likely mean cheaper shipping for me).



And this is a configurable size, which is nice, but you'd have to drill/tap any holes you wanted by yourself (which is why I just bought mine from Printrbot):

[https://www.onlinemetals.com/merchant.cfm?pid=7859&step=4&showunits=inches&id=915&top_cat=60](https://www.onlinemetals.com/merchant.cfm?pid=7859&step=4&showunits=inches&id=915&top_cat=60)


---
**Ivans Nabereznihs** *December 27, 2014 14:30*

I found a MK2A 300x300x2 hotbed for US$ 47 on AliExpress. So I want to use it with mirror and BuildTak 3D Print Surface. My Prusa i3 has BuildTak 3D Print Surface and printing ABS on 90 C without any problem.


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/THitYrHcSvy) &mdash; content and formatting may not be reliable*
