---
layout: post
title: "Going mad this afternoon. Been a while since I messed with my Eustathios, yesterday got a PEI sheet laminated to my glass and ran through tramming the bed with the piece of paper and many repetitions method"
date: April 30, 2017 19:56
category: "Discussion"
author: Ben Delarre
---
Going mad this afternoon. Been a while since I messed with my Eustathios, yesterday got a PEI sheet laminated to my glass and ran through tramming the bed with the piece of paper and many repetitions method.



Went to do some prints this morning, and I notice that the first layer is excessively squished. Now I measured the paper I trammed the bed with to 0.1mm so I set the Global G-Code offset in the Z-Axis in Simplify 3D to -0.1mm to compensate. This should in theory mean that a zero in the slider will result in the head being precisely at the surface of the PEI right?



Checking the gcode I see that the first layer is being printed at Z0.092mm as I would expect, meaning total distance of the head is 0.192mm from the bed (my layer height). However the first layer is clearly way too close to the bed.



Obviously I can just mess with my Z-Axis offset to fix this, but I'm trying to understand where my logic has failed here. Any ideas?





**Ben Delarre**

---
---
**Ben Delarre** *April 30, 2017 20:13*

You know I think I just realized why this isn't working as expected. I trammed the bed without heating the nozzle, I did heat the bed, but completely overlooked the nozzle temperature, which obviously is a pretty large influence on expansion since its much hotter. D'oh!


---
**Zane Baird** *April 30, 2017 20:13*

I've personally given up logic when considering the first layer after playing with different slicers. Instead, I adjust it to achieve the optimal height according to appearance during printing, measure the total height of the print after, and make changes from there. So long as your Z-steps are correct, a 0.05mm change in your true z-axis starging height likely won't make a difference in the functional part but will definitely be obvious in the first layer quality. 


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/8ZkfJJjWGCt) &mdash; content and formatting may not be reliable*
