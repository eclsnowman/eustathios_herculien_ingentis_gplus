---
layout: post
title: "Digikey-Molex connectors. On the BOM it notes one of each part"
date: May 31, 2016 04:21
category: "Discussion"
author: jerryflyguy
---
Digikey-Molex connectors. 



On the BOM it notes one of each part. I'm thinking there should be more than one, any suggestions on how many plugs I should order? I can extrapolate the number of pins from that.



Thnx





(Azteeg ordered, Misumi ordered, Printed parts on their way.. Now just need to order the other components) 





**jerryflyguy**

---
---
**Oliver Seiler** *May 31, 2016 04:34*

I find the micro molex connectors a bit of a pain to deal with, you might consider using something else. The number of pins depends on the carriage/hotend combo you're planning to use (heater, hotend fan, blower, led lights, ...)


---
**jerryflyguy** *May 31, 2016 05:03*

**+Oliver Seiler** thanks Oliver, just trying to sort out where I should be using molex connectors. I've no idea where I should be putting them. It looks like there is a molex connector on the hot end carriage but not sure where else they are needed? Stepper wires? Heater cartridge? Thermistor?


---
**Oliver Seiler** *May 31, 2016 05:12*

Oh right, I've only got one pair on the carriage - don't think you need them anywhere else.


---
**Maxime Favre** *May 31, 2016 05:35*

You need at least 6: 2x Heater, 2x Thermistor, 2x Fan(s). If you add leds, separate command for hot end fan and blower fan it may change.

Order some spare pins, you'll probably break some (and it's cheap).


---
**jerryflyguy** *May 31, 2016 05:43*

**+Maxime Favre** so 6 pins? One connector? [Extra pins is a for-sure... I'm good at wrecking them :) ]


---
**Maxime Favre** *May 31, 2016 05:48*

Buy one set of 8 pins connector(male female) + pins. A spare connector set is always nice to have.


---
**jerryflyguy** *May 31, 2016 05:55*

**+Maxime Favre** very true! Good idea, come to think of it... There are 2 fans On the head.. So I'm thinking two 6 pin connectors may be a good compromise.


---
**Oliver Seiler** *May 31, 2016 06:47*

I'd highly recommend not putting all cables on the same connector, because it prevents you from ever moving on to another carriage with different cable needs without re-wiring the connector (which is a massive pain). It also makes it a lot easier to swap out a part for a replacement e.g. if you have a second heater block.

E.g. a 2 pin for the cooling fan, 4 pin for heater + thermistor, 2 for the blower and 2 for LED if required.


---
**Daniel F** *May 31, 2016 11:28*

I use 6 pin mpx connectors like these: [http://www.aliexpress.com/item/10-pairs-MPX-Connector-s-plug-24K-Goldplated-pin-40Amp-RC-aeromodelling-field-Accessories/32477446474.html?spm=2114.30010308.3.2.TG0QcP&ws_ab_test=searchweb201556_7,searchweb201602_2_10037_10033_507_10032_10020_10017_10021_10022_10009_10008_10018_101_10019,searchweb201603_3&btsid=3df96bbd-d8b3-4922-a04c-ac133218661b](http://www.aliexpress.com/item/10-pairs-MPX-Connector-s-plug-24K-Goldplated-pin-40Amp-RC-aeromodelling-field-Accessories/32477446474.html?spm=2114.30010308.3.2.TG0QcP&ws_ab_test=searchweb201556_7,searchweb201602_2_10037_10033_507_10032_10020_10017_10021_10022_10009_10008_10018_101_10019,searchweb201603_3&btsid=3df96bbd-d8b3-4922-a04c-ac133218661b)

These are campact, ok to solder and support currents up to 40A. I had to tie them together with a zip tie as the went apart during a long print due to vibrations.

With 6 pin you need to combine ground if you want to use cooling for HE transition zone and objects. I only print ABS, so I don't use a fan for object cooling.


---
**jerryflyguy** *May 31, 2016 14:08*

**+Oliver Seiler** totally makes sense! I never thought of that, but glad I asked the question!


---
**jerryflyguy** *May 31, 2016 14:10*

**+Daniel F** how long did they take to come? They are certainly cheap enough. The molex from Digikey are bulkier which may not be a positive if I use a few in that same area.


---
**Daniel F** *May 31, 2016 17:51*

Around 3 weeks to Europe


---
**Jeff DeMaagd** *May 31, 2016 20:23*

Maybe try Farnell from the UK?


---
**jerryflyguy** *May 31, 2016 20:31*

**+Jeff DeMaagd** I think I'll prob just use Digikey, I'm in Canada and they ship here w/ no duty/customs stuff to deal with, it's pretty painless.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/4vBsELiVvUS) &mdash; content and formatting may not be reliable*
