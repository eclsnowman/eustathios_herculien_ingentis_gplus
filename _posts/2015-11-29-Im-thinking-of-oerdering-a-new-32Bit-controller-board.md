---
layout: post
title: "I'm thinking of oerdering a new 32Bit controller board"
date: November 29, 2015 14:02
category: "Discussion"
author: Florian Schütte
---
I'm thinking of oerdering a new 32Bit controller board. So a few questions about that: 



- I found the Azteeg x5 mini only at [panucatt.com](http://panucatt.com). Is there any distributor in europe (Germany)? Also it's out of stock @ panucatt. 



- Why not the smoothiboard (ok, it's pricier)? 



- Are there other good controllers like this two?





**Florian Schütte**

---
---
**ThantiK** *November 29, 2015 14:12*

Smoothieboard is a good choice, and also the new Replicape+BeagleBone Black is likely a good 32 bit controller board due to the PRUs.



I'd do your research on each and see which fits your needs better.


---
**Frank “Helmi” Helmschrott** *November 29, 2015 14:20*

**+Florian Schütte** I'm using an Arduino Due together with the fantastic RADDS board and their 1/128 stepper drivers. I'm quite happy with it.


---
**Florian Schütte** *November 29, 2015 14:30*

**+ThantiK**​ i have a BBB and a self made replicape (thanks to opensource). But the Firmware is crappy as hell and development seems to go very slow. The interface and settings are A bit complicated. Not good done. So im not really happy with that and never got it to work like it should (WLAN issues, display issues etc. Etc.)



**+Frank Helmschrott**​ i have megatronics v2 with Marlin at the moment. Arduino is the reason why i want to change. Also crappy as hell and slow. (But hacking own things is very easy and exzessive made by me)


---
**Florian Schütte** *November 29, 2015 14:33*

**+ThantiK**​ Also replicape is not "New". Made mine about 2 years ago while working for a 3d printing startup who wanted to use them.


---
**Frank “Helmi” Helmschrott** *November 29, 2015 14:35*

**+Florian Schütte** Arduino Duo is not AVR but 32bit. RADDS itself is a bit like RAMPS was for AVR. Details here: [http://doku.radds.org/en/](http://doku.radds.org/en/)



Also the makers are german - i'm sure they'll answer your questions if you have any.



Especially when you like to hack things around a lot you shouldn't go with an all-integrated solution like smoothiebard. You couldn't even change the stepper drivers.


---
**ThantiK** *November 29, 2015 15:02*

**+Florian Schütte**, The replicape just recently released a NEW version.  Revision 2.  By "new replicape" I specifically meant the revision 2 board that just finished kickstarter.


---
**Florian Schütte** *November 29, 2015 15:20*

**+Frank Helmschrott**​​ it's not the AVR that i dislike. It is Arduino that i don't like. Or wich FW you run on the Due? Smoothie?

I know about the installed stepper deivers in smoothieboard. But they can be deactivated to use other exzernal ones. This will be OK for me. That's the reason why i prever azteeg.



**+ThantiK**​​ your right. Read about the rev2 few weeks ago. Forgot about that. But when kickstarter is just finished, i think it will take some month to get one. And the FW still isn't great. 



Also smoothieboard will get a rev2. But that also takes some month to go. They are testing prototypes at the moment and FW work has to be done.  So this two boards are (sadly) no option at the moment.



My biggest question at the moment: where can i buy azteeg in Europe.﻿


---
**Frank “Helmi” Helmschrott** *November 29, 2015 15:26*

**+Florian Schütte** Maybe i just don't get it but how is the firmware related to "Arduino"? You said you ran Marlin so far - i didn't use that - alyways used Repetier until now, but if you don't like it you probably should have tried another firmware first instead of switching boards. But anyway if you prefer to use other boards... they're most probably good too.


---
**Ryan Carlyle** *November 29, 2015 15:34*

Due+RADDS has another good firmware option now, the dc42 fork of RepRapFirmware. It has some great delta features if you're into that. Also runs Marlin and Repetier of course. 






---
**Florian Schütte** *November 29, 2015 15:45*

**+Frank Helmschrott**​​ Marlin is written with help of Arduino "Framework". And this is not really great (inefficient methods for nearly all HW related). Sure, it's CPP and you may rewrite it to be able to compile directly with AVR GCC. But that is to much work. The positive effect is, that the FW is really easy to unterstand and to hack.



I thank you all for your suggestions. I will do a little bit research now and come back for more question ;)﻿


---
**Frank “Helmi” Helmschrott** *November 29, 2015 15:52*

Ok, didn't know you're looking for code perfection - i thought it's about printing. In this case i can't tell and don't care so much :)


---
**Florian Schütte** *November 29, 2015 16:27*

Bad Code -> Bad Firmware -> Bad Printing

+ ugly 8-Bit 




---
**Frank “Helmi” Helmschrott** *November 29, 2015 16:31*

I'm not trying to evangelize for anything here but that's just not true as a general critics on AVR hardware and firmware that relies on it. I have never printed on anything else but Arduino based boards for around 4 years now and haven't had print quality problems that were relating on software. It's more the other way around: I bet you won't find any hard/software base that is able to beat the RADDS/Repetier combo in printing results. I'd be happy to be proven wrong.


---
**Michaël Memeteau** *November 29, 2015 16:59*

Happy with an AZSMZ ordered from Aliexpress. Works with 24V even if polyfuses are spec's up to 16V. A (very) cheap alternative if you don't mind not having the possibility to vary the current by sotware (Regular Polulu driver mount). 


---
**Florian Schütte** *November 29, 2015 17:15*

So, i took a look at the mentioned boards.



- SunBeam 2 (the one from Poland mentioned by **+Alex Skoruppa**): Way to few tecnical information. Documentation only in polish. May i use thermocouples? A display (i don't think so)? Some GPIOs for extensions? (let's say it like Bruce Willis: "GPIO, Motherf****r" - or "Schweinebacke" in german - :D) So this is not attractiv.



- RADDS (mentioned by **+Frank Helmschrott**): Will have to hack it for using with 24V. No network connection. Also not really the thing i want.



- Replicape rev B (mentioned by **+ThantiK**): I like! ^^ Here is why: Great HW (as i can say by builduing and testing rev A by hand), the TCM2100 driver (i was just about to order them), i like this BBB and allready have one (sadly the old 2GB eMMC one). BUT my last experiences with the FW / Thing-Image and the Angström Linux distribution were really really bad. We then tried to code our own FW to make use of the coprocessors but ended also with no good results. This time it was our little experience with printer FW.



So i think i will get out my old BBB and RepliCape and give it another try with the newest Thing-Image. If it will work good, i think rev B Cape will be my choice. Hopefully i will get a rev C BBB. They are widely sold out .



If RepliCape will fail, i think i will try to get a Azteeg anywhere or wil wait for rev 2 of smoothieboard.




---
**Frank “Helmi” Helmschrott** *November 29, 2015 17:19*

RADDS works fine with 24V out of the box. Maybe you haven't looked at the 1.5 version which is out for quite over a year now. [http://doku.radds.org/dokumentation/radds/](http://doku.radds.org/dokumentation/radds/) but yes, it doesn't have network on board. I have it connected to Repetier Server with webinterface on a seperate linux box, others build in a Raspberry Pi or similar within their printer. Others again put in on a UDOO board instead of a plain Arduino which itself has network functionality onboard. But that's only for hackers sort of.


---
**Florian Schütte** *November 29, 2015 17:23*

**+Frank Helmschrott** Arduino based boards (8-Bit ones) do have their disatvanteges. I often have to slow down the print while SD printing, because the controller is not able to read the card fast enough. Also the graphic display is very slow. sometimes it tooks seconds before someting changes on the display. I know this is due to the 8-Bit and my Marlin FW using Arduino. I never tried the repetier FW but i think i will do next WE. I will not expect this issues on a Arduino Due. But the network connection is a big point for me.




---
**Frank “Helmi” Helmschrott** *November 29, 2015 17:26*

Just to be sure: Arduino DUE and RADDS is a 32bit setup that has nothing to do with AVR based electronics. But again: Not trying to evangelize - just trying to clear things up. 



Sidenote: just notized that the Udoo neo board is finally available - think i'll check that out for future RADDS usage as it already is Arduino compatible and has all the network/wifi/bluetooth foo on board.




---
**Ishaan Gov** *November 29, 2015 18:08*

In regards to BBB based controllers, there may also be a MachineKit image that supports 3D printing (I think it's a WIP); just wanted to put that out there


---
**Florian Schütte** *November 29, 2015 19:25*

**+Frank Helmschrott** Yes, i understood. But you said every Arduino board works fine. But i made other experiences with the 8Bit ones. As is said, i don't expect that with a Due (because of 32Bit and higher core freq). Let's end this ;)



**+Alex Skoruppa** Thanks for the link. I was really blind. Of cause it has a display. But again GPIO...two will be used at my actual printer setup, so not much left for further expansions like filament diameter sensor, which is the next thing i want to play with. 



I don't own a printer to print with. I own it to tinker on it :D

This is the reason why i'm like looking for THE OMNIPOTENT BOARD which don't exist, i think. So it's a hard decision for me, which disadvantages are ok and which not.

But all the answers here will help me. So if anyone has some other pros or cons for the one or the other board, tell me.


---
**Øystein Krog** *November 29, 2015 21:17*

Aakaar brainboard (smoothie) looked really nice, but their indiegogo failed and it's been quiet since.


---
**Jo Miller** *November 30, 2015 08:12*

AZSMZ mini , 32 bit , smoothieware, 60$

took me 30 min to hook it up to the Herculien.



Only con:  not for dual extrusion



will use it on my next Delta-build too


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/UTzkVcn6xgb) &mdash; content and formatting may not be reliable*
