---
layout: post
title: "So I'm trying PID auto tune for my hotend and it keeps swinging 10degrees each way my bed is functioning fine.i can't figure it out and I'm still pretty unfamiliar with config for smoothieware"
date: May 24, 2015 03:59
category: "Discussion"
author: Derek Schuetz
---
So I'm trying PID auto tune for my hotend and it keeps swinging 10degrees each way my bed is functioning fine.i can't figure it out and I'm still pretty unfamiliar with config for smoothieware. Does any one have any idea? My thermistor is riorand 100k thermistor, and I have he rr100k in my config selected as I think that's the right one 





**Derek Schuetz**

---
---
**Eric Lien** *May 24, 2015 04:23*

I had to hand tune mine. IMHO Smoothieware auto tune is crap.


---
**Eric Lien** *May 24, 2015 04:38*

I used this post to help determine the P I and D values: [https://groups.google.com/forum/m/#!msg/smoothieware-support/woDiL6iFa0M/FRwf1HYgUKkJ](https://groups.google.com/forum/m/#!msg/smoothieware-support/woDiL6iFa0M/FRwf1HYgUKkJ)


---
**Eric Lien** *May 24, 2015 04:38*

Now I stay +/- 0.3 deg


---
**Derek Schuetz** *May 24, 2015 07:12*

 So I tried those low settings and after about 10 minutes it stabilizes...I don't get why it has so much more trouble


---
**Chris Okuda** *May 24, 2015 07:40*

Can smoothieware do dead-time control? I use that on Repetier and it's far superior to PID for me.


---
**Eric Lien** *May 24, 2015 10:58*

**+Derek Schuetz** once I get home I will give you my newest settings. I overshoot about 4 degrees, then am rock stable in less than 30 sec.


---
**Isaac Arciaga** *May 24, 2015 12:20*

**+Derek Schuetz** is the swing happening once your hotend starts extruding at the start of your print and also when your print cooling fans turn on?


---
**Eric Lien** *May 24, 2015 12:53*

**+Isaac Arciaga** I thought my heater cartridge was going bad until I manually tuned Smoothieware pid on the hot end. It's auto tune was WAY off. In that long forum post I linked to there are good explanations of the functions of P, I, and D. I cannot remember where I ended up... But will check when I get home on Monday. I used to have the same crazy swings as **+Derek Schuetz**​.


---
**Derek Schuetz** *May 24, 2015 13:26*

Thanks **+Eric Lien** 


---
**Brad Hopper** *May 24, 2015 14:27*

Changing I_max to limit initial overshoot was a key setting, not sure why the auto tune doesn't adjust that.


---
**Derek Schuetz** *May 24, 2015 14:54*

@brad hopper But my swings were +/- 10 when I used auto tune settings  


---
**Eric Lien** *May 24, 2015 15:36*

**+Derek Schuetz** I would start with the auto tune settings, then adjust. Per that discussion:



"M303 runs autotune which will give a baseline to start further manual tuning.



the D term opposes oscillations and causes the heater to turn off early during warmup to prevent overshooting.



the P term provides coarse drive when the temperature is far from the target



the I term causes the temperature to slowly move towards sitting right on the target instead of below or above it."



I believe my I term is very small (well below 1).


---
**Brad Hopper** *May 24, 2015 16:11*

The I_max is different from I and in my case the auto pid worked just OK but I was then unable to manually adjust better until I got rid of the massive first overshoot. After that my tweaks could be observed to make a difference.


---
**Isaac Arciaga** *May 24, 2015 18:44*

**+Eric Lien** ah I did not notice the link. Thank you for the information!


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/24ubiBYffEF) &mdash; content and formatting may not be reliable*
