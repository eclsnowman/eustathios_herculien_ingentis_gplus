---
layout: post
title: "Really simple question regarding mosfet driver module for PWM control of cooling fans that have too high of a current for my Azteeg X5mini V3"
date: September 03, 2017 20:36
category: "Discussion"
author: Sean B
---
Really simple question regarding mosfet driver module for PWM control of cooling fans that have too high of a current for my Azteeg X5mini V3.  I don't want to  fry anything and wiring is not more forte.  My question is primarily focused on the correct signal line, should it be the +5V line from Azteeg into the SIG of the driver module?  See horribly drawn image attached.



![images/144a4ab82a2c6a677d457df1711bff0d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/144a4ab82a2c6a677d457df1711bff0d.png)



**Sean B**

---
---
**Zane Baird** *September 03, 2017 21:20*

I would think you need vcc on your external board hooked up to a 5v pin, but everything else looks fine. Haven't used an external circuit for the fan before though. Normally I just wire the fan directly


---
**Sean B** *September 03, 2017 21:22*

I would wire the fan directly but there are two fans and each one exceeds the rating for this connection (2 12v fans wired in series with 24v supply).  I wonder what the difference is between VCC and signal.


---
**Sean B** *September 04, 2017 12:53*

Well I tested it out, for some reasons the fans turn on and stay on when the 5V is on SIG.  The fans shut off if I put it on VCC.  I can't fully test it because when I type the misc command to turn on the fans with M42 it says it doesn't recognize the command, is it case sensitive?




---
**Zane Baird** *September 04, 2017 14:53*

I looked at your diagram again and I understand what's going on now. I think you need to hook up VCC to the +5V, the signal to SW-, and ground directly to the gnd of your azteeg. Then in config you'll have to change the line specifying the M42 pin to:



switch.misc.output_pin                       2.4!              #



This way its a high 5V when you turn on the fan. Otherwise setting the fan on will actually turn it off the way you have wired. 



Let me know if this works for you



Edit: That might be the wrong pin from looking at the diagram on panucatts site. If you aren't using an LCD, I would hook your signal up to the pin labeled 2.4 on EXP1 and you won't have to append the ! to the config file


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/MsShy3Z6j3D) &mdash; content and formatting may not be reliable*
