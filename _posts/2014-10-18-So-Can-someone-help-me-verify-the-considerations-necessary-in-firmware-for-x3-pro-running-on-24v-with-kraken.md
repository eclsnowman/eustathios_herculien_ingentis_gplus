---
layout: post
title: "So... Can someone help me verify the considerations necessary in firmware for x3 pro running on 24v with kraken?"
date: October 18, 2014 17:48
category: "Discussion"
author: Tony White
---
So...

Can someone help me verify the considerations necessary in firmware for x3 pro running on 24v with kraken?

Hot ends require 12v, so pwm max is cut in half

Do I need 12v for thermistors or can I use different/scaled tables?

Steppers should be happy



Need to verify I should add male pins to motor drivers- for i2c connection current limit setting?



Heated bed is rated for 24v



Ready to do wear in!





**Tony White**

---
---
**Miguel Sánchez** *October 18, 2014 17:56*

As power is voltage squared divided by resistance, if you double the voltage  power is four times what it was before. If you do PWM 50% you are still delivering double the power to the 12V heaters now. Whether that is causing you problems or not I do not know, but if you want to keep the power the same your PWM should be 25% instead.



Steppers will be more than happy but if you have polyfuses in your electronics watch out as most of them are only rated 12-15V.


---
**Liam Jackson** *October 18, 2014 18:07*

I thought thermistors were driven from logic voltage (e.g. 5V) so shouldn't matter. I guess check your 5v regualtor can handle 24v and is properly heatsinked if its a linear regulator - as it will get a bit hotter. Do you have fans? might need a 12v regulator to drive those.


---
**Eric Lien** *October 18, 2014 20:22*

My recommendation is buy a 24v heater cartridge and components. They are cheap, and less chance for a horrible meltdown failure. Same with the fans. Just my 2 cents.


---
**Jason Smith (Birds Of Paradise FPV)** *October 19, 2014 13:46*

I second the recommendation for 24v heater cartridges. Had a bad experience once... :)


---
**Tarjei Knapstad** *October 20, 2014 11:04*

Agreed, do yourself a favor and get 24V cartridges :) I've seen a 12V cartrdige literally get red hot myself... Also the auto PID autotuning seems to struggle when I ran a 12V cartridge at 25% PWM. Here's a guy at AliExpress who sells 5-packs of 30W 24V catridges for $10:

[http://www.aliexpress.com/item/5PCS-24v-30W-Ceramic-Cartridge-Wire-Heater-For-Arduino-3D-Printer-Prusa-Reprap/1906915702.html](http://www.aliexpress.com/item/5PCS-24v-30W-Ceramic-Cartridge-Wire-Heater-For-Arduino-3D-Printer-Prusa-Reprap/1906915702.html)


---
**Tony White** *October 21, 2014 05:02*

Thanks for the info guys - remembered the pwm reduction wrong. I'll order the cartridges and double check the regulator. Fans I'll power with a step-down power supply I've used before - 3 Amp capacity: [http://www.amazon.com/dp/B009HPB1OI/ref=sr_ph?ie=UTF8&qid=1413867722&sr=1&keywords=step+down+power](http://www.amazon.com/dp/B009HPB1OI/ref=sr_ph?ie=UTF8&qid=1413867722&sr=1&keywords=step+down+power)


---
**Eric Lien** *October 21, 2014 05:48*

If you did get new fans I recommend these [http://www.ultibots.com/40mm-fan-kysan-tf4010-24h-s/](http://www.ultibots.com/40mm-fan-kysan-tf4010-24h-s/)



Ultibots also has 24v heater cartridges.


---
*Imported from [Google+](https://plus.google.com/+AnthonyWhiteMechE/posts/L5uWFwBkxiX) &mdash; content and formatting may not be reliable*
