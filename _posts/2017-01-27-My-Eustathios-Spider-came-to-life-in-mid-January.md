---
layout: post
title: "My Eustathios Spider came to life in mid-January!!"
date: January 27, 2017 18:33
category: "Discussion"
author: Carter Calhoun
---
My Eustathios Spider came to life in mid-January!!  TLDR:  All worked well and several mods have panned out, all is owed to this community.

<s>-</s>



Researching and building this has been the most fun I've had in a long time, and I am tremendously grateful to this community.  



If YOU have posted, replied, or commented in the past year or two, I have probably read and benefitted from your contributions.. So, thank you to the HercuLien and Eustathios Builders community so much...



Of course, special thanks to **+Eric Lien**, who makes it a priority to support the builders, who is a ready and steady source of encouragement, advisement, and expertise - and makes efforts to be super responsive even with his two little boys asleep at his side and quietly typing out a message on his phone - kudos to you sir...



Super big props to **+Walter Hsiao** for great upgrades.  Please put your Tiko down and keep 'em coming :).



I greatly benefitted from **+Ted Huntington**'s post on a build step-by-step.  It is brief, yet sufficiently comprehensive.



Surprise thank you award to **+Stefano Pagani**, your efforts, obstacles, and willingness to engage the community for solutions and support has been super duper helpful for me to learn from.



Very much looking forward to **+Frank Helmschrott**'s work on carriages, keep it coming.



Many more not directly mentioned here, as addressed above, thank you...



Findings on my build, challenges, things that worked well, continuing needs



MODS THAT WORKED FOR ME - YMMV:

>>RAMPS 1.4 = simple to configure, super cheap and reliable, works for the time being.



>>graphite infused brass bearing / ninjaflex sleeve for the carriage:  More because the BOM push-fit bushings were not to be found.  Though, I did eventually buy the teflon(?) alternatives from SDP/SI, while not currently installed, they seem to fit fine on the rods.



>> 300mm x 300mm MK2a PCB heatbed and one 350W PSU:  I love this solution, saved good amount of money, avoided wiring to mains voltage (because I am liable to start a 2nd fire), avoided finding/milling a custom aluminum plate...YMMV, but I am very happy with this for now.  +Eric Lien informed me that 350w would be insufficient (thank you!), and so I throttled the bang-bang max on the heated to give me some headroom.  Again, seems to be working alright, takes a few minutes to get to 50c, not planning on using ABS.



>> PLA+ printed parts:  eSun PLA+ or PLA Pro as it's called. works fine, no cracked parts.



>> Ball screws stock from Golmart: I increased my Z height by 75mm.  Golmart (aliexpress) has a 500mm ball screw in stock, just ordered it straight w/o any customization, and it works fine.  Definitely should have asked them to reverse the nut though, even after reading WARNINGS and printing Walter's nut reversal tool, I lost ball bearings and screwed it up...Nuts are now running with 8 less and 13 less bearings, respectively...Seems OKAY but, we'll see, and - Dont. Do.  That



>> Misumi 30% off:  New accounts promotion for 30% off, I had to ask for it.  That saved loads on the frame, brackets, rods, etc.



>> RobotDigg FTW:  I got almost everything from RobotDigg (more than is referenced on the BOM).  Major savings going that route, and RobotDigg had great support.



FURTHER CUSTOMIZATIONS:

>> My custom Z stop: Dead simple, precise, easy, and reliable. This is a legit upgrade - picture attached.

1) Affix one corner bracket to the bed support extrusion below the Z endstop, flat end facing up.  

2) Insert longish M5 screw with head facing the endstop. nut above the bracket and below the bracket.

3) Insert nut and, in my case, an M5 thread-knob-thingy.

4) Adjust as needed with easy access, excellent precision and repeatability.



>>Chinese Volcano:  Love this, compliments the great speed capabilities of the Eustathios with a .8mm nozzle and great melt for super fast printing.  Awesome.



AND NEXT UP:

I am now working on installing a Diamond hotend.  I purchased an RRD Fan Extender and a Stepper Expander x2 (admittedly, at this point, it would likely have made more sense to get one of the Smoothieboards that are universally lauded, which is of course still an option...).  



I purchased the BIQU Diamond hotted from Amazon.  It is cheap (~$40), complete (includes fan, heatbreak, pre-wired and assembled) but BEST OF ALL it comes with a carriage that is a direct fit for the Eustathios.  I'm not even sure how this is possible, it's so serendipitous.  I was trying to figure how to adapt it to the gantry and pouring through Thingiverse and it arrived, I inspected, and I literally need to do nothing.  Just so amazing...



If you got to the end of this post, you earn a medal.  Good for you.





![images/aa8ed8ed43da983de7d0bd1ab56ac78f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/aa8ed8ed43da983de7d0bd1ab56ac78f.jpeg)
![images/23320da4a9e25223e1199cdebeb526ec.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/23320da4a9e25223e1199cdebeb526ec.jpeg)
![images/355ed244b9fffb6c580f6b08b6c112fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/355ed244b9fffb6c580f6b08b6c112fa.jpeg)
![images/b2286b36a3c965b92317a3e09cd8e8c6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b2286b36a3c965b92317a3e09cd8e8c6.jpeg)
![images/5d87a2d27ddd5aca3622d741e0f72a22.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5d87a2d27ddd5aca3622d741e0f72a22.jpeg)
![images/afff16a5bebf44d926062ea416ca8b8b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/afff16a5bebf44d926062ea416ca8b8b.jpeg)

**Carter Calhoun**

---
---
**Ted Huntington** *January 27, 2017 19:48*

Congrats and great post Carter. Very informative and interesting choices. So neat that you raised the Z with no trouble. Nice design on the adjustable Z-stop!


---
**Eric Lien** *January 28, 2017 00:32*

Great post. Tons of information. And glad to hear the information and support provided by our community has been helpful. I have said it before, but I'll say it again, this group and it's diverse members are the best 3D Printing resource out there IMHO.



Excited to see all the mods and upgrades you have planned. Keep us posted. Posts like these are the reason I enjoy our community so much.


---
*Imported from [Google+](https://plus.google.com/104205816672263226409/posts/gcY5xeniavW) &mdash; content and formatting may not be reliable*
