---
layout: post
title: "Hey All I'm having a hell of a hard time connecting to my azteeg x5 mini"
date: February 13, 2016 23:48
category: "Discussion"
author: Erik Scott
---
Hey All



I'm having a hell of a hard time connecting to my azteeg x5 mini. If I turn it off (after making some config changes, or just to shut down the printer for a bit) and then turn it back on and try to connect, and it just wont. I've tried removing the USB cable, not removing the USB cable, removing the USB cable before turning it off and reconnecting it after I turn it back on. I've tried the reverse, where I turn it off first, then unplug and replug the USB cable then turn it on, and combinations of this. Still, it's really trial and error when it comes to connecting, and it seems it only connect about 1 in 5 times. 



Has anyone else run into this issue? It's been happening since the very beginning, and I'm getting sick of it. 





**Erik Scott**

---
---
**Gústav K Gústavsson** *February 14, 2016 02:11*

Well azteeg boards I have no experience with but have you tried lowering the baud rate? Have had trouble with some laptops that they can't keep up to the default baud rate and communication is troublesome or nonexistent until I lower the baud rate.


---
**Eric Lien** *February 14, 2016 16:40*

I had the exact same problem with my smoothieboard and the USB controller inside of my desktop motherboard. I would have to restart my computer to be able to reconnect after a power down on the smoothie. I replaced it with an aftermarket USB controller in a PCI slot and that fix the problem for me.


---
**Eric Lien** *February 14, 2016 16:40*

The funny thing is I don't have similar issues with my azteeg x5 on the same computer.


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/axXYBUZSkgR) &mdash; content and formatting may not be reliable*
