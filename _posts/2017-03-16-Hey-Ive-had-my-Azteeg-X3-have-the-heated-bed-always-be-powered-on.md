---
layout: post
title: "Hey, I've had my Azteeg X3 have the heated bed always be powered on"
date: March 16, 2017 00:53
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Hey, I've had my Azteeg X3 have the heated bed always be powered on. I try to turn it off through the Viki to no avail. The light is on and the ssr is flipped on. The light would always be on, even when I got the board with its pre-loaded firmware. Defect?



Anyone have a simplify profile that they wouldn't mind sending over (for the Spider V2) that I could start from?



Now I cannot connect to my X3 at all. I am running S3D and Arduino on a mac and the port doesn't show up like normal. I can connect to my other printers fine. I have tried restarting and 5 different cables. The port on the board is in perfect condition. Printing over SD works fine.





**Stefano Pagani (Stef_FPV)**

---
---
**Jeff DeMaagd** *March 16, 2017 05:10*

The FET sounds stuck on, sounds like a likely defect.



Are you sure you have the right port speed settings? Can you still re flash the board?


---
**Eric Lien** *March 18, 2017 12:40*

Check the bed output on the board. If it is always on your FET probably died. If it is off but the SSR is still on then the SSR is likely faulty. 



For connecting with the MAC do you have the drivers installed? [ftdichip.com - Virtual COM Port Drivers](http://www.ftdichip.com/Drivers/VCP.htm)


---
**Eric Lien** *March 18, 2017 12:41*

If it is the FET you can remap the pins.h to drive the SSR with a different pin since you don't actually need a FET to drive it. Just a signal.


---
**Stefano Pagani (Stef_FPV)** *March 21, 2017 22:31*

**+Eric Lien** Thanks, I have the drivers, and I have connected to it many times before, with nothing changed on my mac. I will re-map the pins, thanks!


---
**Stefano Pagani (Stef_FPV)** *March 22, 2017 20:12*

**+Eric Lien** I have resolved the connection issue and have changed " #define HEATER_BED_PIN 17" in pins.h (17 is the extruder 3 port) but nothing happens.


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/SDYkHxa1xfP) &mdash; content and formatting may not be reliable*
