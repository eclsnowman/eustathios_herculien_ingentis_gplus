---
layout: post
title: "Hi everyone, I would like to put a solid state relays on my eustathios because my bed heats no more than 65 degrees , what relay to choose ?"
date: August 15, 2016 09:11
category: "Mods and User Customizations"
author: Lopez Tom
---
Hi everyone,

I would like to put a solid state relays on my eustathios because my bed heats no more than 65 degrees , what relay to choose ?

Ramps 1.4 power supply 12v





**Lopez Tom**

---
---
**Maxime Favre** *August 15, 2016 09:23*

If your bed is 12V, buy a +20A SSR. Check that your power supply has enough power for the bed.

However since you install the SSR it would be the good time to upgrade the bed for a 24V or 110/230VAC.

It will eat way less amps.


---
**Ted Huntington** *August 15, 2016 16:04*

I have been using this kind: [http://www.ebay.com/itm/Output-24V-380V-40A-SSR-40-DA-Solid-State-Relay-PID-Temperature-Controller-/231693016931?hash=item35f1fadf63:g:9xAAAOSwQTVV~Cnz](http://www.ebay.com/itm/Output-24V-380V-40A-SSR-40-DA-Solid-State-Relay-PID-Temperature-Controller-/231693016931?hash=item35f1fadf63:g:9xAAAOSwQTVV~Cnz) without any problems at all- I bought like 5 of them and have used them for various projects - very cheap and useful.


---
**Ted Huntington** *August 15, 2016 16:05*

just remember to connect the logic to the two low voltage tabs, and the live wire to high voltage tab 1, and one part of the heated pad to high voltage tab 2. The other end of the heated pad, connect to the neutral wire (not to ground!).


---
**Lopez Tom** *August 16, 2016 09:08*

[http://i.imgur.com/bV4RDbx.png](http://i.imgur.com/bV4RDbx.png) 

Like that ?


---
**Maxime Favre** *August 16, 2016 09:22*

yep


---
**Ted Huntington** *August 16, 2016 15:37*

yeah that image is correct for 12v. Sorry about the confusion- I thought you were talking about switching, like many people have, to using a 120V heating pad instead of the 12V heating PCB board. So in that case- for sure do not connect anything higher than 12V DC to a 12V heating PCB ! :) I use the 110v mains live and neutral (AC) instead of 12v and ground from a DC supply- but that is for a heating pad made specifically for 120V- like from alirubber on aliexpress.


---
*Imported from [Google+](https://plus.google.com/107612178038097709041/posts/SrdWuVUyZbZ) &mdash; content and formatting may not be reliable*
