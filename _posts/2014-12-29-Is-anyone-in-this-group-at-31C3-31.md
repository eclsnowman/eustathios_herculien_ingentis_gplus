---
layout: post
title: "Is anyone in this group at 31C3 (31"
date: December 29, 2014 13:41
category: "Discussion"
author: Florian Schütte
---
Is anyone in this group at 31C3 (31. Chaos Communication Congress) right now? May we meet today evening or tomorrow on day 4?

Impression: The pic shows a lagre printer with 1.3m³ build volume. Very nice device :)

![images/9a2a44044eb0cff75b5ca89a0a1d9330.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9a2a44044eb0cff75b5ca89a0a1d9330.jpeg)



**Florian Schütte**

---
---
**Florian Schütte** *January 07, 2015 18:08*

Just discovered that this printer was mentioned here a year ago [https://plus.google.com/108729945898131117315/posts/c8xyLQjWisi](https://plus.google.com/108729945898131117315/posts/c8xyLQjWisi)


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/YQ7iXYvLBRm) &mdash; content and formatting may not be reliable*
