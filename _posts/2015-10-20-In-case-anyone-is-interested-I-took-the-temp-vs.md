---
layout: post
title: "In case anyone is interested. I took the temp vs"
date: October 20, 2015 02:45
category: "Show and Tell"
author: Zane Baird
---
In case anyone is interested. I took the temp vs. resistance values Alirubber gave me for the Herculien heated bed (100k thermistor, 3950 beta value) and fit it to the Steinhart-Hart Equation. The values for the A, B, and C coefficients come out to ~ 0.0006454337947, 0.0002239473005,  and 0.000000087, respectively. 



This is implemented in the smoothie config file by commenting out the line for beta value (if that's what you were previously using) to look as such:



#temperature_control.bed.beta                 3950             



 and adding the following line:



temperature_control.bed.coefficients         0.0006454337947,0.0002239473005,0.000000087



This should allow for accurate temperature readings from 15-125C





**Zane Baird**

---
---
**Zane Baird** *October 20, 2015 02:46*

Note: while it doesn't show up this way on the post, the numbers and "temperature_control.bed.coefficients" should appear on the same line


---
**Jo Miller** *October 20, 2015 07:12*

Thanks Zane



Just what I was looking for.    :-)


---
**Jeff DeMaagd** *October 20, 2015 20:40*

Thanks, I've put it in my notes.



I used r0 and beta to get going, this looks even better.



I don't think their supplied beta number was right either.


---
**Zane Baird** *October 20, 2015 20:48*

**+Jeff DeMaagd** Yeah I'm not confident in the temp readings I was getting using the beta value. I wasn't too concerned as it was for the bed, but I wasn't satisfied in the end. This fits the data they supplied with an RSS of ~3.4 over the 17-125 C range, so it's definitely a much better approach (especially for ABS bed temps)


---
**Jeff DeMaagd** *October 20, 2015 20:53*

Yes. Seriously, thank you.



I had r0 at about 2700 (approximate resistance at 25˚C) and Beta seemed to be most accurate around 3500.


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/Tr9kzbSucjR) &mdash; content and formatting may not be reliable*
