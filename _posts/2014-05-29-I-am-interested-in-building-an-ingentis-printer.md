---
layout: post
title: "I am interested in building an ingentis printer"
date: May 29, 2014 18:39
category: "Discussion"
author: Ethan Hall
---
I am interested in building an ingentis printer. I already built a prusa i2 however I find the build volume limiting and the frame too unstable for really nice prints. I was wondering if you guys could give me your honest opinion on both the ingentis and the Eustathios printer. Also which of the two is cheaper and which is the most reliable.





**Ethan Hall**

---
---
**Ethan Hall** *May 29, 2014 18:53*

a better way to phrase is is to phrase it as two separate questions. question number (1) which of the two printer designs costs the least to make? (2) which printer design is more reliable?


---
**Tim Rastall** *May 29, 2014 20:49*

Ingentis will work out cheaper than Eustathios because it doesn't use belts or pulleys apart from on the Z axis. Eustathios also uses lead screws on the Z axis which can be costly. Eustathios will be more reliable as spectra is harder to maintain and theoretically doesn't provide the same level of repeatability as belts.  I'm currently changing to belts because I'd like to see if print quality differs noticeably. 


---
**Dale Dunn** *May 29, 2014 21:12*

**+Shauki Bagdadi** I'm looking forward to hearing about it.


---
**Ethan Hall** *May 29, 2014 21:52*

Is there a simple way to adapt ingentis to use gt2 belts?


---
**D Rob** *May 29, 2014 22:27*

**+Ethan Hall** sure is. but it will require a modified carriage as the rod spacing needs to change to accommodate the pulleys the gt2 10mm bore pulleys we pushed [http://www.robotdigg.com/product/149/10mm-bore-32-teeth-gt2-pulley](http://www.robotdigg.com/product/149/10mm-bore-32-teeth-gt2-pulley) into making are the smallest od I know of for 10mm bore. its a 32 tooth 2mm pitch. then you need closed loop belts for the motor drive to 1 rod on x and one on y. then cut to length gt2 belts for the other 4. the x y ends will  need to be ones that can clamp and tighten the belts


---
**Ethan Hall** *May 29, 2014 22:30*

do you have to modify any of the stls for the printer or is it just an adjustment to the spacing of the parts on the t slot


---
**D Rob** *May 29, 2014 22:34*

the pillow blocks can just be moved but the carriage would need to have the holes moved further apart between x and y carriage rods unless all changes were made by new x/y ends to adjust the carriage rods. then you would just use calipers as a jig move the pillow blocks and put it all on. but this may have odd effects on the load of the x/y ends


---
**Brian Bland** *May 30, 2014 03:35*

Just print the Eustathios x-y ends and pillow blocks.


---
**D Rob** *May 30, 2014 06:00*

why do I over complicate things


---
**George Salgueiro** *July 02, 2014 07:07*

**+D Rob** why do use 10 mm pulleys instead of 8mm? 


---
**D Rob** *July 02, 2014 07:27*

**+George Salgueiro** Because of the size of the machine. 10mm rods decrease flex. Especially at higher velocity.﻿


---
**D Rob** *July 02, 2014 16:13*

**+Shauki Bagdadi** Yes, and jerk so I sleepily and lazily typed the all encompassing velocity


---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/LGTctGiAzg9) &mdash; content and formatting may not be reliable*
