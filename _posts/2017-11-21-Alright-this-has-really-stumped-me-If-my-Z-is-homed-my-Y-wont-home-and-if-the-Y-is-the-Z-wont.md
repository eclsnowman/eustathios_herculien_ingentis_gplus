---
layout: post
title: "Alright, this has really stumped me... If my Z is homed my Y won't home and if the Y is the Z won't"
date: November 21, 2017 22:41
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Alright, this has really stumped me...



If my Z is homed my Y won't home and if the Y is the Z won't.



I have triple checked my wiring and everything is good (everything on min, no shorts) 

I Sent M119 while holding down just the Z and it reads both Z and Y closed if I hold nothing everything is fine.



I am using an X5 GT (BSD2660 Drivers)



(also if anyone could help with the spew of errors on the second pic that would be great)







![images/473a3b7a788fff6780dc781bb261c8df.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/473a3b7a788fff6780dc781bb261c8df.png)
![images/c38588ff6df5f919dcd188674a13249e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c38588ff6df5f919dcd188674a13249e.png)

**Stefano Pagani (Stef_FPV)**

---


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/WA3XJToozty) &mdash; content and formatting may not be reliable*
