---
layout: post
title: "I just received my fedex notification that my bondtech item will arrive on Friday !"
date: March 12, 2015 02:20
category: "Discussion"
author: Gus Montoya
---
I just received my fedex notification that my bondtech item will arrive on Friday ! So excited :)  To bad it's only going to go to the future build parts bin. :(



[https://d36c0vbvwjb9cx.cloudfront.net/uploads/image/file/60234/Bondtech_Extruder_open.jpg](https://d36c0vbvwjb9cx.cloudfront.net/uploads/image/file/60234/Bondtech_Extruder_open.jpg)





**Gus Montoya**

---
---
**Daniel Salinas** *March 12, 2015 21:29*

I got my parts and everything looks great.  Just a side note but one of the drive gears didn't have a set screw in it so check that when you get your stuff.  I think Martin is traveling to the US so I'm just waiting to hear back from him on that.  **+Eric Lien** will be hanging out with him soon, maybe I can get my question answered by proxy. :)


---
**Eric Lien** *March 12, 2015 21:39*

**+Daniel Salinas** one of them idles on a shaft. So don't install a set screw or you can damage the needle bearing. That bearing presses in.


---
**Daniel Salinas** *March 13, 2015 00:59*

Yeah I know. :)  I ordered 2 mechanical kits so I have 2 drive gears and two idle gears.  Only one of my drive gears came with a set screw.


---
**Gus Montoya** *March 14, 2015 08:37*

Dang, my kit won't come till monday. Eh...no hurry.


---
**Gus Montoya** *March 17, 2015 16:05*

To my surprise I received TWO bondtech units instead of the one I ordered. Ah well, I guess I'll have to change my Eustathios to a dual printer.Don't worry **+Martin Bondéus** I'll send payment for the second unit as described in my email. I'll post pictures later today.


---
**Eric Lien** *March 17, 2015 17:30*

**+Gus Montoya** looks like I need to redesign the new carriage for optional dual extrusion ;)



I wanted to redesign the HercuLien carriage anyway. But that is a project for after MRRF. I am sure there will be a lot there to get ideas from.


---
**Martin Bondéus** *March 17, 2015 17:37*

Or I think you could sell the second unit **+Gus Montoya**  as I think there will be a big demand of the units once everyone starts to use them.


---
**Gus Montoya** *March 17, 2015 19:58*

**+Eric Lien** Hopefully I'll be skillful enough to redesign the carriage myself by that time. But any contribution is appreciated as always.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/8um9atPRVHz) &mdash; content and formatting may not be reliable*
