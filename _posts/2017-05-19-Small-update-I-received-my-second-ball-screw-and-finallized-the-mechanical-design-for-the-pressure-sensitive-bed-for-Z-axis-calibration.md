---
layout: post
title: "Small update: I received my second ball screw and finallized the mechanical design for the pressure sensitive bed for Z axis calibration"
date: May 19, 2017 20:53
category: "Build Logs"
author: Christian Wessels
---
Small update: I received my second ball screw and finallized the mechanical design for the pressure sensitive bed for Z axis calibration. Now i need to create a circuit board for the arduino and mounts to hold the cell sensors.

On the second picture you can see my idea for holding the psu, i printed two bars with hammernut profile on the ends. 

On the third picture you can see why the chosen space (eyeballed) was a bad idea, i missed the ability to mount my baseboard higher by only a few mm.

Last picture is my designated extruder setup for the start. E3D Titan clone from china which looks pretty good for less than half the price.



I am stil waiting on 3 10mm bore 32teeth pulleys which i either lost prior to assembly or have never been shipped in my package. Unfortunatly robotdigg seems to be the only source for those and waiting for stuff from china to arrive is a bit frustrating but i have enough other work left to do ;)



![images/48d098685aa95fd669cc52d4b438d3e2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/48d098685aa95fd669cc52d4b438d3e2.jpeg)
![images/2c2527c223fd22dfb59ee85bb4c7e2a9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2c2527c223fd22dfb59ee85bb4c7e2a9.jpeg)
![images/f930497a68c7bb1efd058396df570494.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f930497a68c7bb1efd058396df570494.jpeg)
![images/c8f64658708346dfa691a50d390630fe.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c8f64658708346dfa691a50d390630fe.jpeg)

**Christian Wessels**

---
---
**Eric Lien** *May 20, 2017 00:11*

Looking great. Love the custom bed probe solution.


---
**Eric Lien** *May 20, 2017 00:12*

And **+SMW3D**​ has those 32 tooth 10mm pulleys:



[smw3d.com - GT Pulleys (GT2 and GT3)](https://www.smw3d.com/gt-pulleys-gt2-and-gt3/)


---
*Imported from [Google+](https://plus.google.com/106958387043410954308/posts/EyDGB7coXrV) &mdash; content and formatting may not be reliable*
