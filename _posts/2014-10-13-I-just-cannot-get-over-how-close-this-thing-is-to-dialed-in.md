---
layout: post
title: "I just cannot get over how close this thing is to dialed in"
date: October 13, 2014 00:45
category: "Show and Tell"
author: Mike Miller
---
I just cannot get over how close this thing is to dialed in. This is prints 6 and 7 (total, including failures to start)...6 stopped when it did because a pulley came of. (That kinda messes up the ability to restart.)



I'll put out an overall video update in a week or so, in the meantime, Sláinte!



![images/4c696122f25908a32fac63873df2029b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4c696122f25908a32fac63873df2029b.jpeg)
![images/7fdd3cc915aef1f93082244b0d07f8c5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7fdd3cc915aef1f93082244b0d07f8c5.jpeg)

**Mike Miller**

---
---
**Eric Lien** *October 13, 2014 03:41*

Congratulations. 


---
**Mike Miller** *October 13, 2014 10:57*

I'll grab the torture test (lots of retractions)

And give it a shot. 



I AM noticing some lateral walking on the igus bushings...movement in X can be seen as tiny movements you can see in the Y direction by looking at the gaps in the bushing. 


---
**Eric Lien** *October 13, 2014 11:31*

**+Mike Miller** You still using the Teflon method to hold them in? Teflon has memory so it does spring back well once deformed.


---
**Mike Miller** *October 13, 2014 12:02*

I am, but the problem (so far) seems to be related to one missing clamp, that was allowing a shaft to walk left and right slightly.  And I can't put one where it needs to be because my clamps are too thick. No big deal, I'll just take a little off on the mill. 



Now the pan-headed set-screw is too big. I briefly considered turning the pan-head off...then decided I'd stop by fastenal for a grub screw on the way home. 



Tolerances are pretty critical in this design, if everything held tightly in all the right planes, I'm hoping it draws correct circles...It cannot print the smallest hole on this [http://www.thingiverse.com/thing:33902](http://www.thingiverse.com/thing:33902)



We'll see how it does on the rest of it. 


---
**ThantiK** *October 13, 2014 12:24*

Now you understand why I dislike Deltas so much.


---
**Mike Miller** *October 13, 2014 12:25*

Preach it, Brother!


---
**Mike Miller** *October 13, 2014 17:16*

Maybe, it might also just be learning to work within the parameters that igus presents. The slop in my z-stage is abhorrent...but if you add a little Z-lift on move, it's <i>predictable</i> and <i>consistent</i>.


---
**Mike Miller** *October 13, 2014 17:26*

I'm just amazed and having a dimensionally accurate printer. 80mm prints at as close to 80mm as I can measure. 


---
**Jim Wilson** *October 18, 2014 20:38*

I was only able to get real accuracy after a few years on and off tweaking my i2. Now its prepping the parts for my slight variant of the ingentis. Accuracy sets you free!


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/cYRghsGr3Ld) &mdash; content and formatting may not be reliable*
