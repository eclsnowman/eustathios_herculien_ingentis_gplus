---
layout: post
title: "How did you all manage the cable routing on your frame?"
date: February 21, 2018 01:07
category: "Discussion"
author: Dennis P
---
How did you all manage the cable routing on your frame?  The ones I am trying to work out are the endstop cables that have to run horizontal then vertical to the bottom. 



Can someone give some pointers to pictures or details? I could not find much searching for 'wiring' or cable management. The best example I have found so far is +Maxime Favre printer. 



I am at a loss of how to support the heatbed cables- the thermistor wires seem  'delicate.'  Cable Chain from the top? Polypropylene wire sleeving? I am not keen on hanging them from the bed. 



Thanks in advance





**Dennis P**

---
---
**Bruce Lunde** *February 21, 2018 02:06*

I used the plastic pieces found in the guide,  they slip into the spaces in the 8020 and 2020, then tuck whatever wires I can into the slots.  I think the pictures of Eric's machine are the best to look at, since they are red they stand out well.

![images/85517b7fbb20be0f3da95062464ed5ed.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/85517b7fbb20be0f3da95062464ed5ed.jpeg)


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/ee37fbasQLf) &mdash; content and formatting may not be reliable*
