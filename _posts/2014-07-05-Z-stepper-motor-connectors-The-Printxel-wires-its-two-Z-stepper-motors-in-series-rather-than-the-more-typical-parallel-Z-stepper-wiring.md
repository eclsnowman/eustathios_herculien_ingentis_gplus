---
layout: post
title: "Z stepper motor connectors. The Printxel wires its two Z stepper motors in series rather than the more typical parallel Z stepper wiring"
date: July 05, 2014 14:39
category: "Discussion"
author: Mike Miller
---
Z stepper motor connectors.



The Printxel wires its two Z stepper motors in series rather than the more typical parallel Z stepper wiring. To do this each Z stepper is connected to the controller with only two wires. The two remaining wires on each Z stepper are then connected together.



The schematic below is when looking at the printer from the back.

![images/5a73288d6d6d998ea9f320abb3b4e2ea](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5a73288d6d6d998ea9f320abb3b4e2ea)



**Mike Miller**

---
---
**Carlton Dodd** *July 05, 2014 14:55*

All four wires to the controller should be going to the same connector.


---
**Mike Miller** *July 05, 2014 15:09*

**+Shauki Bagdadi** if anyone were going to encounter a problem it would have been you at the speeds you're attaining... Parallel is fine by me. 


---
**Liam Jackson** *July 05, 2014 16:14*

Series is better as IF you miss a step, the chances are you'll miss a step on both not just one, keeping them in line for the rest of a print. Also pretty sure the drivers prefer it so you get better temps and makes it easier to tune in the current with the 'turn it down until it skips steps, turn it up till it works, turn it up a bit more' method.



The benefits are minimal, but as its not exactly difficult to wire them either way, might as well pick serial IMO!


---
**James Rivera** *July 05, 2014 18:04*

Interesting. I didn't know you could wire the steppers in series like this or the benefits of doing so. Good post & discussion,  thanks!


---
**Lynn Roth** *July 05, 2014 23:42*

I have run serial on my z axis on my Prusa i3 for over a year with no issues.  I built a little board to do the crossover so that the motors just plug in and I don't have to think about the wiring if I move stuff around.


---
**Liam Jackson** *July 07, 2014 09:52*

The steppers are 2.8v, the drivers do constant current (that's what the pot is for). I'm pretty sure series is better for the driver. 



Two steppers will never have the same resistance, so in parallel the current will take the path of least resistance and one stepper will always be getting more current. That's why it's easier for only one stepper to skip a step in parallel. 

In series both steppers get the same current. 


---
**karabas3** *July 12, 2014 08:15*

I had bad expirience with parallel steppers on my M90. One of steppers stops rotating suddendly. I went to serial wireing. But both steppers won't run until I slow down Z speed to about 180 mm/min. Since that I dislike pairing steppers as it nonpredictable in common case. I use china RAMPS1.4 and stepsticks.


---
**karabas3** *July 12, 2014 15:07*

Therefore combinations driver+ nema17 may be different enough (maybe resistance/inductance/driver realization ). When I started to build M90 Nophead's PDF recommends parallel wireing. Now it recommends serial.


---
**karabas3** *July 12, 2014 15:35*

It links  you to certain manufacturer that simply cannot be available in other countries. Or be more expensive. When I know that any combination will work I can relax recommending it. To replicate your setup I need exact type of stepper and exact type of driver. BTW do you use one driver for both steppers as I do?


---
**James Rivera** *July 12, 2014 18:26*

**+karabas3** You are right. Page 66 says, "The Z motors are wired in series.": [https://github.com/nophead/Mendel90/blob/master/dibond/manual/Mendel90_Dibond.pdf?raw=true](https://github.com/nophead/Mendel90/blob/master/dibond/manual/Mendel90_Dibond.pdf?raw=true)



if Nophead says series, that's good enough for me. He really knows what he is doing.


---
**karabas3** *July 13, 2014 08:51*

I have a feel it is reasonable to higher voltage for serial wireing to get the same torque


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/jjVjrGPcZ6L) &mdash; content and formatting may not be reliable*
