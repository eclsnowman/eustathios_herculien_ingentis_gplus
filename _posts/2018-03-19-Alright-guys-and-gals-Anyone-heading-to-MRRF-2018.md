---
layout: post
title: "Alright guys and gals. Anyone heading to MRRF 2018"
date: March 19, 2018 23:07
category: "Discussion"
author: Eric Lien
---
Alright guys and gals. Anyone heading to MRRF 2018. I am going out. Just want to know who to be on the lookout for.





**Eric Lien**

---
---
**Zane Baird** *March 19, 2018 23:29*

I'll be there


---
**Brad Vaughan** *March 19, 2018 23:29*

I'll be there Saturday, but just walking around (no booth). I'll swing by and say, "hi."


---
**Bruce Lunde** *March 20, 2018 00:04*

Yep, I will be there 


---
**Gary Hangsleben** *March 20, 2018 00:05*

Jake and I will be there again 👍


---
**Eric Lien** *March 20, 2018 00:18*

I am trying to descide what to bring. Nothing new this year (well the adoptabot, but that is with the robotics kids now). I am thinking of traveling with just one or two this year. Setting up 5 last year took it's toll.


---
**Dennis P** *March 20, 2018 00:40*

Do you have time to print a pair of these? You never know who you might have to hide from! 

[retroplanet.com](https://www.retroplanet.com/mm5/graphics/00000006/25340_main.jpg)


---
**Lynn Roth** *March 20, 2018 01:11*

I'll be there on Saturday. Looking forward to it.


---
**Ray Kholodovsky (Cohesion3D)** *March 20, 2018 02:20*

Indeed. 


---
**Scott Hess** *March 20, 2018 03:50*

I found out about this conference AFTER signing up for a college visit with my kid for Thu-Sun.  So I am dejected :-).  I hope it is great fun!  [And if you have suggestions for a backup conference, I'm all ears.]


---
**Eric Lien** *March 20, 2018 04:14*

**+Scott Hess** fear not. There should be some great posts about the conference on the usual places. And there is always next year. Congratulations on the kid and the college visit. It will be a long time before I have to take my oldest around to colleges. I am scared to think what the cost will be by then. Likely 300k plus a kidney or two.


---
**Maxime Favre** *March 20, 2018 10:17*

One day I'll cross the pond ;)


---
**Zane Baird** *March 20, 2018 11:57*

I'll be bringing my Tria delta and brand new core XY will make its first public appearance. Unfortunately no Herculien this year as its a lot of printer to haul around.


---
**Pete LaDuke** *March 20, 2018 14:48*

Hoping to make it Sunday. 


---
**Dennis P** *March 25, 2018 01:02*

pics or it didn't happen! the rest of us have to live vicariously through you all 


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/JNWpD3x2nuV) &mdash; content and formatting may not be reliable*
