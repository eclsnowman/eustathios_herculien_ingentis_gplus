---
layout: post
title: "First Moves for my HercuLien! Connected smoothieboard up, installed pronterface, and a power supply and Boom, operational!"
date: June 15, 2015 00:57
category: "Show and Tell"
author: Bruce Lunde
---
First Moves for my HercuLien!  Connected smoothieboard up, installed pronterface, and a power supply and Boom, operational!



 
{% include youtubePlayer.html id=gS03tvHZpTs %}
[http://youtu.be/gS03tvHZpTs](http://youtu.be/gS03tvHZpTs) 



Lots of bolts and belts to tighten, and on to wiring the unit, then to get the extruder assembled.





**Bruce Lunde**

---
---
**Eric Lien** *June 15, 2015 01:27*

Great to see things moving. One thing to make sure is the cross bars are parallel with the frame and side rods. I just loosen the corner pulleys on the shaft, and the slide the cross rod to the side and put some spacer in between the side rod and the cross rod. Once you know spacing is consistent, then confirm the corner pulley is pushed up tight against the bearing and shim, then tighten down the pulley set screw.﻿


---
**Dat Chu** *June 18, 2015 18:02*

Hey Bruce have you gotten the z bed yet? 


---
**Bruce Lunde** *June 19, 2015 03:16*

 **+Dat Chu**​ I will be cutting the mdf tomorrow, I have the heater, and hope to get the aluminum next week, but have not ordered it yet.


---
**Dat Chu** *June 19, 2015 03:28*

I see. I am trying to source the mdf + aluminum plate and wants to get as much input from others as possible. Maybe there will be a group order pop up somewhere that I can join :)


---
**Bruce Lunde** *June 19, 2015 16:23*

Cannot you use your router to make the MDF?


---
**Eric Lien** *June 19, 2015 17:18*

I used a router for the perimeter shelf cut for the aluminum to sit on. Then routed the perimeter of the relief pocket for the silicon heater. The routed the relief pockets for the plug wires. Then I randomly crossed the center mass remaining with the router which helped break it up into smaller chunks. Then I used a chisel to remove the bulk of the mass in that main relief pocket. Its far less dusty than routing all that material away.


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/TaQJa2vYjY1) &mdash; content and formatting may not be reliable*
