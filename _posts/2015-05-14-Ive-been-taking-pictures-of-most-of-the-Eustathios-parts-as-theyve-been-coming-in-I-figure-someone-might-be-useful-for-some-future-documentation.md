---
layout: post
title: "I've been taking pictures of most of the Eustathios parts as they've been coming in, I figure someone might be useful for some future documentation"
date: May 14, 2015 22:06
category: "Show and Tell"
author: Walter Hsiao
---
I've been taking pictures of most of the Eustathios parts as they've been coming in, I figure someone might be useful for some future documentation.  It's not quite a stock v2 Spyder build, I switched out some parts because I'm cheap (and will probably end up regretting it), but it should be similar.



btw, I may have just missed it, but I think the Eustathios Spyder v2 BOM might be missing the 4 - M5x20mm Button Socket Cap Screws for the Z-axis shaft mount.



![images/70d42a1e5b52f5e09335fa7b5352402b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/70d42a1e5b52f5e09335fa7b5352402b.jpeg)
![images/48aea7e8cea270b417015d85bcab0988.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/48aea7e8cea270b417015d85bcab0988.jpeg)
![images/0bbbbc42c142021732dc20cdb8480d6e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0bbbbc42c142021732dc20cdb8480d6e.jpeg)
![images/5b95b15619906138313a6c24ba9c7519.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5b95b15619906138313a6c24ba9c7519.jpeg)
![images/34df1ddb88e7920445899c25d08550b8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/34df1ddb88e7920445899c25d08550b8.jpeg)
![images/86c317088ed8fb86cd6f6cb807daf353.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/86c317088ed8fb86cd6f6cb807daf353.jpeg)
![images/b33452e879ea133e2c57d5852564674d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b33452e879ea133e2c57d5852564674d.jpeg)
![images/2334d770e80fb201ab502a9a89173070.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2334d770e80fb201ab502a9a89173070.jpeg)
![images/b79f2524782f4c8ca96456a409af693c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b79f2524782f4c8ca96456a409af693c.jpeg)
![images/32bffcde808d7e5655fa85225215d198.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/32bffcde808d7e5655fa85225215d198.jpeg)
![images/1dd7477294db2bfc2dbc2b9ee9746a1c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1dd7477294db2bfc2dbc2b9ee9746a1c.jpeg)
![images/f0a58d806faa1ba72052bcb464ddc4ea.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f0a58d806faa1ba72052bcb464ddc4ea.jpeg)
![images/bc5f1e03f4d453b30784a04d198eab64.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bc5f1e03f4d453b30784a04d198eab64.jpeg)
![images/2d39d0bee0434d688d9d72b69332a2c0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2d39d0bee0434d688d9d72b69332a2c0.jpeg)
![images/772debc1410219a4b2c8859b136c7536.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/772debc1410219a4b2c8859b136c7536.jpeg)

**Walter Hsiao**

---
---
**Derek Schuetz** *May 14, 2015 22:11*

What was used to print that?


---
**Whosa whatsis** *May 14, 2015 22:12*

I like the look of that plastic. Where's it from?


---
**Walter Hsiao** *May 14, 2015 22:19*

It was printed on a RigidBot (one of the kickstarter printers).  The plastic is Hatchbox Transparent Black ABS from Amazon (the PLA is similar, but has a slight yellow cast to it and is very slightly less transparent).  I think it only comes on 1lb spools.  Part of the look comes from being printed at 100% infill.


---
**Walter Hsiao** *May 14, 2015 22:22*

Here's what the PLA version looks like when printing Aria: [http://www.thingiverse.com/make:115536](http://www.thingiverse.com/make:115536)


---
**Isaac Arciaga** *May 14, 2015 22:46*

**+Walter Hsiao**  Good looking prints. How is the rigidity on the transparent abs compared to an opaque? I almost ordered a spool yesterday. I understand in order to have transparency, quite a bit of Butadiene is removed (or not added) to the resin.


---
**James Rivera** *May 14, 2015 23:05*

Since nobody else has mentioned it, nice photography, too!


---
**Eric Lien** *May 14, 2015 23:07*

**+Walter Hsiao**​  Wow. Those are great looking prints. And the pictures are awesome. I look forward to seeing your build. Also count me jealous of the ballscrews. How's the runout on them. That's my only fear with ball screws. The ones I could afford are probably lower quality than I would desire.﻿



Also I look forward to seeing how the one piece graphite plug bushings work. If they work well I might be inclined to get a set to try them.


---
**Walter Hsiao** *May 14, 2015 23:34*

**+Isaac Arciaga**  I haven't really noticed a difference in flexibility, but I generally find transparent ABS to be much more brittle than the opaque versions, and it tends to crack under load.  So maybe not the best choice for printer parts, hopefully it will hold up.



**+James Rivera** Thanks!



**+Eric Lien** Had to look up runout, one of them looks good, the other wobbles a couple of mm on top (that one is also missing a ball that I couldn't fit back in when I took the ballnut off).  They were about $60 for the set (Golmart on Aliexpress), so I figured it was worth a gamble (plus I've wanted to play with ballscrews ever since I first heard of them).


---
**Oliver Seiler** *May 15, 2015 00:59*

Awesome photos and cool material. 3D Printing porn!


---
**Gus Montoya** *May 15, 2015 01:01*

I surely agree with how good it looks!


---
**Whosa whatsis** *May 15, 2015 01:57*

In terms of strength, I would be more worried about the fact that you appear to be printing completely hollow.


---
**Chris Brent** *May 15, 2015 02:01*

Further up the thread it says they're 100% infill...


---
**Eric Lien** *May 15, 2015 02:24*

**+Chris Brent** I was wondering about that. That's pretty great clarity for 100% fill.


---
**Whosa whatsis** *May 15, 2015 02:30*

Ah, my bad. Interesting how 0% fill and 100% fill look so similar. At 100%, I bet you went through at least one entire 1lb spool of that stuff.


---
**Walter Hsiao** *May 15, 2015 02:57*

Ha, definitely more than one, The picture doesn't include the larger z-axis pieces, I think some of those were a quarter pound each.  That filament is lightly tinted, it's very similar to clear ABS when it's hollow.


---
**Mike Kelly (Mike Make)** *May 15, 2015 15:25*

I thought I recognized that light box. Welcome to the club **+Walter Hsiao** 



Do you happen to have a picture of it? Your pictures always look so nice, want something like that


---
**Walter Hsiao** *May 15, 2015 18:21*

Thanks!  I don't have any pictures of the light tent, but it's one of the 24" folding ones, probably identical to this one: [http://www.ebay.com/itm/12-68-9-Sizes-Square-Studio-Light-Tent-Photo-Cube-Softbox-4-Color-Background-/261341357253?pt=LH_DefaultDomain_0&var=&hash=item3cd928a4c5](http://www.ebay.com/itm/12-68-9-Sizes-Square-Studio-Light-Tent-Photo-Cube-Softbox-4-Color-Background-/261341357253?pt=LH_DefaultDomain_0&var=&hash=item3cd928a4c5).  I've actually been looking for something larger, but haven't found one I like yet.  Maybe I should build a herculien with translucent white plastic :)  I also use two manual flashes and mostly photograph on white plastic or brushed aluminum.


---
*Imported from [Google+](https://plus.google.com/+WalterHsiao/posts/ZJGrqKBxyJ1) &mdash; content and formatting may not be reliable*
