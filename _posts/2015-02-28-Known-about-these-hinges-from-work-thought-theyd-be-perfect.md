---
layout: post
title: "Known about these hinges from work thought they'd be perfect"
date: February 28, 2015 02:40
category: "Show and Tell"
author: Derek Schuetz
---
Known about these hinges from work thought they'd be perfect

![images/a014462f7998839f02e5ab8a4574c762.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a014462f7998839f02e5ab8a4574c762.gif)



**Derek Schuetz**

---
---
**Eric Lien** *February 28, 2015 02:57*

Omg. I want. You don't have an extra set do you?


---
**Derek Schuetz** *February 28, 2015 03:10*

we had one box left at work ill see if i can get some more tomorow


---
**Dat Chu** *February 28, 2015 03:30*

Wow. I want one as well.


---
**Eric Lien** *February 28, 2015 03:54*

Looks like on amazon there are some similar brackets called "Delta Lid Stay Brackets", but yours look much nicer.


---
**Eric Lien** *February 28, 2015 03:56*

Also found some results on google under: klok stay brackets


---
**Derek Schuetz** *February 28, 2015 04:53*

Klok ones are them I'll see what I can scrounge from work there for discontinued items so who knows


---
**Daniel Salinas** *February 28, 2015 21:37*

I'll take a set as well please


---
**Derek Schuetz** *March 01, 2015 00:15*

I put an order for some from our spare parts department we'll see if they arrive


---
**Eric Lien** *March 01, 2015 01:11*

**+Derek Schuetz** you are the man.


---
**Eric Lien** *March 01, 2015 01:11*

Now to design a printable one for the masses.


---
**Dat Chu** *March 01, 2015 01:39*

A printable one would be useful to many many projects. 


---
**Mike Miller** *March 01, 2015 13:35*

They kinda look like an IKEA part. they're all about funky kinematics and smooth, silent, closure.


---
**Derek Schuetz** *March 01, 2015 15:13*

They are for an old ikea kitchen top cabinet we use to sell


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/EZsdEpNMBvE) &mdash; content and formatting may not be reliable*
