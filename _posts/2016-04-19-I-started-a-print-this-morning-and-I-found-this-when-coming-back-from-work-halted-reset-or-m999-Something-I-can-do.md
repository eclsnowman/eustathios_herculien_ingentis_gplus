---
layout: post
title: "I started a print this morning and I found this when coming back from work: halted reset or m999 Something I can do ?"
date: April 19, 2016 15:06
category: "Discussion"
author: Maxime Favre
---
I started a print this morning and I found this when coming back from work: halted reset or m999



Something I can do ? Gcode or smoothie related?

![images/9d1d7ed3d1c567a7232befbade089434.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9d1d7ed3d1c567a7232befbade089434.jpeg)



**Maxime Favre**

---
---
**Arthur Wolf** *April 19, 2016 15:23*

Did you enable limit switches ? Are your thermistors well connected ?


---
**Eric Lien** *April 19, 2016 15:23*

Printing from PC or PI via octoprint. I had an issue (long time ago) where I had to increase the serial delay in octoprint to avoid issues causing a hault if there were lots of fine segments were sent from a fine mesh STL sliced with simplify 3D.﻿


---
**Maxime Favre** *April 19, 2016 15:34*

Limit switches are disabled. Thermistors seem fine, the only thing I see is the molex connector on the carriage.

The printhead halted near the middle of the print surface.

I will run the gcode again without filament and see if it happens again.

edit: printed from sd card on the viki2. It was a batch of eusathios parts for a member: Z-supports


---
**Miguel Sánchez** *April 19, 2016 16:13*

If there is a bad thermistor connection and the firmware parks the head away it is not uncommon to see a good thermistor measurement just after the failure as it might be carriage position dependent.


---
**Maxime Favre** *April 19, 2016 17:26*

I ran my break-in gcode, it's that, bad hot-end thermistor connexion on the molex.

It happens not every time but got it on 4/6 run.

I will start searching for something else. 

Thanks everyone for the inputs ;)**+Ashley Webster** for what did you switch ?



Ah the good old screws :D


---
**Eric Moy** *April 21, 2016 01:19*

It's always a good idea to provide service loops for any connectors and fix the wires surrounding the connection to something, so the actual connector doesn't strain at all from the carriage motion. Hope that helps


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/LZ2p1VHoHsM) &mdash; content and formatting may not be reliable*
