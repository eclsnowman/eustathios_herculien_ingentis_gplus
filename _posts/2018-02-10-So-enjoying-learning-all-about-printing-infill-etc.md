---
layout: post
title: "So enjoying learning all about printing, infill, etc"
date: February 10, 2018 14:12
category: "Show and Tell"
author: Bruce Lunde
---
So enjoying learning all about printing, infill, etc. Loaded up a raspberry pi image OctoPi with OctoPrint, and refining settings. Here is a rPi enclosure from Thingiverse that had 8020 in mind. Getting better quality with each tweak.





![images/b50eacb4d0a88a5c0560dfcd20040e37.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b50eacb4d0a88a5c0560dfcd20040e37.jpeg)
![images/d4e1b21f5ad4019d546192c5172ef52e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d4e1b21f5ad4019d546192c5172ef52e.jpeg)
![images/c33e56d69a6ba52b9944b738803fed05.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c33e56d69a6ba52b9944b738803fed05.jpeg)

**Bruce Lunde**

---


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/5L1fccP9i7T) &mdash; content and formatting may not be reliable*
