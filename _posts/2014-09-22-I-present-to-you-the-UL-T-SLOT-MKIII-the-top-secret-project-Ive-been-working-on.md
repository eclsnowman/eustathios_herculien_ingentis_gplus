---
layout: post
title: "I present to you the UL-T-SLOT MKIII, the top secret project I've been working on"
date: September 22, 2014 19:04
category: "Show and Tell"
author: D Rob
---
I present to you the UL-T-SLOT MKIII, the top secret project I've been working on.  Still have to hook up electronics and flash firmware. And finish the  custom hotend I'm working on... (Hail Hydra!).



![images/7f84e26670c8a05029eb0cd57f428bcc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7f84e26670c8a05029eb0cd57f428bcc.jpeg)
![images/8316811a2d7fff65904e0aa565cdc92d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8316811a2d7fff65904e0aa565cdc92d.jpeg)
![images/b10d1d09207902e71e19832089dc645f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b10d1d09207902e71e19832089dc645f.jpeg)
![images/01df0f19f61ea3b28c36a5d79c6d1519.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/01df0f19f61ea3b28c36a5d79c6d1519.jpeg)
![images/a4a18e9b4a072cc227d6a4dd1e708c69.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a4a18e9b4a072cc227d6a4dd1e708c69.jpeg)
![images/ad7a03da637a3eb5908db505a184e930.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ad7a03da637a3eb5908db505a184e930.jpeg)

**D Rob**

---
---
**ThantiK** *September 22, 2014 19:17*

Woah.  Ballscrew?


---
**Miguel Sánchez** *September 22, 2014 19:24*

It seems adventurous enough. Any tests yet?


---
**Mike Miller** *September 22, 2014 19:34*

Sure looks wormy! :D


---
**Jean-Francois Couture** *September 22, 2014 19:36*

Can't wait to see some test videos on this puppy.. everything is going to be spinning around ! :)


---
**Jim Squirrel** *September 22, 2014 19:36*

that is a lot of leadscrew


---
**James Rivera** *September 22, 2014 19:43*

Screws replacing both belts and smooth rods on the gantry, but still using smooth rods (which seems wise) on the  z-axis? Expensive (I think), but interesting. Nice build!


---
**ThantiK** *September 22, 2014 19:58*

Also, what's the pitch on this?  It seems like it needs to be a very high pitch in order to work at the speeds we typically need 3D printers to run at.  Very beneficial though in terms of reduction of part count.


---
**Maxim Melcher** *September 22, 2014 20:21*

Oh, yeah, this is a SOLID work! On my printer will only the y-axis (23x46 cm heated bed, on a 20mm wider linear rail) moved with ballscrew...So many power reserves!

Your hot-end should have no problems with air resistance by high speed ;)


---
**D Rob** *September 22, 2014 20:58*

**+ThantiK** yeah they are 1204 c5 ball screw's. The set ran less than $300 for all 6﻿


---
**D Rob** *September 22, 2014 20:58*

**+Miguel Sánchez** no just finished the mechanics. Been super busy of late.


---
**Miguel Sánchez** *September 22, 2014 22:38*

**+D Rob** please keep us posted, it looks really great.


---
**Eric Lien** *September 22, 2014 22:48*

Congrats. Very cool looking. I am interested to see if using the screws for drive and rails introduces any artifacting. 


---
**D Rob** *September 22, 2014 23:57*

**+Eric Lien** I am too. One of the reasons I am trying this. These are c5 ground ball screws, not rolled, and there is no visible wobble when I travel the x or y via hand pulling the belt that keeps mirrored sides in tandem. There is one belt per axis. then the small drive belts from the x and y steppers. I wish I had some help figuring out 12v DC motors with the round optical encoders I salvaged from some 2d printers then I could bring back some speed. I also believe this should be study enough to carry a spindle (hint hint)﻿


---
**Eric Lien** *September 23, 2014 01:04*

**+D Rob** was it tough setting up the connection belt that joins the two sides? I couldn't tell how you tensioned it since I assume it has to be a closed loop belt to function.




---
**D Rob** *September 23, 2014 01:44*

**+Eric Lien** the plates were designed with belt length in mind and unlike the mk I Ul-T-Slot the corner brackets are two plates. They are more adjustable. I use a flat block on the top and sides to get them jigged into place. The small plate over laps the edge to create a stop for the larger one. And I can print them all for strength and faster print times.﻿


---
**Mike Miller** *September 23, 2014 02:59*

After he switches to 220v...


---
**D Rob** *September 23, 2014 03:20*

**+Shauki Bagdadi** 44.44 steps/mm since there is a36t gt2 on the motor and 32t on the screws. So 13332steps/sec to achieve 300mm/sec. You tell me. I believe I can attain faster speeds if I learn the arcane secrets of 12 or 24v DC motors with the disc encoders like in a 2d printer. I already have some drill motors and opto sensors with the encoder discs. Just need the right documentation. Don't have enough time to figure it myself. I'm busting my butt at a furniture store by day doing r and d for my second job at night and trying to promote my bosses kick starter for a pet toy I designed at work. Not looking good I'm kind of worried. But it is an awesome toy.


---
**Miguel Sánchez** *September 23, 2014 06:36*

**+D Rob**    if you power steppers at higher voltage (24..30V) you'll be able to achieve faster speeds just like that. DC motors require servo feedback and a different controller. Most 2d printer manufacturers use them now so I guess it can be done more cheaply, at least at large scale, but it seems not many people use DC motors in RepRap land. I do not know any cheap (as in pololu's range) electronics for this but maybe other fellas do.


---
**D Rob** *September 23, 2014 06:47*

My system is using a modified for 24v Ultimaker 1.5.7 board 


---
**Mike Miller** *September 23, 2014 15:17*

That's the reason for having ballscrews: reduction of backlash. 



For the opposite, one merely needs to look at the backlash on the Acme leadscrew on my lathe. :) Backlash there is measured in significant fractions of an inch. 


---
**Jarred Baines** *September 25, 2014 10:51*

I love it D Rob - I also planned on redesign my x y with ball screws as you have here, I think this and the servo motor idea you've been taking about will have great results!


---
**Mike Miller** *September 25, 2014 11:20*

So what would happen if the ballscrews were moved from the outsides to the inside? (other than cutting the number of ballscrews in half. :)  (yeah, yeah, how do you drive it, what happens to packaging, where do you put the motors, etc, etc. It's just a thought experiment) 


---
**Jarred Baines** *September 25, 2014 12:18*

Maintenance removed dust ;-)



I thought you would like ball screws **+Shauki Bagdadi**? Rolling vs sliding, no backlash? 


---
**D Rob** *September 25, 2014 15:24*

**+Mike Miller**  the motors would be mounted on the ball screws and have to move on at least 1 axis. A replicator style gantry would be best for that option.﻿


---
**Mike Miller** *September 25, 2014 15:25*

There's also an issue with the mass of the motor on one side...adding twist and mass...or a wierd fishingline/belt arrangement that keeps everything under tension. 



ooh. Or have all three motors (X, Y, Extruder) on the SLED! Man, that's kinky for kinky's sake. 


---
**Mike Miller** *September 25, 2014 15:42*

I'm slowly exposing you to American slang. :)  putting three motors on the sled would be an awful solution (weight, inertia). But an interesting exercise. 


---
**Dale Dunn** *September 25, 2014 18:55*

**+Shauki Bagdadi** , I don't have a lot to say. It's already built, so I'm waiting to hear how things go. I don't see an advantage to the ball screws, but there may be a goal for this machine that I'm not aware of. The mention of Hydra suggests his secret hot end may have 7 extruders, or maybe a 7 color mixing extruder. To move that mass, he might need the thrust the screws will provide. If that's the case, the round rods look a little on the thin side.



The part that does worry me is the arrangement of bearings on the platform. I'd love to hear how rigid that is at the corners. It's already overconstrained, so I'm thinking it may as well have 3 or 4 screws and no smooth rods. I think he's confident that these are straight enough for parallel applications, so I think he missed an opportunity to take advantage of that.



Hmmm. Apparently, I did have a lot to say.


---
**Tim Rastall** *September 26, 2014 00:21*

What **+Dale Dunn** said.  I'm on. printer hiatus due to home renovations, work commitments and a few furniture projects that need completing. Looks good **+D Rob**. I'd look to covert it to an all metal bot and use it for milling. Seems like a waste to just use those screws for fdm. 


---
**D Rob** *September 26, 2014 03:36*

**+Dale Dunn** no the Hydra isn't quite that elaborate. But it leans more to the removing of heads and two growing in it's place. **+Tim Rastall** has hit the nail on the head. I intend the UL-T-SLOT to be multipurpose. This is also why I am interested in high res optical rotary encoder servos. I want light 2.5D milling/routing capabilities as well as later using a laser eye protection paneling for an enclosure as well. The future of this machine will be milling, fdm,and laser etching (via piped laser or diode array) as of yet I am undecided.


---
**D Rob** *September 26, 2014 03:43*

**+Dale Dunn** yes there are 4 smooth rods on the platform. And alignment is excellent. You may notice in the pictures that there are 3 individually mounted pieces for each side of the platform. This made alignment a little easier. And the upper rod/screw clamps are the same as the lower other than modifying the .stl to also screw into the  plywood base that separates the electronics cabinet.So accurate spacing in cad was a breeze, and the accuracy of my mendel 90/i3 was sufficient to print them


---
**Dale Dunn** *September 26, 2014 12:09*

**+D Rob**  Ah, like hydraraptor, but faster. Got it. I should think you'd be able to cut acrylic and plywood fairly well.



Alignment of the platform guides is not my concern, so much as the stability offered by that arrangement of bearings and screws. Is it sufficiently rigid at the corners? Now that I know you plan to cut with it eventually, I'm even more concerned.


---
**Mike Miller** *September 26, 2014 14:08*

You would pick up a LOT of rigidity if you enclosed it in sheetmetal, screwing it to the aluminum in 50mm increments. 


---
**Mike Miller** *September 26, 2014 14:08*

That's my plan, though I'm not sure if I'll use metal or acrylic. 


---
**D Rob** *October 23, 2014 15:22*

**+Dale Dunn** this frame is very rigid. I believe Mitte than adequate for small milling jobs, like acrylic, plywood, PCB, block wood, and maybe more.


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/G5BeqWGpAuB) &mdash; content and formatting may not be reliable*
