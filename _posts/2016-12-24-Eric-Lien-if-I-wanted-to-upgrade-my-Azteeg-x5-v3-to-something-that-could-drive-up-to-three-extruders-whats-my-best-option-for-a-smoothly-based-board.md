---
layout: post
title: "Eric Lien if I wanted to upgrade my Azteeg x5 v3 to something that could drive up to three extruders, whats my best option for a smoothly based board?"
date: December 24, 2016 16:45
category: "Discussion"
author: jerryflyguy
---
**+Eric Lien** if I wanted to upgrade my Azteeg x5 v3 to something that could drive up to three extruders, whats my best option for a smoothly based board?  I know Panucatt has produced a new board (x5 GT), but its out of stock and it'll only run two extruders? 



Right now two extruders would be fine, but I'm hoping to experiment with 3 at some point. In the interest of only buying one more control board, I thought I'd ask if you knew of any other options. I really like the Panucatt board I have, esp the software controlled current settings.



I'd also like the ability to control lights and a couple more fans (more I/o) also. 



Anything out there currently fit the bill?





**jerryflyguy**

---
---
**Eric Lien** *December 24, 2016 17:01*

I think you could add external drivers to the X5 GT. There is also the Cohesion3D Remix board that does 3 extruders out of the box. The Remix runs smoothieware and has a host of MOSFETs. I think you have to compile smoothieware special for triple extruders (per a conversation with Ray the other day.



Another option if Prusa is willing to sell it separately is his 4 extruder multiplexing extruder driver board. It takes a single extruder and then selects which extruder motor to drive via the multiplexer board. Since you never extrude more than one hotend at once this is a very simple and elegant solution.


---
**Ray Kholodovsky (Cohesion3D)** *December 24, 2016 17:03*

Cohesion3D ReMix: [cohesion3d.com - Cohesion3D ReMix](http://cohesion3d.com/cohesion3d-remix/)

6 driver sockets, large bed MOSFET, 4 other medium mosfets (3 hotends and a fan) and another tiny MOSFET for fan/ led. 

I don't have it listed but I can also provide you with a little MOSFET board that would hook up to the servo header for more expansion. 

Only caveat - no software controlled current aka digipots. 

In stock and shipping as quickly as possible. 


---
**Ray Kholodovsky (Cohesion3D)** *December 24, 2016 17:06*

And yep, we like the default smoothie builds which come with max 5 axis by default, but I've already recompiled for 6 and can provide that. So it's just a quick drag in of a new firmware.bin when you are ready to upgrade to triple extruders. 


---
**Jim Stone** *December 24, 2016 17:22*

So...what have people been using to run e3d kraken then? XD



I am actually debating on getting a kraken. And putting it in the herculien. But it has to be liquid cooled.


---
**Ray Kholodovsky (Cohesion3D)** *December 24, 2016 17:24*

**+Jim Stone** well, just say the word and I'll design up a 4th (or even 5th) motor driver module for remix. The problem becomes that there is only 4 thermistor lines available. So now we'd be in spi thermocouple territory as well, as this is the only option I know of to expand the thermistor count. 


---
**jerryflyguy** *December 24, 2016 18:54*

In my case I'm hoping I can run them all into one extruder so I'm not needed extra heaters or thermistors. Just need to be able to control 3 axis motors and three extruders.


---
**Ray Kholodovsky (Cohesion3D)** *December 24, 2016 19:00*

**+jerryflyguy** so in that case, I've seen the prusa splitter board and I'm familiar with what Eric is talking about, I just haven't seen anything like that in the market. 


---
**Arthur Wolf** *December 25, 2016 09:49*

**+jerryflyguy** You can take a Smothieboard 5XC, and just wire an external stepper motor driver to it ( very easy to do, it's in the documentation. and cheap, about $10 ), and it'll run a three-extruder machine no problem.


---
**Javier Prieto** *December 25, 2016 15:44*

Or buy a duet and get the coming expansion board :)


---
**Steve White** *December 28, 2016 19:38*

Since my IRC comment was pasted in, I figured I'd clarify. Arthur has always been very helpful to me when I've asked in IRC. I was just noting that, for someone who isn't "in the know" about MKS vs Smoothie, it's a bit disconcerting to follow the Smoothieware documentation, come to IRC, and catch shit about having an MKS - when you had no history or understanding of the situation. I don't question Arthur's motivations in re: MKS, and Smoothieware is my personal favorite firmware.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/77sat5svRAB) &mdash; content and formatting may not be reliable*
