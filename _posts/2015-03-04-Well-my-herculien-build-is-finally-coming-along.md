---
layout: post
title: "Well my herculien build is finally coming along"
date: March 04, 2015 06:19
category: "Discussion"
author: Daniel Salinas
---
Well my herculien build is finally coming along. Z axis is all buttoned up and lexan side and back panels are installed. Best of all... Full documentation of each build step up to this point. Phew its taking forever but it finally is starting to resemble a printer. 

![images/10ca15f4577ffca54994045f19b93a11.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/10ca15f4577ffca54994045f19b93a11.jpeg)



**Daniel Salinas**

---
---
**Eric Lien** *March 04, 2015 12:45*

I like the accent the red extrusion adds. Looking great, and I cannot thank you enough for the documentation you are doing.


---
**Mikael Sjöberg** *March 04, 2015 14:07*

Look good !


---
**Daniel Salinas** *March 04, 2015 16:14*

Thanks! I had originally intended to do some videos for tricky parts but it was such a straight forward build I feel like the plethora of photos that I took work fine.  **+Eric Lien** will also help me out by adding more diagrams and exploded views.  One thing I learned that I can pass on to everyone is take very special care with the Z-Gantry plates.  The large holes I drilled for the delrin leadscrew nuts were just a smidge off because I had to drill them by hand and that caused all kinds of binding on the Z axis.  I added tips on leveling the Z axis for this but wanted to pass that on.


---
**Eric Lien** *March 04, 2015 16:19*

**+Daniel Salinas** I should really add a degree of freedom on that assembly. Then you could just tighten it down once aligned. 


---
**Derek Schuetz** *March 04, 2015 19:13*

Ya I still think the z gantry plates are the hardest part of this build. Hard to get under a drill press and a lot of fabrication needed for each one


---
**Eric Lien** *March 04, 2015 20:09*

**+Derek Schuetz** I did all mine with paper templates, center punches, a hand dill, and took it slow.



I need to remake the templates, I lost them.


---
**Daniel Salinas** *March 04, 2015 22:22*

Yeah, if I'm being honest I could have taken more time and gradually drilled out the large holes in those.  I think if I had it wouldn't have been so hard for me to get the z axis aligned properly.


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/N14eTkQBZzV) &mdash; content and formatting may not be reliable*
