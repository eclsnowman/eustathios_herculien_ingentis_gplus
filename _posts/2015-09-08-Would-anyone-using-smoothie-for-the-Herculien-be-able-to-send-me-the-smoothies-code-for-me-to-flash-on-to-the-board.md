---
layout: post
title: "Would anyone using smoothie for the Herculien be able to send me the smoothies code for me to flash on to the board?"
date: September 08, 2015 01:00
category: "Discussion"
author: Gunnar Meyers
---
Would anyone using smoothie for the Herculien be able to send me the smoothies code for me to flash on to the board?  I am close to finishing the printer although I have not been getting responses back when I send simple g commands. The printer can run the breakin code just fine, I just have problems when it comes to auto pid and other things. 





**Gunnar Meyers**

---
---
**Zane Baird** *September 08, 2015 01:37*

**+Gunnar Meyers** The smoothie config that I am using is here: [https://www.dropbox.com/sh/383x6ziztxt6r3m/AAAm6J2C6wdX3QlaBZAVmnaFa?dl=0](https://www.dropbox.com/sh/383x6ziztxt6r3m/AAAm6J2C6wdX3QlaBZAVmnaFa?dl=0)



However, it should be noted: You'll have to make sure you are using the same pins as I am for the bed, the hotend (I'm only using 1 currently), the cooling fan, fan for the hotend, etc.



Rather than copying what I shared onto the SD of your smoothieboard(?... the azteed X5 also runs smoothieware), I recommend you compare your current config file and make sure you understand what each is controlling. 


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/fTym1ZQpWya) &mdash; content and formatting may not be reliable*
