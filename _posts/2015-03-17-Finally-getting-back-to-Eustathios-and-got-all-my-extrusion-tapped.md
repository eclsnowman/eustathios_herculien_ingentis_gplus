---
layout: post
title: "Finally getting back to Eustathios and got all my extrusion tapped"
date: March 17, 2015 02:28
category: "Show and Tell"
author: Rick Sollie
---
Finally getting back to Eustathios and got all my extrusion tapped.

![images/7d60684a01f78c4da41c21032665fc42.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7d60684a01f78c4da41c21032665fc42.jpeg)



**Rick Sollie**

---
---
**Seth Messer** *March 17, 2015 03:06*

boom! i keep hesitating on pulling the trigger on all of the stuff from misumi


---
**Gus Montoya** *March 17, 2015 06:26*

**+Seth Messer**  Don't be scared, you may proceed. Looking at control boards at the moment. 


---
**Gus Montoya** *March 17, 2015 07:30*

**+Rick Sollie** Are you building the V2 or the original version?


---
**Rick Sollie** *March 17, 2015 10:03*

**+Gus Montoya** I will have to look at the misumi parts to make sure they haven't changed, as I have already purchased the hardware


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/3p2Pa6DRHNe) &mdash; content and formatting may not be reliable*
