---
layout: post
title: "What's everyone using to control their Herculien?"
date: April 16, 2015 16:15
category: "Discussion"
author: Gunnar Meyers
---
What's everyone using to control their Herculien?  I'm looking at the smoothie board does anyone have any input on the board?





**Gunnar Meyers**

---
---
**Eric Lien** *April 16, 2015 16:26*

Yes, use a smoothieware based solution. You will be happier than with 8bit controllers once you start pushing the speeds up. I love my azteeg x3 for robustness and quality... But for speed the 8bit ones have a low ceiling, especially on circles.


---
**Gunnar Meyers** *April 16, 2015 17:17*

Which board option would you suggest?


---
**Dat Chu** *April 16, 2015 18:11*

It seems everyone is going Smoothieboard x5 with voltage reg and glcd.


---
**Tim Rastall** *April 16, 2015 18:31*

**+Oliver Seiler**​ is using a smoothie I think. 


---
**Øystein Krog** *April 16, 2015 18:31*

There are a lot of new smoothie-compatible boards popping up now, momoinololu, sunbeam, azsm, brainboard (my fav so far).

I would get the original or an azteeg if they had driver sockets (or onboard DRV8825).

Part of the reason for going to 32-bit is being able to use 1/32 microstepping at high speeds!


---
**Tim Rastall** *April 16, 2015 18:34*

**+Øystein Krog**​ azteeg x5 uses on board dvr8825. ﻿

[http://www.panucatt.com/mobile/Product.aspx?id=37857](http://www.panucatt.com/mobile/Product.aspx?id=37857)


---
**Øystein Krog** *April 16, 2015 18:35*

**+Tim Rastall** Oh man, how did I miss that :p


---
**Oliver Seiler** *April 16, 2015 19:11*

I've got a smoothieboard x5, but can't really comment much as my Eustathios isn't printing yet. But from what I can see so far (basic moves, etc.) I'm happy with it.


---
**Eric Lien** *April 16, 2015 19:12*

**+Tim Rastall** only problem is X5 mini is single extruder and not enough fan and LED outputs... But Roy has some fun stuff in the works for multiple extruder controllers.


---
**Jo Miller** *April 16, 2015 19:33*

I just went for the azsm with DRV8825 , mainly because Panacut did´nt respond on @  and robotseed europe were sold out.   (and 59 $   :-) )

sort of an experiment I know, but I m going for the single extruder  solution anyway  and that AZSM should be just enough for that



anyone familiar with the Sunbeam 2.0  board ?  ist also smoothie-compatible


---
**Chris Brent** *April 18, 2015 15:57*

I thought the x5 could do dual extruders. [http://smoothieware.org/3d-printer-guide](http://smoothieware.org/3d-printer-guide)


---
**Eric Lien** *April 18, 2015 15:58*

**+Chris Brent** x5 mini by azteeg cannot.


---
**Chris Brent** *April 18, 2015 17:06*

Ah I see the x5 mini is the the 5x Smoothie! [http://shop.uberclock.com/collections/smoothie/products/smoothieboard](http://shop.uberclock.com/collections/smoothie/products/smoothieboard) 


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/fzu7TKFjd4A) &mdash; content and formatting may not be reliable*
