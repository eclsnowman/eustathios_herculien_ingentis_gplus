---
layout: post
title: "All that work putting it together and now its back in pieces"
date: February 20, 2015 14:58
category: "Discussion"
author: Daniel Salinas
---
All that work putting it together and now its back in pieces. :). I'm not a great writer but I'm going to do a series of docs on assembling the herculien and would appreciate anyone else who has one or building one to double check my steps. 

![images/dd4acb74c7079a0229bcd9ba0192bf3f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dd4acb74c7079a0229bcd9ba0192bf3f.jpeg)



**Daniel Salinas**

---
---
**Jim Wilson** *February 20, 2015 15:24*

I'll be cresting something similar for the Eustathios! What do you think the best medium would be? Just host it on a blog somewhere, a github with pdfs, google doc?


---
**Jason Perkes** *February 20, 2015 15:43*

Writing a build manual is a Herculien task :) BoM chickawowoww


---
**Derek Schuetz** *February 20, 2015 15:49*

**+Daniel Salinas** I'll look at it for you since it'll be fresh in my head 


---
**Bruce Lunde** *February 20, 2015 16:01*

**+Daniel Salinas** Happy to collaborate with you, my parts are on the way, and I should be building sometime after the 24th!


---
**Dat Chu** *February 20, 2015 16:17*

Just waiting for my extrusions to be done by my local supplier. Then it's Herculien time.


---
**Seth Messer** *February 20, 2015 16:30*

**+Jim Wilson** markdown via github is super convenient, but then again, google docs are too; especially if you don't want to go the nerdy code route :)



alternatively (kind of), you can go the github pages route and host a mini blog and still be able to write markdown: [https://pages.github.com/](https://pages.github.com/)


---
**Eric Lien** *February 20, 2015 16:40*

Something with the ability to embed video (even just YouTube links) would be a nice medium.


---
**Eric Lien** *February 20, 2015 16:40*

BTW **+Daniel Salinas**​, you are my hero.


---
**Daniel Salinas** *February 20, 2015 19:09*

It will definitely be in markdown pages and submitted as PRs to the Herculien github repo.  I'm a software developer by trade so I prefer to keep that stuff in github/markdown.  Easy to do version control and keep track of edits.


---
**Seth Messer** *February 20, 2015 19:37*

**+Daniel Salinas** you sir, make me proud. :D would be happy to help with any PRs you need reviewed or anything. of course, that'd only be for readability/normal spelling/grammer mistakes as i don't have parts for a herculien. but i'd still be happy to help wherever i can. just want to pitch in/participate while waiting on printed parts.


---
**Tim Rastall** *February 20, 2015 21:49*

Is there a reason why people arent considering instructibles? Not a leading question,  I'm just thinking github isn't super accessible for the layman. 


---
**Jim Wilson** *February 20, 2015 21:51*

I hadn't thought of it, I think I'll do the initial layout as a google doc and move it to something more reasonable like instructables after its gone through a few test users


---
**Seth Messer** *February 20, 2015 21:58*

**+Tim Rastall** i'd think maybe a final version would be fine for instructables, but the drafts would be better in a community editable format (google docs/github/etc)


---
**Len Trigg** *February 20, 2015 22:58*

Why not put it on the reprap wiki?


---
**Daniel Salinas** *February 20, 2015 22:58*

**+Tim Rastall**​ I intend to develop the manual in markdown and keep track of it via github. The manual is easily accessible to any one by just going to the github page for the herculien. You don't need to use git to view it. That being said anyone in the community is welcome to maintain a published copy at instructables if they want once I'm done. I will do as much of the manual as I can but I currently work for a startup so my time is limited. 


---
**Daniel Salinas** *February 20, 2015 23:04*

**+Len Trigg**​ good question. 1. I detest wiki's. (They're almost always horrible to deal with)  2. Its not my printer :-)   If **+Eric Lien**​ wants to get it put up on the reprap wiki he can. I just wanted to give back to him for putting together an awesome printer by at least getting a build doc started and contribute design feedback where I can.


---
**Daniel Salinas** *February 20, 2015 23:24*

Oh and **+Tim Rastall** I am not at all opposed to instructables in any way.  I'm just very comfortable with github/markdown so I'd prefer to do my writing where I'm most efficient.  I'm sure if there is a need for it the community can find or write a tool to convert the doc to be friendly to that or any site.  AFAIK there are already tools to convert markdown to PDF so we could easily publish that to begin with.


---
**Bruce Lunde** *March 04, 2015 05:11*

**+Daniel Salinas**, using your assembly guide tonight, you could add a part on tapping- I used one of the printed drill guides to drill a piece of 3/4 inch wood to make a tap guide. I have a picture if you think this would be useful to others. Kept my tap straight when doing all the 8020 ends.


---
**Daniel Salinas** *March 04, 2015 06:21*

I have a section up near the top that covers tapping along with a YouTube video showing someone tapping an extrusion. **+Bruce Lunde**​


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/bHgfhTx43o6) &mdash; content and formatting may not be reliable*
