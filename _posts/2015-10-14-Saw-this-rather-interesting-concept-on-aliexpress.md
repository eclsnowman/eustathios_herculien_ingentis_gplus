---
layout: post
title: "Saw this rather interesting concept on aliexpress"
date: October 14, 2015 10:23
category: "Deviations from Norm"
author: Eirikur Sigbjörnsson
---
Saw this rather interesting concept on aliexpress. This is made for the 6mm rods on the Ultimaker2, but I wonder if this could be implemented on some of the designs here. And then there is the question of how good this is



[http://www.aliexpress.com/item/Ultimaker-2-3D-printer-E3D-V6-all-metal-print-head-warm-end-3-mm-kit-Supplies/32461000560.html?spm=2114.01020208.3.222.PCifEA&ws_ab_test=searchweb201556_2,searchweb201527_1_71_72_73_74_75,searchweb201560_1](http://www.aliexpress.com/item/Ultimaker-2-3D-printer-E3D-V6-all-metal-print-head-warm-end-3-mm-kit-Supplies/32461000560.html?spm=2114.01020208.3.222.PCifEA&ws_ab_test=searchweb201556_2,searchweb201527_1_71_72_73_74_75,searchweb201560_1)





**Eirikur Sigbjörnsson**

---
---
**Daniel F** *October 14, 2015 10:57*

This could help with my eustathios, as I still have issues with smooth movements (not sure if my carriage is prinetd orthogonal in all directios). What is the distance between the crossbars for an Ultimaker 2?

8x12x30 mm bushings like these [http://www.robotdigg.com/product/175](http://www.robotdigg.com/product/175) could fit, as the od of 8mm bushing fits the od of the 6mm linear bearing (12mm)


---
**Eirikur Sigbjörnsson** *October 14, 2015 11:12*

Quick measurements tells me the distance between the crossbar (middle of hole to middle of hole) is 15mm, I need to look up the specs to be 100% sure though. I belive there are other sellers on Aliexpress selling this exact same system so maybe more digging can reveal more info and other prices


---
**Daniel F** *October 14, 2015 11:55*

I have to check how much it is for Eustathios, I think it is 14mm but I'm not sure


---
**Mike Miller** *October 14, 2015 12:35*

That's one of the reasons I separated X and Y axes on my printers...then the distance between X and Y bearings is irrelevant. 


---
**Joe Spanier** *October 14, 2015 13:59*

I wish you didnt have to waste money on the counterfeit e3d to buy it.  


---
**Mike Miller** *October 14, 2015 14:05*

I wasn't sure it included the hot-end...it might just be the mount and bearings. 


---
**Bud Hammerton** *October 14, 2015 17:25*

Why not duplicate it, something like this:

[https://drive.google.com/file/d/0ByUVV3BSj0O6bXk1Yk0xSDZ4cUk/view?usp=sharing](https://drive.google.com/file/d/0ByUVV3BSj0O6bXk1Yk0xSDZ4cUk/view?usp=sharing)

[https://drive.google.com/file/d/0ByUVV3BSj0O6VVJhNlJBY1QyQzQ/view?usp=sharing](https://drive.google.com/file/d/0ByUVV3BSj0O6VVJhNlJBY1QyQzQ/view?usp=sharing)

[https://drive.google.com/file/d/0ByUVV3BSj0O6OVVHemFoNkl1Unc/view?usp=sharing](https://drive.google.com/file/d/0ByUVV3BSj0O6OVVHemFoNkl1Unc/view?usp=sharing)



I didn't model any parts fan cooling ducts, as this was only a test mockup. I have the STEP files if anyone want to run with this.



By the way, this uses the 8x12x30 graphite/bronze bearings from Robotdigg, among other places. Also uses M5 and M3 heat set brass inserts from McMaster.


---
**Frank “Helmi” Helmschrott** *October 15, 2015 06:03*

Mabye i just don't get it - what's the big difference of carriages we already have and to the "official" one from Eric? I'm also working on one but basically it is following most of the same concepts than this or the one from Eric - i'm just changing a few smaller things. Maybe i'm just missing the point why this could be any better or have any big differences in concept!?


---
**Bud Hammerton** *October 15, 2015 13:03*

**+Frank Helmschrott**  Everything in design is a trade off; ease of use vs. functionality vs. cost of manufacture vs. ease of manufacture. You get the idea. I think all these designs are available to make small improvements in one or more of these areas. In my particular case, I found it difficult to redesign the existing parts fan for the Volcano, So I wanted something less complex. I also didn't want to be limited to a single parts cooling fan. I wanted a better way to solidly mount the Hot End. So a new design for the carriage was easier than a redesign of what **+Eric Lien** did. It isn't necessarily better, just a different approach to solving similar issues.


---
**Eric Lien** *October 15, 2015 13:44*

**+Bud Hammerton** all iterations are great. I love the variety and creativity most about this group.



BTW I have the part cooling fan done for volcano. I made it for **+Jason Smith**​ a while ago. I will try and find it to add it to the github. And I agree the cooling fan could be improved, I just find an even and wide air distribution of air seems to give me a better cooling effect than blasting at the nozzle with two fans. That way it not only cools the plastic just existing the nozzle... But also the surrounding area. But I will admit, bridging is much harder without a thin blasting air stream right at the nozzle. Also I primarily ABS and PETG, so rarely use much PLA which benefits most from active cooling.


---
**Bud Hammerton** *October 15, 2015 14:18*

**+Eric Lien** I hope you didn't take my response as a criticism of your design. I actually love it. Just trying to show **+Frank Helmschrott** why there are different solutions. I would love the Volcano duct. It just seems like such a logical extruder for the Herc and Eustathios. Didn't understand why more people don't do it.



I may print this carriage out anyway, even though I have yours already done, sitting here in my box of parts. I like the suggestion by others about using a single 8x12x30 graphite/bronze bearing rather than two of the self aligning ones. 



That was about an hours work in Fusion 360, still may need a little touch up. Won't know unless I print it.


---
**Eric Lien** *October 15, 2015 15:16*

**+Bud Hammerton** non taken. I welcome criticism even more than praise. And I figure the more full featured our libe-up of options the more we all benefit.


---
**Eric Lien** *October 15, 2015 15:18*

Also it looks like I did add the duct to the github already: [https://github.com/eclsnowman/Eustathios-Spider-V2/blob/master/STL%20Print%20FIles/Carriage_V4_Part_Cooling_Duct%20(E3D_Volcano_Version).STL](https://github.com/eclsnowman/Eustathios-Spider-V2/blob/master/STL%20Print%20FIles/Carriage_V4_Part_Cooling_Duct%20(E3D_Volcano_Version).STL)


---
**Bud Hammerton** *October 19, 2015 14:28*

**+Eric Lien** Thanks, was able to download and am printing one today. Don't know why I didn't see it the first time around.


---
**Eric Lien** *October 19, 2015 14:35*

**+Bud Hammerton** glad to help. To be honest I need to do some cleanup on the github... But time has not been my friend lately. Too many irons in the fire.


---
*Imported from [Google+](https://plus.google.com/118262882256504121671/posts/ZS9gEBNh9W2) &mdash; content and formatting may not be reliable*
