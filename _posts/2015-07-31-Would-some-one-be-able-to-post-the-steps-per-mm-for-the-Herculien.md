---
layout: post
title: "Would some one be able to post the steps per mm for the Herculien?"
date: July 31, 2015 01:05
category: "Discussion"
author: Gunnar Meyers
---
Would some one be able to post the steps per mm for the Herculien? Also if anyone has the acceleration figured out it would be a great help and it would save me a lot of time. Hopefully I will be printing on Sunday or Monday if everything goes well. 





**Gunnar Meyers**

---
---
**Eric Lien** *July 31, 2015 01:12*

If you pull the config.h from the Marlin firmware folder on the github it will all be in there. But remember steps/mm will depend on what microstepping you choose.


---
**Eric Lien** *August 05, 2015 00:20*

Were you ever able to figure this out? if not let me know.


---
**Gunnar Meyers** *August 05, 2015 03:08*

I think so. I will know for sure soon. 


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/ML3xExocpo7) &mdash; content and formatting may not be reliable*
