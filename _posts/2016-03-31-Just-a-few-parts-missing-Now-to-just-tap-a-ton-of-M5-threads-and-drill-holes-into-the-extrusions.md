---
layout: post
title: "Just a few parts missing! Now to just tap a ton of M5 threads and drill holes into the extrusions"
date: March 31, 2016 20:46
category: "Build Logs"
author: Zachary Tonsmeire
---
Just a few parts missing! Now to just tap a ton of M5 threads and drill holes into the extrusions. Can't wait to start building the darn thing this weekend! I've been lurking on this sub for a while and finally pulled the trigger. Such a great community can't wait to be a part of it! 

![images/f67f2687b43c2716daca163898e32e30.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f67f2687b43c2716daca163898e32e30.jpeg)



**Zachary Tonsmeire**

---
---
**Eric Lien** *March 31, 2016 21:03*

Very excited to see your progress, and make sure to ask questions when you run into anything that doesn't make sense. We have some of the best groups of minds in this community IMHO.


---
**Mike Miller** *March 31, 2016 21:04*

Next time (if there is a next time) I think I'll pay to have the ends tapped before they're shipped to me. 


---
**Ray Kholodovsky (Cohesion3D)** *March 31, 2016 21:11*

The combo tap+drill bits like available at Harbor Freight in the USA are phenomenal for end tapping. Make sure to use a lubricant - canola oil is actually recommend to me for this because it won't ruin the surface of the aluminum like some cutting fluids might. And it works great! You'll break those bits (inside the channel) if you try to do it dry. 


---
**Stephen Baird** *April 01, 2016 02:32*

So where does the Han Solo blaster go in the printer?


---
**Zachary Tonsmeire** *April 01, 2016 03:30*

**+Mike Miller** I defiantly will the next time haha I graduate in May and funds have slowly been running out this last semester

**+Ray Kholodovsky** Thanks for the advice man I defiantly will do that when I am taping the threads and everything. I have that exact Harbor Freight set just need some canola oil now! 

**+Stephen Baird** it's the most important part! Your 3D printer doesn't have its own Han Solo Blaster?! It was my painting table and now it has been converted into my HercuLien building table... I probably should build another table ﻿


---
**Eric Lien** *April 01, 2016 03:51*

**+Zachary Tonsmeire** one thing when you build the frame make sure you're building it on a very flat stable table before you tighten everything up. And make sure to run around the square everywhere. Large equipment is more sensitive to being mildly out of alignment because the lengths are long enough to generate larger errors across the large spans.


---
**Zachary Tonsmeire** *April 01, 2016 04:06*

**+Eric Lien** My fold out table is probably not stable not flat haha. I will defiantly find a better assembly location. Just reading through other peoples builds and how the gantry system works I could defiantly see this as being the most important part to the build. Don't want to force the rods and brushings to move at non right angles causing them bind up/damaged. Thanks for the advice Eric! 


---
**Botio Kuo** *April 11, 2016 03:14*

Hi, Zachary. Could u tell me where did you got the Aluminum Heat Spreader Build Plate? Is there in CA ? Thanks


---
**Zachary Tonsmeire** *April 11, 2016 05:37*

**+Botio Kuo** I deviated from the original design and am attempting to use an 1/8" thick piece of aluminum.  Hopefully with a sheet of glass on top it wont warp too much.  I had a friend cut it out with his aluminum circular saw.  


---
*Imported from [Google+](https://plus.google.com/107266994009434505763/posts/gz9khmsxfoZ) &mdash; content and formatting may not be reliable*
