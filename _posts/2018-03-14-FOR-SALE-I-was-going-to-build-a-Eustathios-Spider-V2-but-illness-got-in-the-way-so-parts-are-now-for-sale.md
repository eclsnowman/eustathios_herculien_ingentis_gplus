---
layout: post
title: "FOR SALE: I was going to build a Eustathios Spider V2 but illness got in the way so parts are now for sale"
date: March 14, 2018 02:33
category: "Discussion"
author: Daithí Ó Corráin
---
FOR SALE: I was going to build a Eustathios Spider V2 but illness got in the way so parts are now for sale. No description (sorry no time), check the pics. NO MINIMUM PRICE (but it has to be worth my while). I'm not selling bits n pieces individually, preferably the whole thing as a lot. Moving house soon so if it doesn't go it's going to charity shop. Location Australia so buyer pays postage. $15USD registered. PayPal only. For sale until paid through PayPal . Thanks!



![images/f059686c20f0ec5980931cf7f0615aef.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f059686c20f0ec5980931cf7f0615aef.jpeg)
![images/317df89f5a8b4f37394054e937f4dfac.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/317df89f5a8b4f37394054e937f4dfac.jpeg)
![images/70b4e6658a391e280126a4eb00d212c6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/70b4e6658a391e280126a4eb00d212c6.jpeg)
![images/9b48536a021ae2373bec960d19c9b40e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9b48536a021ae2373bec960d19c9b40e.jpeg)
![images/88cdd9a4c45e3d0ebb5e522e16b88aa8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/88cdd9a4c45e3d0ebb5e522e16b88aa8.jpeg)
![images/f7f9e40e79bc740ba6f35a51715a4a0c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f7f9e40e79bc740ba6f35a51715a4a0c.jpeg)
![images/dea7e12d7fe7b71043e49ba36fafbc62.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dea7e12d7fe7b71043e49ba36fafbc62.jpeg)

**Daithí Ó Corráin**

---
---
**Daithí Ó Corráin** *March 17, 2018 00:43*

No takers? Going to charity shop in 24 hours if no one wants it...




---
**Stefano Pagani (Stef_FPV)** *June 04, 2018 19:01*

Still have it?? I kinda want to build another now.


---
*Imported from [Google+](https://plus.google.com/104923267981938346235/posts/BfrqzaDvTzM) &mdash; content and formatting may not be reliable*
