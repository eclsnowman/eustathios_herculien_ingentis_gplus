---
layout: post
title: "Got past the belt grinding noise and the vibrating...."
date: July 28, 2015 00:00
category: "Discussion"
author: Brandon Cramer
---
Got past the belt grinding noise and the vibrating.... Does this look like it's working well? 



Should I let it run through the entire break in gcode? 



Panucatt is sending me the X5 mini that I ordered. The one they sent me has the vertical connectors only. Can't wait to get it. This other X5 mini is a little difficult to say the least. 

![images/eaf6acf4df7dfb9dcabf99f8cce58262.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eaf6acf4df7dfb9dcabf99f8cce58262.gif)



**Brandon Cramer**

---
---
**Eric Lien** *July 28, 2015 00:12*

Looks good to me. Yes I would run it, multiple times. If you want for initial break in you can put some light oil on the rods during break-in. After that you should be good. By light I mean like a sewing machine oil, or 3-in-one. Then wipe down the rods when done. Also you can turn up your speed multiplier on the second or third run to determine your top speeds, or to determine any additional tuning required.﻿


---
**Bryan Weaver** *July 28, 2015 00:26*

Was it an alignment issue after all? I was cringing pretty hard at that grinding noise..


---
**Brandon Cramer** *July 28, 2015 01:04*

I tore it all apart and moved the carriage to every corner and readjusted. I think that the rod that goes across the middle might have been causing some issues by pushing the sliding pieces out of whack. I also have never had the belt in the perfect position like it is now. When I could take the carriage and slide it all over the place with one hand I knew I was set. :)


---
**Eric Lien** *July 28, 2015 01:24*

**+Brandon Cramer** yup... All I can tell people is when its right You Will Know.


---
**Gus Montoya** *July 28, 2015 01:34*

**+Brandon Cramer** 

Now your encouraging me to finish my build! I'll be done with my extrance exam tomorrow. Then final app on Wednesday. Wednesday I hope to have some movement. Still need to get my end stops ( i lost the ones I already bought :(   ).


---
**Brandon Cramer** *July 28, 2015 03:48*

**+Gus Montoya** That's funny. This has definitely taken me some time to get this far. I'm excited to get the correct X5 mini so I can finish the wiring.  I hope it arrives soon. 



**+Eric Lien**  I will have to spend sometime running the break in gcode. My coworkers love hearing that white noise as my printer runs.... So just use some 3 in 1 oil and run the break in code a bunch of times. After that I don't need any oil at all?




---
**Eric Lien** *July 28, 2015 04:08*

I haven't oiled mine since. And they print often. The lead screws on the other hand like a fresh dose of this from time to time: [http://www.amazon.com/gp/aw/d/B000XBH9HI/ref=mp_s_a_1_1?qid=1438056489&sr=8-1&pi=AC_SY200_QL40&keywords=super+lube+teflon&dpPl=1&dpID=31-xhQ8JfAL&ref=plSrch](http://www.amazon.com/gp/aw/d/B000XBH9HI/ref=mp_s_a_1_1?qid=1438056489&sr=8-1&pi=AC_SY200_QL40&keywords=super+lube+teflon&dpPl=1&dpID=31-xhQ8JfAL&ref=plSrch)


---
**Mike Thornbury** *July 28, 2015 07:49*

I've been using this: [http://www.amazon.com/Shibari-Intimate-Lubricant-Water-Bottle/dp/B00G5K7L24/ref=sr_1_3?ie=UTF8&qid=1438069748&sr=8-3](http://www.amazon.com/Shibari-Intimate-Lubricant-Water-Bottle/dp/B00G5K7L24/ref=sr_1_3?ie=UTF8&qid=1438069748&sr=8-3)



Everything just seems to get... stiffer ;)


---
**Eric Lien** *July 28, 2015 11:25*

**+Mike Thornbury** That made me chuckle. Thank you sir.


---
**Brandon Cramer** *July 28, 2015 17:59*

If you are using a raspberry pi camera, are you using a wide angle lens? How long of a cable do I need? I can purchase a 2 meter cable on Amazon. 


---
**Eric Lien** *July 28, 2015 18:40*

I am using a C270 logitec webcam. They are cheap if you shop for them, and available almost everywhere. They also do very good internal exposure adjustment. It is easy to remote mount due to the long cable and is plug and play with Octoprint.



Only trick is you will want to pop off the cover and manually adjust the focal ring inward. It is usually not designed to focus on something only inches away. But if you make this adjustment it will be very crisp (there are lots of online tutorials for this hack on the C270.



I also glued this to the front of the web cam. Makes it super easy to get the full bed pictured in the frame: [http://www.amazon.com/gp/product/B005C3CSXC?psc=1&redirect=true&ref_=oh_aui_search_detailpage](http://www.amazon.com/gp/product/B005C3CSXC?psc=1&redirect=true&ref_=oh_aui_search_detailpage)



Best of luck.


---
**Brandon Cramer** *July 28, 2015 18:47*

Thanks. I will order one of those webcam's. I had already ordered that lens on Amazon. I just need to see if I can find it. 



What are you using to power the raspberry pi? I want to wire it off the existing power supply. 


---
**Eric Lien** *July 28, 2015 20:33*

I used two wires off the 120V lines (I just tied in at the 120V inlet to the power supply). On the end of the wires I used covered female spade connectors. If you get the right size ones they can fit over the prongs on a 120V plug prong. Then I just put several layers of heat shrink over them since it is mains power, and hot glued the power brick to the underside of the printer. Then just use a standard USB to Micro-USB cable into the PI. Just make sure the USB power brick is 2A or more.


---
**Brandon Cramer** *July 29, 2015 20:49*

I've been running the break in gcode all morning. I used a little 3-IN-ONE oil on it and this thing has gotten quiet. I love it! 


---
**Brandon Cramer** *July 29, 2015 21:20*

**+Eric Lien** "Also you can turn up your speed multiplier on the second or third run to determine your top speeds, or to determine any additional tuning required."



How do I go about doing this?


---
**Eric Lien** *July 29, 2015 22:27*

**+Brandon Cramer** as a reference I can run the break-in code with a multiplier at 200% (300mm/s) with no grind or slip. As you use your machine you will get to know it better. At that point you can tune your machine to go faster.



You currently are not seeing missed steps at the 150mm/s in the break-in code. Know that your gantry is never perfectly aligned. But it is currently good enough that 150mm/s is no problem. Perfection is not attainable, but closer to perfect is with experience which allows higher speeds.


---
**Brandon Cramer** *July 29, 2015 23:30*

**+Eric Lien** Sorry... How do I set it to 300mm/s? Not sure where I set this in Octopi or elsewhere. 


---
**Eric Lien** *July 29, 2015 23:44*

**+Brandon Cramer** It is under the control tab: [http://i.imgur.com/n5dAmPO.png](http://i.imgur.com/n5dAmPO.png)


---
**Brandon Cramer** *July 29, 2015 23:52*

So set it to 150% and hold on.... :)


---
**Eric Lien** *July 30, 2015 00:09*

**+Brandon Cramer** why not try 10% at a time for starters ;)


---
**Brandon Cramer** *July 30, 2015 00:13*

I set it to 150%. Does this look like it's operating at 150%?



[https://www.dropbox.com/photos/shared_space/MDSVhrfcvHJzWH9](https://www.dropbox.com/photos/shared_space/MDSVhrfcvHJzWH9)






---
**Eric Lien** *July 30, 2015 00:34*

**+Brandon Cramer** did you click the button to set the value after adjusting the slider?


---
**Brandon Cramer** *July 30, 2015 00:35*

Not sure. I will check it tomorrow. It does seem to be running faster. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/JRdGe5U9xxY) &mdash; content and formatting may not be reliable*
