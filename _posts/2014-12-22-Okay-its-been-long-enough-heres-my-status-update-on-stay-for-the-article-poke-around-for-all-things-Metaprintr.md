---
layout: post
title: "Okay, it's been long enough, here's my status update on , stay for the article, poke around for all things Metaprintr"
date: December 22, 2014 02:31
category: "Show and Tell"
author: Mike Miller
---
Okay, it's been long enough, here's my status update on #IGentUS, stay for the article, poke around for all things Metaprintr.





**Mike Miller**

---
---
**James Rivera** *December 22, 2014 03:58*

NICE!  My first reaction was, "gee, those belts going from the bottom to the top sure are long..." but after reading your design constraints, I understand the benefit: all the motor wiring is in the bottom--sweet!


---
**Mike Miller** *December 22, 2014 04:10*

Also, wood is a great sound dampener. I insulated one motor with cork and it didn't make a bit of difference. You make a rigid metal cube for a printer, you're also kinda making a tuning fork. 


---
**James Rivera** *December 22, 2014 06:57*

You know, I'm looking a little closer at this, and realizing this frame may be <b>exactly</b> what I want to do with the extrusions I already have! Just space them like you have.  I do still need to order a few more pieces for the build plate support, but I need to order gantry smooth rods, bearings, and pulleys anyway.  A fully enclosed case (possibly with filtration) is something I would like, too.


---
**Mike Thornbury** *December 22, 2014 21:11*

Hi **+Mike Miller**, I also am building a printer using Igus sliding parts. One question though, why didn't you press-fit the bushes?




---
**Mike Miller** *December 22, 2014 21:13*

The ones on the v1 carriage were pressfit... The other parts were printed for me by a long-distance friend, my delta and I at the time weren't on speaking terms.


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/bRjECotun4w) &mdash; content and formatting may not be reliable*
