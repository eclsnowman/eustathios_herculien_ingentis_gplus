---
layout: post
title: "120v bed assembly installed. I will wire it up for the maiden voyage tomorrow"
date: October 30, 2014 05:13
category: "Show and Tell"
author: Eric Lien
---
120v bed assembly installed. I will wire it up for the maiden voyage tomorrow. I have found my wiring skills drop exponentially after midnight. Or I should say my fire starting skills go up.

![images/826481029abd759a9ab3812fc16513cd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/826481029abd759a9ab3812fc16513cd.jpeg)



**Eric Lien**

---
---
**James Rivera** *October 30, 2014 06:38*

She's purty!


---
**Thomas Sanladerer** *October 30, 2014 10:33*

I'd be afraid that the MDF base would warp from the heat.


---
**Jean-Francois Couture** *October 30, 2014 11:46*

**+Eric Lien** Did you crank up the vRef on the stepper driver ? that looks like a pretty heavy setup. And are you going to print directly on the aluminium or use a piece of glass ? 


---
**Eric Lien** *October 30, 2014 12:42*

**+Jean-Francois Couture** I will have glass on top. The real weight is the 1/4" aluminum. The MDF has very little weight (it is almost entirely hollow from being routed out to countersink the bed and heat pad.



I have a high torque 60mm long nema 17 driving the bed. There has never been an issue yet. The gear reduction from the pullies and lead screws make simple work of that.


---
**Eric Lien** *October 30, 2014 12:47*

**+Thomas Sanladerer** the MDF is only an insulator. Bed leveling is completely divorced from the MDF so I hope even if it tried to warp there would be no net consequence other than an air gap. The MDF is sealed with 500F exhaust manifold enamel, then several layers of Kapton tape. I hope that helps. Time will tell.



Up till now I insulated the old bed with 3 layers of 1/4" cork followed by 1/4" fiber hardboard. It has performed well for almost 1 year.﻿


---
**Jean-Francois Couture** *October 30, 2014 13:29*

I have my pad sticking under a 3mm (1/8") aliminium plate and that plate has no insulation what so ever under. I have no problems printing ABS with hiarspray + 100C bed. Insulating is a added bonus so i'm not worried that it wont perform badly :-)


---
**Eric Lien** *October 31, 2014 03:48*

New heated bed with SSR works great. 110C in about 3.5 minutes (not bad with a 1/4" thick chunk of aluminum for a heat spreader).



Based on thermocouple testing via my multimeter and also via my IR thermometer it takes about another 2-3 minutes before the glass top equalizes to the same temp. But all in all I couldn't be happier. Temperature is rock stable after some PID tuning.



Now it is time to cleanup the wiring (and to be honest also cleanup the office from all the tear down work recently).



One note is I had to drop the "Max Bed Power" in marlin from 255 to 220 to avoid overshoot and PID overtemp failure. So (800W/255) x 220 = 690W. I think that means a 700W bed would have likely been the sweet spot.



If anyone wants my old 400W 24V bed hit me up. It is around 420mm x 430mm. It will come with the aluminum heat spreader, cork insulation, and everything. I am in Minnesota. If you are local you can pick it up free of charge. If shipping is required we can figure something out. 


---
**Jean-Francois Couture** *October 31, 2014 12:05*

Next time I order one, i'm asking that they put a small hole in the middle so I can pass the thermistor directly to the glass (also drilling the aluminium plate between)



Great job.. hope it gives out good prints. :)


---
**Eric Lien** *October 31, 2014 12:34*

**+Jean-Francois Couture** I have a 12hr print going since last night. It is working perfectly so far. The 1/4" heat spreader is far better at maintaining flatness across the bed. My 1/8" one had far more of a bowing issue at the corners by the mounting bolts/springs.


---
**Jean-Francois Couture** *October 31, 2014 13:11*

I did try to get a 300x300 x 1/4" for mine but the prices here are outrageous.. more that 60$ for the bed alone with a 100$ minimum purchase. that why I got a 1/8, it was only 23$



looking at the picture, how did you make the glass stick on the bed ?


---
**Eric Lien** *October 31, 2014 15:01*

**+Jean-Francois Couture** right now its just painters taped down which works fine. But long term I will design spring loaded clips that swing out of the way at the corners.


---
**Mike Thornbury** *November 09, 2014 15:08*

If it was me, I wouldn't even stick it down - the forces on the bed are entirely vertical. Where's it going to go?


---
**Eric Lien** *November 09, 2014 17:49*

**+Zut Alors** nozzle catches could slide the plate.


---
**Mike Thornbury** *November 10, 2014 04:35*

And so a well-fitted plate would overcome those. Warm ABS vs hardwood? I like the idea of a plate that I can take out - put all the connectors on a plug. Makes it easy to clean, easy to maintain. I have a 230mm x 300mm 6mm aluminium plate as my spreader - it weighs about a kilo. Add in the heater and glass and it would take a heck of a nozzle catch to knock it out of its bed. I have a CNC router, I will be making a custom-fitted hardwood platform.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/bAM67qYMtzj) &mdash; content and formatting may not be reliable*
