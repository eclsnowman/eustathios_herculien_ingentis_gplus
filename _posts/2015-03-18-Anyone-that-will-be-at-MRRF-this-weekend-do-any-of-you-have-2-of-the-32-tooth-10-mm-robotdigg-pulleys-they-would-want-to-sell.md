---
layout: post
title: "Anyone that will be at MRRF this weekend, do any of you have 2 of the 32 tooth 10 mm robotdigg pulleys they would want to sell?"
date: March 18, 2015 06:12
category: "Discussion"
author: Joe Spanier
---
Anyone that will be at MRRF this weekend, do any of you have 2 of the 32 tooth 10 mm robotdigg pulleys they would want to sell? I'm 2 short. I can either use a mix of 36 and 32 tooth or print two 32 tooth pulleys right now. 



Luckily, if I mix it would be doable to have the drive pulleys be the 36 and the idler pulleys be all 32. So at least they would match. But then I have to figure out 20:36:32 teeth drive numbers. 







**Joe Spanier**

---
---
**Eric Lien** *March 18, 2015 06:18*

I do.


---
**Isaac Arciaga** *March 18, 2015 07:50*

**+SMW3D** has them and it also looks like the "no shoulder" versions as well! [http://www.smw3d.com/gt-pulleys-gt2-and-gt3/](http://www.smw3d.com/gt-pulleys-gt2-and-gt3/)

Maybe **+Brandon Satterfield** can get them to you before you leave!


---
**Joe Spanier** *March 18, 2015 13:06*

Awesome Eric!  It looks like id have to pay about 30$ in shipping to get them when j need them. I'll try my printed versions till this weekend. 



I owe you a beer Eric!


---
**Brandon Satterfield** *March 18, 2015 19:18*

**+Isaac Arciaga** I wondered when someone would notice the new shoulderless pulleys. Your awesome! Have a size you want, I can easily get them, just let me know. Didn't carry the 10mm bores till Eric used them. 





**+Joe Spanier** 30.00 in shipping... Ouch. Keep me in mind before spending that much.


---
**Joe Spanier** *March 18, 2015 19:24*

No that was from you to get them by friday. If you can get them to IL before friday with less then 2day air shipping the order will be in in a few minutes.


---
**Brandon Satterfield** *March 18, 2015 19:41*

**+Joe Spanier**. Just checked and your right on, to run a little box across the country 2 day am delivery was at cheapest 26.00. 



Wish I could help more man, sorry. I'll split it with you if that'll help? I can ship today, but can't make a promise once it is in their hands..




---
**Joe Spanier** *March 18, 2015 19:55*

Really? Thats awesome. But at this point Ill just use my printed ones and grab the ones from Eric at MRRF. We are leaving pretty early on friday and Im afraid it would be wasted effort. I will be ordering some spares from you though. Thanks man. 


---
**Brandon Satterfield** *March 18, 2015 20:23*

That's cool, wish we could go... Think we will run up next year.


---
*Imported from [Google+](https://plus.google.com/+JoeSpanier/posts/3pa7kdRLV9E) &mdash; content and formatting may not be reliable*
