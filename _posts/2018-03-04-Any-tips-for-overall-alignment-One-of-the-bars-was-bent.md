---
layout: post
title: "Any tips for overall alignment? One of the bars was bent"
date: March 04, 2018 22:45
category: "Build Logs"
author: Dennis P
---
Any tips for overall alignment?  One of the bars was bent. I am not sure how I missed it, but Jared at PD-Tech replaced them and took care of me and sent replacements free of charge. . .



This is what a 10mm rod out 0.25mm looks like: 

[https://drive.google.com/open?id=1JsUw8HsI7kdR73KsyofTYbRRoag4qYkn](https://drive.google.com/open?id=1JsUw8HsI7kdR73KsyofTYbRRoag4qYkn)



and measuring TIR...with the best Miluloyo that Ali Express has to offer (my good indicators are at work)

[https://drive.google.com/file/d/1KLskSeZFJ9ujhDcS1NQe3q9Cu1kUHkUO/view?usp=sharing](https://drive.google.com/file/d/1KLskSeZFJ9ujhDcS1NQe3q9Cu1kUHkUO/view?usp=sharing)



But now after taking things apart and reassembling with the new rods, one rod at a time, it feels like the overall motion is WORSE than when I started. 



In Y, I get silky smooth motion in the +Y direction, but if I try to move in the -Y, its like I am fighting a one-way clutch bearing. The carriage just locks up. I have to fight it to move at all in that direction. It feels like it catches and sticks from side to side all the way back. Its like the rods are not parallel. 



If I loosen all the pulleys, its silky smooth in +X and -X, but as soon as I try to set pulleys with the spacing gauges, its REALLY hard to move anything. 



I am pretty sure this is the 'worse part' of 'it gets worse before it gets better'. . . I can not for the life of me figure out how to get this thing aligned and set. I know its been done but I am stumped. 



Any advice or suggestions on getting things just so? Any troubleshooting tips?







**Dennis P**

---
---
**Daniel F** *March 04, 2018 23:58*

Difficult to judge from distance. When I first put mine together, it didn't run smooth at all. First I had to straighten the rods, that was really hard, it took forever. I managed to get them +-0.03mm. Next problem was that the cross gantry was not perpendicular due to warping, I had to heat it up and adjust the angle to 90°. It's not only the outer 10 mm rods that need to be parallel and perpendicular, it's the gantry as well. Many constraints. Don't give up. Search the community for alignment, most people had difficulties, there are quite some post about it. 


---
**Dennis P** *March 05, 2018 00:22*

**+Daniel F**, I follow what you are saying. I have been checking as I go and trusted that everything was still good. I should re verify alignments. 



I think I figured out part of it- when I loosen all the pulleys up, they move inboard to where they want to live on the shafts. 



I have been trying to get all the endplay out of the rods by pushing the pulleys tight to the shims between the pulley and bearing. This was what I thought part of my noise was, the wandering of the shafts. I am trying to figure out how to measure the gaps between the bearings and pulleys. i don't have feeler gauges here right now.  



I need to come up with better search terms too- I am certain this has been discussed before. 


---
**j.r. Ewing** *March 05, 2018 02:26*

From a machinist point of view I would set one rod call it god and set the other to it.   Your not going to see. .25 mm with naked eye. My eye is well trained and best I can do is .1mm



If your attaching things and that’s where it starts to bind that’s prolly the path to the root cause.  



Are these 8mm rods spanning 30 inches? They would not be straight under their own weight


---
**Dennis P** *March 05, 2018 20:06*

**+j.r. Ewing** if you watch the video, a about the 3 & 11 sec marks, watch the left  bearing housing moving up and down when the carriage moves to the right.  it sort of acts like an amplifier, you can see the movement pretty well

my frustration, and i knew this going in, is that there is no reference surface to gauge from. the only surface i have large enough to gauge from the top of the table saw. so i am going to have to pick the best 'side' to lay down and measure from. 

the 8mm rods are going 12" +/-. if the weight of the carriage is on the order of 250 grams, deflection of the 2 rods is 0.0035 mm. I would call that stiff enough. 

searching some of the other posts, both here an in the predecessor machine groups like Ingentis, it seems that the approach is to let the printer tell you where it wants things and make the best that you can out of it.  

 

![images/12cffc0ccc705045dda299507c82efa4.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/12cffc0ccc705045dda299507c82efa4.png)


---
**Bruce Lunde** *March 08, 2018 19:57*

  **+Dennis P**  How are you coming along with the alignment?  DId you also have any luck with the PID tuning?


---
**Dennis P** *March 10, 2018 05:54*

Slow- I am hoping to find some time this weekend to work on the printer. Alignment- the bent shaft is replaced. I have most of my wiring extended that needed to be. Have a working board/controller again. So we are making progress. slowly, but positive forward motion.  


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/UurvVxNWnBU) &mdash; content and formatting may not be reliable*
