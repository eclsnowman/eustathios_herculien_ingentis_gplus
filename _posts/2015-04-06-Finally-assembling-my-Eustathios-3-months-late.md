---
layout: post
title: "Finally assembling my Eustathios... 3 months late"
date: April 06, 2015 23:06
category: "Discussion"
author: Jim Wilson
---
Finally assembling my Eustathios... 3 months late. However, is the bearing pictured below has a bit of wiggle, is it intended to be snug? Do I have the wrong bearing if so? If the right bearing, have I had an accuracy issue all along I haven't noticed?



There is slight play, maybe 0.4 mm, between the outer diameter of the bearing and the socket. 

![images/27283f9ee61538d2c52398aac623f3b2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/27283f9ee61538d2c52398aac623f3b2.jpeg)



**Jim Wilson**

---
---
**Eric Lien** *April 06, 2015 23:17*

It really depends on calibration between printers. If you have some kapton tape put a loop or two around the bearing before pressing it in, repeat until snug.


---
**Eric Lien** *April 06, 2015 23:19*

Also that looks like a 608 bearing. Those should house 6900 series bearings (i.e. 10mm ID bearings).


---
**Jim Wilson** *April 06, 2015 23:24*

That'll do it! Ty man.


---
**Eric Lien** *April 06, 2015 23:27*

Mine might be tighter since I print in abs, and also because I print them standing up versus laying down. That creates a little flat at the top which may be the difference. Let me know if you want me to make a tighter tolerance version for your printer.


---
**James Rivera** *April 06, 2015 23:34*

**+Eric Lien**​ bingo. That's what I should've done with the ones I've printed so far (lay them flat). I will do that for the remaining ones.


---
**Isaac Arciaga** *April 07, 2015 01:22*

**+Jim Wilson** After vapor smoothing, the diameter of my bearing insert became a touch larger. What I did was smear a little Permatex Orange on the outer diameter of the bearing and inserted the bearing into the holder. Then wipe off any excess with a paper towel. The Permatex gasket fills the void and will prevent the bearing from popping out.

 


---
**Isaac Arciaga** *April 07, 2015 01:25*

Sorry, its actually red [http://amzn.com/B0002UEN1A](http://amzn.com/B0002UEN1A)


---
**Isaac Arciaga** *April 07, 2015 01:27*

**+Eric Lien** imo your model was dead on. Good job!


---
**Daniel F** *April 13, 2015 13:37*

Just printed mine in ABS (standing). I used **+Eric Lien** s  STL files without scaling, 80% infill. After removing support and cleaning with a file the inner diameter was about 21.8mm (no vapor smoothing). I then heated up the bearing with a hot air gun (not too hot, otherwise the lubricant evaporizes) and pressed it in. Fits tight. Now I have to wait for the Motedis aluminium extrusions and continue to print other parts...


---
**Eric Lien** *April 13, 2015 17:22*

**+Daniel F** you will have to let me know how it stacks up against your CoreXY/QR. 


---
*Imported from [Google+](https://plus.google.com/101778058628996936791/posts/5hStVDYo1Sp) &mdash; content and formatting may not be reliable*
