---
layout: post
title: "It has been awhile, my Herculien still going strong but I am only using it to print large items with a 1.2mm Volcano nozzle"
date: September 04, 2018 08:25
category: "Show and Tell"
author: Vic Catalasan
---
It has been awhile, my Herculien still going strong but I am only using it to print large items with a 1.2mm Volcano nozzle. Needed a smaller printer and this printer started as a Cetus printer. It is a great little printer for what it is, but I needed much more customized settings, heatbed control, the electronic endstop was unreliable. Gutted the electronics, installed a Aero Extruder at first and that was a disaster for me anyways (inserting a new filament was a nightmare) , replaced with a Bondtech BMG extruder (I should have started with it in the first place) added on a E3D-V6 hot end. Gonna try .25mm nozzle, been playing with .4mm for now. I used my spare Azteeg X3 controller with TMC2208 drivers (super quite, fan noise only during printes). Changed out the hot end fan with a 40mm fan. Attached an Octoprint/Raspberry Pi with a cam mounted to the Z axis. If anyone is interested in doing the same thing, I can post the printed files for conversion.



Happy Printing

Vic



 



 



![images/1c9f13b02707098562e5dfb591def0f0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1c9f13b02707098562e5dfb591def0f0.jpeg)
![images/1813925b3e9b5218ef8d9e94e2d3c9ee.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1813925b3e9b5218ef8d9e94e2d3c9ee.jpeg)
![images/3fa2fb9b911cd20f1b380a63e15949db.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3fa2fb9b911cd20f1b380a63e15949db.jpeg)

**Vic Catalasan**

---
---
**Eric Lien** *September 05, 2018 03:02*

It's been a while. Great to see you are still actively printing and modding. Very slick printer you have there. Yeah HercuLien is a bit too big to heat up for smaller prints. And small printers are a treat to tune in (so much easier). 



Please keep us updated on things you are working on if you get time. Things on G+ have slowed down. So it's always great to see old friends.


---
**Vic Catalasan** *September 05, 2018 08:31*

Thanks Eric, I learned most of my 3D printing from building the Herculien. The Cetus is a very nice printer 'as is' but customizing and tinkering was much more fun! I still dislike the belt on the Z as the head constantly slams on the bed when powering off. I might try and replace the Z belt with a ballscrew method.


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/hpqYXtormHX) &mdash; content and formatting may not be reliable*
