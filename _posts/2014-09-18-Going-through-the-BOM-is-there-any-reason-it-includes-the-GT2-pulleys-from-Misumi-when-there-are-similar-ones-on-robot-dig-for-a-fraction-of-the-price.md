---
layout: post
title: "Going through the BOM, is there any reason it includes the GT2 pulleys from Misumi when there are similar ones on robot dig for a fraction of the price?"
date: September 18, 2014 01:06
category: "Discussion"
author: Ben Delarre
---
Going through the BOM, is there any reason it includes the GT2 pulleys from Misumi when there are similar ones on robot dig for a fraction of the price?



For instance, the BOM specifies these GPA32GT2060-A-P8 from Mismui at $16.11 each, look like standard 32 tooth GT2 pulleys to me with 8mm bore. RobotDigg has these at $2 each... [http://www.robotdigg.com/product/222/GT2-Pulley-32-Teeth-8mm-Bore](http://www.robotdigg.com/product/222/GT2-Pulley-32-Teeth-8mm-Bore)



Any reason I shouldn't switch all the pulleys over to robot digg?





**Ben Delarre**

---
---
**Jason Smith (Birds Of Paradise FPV)** *September 18, 2014 01:40*

You may have trouble with the hubs on the robotdigg pulleys not allowing enough clearance in some cases, as they may interfere between the x and y shafts. You may be able to make them work though. If so, please post pics, and I'll update the BOM. 


---
**Ben Delarre** *September 18, 2014 01:41*

Aha, I see what you mean. Well, if I'm doing a robotdigg order anyway a few cheap pulleys will not break the bank. Worth a look i guess.


---
**Jason Smith (Birds Of Paradise FPV)** *September 18, 2014 01:46*

Actually, for the 8mm bore pulleys on the bottom of the lead screws, you could probably get away with the robotdigg pulleys without a problem.  I didn't read your original post closely enough, and thought you were asking about the 10mm bore pulleys on the xy shafts.  I actually already accounted for the xy shaft clearance issues in the BOM, which is why there are 10mm bore pulleys from both Misumi and Robotdigg.


---
**Ben Delarre** *September 18, 2014 01:52*

Looks to me like there would be clearance on the 5mm bore pulleys too?


---
**Ben Delarre** *September 18, 2014 01:52*

**+Jason Smith** do you have a solidworks or similar rendering of the eustathios design with the most recent changes?


---
**Jason Smith (Birds Of Paradise FPV)** *September 18, 2014 01:58*

**+Ben Delarre** Yes, there is also clearance for the 5mm pulleys.  **+Eric Lien**  probably has the most updated design ([https://github.com/eclsnowman/Lien3D_Eustathios_Spider](https://github.com/eclsnowman/Lien3D_Eustathios_Spider)), but I'm not sure if it includes the most recent pulley updates.  Robotdigg didn't carry all of these pulleys when I created the original belt/pulley based design.


---
**James Rivera** *September 18, 2014 02:00*

**+Ben Delarre** this is the one linked to in the about block at the top of the community page:



[https://github.com/eclsnowman/Lien3D_Eustathios_Spider](https://github.com/eclsnowman/Lien3D_Eustathios_Spider)


---
**Ben Delarre** *September 18, 2014 02:01*

**+Jason Smith** yeah I've noticed that they have been keeping an eye on all the 3d printer makers out there and seem to be rolling out whats popular. I tried **+Eric Lien**'s github but that appears to be a later version, guess its time for an upgrade unless Eric can output them in another format?


---
**Ben Delarre** *September 18, 2014 02:02*

'Doh! Totally didn't notice the folder with 'other 3d formats' :-)


---
**Ben Delarre** *September 18, 2014 02:40*

**+Jason Smith** going through the misumi parts now, looks like they no longer do the 56mm length stepped shaft at the end of the lead screws, they only go up to 40mm. Looking at the 3d model this might actually mean the z-axis motor mount needs changing to raise the motor shaft to keep that pulley aligned?


---
**Miguel Sánchez** *September 18, 2014 06:20*

Misumi are hubless. Fitting screws are in the teeth area. This saves some more rod for having longer carriage motion. 


---
**Eric Lien** *September 18, 2014 11:43*

**+Ben Delarre** Ben there should be a folder on the github called other formats. Some formats are a little behind the solidworks version. But there is a step version from 9-11-14.


---
**Eric Lien** *September 19, 2014 01:02*

I have the robotdigg pulleys modeled on my HercuLien. I will make a model configuration of Eustathios with the robotdigg pulleys. They are a much better value.﻿


---
**Ben Delarre** *September 19, 2014 01:16*

**+Eric Lien** excellent that would be great. I took a look at the models as they stand and it looks like you would only need 2 of the 10mm bore pulleys and the rest could be RobotDigg. Then the hubless ones are used near the bearing holder and the hubbed ones can go next to them without issue. All the other pulleys look to have enough clearance already.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/3QXRzzTAmAT) &mdash; content and formatting may not be reliable*
