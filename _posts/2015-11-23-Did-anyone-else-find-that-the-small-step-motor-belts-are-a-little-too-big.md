---
layout: post
title: "Did anyone else find that the small step motor belts are a little too big?"
date: November 23, 2015 21:06
category: "Discussion"
author: Ted Huntington
---
Did anyone else find that the small step motor belts are a little too big? I bought the motor gear on ebay but it has the same specs as the one on the BOM- I haven't tested it but it seems too loose.

![images/c41fca37301bd1589dc35aa572852a01.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c41fca37301bd1589dc35aa572852a01.jpeg)



**Ted Huntington**

---
---
**Ted Huntington** *November 23, 2015 21:07*

One solution is to add washers on the bearing holder to move it farther away, but I must have done something wrong for the original design to be too loose.


---
**Ted Huntington** *November 23, 2015 21:08*

this is on a Eustathios Spider 2


---
**Eric Lien** *November 23, 2015 21:15*

The motor slides down the frame to tension it.


---
**Igor Kolesnik** *November 23, 2015 21:18*

Motors can be adjusted, just lower them.


---
**Ted Huntington** *November 23, 2015 21:29*

duh of course! thanks - why I didn't think of that! That is one of the great features of the aluminum extrusion- parts can so easily be adjusted or removed and put back on. Thanks again


---
**Eric Lien** *November 23, 2015 21:52*

One thing to note is don't put too much tension on them. Make it tight... But don't push down so hard the motor mount deflects. That puts the pulley out of alignment and makes the belt track to one side. Then the pulley will rub on the belt introducing artifacts in the print.





Ask me how I know ;)﻿


---
**Ted Huntington** *November 24, 2015 00:22*

Ok I will make it tight but not too tight. One thing I am battling is that the frame can fall into a rhoboid shape. The RepRap Mendel has a similar problem but has a diagonal threaded rod to keep the frame square. With the Eustathios I'm just hoping that after I smush the frame into a somewhat square shape, it will stay that way ;( 


---
**Eric Lien** *November 24, 2015 00:56*

**+Ted Huntington** yes squaring and aligning a Eustathios frame and Axis is the most difficult part of the build. But once done it stays. Or at least mine has for 2yrs now.


---
**Ted Huntington** *November 24, 2015 04:12*

**+Eric Lien**

that's good to read - any helpful pointers? Maybe using a square is a good idea- but how do I adjust the extrusions? - they are all identical in length- perhaps just tiny variations in the angle they are at when fastened can be used to make it more square.


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/7siwTB6B9Cs) &mdash; content and formatting may not be reliable*
