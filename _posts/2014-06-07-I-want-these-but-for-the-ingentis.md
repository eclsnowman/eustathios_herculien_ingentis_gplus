---
layout: post
title: "I want these... but for the ingentis"
date: June 07, 2014 19:02
category: "Deviations from Norm"
author: ThantiK
---
I want these... but for the ingentis.  I'm guessing that the hot end mount would have to be redesigned for different cross bar positioning though. :[





**ThantiK**

---
---
**Jarred Baines** *June 08, 2014 11:21*

**+Tim Rastall**'s new design is a lot like this, with belts... Check out his post with CAD files a few posts back.



Tim - Have you printed / used those style x/y ends? I'm currently deciding on my x/y setup and I can see the benefits of both the original style blocks and your new "all bars on the same plane" style ones. Which have performed better in practice do you think?


---
**David Gray** *June 08, 2014 13:58*

its not exactly pretty but here is a openscad representation (not exact) of **+Tim Rastall**'s xy ends.

[https://gist.github.com/N3MIS15/ba4be98cd69a828b95ca](https://gist.github.com/N3MIS15/ba4be98cd69a828b95ca)

[https://dl.dropboxusercontent.com/u/56711503/xy.png](https://dl.dropboxusercontent.com/u/56711503/xy.png)


---
**Tim Rastall** *June 08, 2014 15:17*

**+Jarred Baines** not quit yet.  Still getting things back together after an electronics failure.. Hopefully this week! 


---
**Jarred Baines** *June 11, 2014 11:56*

What do you mean accelerate paper instead of weights **+Shauki Bagdadi**?



You're smart - which design do you think? Original ingentis or like pictured in this post?


---
**Eric Lien** *June 11, 2014 12:49*

**+Shauki Bagdadi** I can assure you side loading by pushing on the hot end nozzle with my finger causes no roll on the rod ends. Its far more ridged than I imagined. You would have to see it in person to know.



In theory your evaluation is correct. In practice it's really nice. 



Just a side note this is not my design. Credit goes to **+Tim Rastall** and **+Jason Smith**. I just modeled in SW and made tweaks. I think some people are mistakenly giving me credit. I am standing on the shoulders of giants.


---
**Jarred Baines** *June 11, 2014 13:09*

I see how Erics design puts torque on the shafts, I originally thought tims was perfect (intact his design is based on my suggesting that it would eliminate that torque ;-) but when I think about it, there is a downward force on the x/y ends from the center shafts... the center shafts are not FULLY supported by the outer shafts, they are suspended within the plastic x/y end...



I STILL feel it looks like a better design,  but... I'm interested to see how **+Tim Rastall**'s trials go,  since he's had both designs, he can give us all some unbiased input here :-)


---
**Jarred Baines** *June 11, 2014 13:23*

I don't understand why ultimaker 2 is more eustathios (**+Jason Smith** style design) than ingentis (**+Tim Rastall** new design)...



There's obviously a lot of brains and time and research gone into that machine, why have they opted for that design when they could have gone with the new design?



They make me feel like I'm missing something here?


---
**Jarred Baines** *June 11, 2014 14:13*

Lol - these designs are all inspired by the ultimaker, ultimaker is both a father and a dwarf ;-P



Their source is shared on [youmagine.com](http://youmagine.com) too if you (for some reason) want to see how they do things ;-)



I did have a look, nothing we dont already know through the efforts here - I just remember being surprised by their choice of x/y ends but you are correct, for their build size that much stability would not be necessary...


---
**Tim Rastall** *June 11, 2014 20:34*

Just getting electronics back together.  Printed an xy last night but something is up with my Y stepper so the end result was rather slanty :(. Will Try again tonight.


---
**Tim Rastall** *June 11, 2014 20:39*

**+Shauki Bagdadi** double steppers is something I've thought about but I'm working on something new that doesn't use shafts in at least 1 axis so I probably won't get around to testing that idea. 


---
**Tim Rastall** *June 11, 2014 20:45*

**+Eric Lien** The giants are Ultimaker and to some degree Sublime (the creator of Tantillus). Props to you for further improvements though! 


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/6gaGJER2P2z) &mdash; content and formatting may not be reliable*
