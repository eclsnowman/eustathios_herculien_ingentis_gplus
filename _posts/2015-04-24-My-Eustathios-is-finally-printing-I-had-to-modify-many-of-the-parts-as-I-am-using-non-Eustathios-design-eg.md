---
layout: post
title: "My Eustathios is finally printing. I had to modify many of the parts as I am using non Eustathios design e.g"
date: April 24, 2015 05:24
category: "Show and Tell"
author: BoonKuey Lee
---
My Eustathios is finally printing. I had to modify many of the parts as I am using non Eustathios design e.g. 36 teeth belt gear, 8mm rods, etc.  I am using what I have in stock.  Right now, it is running on 12V RAMPS, non heated bed.  When I have time again, I can try to upgrade to 24V and put up the casing.



The printing is reasonably good except for the vertical band.  Anyone know what is causing this?



![images/9b97877659f8077ca45d74cc7bb74a5e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9b97877659f8077ca45d74cc7bb74a5e.jpeg)
![images/f1a22fc6de2da059845154b4927cb3e5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f1a22fc6de2da059845154b4927cb3e5.jpeg)

**BoonKuey Lee**

---
---
**James Rivera** *April 24, 2015 05:56*

What are your print settings? Also, speed and acceleration settings? The video you posted doesn't look too fast, but I'm wondering if this might be ringing due to acceleration and/or jerk settings being too high? Also, how many vertical shells do you have? This is a pretty good troubleshooting page: [http://support.3dverkstan.se/article/23-a-visual-ultimaker-troubleshooting-guide#ringing](http://support.3dverkstan.se/article/23-a-visual-ultimaker-troubleshooting-guide#ringing)


---
**Isaac Arciaga** *April 24, 2015 06:54*

Can you print out a 20mm calibration cube? It would be easier to see what's actually going on across the board.

[http://www.thingiverse.com/thing:56671](http://www.thingiverse.com/thing:56671)



I agree with **+James Rivera** the ringing moving away from the rear of the vessel is most likely 1 or a combination of these parameters: Acceleration/Jerk/Perimeter Speed/Belt Tension.



*Edit - Just saw your video. You can scratch off Perimeter Speed :)



I'd start off simple and try lowering the perimeter speed to something like 30mm/s and see if it helps. Otherwise that's a pretty damn good print you got there!


---
**BoonKuey Lee** *April 24, 2015 08:40*

Thanks all.  I will work on Acceleration/Jerk/Perimeter Speed/Belt Tension.


---
*Imported from [Google+](https://plus.google.com/105655952080130147525/posts/SCGSrJPiWB3) &mdash; content and formatting may not be reliable*
