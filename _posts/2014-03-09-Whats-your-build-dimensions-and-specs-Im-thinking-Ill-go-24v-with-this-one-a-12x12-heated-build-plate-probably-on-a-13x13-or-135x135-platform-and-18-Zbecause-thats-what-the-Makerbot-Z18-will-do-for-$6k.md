---
layout: post
title: "What's your build dimensions and specs? I'm thinking I'll go 24v with this one, a 12x12 heated build plate (probably on a 13x13 or 13.5x13.5 platform and 18\" Z...because that's what the Makerbot Z18 will do for $6k"
date: March 09, 2014 03:05
category: "Discussion"
author: Mike Miller
---
What's your build dimensions and specs?



I'm thinking I'll go 24v with this one, a 12x12 heated build plate (probably on a 13x13 or 13.5x13.5 platform and 18" Z...because that's what the Makerbot Z18 will do for $6k





**Mike Miller**

---
---
**D Rob** *March 09, 2014 03:38*

**+Mike Miller** Check my vid with all the flashing lights it shows the actual print area. with 2020 instead of 1010 I would have probably gotten my 12x12 even with dual extrusion. next build :) I use 22 inch upright posts and 2 squares made from 4 x 17" each gives 18 x 18 then 6 x 16" extrusions to go between the uprights to hold the top of the rods and z idler pulleys. and the chamber base plate is mounted in the other 4 like a window frame. the electronics and z drive is on the bottom of this plate. the second square mounts on the bottom of the uprights. and this has a plate of plastic/plywood in it like a window frame. and to access the electronics remove the 4 bolts in the corners via holes in the goopyplastic feet with an allen﻿


---
**D Rob** *March 09, 2014 03:40*

that is the revised build. I start making vids on assembly tomorrow. Im tearing it down and replacing the extrusions that were different than above in the video. Ill post the vids soon


---
**Mike Miller** *March 09, 2014 19:05*

I currently have 8 2M 20x20 extrusions, I'm pretty sure they won't be the limiting factor...Grabbing a tape meassure, an 18x18x18 build envelope would be too large, and I've heard an 18x18 heated bed draws a LOT of power. 


---
**D Rob** *March 09, 2014 22:28*

I'm doing 12x12x 16ish? not sure right yet Im rebuilding then starting the next lol 24v is the way to go. get ultimaker electronics from ebay and get a 17a 24v  supply. swap the 7812 linear regulator for a drop in switching regulator 2a and up, mine is 4 with a trim pot $25, they have a $6 non adjustable one on ebay, and then its perfectly ready for 24v. also the gadgets 3d controller works with this board. just remove the reset button from the controller its the only thing pinned different and using it would ground the 3.3v rail probably a bad idea.


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/YX8tg9XkyC1) &mdash; content and formatting may not be reliable*
