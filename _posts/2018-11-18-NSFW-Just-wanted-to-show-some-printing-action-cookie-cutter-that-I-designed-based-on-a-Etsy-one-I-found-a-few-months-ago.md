---
layout: post
title: "NSFW Just wanted to show some printing action cookie cutter that I designed based on a Etsy one I found a few months ago"
date: November 18, 2018 20:43
category: "Show and Tell"
author: Sean B
---
<b>NSFW</b> Just wanted to show some printing action cookie cutter that I designed based on a Etsy one I found a few months ago.  Printed this as 160 mm/s and it turned out really well; unfortunately I ruined it by trying to anneal in my oven.  So I printed a second one that I will not anneal.




**Video content missing for image https://lh3.googleusercontent.com/-yKQ7rVs62pU/W_HO4_tIw6I/AAAAAAAAnAg/5SVnGH-9LBMijSms_7FOE_EdduqnzrjMQCJoC/s0/VID_20181117_153931.mp4**
![images/f0106914d2c0669c96bc7ed5af891f94.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f0106914d2c0669c96bc7ed5af891f94.jpeg)
![images/6c34b9fabb2b051e7d3b67d602008d11.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6c34b9fabb2b051e7d3b67d602008d11.jpeg)
![images/9c931668c27dd215658fb4595b0e32d0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9c931668c27dd215658fb4595b0e32d0.jpeg)

**Sean B**

---
---
**Stefano Pagani (Stef_FPV)** *November 18, 2018 21:52*

I loose steps past 70, what are your accel values?


---
**Sean B** *November 18, 2018 22:37*

Whoops I should correct that.  In simplify 3d I programmed 80mm/s but bumped it up to 150% after it started, so it really ran at 120.  I will have to pull up my config in a bit, I don't have it in front of me.  


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/Fbr51r5BJhd) &mdash; content and formatting may not be reliable*
