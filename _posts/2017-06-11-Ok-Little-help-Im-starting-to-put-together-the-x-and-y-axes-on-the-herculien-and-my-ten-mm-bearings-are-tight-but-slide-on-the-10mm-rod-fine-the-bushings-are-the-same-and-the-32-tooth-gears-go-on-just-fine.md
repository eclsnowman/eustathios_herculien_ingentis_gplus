---
layout: post
title: "Ok, Little help... I'm starting to put together the x and y axes on the herculien, and my ten mm bearings are tight, but slide on the 10mm rod fine, the bushings are the same, and the 32 tooth gears go on just fine"
date: June 11, 2017 18:33
category: "Discussion"
author: James Ochs
---
Ok, Little help... I'm starting to put together the x and y axes on the herculien, and my ten mm bearings are tight, but slide on the 10mm rod fine, the bushings are the same, and the 32 tooth gears go on just fine.  The 10mm shims, however, will not fit on the rods.  I tried running a 10mm drill through a couple, but it didn't help.



Did I just get a bad pack of shims?  Any ideas?





**James Ochs**

---
---
**Eric Lien** *June 11, 2017 18:44*

easiest way to tell is to measure the Shim ID and Rod OD. Now measuring small ID's with a caliper is tricky to get right since caliper jaws have an offset, and the blade width can throw things off. But you should be able to get an idea which is the culprit anyway.


---
**James Ochs** *June 11, 2017 18:47*

As close as I can tell they a within a couple of hundredths, but it looks like the shims are just under 10mm and the rods are very close to 10 :p


---
**Nick Dunne** *June 12, 2017 18:19*

I got the BOM listed shims from McMaster and had this same issue.  I'm not sure which was actually off on size, but what I did to get around it was to use a light grinding bit in my dremel on the inside of the shims.  Held the shims with vice grips, ran the dremel around the ID of the shim using light pressure a few times, and checked fit.  Repeat until the shim fits.


---
**James Ochs** *June 12, 2017 18:22*

Thanks, I'll give that a shot next. 


---
**Eric Lien** *June 12, 2017 20:26*

I will have to find some other shims if these are an issue. Yeah, looking at the tolerances the -0.3mm is an issue. I will try to find some ones that have a proper asymmetric (offset) tolerance to fit over the shaft. 


---
**Eric Lien** *June 12, 2017 20:29*

![images/201ac8e6d62ccf5d0b511642b79a37ff.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/201ac8e6d62ccf5d0b511642b79a37ff.png)


---
**James Ochs** *June 13, 2017 01:52*

The dremel trick worked.  I had also ordered some shims off of amazon that will be here tomorrow.  If they fit I'll send a link.


---
**James Ochs** *July 14, 2017 12:54*

I forgot to follow up, but the ones I got from amazon had the same problem....to small on the ID even though they said -0 +.03 tolerance


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/fSeAvsDjYSq) &mdash; content and formatting may not be reliable*
