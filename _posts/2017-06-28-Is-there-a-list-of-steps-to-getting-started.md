---
layout: post
title: "Is there a list of steps to getting started?"
date: June 28, 2017 03:22
category: "Discussion"
author: Mike Brown
---
Is there a list of steps to getting started?  I like that there's a lot of custom work going on, but from the little bit I've read are some of the custom parts better to use the official parts?  Right now I'm just going down the list of STL files, looking at the picture to see how many I need to print, and moving on to the next one.  I've got a Tantillus that's (mostly) going strong but I'm a little confused on how to print the carriage.  The version that says it has built in supports has a gap between the base under the hanging part and the carriage itself.  I'm assuming I'll need to turn on supports and it will fill in there as needed, but will my slicer (KISSlicer) add supports as needed to the non-built-in support one?  I'm printing in PLA.





**Mike Brown**

---


---
*Imported from [Google+](https://plus.google.com/+MikeBrown421/posts/MnW6KEDTEYg) &mdash; content and formatting may not be reliable*
