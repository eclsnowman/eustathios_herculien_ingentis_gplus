---
layout: post
title: "Assuming I've set up permissions correctly, there are a number of Ingentis files in this folder"
date: May 19, 2014 20:54
category: "Deviations from Norm"
author: Tim Rastall
---
Assuming I've set up permissions correctly,  there are a number of Ingentis files in this folder.  Either IGES, STEP or both.  One is the default Ingentis with mk1 Kraken carriage the other is the belt drive version with redesigned xy ends (that should work with both spectra and belts) and an lm8uu dual hotend carriage. The carriage and xy ends have not been tested yet! 





**Tim Rastall**

---
---
**Jarred Baines** *June 05, 2014 11:20*

Thanks for this Tim - finally got around to accessing it today, I gotta learn more about importing with inventor - I've seen people lately using rhino and it looks so easy! Especially STLs - I was blown away by how easy STL editing was. 



Anyways - thanks again! Appreciate your efforts mate ;-)


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/fjyAKdZzYmq) &mdash; content and formatting may not be reliable*
