---
layout: post
title: "Look what greeted me when I got home"
date: June 05, 2015 23:56
category: "Show and Tell"
author: Bruce Lunde
---
Look what greeted me when I got home. My Herculien build can now continue with my new rods, replacing the not so "precision" rods from a discount vendor.

![images/7f2022c8dff1940d88bd5422881c6d7c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7f2022c8dff1940d88bd5422881c6d7c.jpeg)



**Bruce Lunde**

---
---
**Dat Chu** *June 05, 2015 23:58*

Yay. My drill press is coming in the mail as well. Time to make these holes. 


---
**Gus Montoya** *June 06, 2015 01:17*

Nice!


---
**Isaac Arciaga** *June 06, 2015 06:05*

**+Bruce Lunde** did it resolve your issue?


---
**Bruce Lunde** *June 06, 2015 14:43*

**+Isaac Arciaga**      Yes, the  bearing and gears fit nicely now.  Now I have to work on getting everything aligned, it does not move easily at this point.


---
**Brad Hopper** *June 06, 2015 17:49*

Love getting Misumi boxes!


---
**Eric Lien** *June 06, 2015 19:37*

**+Bruce Lunde**​ alignment does take a while. But once its done it stays aligned. I can't wait to see your update post of first movements... Thats when things get exciting :)﻿


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/aRUBiacu7te) &mdash; content and formatting may not be reliable*
