---
layout: post
title: "Shared on April 15, 2014 15:02...\n"
date: April 15, 2014 15:02
category: "Show and Tell"
author: D Rob
---






**D Rob**

---
---
**Dale Dunn** *April 15, 2014 17:56*

When is someone going to implement a permanently level bed? One sturdy enough to maintain calibration.


---
**Shachar Weis** *April 15, 2014 22:00*

What is the trick here? 


---
**D Rob** *April 15, 2014 22:24*

apparently the gear motor with worm turns the gear the gear has a component that triggers the 3 releases in its turn one at a time. while that point is released the nozzle pushes it down to 0 with nozzle in place the gear continues around re engaging the lock and then the next 2 points. the lock must be friction based


---
**D Rob** *April 15, 2014 22:25*

here is the OP [https://groups.google.com/forum/#!topic/makerbot/R3lSfnCz5W4](https://groups.google.com/forum/#!topic/makerbot/R3lSfnCz5W4)


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/NMx42Cijjpr) &mdash; content and formatting may not be reliable*
