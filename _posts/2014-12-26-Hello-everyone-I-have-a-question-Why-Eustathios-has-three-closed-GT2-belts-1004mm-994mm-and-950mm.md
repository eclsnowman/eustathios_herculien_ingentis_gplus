---
layout: post
title: "Hello everyone, I have a question. Why Eustathios has three closed GT2 belts 1004mm, 994mm and 950mm?"
date: December 26, 2014 07:34
category: "Discussion"
author: Ivans Nabereznihs
---
Hello everyone,



I have a question. Why Eustathios has three closed GT2 belts 1004mm, 994mm and 950mm? Can not use a standard size eg 976mm or 1140mm. Thanks





**Ivans Nabereznihs**

---
---
**Jason Smith (Birds Of Paradise FPV)** *December 26, 2014 18:38*

One belt is housed entirely underneath to drive both lead screws for the z axis. The other two belts run from the steppers underneath vertically up to the x and y-axis pulleys. 


---
**Ivans Nabereznihs** *December 26, 2014 18:48*

Thanks. 1004/994 is for X/Y and 950 is for Z. I think increase the size of the Z axis about 10 cm. So I want to buy two 2GT closed belts 1140mm for X / Y and 976mm Z axial. If I change the sizes of belts there will be no major problems?


---
*Imported from [Google+](https://plus.google.com/118257495094694997465/posts/MiA2VprdJ61) &mdash; content and formatting may not be reliable*
