---
layout: post
title: "Anyone have crazy thermistor readings? I am trying to figure this out and my electronics knowledge is hampering my efforts"
date: March 02, 2018 02:56
category: "Discussion"
author: Dennis P
---
Anyone have crazy thermistor readings? I am trying to figure this out and my electronics knowledge is hampering my efforts. 



These are my temp readings at rest.  [https://imgur.com/t5Vc4R2](https://imgur.com/t5Vc4R2)



I am my gut is telling me that i am picking up interference from somewhere, but where? 



Power supply is a converted Dell ATX one that I modded per the reprap wiki pages. I have a 1.2MegOhm 5W power resistor shunting the +5volt rail to ground for the base load. i did not have a 4.7Ohm 10W resistor.



I am using an Arduino Mega 2560 and Ramps 1.4. the heated bed is one of the AliExpress Ali Rubbber ones. the hotend is an E3D clone with unknown thermistor. I set the hotend to be a generic 100K 3950 one and the heatbed as Honeywell one and tried creating a custom themristor table from Eric's old Marlin configs.  



I disconnected the AC side of things for the heated bed after this and disabled the heated bed in the config. 



Looking at the thermistor section of the schematic, it looks like the circuits are buffered (? i dont know if I am using the right terminology) . There are caps and resistors between the signal and ground legs



[http://reprap.org/wiki/File:RAMPS1.4schematic.png](http://reprap.org/wiki/File:RAMPS1.4schematic.png)



Anyone have any suggestions on where to start looking? What to test? 



Thanks in advance. 

Dennis

 





**Dennis P**

---
---
**Oliver Seiler** *March 02, 2018 03:25*

I'd start with checking the power supply. Your 1.2MOhm resistor is hardly going to generate any load at all, so check that your PSU creates a stable 12V output first. Have you got a multimeter?

Alternatively connect a automotive light bulb to the 5V rail instead of your resistor.




---
**Oliver Seiler** *March 02, 2018 03:28*

I assume that you have double checked the connections for the thermistors and the wiring diagram...




---
**Dennis P** *March 02, 2018 06:02*

I think the problem might be the 5V system between the Pi and the Arduino and the RAMPS.  I did try putting the baseload on the other ATX 5V but it did not do anything. I get 11.42 vols out of the ATX power supply. I will get a bigger resistor to put in there instead, or add a couple to get a larger resistance. 



RAMPS will run the logic off of the USB link between the Pi and the Arduino. The RAMPS/Arduino would randomly reboot and LED1 would flash. I would get the boot screen and then it get to the main home screen and display temps and then reboot again. 



So I am chasing that down now. 


---
**Oliver Seiler** *March 02, 2018 06:07*

Definitively check your power supply if you get random reboots. Also 11.4V sounds a little low.

Adding another resistor of similar rating will not really make a difference. Get the proper 4.7 Ohm 10W one, or connect something else to the PSU, like an old drive, CD-ROM or whatever.

How do you power the Pi? Same PSU?

I don't know for sure whether the RAMPS board uses the 5V or 12V for the thermistors, but either way you need your board to run reliably before further troubleshooting.




---
**Oliver Seiler** *March 02, 2018 06:08*

You could try and plug in your Arduino to a computer or charger via USB while not connecting your ATX PSU and see if you get reboots and/or random temp readings.




---
**Oliver Seiler** *March 02, 2018 06:11*

Seems that the original RAMPS board powers the thermistors from the VCC rail, so it should work if powered by USB


---
**Dennis P** *March 02, 2018 19:14*

Themistors are connected to the VCC which is 5V. I will try to sperate out mega/ramps and only the thermistors. The only other thing I did not replace is the LCD or Raspi. Raspi seems to be working- octprint works. 

So if i understand this correctly, i should be able to read thermistors with ONLY the mega/ramps connected to usb to the raspi. 




---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/3VKKkRt1H8F) &mdash; content and formatting may not be reliable*
