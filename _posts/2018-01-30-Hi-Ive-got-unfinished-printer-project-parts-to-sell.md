---
layout: post
title: "Hi, I've got unfinished printer project parts to sell"
date: January 30, 2018 02:35
category: "Discussion"
author: Daithí Ó Corráin
---
Hi, I've got unfinished printer project parts to sell. Wanted to check first is it okay to post it here **+Eric Lien**?





**Daithí Ó Corráin**

---
---
**Eric Lien** *January 30, 2018 03:03*

Is it for this style of printer?


---
**Daithí Ó Corráin** *January 31, 2018 00:33*

**+Eric Lien** Yes, it's for Eustathios Spider - V2 




---
**Dennis P** *January 31, 2018 01:25*

where are you located? there are a few of us currently building 


---
**Eric Lien** *January 31, 2018 01:33*

**+Daithí Ó Corráin** sure thing, post away.


---
*Imported from [Google+](https://plus.google.com/104923267981938346235/posts/2s4jwEY78Kd) &mdash; content and formatting may not be reliable*
