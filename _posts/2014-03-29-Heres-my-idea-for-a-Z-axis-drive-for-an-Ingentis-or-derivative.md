---
layout: post
title: "Here's my idea for a Z axis drive for an Ingentis or derivative"
date: March 29, 2014 03:18
category: "Discussion"
author: Dale Dunn
---
Here's my idea for a Z axis drive for an Ingentis or derivative. Again, all three axes are shortened by 200 mm to keep everything in view.



The M6x1.0 threaded rod gives 0.005 mm per whole step, but will be admittedly slow. The threaded rod is supported by 696-ZZ bearings. I didn't want to support it at both ends, but with the Z stroke I think I can get, it will be 425 mm long. The green piece carries the nut and isolates the carriage (blue) from any wobble in the screw. It's a derivative of this piece: [http://www.thingiverse.com/thing:20147](http://www.thingiverse.com/thing:20147) The nut carrier will twist slightly on the screw with any wobble, leading to small, cyclic Z position errors. If I have any noticeable banding, and I can't straighten the screw, I'll have to come up with a nut carrier that doesn't twist with wobble.



But all that's beside the point. The point is, driving the Z axis with a screw like this gives you a simple Z axis drive that won't fall when power is lost, and there should be room for 300 mm of Z axis stroke within the standard Ingentis frame dimensions. 



The pulleys for the Spectra line are 624-UU U-groove bearings. The square tube is more 10x10x1 aluminum.



It just occurred to me that the bearing at the motor end of the screw is redundant. And I have an idea now for a way to decouple screw wobble from the carriage without inducing any Z error. I'll implement that as the rest of the design matures.



![images/3c66c31f47fe0a3cc00f9b949529e3c5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3c66c31f47fe0a3cc00f9b949529e3c5.jpeg)
![images/11ec07e9094d06101b93dfe71cbc523a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/11ec07e9094d06101b93dfe71cbc523a.jpeg)

**Dale Dunn**

---
---
**Tim Rastall** *March 29, 2014 04:33*

Nice! 


---
**D Rob** *March 29, 2014 04:44*

love it! cheaper than worm gear. the spectra being the part that actually moves the bed I doubt wobble will be an issue


---
**D Rob** *March 29, 2014 04:50*

if it is tight enough that is


---
**Jarred Baines** *March 29, 2014 14:11*

I find the threaded rod really grinds... I see way too much 'nut dust' on my printer bench from my mendel Z screws to want to use threaded rod on my ingentis!


---
**William Frick** *March 29, 2014 17:50*

**+Jarred Baines** 'Nut dust' indicates a lubrication problem .... ie none.    **+Dale Dunn**  1 threaded rod is a + in reducing cost. The nut will have some force against it that should change during the print phase so I doubt 'z-wobble' will be a problem. If the nut carrier was sliding in an extrusion slot it may not be an issue at all.


---
**Dale Dunn** *March 29, 2014 19:23*

Massive edit: **+William Frick**, the nut carrier can't be guided in a slot. It needs to be free to move with the screw's out-of-straightness.﻿ +1on the lubrication. I'd like to find a way to permanently lube it. A nut made of Delrin AF sounds about right.


---
**William Frick** *March 29, 2014 20:02*

**+Dale Dunn** Maybe it it is the nut that needs to be free to move .... with the threaded rod. The twist on the nut will only occur once during a print.


---
**Dale Dunn** *March 29, 2014 20:10*

The twist on the nut happens every time the screw swings around. 



Researching Delrin nuts has me thinking about a slightly more coarse trapezoidal lead screw. I need to look it up now that it's published,  but I'm pretty sure Ultimaker isn't using anything close to a 1 mm pitch screw.


---
**Jarred Baines** *March 29, 2014 22:29*

Threaded rod is designed to bind, its intentionally rough surface is designed to 'dig in' to the threads of a but when it is tightened, a trapezoidal / acme thread is designed for this job... FWIW - just get leadscrews...



 I do lube the screws occasionally but - plain and simple - putting that bent, fast wearing component on a machine that's otherwise made of pretty good quality components just seems wrong... Because screw thread profiles come virtually to a point, it only takes a little wear to get a good amount of slop because wear changes the outside diameter... With a leadscrew, wearing the face doesn't change the fit of the screw (apart from backlash which will be significantly less and is easier to compensate for)



Also, materials for threaded rod, you have the choice of zinc plating (ugh) or relatively soft stainless steel... No hardened materials, which is what I would expect a good leadscrew to be made of (to resist wear).


---
**Dale Dunn** *March 30, 2014 02:33*

Threaded rod does not have a rough surface unless a surface treatment adds the texture. Zinc plating will have some texture, but stainless will be just the naked rolled threads. Smooth.



Ordinary 60° threads engage on the flanks, not the crest. There is clearance between the crest and mating root when assembled. So wear only increases lash, just like with trapezoidal screws. The advantage of trapezoidal screws is higher load capacity due to a thicker root and the loads being applied to a much more nearly perpendicular face. Oh, and usually more precise manufacturing.



That said, I agree that this is not a place to skimp. What little research I was able to do on Delrin nuts this afternoon has me leaning toward the lead screws atm.


---
**William Frick** *March 30, 2014 03:04*

As long as we stick to trying to perfect a method with a intrinsic flaw (e.g. stepper motors and threads) we can only approach its' theoretical best. Maybe some variation on Z like the hydraulic system in the Peachy printer could bear some thought.


---
**William Frick** *March 30, 2014 17:16*

Didn't mean to have the last word ! I like this design idea for the economy of parts, and with a lead screw could be very good.


---
*Imported from [Google+](https://plus.google.com/107543004166274026989/posts/49hJJ4zX9qN) &mdash; content and formatting may not be reliable*
