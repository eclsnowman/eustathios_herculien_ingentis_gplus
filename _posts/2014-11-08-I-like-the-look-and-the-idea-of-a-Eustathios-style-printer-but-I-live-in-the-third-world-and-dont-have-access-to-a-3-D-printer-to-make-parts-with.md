---
layout: post
title: "I like the look and the idea of a Eustathios-style printer, but I live in the third world and don't have access to a 3-D printer to make parts with"
date: November 08, 2014 00:20
category: "Deviations from Norm"
author: Mike Thornbury
---
I like the look and the idea of a Eustathios-style printer, but I live in the third world and don't have access to a 3-D printer to make parts with. But I do have a bunch of 2020 and a CNC router, so I am going to look at making a variant out of various engineering components and stuff I can mill from blocks of MDF.



I can easily get 8mm pillow bearings, which attach to the extrusion just fine. I also have a bunch of OpenBuild v-trak spare from another project that I could use to make the z-gantry and x/y sliding parts.



Time to get busy with the CAD...



*and who's this Norm guy we are deviating from?





**Mike Thornbury**

---
---
**Mike Thornbury** *November 08, 2014 00:59*

Looking at the current method of driving the z-axis by leadscrew, I thought I might simplify it by making it belt all the way. But, I realised that the screws also do double duty as gantry location/support.



Still, for me, shaft is cheap, screw is more problematic, and I have about 20 GT2 pulleys about the place already.



(how do I add a picture to a post?)


---
**Wayne Friedt** *November 08, 2014 01:32*

Where are you?


---
**Tim Rastall** *November 08, 2014 01:56*

It should be quite practical to make an all metal variant. I'm doing something similar using aluminium plate and L shaped extrusions. Everything is screwed, cable tied or riveted together, no printed parts so far.


---
**D Rob** *November 08, 2014 04:44*

I also have a friend, locally, who has an Ul-T-Slot mkIII that uses milled aluminum corner plates and carriage. Note **+Tim Rastall** started the ingentis with a belt driven z


---
**Mike Thornbury** *November 08, 2014 09:24*

**+Wayne Friedt** Brunei.


---
**Wayne Friedt** *November 08, 2014 09:31*

**+Zut Alors** We are neighbors, sort of. I could possibly ship you printed parts if needed.I am currently in Manila.


---
**Mike Thornbury** *November 08, 2014 09:45*

Wow! That would be amazing. Thanks Wayne. EMS is very regular between Brunei and Philippines.


---
**Wayne Friedt** *November 08, 2014 10:10*

Great. Send me a PM when you are ready so we can decide what to do.


---
**Mike Thornbury** *November 08, 2014 10:13*

Will do, thanks so much.


---
**Mike Thornbury** *November 09, 2014 10:44*

An afternoon well spent... I've taken apart Rich's design and made it 'flat-compatible' - I would post the pics, if I could work out how to load pictures... why is it so difficult?


---
**Mike Miller** *November 09, 2014 17:46*

You might also want to look into **+Shauki Bagdadi**'s QuadRap printers, they're designed to be built without printed parts, with hand tools, and inexpensive components you can find pretty much world wide. 



You'll need a Laptop, controller (RAMPS 1.4 being the standard), 4-5 NEMA17 stepper motors, wiring, and extruder specifics that you'll most likely not find locally. But those are solvable, and laptop aside, can be had for less than $150 US. 


---
**Mike Thornbury** *November 10, 2014 05:08*

Thanks Mike. I make CNC machines, I have all the electronics I need - around 50+ Arduino boards sitting on my bench, about 20 steppers and a couple of TinyG controllers I am converting to bluetooth.



As to computers - about 10 of them of various vintage :) 



Being in the third world means I can't run down to the shops for machine screws or aluminium, so I buy in quantity. I do have a couple of Melzi controllers and a Prusa, but it's shite. A really bad Chinese one that became the impetus for designing/finding a better printer, more suited to the materials available locally (we can't even get threaded rod here).



There's a lot of interest here, especially among the modeller community, but a lot of reticence - people have shelled out big money to ship machines in from overseas, only to have problems with them. There's no support or local club-type resources. I am hoping to change that with maker-type services. People can already get things CNC-machined from me, I have about 24kg of ABS and PLa filament here just waiting for the machine so they can get printed parts too.



But, I am a big fan of using the right tool for the job. Printing 40 hours worth of components that I can cut with CNC in 5-10 minutes at a fraction of the price just doesn't seem sensible... and using what I have is - such as the 50M of GT3 belts and 20-odd sprockets, instead of threaded rod: [https://lh3.googleusercontent.com/-ch0G02P7fNQ/VGBHyHRgLTI/AAAAAAAABK0/vzbtgFyEC4Y/w618-h536-no/Screen%2BShot%2B2014-11-08%2Bat%2B8.56.43%2Bam.png](https://lh3.googleusercontent.com/-ch0G02P7fNQ/VGBHyHRgLTI/AAAAAAAABK0/vzbtgFyEC4Y/w618-h536-no/Screen%2BShot%2B2014-11-08%2Bat%2B8.56.43%2Bam.png)


---
**Tim Rastall** *November 10, 2014 05:28*

**+Zut Alors** that design is very close to the ingentis bed. I recommend a geared down nema so you achieve a high steps per mm value.  It's critical to achieving fine and consistent z res. There is a section 9 that topic in the original ingentis thread on the reprap forums. [http://forums.reprap.org/read.php?279,222917,page=5](http://forums.reprap.org/read.php?279,222917,page=5)


---
**Mike Thornbury** *November 10, 2014 06:00*

Thanks Tim, I have a bunch of .9deg high-resolution steppers. With 16-tooth GT2 sprockets that should give me a step resolution of 0.008mm before turning on microstepping. The TinyG I will probably use has standard support for 1/8 steps, and you can set 1/16 if you are feeling lucky. That would give me .001mm or .0005mm resolution - enough, I think.


---
**Mike Thornbury** *November 10, 2014 06:06*

Hey Tim,  see we hail from the same place - and we have had at least one of the same employer in the past - I worked for NEC on The Terrace about 30 years ago. Not that I've lived in Wellington for more than 15 years now. My Dad's a crazy inventor, too - lives in P'ram.


---
**Tim Rastall** *November 10, 2014 06:17*

Hah, small world! Given the tenure of some of our staff, you might even have worked with some :). If you knew the tssc folk then almost certainly. If you've not been to Welly for a while, it's worth a visit, much changes in 15 years.


---
**Mike Thornbury** *November 10, 2014 10:20*

I came back for a visit in 2011. Bloody cold and windy :)


---
**Mike Thornbury** *November 10, 2014 10:21*

PS, changed my nom-de-guerre.


---
**Mike Miller** *November 10, 2014 12:42*

Then you have the base equipment set to build a VERY good printer. ;) The Eustathios/Ingentis base makes for a great structural basis for a printer of reasonable volume. (mine was set to 350x350x400...a little less in practical terms), but 1M^3 is certainly doable, if you've got the acreage and the right diameter rods. I standardized on 10mm ceramic coated **+igus Inc.** rods and have been very happy with them...they're straight and smooth running, and can be cut easily using carbide tips on a lathe. 



A set of calipers, another set of parallels, and the ability to make X bars of Y Length to a reasonable degree of accuracy, and you'll have a very good build experience. 


---
**Mike Thornbury** *November 11, 2014 03:29*

I'm more about going small, portable and cheap, rather than exotic. I don't see the point of using very expensive (and heavy) rods when my whole design ethos is about simplicity and weight reduction. Ideally I would use carbon fibre rods for XY. If they end up being disposable after a certain amount of time, I can live with that, as long as the design makes that easy to do.



But, ideally I would use some sort of bearing surface that doesn't eat the CF tube - like PTFE tubing, for example.



Materials technology is advancing faster than my ability to read about it, but sticking to 'tried and true' isn't my bag. 



If I am going to design a machine to use CNC cut MDF components, I want it to be small and light and cheap. The biggest problem I see with 3D printers (especially DIY and kitsets) is in getting things square - something the CNC makes a piece of piss. Using off-the-shelf components like pulleys from embroidery machines (CHEAP!), regular sealed bearings, external laptop-style power supplies, accurate extruders and heads of a well-accepted and supportable design is where I am at. 


---
**Mike Thornbury** *November 12, 2014 10:23*

I need to thank **+Mike Miller**  for starting me on the subject of rods - and realising that price isn't everything. I have, over the course of today, looked at both carbon fibre and aluminium as potential rod material an after discussions with a materials specialist, will be trialling some of each, along with a polymer bearing suitable for each.



#Flatsli3er


---
*Imported from [Google+](https://plus.google.com/101708620681849403392/posts/3vaiQbNavBT) &mdash; content and formatting may not be reliable*
