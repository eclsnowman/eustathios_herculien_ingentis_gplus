---
layout: post
title: "Howdy everyone. I may have found a supplier for borosilicate glass, custom cut, to replace VoxelFactory (or until VF is able to start providing the service again)"
date: April 06, 2015 15:35
category: "Discussion"
author: Seth Messer
---
Howdy everyone. I may have found a supplier for borosilicate glass, custom cut, to replace VoxelFactory (or until VF is able to start providing the service again).



At any rate, they do have  $50 minimum order. Here's their email response:



"""""

As we discussed, we do have a $50.00 minimum order requirement, I am pleased to quote your request as follows:

 

Clear Borosilicate Glass, Edges Cut & Swiped (by hand)…not a machined edge.

3.3mm thick X 345mm Wide X 360mm Long [*we will convert to inches as close as we can]

 

Qty. 1                                  $50.00

Qty. 2                                  $31.06/each

Qty. 5                                  $27.95/each

 

*Shipping is NOT included in this price…that would be additional

 

Payment Terms: Credit Card payment in advance

"""""



Here's Steve's contact information, you can contact him directly should you decide to proceed.



Steve Meinhardt

Inside Sales

Cincinnati Gasket & Industrial Glass

513-761-3458 PH / 513-761-2994 FAX

meinhardts@cincinnatigasket.com





**Seth Messer**

---
---
**Eric Lien** *April 06, 2015 17:00*

Looks good. But be careful posting peoples email. I can lead to heavy spam for them.


---
**Seth Messer** *April 06, 2015 17:17*

No worries **+Eric Lien**, I confirmed with him first, before I posted it.


---
**Isaac Arciaga** *April 06, 2015 18:07*

Seth, thank you for the info!


---
**Eric Lien** *April 06, 2015 18:45*

**+Seth Messer** ahh. Good man, good man.


---
**Mike Kelly (Mike Make)** *April 06, 2015 20:08*

Yeah Steve's a good guy. I've placed two orders through him. Highly recommend CGI. 





Shippings usually $25 but it's very well packaged


---
**Seth Messer** *April 06, 2015 20:12*

**+Mike Kelly** Thanks Mike! Glad we've got someone here that's already had some experience with them. Yeah, Steve was super nice and helpful on the phone.


---
**Eric Lien** *April 06, 2015 20:49*

**+Seth Messer** would you mind changing the BOM, looks like a good choice to me. But also we should mention in the BOM that window glass is a good option for those with a lower budget. It has served me well, but man those edges can be sharp. I took them down slightly with very fine emery cloth followed by crocus cloth.


---
**Dat Chu** *April 06, 2015 21:14*

so $75 for one shipped to my house? Did I read from someone within our group that borosilicate is not necessary? Normal glass should be fine?


---
**Eric Lien** *April 06, 2015 21:48*

**+Dat Chu** yup plain glass is fine.


---
**Seth Messer** *April 06, 2015 21:58*

I don't mind one bit. Will get it updated and cleaned up this evening.


---
**Seth Messer** *April 06, 2015 21:59*

**+Dat Chu** before I heard back from these guys at CGI, I had my local glass company cut me some glass to spec. i did 3/16" though.. to try and keep it close to the borosilicate thickness from the BOM.


---
**Dat Chu** *April 06, 2015 22:01*

My local glass company quoted me $75 for one piece of glass so I said, sorry but no thank you. I need to try out Home Depot or something.


---
**Mike Kelly (Mike Make)** *April 06, 2015 22:02*

If you do a group buy the price drops pretty quick as seen in his original quote. 



Borosilicate is probably excessive but when dealing with 100+C glass it's better to error on the side of strength in case something cold ever touches it by accident. 


---
**Seth Messer** *April 06, 2015 22:03*

Wow **+Dat Chu** that's crazy. Mine is doing it for  $11 before tax


---
**Dat Chu** *April 06, 2015 22:23*

I will be up for some group buy if the price is right :). I definitely don't want broken glasses around.


---
**Isaac Arciaga** *April 21, 2015 18:42*

I placed the order for my Eustathios glass through Steve at Cincinnati Gasket. It's much cheaper buying more than 1 piece sort of like a BOGO. I bought two pieces since I always like to have a spare handy especially for a custom size like this. $29.55 each. Shipping will be added on which was estimated to be $20-$25.


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/BhqfYwSN2cB) &mdash; content and formatting may not be reliable*
