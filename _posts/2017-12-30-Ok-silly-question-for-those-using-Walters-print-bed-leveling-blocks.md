---
layout: post
title: "Ok silly question for those using Walter's print bed leveling blocks"
date: December 30, 2017 18:17
category: "Discussion"
author: Ryan Fiske
---
Ok silly question for those using Walter's print bed leveling blocks. How exactly do I attach my bed using those spring spacers? There's no hex indent for a captive nut on the bottom so I'm a little bit lost how to build up that stack. I get how to attach it to the extrusions, just lost on mounting the bed plate. Thanks in advance!





**Ryan Fiske**

---
---
**Brad Vaughan** *December 30, 2017 18:28*

I used wing nuts on the bottom. That allows for easier adjustment.


---
**Ryan Fiske** *December 30, 2017 18:35*

Brilliant! I don't know why I didn't think of that, thanks!


---
**Brad Vaughan** *December 30, 2017 18:36*

NP. You might also toss a washer between the printed part and the wing nut, but isn't strictly required.


---
**Ryan Fiske** *December 30, 2017 18:40*

Good idea. Is there anything to add to prevent the screw from spinning?


---
**Brad Vaughan** *December 30, 2017 18:46*

Blue loctite would do it. You could use a nylon locknut instead of the wing nut. Honestly, I don't tend to have an issue. I use a heavy spring, so that provides enough tension to prevent the wing nuts from backing off. The bed doesn't vibrate too much like it would in a Prusa setup with the bed moving in the Y axis.


---
**Dennis P** *January 02, 2018 00:44*

I was looking at this too and was trying to figure it out. The picture of the part is a little confusing. It look like there is a spade terminal and a wire under the aluminum bed. I can imagine it being a electrical bonding jumper a la Zane's description. I was planning to modify the bed by countering a M3 flat head screw, then using a fiber washer and a lock nut trapped within the spring. Then using a spring cup/keeper and graduated nut to level the whole business.  The arrangement works pretty good on my other printer. [https://www.thingiverse.com/thing:1259206](https://www.thingiverse.com/thing:1259206)  and this [https://www.thingiverse.com/thing:874155](https://www.thingiverse.com/thing:874155)


---
**Ryan Fiske** *January 07, 2018 00:48*

Dennis, I just put everything together for the first time just now and noticed it does seem to spin a bit with just a low profile m5 screw and wing nut. I think placing a lock washer between the screw and the bed might alleviate the issue, but I'm all out of anything at the moment to test it with. I'm also dealing with X/Y alignment issues right now so it'll be awhile.


---
**Dennis P** *February 08, 2018 04:07*

**+Ryan Fiske** I think I figured out the bed leveling blocks... I am pretty sure that Walter had cross members at the ends of his bed supports. You can see them in this picture on his web page- so part of the block with cross shaped tenons nests into the end of the side pieces and the other tenon into the recess of the cross piece. 

[thrinter.com - Eustathios Initial Build - Thrinter](http://thrinter.com/eustathios-initial-build/)


---
**Brad Vaughan** *February 08, 2018 04:35*

I'm using the bed blocks from **+Walter Hsiao** . If you want picks of them, here they are from the side. I can get others if you need them.



I have them setup such that the front and back extrusions fit inside the left and right extrusions (you can barely see that in the picture).

![images/eb396ef777bbf3e7e2eaa320bcb97a75.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eb396ef777bbf3e7e2eaa320bcb97a75.jpeg)


---
**Ryan Fiske** *February 08, 2018 15:57*

**+Brad Vaughan** and  **+Dennis P** thanks for the help. I do have the blocks set up correctly my question was more about the stackup that mounts the plate to the frame (screw, spring, plastic spacer,nut) but after tinkering, I figured it all out thanks to the wing nut recommendation. I'll post a photo soon when i get a chance to show it. Unfortunately, I'm in a bit of a pickle with the 3d printed Z axis supports. The short ones that Walter designed that I was using must have been slightly undersized and they cracked in half! I have another set, but my rectangular bed is too large to for between them! I need to figure out how I'm going to reprint a set now that my other printer was taken apart to build this guy.


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/bnFtiLFd2PV) &mdash; content and formatting may not be reliable*
