---
layout: post
title: "Misumi Order Placed McMaster-Carr order Placed E3D hotends and volcano upgrade order placed Printed parts in process Now is the hard part of sourcing all the other stuff"
date: February 03, 2015 17:05
category: "Discussion"
author: Derek Schuetz
---
Misumi Order Placed

McMaster-Carr order Placed

E3D hotends and volcano upgrade order placed

Printed parts in process



Now is the hard part of sourcing all the other stuff 





**Derek Schuetz**

---
---
**Dat Chu** *February 03, 2015 18:21*

Awesome. My stuff is all placed as well. Just need to find the time to swing by and pick up my order from my local sourcer. 


---
**Derek Schuetz** *February 03, 2015 18:25*

Is the Azteed really the best electronics to go with? is it set for 24v already? ive used ramps on my last 3 printers but this will be my first 24v printer and i really dont want to deal with resoldering a ramps board.


---
**James Rivera** *February 03, 2015 19:21*

If I was shopping for a 3D printer controller board right now, then I would probably buy either the Azteeg X5 (or a Smoothieboard). Ideally, I would like an ARM board with the ability to use the new Trinamic stepper drivers that **+Thomas Sanladerer** reviewed here: 
{% include youtubePlayer.html id=mYuZqx8xwTg %}
[https://www.youtube.com/watch?v=mYuZqx8xwTg](https://www.youtube.com/watch?v=mYuZqx8xwTg)



Unfortunately, it seems like there is no ARM CPU based solution that doesn't already have the stepper drivers soldered down (none of which use these new Trinamic drivers). That being said, I think the Smoothieboard will let you use external stepper drivers (if you are knowledgeable enough and willing to solder them in yourself), but then you are paying for stepper drivers you won't use.



So, if you want to use these fancy new drivers, it seems you will need a board with sockets, so the Azteeg X3 or X3 PRO (both a bit pricey, but better than cheap Chinese RAMPS boards--in many ways) would seem to be the best choice.

[http://www.panucatt.com/Azteeg_X3_Pro_p/ax3pro.htm](http://www.panucatt.com/Azteeg_X3_Pro_p/ax3pro.htm)

(<b>Wide input(12-24V)</b>)



Also, the X5 has an ARM CPU and 32 microsteps, and it does have the input you want: <b>12-30V</b>, according to their wiring diagram:

[http://files.panucatt.com/datasheets/x5mini_wiring_v1_1.pdf](http://files.panucatt.com/datasheets/x5mini_wiring_v1_1.pdf)



<i>But yeah, they should probably add the input voltage to their specifications page</i>

[http://www.panucatt.com/azteeg_X5_mini_reprap_3d_printer_controller_p/ax5mini.htm](http://www.panucatt.com/azteeg_X5_mini_reprap_3d_printer_controller_p/ax5mini.htm)



Or...just get a cheap Chinese RAMPS board and put the Trinamic drivers on it. Of course, you may end up buying another one very soon when it burns itself up. If you want higher quality, you have to pay for it.  ;)


---
**Derek Schuetz** *February 03, 2015 19:34*

Azteeg x3 it is...thanks James..: I really want to avoid cutting corners with this build I want something precise and consistent


---
**Eric Lien** *February 03, 2015 20:23*

If it were me I would go x5 mini at this point. You can hit the ceiling on speed with the x3. That's my next upgrade.


---
**Derek Schuetz** *February 03, 2015 20:30*

I went x3 because my first printer ran a printer boardwith integrated steppers, one blew and I had an $80 piece of junk.  And do you mean it can't reach max speed with an x3?


---
**James Rivera** *February 03, 2015 21:30*

The X5 has an ARM CPU (so it can go faster than Atmel Mega based boards) and 32x microstepping. It does not have the Trinamic drivers, but they're close (and should be quieter/better than cheapo Allegro 4988 drivers).


---
**Dat Chu** *February 03, 2015 22:07*

So between the X5 and a Rambo board, what should I opt for? They cost the same and I want the best for my upcoming Herculien.


---
**Eric Lien** *February 03, 2015 22:48*

X5 is only single extruder. So for dual you would want X3, smoothie board, or Rambo. Rambo is a great board. I just am getting frustrated with blobs due to stuttering at speeds above 100mm/s and lots of short moves like arcs (since they are actually lots of little segments).


---
**Dat Chu** *February 04, 2015 01:12*

So maybe I should get a smoothieboard then? With Herculien, am I looking at printing above 100mm/s speed? 


---
**Eric Lien** *February 04, 2015 01:48*

**+Dat Chu** totally doable, but a lot of times it depends on part geometry. Sometimes part features are not conducive to screaming speeds. But large highly filled parts are no problem. Just slow down outermost perimeters for best visual appearance.


---
**Dat Chu** *February 04, 2015 01:49*

This is the first time I heard about part geometry demanding print speed. Is there a slicer that take care of this? 


---
**Eric Lien** *February 04, 2015 01:59*

**+Dat Chu** I just mean small features where there isn't enough cooling between layers, or tons of retracts and fast short moves causing harmonics. You will find with any printer certain speeds are more problematic than others.


---
**Eric Lien** *February 04, 2015 02:02*

**+Dat Chu** I do a lot of very large prints (10" OD base). Cooling is no issue on them, the layer times even at high speed are sufficient.


---
**Dat Chu** *February 04, 2015 02:10*

I see. So if I have multiple small objects, just bring them at the same time and I won't have the issue (as much)? 


---
**Joe Kachmar** *February 04, 2015 17:28*

**+James Rivera**  There are actually a few ARM solutions that don't have soldered down steppers, but they require substantially more legwork to get running than



In my opinion, the best ARM solution at the moment looks like a BeagleBone Black with  **+Charles Steinkuehler** 's CRAMPS board and MachineKit images. It means switching from the more 3D Printer-centric Marlin or Smoothieware firmwares to a bull blown LinuxCNC-based OS though.


---
**James Rivera** *February 04, 2015 18:20*

**+Joe Kachmar** I thought about BBB+Replicape, but I totally forgot about CRAMPS! That is a socketed design that should allow use of the Trinamic stepper drivers. That being said, I'm having trouble finding a place online that sells them. Do you know of a supplier? Not even the RepRap wiki page for CRAMPs lists one.

[http://reprap.org/wiki/CRAMPS](http://reprap.org/wiki/CRAMPS)


---
**Joe Kachmar** *February 04, 2015 18:29*

**+James Rivera** Well now I feel like a complete ass for not updating the wiki when a friend sent me a supplier!



[http://pico-systems.com/osc2.5/catalog/product_info.php?products_id=35&osCsid=fobcbl0ce82p4rehg5bss7ln46](http://pico-systems.com/osc2.5/catalog/product_info.php?products_id=35&osCsid=fobcbl0ce82p4rehg5bss7ln46)



For anyone who wants a more detailed picture of the board:

[http://pico-systems.com/cramps.html](http://pico-systems.com/cramps.html)



I hadn't heard about the TMC2100 drivers, but I might put an order in for a set of 4 now, especially since Watterott has them for the same price as Panucatt's DRV8825s.


---
**James Rivera** *February 04, 2015 18:31*

**+Joe Kachmar** Doh! Sorry--that was not my intention. :)

EDIT: I have posted an edit to include a short version of the link (unique id stripped) to the product page at the bottom of the CRAMPS wiki page. But I think it will need to be approved first, because I'm not seeing it update. Also, as my first RepRap.org edit, I may have violated some guideline for how to post, so it may not show up at all. But hey, I tried. :)

EDIT EDIT: It looks like it was already there--I just didn't see it under the "Build and Use" section.

[http://reprap.org/wiki/CRAMPS#Build_and_Use](http://reprap.org/wiki/CRAMPS#Build_and_Use)


---
**Dat Chu** *February 04, 2015 18:38*

I really want to get the Trinamic silent step but then both Rambo and Azteeg don't have an easy way to use those :(. What is a boy to do?


---
**Joe Kachmar** *February 04, 2015 18:45*

**+James Rivera** Not a problem, I'm glad you asked about it. The more people who see MachineKit as a viable 3D Printing control option, the more love the project is going to get. As-is it's a small group relative to the greater RepRap community working on some really exciting stuff.



**+Charles Steinkuehler** Outlined some of the work being done right now in this post: [http://blog.machinekit.io/2015/01/3d-printing-with-machinekit.html](http://blog.machinekit.io/2015/01/3d-printing-with-machinekit.html)


---
**Walter Hsiao** *February 05, 2015 05:59*

FYI, for anyone looking to get a smoothieboard, there's a group buy happening on the delta google groups: [https://groups.google.com/forum/#!topic/deltabot/WNTOnAk4jZM](https://groups.google.com/forum/#!topic/deltabot/WNTOnAk4jZM) (looks like it'll be $123 + shipping for a 5xC).


---
**Dat Chu** *February 05, 2015 14:33*

**+Walter Hsiao** awesome info. Signing up for one.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/Fg9a4btrjkN) &mdash; content and formatting may not be reliable*
