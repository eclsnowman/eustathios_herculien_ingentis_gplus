---
layout: post
title: "Anyone got 145 bucks burning a hole in their pocket?"
date: May 10, 2014 07:25
category: "Deviations from Norm"
author: Tim Rastall
---
Anyone got  145  bucks burning a hole in their pocket? That's what it would take to convert a direct drive Ingentis to Syncromesh based on using 8 pulleys and 5m of cable. This is one of the down-sides of the Ultimaker mechanics - it's very pulley intensive.

[https://sdp-si.com/eStore/Catalog/PartNumber/A%206P%209M1201510](https://sdp-si.com/eStore/Catalog/PartNumber/A%206P%209M1201510)
 ([https://sdp-si.com/eStore/Catalog/PartNumber/A%206J%209MAS1209](https://sdp-si.com/eStore/Catalog/PartNumber/A%206J%209MAS1209))





**Tim Rastall**

---
---
**Jarred Baines** *May 10, 2014 07:36*

Enlighten me quickly Tim, whats the benefit of synchromesh?


---
**Tim Rastall** *May 10, 2014 07:47*

Principally,  it has a central core of braided steel wire so it's much less stretchy than belts.  It also aligns on the pulley in very fluid way so less vibrations.  **+Whosa whatsis** is a fan and uses it on the Bukito/bukobot. 


---
**D Rob** *May 10, 2014 08:38*

I've mentioned elsewhere that if we drop a pulley off of the x axis, and one off of the y by placing the x/y drive motors outside the frame and aligned with the shaft it turns . The belt would be on the motor pulley, and cross over and under the nearest rod and go on a pulley on the further rod. The pulleys on the opposite end of the rods will keep them turning in tandem and belt clamps can still be used. this will cut costs on 2 pulleys and 2 belts. also with a hinged spring setup a belt tensioner could be incorporated into the motor mount design.


---
**Dale Dunn** *May 10, 2014 14:18*

My problem with synchromesh is size of the smallest pulley compared to GT2. (Maybe not for Ø10mm shaft, I haven't looked.) I'm still planning to drive with Ø5mm shafts, which allows a very small pitch diameter on GT2. So I would be trading rigidity of the belt for rigidity of the stepper position. I can't find elasticity data for belts or synchromesh, so I have no way of calculating which would be a more rigid system.



You could cut the amount of synchromesh components used by only using it on half of each belt run. Each loop of belt or cable only needs teeth for the portion that runs over the drive pulley. The other half (the half that runs over the idler pulley) could be Spectra or some other rigid but cheap cord.


---
**Jarred Baines** *May 10, 2014 14:25*

:-O

Oh, I was trying to look at that link on my phone before and it was all garbled, I see now that the synchromesh is a kind of 'helical' belt which would definitely deliver smoother motion than the GT2 belts.



How does it not 'twist' though? because if it is able to twist at ALL then the the position will be lost...



I'm liking the steel braid / helical engagement... hmmm...


---
**Jarred Baines** *May 10, 2014 14:37*

Oh wait...



Cumulative error 4.07mm over 100P (P being 6.35)



So, over 635mm there's up to 4.07mm error.



Over the 300mm of travel required for our machines, that's close on 2mm... yuuuuuck.... :-/



Am I reading that right?



It's also an imperial belt... so, I'm assuming there would be rounding errors... (nit-picky but, it WILL have SOME effect... I don't understand how **+Whosa whatsis** likes these if he's using imperial pitch because he's also fairly adamant that Z artifacts are caused by this same rounding error)


---
**Whosa whatsis** *May 10, 2014 17:13*

**+Jarred Baines** I am against using them for Z. The small non-accumulating error that it can create within the X/Y plane is dwarfed by the normal and unavoidable artifacts of trying to position a blob of plastic ~.5mm wide to an arbitrary position with more decimal places than your steps/mm value, to within a few microns, but that same error in Z results in entire layers being under/over-compressed, which does cause visible artifacts in otherwise good prints.



I don't know what SDP did to get those ratings, but based on our testing, they're pretty pessimistic. The cable is not just one steel cable, It's two of them, one wrapped helically around the other with their nylon coatings fused together. Because of this, they refuse to twist without coiling (which they can't do under tension).



When I first heard about them I was more skeptical than anyone, but I did every test I could think of to try to prove that they were not suitable, but found that for X/Y, with purely inertial loads, in every respect they are at least as good as belts, if not better, while being much smoother, quieter (you don't realize how much noise your belts are making until you replace them) and more resistant to stretching.


---
**Jarred Baines** *May 11, 2014 14:10*

Yes I can imagine the difference in noise would be similar to the reduction you experience going from straight toothed gears to helical/herringbone - and I see the point in z layering vs x/y positioning - these would have less shudder / vibration on  sharp cornering I would imagine? That and the helical engagement are probably whats got you hooked?


---
**Whosa whatsis** *May 11, 2014 16:53*

Helical engagement and the resistance to stretching, yes.



I've also developed some really nice ways to handle doing idlers and attachment points with them, that wouldn't work as well with belts. For the idlers, I found that two 625 bearings with a washer between makes a nice groove for it to ride in, and gives almost exactly the same diameter as the motor pulleys we use, for a nice clean look. Of course, most of the types of idlers used for spectra should work too. For the ends, I've been putting a slot or hook on the carriages and crimping them with a washer captured on each end, so that it holds strongly when the synchromesh is pulled tight (on the Bukito, I accomplish this with a sliding idler that makes getting perfect tension faster and easier than on any other machine I've ever used), and is easy to remove when the tension is released.


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/jhvf4cgTaSz) &mdash; content and formatting may not be reliable*
