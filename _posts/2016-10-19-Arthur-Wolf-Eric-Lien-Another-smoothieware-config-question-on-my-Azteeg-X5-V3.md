---
layout: post
title: "Arthur Wolf Eric Lien Another smoothieware config question on my Azteeg X5 V3"
date: October 19, 2016 23:55
category: "Discussion"
author: jerryflyguy
---
**+Arthur Wolf** **+Eric Lien** 



Another smoothieware config question on my Azteeg X5 V3.



I'd like to enable the auto temp switch to turn my cooling fan on (on the hot end) when my hot end is more than 100C.



I've got it mostly configured but have a question as to what the configuration of these two lines should be:



#1

"Temperatureswitch.hotend.designator"



I have this set to "T" but not sure if that is ok



#2



"Temperatureswitch.hotend.switch" this is set to "misc" but I'm thinking this is incorrect? My "switch.misc" runs the part cooling fan. If I want it to toggle the "switch.fan" (M106) what do I replace the "misc" with?









**jerryflyguy**

---
---
**Eric Lien** *October 20, 2016 15:49*

look here at some example code from my X5 GT config that is running my HercuLien: [http://pastebin.com/SFMsXqvs](http://pastebin.com/SFMsXqvs)



First you define the heatsink fan switch (in my case switch.heatsinkfan which has an output pin of 1.22 on the X5 GT)



Then you define the toggle conditions for which temperatureswitch.hotend.switch will turn switch.heatsinkfan on and off (in my case at 50.0C).



Hope that helps. 

[pastebin.com - Auto Heatsink Fan Switch - Pastebin.com](http://pastebin.com/SFMsXqvs)


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/aQ3KeSZmA6z) &mdash; content and formatting may not be reliable*
