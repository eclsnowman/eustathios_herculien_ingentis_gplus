---
layout: post
title: "Aluminum extrusion all cut to size thanks to Mike Smith"
date: March 04, 2015 15:16
category: "Discussion"
author: Rick Sollie
---
Aluminum extrusion all cut to size thanks to Mike Smith.  Do you need to drills and tap or can you use the brackets that Misumi provides?

![images/bc0b96fe6897df54ca07c5d6b9996747.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bc0b96fe6897df54ca07c5d6b9996747.jpeg)



**Rick Sollie**

---
---
**Eric Lien** *March 04, 2015 16:43*

I assume this is a Eustathios? If so I recommend doing both the threaded ends with matching cross holes in the extrusion to allow axis to the buttonhead, and the misumi corner brackets. This makes for a very ridged frame.


---
**Rick Sollie** *March 04, 2015 17:42*

**+Eric Lien** Yes this will be an Eustathios with 2 variations. Increase height and the bottom will have extrusion around the base rather than have legs. This will be to inclose all electronics.  Thanks for the advice.


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/hoLe5ZgJ1E9) &mdash; content and formatting may not be reliable*
