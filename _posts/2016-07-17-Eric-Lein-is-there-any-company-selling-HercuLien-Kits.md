---
layout: post
title: "Eric Lein is there any company selling HercuLien Kits?"
date: July 17, 2016 01:47
category: "Discussion"
author: Robert Clayton
---
**+Eric Lein** is there any company selling HercuLien Kits? I would like to build one but if I have to have a 3D printer to make a my first 3D printer it is difficult to begin.





**Robert Clayton**

---
---
**Ray Kholodovsky (Cohesion3D)** *July 17, 2016 01:50*

Based on what I have seen there are a significant # of people out there that are happy to print parts for another new printer user.   

3DHubs would get expensive for a large # of parts especially printer at high infill (needed for strength). 


---
**Robert Clayton** *July 17, 2016 01:56*

Anyone have the time to volunteer?﻿ Thank you in advance. I hope to begin very soon.﻿


---
**Ray Kholodovsky (Cohesion3D)** *July 17, 2016 02:00*

I like E3D hotends, motors from openbuilds, silicone heaters from Keenovo or Alirubber.  I create some really powerful control boards powered by smoothie so I'm biased in that regard.


---
**Robert Clayton** *July 17, 2016 02:49*

Thanks Ray!


---
**Eric Lien** *July 17, 2016 03:17*

No kits that I know of for sale. I used to print kits for people. But after 20+ kits sent out between HercuLien and Eustathios I called it quits. But hopefully you can find someone to print you a set. Another option is a starter printer like an Wanhao I3 kit. They are reasonable in price, could probably get most of your money back on resale locally to someone, and will teach you the basics. A HercuLien or Eustathios is an amazing printer... But the build is not for the faint of heart. Of those 20+ kits the completion ratio is probably 60% (hence why I stopped printing them for folks, because I only ever charged for shipping).



A starter printer gives you two things, experience in tuning once you build a big printer... And a way to make the parts.



Best of luck, and glad to hear you want to build one. It is a great hobby, you will learn even more than you expect to :)


---
**CkTBrD** *July 19, 2016 15:21*

I do have a spare Eustathios kit if you like. Just printed parts.


---
**jerryflyguy** *July 19, 2016 17:23*

**+E. LeFort** dang! Wish I'd asked this question earlier! I've found a local guy who is helping me out but it's been a slow process. More than a month in and I'm still waiting for the second half of the parts.  What's your kit of printed parts worth? 


---
**CkTBrD** *July 19, 2016 19:21*

Sent you a PM **+jerryflyguy**  


---
*Imported from [Google+](https://plus.google.com/103318213942990361542/posts/LtAwdHX2o7e) &mdash; content and formatting may not be reliable*
