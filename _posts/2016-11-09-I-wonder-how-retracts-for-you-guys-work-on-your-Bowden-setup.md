---
layout: post
title: "I wonder how retracts for you guys work on your Bowden setup?"
date: November 09, 2016 09:56
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
I wonder how retracts for you guys work on your Bowden setup?



I'm using the Bondtech and of course reliable movement of the filament isn't an issue with that. The E3D v6 on the other hand looks to have problems in my setup with filament beeing printed too slow or with too many retracts.



Depending on the filament I see irregular extrusion: nearly no extrusion first and then a blob of filament coming out of the nozzle. I see this happening with frequent retract following each other or in some smaller prints. This seems to be worse with filament that has quite a big zone of melting going from soft/honey like/gooey to liquid.



Is my 40mm fan probably not good enough to deliver the right airflow? I thought it should be but maybe that could be the problem? I have seen this in the past already with the 30mm fan so I thought it couldn't be that problem but ineffective cooling is the only thing I could imagine. Or is it just a general problem with this situation in combination with longer retracts like we need the on the bowden setup?



Maybe **+Sanjay Mortimer** can add something to the discussion.







**Frank “Helmi” Helmschrott**

---
---
**Maxime Favre** *November 09, 2016 10:21*

No short answer here. It depends of the material, retraction settings and temperature. For PLA and ABS I use 5mm @60mm/s. A bit longer and slower for Petg. Extruder temp is critical. Run a test print and try to adjust the temp by 5°C step (I usually start at the highest recommended temp and lower it. In simplify 3d there is settings called "extra restart distance" and "coast at end" which are more than helpfull.


---
**Roland Barenbrug** *November 09, 2016 11:46*

Had similar problems which I essentially solved by going back to basics: first make sure extrusion works fine when printing in continuous flow (no retraction an no break) e.g. small tower. That showed already that slow printing (< 20mm/s with PLA in E3D Volcano) can be an issue.

Next introduced retraction (not more than 4.5mm otherwise pulling melted fillament in cold zone), doing it as fast as possible (65 mm/s) with high acceleration.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/jnZ2REfszab) &mdash; content and formatting may not be reliable*
