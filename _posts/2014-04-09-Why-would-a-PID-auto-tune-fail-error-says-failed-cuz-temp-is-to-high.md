---
layout: post
title: "Why would a PID auto tune fail, error says failed cuz temp is to high"
date: April 09, 2014 02:34
category: "Discussion"
author: Wayne Friedt
---
Why would a PID auto tune fail, error says failed cuz temp is to high. When i turn the hotend on it will regulate the desire temp:ie 200c. Its just the AUTO tune that fails. I raised the #define PID_FUNCTIONAL_RANGE from 10 to 25 but didn't change anything.





**Wayne Friedt**

---
---
**Daniel Fielding** *April 09, 2014 02:35*

Is the hot at room temperature before you start the auto tune


---
**Wayne Friedt** *April 09, 2014 02:44*

Room temp. It over shoots the 200 mark to 250 or more then errors from that. The PID only starts working withing 10 degrees of the mark so i think it is just heating faster that the PID can take control. Maybe if i turned the max out put from 255 to say 155.


---
**Whosa whatsis** *April 09, 2014 02:53*

I've seen this happen before with heaters that are over-powered or have too much thermal isolation from the sensor so that the sensor's reading continues to rise for too long after the heater shuts off.



There's a hard-coded safety limit in temperature.c. Search for "PID Autotune failed! Temperature too high" in that file and look one line above it to find the limit so that you can change it.


---
**Wayne Friedt** *April 09, 2014 03:22*

I can not locate temperature.c.

But there is something else i see

The temp sensors should be -1 or just1



#define TEMP_SENSOR_0 -1

#define TEMP_SENSOR_1 -1

#define TEMP_SENSOR_2 0

#define TEMP_SENSOR_BED 0


---
**Whosa whatsis** *April 09, 2014 03:25*

It should be set to the number corresponding to the specific thermistor that you're using, according to the table in the comments directly above. -1 is "thermocouple with AD595"


---
**Wayne Friedt** *April 09, 2014 03:29*

Yes i was actually referring the the - in front of the number. The version i am using doesn't have the - in front of the number or i have omitted it. I am using 

#define TEMP_SENSOR_0  5



Could this cause a problem


---
**Whosa whatsis** *April 09, 2014 03:34*

#define TEMP_SENSOR_0  1

means something different from

#define TEMP_SENSOR_0  -1

both are valid options, but the latter is the default and most likely means that the configuration has not been properly set up for the machine.


---
**Wayne Friedt** *April 09, 2014 03:37*

Thanks for your input


---
**William Frick** *April 11, 2014 23:34*

The over temp error could be triggered by exceeding the max temp setting in Marlin. Had that when i first tried PID autotune on my E3D.


---
**Whosa whatsis** *April 12, 2014 02:08*

**+William Frick** Pretty sure that error would look different. It would say something about MAX_TEMP instead of saying that PID autotune failed.


---
**William Frick** *April 12, 2014 14:04*

**+Whosa whatsis** It was a couple of months ago .... you are probably right.


---
**Wayne Friedt** *June 10, 2014 02:50*

**+Whosa whatsis** I was finally able to get into temperature.h and change the max temp from 20 to 30. That fixed the over shoot and i was able to complete the auto tune. Thanks!


---
*Imported from [Google+](https://plus.google.com/+WayneFriedt/posts/PM9DkKtogKZ) &mdash; content and formatting may not be reliable*
