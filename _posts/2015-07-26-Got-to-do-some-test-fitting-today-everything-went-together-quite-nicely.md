---
layout: post
title: "Got to do some test fitting today, everything went together quite nicely"
date: July 26, 2015 22:03
category: "Show and Tell"
author: Bryan Weaver
---
Got to do some test fitting today, everything went together quite nicely. This thing is a freakin tank! Can't build the gantry until I get those ball screws trimmed up. Screwed up and ordered 500mm instead of 450mm.



I had blue ABS on hand, and 1000mm black 2020 aluminum extrusion was only $5.50 per stick when I ordered from SMW3D. Therefore, I ended up with a blue and black color scheme. I think it looks pretty good.

![images/4d16f2a3da48faf6f341d2bc7c43af22.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4d16f2a3da48faf6f341d2bc7c43af22.jpeg)



**Bryan Weaver**

---
---
**Eric Lien** *July 27, 2015 00:58*

Looks great **+Bryan Weaver**​, love the black and blue look. And the color scheme is fitting... Printers kick my ass from time to time ;)


---
**Brandon Cramer** *July 27, 2015 02:20*

That blue and black together looks awesome! Makes me want to switch from red to blue almost.... ;) 


---
**Erik Scott** *July 27, 2015 03:36*

My new build is gonna be blue and black. Looks fantastic! I like the second crossbar on the bed assembly, but I don't think it is all that necessary. What color are the floor and side panels going to be?


---
**Bryan Weaver** *July 27, 2015 03:55*

Yeah, the second cross bar isn't necessary, this thing is solid as a rock.  But I had a leftover ~600mm length of 2020 left after I had everything cut, so I put some of that to use.



Not sure yet what I'm going to do with the bottom and side panels. I have some clear acrylic sheet leftover from another project. I've been wondering how that would look..


---
**Erik Scott** *July 27, 2015 04:02*

I was considering clear acrylic too. I don't want to make it black as I think that'll be just too much black.



I'm sure it has been posted before, but do you have any more info on that ball screw mod? I'd like to add that to my CAD files. I'm hoping to sort of document all the various changes and variants in a single OnShape document. ﻿


---
**Bryan Weaver** *July 27, 2015 04:12*

Check out Walter Hsiao's designs on thingiverse. He's got modified bed supports and z-axis supports to use the ball screws from aliexpress.



[http://www.thingiverse.com/walter/designs/page:1](http://www.thingiverse.com/walter/designs/page:1)


---
*Imported from [Google+](https://plus.google.com/111820797809026464429/posts/E9UUdwAmzA9) &mdash; content and formatting may not be reliable*
