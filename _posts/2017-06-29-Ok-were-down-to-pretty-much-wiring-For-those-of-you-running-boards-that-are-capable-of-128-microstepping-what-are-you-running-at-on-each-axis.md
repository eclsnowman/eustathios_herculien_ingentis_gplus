---
layout: post
title: "Ok, we're down to pretty much wiring ;) For those of you running boards that are capable of 128 microstepping, what are you running at on each axis?"
date: June 29, 2017 01:10
category: "Discussion"
author: James Ochs
---
Ok, we're down to pretty much wiring ;)



For those of you running boards that are capable of 128 microstepping, what are you running at on each axis?  It seems like overkill for the extruders and Z axis... If you are running at 128 on all axes, have you encountered any issues?  I've got the radds board with the raps128 drivers.





**James Ochs**

---
---
**Eric Lien** *June 29, 2017 01:19*

I agree, anything more than 32x on extruder or Z is overkill. Extruder already has a gear-reduced stepper, and Z has pulley reduction. 


---
**Ryan Carlyle** *June 29, 2017 01:59*

Calculate your microstep/second rate at 1/128 for your desired drivetrain and speeds, and you'll probably find that you're way over the step pulse generation frequency limits for your controller. As a starting point, I would try to stay below 80khz for 32bit boards or 40 khz for 8bit boards, although there's some firmware-specific stuff in there to worry about such as double/quad/octo-pulsing the steppers to make up for high step frequencies. 



For a point of reference, I run 1/32 stepping with THB6128 drivers on 0.9 degree steppers with a RADDS/Due. 


---
**James Ochs** *June 29, 2017 16:14*

Thanks Ryan, I've been playing a bit with your stepper simulator too... Nice bit of work there, thanks for putting it out!


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/ToKkshcFBVp) &mdash; content and formatting may not be reliable*
