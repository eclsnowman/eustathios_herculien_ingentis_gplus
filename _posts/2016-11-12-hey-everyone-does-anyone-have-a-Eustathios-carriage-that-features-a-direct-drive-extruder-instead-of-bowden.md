---
layout: post
title: "hey everyone does anyone have a Eustathios carriage that features a direct drive extruder instead of bowden?"
date: November 12, 2016 01:04
category: "Discussion"
author: Derek Schuetz
---
hey everyone

does anyone have a Eustathios carriage that features a direct drive extruder instead of bowden? I dont really print that quickly and want to be able to use flexible. and a floating extruder just seems like too much 





**Derek Schuetz**

---
---
**Oliver Seiler** *November 12, 2016 01:56*

Have a look here 

[http://www.thingiverse.com/thing:893849](http://www.thingiverse.com/thing:893849)



[thingiverse.com - Space Invaders Carriage (Eustathios) by walter](http://www.thingiverse.com/thing:893849)


---
**Oliver Seiler** *November 12, 2016 01:56*

Also this one

[http://thrinter.com/cartesian-flying-extruder/](http://thrinter.com/cartesian-flying-extruder/)

I'd be keen to give that a try myself but haven't found any more detailed plans/part list though...



[thrinter.com - Cartesian Flying Extruder – Thrinter](http://thrinter.com/cartesian-flying-extruder/)


---
**Oliver Seiler** *November 12, 2016 08:40*

Should have added the original space invaders extruder (first link) allows for mounting a direct drive extruder (not the flying extruder setup)


---
**Norr Norr** *March 07, 2017 12:20*

الله


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/Ydsb76TUJcC) &mdash; content and formatting may not be reliable*
