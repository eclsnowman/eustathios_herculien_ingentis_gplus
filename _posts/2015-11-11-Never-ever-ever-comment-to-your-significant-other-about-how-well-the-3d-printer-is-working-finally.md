---
layout: post
title: "Never, ever, ever, comment to your significant other about how well the 3d printer is working finally"
date: November 11, 2015 06:22
category: "Discussion"
author: Ben Delarre
---
Never, ever, ever, comment to your significant other about how well the 3d printer is working finally. You're just asking for trouble.



Just spent 2hrs cleaning the nozzle and extracting tiny bits of PETG from inside my bondtech extruder. Somehow it managed to push the filament out the side of the extruder casing, kinked and broke off a piece inside the bowden connector and clogged the nozzle all in one go. This was about 3hrs into a 4hr print. Very annoying.



Really not having much luck with this eSun PETG, almost always getting clogging or bed adhesion problems. Though I am still printing on a cold bed glass bed so that's probably part of it. This was printing at 255c at 60mm/min. 





**Ben Delarre**

---
---
**Ray Kholodovsky (Cohesion3D)** *November 11, 2015 06:28*

60 is probably too fast. I'm finally getting decent results at 45mm/sec at 245 degrees, although your 255 is probably better. 



I used to print at 260 at 85mm/sec (my typical PLA speeds), there were issues to say the least. Haven't had any issues with contamination though. Just a spring loaded extruder lever and an E3Dv6 over here. ﻿



More thoughts: heated bed at 70-90 degrees, aqua net hairspray before each print helps a lot. Prints pop off by themselves once the bed cools. ﻿



Your bondtech is very possibly exerting way too much force on the filament and these are the results. 


---
**Ben Delarre** *November 11, 2015 06:30*

Thanks for the tip. I might give a slower speed a go this weekend. Can't wait to get my new CNC up and running over thanksgiving so I can finally get the aluminum bed cut. More machines ftw!


---
**Ray Kholodovsky (Cohesion3D)** *November 11, 2015 06:38*

Yeah, just crank the feedrate down for the time being or print something that deliberately gets slowed down like the 5mm steps cube or something else with thin walls. You'll see great results. I really think 45 is the magic speed here. 



I've been doing trials for PushPlastic's new PETG filament the last few days. Pics (the yellow is a good esun petg print for comparison) here: [https://goo.gl/photos/pun9HxmuRQV8cCmX8](https://goo.gl/photos/pun9HxmuRQV8cCmX8)



Can't even talk about CNCs. I have 2 frames sitting here - one 12x12 fully built and one 24x48 half built OX - that I just never got around to running, but I need to prototype metal plates for my new printer design. 


---
**Jim Stone** *November 11, 2015 06:38*

Esun filaments and MG chemicals are imo. To be stayed away from. Tolerances are shit  ( or were) and they just aren't great in general. 



If you live in the states a filament producer that has the tightest tolerances I know of is there.



Atomicfilament.com check em out.



They also use only virgin plastics unlike maker geeks and the likes


---
**Ray Kholodovsky (Cohesion3D)** *November 11, 2015 06:50*

**+Jim Stone** I think you're generalizing a bit much. I would agree with your perspective that esun may not be the right fit for someone who just wants to plug and print, but very few materials are. I've had very few issues with the esun PLA from microcenter (Inland brand) - I've learned to print it hotter than normal for the layers to stick together strongly, and there was one incident of filament being too thick in one point causing a jam. Other than that it's been very close to 1.7mm. These quirks are ok to me considering the great value that this material is at $15/kg. 

A lot of people are delighted with the filament they get from ToyBuilder Labs and also from Prototype Supply. I can tell you for a fact that ToyBuilder sells rebranded esun (think private labeling) and I have a very strong suspicion that Prototype Supply does as well based on how the spool itself is manufactured, the design and placement of the identification stickers, the recommended processing temperatures, and the available colors. 


---
**Ben Delarre** *November 11, 2015 06:53*

I'm not sure it's the filament to to be honest.  Probably my own ineptitude. Bad calibration of the pid perhaps (I always get more ooze than I really should) , wrong print speeds or perhaps something to do with my lack of heated bed. 


---
**Ray Kholodovsky (Cohesion3D)** *November 11, 2015 07:00*

**+Ben Delarre** I wouldn't be so quick to make that judgement - none of those things would directly have the effect you describe. And even printing too fast never resulted in a jam for me, just a bubbled and poor quality printout. 



As long as your print isn't coming off your not heated bed, you should be fine. They do advertise the ability to print come onto blue tape, but I never trusted that. 



Just slow down (ha ha) and take it one step at a time, let us know what progress you make in diagnosing the issue. 


---
**Isaac Arciaga** *November 11, 2015 07:05*

Regardless of which PETG brand you try, it's best to print it with a heated bed. Some can get away without it printing small parts, but most will give up trying.



**+Jim Stone** I agree (full disclosure, he is a friend), Atomic Filaments is quality. Top notch. He extrudes every bit of material himself and takes pride on keeping very tight tolerances and consistency. His PETG that hasn't hit the market yet (but soon) is probably the best i've used to date. 


---
**Ben Delarre** *November 11, 2015 07:35*

Yeah. I think I'll leave the PETG alone till I get the heated bed together.


---
**Ryan Carlyle** *November 11, 2015 15:46*

Esun's PLA and ABS are top notch... but their PETG is newer (and apparently harder to manufacture) so it still has a lot of diameter variation. It's not hard to calibrate your printer on a 1.70mm section and a few feet later hit 1.80mm and suddenly you're over-extruding and jamming and covering the nozzle in molten plastic. So err on the side of under-extruding to be safe.



PETG also definitely needs to be printed slower than other filaments, not in terms of feedrate but in VOLUME FLOW RATE... calculate extusion width * layer height * feedrate and keep that number below, say, 4 mm^3/sec. (Your printer's limit will depend on your printing temp, nozzle size, and extruder drive.)



Light print cooling airflow (less than PLA but more than ABS) and a ~70C heated bed will significantly improve results. 


---
**Jason Smith (Birds Of Paradise FPV)** *November 11, 2015 16:16*

Just tried upping the nozzle temp on some ESUN PETG from 245C to 255C, and wow, what a difference.  Much better.  Using E3D Volcano with .4mm nozzle.  40mm/s, 85C bed, .1mm layer height, fan on 100% on 2nd layer.


---
**Ben Delarre** *November 11, 2015 16:19*

Huh.  I was under the impression you wanted no cooling on petg. I will give that a  go next time. 


---
**Jason Smith (Birds Of Paradise FPV)** *November 11, 2015 16:21*

Well, my fan is weak, so YMMV with the cooling. I did try it with no fan, and that was a disaster :)


---
**Jason Smith (Birds Of Paradise FPV)** *November 11, 2015 16:26*

[https://drive.google.com/file/d/0B2629YCI5h_wU0lXXzR5VGE3eFE/view?usp=sharing](https://drive.google.com/file/d/0B2629YCI5h_wU0lXXzR5VGE3eFE/view?usp=sharing)


---
**Eric Lien** *November 11, 2015 17:53*

Yeah, I run PETG from Esun around 250 at 45mm/s up to 260 at 80mm/s. The only one I have fought jamming on was the clear PETG from microcenter. It gets soft at a lower temp so is more prone to swelling as it enters the heartbreak causing jams.



But the newer solid colors from Esun (Solid Red, Solid Black) work at much higher speeds. Also my rolls of Esun have had great dimensional consistency over the roll.


---
**Chris Brent** *November 11, 2015 22:36*

When you're all talking speeds are talking about outside/external perimeters? 45mm/s seems slow for infill and extra perimeters.

 I've been run Hatchbox PETG on my new delta at 70mm/s perimeters and 35mm/s on external perimeters with Slic3r doing auto speed for infill. Bed @ 70C and somewhere between 245 and 250 for the hot end. I'm still fighting some oozing and a little stringing but I'm super happy so far. I did finally managed to block my E3D V6 trying to print at 100mm/s infill and faster perimeters. So far I haven't used a fan, but I finally rigged one up so I'll see if that helps the bridging issues I had on Benchy. I love PETG so far, although I've never used PLA as my old Thing-O-Matic was always used ABS. You do want to give it a couple of minute to cool before you pull it off the bed, it's a little soft if you go at it straight away. Benchy here was run a little slower and @ 255C. [https://plus.google.com/+ChrisBrent/posts/Zn4wkybv1u9](https://plus.google.com/+ChrisBrent/posts/Zn4wkybv1u9)


---
**Eric Lien** *November 11, 2015 23:02*

**+Chris Brent** I have found about the same in regards to PETG. For stringing try just a tad hotter like 255C. Often stringing can be a result of ooze due to back-pressure. My stringing all but disappeared after adding a little coast at the end (simplify3d), slightly longer retract, and increased temps.


---
**Chris Brent** *November 11, 2015 23:57*

Thanks **+Eric Lien** I'll try bumping up the C's again. My assumption was that the stringing was because the plastic was getting viscous from heat, but I can why the back pressure could cause this too. I'm too impatient/time constrained to print multiple things and only alter one parameter :) You reminded me that I need to get Simplify3D going with this printer. I had some weird issues on the ToM so I've shied away from it, but it's time to try again.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/VHLeKznSYQF) &mdash; content and formatting may not be reliable*
