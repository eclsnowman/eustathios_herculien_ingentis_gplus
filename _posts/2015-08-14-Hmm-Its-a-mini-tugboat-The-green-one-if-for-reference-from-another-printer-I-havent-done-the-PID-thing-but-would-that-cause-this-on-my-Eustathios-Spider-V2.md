---
layout: post
title: "Hmm. It's a mini tugboat? (The green one if for reference from another printer) I haven't done the PID thing but would that cause this on my Eustathios Spider V2?"
date: August 14, 2015 20:07
category: "Discussion"
author: Brandon Cramer
---
Hmm. It's a mini tugboat? (The green one if for reference from another printer)



I haven't done the PID thing but would that cause this on my Eustathios Spider V2?

![images/3f756d84b443c5bdb0be0c04f3b4fda9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3f756d84b443c5bdb0be0c04f3b4fda9.jpeg)



**Brandon Cramer**

---
---
**Erik Scott** *August 14, 2015 20:09*

Z steps per mm is wrong. 


---
**Frank “Helmi” Helmschrott** *August 14, 2015 20:10*

You may probably look at your Z steps/mm. Looks like they are wrong. 


---
**Brandon Cramer** *August 14, 2015 20:12*

Where do I check for Z steps per mm?


---
**Jason Smith (Birds Of Paradise FPV)** *August 14, 2015 20:12*

The black boat is built for speed.


---
**Erik Scott** *August 14, 2015 20:18*

**+Brandon Cramer**​ check the configuration file for your printer board. Also have a look at the prusa calculator. Hom many teeth do you have on the leadscrew pulleys and the Z -axis motor pulley? 


---
**Brandon Cramer** *August 14, 2015 20:32*

32 teeth is what I ordered. I ordered (3) 20 teeth which I assume is what I put on the nema motors. 


---
**Erik Scott** *August 14, 2015 20:39*

Okay, go to the prusa calculator and put in your info: steps per rev (200 or 400), micro stepping (1/16 or 1/32 or whatever), screw pitch (2mm) and gear ratio (20:32). It should spit out something like 2560 or 5120 steps/mm. 



Before printing another boat, print a simple 20mm calcube just to make sure you have the stepping right. Once that's good, then think about tuning your accel and speeds using the boat. 


---
**Brandon Cramer** *August 14, 2015 20:45*

**+Erik Scott** I see on the Prusa calculator they don't have 1/32. That got me to thinking I should set mine to 1/16.



Looking in the config file now, Z is set for 2560 per mm. Maybe just changing the micro stepping to 1/16 will fix my issue?


---
**Eric Lien** *August 14, 2015 21:17*

**+Brandon Cramer**​​ I run lower microstepping on Z (1/16) even though the board can do more, in fact I only print in layer heights that are even full steps anyway so microstepping in unnecessary, but it makes the steppers less noisey.



You can calculate your the optimal layer heights in the prusa calculator.﻿


---
**Brandon Cramer** *August 14, 2015 21:23*

**+Eric Lien** Most of this is like a foreign language to me. Are you saying I shouldn't have any of the jumpers on the micro stepping pins? After changing the micro to 1/16 it does make this annoying sound when it moves to the next layer. Obviously, I'm printing this 20mm cube so I will hear it a lot more than normal. 



If there is a different way, I'm not aware of it. 


---
**Brandon Cramer** *August 14, 2015 21:30*

**+Erik Scott** So the cube is good on Z now. But X and Y are 19.43mm?


---
**Erik Scott** *August 14, 2015 22:31*

If you have motors that do 200 steps/rev, you'll want to set your X and Y steppers to 80 steps/mm with 1/16 microstepping or 160 steps/mm for 1/32 microstepping. If your motors do 400 steps/rev, then double those numbers. If things are coming out small, check your extrusion settings and make sure you're not under-extruding. 


---
**Eric Lien** *August 14, 2015 22:54*

**+Brandon Cramer** do a single wall extrusion width calibration to confirm your extrusion multiplier is correct.




{% include youtubePlayer.html id=cnjE5udkNEA %}
[https://youtu.be/cnjE5udkNEA](https://youtu.be/cnjE5udkNEA)


---
**Eric Lien** *August 14, 2015 22:59*

**+Brandon Cramer** I was just saying you should select layer height that end up as full steps not microsteps. For example on a Eustathios 0.2080 is a layer height of exactly 13 full stepper steps, but .200 layer height is 12.5 steps so it is being held in a microstep. Try choosing layer heights that are full steps. You can calculate it easily on the prusa calculator.


---
**Brandon Cramer** *August 14, 2015 23:54*

So this is like a bad Christmas experience... You get that awesome toy that you want to play with but it doesn't have any batteries. This 25 watt heater is killing me. It is struggling to get to 215C. I want to do all this testing and play with my printer but it can't get up to 215C and if it ever does it takes 10 minutes I swear...



I printed this solid 20mm cube and the layers pulled apart when I removed it from the bed. Which I can only assume is because I adjusted the temp down to 210C to actually be able print something. 



The 40 watt heater I ordered from somewhere across the Atlantic Ocean... got shipped to my address without an apartment number on it. So they shipped it back. Oye!!!! So I ordered (2) 40W heaters on ebay this morning.



Guess I will just walk away for the weekend. 



Thanks for your help **+Eric Lien** and **+Erik Scott** I appreciate it. 


---
**Erik Scott** *August 15, 2015 00:22*

If the layers pulled apart, you may be under extruding. Watch this video: 
{% include youtubePlayer.html id=YUPfBJz3I6Y %}
[https://www.youtube.com/watch?v=YUPfBJz3I6Y](https://www.youtube.com/watch?v=YUPfBJz3I6Y) and make sure you have the correct filament diameter selected. 215C should be enough to print PLA, if that's what you're using. 


---
**Eric Lien** *August 15, 2015 01:15*

**+Brandon Cramer** have re tuned the PID. If not it is likely throttling the heater because my values on the github are for the 40watt heater.



Also mark the filament, then extrude 100mm at temp through the hotend ( via octoprint or whatever you are using) then measure how much actually came out. You may be slipping.


---
**Ian Hoff** *August 15, 2015 23:39*

I hang out in IRC and I recommend A specific guide for tuning rather often, since its not very hardware specific. You probably can skip most of it since xyz steps are calculated, and just pay attention to the extruder portions that deal with determining proper temp, and calibrating Esteps.



[http://reprap.org/wiki/Triffid_Hunter%27s_Calibration_Guide](http://reprap.org/wiki/Triffid_Hunter%27s_Calibration_Guide)


---
**Brandon Cramer** *August 17, 2015 15:40*

**+Eric Lien** I put a piece of tape on the filament and extruded 100mm but only 1/2 of that extruded.....


---
**Eric Lien** *August 17, 2015 16:01*

**+Martin Bondéus**  recomends 491 steps/mm on 1.75 filement. My testing on my units has shown more like 486 steps/mm. So you just need to make sure you have your steps/mm in firmware and microstepping on the board jumpers set correctly.


---
**Brandon Cramer** *August 17, 2015 16:04*

**+Eric Lien** I changed the jumper from 1/32 to 1/16. I'm wondering if I should have everything set to 1/32 and change the config file instead? I think this is starting to make sense. 



Once I changed it to 1/16 it was a lot closer to 100mm extruded. 


---
**Eric Lien** *August 17, 2015 16:07*

You can certainly do that. I run a few items at lower micro-steps on purpose, like the Z axis, and the extruder. But there is nothing stopping going to 1/32 all around. 


---
**Brandon Cramer** *August 17, 2015 16:13*

I think that is what I have set now. Both Z Axis and the extruder are set for 1/16. I will leave them be. I'm printing this solid 20mm cube to see if I'm close to those specs. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/3Gf5oq6THSk) &mdash; content and formatting may not be reliable*
