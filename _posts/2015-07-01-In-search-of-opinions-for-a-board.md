---
layout: post
title: "In search of opinions for a board"
date: July 01, 2015 18:33
category: "Discussion"
author: Luis Pacheco
---
In search of opinions for a board. I followed the HercuLien BOM and went with a Azteeg X3 V2 but I ended up receiving a bad board. It's been weeks that I've been trying to get any replies from Panucatt for a replacement but it seems they don't believe in customer service.



Does anyone have an opinion on a different board I can get for the printer running 24v?



In the picture (Sorry for the mess, I promise it'll be cleaner once it's running.) I have been testing the motors using a spare RAMPS board that I have for my other printer but it is a 12v board from what I read.



I'm anxious to get this up and running and the board is the only thing holding me back!



Thanks!

![images/7a0d3c9edbc5aedcf3724811bd0190c8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7a0d3c9edbc5aedcf3724811bd0190c8.jpeg)



**Luis Pacheco**

---
---
**Dat Chu** *July 01, 2015 19:08*

You can get a SmoothieBoard. With a vreg, you can run at 24 or 12 easily. Uberclock sells them.


---
**Dat Chu** *July 01, 2015 19:09*

Those are very nice angle brackets you got **+Luis Pacheco** . Can you tell me where you got them from?


---
**Luis Pacheco** *July 01, 2015 19:49*

**+Dat Chu** What do you mean by a vreg? I have the 24v power supply, could I not run it straight to the board? The angle brackets were sourced through a sheet metal supplier we use here at work (S&B Metal Products). Honestly, they were expensive for what they are but this printer has been a work-funded project.


---
**Dat Chu** *July 01, 2015 19:54*

[http://shop.uberclock.com/products/switching-regulator-dc-to-dc-5v](http://shop.uberclock.com/products/switching-regulator-dc-to-dc-5v)



That way you can run at 24V or at 12V


---
**Vic Catalasan** *July 01, 2015 20:17*

I have 2 Azteeg X3 V2 and both are working fine even with the Viki 2 panels hooked up to them. It took a lot of time for me to hook them up and get them programmed because it was my first time seeing a 3D printer board and not to mention the firmware was daunting at first.


---
**Godwin Bangera** *July 01, 2015 20:18*

oh damn ... so there are chances of panucatt boards being bad ?? Damn, i had bought 3 pro boards with drivers etc, and i have not even tested them ... hope nothing is wrong with my boards (fingers crossed )   :|


---
**Eric Lien** *July 01, 2015 21:30*

**+Luis Pacheco** 



Roy at Panucatt is a great guy, but I have found it is best to reach him at night. I also know he is working hard on a new project, so it may just be a matter of being over loaded.



I have a spare Azteeg x3 since I upgraded my Eustathios to smoothie. I could send you it to get going. Where are you located.


---
**Eric Lien** *July 02, 2015 02:32*

Also I just sent a message to Roy that you are looking for a reply.


---
**Luis Pacheco** *July 02, 2015 11:39*

**+Eric Lien** I have actually talked to him over the phone about a week or so ago and he does seem like a genuinely nice guy. Just a little frustrating when I get one reply back every week-week and a half. I did receive an email back last night/this morning from Roy saying he sent a new board out but thanks for the offer!


---
**Luis Pacheco** *July 02, 2015 11:41*

**+Eric Lien** **+Godwin Bangera** I'm not quite sure what went wrong with the board, the only way to get any lights on it to come on is via USB but the computer will not recognize it. If I power via the power supply I get nothing on the board, no lights or anything. (And I did switch jumpers to make sure I was selecting the correct setting)


---
**Chris Purola (Chorca)** *July 06, 2015 07:04*

Just to throw it out there, **+Dat Chu** , Openbuilds sells some nice brackets for pretty cheap for v-slot/t-slot!

http://openbuildspartstore.com/brackets/


---
**Dat Chu** *July 06, 2015 13:05*

Thank you **+Chris Purola**​, the open build brackets are for joining together parts. The bracket for Herculien is holding together the z stage. It's much bigger and requires custom drillings. 


---
*Imported from [Google+](https://plus.google.com/110276424073473119972/posts/Mhz58HWvcgF) &mdash; content and formatting may not be reliable*
