---
layout: post
title: "I'm not sure if these are at retract or restart locations, but they appear on the print just as advertized on the g-code preview"
date: May 11, 2017 20:16
category: "Discussion"
author: Benjamin Liedblad
---
I'm not sure if these are at retract or restart locations, but they appear on the print just as advertized on the g-code preview.



Any advice for getting rid of these (besides sanding)?



![images/6d67977ccd58f7634522fe1e7c4e9ff0.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6d67977ccd58f7634522fe1e7c4e9ff0.png)



**Benjamin Liedblad**

---
---
**Ted Huntington** *May 11, 2017 20:50*

Is that Cura and RepetierHost?


---
**Benjamin Liedblad** *May 11, 2017 20:52*

No... Slicing with Simplify3d. Running on OctoPrint. 


---
**Eric Lien** *May 11, 2017 22:54*

Try inside->out, and outside->in for perimeters. Compare the two and let me know which you like better.



I already know the answer, I Just want to see if it makes the difference I am expecting :)


---
**Sébastien Plante** *May 12, 2017 17:59*

isn't this related to the 3D model (inverted face) ?


---
**Stefano Pagani (Stef_FPV)** *May 15, 2017 14:59*

just uncheck the box on the left that says show retractions


---
*Imported from [Google+](https://plus.google.com/111192213763748051453/posts/cXDhk1JLcXC) &mdash; content and formatting may not be reliable*
