---
layout: post
title: "Thanks to all who commented on cutting down the aluminium profile"
date: January 10, 2015 17:32
category: "Show and Tell"
author: Richard Mitchell
---
Thanks to all who commented on cutting down the aluminium profile. I spent a little time squaring the blade on my chop saw (it's an Evolution RAGE saw and went through the extrusion no problem)



The rather basic clamping endstop to cut all the pieces to the same length worked nicely and I have 10 300mm lengths and 4 400mm lengths for my printer with 1400mm left over for the z bed.



For the t nuts after a bit of experimentation and printing a few designs from thingiverse (none worked or seemed strong enough) I've settled on just full size M5 nuts with two edges ground down on a bench grinder (you could also do this via filing but that was talking too long). A M5 half nut spun in the slot and a washer and half nut didn't fit for me.



I plan on connecting the lengths by tapping the centre with an M5 tap and using M5x16 button cap bolts tightened though a hole I'll drill through the extrusion.



![images/7e4a0f424864395f22cd1026a24f36a0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7e4a0f424864395f22cd1026a24f36a0.jpeg)
![images/da2ff4347755da4778faf40fc5256c01.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/da2ff4347755da4778faf40fc5256c01.jpeg)
![images/a76d7bbeddc7596df3d4b67277a8da3e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a76d7bbeddc7596df3d4b67277a8da3e.jpeg)
![images/38b19fcf5f07054cedec2c6d02ba7830.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/38b19fcf5f07054cedec2c6d02ba7830.jpeg)

**Richard Mitchell**

---
---
**Florian Schütte** *January 10, 2015 17:36*

I like the M5 nuts hack :)


---
**Jim Wilson** *January 10, 2015 17:54*

Not bad! Be careful during assembly though, its REALLY easy to forget you need a nut in the middle of a build and if you can't slide the nuts into the slit directly, only from the ends, then its easy to build yourself into a corner and have to take parts off to get more nuts slid into place.﻿



Edit: Sorry, to clarify, this is if you need to slide these nuts in from the end as opposed to the kind that can roll directly into the slots from the face.


---
**Richard Mitchell** *January 10, 2015 17:55*

I'm all about the easy and cheap way of doing things 😇﻿


---
**Richard Mitchell** *January 10, 2015 17:59*

I was planning on assembling finger tight first and only tightening everything up once it's all in place so I can dismantle and add nuts without damaging things.



For smaller pieces later I may design a 3D printed t nut that can be inserted into the slot directly and accept an M3 bolt.


---
**Eric Lien** *January 10, 2015 18:14*

I really like the button head method with a 5mm hole in the other extrusion for an Allen wrench. It saves a lot on corner brackets and is suprizingly ridged. I found making drill guides for the through holes ensures proper alignment and makes the process much quicker.


---
**Mike Thornbury** *January 10, 2015 22:17*

As to t-nuts, buy roll-in nuts, then you won't have the problem of forgetting, plus they stay where you put them, so you can lay them out.



They are like 20c vs 15c for the slide-in ones.﻿



[http://www.tslotparts.com/fasteners-intro.asp](http://www.tslotparts.com/fasteners-intro.asp)


---
**Mike Thornbury** *January 10, 2015 22:21*

Also, to save your sanity, look at self-tapping m5 bolts... Just drill the end to 4mm and drive her in.



I've stopped tapping entirely.


---
**Mutley3D** *January 11, 2015 04:59*

If you use thin nuts, you can drop themm in, or....drill a hole into the edges of the extrusion ie not all the way through, just to make an entry point to drop normal m5 nuts in helps to conquer those moments when you realise youve put in one too few nuts.


---
*Imported from [Google+](https://plus.google.com/111547506541230089782/posts/2HbM8wQmx4c) &mdash; content and formatting may not be reliable*
