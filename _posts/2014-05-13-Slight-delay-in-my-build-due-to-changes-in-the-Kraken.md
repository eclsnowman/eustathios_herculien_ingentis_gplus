---
layout: post
title: "Slight delay in my build due to changes in the Kraken"
date: May 13, 2014 05:18
category: "Discussion"
author: Brian Bland
---
Slight delay in my build due to changes in the Kraken.  Can't use **+Tim Rastall** carriage.  Mounting holes are 1mm farther apart in the longer direction and the coolant barbs are closer together so the hoses would rub on the x shaft.



Anybody have any ideas on a mount for the Kraken?





**Brian Bland**

---
---
**Tim Rastall** *May 13, 2014 06:49*

I can probably adjust the carriage. I also have a new version that uses lm8uu instead of bronze bushings if you'd prefer. 


---
**Brian Bland** *May 13, 2014 06:58*

I would like to see what you have.  The Kraken drawing on E3D's website has the correct mounting hole dimensions, but it still shows the barb holes in line with the bowden holes.  The barb holes are now inboard of the bowden holes.


---
**Jarred Baines** *May 13, 2014 12:33*

Lm8uu carriage sounds good, have you tried it? Any better / worse in performance?


---
**Eric Lien** *May 13, 2014 23:55*

I would like to see the lm8uu version also.


---
**Tim Rastall** *May 16, 2014 08:01*

I've not tried the design.  Swapped out the Kraken for the V6 E3d I'm testing.  Should work well and it's very modular/ easy to print. The lm8uus should perform just like the Ultimaker ones. They'll likely be noiser but maintain alignment better and not wear down like bronze bushings. 


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/NLXXFdob2hB) &mdash; content and formatting may not be reliable*
