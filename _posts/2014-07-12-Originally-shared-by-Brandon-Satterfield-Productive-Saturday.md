---
layout: post
title: "Originally shared by Brandon Satterfield Productive Saturday!"
date: July 12, 2014 21:45
category: "Show and Tell"
author: Brandon Satterfield
---
<b>Originally shared by Brandon Satterfield</b>



Productive Saturday! 



![images/18268c63a560b1d906fec7f47c98653c.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/18268c63a560b1d906fec7f47c98653c.gif)
![images/56eaeec12e24168cba92afa6ac42461f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/56eaeec12e24168cba92afa6ac42461f.jpeg)
![images/4f1a555db5dee77f7a44015d4c8ad13d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4f1a555db5dee77f7a44015d4c8ad13d.jpeg)
![images/da8c093899cc18e134c47419e90a8d2b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/da8c093899cc18e134c47419e90a8d2b.jpeg)
![images/ab53838a3f378d4d549f1ccb03a57c0c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ab53838a3f378d4d549f1ccb03a57c0c.jpeg)
![images/613a04619c6e3270467d9710d965a6a5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/613a04619c6e3270467d9710d965a6a5.jpeg)
![images/ee2a403024a0adf6baefb362f412952a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ee2a403024a0adf6baefb362f412952a.jpeg)
![images/4a4224526cfdeda31d3da9be36066a56.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4a4224526cfdeda31d3da9be36066a56.jpeg)
![images/84fa92492067c244e85ad71fc0370112.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/84fa92492067c244e85ad71fc0370112.jpeg)
![images/f749a98a307c5d0253fff5f815880f73.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f749a98a307c5d0253fff5f815880f73.jpeg)

**Brandon Satterfield**

---
---
**Robert Burns** *July 12, 2014 21:48*

You can hide a belt better than David Carradine


---
*Imported from [Google+](https://plus.google.com/111137178782983474427/posts/Fge1JgmPzHf) &mdash; content and formatting may not be reliable*
