---
layout: post
title: "On the ingentis bom for the 20 x 20 aluminum profile it says to ask for 8pieces instead of six"
date: November 30, 2014 21:31
category: "Discussion"
author: Ethan Hall
---
On the ingentis bom for the 20 x 20 aluminum profile it says to ask for 8pieces instead of six. Should I do this during checkout under the comments section or should I send the seller a message?





**Ethan Hall**

---
---
**David Heddle** *November 30, 2014 21:48*

If you send the seller a message they are usually very helpful


---
**Jim Wilson** *December 01, 2014 15:36*

That's where I did it. The seller then emailed me to reconfirm and I gave them the exact dimensions, got exactly what I asked for.


---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/VEYVc4uuLWj) &mdash; content and formatting may not be reliable*
