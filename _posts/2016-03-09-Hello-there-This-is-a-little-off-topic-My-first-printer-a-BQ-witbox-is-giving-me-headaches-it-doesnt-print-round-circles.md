---
layout: post
title: "Hello there, This is a little off-topic: My first printer, a BQ witbox is giving me headaches, it doesn't print round circles"
date: March 09, 2016 17:36
category: "Discussion"
author: Maxime Favre
---
Hello there,



This is a little off-topic:

My first printer, a BQ witbox is giving me headaches, it doesn't print round circles. I printed a test piece, see photo. Straight lines and infill is not best quality, but fine. However the circles are another story:

- They're printed clockwise, see the angles at 13 and 19 o'clock.

- This one is at 60mm/s, same results with different speeds. 

- Cura, slic3d or Simplify3d, same results

- Hardware seems fine, no belt or hotend play.

Do you have any idea where to start searching ?

- Replace x-y steppers ?

- Replace electronics ?



If this is too far from the group's topic, feel free to remove this post ;)

![images/51640f46c1358bc2b988a63df2803525.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/51640f46c1358bc2b988a63df2803525.jpeg)



**Maxime Favre**

---
---
**Eric Lien** *March 09, 2016 17:41*

If this is the orientation it printed, looks like backlash in Y (bed direction reversing is not crisp, and that lag causes the lack of roundness).


---
**Miguel Sánchez** *March 09, 2016 17:43*

Loose pulley?


---
**Maxime Favre** *March 09, 2016 17:48*

Yes the picture is as printed, X horizontal, Y vertical


---
**Maxime Favre** *March 09, 2016 18:04*

**+Miguel Sánchez**  Pulleys are ok, I checked first. In this case I think it should be visible on squares and infill.

**+Eric Lien**  good point, I'll check the Y axis. Maybe the Y belts are worn out and there's some play with the pulleys


---
**Eric Lien** *March 09, 2016 18:07*

Try and engauging the Y stepper so it's locked and then try and move the bed. You can then determine if it's perhaps the glass moving on the bed, or the bed moving on the belt clamp, or the pulley moving on the motor, etc.


---
**Miguel Sánchez** *March 09, 2016 18:07*

**+Maxime Favre** If you draw a line with a dark marker across shaft and pulley side you can double check that line is never changing alignment. It is not uncommon that it looks ok on first inspection but a loose grub screw allows certain play only when motion direction changes. Squares will not be affected on their squareness but measure would be a bit off.


---
**56 s_0** *March 09, 2016 18:20*

Also make sure your X- and Y-axis' are square.


---
**Walter Hsiao** *March 10, 2016 00:17*

If you watch Tom's review of the witbox, he blames it on the stickness of the igus bushings.  
{% include youtubePlayer.html id=7KcdMEmCj0g %}
[https://www.youtube.com/watch?v=7KcdMEmCj0g&list=UUb8Rde3uRL1ohROUVg46h1A](https://www.youtube.com/watch?v=7KcdMEmCj0g&list=UUb8Rde3uRL1ohROUVg46h1A) (half way through the video).


---
**Eric Lien** *March 10, 2016 00:38*

**+Walter Hsiao** that would make sense. I had similar issues on my old corexy using pbc linear bearing that are similar to the Igus type.


---
**Maxime Favre** *March 10, 2016 09:41*

**+Walter Hsiao** Thanks for the link ! I'll look for replacing thoses igus sticky things. Sadly I bought the printer 10 month before his review, it maybe would have changed my mind. 


---
**Chris Brent** *March 11, 2016 04:19*

Could you try **+Mike Miller**'s trick posted here [https://plus.google.com/+MikeMiller0/posts/RdWbwVSw3Es](https://plus.google.com/+MikeMiller0/posts/RdWbwVSw3Es) ?


---
**Mike Miller** *March 11, 2016 04:51*

That's only for carriages that drag upon initial assembly...IGUS bushings seem to have non-circular prints of a certain size, I had no issues with larger circles, or really small holes...larger non circular issues may be caused by un matched belt tension. 


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/6w3rpXAjfpK) &mdash; content and formatting may not be reliable*
