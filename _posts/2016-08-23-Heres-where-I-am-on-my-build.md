---
layout: post
title: "Here's where I am on my build"
date: August 23, 2016 22:11
category: "Build Logs"
author: Stefano Pagani (Stef_FPV)
---
Here's where I am on my build. I'm using an X3 with dual saintflint's powering a Chimera. Any advice/tips you guys have? This is my first printer build, but I've had 4 years experience with MakerGear m2's, prusia i3's and the hell on earth that makes up MakerBot (well the newer models at least) :P Do I put the whole carriage (with cross rods and all) onto the guide/drive rods as one piece​? or just put the bushing holders on and slide the cross rods on the those?



![images/ebda8b7dc4fcd2ecfa9033138c19e728.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ebda8b7dc4fcd2ecfa9033138c19e728.jpeg)
![images/854d15108ead15c2cb90c34f5859e441.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/854d15108ead15c2cb90c34f5859e441.jpeg)
![images/bad1f5f178ce132748e71b7c528a55eb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bad1f5f178ce132748e71b7c528a55eb.jpeg)

**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *August 23, 2016 22:53*

Assemble the side rod assemblies onto the frame, then put the cross rods into the carriage, and slide the cross rods into the side carriages. The side carriages can be kind of rotated up onto the cross rods then pushed in.


---
**Eric Lien** *August 23, 2016 22:57*

Then comes the fun part... Traming all those degrees of freedom onto printed parts which are by their nature slightly imperfect :)



Look back through old threads here and it has been discussed often how to properly tram the X/Y gantry.



Good luck, looks like you are getting very close :)


---
**Stefano Pagani (Stef_FPV)** *August 23, 2016 23:54*

**+Eric Lien** Thanks! Great to have such a supportive community :) 


---
**Eric Lien** *August 24, 2016 00:16*

It's the best community out there... But I might be biased :)


---
**Sean B** *August 24, 2016 03:18*

Trying to get my x y in now.  I kept trying to install the belts with the pulley set screws all tight.  Much easier when loose. <s>Insert stupid emoji here</s>




---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/ZcQbSk9Qb5c) &mdash; content and formatting may not be reliable*
