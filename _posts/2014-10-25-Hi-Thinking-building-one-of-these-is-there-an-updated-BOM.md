---
layout: post
title: "Hi Thinking building one of these, is there an updated BOM?"
date: October 25, 2014 08:07
category: "Discussion"
author: Keith Dibling
---
Hi



Thinking building one of these, is there an updated BOM?



Also is it designed for 10mm smooth rods for X and Y?





**Keith Dibling**

---
---
**Eric Lien** *October 25, 2014 12:02*

Jason Smith has his one on github. It has 90% of all things you need. I have been meaning to generate one from my solidworks assembly that includes all nuts and bolts. But free time is hard to find these days and my CAD station mobo fried this week. 


---
**Mike Miller** *October 25, 2014 13:27*

It's not designed for 10mm natively. The cross slides are expectgin (I think) 8mm, but with IGUS bushings and a drill, I made 'em work with 10mm. I also needed to make a temporary carriage with larger openings as the carriages are all looking for 8mm rods/bearings to pass through them. I haven't gotten around to iteration 2, which will have a fan for the plastic. 


---
**Keith Dibling** *October 25, 2014 14:26*

That's good is I have some 8mm rods and 10mm for z axis, but rods I have that support print head are only 6mm.






---
**Keith Dibling** *October 26, 2014 15:00*

Downloaded from YouMagine, some of the parts arnt manifold and netfabb can't repair.



Anyone got a repaired set of parts?


---
**Eric Lien** *October 26, 2014 15:33*

[https://github.com/eclsnowman/Lien3D_Eustathios_Spider/tree/master/Other%203D%20Formats/STL](https://github.com/eclsnowman/Lien3D_Eustathios_Spider/tree/master/Other%203D%20Formats/STL)


---
**Keith Dibling** *October 26, 2014 16:17*

Thank you Eric Lien 


---
**Eric Lien** *October 26, 2014 16:26*

If you have solidworks, or are able to open step there are full assembly models in that git repo


---
**Keith Dibling** *October 26, 2014 17:15*

Yes I have SolidWorks :)


---
*Imported from [Google+](https://plus.google.com/103631495948845103844/posts/AqAm62jbGrQ) &mdash; content and formatting may not be reliable*
