---
layout: post
title: "Are bushings generally used for both axial and radial motion?"
date: April 21, 2015 15:00
category: "Discussion"
author: Daithí Ó Corráin
---
Are bushings generally used for both axial and radial motion? From what I've seen the majority are designated neither, most others considered linear and the remaining minority axial and radial. My common sense tells me that the surface of the bushing facilitates the motion so a smooth bushing would work as well axially as radially (as opposed to a regular bearing or linear bearing) but only a small minority are explicitly for both directions . Please advise.. :-)





**Daithí Ó Corráin**

---
---
**Jeff DeMaagd** *April 21, 2015 21:33*

For bronze, teflon or Igus bushings, they can do either and both. It's when you do the ball bearing ones that you must be careful. The ones that can do unlimited linear <b>and</b> radial motion are super expensive.


---
**Daithí Ó Corráin** *April 22, 2015 13:17*

Thank you


---
*Imported from [Google+](https://plus.google.com/104923267981938346235/posts/aU2eCK9KHng) &mdash; content and formatting may not be reliable*
