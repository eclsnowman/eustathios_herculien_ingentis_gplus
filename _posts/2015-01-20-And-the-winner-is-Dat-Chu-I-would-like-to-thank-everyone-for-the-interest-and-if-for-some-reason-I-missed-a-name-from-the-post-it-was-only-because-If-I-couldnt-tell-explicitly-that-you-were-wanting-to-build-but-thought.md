---
layout: post
title: "And the winner is: Dat Chu I would like to thank everyone for the interest, and if for some reason I missed a name from the post it was only because If I couldn't tell explicitly that you were wanting to build, but thought"
date: January 20, 2015 04:50
category: "Discussion"
author: Eric Lien
---
And the winner is: **+Dat Chu** 



I would like to thank everyone for the interest, and if for some reason I missed a name from the post it was only because If I couldn't tell explicitly that you were wanting to build, but thought instead your comment was in support of the ideal. 



Dat: hit me up with any questions on your journey, and please post pictures and feedback of ideas for improvements. I have built this in a vacuum, so some fresh air and sunlight can only do good things.



The hardest part of the build is drilling the holes, for example where the rods come out the sides of the 20x80. It is half/on a rib in the extrusion so would be best suited to use a 1/2" end mill verses a standard drill bit. The reason is the tapered end of a standard bit tries to walk it off target due to the rib. It is doable with a vice and drill press (that's how I did it).



And when you are getting close to "Print It Forward" let me know how I can help.



Also note I have a few goodies to add since the last post. I printed the second extruder, second spool mount, extrusion drill-start guides, and the cable chain for routing wires from the frame to the heated bed. Also I will include an Omron G3NA_220B DC to AC SSR to run the heated bed. It is a high quality SSR, and perfectly rated to the task.



A note on the drill guides is use them to dimple the location, but avoid drilling the holes completely using them, since they are just PLA plastic. They are more to save measuring by locating the holes correctly. One is for the 20x20 vertical uprights, one gets the cross holes to hold the z vslot track to the frame, and one marks centers for where the shafts go through the 20x80 for the knobs and drive pulleys. 



![images/0255f404526844dd04eb1b9c4632721d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0255f404526844dd04eb1b9c4632721d.png)
![images/a05f3208e18094ac534118fcfc287fb8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a05f3208e18094ac534118fcfc287fb8.jpeg)

**Eric Lien**

---
---
**Dat Chu** *January 20, 2015 04:54*

Yay! Seriously, I never win any lottery. Ever. :)


---
**Eric Lien** *January 20, 2015 05:09*

**+Dat Chu** I hear ya, I never even won at bingo as a kid ;)


---
**Robert Cicetti** *January 20, 2015 16:44*

**+Dat Chu**

Congrats! Hope you can pay it forward soon.


---
**Dat Chu** *January 20, 2015 16:47*

Such is the plan. I should be able to print a PLA set with my current printer if you guys are into PLA. Is there a specific material requirements for these parts **+Eric Lien**​? 


---
**Eric Lien** *January 20, 2015 17:46*

With it enclosed I print most parts in ABS since the ambient heat is higher due to the heated bed. Plus I like ABS better unless the part requires rigidity like the lead screw bearing holders or spool holders.


---
**James Ochs** *January 20, 2015 20:10*

**+Dat Chu**  Nice!  Keep us posted on how the build goes!


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/YtJaA4EZi7K) &mdash; content and formatting may not be reliable*
