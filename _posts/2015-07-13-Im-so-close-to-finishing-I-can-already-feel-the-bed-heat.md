---
layout: post
title: "I'm so close to finishing, I can already feel the bed heat!"
date: July 13, 2015 18:22
category: "Discussion"
author: Luis Pacheco
---
I'm so close to finishing, I can already feel the bed heat! But I'm having some issues that I need to resolve before continuing. These issues seem to be firmware related, at least I think so.



The printer is as hooked up as it can be (1 extruder instead of 2) and I went ahead and flashed the firmware straight from the HercuLien github. Once I connect via Repetier-Host I get nothing. Homing doesn't work, no idle temperatures, fans stay off, almost like it's still waiting on something but never receives the go ahead.



I found an older marlin fw from an older printer build, flashed it, and everything works. I can move x/y/z, homing works great, idle temperatures are appearing and I can heat the hot end and bed. My only issue is that I still get no fan activity (blower and heat sink fan). I cannot continue with test printing without the fans since I had to print my carriage pieces out of PLA and I'm afraid that if I try to test print all I'll do is melt the carriage.



I've attached a copy of the zipped FW that seems to work, could any experienced builders be able to help me out on what may be missing on the FW or what I'm doing wrong?



Thanks in advance!





**Luis Pacheco**

---
---
**Eric Lien** *July 13, 2015 19:26*

Are you using the azteeg x3 controller? If so are you using the viki1 or Viki2 LCD. If you are using the viki2 you need one of the newer versions of Marlin. It will take some time to hand edit the new firmware with the right settings. But there are a few people who are running on newer versions of Marlin. Maybe they could post their firmware to try and load.



I would look the firmware over, but I am out of town right now.


---
**Luis Pacheco** *July 13, 2015 19:37*

At the moment I'm not using either. I'm currently using my laptop to control the printer, haven't hooked up the Viki1 yet.


---
**Ishaan Gov** *July 13, 2015 19:56*

Could it be something like a MINTEMP error that's being thrown by an unconnected second thermistor and locking everything up?


---
**Luis Pacheco** *July 14, 2015 11:58*

**+Ishaan Gov** It's possible that a MINTEMP could be locking it up from the unconnected second thermistor, the FW I linked to seems to work (sans fans) though. I can heat hot end and the bed, move x/y/z and home them properly.


---
**Ishaan Gov** *July 14, 2015 12:02*

How have you hooked up your heat sink and blower fans in your printer?


---
**Luis Pacheco** *July 14, 2015 14:35*

I have the fans hooked up the same way **+Eric Lien** noted on the Azteeg wiring PDF. One of them is connected to (D17)HOT-END 3 / FAN and the other to (D16)HOT-END 4 / FAN on the top cover.


---
**Eric Lien** *July 14, 2015 15:39*

The hotend fans do not turn on until 50C in my firmware.


---
**Luis Pacheco** *July 14, 2015 15:40*

I did kick the hot end up to 50C just in case but I got nothing. I'll try to go a bit higher just in case. 


---
**Vic Catalasan** *July 14, 2015 17:07*

I run the Azteeg X3 V2 and Viki 2. I have most of it working, unfortunately the fan does not go on automagically so on my Pronterface I added a button to turn the fan on (hot end and bed fan in case I wanted them on manually. Automatically I include the gcode command for the hot end and Azteeg board fan. Manually you can send M42 s5 250 (hot end fan) and M42 s16 250 ( Azteeg fan)  you need a fan for the step drivers or it may shut down during prints.



You need to configure the pin.h assignment telling what pin your using for the hot end. I am fairly new to all this and took a bit of time comparing the settings from Eric's marlin and Viki2 marlin settings. 


---
**Vic Catalasan** *July 15, 2015 06:10*

You need to check your configuration_adv.h file to include this if you want auto start fan on pin 16



// Extruder cooling fans

// Configure fan pin outputs to automatically turn on/off when the associated

// extruder temperature is above/below EXTRUDER_AUTO_FAN_TEMPERATURE.

// Multiple extruders can be assigned to the same pin in which case

// the fan will turn on when any selected extruder is above the threshold.

#define EXTRUDER_0_AUTO_FAN_PIN   16

#define EXTRUDER_1_AUTO_FAN_PIN   16

#define EXTRUDER_2_AUTO_FAN_PIN   -1

#define EXTRUDER_AUTO_FAN_TEMPERATURE 50

#define EXTRUDER_AUTO_FAN_SPEED   255  // == full speed


---
**Luis Pacheco** *July 15, 2015 12:54*

**+Vic Catalasan** I'll give that a try this morning and see how it works.


---
*Imported from [Google+](https://plus.google.com/110276424073473119972/posts/f28T27ZsmPY) &mdash; content and formatting may not be reliable*
