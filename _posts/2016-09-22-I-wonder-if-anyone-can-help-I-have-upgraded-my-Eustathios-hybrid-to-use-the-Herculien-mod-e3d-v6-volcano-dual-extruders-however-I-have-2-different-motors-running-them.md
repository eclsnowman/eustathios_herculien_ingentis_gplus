---
layout: post
title: "I wonder if anyone can help. I have upgraded my Eustathios hybrid to use the Herculien mod e3d v6 + volcano dual extruders, however I have 2 different motors running them"
date: September 22, 2016 21:50
category: "Discussion"
author: Adam Morgan
---
I wonder if anyone can help. I have upgraded my Eustathios hybrid to use the Herculien mod e3d v6 + volcano dual extruders, however I have 2 different motors running them. 1 geared nema 17, and 1 non geared nema 17 with bondtech drive gears. 



The problem I have is with Marlins firmware only allowing 1 set of steps per mm for the extruder. I have got around this using Gcode, but quite frankly its doing my head in having to edit ever  file I try to print. I found MarlinX2 firmware which allows you to do this but 1, its a year out of date, and 2, I cant actually get it to compile for my Rambo board.



So.... has anyone managed to get MarlinX2 working on a Rambo? If so how? Or does any have a better idea of how to simplify the gcode process? 



Cheers for the help peeps.





**Adam Morgan**

---
---
**Adam Morgan** *September 22, 2016 22:03*

oh I should add my gcode process consists of opening the gcode in a text editor and using find and replace to add the commands the change the steps per mm when theres a tool change. I know its easy to do this with Simplify3d, but I cant bring my self to fork out the money to buy it, no matter how good it is and I cant seem to find a list of place holders for cura or repetier to add and if statement in to the tool change gcode.


---
**Øystein Krog** *September 23, 2016 08:11*

Marlin has a PR for independent extruder steps, use that. ﻿ (on github) 


---
**Adam Morgan** *September 23, 2016 08:33*

Hi **+Øystein Krog**, sorry what do you mean by PR?


---
**Øystein Krog** *September 23, 2016 08:34*

Pull request, a new change in the code. I think magokimbra submitted it. Just go to the github repository and look at the pull requests :) 


---
**Adam Morgan** *September 23, 2016 08:35*

Ah wicked. I'll check it out now thanks!


---
**Øystein Krog** *September 23, 2016 11:23*

[github.com - Step for extruder by MagoKimbra · Pull Request #4849 · MarlinFirmware/Marlin](https://github.com/MarlinFirmware/Marlin/pull/4849)


---
**Adam Morgan** *September 24, 2016 22:57*

**+Øystein Krog** Just wanted to say thanks, this sorted me right out! :) 


---
*Imported from [Google+](https://plus.google.com/117208540149253709671/posts/Li651XdwKYu) &mdash; content and formatting may not be reliable*
