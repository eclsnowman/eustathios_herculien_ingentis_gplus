---
layout: post
title: "Lately has been skipping a beat for me"
date: June 21, 2014 05:54
category: "Discussion"
author: Wayne Friedt
---
Lately  #simplfy3d  has been skipping a beat for me. First time the Z steps were off, a 20mm cube came out 24mm tall now the extruder is spinning about 300 mph and shredding the filament. If i do a extruder calibration of 50mm it is correct but then slice and print, not good. If i slice in Cura then print in S3D it is fine for some reason it has been changing the EEPROM, i guess. This is happening on 2 printers not just one. Kinda irritated $140 and all.





**Wayne Friedt**

---
---
**Daniel Fielding** *June 21, 2014 06:06*

Check if s3d has relative or absolute extrusion turned on.


---
**Wayne Friedt** *June 21, 2014 06:14*

There is relative extrusion distance and it is unchecked. Should it be?


---
**Tim Rastall** *June 21, 2014 06:31*

**+Wayne Friedt** depends on your machine. If the machine is set to  relative and you send absolute code,  it will start off fine then quickly go crazy over extrusion.  Check your gcode to see what simplify thinks it's using.  You can force the bot to absolute positioning by using the G90 command before starting the print.


---
**Wayne Friedt** *June 21, 2014 10:54*

Back in the drivers seat again. I reset back to factory specs then reset up all my other settings. 


---
*Imported from [Google+](https://plus.google.com/+WayneFriedt/posts/LyKhAkonvWX) &mdash; content and formatting may not be reliable*
