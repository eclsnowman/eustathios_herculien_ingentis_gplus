---
layout: post
title: "I'm changing my Prusa mendel i2 (first printer) with a refresh of a Prusa_i3_rework"
date: September 07, 2014 04:02
category: "Show and Tell"
author: Jean-Francois Couture
---
I'm changing my Prusa mendel i2 (first printer) with a refresh of a Prusa_i3_rework. All new frame and parts and only keeping the electronics and some smooth rods. This is the first part printed with my Eustathios variant: red ABS, H:240C, B:110C, 1hour to print.



The part is modified to use 3/8' thread rod (instead of the normal 10mm) and the top is widen to use a 10mm smooth rod (instead of a 8mm)



many more parts ahead to print :)﻿

![images/f4c887d1e7341e970d3115d3a4609ac8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f4c887d1e7341e970d3115d3a4609ac8.jpeg)



**Jean-Francois Couture**

---
---
**Eric Lien** *September 07, 2014 04:25*

Nice finish. I still fight z ribbing on my Eustathios. I haven't pegged down the source yet. Nice to see yours is running well.


---
**Miguel Sánchez** *September 07, 2014 07:50*

it seems different than this: [https://github.com/josefprusa/Prusa3-vanilla/blob/master/distribution/y-corners.stl](https://github.com/josefprusa/Prusa3-vanilla/blob/master/distribution/y-corners.stl)


---
**Jean-Francois Couture** *September 07, 2014 11:39*

**+Miguel Sánchez** Thats because it comes from this one : [https://github.com/josefprusa/Prusa3/blob/master/box_frame/sample_stls/default_box_gt2_lm10uu/y-axis-corner.stl](https://github.com/josefprusa/Prusa3/blob/master/box_frame/sample_stls/default_box_gt2_lm10uu/y-axis-corner.stl)



but your link does show a better looking one. I'll look into it.. maybe I can modify it to fit my needs. thanks !


---
**Miguel Sánchez** *September 07, 2014 11:44*

If using a standard Prusa frame watch out, as the M10 rods are not at the usual height. The zip tie hole for securing the smooth rods is not present: not a bid deal but convenient.


---
**Jean-Francois Couture** *September 07, 2014 12:38*

**+Miguel Sánchez** Could the M10 make trouble elsewhere ? I'm also building a custom bed so i'll adjust for the LM10UU there also.


---
**Miguel Sánchez** *September 07, 2014 13:07*

**+Jean-Francois Couture** Original i3 design had short clearance for the carriage and frame. Bed is mounted on top of carriage and usually it is not a problem. I did not know you were using 10mm OD rods. That again is not i3 standard (they use 8mm rods instead). I've hosted six Prusa i3 workshops last year but we stuck to single frame [http://www.thingiverse.com/thing:172887](http://www.thingiverse.com/thing:172887) I can see yours is going to be a wooden frame.


---
**Jean-Francois Couture** *September 07, 2014 13:14*

**+Miguel Sánchez** I like the single frame. i'm thinking of making it out of MDF 15mm, in a single piece on my router table.. (trough hole drop-in on a spiral cutter technique) and put longer back threaded rods so I can attach diagonal cross-braces with the top of the frame (I'll have to design those parts has I build).



Should be fun to turn my old i2 into a i3.


---
**Miguel Sánchez** *September 07, 2014 13:23*

**+Jean-Francois Couture** 15mm MDF seems overkill but if you have some leftover it is more than strong enough. I guess you know **+Shane Graber** design for 6mm composite board (or MDF) [https://github.com/sgraber/Prusa3_LC](https://github.com/sgraber/Prusa3_LC) you can use that and adapt it to your thicker board.


---
**Jean-Francois Couture** *September 07, 2014 13:47*

**+Miguel Sánchez** Thanks, I was looking for drawings.. those will help a lot. ! 


---
*Imported from [Google+](https://plus.google.com/105576148076542448710/posts/gXZaPwYmCFj) &mdash; content and formatting may not be reliable*
