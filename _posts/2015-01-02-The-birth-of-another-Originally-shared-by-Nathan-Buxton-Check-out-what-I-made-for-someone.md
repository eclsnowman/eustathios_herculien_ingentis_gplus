---
layout: post
title: "The birth of another :) Originally shared by Nathan Buxton Check out what I made for someone"
date: January 02, 2015 16:28
category: "Show and Tell"
author: Eric Lien
---
The birth of another #HercuLien :)



<b>Originally shared by Nathan Buxton</b>



Check out what I made for someone.





**Eric Lien**

---
---
**Robert Cicetti** *January 02, 2015 16:33*

I'd love to build one myself. How much would the printed parts cost?


---
**Eric Lien** *January 02, 2015 18:16*

**+Robert Cicetti** my big printer is tied up with work stuff most of the time. **+Nathan Buxton**​ you want to sell Robert a set?


---
**Marc McDonald** *January 02, 2015 18:33*

**+Eric Lien** , **+Nathan Buxton**  I see Nathan posted the pictures of my parts printed for the HercuLien. I was planning on posting soon when the unit was assembled. I will post a few images of the HercuLien as it is currently assembled.


---
**Marc McDonald** *January 02, 2015 19:42*

**+Ashley Webster** The Vslot extrusions were from openbuilds, the remaining extrusions were from Misumi similar to Eric's BOM.


---
**Chris English** *January 02, 2015 20:25*

I started gathering parts for the Eustathios, but I'm tempted more and more by this one.  What do you guys think??


---
**Eric Lien** *January 02, 2015 21:14*

**+Chris English** if you don't need huge prints Eustathios is a great option. But I needed a larger envelope hence why I designed HercuLien.



Both a great machines, but larger printers do have some nuances. More inertia on moving parts, more wattage needed, etc.


---
**Marc McDonald** *January 02, 2015 22:26*

**+Ashley Webster** I do not remember any additional anti-dumping fees for importing v-slot or other extrusions.


---
**Chris English** *January 02, 2015 23:08*

**+Eric Lien** Thanks for the tips.  This is my first build so I'll stick to the #Eustathios for now.  I'm sure I'll learn plenty in the process.  Great stuff you got going on!


---
**Gus Montoya** *February 05, 2015 17:40*

If you still have your old parts? Would you mind parting with them? It will save me some printing time. Even though I have my printer working, it's iffy al the time.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/4ZKMK56kZc9) &mdash; content and formatting may not be reliable*
