---
layout: post
title: "Collecting...collecting....collecting.... (and yeah, the diamond-plate won't be structural...or, sadly, mostly visible"
date: June 28, 2014 17:38
category: "Discussion"
author: Mike Miller
---
Collecting...collecting....collecting.... (and yeah, the diamond-plate won't be structural...or, sadly, mostly visible. It'll be between the built frame, a layer of cork, a heated bed, and a glass build surface. 12"x12"

![images/d65d35c7c7d8d408d26a36c4956f6406.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d65d35c7c7d8d408d26a36c4956f6406.jpeg)



**Mike Miller**

---
---
**Mike Miller** *June 28, 2014 17:57*

There'll be a week's delay while the family vacations in Virginia...I'll use that time to order, I dunno...  **+igus Inc.** parts? :D


---
**Joe Spanier** *June 28, 2014 18:57*

Diamond plate is a good idea for a bed platform. You can't buy plate aluminum locally or for a not ridiculous price anywhere around me. I'll have to check out diamond plate prices at menards


---
**Mike Miller** *June 28, 2014 18:59*

Its relatively cheap and available, its just not very precise, nor would it be thermally homogenous...luckily, I'm not relying on it for either. 


---
**Joe Spanier** *June 28, 2014 19:01*

Right. I would be using it like you are. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/8MFXm4R8CwX) &mdash; content and formatting may not be reliable*
