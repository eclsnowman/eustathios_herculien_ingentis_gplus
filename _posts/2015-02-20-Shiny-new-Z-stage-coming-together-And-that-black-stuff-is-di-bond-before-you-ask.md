---
layout: post
title: "Shiny new Z stage coming together. And that black stuff is di-bond before you ask"
date: February 20, 2015 08:36
category: "Show and Tell"
author: Tim Rastall
---
Shiny new Z stage coming together. And that black stuff is di-bond before you ask. 

![images/4c4563722a150334682cdab32416e242.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4c4563722a150334682cdab32416e242.jpeg)



**Tim Rastall**

---
---
**Lynn Roth** *February 20, 2015 11:53*

I like the idea of using the frame for the guides.

Are all four corners the same?  If so, is there any issue with being overconstrained?


---
**Tim Rastall** *February 20, 2015 21:24*

**+Lynn Roth** not as badly as using 4 shafts because their are only 4 planar constraining surfaces.  There's enough give in the system to prevent binding and the bed is deliberately heavy. Proof will be in the printing I guess. 




---
**Jason Barnett** *February 21, 2015 05:40*

Where did you buy your Di-bond from? How was the price for it?


---
**Tim Rastall** *February 21, 2015 05:59*

Local Aluminium supplier in New Zealand (Ulrich).


---
**Lynn Roth** *February 21, 2015 13:56*

**+Tim Rastall**​ thanks for the explanation. That is helpful.  I look forward to seeing the results.


---
**Eric Lien** *February 23, 2015 01:40*

Man I can't believe I just noticed the angle aluminum. Nice trick.


---
**Tim Rastall** *February 23, 2015 04:36*

**+Eric Lien** yeah.  Driven by necessity when I realized the wheels I'd bought weren't going to run nicely on the slot in the profiles. 


---
**Tim Rastall** *February 23, 2015 04:37*

**+Jason Barnett** it costs $80 nzd for a 2.4mx1.2m sheet. 


---
**Tim Rastall** *February 24, 2015 08:13*

**+Ashley Webster** yep,  it's just regular Misumi 2020 I had a pile of. Vslot and delrin wheels are quite pricey compared to angled extrusion and shower door wheels :) 


---
**Tim Rastall** *March 06, 2015 19:07*

**+Ashley Webster** they're nylon and don't move much so I'm not expecting any wear issues. 


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/K3w1nYk65eT) &mdash; content and formatting may not be reliable*
