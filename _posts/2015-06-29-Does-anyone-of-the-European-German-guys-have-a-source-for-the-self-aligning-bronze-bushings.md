---
layout: post
title: "Does anyone of the European/German guys have a source for the self aligning bronze bushings?"
date: June 29, 2015 06:19
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Does anyone of the European/German guys have a source for the self aligning bronze bushings? I couldn't find them here so far - only overseas sellers.





**Frank “Helmi” Helmschrott**

---
---
**Daniel F** *June 29, 2015 07:39*

Bought mine from Lulzbot, I think they were shipped from the UK. Unfortunately, the 8mm ones are out of stock. Link to the 10mm version: [https://www.lulzbot.com/products/10mm-bronze-bushing-4-pack](https://www.lulzbot.com/products/10mm-bronze-bushing-4-pack)


---
**Jo Miller** *June 29, 2015 08:54*

Nee, hab die 10mm  leider auch über Lulzbot bestellen müssen, in Europa nichts gefunden.  falls du den Herculien baust, ich hab  das  Misumi trapezgewinde und die 2 Misumi Trapez Mutter      2 x bestellt, also einen Set übrig, falls Du Interresse hast..


---
**Frank “Helmi” Helmschrott** *June 29, 2015 08:56*

Ich baue einen Eusthatios, allerdings mit Kugelumlaufspindeln - danke fürs Angebot. Hast du nur 10mm gekauft oder die 8mm anderswo gefunden?


---
**Jo Miller** *July 06, 2015 20:50*

Yep, hab die 10mm bei lutzbot direkt gekauft, allerdings vermute ich das es mit den Igus -plastik sortiment auch funktionieren müsste


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/NbYu2J9bhmc) &mdash; content and formatting may not be reliable*
