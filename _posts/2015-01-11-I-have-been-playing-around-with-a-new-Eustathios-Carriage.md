---
layout: post
title: "I have been playing around with a new Eustathios Carriage"
date: January 11, 2015 04:30
category: "Discussion"
author: Eric Lien
---
I have been playing around with a new Eustathios Carriage. I am not happy with it yet, but I wanted better cooling distribution since it is my one issue I fight right now with this printer. I wanted to get your guys input. I have printed a few prototypes to balance the discharge air around the ring. It yields great volume and distribution, but I feel I am over thinking things with the design. 



Another issue is the bed mounts will need to be raised since I tried to tuck the nozzle up closer to the cross bars to make a more rigid connection of the hot end and less whiplash effect since the lever arm that is the hot end is closer to the control points that the bushings provide. So the current design will not let the bed reach far enough (but this is an easy fix by moving some holes).



Here is a google drive folder with the model in solidworks (Pack-N-Go zip), 3D PDF (requires actual acrobat reader, not foxit or the like), as well as STL files.



[https://drive.google.com/open?id=0B1rU7sHY9d8qRWhOd2pydGxaUWc&authuser=0](https://drive.google.com/open?id=0B1rU7sHY9d8qRWhOd2pydGxaUWc&authuser=0)



The STL of the duct has a small brim built into the model on the center ring since simplify3D would not add brim there. When I was prototyping the piece kept coming loose before the print completed because the bed contact area is so low and the nozzle can catch during printing.



![images/d76bab237f30a62b14ca5309d30d0374.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d76bab237f30a62b14ca5309d30d0374.png)
![images/29d6d0214261361905c3b457142b96ae.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/29d6d0214261361905c3b457142b96ae.png)
![images/cff12ef1391dd0f037b9a4cba45c6d7a.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cff12ef1391dd0f037b9a4cba45c6d7a.png)
![images/22fbf7f0a0b81ff735833cf57b5340ed.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/22fbf7f0a0b81ff735833cf57b5340ed.png)

**Eric Lien**

---
---
**Wayne Friedt** *January 11, 2015 04:44*

Looks pretty fancy. Probably just going to have to try it.


---
**Mike Miller** *January 11, 2015 05:01*

And here I am, printing out your old one....


---
**Jim Wilson** *January 11, 2015 05:03*

I'm no expert by any means, but I found much better results aiming the air all in one direction, I don't get any noticeable difference on the side of parts facing the air as the side away from the air, I blow it directly across the head, but just above the part itself. Pics of my current hot end to follow showing what I mean.


---
**Jim Wilson** *January 11, 2015 05:07*

[https://plus.google.com/101778058628996936791/posts/RBpZa7z8TTn](https://plus.google.com/101778058628996936791/posts/RBpZa7z8TTn)



Does that make sense? Or is the part exhibiting a problem I'm not noticing related to cooling?


---
**Tim Rastall** *January 11, 2015 05:23*

If you are using linear ball bearings for the carriage, You could conceivably do away with the full cylinder holes for the bearings and use half cylinders with the bearings secured with cable ties. This should make it easier to get good alignment and less fiddly support material removal. 

I agree you should move the hot end as high up as practical.  Ideally the orifice should be just below the lowest point of the carriages bearings thus minimising any bending moment caused by the hot end tip dragging a bead of plastic out behind it.﻿


---
**James Rivera** *January 11, 2015 05:32*

**+Jim Wilson**​ that is a design I considered, too, but the problem with using it that while you always want the upper portion of the hot end to have air blowing over it, you may want to have the part cooling be variable. That being said, if you're printing as fast as **+Eric Lien**​ likely does, two fans always on from the top down might be good idea. :)


---
**Jim Wilson** *January 11, 2015 05:37*

I have a separate one blowing directly across it, two very tiny 25mm fans, it sort of shows in one of the images but my angles were poor, my fault


---
**James Rivera** *January 11, 2015 08:05*

Or maybe I'm just blind. :(


---
**Jason Barnett** *January 11, 2015 08:12*

I just finished fighting this same problem. I decided to use my old CPAP machine as the air source. The air flows across the cooling fins first, then is route down and exhausted through a series of nozzles directed at the print nozzle.

Here is the link to it on youmagine...

[www.youmagine.com/designs/e3d-heatsink-and-project-cooling-for-cpap](http://www.youmagine.com/designs/e3d-heatsink-and-project-cooling-for-cpap)






---
**Tim Rastall** *January 11, 2015 09:32*

So, if you're cooling you print using the same fan as your E3d, how do you deal with ABS prints when you don't want to cool the print? 


---
**SalahEddine Redjeb** *January 11, 2015 10:40*

Okay boss, that's a nice looking extruder mount there, i have a couple of question:

If i understood well the design, this one is atached by using a zip tie right?

if that is the case, i am afraid the makerbot software will generate support in a manner making it impossible to remove, is there any possibility you could point me to a simple software i could use to generate appropriate support structures (i am not very efficient with autocad, solidworks and this kind of advanced software...)


---
**Eric Lien** *January 11, 2015 12:10*

**+SalahEddine Redjeb**​ I use simplify3d, but craftware should let you do it. I can also design an stl with an sacrificial piece and an airgap where support is needed.﻿



Also it can print without support except for the area for the heat sink fan which I offset for duct clearance. If you have the option for support only when support hits the bed, that would take care if it. I know Cura has this option.


---
**Eric Lien** *January 11, 2015 12:31*

**+Jim Wilson**​ I have one directional cooling on HercuLien with large 50mm blowers. It still causes issues on overhangs more than 40 degrees at speeds above 70mm/s unless the part is very large where layer times aid in cooling.



**+Tim Rastall**​​ I would love to get the nozzle directly below the lower bushing. It would take some changes to bed mounts so the nozzle could reach the bed... But that could be figured out. The big issue I have fought is airflow path for the heat sink on the E3D. But perhaps a ducting design with fans above the upper bushing instead of below it? Also I do fight catches from time to time. So I worry about making assemblies too ridged. I have thought about spring loading the hot end. I have never seen an ultimaker in person... But it looks like this is what they do: [http://ultimaker.ipbhost.com/uploads/gallery/album_7/gallery_423_7_225254.jpg](http://ultimaker.ipbhost.com/uploads/gallery/album_7/gallery_423_7_225254.jpg)﻿



One last downside to the nozzle being so close to the gantry is you sacrifice sequential part printing in slicers since nozzle to gantry distance is so low.


---
**Dale Dunn** *January 11, 2015 19:05*

Whiplash effect and lever arm: The carriage I'm working on (off and on, but mostly off) is gripping the extruder by the heat sink, not the groove mount. I haven't tested this, but if the heat sink is cool enough for and ABS fan bracket, why not mount it by the fan bracket?


---
**Tim Rastall** *January 11, 2015 19:24*

**+Eric Lien** I think that spring is there to alleviate the impact of the head crashing into the bed.  The Ultimaker bed has no Springs under it's mounting points as I recall.

If I were you,  I'd focus on the causes of the print catching,  rather than trying to mitigate it's effects. Just my opinion obviously  :) 


---
**Jason Barnett** *January 11, 2015 20:23*

**+Tim Rastall** that's easy. I don't print with ABS.😁 

I have been printing with PLA only, for quite awhile so that issue did not even occur to me. I guess it would be easy enough to make a removable section to prevent the downward ducting of the air.


---
**SalahEddine Redjeb** *January 11, 2015 23:43*

**+Eric Lien** i just remembered something, i have 10mm rods, and lm10uu, so i guess ill have to rework it anyway, i guess ill be adding some supports.

The makerware software (which is used for replicators) doesnt allow much of options, much like most closed source software; you onlu have the option of supports or not, not even allowing configuration of supports...


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/VZQNccsWhqL) &mdash; content and formatting may not be reliable*
