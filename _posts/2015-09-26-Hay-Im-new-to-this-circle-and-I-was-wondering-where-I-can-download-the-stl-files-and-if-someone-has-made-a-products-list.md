---
layout: post
title: "Hay I'm new to this circle and I was wondering where I can download the stl files and if someone has made a products list"
date: September 26, 2015 15:30
category: "Discussion"
author: Orrin Naylor
---
Hay I'm new to this circle and I was wondering where I can download the stl files and if someone has made a products list. Thanks





**Orrin Naylor**

---
---
**Eirikur Sigbjörnsson** *September 26, 2015 15:39*

Welcome to the group. There are links to the Github files for Ingentis, Eustathios and the Herculien on the right side of this page. Should be enough to get you started :)


---
**Orrin Naylor** *September 26, 2015 17:06*

Thanks. Can't wait to build my own.


---
*Imported from [Google+](https://plus.google.com/108930960082385878809/posts/biYbZ6HCyZY) &mdash; content and formatting may not be reliable*
