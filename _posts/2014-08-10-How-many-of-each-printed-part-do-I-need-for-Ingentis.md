---
layout: post
title: "How many of each printed part do I need for Ingentis?"
date: August 10, 2014 18:13
category: "Discussion"
author: Nick Parker
---
How many of each printed part do I need for Ingentis?



I'm just firing parts off based on the picture right now, but it's hard to tell between the A and B bearing mounts, etc.





**Nick Parker**

---
---
**D Rob** *August 10, 2014 19:29*

Which ingentis version? Eustathios? Original?


---
**Nick Parker** *August 10, 2014 19:34*

Original belt Z.


---
**D Rob** *August 10, 2014 20:13*

I'll make you a list. 


---
**Nick Parker** *August 10, 2014 20:20*

Thanks!


---
**D Rob** *August 11, 2014 00:43*

2 xy ends a, 2 xy ends b, 2 z carriages, 4 upper rod blocks, 4 lower rod blocks, 1 carriage, the belt clamps


---
**D Rob** *August 18, 2014 10:39*

Z upper mount x2 z lower mount x2, 


---
*Imported from [Google+](https://plus.google.com/+nickparker/posts/aQtd3Spds6D) &mdash; content and formatting may not be reliable*
