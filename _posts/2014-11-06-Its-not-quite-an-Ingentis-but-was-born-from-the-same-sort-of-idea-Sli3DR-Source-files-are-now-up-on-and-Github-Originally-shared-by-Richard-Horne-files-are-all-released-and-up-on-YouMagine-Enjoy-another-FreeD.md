---
layout: post
title: "It's not quite an Ingentis, but was born from the same sort of idea - Sli3DR Source files are now up on and Github - Originally shared by Richard Horne files are all released and up on YouMagine - Enjoy another 'FreeD'"
date: November 06, 2014 20:03
category: "Show and Tell"
author: Richard Horne
---
It's not quite an Ingentis, but was born from the same sort of idea - Sli3DR Source files are now up on  and Github - [https://github.com/RichRap/Sli3DR](https://github.com/RichRap/Sli3DR)



<b>Originally shared by Richard Horne</b>



 #Sli3DR  files are all released and up on **+YouMagine** - Enjoy another 'FreeD'  #RepRap  Printer - Have fun :)





**Richard Horne**

---
---
**Uche Eke** *November 06, 2014 20:07*

Fantastic!! :) **+Richard Horne**​


---
**Ricardo de Sena** *November 06, 2014 20:24*

Great Richard!!! Thanks.


---
**Mark Hindess** *November 06, 2014 21:03*

**+Richard Horne** Sorry to hear about your disk problems. In future you should remember to back up your files to github and youmagine right away. ;-)


---
**Dale Dunn** *November 06, 2014 22:41*

**+Shauki Bagdadi** , if you want to make all the motors stationary on  #QR1500 , the cable arrangement on Sli3DR could do that for you.


---
**Eric Lien** *November 07, 2014 00:11*

**+Richard Horne**​ great job.



If the failed disk was a spinning hard drive give spinrite a crack at it. It has saved data for myself and my family members several times.


---
*Imported from [Google+](https://plus.google.com/+RichardHorne_RichRap3D/posts/BpkE3CYzpck) &mdash; content and formatting may not be reliable*
