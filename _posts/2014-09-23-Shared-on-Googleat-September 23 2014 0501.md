---
layout: post
title: "Shared on September 23, 2014 05:01...\n"
date: September 23, 2014 05:01
category: "Discussion"
author: D Rob
---






**D Rob**

---
---
**Miguel Sánchez** *September 23, 2014 06:41*

Exactly, you need a processor for each motor, and to tune the loop for your particular system. 


---
**D Rob** *September 23, 2014 06:44*

I have some cheap Leonardo micro boards. And some h bridges


---
**Miguel Sánchez** *September 23, 2014 07:10*

It sounds like a plan :)


---
**Denis Seguin** *September 23, 2014 12:40*

Great :)


---
**Michael Ball** *September 25, 2014 22:34*

Ahhhh... So very nice.. 

Sorry I fell off on my development..  Life getting in the way of play.


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/7HULJuMy7wr) &mdash; content and formatting may not be reliable*
