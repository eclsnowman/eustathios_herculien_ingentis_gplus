---
layout: post
title: "This is a job I finished a couple of weeks ago"
date: February 04, 2016 01:10
category: "Show and Tell"
author: Zane Baird
---
This is a job I finished a couple of weeks ago. Printed in Bronzefill at 0.111mm layer height with a lot of pre- and post-processing. I will do a full write-up eventually.



[https://www.chem.purdue.edu/media/news/2016/Prof%20Cooks%20receives%20unique%203-D%20gift.php](https://www.chem.purdue.edu/media/news/2016/Prof%20Cooks%20receives%20unique%203-D%20gift.php)





**Zane Baird**

---
---
**Eric Lien** *February 04, 2016 01:31*

As usual **+Zane Baird**​, your work is impeccable. Congratulations on getting a nice write-up on your hard work. And even more congratulations and mixing so many skills together into such a wonderful outcome. I can't tell you how happy I am to see something that I designed working so well for you. And you are doing so many great customizations to it to make the HercuLien even better.


---
**Bruce Lunde** *February 04, 2016 03:32*

Awesome work, amazing that there are 6 pieces there! Bronze fill is cool material.


---
**Martin Bondéus** *February 04, 2016 11:27*

Nice work Zane, really impressive!


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/XJ68R7RTP72) &mdash; content and formatting may not be reliable*
