---
layout: post
title: "Anyone have a design for the standard herculien Of the part cooling fan shroud/guide?"
date: November 18, 2016 06:29
category: "Discussion"
author: Jim Stone
---
Anyone have a design for the standard herculien



Of the part cooling fan shroud/guide?



The current simple box shaped one is falling off constantly. Also isn't directional enough and keeps lowering the hotend to the emergency cutoff limit.



Even without emergency cutoff. And temp set to 230. The hotend can't keep up. And I'm using the upgrade 24v cartridge too :( it kept falling under 190c





**Jim Stone**

---
---
**Greg Nutt** *November 18, 2016 12:06*

Are you using a 30W or 40W heater?


---
**Jim Stone** *November 18, 2016 12:07*

Currently 40w

Have used the 25w(24v oddly doesn't have 30w).



 And same story. Was just more dramatic


---
**Eric Lien** *November 18, 2016 13:45*

Yeah, cooling solution on HercuLien was never something I was proud of. Just could never come up with a better solution. 



Mine prints almost exclusively ABS so it was never a big problem.



Maybe take a look at the **+Zane Baird**​ carriage and fan ducts.


---
**Zane Baird** *November 18, 2016 14:19*

The ducts on my carriage still don't perform as well as I'd like. Making a better version is on my list of things to do... I need a solution that works as well as the duct I made for the Tria. That works better than I would have hoped.


---
**Jim Stone** *November 18, 2016 15:28*

Atm. Carriage swapping is a no no. I had to beat my rods into place. I'm not sure if I can get them out to swap carriage


---
**Zane Baird** *November 18, 2016 15:51*

**+Jim Stone** Do you mean you had to beat your rods into the bearings on the corner brackets? The first rods I bought weren't spec'd for the bearings and I had a similar issue but I ditched them for Misumi and the fit was flawless. Worth every penny


---
**Jim Stone** *November 18, 2016 15:54*

**+Zane Baird** And into the gears afaik XD

Yeah i used drill rod from the local machine supply store so idk tolerance.



I just know i dont have the money to get new ones. this printer got me broke haha.


---
**Zane Baird** *November 18, 2016 16:02*

**+Jim Stone** Its unfortunate that I didn't have my new XY belt tensioners designed when I sent you those parts... That would have allowed you to change the carriage without taking out the guide rods on the side


---
**Jim Stone** *November 18, 2016 16:08*

+ well maybe i get lucky and just have to change the bearings at most.


---
**Greg Nutt** *November 18, 2016 16:20*

I wonder if designing a heat shield to go around the heater block would help.


---
**Jim Stone** *November 18, 2016 16:25*

**+Greg Nutt** if i was gonna do that. i would spend the money that went into that...on the new style e3d block upgrade with the silicone sock theyre making.



that would literally solve my problem and i know it.



but me being me and the doofus i am. i have to not spend money first


---
**Greg Nutt** *November 18, 2016 19:35*

Actually the sock may not help. I have another customer using sock that has similar issues.


---
**Jim Stone** *November 18, 2016 19:36*

So...a silicone sock...a thermal barrier...a hest shield 



Doesn't work


---
**Zane Baird** *November 18, 2016 19:39*

**+Jim Stone** Its just a thin barrier and apparently it does degrade over time. I haven't seen any life-changing revelations from anyone using the silicone sock, but maybe I haven't been following it close enough.


---
**Jim Stone** *November 18, 2016 19:40*

Well that sucks that it degrades. It certainly is a nice idea to prevent the sticking


---
**Greg Nutt** *November 18, 2016 19:53*

I was thinking more of a shield that wasn't in direct contact with the heat block, leaving an air cushion as an insulator.


---
**Mike Miller** *November 19, 2016 13:30*

You might check the health of your power supply. I had a similar behavior and the supply was on its way out. 


---
**Jim Stone** *November 29, 2016 03:11*

**+Zane Baird** Finally got around to printing your fan shrouds. however the hole is oversized by a few mm. are you using larger fans?


---
**Zane Baird** *November 29, 2016 04:06*

**+Jim Stone** My modified carriage and fan ducts use 40x20mm fans. This would easily explain your problems if you are using the 50x50x15mm fans specified in the BOM. Are you using the 50mm fans? If so, let me know and I will do my best to design you a fan duct that works for that situation.


---
**Jim Stone** *November 29, 2016 04:25*

Yes im using 50x50x15



This one specifically [http://www.ebay.ca/itm/1pcs-Brushless-DC-Cooling-Blower-Fan-5015-FSY50S24M-DC-24V-50-x-50-x-15mm-2-Pin-/300764402903?hash=item4606f494d7](http://www.ebay.ca/itm/1pcs-Brushless-DC-Cooling-Blower-Fan-5015-FSY50S24M-DC-24V-50-x-50-x-15mm-2-Pin-/300764402903?hash=item4606f494d7)



i assume the important parts are the same.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/SUCNrFmRCKp) &mdash; content and formatting may not be reliable*
