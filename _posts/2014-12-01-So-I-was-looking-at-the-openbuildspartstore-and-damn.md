---
layout: post
title: "So I was looking at the openbuildspartstore, and damn"
date: December 01, 2014 21:06
category: "Discussion"
author: R K
---
So I was looking at the openbuildspartstore, and damn. T-slot is still a lot cheaper, and the price of the v-slot wheels/plates is pretty up there.



I've been googling around but I'm not seeing what should be there (I think) - e.g. a v-slot alternative for t-slotted extrusion. The idea here being to lose linear bearings on rails and use the t-slot and wheels for linear movement.



Any ideas?





**R K**

---
---
**Chandler Wall** *December 01, 2014 21:44*

You may want to take a look at the post history of **+Shauki Bagdadi**. More specifically, his  #QuadRapCarriage  and  #OctoCarriage  designs may be similar to what you want. Alternatively, he's recently made progress on a v-slot design, #vCarriage. I haven't had a chance to look into his most recent designs, but they're usually effective, minimal, and intuitive. It's possible that his approach to v-slot movement is less expensive than the current approaches.


---
**Mike Thornbury** *December 02, 2014 00:58*

I only buy the wheels, not the wheel assemblies. I have a bunch of bearings and washers and screws, I can put a v-wheel together for about $2 less than a set on openbuilds goes for.



As to the extrusion, you could build a jig and route one face of t-slot with a 90deg bit to give you two 45deg faces. Once I have cleared some projects out of the way I will give it a try with some 2020 I have lying around.﻿


---
**R K** *December 02, 2014 04:50*

Thanks guys - good reading to pull through. Right now it looks like short-term, I'll build something simpler. Once I can print out parts I'll aim for something more like the Eustathios - cutting out the cost of the bloody gantry plates and connecting bits and bobs would help a bloody ton. On a brighter note, openbuildspartstore has/had 300mm ACME screws for 10$ each, so that helps a bit :p



Mike, interesting idea on the jig-thing. Unfortunately I'm a bit lacking in terms of space to do...anything like that. Hah. But as far as the wheels go, I don't think the V-wheel will work for standard t-slots. Moot point at this point, I picked up v-slots today anyways. 


---
**Mike Thornbury** *December 02, 2014 04:59*

a 'jig' is, in my case, a couple of bits of mdf with  a hole for my router... about 1sq foot.

 

If you can't find room for one sq foot, you must sleep standing up...


---
*Imported from [Google+](https://plus.google.com/102580771315197780541/posts/g6pBZmcDNe3) &mdash; content and formatting may not be reliable*
