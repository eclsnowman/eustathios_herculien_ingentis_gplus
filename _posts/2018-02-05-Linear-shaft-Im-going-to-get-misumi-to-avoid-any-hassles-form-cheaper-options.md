---
layout: post
title: "Linear shaft. I'm going to get misumi to avoid any hassles form cheaper options"
date: February 05, 2018 23:07
category: "Discussion"
author: Julian Dirks
---
Linear shaft.  I'm going to get misumi to avoid any hassles form cheaper options.  Should I be getting chromed rods?  The code in the BOM (eg SFJ10) appears to be un-chromed? Which is the correct code for the chromed ones?



Also I'm going to with a direct drive bondtech.  Should I upgrade the 8mm rods to 10mm?  Will increasing to 10mm reduce print quality given I already will have additional mass with the direct drive system?



Thanks





**Julian Dirks**

---
---
**Eric Lien** *February 08, 2018 04:36*

I know several people who went with 10mm cross rods on Eustathios. There should be no ill effects. I can print faster than I can cool on HercuLien which uses 10mm rods and even a longer span.



The rods spec'd are induction hardened bearing steel. So you will be fine without chrome. You can go to chromed rods but make sure to maintain the G6 shaft fit tolerance. Otherwise the fit between the self aligning bushings and the rods will not be correct.


---
**Dennis P** *February 08, 2018 04:49*

EDIT: I just realized that you said in another post that you are in NZ. US suppliers might make the most sense for you.  I recently got hardened rods domestically from a guy on Ebay. His store was called PD-Tech and he ships from somewhere in TN. He cuts to order. My rods came USPS priority and were WELL packed. He used to only list 10mm but he did have 8mm when I asked that I bought too.  His prices were descent, not much more than buy from overseas and the quality seems pretty good. His service has been really good to. I had one that was funny that turned out to be a burr that didnt clean up at the end.  

[stores.ebay.com - Items in PDTech-3D store on eBay!](http://stores.ebay.com/PDTech-3D?_trksid=p2047675.l2563) 


---
**Oliver Seiler** *February 08, 2018 05:13*

I've upgraded from 8mm to 10mm rods and it's certainly worth it. The gantry easily moves the weight and you get less deflection, especially if you want to use a direct drive extruder. 


---
**Julian Dirks** *February 08, 2018 09:57*

Great thanks 10mm non chromed all round it is.



Actually I was pretty tempted to put linear rails instead of 8mm as I see a few people have done that but figure I should walk before I run



What is a good brand of PETG to make the printed parts from. Esun is easily available here but can get some outside nz if people think it is worth investing in better quality filament?


---
**Oliver Seiler** *February 08, 2018 10:21*

I can highly recommend ColorFabb filaments. Not the cheapest, but consistent high quality. Shipping to NZ is a killer, but if you max out the Customs allowance (<$400) it's ok. Also 3DPrintergear in AU stock ColorFabb. I've recently put through 6kg of their economy filament and it works well.

Having said that my Eustathios ran on PLA parts for quite some time and still has a few here and there. I wouldn't recommend it but it works if you have limited choice. So from that eSun is probably totally fine.


---
**Eric Lien** *February 08, 2018 13:00*

I have printed spools and spools of esun petg. Works great. Just keep it dry or stringing can become worse.


---
**Dennis P** *February 08, 2018 17:57*

I have been printing with Inland rebranded eSun PETG. The stickers on the rolls are eSun batch numbers. The silver and the white are nice printing  filaments. Finished prints are 'hard' like PLA or ABS. However, the same prints with the translucent blue and magenta PETG versions turn out 'soft', too rubbery for my tastes. And those had a tendency to string a bit. YMMV 


---
**Julian Dirks** *February 11, 2018 09:59*

Thanks for your advice.  In the end I have ordered non-chromed 10mm rods ex Misumi inc gantry. 



On to the frame now. **+Oliver Seiler** have you bought anything from Mecha4makers?  I see they are in Welly and they cut/mill etc. t slot.  



Will look at Esun PETG/colorfabb.  I've never printed with PETG before but reading up on it sounds like it flexes a bit.  Is there anything to be gained by getting super tough PETG eg one with carbon fibre?


---
**Oliver Seiler** *February 11, 2018 11:24*

I haven't used Mecha4makers, so can't really comment on that.

Colorfabb XT (PETG) is my main go to filament and you'll be totally fine using it for Eustathios parts (as I do with exception of fan shrouds that come close to the hotend which I have used Colorfabb HT for).

XT CF20 prints very nicely (I've used it a lot, too), with a great finish and is certainly stiffer, but you need a hardened nozzle and I think it's overkill for what you need. 


---
*Imported from [Google+](https://plus.google.com/113795478307151372873/posts/KZFJGS1YoRR) &mdash; content and formatting may not be reliable*
