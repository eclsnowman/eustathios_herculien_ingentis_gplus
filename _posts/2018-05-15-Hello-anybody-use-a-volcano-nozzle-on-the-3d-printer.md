---
layout: post
title: "Hello, anybody use a volcano nozzle on the 3d printer"
date: May 15, 2018 11:07
category: "Discussion"
author: Mike Gallo
---
Hello, anybody use a volcano nozzle on the 3d printer.

What software do you use to generate the gcode and what setting to print it.  My initial print layer it's bad .can't figure out what I do wrong.  I try Cura or slicer , no luck.

Nozzle size used 1.2

Printer x y core

Board ramps1.4

Step driver Dr 8825

Power supply 12v 12amp



![images/5457d9abc5e28faef129d0f18d6b5fc3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5457d9abc5e28faef129d0f18d6b5fc3.jpeg)



**Mike Gallo**

---
---
**Eric Lien** *May 15, 2018 20:27*

Hello **+Mike Gallo**, welcome to the group. 



On my HercuLien printer I have two Volcano Nozzles. But they are only 0.6mm orifice . So not a monster like a 1.2mm or something like that. I just use the volcano for the extra melt capacity. 



I slice in Simplify3D. My rule of thumb is run a thick first layer height (around 1.5X the nozzle diameter). I then go to lower layers on layer 2 and up. It allows for better part adhesion to the bed and allows a little more wiggle room for first layer height calibration.


---
**Zane Baird** *May 15, 2018 20:58*

**+Mike Gallo** To add to what **+Eric Lien** has suggested, I would also go very slow with with the 1.2mm nozzle. 



I would start by calculating the volumetric flow rate of your typical settings for a 0.4mm nozzle with a regular E3d V6 (layer height * extrusion width * print speed). You can easily get 2x that volumetric rate with the volcano which maxes out at around 30mm/s (this is very general and depends on the material and temperature). You'll have to adjust your speed accordingly and it will seem like you are going very slowly, but the amount of plastic you are extruding is actually quite a lot. 



An example:



For a 0.2mm layer height  with 0.5mm extrusion width @ 100mm/s the volumetric flow rate comes out to: 0.2x0.5x60 = 10 mm3/s



Now lets say you want to max out your volcano print speed with 1.2mm nozzle @ 30mm3/s volumetric extrusion rate, we need to rearrange this equation to:

(30mm3/s) / layer height / extrusion width = print speed



So with a layer height of 0.6mm and an extrusion width of 1.5mm we get:

30 / 0.6 / 1.5 = 33.3 mm/s  



Note that this is the MAXIMUM speed you will be able to run with these layer settings. Realistically you would want to operate in the 20-25 mm3/s range which equates to 20-27 mm/s print speed.



My guess is you need to slow down quite a bit with the 1.2mm nozzle to see the improvement in print quality. The effect isn't as pronounced with the smaller nozzles as you aren't dealing with nearly as high a rate of volumetric extrusion. I would suggest trying Slic3r if you don't want to do the math on your own as this has a tick box that will allow you to enable volumetric control of extrusion rate and will adjust speeds accordingly.




---
**Bruce Lunde** *May 15, 2018 22:53*

I have the volcano, using cura with no problems at this time. I have the .6 nozzle though, but most of my problems were due to initial settings,  that this group helped me out on.


---
**Mike Gallo** *May 16, 2018 00:06*

**+Eric Lien** 

thank you for the input , i do have the kit of nozzle size from 0.2 to 1.2 so i will try with your setting. 


---
**Mike Gallo** *May 16, 2018 00:14*

**+Bruce Lunde**  yes, you right i have the same kind problem , initial print starts bad.  i hope to fix the  problem i have.  


---
**Mike Gallo** *May 16, 2018 02:21*

so, far:

 i try the formula of Zane Baird and i got i little better . i think i have other issue now  the z-offset not the right height, 

one more think the nema17 motor get to hot. my vref for the motor are X .515, Y .530, Z .939 (2 motor)  driver step drv8825 polulu.  

here a sample of top rocket printed in  few minutes. 

software use repetier-host , engine slicer Cura.

should i leave the feed rate to 100, i reduced to 80%, i forgot to turn the fan at halfway print( oops).the printed object its hard to crush ,infill at 20%



![images/d085907f5962bdb8d7db2027e59ca778.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d085907f5962bdb8d7db2027e59ca778.jpeg)


---
**Eric Lien** *May 16, 2018 03:31*

Why so much current on Z?



Z motor is being held always on, to hold position, so will generate a bunch of heat at that vRef. If your Z is leadscrew driven the mechanical advantage on Z should allow for a lower (not higher) vRef than the other axis.


---
**Mike Gallo** *May 16, 2018 03:35*

Yes , the z axis have two nema 17 and lead screw to move the bed up and down. So you saying to lower the Vref to a .450- .500mVolts.

I test it tomorrow. Thank you.


---
*Imported from [Google+](https://plus.google.com/108921378497443668729/posts/a2aNw1ns7s6) &mdash; content and formatting may not be reliable*
