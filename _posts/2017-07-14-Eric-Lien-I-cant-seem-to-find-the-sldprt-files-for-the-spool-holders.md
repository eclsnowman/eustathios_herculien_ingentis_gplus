---
layout: post
title: "Eric Lien I can't seem to find the sldprt files for the spool holders"
date: July 14, 2017 12:51
category: "Mods and User Customizations"
author: James Ochs
---
**+Eric Lien** I can't seem to find the sldprt files for the spool holders.  The 50mm ones were perfect for my petg spools, but the 32 mm ones are 10mm too short for the pla spools I have.  Are they in one of the zip files for the other formats?



Also, does anyone know if there's a way to convert solidworks files to fusion 360 and keep the history/timeline?  the sldprt files import to fusion 360 pretty easily, but there's no history so a simple mod like adding the 10mm to the extrusion becomes a bit more complex ;)



Thanks!





**James Ochs**

---
---
**Eric Lien** *July 14, 2017 16:18*

I have many sizes here (STL and solidworks).



[https://www.youmagine.com/designs/spool-holders](https://www.youmagine.com/designs/spool-holders)


---
**Eric Lien** *July 14, 2017 16:19*

I can try and make a variation for you. Can you confirm all the Dimensions you need?


---
**James Ochs** *July 15, 2017 01:03*

thanks!  the 32mm diameter is perfect, but the spools I have are 86mm wide (the 32mm spool holder is 76mm from the base to the start of the lip on the end.)



I think another 11mm in length would probably do it.


---
**Eric Lien** *July 15, 2017 07:20*

[drive.google.com - 32mm_Non-Bearing_Spool_88L_Hub.SLDPRT - Google Drive](https://drive.google.com/file/d/0B1rU7sHY9d8qR1VCSWoxdlhGWjA/view?usp=sharing)



[https://drive.google.com/file/d/0B1rU7sHY9d8qVFQtYXFWOTRKNWc/view?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qVFQtYXFWOTRKNWc/view?usp=sharing)


---
**Eric Lien** *July 15, 2017 07:21*

There is the STL and the Solidworks Versions of the 32mm OD x 88mm Long.




---
**James Ochs** *July 15, 2017 17:46*

Thanks Eric, that worked perfectly!


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/1W21AUHPJCP) &mdash; content and formatting may not be reliable*
