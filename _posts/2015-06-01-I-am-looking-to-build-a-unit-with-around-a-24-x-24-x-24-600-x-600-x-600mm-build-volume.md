---
layout: post
title: "I am looking to build a unit with around a 24\" x 24\" x 24\" ( 600 x 600 x 600mm) build volume"
date: June 01, 2015 13:15
category: "Discussion"
author: William Eades
---
I am looking to build a unit with around a 24" x 24" x 24" ( 600 x 600 x 600mm) build volume.  Which one of the three would be the best starting point?





**William Eades**

---
---
**Godwin Bangera** *June 01, 2015 13:29*

Hercules !!!

(sorry HercuLien, in my opinion ;)     )


---
**Dat Chu** *June 01, 2015 13:42*

Herculien 


---
**William Eades** *June 01, 2015 13:53*

**+Godwin Bangera** **+Dat Chu** Thanks.  I started out with a Rigidbot Regular, but need a MUCH larger BV.


---
**Eric Lien** *June 01, 2015 14:23*

**+William Eades** for 24" cubed build volume I would start looking beyond round rods for the axis. Runout and deflection will be your enemy on larger and larger spans. This is because the rods act to both power transmission (rotation) and an axis guide (translation). I would look into linear guide rails for the sides, then slave rods to tie the two sides together. Look into **+Tim Rastall**​ and his newer ideas. 


---
**William Eades** *June 01, 2015 14:39*

**+Eric Lien** I had been looking at various formats for the build and had planned on guide rails because of the induced harmonics and flexion in the long rod spans.  That said, I still really like the straight forward simplicity of your design.


---
**Eric Lien** *June 01, 2015 15:14*

**+William Eades** thank you. I have been happy with the design. I am excited to see what you come up with.


---
**William Eades** *June 01, 2015 15:41*

**+Eric Lien** Being an old lab rat it will probably be an over-engineered nightmare...but it will be accurate!  ;-}


---
**Eric Lien** *June 01, 2015 17:16*

**+William Eades** I learned the same thing from my dad "if you can't drop it, and still have it work perfectly... Then it needs to be built stronger"


---
*Imported from [Google+](https://plus.google.com/+WilliamEades_Frostbite/posts/dGAE4sFmaNL) &mdash; content and formatting may not be reliable*
