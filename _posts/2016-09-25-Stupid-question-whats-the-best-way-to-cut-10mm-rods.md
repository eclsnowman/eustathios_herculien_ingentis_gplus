---
layout: post
title: "Stupid question: what's the best way to cut 10mm rods?"
date: September 25, 2016 08:39
category: "Discussion"
author: Oliver Seiler
---
Stupid question: what's the best way to cut 10mm rods? Since I'm using Walter's Mini space invaders carriage I want to move to 10mm rods and have a couple that are a little bit too long. I've read somewhere that they are difficult to cut, but haven't tried myself yet.





**Oliver Seiler**

---
---
**Sven Eric Nielsen** *September 25, 2016 10:34*

Flex and high speed disc. No problems so far. 


---
**Ryan Carlyle** *September 25, 2016 12:04*

Any high-speed abrasive cutoff is fine. Don't bother with a hacksaw, the hardened rod surface is almost as hard as the hacksaw teeth. 


---
**Stephen Baird** *September 25, 2016 17:21*

In my experience hacksaws just bounce on the surface of a hardened rod while the rod laughs, it doesn't even leave a scratch. A dremel (or proxxon) reinforced cut off wheel made short work of mine. 


---
**Sven Eric Nielsen** *September 25, 2016 17:41*

**+Oliver Seiler**​

Seriously, don't waste your time with other toys. Just use this :



![images/6c66bafc5c1d3d6499bf1b082b781c17.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6c66bafc5c1d3d6499bf1b082b781c17.jpeg)


---
**Roland Barenbrug** *September 27, 2016 18:45*

**+Sven Eric Nielsen** Brand, type ?


---
**Sven Eric Nielsen** *September 28, 2016 06:30*

In my case it's Einhell. But it doesn't matter. I think even the cheapest thing you can get will do this simple job. 


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/4oePamQ5Rqv) &mdash; content and formatting may not be reliable*
