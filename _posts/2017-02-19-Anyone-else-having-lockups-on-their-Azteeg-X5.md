---
layout: post
title: "Anyone else having lockups on their Azteeg X5 ?"
date: February 19, 2017 04:20
category: "Discussion"
author: jerryflyguy
---
Anyone else having lockups on their Azteeg X5 ? I've had several and it's starting to tick me off, 24hrs into a 30hr print and it's wrecked. Happens randomly, sometimes it's once a week, then it won't happen for a month. 



Things to test or check? 





**jerryflyguy**

---
---
**Eric Lien** *February 19, 2017 04:23*

Is it the board locking up, or the comms? What do you use to stream the gcode? Laptop, on board SD, LCD display SD, octoprint?


---
**jerryflyguy** *February 19, 2017 04:26*

It's looks like normal on the viki but is unresponsive when I try to do anything via the knob. 



I load files via USB to the card. Card is installed in the slot on the actual board(not the viki)


---
**Sean B** *February 19, 2017 04:29*

Maybe your USB is going to sleep. Make sure you turn off the USB selective suspend, I think that's what it is called... Basically it tries to save battery on laptop and puts the USB ports to sleep.


---
**Eric Lien** *February 19, 2017 04:32*

How are your thermals on the board? You have active cooling on the board?



One downside of the SD Printing method is you lose out on potentially helpful serial data that might explain the reason for the failure. Maybe try a new SD card after a clean format (I had a bad SD card once that gave me similar headaches).



Another option is to put a voltmeter on the PSU and confirm you have stable output voltages under load (had a PSU go bad once too).



Last thing that comes to mind is try a more recent version of smoothieware firmware and edit a clean config file (depending on how old your old smoothieware version is since sometimes they make changes where old  config files don't work).


---
**Oliver Seiler** *February 19, 2017 05:26*

I get that very rarely when I leave the USB connected to my laptop, so I always disconnect usb before printing from sd card. Then use the web interface to monitor or adjust. 


---
**jerryflyguy** *February 20, 2017 02:48*

**+Eric Lien** I think the thermals are ok? 120mm fan blowing on it continuously. It loses steps but will try to print if the fan is removed (different failure)



Will try the other options you mentioned. One question, where do I go get the latest firmware? Also how do I install it? I assume this is different from the Config file?


---
**Oliver Seiler** *February 20, 2017 03:01*

**+jerryflyguy** see here for updating the firmware

[smoothieware.org - flashing-smoothie-firmware [Smoothieware]](http://smoothieware.org/flashing-smoothie-firmware)




---
**Stefano Pagani (Stef_FPV)** *February 26, 2017 15:23*

Make sure your sd is not full. If you put stuff on it with simplify directly it sometimes glitches. I would put in on your computer than on the SD.


---
**jerryflyguy** *February 26, 2017 15:28*

**+Stefano Pagani** I put it into a folder and then copy it across.. there ~10 files, each ~50mb so don't think it's full? 


---
**jerryflyguy** *March 10, 2017 21:48*

So just an update. I loaded new firmware, updated the config, formatted the SD and I'm still getting lockups, maybe even worse that before. Will check PS voltages but I'd think that's an outside chance as far as a failure. It's a oversized meanwell PS, but then.. anything is possible 


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/PJos62cWbbw) &mdash; content and formatting may not be reliable*
