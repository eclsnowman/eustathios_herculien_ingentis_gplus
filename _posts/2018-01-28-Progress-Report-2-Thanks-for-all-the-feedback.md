---
layout: post
title: "Progress Report #2, Thanks for all the feedback"
date: January 28, 2018 05:55
category: "Discussion"
author: Bruce Lunde
---
Progress Report #2,  Thanks for all the feedback.  I am getting much closer to where I want to get to this weekend!  Thanks for all the responses.  I think X and Y are on the money.  Tightened belts, check settings,  and got  the latest version of Cura. Now I just have to figure out the Z.  This test cube ( **+Dennis P** ) told me to try really helped.  Tweaked, tried to print, tweaked again.   Z is off by a bit.  But much better than yesterday.







![images/ef73874e90c5d2a24dde2e691682fc6f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ef73874e90c5d2a24dde2e691682fc6f.jpeg)
![images/889a64d77fee4520039fdb5f953eef3b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/889a64d77fee4520039fdb5f953eef3b.jpeg)
![images/eb352a82778dc4551659ef9b60b974e3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eb352a82778dc4551659ef9b60b974e3.jpeg)
![images/2680e07a8575ce6939a4ddda6b145e10.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2680e07a8575ce6939a4ddda6b145e10.jpeg)

**Bruce Lunde**

---
---
**Dennis P** *January 28, 2018 15:54*

Did you print that in Vase mode? How thick a wall? Did it warp at the bottom? If so, that might give you error in the Z. 

Steps- what did you calculate for  steps per mm? 1/16 microstep?

2mm leadscrew pitch? 20T pulley on motor? 32T on leadscrew? 

(((360 deg/1.8 deg/step)<b>(1/(1/16utep)))/2mm/rev)</b>(32T/20T)=1000 steps/mm ?

How many layers did your slicer compute for the object and at what thickness? Did it scale it up to account for ABS shrinkage?

(50 layers)<b>(1mm/layer)</b>(102%/100%)= 51mm 


---
**Bruce Lunde** *January 28, 2018 16:42*

**+Dennis P** I  don't know what vase mode is?  I just used the default setting on Cura and put in the basic size of my bed and my nozzle size (.6 mm), then loaded the stl you sent.  I will go and review the items you mentioned.

I am using a smoothie board and smoothieware, I will go and double check the setting there. I think my board is 1/16 microsteps. I have the Mitsumi leadscrews, I believe they are 12mm/2mm pitch, and my steps are at 3200. I used the standard parts from the BOM,  so 36 tooth for lead screws and 20T on motor.  I need to check that calibration, another one I last touched a year ago.


---
**Dennis P** *January 28, 2018 17:25*

with 1/16 ustep, 2mm pitch, and 36T/20T ratio,  I get 3555.55 steps/mm.... If that is right, and you are using 3200, then the difference is close to what you measured. 3555.55/3200 = 1.111 =>55.55 on the 50mm cube. 



EDIT: My math is wrong. That should be 1/4 of that, 888.88. 



Vase mode is single extrusion outer wall with a bottom. Honestly, I dont know Cura. On my older version its called Spiralize or something like that and its hidden in the Expert settings. Its one continuous corkscrew extrusion. I think you can trick it by setting the shell width to be your nozzle width, 0% infill, no top layers, one bottom later.  With my slicer, using 0.6mm nozzle, 0.4mm layer height it says it would take about half an hour to print the shell. How long are your test prints taking? 


---
**Bruce Lunde** *January 28, 2018 17:56*

**+Dennis P** That print took just short of 5 hours last night. LOL, I have a lot to learn!  

I will try again with Cura slicing that model, and lower the infill. Time to put on some of my favorite music from the 60's and read!  I have a singular focus this time around,  I will not allow myself to play with any other toys until I get this lined out.   Thanks for the feedback, and the math!


---
**wes jackson** *January 28, 2018 23:54*

Remember smoothie is 1/32 step, not 1/16.


---
**Bruce Lunde** *January 29, 2018 03:04*

**+wes jackson** Mine is older, it is 1/16.


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/MzasNXxp6u2) &mdash; content and formatting may not be reliable*
