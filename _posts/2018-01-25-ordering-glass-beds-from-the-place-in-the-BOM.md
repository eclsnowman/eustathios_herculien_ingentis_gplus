---
layout: post
title: "ordering glass beds from the place in the BOM"
date: January 25, 2018 16:51
category: "Discussion"
author: wes jackson
---
ordering glass beds from the place in the BOM. anyone want to do a groupbuy? i'll be getting the quotes later today and will post them.





**wes jackson**

---
---
**Eric Lien** *January 25, 2018 17:35*

What are peoples thoughts about wanting the system to integrate a probe and PEI covered spring steel flex plate? 


---
**Eric Lien** *January 25, 2018 17:35*

BTW Wes, thanks for coordinating a potential group buy.




---
**wes jackson** *January 25, 2018 17:51*

Np! And are you thinking like the mk3? Friend has the mk3 and the bed is pretty nice


---
**Zane Baird** *January 25, 2018 18:11*

I would love to have a PEI covered spring steel flex plate. On both my Herculien and coreXY.


---
**Eric Lien** *January 25, 2018 18:34*

Anyone know a good source for waterjet cutting and plating springsteel? And what grade spring steel is good for our temp ranges?



Thanks,


---
**Ryan Fiske** *January 25, 2018 18:58*

I would definitely be interested!


---
**Eric Lien** *January 25, 2018 19:17*

looks like maybe people are using 1095 Spring Steel? 




---
**Eric Lien** *January 25, 2018 19:17*

Then again Buildtak makes a 12x12 version, perhaps going with a standard one that is easily sourced would be better?



[https://www.buildtak.com/product/flexplate/](https://www.buildtak.com/product/flexplate/)


---
**Zane Baird** *January 25, 2018 19:48*

**+Eric Lien** Going with a 12x12" might be a way to make it "universal" and instead design magnetic mounting options that are compatible with each printer. For oddly shaped beds like that of my coreXY (11.5"x19.75") or larger printers like the Herculien I think it would be good to find an OEM willing to provide custom sized sheets though




---
**Ryan Fiske** *January 25, 2018 20:13*

I noticed the prusa mk3 uses a couple alignment pins on their spring steel sheets to keep it in place. Is that something to consider as well?


---
**Eric Lien** *January 25, 2018 20:17*

**+Ryan Fiske** Yes, I like that design. Magnets are good at pulling in line with the magnetic poles, bad bad in shear across the poles. So pins take all the shear load to avoid shifts, and the magnets just hold it down.


---
**wes jackson** *January 25, 2018 20:18*

Here is the below quotes for the glass. This does NOT include shipping/freight.



Clear Borosilicate Glass, Edges Cut & Swiped



345mm (13.58”) Wide X 360mm (14.17”) Long



 



3.3mm thickness



 



Qty. 1                          $50.00 (minimum)



Qty. 2-5                       $27.61/each



Qty. 10                        $25.88/each



 



5mm thickness



 



Qty. 1                          $50.00 (minimum)



Qty. 2-5                       $36.54/each



Qty. 10                        $35.08/each






---
**wes jackson** *January 25, 2018 20:19*

Also, pins would be nice for alignment, but the magneys should be strong enough by themselves


---
**Dennis P** *January 26, 2018 00:31*

i am interested in the group buy too! i think its kinda curious how this fall during girl scout cookie season too....the glass migth be cheaper by the ounce  anyhow, i have yet to see an i3Mk3 bed in the wild- I would be worried about the residual stresses in the spring steel. it is GOING to have some warp in it naturally on account that its a coil product. as far a grade goes,   could be 4130 too. 

my plan was to get some PEI sheet from CS Hyde and then go from there.. they do have up to 10-20-30-40 mil sheets. the 30 & 40 roughly translates to 26 ga and 20 gauge sheet metal. you could use magnets to trap it to a ferrous element under the sheet and locating pins to fix it in X & y. What I am hoping will work is to use water based glue stick or aquanet to stick it to to the glass. That way if i needed to get the film off, soak it off and flex it a bit to release the print then reattach. Prblem is, PETG sticks too good to PEI. I am ok with just Aquanet on glass. 



[catalog.cshyde.com - 3D Printing Surface/Platform: Ultem® PEI, Kapton® Tape, FEP Tape, 3M™ Painters Tape.](http://catalog.cshyde.com/category/3d-printing-materials)  




---
**wes jackson** *January 26, 2018 06:22*

if you go glass, the best bed is elmers E38XX glue, undiluted. spread it out on the bed, wet your hands and smooth the top out. it will get you a mirror finish and  once the print cools it slides right of. as far as the spring steel goes, it does deform, but only naturally back to the origianal position, like muscle wire or sorbothane.


---
**Bruce Lunde** *January 27, 2018 18:48*

**+wes jackson** I am also interested in the group purchase . I currently am using a piece of glass form an old refrigerator, and it is under sized.


---
**wes jackson** *January 28, 2018 09:06*

so thats 3 so far, how long should we wait?


---
*Imported from [Google+](https://plus.google.com/109780842416628726318/posts/BsGxQbYYC5m) &mdash; content and formatting may not be reliable*
