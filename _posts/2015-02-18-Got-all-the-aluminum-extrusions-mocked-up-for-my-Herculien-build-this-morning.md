---
layout: post
title: "Got all the aluminum extrusions mocked up for my Herculien build this morning"
date: February 18, 2015 16:16
category: "Discussion"
author: Daniel Salinas
---
Got all the aluminum extrusions mocked up for my Herculien build this morning.  Now I need to tear them down and start doing videos of assembly since I'm waiting on 4 more eccentrics which won't be here til Friday.  Hopefully soon **+Eric Lien** we'll have a build doc with pics and videos for building a Herculien.  I need to print a gopro tripod mount today while I'm working so I don't make completely amateur videos :)





**Daniel Salinas**

---
---
**Eric Lien** *February 18, 2015 18:43*

You are a gentleman and a scholar good sir.


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/XAc22f8JSiH) &mdash; content and formatting may not be reliable*
