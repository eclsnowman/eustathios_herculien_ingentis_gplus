---
layout: post
title: "Has anyone tried this?"
date: March 14, 2015 09:36
category: "Discussion"
author: Gus Montoya
---
Has anyone tried this?



[http://www.3ders.org/articles/20150308-test-your-3d-printer-limits-with-this-hard-to-print-test-object.html](http://www.3ders.org/articles/20150308-test-your-3d-printer-limits-with-this-hard-to-print-test-object.html)





**Gus Montoya**

---
---
**Uche Eke** *March 14, 2015 09:39*

Not yet, but I'm going to now :) I tried an overhang test when calibrating but this looks far more comprehensive in scope



Thanks Gus **+Gus Montoya** 


---
**Gus Montoya** *March 14, 2015 10:01*

Please keep us in the loop, in your printing this test object. Once I finish my printer, I want to try it also.


---
**Gus Montoya** *March 17, 2015 06:30*

I wonder if I should even print this to calibrate my printer once it's done. Honestly if it does not print well, I won't know what or how to troubleshoot. 


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/EfKQi5G8EfC) &mdash; content and formatting may not be reliable*
