---
layout: post
title: "This is my technique for the frame"
date: March 11, 2014 03:54
category: "Show and Tell"
author: D Rob
---
This is my technique for the frame. I believe ,as most people do in what they do, that this could be a superior frame layout and the vid explains why

![images/6dda7f73626b28bab3ac0995d326399e.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6dda7f73626b28bab3ac0995d326399e.gif)



**D Rob**

---
---
**Eric Moy** *March 11, 2014 09:38*

Didn't you buy the **+MISUMI USA** extrusion with the first150--Reddit promotion? There's still time if you haven't. The extrusion and brackets are really inexpensive. Plus you can buy the preconfigured box, so everything is precut and kitted. It's for those of us who are to busy/lazy to mill cut the parts themselves. Your method is pretty clever, but I'm still concerned that it does not guarantee squareness as you could end up with a rhomboid instead of a cube, but clever nonetheless.


---
**Dale Dunn** *March 11, 2014 12:12*

I don't trust using a tape measure to align the frame. Even a tape measure that is marked in 1/32" (0.5 mm outside the US?) will not really be that accurate in application. In the cases where a measurement must be made, cut a piece to use as a jig everywhere that dimension is repeated. Then your measurement should be repeatable within a few thousandths of an inch, maybe 0.010" [0.25 mm] even though it's not nearly that accurate. At least the frame will be (more) square.



I'm thinking that if each set of parallel pieces are cut at the same time or matched with a file as described, you will get a more square frame than adjusting butt joints with a tape measure. Adjusting them with a master piece for each length might be best for those of us who don't have tools to cut several pieces at once to get matched lengths.


---
**D Rob** *March 11, 2014 13:46*

**+Dale Dunn** Very good. A jig is an excellent idea. I had misumi cut the extrusions which will be here day after tomorrow. For the next itteration :) 


---
**D Rob** *March 11, 2014 13:55*

**+Eric Moy** I did order some from misumi, but not brackets as they are not necessary. For the square top and bottom frame it is almost impossible to end up with a rhombus if you measure correctly or use a jig as **+Dale Dunn**  mentioned. This method makes the only critical cuts the upright extrusions. Any timet you have a piece that sits between two pieces it is a spacer. if it is longer orshorter than the others it isn't adjustable. This cant be done away with 100% without strange design choices that weaken the frame, but for two major areas it really gives some tolerance for cuts. if there are slight overhangs you can file the outside edges after its  bolted tight. so the goopy plastic corners fit well.


---
**Dale Dunn** *March 11, 2014 14:52*

I'm looking forward to hearing how accurate and consistent Misumi's cuts are. IIRC, you can select lengths in 0.5mm increments.


---
**D Rob** *March 12, 2014 00:57*

I ordered by the mm don't recall if that was as tight as I could


---
**Eric Moy** *March 12, 2014 09:26*

**+D Rob** I agree that you've limited the critical cuts to just the vertical if using a jig, but the brackets are a huge improvement, though technically not necessary. They form a larger frame at each corner, which improves rigidity immensely by spreading the moment. Your current design has each corner fastened by 2 screws, both self tapping into the axial hole of the extrusion. Using brackets, you have the load shared between 6 screws, threaded into steel nuts with preformed threads. They can be finger tightened, and the threads are perfect. There is no need for further printed corners, and the brackets made from machined extrusion, so the dimensions will be very tight. It will be far more accurate and rigid than printed ABS. Using a jig, you technically would not need to machine any ends using the bracket, they could all be rough cut, no holes drilled and no self tapping screws. 



But buying the precut parts from **+MISUMI USA**, all I needed was a couple straight edges (i used another piece extrusion, or another bracket) and a flat working surface to square things up, as the parts themselves could be used as squares.



Either way, cool video﻿. And I'm figuring that you can't use the brackets in many places, as it would interfere with the printed parts that hold the rails.﻿


---
**Dale Dunn** *March 12, 2014 12:29*

How about relying on acrylic or polycarbonate side panels to enclose the build area and strengthen the corners at the same time?


---
**D Rob** *March 12, 2014 14:36*

**+Eric Moy** there is no perceived slip nor rhombusing when I flex my frame. I do not use self tapping screws. I have an aluminum tap for 1/4-20 the screw's hold a positive force and the square cut butt end spreads out the surface area adding to the rigidity. This when properly tightened prevents the bolt location from acting as much of a fulcrum 


---
**D Rob** *May 25, 2014 21:05*

**+Bijil Baji** this may help you also


---
**Bijil Baji** *May 25, 2014 21:09*

Thanks **+D Rob**  But the problem i Have with it is there is no holes for mounting Screws in the middle i have  attached a picture of cross section  of extrusion


---
**D Rob** *May 25, 2014 21:31*

you would just drill a hole the a little bigger than the allen wrench used to tighten it. Based on your post showing 40 x 40 extrusion you would center your hole in the slot 20mm from the end of the extrusion for a square like in my video


---
**Bijil Baji** *May 26, 2014 05:24*

**+D Rob** I have this from my CNC endavours. I have a lot of it left over hear


---
**D Rob** *May 26, 2014 07:32*

I posted on another of your posts I just realized the square hole then. You may just have to bite the bullet and get the brackets or you could possibly get by with common l brackets from the hardware store. These will need to be adjusted. Bend then until they a perfect 90 with a vise and check with a square


---
**D Rob** *November 08, 2014 22:02*

**+Shachar Weis** here is the video


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/4dW4avdynUj) &mdash; content and formatting may not be reliable*
