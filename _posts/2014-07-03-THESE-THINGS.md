---
layout: post
title: "THESE THINGS!"
date: July 03, 2014 03:12
category: "Show and Tell"
author: Tim Rastall
---
THESE THINGS! 





**Tim Rastall**

---
---
**George Salgueiro** *July 03, 2014 03:20*

Still waiting for mine...


---
**Eric Lien** *July 03, 2014 05:00*

What about these [http://robotdigg.com/product/175/Selfgraphite-8*12*30mm-Linear-Bearing](http://robotdigg.com/product/175/Selfgraphite-8*12*30mm-Linear-Bearing)


---
**Eric Lien** *July 03, 2014 05:00*

Just with they also had 10mm﻿ in the self graphite style. But they are available on aliexpress.


---
**James Rivera** *July 03, 2014 05:39*

How do you order from Robotdigg.com? I tried and it would not tell me the shipping cost. ?:-/  Kind of important to me.


---
**Wayne Friedt** *July 03, 2014 07:01*

**+James Rivera** you have to submit an order them email them for the shipping cost.


---
**Liam Jackson** *July 03, 2014 07:28*

How did these compare to the SDP-SI self aligning bronze bushings? 


---
**George Salgueiro** *July 04, 2014 06:02*

**+James Rivera** Derek will send you an email. 


---
**George Salgueiro** *July 04, 2014 06:02*

You can pay with paypal


---
**Tim Rastall** *July 04, 2014 06:24*

They are also on Aliexpress.com with shipping included in the price.  Just search for robotdigg. 


---
**James Rivera** *July 04, 2014 06:55*

**+Tim Rastall** Ok, that works. Thanks!


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/Bun8jJG8Vh1) &mdash; content and formatting may not be reliable*
