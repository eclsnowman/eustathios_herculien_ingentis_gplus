---
layout: post
title: "Eric Lien I'm probably just being blind again, but I can't seem to find the printed part stl / files for the slider thingy that keeps the lid from falling over backwards, and I noticed that in the new pictures in the repo"
date: May 26, 2017 14:06
category: "Discussion"
author: James Ochs
---
**+Eric Lien** I'm probably just being blind again, but I can't seem to find the printed part stl / files for the slider thingy that keeps the lid from falling over backwards, and I noticed that in the new pictures in the repo you've replaced that with a metal folding thing (I don't know what that type of hinge is called)... do you have a part number/vendor for it?  Thanks!





**James Ochs**

---
---
**Eric Lien** *May 26, 2017 14:42*

Unfortunately that metal hinge an Ikea Klok Ferrari Hinge and is now an obsolete part. It was given to me by a member of the community who worked there and used it on their build.



Looking on Ebay there are some available: 



[ebay.com - Details about  Ikea Klok Stay Ferrari Itlay For Cabinet Cupboard - Brand New Sealed Package](http://www.ebay.com/itm/Ikea-Klok-Stay-Ferrari-Itlay-For-Cabinet-Cupboard-Brand-New-Sealed-Package-/162216431391?hash=item25c4da4f1f:g:fakAAOSwCGVX5x0~) 



If you find a good reliable source let me know and I will add it to the BOM.


---
**Eric Lien** *May 26, 2017 14:43*

Don't use my printed version of the banana slide stop. It was a poor design and I should really get around to updating that portion of the model.


---
**James Ochs** *May 26, 2017 15:09*

Ok, I'll poke around and see what I can find.  It looks like there are a few options along those lines, I may try a pneumatic one.


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/FabRZtET9CL) &mdash; content and formatting may not be reliable*
