---
layout: post
title: "Building update - I recieved the aluminium plate, plasma cutted by friend, I still need to find a glass supplier"
date: March 07, 2016 17:11
category: "Build Logs"
author: Maxime Favre
---
Building update

- I recieved the aluminium plate, plasma cutted by friend, I still need to find a glass supplier.

- I ended buying the smoothie, Azteeg looks great but I like to have some spare mosfets.

- Right now, cables management !

- I'm still waiting my aliexpress/ebay orders, hope they won't take two months to come...



![images/219e6f6b4a5fbf3d47a038d79e43b2e2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/219e6f6b4a5fbf3d47a038d79e43b2e2.jpeg)
![images/e0094a6a2a29930023574e5e1b4f04d9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e0094a6a2a29930023574e5e1b4f04d9.jpeg)
![images/21fe222a8adcb8bc70ddafb9c8b22673.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/21fe222a8adcb8bc70ddafb9c8b22673.jpeg)

**Maxime Favre**

---
---
**Eric Lien** *March 07, 2016 18:08*

WOW, that looks amazing. I love the cable tray setup under the 20x20. So clean. And the cable chain looping up to the hot end, then reversing and back down to the bed. This will be one sharp printer once completed.


---
**Seif El-Akkad** *March 07, 2016 18:10*

great job what will be the print area size?


---
**Maxime Favre** *March 07, 2016 20:27*

**+Seif El-Akkad** Standard Eustathios bed size, 320x320. Print surface will be smaller with the space invader carriage 290x290.


---
**Maxime Favre** *March 07, 2016 20:33*

**+Eric Lien**​  Thanks ! The 20x15 cable chain for the bed is bit big I'll try with a 10x10 or 10x15 later. The cables tray is 20x20, perfect match. Hope it gives ideas to some of you to get a clean wiring ;)﻿


---
**Maxime Favre** *March 10, 2016 09:37*

Golmart failed, I just recieved the ballscrews but with wrong machined ends. They send me the correct ones for free


---
**Eric Lien** *March 10, 2016 13:46*

**+Maxime Favre** I guess that is both bad and good. Sorry to hear about the mix up. But glad to hear they are rectifying it.


---
**Maxime Favre** *March 10, 2016 14:11*

Yep no big deal except the two weeks delay. They offered a remplacement immediately. I'll check if a friend can machine the ends but those ballscrews are pretty hard stainless steel


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/c8wskdTA1wX) &mdash; content and formatting may not be reliable*
