---
layout: post
title: "I just wanted to take a quick survey of the group: what plastic are you using to print your parts?"
date: February 10, 2017 15:23
category: "Discussion"
author: Shane Graber
---
I just wanted to take a quick survey of the group:  what plastic are you using to print your parts?  PLA?  ABS?  Other?





**Shane Graber**

---
---
**Michaël Memeteau** *February 10, 2017 15:45*

PLA and PETG mainly like a lot of people around  here I guess.


---
**Zane Baird** *February 10, 2017 15:48*

I've had really good results with PETg for most part. It has held up well  even in the enclosure of the Herculien. For my corner brackets I use CF-filled PETg for the added stiffness. In the future I may end up using a CF-filled nylon for some added heat resistance.



In reality, it all depends on what printing capabilities you have at your disposal


---
**Sean B** *February 10, 2017 15:52*

PETG pretty much exclusively.


---
**Shane Graber** *February 10, 2017 16:58*

I have not printed with PETG before.  What settings do you normally run?  I have an E3D v6 hotend, 0.4mm nozzle, heated PEI bed.


---
**Zane Baird** *February 10, 2017 17:05*

**+Shane Graber**  70C bed, 240-260C hotend (depending on filament and speed) and 0.2mm layers (typically). 40-60mm/s is a good range to aim for but you can easily crank it up to 80mm/s once you get it dialed in.



A note, you really need to dial in your extrusion to get good results with PETg. I normally do the single wall test to set my extrusion multiplier and aim for a very slight underextrusion. Keep infill moderately high (around 30%) for the best results.


---
**Eric Lien** *February 10, 2017 17:25*

I made all my parts a long time ago so used to ABS. But PETG should hold up great. The only thing I really like about ABS is that I can acetone soften the plastic bushing holes on the hotend carriage and side rail carriages before I pushed in the bushings so it both glues them in and make sure that I don't crack the part pressing in the bushing.


---
**Eric Lien** *February 10, 2017 17:26*

**+Zane Baird**​ is a whiz at PETG, so I you can't really go wrong using his settings :-)


---
**Zane Baird** *February 10, 2017 17:45*

A note on **+Eric Lien** advice on bushing inserts. With petg there is enough tensile strength that I've only ever cracked a part once when I basically pounded the bushing in. And it has hold equal to (sometimes better) than ABS/acetone insertion. PETG isn't as forgiving when you aren't printed on a well calibrated machine though


---
**Shane Graber** *February 10, 2017 17:52*

While I'm thinking about it: is anyone bringing their machine to the Midwest Reprap Festival coming up?




---
**Eric Lien** *February 10, 2017 18:18*

**+Shane Graber**  If I have room in my car I will have the following at MRRF: HercuLien, Eustathios, Talos3D Tria Delta, Talos3D Spartan Cartesian, ... And maybe something new if things go well :)


---
**Zane Baird** *February 10, 2017 18:26*

I'll have my Herculien and my Talos3D Tria as well.


---
**Ryan Jennings** *February 10, 2017 18:36*

**+Eric Lien** if all goes well there will be a couple other eusts at MRRF also. But you have piqued my interest on something new


---
**Eric Lien** *February 10, 2017 20:41*

**+Ryan Jennings** let's just say I have been having fun recently :)


---
**Markus Granberg** *February 11, 2017 00:10*

Nylon, abs, petg and tpe


---
**Shane Graber** *February 14, 2017 02:46*

Roughly how many kg of filament are required?  I'm placing an order for PETG.  :)


---
**Eric Lien** *February 14, 2017 23:37*

Depending on your perimeter and infill% you could get away with 1 roll. But if it were me I would get 2 rolls and go 3+ perimeters on all components under stress and 20%+ infill.


---
**Stefano Pagani (Stef_FPV)** *February 26, 2017 15:25*

I used 3 rolls running at 3 shells and %50 infil




---
**David Chapman** *March 05, 2017 22:56*

Thank you Shane just actually getting  started So you have a nice day K 


---
**David Chapman** *March 05, 2017 23:04*

**+Eric Lien** **+Eric Lien**  Sound sound I vote sure super very well peace .




---
*Imported from [Google+](https://plus.google.com/+ShaneGraber/posts/E9z966yqFfL) &mdash; content and formatting may not be reliable*
