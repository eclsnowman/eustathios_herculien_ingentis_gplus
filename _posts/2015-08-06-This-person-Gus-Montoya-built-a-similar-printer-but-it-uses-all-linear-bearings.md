---
layout: post
title: "This person Gus Montoya built a similar printer ( ) but it uses all linear bearings"
date: August 06, 2015 16:20
category: "Discussion"
author: Ted Huntington
---
This person Gus Montoya built a similar printer ([https://plus.google.com/u/0/107253148599893854804/posts/4REdLTz36HD?cfem=1](https://plus.google.com/u/0/107253148599893854804/posts/4REdLTz36HD?cfem=1)) but it uses all linear bearings. Are there any advantages to using the bushings linear+rotation design versus just the linear design?





**Ted Huntington**

---
---
**Frank “Helmi” Helmschrott** *August 06, 2015 16:25*

He's not using linear bearings but Igus plastic bushings. Also he does a similar but different mechanical approach. You may reach good results with both but the Igus bushings are definitely not a perfect fit like linear bearings. I would for example not using these plastic bushings on the Z-axis - they may work on the Y axis on this H-Bot/CoreXY-like design. Hard to tell from a few photos.


---
**Frank “Helmi” Helmschrott** *August 06, 2015 16:27*

just saw the Video. It's an H-Bot that i definitely would not prefer over a CoreXY.


---
**Ted Huntington** *August 06, 2015 16:37*

Yeah- I had never heard of plastic bearings- he calls them J plastic bearings- I thought there perhaps is some advantage to plastic over metal? Perhaps lighter and just as strong and similar price- but I haven't researched it. Yeah they are probably more accurately called bushings because they don't have ball bearings in them.


---
**Ted Huntington** *August 06, 2015 16:49*

I worry that requiring the bushings to accommodate rotation too adds to the friction, complexity and potential problems of the design- but probably it's a very minor worry or no problem at all.


---
**Frank “Helmi” Helmschrott** *August 06, 2015 16:49*

The difference between plastic and metal (mostly self lubricating) bushings is that you need slightly more play with the plastic to be able to move easily. And more play needs to more possible problem with 3d printing. There are some Igus products that work quite well in 3d printing but you definitely have to watch close where they work and where they don't.


---
**Ted Huntington** *August 06, 2015 17:03*

I am trying to decide between the CoreXY and H-Bot design for my next 3D printer build. I think the H-Bot design is maybe more simple than the CoreXY design- but perhaps there are clear advantages to the CoreXY (Eustathios) design?


---
**Ted Huntington** *August 06, 2015 17:10*

Perhaps the linear rails on the Y of the CoreXY add to the stability of the carriage for larger designs.


---
**Eric Lien** *August 06, 2015 17:19*

I have built both H bots and corexy using pbc linear bushing (similar to the igus bus PBC uses an aluminum housing and a Teflon/graphite hybrid inner coating). I guess I will say corexy and Hbot can be finicky without a really stiff gantry. I would use linear guide rails if I ever did it again.


---
**Ishaan Gov** *August 06, 2015 17:46*

Also, a lot of people use Igus bushings, designed for a press-fit into metal﻿, with a plastic clamp type of mount. This means that the bushings are oversized and have A lot of backlash without the press-fit 


---
**Ishaan Gov** *August 06, 2015 17:49*

**+Ted Huntington**, "J-plastic bushings" are probably the series of bushings that he's using; for my build, I'm also using J-series bushings (JSM-0810-16)


---
**Mutley3D** *August 06, 2015 18:01*

Linear bearings are not designed for rotation, only sliding along the shaft. Bushes are used to allow both rotation and linear motion together.


---
**Frank “Helmi” Helmschrott** *August 06, 2015 18:07*

**+Mutley3D** there's no rotation on the linked printer as far as i can see.


---
**Ted Huntington** *August 06, 2015 20:58*

Thanks for all the comments all- yes I am basically comparing what is apparently being called CoreXY versus H-Bot design- CoreXY where the smooth rods endure linear motion but also endure rotation about the long axis of the rod, and the X and Y smooth rods intersect at the carriage, versus the H-Bot design where the motion on the smooth rods is strictly linear, and the carriage only moves along X smooth rods.


---
**Eric Lien** *August 06, 2015 21:04*

Corexy should be linear only motion only just like an Hbot. Corexy is just a Hbot but where there are two belts instead of one. On the Corexy the belts have to cross over at some point, but the math and motion is identical. Both are just a 45degree coordinate transform of a standard Cartesian kinematic. The nice thing about corexy is the racking forces are more balanced than an hbot.



CoreXY: [http://www.corexy.com/reference.png](http://www.corexy.com/reference.png)



Hbot: [https://lh3.googleusercontent.com/-MxJfr_LJTI8/UomCgx0-KeI/AAAAAAAAAZI/qbFWUluQFso/s1600/std_hbot.png](https://lh3.googleusercontent.com/-MxJfr_LJTI8/UomCgx0-KeI/AAAAAAAAAZI/qbFWUluQFso/s1600/std_hbot.png)


---
**Ted Huntington** *August 06, 2015 21:19*

Yeah, so I have it wrong- I am comparing H-Bot with the Eustathios Spider V2 design, which must have some generic name. I thought that the CoreXY was totally different- yeah the geometry and motion design of the CoreXY looks so complicated- I know I am not going to mess with that! Perhaps there is a generic name for the Eustathios Spider V2 design- where both X and Y smooth rods are connected to the carriage.


---
**Mutley3D** *August 06, 2015 21:20*

**+Frank Helmschrott**​ it was more a brief description of the difference between the two bearings as opposed to a direct reference of the printer, to invoke OPs own deduction ;p


---
**Ted Huntington** *August 06, 2015 21:24*

perhaps +-Bot versus H-Bot?


---
**Vic Catalasan** *August 06, 2015 21:50*

**+Eric Lien** I was thinking a corexy with linear bearings. Now I need a smaller machine than the Herculien. It might not be a bad idea to start thinking of one :)


---
**Eric Lien** *August 06, 2015 22:28*

**+Vic Catalasan** I have a corexy out on my github... BTW don't build it ;) it was more hassle than it was worth. But if you built one like **+Martin Bondéus**​... Then you would have something :)




{% include youtubePlayer.html id=pAvWq0DD8uE %}
[https://youtu.be/pAvWq0DD8uE](https://youtu.be/pAvWq0DD8uE)


---
**Chris Brent** *August 06, 2015 22:55*

Wow that's quite a machine!


---
**Eric Lien** *August 06, 2015 22:57*

**+Chris Brent** I know, it makes mine look like they are standing still.


---
**Vic Catalasan** *August 06, 2015 23:11*

**+Eric Lien** Yup that is a nice one! redesign with Eustathios in mind? I want to make one that is 250x250x250 build area. 


---
**Marcos Duque Cesar** *August 07, 2015 02:32*

**+Mutley3D**

LBUW or LBUS is capable of rotary motion. Misumi have it (made in Vietnam).


---
**Mutley3D** *August 07, 2015 03:24*

**+Marcos Duque Cesar** Learning something new everyday. Although i was referring to the bearings in the traditional sense. I'll take a look at those for my build. they might be an ideal solution. I wonder about noise though...Igus or infused brass (oilite or graphite) are pretty silent and dont damage lower quality (cheaper) rods


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/HBVHwDfqxxz) &mdash; content and formatting may not be reliable*
