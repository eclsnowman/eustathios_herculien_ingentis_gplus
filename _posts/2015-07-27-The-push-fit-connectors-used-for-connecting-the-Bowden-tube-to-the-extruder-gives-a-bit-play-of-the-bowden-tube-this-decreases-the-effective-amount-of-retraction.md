---
layout: post
title: "The push-fit connectors used for connecting the Bowden tube to the extruder gives a bit play of the bowden tube, this decreases the effective amount of retraction"
date: July 27, 2015 12:51
category: "Show and Tell"
author: Martin Bondéus
---
The push-fit connectors used for connecting the Bowden tube to the extruder gives a bit play of the bowden tube, this decreases the effective amount of retraction. In order to counteract this there are two solutions.

1. Pushing the bowden tube hard and at the same time pull the plastic part of the connector upwards, it is quite fiddly but it works ok.



2. Using a small clip to insert below the collar of the plastic part, this takes away the play of the connector.



This would allow you to reduce your retract and make your retracts more consistent. This will also help if you have problems that the bowden tube gets pushed out of the connector. Also remember to

cut 5 mm off the bowden tube if you have pushed it out, the metal clips inside the connector needs a fresh surface in order to grip the tube correctly.



For normal use the gripping force should be more than enough, if you have temperature problems or foreign debris that clog your nozzle the bowden will get pushed out since the extruder does not slip. There could also be a build up on burn plastic inside the nozzle that inhibit correct heat transfer to the filament. For the E3D nozzles I have found that cleaning it out with a 2.0 mm drillbit by hand is a good way to remove any burnt residues.



You can download the clip for 1.75 & 3.0 mm here at [www.bondtech.se/stl](http://www.bondtech.se/stl) 

Print with a layerheight of 0.20 mm, fintune your needed thickness by scaling the models in Z.﻿

![images/31f287c4cc1e28692dace388e3be5d1b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/31f287c4cc1e28692dace388e3be5d1b.jpeg)



**Martin Bondéus**

---
---
**Seth Mott** *July 27, 2015 12:58*

I don't have a Bondtech, but Ithink this will help out my bowden retraction issues as well.  Thanks!


---
**Ian Hoff** *July 27, 2015 13:35*

I Have killed several kinds of pushfits. The clip will help, strain relief to prevent any any wiggle will help. I have started using nuts and threading them on to the ptfe.  8x32 thread on very snugly, enough it narrows the tube slightly. I put a 2mm drill bit in the end of the tube to stop it from squishing as much when threading the nuts on. I do turn the drill around and ream the end but very little come out. this has been one of the better connectors for me so far.



There may be a more appropriate nut, but these were handy and have not failed me yet.


---
**Martin Bondéus** *July 27, 2015 13:41*

You are welcome **+Seth Mott** !

**+Ian Hoff** The first official version were with M4 nuts threaded onto the ptfe-tube. Since users wanted a more convenient way of connecting & disconnecting the tube push-fits were integrated into the design. By using a 2mm drill-bit inside the tube when the nut is threaded on improves the function but I have stripped a number of ptfe-tube also with the nut, I guess the extruder is too strong :-)


---
**Joe Spanier** *July 27, 2015 14:17*

Nice Martin!


---
**Vic Catalasan** *July 27, 2015 15:54*

Love the idea thanks! Since getting the Bondtech extruder I have yet to mess with it and no slipping. My first 5+ hour print was very consistent throughout the print with a .8mm nozzle, I am going to try the 1.2 mm Volcano nozzle on it and reduce print time.


---
**Eric Lien** *July 27, 2015 19:06*

**+Vic Catalasan**​ only downside is everyone is purchasing my secret weapon now. People used to be impressed by my prints... Now the real pros will put me to shame ;)﻿


---
**ThantiK** *July 27, 2015 23:46*

**+Eric Lien** your secret weapon?  The Bondtech?


---
**Eric Lien** *July 28, 2015 01:23*

**+ThantiK**​ yup. The extrusion is always, 100%, everytime spot on... Even if my settings could use some tweaking. Have not had a jam in well over a year of printing. And no sign of wear on the bondtech. And my gears are before he started hardening them. He almost built it too good. No residual parts sales... That would never fly at makerbot, where extruders are consumables ;)﻿


---
**Ian Hoff** *July 28, 2015 03:55*

Heh, shoulda noticed I was commenting on a mfg's stuff... 



You have a lot more r&d than I have for sure, and with the glowing reviews of these guys I will certainly keep this extruder in the back of my mind :)


---
*Imported from [Google+](https://plus.google.com/+MartinBondéus-Bondtech/posts/VMas54yq3he) &mdash; content and formatting may not be reliable*
