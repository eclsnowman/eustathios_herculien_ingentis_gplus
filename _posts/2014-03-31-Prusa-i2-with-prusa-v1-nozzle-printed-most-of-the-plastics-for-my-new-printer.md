---
layout: post
title: "Prusa i2 with prusa v1 nozzle printed most of the plastics for my new printer"
date: March 31, 2014 03:00
category: "Show and Tell"
author: Jim Squirrel
---
Prusa i2 with prusa v1 nozzle printed most of the plastics for my new printer

![images/c15ae239dcfe8be4684602e1a0d6eced.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c15ae239dcfe8be4684602e1a0d6eced.jpeg)



**Jim Squirrel**

---
---
**Ricardo de Sena** *March 31, 2014 22:34*

Good job!!!


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/icBDU82qTBR) &mdash; content and formatting may not be reliable*
