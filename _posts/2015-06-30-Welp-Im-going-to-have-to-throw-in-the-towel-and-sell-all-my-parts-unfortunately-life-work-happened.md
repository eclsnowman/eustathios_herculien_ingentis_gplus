---
layout: post
title: "Welp, I\"m going to have to throw in the towel and sell all my parts unfortunately (life/work happened)"
date: June 30, 2015 00:42
category: "Discussion"
author: Seth Messer
---
Welp, I"m going to have to throw in the towel and sell all my parts unfortunately (life/work happened). I ended up purchasing all the best parts I could get (wanted the best FDM printer I could get).



Anyway, I have $1792.69 total in parts, most of the subassemblies are put together, however not tightened down or attached to extrusion.



If anyone is interested, please let me know. I of course want to get as much recovered as I can, but am also listening to offers. I've got a cost breakdown of everything too if you're interested.



Will also throw in my (only roll of filament actually) unopened roll of colorfabb woodfill.



I also have a ton of pictures of what i've assembled thus far and will give those to Eric for the next person to take up the helm of a build guide.





**Seth Messer**

---
---
**Eric Lien** *June 30, 2015 01:01*

It is sad that you were never able to complete the build, but we all understand that life sometimes gets in the way of our hobbies. I am sure these parts will find a good home.



You were always a fun part of the discussions... So I hope you won't be a stranger. Please keep in touch from time to time.



Take care Seth.


---
**Seth Messer** *June 30, 2015 01:12*

Thanks so much Eric. This community is lucky to have you as a contributor.


---
**Gus Montoya** *June 30, 2015 01:36*

I am interested in the build plate :)   I already bought a sheet of aluminum. But the quotes I am getting back to cut it are way too high. The average seems to be $150.00 with $125.00 the lowest. For 3 eustathios build plates or 2 eustathios and 1 herculein. It's not realistic for me. **+Dat Chu**  told me he had a qoute for his build plate at $45 shipped. I still need to factor the material and shipping cost. So if you have a build plate to part with, I would much like to take it off your hands. I can still return the sheet of metal I've already bought.  (I'm also interested in your breakdown and some pics)


---
**Erik Scott** *June 30, 2015 02:55*

Seth, very unfortunate you have to abandon it. Is there no way you could salvage it? If you really can't, do you have the silicone heater for the bed? If you absolutely sure you want to abandon the project, I would be willing to buy that from you. 



That's unfortunate Gus, I was looking forward to getting one from you. That said, it doesn't sound too far off the mark. McMaster says it's about $60 for an 18"x18" piece of 1/8" thick 6061 Aluminum. Or are those quotes just to cut it? Do they know it's just a simple 2D cutting job?


---
**Gus Montoya** *June 30, 2015 04:09*

**+Erik Scott** Yes the $150.00 is just to cut the sheet. The pricing is separated in the following way:



Material Prep time:20 min

Cut time: 18 min

Machine cleanup time: 20 min



Overall according to the machine shop it's about a 1 hr job, hence the $150.00. I was able to locate another shop that gave me two different quotes $45/hr and $75/hr. But they don't have the big machines the $150.00/hr places do. So it might end up costing more. 

   I really don't see how **+Dat Chu** got a $45.00 quote shipped. I've contacted Discount steel and I didn't get that price. 


---
**Bryan Weaver** *June 30, 2015 13:21*

I saw your comment on my Reddit post yesterday, sorry to hear you're giving it up.  I was very interested when you said you already had all parts in-hand, but i'm looking to go a bit more economical route and try to keep below $1000.  Best of luck to you.



As for the build plate, $150 just for cutting?!  It may not turn out pretty, but it sounds like i'm going to be cutting by hand :).  I've already got a 12" wide sheet of 3/16" aluminum, just not sure yet if I can stretch that enough to make it work..


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/j3SPFd1TyGd) &mdash; content and formatting may not be reliable*
