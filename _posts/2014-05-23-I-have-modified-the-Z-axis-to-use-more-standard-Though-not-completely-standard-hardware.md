---
layout: post
title: "I have modified the Z axis to use more standard (Though not completely standard) hardware"
date: May 23, 2014 05:38
category: "Deviations from Norm"
author: Erik Scott
---
I have modified the  #Eustathios  Z axis to use more standard (Though not completely standard) hardware. I decided I'd take the time to CAD up my Z-axis modification to make sure everything fits (It does!). I've decided to center the lead screw and double the smooth rods to 2 on each side for extra rigidity. I've also gone with 2 steppers as it will allow me to simply attach the lead screws directly with a coupling. Any thoughts or issues?



I'm toying with the idea of belting the Z-axis steppers together so they never get out of sync. You can see the timing belt pulleys just below the lead screw couplings. I haven't seen this on many other printers, however, and it does add some complexity. Do people have any thoughts on this?



![images/ae52c4aaa0f3828d77006cdd5f3beaa8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ae52c4aaa0f3828d77006cdd5f3beaa8.jpeg)
![images/ced4f89076780885c1541d1729d21a0d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ced4f89076780885c1541d1729d21a0d.jpeg)

**Erik Scott**

---
---
**Eric Lien** *May 23, 2014 06:09*

I run duel z screws off one stepper in both my corexy and my Eustathios. It works great. 


---
**Chet Wyatt** *May 23, 2014 11:09*

[http://forums.reprap.org/read.php?279,222917,245187#msg-245187](http://forums.reprap.org/read.php?279,222917,245187#msg-245187)



Just throwing it out there for discussion :) 


---
**Erik Scott** *May 23, 2014 14:18*

I'd like to know his reasoning. With such a large bed, I figured it'd be best to resist flex any way I could. 


---
**Eric Lien** *May 23, 2014 20:28*

**+Erik Scott** You are right... But remember extra constraints + any misalignment = extra friction.


---
**Erik Scott** *May 23, 2014 20:34*

This is something I've considered. I have a few methods in mind for aligning the smooth rods. I have left room for them to be adjusted, so it's a bit flexible. I'll also likely have to keep it well lubricated. I have two LM10UUs in each bearing holder (8 total for the bed) which should help with alignment. Worst case scenario is I have to go back to **+Jason Smith** 's original design, which wouldn't be the end of the world. 


---
**Erik Scott** *May 23, 2014 21:41*

Would you be willing to share it, if you haven't already?


---
**Erik Scott** *May 24, 2014 03:05*

Great! I'll have a look. 


---
**Tim Rastall** *May 24, 2014 06:19*

**+Erik Scott** I ended up choosing the single shafts per side as I was concerned about over constraining the Z with 2 per side.  The Ingentis bed,  using belts, doesn't exhibit any deflection during prints but the bending moment exerted on the bearing mounts is pretty severe if you apply accidental pressure.  If I had more time I'd swap the 10mm shafts out for 12mm ones and use SC12UU bearing holders (instead of the printed plastic ones). I'd like to see how your design pans out :) 


---
**Tim Rastall** *May 24, 2014 06:22*

**+Shauki Bagdadi** design is very clever but it uses a lot of belts and pulleys to acheive the 4 corner,  single screw moment. It may be a challenge to integrate it into the Ingentis /Eustathios envelope. 


---
**Jarred Baines** *June 05, 2014 11:15*

**+Erik Scott**, what **+Eric Lien** is saying is by adding more bearings and rods you are"constraining" the bed to be aligned. If the bed and lead screws and 4 shafts are all perfectly aligned then this setup will make it very rigid, but if all the rods, screws and bed aren't perfectly aligned without constraining them, constraining in so many places will add a lot more friction (which would matter more for x/y moves but is still a point to consider for z axis also.)


---
**Erik Scott** *June 05, 2014 20:26*

I understand his concern, and this may end up being the case. However, I'm going to give this a shot and see how it goes. 


---
**Jarred Baines** *June 05, 2014 23:01*

Yeah - I'm thinking I might also go down that path - the xmaker (search on youmagine) is very similar and he says its getting good results.

Why not, you can always unconstrain later if its necessary ;-)


---
**Tim Rastall** *June 05, 2014 23:36*

As long as the shafts are straight and you can leave the top brackets loose while you are getting everything aligned, I think this will work.


---
**Erik Scott** *June 06, 2014 01:36*

**+Tim Rastall** That is exactly how I'm planning on aligning them. I'm almost at that point now, just waiting for the bearings to arrive. 


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/gktKmk6ocHn) &mdash; content and formatting may not be reliable*
