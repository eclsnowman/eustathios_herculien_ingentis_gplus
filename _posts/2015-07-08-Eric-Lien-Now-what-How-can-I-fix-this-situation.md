---
layout: post
title: "Eric Lien Now what? How can I fix this situation?"
date: July 08, 2015 23:14
category: "Discussion"
author: Brandon Cramer
---
**+Eric Lien**  Now what? How can I fix this situation? 

![images/b4c832d94c3d7728b298221242a1ecf2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b4c832d94c3d7728b298221242a1ecf2.jpeg)



**Brandon Cramer**

---
---
**Ricardo de Sena** *July 08, 2015 23:21*

ABS?


---
**Eric Lien** *July 08, 2015 23:26*

PLA or ABS?


---
**Brandon Cramer** *July 08, 2015 23:28*

PLA


---
**Erik Scott** *July 08, 2015 23:38*

CA and a clamp. Or re-print. 


---
**Brandon Cramer** *July 08, 2015 23:39*

Re-print exactly the same? What is CA?


---
**Chris Brent** *July 08, 2015 23:53*

CA = cyanoacrylate = super glue


---
**Brandon Cramer** *July 08, 2015 23:55*

I will try reprinting it a few times before I use super glue. That just doesn't seem right. :)


---
**Eric Lien** *July 09, 2015 00:26*

I use ABS so I can acetone soften the hole before pressing in the bushing. It also acts to glue the parts in. If you stay with PLA put sand paper on an appropriate sized cylinder (sockets work nice), and open up the hole if your printer is making them underside. 



I have tuned my printer to the point I am usually 0.1 to 0.15mm from nominal now. So I haven't run into the cracking in a while. Also I would consider PETG for the carriage. It has incredible interlayer bonding so may perform better. And has a shrink factor similar to PLA.﻿


---
**Eric Lien** *July 09, 2015 01:02*

If you use ABS acetone on a q-tip works great to soften the hole before pressing it in. Just get the q-tip wet with acetone and wipe it on the mating surface, a few applications are required. Once it is softened it you can press it in like a hot knife through butter.


---
**Vic Catalasan** *July 09, 2015 01:18*

A Dremel sanding drum worked for me. You should not have to apply so much force to get the bearings in, doing so will crack the housing and make it hard for the bearing to align. When you print you might try higher fill ratio as well.  


---
**Erik Scott** *July 09, 2015 01:38*

If you do re-print it, make sure you make the hole a bit bigger. You can find the cad files for my printer here: [https://cad.onshape.com/documents/a45f6c99e2154f3b94955175/w/443cf4d7046e41d1b21d5426](https://cad.onshape.com/documents/a45f6c99e2154f3b94955175/w/443cf4d7046e41d1b21d5426)



Go ahead and open up the carriage and adjust the holes to account for the error. 



If you don't want to use my version, you can adjust Eric Lien's version in designspark, the files for which he kindly provided. 


---
**Eric Lien** *July 09, 2015 01:51*

**+Vic Catalasan** I like high perimeter medium infill over high infill.


---
**Jeff DeMaagd** *July 09, 2015 13:57*

Is there a FAQ or readme on the github on assembly techniques? I think this is the third time this has been reported, and it's an easily avoided problem if the user knows what to do.


---
**Eric Lien** *July 09, 2015 14:40*

**+Jeff DeMaagd** no readme yet. Seth was working on it, but has had things come up. I would love it if the community could help on the assembly instructions side. I think many have taken pictures and notes... But with my new job I have less time than before.


---
**James Rivera** *July 09, 2015 18:27*

+1 on **+Eric Lien**'s suggestion for using a Q-tip with acetone (if ABS). I did the same thing to "weld" some plastic on a broken tripod--works like new!  Also, I have not tried it myself (yet), but for PLA, perhaps a hot glue gun might work?


---
**Jeff DeMaagd** *July 09, 2015 20:32*

**+Eric Lien** Understood. I won't be able to contribute for a while either.


---
**Brandon Cramer** *July 09, 2015 21:40*

I got this working. I ended up wrapping some sand paper around a socket and was able to sand away enough to allow the bearings to fit. I wasn't patient enough the first time of sanding and cracked it again. Good thing I printed 2 of them. :)



How easy should these slide around? Will running the gcode break in help loosen these rods and bearings up? It definitely doesn't move around very smooth at this point. 


---
**Isaac Arciaga** *July 09, 2015 22:03*

I just checked the diameters of the bushing holes on the carriages. They are 15.9mm (Bushing is 16mm). If it was printed on a calibrated printer, the bushing should slip right in with a firm push with a thumb.



For anyone having fitment issues, please do not force them in! Use sand paper to sand the diameter of the hole down until you are able to push the bushing in place with a thumb/finger. Use the 8mm or 10mm rod to wiggle the bushing out if its stuck and repeat the process. You only want a firm fit. Not a super tight fit. Remember, that bushing needs to swivel 5 degrees inside its black sleeve in order for the 'self aligning' feature to do its job. A super tight fit will turn the black sleeve into a clamp preventing the bushing to swivel around.


---
**Eric Lien** *July 09, 2015 22:05*

Aligning takes some time. Lots of possibilities for compound errors. Start by aligning all the upper rods. Then take the carriage to each corner by hand and feel for binding. Adjust as needed on the lower rod so it moves smooth, then re-run the break-in. Then recheck to each corner by hand feeling for binding. Lather/Rinse/Repeat until it is smooth. Once it is right... You will know.



Best of luck.﻿


---
**Eric Lien** *July 09, 2015 22:54*

**+Isaac Arciaga** yeah that is the plan. Slight interference fit combined with acetone softening. I guess I should make a "loose fit variant". 


---
**Bryan Weaver** *July 10, 2015 15:54*

I've never thought of acetone softening.  I finished printing all my bearing holders yesterday and they're all a little too snug for the bearings.  I was ready to go to town with a dremel.  Definitely going to give the acetone a shot.


---
**Igor Kolesnik** *July 20, 2015 11:58*

**+Eric Lien** I am going to get all my parts in a month or so. Will be taking detailed pictures to help with assembly guide.


---
**Eric Lien** *July 20, 2015 12:08*

**+Igor Kolesnik** great news. I sure could use the help.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/gty4XX8SfuW) &mdash; content and formatting may not be reliable*
