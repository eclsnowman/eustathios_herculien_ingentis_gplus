---
layout: post
title: "Any recommendations for where to buy geared nema17s on Aliexpress?"
date: January 01, 2015 07:48
category: "Discussion"
author: Tim Rastall
---
Any recommendations for where to buy geared nema17s on Aliexpress?





**Tim Rastall**

---
---
**Miguel Sánchez** *January 01, 2015 10:34*

Not Aliexpress but I am very happy with the ones from [http://www.omc-stepperonline.com/](http://www.omc-stepperonline.com/)


---
**Eric Lien** *January 01, 2015 12:21*

I like robotdigg. [http://www.robotdigg.com/product/103/Nema17-40mm-Stepper-Gearmotor](http://www.robotdigg.com/product/103/Nema17-40mm-Stepper-Gearmotor) or [http://www.robotdigg.com/product/90/Geared-Nema17-Stepper-Motor](http://www.robotdigg.com/product/90/Geared-Nema17-Stepper-Motor)


---
**Jim Wilson** *January 01, 2015 16:38*

Got my last one from robotdigg and it works great.


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/guGNHsPPbNj) &mdash; content and formatting may not be reliable*
