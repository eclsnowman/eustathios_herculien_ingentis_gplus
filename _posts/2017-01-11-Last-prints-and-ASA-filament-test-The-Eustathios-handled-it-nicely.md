---
layout: post
title: "Last prints and ASA filament test. The Eustathios handled it nicely"
date: January 11, 2017 16:54
category: "Show and Tell"
author: Maxime Favre
---
Last prints and ASA filament test. The Eustathios handled it nicely. It will be my go-to filament for outdoor use. Bye smelly ABS !



<b>Originally shared by Maxime Favre</b>



Projects so far this week :



- a 2 axis "turret" with pan and tilt for testing a solar tracker/heliostat. I plan to do something big and heavy in the garden later. This one is is the baby test bench.

All printed in PLA in speedy mode.

Adapted thingiverse thing.



And secondly a winch powered by a NEMA17 with planetary reductor. The spool was made on the lathe, the shell parts are 3d printed. It was my first run with ASA filament (better ABS in every way). I came out really nicely after a few temp tweaks.

It'll open and close the door of our next family project, a chicken coop (which will probably be more automated than my house) . ;)

Own design.





![images/3f2c04093ebfbc97f20a92d802b6cff5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3f2c04093ebfbc97f20a92d802b6cff5.jpeg)
![images/cfc9c52e5d6eb3a99cacb157f0d5ce3a.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cfc9c52e5d6eb3a99cacb157f0d5ce3a.png)
![images/e0393ffc3a549b295dea69c450a4069b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e0393ffc3a549b295dea69c450a4069b.jpeg)
![images/23a1952cc32c181987ac6ae45f5cc3e2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/23a1952cc32c181987ac6ae45f5cc3e2.jpeg)

**Maxime Favre**

---
---
**jerryflyguy** *January 11, 2017 20:44*

Nice prints! So do you find ASA less smelly vs ABS? Which brand of filament are you using?


---
**Eric Lien** *January 11, 2017 21:08*

Great looking prints, and fun project. Glad to see you putting the printer through its paces.


---
**Maxime Favre** *January 11, 2017 21:54*

**+jerryflyguy** Found it on a local store:

[https://www.3d-printerstore.ch/3D-Filament/3D-Filament-1-75-mm/ASA-Filament-1-75mm:::49_51_77.html](https://www.3d-printerstore.ch/3D-Filament/3D-Filament-1-75-mm/ASA-Filament-1-75mm:::49_51_77.html)

Didn't put my nose against the hot end but it didn't smell anything special in the room.


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/PzHNac3uSRP) &mdash; content and formatting may not be reliable*
