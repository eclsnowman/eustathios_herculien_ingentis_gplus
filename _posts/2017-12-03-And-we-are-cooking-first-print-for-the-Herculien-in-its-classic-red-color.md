---
layout: post
title: "And we are cooking, first print for the Herculien in it's classic red color"
date: December 03, 2017 06:04
category: "Discussion"
author: Gus Montoya
---
And we are cooking, first print for the Herculien in it's classic red color. I decided to go PETG. I am not setup for fumes from ABS.  Printing one of the 3 extrusion drill guides.



![images/4f6a10854a665832acefdf9d63ccbd0e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4f6a10854a665832acefdf9d63ccbd0e.jpeg)
![images/664951b7cbdaebb334b207607a95455a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/664951b7cbdaebb334b207607a95455a.jpeg)

**Gus Montoya**

---
---
**Eric Lien** *December 03, 2017 06:38*

Uhmm you probably want to rotate that so the L section is contacting the bed. Thats how I Intended for it to be printed.


---
**Gus Montoya** *December 03, 2017 07:08*

ohh..yeah I wasn't having much luck that way. Gotta do it again. :(




---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/hGAumoLD2rG) &mdash; content and formatting may not be reliable*
