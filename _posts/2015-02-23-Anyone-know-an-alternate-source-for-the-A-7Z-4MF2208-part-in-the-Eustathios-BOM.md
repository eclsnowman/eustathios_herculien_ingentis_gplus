---
layout: post
title: "Anyone know an alternate source for the A 7Z 4MF2208 part in the Eustathios BOM?"
date: February 23, 2015 17:01
category: "Discussion"
author: Ben Delarre
---
Anyone know an alternate source for the A 7Z 4MF2208 part in the Eustathios BOM?



Its the flanged pillow block for the Z-axis and its currently out of stock at SDP-SI. 





**Ben Delarre**

---
---
**Ben Delarre** *February 23, 2015 18:16*

I'm guessing it wouldn't be too hard to swap out with any other 8mm bore pillowblock with bearing, but I think it would need a redesign of the printed part it mounts onto.


---
**Eric Lien** *February 23, 2015 18:46*

If you have solidworks and are adventurous... I have a new version I am working on that uses 608zz bearings in place of the pillow block bushing. It is not done yet, but can give you some thing to work with: [https://github.com/eclsnowman/Eustathios-V2/tree/master/Solidworks%20Models](https://github.com/eclsnowman/Eustathios-V2/tree/master/Solidworks%20Models)


---
**Ben Delarre** *February 24, 2015 18:05*

I'll give that a go tonight, thanks Eric!


---
**Eric Bessette** *March 21, 2015 18:00*

I found these, [http://www.ebay.com/itm/like/231452345160?lpid=82&chn=ps](http://www.ebay.com/itm/like/231452345160?lpid=82&chn=ps), but I'm not sure if their mounting holes are the same 38mm apart.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/2vKccS1usd6) &mdash; content and formatting may not be reliable*
