---
layout: post
title: "What I did today... The black paint bled a bit, which is disappointing, but otherwise it turned out pretty well"
date: March 07, 2015 00:41
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
What I did today...

The black paint bled a bit, which is disappointing, but otherwise it turned out pretty well. Super Mario is imminent. 



![images/1a50afd7c3271d2d9b1a6f5dcf17ec27.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1a50afd7c3271d2d9b1a6f5dcf17ec27.jpeg)
![images/6b7c8c9669d18e8a0e99808f06b99b47.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6b7c8c9669d18e8a0e99808f06b99b47.jpeg)
![images/03026de2a569ae550c33b1852ff1a21e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/03026de2a569ae550c33b1852ff1a21e.jpeg)

**Jason Smith (Birds Of Paradise FPV)**

---
---
**Gus Montoya** *March 07, 2015 01:15*

Looks nice, what was the print time?


---
**Jason Smith (Birds Of Paradise FPV)** *March 07, 2015 01:39*

It was around 3hrs total for all 3 pieces. 


---
**Gus Montoya** *March 07, 2015 02:16*

Did you upgrade your electronics or are you using your original stuff? I have so many things I want to print ><  :)


---
**Robert Burns** *March 07, 2015 04:34*

That is lovely ;-)


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/ZnWD32vQyDh) &mdash; content and formatting may not be reliable*
