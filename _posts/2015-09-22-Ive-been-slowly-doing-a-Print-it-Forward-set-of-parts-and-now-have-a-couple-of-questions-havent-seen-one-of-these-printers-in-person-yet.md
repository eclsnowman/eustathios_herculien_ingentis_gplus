---
layout: post
title: "I've been slowly doing a Print-it-Forward set of parts and now have a couple of questions (haven't seen one of these printers in person yet)"
date: September 22, 2015 01:20
category: "Discussion"
author: Brian Jacoby
---
I've been slowly doing a Print-it-Forward set of parts and now have a couple of questions (haven't seen one of these printers in person yet).



a) Are the Electronics Mount and Electronics Mount 2 both used or are they alternative choices for the same part?  b) I've printed the threaded flange and spool holder but they don't screw together - am I overextruding or something?





**Brian Jacoby**

---
---
**Eric Lien** *September 22, 2015 22:21*

Sorry about the delay in getting back to you. Version two of the electronics mount just uses less plastic. So either could be used, my preferences is version 2. For the flanges you may be over extruding, or the threads are warping due to heat while printing?


---
*Imported from [Google+](https://plus.google.com/102160258356530092878/posts/gf3WDQ44E1A) &mdash; content and formatting may not be reliable*
