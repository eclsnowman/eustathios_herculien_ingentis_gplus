---
layout: post
title: "I am happy today, got the entire afternoon off to stay home and I am close, so very close to getting my printer going, but I have a few things to adjust"
date: August 15, 2016 00:05
category: "Discussion"
author: Bruce Lunde
---
I am happy today, got the entire afternoon off to stay home and I am close, so very close to getting my printer going, but I have a few things to adjust.  

Q1:  I have a basic question on printer orientation for XYZ setting.  When I am standing at the front of my HercuLien,  Does that line up with the basic pronterface  controls screen? See photos.



Q2: Do you set up zero to be the center of the  build plate, or one of the front corners?



![images/4a004631342ce650273c4ba165592f7f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4a004631342ce650273c4ba165592f7f.jpeg)
![images/f126e21b014f0d347426b44dafecce8a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f126e21b014f0d347426b44dafecce8a.jpeg)

**Bruce Lunde**

---
---
**Eric Lien** *August 15, 2016 00:23*

From the front looking at the printer, zero at left corner closest to you. Z zero is with the bed all the way up, and goes positive as it moves down. X is the travel right to left (traveling right is X+). Y is the carriage moving towards/away from you (away is Y+).


---
**Eric Lien** *August 15, 2016 00:24*

So glad to finally see it coming close to completion. It's been a long time coming. But congratulations on reaching the home stretch.


---
**Bruce Lunde** *August 15, 2016 02:16*

Thanks, **+Eric Lien** I hope to get these last few bugs worked out, and to start printing.


---
**Ramon Daniel** *August 15, 2016 19:10*

I want to build one of these, but I'm a noob at 3D printing still. Where can I get the parts? I don't want a kit, too expensive. Also, I have a Makerbot Dual clone I can use for some part printing.


---
**Eric Lien** *August 15, 2016 19:25*

**+Ramon Daniel** [https://github.com/eclsnowman/HercuLien/blob/master/BOM/HercuLien_BOMV3.pdf](https://github.com/eclsnowman/HercuLien/blob/master/BOM/HercuLien_BOMV3.pdf)



The Github has most everything you should need. All part models (in many formats) as well as pictures, and some build instructions are available. Just take some time to look over the Github and I think you should find it a great resource. Now, be forewarned the instructions are not like Ikea furniture, it is a community effort not a purchasable product. But between the github and this community all questions can usually be answered quickly.



Part sourcing can take a while, and the cost is not cheap. But like any large scale printer the costs and and tuning is non-linear to cost of smaller machines. But if you can print all your own parts that's half the battle. The rest is just time an money :)



[https://github.com/eclsnowman/HercuLien](https://github.com/eclsnowman/HercuLien)


---
**Ramon Daniel** *August 16, 2016 00:55*

**+Eric Lien** Ah, thank you sir!


---
**Mikael Sjöberg** *August 21, 2016 07:08*

Looking awesome Bruce! I am not that far behind you . I will order the hotend now. Otherwise I have installed the end stops and managed to orient and adjust the rods with the guidance of Eric!

I follow your progress! Good luck with the rest ! 


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/btqLY8JNyWJ) &mdash; content and formatting may not be reliable*
