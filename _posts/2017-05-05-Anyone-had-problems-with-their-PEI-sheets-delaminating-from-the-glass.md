---
layout: post
title: "Anyone had problems with their PEI sheets delaminating from the glass?"
date: May 05, 2017 17:52
category: "Discussion"
author: Ben Delarre
---
Anyone had problems with their PEI sheets delaminating from the glass?



I recently attached my PEI, using the 468mp adhesive 12x12" sheets. I cleaned the glass thoroughly with alcohol, and got the glue down smooth. I then applied the PEI on top, and squashed it down with a credit card a bunch till it looked pretty even. There were still a whole load of tiny little bubbles (and i mean really small, head of a pin sort of texture), that I couldn't brush out, but figured this was ok. I then stuck it on the heated bed and set the temp to about 50 degrees and then placed a bunch of weights on top to really squish it down and left it there for about an hour. I turned it off and left it overnight.



First couple of days printing was superb, absolutely awesome. But today I find there's a large bubble developing right in the center of the sheet. Its clearly not flat anymore. I can try and squish it out, but I have a horrible feeling I'm going to have to pull the PEI off again and repeat, and honestly I'm not sure if the PEI sheet will survive being removed. Cleaning the glass of that horrible glue is a real pain in the ass too, so I might have to replace the whole lot.



Any suggestions?





**Ben Delarre**

---
---
**Matthew Kelch** *May 05, 2017 18:08*

As a last resort I'd try carefully putting a very small hole in the PEI at the center of the bubble. A small drill bit should work, just be careful to not go too deep and nick the glass. 


---
**Ben Delarre** *May 05, 2017 18:09*

Hmm good idea. Worth a shot at least


---
**Oliver Seiler** *May 05, 2017 18:59*

I had the same problem with another printer and managed to squasch the bubble sideways until it popped out. It took a while, but is worth it. 

Before you rip it off altogether, try and push a small needle from the side between glass and PEI towards the bubble to create a little channel for the air to escape. Then make sure to press the sheet against the PEI where the bubble used to be for some time to make it stick.


---
**Ryan Carlyle** *May 05, 2017 21:37*

How hot is your build plate? 468 starts to lose stick over 100C or so.  (I run it at 95C for ABS.)


---
**Ben Delarre** *May 05, 2017 23:18*

I've never taken the bed over 70. But I think this may have happened since I had a particularly hard to remove part so perhaps pulled up on the center a little.



 **+Oliver Seiler**​ was right though with kuh repetition I could gradually push the bubble out to the edge where it promptly disappeared. Hopefully it will now stay gone.


---
**Oliver Seiler** *May 05, 2017 23:38*

FYI, I've used mine on the Eustathios at 105 degrees and it still sticks rock solid. I wasn't aware of the 3m limitation, but it doesn't seem to matter too much so far.


---
**jerryflyguy** *May 06, 2017 01:35*

I have a thicker PEI sheet (2-3mm?) on my Eustathios and stuck it down with a 3M sticky sheet product, run 110-115C on the bed with zero issues, I have a fair number of small bubbles also. 


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/BtVYMRid8Dd) &mdash; content and formatting may not be reliable*
