---
layout: post
title: "Learned about this today. Any thoughts on making a public Ingentis/Eustathios/Herculien that anyone can view and contribute to?"
date: March 10, 2015 23:35
category: "Discussion"
author: Erik Scott
---
Learned about this today. Any thoughts on making a public Ingentis/Eustathios/Herculien that anyone can view and contribute to? 





**Erik Scott**

---
---
**Eric Lien** *March 11, 2015 01:12*

I have every file for mine on github, any contributions are gladly accepted. **+Daniel Salinas**​ is already doing great work with the build instructions.


---
**Erik Scott** *March 11, 2015 01:27*

The idea is that this is something everyone would be able to contribute to, not just people who have access to Solidworks. Git hub is nice, but only if you have the software to make real contributions. 


---
**Eric Lien** *March 11, 2015 01:54*

**+Erik Scott** I freely give permission to anyone to import the files I have released into this or any other projects they would like. I would be interested to see what HercuLien would run like in this software. Even on my fairly powerful work station and things can bog down opening the full assembly in solidworks.


---
**Erik Scott** *March 11, 2015 01:57*

I'd be more than happy to do the conversion, but I'm just hoping that people will use it. The idea would be to have a one-stop place for people to contribute ideas and such. 


---
**Jason Smith (Birds Of Paradise FPV)** *March 11, 2015 01:59*

**+Erik Scott** Something like this could be an option, but I would want to be able to use the software first to ensure it is functional/legitimate enough to warrant moving the design there.  Part of the reason I used Sketchup for the original Eustathios design was because it was free and relatively easy to learn.  However, Sketchup has limitations, which is part of the reason **+Eric Lien** 's design is much cleaner than mine (in addition to him being much, much better at modeling than me).  Solidworks costs money because it's good software, and I'd hate to run into design limitations based on the CAD software used.  Another concern is that I'd like to keep the source files fully open/accessible, and I'm not sure if Onshape will support open access to the native files or export to other formats.  Bottom line, I'd like to wait for this to gain some legitimacy before jumping ship.  I requested an account, but am not sure how long it will take to get a beta invite.  


---
**Joe Spanier** *March 11, 2015 02:09*

This is part of the reason I've been doing the work I'm doing in Fusion360. You can import native CAD files (currently have creo and solid works and native fusion files in my design), its free for makers and nonprofits and its pretty easy to learn. 



I also like the collaborative work spaces. Multiple people can work on or have access to a project. There is revision control and access controls and everything lives in the cloud. 


---
**Erik Scott** *March 11, 2015 02:22*

**+Jason Smith** I totally understand the concern. However, from what I can gather, even the free account is fully featured. I've watched their videos and it seems pretty similar to solidworks in terms of functionality. Of course, it'll have to be evaluated, but I'd be more than happy to maintain my own version of the Eustathios there and I hope others would be willing to contribute. Now I just need them to get back to me with an invite...



**+Joe Spanier** Fusion 360 really confuses me. They claim it's free for hobbiests and individuals, but I cannot for the life of me actually find to place to download that version. I've used it a bit with my student account, but I really can't use that anymore, now that I'm no longer a student. It's got a lot if cool functionality, but I don't see much benefit of everything being in the cloud without it being collaborative in some way. 


---
**Joe Spanier** *March 11, 2015 02:25*

They kind of hide the download link but its there. Cloud is awesome. When you are working across multiple PC's or work locations having the cloud is immensely useful. Also not having to work about storing your work locally is nice. It caches 15 days of your work to your PC though in case you need to work without internet. 


---
**Erik Scott** *March 11, 2015 02:31*

It must be really well hidden then, because all I see is "free trial" and "buy now @ $25/mo." 



I'll have to give it another look though. Right now, I'm using this program called "Designspark Mechanical" which is good for some of my other projects, but is really just a dumbed down spaceclaim. However, it's much better than sketchup in my opinion. 


---
**Eric Lien** *March 11, 2015 02:56*

**+Joe Spanier** I have been looking for that hidden link also. If you help me I will add fusion360 to my list of alternative formats I export my projects in.


---
**Joe Spanier** *March 11, 2015 03:00*

[http://fusion360.autodesk.com/pricing#](http://fusion360.autodesk.com/pricing#)



click the *Do I qualify for free use" link and then click either the student or educator link or the startup link. 


---
**James Rivera** *March 11, 2015 06:25*

Only good for 1 year--just long enough to hold your data hostage: 

"To see if you qualify as a “Startup” see the Autodesk Web Services Entitlements Page. If you selected the Startup entitlement, you will have use of Autodesk® Fusion 360™ Ultimate for one (1) year. You will be notified at the end of your entitlement term via email or via a notice to your account. <i>At the end of one (1) year, you will have the option to re-select the startup entitlement or transition to a commercial entitlement.</i>"



Basically, after 1 year they could easily change the terms. No stank you.



I signed up for OnShape, but they only sent a "Nice to meet you" message:



"Hi ,



Thanks for requesting an invite to Onshape. We will send you one as soon as possible.



-The Onshape Team"


---
**James Rivera** *March 11, 2015 06:28*

Is FreeCAD a viable alternative?

[http://www.freecadweb.org/](http://www.freecadweb.org/)


---
**Eric Lien** *March 11, 2015 06:51*

**+Joe Spanier** did you select startup or education?


---
**Joe Spanier** *March 11, 2015 12:31*

I've selected both but I usually click education because of the MakerSpace. When I talked to the fusion guys at IMTS they told me anyone like us qualifies as edu. 


---
**Erik Scott** *March 11, 2015 13:32*

**+James Rivera**​ I've used freeCAD a bit, but I find it really hard to use. It also lacks constrained assemblies, which is useful if you're making things with moving parts, like printers. That said, it can do both sketch-based feature modeling like solid works, as well as constructive solid geometry using booleans and such. 


---
**Joe Spanier** *March 11, 2015 13:54*

Well with autodesks recent commitment to makers and education I'm all for supporting it. When I talked to them at IMTS they told me as long as people are using the software they will keep up the free portion. Its only smart for them really. If you design your product in a really great free cad why would you change to a different software when your big?


---
**Joe Spanier** *March 11, 2015 13:56*

**+Erik Scott**​ that's my problem with freecad as well. I dont want to have to know the exact world coordinates of my part to do an assembly. To me that's not assembly. That's body placement


---
**James Rivera** *March 11, 2015 17:11*

FreeCAD has constraints.


---
**Joe Spanier** *March 11, 2015 17:50*

In assemblies? So I can click to surfaces and it will stick them together now? Things snap together for positioning? If it does Ill take a look again. 


---
**hon po** *March 12, 2015 04:51*

Someone wrote an assembly2 module for FreeCAD, but I find it not stable enough after placing more than a few parts. FreeCAD has constraints but need to create simple copy to do the placement. So editing afterwards could be tricky. Also, it's not a joint, cannot drag the carriage around like **+Joe Spanier** demoed.



Using draft move in freeCAD:


{% include youtubePlayer.html id=lfinO3EGXeo %}
[https://www.youtube.com/watch?v=lfinO3EGXeo](https://www.youtube.com/watch?v=lfinO3EGXeo)

Learned a lot from **+61quick** youtube video.


---
**Jo Miller** *April 05, 2015 09:13*

**+Eric Lien** where can I find the the build documentation of Daniel Salinas ? , is it linked to the official Gifthub site of the herculien ?


---
**Eric Lien** *April 05, 2015 09:38*

**+Jo Miller**​, **+Daniel Salinas**​ is finishing it up. Once completed he said he will push it back to my Github. Until then you can find it here: [https://github.com/imsplitbit/HercuLien/tree/master/Documentation](https://github.com/imsplitbit/HercuLien/tree/master/Documentation)


---
**Jo Miller** *April 06, 2015 07:35*

thanks Eric


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/VNGAHHQUdVu) &mdash; content and formatting may not be reliable*
