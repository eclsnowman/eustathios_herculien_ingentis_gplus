---
layout: post
title: "Printed a whole set of new XY ends for another ."
date: May 25, 2014 15:42
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
Printed a whole set of new XY ends for another  #Eustathios .  Need to get a higher quality webcam :(

![images/c216d2cb74d7ffcfe4b3513b840b4150.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c216d2cb74d7ffcfe4b3513b840b4150.gif)



**Jason Smith (Birds Of Paradise FPV)**

---
---
**ThantiK** *May 25, 2014 16:51*

I don't even use those extra little blocks myself.  I use the 3-ziptie method.  Using those 2 little blocks puts strain on the plastic, which is likely to warp and/or break in the sun or under tension.  If you tension the belt against itself, and then merely use the attachment to drive the block, the block will last longer.


---
**Eric Lien** *May 25, 2014 17:49*

If its a Logitech c270 you can pop off the cover and adjust the focal ring for close up focus. There is a good writeup on this if you Google for it.


---
**Jason Smith (Birds Of Paradise FPV)** *May 26, 2014 18:43*

**+Eric Lien** Thanks for the tip.  I can't figure out why logitech glues the focus ring in place which is adjusted for such a long focal length by default.  Much clearer videos are on their way!


---
**Jarred Baines** *June 05, 2014 00:58*

**+Anthony Morris**  Could you expand on this "3 ziptie method"?


---
**ThantiK** *June 05, 2014 01:27*

Toothed belt: Loop one end back on itself so that the teeth interlock.  Put a ziptie around it to lock the teeth in place.  Do the same thing on the other end of the belt.  Take the two loops that you've made on each end of the belt, and put a ziptie around both loops and tighten.  Benefit is that you're tightening the belt around itself, you have a convenient place to just yank on to tighten it more, and no screws or weird contraptions to have to add.


---
**ThantiK** *June 05, 2014 01:29*

[http://3.bp.blogspot.com/-YR4PGydCYHo/UIT8CCnTnvI/AAAAAAAAAIs/7a81b_d3AUs/s1600/belt_tensioner.jpg](http://3.bp.blogspot.com/-YR4PGydCYHo/UIT8CCnTnvI/AAAAAAAAAIs/7a81b_d3AUs/s1600/belt_tensioner.jpg)


---
**Jarred Baines** *June 05, 2014 01:33*

Ooh - I like that :-)



Cheers mate!


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/CHHrEretFF4) &mdash; content and formatting may not be reliable*
