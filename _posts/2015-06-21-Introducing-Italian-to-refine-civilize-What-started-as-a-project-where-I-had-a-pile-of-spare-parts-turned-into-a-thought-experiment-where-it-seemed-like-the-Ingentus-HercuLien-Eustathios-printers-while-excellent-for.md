---
layout: post
title: "Introducing (Italian: to refine, civilize) What started as a project where I had a pile of spare parts turned into a thought experiment where it seemed like the Ingentus, HercuLien, Eustathios printers while excellent for"
date: June 21, 2015 14:13
category: "Deviations from Norm"
author: Mike Miller
---
Introducing  #ingentilire   (Italian: to refine, civilize) 



What started as a project where I had a pile of spare parts turned into a thought experiment where it seemed like the Ingentus, HercuLien, Eustathios printers while excellent for large envelopes, were not so much for smaller ones. When the printer shrinks to roughly 1 foot cubed, the awesome print models got less awesome. 



This printer's goals:

1. Batten everything down...this printer plans on being portable!

2. minimize printed parts, make them symmetric to avoid model complexity. 

3. Maximize interior volume (The extruder carriage is a non-trivial issue. **+Eric Lien**'s carriage incorporates everything necessary to produce excellent prints...and it's now the biggest cookie-monster of print envelope. Once the printer is operational, it'll be the next part of the project. Every mm I can shave off an actively cooled carriage with parts-fan, will be a mm I regain in print volume. 

4. Use up my leftover parts! Are 10mm bearing shafts overkill on a printer this size? Sure. But it's what I had lying about. 

5. Solve my issues in the Z Axis.  #IGentUS  has a belt driven, wobbly, hard to calibrate build plate. It's fine now, but it took a LOT of fiddling to get ANY semblance of calibration. It shall not be moving, lest it lose it's tram. 



It's already benefitting from the hivemind here (**+Dat Chu** forwarded an idea for belt tensioning that is working well), and I'll publish the parts that end up being unique to the printer...tho with my CAD skills being what they are, you might as well just recreate them from scratch.



<b>ingentilire</b> will most likely have a cantilevered, unheated bed as it's a project my son and I are working on. 



![images/dc44d6f6fb4043604847c808472332b0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dc44d6f6fb4043604847c808472332b0.jpeg)
![images/b9c34a17fbc60bbde026a9ed660d5585.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b9c34a17fbc60bbde026a9ed660d5585.jpeg)
![images/8b60cca35fe499413544f466f5936fbc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8b60cca35fe499413544f466f5936fbc.jpeg)

**Mike Miller**

---
---
**Eric Lien** *June 21, 2015 14:36*

Love the variations. I am excited to see the first prints.


---
**Dat Chu** *June 21, 2015 15:57*

This looks very good. I am glad you are sharing your design with the community. You mention wanting to minimize printed parts. Comparing to ingentis this looks like a similar number of printed parts. Perhaps I am missing something. 


---
**Mike Miller** *June 21, 2015 16:24*

They're multiple prints of the same part...XY retainer times 4, bearing retainers times 8, etc.


---
**Dat Chu** *June 21, 2015 17:34*

I see.  That does reduce potential errors with mismatching parts. 


---
**James Rivera** *June 21, 2015 19:17*

Looks great! I'm looking forward to seeing what your Z-axis solution is, too.


---
**Mike Miller** *June 21, 2015 20:13*

Oh man, no pressure there! :D


---
**Jeff DeMaagd** *June 21, 2015 20:25*

I think 10mm rod is fine for that size. The main down side is maybe more inertia and it doesn't allow you to use as small pulleys as would be nice to use.


---
**Mike Miller** *June 22, 2015 00:08*

Two of the rods are conventional, the other two are Igus and aluminum. I suspect they won't react well to the Bronze impregnated bushings...But I'm doing it anyway. ;)


---
**SalahEddine Redjeb** *June 22, 2015 13:14*

I love it, i just love, there is no other word, i might have a suggestion for you, i'll upload the part whenever i draw it


---
**Frank “Helmi” Helmschrott** *June 27, 2015 10:28*

Oh yeah, just when i started thinking about my long-time-dream of a small and portable desktop printer again i came across your post. I'm very keen to see how it develops and how it works out for you. I also have some aluminum rods and igus bushings lying around and was thinking about using them in a smaller one. Let's see some prints and videos as soon as it works.


---
**Mike Miller** *June 27, 2015 12:40*

If you temper your expectations with IGUS, I think You'll find they have some pretty good upsides and a few downsides...they're very quiet, they're zero maintenance, and they can't print circles of a certain size.  😳 and nothing's cheaper than the parts you already have. 






---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/AahQMQpi1Mh) &mdash; content and formatting may not be reliable*
