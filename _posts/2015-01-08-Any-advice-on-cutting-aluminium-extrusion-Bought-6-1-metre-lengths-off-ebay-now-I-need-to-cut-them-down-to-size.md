---
layout: post
title: "Any advice on cutting aluminium extrusion? Bought 6 1 metre lengths off ebay now I need to cut them down to size"
date: January 08, 2015 14:14
category: "Discussion"
author: Richard Mitchell
---
Any advice on cutting aluminium extrusion? Bought 6 1 metre lengths off ebay now I need to cut them down to size. Was planning on using a chop saw and a block to set the length. Thoughts?





**Richard Mitchell**

---
---
**Mike Miller** *January 08, 2015 14:32*

You'll want to make sure the cut is perpendicular as any out-of-true cuts will translate down the line to headaches in building the printer. It's far from an impossible task, but you do need some care. 



Or you could have a 1m^3 printer. :D



Or you can use **+Shauki B**'s Quadrap design, it's VERY rigid, and ensures all corners are square. [http://4.bp.blogspot.com/-Y8GRa4mq0io/Uz3657PmINI/AAAAAAAADsQ/dtwOJPqqH4U/s800/quadstrap+3d+printer+1.png](http://4.bp.blogspot.com/-Y8GRa4mq0io/Uz3657PmINI/AAAAAAAADsQ/dtwOJPqqH4U/s800/quadstrap+3d+printer+1.png)


---
**Carlton Dodd** *January 08, 2015 14:57*

Chop saw sounds perfect. I used a hacksaw and a mitre box.


---
**Richard Mitchell** *January 08, 2015 15:44*

The only issue I'm feeling with the chop saw is a little play in the mechanism which doesn't inspire me. Perhaps a simple mitre box isn't a  bad idea. A table saw and slide would be nice but I don't have one (or room for one).



I've thought of joining on flats like the quadrap design but I'm trying to make a small 8mm based version and I plan to put panels on it to make a heated chamber when done and that makes it more difficult.


---
**Mike Miller** *January 08, 2015 15:46*

I used a lathe...since its, like, 8 cuts, you might find someone local that could do you a favor if your chopsaw  isn't up to task. 


---
**Mike Thornbury** *January 08, 2015 15:48*

**+Richard Mitchell** just take it slow and steady -and anchor the 'static' side of your cut so your free hand can control the cut piece. I have had a piece of extrusion decide it was destined to be part of a Tomahawk missile before.



It pays to dial in your saw. I used an inclinometer I got from Rockler -invaluable.﻿


---
**R K** *January 08, 2015 18:57*

Miter saw and box + clamps to keep the entire thing secured so it can't move at all would be the best bet. Lacking a miter box, you need to clamp something flat at the cut line as perpendicular as possible and try to cut along that.


---
**Richard Mitchell** *January 08, 2015 19:41*

**+Mike Miller**​ hadn't thought of a lathe. Another good idea.


---
**Joe Spanier** *January 09, 2015 05:25*

I used my chop saw on my recent laser build. About 40 cuts in about an hour. Went awesome. Just get a good nonferrous blade and your off


---
**Mike Miller** *January 09, 2015 11:19*

**+Richard Mitchell** using a lathe depends greatly on having a spindle large enough to accept the extrusion...not a lot of people have access to a lathe, much less one with a big enough envelope.  (South Bend 10L here)


---
**Mutley3D** *January 11, 2015 02:50*

I use exactly the same chop saw, but i cut 1-2mm overlength and then face off on a mill to get the ends perfectly squared off. Using a lathe youd need a 4 jaw chuck aswell as a large enough spindle to swallow the extrusion although a lathe would do an qually goood job as facing off on a mill (if not better)....Oh and thanks for the add :)


---
**Mike Miller** *January 11, 2015 06:26*

Funny thing that...a three jaw self centering chuck will grab one side in the groove, and the other two flats and make a passible face cut.


---
*Imported from [Google+](https://plus.google.com/111547506541230089782/posts/NugSDdKUnKf) &mdash; content and formatting may not be reliable*
