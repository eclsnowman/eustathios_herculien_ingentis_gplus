---
layout: post
title: "FIINALLY cleaned up the wiring and installed the lower plexiglass plates"
date: February 18, 2015 04:36
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
FIINALLY cleaned up the #Eustathios wiring and installed the lower plexiglass plates. Mounted **+Eric Lien** 's new carriage modified for linear bearings and my new Volcano. The acryllic base for my bed warps under heat, so I've got some aluminum plate on order. I've also got more acryllic on the way for an enclosure and a sheet of PEI for a build surface. This is going to be a whole new machine by the time I'm done with it :)



![images/58ca40b48a91c08730257ee9108096c4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/58ca40b48a91c08730257ee9108096c4.jpeg)
![images/2d4e480049eeae56ef0a5162e7bfc11c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2d4e480049eeae56ef0a5162e7bfc11c.jpeg)
![images/ba42116958f5c2685f48cdd2a0fc6142.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ba42116958f5c2685f48cdd2a0fc6142.jpeg)
![images/ddf145ce3cfeef583ac43fcf0c3d1eb4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ddf145ce3cfeef583ac43fcf0c3d1eb4.jpeg)
![images/e286c61c2a9225a35b1cc3d6a13165a4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e286c61c2a9225a35b1cc3d6a13165a4.jpeg)
![images/5bf7330b2f039bc8ff001723b0f04dc8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5bf7330b2f039bc8ff001723b0f04dc8.jpeg)

**Jason Smith (Birds Of Paradise FPV)**

---
---
**Eric Lien** *February 18, 2015 05:20*

Nice work. Sorry I haven't redone the shroud yet for the extra length of the volcano. I tried but I had tons of assembly mates tied to the E3D v6 surfaces. So trying to change it made my mates all blow up.



I will give it a go again tonight.



But wow, that sucker is bright. Like looking at the sun ;)


---
**Daniel Fielding** *February 18, 2015 08:32*

Video or it didn't happen; )


---
**Seth Messer** *February 18, 2015 14:55*

**+Jason Smith** pardon my ignorance, but,  what are the 3 strips lined up, parallel to the controller board?


---
**Jason Smith (Birds Of Paradise FPV)** *February 18, 2015 18:39*

**+Seth Messer**, those are terminal strips to tidy up the wiring while keeping the controller modular from the machine itself. [http://www.radioshack.com/12-position-european-style-mini-terminal-strip/2740680.html#.VOTb56CCPCR](http://www.radioshack.com/12-position-european-style-mini-terminal-strip/2740680.html#.VOTb56CCPCR)


---
**Seth Messer** *February 19, 2015 02:40*

**+Jason Smith** thanks! i picked a few up from my local going-out-of-business radio shack. :D


---
**Jason Smith (Birds Of Paradise FPV)** *February 19, 2015 02:41*

**+Seth Messer**, I've been meaning to do that myself, but I'm sure the store has been picked over by now. 


---
**Seth Messer** *February 19, 2015 02:42*

1st store i went to was emptied out, 2nd one was still near fully stocked, but "only" 20% off everything, but was told to come back in a couple weeks when they will start increasing the discounts.


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/W59uguu3hCK) &mdash; content and formatting may not be reliable*
