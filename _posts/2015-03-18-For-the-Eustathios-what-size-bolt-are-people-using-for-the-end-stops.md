---
layout: post
title: "For the Eustathios, what size bolt are people using for the end stops?"
date: March 18, 2015 05:50
category: "Discussion"
author: Eric Bessette
---
For the Eustathios, what size bolt are people using for the end stops?  It seems like M3 is too large for the E3D end stops I have and the 3D part.



Also, what are people using to bolt the belt tensioner C with tensioners A and B?  Misuma USA doesn't seem to offer an M3 30mm bolt (BCB3-30). 



 Thanks for your help.





**Eric Bessette**

---
---
**Oliver Seiler** *March 18, 2015 05:57*

I've carefully put a 3mm drill through the E3D end stop holes and used M3 screws. Works fine.


---
**Jeff DeMaagd** *March 18, 2015 06:05*

Mini basic switches have holes for 2.5mm screws.


---
**Joe Spanier** *March 18, 2015 06:08*

I use zip ties. They work great. 


---
**Eric Lien** *March 18, 2015 06:21*

I drill them out to 3mm also. Who the heck keeps 2.5mm bolts around. I have buckets of 3mm.


---
**Erik Scott** *March 18, 2015 13:11*

If you're in the US you can use #2 screws. Otherwise, M2 will work, if you have them. 


---
**Eric Bessette** *March 19, 2015 02:22*

I decided to get some M2 16mm bolts from McMaster Carr.  Thanks for the advice everyone.


---
*Imported from [Google+](https://plus.google.com/106288190144199995561/posts/UsNeohUP4ES) &mdash; content and formatting may not be reliable*
