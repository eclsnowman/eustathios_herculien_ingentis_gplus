---
layout: post
title: "Euth frame done"
date: May 15, 2015 15:16
category: "Show and Tell"
author: Derek Schuetz
---
Euth frame done

![images/4ebbd04487a40e1400e1a2e50c84c6d3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4ebbd04487a40e1400e1a2e50c84c6d3.jpeg)



**Derek Schuetz**

---
---
**Vic Catalasan** *May 15, 2015 16:10*

Derek, what made you want to build the Euth after building the Herc?


---
**Daniel Salinas** *May 15, 2015 16:28*

Black frame with red or orange parts.... man! thats almost enough to make me want to build one.


---
**Eric Lien** *May 15, 2015 16:54*

**+Daniel Salinas** black and yellow would be great... Would remind me of my old cbr600 that I sold when I had kids. Ahh, the things you give up when having kids and get old ;)


---
**Derek Schuetz** *May 15, 2015 17:14*

I like building printers and I didn't want to retap and drill so many holes to gain minor build volume . I'll get some acrylic cut to make an enclosure for this eventually


---
**Gus Montoya** *May 15, 2015 17:29*

If you lived close enough we could have a building party :)  Nice job. 


---
**Isaac Arciaga** *May 15, 2015 19:51*

**+Daniel Salinas** shhhh! I'm doing black on orange! **+Derek Schuetz** Looking good! Did you paint the 90 brackets? I was deciding whether or not I should. I think I should... :)


---
**Derek Schuetz** *May 15, 2015 20:18*

Painted them since the black anodized were 3 times the cost


---
**Daniel Salinas** *May 15, 2015 20:19*

My Camaro is black and orange


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/AAPayz691Cs) &mdash; content and formatting may not be reliable*
