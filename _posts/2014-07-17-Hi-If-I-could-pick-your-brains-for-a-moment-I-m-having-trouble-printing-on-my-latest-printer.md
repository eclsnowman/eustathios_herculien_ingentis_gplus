---
layout: post
title: "Hi, If I could pick your brains for a moment, I 'm having trouble printing on my latest printer.."
date: July 17, 2014 00:37
category: "Show and Tell"
author: Jean-Francois Couture
---
Hi, If I could pick your brains for a moment, I 'm having trouble printing on my latest printer.. (ingentis / Eustathios / rigidbot). On the photo, i've done a torture test. On the left, my old trusty Prusa I2 /w direct drive filament. On the right, my clone using bowden and airtripper's extruder. both have the same setting to print out. 80mm/s with 205C temp. they both have a J-head (the prusa has a brass ending while the clone has a aluminium one). both tests were made witht the same spool.. Has you can see, the clonde is having trouble printing. 



Its got a Nema17 step motor with a MK8 gear. its set to 149 in the firmware with a 1/16 step. I dont feel # hear the stepper skipping so I dont think thats the problem.. 



Anyone have a suggestion ? I'm all out of ideas on this one. 



Thank !

![images/f59344fa9613a1abacc63011cfcd85a0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f59344fa9613a1abacc63011cfcd85a0.jpeg)



**Jean-Francois Couture**

---
---
**George Salgueiro** *July 17, 2014 00:39*

Change the j head and print with the clone.


---
**Jean-Francois Couture** *July 17, 2014 00:44*

Yeah, the problem is that I can't install the push to connect on the J-head thats on the prusa. I cant screw it on. I would have to tap the head.. i'm not sure thats a good idea


---
**Jason Smith (Birds Of Paradise FPV)** *July 17, 2014 01:04*

Could be a temperature issue. The one on the right looks too cold to me. Maybe try cranking it up 20 degrees or so. Maybe your thermistors aren't calibrated equally. I know on the e3d v6, you have to make sure the thermistor is secured all the way inside the heater block or it will read low, especially when the fan kicks on. 


---
**Carlton Dodd** *July 17, 2014 01:26*

Back to basics question:

It appears you're under-extruding; have you calibrated your extruder steps/mm?


---
**Jean-Francois Couture** *July 17, 2014 02:02*

**+Jason Smith** I did one @ 215C and it looks a bit less messy but still has some problems on the layers. I have #1 as the thermistor in Marlin.. maybe #5 would be more appropriate.


---
**Jean-Francois Couture** *July 17, 2014 02:03*

**+Carlton Dodd** Yes I did.. used a piece of filament, marked it at 50mm,100mm,150mm and 200mm and calibrated the firmware until I could move between those marks and been on the dot every time.


---
**Carlton Dodd** *July 17, 2014 02:07*

**+Jean-Francois Couture** Well, darn.  That's the extent of my thoughts.


---
**James Rivera** *July 17, 2014 03:12*

It definitely looks like either it is  under-extruding, or the temperature is too low (assuming the filament is not simply getting chewed up and skipping on the hobbed bolt/gear).



If under-extruding, it could be the stepper skipping or a calibration problem. Since you've nailed the extrusion length, skipping steppers might be the problem. Are they getting too hot?  Or perhaps the driver chips are getting hot? Do you have heat sinks of the driver chips?



If the extruder stepper motor or the driver chip are both not too hot, then Occam's razor likely applies and it is probably the temperature.



If extrusion temp is low (what it looks like), then maybe try going up 5 deg C increments and re-print; repeat a few times, but 20 degrees is probably too wide a range since it is already melting.


---
**James Rivera** *July 17, 2014 03:13*

Or, could it be the filament has absorbed water?



Here's a reference: [http://reprap.org/wiki/Print_Troubleshooting_Pictorial_Guide](http://reprap.org/wiki/Print_Troubleshooting_Pictorial_Guide)


---
**Jean-Francois Couture** *July 17, 2014 03:24*

**+James Rivera** I tried 200C, 205C, 210C, 215C, and now 220C, all the same..



Driver overheat could be a cause, theres a heatsink on the DRV8825 (should not be but there is..) I can keep my finger on it but it is hot to the touch. I just dialed down the pot a bit while is was printing.. it was getting less and less warm. I'll have to restart tomorrow 'cause its bed time over here ! :)



Dont think its a humidity problem. Like I said, both print were done one after the other.. just changement the spool from one printer to the other.



Maybe I do have under feeding.. but, that would trough all my calibrations out the door.. <b>sigh</b>


---
**Jason Barnett** *July 17, 2014 04:23*

Check the actual nozzle size. I am sure you are under-extruding, if your calibrations are correct, than it must be a setting. I am betting the actual nozzle size is larger than what it is marked. This would cause an otherwise properly calibrated printer to under-extrude.


---
**D Rob** *July 17, 2014 05:41*

Also check the filament diameter in several places and around in each place. This looks like maybe the filament size is smaller than programmed in the software.﻿


---
**karabas3** *July 17, 2014 06:28*

Better to use geared extruder. Bowden adds load to stepper. So your diver/motor is hot.


---
**Jean-Francois Couture** *July 17, 2014 12:00*

**+Jason Barnett** I was not aware of that ! thanks for sharing, i'll check and see... maybe also ramp up the step/mm in the firmware.


---
**Jean-Francois Couture** *July 17, 2014 12:01*

**+D Rob** Did that, I was pretty mush around ~ 1.75mm 


---
**Jean-Francois Couture** *July 17, 2014 12:03*

**+karabas3** So true. I have that on my prusa i2 and its working great.. I did try Richrap's version for the bowden tube.. just not on the latest machine. maybe i'll look into it. :)


---
**D Rob** *July 17, 2014 12:27*

**+Jean-Francois Couture**  were you using calipers? calipers are not an optional piece of equipment in this process. accurate measurements need to be taken on every roll of filament. try subtracting .03mm from filament diameter. also try a different slicer. cura if you are using slic3r or kiss, and one of the others if using cura 


---
**Carlton Dodd** *July 17, 2014 12:58*

**+Jean-Francois Couture** In extrusion, "pretty mush (sic) around ~1.75mm" is not close enough.

Remember that 1.7mm diameter (2.270 mm^2 cross-section) is 5.7% less plastic than 1.75mm diameter (2.405 mm^2 cross-section).  This is why inconsistent diameter filament sucks to work with.


---
**Carlton Dodd** *July 17, 2014 13:16*

**+Jean-Francois Couture** I did a quick comparison sheet: [https://docs.google.com/spreadsheets/d/1tuS-unlm1qB_Qm2DWabqFXfo6vXe12kYtgU8JagA4tw/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1tuS-unlm1qB_Qm2DWabqFXfo6vXe12kYtgU8JagA4tw/edit?usp=sharing)


---
**James Rivera** *July 17, 2014 13:19*

I'll probably get negative feedback for this,  but this is one reason why I have wanted to stick with 3mm filament. A .01mm variance is a greater error percentage with smaller (1.75mm) filament.


---
**Jean-Francois Couture** *July 17, 2014 14:50*

**+D Rob** Yes, I used one to check the diameter... I was @ 1.74mm . I think is within tolerance.


---
**Jean-Francois Couture** *July 17, 2014 14:52*

**+James Rivera** its got some ups and downs...stiffer bowden tube but harder for the extruder to push through.


---
**Jean-Francois Couture** *July 17, 2014 14:53*

**+Carlton Dodd** Right, should of said. 1.74mm to 1.76mm in average. :)


---
**Jean-Francois Couture** *July 17, 2014 17:40*

**+Ashley Webster**  hum.. I use small leads from resistors dans i keep when doing electronics.. insert it when the head is hot.. but I have to admit that, I havent cleaned the other way for some time.. could be a clog.. i'll look into it !



Thanks


---
**James Rivera** *July 17, 2014 17:48*

**+Jean-Francois Couture** spare resistors is a good idea! If you have any guitar wire lying around. it is also good as a "pipe cleaner" of sorts for clogged extruder nozzles.


---
**Jean-Francois Couture** *July 17, 2014 18:45*

**+Ashley Webster** Thanks, I have some nylon at home, (don't remember if its 618 or 645) to clean up the barrle after  trying the cold pull for PLA tempratures.


---
**Jean-Francois Couture** *July 17, 2014 18:46*

**+James Rivera** I do have some but there all still on the guitar ! LOL !


---
**James Rivera** *July 17, 2014 21:34*

**+Ashley Webster** good to know, thanks! I wish I had known this before I threw out (aside: or misplaced? sometimes I feel like the absent-minded professor) my prior Ubis hot end that got ultra charred when I tried to PID auto-tune it and left it running too long (slaps forehead). I have some Taulman 618 I have never used. If I just misplaced it I will definitely give the cold pull method a try.


---
**Jean-Francois Couture** *July 20, 2014 04:26*

Well, to close this thread, I actually made a adapter to use the Prusa i2's J-head onto the latest printer (that J-head is not bowden / push to connect compatible). a little bit of tuning and everything is working like a charm.. I actually printed another torture test @ 100mm/s and it was better that what my prusa could do. This goes down to, a bad J-head.. I was assured buy the seller that the "defective" j-head was a genuine one.. well, made some research on the net and its a cheap china clone.. that would explain all the troubles I had with it. the prusa's J-head was a genuine and still working great. So, I want to thank everyone for there input and will be buying a new head from [hotends.com](http://hotends.com) (when ever they'll have some in stock) from now on :-)


---
*Imported from [Google+](https://plus.google.com/105576148076542448710/posts/KzJbNxfcmN4) &mdash; content and formatting may not be reliable*
