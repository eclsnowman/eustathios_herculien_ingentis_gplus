---
layout: post
title: "I have broken it breaking it in :( What seemed to start so well ended rather embarrassingly disastrous"
date: February 25, 2018 02:48
category: "Build Logs"
author: Dennis P
---
I have broken it breaking it in :(  What seemed to start so well ended rather embarrassingly disastrous. 



How much of a shakedown would the break-in be? What are the typical problems that show up and resolutions?



Could 200mm/s be too fast for RAMPS/TMC2100 drivers?



At first there was a bit of a hum that I assumed was bearing 'stiction' but seemed illogical as I had just oiled the rods . Then it got worse and I started to think that the belts seemed looser, and then it was obvious that the belts were loose and I was skipping teeth. The last part of the video the printer is trying to put the effector out of bounds. Its almost embarrassing to show,  but I believe that only through talking openly about our failures can we succeed.  



3 segment video- I had to use my phone to capture the sounds. Forgive any bobbles or sneezes...



[https://drive.google.com/file/d/1xZa1JrRRPBLuLB6kjFws0GZzpEl1vIT6/view?usp=sharing](https://drive.google.com/file/d/1xZa1JrRRPBLuLB6kjFws0GZzpEl1vIT6/view?usp=sharing) 



I will admit to suffering from a little instant gratification threw on piece of glass and tried printing a couple of test cubes but it did not go well. 



I don't even know where to start looking. I do have one axis with 2 rubber/kevlar reinforced belts and one with 2 polyurethane/steel belts. I don't think this could be the cause or matters. 



I am thinking its either something mechanical or the RAMPS/TMC2100 is not up to snuff. 



I am thinking that I will start with rechecking the frame and try to double check the main rods and see if there is something amiss there.   



In case its relevant, here are my Marlin files. I bumped the default speed and acceleration to somewhere between the defaults and the ones I read in the Smoothieware files. 



Marlin Config - [https://drive.google.com/file/d/1XBLctN5nLLJKyjET-AaUnTqJm0CJu9Ip/view?usp=sharing](https://drive.google.com/file/d/1XBLctN5nLLJKyjET-AaUnTqJm0CJu9Ip/view?usp=sharing)



Marlin Config.adv.h - [https://drive.google.com/file/d/1R4xQ4GO2VO3PRWQ-Mu7Klp0ApNklnI-J/view?usp=sharing](https://drive.google.com/file/d/1R4xQ4GO2VO3PRWQ-Mu7Klp0ApNklnI-J/view?usp=sharing)









**Dennis P**

---
---
**Eric Lien** *February 25, 2018 03:00*

Check to make sure you aren't overheating the tmc2100 drivers causing missed steps due to thermal shutdown. The TMC are great drivers for many things, but I will admit they are the most sensitive drivers I have used in terms of getting current set correctly.



Until you get the current tuned in for your motors and torque requirements... 200mm/s is a bit too ambitious imho. Especially since the gantry and everything is not broken in yet.


---
**Eric Lien** *February 25, 2018 03:04*

As far as acceleration, i used to think i needed acceleration crazy high. But over the years, once you get above 1500 there is a very diminished return. 1500-3000 is a good range for a Eustathios. Any more than 3000 and you will never see the benefits.


---
**Dennis P** *February 25, 2018 05:47*

Thanks for the thoughts. 



200mm/s seemed fast to me too. I did not question it since 200mm/s was the speed noted in the breakin gcode file that was on github. 



Is there a different one, or more updated one we should be using?  



I have been studying the gcode file in an attempt to rewrite it into a spreadhsheet so that we can change parameters for bigger or smaller machines and change the travel speeds  


---
**Ryan Fiske** *February 25, 2018 06:21*

Thanks for sharing this, I am also having similar issues albeit not quite as severe. I get the chatter/buzzing like you do running the break in code. I let it do its thing once through and I then went around and adjusted the lower rod bearing holders one at a time like in Eric's video and that seemed to lessen it a bit. I'm going running again will do the same. I'll try to post a short clip as well.


---
**j.r. Ewing** *February 25, 2018 09:14*

**+Dennis P** there are spread sheets on the internets to do exactly what you seek. I speak g code for a living.  The F command is what you seek ie F200. Must have decimal or turns into f.2 it stays modal until called again. 



Do any controllers support g2 g3? 


---
**Eric Lien** *February 25, 2018 14:02*

**+Dennis P** i meant printing at 200mm/s is ambitious. The break-in code can be sped up or slowed down via your lcd by rotating the knob on the main status screen (feed rate override). I have run it at 2x before (though I don't recommend it).



The main thing we need to figure out for you is the source of the skipping steps. With all the long fast moves of the break-in code, any skipped step error will accumulate fast and cause and "out of bounds" condition.



My first guess is a thermal shutdown issue. I have fought with TMC drivers before, so the current there will be where I would start.


---
**Eric Lien** *February 25, 2018 14:13*

**+Ryan Fiske** for the chatter (at the end of the travel move I assume). You can try tuning acceleration and jerk (or acceleration and junction deviation on smoothieware). I have noticed with my machines, break-in takes time. And I haven't found a good way to rush it along. Its like soaking a pan with burned on food. Give it a little soak in time :)



One trick I recommend to people is put a stack of 7 or so Post-it notes under one of the printer frames feet. Run the break-in code and listen, does it sound better or worse. Then stop the code, move the stack of Post-it notes to the second foot... Rinse and repeat with all 4 feet.



What that is checking is if imparting a twist in to the frame on purpose makes things better or worse. If for any of the 4 feet the stack makes noise and chatter better... Then you have a mild frame alignment issues. Or you have a table which isn't as flat as your frame, causing the table to impart twist into your squared printers frame.


---
**Ryan Fiske** *February 25, 2018 17:02*

**+Eric Lien** thanks so much for the suggestion on acceleration. The stock config file on the main page has acceleration set at 9000. I put it down to 3000 and am no longer experiencing the chatter/buzzing! I lowered my current from 1A to 0.8A as well and it's still fine! Going to run it through another break-in cycle then attempt it at a slightly lower current.


---
**Michaël Memeteau** *February 25, 2018 19:13*

The left belt on the video seems a tad loose. Be also sure that the screws on the pulley are properly tightened on the stepper axis, any small shift will throw your printer out of square. Good luck... You're so close!




---
**Dennis P** *February 25, 2018 19:42*

I started dissecting the breaking gcode file... the comments say 200 mm/s but its actually 166.67 mm/s if you convert the F10000. 



I learned a lot by deciphering it line by line. I ultimately decided to write a spreadsheet to write the code snippets. **+j.r. Ewing** could you review and comment on it? **+Eric Lien** have time to look and share your thoughts? I would like to improve on it and make a little more elegant for everyone to use. 



I did linear moves for small and large squares and diamonds on one tab and circles on the other tab with G2 and G3.  I would appreciate suggestions and comments. 



My intent was to be able to change the bed dimensions, Z offset, boundary margin, square size, travel speed, etc and have it auto-populate the appropriate locations.  We can certainly add fields and parameters to the strings. 



[drive.google.com - Break-in_Code.xlsx](https://drive.google.com/open?id=1pyfs6ZslU-lZT92oVKiutRWEE7EcfzLw)


---
**Dennis P** *February 25, 2018 19:47*

**+j.r. Ewing** I looked up G2/G3 on the reprap wiki for this last night. 



[http://reprap.org/wiki/G-code#G2_.26_G3:_Controlled_Arc_Move](http://reprap.org/wiki/G-code#G2_.26_G3:_Controlled_Arc_Move)



It looks like most of the mainline firmwares support it. I don't think that its up to date though, even though the wiki says no for Marlin, the Marlin pages say yes. Smoothie, Marlin, Repetier, etc...  



[http://marlinfw.org/docs/gcode/G002-G003.html](http://marlinfw.org/docs/gcode/G002-G003.html)



When I ran the snippets I generated in S3D gcode preview, it drew the circles. I could not simulate it line by line though.



Do you have a suggestion for gcode simulator to try? 

[reprap.org - G-code - RepRapWiki](http://reprap.org/wiki/G-code#G0_.26_G1:_Move)


---
**Dennis P** *February 25, 2018 19:52*

**+Michaël Memeteau** that left belt and holder have been problematic. Its really loose at this point. I cant not tell if its the belt stretching or belt slipping.  I am actually reprinting another set b/c i don't like the fit of the belt and belt clamp. 


---
**Eric Lien** *February 25, 2018 20:41*

**+Dennis P** i will certainly look over this tonight. That would be a great addition. I generated the break-in gcode years ago as my first attempt at hand coding gcode. I haven't looked at since, just ran it. So having a system by which to custom generate one based on input variables would be awesome.


---
**Michaël Memeteau** *February 25, 2018 21:02*

**+Dennis P** Try [gcode.ws - gCodeViewer - online gcode viewer and analyzer!](http://gcode.ws/)




---
**Dennis P** *February 25, 2018 22:00*

**+Michaël Memeteau** The analyzer does not like my simplistic gcode. :( I think i am missing some instructions that it might be looking for. I think that it is looking for a properly formatted print file with layer info and the like.  

These two seem to display the tracks

[https://nraynaud.github.io/webgcode/](https://nraynaud.github.io/webgcode/)



[ncviewer.com - NC Viewer // Online GCode Viewer and Machine Simulator](https://ncviewer.com/)


---
**j.r. Ewing** *February 25, 2018 22:19*

**+Dennis P**  I amkinda new to the 3D printing world but I’ve been a cnc programmer 5 axis and lathe live tooling c y axis for 20 years.  I’ve been using cura I noticed it’s all g1 linear moves in some cases it’s a blocks per second thing that limits feed rates.   Fitting arcs (g2/3)can shorten the code and make smoother parts  but the controller does more work. I’m going to start building one of these ASAP. I was posting code as marlin. I’m getting a smoothie Ware this week. It supports g2g3. On a 3 axis always use IJK delta start to center. Not just R. There is a real chance you can zig not zag



All my machine warmup programs do figure 8 after each pass around picks up speed. 





Do you have to have % at beginning and end  and O#### for printer code?



Sorry for hijack

 








---
**j.r. Ewing** *February 25, 2018 22:21*

**+Dennis P** search for backplotter. Cimco has a freebie I think still may not like the E in the code though


---
**Dennis P** *February 25, 2018 23:29*

**+j.r. Ewing** no hijack... i TOTALLY hear you on this. I too come from some machining experience. I hate this simplistic triangle crap... but what you and I need to 'appreciate' is that the meshes and layers that these printers and slicers rely on is an antiquated concept of surface generation via layers and a mesh of triangles because that was the most efficient way with the computing power we had TWENTY years ago. Its very much like a CAT scan in reverse because it was invented at the same time. 



[https://en.wikipedia.org/wiki/Stereolithography](https://en.wikipedia.org/wiki/Stereolithography)



Fast forward 20 years and we are doing 5D & 6D CAM from CAD directly. There is not intermediate dumbing down of the physical model to fabricate it like the the consumer world of 3D printing that we are dabbling in.  I want Mastercam for 3D printing. I suspect that the complete workflows from ACAD and SW might be more efficient, but I dont have the funds to time to dabble.   



I made a post a few months ago complaining about this.. .Cura is the worst for me, S3D is unnecessarily complicated and KISS is lacking in the interface department- but I consistently get NICE prints with KISS so I stick with it. I have an easier time following the KISS gcode files too. 








---
**j.r. Ewing** *February 26, 2018 01:34*

If you don’t want to trig out the arcs use 45 degree corners and linear moves there’s a few free 2.5d cam packages out there that will give you centerline tool paths. Autocad 360 the home use one I thought did this. Remember I is not the Y.   I could give you the code you seek if you really are at a hardspot but you should learn it. 



It maybe possible to run a filter and fit arcs to existing point move code. My buddy had to use it to get a huge program on a small controller.  The layers should stack just the same even with .005 spline tolerance once my righ is running again I see science progestin



I am fluent in Masterscam, Feature-cam and solid works 


---
**Dennis P** *February 26, 2018 05:43*

**+j.r. Ewing** I just wanted a couple of quick and dirty parametric arc moves to plot out circles in excel so that it could easily be modified for bed sizes. 

 


---
**Dennis P** *February 26, 2018 06:22*

Here are a couple of videos of the printer reassembled with the speeds and acceleration turned down. First is the Break-In Circle gcode,  second is the Linear/Diamonds. Just one cycle of movements each as derived from the spreadsheets I posted. 



Curiously, the circles with arc moves is NOT smooth at all. I was expecting a much smoother motion.  Now I need to figure out what is behind the stutter. 




{% include youtubePlayer.html id=ltaYS52vLts %}
[youtube.com - Circles Break-in Code](https://youtu.be/ltaYS52vLts) and 
{% include youtubePlayer.html id=5RWIYjejMjI %}
[https://youtu.be/5RWIYjejMjI](https://youtu.be/5RWIYjejMjI)





 


---
**Eric Lien** *February 26, 2018 13:07*

**+Dennis P** the issue as far as I understand it is even with a gcode arc moves, the controller just takes that code and breaks it up into a bunch of line segments anyway. The only difference is now the controller is responsible for that math. 



Only controller at the hobby level that I know that actually does arcs correctly is TinyG and G2.




{% include youtubePlayer.html id=aucE49ZBXx0 %}
[https://youtu.be/aucE49ZBXx0](https://youtu.be/aucE49ZBXx0)


---
**Dennis P** *February 27, 2018 02:36*

I turned down the speeds and ran the single cylce gcodes again. The linear moves are much nicer, but the chatter/stickshion is still there. The circles on the other hand were also interesting in terms of where on the arc the issue shows up. 



Would anyone be willing to try running these scripts and reply with your observations? Each of these is the gcode generated by the spreadsheet I posted earlier.They are just one cycle each for a bed size of X290, Y280, with a 20mm margin around the perimeter, F7500.0.  



[drive.google.com - Break-In_GCODE_CIRCLES_DJP.gcode](https://drive.google.com/file/d/1f0ZXMR7l6OZvPvrKEvQGW0szjK1l2mCX/view?usp=sharing)



[https://drive.google.com/file/d/1KPXCQXVFCcJWrHgOXRG-knSHOrAHS0MG/view?usp=sharing](https://drive.google.com/file/d/1KPXCQXVFCcJWrHgOXRG-knSHOrAHS0MG/view?usp=sharing)



My next thing to try is to tune the voltage on the TMC2100's and see what if any effect that has. I may swap them out to a set of DRV's I have too to see if there is any difference.  



Thanks in advance and thanks again for everyone's support on getting my rig going. 


---
**Michaël Memeteau** *February 28, 2018 11:44*

I don't know if it has been tried before, but slightly (and I repeat, slightly) heat up the plastic around the bearing/bushing with a torch and run the break-in routine should do a nice job at self-aligning the carriage, don't you think?


---
**Dennis P** *March 01, 2018 05:12*

**+Michaël Memeteau** i thought about a sandpaper wrapped mandrel or really sharp reamer to true the holes. i used the rods as a guide to press the bushings into the bores and they went in pretty smoothly. 




---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/gWQY6jpK3hh) &mdash; content and formatting may not be reliable*
