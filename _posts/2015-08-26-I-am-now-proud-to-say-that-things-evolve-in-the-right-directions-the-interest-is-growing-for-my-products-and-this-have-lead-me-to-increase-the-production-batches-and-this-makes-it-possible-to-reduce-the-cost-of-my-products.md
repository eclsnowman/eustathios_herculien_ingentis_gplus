---
layout: post
title: "I am now proud to say that things evolve in the right directions, the interest is growing for my products and this have lead me to increase the production batches and this makes it possible to reduce the cost of my products"
date: August 26, 2015 19:29
category: "Show and Tell"
author: Martin Bondéus
---
I am now proud to say that things evolve in the right directions, the interest is growing for my products and this have lead me to increase the production batches and this makes it possible to reduce the cost of my products. I would like to thank all you fantastic customers that have made this  possible. 



Prices are reduced with up to 30% and I hope that this will make it possible for more people to start to use the best and most reliable extruder on the market.  Also there is a growing number of custom mounts in order to get an easy installation.



[http://www.bondtech.se](http://www.bondtech.se)



With best regards

Bondtech AB / Martin﻿





**Martin Bondéus**

---
---
**Frank “Helmi” Helmschrott** *August 26, 2015 20:34*

Good News, Martin, Keep up the good work.


---
*Imported from [Google+](https://plus.google.com/+MartinBondéus-Bondtech/posts/fuLLZceR427) &mdash; content and formatting may not be reliable*
