---
layout: post
title: "I am pouring through the wealth of knowledge here in this community, haven't yet found this answer, so I am throwing it out to you guys...: Can I increase the Z height of the Spider V2 simply by purchasing longer smooth rods,"
date: November 28, 2016 04:12
category: "Build Logs"
author: Carter Calhoun
---
I am pouring through the wealth of knowledge here in this community, haven't yet found this answer, so I am throwing it out to you guys...:  Can I increase the Z height of the Spider V2 simply by purchasing longer smooth rods, ball screws, and extrusions?  If so, I want to increase Z height by 75mm. 



Assuming this is accurate...



The BOM links to vertical extrusions from Misumi with screw holes drilled relative the the edges, so simply extending the length of these (562mm to 637mm) should do the trick, thoughts?



And, smooth rod from 435 to 510mm?



Would these 500mm ball screws be appropriate appropriate ([https://www.aliexpress.com/item/RM1204-Ball-Screw-L500mm-Ballscrew-With-SFU1204-Single-Ballnut-For-CNC-Processing-length-can-be-customized/1588730692.html?spm=2114.13010208.99999999.264.w6L2ba](https://www.aliexpress.com/item/RM1204-Ball-Screw-L500mm-Ballscrew-With-SFU1204-Single-Ballnut-For-CNC-Processing-length-can-be-customized/1588730692.html?spm=2114.13010208.99999999.264.w6L2ba)).



And, mad props to **+Neil Merchant**, your build is nearly there - looking forward to see those first prints.









**Carter Calhoun**

---
---
**Eric Lien** *November 28, 2016 04:52*

For the ball screws just contact GolMart based on the other PN and ask them to make it 75mm longer. They will tell you the cost change if any. That's likely the safest bet. The rest seems correct to make it taller.


---
*Imported from [Google+](https://plus.google.com/104205816672263226409/posts/B55cDyTJDTV) &mdash; content and formatting may not be reliable*
