---
layout: post
title: "If you are interested in a new hot-end or Bondtech SMG, Hobby-Fab is closing and they are selling off inventory"
date: January 15, 2019 21:28
category: "Discussion"
author: Bruce Lunde
---
 If you are interested in a new hot-end or Bondtech SMG, Hobby-Fab is closing and they are selling off inventory. Look at their inventory,  add it to your cart, then use Coupon code CYAHF to get deep discounts of 30-50%. [https://www.hobby-fab.com](https://www.hobby-fab.com)





**Bruce Lunde**

---
---
**Satchel Sieniewicz** *January 16, 2019 14:37*

where do you put the coupon code


---
**Bruce Lunde** *January 16, 2019 17:29*

**+Satchel Sieniewicz** when yo review your cart, there is a line at the bottom to put in the code.


---
**Satchel Sieniewicz** *January 17, 2019 01:28*

**+Bruce Lunde** thanks mate


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/2SeTYz3YVFs) &mdash; content and formatting may not be reliable*
