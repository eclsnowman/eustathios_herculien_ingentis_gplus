---
layout: post
title: "I'm trying to find a link to a 300mm^2, 110v Silicone heater but can't come up with one on Ali Express"
date: December 20, 2015 05:41
category: "Discussion"
author: Kevin Conner
---
I'm trying to find a link to a 300mm^2, 110v Silicone heater but can't come up with one on Ali Express.  

Anyone got any pointers? 

Do I have to make a custom request? 

Thanks ahead of time.





**Kevin Conner**

---
---
**Ray Kholodovsky (Cohesion3D)** *December 20, 2015 06:04*

Ali Brother or Keenovo, I've had Keenovo make me custom sizes and power specs before. Shoot them an email if you don't find what you need. 


---
**Oliver Seiler** *December 20, 2015 06:12*

Just ask for a quote for the size and volts/wattage here:

[http://alirubber.en.alibaba.com/](http://alirubber.en.alibaba.com/)


---
**Oliver Seiler** *December 20, 2015 06:15*

I think most people here have been dealing with sivialiang@alirubber.com.cn

Just send an email. They are pretty quick.


---
**Hakan Evirgen** *December 20, 2015 13:49*

Keenovo is really good! They also make custom sizes but 300mm is standard.


---
**ThantiK** *December 20, 2015 16:17*

Seconding the alirubber recommendation.  They will make custom sizes, at custom resistances/wattage/voltage


---
**Kevin Conner** *December 21, 2015 05:12*

Thanks for the advice fella's!


---
**Kevin Conner** *December 22, 2015 06:35*

Found this one too. I can lump it in for shipping [http://www.robotdigg.com/product/115/300mm+Silicone+Rubber+Heater+Pad](http://www.robotdigg.com/product/115/300mm+Silicone+Rubber+Heater+Pad)




---
**Chris Brent** *December 23, 2015 18:37*

That one's "12V or 24V 300W" What happens if you throw 110V AC at it?


---
**Kevin Conner** *December 23, 2015 18:39*

110v was one of the options


---
**Chris Brent** *December 23, 2015 18:43*

Whoops, didn't see the drop down sorry.


---
**Kevin Conner** *December 23, 2015 18:50*

No worries. I always appreciate verification!  Now if I could just get through robotdigg's weird "human in the middle" shipping quote deal I could get the parts shipped!


---
*Imported from [Google+](https://plus.google.com/+KevinConner/posts/8LX1u3vUuHL) &mdash; content and formatting may not be reliable*
