---
layout: post
title: "My printer's having a hard time doing small details with low-extrusion areas (fine details at low layer heights)"
date: August 19, 2016 02:23
category: "Discussion"
author: Erik Scott
---
My printer's having a hard time doing small details with low-extrusion areas (fine details at low layer heights). I think it's something to do with the bowden setup and such. I've been looking at doing a new carriage, and I like **+Walter Hsiao**'s mini space invader one, so I think i'm going to switch to that, but because of this extrusion issue, I think it might be worth going whole-hog and doing the flying extruder. Does anyone know where i can find the files to do this? I can't seem to find them anywhere.





**Erik Scott**

---
---
**Eric Lien** *August 19, 2016 02:28*

Can you give some examples of your problem prints. We might have some tips before ditching Bowden. But if you are set on it, I know Walter has all the files on thingiverse for the flying extruder hinges and carriage.


---
**Michaël Memeteau** *August 19, 2016 07:32*

Try here:

[http://thrinter.com/cartesian-flying-extruder/](http://thrinter.com/cartesian-flying-extruder/)

[http://www.thingiverse.com/thing:1264376](http://www.thingiverse.com/thing:1264376)

[http://www.thingiverse.com/thing:1167736](http://www.thingiverse.com/thing:1167736)




---
**Erik Scott** *August 19, 2016 13:45*

I'm at work at the moment so I can't post any pictures, but it tends to stop extruding enough plastic if I'm printing small details at a low layer height. 



**+Michaël Memeteau**​ I saw those. I don't have the extruder mount or the lengths of the aluminum extrusions. 


---
**Michaël Memeteau** *August 31, 2016 09:17*

**+Erik Scott** I've just figured how to calculate the length. It's quite simple and depends on the geometry of your printer. The first beam must be able to reach the center of the printing area. The second arm will have the length equal to the radius of the circle in which the printing area is included.

In my case (#Black Panther), I can only use the corners (I use the lateral side as rolling surface), which means that the first beam is longer than in Walter's case (~310 mm vs ~220 mm). The second beam is 220 mm as well). My printer is 500 x 500  but printable area is more like 340 x 340 mm.

I've made a short study with Onshape, you can check it here:

[cad.onshape.com - Onshape](https://cad.onshape.com/documents/61223225160d4028d521ff73/w/e3afe5f3d5808735f3f49908/e/fb85e5aa9d6c9ab914bfdc14)



Meanwhile, I'm also experimenting an idea I had already a while ago which consist in a pyramid on top of which the extruder is seated in a cardan. Will post more when ready... Good luck for you mods!




---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/DJUEVUkp1r7) &mdash; content and formatting may not be reliable*
