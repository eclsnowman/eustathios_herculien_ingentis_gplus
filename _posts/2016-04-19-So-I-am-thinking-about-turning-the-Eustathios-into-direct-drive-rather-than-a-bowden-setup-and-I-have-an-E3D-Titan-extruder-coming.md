---
layout: post
title: "So I am thinking about turning the Eustathios into direct drive rather than a bowden setup and I have an E3D Titan extruder coming"
date: April 19, 2016 12:40
category: "Discussion"
author: Bud Hammerton
---
So I am thinking about turning the Eustathios into direct drive rather than a bowden setup and I have an E3D Titan extruder coming. I am hoping **+Walter Hsiao** is thinking about the same thing (although that is entirely wishful thinking on my part and some selfishness) and decides to either mod his current extruder carriage or create a new one. Is there anyone else who is running direct drive or is everyone running bowden?





**Bud Hammerton**

---
---
**Eric Lien** *April 19, 2016 13:19*

If you want to run Flexible then Direct Drive is the way to go. But if flexible isn't a big driving force I still like Bowden. The weight savings is very valuable for higher printing speeds. And with a Bondtech as the bowden drive I can't tell the difference in print quality between my direct drive and bowden machines. Sure it takes more time to tune in the retract amount, retract speeds, wipe, coast, etc settings. But once done it is a set it and forget it profile for me.


---
**Bud Hammerton** *April 19, 2016 13:26*

**+Eric Lien** I appreciate the comments, My thought process was to use a small lightweight stepper like something around 20 oz/in. Since the Titan is geared 3:1 there should be no need for some big 50-60 oz/in stepper motor. Yes it will add weight to the extruder but we are probably talking about less than 180-200g total. I have no need for flexible filament yet. I am thinking about future projects. That said, I could always adapt the other printer I have to make better use of flexible filament as it has a Greg's Wade on it already.


---
**Øystein Krog** *April 19, 2016 15:22*

Look at the ultimaker forums for an example of a very lightweight direct drive extruder. Some of my previous posts have links. I created one too, but for my Cartesian style machine. 


---
*Imported from [Google+](https://plus.google.com/+BudHammerton/posts/6z1rpS9puac) &mdash; content and formatting may not be reliable*
