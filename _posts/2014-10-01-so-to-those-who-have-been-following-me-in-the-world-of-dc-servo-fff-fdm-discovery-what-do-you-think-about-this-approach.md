---
layout: post
title: "so to those who have been following me in the world of dc servo fff/fdm discovery what do you think about this approach?"
date: October 01, 2014 23:00
category: "Discussion"
author: D Rob
---

{% include youtubePlayer.html id=HzVCyFR6taY %}
[DIY Servo Controller - Four Axis - PCBs](http://www.youtube.com/watch?v=HzVCyFR6taY) so to those who have been following me in the world of dc servo fff/fdm discovery what do you think about this approach? H-Bridges are fairly cheap, but I don't know how to implement this into current firmware and design. The H-bridge boards and encoder input ports could connect through a pololu form factor break out board and firmware could do the rest. What say you gentle men, and ladies?





**D Rob**

---
---
**Miguel Sánchez** *October 02, 2014 06:46*

I've ordered a dual H-bridge for $1.5 off eBay. But that can handle 800mA max. The ones above are like 7.5A at 100V.  IMHO,  If we want to have a chance to replace steppers it has to be done with cheaper or equal cost and similar sized motors. There is no doubt it can be done, the question that remains is whether it can be done cost-effectively. 


---
**D Rob** *October 02, 2014 15:21*

Price follows the tech when a lot of competition arises. We need to make the beta prove a need and the give it to a thousand Chinese companies. That should drive price down


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/88k6LsBxw3W) &mdash; content and formatting may not be reliable*
