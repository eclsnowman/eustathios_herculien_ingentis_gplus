---
layout: post
title: "Finally getting the hang of fusion 360"
date: March 09, 2015 03:28
category: "Deviations from Norm"
author: Joe Spanier
---

{% include youtubePlayer.html id=5mE39OcIcuc %}
[https://www.youtube.com/watch?v=5mE39OcIcuc&feature=youtu.be](https://www.youtube.com/watch?v=5mE39OcIcuc&feature=youtu.be)



Finally getting the hang of fusion 360. Modeling my Eustanthios variant. 





**Joe Spanier**

---
---
**Владислав Зимин** *March 09, 2015 07:55*

**+Joe Spanier** what are the advantages of this design?


---
**Joe Spanier** *March 09, 2015 19:30*

The main thing is Im doing it haha. It drops the aluminum extrusion for cnc routed MDF, because well, I like making stuff that way. It will use a different Z design too. Not sure its better, some may call it a down grade, but it will be a nice heated box and hopefully look awesome. Also Im planning for a 12x12x18 build area. 


---
*Imported from [Google+](https://plus.google.com/+JoeSpanier/posts/fytDCb85v3b) &mdash; content and formatting may not be reliable*
