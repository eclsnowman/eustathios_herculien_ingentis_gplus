---
layout: post
title: "First time in ages I've of able to get to printing and check the community"
date: December 08, 2015 12:04
category: "Deviations from Norm"
author: D Rob
---
First time in ages I've of able to get to printing and check the community. I'm so proud to see how our community has grown so much. Welcome to all of the newer members!





**D Rob**

---
---
**Eric Lien** *December 08, 2015 13:09*

Long time no see **+D Rob**​. How's it going?﻿


---
**D Rob** *December 08, 2015 14:31*

Been good. Almost done with finals! The end to my freshman year. I feel too old for this stuff. Man it sure is nice to see all this activity on our community.


---
**D Rob** *December 08, 2015 14:31*

How have you been?


---
**Eric Lien** *December 08, 2015 14:46*

Great. Recently changed jobs (back to an old job technically) but life is good. I have been having a lot of fun with printers and CNC. I built a Delta with **+Alex Lee** which was a blast and is super fun to watch. And most recently built a Talos Spartan printer (I worked on this with Alex too). So now I have 4 printers :)



And for CNC cutting I worked on the R7 CNC by SMW3D with **+Brandon Satterfield** . So I have been pretty busy with projects. I need to take a step back from designing new stuff for a while and just make stuff with my machines. Plus I am out of desk and garage space.



My two boys are doing great. My youngest just turned 3yrs old yesterday. They grow up too fast. People always say that, but it doesn't sink in until you take a step back from the hectic life of parenting.


---
**D Rob** *December 09, 2015 06:21*

That's great man! glad to see you are doing well


---
**Tim Rastall** *December 16, 2015 06:06*

Hello chaps. Also just poked my head in to see what was happening.  Sounds like you've both been busy. Me too: Fully occupied with work for the last 9 months, no time for printers! I did buy a zortrax m200 for the lab at work though, it's a great machine - best fdm quality I've come across. Also, just hired a graduate to work full time on solidworks stuff too. All the printing knowledge is definitely coming in handy for the prototypes we are making :D. Glad the community is still going. Good work **+Eric Lien**​.


---
**Eric Lien** *December 16, 2015 06:26*

**+Tim Rastall** great to hear things are going well for you. The community you created is still going strong and I can't thank you enough for everything you did with your open design and years of knowledge you shared.



There are lots of great makers here now, and new members regularly going which makes me proud each day to be a part of this wonderful community.


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/TTk4pWALQh7) &mdash; content and formatting may not be reliable*
