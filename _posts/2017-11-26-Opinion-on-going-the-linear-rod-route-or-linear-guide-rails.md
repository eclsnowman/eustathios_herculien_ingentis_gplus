---
layout: post
title: "Opinion on going the linear rod route or linear guide rails"
date: November 26, 2017 23:54
category: "Discussion"
author: Gus Montoya
---
Opinion on going the linear rod route or linear guide rails. On my first Eustathios spider build I used the rods and they rusted. I did use 1 to 1 all purpose oil on them. I want to buy as much of my items on cyber Monday as I can. Can anyone give me their two cents? Was i suppose to use lithium, grease instead? **+Sonny Mounicou**  Can you advice? 





**Gus Montoya**

---
---
**Øystein Krog** *November 27, 2017 10:56*

If you can afford to use rails it's better in every way, but I don't really see how proper rods could rust in a normal environment..


---
**Ryan Carlyle** *November 27, 2017 14:13*

**+Øystein Krog**  Non-chromed rods (eg bare tool steel) will rust pretty aggressively if they're not kept oiled/greased and the humidity is over about 40%. Personally I tend to shell out a little more for chromed rods so it's not an issue. 


---
**Øystein Krog** *November 27, 2017 14:30*

Ah, I was not aware using non-chromed rods was an option at all :) These days getting really good (chromed) rods from china is so cheap that I kind of assumed that's what everyone does.


---
**Ryan Carlyle** *November 27, 2017 14:35*

Non-chromed is kind of silly because the cost of chroming is so low compared to the hassle of preventing or dealing with rust. Even if you order from somewhere somewhat expensive like Misumi, it's $10.00 for 300mm x 8mm chromed/hardened rod vs $8.14 for hardened. 


---
**Gus Montoya** *November 28, 2017 02:52*

I basically used the link in the BOM. Not sure if it's been updated since. Thanks all for your replies. 


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/XTJTHF2ojAN) &mdash; content and formatting may not be reliable*
