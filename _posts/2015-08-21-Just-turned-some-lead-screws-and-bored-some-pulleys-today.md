---
layout: post
title: "Just turned some lead screws and bored some pulleys today;"
date: August 21, 2015 02:56
category: "Show and Tell"
author: Ishaan Gov
---
Just turned some lead screws and bored some pulleys today; Z axis is coming together for my Ingentis Variant, the stepUP 😀

--TR10x2 lead screws

--Custom hand-milled nut blocks

--8 x 19mm thrust bearing

--2:1 stepper motor reduction

--SK16 bearing blocks clamp 688zz bearings inside 



![images/2eb93a82d34f04aa443cb2db3775f4ba.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2eb93a82d34f04aa443cb2db3775f4ba.jpeg)
![images/720f617e79a47e87abf45d455b4e9949.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/720f617e79a47e87abf45d455b4e9949.jpeg)
![images/fce913888828d914b502cdca76988eff.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fce913888828d914b502cdca76988eff.jpeg)
![images/378ca418bd82debf5b302d51d1d23d90.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/378ca418bd82debf5b302d51d1d23d90.jpeg)

**Ishaan Gov**

---
---
**Eric Lien** *August 21, 2015 03:23*

Wow, this is one heck of a machine you have coming together. I can't wait to see it in motion.


---
**Ishaan Gov** *August 21, 2015 03:40*

Thanks **+Eric Lien**, without your inspiration, this build wouldn't have started! 


---
**Rick Sollie** *August 29, 2015 18:24*

I really like this idea and if I get the chance to build another  I'm going to go with these rails. Keep posting, I'm interested to see how this works.


---
**Ishaan Gov** *August 29, 2015 19:01*

**+Rick Sollie**, To be honest, I'm not a huge fan of the V-wheels and the v-slot (not because of quality issues; openbuilds rocks) because they have more deflection than I'd like them to have; I'd much rather go with 12mm or 16mm chrome steel shafts with SC-series or LMF-series bearing blocks for more rigidity


---
*Imported from [Google+](https://plus.google.com/113675942849856761371/posts/fLvKZdCsGGE) &mdash; content and formatting may not be reliable*
