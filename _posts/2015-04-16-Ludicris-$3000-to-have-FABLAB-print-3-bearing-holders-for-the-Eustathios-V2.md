---
layout: post
title: "Ludicris!!!! $30.00 to have FABLAB print 3 bearing holders for the Eustathios V2"
date: April 16, 2015 17:02
category: "Discussion"
author: Gus Montoya
---
Ludicris!!!!  $30.00 to have FABLAB  print 3 bearing holders for the Eustathios V2. At that rate, it'll cost more then a $100 to print all the parts.  I prefer to wait for the "print is forward". I guess I'll put the rest of the expensive parts purchases on hold till I get the 3D printed items.





**Gus Montoya**

---
---
**Derek Schuetz** *April 16, 2015 17:22*

you never responded to my post about being able to help you out if you supply the material


---
**Mykyta Yurtyn** *April 16, 2015 17:23*

I'm experimenting with slightly different configuration of bearing holders, but printed the regular ones for a fallback, just in case. If my experiments workout, I will not need the standard holders and can send them to you if you still need them. It will be at least after next weekend though


---
**Gus Montoya** *April 16, 2015 17:28*

**+Derek Schuetz** I lost your message. For some reason I couldn't get it back. Since I messaged more then 1 person, I also forgot whom responded. I'll take your offer. Can you email me? gus.montoya@gmail

**+Mykyta Yurtyn**  Sure I'll take em :)   Keep me posted.


---
**Isaac Arciaga** *April 16, 2015 20:36*

**+Gus Montoya** are you needing the bearing holders for the 10mm gantry rods?


---
**Gus Montoya** *April 16, 2015 23:06*

**+Isaac Arciaga**  I am needing every 3d printed part required for the Eustathios V2 build. 


---
**Isaac Arciaga** *April 16, 2015 23:19*

**+Gus Montoya** I probably have all the plastic for the gantry that I can send to you. I'd have to check tonight.


---
**Gus Montoya** *April 16, 2015 23:35*

That would be awesome :) Can you send a picture to my email? This is the V2 model correct?


---
**Isaac Arciaga** *April 17, 2015 02:21*

**+Gus Montoya** it looks like **+Derek Schuetz** can do better by printing the whole set for you if you send him a spool.


---
**Gus Montoya** *April 18, 2015 09:13*

Sent money, I love this community. Thanks derek!


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/b5UaMEoao2e) &mdash; content and formatting may not be reliable*
