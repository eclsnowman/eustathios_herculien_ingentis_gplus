---
layout: post
title: "Need to start thinking about controllers. I've budgeted for a good quality one, and am thinking maybe a duet with no screen but am interested to hear what people are using and/or recommend?"
date: February 26, 2018 00:53
category: "Discussion"
author: Julian Dirks
---
Need to start thinking about controllers.  I've budgeted for a good quality one, and am thinking maybe a duet with no screen but am interested to hear what people are using and/or recommend?





**Julian Dirks**

---
---
**Eric Lien** *February 26, 2018 01:25*

As far as brands I really like: Panucatt Devices, Cohesion3d, and Duet3D.



Smoothieware based boards are great. But everything I have read and heard about the DuetWiFi really makes me want to try that as well. A lot of people I respect really praise the board and it's firmware. I wish there was an ecosystem of boards around the firmware similar to Marlin and Smoothieware. Competitive boards would push the firmware and hardware forward IMHO.


---
**wes jackson** *February 26, 2018 01:27*

Im going to be doing beaglebone black with the CRAMPS cape and mavhinekit


---
**Oliver Seiler** *February 26, 2018 01:30*

I can wholeheartedly recommend the DuetWifi, it's way better than the Smoothieboards I've used. You don't need a display, but it's very convenient.


---
**Julian Dirks** *February 26, 2018 01:46*

**+Oliver Seiler** What is it that makes it better than smoothies?


---
**Dennis P** *February 26, 2018 03:50*

**+wes jackson**  can you post links for the CRAMPS and machinekit? I want to make sure I am looking at the same thing 


---
**Daniel Kruger** *February 26, 2018 04:07*

**+Dennis P** Unless  you are a Linux person, machinekit is a fairly deep rabbit hole to jump in to.


---
**Jeff DeMaagd** *February 26, 2018 04:08*

**+Julian Dirks** Duet's web server is far nicer and more powerful. It lets you control just about anything on the machine including edit g code and all configuration files. It also has actual decent SD card performance where smoothieboard will take a minute to save a 3MB file and Duet does it in maybe 5-10 seconds. There's also a lot more expansion options built into the hardware. The Trinamic motor drivers are very nice and powerful.


---
**Oliver Seiler** *February 26, 2018 04:53*

**+Julian Dirks** from the top of my head

- the stepper drivers are very good (reliable, powerful, quiet) and better than the ones on the Smoothieboards (unless you use external ones). My prints look smoother at similar settings/speeds.

- the web interface is excellent and gives you a proper UI to do pretty much anything you need. Smoothieware is very limited in comparison. The DuetWifi has reliable Wifi on board

- the configuration works much better for me. I had to get used to put everything into GCode over using a properties file, but it actually works well, is flexible and can be changed on the fly

- features like PID tuning simply work at first attempt. I never got my temperatures anywhere as stable on Smothieware. Maybe it is technically possible, but I never managed.

- support is great and always friendly

- I did read that the Duet HW is better protected against shorts etc. - I have not verified that but got pretty frustrated when my Smoothieboard got grilled to the MCU when a thermal resistor input cable shortened to GND.


---
**Dennis P** *February 26, 2018 05:25*

**+Daniel Kruger** honestly, I would much rather scrap all this lightweight hardware and just run EMC to control a printer. The cost of FPGA I/O card is fractions of what some of these boards cost.  But yeah, rabbit holes they are. 


---
**Michaël Memeteau** *February 26, 2018 13:05*

I'm also considering a similar move. Look at the video experiment with pressure advance management. Using a diamond head with the same filament color to overcome the issue of not being able to melt the filament fast enough is simple as it is effective.

Smoothieware always had some issue to integrate that into the base code, it's apparently closely linked to the way the cinematic is programmed but I don't know enough about it to help here.




{% include youtubePlayer.html id=lnYYNfVoxmQ %}
[youtube.com - Duet pressure advance testing](https://www.youtube.com/watch?v=lnYYNfVoxmQ) 


---
**wes jackson** *February 26, 2018 14:39*

Dunno with pressure advance, but machinekit does this by tying the extruder motor to the speed of the hotend. This allows for a constant extrusion, and not under at the beginning and over at the end. Link to cramps cape below.

Assymbled: [http://pico-systems.com/osc2.5/catalog/product_info.php?cPath=5&products_id=35](http://pico-systems.com/osc2.5/catalog/product_info.php?cPath=5&products_id=35)



[pico-systems.com - Pico Systems : - Contact for Price!](http://pico-systems.com/osc2.5/catalog/product_info.php?cPath=5&products_id=35)


---
**Michaël Memeteau** *February 26, 2018 16:02*

**+wes jackson** Linking extrusion speed and hot-end speed is standard in every 3D printer controller I know of... Advanced pressure management, not so much! Extruding soft plastic which pressure builds up in the melting chamber is everything but trivial and linear.


---
**Julian Dirks** *February 26, 2018 21:45*

Thanks for the helpful comments everyone.  I'm going to roll with a Duet.


---
**wes jackson** *February 27, 2018 05:05*

**+Michaël Memeteau** 3D printers dont link speed, accel, or any movement of the carriage to extrusion they say move from here to here using this top speed, and while your at it, i need this much extruded. pressure advance is a kind of software hack to make this a bit better, while what machinekit does is actively change extruder speed during the move, causing a more uniform extrusion. let me know if its not clear and ill try to explain it better.



edit: i forgot to mention, this is live on the controller side, not on the slicer side. Its working with the live values, not the optimistic ones the slicer is working with.


---
**Michaël Memeteau** *February 27, 2018 09:31*

**+wes jackson** Thank you for correcting me: the slicer does this link between the speed of the extruder and the cinematic. The controller is a different beast and, effectively, must handle data in a more real-time manner. I do believe that pressure advance is based on trying to model the physics behind the pressure build-up in the melt chamber.

More info here (inclusive with Machine Kit):



* [http://basdebruijn.com/2014/05/machinekit-and-additive-manufacturing/](http://basdebruijn.com/2014/05/machinekit-and-additive-manufacturing/)

* [bernhardkubicek.soup.io - My anti oozing algorithm was implemented using linuxcnc +beaglebone. Just lik...](http://bernhardkubicek.soup.io/post/425547834/My-anti-oozing-algorithm-was-implemented-using)


---
*Imported from [Google+](https://plus.google.com/113795478307151372873/posts/8DkXF5ryL4g) &mdash; content and formatting may not be reliable*
