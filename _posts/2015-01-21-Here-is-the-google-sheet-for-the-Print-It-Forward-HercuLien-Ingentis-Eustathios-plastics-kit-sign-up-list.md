---
layout: post
title: "Here is the google sheet for the \"Print It Forward\" HercuLien / Ingentis / Eustathios plastics kit sign up list"
date: January 21, 2015 01:08
category: "Discussion"
author: Eric Lien
---
Here is the google sheet for the "Print It Forward" HercuLien / Ingentis / Eustathios plastics kit sign up list. Pretty basic right now, but it should be fully open for editing by all now (so make it pretty if you have the skills :)﻿





**Eric Lien**

---
---
**Florian Schütte** *January 21, 2015 11:01*

May you explain what and how in the sheet?


---
**Eric Lien** *January 21, 2015 11:52*

+Florian Schütte good idea :)


---
**Mike Thornbury** *February 03, 2015 10:07*

You can take my name off, if you like. I have another idea and don't want to take up a more serious builder's place. I have such a long list of CNC stuff backlogged, it would be ages before I can get to build one of these.



Thanks.


---
**Seth Messer** *February 12, 2015 20:20*

Just want to thank you for starting this to give the newcomers a chance at a 3d printer-filled life. :D


---
**Mikael Sjöberg** *February 15, 2015 19:47*

Fantastic intiative! I have added my name!


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/bg7NPYn4AA1) &mdash; content and formatting may not be reliable*
