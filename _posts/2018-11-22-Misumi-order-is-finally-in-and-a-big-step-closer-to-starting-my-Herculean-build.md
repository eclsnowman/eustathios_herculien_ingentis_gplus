---
layout: post
title: "Misumi order is finally in and a big step closer to starting my Herculean build"
date: November 22, 2018 00:27
category: "Build Logs"
author: William Rilk
---
Misumi order is finally in and a big step closer to starting my Herculean build.  Exciting times ahead!

![images/edc7781fbd937791863cc8890fa7b04d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/edc7781fbd937791863cc8890fa7b04d.jpeg)



**William Rilk**

---
---
**Eric Lien** *November 22, 2018 01:44*

Exciting times indeed. I look forward to watching your progress. 


---
*Imported from [Google+](https://plus.google.com/100191047182984055447/posts/1t5YGYhTKz2) &mdash; content and formatting may not be reliable*
