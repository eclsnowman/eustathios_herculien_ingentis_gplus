---
layout: post
title: "I have enough extrusion for a 500mm cube, I have 10mm, 8mm smooth rods, I have 10mm trapezoid screws etc, I think it's time I start building"
date: January 20, 2015 07:37
category: "Discussion"
author: Keith Dibling
---
I have enough extrusion for a 500mm cube, I have 10mm, 8mm smooth rods, I have 10mm trapezoid screws etc, I think it's time I start building. 



Do you all use the same set of stl files or are all machines individual?





**Keith Dibling**

---
---
**Jim Wilson** *January 20, 2015 11:19*

Most of the printed parts stay the same, even if you alter the extrusion length or overall dimensions, you just need to also alter the smooth rod lengths to match.﻿


---
**Keith Dibling** *January 20, 2015 11:47*

I've all rods already..



Not been on the group for a bit, what's a ' HercuLien'?



Also, are the printed part stl's available from one source?


---
**Jim Wilson** *January 20, 2015 11:52*

[https://github.com/jasonsmit4/Eustathios](https://github.com/jasonsmit4/Eustathios)



And



[https://github.com/eclsnowman/HercuLien](https://github.com/eclsnowman/HercuLien)


---
**Keith Dibling** *January 20, 2015 12:27*

Thanks Jim






---
**Keith Dibling** *January 21, 2015 08:26*

I've started to print the parts, the bearing holders A x4 and B x4... Are these the same? I cannot see a difference once printed


---
*Imported from [Google+](https://plus.google.com/103631495948845103844/posts/bi8eJsLjU5W) &mdash; content and formatting may not be reliable*
