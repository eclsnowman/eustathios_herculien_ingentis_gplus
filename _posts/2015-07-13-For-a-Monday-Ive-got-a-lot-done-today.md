---
layout: post
title: "For a Monday, I've got a lot done today!"
date: July 13, 2015 22:00
category: "Discussion"
author: Brandon Cramer
---
For a Monday, I've got a lot done today!

I need to go down to the hardware store and overpay for (4) 5mm X 25mm Socket Head Screws/Bolts. Not sure where they went, but I definitely ordered them.... I bet they wont be .17 cents each  :) 

Question about the power supply and solid state relay. How do they attach to the Eustathios Spider V2 printer? I also assume there is an electrical guide to hooking up all this wiring? :-)



![images/0814a4520eb6132ef5b8d96f533ed8c1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0814a4520eb6132ef5b8d96f533ed8c1.jpeg)
![images/cec52f268643afb11e3d86292d58b20b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cec52f268643afb11e3d86292d58b20b.jpeg)
![images/42fdbeaea7454760aa7c31de80bbc512.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/42fdbeaea7454760aa7c31de80bbc512.jpeg)

**Brandon Cramer**

---
---
**Eric Lien** *July 14, 2015 01:05*

For the solid state relay you can use this diagram from the Herculien. Note where it says 120 volt live and neutral that power is from the outlet switch/fuse/plug module



 [https://github.com/eclsnowman/HercuLien/raw/master/Documentation/Azteeg_x3_v2_wiring_(with_notes).pdf](https://github.com/eclsnowman/HercuLien/raw/master/Documentation/Azteeg_x3_v2_wiring_(with_notes).pdf)


---
**Eric Lien** *July 14, 2015 01:14*

Power flows through the load side at mains voltage into and out of the bed back to neutral. The normal bed output on the board connects into  the SSR inputs and acts to signal the SSR load contact to open and close.


---
**Gústav K Gústavsson** *July 14, 2015 02:31*

Did some sketches freehand for **+Gus Montoya** last week regarding heated bed connections and SSR as the link **+Eric Lien** sent then was not working. This new link works ;-) but if you want I can send you the sketches (includes outlet switch, and fuse)


---
**Brandon Cramer** *July 14, 2015 02:54*

**+Gústav K Gústavsson** That would be great! I would rather ask questions now before I start hooking up the wiring and break something. Thank you!


---
**Eric Lien** *July 14, 2015 03:08*

**+Gústav K Gústavsson** weird, link works fine for me.


---
**Brandon Cramer** *July 14, 2015 22:02*

Holy T-Nuts! I pretty much tore the entire printer apart adding those last few T-Nuts I needed for holding the Plexiglas. What a pain!

Not sure if I got the answer on how to mount the electrical equipment underneath the Eustathios Spider V2. Is it glued on or are there holes drilled in the Plexiglas to hold it from above?


---
**Eric Lien** *July 14, 2015 22:34*

**+Brandon Cramer** I just drilled the holes by hand. Never modeled it. So many people exchange electronics, don't want holes that don't line up. Also I should add some drop-in T-nuts to the BOM for things like this ;)


---
**Brandon Cramer** *July 14, 2015 23:48*

**+Eric Lien** Wait.... They have drop-in T-nuts. :) 



Drilling the holes seems easy enough.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/PyDrvUiLww8) &mdash; content and formatting may not be reliable*
