---
layout: post
title: "Why is it so dead? Anyone making anything on their 3d printer or making progress on their build?"
date: May 12, 2015 05:30
category: "Discussion"
author: Gus Montoya
---
Why is it so dead? Anyone making anything on their 3d printer or making progress on their build? I'm still wainting on my viki LCD. It's out of stock...





**Gus Montoya**

---
---
**Derek Schuetz** *May 12, 2015 06:09*

Just waiting on my parts to trickle in then the building starts I'll also have a form 1+ arriving in the mail soon


---
**Eric Lien** *May 12, 2015 09:42*

**+Derek Schuetz** wow, nice.


---
**Eric Lien** *May 12, 2015 11:56*

Looks like you can get a Viki 1 on eBay for $50﻿, you would need to use the spider v1 mount.﻿



[http://m.ebay.com/itm/151673748690?nav=SEARCH](http://m.ebay.com/itm/151673748690?nav=SEARCH)﻿



But I do recommend the viki2, it works better with smoothieware I think (others had trouble with viki1 if I recall).


---
**Jim Wilson** *May 12, 2015 13:48*

Quiet because I... Found out that I inadvertently... Printed an entire set of Eustathios v2 parts at 107% size... >.<


---
**Vic Catalasan** *May 12, 2015 16:27*

LOL how did you do that? I am guessing you did not re-scale it but your calibration was off?



I will get busy here shortly, I am only waiting on Misumi and Robotdigg orders...and I am building two complete system one for me and the other for a local friend. 


---
**Jim Wilson** *May 12, 2015 16:29*

I rolled back to an older marlin config where I had improper steps.



 Headdesk-headdesk-headdesk.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/hgkEJ5aEYUm) &mdash; content and formatting may not be reliable*
