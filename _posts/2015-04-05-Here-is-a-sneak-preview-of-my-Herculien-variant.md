---
layout: post
title: "Here is a sneak preview of my Herculien variant...."
date: April 05, 2015 20:07
category: "Discussion"
author: Eirikur Sigbjörnsson
---
Here is a sneak preview of my Herculien variant....

![images/dfba0f163c34375fade4b9146cbe0177.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dfba0f163c34375fade4b9146cbe0177.jpeg)



**Eirikur Sigbjörnsson**

---
---
**Chris Brent** *April 05, 2015 21:54*

What did you vary?


---
**Eirikur Sigbjörnsson** *April 05, 2015 22:14*

It has bigger print volume (estimation atm is 425x425x500mm) the motors are on the inside of the frame


---
**Eric Lien** *April 05, 2015 23:13*

Looks great. Can't wait to see more.﻿


---
**Eric Lien** *April 05, 2015 23:20*

FYI, it looks like the upper lead screw supports are upside down.


---
**Eirikur Sigbjörnsson** *April 06, 2015 00:01*

yeps I know :) - some minimal adjustments still going on. but the basic concepts seem to work. Need to place the endstops for x-y, and some other smaller parts.I'm working on the electrical compartment atm. Still long way from starting to print though :)


---
*Imported from [Google+](https://plus.google.com/118262882256504121671/posts/83iZVCoY7mG) &mdash; content and formatting may not be reliable*
