---
layout: post
title: "I've got some new pics from our build session last night"
date: July 15, 2015 08:04
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
I've got some new pics from our build session last night. We're getting close. Some cabling works to do and still some things to find out. I still don't know how i wanted to mount the Bondtech extruder. The printable V-Mount that **+Martin Bondéus** offers to print works well but mounted at the right front corner would make the motor point to the front which is ugly and impractible here. Maybe i have to draw myself a custom mount.



Another week or so and it should be ready for the first moves. Still keen how it will run as hand-movement is not so smooth currently. Will see how it runs then.



![images/dc9f0c790e0c78a85426bb93b0bf9151.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dc9f0c790e0c78a85426bb93b0bf9151.jpeg)
![images/b193071861f22152ab5205c053d17996.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b193071861f22152ab5205c053d17996.jpeg)
![images/1b4bd0561a7d66d695643c14ee460545.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1b4bd0561a7d66d695643c14ee460545.jpeg)
![images/a734e149c78164cc605954fb88bbd161.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a734e149c78164cc605954fb88bbd161.jpeg)
![images/8c5cd84036c8410fa7dfff7413be1bd2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8c5cd84036c8410fa7dfff7413be1bd2.jpeg)
![images/6f542a9f3cf431ec76ba71ae26df950c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6f542a9f3cf431ec76ba71ae26df950c.jpeg)
![images/e64f3404c8f33b55d99a912612fdd981.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e64f3404c8f33b55d99a912612fdd981.jpeg)

**Frank “Helmi” Helmschrott**

---
---
**Gus Montoya** *July 15, 2015 09:05*

Wow, looks like it's ready to go. Let her rip already haha :)


---
**Eric Lien** *July 15, 2015 10:34*

You can spin the extruder. just take the tab out of the indexing teeth in the mount.﻿ That index is to hold it stiff when in direct drive mode held vertically on a mount.


---
**Eric Lien** *July 15, 2015 10:38*

So exciting seeing so many of these coming together. Great work!


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/PvkXE7dPH4M) &mdash; content and formatting may not be reliable*
