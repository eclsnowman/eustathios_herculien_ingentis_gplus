---
layout: post
title: "So, as some of you predicted, my Z-axis is giving me hell"
date: July 27, 2014 04:46
category: "Deviations from Norm"
author: Erik Scott
---
So, as some of you predicted, my Z-axis is giving me hell. The lead screws will randomly bind up at certain places along the screw and the steppers will make a god-awful noise. Sometimes, once the motors stop, it'll unbind enough to travel again if I tell it to. Other times, it's just stuck. I tried shortening the center aluminum extrusion on the bed assembly as I determined it was a bit long for the spacing of the 2 z-steppers, but it only helped a bit. It still gets stuck now and then. I've gotten it to home maybe twice without it binding up. 



You can see in the pics that I have belted the 2 steppers together with a tensioner in the middle. this helps A LOT as one stepper will help the other out if it's encountering too much resistance. The tensioner is just 2 idlers and a timing belt pulley that's free to spin. It takes out the slack in the belt, as I was unable to find any smaller ones that fit. 



Any ideas on how to salvage it this design? I'm really close to just throwing in the towel and going with **+Jason Smith** 's original design. It's really frustrating.  



(On a more positiove note, the rest of the printer appears to work perfectly!) 



![images/f51dfb29d8968eda76bdb21131b087fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f51dfb29d8968eda76bdb21131b087fa.jpeg)
![images/2f67a4e9b481b74e776a5e37a84cc50c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2f67a4e9b481b74e776a5e37a84cc50c.jpeg)
![images/5c7d3d21b76477b72dae136470a38f5a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5c7d3d21b76477b72dae136470a38f5a.jpeg)
![images/f8521097c5d7c99bd1a7ef5c8041ec83.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f8521097c5d7c99bd1a7ef5c8041ec83.jpeg)
![images/6f9dd219f9899bffacdfb5ee08a682ed.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6f9dd219f9899bffacdfb5ee08a682ed.jpeg)

**Erik Scott**

---
---
**ThantiK** *July 27, 2014 04:57*

Quit over-constraining the bed for a start.  You don't need 4 parallel rods.  Get rid of two of them, one from each side, diagonal from one another.



Also, don't trap the nuts on the screws so tightly.  They need room to wiggle a little.


---
**Eric Lien** *July 27, 2014 05:06*

I agree aligning 4 rods vertically will be a brute. Tolerances of the printed z bearing brackets would have to be perfect because you are stacking 4 tolerances at least. It could be done, but I don't think its worth it. 



I can reef on my bed with zero motion and it only has 2 rods. I print daily and haven't re-leveled on over a month.


---
**Erik Scott** *July 27, 2014 05:19*

It does help slightly if I let the top ends of the smooth rods float completely freely. However, the resistance that's causing the issue is not in the linear bearings. The bed moves up and down very nicely without the screws in place. The resistance has to do with the screw-nut interface. The resistance changes based on the rotation of the stepper and the position of the screw the nut is currently at. When I go to check this by just spinning it with my fingers, sometimes it's really easy to spin. Other times, it's really hard. 



I'm looking at prices of the original design, and can't find MTSRA12-410-S56-Q8 on misumi. I can find RMTSRA12-410-S56-Q8, however. Is there a difference?


---
**Eric Lien** *July 27, 2014 05:44*

I don't know. My invoice from them says the mstra12.


---
**Eric Lien** *July 27, 2014 05:50*

**+Erik Scott** if the bed truly moves easy less the screw then work on allowing slop on the nut x/y so it can decouple except in z. ﻿


---
**Jean-Francois Couture** *July 27, 2014 13:16*

i'm sure that if you look at the treaded rods while they're turning, the axes in witch they turn will not be bang on.. look for a anti-backlash nut method.. easy to construct.



my 0.02$ :)


---
**Dale Dunn** *July 27, 2014 18:24*

Redesign the nut brackets so they float in the xy plane. Don't allow the nut to turn as it floats,  or you'll get artifacts in the print from the tiny changes in z. The acrylic (?) Plates you already have look like they should be able to carry the z axis load, you just need a way for the nut to float between them. 


---
**James Rivera** *July 27, 2014 20:02*

**+Erik Scott** when I added the Super-Z mod to my  #Printrbot   (height increase to ~23 inches) I found that the design to constrain the top of the threaded rods was a poor choice because the el cheapo non-trapezoidal imperial threaded rods they shipped me were <b>far</b> from straight (the banana shape was blatantly obvious). I allowed the tops to move freely and that fixed the side to side ribbing I was seeing (smooth rods are straight, but over such a long height they can still be forced to flex a bit by such poor threaded rods).



However, the smooth rods do have a high straightness tolerance so I did <b>not</b> allow them to move (as you have done) because doing so would defeat the purpose of having them in the first place--keeping the X/Y in place while the threaded rod adjusts the Z.



So, if allowing the smooth rod tops to wiggle reduces your binding problem, then I submit that either A) your threaded rods are not straight, or B) your smooth rods (which are likely very straight) are not properly aligned. I would also second **+Eric Lien**'s remark about allowing the nut to wiggle a little in the X/Y because the smooth rods should be keeping the platform fixed in that plane.


---
**Erik Scott** *July 27, 2014 21:15*

My threaded rods are unconstrained at the top. I am very aware of why you shouldn't do that. 



I'm not sure how I'd make the nut float, but I think the spring I have below the nut may be too strong and not slowing the but to tilt with the curve of the threaded rod. I'm going to give that a shot first. 


---
**Eric Lien** *July 27, 2014 21:32*

**+Erik Scott** you can do just x by making the nut trap a slot along x. Then align the screw in y by making the mount shorter and placing it in the correct location along the bed extrusion.﻿


---
**Stephanie A** *July 27, 2014 21:56*

It would be possible to make a square nut, double height, that fits into a slot for x and y.

Or you can use a thinner screw which allows for flex. 

There is also a fitting called an oldham coupling, which allows x and y travel while rotating. If you axis slides without resistance up and down (read: smooth and nearly frictionless) and the nut can move without resistance on the screw alone, then the oldham coupling is what you need. 


---
**Jason Smith (Birds Of Paradise FPV)** *July 28, 2014 02:21*

Admittedly, I had to "break in" my z axis when I first assembled my printer my lead screw would occasionally bind similar to what you describe, but after oiling and realigning the screws, and running an hour of g code to move the bed up and down, it became silky smooth.  


---
**Eric Lien** *July 28, 2014 02:24*

**+Jason Smith** same here. Superlube on amazon and a similar gcode did the trick for me. 


---
**Erik Scott** *July 28, 2014 05:38*

Haha, ya, I've been oiling the hell out of it. 



I took out the springs and I get no binding whatsoever. Unfortunately, this also causes the nuts to occasionally ride down in the hex channel I have for them and not actually lower the bed due to slight resistance in the linear bearings. I added some lighter springs that barely exert any force when the nut is in the ideal position, and I once again got problems. Sometimes it will bind and sometimes the nut will just ride down compressing the spring until the linear bearings decide not to hold it anymore. 



So basically, I needed to give the nut room to rock a bit to account for the threaded rod being not quite straight. Fixing this, however, isn't that easy... Very frustrating. 



I think I'm going to be going with your original design, **+Jason Smith** . I can't find the exact lead screw you specified in your BOM. Would you be able to point me to an equivalent one by any chance? 


---
**Jason Smith (Birds Of Paradise FPV)** *July 28, 2014 11:22*

I'll look for an equivalent lead screw tonight. Have you thought about trying a regular lead screw nut without a spring?


---
**Erik Scott** *July 28, 2014 11:34*

Don't worry about the leadscrew replacement, I found one after a bit more searching. Might be worth updating the BoM for others, though. 



I'll have a look at some leadscrew nuts, see if it makes a difference. 


---
**karabas3** *July 29, 2014 18:42*

1.1 use serial Z motors wireing

1.2 if already done, low down Z speed below 180mm/min

2 use Z-isolators


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/JXmjUpqcThX) &mdash; content and formatting may not be reliable*
