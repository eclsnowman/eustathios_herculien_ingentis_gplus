---
layout: post
title: "Latest prints from HercuLien. First was a car for my boys to test the centrifugal blowers on small features in PLA at medium speeds"
date: August 30, 2014 00:02
category: "Show and Tell"
author: Eric Lien
---
Latest prints from HercuLien. First was a car for my boys to test the centrifugal blowers on small features in PLA at medium speeds. Then some Lego's to test tolerances. And now I have a larger piece for work. It is the cope half of a foundry pattern with the riser printed integrally. 



![images/095eb65ab43a9cb91650083a9aaf5715.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/095eb65ab43a9cb91650083a9aaf5715.jpeg)
![images/3de5ea649ad5dc668a81c89685911e9b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3de5ea649ad5dc668a81c89685911e9b.jpeg)
![images/407a833b0e29771e613c6f55d4accbb3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/407a833b0e29771e613c6f55d4accbb3.jpeg)
![images/fdb06894cce542549a4499b7a92db380.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fdb06894cce542549a4499b7a92db380.jpeg)
![images/d0ddc7f12eebbb71f80ae8e85c311f1a.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d0ddc7f12eebbb71f80ae8e85c311f1a.png)

**Video content missing for image https://lh5.googleusercontent.com/-M9snePllpII/VAEUnt5t02I/AAAAAAAAjK0/o6LidCbQgjc/s0/VID_20140829_175909.mp4.gif**
![images/3d31203e4887f3122ff06e9ba57434c2.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3d31203e4887f3122ff06e9ba57434c2.gif)

**Video content missing for image https://lh4.googleusercontent.com/-OEPgarVTMFo/VAEUnsTYr3I/AAAAAAAAjK0/2mdNbZVON80/s0/VID_20140828_235249.mp4.gif**
![images/28648741e8a9f79323c65e71c0528141.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/28648741e8a9f79323c65e71c0528141.gif)

**Eric Lien**

---
---
**James Rivera** *August 30, 2014 00:54*

**+Eric Lien** The car looks a little "blobby", but the lego pieces aren't too bad.  Do you think there would be any benefit from ducting the fan exit to be closer to the tip of the hot end (or just below it), or do you think there is just some calibration tweaking to be done? (e.g. extrusion multiplier)


---
**Eric Lien** *August 30, 2014 02:08*

**+James Rivera** I think retract distance and temp are the big factors here. Plus the blobs are on 45deg overhangs and very small. 



Ducting could also help. 



FYI Lego's were printed at 100mm/s 10000mm/s2 acceleration.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/fNKaf54YS8G) &mdash; content and formatting may not be reliable*
