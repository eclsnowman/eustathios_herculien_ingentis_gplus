---
layout: post
title: "can anybody point me to a reliable source of dyneema 1mm braided fishing line in the uk ?"
date: February 08, 2016 08:33
category: "Discussion"
author: ekaggrat singh kalsi
---
can anybody point me to a reliable source of dyneema 1mm braided fishing line in the uk ? thanks





**ekaggrat singh kalsi**

---
---
**Tomek Brzezinski** *February 08, 2016 14:01*

You explicitly asked for a UK reliable source, but I'm just noting I've had great luck with ebay sellers.  



If you really are looking for a domestic source, some of the ebay sellers will be domestic, but the best bet is to look for deep sea fishing suppliers, since it's used for very big fish. 


---
**ekaggrat singh kalsi** *February 08, 2016 15:11*

What i am looking for is the genuine source and not the chinese knock offs rampanent on ebay. 


---
**Tomek Brzezinski** *February 08, 2016 15:43*

Do you have a particular need for that? I have found the knockoffs of good quality.  I go up slightly in size to be cautious, in case there's a higher rate of defects in the knockoff stuff, but eitherway I probably have something stiffer and with a higher yield point than a slightly thinner "genuine" cord. 



Anyway like I said I'd look into fishing suppliers for genuine UK sources. You can also consider kevlar kite line.  It's for kite-boarding, very high quality. 



As an individual I'm very comfortable going with the ebay stuff because I can quickly tell if I got egregiously bad product. If I was trying to assemble a lot of machines, maybe then I would start tightening my supply sources.


---
**ekaggrat singh kalsi** *February 08, 2016 15:54*

I sourced a few knockoffs but they were a bit more stretchy than necessary. I am building a gus simpson and need the strings to be non stretchy . 


---
**Chris Brent** *February 08, 2016 17:45*

Most kite lines are spectra/dyneema fibres (trade names for UHMWPE made into fibres). They're usually thicker than 1mm though, unless you find "race" lines. Then they'll be much more expensive than fishing line. If you're line is too stretchy try pre tensioning a long piece.Spectra/Dyneema doesn't stretch (and even the "knockoffs" are usually UHMWPE if you melt it, it has a distinct smell) much but it does creep, usually because of the braid. If you load it up you should be able to take a lot of that out. Try and find a 12 strand or 16 strand weave to deal with that.

The other source is fishing line. In my experience the kite line is higher tolerance, but fishing line comes in a lot more weave/lb/diameter sizing.

Wow, things I learnt making kites that I never though I would use for a 3D printer discussion ;P


---
*Imported from [Google+](https://plus.google.com/+ekaggrat/posts/3qRmmwH9Z6m) &mdash; content and formatting may not be reliable*
