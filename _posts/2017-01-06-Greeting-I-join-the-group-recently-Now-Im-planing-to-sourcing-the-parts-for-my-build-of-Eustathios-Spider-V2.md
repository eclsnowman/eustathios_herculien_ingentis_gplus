---
layout: post
title: "Greeting, I join the group recently. Now I'm planing to sourcing the parts for my build of Eustathios Spider V2"
date: January 06, 2017 03:11
category: "Discussion"
author: larry huang
---
Greeting, I join the group recently. Now I'm planing to sourcing the parts for my build of Eustathios Spider V2. While my build will stick to the plan most of the parts, I wonder if upgrade the Z Axis timing belt to 2GT-10mm will gain any advantage? Or is it just a waste to upgrade it? Thanks.





**larry huang**

---
---
**Stefano Pagani (Stef_FPV)** *January 06, 2017 22:07*

If you need a Mc Master project order for all the hardware sourced through it, let me know. I can provide part lists for all the sellers I used, but I'm sure you can find cheeper, esp if you order all the screws from a cheaper source. Be sure to get at least 15 drop in t nuts! You'll thank me later :P


---
**Stefano Pagani (Stef_FPV)** *January 06, 2017 22:08*

Also, I would suggest doing **+Walter Hsiao**'s ballscrew, bed and carriage upgrade. 


---
**larry huang** *January 07, 2017 06:01*

**+Stefano Pagani** Thank you for the info. And yes, I plan to use 1204 ball screw.


---
*Imported from [Google+](https://plus.google.com/104789216831460398100/posts/FrPYvsvxZKs) &mdash; content and formatting may not be reliable*
