---
layout: post
title: "Printing parts to my colleague, is it the first Herculien in Sweden?"
date: May 04, 2015 21:12
category: "Show and Tell"
author: Martin Bondéus
---
Printing parts to my colleague, is it the first Herculien in Sweden?

![images/63b5a1ad163668d5fb64ae41fdb68bd4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/63b5a1ad163668d5fb64ae41fdb68bd4.jpeg)



**Martin Bondéus**

---
---
**Gus Montoya** *May 04, 2015 22:14*

I like this, how the Lein printer spreads for the good of community. :)


---
**Sébastien Plante** *May 04, 2015 22:15*

you must have an awesome extruder to give a great quality print like that! 


---
**Joe Spanier** *May 04, 2015 22:22*

Might be the prettiest


---
**Eric Lien** *May 04, 2015 22:29*

Looking very nice Martin. I am excited to see another one take shape. I like that color green. I assume this is for **+Mikael Sjöberg**​ ?



By the way, any progress on your corexy? I can't wait to see peoples reactions when you show it. It is a remarkable printer, and SO FAST.


---
**Joe Spanier** *May 04, 2015 22:34*

I for one would like to see it


---
**Jo Miller** *May 04, 2015 22:54*

looks nice , by the way, when will you open your shop ?


---
**Mikael Sjöberg** *May 05, 2015 04:09*

Yes , and Im very exited. The aluminium bed has been made and I have the Z-axis+nuts at manufacturing, VSlot wheels , hardened X & y rods, T -slots on its way. Exited :)


---
**Martin Bondéus** *May 05, 2015 04:52*

**+Jo Miller** the shop is open.


---
**Martin Bondéus** *May 05, 2015 04:56*

**+Eric Lien** I got some time during this weekend to work with it, I have had an issue with binding when the belts were tightened but I think I have came up with a solution for that now. 


---
**Mikael Sjöberg** *May 05, 2015 18:49*

Thanks Martin Bondeus for the excellent parts !

I will post some pictures When I start the assembling 


---
*Imported from [Google+](https://plus.google.com/+MartinBondéus-Bondtech/posts/YCYZfBV895a) &mdash; content and formatting may not be reliable*
