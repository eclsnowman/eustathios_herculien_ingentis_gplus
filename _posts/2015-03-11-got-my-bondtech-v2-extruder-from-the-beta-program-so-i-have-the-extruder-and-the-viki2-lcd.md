---
layout: post
title: "got my bondtech v2 extruder from the beta program :) so i have the extruder, and the viki2 lcd"
date: March 11, 2015 15:19
category: "Discussion"
author: Seth Messer
---
got my bondtech v2 extruder from the beta program :)



so i have the extruder, and the viki2 lcd. i'm practically printing stuff already. oh, and HUGE thanks **+Eric Lien** for the Eustathios V2 updates. you've made a ton of progress. Hope you and the family are feeling better from the recent funk (been going around down here in Alabama too).



Update, my sheet of PEI from amazon is due for Friday! It's like Christmas... in March. :D





**Seth Messer**

---
---
**Eric Lien** *March 11, 2015 15:42*

I am getting close. But I am going through and verifying parts and part numbers. I don't want people ordering the wrong stuff because I made a type on my file name that translated into the BOM. Also I need to do my conversions out to STL's and other formats before I release it Officially.


---
**Joe Spanier** *March 11, 2015 16:12*

Did you get a shipping notice on your bondtech? Just wondering if I missed an email or if mine just hasnt shipped.


---
**Seth Messer** *March 11, 2015 16:14*

**+Joe Spanier** I did get a FedEx tracking number once it was submitted to be shipped. In case you weren't getting his update emails, basically there were some delays and he's been getting stuff out in the order it was purchased. Hopefully you get that shipping notice soon!


---
**Eric Lien** *March 11, 2015 16:22*

**+Joe Spanier** I believe he just received today the stepper motors which got delayed by the Chinese new year. I think he plans to get the remaining orders shipped ASAP.


---
**Martin Bondéus** *March 11, 2015 16:39*

Hi, that is correct, I am doing my very best to pack & book shipping as fast as possible.


---
**Seth Messer** *March 11, 2015 16:42*

thanks so much **+Martin Bondéus** you're doing an awesome job. can't wait to get everything else printed/purchased and start using this little gem.


---
**Ben Delarre** *March 11, 2015 16:43*

Yes thanks **+Eric Lien**​ for your work on v2. I will be implementing the z stage from that with the rest of the design from v1. For a couple of reasons,  not least of which being I had already ordered the v1 belts! I do like having a square unit too,  will make it easier to fit into a shelf. 


---
**Joe Spanier** *March 11, 2015 17:47*

I was just curious. I missed my shipping notification for my Cyclops from Filastruder and emailed Tim. His reply was to go get my mail because it was delivered haha.


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/hhF32RDZ2hM) &mdash; content and formatting may not be reliable*
