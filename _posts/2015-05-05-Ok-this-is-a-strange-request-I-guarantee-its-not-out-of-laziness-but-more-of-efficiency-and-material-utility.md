---
layout: post
title: "Ok this is a strange request. I guarantee it's not out of laziness but more of efficiency and material utility"
date: May 05, 2015 02:56
category: "Discussion"
author: Gus Montoya
---
Ok this is a strange request. I guarantee it's not out of laziness but more of efficiency and material utility.

 Is anyone willing to resell some hardware ? I'm sure some will not have use of 100 pack of Black-Oxide Class 12.9 Socket Head Cap Screw M3 Thread , 25mm L, 0.50 pitch (as an example). I can always order from Mr Metric or Tim craft aviation RC. 



[https://www.dropbox.com/s/8t7cmxcs437k7wr/DocMcMaster%20list.pdf?dl=0](https://www.dropbox.com/s/8t7cmxcs437k7wr/DocMcMaster%20list.pdf?dl=0)





**Gus Montoya**

---
---
**Brandon Cramer** *May 05, 2015 03:16*

Check out bolt depot. You can order by piece instead of 50 or 100 packs. They might not have everything you need but they should have a descent amount of it.


---
**Gus Montoya** *May 05, 2015 03:22*

I'll check it out, Tim craft aviation RC has the best prices that I've seen. But more interested in getting hardware off peoples hands that they won't use. Trying to use the community to increase material utility.


---
**Mike Thornbury** *May 05, 2015 03:36*

I tend to find that anything not used on THIS project, will be used on THAT one.



I still have bolts and fittings and screws I reclaimed in 1975.


---
**Gus Montoya** *May 05, 2015 06:33*

I finished my order, using 3 different venders. I squeezed every penny out of the order. I hope I am not disappointed what I get. Let the parts rain in. :)  Documenting assembly is going to take some time.


---
**Derek Schuetz** *May 07, 2015 20:47*

how did you get it to only 3 vendors?


---
**Gus Montoya** *May 08, 2015 07:57*

I price hunted, had to see item was cheapest to each vender. I also took shipping into concideratiom.


---
**Gus Montoya** *May 08, 2015 23:56*

**+Derek Schuetz** I should add that the 3 venders are just for the hardware specifically nuts and bolts. I still had to go with other venders for electroncis etc.


---
**Derek Schuetz** *May 09, 2015 00:11*

Oh ok that makes more sense was that really cheaper then just going all through McMaster? I totaled it all up on there and it was about $200 plus shipping luckily I had about $70 in leftover nuts and bolts 


---
**Gus Montoya** *May 09, 2015 02:08*

It was much cheaper. I think I saved more then $100. 


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/b95QVfRfhGQ) &mdash; content and formatting may not be reliable*
