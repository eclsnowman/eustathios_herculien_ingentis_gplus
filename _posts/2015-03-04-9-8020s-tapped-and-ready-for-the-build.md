---
layout: post
title: "9 8020's tapped and ready for the build"
date: March 04, 2015 04:00
category: "Show and Tell"
author: Bruce Lunde
---
9 8020's tapped and ready for the build. This is getting real now!

![images/f4d875447f6c4f8bb838cc1c54cc4e41.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f4d875447f6c4f8bb838cc1c54cc4e41.jpeg)



**Bruce Lunde**

---
---
**Gus Montoya** *March 04, 2015 04:26*

That looks as real as it's ever going to be :)


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/CjsfhFuHKPi) &mdash; content and formatting may not be reliable*
