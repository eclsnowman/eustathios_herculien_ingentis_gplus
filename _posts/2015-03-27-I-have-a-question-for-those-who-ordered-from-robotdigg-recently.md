---
layout: post
title: "I have a question for those who ordered from robotdigg recently?"
date: March 27, 2015 15:50
category: "Discussion"
author: Mykyta Yurtyn
---
I have a question for those who ordered from robotdigg recently? How long did it take you to get your order (in US)? Did you pay standard Air Mail shipping or more expensive FedEx/UPS? 





**Mykyta Yurtyn**

---
---
**Dat Chu** *March 27, 2015 15:52*

FedEx. Cost me 70 or so. Got it in a week. 


---
**Bruce Lunde** *March 27, 2015 16:16*

I paid regular shipping, got it in 7 days


---
**Isaac Arciaga** *March 27, 2015 16:16*

UPS for a lot of tnuts. It was $30 got it in less than a week in SoCal. It was still much cheaper than what I would've spent at Misumi.

If it's a small, light (in weight) order then expedited shipping might not be worth it. Standard shipping was 15 days for me. Still faster than most places on AliExpress AND with direct contact via email or Skype.


---
**Dat Chu** *March 27, 2015 17:28*

Very fast shipment. And the packaging is great. I feel like someone can drop that package and everything will still be in tact.


---
**Seth Messer** *March 27, 2015 19:57*

Any of you have troubles searching/finding the part numbers listed in the BOM (on [robotdigg.com](http://robotdigg.com))?


---
**Mykyta Yurtyn** *March 27, 2015 21:05*

yeah, robotdigg have a good opportunity to improve their search feature... google helped "site:[robotdigg.com](http://robotdigg.com) part-i-need"


---
**Jason Smith (Birds Of Paradise FPV)** *March 28, 2015 01:03*

It's normally pretty fast, but Chinese New Year has slowed down a lot of shipments from China lately. 


---
**Dat Chu** *March 28, 2015 01:16*

I can't seem to access [robotdigg.com](http://robotdigg.com) anymore :(


---
**Seth Messer** *March 28, 2015 01:25*

I'm presently on it **+Dat Chu**. In fact, I was about to post up and ask which microswitch you guys got, this is the one I was thinking we were supposed to get: [http://www.robotdigg.com/product/154/Microswitch-Board-w/-Lead-Wires-for-3D-Printers](http://www.robotdigg.com/product/154/Microswitch-Board-w/-Lead-Wires-for-3D-Printers)


---
**Dat Chu** *March 28, 2015 01:26*

Something is wrong with my internet DNS then :( Hmm.


---
*Imported from [Google+](https://plus.google.com/103799104844197134662/posts/iuFsGSZ1HP5) &mdash; content and formatting may not be reliable*
