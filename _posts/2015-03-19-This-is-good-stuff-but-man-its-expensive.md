---
layout: post
title: "This is good stuff, but man it's expensive"
date: March 19, 2015 02:50
category: "Discussion"
author: Gus Montoya
---
This is good stuff, but man it's expensive.



[https://www.kickstarter.com/projects/879356311/filamet-the-metallic-printer-filament-for-artists/description](https://www.kickstarter.com/projects/879356311/filamet-the-metallic-printer-filament-for-artists/description)





**Gus Montoya**

---
---
**Jeff DeMaagd** *March 19, 2015 22:56*

If it's real that good, maybe it's worth it. But supporting it seems a gamble.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/9t6otHiE1Fg) &mdash; content and formatting may not be reliable*
