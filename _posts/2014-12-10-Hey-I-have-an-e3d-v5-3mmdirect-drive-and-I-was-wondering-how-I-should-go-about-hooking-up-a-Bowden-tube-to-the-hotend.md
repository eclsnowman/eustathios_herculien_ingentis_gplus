---
layout: post
title: "Hey! I have an e3d v5 3mm(direct drive) and I was wondering how I should go about hooking up a Bowden tube to the hotend"
date: December 10, 2014 02:53
category: "Show and Tell"
author: Ethan Hall
---
Hey! I have an e3d v5 3mm(direct drive) and I was wondering how I should go about hooking up a Bowden tube to the hotend. I was also wondering your favorite supplier of Bowden tube.

I plane to one day upgrade to the cyclops but that's pretty low on the priority list at the moment.





**Ethan Hall**

---
---
**Eric Lien** *December 10, 2014 03:09*

[http://www.robotdigg.com/product/134/PTFE-Tube-10-Meters-ID4mm-OD6mm-for-3mm-Filament](http://www.robotdigg.com/product/134/PTFE-Tube-10-Meters-ID4mm-OD6mm-for-3mm-Filament)


---
**Eric Lien** *December 10, 2014 03:10*

[http://www.smw3d.com/3-0mm-bowden-tubing/](http://www.smw3d.com/3-0mm-bowden-tubing/)


---
**Eric Lien** *December 10, 2014 03:14*

I think there may be adapters out there, or make your own. Use a nut around the tube to allow you to retain the tube.﻿


---
**D Rob** *December 10, 2014 03:19*

Those are links to 2mm id. he is using 3mm filament


---
**D Rob** *December 10, 2014 03:20*

I buy from e3d they have really good PTFE tubing. Thick walled.


---
**Lynn Roth** *December 10, 2014 03:22*

The tubes that Eric listed are for 1.75mm filament.  I used tube from Mcmaster Carr - part # 5033K31   [http://www.mcmaster.com/#catalog/120/144/=uyfisl](http://www.mcmaster.com/#catalog/120/144/=uyfisl)  1/8" ID, which seems to work well for me.  


---
**Eric Lien** *December 10, 2014 04:08*

**+D Rob** links corrected above for 3mm.


---
**Brandon Satterfield** *December 10, 2014 04:26*

The tube at SMW3D is from E3D. Couldn't agree more +D Rob it is a thicker wall, and fits perfect in the Bowden connectors. I threw away a number of things I ordered to compare. Thanks Eric.



There is an adapter last I searched this out but seems there was a drawback, can't recall what it was.



**+Ethan Hall** Why not move to the V6? It is the season of giving.. Give your printress something awesome! Haha 


---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/GMee23W3CBp) &mdash; content and formatting may not be reliable*
