---
layout: post
title: "Why does this keep happening? Mid print, it just stops printing and I can't push any filament in through the hotend"
date: July 07, 2016 18:30
category: "Discussion"
author: Brandon Cramer
---
Why does this keep happening? Mid print, it just stops printing and I can't push any filament in through the hotend. The hotend shows that it's 230 some degrees for Hatchbox PLA. 

![images/422bbb1bc0b550a9f9d18f2c7042471d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/422bbb1bc0b550a9f9d18f2c7042471d.jpeg)



**Brandon Cramer**

---
---
**Eric Lien** *July 07, 2016 18:40*

Sometimes with PLA going too slow can be a problem on all metal hot ends. Can you put some of your settings to help us troubleshoot?




---
**Botio Kuo** *July 07, 2016 18:45*

My setting for hatchbox pla is 210. and printing very well. maybe you can try it.


---
**Ray Kholodovsky (Cohesion3D)** *July 07, 2016 18:54*

Any cold extrusion prevented error show up on the controller? Hoping the hotend didn't fail to keep up temperature. 


---
**Brandon Cramer** *July 07, 2016 18:55*

[filament:Hatchbox Filament]

bed_temperature = 0

bridge_fan_speed = 100

cooling = 0

disable_fan_first_layers = 1

extrusion_multiplier = 1

fan_always_on = 1

fan_below_layer_time = 60

filament_colour = #FF0000

filament_diameter = 1.75

first_layer_bed_temperature = 0

first_layer_temperature = 235

max_fan_speed = 100

min_fan_speed = 40

min_print_speed = 20

slowdown_below_layer_time = 20

temperature = 230



[print:50 mms Speed 60% Infill W Brim]

avoid_crossing_perimeters = 0

bottom_solid_layers = 6

bridge_acceleration = 0

bridge_flow_ratio = 1

bridge_speed = 50

brim_width = 5

complete_objects = 0

default_acceleration = 0

dont_support_bridges = 1

external_fill_pattern = rectilinear

external_perimeter_extrusion_width = 0

external_perimeter_speed = 50

external_perimeters_first = 0

extra_perimeters = 0

extruder_clearance_height = 20

extruder_clearance_radius = 20

extrusion_width = 0.4445

fill_angle = 45

fill_density = 60%

fill_pattern = rectilinear

first_layer_acceleration = 0

first_layer_extrusion_width = 200%

first_layer_height = 0.25

first_layer_speed = 50%

gap_fill_speed = 50

gcode_comments = 0

infill_acceleration = 0

infill_every_layers = 1

infill_extruder = 1

infill_extrusion_width = 0

infill_first = 0

infill_only_where_needed = 0

infill_overlap = 15%

infill_speed = 50

interface_shells = 0

layer_height = 0.2

max_print_speed = 80

max_volumetric_speed = 0

min_skirt_length = 0

notes = 

only_retract_when_crossing_perimeters = 1

ooze_prevention = 0

output_filename_format = [input_filename_base].gcode

overhangs = 0

perimeter_acceleration = 0

perimeter_extruder = 1

perimeter_extrusion_width = 0

perimeter_speed = 50

perimeters = 2

post_process = 

raft_layers = 0

resolution = 0

seam_position = aligned

skirt_distance = 2

skirt_height = 1

skirts = 3

small_perimeter_speed = 50

solid_infill_below_area = 20

solid_infill_every_layers = 0

solid_infill_extruder = 1

solid_infill_extrusion_width = 0

solid_infill_speed = 50

spiral_vase = 0

standby_temperature_delta = -5

support_material = 0

support_material_angle = 45

support_material_contact_distance = 0.2

support_material_enforce_layers = 0

support_material_extruder = 1

support_material_extrusion_width = 0

support_material_interface_extruder = 1

support_material_interface_layers = 0

support_material_interface_spacing = 3

support_material_interface_speed = 100%

support_material_pattern = rectilinear

support_material_spacing = 3

support_material_speed = 50

support_material_threshold = 0

thin_walls = 1

threads = 4

top_infill_extrusion_width = 0

top_solid_infill_speed = 50

top_solid_layers = 6

travel_speed = 50

xy_size_compensation = 0



[printer:Eustathios Spider V2]

bed_shape = 0x0,285x0,285x285,0x285

before_layer_gcode = 

end_gcode = M104 S0 ; turn off temperature\nG28 X0  ; home X axis\ng28 y0  ; home y axis\nM84     ; disable motors

extruder_offset = 0x0

gcode_flavor = reprap

layer_gcode = 

nozzle_diameter = 0.4

octoprint_apikey = 

octoprint_host = 

pressure_advance = 0

retract_before_travel = 2

retract_layer_change = 1

retract_length = 8

retract_length_toolchange = 0

retract_lift = 0

retract_restart_extra = 0

retract_restart_extra_toolchange = 0

retract_speed = 30

start_gcode = G28 X0; Home X\nG28 Y0; Home Y\nG28 Z0; Home Z

toolchange_gcode = 

use_firmware_retraction = 0

use_relative_e_distances = 0

use_volumetric_e = 0

vibration_limit = 0

wipe = 0

z_offset = 0


---
**Eric Lien** *July 07, 2016 19:17*

Nothing jumped out at me other than perhaps an 8mm retraction which seems high for PLA. But I would say your speed looks fine so you are not going too slow. I would try shorter retraction, faster retraction speed, and going hotter and see if it still occurs. Also I recommend a nylon cold pull to clear out the nozzle just for good measure.


---
**Jason Smith (Birds Of Paradise FPV)** *July 07, 2016 19:18*

PLA jams are no fun.  Perhaps 8mm might be too much retraction and could be causing hot plastic to freeze above the melt chamber.  Do you have to disassemble the hotend each time to clear the plastic?


---
**Derek Schuetz** *July 07, 2016 19:32*

I'm pointing to retraction I could never get a good retraction with plans a Jam would be imminent 


---
**Brandon Cramer** *July 07, 2016 20:34*

I lowered the retraction to 4mm and changed the retraction speed to 40mm/s. I have the temp set to 230 but while it's printing, the temp is currently 225ish. Watching the temperature curve in Repetier Host, I don't see the temperature dropping down below 220. It ranges from 223 to 230. 



When it jams, I pull the filament out of the hotend and cut a piece of it off. After that I put it back together and push some filament through. 



I ordered a new E3D V6 hotend yesterday. Might be overkill, but I've been using this existing one for quite some time. 


---
**Eric Lien** *July 07, 2016 20:59*

I think it might be time to hand tune the PID. I am not sure if it has gotten better, but the autoPID tune in smoothieware never gave me very good results. I always ended up hand tuning PID to get fast heat up, responsive change on print speedup/slow down, and stable temps.



This graphic more than any other explanation I have found explains how to properly tune PID: [https://en.wikipedia.org/wiki/File:PID_Compensation_Animated.gif](https://en.wikipedia.org/wiki/File:PID_Compensation_Animated.gif)


---
**Jo Miller** *July 08, 2016 14:41*

+ eric  thanks for that wiki/PID  link  !


---
**Mike Miller** *July 09, 2016 15:41*

You might also burn out the nozzle...I use a butane cooking torch and a pair of needle nose pliers I don't care about. 


---
**Roland Barenbrug** *July 11, 2016 15:09*

Check the fan that's attached to the heat sink. Cooling seems almost always the problem with E3D extruders that have a jam after some time of printing.  Make sure the lower part of the heatsink is well in the air flow. A new E3D extruder is probably not going to change anything (apart of a new fan that might work better).




---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/Nvo3FJ7ghmb) &mdash; content and formatting may not be reliable*
