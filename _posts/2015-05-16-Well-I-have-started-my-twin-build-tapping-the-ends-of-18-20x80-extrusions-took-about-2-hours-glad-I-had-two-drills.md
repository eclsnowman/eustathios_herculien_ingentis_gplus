---
layout: post
title: "Well I have started my twin build, tapping the ends of 18 20x80 extrusions took about 2 hours, glad I had two drills"
date: May 16, 2015 06:14
category: "Show and Tell"
author: Vic Catalasan
---
Well I have started my twin build, tapping the ends of 18 20x80 extrusions took about 2 hours, glad I had two drills. I pre-assembled it without making any holes since I have yet to received my drill templates. Anyways probably a good idea and now I can label the pieces and place t-nuts where they should be.



 



![images/9935ef825ad57dacb8486e0a55a857cf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9935ef825ad57dacb8486e0a55a857cf.jpeg)
![images/e1616424fa49a5a60837595c1caaa4bc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e1616424fa49a5a60837595c1caaa4bc.jpeg)
![images/92579571956e078dddfdf33f0d2f32b9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/92579571956e078dddfdf33f0d2f32b9.jpeg)
![images/e89799e66101f65f2fa92427851aad15.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e89799e66101f65f2fa92427851aad15.jpeg)

**Vic Catalasan**

---
---
**Eric Lien** *May 16, 2015 06:30*

Bravo. Building two at once. Now that's ambitious ;)


---
**Vic Catalasan** *May 16, 2015 07:05*

Thanks to you this is an easy build, its practically a kit!



I cant say that for the CNC machine I built a few years ago.



[https://lh3.googleusercontent.com/YhsZD8DDYmy4Wwj-WQfLqzA51uJufiLuxHgCa554Zwx6yy7I51Ir7W7QkI3KV3RezvR8ZOu0AnoO_w=s0](https://plus.google.com/photos/108399297878867262359/albums/5950248598121974433?banner=pwa)


---
**Vic Catalasan** *May 16, 2015 07:25*

I think the hardest part of this 3D printing is not making the machine but using... and getting it to work.... at least for me as this is my first 3D printer


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/7HGzrGfDJ7H) &mdash; content and formatting may not be reliable*
