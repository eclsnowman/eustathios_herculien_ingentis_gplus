---
layout: post
title: "I'm literally only a week into owning a printer at all, and I'm already thinking about my second"
date: August 05, 2017 13:23
category: "Discussion"
author: Chris Thompson
---
I'm literally only a week into owning a printer at all, and I'm already thinking about my second. 



I have the Micro Center branded version of the Wanhao Duplicator i3 plus that will, within the next day or so, have the all metal hotend to allow printing PETG. 



My next printer is going to be a Herculien with no shortcuts. 



The issue I have is that while this group is great and I've explored the github repo, I haven't yet found a good summary page for someone starting from scratch.



I'd be happy to create one if none exists, but what I'm looking for is a summary of the HercuLien, all of the possible mods, pros and cons, etc. 



What I need is a toe hold into this project so I can start planning part acquisition. Where do I start?





**Chris Thompson**

---
---
**Eric Lien** *August 05, 2017 13:48*

If you would be willing to write an intro guide, and post it to the group and GitHub... Then I will give you all the help you need. Where do you want to start?


---
**Eric Lien** *August 05, 2017 13:56*

For upgrades to HercuLien, here is my recommendations:



Side carriages by **+Zane Baird**​ in the GitHub community mods section.



Ditch the misumi leadscrews, and use the golmart ball screws, or tr8x8 leadscrews. Both are cheaper and perform better.



For controller go with X5 GT, or perhaps a duet wifi.



Use a raspberry pi and control via octoprint.



For the carriage there are several options. Dual Bowden, dual Bowden Cyclops/Chimera, single Bowden by Zane which yields much larger printable area, or Bondtech BMG direct Drive.


---
**Eric Lien** *August 05, 2017 13:58*

Power supply can be made smaller (see recently added upgrade to the GitHub to the community section). My 500W supply is back from when I still ran the heated bed at 24V.


---
**Chris Thompson** *August 05, 2017 14:22*

I'll try to undertake this, but I'll warn you that my speed might not be quick. My budget might not be up to buying all the parts at once.



I need to evaluate different carriage considerations, I love the look of the E3D Aero, but I'd prefer bowden to "Add Lightness", I also want to print TPU so direct drive seems "better" but I see people saying that bowden can be done with things like ninjaflex.



My Wanhao actually has never printed something without octoprint, it was step one. Pi Zero W with a logitech webcam and a bolt on hub the size of the pi zero and my very first calibration cube print was by dragging gcode into octo, so it's required. 



Some sort of smoothieboard is guaranteed, either the Azteeg x5 or I had a conversation with the guy behind the Cohesion3D, so I need to choose.



I'm also considering 110v silicone heating pad and an SSR.



If I'm going to do this, I want no compromise.



I'll start by getting the parts printed, the extrusions and ball screws ordered.



One question, right now, without the Micro Swiss, the only thing I can print reliably is PLA. Is there an actual difference between PLA and PETG in this setup?


---
**Chris Thompson** *August 05, 2017 14:47*

Question #2 of infinite:



I got this from the BOM: [aliexpress.com - Aliexpress.com : Buy RM1204 Ball Screw L480mm Ballscrew With SFU1204 Single Ballnut For CNC Processing length can be customized from Reliable for cnc suppliers on golmart](https://www.aliexpress.com/store/product/RM1204-Ball-Screw-L480mm-Ballscrew-With-SFU1204-Single-Ballnut-For-CNC-Processing-length-can-be-customized/920371_32612035532.html)



Is this correct for a herculien to just order as is, or do I need to interpret the chinese somehow and ask for the custom offsets in the table in the listing? 



It looks like the default order is 480mm long, stepped down to 8mm diameter for 26mm on one end, 10mm on the other. 



It mentions 3d Printer but not herculien specifically.



I'll put in the order today to get them on the way by slow boat.


---
**Eric Lien** *August 05, 2017 14:52*

Yes Cohesion3D ( **+Ray Kholodovsky** ) is awesome. I have done work with him in the past. Great guy.



Yes for the bed only go mains voltage with SSR. Heat up times and current draw at 24V is a non-starter IMHO.



For Ninjaflex I would say unless you go 3mm filament doing bowden is a no go. Nice thing about the side carriages by **+Zane Baird** is changing out the center carriage is fairly easy.



That being said my recommendation is do flexible on the i3, and do large and fast prints on the HercuLien. Trying to make a "Swiss Arm Knife" printer might mean you get a jack of all trades master of none situation. That's why I have  five now :) 




---
**Eric Lien** *August 05, 2017 14:56*

**+Chris Thompson** yes that is the correct ball screw. I had them make a custom listing for it. Only thing to mention is make sure the install the ball screw nut the correct orientation per the drawing. Or else you will need to print this nut reversing tool: 



[thingiverse.com - 1204 Ball Nut Inverter by walter](https://www.thingiverse.com/thing:809141)


---
**Chris Thompson** *August 05, 2017 15:11*

Excellent, thanks. Also looking at extrusions, found [makeralot.com - Aluminum Extrusion Kit for Herculien 2020 2080 V Slot : MAKERALOT, Maker Tools and Materials](https://www.makeralot.com/aluminum-extrusion-kit-for-herculien-2020-2080-v-slot-p215/)



Thought, I bet I can beat that by buying bulk meters and cutting my own (in the fantasy world where I can cut something to 0.5mm precision).



Yeah, not even close. Just the 2020 alone looks like $250.



I'll get some parts ordered and try to figure out my next steps.








---
**Eric Lien** *August 05, 2017 15:17*

Also note the cover extrusion is separate on the BOM from the base frame.



Also yes extrusion cut accuracy is critical for this printer. Inaccurate extrusion cuts (in length or squareness) will mean the printer will never work correctly.


---
**Chris Thompson** *August 05, 2017 15:23*

I was not anticipating $160 in shipping from makeralot. This may have to wait for the next payday.




---
**Eric Lien** *August 05, 2017 17:45*

To be honest, I might recommend using misumi for the frame parts. 


---
**Eric Lien** *August 05, 2017 17:48*

**+James Ochs**​ who did you use for your frame on your recent build?


---
**James Ochs** *August 05, 2017 18:54*

I used 80/20inc for the 20x20 extrusion and ooenbuilds for the 20x80.



Use Misumi.



The 80/20 (who oddly don't seem to carry   20x80 extrusion) extrusions have a slot that is much more narrow so that drop in nuts don't fit.  The cuts were all good, but it turned out to be slightly more expensive  than Misumi.  I had a hard time with Misumi in the past (mostly around getting setup as an individual buyer), so it wasn't my go to place.  I did wind up ordering replacements for peices I messed up through them as well as a fair bit of other hardware and didn't experience any problems, so it seems like they sorted that out.


---
**James Ochs** *August 05, 2017 18:59*

Oh, and don't do what I did and wind the ball screw nut off the screw without that tool.  Unless you enjoy chasing bearings all over your shop 


---
*Imported from [Google+](https://plus.google.com/115929464558533781010/posts/Q6fSqYXf3YV) &mdash; content and formatting may not be reliable*
