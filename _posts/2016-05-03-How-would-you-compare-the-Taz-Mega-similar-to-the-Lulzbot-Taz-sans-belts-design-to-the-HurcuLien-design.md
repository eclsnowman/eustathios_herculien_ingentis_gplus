---
layout: post
title: "How would you compare the Taz-Mega (similar to the Lulzbot Taz sans belts) design to the HurcuLien design"
date: May 03, 2016 15:17
category: "Discussion"
author: Robert Clayton
---
How would you compare the Taz-Mega (similar to the Lulzbot Taz sans belts) design to the HurcuLien design. Other than the increased cost due to not being belt driven might it be more accurate or possibly allow for heavier heads, etc? An improved bed design would be needed.

﻿





**Robert Clayton**

---
---
**Tomek Brzezinski** *May 03, 2016 16:26*

I would be very curious to know the speed limits on the X and Y axis. Long lengths of rod like that which isn't too thick, potentially have serious critical speed limits.  They start resonating and wobbling like crazy when you move fast. Granted, you probably can't move fast because they're big and heavy.  



Also, I really dislike seeing those helical Z-couplers.  They have axial stretch. You don't want axial stretch for a positioning device. Everyone should be using spider couplings / lovejoy / other better forgiving but not inaccurate couplers.


---
**Tomek Brzezinski** *May 03, 2016 16:27*

Oh, also note: This is perfectly reasonably that they use rods, <b>because it's a CNC machine</b>. There is much higher load on a CNC machine than a 3D printer. 



So I really should have made that clear. Two very different design goals. I'm not a big fan of 3D printer + CNC combo machines, because they have extremely different design goals.


---
**Dave Hylands** *May 03, 2016 16:45*

The helical couplers should be fine if they're properly load isolated by using thrust bearings


---
**Tomek Brzezinski** *May 03, 2016 17:16*

That would be true, but I am under the impression it would negatively influence the ability for them to account for axial misalignment, because once constrained by thrust bearings they couldn't readily bend.  I have also never seen them properly constrained with thrust bearings on a reprap, but it may be the CNC'ers do that (since the issues would be bigger in a CNC anyway.)



However, I should have noted spider couplings will need thrust bearings, which I honestly forgot, so they do cost a good bit more than helicals sans thrust bearings. 



One of the least professional but really savvy couplers I've seen reprappers use is just perfectly sized tubing that allows for bend but is axially rigid.


---
**Vic Catalasan** *May 04, 2016 03:23*

I built a CNC machine for cutting large aluminum plates and parts. Also been printing with my Herculien for many months now, faster than making things out of wood. Electronically identical in nature but totally different kinds of tools. Mix the two and you have the worst of both worlds instead of the best for each type. 



Those helical gears are great for light duty, I tried them on my CNC and I can rotate the rod 1/8 turn by hand, ended up using the oldham style couplings.


---
*Imported from [Google+](https://plus.google.com/103318213942990361542/posts/UVVEFPSMR5t) &mdash; content and formatting may not be reliable*
