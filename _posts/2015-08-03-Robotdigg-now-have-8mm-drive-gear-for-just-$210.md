---
layout: post
title: "Robotdigg now have 8mm drive gear for just $2.10"
date: August 03, 2015 06:20
category: "Show and Tell"
author: Marcos Duque Cesar
---
Robotdigg now have 8mm drive gear for just $2.10.

[http://www.robotdigg.com/product/390/MK7+Filament+Drive+Gear](http://www.robotdigg.com/product/390/MK7+Filament+Drive+Gear)





**Marcos Duque Cesar**

---
---
**Eric Lien** *August 03, 2015 11:12*

Very nice to see they carry those now, the only problem I have with most of the cheap drive gears is the tooth profile machining is often very dull and so tends to have poor grip. But it would be nice to see what someones experience is using it. 


---
**Eirikur Sigbjörnsson** *August 03, 2015 16:24*

We replaced the original drive gear on the Ultimaker2 with this one (It's a bit wider than the original so we had to tweak a little bit with the firmware). Its much better than the original one (which is straight and doesn't have the curve like the robotdigg one). The original one kept eating into the material which this one doesn't. I have no complaints about the quality, it does it's job quite well. They carry now a narrower one so I'll probably add one to my next order to try out on the Ultimaker.


---
**Eric Lien** *August 03, 2015 16:54*

**+Eirikur Sigbjörnsson** Thanks for the feedback. This is great to know.


---
*Imported from [Google+](https://plus.google.com/100788794198447857065/posts/NEp3SQ68cXW) &mdash; content and formatting may not be reliable*
