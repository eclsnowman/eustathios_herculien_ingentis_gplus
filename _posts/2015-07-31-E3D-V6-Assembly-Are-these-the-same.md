---
layout: post
title: "E3D V6 Assembly Are these the same?"
date: July 31, 2015 16:34
category: "Discussion"
author: Brandon Cramer
---
E3D V6 Assembly 



Are these the same? One has a resistance of 24 and the other 25.5. 



Looking at this link under Heater Cartridge it mentions two different leads.



[http://wiki.e3d-online.com/wiki/E3D-v6_Assembly](http://wiki.e3d-online.com/wiki/E3D-v6_Assembly)

![images/88a05276ece85f56faaf354c094a5cb5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/88a05276ece85f56faaf354c094a5cb5.jpeg)



**Brandon Cramer**

---
---
**Jim Wilson** *July 31, 2015 16:41*

I've had them with slightly varying resistances too, I was never sure either. However, they gave very similar temps and heatup times when using the exact same marlin settings so I didn't pay it much mind.


---
**Frank “Helmi” Helmschrott** *July 31, 2015 16:58*

Are these the ones that came with the E3D? Than they're probably only the 25W versions, you should think about using 40W ones.


---
**Brandon Cramer** *July 31, 2015 17:05*

**+Frank Helmschrott** Yes they are. So I need to order the 40W leads then I take it?



I have a question about the cooling duct. It doesn't seem like these are going to work together. The opening in the duct doesn't seem large enough?



[https://www.dropbox.com/s/xgv35dewprkmvx6/2015-07-31%2010.03.17.jpg?dl=0](https://www.dropbox.com/s/xgv35dewprkmvx6/2015-07-31%2010.03.17.jpg?dl=0)


---
**Frank “Helmi” Helmschrott** *July 31, 2015 17:08*

You should be okay to start with the 25W but it will sure be not powerful enough if you go a bit faster. I've just put that aside before even testing it. Don't know why they exchanged the 40W in the bundles with the 25W ones.



Regarding the duct: You have to cut out the inner, thicker part carefully. At least that was what i had guessed - couldn't find any information. I managed to cut it out so the inner part still nearly reaches down to the same level than the outer ring. This way you avoid the air flowing to your heater. Didn't work for me the first time - only needed one more print to find that out and now it seems to be good.


---
**Frank “Helmi” Helmschrott** *July 31, 2015 17:09*

Maybe **+Eric Lien** though can tell us the "official" way to do it :)


---
**Brandon Cramer** *July 31, 2015 17:21*

**+Frank Helmschrott** So you cut the opening to allow for the entire block to fit and be level with the bottom of the cooling duct?



I ordered the 40W leads. 


---
**Frank “Helmi” Helmschrott** *July 31, 2015 17:23*

i completely cut the thick part of the duct away - that piece only seems to be there to make it print better because otherweise the "inner wall" of the duct would touch the print bed only with a thin wall which wouldn't be easily printable. Guess it's not more than a better Brim if you will.



Would show you a photo of mine mounted to the printer but i'm currently not down in the basement. Will post one later if you still need.


---
**Bryan Weaver** *July 31, 2015 18:40*

Look at these pictures here so you can see how the air duct is supposed to look when you get everything cleaned up.  It  was pretty delicate process.  I cracked mine  a little bit in the process, but it all worked out in the end.



[https://plus.google.com/photos/+EricLiensMind/albums/6154111561734842705?sqi=108524206628971601859&sqsi=b7a8b239-1809-4a68-bb05-ac3bff2fe3e1](https://plus.google.com/photos/+EricLiensMind/albums/6154111561734842705?sqi=108524206628971601859&sqsi=b7a8b239-1809-4a68-bb05-ac3bff2fe3e1)


---
**Brandon Cramer** *July 31, 2015 20:10*

**+Bryan Weaver** That helps! I've cut out all the extra out of the middle and it fits much better. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/7Lu6iXP1s7M) &mdash; content and formatting may not be reliable*
