---
layout: post
title: "it seems I can't use 3mmX12mm_Buttonhead(McMaster_92095A183) to screw in Endstop Subminiature Switch ...."
date: June 09, 2016 06:07
category: "Build Logs"
author: Botio Kuo
---
it seems I can't use 3mmX12mm_Buttonhead(McMaster_92095A183) to screw in Endstop Subminiature Switch .... Is anyone also has this problem ? or I need to re-drill a hole ? Thanks





**Botio Kuo**

---
---
**Maxime Favre** *June 09, 2016 06:09*

I remember asking the same question ;) Drill them to 3mm.


---
**Botio Kuo** *June 09, 2016 06:19*

**+Maxime Favre** Hi Max, do you put an alumimun plate under your power supply for secured? and also how did you organize the cables and wires ? I like your printer final clean outlook. Thanks


---
**Maxime Favre** *June 09, 2016 06:34*

I didn't put anything under the PS but mine is probably smaller and lighter than the BOM's one. For cable managment I used 20mm wide cable trays under extrusions and cable chains. Scroll down this group for more pictures.


---
**Derek Schuetz** *June 09, 2016 19:12*

Lots of zip ties and cable sleeving will give it a nice clean look. I just directly glued my power supply to the acrylic and it held up pretty well 


---
**Derek Schuetz** *June 09, 2016 21:33*

[https://imgur.com/a/ylQkN](https://imgur.com/a/ylQkN)



My machine after completed and the underside. Somewhat clean and managed. I have also added panes to the sides and back to reduce drafts 


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/HkteG9oBNyZ) &mdash; content and formatting may not be reliable*
