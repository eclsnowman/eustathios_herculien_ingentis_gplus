---
layout: post
title: "Eric Lien i will try to print a modified version of your XY Belt Tensioners instead of the ones from Walter Hsiao as i have difficulties getting belt tensioning done reliably with the Flex inserts"
date: July 21, 2015 10:22
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
**+Eric Lien** i will try to print a modified version of your XY Belt Tensioners instead of the ones from **+Walter Hsiao** as i have difficulties getting belt tensioning done reliably with the Flex inserts. I wonder how you suggest to print yours? Generally i would think it would be good to orient them like in the screenshot but how about that belt teeth inside the slots? They probable would mess up this way? Print everything with support and turn them around so the teeth are facing up doesn't sound great either.

![images/e0e226d891018d4d31c5ad548b6867ee.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e0e226d891018d4d31c5ad548b6867ee.gif)



**Frank “Helmi” Helmschrott**

---
---
**Isaac Arciaga** *July 21, 2015 10:47*

**+Frank Helmschrott** you shouldn't need any support the way you have the carriages plated in your photo. You'll be fine as long as you are not too much of a speed demon on your perimeters.

There can be a weak spot on this particular part that is prone to delamination/breaking. The red lines in this photo indicate the weak area when clamping down on the 8mm shaft [http://i.imgur.com/SXcmRxO.gif?1](http://i.imgur.com/SXcmRxO.gif?1)

Add more filet/chamfer there if that area fails on you. 



*Edit - correction, you may need support for the lower bushing hole. But none needed for the belt teeth.


---
**Eric Lien** *July 21, 2015 10:57*

I print exactly as shown with support in the area of the lower bushing only via manual support in Simplify3D. 



I have never had any cracking on this part before, but one trick is you barely have to tension the clamp at all around the crossrods. Too much clamping force and you can deform the part and create binding.


---
**Erik Scott** *July 21, 2015 13:38*

I chamfer the interior of the hole where it transitions to the narrow bit such that I don't need support. I did the same on the carriage. 



That weak area shouldn't be a problem if you have decent inter-layer adhesion and good tolerances in the 8mm hole. My original parts broke in that place because they were printed on a printer that was kinda crappy, but I fixed them with a bit of CA. 


---
**Frank “Helmi” Helmschrott** *July 21, 2015 14:05*

Ok thanks guys. I don't have the inner smaller part as i'm using the 30mm robotdigg graphite bushings. So should print fine for me.


---
**BİRALOGEG Ali İçer** *July 23, 2015 00:19*

i3 : Cura  - Herculien : ?


---
**Frank “Helmi” Helmschrott** *July 23, 2015 06:20*

**+Ali içer** use whatever software you like. If you enjoy the simplicity of Cura you can of course also use it with Eusthatios and Herculien.


---
**BİRALOGEG Ali İçer** *July 23, 2015 10:47*

Thank you


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/GcPYSZuU8hP) &mdash; content and formatting may not be reliable*
