---
layout: post
title: "I'm moving slow and steady on the build"
date: July 28, 2015 06:36
category: "Discussion"
author: Gunnar Meyers
---
I'm moving slow and steady on the build. Sometimes I feel like I take one step forward and end up three steps back. This is definitely the most complex time consuming rep rap I've built. Could someone also post pictures of the carriage fully assembled with hotends?



![images/4173bcf225501d2e184fc7e0f3708e57.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4173bcf225501d2e184fc7e0f3708e57.jpeg)
![images/e124fdf8c794b6df3c3c68fb7cccf88d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e124fdf8c794b6df3c3c68fb7cccf88d.jpeg)
![images/a4a51ff3668e6dc85fe985a2af62bee7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a4a51ff3668e6dc85fe985a2af62bee7.jpeg)
![images/7e233b89496037f2f897a4d8ba381091.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7e233b89496037f2f897a4d8ba381091.jpeg)

**Gunnar Meyers**

---
---
**Vaughan Lundin** *July 28, 2015 07:30*

Looks like an awesome build.


---
**Vic Catalasan** *July 28, 2015 16:08*

Looking great! All the work you put in will all be worth it.


---
**Eric Lien** *July 28, 2015 23:01*

I can do that soon, but have you pulled up the edrawings model? It should be all there. Except wire routing... Which honestly could be improved on mine... But it works so I haven't changed it.


---
**Eric Lien** *July 28, 2015 23:05*

Also please look through my old posts, it may help. Like this one (note this is the old carriage for E3D v5 hotends... But is very similar): [https://plus.google.com/+EricLiensMind/posts/fWmxDAhW3nz](https://plus.google.com/+EricLiensMind/posts/fWmxDAhW3nz)


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/gf7asX9b4Mz) &mdash; content and formatting may not be reliable*
