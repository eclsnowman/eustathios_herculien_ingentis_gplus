---
layout: post
title: "So here is my obligatory 3d printed parts for the spider v2 all partss are abs from a folgertech 2020"
date: April 19, 2016 02:25
category: "Discussion"
author: Sean B
---
So here is my obligatory 3d printed parts for the spider v2 all partss are abs from a folgertech 2020.  I am very slowly putting it together and purchasing parts (wife... budget).  I was wondering what % infill I should be shooting for, basically anything load bearing I have been doing 100%, should this also includes the carriage?  I was going to go with Walter's space invader mini.  So far I have ordered the ballscrews and some bearings.  I was going to purchase the extrusion from the maker shop that posted on here a few weeks ago.  Does anyone know if they will start pure drilling the holes?  

![images/6d83d8964ea23e5be78473f3a8c320c9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6d83d8964ea23e5be78473f3a8c320c9.jpeg)



**Sean B**

---
---
**Derek Schuetz** *April 19, 2016 03:22*

I never went above 80% infill on any parts i have printed and it's usually more than enough. If you want all of your extrusion cut and drilled for you I would just go with misumi. I think the all the extrusion, rods, lead screws from misumi cut and ready for assembly went for $500 when I built mine and those were black anodized 


---
**Sébastien Plante** *April 19, 2016 03:54*

Can't find the tests, but there was a videos on YouTube showing that anything over 40% was useless and even becoming weaker in some cases. don't know if it's that true.


---
**Walter Hsiao** *April 19, 2016 06:00*

I used a fairly low infill rate on the carriage, I'd guess under 20%,  I think I used grid infill, and 2 or 3 perimeters.  I tend to like printing transparent filaments at 100%, but it would have added too much weight in this case.


---
**Maxime Favre** *April 19, 2016 07:42*

Same here: 3 perimeters, 4 when strength needed. I didn't print anything over 20% grid.


---
**Sean B** *April 19, 2016 12:54*

Interesting..  perhaps I have been wasting filament.  Here was my motivating force behind high infill: [http://my3dmatter.com/influence-infill-layer-height-pattern/](http://my3dmatter.com/influence-infill-layer-height-pattern/)   I shouldn't say all load bearing parts are 100% as I have been playing with it a little.  I have 3 perimeters set as default, only increased it in Repetier as the default was set to 2.  Presently I have a range between 75 and 100% for most of my parts.


---
**Stefano Pagani (Stef_FPV)** *July 11, 2016 02:18*

Oops, I just did a batch of my parts solid, took up 2/3's of a roll!


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/Yfjs3G1Vewc) &mdash; content and formatting may not be reliable*
