---
layout: post
title: "Anyone have the PayPal info for alirubber specifically the one who goes by Silvia Lang"
date: May 07, 2015 14:40
category: "Discussion"
author: Derek Schuetz
---
Anyone have the PayPal info for alirubber specifically the one who goes by Silvia Lang. She sent me a quote but did not include PayPal info





**Derek Schuetz**

---
---
**Bruce Lunde** *May 07, 2015 15:27*

I emailed them and they sent the paypal info back: this is what I used: Paypal account: alirubber.com@163.com  I had to send a confirmation back so they would check with accounting to ensure pmt received before they processed my order.


---
**Gus Montoya** *May 07, 2015 16:38*

I paied to the same acct as Bruce,  my contact was Daisy Huang.


---
**Derek Schuetz** *May 07, 2015 16:42*

**+Bruce Lunde** was your contact sivia liang. i believe theres 2 different alirubbers and i want to make sure i send to the right one


---
**Gus Montoya** *May 07, 2015 17:02*

Two alibaba's? oh ic. Just email her back, if she doesn't get back to you email this person: daisyhuang@alirubber.com.cn it's the same person Dat Chu used for the group buy. I also bought from her and things went smoothly.


---
**Bruce Lunde** *May 07, 2015 17:29*

**+Derek Schuetz**​ Daisy was my contact as well.


---
**Jo Miller** *May 07, 2015 20:16*

Yep, Daisy


---
**Eric Lien** *May 07, 2015 23:34*

Daisy (sure its not her real name) is who I have contacted. Very responsive.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/FLVZxifGNd2) &mdash; content and formatting may not be reliable*
