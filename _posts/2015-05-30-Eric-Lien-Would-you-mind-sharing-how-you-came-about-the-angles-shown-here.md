---
layout: post
title: "Eric Lien Would you mind sharing how you came about the angles shown here?"
date: May 30, 2015 21:12
category: "Discussion"
author: Erik Scott
---
**+Eric Lien** Would you mind sharing how you came about the angles shown here? I can measure the angles, but they're all like 23.8818 and the decimals keep coming if I increase precision. I'm trying to re-cad the carriage with a clamp-style mounting method and I want to keep things as compatible as possible. 

![images/293eaa51a920b0d17fc46e92c966cf83.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/293eaa51a920b0d17fc46e92c966cf83.png)



**Erik Scott**

---
---
**Eric Lien** *May 30, 2015 22:14*

It was done by modeling in assembly based on how I located the fan. The fan was located based on distances, not angles. So it might be difficult to do based on angles unless you just choose a new one... Then redraw the bosses based on that new angle.


---
**Erik Scott** *May 30, 2015 22:17*

Do you know those positions and distances?


---
**Eric Lien** *May 31, 2015 00:06*

I will have to go back and look at the solidworks tree. I will try to look tonight.


---
**Erik Scott** *May 31, 2015 00:47*

Alright, thanks. Much appreciated. 


---
**Eric Lien** *June 03, 2015 21:49*

Sorry for the delay. Yeah the way I built the features is messy. It should probably be adjusted. I mated the back of the fan to the angle face created by this loft extrude: [http://i.imgur.com/MtqIPHP.png](http://i.imgur.com/MtqIPHP.png)



The loft was designed to create a shell frame for the duct around the axis that I had defined for the hot end location. 



It works but would make for one hell of an ugly print to fully define the model. So I think it would be worth it to set the feature based on an angle, the redefine things based on that.


---
**Erik Scott** *June 05, 2015 01:39*

Interesting. I tried my hardest to find a nice dimension that defined that plane, but nothing seemed to work. So I figured it fell out of the fan placement which in turn was defined by something else. I guess not. Looks like I may change the design slightly there as well. Thanks for the response though! Much appreciated, sir!



By the way, the reason I'm re-cadding the carriage is to add a clamp-mount similar to this: [http://i.imgur.com/5OdGfvz.png](http://i.imgur.com/5OdGfvz.png)

This is a modified extruder to a TS wilson I printed for a friend. I was quite dissatisfied with the design, so I re-designed many of the printer's in small (and large) ways. This is one of the things I added. What I liked about this was I did't have to worry too much about getting the tolerance perfect. It printed a little oversize, but I could screw it right in and it seated wonderfully and was totally rigid. I was wondering what your thoughts on it are. I understand it would add a bit more weight/bulk to the print-head. 


---
**Eric Lien** *June 05, 2015 02:26*

**+Erik Scott** it is a good idea. I just kept things similar to the original Eustathios, but I use something similar on HercuLien which makes for a much more solid mount.


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/XUbDAqwTi5c) &mdash; content and formatting may not be reliable*
