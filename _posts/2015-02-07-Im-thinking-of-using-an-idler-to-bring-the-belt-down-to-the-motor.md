---
layout: post
title: "I'm thinking of using an idler to bring the belt down to the motor"
date: February 07, 2015 20:53
category: "Deviations from Norm"
author: Richard Mitchell
---
I'm thinking of using an idler to bring the belt down to the motor. This will allow me to use one belt length tensioned in the xy block.



Is there anything I'm missing here that means this is a bad™ idea?

![images/d612a8630becc3a415ca953e1f93a2a8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d612a8630becc3a415ca953e1f93a2a8.jpeg)



**Richard Mitchell**

---
---
**Jason Smith (Birds Of Paradise FPV)** *February 07, 2015 21:00*

I had never thought of doing it that way. I can't think of any particular reason why it would be a "bad" idea. Let us know how it works out. Should save money on an extra belt and pulley. 


---
**Eric Lien** *February 07, 2015 21:02*

Only issue I see is the cantilever on the idler. I haven't ever been a big fan of it because over time they always seem to migrate with the force causing them to ride to the edge of the idler and rub.﻿


---
**Miguel Sánchez** *February 07, 2015 21:07*

Only a few teeth will be engage the pulley in the corner of the picture. You will tell us whether this is a problem or not.



Yet another choice could be to set the motor in the middle of the axis instead of at one corner and use to two idlers, same belt distance, one more idler, more teeth in both toothed pulleys.


---
**Richard Mitchell** *February 07, 2015 21:10*

I quite like that idea of certainly a possible variation if the number of teeth causes problems. I will be moving the idler higher up to be more in line with "original" line of the belt in the next mockup.


---
**Dale Dunn** *February 07, 2015 22:56*

**+Eric Lien** . this is what crowned pulleys are for.



[http://www.thingiverse.com/thing:14117](http://www.thingiverse.com/thing:14117)


{% include youtubePlayer.html id=foQmpN2twf8 %}
[https://www.youtube.com/watch?v=foQmpN2twf8](https://www.youtube.com/watch?v=foQmpN2twf8)


---
**Eric Lien** *February 07, 2015 23:02*

**+Dale Dunn** now if only robotdigg of misumi would machine them that way ;)


---
**Mike Thornbury** *February 07, 2015 23:55*

A hand drill and a piece of sandpaper.


---
**Mike Thornbury** *February 08, 2015 00:12*

**+Miguel Sánchez** you can fix the engagement issue with a pair of idlers:



https://www.dropbox.com/s/cu4dnbjwxm32o5a/Screenshot%202015-02-08%2008.11.26.png?dl=0


---
**Miguel Sánchez** *February 08, 2015 10:29*

**+Richard Mitchell** That's what I was proposing above but too lazy to draw (but I suggested to put motor mid-way in the axis, with your way it can be kept in the corner).



Still I have the doubt about having two different belt lengths on each side and how that will affect getting them similar tension.


---
**Richard Mitchell** *February 08, 2015 10:51*

**+Miguel Sánchez**​

The length is actually pretty similar x and z. The main issue with putting multiple idlers is going to be clearance of the xy block I think. Yet to play with it and work out a block design.



**+Dale Dunn**​

May play with a crowned idler instead of guide washers for the fun of it,


---
**Mike Thornbury** *February 08, 2015 13:02*

I am a big fan of idlers, I think they give you a lot of options for power-train design.



In the same vein as above, this is the same thing, but packaged differently. The idler can be as simple as a piece of pipe sheathing an M5 screw - it's just to change direction, which makes for a lot of options when you want to fit into a small space.



[https://www.dropbox.com/s/cf7znr22ggpy2o9/Screenshot%202015-02-08%2020.59.22.png?dl=0](https://www.dropbox.com/s/cf7znr22ggpy2o9/Screenshot%202015-02-08%2020.59.22.png?dl=0)



Same bits, different packaging:



[https://www.dropbox.com/s/csnglagb953y58q/Screenshot%202015-02-08%2021.06.37.png?dl=0](https://www.dropbox.com/s/csnglagb953y58q/Screenshot%202015-02-08%2021.06.37.png?dl=0)


---
**Richard Mitchell** *February 08, 2015 18:47*

The first one looks a bit of a tight radius for the belt although I doubt if that's actually an issue.



I had thought of the second idea but it's quite a bit more belt and I'm trying to put 4 motors, PSU, RAMPS and raspberry pi underneath the floor panel already.



Be interesting to see how it works in practice.


---
**Richard Mitchell** *February 08, 2015 19:28*

For reference found some documentation on GT2 belts. In short I think 624ZZ or bigger is ok for the backside(snigger) idler and tooth grip should be OK(ish).



[http://www.sdp-si.com/D265/PDF/D265T056.pdf](http://www.sdp-si.com/D265/PDF/D265T056.pdf)



**+Dale Dunn**​ "Backside idlers should be flat and

uncrowned."

**+Miguel Sánchez**​ "The

designer should make sure that at least 6 belt teeth are in mesh on load-carrying pulleys...In order to minimize the potential for belt ratcheting[tooth jumping], each loaded pulley in the system

should also have a wrap angle of at least 60°."

**+Mike Thornbury**​ "Flat backside idlers should be

used with diameters at least 30% larger than the minimum recommended inside pulley size." [OD 12.7mm for 2mm GT2]


---
**Dale Dunn** *February 08, 2015 21:38*

**+Richard Mitchell** , in this application, it looks as though you could make this into a front-side idler by adding a twist to the belt.


---
**Richard Mitchell** *February 08, 2015 21:48*

**+Dale Dunn** If did that I'd probably be back to needing an pulley as the minimum OD for a flat inside idler is 25.4mm which is much larger than 12.7mm for a backside flat idler.


---
**Mike Thornbury** *February 09, 2015 02:21*

If your drive pulley only needs 60deg of contact, packaging becomes even easier. My M5 idler is 10mm, not far off the recommendation in your reference. I also find it coincidental that the sizes given are 1", 1/2", 1/4" equivalents... how much R&D went into deciding that 12.7mm was correct and 11mm wasn't?  I didn't read the doc, is it from Gates?


---
**Mike Thornbury** *February 09, 2015 03:24*

From Gates' GT2 Engineering design guide: "[Idler] Diameters should not be smaller than 1.3 times the smallest recommended inside sprocket size."


---
**Richard Mitchell** *February 09, 2015 05:35*

That's similar to the docs I found but they differentiate between inside and outside idlers. They say 30% bigger too but for smooth idlers they have to be bigger for inside idlers because of the tooth profiles. I'm only going to have 4.5 teeth in contact on my 90° turn but I don't think we're anywhere near the torque limit.


---
**Mike Thornbury** *February 09, 2015 10:16*

Not even close. The 5mm 2MR  belt can take horsepower :)



Look for 'Gates GT2 Design Guide' - about 150+ pages of tech application details.


---
*Imported from [Google+](https://plus.google.com/111547506541230089782/posts/GyBkKdfipk5) &mdash; content and formatting may not be reliable*
