---
layout: post
title: "Greatest feeling is getting it working after hours of trying to understand marlin with no previous experience beyond configuration.h screen"
date: March 07, 2015 03:56
category: "Show and Tell"
author: Derek Schuetz
---
Greatest feeling is getting it working after hours of trying to understand marlin with no previous experience beyond configuration.h screen 

![images/4e0996720d59bd589da51761fd1863c2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4e0996720d59bd589da51761fd1863c2.jpeg)



**Derek Schuetz**

---
---
**Paul Sieradzki** *March 09, 2015 01:47*

Is this the VIKI 2? How is it compared to the v1? I had a v1 and almost immediately ebayed it because the scroll wheel really was terrible.


---
**Derek Schuetz** *March 09, 2015 04:14*

I like it so far but not hinging much to compare it to except for the basic lcd that comes with sainsmart ramps kit


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/8UExX3upnLe) &mdash; content and formatting may not be reliable*
