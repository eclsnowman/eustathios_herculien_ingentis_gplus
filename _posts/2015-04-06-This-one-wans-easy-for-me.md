---
layout: post
title: "This one wan's easy (for me ;) )..."
date: April 06, 2015 22:04
category: "Show and Tell"
author: Ricardo Rodrigues
---
This one wan's easy (for me ;) )... but another model in FreeCAD

![images/202b2d9aab8619a4eb2ba5b0f81eaba7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/202b2d9aab8619a4eb2ba5b0f81eaba7.jpeg)



**Ricardo Rodrigues**

---
---
**Eric Lien** *April 06, 2015 23:01*

Great work.


---
**Ricardo Rodrigues** *April 06, 2015 23:03*

Thanks, at the very least I'm learning how to Model. 

😊


---
**Jim Wilson** *April 06, 2015 23:08*

Nicely modeled!



Do you have the option to fillet the edges? You can add significant strength to parts, especially at 90 deg corners if you can give them a slight fillet or chamfer. (Many of the Eustathios v2 parts have the edges I mean.)


---
**Ricardo Rodrigues** *April 06, 2015 23:11*

I'm having some issues when I tried to fillet the model, but you can notice the chamfer on the bottom part. 


---
**Jim Wilson** *April 06, 2015 23:13*

In some CAD software, you have to split parts up, fillet, then recombine them if it can't figure out all the edges of a complicated part.



A blind hint, I haven't used freecad, but it may be worth looking at workflow-wise.


---
**Ricardo Rodrigues** *April 06, 2015 23:13*

Normally my questions are about what are the best practices.  I'm guessing that I will have to redo everything to be standard in the way I'm modelling. For the time being I'm just doing what comes to mind first. 


---
**Ricardo Rodrigues** *April 06, 2015 23:18*

**+Jim Wilson**​ I can do some fillets but the software gave some errors and I gave up.  Perhaps I will try  another approach. 



I'm using freecad because I like the Open-source ideology and I'm using Linux 😀 (because I like the Open-source ideology) 😉


---
**Erik Scott** *April 07, 2015 02:02*

Fillets only help structurally on concave corners. The rounds on the convex edges are just there for appeal or to cut down on rough edges. 



Here's my version of the exterior XY Motor Mount: [http://i.imgur.com/KNjosh3.png](http://i.imgur.com/KNjosh3.png). I added some exterior fillets for appeal, but the interior ones are important, especially considering how close they are to those extruded bits. 


---
**Jim Wilson** *April 07, 2015 02:03*

^


---
**Mike Miller** *April 07, 2015 12:33*

And because detail is free! (unlike, you know, working in wood...which is why I was never good at working in wood.)


---
*Imported from [Google+](https://plus.google.com/+RicardoRodriguesPT/posts/5ihCN3cQH4p) &mdash; content and formatting may not be reliable*
