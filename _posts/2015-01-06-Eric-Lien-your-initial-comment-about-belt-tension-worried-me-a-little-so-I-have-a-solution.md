---
layout: post
title: "Eric Lien , your initial comment about belt tension worried me a little, so I have a solution!"
date: January 06, 2015 22:41
category: "Show and Tell"
author: ThantiK
---
**+Eric Lien**​, your initial comment about belt tension worried me a little, so I have a solution! Loosen up the bearing holders on each side slightly and pull them away from the t slot. Then attach belt and screw bearing holders back down. 😁If they slack off over time, I'll cut off a tooth from the edge of a belt and do it again, pulling it in slightly further.


**Video content missing for image https://lh3.googleusercontent.com/-UWxXtA2EzTs/VKxklj48UCI/AAAAAAAAXFM/sE-_58Wee2c/s0/VID_20150106_170620.mp4.gif**
![images/8ed04aac8c15cbffd468024ac1102856.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8ed04aac8c15cbffd468024ac1102856.gif)



**ThantiK**

---
---
**Eric Lien** *January 06, 2015 22:48*

Nice solution.


---
**Mike Thornbury** *January 06, 2015 23:04*

A sprung idler, as is common in transmission-belt systems on cars and machinery, would be the most obvious solution to belt stretch over time.


---
**ThantiK** *January 06, 2015 23:09*

**+Mike Thornbury** that's actually a bad idea.  It looks good on paper, but the inertial forces on corners, and the spring, cause damping effects on the edge of parts at the speeds I'm typically printing at.  I learned this back from **+Triffid Hunter** a long time ago, who used a closepin spring for an auto belt tensioner.



Additionally, any difference in spring tension can cause the rods to move ever so slightly different across the rods, causing binding.


---
**Mike Thornbury** *January 07, 2015 02:09*

The carriage is only being 'driven' by the top length of belt, putting your tensioner on the bottom length won't affect your top length at all. I it would, then cars timing belts would adjust the timing as they loosened off - which is completely contrary to the design ideal. They 'gather' the loose belt on the opposite side from the 'timing' side, as you should. I wasn't talking about a clothespin - more like a sliding idler set up you would see in a high-performance motorcycle engine. The rotational speeds introduce whip in the cam chain which have enough force to smash a timing gear. The idler uses a very strong spring and a low-friction slider to take up the slack in the chain without changing the timing at all.


---
**Brandon Satterfield** *January 07, 2015 04:36*

Automobile and motorcycle engines only accelerate in one direction. Deceleration is only fractional even in the hardest downshift in a corner on a track day with a squid at the helm.



Best way to discover this, treat it like it is from Missouri, the show me state. 



How about a print show down? Idler vs constant tension. 12" high twisted vase at 120mm/s, 9000mm/s^2, images should provide the "proof is in the pudding".






---
**Mike Thornbury** *January 07, 2015 12:09*

Good point about the direction - will have to look up some belt/electric schematics.



I have no idea WTF Missouri has to do with anything... I'm not afflicted with Americanitis.


---
**Jeremie Francois** *January 07, 2015 15:31*

**+ThantiK** yes and no. Actually you can have th ebest of both world, with springs and lockable thing once properly tensioned (as with my old stepper/short belt tensioners). [http://www.thingiverse.com/thing:40041](http://www.thingiverse.com/thing:40041)

BTW check the discussion on TV where a guy computed that the effect should not be noticeable with the proper K (which matches my experience, does not seem to be noticeable even at great speed on my UMO).

Now I still like your solution as it cannot be made simpler ;)


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/PN1XWxtHF9U) &mdash; content and formatting may not be reliable*
