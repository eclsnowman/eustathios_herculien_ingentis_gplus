---
layout: post
title: "I think I have goofed on configuring Marlin and my coordinate system"
date: May 03, 2018 21:17
category: "Discussion"
author: Dennis P
---
I think I have goofed on configuring Marlin and my coordinate system.  This is the first time I printed text upside down and it came out backwards and upside down. The line of text and symbol are flip-flopped top/bottom and they read backwards.  



I think I need to flip the motor connector and invert the axis flag in Marlin. But I am really confused because that makes not sense if I consider the Right Hand rule. X cross Y, Z is up. Then Z would also have to invert as well.



What am I missing?

![images/c4557f8cfa8f6393d7143b939195f428.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c4557f8cfa8f6393d7143b939195f428.png)



**Dennis P**

---
---
**Eric Lien** *May 03, 2018 22:05*

The perspective makes my thumb look "HUGE" :)![images/466bc14e4eabe5e750f7118447991da8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/466bc14e4eabe5e750f7118447991da8.jpeg)


---
**Eric Lien** *May 03, 2018 22:10*

Flat hand with fingers pointing in X positive, curl fingers towards Y positive, thumb points in Z positive. But the nozzle can't move up, so the bed moves down (which is the same as the nozzle moving up in the parts reference frame).


---
**Zane Baird** *May 03, 2018 22:12*

When you say you “flipped” it on the slicer I think you mirrored the object which would explain the resulting print. You need to rotate in the slicer, not flip or mirror. If your x axis moves to the right, y axis away from you and the bed moves down (all for positive jogging movements) then your motors are all configured correctly


---
**Dennis P** *May 03, 2018 23:28*

**+Zane Baird**  I think you might be right. I dropped the file into [gcode.ws](http://gcode.ws) and what I see is that looking down on the print, the first later reads correctly from what would be the inside of the object, where it should be reversed.

  


---
**Dennis P** *May 03, 2018 23:31*

**+Eric Lien** what is throwing me off is the +Z should be up, but the bed moves down. Are we saying that the +Z is truly up, like I imagine it, but we  trick the printer into moving down by reversing the motor connection?  


---
**Eric Lien** *May 04, 2018 01:16*

**+Dennis P** Z positive is up. What matters to the printers gcode is the nozzle height in relation to the bed. Nozzle at the bed is Z0.  Then Z=1 is the bed moved down 1mm (relative distance of bed to nozzle = 1) . 



Don't think of the bed down as Zdown. If it helps, think of locking the bed plate itself into a set of vice jaws...but let the rest of the printer float in relation to the bed. In this instance the nozzle would be moving up as Z gets more positive just like you are used to with i3 style printers where the gantry raises. 



Relative coordinate systems (aka reference frames) can get really fun when you approach light speeds. if you are ever looking for a fun rabbit hole to dive down, minutephysics has generated a really great set of videos recently on relative perspectives and how it effects our observation of whats happening. He build a mechanical lorentz transformation graph which put it into an easy to grasp way I never received in school. 



His channel is really worth a sub: [youtube.com](http://youtube.com) - minutephysics


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/DiAu2vQr6pq) &mdash; content and formatting may not be reliable*
