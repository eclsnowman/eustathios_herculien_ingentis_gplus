---
layout: post
title: "That point where you have to take your 15.5\"x15.5\" cast build plate and take 1/4\" off both sides for clearance...and your band saw is incredibly slow and inaccurate...and the mini mill can't hold it...and it doesn't fit the"
date: April 02, 2016 13:47
category: "Discussion"
author: Mike Miller
---
That point where you have to take your 15.5"x15.5" cast build plate and take 1/4" off both sides for clearance...and your band saw is incredibly slow and inaccurate...and the mini mill can't hold it...and it doesn't fit the printer.





**Mike Miller**

---
---
**James Rivera** *April 02, 2016 19:30*

Doh! :-(


---
**Jeff DeMaagd** *April 03, 2016 10:11*

You didn't hear it from me, but a table saw with a carbide tipped blade can do a good job. But wearing safety equipment (gloves, goggles face shield, apron) is very important, and it helps to lubricate the blade before each cut.


---
**Mike Miller** *April 03, 2016 14:11*

By the time I gear up and buy the blade, it might be easier to just take a $20 over to the nearest machine shop. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/cw6jLDbSTB1) &mdash; content and formatting may not be reliable*
