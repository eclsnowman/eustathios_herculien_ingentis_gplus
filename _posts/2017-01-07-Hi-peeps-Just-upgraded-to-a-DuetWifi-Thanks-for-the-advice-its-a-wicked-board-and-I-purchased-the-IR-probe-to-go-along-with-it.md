---
layout: post
title: "Hi peeps, Just upgraded to a DuetWifi (Thanks for the advice, its a wicked board) and I purchased the IR probe to go along with it"
date: January 07, 2017 19:01
category: "Discussion"
author: Adam Morgan
---
Hi peeps,



Just upgraded to a DuetWifi (Thanks for the advice, its a wicked board) and I purchased the IR probe to go along with it. I just wondered if anyone else use an IR probe with their Eustathios and if so how they went about mounting it?





**Adam Morgan**

---
---
**Eric Lien** *January 07, 2017 19:46*

I haven't seen anyone do it yet. If you find a good way please share. Also share your impressions on the DuetWifi. I have been hearing great things about that controller.﻿


---
**Jim Stone** *January 08, 2017 06:17*

I've been told IR probes are a pain I'm the butt for z axis because they work better on a flat/satin surface rather than a glossy or shiny one



I have only experience with tactile switches as of late.


---
**Adam Morgan** *January 08, 2017 13:42*

**+Eric Lien** Well I haven't much exposure with 3D printer boards, all I have ever used is a Printrboard and Rambo and now the DuetWifi, but I have to admit I am very impressed. I am not sure if the Smoothieboard is the same but the ability to modify the firmware live is really fun, I have only gone up to x64 microstepping so far, the motors are now virtually silent, quiet so much that i forget that there's a print going when I am sat with my back to it. 

The web interface is wicked, a tad buggy when it looses connection, but it doesn't interfere with the print at all. All the options you would want with the ability to write custom Macros and have buttons appear in the interface. I like that there's a built in editor to edit the firmware files on the sd card, so I can make changes anywhere. The board can power the web interface from usb, so I can play with it while the main power is switched off. 

The documentation on the website is pretty complete, every issue I have had has been resolved by reading through that, it was a bit odd coming from Marlin onto RepRepFirmware but eventually got my head around it mainly because of their wiki. 



So yeah, overall really impressed. Still learning best way to tweak it though as I never really got my Marlin firmware to its "optimal" settings. Also still plan to try out the IR Probe as I do like my auto tram correction, I know a good level bed is best but I can never get to get it to that sweet spot.  


---
*Imported from [Google+](https://plus.google.com/117208540149253709671/posts/EX9L4axgcEF) &mdash; content and formatting may not be reliable*
