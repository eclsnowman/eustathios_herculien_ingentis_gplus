---
layout: post
title: "Question for those in the know or tried it"
date: December 12, 2016 12:17
category: "Discussion"
author: Pieter Koorts
---
Question for those in the know or tried it. Would the Eustathios be a good candidate for using a direct drive extruder so that ultra flexible filaments could be used or is that a bad idea on the Eustathios?



I understand that adding more mass to the moving carriage will limit speed but would other factors come into play like issues with spinning rods or rod bending on direction changes?





**Pieter Koorts**

---
---
**Eric Lien** *December 12, 2016 12:36*

For trying direct drive I would suggest the flying extruder setup by **+Walter Hsiao**​. Another option is mounting it on the carriage directly but I might suggest going to 10mm cross rods to help fight deflection across the span.



I will love to see what you come up with. That's one of the main reasons the open source design of this printer is so much fun. Because people make so many cool upgrades and modifications since the files are there for you to hack and do with what you see fit.


---
**Pieter Koorts** *December 12, 2016 12:39*

Agreed on the modding thing just wanted to see if someone has already tried it and maybe run into flex problems with the added mass. Noted on the thicker rods


---
**Eric Lien** *December 12, 2016 13:46*

**+Pieter Koorts** you could always try something like this, it is from the HercuLien and I never tested it... But it's a starting point: [https://plus.google.com/+EricLiensMind/posts/Y4V8sHcjmgu](https://plus.google.com/+EricLiensMind/posts/Y4V8sHcjmgu)


---
**Torsten Endres** *December 12, 2016 15:23*





I modified one of **+Eric Lien**s older designs for the chimera hotend resulting in a bulky hybrid direct/bowden carriage. One hotend ist feeded by a E3D titan extruder while the second hotend works as a bowden hotend. After switching from the original dual bowden hotend the only disadvantage i noticed was a slightly stronger ringing due to the increased mass. My motivation was to be able to print flexible filaments and soluble support filaments. The results with flexible TPU filament are satisfactory (50mm/s works good). 



Here is also a nice design for the E3D V6 hotend:

[http://www.thingiverse.com/thing:1929948](http://www.thingiverse.com/thing:1929948)

![images/f67ac288e6927182261521329822de40.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f67ac288e6927182261521329822de40.jpeg)


---
**Eric Lien** *December 12, 2016 16:56*

**+Torsten Endres** Excellent work. Any chance you would be willing to supply the native model files that you created (Fusion360, Solidworks, Etc). I would love to get these onto the Github for the Eustathios Spider V2 under the Community Mods and Upgrades area: [https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades)



Also if you wanted to make a post about this upgrade in the group and some feedback with your experiences that would be great.


---
**Jules Ouellette** *March 02, 2017 18:12*

I have a custom coreXY that I use with the Modicum extruder and I have no issues printing flexible filaments with a 90A shore hardness (Semiflex, SainSmart TPU). I use 30mm/s and no retraction and don't get many strings. It prints very well.



Here's a iPhone 6 plus case I printed:



![images/88f344ca59cff59e7ac9999130b7e449.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/88f344ca59cff59e7ac9999130b7e449.jpeg)


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/dau9WDeT8EV) &mdash; content and formatting may not be reliable*
