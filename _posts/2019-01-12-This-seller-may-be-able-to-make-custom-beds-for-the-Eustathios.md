---
layout: post
title: "This seller may be able to make custom beds for the Eustathios!"
date: January 12, 2019 13:48
category: "Show and Tell"
author: Stefano Pagani (Stef_FPV)
---
This seller may be able to make custom beds for the Eustathios! I have one for my prusa, it’s awesome no talc needed for petg

![images/d4d99cb9e3645eff0f57b6e7f2805e81.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d4d99cb9e3645eff0f57b6e7f2805e81.png)



**Stefano Pagani (Stef_FPV)**

---
---
**Michaël Memeteau** *January 12, 2019 15:09*

Talc for PETG? Never heard about it... Would you care to elaborate? Thanks


---
**Eric Lien** *January 12, 2019 15:20*

**+Michaël Memeteau** maybe Tak... Like BuildTak?


---
**Ryan Carlyle** *January 12, 2019 20:17*

Talc to reduce adhesion so the PETG doesn't weld to the PEI, I think


---
**Jeff DeMaagd** *January 12, 2019 21:13*

I’ve never needed to do anything special other than use a lower temp. 65°C seemed to work fine.


---
**Stefano Pagani (Stef_FPV)** *January 12, 2019 22:14*

**+Ryan Carlyle** yeah sorry for being unclear I use baby powder on my pei so PETG doesn’t bond at 90c also no warp :)


---
**Dale Dunn** *January 12, 2019 23:01*

Can't find a link to the seller. Maybe because I'm on mobile?


---
**Stefano Pagani (Stef_FPV)** *January 14, 2019 00:41*

**+Jeff DeMaagd**![images/f4b62046dfebc7e8fea9183fc761dbc5.jpg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f4b62046dfebc7e8fea9183fc761dbc5.jpg)


---
**Jeff DeMaagd** *January 14, 2019 03:37*

That’s very nice. Did you have warp at lower bed temps though?


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/9uhD92MBh9P) &mdash; content and formatting may not be reliable*
