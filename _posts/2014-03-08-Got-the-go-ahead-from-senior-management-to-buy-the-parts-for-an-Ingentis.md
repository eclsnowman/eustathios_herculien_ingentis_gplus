---
layout: post
title: "Got the go ahead from senior management* to buy the parts for an Ingentis :-) !"
date: March 08, 2014 14:03
category: "Deviations from Norm"
author: Jarred Baines
---
Got the go ahead from senior management* to buy the parts for an Ingentis :-)  #Stoked !



I'll probably have a lot of questions such as these right here;



1)  What resolution do you guys normally print gears at? there are a lot on this thing and I want to do it well as I'll probably grab motors etc from my mendel and want a seamless change.



2) My mendel comes with a nema 14 for the extruder, I'm confident I can modify **+Tim Rastall**'s file to fit it, but do you think it will have enough grunt, at least for basic printing for now (I just NEED to get away from this mendel, it's badly worn and only works well printing very slowly)



3) Why rumba? I am going to be cautious about decisions this time around, I jumped on the mendel wagon before thinking too much and after a few months wished I hadn't. Is the Rumba cheap? has room for expansion? what influenced the choice?



4) **+Tim Rastall** - I have a cousin, brother and a friend or two who might be interested in building one of these, I am yet to ask them, but - I wanted to ask your blessing to organise a group buy-and-build here in AU, not for profit - but... It's YOUR thing and it's a work in progress so I thought I'd see what you thought ;-)



*senior management = the missus ;-)





**Jarred Baines**

---
---
**Daniel Fielding** *March 08, 2014 14:05*

Where in Australia are you?


---
**Jarred Baines** *March 08, 2014 14:09*

Mernda, north of Melbourne, you in AU too?


---
**Daniel Fielding** *March 08, 2014 14:17*

Yeah, greenbank in south east queensland.


---
**Jarred Baines** *March 08, 2014 14:38*

Ah k, thinking of building an Ingentis?


---
**Tim Rastall** *March 08, 2014 18:06*

**+Jarred Baines** go mad mate,  the licence is not for profit I think but as long as you share any design changes, it's yours to do with as you like :) 


---
**Tim Rastall** *March 08, 2014 18:09*

Rumba was cheap :).  I'd probably go for an azteeg x3 if I was starting again. RAMPS fine too but you've got to be careful to ensure it will support high wattage heat beds. 


---
**Jarred Baines** *March 08, 2014 18:09*

Cheers ;-) I'll keep u informed if there are any new bots being built!


---
**Jarred Baines** *March 09, 2014 01:06*

And the azteeg would be your choice for the high current?



I'm after a board which will support marlin, switchable fan, 3 - 4 heads and smart LCD if possible, any of the above do that / not do that to your knowledge?


---
**Tim Rastall** *March 09, 2014 01:52*

Rumba can do that with 3 heads but is hb mosfet and tracks can't handle a 300x300 heat pad. I have mine switching a relay. 


---
**Jarred Baines** *March 09, 2014 06:21*

I reckon I'll go that route then as I will probably switch the HB with a relay too - is the HB something you can easily add to the BOM when you get time mate?


---
**Tim Rastall** *March 09, 2014 06:36*

I'd actually ask **+Eric Lien** as he's been through that a few times recently. 


---
**Eric Lien** *March 09, 2014 07:18*

**+Jarred Baines** love my azteeg. Support from Roy can be hit or miss. But it drives my 400 watt heated bed directly like a champ. But I'm running 24v... So that helps.


---
**Jarred Baines** *March 09, 2014 10:16*

Any info on 24 vs 12v setups on the net boys? I like 12 cos its compatible with all my current gear but may change if its worth it!


---
*Imported from [Google+](https://plus.google.com/+JarredBaines/posts/Rnm7USMdxbg) &mdash; content and formatting may not be reliable*
