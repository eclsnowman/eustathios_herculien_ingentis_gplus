---
layout: post
title: "Hey Guys, I got my Azteeg X3 setup and talking to my computer with Marlin, however, there is no motor movement"
date: December 24, 2018 04:18
category: "Discussion"
author: Gunnar Meyers
---
Hey Guys, I got my Azteeg X3 setup and talking to my computer with Marlin, however, there is no motor movement. I can see through the logs on Simplify 3D that the Board is receiving the command but still no movement. I have setup the boot pins for 16 steps. Is there anything I could be forgetting to do? Thanks.





**Gunnar Meyers**

---
---
**Dennis P** *December 24, 2018 05:45*

marlin may not move unless it homed first.its a 'feature'....

can you issue a M119 command? what are the results? lets check your endstop stated first.

if you are powering up for the first time, i would suggest you leave off the belts connecting the motors to the axis and issue home command one axis at a time. then you can verify your directions are correct. manually trip that axis endstop pause a second then trip it again to mimic the homing behavior. do this for each axis and correct as needed. then reconnect the last link belts and you should be going along pretty well. 

 




---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/GB17TWitprp) &mdash; content and formatting may not be reliable*
