---
layout: post
title: "Any one have any suggestions for the 8mm Bronze Bushing for the Eustathios, searched everywhere and seems noboddy has them in stock?"
date: January 11, 2017 20:22
category: "Discussion"
author: Amit Patel
---
Any one have any suggestions for the 8mm Bronze Bushing for the Eustathios, searched everywhere and seems noboddy has them in stock?





**Amit Patel**

---
---
**Eric Lien** *January 11, 2017 21:00*

You are right. I do not see them anywhere :(



But it does look like the teflon filled acetal ones are availible out there. Not sure how they perform by comparison. But Might be worth a try:



[shop.sdp-si.com - Product &#x7c; Designatronics Store &#x7c; Stock Drive Products/Sterling Instrument](http://shop.sdp-si.com/catalog/product/?id=A_7Z44MPSD08MAF)



Another option is to print a ninjaflex sleeve for a plain bronze bushing like Walter did a while back. A bushing like this with something around it to allow for misalignment might work: ([https://goo.gl/U76OoN](https://goo.gl/U76OoN))


---
**Eric Lien** *January 11, 2017 21:02*

another option is to use 10mm cross rods, and 10mm self aligning bushings. Those seem to be in stock, and the walter side carriages are already setup for 10mm cross rods, and the 8mmID and 10mmID bushings have the same outside dimensions.


---
**Amit Patel** *January 11, 2017 22:13*

Yea was thinking of doing that **+Eric Lien** but already placed the ordered for the 8mm rods. SDP is saying 4 weeks on my order so will just wait and see. Worse case will order the acetel ones as a backup.


---
**Ted Huntington** *January 12, 2017 00:42*

I want to add that I see deflection from the 8mm carriage rods in the middle of the bed on the Eustathios Spyder 2 I built- I think the weight of the carriage makes it drop about 1 or 2 mm near the middle. So I think 10mm rods would solve it. In addition, you could, with redesign use regular linear bearings for the carriage.


---
**larry huang** *January 12, 2017 04:43*

**+Ted Huntington** What kind of carriage are you using? If the two 8mm rod drop 1 or 2 mm then that must be really heavy? And I imagine the printer will not print the first layer right.  Or maybe the rod was not harden?


---
**Ted Huntington** *January 12, 2017 05:56*

**+larry huang** I am using the original carriage. I presume the rods are hardened they came from: CNC Automation [aliexpress.com - RM1204 Ball Screw SFU1204 L= 450mm Rolled Ball screw rail with single Ballnut for CNC parts](https://www.aliexpress.com/item/RM1204-Ball-Screw-SFU1204-L-450mm-Rolled-Ballscrew-with-single-Ballnut-for-CNC-parts/1933681333.html?spm=2114.13010608.0.0.BAgEhg) . Actually I just re-measured, and the problem I am seeing is possibly with the bed. The X droops in the middle but the Y droops on the outside. It is only a problem for large prints. 


---
**larry huang** *January 12, 2017 07:48*

**+Ted Huntington** Ok, that's more assure, because I plan to use 8mm rods too, I purchased 10mm replacement any way. If the built plate was not flat, even a tiny, it's still a big problem. 


---
**Frank “Helmi” Helmschrott** *January 16, 2017 20:07*

**+Ted Huntington** if your carriage drops 1 or 2mm with the 8mm rods you must have some rubber rods :-) (just kidding) - should really be not that much of an issue if you are not carrying around some motors with the carriage. 10mm rods again add weight to the party which should be a goal to keep it light.


---
**Frank “Helmi” Helmschrott** *January 16, 2017 20:08*

Regarding the bronze bushings from SDP-SI I had some mail chatter with them the other day. They promised to have them in stock quickly even though the website doesn't reflect this. But they are currently moving their production which could add a week or two in delay. They also said they are shipping to germany so I may give them a try in the future.


---
**Ted Huntington** *January 17, 2017 03:07*

**+Frank Helmschrott** Rub'er rods indeed- I've never even seen 'er rods ahaha. I came to realize that it is the bed that must be uneven and not the weight of the carriage.


---
**Frank “Helmi” Helmschrott** *January 17, 2017 06:32*

**+Ted Huntington** I had some serious misalignment issues for a while that I first thought was at the carriage, then the bed. It took me a while to figure out that the size and bearing position at the carriage was the issue. After fixing that i suddenly noticed how smooth a carriage can run LOL. 


---
**Ted Huntington** *January 17, 2017 06:41*

**+Frank Helmschrott** I think you use different bushings so maybe that is why the issue- I just used the press fit self-aligning bushings on Eric's BOM. Now I am finding that some of my ABS parts are cracking- this seems to be a problem with 3D printed parts (in both ABS and PLA at least which I mostly use)- over long periods of time (a year, two years) many parts seem to crack more easily. I mostly use 3D printed parts for mechanical purposes and so perhaps this is why I find this more than other people.


---
*Imported from [Google+](https://plus.google.com/100854251935781152705/posts/YD7U4DbX2HU) &mdash; content and formatting may not be reliable*
