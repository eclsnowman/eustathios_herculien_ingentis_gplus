---
layout: post
title: "Slowly making progress, finally finished all the mains wiring with SSR"
date: August 29, 2016 03:18
category: "Discussion"
author: Sean B
---
Slowly making progress, finally finished all the mains wiring with SSR.  Not a huge fan of Dupont connectors either.  I had a bent 10 mm rods from Aliexpress, contacted the seller and the replacement was also bent. Paid a bit in shipping for a Misumi one, but the quality is definitely better.



 I have a couple questions though.



1.  What is the required wiring between the Pi and the Azteeg x5 Mini?  I have a bazillion wires floating around now and don't want to much it up.



2.  How does everyone power their Pi?  I have seen previous posts about a step down convertor, but can it be run directly off the Azteeg?



3.  What should the z micro stepping be set to?  I put it on 1/32 like X, Y, Z, and E.  I have 4mm pitch ballscrew from Golmart.  I have seen other posts about reducing this, is this necessary or will I run into issues with these settings?



So I purchased the only micro switches I found on RobotDigg: [http://www.robotdigg.com/product/154/Microswitch+Board+w/+Lead+Wires+for+3D+Printers](http://www.robotdigg.com/product/154/Microswitch+Board+w/+Lead+Wires+for+3D+Printers)

These definitely won't work with the Eustathios Spider 2 printed parts.  Does anyone have a suggested site for ordering the correct switches?  Would the following work?



[https://amzn.com/B00G2E6UZC](https://amzn.com/B00G2E6UZC)





**Sean B**

---
---
**Eric Lien** *August 29, 2016 03:37*

For the pi, just mount a 2a usb wall adapter to the underside. That's the easiest way. Otherwise you can use one of these connected to your printer PSU: [https://www.amazon.com/dp/B016XI99X6/ref=cm_sw_r_cp_apa_H26WxbSZ4ZTGM](https://www.amazon.com/dp/B016XI99X6/ref=cm_sw_r_cp_apa_H26WxbSZ4ZTGM)


---
**Eric Lien** *August 29, 2016 03:41*

For the z the 1/32 microstepping is overkill. Calculate the step/mm and you will see it gets a bit excessive. I like 1/8 or 1/16 on Z. We already get mechanical reduction going from a 20 tooth motor pulley to a 32 tooth pulley on the ball screw. 


---
**Eric Lien** *August 29, 2016 03:44*

To connect the printer controller to the pi all you need is the usb from the pi to the controller usb input. You could wire it instead over the tx/Rx lines and skip the usb... But the Usb is really the most stable way in my experience.﻿


---
**Eric Lien** *August 29, 2016 03:52*

For the end stops something like this:



 [http://www.robotdigg.com/product/141/Endstop+Subminiature+Switch+SS-5GL](http://www.robotdigg.com/product/141/Endstop+Subminiature+Switch+SS-5GL)



Most any sort (single pole double throw) microswitch should work.



Here are some more:



[https://www.amazon.com/dp/B00E0JOP76/ref=cm_sw_r_cp_apa_hb7Wxb8AEHETT](https://www.amazon.com/dp/B00E0JOP76/ref=cm_sw_r_cp_apa_hb7Wxb8AEHETT)



[https://www.amazon.com/dp/B00STL21R2/ref=cm_sw_r_cp_apa_Hd7WxbPHTGNGC](https://www.amazon.com/dp/B00STL21R2/ref=cm_sw_r_cp_apa_Hd7WxbPHTGNGC)






---
**Daniel F** *August 29, 2016 09:14*

additionally, I had to drill out the mounting holes of the end stops to 3mm


---
**Jeff DeMaagd** *August 29, 2016 12:20*

The really small micro switches have holes for 2.5mm hardware. One size up and it probably work with 3mm screws. Whatever standard that has the main switch body about 28mmx20mm I think.


---
**Eric Lien** *August 29, 2016 13:09*

Yes as others have mentioned you need to open up the holes on the switch to 3mm. There should be enough meat in the switch housing to do this (there has been on every switch I have bought before). I just do it this way to avoid having to keep yet another size of bolt on hand since I have so many 3mm around already.


---
**Sean B** *August 30, 2016 00:28*

Thanks for all the information!  I just found some other switches I had from an old project, I'll have to see if they fit.  I know the ones I currently have, have no chance of working.



I am leaning towards Tx/Rx line option because it's getting pretty crowded on the bottom of the printer.  Have you had experience with inconsistent power?


---
**Eric Lien** *August 30, 2016 00:38*

**+Sean B** you need really stable power into the pi with current to spare. Also make sure to set the Azteeg to run off printer PSU power (there is a jumper) not powered over the PI usb. It yields more stable performance in my experience.


---
**Sean B** *August 30, 2016 00:40*

Oh yes, I saw that jumper.  Thanks for the heads up.


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/YQ7rnsqTjPs) &mdash; content and formatting may not be reliable*
