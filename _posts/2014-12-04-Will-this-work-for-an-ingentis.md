---
layout: post
title: "Will this work for an ingentis?"
date: December 04, 2014 20:05
category: "Discussion"
author: Ethan Hall
---
[https://www.fastenal.com/web/products/details/0956819](https://www.fastenal.com/web/products/details/0956819)

Will this work for an ingentis?





**Ethan Hall**

---
---
**ThantiK** *December 04, 2014 22:59*

That's 2020.  Same stuff, slightly different profile (center piece has those wings in the cutout to help with tapping)


---
**D Rob** *December 04, 2014 23:37*

What is the center hole size?


---
**Ethan Hall** *December 05, 2014 01:33*

Bore Diameter:0.205"

Thats what the website says.


---
**Mike Thornbury** *December 05, 2014 02:56*

 It's from 80/20, not cheap.﻿ [http://www.8020.net](http://www.8020.net)


---
**Ethan Hall** *December 05, 2014 02:57*

**+Mike Thornbury**​ Where do you suggest I purchase it from?﻿


---
**Mike Thornbury** *December 05, 2014 03:01*

The link you provided has misnamed it. It's 20-series, not 10-series.



Dunno where you should buy it, don't live in your country.



I pay $15/M for 2040﻿



By 'not cheap' I meant, 'good quality'. At just over $10/M that's pretty good.


---
**D Rob** *December 05, 2014 04:50*

I suggest **+MISUMI USA**​​ they have accurate cuts to save time and effort. The 3 things any task take are: time, money, and effort. More of one decreases the amount of the others needed. If i put more time and effort in cutting and taping myself then it's cheaper. More money and **+MISUMI USA**​​ will drill and thread for you. Take parts out if the box and assemble. ﻿


---
**Mike Thornbury** *December 05, 2014 06:28*

I would love to use Misumi, but over here they are... Difficult. They won't ship to me. I can buy anything I want, but have to organise shipping from Sg or My to here.... Not easy when you aren't a corporate. Locally FedEx and DHL won't give me an account, because I am not a 'company', but I am not allowed to be a company (wrong sort of visa) and yet I do about $6k with DHL alone each year, which they acknowledge is more than 50% of their customers.



Welcome to the Asian mindset...


---
**D Rob** *December 05, 2014 06:43*

That sucks what country are you in?


---
**Mike Thornbury** *December 06, 2014 02:17*

Brunei


---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/hWrxgy7h2ZK) &mdash; content and formatting may not be reliable*
