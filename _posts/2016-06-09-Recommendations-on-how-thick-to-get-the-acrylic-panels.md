---
layout: post
title: "Recommendations on how thick to get the acrylic panels?"
date: June 09, 2016 21:03
category: "Discussion"
author: jerryflyguy
---
Recommendations on how thick to get the acrylic panels? I'm assuming 1/8 is enough or?





**jerryflyguy**

---
---
**Derek Schuetz** *June 09, 2016 21:19*

Are you talking for Herculien?


---
**jerryflyguy** *June 09, 2016 21:20*

Sorry, I was meaning the Eustathios V2


---
**Derek Schuetz** *June 09, 2016 21:21*

1/8 will be fine if you have the option though and are willing to spend more I'd do poly carbonate. Acrylic scratches easily 


---
**Eric Lien** *June 09, 2016 21:29*

1/8" will tend to be floppy. on large panels they will act like a diaphragm and amplify the noise. I would go thicker. 



If cost is a concern use 1/4" fiber board and prime/paint it. 


---
**jerryflyguy** *June 09, 2016 22:12*

Ok, 1/4" it is.. Sent the file to the laser cutter, we'll see what it costs.. Might start out as fiberboard until I am able to refill the built budget


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/bC3Gvj4bMgg) &mdash; content and formatting may not be reliable*
