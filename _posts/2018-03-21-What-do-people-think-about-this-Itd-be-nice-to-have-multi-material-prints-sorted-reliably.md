---
layout: post
title: "What do people think about this? It'd be nice to have multi material prints sorted reliably"
date: March 21, 2018 23:24
category: "Discussion"
author: Oliver Seiler
---
What do people think about this?

It'd be nice to have multi material prints sorted reliably.

[https://e3d-online.com/blog/2018/03/21/tool-changer-q](https://e3d-online.com/blog/2018/03/21/tool-changer-q)





**Oliver Seiler**

---
---
**Eric Lien** *March 21, 2018 23:54*

It looks interesting. They show the repeatability in z on the dial indicator. I would like to see repeatability in xy also.


---
**Scott Hess** *March 22, 2018 05:02*

I have to think that if they have the kind of precision they'd need, you could also do interesting things with the Prusa machine in the slicer.  For instance, given good enough ability to return to an xyz spot, you could plausibly print 5 layers at a time in one material before having to hit the purge tower, which would reduce waste by 80%.


---
**Daniel Kruger** *March 22, 2018 13:24*

Similar to [http://www.eclips3d.com/](http://www.eclips3d.com/)

[eclips3d.com - Eclips3D](http://www.eclips3d.com/)


---
**Bruce Lunde** *March 23, 2018 01:42*

I think it is very cool, but I doubt I could afford it.  The idea of a second unit on mine will be enough, but I can see that it would be a great tool if you are printing things for a fee, it would give good capabilities for that.


---
**Oliver Seiler** *March 23, 2018 01:46*

I've been printing many parts for a client using a combined PET/TPU process using a Chimera and while it's been working well, it requires manual oversight and nozzle ooze removal despite using a purge area. Making multi material processes work autonomously would be a great benefit for this type of work.


---
**Ryan Carlyle** *March 23, 2018 02:11*

This is CRAZY SIMILAR to Carl Raffle's Clone R1. (Which is a good thing. He stopped making them after the beta run.) 


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/A23dDDmJQuV) &mdash; content and formatting may not be reliable*
