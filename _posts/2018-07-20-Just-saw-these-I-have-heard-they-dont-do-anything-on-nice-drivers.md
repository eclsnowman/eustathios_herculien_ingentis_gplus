---
layout: post
title: "Just saw these. I have heard they dont do anything on nice drivers"
date: July 20, 2018 12:08
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Just saw these. I have heard they don’t do anything on nice drivers.



I have both 2224 and Bigfoot 2660 drivers (going to go back to the Bigfoots for that nice 256 microsteping) 



I also have prints where ringing is very apparent...

Will these fix that or should I switch to the Bigfoot drivers?



Running the 2224 in the louder mode (not steathchop)





![images/d2124d31ea380ed74a13f2afe584f2cf.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d2124d31ea380ed74a13f2afe584f2cf.png)
![images/9537d7208a65ea4782ff7c30be6cecb3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9537d7208a65ea4782ff7c30be6cecb3.jpeg)
![images/e96818f1e5023d5b04e228f4af2b7a9c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e96818f1e5023d5b04e228f4af2b7a9c.jpeg)

**Stefano Pagani (Stef_FPV)**

---
---
**Stephanie A** *July 20, 2018 13:33*

Ringing typically isn't caused by the drivers. You can fix it better with better belts, higher resolution motors, and a better frame. The rest is settings, jerk and acceleration.


---
**Ryan Carlyle** *July 20, 2018 14:14*

The smoothers only help if your driver has issues with microstepping current control. A lot do, to varying degrees. 8825s are really bad on 24v and can have issues on 12v too, 498x have minor issues on 24v, etc. You can tell if that's the case by printing a big cylinder VERY slowly, like 5 mm/s. The photos here suggest you may get some benefit, but I don't know how fast you printed it or whether there might be another cause of the apparent stair-stepping like a coarse microstep size or something. 


---
**ekaggrat singh kalsi** *July 21, 2018 23:48*

this is not ringing . i had a similar issue on my corexy and i couldn't track it for a long time until recently when i updated my frame with better clamps.. the issue was that when the belts were tightened they would pull the linear bearing on the y axis rods slightly . so the bearing were getting preloaded a lot in one direction .. the bumps were very slight but still visible.. another thing that causes it is trying to run the extruder faster than it can push the filament smoothly .. that creates a pulsating appearance on the surface.. print slower and the extruder issue goes away .. both issues look like ringing and are a big PITA to track!


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/4e1Yv67Fcnn) &mdash; content and formatting may not be reliable*
