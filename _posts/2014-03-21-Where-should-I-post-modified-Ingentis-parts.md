---
layout: post
title: "Where should I post modified Ingentis parts?"
date: March 21, 2014 03:24
category: "Discussion"
author: Brian Bland
---
Where should I post modified Ingentis parts?  Should I post on Youmagine as a deviation of the Ingentis?  I modified the Z axis parts for 12mm shafts and LMU12 linear bearings.





**Brian Bland**

---
---
**Tim Rastall** *March 21, 2014 05:42*

Yeah,  Youmagine is the place at present. 


---
**Jarred Baines** *March 21, 2014 12:31*

Id say if its a single mod, post it as "modified ingentis z axis” rather than post a complete machine.



Post link here if u can, I might like that mod :-)


---
**Brian Bland** *March 22, 2014 18:56*

Just posted the Z axis parts for 12mm shafts and bearings.



[https://www.youmagine.com/designs/ingentis-12mm-z-axis-parts](https://www.youmagine.com/designs/ingentis-12mm-z-axis-parts)


---
**Jarred Baines** *March 23, 2014 01:33*

Cheers **+Brian Bland**


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/8t45WRKhokR) &mdash; content and formatting may not be reliable*
