---
layout: post
title: "Has anyone tried printing PETG through an E3D Volcano?"
date: November 03, 2015 01:01
category: "Discussion"
author: Zane Baird
---
Has anyone tried printing PETG through an E3D Volcano? I've had great results with PETG on my other printers with an E3D V5 and V6, but no matter what I adjust I just can't get rid of ooze printing it through the Volcano. 





**Zane Baird**

---
---
**Alex Lee** *November 03, 2015 02:15*

I haven't tried PETG with a Volcano, but I'm very familiar with the oozing you mentioned.



I'm trying a 0.3mm nozzle for PETG with the theory that it'll cause less oozing, and while I think it does, I wasn't sure if it was just further calibration that fixed it, or if the nozzle size had indeed helped.



Based on your experience with the Volcano, now I think using a smaller nozzle for PETG does indeed help with oozing.



I'm sorry this doesn't help you, but it did help me. Thanks!


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/6ttxv3rAjDC) &mdash; content and formatting may not be reliable*
