---
layout: post
title: "Eric Lien was correct. I had to fix my belt system from the cross over to one on top and one on bottom"
date: May 02, 2014 08:01
category: "Discussion"
author: Wayne Friedt
---
**+Eric Lien** was correct. I had to fix my belt system from the cross over to one on top and one on bottom. Made these little height extenders for the carriage. The motors should moved out about 6mm or so but that would require a new top piece. I will try her again and see how she goes.



![images/338744378687b02afb61031e8852c0a3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/338744378687b02afb61031e8852c0a3.jpeg)
![images/d3ff8c2f60394b2e8f8cb11b41f686d4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d3ff8c2f60394b2e8f8cb11b41f686d4.jpeg)
![images/ac900cb919000acdeda01ec6cc998895.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ac900cb919000acdeda01ec6cc998895.jpeg)
![images/a593d02e40ee497e226c966757931374.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a593d02e40ee497e226c966757931374.jpeg)
![images/972a38647d9c0a445c175e3cf981d59a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/972a38647d9c0a445c175e3cf981d59a.jpeg)

**Wayne Friedt**

---
---
**Eric Lien** *May 02, 2014 11:29*

Glad to help. Belt alignment, belt tension/balance and component rigidity have been my hardest battles with corexy to maintain accuracy.


---
*Imported from [Google+](https://plus.google.com/+WayneFriedt/posts/aAN3HsAy5nv) &mdash; content and formatting may not be reliable*
