---
layout: post
title: "I am sourcing to build a Ingentis, can I use a RAMPS 1.4 board?"
date: March 14, 2014 01:59
category: "Discussion"
author: BoonKuey Lee
---
I am sourcing to build a Ingentis, can I use a RAMPS 1.4 board? What is the recommended board?





**BoonKuey Lee**

---
---
**D Rob** *March 14, 2014 02:18*

yes you can. But use what ever you feel most comfortable with. This design is so close to the ultimaker I opted for their board 1.5.7 in a sainsmart clone with arduino mega clone from them as well. geeetech makes a cheaper board. I have one for one build. The sainsmart version isnt in yet but I will give a comparison when I get it. I like the ultiboard because with little modification it is 24v ready. Switch the 7812 for a switching regulator or put a heat sink and fan blowing toward it and a dc/dc ssr for heated bed. this allows faster heat times and thinner wire that wont get hot


---
*Imported from [Google+](https://plus.google.com/105655952080130147525/posts/KenWX4zVWKb) &mdash; content and formatting may not be reliable*
