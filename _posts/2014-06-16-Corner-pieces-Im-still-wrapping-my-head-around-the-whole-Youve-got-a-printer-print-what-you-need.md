---
layout: post
title: "Corner pieces: I'm still wrapping my head around the whole 'You've got a printer , print what you need!'"
date: June 16, 2014 12:45
category: "Discussion"
author: Mike Miller
---
Corner pieces: 



I'm still wrapping my head around the whole 'You've got a <i>printer</i>, print what you need!'



I was planning on machining aluminum for corner bracing (1/8" or 1/4" plate) but I could probably get the structure I need from PLE...question is: What would you recommend as necessary for infil? Sure, I could go 100%, but that seems like overkill. 





**Mike Miller**

---
---
**Wayne Friedt** *June 16, 2014 12:53*

Not sure what PLE is but 25% is pretty strong. 40% with 4 shells is very strong.


---
**Mike Miller** *June 16, 2014 12:57*

Sorry, pla...too much blood in my caffeine system. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/SbTHbSERzAs) &mdash; content and formatting may not be reliable*
