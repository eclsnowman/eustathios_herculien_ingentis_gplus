---
layout: post
title: "Help! I am really stuck with my printer"
date: December 01, 2016 21:58
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Help! I am really stuck with my printer. I have reprinted all the parts from PETG and spent hours heat fitting the bearings in the plastic while it was on the rod. Everything seemed perfect, I could blow the mount from side to side on the rod. Then I put everything on the printer, used to spacers to space the pulleys off, tensioned all the belts exactly the same, first pretty tight, the really loose. I loosened the set screws on the belt pulleys then used the alignment thingys to make everything true. It take around 5in lbs of force to turn the pulley where the stepper connects to without the stepper on it. The stepper wont budge it (obviously). What am I doing wrong? Is it possible that someone could facetime me or give me advice? I spent 4 total hours doing the carriage this time, and everything felt just right until I assembled it all. I'm thinking about buying the same bushings that **+Walter Hsiao** used on his build. I am using my cleaned of design of Eric's Chimera carriage. I can push the carriage around with my hand, but it takes allot of force. 





**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *December 01, 2016 22:57*

Can you do a hangout instead. Or heck I can make it a YouTube live event. This question comes up all the time and it might be nice to have a visual that other people can reference to when they get to this point in the build. I will make no illusions, you are now at the hardest part of the build... But we will get you through it.



I need a little bit of time so I could set it up but I would definitely do it.



What you are experiencing now is the fun and thrills of stack tolerance error. No part is perfect and so using the alignment tools is just the first step in getting everything to find its equilibrium. From there you need to go around and loosen and tighten things, move things, etc such that the inherent errors in each part find entropy within the system (lowest friction state as the sum of all parts errors). I will show what I do. I will not claim it is the only way. Or even the best way. Just that it is my way and has worked well for me in the past.


---
**Markus Granberg** *December 01, 2016 22:59*

What bearings are you using? Ball bearings or bushings? What rods (hardness)? Bushings are very sensitive to misalignment! Ball bearings are more forgiving but needs hardened rods. I think your problem is over constrained bearings. I made a post about a more forgiving way of munting bushings way back. 


---
**Markus Granberg** *December 01, 2016 23:02*

[https://plus.google.com/108624439407822232001/posts/AKPFfsh3m5x](https://plus.google.com/108624439407822232001/posts/AKPFfsh3m5x)


---
**Sean B** *December 02, 2016 00:23*

I had huge issues with binding.  I ended up run my cross rods in a drill full blast while sliding up and down for about an hour.  I would also suggest gently adjusting the cross rods to align the bushings in the carriage.


---
**Stefano Pagani (Stef_FPV)** *December 02, 2016 01:00*

Yeah i'm using the Misumi hardened rods and brass bushings.




---
**Stefano Pagani (Stef_FPV)** *December 02, 2016 01:02*

**+Sean B** Thats a good idea, I have a 400V stepper that I have and I could smash the carriage around :P


---
**Stefano Pagani (Stef_FPV)** *December 02, 2016 01:02*

**+Markus Granberg** Thanks for the post! Ill get reading :)




---
**Stefano Pagani (Stef_FPV)** *December 02, 2016 01:03*

**+Eric Lien** Yeah that would work great! Tell me when your ready! Ill pm your through the chat.


---
**Eric Lien** *December 02, 2016 01:06*

I can't do tonight. But I will try and setup something for soon. My wife is taking the kids out for the night on Saturday. I could do Saturday afternoon, or evening.



I will purposely mess up my printers alignment, then take you and whoever wants to attend how I would go about correcting it.


---
**Eric Lien** *December 02, 2016 13:10*

Looking like Saturday evening is on. I need to read up on doing a hangouts on air via YouTube live. But it will be some time around 7:30. Once I know more I will post a link.


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/br6QiGu7k6c) &mdash; content and formatting may not be reliable*
