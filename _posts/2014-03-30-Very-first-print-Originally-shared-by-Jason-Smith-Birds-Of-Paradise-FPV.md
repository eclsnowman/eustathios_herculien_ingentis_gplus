---
layout: post
title: "Very first print! Originally shared by Jason Smith (Birds Of Paradise FPV)"
date: March 30, 2014 04:00
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
Very first #Eustathios  print!﻿



<b>Originally shared by Jason Smith (Birds Of Paradise FPV)</b>





**Jason Smith (Birds Of Paradise FPV)**

---
---
**Dale Dunn** *March 30, 2014 04:15*

Looks great for a first print. What are the vital stats? layer height, speed and acceleration, etc.


---
**Jason Smith (Birds Of Paradise FPV)** *March 30, 2014 04:24*

Layer Height = .1mm

Speed = 60mm/s

Acceleration = 3000 m/s^2

Material = PLA

Temp = 220 C


---
**Riley Porter (ril3y)** *March 30, 2014 04:54*

My ultimaker did not print this well for the first print. That is awesome.


---
**Eric Lien** *March 30, 2014 05:42*

I posted on the public link for this already. But I am pumped to build it. I am about 50% done with the printed parts, and all the misumi parts are on the way.  Just need to order the sdp-si bits and decide on my electronics.


---
**Tim Rastall** *March 30, 2014 06:10*

Very nice Jason.  I know you revised many of the parts to suit your purposes but I still get a kick out of seeing another bot working that in some way was derived from mine :).


---
**Riley Porter (ril3y)** *March 30, 2014 12:43*

**+Tim Rastall** agree.


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/cY3q9cdxQig) &mdash; content and formatting may not be reliable*
