---
layout: post
title: "I'm ordering up my parts for my build and I'm stuck deciding between 8mm and 10mm rods for the gantry"
date: October 23, 2017 17:37
category: "Deviations from Norm"
author: Ryan Fiske
---
I'm ordering up my parts for my build and I'm stuck deciding between 8mm and 10mm rods for the gantry. Is there any downside to moving to 10mm? If I wanted to attempt direct drive I know it's recommended, and I am having trouble sourcing the 8mm bronze bushings anyways so unless I'm missing a negative, it seems like it'd be maybe easier to just go with 10mm. I'm just concerned there's a negative that I've missed.





**Ryan Fiske**

---
---
**Eric Lien** *October 25, 2017 22:25*

You will be fine with 10mm cross rods. They run fine on HercuLien with no limitations on speed, and several members have done it on Eustathios. Excited to see another printer beginning its build process. Please post updates as you proceed with your build. I love seeing new printers come together.


---
**Eric Lien** *October 25, 2017 22:29*

Btw sorry your post got caught in googles spam filter for a few days.


---
**Sean B** *October 26, 2017 00:17*

I actually would go with 10mm if I were to build again.  Little too much flex in 8 sometimes.


---
**Ryan Fiske** *October 26, 2017 15:44*

Thanks for the input! Eric, is there something I'm doing that's causing my posts to go to spam? Or is this a common thing? It's happened before to one of my posts so I'm curious if I'm too blame.


---
**Eric Lien** *October 26, 2017 20:48*

**+Ryan Fiske** Nothing you are doing. Google is just aggressive with spam filtering. I have to delete around 20 spam posts per day out of the filter. If I forget to look... they build up.




---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/87JUVffRmVE) &mdash; content and formatting may not be reliable*
