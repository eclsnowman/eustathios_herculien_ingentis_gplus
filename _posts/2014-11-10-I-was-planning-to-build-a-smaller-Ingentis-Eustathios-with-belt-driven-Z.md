---
layout: post
title: "I was planning to build a smaller Ingentis / Eustathios with belt driven Z"
date: November 10, 2014 15:53
category: "Discussion"
author: hon po
---
I was planning to build a smaller Ingentis / Eustathios with belt driven Z. Not as small as the mini proposed by Miguel Sánchez, but housing the common MK2 heated bed. So I am really interested when Richard Horne posted the Sli3DR files.



I used FreeCAD mainly, so imported stl of the XY gantry to study:

[https://drive.google.com/file/d/0B4bLJHgWDzxfNDFQaWtZUDBtbm8/view?usp=sharing](https://drive.google.com/file/d/0B4bLJHgWDzxfNDFQaWtZUDBtbm8/view?usp=sharing)



Compared to the medium Eustathios that I planned, note some major difference that affect my choice of design (please correct me if I am wrong).



1. Similar footprint Eustathios can have build area of 200x200mm. The Sli3DR carriage has motion range of 177x123mm.



2. Sli3DR can be build with very cheap parts. Eustathios requires self-aligning bushing, which is a bit more expensive (especially for us that need to shop oversea), and also more demanding on linear shaft quality.



3. Theoretical XY step of Sli3DR is 56.59 for a common 16 microsteps setup. Z steps with 5.18 geared motor is 293.13, does that mean Z artifact for metric layer height?



4. I guess the Sli3DR has similar speed to coreXY, slower than Eustathios??

![images/65904b8a06747d5c9565b61492ca4b9c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/65904b8a06747d5c9565b61492ca4b9c.jpeg)



**hon po**

---
---
**Miguel Sánchez** *November 10, 2014 17:39*

I am really interested on the comparison too. It seems Eustathios will be more efficient in terms of print surface vs printer envelope.  I do not think Z-axis is going to cause artifacts but I am eager to learn more about it. No idea about which can be faster.


---
**Eric Lien** *November 10, 2014 20:17*

I have found real world "faster" is a function of weight, rigidity, and extrusion. Work on those items and this design could indeed be quicker than Eustathios.﻿


---
**hon po** *November 11, 2014 02:18*

**+Miguel Sánchez** I'm also interested to see how it performs. BTW, your mini is really interesting, I see you are not using self aligning bushing, any problem with fast motion? Your hotend mount is very simplistic, what hotend do you have in mind? No fan required?



**+Eric Lien** I actually base my assumption on your conclusion that your Eustathios "Spider" is much faster than your CoreY. So for a similar rigid frame, with similar weight (both moving 2 smooth rods + hotend), do you expect similar speed from both design?


---
**Mike Thornbury** *November 11, 2014 02:20*

Inertia is a bitch!



Different controllers are going to give different results. The difference between a simple GRBL-equipped Arduino Uno and a TinyG or Mach3, where they have spent years optimising the code to better handle jerk and acceleration, as well as the app used to create your G-code in the first place, are going to have as much of an input to top speed as mechanical design (once you have reached a certain point, obviously).



I am looking to reduce weight of all moving parts - specifically the heavy steel rods. I have some immensely strong and light carbon fibre tubes - if only I can work out a bearing that doesn't eat away at the surface, I think I will be able to significantly reduce the inertia of the x-y axes.



On the subject of print area vs printer volume, I agree - I think there is a lot of 'wasted' space in the Sli3dr, but prefer not to look at it as an error, but as an aid to assembly. Using printed parts the way Rich has, means that assembling the machine and routing the drive belt needs a bit of wiggle-room. I don't intend using threaded rod for a z-axis and thus can make the whole machine a lot slimmer. I was initially looking to make a Tantilus variant, but coincidentally Rich released the Sli3er drawings on the day I was starting my redesign of Tantilus, so that went straight in the bin.



I think the potential for a small, accurate, robust, yet portable printer is in there just waiting for me to bring it out :) 


---
**Mike Thornbury** *November 11, 2014 02:28*

<i>3. Theoretical XY step of Sli3DR is 56.59 for a common 16 microsteps setup. Z steps with 5.18 geared motor is 293.13, does that mean Z artifact for metric layer height?</i>



This confused me - yesterday, coincidentally, I worked out the stepping for my available drive method and steppers and controllers on hand.



For a Z-axis I have 0.9deg steppers and 2mm GT2 belting with 16-tooth sprockets and GRBL-based controller - which made the calculation: 400steps/rotation, 32mm/rotation, 0.08mm/step, 8 microsteps possible, thus .01mm/step accuracy.



What is '293.13'? And the 'Theoretical XY step' is surely down to the type of stepper used and the size of the pulley?



Although, I am very much a novice in the realms of 3D printing.


---
**hon po** *November 11, 2014 02:45*

**+Mike Thornbury** I use Prusa calculator for sanity check:

[https://www.google.com.hk/#q=reprap+calculator](https://www.google.com.hk/#q=reprap+calculator)



Sli3DR use 18mm diameter printed spool. 1.8deg stepper are commonly used. So I calculated 56.59, the spectra line actually wound longer per revolution as line thickness cause wound diameter bigger than spool diameter by half of spectra line thickness. But each printed spool may be different, so the step size need to be calibrated anyway.



The Z use 5.18:1 planetary gear motor, so 5.18x56.59 =293.13


---
**hon po** *November 11, 2014 02:51*

I also don't view the waste space as bad design decision, it's just a compromise when you design to meet conflicting goals.


---
**Mike Thornbury** *November 11, 2014 03:00*

Where do you find these planetary gear motors? I have never come across them before.


---
**Mike Thornbury** *November 11, 2014 03:10*

**+hon po** Thanks for the link to the calculator - I always do it by hand, but it's nice to see I am right :) So 293.13 = .29313mm/step.



The other thing I am confused about - your steppers reduce the ratio, correct? So your 5.18 should divide the number of mm/step, not multiply it?


---
**Eric Lien** *November 11, 2014 03:12*

**+Mike Thornbury** [http://www.robotdigg.com/product/103/Nema17-40mm-Stepper-Gearmotor](http://www.robotdigg.com/product/103/Nema17-40mm-Stepper-Gearmotor)


---
**Mike Thornbury** *November 11, 2014 03:18*

Thanks **+Eric Lien**  So that's a 1.8deg motor with a 5.18 reduction ratio, giving .35deg/step (3x more accurate than my .9deg/step motors).



So, using **+hon po**'s calculation from above, he would get 1036 steps per rotation, giving a result of .71, not 293.13...



What am I missing?


---
**hon po** *November 11, 2014 04:18*

If using 16 microsteps => 16576 microsteps per rotation. 18mm diameter spool has circumference (18*pi) 56.5487mm.



16576 steps per 56.5487mm => 293.1279 steps per mm.


---
**Mike Thornbury** *November 11, 2014 06:14*

I must have been looking at it all wrong... I thought the figure was micrometer per step :)



Given an extruded filament of .2mm, do you really need to be using a microstep figure of 1/16?


---
**Mark Hindess** *November 11, 2014 08:06*

I suspect you could improve print area by modifying the x carriage and/or x ends. I'm considering making a printer with the same unnamed motion arrangement (and identical Z drive)  but I'm thinking about using a vertical x rod arrangement. Obviously, I lose a little in Z but I have a delta when I need more Z. I'm interested in the answers to some of the other questions. Thanks for raising them.


---
**hon po** *November 12, 2014 01:44*

**+Mike Thornbury** I thought I read somewhere of quieter & smoother movement (less vibration) when number of microsteps increased.



**+Mark Hindess** I think you may gain some more build area but not much. It look pretty optimized already. Unless you could throw away the 2020, or rearrange the V groove pulley.



What advantage for a vertical rods arrangement?


---
**Mike Thornbury** *November 12, 2014 08:51*

I think 'smoother' is more a function of the controller's code. Hence the TinyG and Smoothstepper claims for smoother/better performance, while using larger microsteps (like 1/8, 1/4).


---
**Mark Hindess** *November 12, 2014 10:36*

**+hon po** I'm not sure vertical rods are better but I don't think it will be significantly less stable. I'm moving the two systems of bearings/spectra so that one is below the other - which should also give more space - and I think that the vertical rods will fit better with this arrangement. I've not got as far as the x-end design yet though so I might still change my mind. I'm a little tempted to use a prusa i3 rod spacing and a compatible carriage so I can borrow from the extensive selection of hot end mounts but this might add to much weight.


---
*Imported from [Google+](https://plus.google.com/111735302194782648680/posts/QCqeJC1ttpT) &mdash; content and formatting may not be reliable*
