---
layout: post
title: "which version of marlin you use ? Ramps 1.4"
date: August 22, 2016 13:31
category: "Discussion"
author: Lopez Tom
---
which version of marlin you use ? 

Ramps 1.4





**Lopez Tom**

---
---
**Eric Lien** *August 22, 2016 15:20*

Which printer? HercuLien? Eustathios?



My personal printers now all run smoothieware based boards by Panucatt Devices. I run an Azteeg X5 Mini V3 on me Eustathios, and an Azteeg X5 GT on my Herculien (sorry this board is not yet officially released).



My HercuLien used to run an Azteeg X3 which served me well for many years.


---
**Lopez Tom** *August 22, 2016 16:09*

eustathios V2




---
**Eric Lien** *August 22, 2016 16:29*

I recommend the Azteeg X5 mini V3 if you don't already have a controller.


---
**Eric Lien** *August 22, 2016 16:29*

But that's not Marlin. That's smoothieware.


---
**Roland Barenbrug** *August 22, 2016 19:02*

Running Repetier firmware on a Arduino Due / RADDS 1.5 combo. Started with Marlin on an 8-bit AVR/Ramps combo and eventually gave up. Bottom line not fast enough when using a high degree of micro stepping.


---
**Lopez Tom** *August 24, 2016 10:05*

what is the difference between arduino + ramps and the Azteeg for example ? That improve your quality ?




---
**Lopez Tom** *August 25, 2016 15:08*

**+Eric Lien** ? :)


---
**Eric Lien** *August 25, 2016 17:33*

Ramps is 8bit controller, smoothieware board like an Azteeg x5 mini v3 is a 32bit board. 8bit boards are perfectly acceptable at slower speeds. But as you speed up you will eventually run out of computational power and motion can stutter resulting in poor print quality.



IMHO if I was building a new printer today I would only use a newer style controller. The cost difference is negligible and you have a setup with better future proofing. Also smoothieware has an easier configuration method, and as of recent updates even better acceleration/motion planning.


---
**Lopez Tom** *August 26, 2016 11:17*

Thank you for you explication :)


---
*Imported from [Google+](https://plus.google.com/107612178038097709041/posts/3KNnYyesH6D) &mdash; content and formatting may not be reliable*
