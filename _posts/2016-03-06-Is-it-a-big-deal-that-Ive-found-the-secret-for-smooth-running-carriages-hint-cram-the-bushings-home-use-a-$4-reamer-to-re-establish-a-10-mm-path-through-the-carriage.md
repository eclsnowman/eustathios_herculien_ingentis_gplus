---
layout: post
title: "Is it a big deal that I've found the secret for smooth running carriages (hint, cram the bushings home, use a $4 reamer to re-establish a 10 mm path through the carriage)"
date: March 06, 2016 15:36
category: "Discussion"
author: Mike Miller
---
Is it a big deal that I've found the secret for smooth running carriages (hint, cram the bushings home, use a $4 reamer to re-establish a 10 mm path through the carriage).



Is it worth writing up? Because it's been a problem that's plagued me for years, and now it's a non-issue. Works for IGUS as well as bronze...I don't want to spend 10 minutes of your time with a 20 second solution...Youtube is full of that.

![images/80c1b59455505fc4095bc9fbc23fa79b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/80c1b59455505fc4095bc9fbc23fa79b.jpeg)



**Mike Miller**

---
---
**Daniel F** *March 06, 2016 16:00*

What are you trying to tell us? If it's about revealing the secret of smooth running carriages--yes.


---
**Mike Miller** *March 06, 2016 16:25*

alright, Keep your eyes peeled. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/h2kTUsnCCjR) &mdash; content and formatting may not be reliable*
