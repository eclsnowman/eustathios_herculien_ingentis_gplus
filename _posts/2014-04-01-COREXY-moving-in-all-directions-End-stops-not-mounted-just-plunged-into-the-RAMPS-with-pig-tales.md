---
layout: post
title: "COREXY moving in all directions. End stops not mounted just plunged into the RAMPS with pig tales"
date: April 01, 2014 04:04
category: "Show and Tell"
author: Wayne Friedt
---
COREXY moving in all directions. End stops not mounted just plunged into the RAMPS with pig tales.



![images/e3405e78b0993c9c5a83088c2bac1f23.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e3405e78b0993c9c5a83088c2bac1f23.gif)



**Wayne Friedt**

---
---
**Brian Bland** *April 01, 2014 04:19*

Looks nice.


---
**Eric Lien** *April 01, 2014 04:34*

Great job Wayne. Let me know how circles turn out. For some reason I really fight that on mine. I think I need to come up with a better tensioning system.


---
**Eric Lien** *April 01, 2014 04:38*

Also it looks like frame buckle is getting you at the corners where the triangles I mentioned before applies a torque due to the height transition to the idler.


---
**Wayne Friedt** *April 01, 2014 04:49*

Looks like just the slights buckle on the left side. Very hard to detect. I didn't even see that until you said. Tricky to get everything going in the correct directions and then make the endstops work in the corners you want.  Next go around i will try over and under. No calculation of steps yet, just movement 


---
**Martin Bleakley** *April 01, 2014 05:21*

I like this, almost makes me want to re-design my printer :)


---
**Wayne Friedt** *April 01, 2014 05:26*

Following mine may not be best choice as i am no expert or 3D printer authority. 


---
**Eric Moy** *April 01, 2014 09:10*

Awesome!


---
**Wayne Friedt** *April 01, 2014 09:38*

Thanks guys


---
*Imported from [Google+](https://plus.google.com/+WayneFriedt/posts/iMiLQUKBgey) &mdash; content and formatting may not be reliable*
