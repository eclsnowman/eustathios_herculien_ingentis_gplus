---
layout: post
title: "As a PSA, I had a weird issue with my x.y axis surging"
date: September 23, 2017 22:48
category: "Discussion"
author: Sonny Mounicou
---
As a PSA, I had a weird issue with my x.y axis surging.  Its weird to describe, but it would almost pause for half a second, then continue.  The steppers weren't skipping, so it was a bit confusing.  I swapped motors, stepping level, digipot levels, etc. but nothing helped.  I fixed it by removing the dampers.  What would happen is that when the axis got to a sticky spot, it would cause the damper to bend until the motor applied enough torque to overcome it.  



I'm guessing after things break in, the problem might go away.  I've been fighting this issue for about a week and it has been driving me crazy.  I'm quite happy to be over it ...





**Sonny Mounicou**

---
---
**Eric Lien** *September 26, 2017 13:01*

Yeah, there is a "stick/slip" that can occur on plastic bushing style bearing surfaces. And it gets exaggerated by any misalignment or racking forces. The difference between the coefficient of static vs kinetic friction between the rods and the plastic bushings is why I tend to use the bronze bushing. 


---
**Sonny Mounicou** *September 26, 2017 13:36*

I started with bronze bushings and had nothing but problems with them.  The IGUS are a bit better for me.  They do not like skin oil on the rods though.  I found that was one easy way to inadvertently add friction.  Oh, and if you happen to have superglue fumes near the fingerprints, it gets worse :)  


---
**Sean B** *October 11, 2017 01:07*

I tried installing dampers on the steppers to keep everything silent but noticed this same thing along with severe flexing.  Got rid of them within an hour if installing them.


---
*Imported from [Google+](https://plus.google.com/113710966260079086437/posts/8GH7jQEKuz3) &mdash; content and formatting may not be reliable*
