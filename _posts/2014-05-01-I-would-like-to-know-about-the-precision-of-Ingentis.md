---
layout: post
title: "I would like to know about the precision of Ingentis"
date: May 01, 2014 15:19
category: "Discussion"
author: George Salgueiro
---
I would like to know about the precision of Ingentis. Is direct drive more precise than Bowden? Is bushings better than linear bearings. If so where can I find those bushings. I really intent to build my 3d printer.





**George Salgueiro**

---
---
**Mike Miller** *May 01, 2014 15:25*

While some qualitative statements can be made (Bowden is harder to tune for stringing and retract than direct drive), there are often other factors that affect precision. Bad bushings may still be better than okay linear bearings, but it won't matter if extrusion calculations are off or if the bed isn't flat and level to the nozzle, or the e-steps aren't properly calibrated, or the filament has stretched, or...or...or...


---
**George Salgueiro** *May 01, 2014 15:48*

**+Mike Miller** do you prefer Bowden or direct drive?


---
**Dale Dunn** *May 01, 2014 16:00*

For the Bowden tube vs direct drive decision, you have to figure out if you want to prioritize high acceleration over easy retract tuning. Some filaments do not do well in Bowden extruders, such as flexible materials.



A similar question applies to bushings vs linear bearings. Neither one is better, except as it suits your needs.


---
**Mike Miller** *May 01, 2014 16:29*

**+George Salgueiro** Only have experience with Bowden. My prints are stringy. :) Investigating Direct Next. 


---
**George Salgueiro** *May 01, 2014 17:18*

**+Mike Miller** when i look into ultimaker's prints, i see a superior quality than I see in most direct drive prints. 


---
**Mike Miller** *May 01, 2014 17:27*

**+George Salgueiro** Ultimaker is one of the cream of the crop of the current printers and has a GREAT reputation in the Open source Community. You also have to look at who is tuning for what. I'm tuning to a single iteration of a single 3DR Deltabot, Uiltimaker, Prntrbot, Makerbot and the like can tune a single set of factors that scales across thousands (if not hundreds of thousands) of machines. 



I'd love to see a great big catalog of configs that people use succesfully on their printers, but there's a certain pride and Intellectual Property-ness with those parameters. Further, my Bowden with 400 mm tube and an e3D will act differently than a 200 mm tube and J-head PEEK extruder. 


---
*Imported from [Google+](https://plus.google.com/103579216912687360818/posts/7pWiN3ML1Ky) &mdash; content and formatting may not be reliable*
