---
layout: post
title: "Hi Folks! I am new here and have started ordering my parts for Herculien build"
date: May 06, 2015 07:44
category: "Discussion"
author: Vic Catalasan
---
Hi Folks!



I am new here and have started ordering my parts for Herculien build. Is there a forum where people can post build threads so that I can follow along ?



I have built a CNC machine for milling aluminum based on several designs and now interested in building a nice 3D printer. Herculien looks like a great design so I am all in!



Vic





**Vic Catalasan**

---
---
**Eric Lien** *May 06, 2015 15:02*

Most everyone posts here in the google plus group. I have seen some posts on reddit. Also **+Daniel Salinas**​​ did a great job making an assembly guide. I still need to add to it: [https://github.com/eclsnowman/HercuLien/blob/master/Documentation/README.md](https://github.com/eclsnowman/HercuLien/blob/master/Documentation/README.md)





But my new job just started so time will be tight in the coming months.﻿


---
**Vic Catalasan** *May 06, 2015 17:34*

Thanks that document will be very helpful during my build. 


---
**Derek Schuetz** *May 07, 2015 14:54*

I wish you luck Vic it's quite a build


---
**Vic Catalasan** *May 07, 2015 16:38*

Thanks Derek, I have been wanting a 3D printer for quite some time now and have recently decided to build my own. I have seen all kinds of designs and I think this is one of the best one out there. Thanks to Eric and everyone here for providing all the information needed. For sure its going to be a big learning curve as I have no experience with 3D printing. I was thinking of attaching an extruder to my 10"x20" aluminum mill that is 350lbs but it will be painfully too slow to print anything useful. Plus the controller is really meant for CNC milling. 


---
**Gus Montoya** *May 07, 2015 16:40*

Can you link us to Daniel Salinas's build? I never knew it exsisted.


---
**Vic Catalasan** *May 07, 2015 16:40*

Hi Derek, I have notice your from CSULB, I graduated as a 49er many years ago! GO BEACH!


---
**Derek Schuetz** *May 07, 2015 16:41*

Ya sitting in the USU now so close to being done...ive been here to long


---
**Vic Catalasan** *May 07, 2015 16:45*

LOL your lucky.... after that you have to work! .... fun times! Is the bowling alley still there? What you studying? 


---
**Derek Schuetz** *May 07, 2015 16:47*

Oh I'm already working full time been doing it through school, already have a decent job. but ya the bowling alley is still here. Should graduate with a degree in business economics


---
**Eric Lien** *May 07, 2015 17:32*

**+Gus Montoya** here is **+Daniel Salinas**​ got hub fork:[https://github.com/imsplitbit/HercuLien?files=1](https://github.com/imsplitbit/HercuLien?files=1)



Also he has several post if you pull up his profile on g+


---
**Daniel Salinas** *May 07, 2015 17:33*

That reminds me I need to send a pull request on github for my door stay and electronics box


---
**Gus Montoya** *May 07, 2015 17:43*

My apologies, I'm experiencing senior moment. He printed quad copter frames. I do recall now. 


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/ZeywADDDqSz) &mdash; content and formatting may not be reliable*
