---
layout: post
title: "I suppose this is kind of like one of these printers, a big cube"
date: February 04, 2015 22:51
category: "Show and Tell"
author: Chris Purola (Chorca)
---
I suppose this is kind of like one of these printers, a big cube. I'm going to start with CoreXY and then if it doesn't work how I want, i'll move to something different.



<b>Originally shared by Chris Purola (Chorca)</b>



Well I built the frame. Its huge! Got the other two printers there in the pic as well, the larger one would fit inside the frame I just built. It is rock solid and very sturdy, so I'm excited for some good prints.

Next up is to cut and mount the openbuilds v-rail and get the gantry designed as well as sizing up the z motors and rail system to get that aluminum ordered too. Hopefully some more progress soon!



Thanks **+MISUMI USA** for having some awesome aluminum extrusion frame kits available!



(2ft³ build area, hopefully)

![images/777fddf0537a52633e9c641c414951d2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/777fddf0537a52633e9c641c414951d2.jpeg)



**Chris Purola (Chorca)**

---
---
**Ricardo de Sena** *February 04, 2015 22:53*

Great!!!!!! Big!!!!!


---
**Daniel Salinas** *February 05, 2015 00:01*

GREAT SCOTT!!!


---
**Mutley3D** *February 05, 2015 00:30*

wow, looking forward to lifesize prints of a cross legged human :)


---
**Mack Hooper** *February 05, 2015 16:15*

Nice!


---
**Rick Sollie** *February 06, 2015 01:59*

Wow. If you keep going your next one will be humongous! 


---
**Kyle Wade** *February 07, 2015 18:53*

make 3d scan of self holding mini 3d printer, 3d print buhda statue of self﻿ at full scale?


---
*Imported from [Google+](https://plus.google.com/+ChrisPurola/posts/Awh1FjmoMaY) &mdash; content and formatting may not be reliable*
