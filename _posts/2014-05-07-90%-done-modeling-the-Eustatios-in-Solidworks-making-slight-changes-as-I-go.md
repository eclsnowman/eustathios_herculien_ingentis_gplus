---
layout: post
title: "90% done modeling the Eustatios in Solidworks (making slight changes as I go)"
date: May 07, 2014 05:45
category: "Show and Tell"
author: Eric Lien
---
90% done modeling the Eustatios in Solidworks (making slight changes as I go). I am just passing the time while I wait for my steppers to arrive from china to build it. Every nut, bolt, and fitting will be included when I am done. I am using it as a project to learn BOM and print making functions of Solidworks. Once I get completed I can post in multiple formats (step, stl, sketchup, spaceclaim). Let me know if anyone is interested.﻿



EDIT: OK Its up on github now: [https://github.com/eclsnowman/Lien3D_Eustathios_Spider](https://github.com/eclsnowman/Lien3D_Eustathios_Spider)

![images/2d35ec7ec9f534b96422ffdb9ef17127.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2d35ec7ec9f534b96422ffdb9ef17127.jpeg)



**Eric Lien**

---
---
**Daniel Fielding** *May 07, 2014 05:50*

I would interested in looking at it.


---
**Brian Bland** *May 07, 2014 05:53*

Definitely interested.


---
**Daniel Fielding** *May 07, 2014 05:54*

Anything that is free. 


---
**Eric Lien** *May 07, 2014 05:54*

Any particular format? It is modeled in SW 2014 right now.


---
**Eric Lien** *May 07, 2014 05:58*

Just for clarity if anyone doesn't know Eustathios is a **+Jason Smith**  design not mine. I am just making a few mods and he designed it in Sketchup which I have never had much luck with so I decided to model it in the software I know. I only started using solidworks recently... so I can use the practice.


---
**Wayne Friedt** *May 07, 2014 06:03*

I see you are moving away from the COREXY. I am wondering about one smooth rod per side on the Z but i see a lot of other people do this. On my 12x12 i am using 2 Z rods bot on one side only. On my 24 i am using 2 Z rods per side so iam interested to see how this work's out for you. How do you get a accurate measurement for the closed belts. I have found that a 4 point bed leveling system is far harder to level than a 3 point


---
**Eric Lien** *May 07, 2014 06:10*

**+Wayne Friedt** for belts I use the belt function in solidworks. You can either define the belt length and components (with degrees of freedom left in the model) will move to the proper location, or fully define the locations and it calculates the belt length. I am not moving away from corexy. This is just my 2nd printer. I liked Jason's design so much I had to build it. My 24x24 not is still corexy.


---
**Tim Rastall** *May 07, 2014 06:54*

I have an (arguably) more elegant xy end/belt clamp design if you want it. 




---
**Tim Rastall** *May 07, 2014 07:12*

What's the deal with the bed plate? Seems to be needlessly complicated and accordingly expensive to produce..  Or am I missing something? ﻿


---
**Tim Rastall** *May 07, 2014 07:17*

Urhg,  that sounded a bit negative,  not intended to be.  I really like the overall design and it's great to see it in a better cad format.  Nice work **+Eric Lien**


---
**Eric Lien** *May 07, 2014 11:02*

**+Tim Rastall** bed plate is just a mockup for now. It looked close to the bracket and this plate is acting as my heat spreader for the silicone bed. So I mocked up some holes to increase surface areas to dissipate heat... Where a better solution is to move it away from the bracket.


---
**Eric Lien** *May 07, 2014 11:03*

**+Tim Rastall** I would love to see the xy ends you have.


---
**Eric Lien** *May 07, 2014 11:06*

**+Ashley Webster** Let me get another day or two to finish up the design and I will send it out there.


---
**Eric Lien** *May 07, 2014 11:17*

On a side note I am torn on the extruder carriage. Seems like there are a lot of variation on the design with the ingentis versions out there.



I am actually contemplating a modular design where I could run Bowden for speed 90% of the time. But for flexible filament or slower speeds a pg35l geared stepper could mount for direct drive (kind of like the zortrax printer). I am having trouble visualizing the design while keeping it compact and balancing the weight. I would love some ideas if anyone has some.﻿


---
**Tim Rastall** *May 07, 2014 12:52*

I'd avoid pg35l if I were you.  Been there,  killed 2 of them. You'd be better off with a geared down Nema 14. I'd been working on a variant of the airtripper that uses a small closed belt and printed gt2 wheels.  I'll dig it out. 


---
**Dale Dunn** *May 07, 2014 13:03*

I'll definitely have a look at it in SW14 if you make it available. My day job is about 90% SolidWorks, so if you have any questions, go ahead and ask.



Do you plan to print ABS? One thing I've been wondering about with this recent crop of largish T-slot printers (since Ingentis, really) is why they are large enough to be enclosed for ABS, but I don't see that inherently in the design. I see corner brackets used on T-slots in these designs, but wouldn't polycarbonate side panels for an enclosure do the job better?


---
**Eric Lien** *May 07, 2014 13:52*

**+Dale Dunn** I will be enclosing it also. I did that with my corexy. I figure with the corner brackets + panels it will be bullet proof. I have always built things BT asking the question "could I park a car on it" :)



But I don't want to count on the panals for all the strength. My corexy has the panels mounted with wing nuts on the outside, and slot nuts with studs in the extrusion. IMHO ease of maintenance and accessibility is king.﻿



My enclosed corexy reaches thermal equilibrium of 55C in the enclosure when the bed is 110C. This is about perfect for ABS.


---
**Eric Lien** *May 07, 2014 13:57*

**+Tim Rastall** love to see the design of the direct drive ingentis carriage. I am thinking of making it 2x8mm rods horizontal per axis on the gantry if I go direct drive (ala the zortrax design). This is to add rigidity for the added weight on the extruder carriage from the stepper.



Thoughts?


---
**D Rob** *May 07, 2014 17:40*

Have you started building yet?


---
**D Rob** *May 07, 2014 17:50*

**+Eric Lien** a thought just occurred to me that would save on pulleys. If the motor is mounted on the outside with the shaft parallel to the rods one pulley on each (x/y) could be left out. The belt would go, from the motors pulley, above and below the shaft immediately next to it. Connecting to a pulley on the far side smooth rod. The non motor end would remain the same as was to keep the two rods in tandem the night mount could then incorporate a tensioning mechanism (hinge/springs?) beneath it.﻿


---
**D Rob** *May 07, 2014 17:54*

This would also cut out the need of two drive belts


---
**Shachar Weis** *May 07, 2014 18:03*

Sign me up, I'm very interested.


---
**Eric Lien** *May 07, 2014 18:33*

**+D Rob** I think if tension is correct the added belts should not add much slop, and to be honest one of the items I likes best about the Eustatios was how well contained it is within the footprint. To be honest I think it's worth the cost of some belts.But time will tell once I start printing So far all I have is the frame built and parts printed while I wait for the Aliexpress motors to arrive and the New Azteeg X3 Controller to hit availability.



I haven't seen many updates on the progress from **+Jason Smith** . I see he changed his carriage design from dual extruder to single, and appears to have changes to  linear ball bearings verses bronze bushings.



Jason, any input on your experiences so far would be great.


---
**Eric Lien** *May 07, 2014 19:49*

**+Ashley Webster** Bed could be changed to three point level. I use that on my Corexy with a much larger bed. For now I just drew it with Jason's design. As far as springs I still like them. I haven't had to adjust bed level on weeks of consistent printing. But the build plate on my CoreXY is 1/4" thick aluminum which likely helps with rigidity. Plus springs are nice for some give if you crash the extruder into the bed (if you forget to home before moving around.


---
**Tim Rastall** *May 07, 2014 20:46*

**+Dale Dunn** funny you should mention that.  My new design has a dibond shell that isn't critical (strictly speaking) but has been integrated into the design from an early stage.


---
**Tim Rastall** *May 07, 2014 20:55*

**+Eric Lien** I'll dig out the relevant files over the next few days.  I think dual shafts for the cross members on each axis is overkill.  A thicker shaft may work just as well.  May be worth considering 10mm aluminium instead of steel. There is also the aluminium box section/roller bearing option a la Quadrap. 


---
**Tim Rastall** *May 07, 2014 20:59*

Hmm.  Just looked at the Zortrax again.  There is something appealing in the design..  Shaft alignment would be critical.  It's possible printed parts might not have high enough tolerances. 


---
**Eric Lien** *May 07, 2014 22:53*

**+Tim Rastall** I ran into this alignment issue on my corexy with two bearings x 2 shafts. I think one rod with two bearings and one rod with one bearing might do the trick since you only need 3 points to establish a plane.﻿



Looks like its time to play with solidworks stress analysis tools. Looks like I am in for some late night googling on how to use this feature.



﻿**+Shauki Bagdadi** I know you are one of those true blue real engineer types. If I remember correctly I have seen some posts with you showing solidworks force analysis. If I can get the weights, center of masses, position of the stepper in the assembly, etc this should be fairly easy to confirm proper size/orientation of the rods for a well balanced machine to avoid undue deflection?


---
**Jason Smith (Birds Of Paradise FPV)** *May 08, 2014 01:58*

**+Eric Lien**  , First off, great work!  Depending on how far your design branches off, I would probably be willing to host your source files on the Eustathios Github & Youmagine pages.  **+D Rob** ,  So far, I've had no issues with the current belt/pulley setup (other than increased cost), and I'm very happy with the self-contained design.  After all, the setup is very similar to the Ultimaker layout, and it certainly has no quality issues.

On another note, I've been meaning to take a look at **+Tim Rastall**'s xy end/belt clamp design and incorporate it into the Eustathios.  That said, I've had absolutely no problems with the current design.


---
**Tim Rastall** *May 08, 2014 02:11*

**+Jason Smith** I'm sure the belt clamp you are using works fine! And I would have done exactly the same thing in order to minimize the number of from-scratch designs you'd have to do. The one I've designed was a result of starting again from scratch without needing to worry about securing a spectra line and aligning the cross member with the outer support/drive shaft in order to minimize bending moments and increase Z axis usable space - as much as anything it was a response to **+Jarred Baines** talking about how close you can come to a platonic ideal when designing mechanical components.


---
**Eric Lien** *May 08, 2014 10:56*

**+Jason Smith** I plan on building it up as you designed it first. Saving that file. Then making my mods. Only changes so far are small rounding changes on dimensions. I am a sucker for whole numbers. So a dimension of 6.1mm would be changed to 6mm when defining the sketch.



Once I get it done I will get you the model in a configuration as close as possible to "stock" as I can for the Github.﻿ Could you post a close up picture of how you used the thrust bearings and 8mm collar. I guessed but think I got it right. Also the spring bed mounts were a guess too.



Last question is there seems to be no adjustment designed in for spacing between the 10mm Z support rods. Did you see any issue during assembly with the stack tolerances from the bed assembly causing bind when the bed moves up an down?


---
**Jarred Baines** *May 08, 2014 16:39*

You guys are all amazing - longer post when I have time but I would love the SW format files and have seen a few awesome bots with the zortrax "dual rod" design... initially the extra weight seems like an issue but I think the added rigidity is worth it... honestly I'd sacrifice speed for accuracy any day of the week - results from the zortrax are very good... I am really digging the idea of optional bowden/direct drive as well, I've just started a new job at a 3D printer shop and I have to say the most rigid machines have the best performance hands-down... a lightweight bowden setup just doesnt compare to a direct drive sitting on 12mm rods, driven by around 10-12mm belts and even being of a more "makerbot" style x/y the rigidity of those components easily over-compensates for the weight of the x motor and direct drive extruders...



Its been a real eye opener actually... here I was, day one, poo-pooing the design of one of the machines in store and it produced some phenomenal prints...


---
**Tim Rastall** *May 08, 2014 20:45*

**+Jarred Baines** what sort of belts are the 10-12mm ones you're referring to? 


---
**Eric Lien** *May 08, 2014 21:08*

**+Tim Rastall** gt2 belt profile can be purchased in widths other than the 6mm common in today's repraps.


---
**George Salgueiro** *May 09, 2014 00:17*

**+Jarred Baines** which heavy machine is more precise than ultimaker? 


---
**George Salgueiro** *May 09, 2014 00:22*

**+Tim Rastall** do you think direct drive is more interesting than Bowden?


---
**Eric Lien** *May 09, 2014 07:00*

OK its up on github now. Not finished. But getting closer each day. And as of 3min ago my azteeg x3 v2 is on order. Roy has been pretty gracious about me pestering him the last few weeks about the new x3.



[https://github.com/eclsnowman/Lien3D_Eustathios_Spider](https://github.com/eclsnowman/Lien3D_Eustathios_Spider)﻿


---
**Jarred Baines** *May 09, 2014 14:46*

**+Tim Rastall** as **+Eric Lien** says I think theyre gt belts, may be 3 or 5mm pitch, ill have a closer look...



**+George Salgueiro** I wouldnt say it was "more accurate", I was just surprised how much better than the makerbot it is considering its the same carriage style... the bot im speaking of is a profi3dmaker by 3d factories... its fairly expensive - compared to our open source machines, but they are very solidly built, aluminium rather than plastic in most places and thick belts, thick rods, thicker extrusions than ours and dual head direct drive as well as a 1/4" or so bed... its pretty quick considering how much those components weigh and yeilds great quality prints but I havent seen an ultimaker in real life so I can't comment on the quality vs ultimaker...



**+Shauki Bagdadi** I am starting to understand your love of rolling vs sliding... it may have more inertia but so much less friction... with good strong belts to control the movement and a strong frame, bearings sound great ;-)


---
**Dale Dunn** *May 09, 2014 18:03*

**+Shauki Bagdadi** , I don't remember calculating inertia of the small bearings previously (I was just looking at it last night for my extruder carriage, 1.26g each), but I could easily have forgotten. I do remember working out that the 10x10x1 aluminum box tube is both lighter and more rigid than Ø8mm steel rod. Uh, here: [https://plus.google.com/116889746506579771100/posts/c1BbZU2eigq](https://plus.google.com/116889746506579771100/posts/c1BbZU2eigq)


---
**Tim Rastall** *May 09, 2014 20:21*

**+Eric Lien** I think the larger widths are usually gt3 or gt4. Makes like harder as increased pitch usually denotes a larger minimum pulley diameter. 


---
**Tim Rastall** *May 09, 2014 23:59*

**+George Salgueiro** not sure about interesting but they both have their pros and cons,  principally Bowden can't handle flexible filaments like ninja flex. 


---
**Eric Lien** *May 10, 2014 00:29*

Any other formats people want other than solidworks?


---
**Jarred Baines** *May 10, 2014 00:56*

I use inventor which seems to take solidworks files rather well, I'm good and thanks a million for uploading the files **+Eric Lien** 



**+Shauki Bagdadi** I have a full set of ingentis / eustathios compatible parts at my house... don't make me feel bad for not looking into QR first :-P



I forgot that you use hollow tubes, that's great - I've also been looking / thinking about 4 steppers for x/y... If there is a direct drive option, I will take it! Eliminating backlash and other errors gets us closer to that "ideal" situation, the "perfect" machine...



I guess what I was trying to point out with the heavy belt / heavy rod machine I mentioned is that we are constantly trying to modify the machines to be LIGHTER... this generally means introducing weaker components...



I have recently been speaking to the developer of a VERY large and heavy-duty 3D printer (can't say more than that unfortunately but some people may guess who I'm talking about). Picture these two scenarios:



standard ingentis build with 10mm / 8mm rods... to achieve high speeds with these "weaker" sized rods, you need to reduce weight... so we go for a bowden setup, limiting materials and introducing oozing / hysteresis problems...



IDEALLY direct drive extruder gives better EXTRUSION results... but, the added weight can introduce vibrations, loss of steps (limits speed) and other problems.



If you had 20mm rods and 16mm belts (extreme scenario), 50mm frame extrusions etc - you will have much more rigidity and guess what - the extruder DOESN'T change - it still weighs the same, so you basically increase your stability-to-"extruder weight" ratio...



They run like, 25-32mm rods, 25mm belts... MASSIVE steppers on x/y... but a standard e3d direct drive... they could have 4 or 8 of them on a carriage and you wouldn't notice any vibration because it's just SOOO solid...



reducing weight of the head is one direction to take...



Increasing the rigidity of the machine is the opposite direction, more expense, but you don't have to sacrifice components...



Combining the two (using 20mm hollow aluminium for example) may be a winning combo! Haven't done any research further than what I'm throwing out here, but you can only go so far with reducing weight before you start introducing components that are too weak / likely to fail easier...


---
**Tim Rastall** *May 10, 2014 03:44*

**+Jarred Baines** actually,  if you are going for massive scale. V slot and rollers makes much better sense, shafts of the size you describe are much more costly than the more ubiquitous 10-16mm used in cnc. Also,  with a vslot extrusion,  you can buttress a long span or use a, rectangular profile. I also think synchromesh is worth considering as it is so much stronger than belts and with wider (more expensive) belts,  the price difference will become less prohibitive. 


---
**Jarred Baines** *May 10, 2014 04:25*

Totally agree...



Their machine is drastically over-engineered and cost them a mid-range family sedan worth to build...



It just occurred to me when I saw it that you could (for instance) put 4 direct drive hot ends, a laser module and a routing head on that thing :-)


---
**Jarred Baines** *May 10, 2014 04:48*

<b>Downloads design and starts making some parts out of aluminium</b>



Thanks again!!! :-)



Sooo much easier to get dimensions of the parts now!


---
**Jarred Baines** *May 10, 2014 05:10*

Oh wait...

I can get this to work in eDrawings but not in inventor at the moment (2013 format)



Are you able to save them as something 2010 or earlier **+Eric Lien**? Or STEP may work, but I know in the past solidworks models come in beautifully for me...


---
**Jarred Baines** *May 10, 2014 07:06*

Bolt the ingentis / qr frame to a solid bench **+Shauki Bagdadi**? Do you think that will make all the difference? I was going to make my frame 950mm high so - if just bolting the bottom to a table will eliminate a lot of problems I will have to try it!


---
**Tim Rastall** *May 10, 2014 07:31*

Worth considering the implications of an enclosure too. 


---
**Jarred Baines** *May 10, 2014 07:38*

Thanks for the info! :-)


---
**Eric Lien** *May 10, 2014 11:59*

**+Jarred Baines** I can do step. But solid works isn't like AutoCAD. They don't let you save back versions... They want planned obsolescence.



For some reason when I save out as step some of the sub-assembly are not inheriting the proper orientation from the final assembly. So components are rotated incorrectly... Anyone have a suggestion? 


---
**Dale Dunn** *May 10, 2014 14:02*

Does the error happen when you export from SW, or when you import it back in?


---
**Eric Lien** *May 10, 2014 14:51*

Yup. But I fixed by export as step from SW, Then import step in SW, fixed mates, then reexported. Seems fixed. There must be some under/over defined mates in my main assembly. 


---
**Eric Lien** *May 10, 2014 14:52*

Files uploading now. I added alternate CAD versions in a folder. I have Sketchup, Step, Spaceclaim, and 3D PDF.


---
**Jarred Baines** *May 10, 2014 14:58*

Thanks mate ;-)



This will help a lot (big fan of the round numbers here too)


---
**Eric Lien** *May 10, 2014 15:08*

**+Jarred Baines** let me know if you see any errors. I do most of my work between 10pm (after kids and wife are in bed) and 2am. I start work at 7:00am... So I am just saying I could likely use some proofing.


---
**Jarred Baines** *May 10, 2014 15:49*

ha ha - yeah... I'm struggling to go to bed @ 2pm right now...



I'll keep it in mind ;-) thanks for the heads up!


---
**George Salgueiro** *May 10, 2014 19:05*

**+Shauki Bagdadi** you had the same idea that i had! Do you have a prototype? 


---
**Jason Smith (Birds Of Paradise FPV)** *May 14, 2014 03:17*

**+Eric Lien** Sorry for the delay.  I'll post the pics of the thrust bearings and spring bed mounts separately.  You're correct that I didn't include any adjustment in the spacing between the 10mm Z support rods. I guess I kind of lucked out there, and haven't had any issues.

I did switch to a single extruder carriage in the interested of minimizing weight and maximizing build volume.  In the process, I switched from bushings to linear bearings for the carriage.  I've been pretty happy with it so far, as I don't really have the desire to use more than one extruder.  I'd be happy to host both designs as alternatives for those that do though.  Again, really great work!


---
**Eric Lien** *May 14, 2014 03:46*

**+Jason Smith** thanks for the followup. I am working on a convertible bowden-to-direct drive carriage. But it's taking me longer than I thought.



I have all the files up on github. Once I get it finalized perhaps you could add the link to your github readme, or clone the repo over. I also included several cad formats for interoperability between the different cad platforms.



I am in agreement on dual extrusion. It's overrated.﻿



I love the design. Great job.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/QSXigUPmRef) &mdash; content and formatting may not be reliable*
