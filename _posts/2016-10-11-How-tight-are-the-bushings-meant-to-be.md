---
layout: post
title: "How tight are the bushings meant to be?"
date: October 11, 2016 21:50
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
How tight are the bushings meant to be? I used to alignment tools to align and carriage with the printer upside-down. My carriage is still almost impossible to move with my pinky finger. Is there something I'm missing here? Some of the bushings had the brass parts loose when I put the rods in. (I have no steppers on)





**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *October 11, 2016 22:08*

If the bronze parts were loose something is wrong with the bushings. If everything is aligned you should be able to move the gantry with just a finger.



One question, did you use a rod to align each of the sets of bushings together (run a rod between the two bushings at each location on the side carriages and both sets in the carriage). If they bind you need to tweak the alignment if the two bushings until they run without resistance.



The central bronze bushing is epoxy potted into the aluminum housing. This allows the bronze piece to act as a spherical joint inside the housings. Alignment of the two bronze bushings can be a little fiddly, but once aligned they will stay... Especially after running the break-in gcode which allows the assembly of parts to find their equilibrium.



Ask any of the people who built a HercuLien or Eustathios. This rod and bushing alignment process is the worst part of the build. But once done the work is worth the effort, trust me. As I always say: When it's right... You will just know.


---
**Eric Lien** *October 11, 2016 22:23*

Also the alignment tool to set the vertical rod spacing on the sides is just a starting place. In the end you use these to get the rods consistent. Then you should follow up by running the central carriage into the corners, then loosen the lower bearing blocks one at a time to let it find it's equilibrium. Then tighten it down, and move to the next corner. You may have to go around a few times. This sets the rod spacing to match the actual dimensions your carriage printed at (since no printed part is perfect, and bushing tolerances on the shafts are tight by design). The tolerance of the bronze bushing into the shaft is actually higher than the tolerance of a standard lmu bearing to it's shaft on most printers. 



There are many components and degrees of freedom that add up to what they call the stack tolerance: frame length cut tolerance, printed bearing block tolerance, printed side carriage tolerance, side carriage to bushing tolerance, bushing to shaft tolerance... etc. This unfortunately means you need to let the assembly which is subject to the compound errors find it's own balance.


---
**Stefano Pagani (Stef_FPV)** *October 12, 2016 03:57*

**+Eric Lien** Thanks so much for the explanation. I put the bushings as far into the part as they could go and then used a vice to press them in.  To get them to align so the rod could go through, I put the rod in one and angled it to align with the other (breaking the epoxy) That was probably not how I was meant to do it. I will print new parts out of abs because my pla ones are already breaking. I will proabbly need to get new bushings as well.


---
**Eric Lien** *October 12, 2016 04:19*

**+Stefano Pagani** yeah I like ABS on those parts. Then I soften the bushing hole with acetone on a qtip (it takes several treatments). Once the hole is softened enough the bushings press in easy and then let the part harden again (I go 24hrs for the abs to set back up).


---
**Stefano Pagani (Stef_FPV)** *October 12, 2016 12:58*

**+Eric Lien** Ok, thanks so much. Just to confirm, I should buy new bushings and print the parts out of abs, then soften the abs with acetone and put it on the rod, then put the bushings on? I had everything moving smoothly before, I think everything operated better when the cross rods had a slight angle. Putting the belts on set them true and made them stiff. I probably won't be able to remove the bushings and use them again once they are acetone welded in, correct

?


---
**Roland Barenbrug** *October 13, 2016 18:49*

TIP: when pressing the bushings into the plastic parts make sure to have a rod placed through both bushings. Then press the bushings. This will greatly help in alignment of the 2 bushings.




---
**Stefano Pagani (Stef_FPV)** *October 13, 2016 21:30*

Welp, most of my pla parts have cracked... Now I gotta fabracobble a heated bed for my 2X


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/Sh8QhM3HjgK) &mdash; content and formatting may not be reliable*
