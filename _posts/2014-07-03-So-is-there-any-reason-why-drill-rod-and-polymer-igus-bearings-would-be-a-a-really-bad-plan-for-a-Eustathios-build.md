---
layout: post
title: "So is there any reason why drill rod and polymer igus bearings would be a a really bad plan for a Eustathios build?"
date: July 03, 2014 03:54
category: "Deviations from Norm"
author: Joe Spanier
---
So is there any reason why drill rod and polymer igus bearings would be a a really bad plan for a Eustathios build? I ask because I have loads of bearings and 3/8 drill rod that Im not going to use. 





**Joe Spanier**

---
---
**James Rivera** *July 03, 2014 05:49*

3/8" is about ~9.525mm.  Sounds like a decent idea to me, assuming they're straight enough. And maybe make it a bit smaller. I think they might flex too much in a large frame.


---
**Joe Spanier** *July 03, 2014 11:57*

I can get 3/8 drill rod for 3$ a meter so it's really attractive from a price point of view. 


---
**Eric Lien** *July 03, 2014 17:22*

I see no issue other than lack or pulleys with the correct ID. But with a good enough printer you could make them. In fact I have thought about doing this for the sides of the Eustathios that require the double pulley for both the motor and cross belt.


---
**Joe Spanier** *July 03, 2014 17:34*

Thats a good point eric. I could bore them but theres no certainty they will be on center with my lathe.  The printing idea has some merit. I printed some pulleys on the Lulzbot at work and Im still shocked at how good the are. More thinking...


---
**Eric Lien** *July 03, 2014 18:05*

**+Joe Spanier** unfortunately there's no machine stock to remove. The pulley ID on a 10mm is larger than the shaft you're using. I'd say try printed pulleys and see how it goes. If that doesn't work bite the bullet and buy the 10mm rod.


---
**Joe Spanier** *July 03, 2014 19:56*

will probably bite the bullet and go 10. rather do it once. I was thinking getting 8 or 5mm pulleys but it doesnt look like robot digg has them in 32


---
**Eric Lien** *July 03, 2014 21:19*

**+Joe Spanier** nope. But **+Jason Smith** uses 20 tooth on the steppers and 32tooth on the shafts with no apparently speed limitation issues.


---
*Imported from [Google+](https://plus.google.com/+JoeSpanier/posts/EiTYtppk33M) &mdash; content and formatting may not be reliable*
