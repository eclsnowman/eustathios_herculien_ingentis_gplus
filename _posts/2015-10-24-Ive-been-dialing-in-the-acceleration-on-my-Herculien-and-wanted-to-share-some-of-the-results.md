---
layout: post
title: "I've been dialing in the acceleration on my Herculien and wanted to share some of the results"
date: October 24, 2015 01:54
category: "Discussion"
author: Zane Baird
---
I've been dialing in the acceleration on my Herculien and wanted to share some of the results. To examine the effects of different acceleration I printed a model provided by **+Eric Lien** that is used to test backlash. The part shown in the photograph was printed at 100mm/s perimeter speed and I updated the acceleration via an M204 S<Accel_value> command at different times during the print. Based on these results I've turned down my acceleration in X and Y from the default 9000 mm/s^2 to a cool 3000 mm/s^2. This reduces ghosting to a minimum on sharp corners. While this increases print times based on acceleration limited velocity, when you run the numbers it doesn't translate to much loss. Based on Smoothieware's default 1000 ticks/s update rate for acceleration, a speed of 100 mm/s is reached in a distance of ~1.5 mm (0.45mm for 9000 mm/s^2). 



I believe most of the ghosting I observe at high acceleration values is an effect of both my belt tension and the longer length of the Volcano hotend (10mm longer than the V6). With a different carriage design this may be improved slightly, but I think most of it is the result of belt tension.



As a note, this model was printed with a Volcano Hotend fitted with a 0.6mm nozzle with a layer height of 0.2mm and an extrusion width of 0.75mm. junction_deviation in the smoothie config was set at the default of 0.05. My next tests are playing with junction_deviation as this has large effects on the speed and acceleration planning in smoothieware. 

![images/176b1478b24663880388e7978f46bab1.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/176b1478b24663880388e7978f46bab1.png)



**Zane Baird**

---
---
**Eric Lien** *October 24, 2015 02:06*

Great stuff. Keep it coming. One question, do you see the same ghosting on both axis X/Y. Or is it worse on one than another?


---
**Zane Baird** *October 24, 2015 02:11*

**+Eric Lien** It's mostly the same on both axes. Hard to tell a difference from the sides, but there are minor variations, leading me to believe it's mostly a belt tension issue.



Also, I have a roll of Colorfabb CF20 that I'm going to print my next carriage with. The stiffness from the carbon fibers should eliminate any flex on the carriage


---
**Ryan Carlyle** *October 24, 2015 02:58*

Yeah, I've found calibrating acceleration on Smoothie to be a bit more complex than other firmwares, because the corner speed is calculated from BOTH junction deviation and acceleration (along with corner angle). So it's not obvious whether high accelerations are causing the ringing, or the high corner speeds caused by the high acceleration setting. (I'm leaning towards corner speeds being the culprit in this case.)


---
**Zane Baird** *October 24, 2015 03:04*

**+Ryan Carlyle** My initial tests in which I kept the high accel and varied junction_deviation didn't have near the impact of accel variation. But the interplay between the two definitely makes it a little complex to dial in for the best performance.


---
**Ryan Carlyle** *October 24, 2015 03:34*

**+Zane Baird** yeah, 9000 is very high, if you think about how long it takes to accelerate to/from 100mm/s. That's what, 11 milliseconds? I don't know what your drivetrain steps/mm is, but let's just say it's 80 microstep/mm or 5 full steps/mm.  (Full steps are what count for motor torque.) Just in simple round number estimates without doing too much acceleration math, 11 milliseconds at 100mm/s would be <90 microsteps or <5.5 full steps. It's even less distance when you consider deceleration. So you're asking the drivetrain to decelerate for the corner in less than 5 full steps. Whereas cornering at top speed and relying entirely on the motor's pull-in torque would mean asking it to accelerate in less than 1-2 full steps. (Exceeding 2 full steps overshoot causes the stepper to skip.) So in VERY rough terms, 9000mm/s is only a few times less abrupt than not slowing down at all. At least at 100mm/s. 



Yeah, I'm playing fast and loose with the dynamics math, but the exact numbers aren't important, just that a very high acceleration can act a lot like an abrupt impact. 


---
**Zane Baird** *October 24, 2015 03:40*

**+Ryan Carlyle** I set up a spreadsheet to calculate based on smoothieware's update speed and after doing so I quickly realized how little the 9000 mm/s2 accel rate was influencing my effective print speed. Computing the numbers has really helped me to establish good baselines for print speeds as well. Seeing the volumetric flow rate for the layer heights and speeds I was using was really insightful


---
**Ryan Carlyle** *October 24, 2015 03:52*

**+Zane Baird** cool, I love stuff like that :-)  I actually haven't ever looked at the Smoothie accel update speed, that's a really good point. I'm still curious to see what the "refactor" they're planning for the motion code will look like. 


---
**Michaël Memeteau** *October 24, 2015 08:16*

**+Zane Baird**​ Maybe something in the same spirit of tinyG?


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/PGUmtAn78xd) &mdash; content and formatting may not be reliable*
