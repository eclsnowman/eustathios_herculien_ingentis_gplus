---
layout: post
title: "Now it feels like we're making some progress!"
date: May 31, 2017 02:19
category: "Build Logs"
author: James Ochs
---
Now it feels like we're making some progress!  



![images/d646c5aafb430c1d81ac2e446bb744ee.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d646c5aafb430c1d81ac2e446bb744ee.jpeg)
![images/7470b0c941b63fbee687c396122be315.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7470b0c941b63fbee687c396122be315.jpeg)

**James Ochs**

---
---
**Eric Lien** *May 31, 2017 02:39*

Looking great. Keep the posts coming. I really enjoy watching these printers come together. 


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/bAPAdTtkch8) &mdash; content and formatting may not be reliable*
