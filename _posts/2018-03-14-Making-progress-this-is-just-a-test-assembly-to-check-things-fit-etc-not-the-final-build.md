---
layout: post
title: "Making progress (this is just a test assembly to check things fit etc not the final build)"
date: March 14, 2018 21:45
category: "Build Logs"
author: Julian Dirks
---
Making progress (this is just a test assembly to check things fit etc not the final build).  Heated bed and bed plate the last big items to sort out.  I got a mains silicon heater but have since started having second thoughts - 220V here, kids etc.  So thinking I might revert to a 24 volt heater for the time being.  How long is it likely to take to heat the bed to 60 Deg C? (PLA/PETG).  I have a duet. Power supply is 24v, 10A.  SSR still? Second power supply? etc.  Recommended 24V silicon heater? Any advice much appreciated. 

![images/9d3284d6abbf347c718bff9e3b6f60a3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9d3284d6abbf347c718bff9e3b6f60a3.jpeg)



**Julian Dirks**

---
---
**Eric Lien** *March 14, 2018 22:19*

I will say, don't fear mains wiring. You don't fear your toaster or you vacuum do you :)



I jest but really a so long as you give the bed heat spreader a path to ground (so you limit any chance of charging an isolated conductive object) you will be just fine. Plus the current is much lower at 220V, so in my opinion it's safer and able to hit higher wattage for high bed temps much easier.



But that's just my 2cents.


---
**Daniel F** *March 14, 2018 22:58*

Good to see a new Eustathios. I agree with **+Eric Lien** I would go for 230V it’s worth it. With 24V it takes a long time to heat the bed and you would need a PSU that can support around 15A @24V (depends on your heater, mine has 350W). It is not efficient to transform 230 mains to 24V and produce heat with that. And you most likely need a PSU with a fan that means more noise. 


---
**Oliver Seiler** *March 14, 2018 23:15*

I'm using a 24V 350W silicon heater from Alirubber and that works fine for me. I'm printing PET at 85 degrees and it doesn't take too long - I usually turn on the printer, set the bed to 85 before I upload the file, check the bed and printer, load the right filament, etc. and by the time I'm ready to start the print the bed is at the right temperature.

I also print PC at 105, sometimes 110 degrees bed and that takes a little longer to reach (but it can hold it easily, even in my cold shed in winter). If you want 100+ degrees on your bed regularly I'd use a slightly more powerful heater and AliRubber can supply them for you.

I'm using an AliRubber silicon heater on another printer, too, and after about a year the silicon cracked open and the wires got exposed where they go into the main mat. It didn't short to the frame, so had this been a mains power heater there would have been a very real chance of me touching it.

You'll probably also be using tools with your printer (e.g. metal spatula to remove prints) and there's always a small risk of accidentially ramming that into the cables/mat.

That's why I'd really recommend staying away from mains powered heater.



I'm using a 500W 24V PSU from AliExpress that's been reliable (and quiet) for over 2 years now. I did replace the fan inside once after the bearings had failed, but that happens with any (cheap) PSU.

I found a relay that's originally used in Microwaves and that can handle the current and many switching cycles as a cheap alternative to an expensive DC SSR/FET that would require some sort of cooling.



Either way you should make sure that there's an RCD ideally directly on the plug that powers the printer or nearby on the line.



I've also embedded a thermistor into the aluminium bed to get a more accurate reading and can recommend that. Same way as the old E3D thermistors were embedded into the heater blocks with an M3 screw.


---
**Oliver Seiler** *March 14, 2018 23:16*

Looking good BTW!


---
**Oliver Seiler** *March 14, 2018 23:32*

A quick test with my 350W heater, starting at 21 degrees (5mm aluminium bed with 5mm glass on top, measured on the bed)

- 60 after 7 mins

- 65 after 8 mins 20 seconds

- 75 after 11 mins

- 85 after 14 mins 30 seconds.

![images/f932226e0c57ff663384fdd172c47261.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f932226e0c57ff663384fdd172c47261.jpeg)


---
**Julian Dirks** *March 15, 2018 00:44*

**+Eric Lien** I'm kind of scared of my wiring skills more than anything. I also got my 220v heater from Robitdigg and then read a few bad things on here about their heaters.



**+Oliver Seiler** thanks for the info.  That's probably about as fast as my Wanhao heats up which has never been a problem.  Honestly after printing PETG for my printer parts I'm not sure I'll ever buy more ABS so 60 Deg C would cover 90% of what I print now. 



On the other hand I already have the 220V heater and I'm looking at a new heater and a beefier power supply if I go to a 24V heater.  hmmmm.



Our house has RCD's built into the fuseboard. Is that good enough or do you need one at the plug?




---
**Oliver Seiler** *March 15, 2018 01:25*

**+Julian Dirks** my PSU was US$25 and the AliRubber heater US$22 (both including shipping), so it's not a huge investment. 



You can also pick up a plug-in RCD quite cheaply ($15 at Mitre10). As I understand the number of outlets and devices connected to the RCD can effect how fast it releases, so I would rather invest in another one (that's advice from a sparky friend of mine). Also if you have a single RCD in your switchboard that might very well only cover bathroom outlets and no other circuits.



[aliexpress.com - Wholesale 24V 21A 500W Switching Power Supply AC 100-240V Power Switch Led Driver Adapter For Led Strip light lamp, New](https://www.aliexpress.com/item/Wholesale-24V-500W-21A-Switching-Power-Supply-AC-100-240V-Power-Switch-Led-Driver-Adapter-For/1959640346.html)


---
**Daniel F** *March 15, 2018 12:29*

Either way will work--the good thing about open source community, you can profit from other peoples experience and then decide what best fits you. Concerning the Fuse you could use a C14 Inlet Module Plug Switch with fuse (5x20), like this one:  [aliexpress.com - 3 Pin IEC320 C14 Inlet Module Plug Fuse Switch Male Power Socket 10A 250V](https://www.aliexpress.com/item/3-Pin-IEC320-C14-Inlet-Module-Plug-Fuse-Switch-Male-Power-Socket-10A-250V/32755112773.html?spm=2114.search0104.3.2.53677dfefl0NHf&ws_ab_test=searchweb0_0,searchweb201602_4_10152_5711320_10151_10065_10344_10068_10342_10343_10340_10341_10696_10084_10083_10618_10307_5711220_5722420_10134_10313_10059_10534_100031_10103_10627_10626_10624_10623_10622_10621_10620,searchweb201603_25,ppcSwitch_5&algo_expid=06678292-b26e-4c67-846e-0b741c7f24ac-0&algo_pvid=06678292-b26e-4c67-846e-0b741c7f24ac&priceBeautifyAB=0)


---
**Eric Lien** *March 15, 2018 13:43*

**+Daniel F** That's the one I use. I have bought them from Aliexpress before, and they even have them pretty cheap on Amazon now (under 5$, and prime shipping)


---
**Dennis P** *March 15, 2018 15:07*

**+Julian Dirks** and **+Oliver Seiler**, how sensitive are the RCD's? Here we call them Ground Fault Interrupters and are mainly used in wet locations to prevent electrocutions. We also have Arc Fault Interrupters which sense the difference between short circuits and 'normal' arcing like internal to a switch or when you insert a plug.  Do your residual current devices protect a gang of breakers or are they on individual circuits?  


---
**Oliver Seiler** *March 15, 2018 19:11*

**+Dennis P** I don't know what the exact regulations are here in NZ. Some fuseboards are pretty ancient and have had alterations/additions over the years - so you'll find anything from open fusewires to the latest and greatest.

RCDs can protect against electrocution when you touch one of the wires (not when you hold both at the same time) and also against fire  when the active wire shorts to something other than the neutral/earth wire.

RCDs come with different sensitivities (e.g. 30mA, 100mA and 300mA) and you can find 100/300mA ones mainly as fire protection on multiple circuits. To prevent elecrocution for people/animals you need a 30mA RCD. That's why you most commonly find a 30mA RCD protecting the outlets in bathrooms. They are also mandatory on any outdoor plugs now.

The sub-distribution board in my shed that I've built 3 years ago has a 30mA RCD covering all outlets there.

Arc Fault Interruptors are much more sophisticated and protect against faulty wiring (e.g. loose connections or arching shorts) that don't trigger the main fuse.




---
**Julian Dirks** *March 15, 2018 21:05*

We got our house rewired a year or so ago and they took out the RCD on the bathroom plug and now the main fuseboard has 3 x 30mA RCD's and the garage board has one as well. 



I don't understand the difference in the liklihood of serious harm from 110v to 220v but I used to travel to the US a lot and noticed a big contrast in the attitude to home wiring.  In the US you do it yourself, here not so much.  I'm reasonably handy round the home and do most things myself except electrical.


---
**Julian Dirks** *March 15, 2018 21:06*

**+Daniel F** Thanks, yes I got the Amazon one from the BOM


---
*Imported from [Google+](https://plus.google.com/113795478307151372873/posts/U1ujpxsmmDv) &mdash; content and formatting may not be reliable*
