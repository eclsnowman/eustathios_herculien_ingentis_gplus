---
layout: post
title: "Just wanted to leave a short update on our build"
date: July 01, 2015 09:46
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
Just wanted to leave a short update on our build. Finally got something done yesterday. The x/y carriage still needs some work as the bushings (Robotdigg graphite bushings) don't align well yet. Need to reprint the carriage with higher quality and be a bit creative with the accessories (sleeves and stuff).

![images/6e034809da79703df46ad209b1fae4a0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6e034809da79703df46ad209b1fae4a0.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**Vim Au** *July 02, 2015 01:06*

Are those the 'Selfgraphite' bushings? I'd be very interested to know how they work out.


---
**Frank “Helmi” Helmschrott** *July 02, 2015 07:10*

yes they're the ones with the graphite depots. The first impression is quite nice though aligning them is a bit of a challenge due to their length. Of course also the self aligning feature is missing.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/WMo3oc7uN1d) &mdash; content and formatting may not be reliable*
