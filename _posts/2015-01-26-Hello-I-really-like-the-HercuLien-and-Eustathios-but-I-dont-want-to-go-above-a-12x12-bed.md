---
layout: post
title: "Hello, I really like the HercuLien and Eustathios but I don't want to go above a 12\"x12\" bed"
date: January 26, 2015 22:18
category: "Discussion"
author: Isaac Arciaga
---
Hello, I really like the HercuLien and Eustathios but I don't want to go above a 12"x12" bed. What would I need to adjust/replace to scale down the build area to a 12x12 for both machines?





**Isaac Arciaga**

---
---
**Jim Wilson** *January 26, 2015 22:23*

Probably just the aluminum extrusion, smooth and threaded rods. Printed parts would most likely be identical


---
**Dat Chu** *January 26, 2015 22:38*

I am going in the other direction. I want bigger than current Herculien. :D 


---
**Dat Chu** *January 26, 2015 22:44*

**+Eric Lien** would be able to comment on which item in the BOM you need changing of sizing.


---
**Isaac Arciaga** *January 26, 2015 22:53*

Thank you. I would love to scale the size down for both machines to fit my desk space. I have a gift cert at OpenBuilds that I can use on extrusions :)


---
**Jason Smith (Birds Of Paradise FPV)** *January 26, 2015 23:21*

The Eustathios is right at 11.8" x 11.8" already. 


---
**Eric Lien** *January 26, 2015 23:24*

**+Jason Smith** is correct. If you are looking for a 12x12 printer Eustatios is the one. No sense putting the extra weight of 10mm rods if the span doesn't require it.


---
**Eric Lien** *January 26, 2015 23:35*

**+Dat Chu**  **+Isaac Arciaga** if you are looking to resize HercuLien this is the print I would start with: [https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Herculien_Frame.PDF](https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Herculien_Frame.PDF)



Unfortunately It would take some time to explain everything for every possible way the dimensions could be changed. Just make sure the smooth rods, and associated extrusions are shrunk or expanded by the same amount you want it to change.



For reference the HercuLien as drawn can do 338mm X by 358mm Y x 320mm Z.  You could get more if the blowers were moved from the back and mounted in another way so they aren't being dragged around like a trailer (I always wanted to do this, I just never found a good way.



Also another thing to gain in X would be to reconfigure the location of the Z Leadscrews. The upper bearing support limits travel in X.



I think a different way of routing the belts and stepper for Z could be done to get more Z travel. Currently the z belt tensioning cross beam is the limiting factor. Perhaps inverting the stepper and dropping the cross bar down could do this. 



Lastly if you wanted only 1 extruder you could gain more with the new carriage I posted recently. This is what I plan to do soon because I literally never use my dual extruder so the weight is just a burden.


---
**Tim Rastall** *January 27, 2015 00:16*

**+Dat Chu**​ **+Eric Lien**​​ Danger danger.  When I looked into scaling the mechanical platform for the Ultimaker style system all these bots share,  the cost per mm^3 of build volume jumps up while the system performance drops.  This is due to the increased cost in finding really straight shafts above 500mm in length, the need to move to 10mm cross beams at a certain point (as 8mm will deflect too much).  Plus at some point the outer shafts may need to increase in diameter (12mm shafts are less common and thus more costly) and that means 12mm bore gt2 pulleys.  All of the increase in weight means you're fighting more inertia with 6mm belts so you'll see more deflection,  springiness and backlash in the system.  Longer outer shafts will also amplify any run out issues. ﻿ edit: does herculean already have 10mm cross beams? 


---
**Tim Rastall** *January 27, 2015 00:18*

This is why I started on Procerus as its designed to scale in a much linear fashion.


---
**Eric Lien** *January 27, 2015 00:23*

**+Tim Rastall** yes 10mm on HercuLien all around.


---
**Dat Chu** *January 27, 2015 02:48*

Since herculien has 10mm all around, I assume extending the z axis by simply extending the extruder and the threaded stepper motors would be doable without any performance drop? 


---
**Eric Lien** *January 27, 2015 03:02*

I think so. Just don't go crazy. The mass is moving at the top of the frame. The taller you go the more effect that will have due to the length of the LEVER to the base. But adding another 100 or 200 mm would be no problem.


---
**Isaac Arciaga** *January 27, 2015 05:25*

**+Jason Smith** I noticed the 321mm and 425mm extrusions in your BoM were tapped and the 355mm extrusions have a hole drilled down the middle. Are these still required? Can 90 brackets be used instead?


---
**Eric Lien** *January 27, 2015 05:54*

**+Isaac Arciaga** they can, but they help rigidity. To be honest I never even installed all the brackets the tapped holes and a button head screw work amazingly well. Plus it really pulls the frame together.


---
**Isaac Arciaga** *January 27, 2015 11:04*

**+Eric Lien** thank you. I already placed the order for the extrusions. I'll be using 2020 from Openbuilds. I'll have them drilled and tapped for the Eustathios. Question regarding the stepped lead screw in the BoM. The Misumi part# listed calls for a "S56" (56mm step) MTSBRA12-410-S56-Q8. Is it suppose to be a 26mm step perhaps? The max step available is 40mm.


---
**Eric Lien** *January 27, 2015 13:44*

**+Isaac Arciaga** see this thread: [https://plus.google.com/102704056502370028084/posts/c5i8um6go2d](https://plus.google.com/102704056502370028084/posts/c5i8um6go2d)


---
**Isaac Arciaga** *January 28, 2015 22:12*

**+Eric Lien** I'm looking at your Eustathios Spider files/drawings from GitHub. Is there much difference between your variant and the original? Are the frame dimensions the same? I ask because your full assembly drawing is much easier for me to follow (on SpaceClaim) and build from. Also I want to thank you for taking the time to export your drawings to different formats. It is so much easier for us!


---
**Eric Lien** *January 28, 2015 23:24*

**+Isaac Arciaga** they should be dimensionally the same, just more fillets on mine. I used Jason's STL's to dimension from in Rhino, then modeled each in solidworks with only minor alterations. The only big change was the carriage shape.


---
**Isaac Arciaga** *January 28, 2015 23:41*

Awesome. Thank you so much! **+Eric Lien** ! Completing the shopping list for the BoM now! I'm a little over budget but it looks to be worth it. I'll be asking a bunch more questions soon!


---
**Eric Lien** *January 29, 2015 00:30*

**+Isaac Arciaga**​​ keep em coming. And take notes, pictures, and video if you can. The more we put out there, the easier it is for the next guy.



One thing I would do is use a azteeg x5 mini vs x3 if I did it again. So if you have that in your budget I would go that way. The Smoothieware is a great platform.﻿



Also take notes for the BOM if you can.


---
**Isaac Arciaga** *January 29, 2015 00:44*

**+Eric Lien** I have a couple brand new Smoothie 4x's laying around but it looks like it may not allow two motors for Z? I also have a Rambo 1.3.


---
**Eric Lien** *January 29, 2015 00:57*

**+Isaac Arciaga** two motors for Z aren't needed for Eustathios. It has one and a belt.


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/2DXBwHUwifw) &mdash; content and formatting may not be reliable*
