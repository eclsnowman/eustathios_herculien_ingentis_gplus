---
layout: post
title: "Does anyone know roughly what the cost to build a Herculien is?"
date: April 05, 2015 15:37
category: "Discussion"
author: Gunnar Meyers
---
Does anyone know roughly what the cost to build a Herculien is?  I have been really amazed with the quality and speed it can achieve. 





**Gunnar Meyers**

---
---
**Daniel Salinas** *April 05, 2015 16:00*

My build was somewhere in the $1600 range


---
**Eric Lien** *April 05, 2015 16:12*

Not cheap, but larger and cheaper than off the shelf solutions. And the enclosure sure makes ABS a treat IMHO ;)


---
**Gunnar Meyers** *April 05, 2015 17:36*

Also what is the rough size it can print?


---
**Eric Lien** *April 05, 2015 17:54*

Current setup 338x358x320. Could gain more Y and Z with some tweaks...


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/hZu1dxciaVP) &mdash; content and formatting may not be reliable*
