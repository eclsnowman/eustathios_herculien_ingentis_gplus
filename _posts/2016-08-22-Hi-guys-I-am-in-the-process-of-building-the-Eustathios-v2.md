---
layout: post
title: "Hi guys, I am in the process of building the Eustathios v2"
date: August 22, 2016 21:04
category: "Discussion"
author: Fred Ling
---
Hi guys,



I am in the process of building the Eustathios v2.  Can someone tell me what is the expected level of resistance when trying to slide the carriage in X or Y direction along the linear rods?  This is newly assembled before running the breakin code.  i can slide the carriage very easily before connecting the rods to the belt tensioners.  But once connected to the belt tensioners, it becomes quite a bit harder to slide.  Should I assume this means that there's some slight misalignment between X& Y for the 14mm differential?  



Thanks,





**Fred Ling**

---
---
**Ted Huntington** *August 23, 2016 02:20*

Yes I think it must be misalignment. I found that having one side even a few mm too low was enough to cause a lot of friction. Another thing is that it helps to work the carriage manually a little before running the break in code to help the bushings become aligned.


---
**Fred Ling** *August 26, 2016 19:31*

Thanks Ted.  Seems like this misalignment is caused by slight warp in the bottom of bearing holder which is printed with eSUN PETG.  I have replaced them with eSUN PLA+, and it is perfect now.


---
**Ted Huntington** *August 27, 2016 02:42*

good to hear you got it ironed out. Yeah the movement of the carriage should be very easy and frictionless. I found that my bearing holders would break under the strong force against them and finally just printed them with 100% infill and have not had any problem with them since. But yes, I found that little things like, that a rod extended a little too far and was rubbing against the plastic behind the bearing caused problems, that the belt was not perfectly aligned onto the pulley- things like that - that most people probably at first glance do not think about.


---
*Imported from [Google+](https://plus.google.com/100195596840707097796/posts/esn868tcu8h) &mdash; content and formatting may not be reliable*
