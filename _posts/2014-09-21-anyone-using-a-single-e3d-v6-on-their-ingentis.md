---
layout: post
title: "anyone using a single e3d v6 on their ingentis?"
date: September 21, 2014 10:03
category: "Discussion"
author: Jim Squirrel
---
anyone using a single e3d v6 on their ingentis? if so toss me and stl...not that I'm too lazy to design one. But right now is past time to plant for a good winter crop and i'm about two weeks behind.  That's not including a deer plot. (for those that are confused, I have a veggie garden and hunt deer during the winter) Thanks in advance!





**Jim Squirrel**

---
---
**Eric Lien** *September 22, 2014 00:38*

My carriage should work, but requires supports to print. [https://github.com/eclsnowman/Lien3D_Eustathios_Spider/tree/master/Other%203D%20Formats/STL](https://github.com/eclsnowman/Lien3D_Eustathios_Spider/tree/master/Other%203D%20Formats/STL)


---
**Eric Lien** *September 22, 2014 00:41*

It's called "extruder carriage v3" and uses two 40mm fans. One for hot end cooling, and one for part cooling.﻿


---
**Jim Squirrel** *September 22, 2014 17:36*

cool beans thanks!


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/WyTdPU2ttK5) &mdash; content and formatting may not be reliable*
