---
layout: post
title: "What is the proper steps/mm to use when using the Golmart 1204 ball screw with 32t GT2 Pulleys?"
date: October 21, 2015 21:34
category: "Discussion"
author: Seth Mott
---
What is the proper steps/mm to use when using the Golmart 1204 ball screw with 32t GT2 Pulleys?





**Seth Mott**

---
---
**Ian Hoff** *October 21, 2015 22:55*

I have to make a lot of assumptions to come up with a number

800



I have assumed the following

16x microstepping

200 steps per revolution motors

matching toothed pulleys since you only mention 1 size, one on the motor and one on the transmission shaft.  meaning there is a 1:1 gear ratio 


---
**Seth Mott** *October 21, 2015 23:04*

That is what I came up with with the RepRap steps calculator.  They are 1204 Ball screws with 4mm pitch


---
**Igor Kolesnik** *October 22, 2015 02:27*

It depends on your stepper setup. Is it 1/8, 1/16 or 1/32?


---
*Imported from [Google+](https://plus.google.com/108197211464469447407/posts/P6QVn8GyPit) &mdash; content and formatting may not be reliable*
