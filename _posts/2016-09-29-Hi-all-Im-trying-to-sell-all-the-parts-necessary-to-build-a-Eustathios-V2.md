---
layout: post
title: "Hi all, I'm trying to sell all the parts necessary to build a Eustathios V2"
date: September 29, 2016 17:14
category: "Discussion"
author: Seth Messer
---
Hi all, I'm trying to sell all the parts necessary to build a Eustathios V2. Notable parts in the mix, include a smoothieboard, a viki2 lcd, a bondtech extruder. I'm not wanting to part it out, I'd rather sell it as a whole "kit". 



This has been sitting unused/unbuilt for a year now in my spare bedroom; I've just not had the time, nor desire to build anymore. I was originally wanting to just get my money back, but will sell everything for $1000 before shipping (all the parts in the BOM, including glass, aluminum, PEI, everything! I'll also toss in all the filament I have, including an unopened roll of wood filament). We can figure out shipping so that it's fair for the both of us (and whatever method is safest for the parts), especially if the buyer is outside of the USA. 



Please message me if you have ANY questions, or want to see pics, parts list, price list, or whatever! I'd be happy to do a google hangout, or whatever is convenient to show ALL the parts, and talk through everything. 



The 3d printed parts were done by Eric Lien, as a favor to help document the build process, but I never delivered on that, so I ask that whomever purchases and builds, will give back to the community by printing a set of build parts as their first "real prints".



Thanks and PLEASE reach out if you have interest. I'm negotiable on the price too (I've spent over $1800 on all the parts so far).



- Seth





**Seth Messer**

---
---
**jerryflyguy** *September 29, 2016 17:59*

Damn, that's a steal! Wish I'd seen this before starting to build!


---
**Neil Merchant** *September 29, 2016 23:00*

I'm definitely interested - I started looking at buying parts for a build last week but haven't ordered anything yet,  so this seems like a great opportunity. I'm located in Vancouver, BC - any idea what shipping would be like?


---
**Gus Montoya** *September 29, 2016 23:42*

HI seth, could you please email me. gus.montoya@gmail.com. I'm back in the game and also please send pictures :)


---
**Eric Lien** *September 29, 2016 23:52*

Here are pictures of the printed parts: 

![images/ee639165becf6774b86600711f6d64a5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ee639165becf6774b86600711f6d64a5.jpeg)


---
**Seth Messer** *September 30, 2016 03:43*

**+Neil Merchant** might you be able to contact me directly so i can get your mailing address to figure out shipping. also, i know you'd want it ASAP, but to keep shipping costs down, obviously, slower would be cheaper; would that be ok with you? This weekend I'll start figuring up box sizes and weights and get you an accurate shipping quote to Vancouver, BC.


---
**Seth Messer** *September 30, 2016 03:56*

**+Gus Montoya** email sent! though I'll give Neil a chance first since he responded with interest first. Though per my email I'll still snap pics and send this weekend. Have a great evening.


---
**Neil Merchant** *September 30, 2016 05:05*

you can reach me directly at njlmerchant@gmail.com, I'll get you an address and we can figure it out. I'd also definitely appreciate a few pictures, just to confirm. I've gotten used to waiting for things to ship from china so I'm fine with waiting a few weeks if it helps keep costs down.






---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/JbVeF7fpaeU) &mdash; content and formatting may not be reliable*
