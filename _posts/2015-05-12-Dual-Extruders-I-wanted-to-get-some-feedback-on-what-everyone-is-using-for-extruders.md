---
layout: post
title: "Dual Extruders? I wanted to get some feedback on what everyone is using for extruders"
date: May 12, 2015 16:44
category: "Discussion"
author: Vic Catalasan
---
Dual Extruders? I wanted to get some feedback on what everyone is using for extruders. I ordered one of the E3Dv6 (1.75) for fine printing but what is a good alternative hot end for fast printing? also how do you go about preventing the other hot end from hitting your print?



Not interested in dual color at this time but would like to print two different materials one at a time without changing hot ends and re-spooling.





**Vic Catalasan**

---
---
**Derek Schuetz** *May 12, 2015 16:51*

Dual Extruders is a nightmare... I would go with one get a Volcano Hotend since nozzles now go from .4 to 1.2 to have fast prints. In order to make sure the other nozzle does not hit your print its all about the settings in your slicer, generally you want both nozzles to be the same though since you need them both to be at the same distance from the bed which is hard if you do not have same hotend and may require you to create a custom carriage to adjust for height differences, also you can't PID the hotends individually so you have to make sure there pretty similar in there thermal conductivity also. If you want to try dual extrusion i would get a Chimera/cyclops to eliminate a lot of the above headache, but i don't know to much personally about those just heard good things 


---
**Eric Lien** *May 12, 2015 17:15*

In my opinion its not worth the hassle. Spend the money from the 2nd hot end on s3d for better supports.


---
**Vic Catalasan** *May 12, 2015 17:35*

So I am guessing two volcano with two different tips would work well since they would easily be the same distance to the bed? 



I am surprised they cannot have independent PID controls? 



Update... I will be order another E3D and both will have volcanos. I hope it will do the trick without mods and have the same conductivity


---
**Derek Schuetz** *May 12, 2015 18:01*

I tried two volcanoes and decided to just use one since I couldn't tell my slicer which hotend to use so regardless I still had to swap of nozzles 


---
**Vic Catalasan** *May 12, 2015 18:29*

You could at least select which extruder to feed right? 


---
**Derek Schuetz** *May 12, 2015 19:31*

Could not find the Option to...that's was my original idea to have one have a fat nozzle and other yo have a thin nozzle


---
**Erik Scott** *May 13, 2015 01:09*

I had 2 extruders on my printer. I could never get them at the same height, so I always printed from the lower one (luckily, it was the first extruder in the software) and never touched the second. It would regularly clip my prints though, and I eventually removed it. Lowering the mass of the printhead is desirable on these kinds of printers anyway, and I'm moving to Eric Lien's v4 carriage soon. 


---
**Eric Lien** *May 13, 2015 01:58*

**+Erik Scott** if I get any spare time soon I plan to make a cyclops version of the V4. I already have the extended volcano version. I may as well have the hat trick.


---
**Erik Scott** *May 13, 2015 03:21*

I've been thinking about it a bit myself. I think the single nozzle is really the solution to the dual extrusion problem. 


---
**Jeff DeMaagd** *May 13, 2015 05:02*

Yes, dual is pretty hard to set up. I've done dual J-Head since last August and just set up dual Picos last weekend. Mostly, it's ABS with HIPS support that easily breaks away or dissolves overnight.



First, I don't recommend dual until you have your single extrusion knowledge down pat. Second, yes, it requires matching the hot end lengths. My way of fixing slight mismatch in lengths is to use pieces of plastic shim stock sheets to raise one side of the extruder system body a tiny bit higher than the other side.



It seems there's not a lot or resources or people that know how to make it work, there were all of two people doing dual extrusion builds in all of MIdwest RepRap Fest this March.



Cura seems the easiest for software.﻿


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/7qWREMf2Dt9) &mdash; content and formatting may not be reliable*
