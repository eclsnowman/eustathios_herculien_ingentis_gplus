---
layout: post
title: "I'm giddy with anticipation! A Box came in the mail and it lets me introduce to you the latest build project"
date: July 10, 2014 02:43
category: "Deviations from Norm"
author: Mike Miller
---
I'm giddy with anticipation! A Box came in the mail and it lets me introduce to you the latest build project. My printer has a name:



IGentUS



It's through the kind assistance of **+igus Inc.** that I hope to give a little back to the community that's been extremely patient with my learning curve. 



This is an Ingentis based printer, using polymer guides instead of metal bushings and linear bearings. The orange parts are Ingentis, the Blue parts are those I've printed to adapt it to the **+igusInc** parts, and the build volume is solidly between small and crazy huge (300x300x450, 12x12x18in)



It'll be using braided fishing line, and I hope to strike a balance between speed, resonance, and print quality. 



Hopefully it departs enough from the printers that came before to give you something to consider when you build your next printer, and I hope it does the igus parts justice as they're SMOOOOOOOTH!

![images/b171b3d5d2d13a6e4af9606beff5cccc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b171b3d5d2d13a6e4af9606beff5cccc.jpeg)



**Mike Miller**

---
---
**James Rivera** *July 10, 2014 03:18*

Sweet! I can't wait to see how it turns out! I've been wanting to make my next printer with polymer bearings, but finding the time...sheesh. Pardon me while I live vicariously through you.  :)


---
**Eric Lien** *July 10, 2014 03:25*

What bearing is that in the orange block?


---
**Mike Miller** *July 10, 2014 03:31*

It's an igus part number, B608A7E 

[http://www.igus.com/wpck/5706/xiros_A500?C=US&L=en](http://www.igus.com/wpck/5706/xiros_A500?C=US&L=en)



honestly, this stuff is rated for temps and duty cycles that no FFF printer will ever see.


---
**igus Inc.** *July 11, 2014 19:43*

Glad you got it! Let us know how it goes! :)


---
**Mike Miller** *July 11, 2014 20:09*

I'll have some news fairly shortly on shimming the bushings to the Ingentis parts


---
**karabas3** *July 12, 2014 08:24*

Why don't to print bushings from POM? I already bought a reel of POM to try out. I hope acetal will slide good to forget about buying bearings. You can made bushing as a clamp to compensate any possible wearing...


---
**Mike Miller** *July 12, 2014 12:44*

Because as cool as printing stuff is, my current printer would make CRUMMY sliding surface things. I want to spend time making and using the printer, not overcoming the limitations of my skill set and current printer. ;) I also want to build it and forget about wear. 


---
**karabas3** *July 12, 2014 15:13*

Plastic sliding surface does not need to be smooth or flat because it's plastic. So it does not matter. The matter is what plastic to use.



I think for steel rod and alu rod we need different plastic.


---
**Mike Miller** *July 12, 2014 15:30*

the igus rods are ceramic coated and <i>very</i> smooth. They're specified for their linear bushings and while I'm sure other aluminum rods would work, I'd expect them to not work as well. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/jcRevUqPFh8) &mdash; content and formatting may not be reliable*
