---
layout: post
title: "So a ingentis is designed to only work with the bowden design right"
date: March 13, 2014 02:32
category: "Discussion"
author: Chad Nuxoll
---
So a ingentis is designed to only work with the bowden design right. Have you guys had any troubles with using the bowden design hotends if so what





**Chad Nuxoll**

---
---
**Mike Miller** *March 13, 2014 02:39*

I have an e3d that I cannot keep from eventually plugging, it's currently driving me nuts. 



The Ingentis design uses a bowden to increase the speed of the extruder, I'm half tempted to see if I can't make a direct drive work, just from a reduced hassle standpoint. 


---
**Chad Nuxoll** *March 13, 2014 02:42*

ya from what i have collected about the bowden design hasn't been the best. I was as well going to get a E3D hot end. I was almost thinking about making a modified design of the ingentis to remove the bowden design


---
**Mike Miller** *March 13, 2014 02:50*

The Ingentis is designed to ba an 'all boxes checked' printer, which includes the Kraken ;) I wouldn't base your impressions on my experience. I've had a 'working' printer less than two months...less than that if  you count the time spent waiting on parts like the e3d to replace my melted J-head. 


---
**Wayne Friedt** *March 13, 2014 03:03*

The Ingentis can and does work with direct drive extruders on the carriage. Not exclusively Boden.


---
**Chad Nuxoll** *March 13, 2014 03:09*

O really I didnt know that Ive only ever seen it with the bowden so I figured that was just part of the design. Mike Ive been looking the difference between direct and bowden and from while know and heard alot about hard to calibrate, retraction problems, ozzing, and some more stuff and figured I would go with a direct drive system. 


---
**Wayne Friedt** *March 13, 2014 03:18*

I was using **+Eric Lien** as an example and i guess it is not exactly an Ingentis so my example is not correct. 


---
**Chad Nuxoll** *March 13, 2014 03:22*

O ya Ive been looking at his design cant seem to see enough in his videos to see how he has it set up


---
**Tim Rastall** *March 13, 2014 05:36*

No reason why you couldn't use direct drive. Id use a nema 14 and an ez-struder or similar. Would need a new carriage design obviously. 


---
**Eric Lien** *March 13, 2014 06:10*

**+Chad Nuxoll** Here is a top view showing the CoreXY Mechanics:



[http://i.imgur.com/YhOMHVu.jpg?1](http://i.imgur.com/YhOMHVu.jpg?1)



Here is a 3D pdf that should be an easy way to roll around the model and see the workings:



[https://drive.google.com/file/d/0B1rU7sHY9d8qc1lGdUhIanA1UGs/edit?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qc1lGdUhIanA1UGs/edit?usp=sharing)



Also here is the link to view the model in other formats (since I work in Solidworks natively):



[https://drive.google.com/folderview?id=0B1rU7sHY9d8qWE1TTXM3aUpOdFk&usp=sharing](https://drive.google.com/folderview?id=0B1rU7sHY9d8qWE1TTXM3aUpOdFk&usp=sharing)



Hope It Helps.


---
*Imported from [Google+](https://plus.google.com/112388822161261154200/posts/MJSusUd6RG6) &mdash; content and formatting may not be reliable*
