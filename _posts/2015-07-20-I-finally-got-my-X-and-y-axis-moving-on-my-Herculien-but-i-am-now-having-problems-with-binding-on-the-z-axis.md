---
layout: post
title: "I finally got my X and y axis moving on my Herculien but i am now having problems with binding on the z axis"
date: July 20, 2015 20:58
category: "Discussion"
author: Gunnar Meyers
---
I finally got my X and y axis moving on my Herculien but i am now having problems with binding on the z axis. It just seems tight and he belts are skipping. I hope someone has insight. 





**Gunnar Meyers**

---
---
**Eric Lien** *July 20, 2015 22:58*

Did you make sure everything was aligned and moving freely before engaging the drive belt?



A common mistake many people make is on the lower leadscrew mount. It should NOT be up tight to the vertical 20x80 v-slot. It sits spaced away allowing you to perfectly align the fixed upper mount, the lead nut, and the lower mount. Run the bed all the way down by hand so the upper mount and lead-screw nut guide the lead-screw lower mount to its perfect position. The tighten down the lower mount. Then engage the drive belt by tensioning the two idlers on the lower cross bar 20x80.



Also I recommend superlube for the lead-screw. It is a silicone based lubricant that works well between the delrin nut and the metal leadscrew. It is cheap on amazon: [http://www.amazon.com/Super-Lube-21030-Synthetic-Grease/dp/B000XBH9HI](http://www.amazon.com/Super-Lube-21030-Synthetic-Grease/dp/B000XBH9HI)



Just do not ever use superlube on the bronze bushings. It will clog the pours in the bronze and they will no longer self lubricate.



Best of luck. 


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/YK82XUhXu5y) &mdash; content and formatting may not be reliable*
