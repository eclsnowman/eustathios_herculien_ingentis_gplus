---
layout: post
title: "Thanks to the generosity of Eric Lien at the 2018 MRRF I was recently able to change out the Smoothieboard V1.0b I'd been running on my Herculien to an Azteeg X5 GT with Bigfoot BSD2660 drivers"
date: May 02, 2018 01:05
category: "Discussion"
author: Zane Baird
---
Thanks to the generosity of **+Eric Lien** at the 2018 MRRF I was recently able to change out the  Smoothieboard V1.0b I'd been running on my Herculien to an Azteeg X5 GT with Bigfoot BSD2660 drivers. 



These drivers are based on the Trinamic TMC2660 chip and have LOTS of features, one of which I was very excited about was the 1/256 interpolated microstepping. With this feature enabled the processor is less taxed by step generation (still performed at 1/16) but all the benefits of smooth acceleration and quiet operation are realized through the on-driver conversion to 1/256.



While seemingly simple to set up, there were some modifications that had to be made to the configuration file in order to enable interpolation (as well as some other neat features of these drivers). But once I did, WOW... I was blown away at how quiet my Herculien was performing with no loss in print quality and cooler-running motors.



In order to examine the effect of enabling the advanced driver features I printed a model with different edge curvature at a constant speed of 60mm/s @ 2500 mm/s2 for all features in Atomic Black PLA. Using the same g-code file for each print, I tested the following driver settings: 



80% speed w/interpolation, 

100% w/interpolation, 

no interpolation - 1/16 microstepping, 

no interpolation - 1/64 microstepping 



(top to bottom in most, left to right in one image). As can be seen in the images, there are only very minor differences. However, I can now barely hear the printer when it is running.



See recent (pending) commits to the github for details on the configuration and how registers on the driver chips were edited to enable different advanced driver features.



![images/d5f147568b731c7f6221bb71a571ac17.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d5f147568b731c7f6221bb71a571ac17.jpeg)
![images/8b4e71de45429491900f0d564a7ea2d0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8b4e71de45429491900f0d564a7ea2d0.jpeg)
![images/5cac2e2702cbf7c9b3a14395e24d4110.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5cac2e2702cbf7c9b3a14395e24d4110.jpeg)
![images/85ffd6c2b3e958811b16f7017ceec72b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/85ffd6c2b3e958811b16f7017ceec72b.jpeg)
![images/7dcf598e61c1c42099fc8a6ebb840291.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7dcf598e61c1c42099fc8a6ebb840291.jpeg)
![images/03ac63744a13062b3f46dd8c96d51675.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/03ac63744a13062b3f46dd8c96d51675.jpeg)
![images/f6415cd126d05946da084126ec10df05.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f6415cd126d05946da084126ec10df05.jpeg)

**Zane Baird**

---
---
**Brandon Cramer** *May 02, 2018 02:32*

It’s amazing how quiet the Azteeg X5 mini is with the Trinamic drivers. The only thing that makes noise on my Eustathios Spider V2 is the fan. 


---
**Eric Lien** *May 02, 2018 03:00*

**+Zane Baird** excellent work as always. Glad to hear you were able to make use of the board and do the testing. It is something I had been meaning to do on that board for a long time, but just never got around to it since my pre-production X5 GT is still kicking. 



But now that you have the 2660 drivers figured out I might have to go back and revisit the upgrade. 



I just merged your pull request (but on a side note the pull request was blank as far as I could tell). No file changes in the commit.


---
**Zane Baird** *May 02, 2018 11:23*

**+Eric Lien** Not sure what I did wrong the first time, but the new pull request I created looks to contain the correct files this time


---
**Eric Lien** *May 02, 2018 12:16*

**+Zane Baird** second one worked. 



Files are up: [https://github.com/eclsnowman/HercuLien/tree/master/Azteeg%20X5%20GT%20Smoothieware/Bigfoot%20BSD2660%20interpolation](https://github.com/eclsnowman/HercuLien/tree/master/Azteeg%20X5%20GT%20Smoothieware/Bigfoot%20BSD2660%20interpolation)




---
**Zane Baird** *May 02, 2018 13:35*

Another note here: These parts were printed in duplicate (rotated 90°) so that the effects on retraction were still observable. The retraction could use some tuning in this case, but you can see from the 80% speed (top or leftmost in images) slowing down a bit helps the blobs from retraction as can be expected. 


---
**Ryan Carlyle** *May 02, 2018 16:54*

2660s are nice. Duet WiFi/Ethernet also uses them. 


---
**Zane Baird** *May 02, 2018 17:06*

**+Ryan Carlyle** Indeed they are! I have a Duet Wifi on my coreXY and love it. Didn't think anything could beat the ease of configuring smoothieware, but the RepRap firmware on the Duet Wifi is about the easiest it could get.


---
**Ryan Carlyle** *May 02, 2018 17:25*

**+Zane Baird** RRF configurations can be changed during runtime, too :-) not that you should be making a habit of that...


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/A4QYFJ4VpJq) &mdash; content and formatting may not be reliable*
