---
layout: post
title: "Got a problem that hopefully you guys have encountered and can help me with"
date: March 10, 2018 16:15
category: "Discussion"
author: Ryan Fiske
---
Got a problem that hopefully you guys have encountered and can help me with. For reference I'm using Slic3r.



I've started attempting my first prints, and one thing that is bothering me a bit is that after XYZ homing, the hotend drags across the bed to get to the starting point, leaving a little trail of ooze as it goes. Is there a quick and easy way to drop the bed a little in Z prior to initial travel to prevent this? 





**Ryan Fiske**

---
---
**Dennis P** *March 10, 2018 17:26*

What is your Lift Z value? Its under the printer tab. that should control the z lift for travel moves


---
**Ryan Fiske** *March 10, 2018 17:29*

I was hoping for a non-global edit so that it'd only do it on the intitial travel move, but I just noticed the PE edition of Slic3r that I have started using has a height limit you can force, so that'll likely do it! 


---
**Dennis P** *March 10, 2018 18:05*

Does Slic3r have allow you to have gcode snippets for printer gcodes? 

In KISS, we have pre and post gcode options, and in the section for "Select New Extruder & Warm" – each time an extruder is selected immediately before printing we can look ahead to the next path. I don't see why you could not add a Z0.5 to the string to move it up 

 

; Head to the start of the next path

G1 X<NEXTX> Y<NEXTY> F6000

G4 P0

; Select the Tool (extruder)

T<EXT+0>




---
**Eric Lien** *March 10, 2018 18:27*

Add a small z-lift into your slicers start gcode. Then it will go up before going to the first position.


---
*Imported from [Google+](https://plus.google.com/108184373210415975396/posts/XnxTX2vXapL) &mdash; content and formatting may not be reliable*
