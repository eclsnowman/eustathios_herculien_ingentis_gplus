---
layout: post
title: "Eric Lien (Or anyone else who might have a minute), could you please post pictures of your x, y, and z endstop mounts on your Herculien?"
date: June 13, 2017 01:54
category: "Tutorials"
author: James Ochs
---
**+Eric Lien** (Or anyone else who might have a minute), could you please post pictures of your x, y, and z endstop mounts on your Herculien?  I couldn't find a clear shot of them in Github or here....



Thanks!





**James Ochs**

---
---
**Eric Lien** *June 13, 2017 02:07*

Z axis

![images/574c24d34979c6e2d8a45a3ae7cf7134.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/574c24d34979c6e2d8a45a3ae7cf7134.jpeg)


---
**Eric Lien** *June 13, 2017 02:08*

X axis

![images/c7b3df189afeb04de802d49b75d7330a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c7b3df189afeb04de802d49b75d7330a.jpeg)


---
**Eric Lien** *June 13, 2017 02:08*

Y axis

![images/878bb4633b41a77913ae38e22adb56d6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/878bb4633b41a77913ae38e22adb56d6.jpeg)


---
**James Ochs** *June 13, 2017 02:11*

Thanks! that was exactly what I needed ;)  I was having a hard time visualizing the orientation they should be in.




---
**Eric Lien** *June 13, 2017 02:57*

**+James Ochs** While you go through the build as you run into questions always feel free to ask. And if you have any notes/tips or build instructions you would be willing to submit to the github that could help future builders that would be great too.


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/WBmnSHpSWCm) &mdash; content and formatting may not be reliable*
