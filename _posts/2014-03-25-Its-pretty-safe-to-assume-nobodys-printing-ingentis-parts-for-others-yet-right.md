---
layout: post
title: "It's pretty safe to assume nobody's printing ingentis parts for others yet, right?"
date: March 25, 2014 03:30
category: "Discussion"
author: Mike Miller
---
It's pretty safe to assume nobody's printing ingentis parts for others yet, right?



Those of you that came from the 3D printing forum know the headaches I've had trying to dial in my Delta, and I'm faced with a decision. There's just no way this printer is going to be able to print the parts for an Ingentis. The Table pieces are larger than the build platform, and my attempts to print extenders to MAKE the printer have a larger envelope have been failing hopelessly. So I have a Delta (6mm rods and bearings), all of it's Vitamins and Minerals....and 8(!), 2 meter 20x20 lengths of extrusion, purchased to make an Ingentis or variant. I've got an e3d based bowden extruder (needing nothing), and a 6 channel Rumba to power it all. 



At the very least, I need the bearings/rods/bushings, pulleys and belts, and the plastic bits....or dodge for something less cutting edge where kits and bobs are already available. (You know what I'd prefer.)





**Mike Miller**

---
---
**Brian Bland** *March 25, 2014 03:33*

I can print what you need.  We can work something out.


---
**Mike Miller** *March 25, 2014 03:37*

I'll PM you. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/CWc3JNQEJno) &mdash; content and formatting may not be reliable*
