---
layout: post
title: "Any tips to bed leveling with the 4 adjusters?"
date: April 04, 2018 22:27
category: "Discussion"
author: Dennis P
---
Any tips to bed leveling with the 4 adjusters? I feel like I am trying to negotiate with Jello. 



I thought I understood how to level a 4 point bed from my i3 doing it diagonally and  everything preheated but I just can not get it right. 



I don't want to convert it to 3 point leveling- the bed is just so big. 







**Dennis P**

---
---
**Brandon Cramer** *April 05, 2018 00:01*

I usually use the paper thickness on each corner and then level the bed while it's printing after that. 


---
**Eric Lien** *April 05, 2018 01:42*

Can you describe what issues you are fighting? Where are you at in the range of the spring travel? Any chance you have a warped piece of aluminum or glass? 



Check for bed flatness:

With all spring tension off the bed and it up to temp on the bed, Check cross corners and down each side of the bed with a high quality straight edge. Use a feeler gauge. Can you fit the feeler gauge under the straight edge anywhere? If you don't have feeler gauges use the light test under the straight edge. Can you see light under the straight edge anywhere along the length.


---
**Eric Lien** *April 05, 2018 01:51*

Another pointer: level like you would tune a guitar... Add tension until perfect. If you need to loosen to level, you should overshoot the loosen, then add tension until your feeler gauge (or paper) fits between the nozzle and the bed.



My order is front left, front right, back right, back left, then confirm center, then recheck front left, recheck front right, recheck back right, recheck back left. If the recheck was good, start the print. Once level I am good for a long long time.


---
**Dennis P** *April 05, 2018 05:43*

i checked the glass really quick with a rule tonight when i got home. it looks like i have light leaking under it in a couple of places in one direction. the other looks ok. i will check again tomorrow and check the aluminum heat spreader too. but with a piece of 1/8 sheet, ~15x18, i am not expecting much flatness 


---
**Eric Lien** *April 05, 2018 23:16*

**+Dennis P** I spent a half hour in the garage with a precision straight edge and a deadblow when I first made my heat spreader plate. Took some time and careful selections on where to hit. But it brought my bed in really flat. In retrospect cast tool plate would have been easier. But I got it flat in the end.


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/F2aeSATUQza) &mdash; content and formatting may not be reliable*
