---
layout: post
title: "Starting to look like a printer"
date: April 25, 2017 01:20
category: "Build Logs"
author: Amit Patel
---
Starting to look like a printer



![images/d7c63fc80fdabc55879d35b8832df964.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d7c63fc80fdabc55879d35b8832df964.jpeg)
![images/539e614230f3e67975ea613e33d428ed.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/539e614230f3e67975ea613e33d428ed.jpeg)
![images/4a5a3570e850effb22ac244e6f9acd28.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4a5a3570e850effb22ac244e6f9acd28.jpeg)
![images/d149b841591e983f042ed54e48ac78aa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d149b841591e983f042ed54e48ac78aa.jpeg)
![images/6cbe130724dc545d957ba4f771db5d3b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6cbe130724dc545d957ba4f771db5d3b.jpeg)
![images/7d6fd8016d3987c5e386b6ece912cd11.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7d6fd8016d3987c5e386b6ece912cd11.jpeg)

**Amit Patel**

---
---
**Eric Lien** *April 25, 2017 01:54*

Your builds always look impeccable. Wonderful job. I can't wait to see how the direct drive carriage works for you. I think you are the first person to actually test it :)


---
**Amit Patel** *April 25, 2017 02:02*

thx **+Eric Lien**​ I should have it running this weekend and will post some pics, as of now just need to finish the wireing, im excited about the direct drive as I should be able to print some crazy parts


---
**Ray Kholodovsky (Cohesion3D)** *April 25, 2017 02:07*

That kitten is slightly bigger than I remember it to be :)


---
**Eric Lien** *April 30, 2017 11:29*

Any progress since your last post? I am very excited to see your build completed.


---
*Imported from [Google+](https://plus.google.com/100854251935781152705/posts/EJEEE99mMEH) &mdash; content and formatting may not be reliable*
