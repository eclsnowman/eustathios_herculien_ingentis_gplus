---
layout: post
title: "Any advise for printing ABS? I hate the stuff"
date: January 23, 2016 15:32
category: "Discussion"
author: Kevin Conner
---
Any advise for printing ABS? I hate the stuff.

235 nozzle

85 bed

21 ambient, still

Brim. Usually 20 but only 9 would fit on this print. No difference.

Matterhackers pro ABS

Aqua Net extra super hold unscented



I'm going to try with an ABS/acetone solution but have little hope. Any advice would be welcome!

![images/e530bece5e5fa9a49248887f423461cd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e530bece5e5fa9a49248887f423461cd.jpeg)



**Kevin Conner**

---
---
**Gary Hangsleben** *January 23, 2016 15:38*

For my printer I have went to 3deez coating on a glass plate. 105 bed temp, enclosed the  build area to shield from drafts and when the basement is cold I intoduce a 100 watt shop light to add a little radiant heat bulb to the build area.


---
**Stephen Baird** *January 23, 2016 15:43*

You'll never get satisfactory adhesion at 85c for the bed, you want 100c or as close as you can possibly get to it. 


---
**Michaël Memeteau** *January 23, 2016 15:45*

... Also trying to fit the part in the diagonal would allow for larger brim. I would also bring the bed temp. up.




---
**Kevin Conner** *January 23, 2016 15:46*

**+Nathan Walkner**​, **+Gary Hangsleben**​

I will have to replace all the PLA components so they don't warp. 



I had the bed set to 100 but 12v just doesn't cut it! 



I have been able to sustain 100 on the bed but still got warping.﻿ perhaps drafts. Time to finish that enclosure I started I guess!



I am interested in new bed coatings. I'll give this a shot.


---
**argas231** *January 23, 2016 16:09*

ABS can be a bear to deal with     Try some 3D EeZ and coat your build pate with it   [www.3D-EeZ.com](http://www.3D-EeZ.com)   


---
**Eirikur Sigbjörnsson** *January 23, 2016 16:12*

After I installed a Printbite plate on the Ultimaker2 I have access to there have been zero bed adhesion issues. For ABS I use 230°c and 90°c bed. So my advise would be installing something like that. I haven't even used a glue stick since I installed the plate and I never have to remove the plate for cleaning, just wipe it with a paper towel or swipe over it with Acetone.


---
**Eric Lien** *January 23, 2016 16:15*

Try adding some thicker discs to the model at the corners as hold down tabs. The problem is corners cool from both sides so you'll never be able to maintain equal temperature in corners. So a thick cylindrical tab at the corner helps create more mass and more contact area.


---
**Eric Lien** *January 23, 2016 16:21*

Also have you ever thought about trying PETG. It is go to filament now.


---
**Joe Spanier** *January 23, 2016 16:35*

Uhu or Elmer's glue stick. 100-110c. 


---
**Ryan Carlyle** *January 23, 2016 17:43*

Gonna be the jerk here here and say "don't even try" until you get the PLA parts out of your printer. A warm environment is mandatory for ABS, period. I wouldn't recommend under 35C. (Getting up to 50C is huge, ABS turns into a fantastic filament to print with.) 



Even if you succeed in getting the first layer to stick,  the internal warping stresses between layers higher in the print (away from the heatbed) will cause the part to be much weaker than it should be. Printing brittle ABS kind of defeats the point of printing ABS, unless the only thing you need from the part is very high temp resistance. You get strong and warp-free ABS parts by printing in a hot environment.



More general advice on top of the other stuff people have posted:

1) Low layer heights cause slower and more gradual accumulation of warping stresses, which helps prevent corner-lifting and cracking... say 0.15mm

2) High nozzle temps improve layer bonding so you don't get cracking and help add more warmth to the print to slow down thermal contraction. Particularly if you don't need a lot of retraction, ABS is usually pretty tolerant of high temps, try cranking it up to 250C or thereabouts


---
**Dale Dunn** *January 23, 2016 18:07*

**+Ryan Carlyle** needs more plusses. I recently put a box over my printer, and added a 100W light bulb too (still had a few in the garage). Bed adhesion is almost a non-issue when printing in a warm enough environment.


---
**Stephanie A** *January 23, 2016 18:10*

You absolutely need an enclosure for abs. And for that, your parts need to be able to take the heat. 

I was having a lot of problems with abs, and have gone through a few crazy solutions to fix. First I start with a high bed temp for the first layer, 100c or more, then drop it down to 85c. This will help your first layer to adhere by conforming to the surface. But if you're bottom layer stays at 100c then it is too soft and will flex and warp as upper layers cool. 

I also found that some glass is better than others. For whatever reason, some glass adheres better to abs, some don't. The first sheet I had, prints stuck so well it cracked and chipped it. My replacement piece, they hardly adhere. I use acetone/abs mix, which works very well. But for glass where it just won't stay, I switched to kapton tape. That made a huge difference. Other tapes might work too, I haven't tried. 

But an enclosure is a must. 


---
**Liam Jackson** *January 23, 2016 18:18*

Acetone wiped Kapton or buildtak, 110c bed, 18-20c room no drafts, press down the first layer or use a raft. 


---
**Frank “Helmi” Helmschrott** *January 23, 2016 18:32*

You probably won't need need a box around your printer but printing ABS on pure glas and below 100°C it way too optimistic.



You should get yourself a better printing surface than just glass. I get real good (sometimes too good) adhesion with Buildtak buildplates, also other print plates work well like the one from MTPlus.de or others.



You may still need to have a chamber depending on the material you use and the temperature around.



Like **+Eric Lien** said i would probably think about using PETG instead of the ABS.


---
**Dale Dunn** *January 23, 2016 18:40*

**+Frank Helmschrott** You can print smaller objects without a box, and larger ones that are largely flat on the platform. I did so for years. But the image in the original post shows a box nearly 180 mm on one side, and that will always either curl off the bed or delaminate if the whole thing isn't kept warm during the print.



PETG is interesting and makes very tough parts, but I found it too hard to remove supports. Layer adhesion is just that good.


---
**Ryan Carlyle** *January 23, 2016 19:09*

Oh, by the way, another trick you can use for large ABS enclosure boxes is to add a lot of little vent holes to the sides, particularly near corners. The holes break up the stresses so the contraction doesn't all add up cumulatively.Then radius/bevel the corners if you can. Long, straight extrusion strands leading up to sharp corners are the worst possible thing for warping/cracking. The sharper the corner, the worse it will tend to warp. 


---
**Rick Sollie** *January 23, 2016 19:29*

**+Eric Lien** Have to agree with Eric on this one,  I was printing a large print that kept curling, even with some (cardboard)  draft protection.  I went back and added some 1mm x 20mm discs on the bottom corners and not more curling.


---
**Gústav K Gústavsson** *January 23, 2016 19:43*

Bed 100° C min, no fans, enclosed if possible, Printbite does magic. Never use glue anymore which was a must before. And keep infill to minimum that you are comfortable with. Had one part with 80% infill and it always warped and broke loose. Lowered to 60% and it printed fine.


---
**Dale Dunn** *January 23, 2016 19:59*

Summary of comments so far: Ways to change your design so it will print in ABS, and a way to change your printer so it can print nearly any design in ABS (and stronger too).



If your machine is built of PLA, you must do the former. If it is built of ABS (or preferably, metal) do the latter.


---
**Kevin Conner** *January 24, 2016 03:45*

**+Eric Lien** yes, I print in PET variants almost exclusively now. I still have an urge to master all aspects. ;)


---
**Kevin Conner** *January 24, 2016 03:50*

+Dale Dunn, for PETG, I print on glass with hair spray. It sticks great. To remove it put the plate in the freezer. The moisture in the air softens the spray and the part shrinks just enough to pop the print loose.


---
**Kevin Conner** *January 24, 2016 03:53*

**+Rick Sollie** **+Eric Lien**​, the bottom z axis blocks of the eistathios build have these discs. They still popped loose. Time to finish my enclosure I think. :)


---
**Godwin Bangera** *January 24, 2016 08:06*

Buildtak !


---
**Jeff DeMaagd** *January 24, 2016 14:06*

I use 110°+C heat bed, PEI build surface and an enclosure that's heated to 45°C or higher. I didn't see how you arrived at an 85°C bed temp figure. I've never seen anyone recommend that low of a temp for ABS. I've not used a brim in a couple years. I do use a 3 perimeter skirt to make sure the bed is level and the lines are packing well without over packing.


---
**Kevin Conner** *January 24, 2016 15:39*

**+Jeff DeMaagd**​​ I set my hbp to 100c and it tops out at 85 or so. It's an old 12v prusa hbp and doesn't like to get that hot.﻿


---
**Jeff DeMaagd** *January 24, 2016 16:46*

Pre MK2? I'm pretty sure MK2 and newer will get up to temp as long as you have sufficient PSU capacity. Have you checked your voltage when the bed is on?


---
**Kevin Conner** *January 24, 2016 18:13*

**+Jeff DeMaagd** it's MK2. where should I check the voltage at? I have a 360W PSU but haven't verified what the sticker says. where should I check the voltage and what amount of drop is acceptable?


---
**Jeff DeMaagd** *January 24, 2016 18:48*

I'd say if the drop between bed on and bed off is more than 1 volt, it might be your culprit.


---
**Jeff DeMaagd** *January 24, 2016 18:49*

I.e. 11V when bed is on, 12V bed is off.


---
**Glenn West** *January 25, 2016 09:51*

ive printed abs on elmers magic purple disappearing glus on 100C glass with no problem


---
**Pete LaDuke** *January 25, 2016 14:16*

lots of good comments here, thought I'd add one more alternative.  I print on glass, with this product which is easy to apply.

[http://airwolf3d.com/shop/wolfbite-prevents-3d-printed-parts-from-warping](http://airwolf3d.com/shop/wolfbite-prevents-3d-printed-parts-from-warping)



no enclosure (but need one) for ABS, 100C bed, 230-235 nozzle, 30-40 speed.  Warning read instructions.  I have literally destroyed my glass by fusing the abs to the glass.  So follow directions.  the 3d-eez +argas231 looks promising as well


---
**Glenn West** *January 26, 2016 01:25*

I used to have the heat up problem. Found few problems not heavy enough PSU and then cable not thick enough so voltage drop. After both fixed no problems


---
**Kevin Conner** *January 26, 2016 02:39*

Hi Glen. how big is your PSU? what gauge wire worked? I've got 8g speaker wire (possible culprit. I have not tested the impedence. it never gets warm to the touch.) and a 360w PSU (also never verified, Amazon special.)



Thanks for all the interest everyone!


---
**Glenn West** *January 26, 2016 03:40*

Mine is 500watt and I use silicon wire Super thick. I'd use wire designed for 15 to 20 amp for my first printer at 12v. Second printer I switched to 24 volt I can themally shatter glass


---
**Pete LaDuke** *January 27, 2016 14:11*

**+Nathan Walkner** you could be right, but I don't exactly agree with you.  The ABS slurry I have used in the past did not perform at the same level as the wolfbite product.  Perhaps I should run some tests sometime, overall I'm happy with the wolfbite product.  easy to apply and it works.


---
**karabas3** *February 14, 2016 07:42*

Your ambient  is wrong. You need 60-65C ambient for ABS. No other way.


---
*Imported from [Google+](https://plus.google.com/+KevinConner/posts/2DKNkKJTiy3) &mdash; content and formatting may not be reliable*
