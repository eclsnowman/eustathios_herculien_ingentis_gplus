---
layout: post
title: "Making printers is easy, keeping them living is hard"
date: May 08, 2016 14:35
category: "Discussion"
author: Mike Miller
---
Making printers is easy, keeping them living is hard. Making a dynamic system with fine tolerances, and necessary maintenance is not an easy thing to do.



If you read between the lines, Ingentilire is giving me fits, and I just haven't had the burning desire to get IGentUS back up and running with it's upgrades. 



After swapping all components that AREN'T wiring, I found that it was a wiring short that I can't seem to put a finger on. Changed how they were routed and while the core issue hasn't been found yet, got it printing again...only now that the poly fuses were swapped out for real fuses, something is causing a big enough load on the system to blow fuses...which might be one of those little stupid issues like: "You need to relube the rails and retension the belts because the drag is causing things to heat up."



This does not address the Z auto-levelling, nor the fact the bigger power supply's fan is constant on, so the printer is no longer quiet when not in use. 



Ugh. 



Currently have 0 working printers. 





**Mike Miller**

---
---
**Jason Smith (Birds Of Paradise FPV)** *May 08, 2016 14:57*

The frustration you're describing, myself and most of the rest of us in this group have all been there. Hang in there. Throw out your assumptions about what is causing the problem and take some time away to think things through. You'll get it. 


---
**Jason Perkes** *May 08, 2016 15:34*

Check what you disturbed when you did the fuse mod - try moving and wiggling things with heaters on and motors energized to load the electricals up. I would even poke the control PCB a little to make sure its not in internal board fault. Just some ideas


---
**Mike Miller** *May 08, 2016 16:00*

**+Jason Perkes** yeah, I pretty much disturbed everything in troubleshooting....I will say that the poly fuse was rated at 14 amp and the car fuse is a 5 amp, so that's an easy first thing to try...but really, it's a good chance to rewire the cables from top to bottom with connections. Heck, maybe even upgrade the ecu to 32 bits


---
**Jason Perkes** *May 08, 2016 16:30*

lol 14amp to 5 amp, ahemcoughkickpokecough


---
**Mike Miller** *May 08, 2016 16:31*

At some point I'd got it through my head (by reading the internets) that one circuit was sized to 5amps for the heater cartridge, and the other circuit was sized to 14 amps for the heater bed.


---
**Mike Miller** *May 08, 2016 16:34*

imagine my surprise when I desoldered two identical polyfuses...




---
**Jason Perkes** *May 08, 2016 16:40*

OK thats actually not a bad assessment and to the most part its true but possibly a bit outdated depending how many hotends and what wattage. I reckon a 10 amp fuse for the motor/hotend/electronic circuit would be about right. 2 identical polyfues? both 5amp or 14amp?


---
**Mike Miller** *May 08, 2016 16:41*

Both 14. I still have em...will probably just put them back in as they weren't causing the problem.


---
**Tomek Brzezinski** *May 08, 2016 19:56*

For the record, rails are probably not the issue, because the stepper motors have a fixed amount of power usage depending on their current setting. This is counter intuitive if you're more used to DC motors


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/EPwMLn4EWvN) &mdash; content and formatting may not be reliable*
