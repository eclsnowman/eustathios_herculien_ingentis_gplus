---
layout: post
title: "Hi all, just wanted to say how much I appreciate the work done by Tim, Jason, and Eric, as well as everyone else in the community"
date: January 27, 2015 01:17
category: "Discussion"
author: Joe Kachmar
---
Hi all, just wanted to say how much I appreciate the work done by Tim, Jason, and Eric, as well as everyone else in the community. Having had the opportunity to use and assemble a few different RepRaps as well as some commercial printers, the XY-gantry style is my favorite and I really want to build a Eustathios of my own.



I've been putting together a parts order from Misumi* as well as a few other places, and the only real problem I've run into is my inability to get printed parts. I'm kind of out in the middle of nowhere, and the closest 3DHubs printer would charge a few hundred dollars for all the parts listed on Youmagine.



I was thinking of building a RepStrap from a kit for the printed parts, but that's a pretty significant added cost (even if I recycle the motor, electronics, hotend, and any other parts i can scavenge). I'm assuming there have been others here in my position before, and I was just wondering what you all did.



Thanks!



*I know Misumi stopped making the lead screws listed in the BOM. Are MTSBRB12-410-S56-Q8-C3-J0 lead screws okay? (same part, but with a 3mm keyway at the base of the shaft)





**Joe Kachmar**

---
---
**Eric Lien** *January 27, 2015 02:17*

If you want to join the sign up for print it forward **+Dat Chu**​ is building a HercuLien and will be printing a kit for the next person. I had it setup for random selection in my giveaway.



[https://docs.google.com/spreadsheets/d/tFvgSsIlOALT_M1qGBuZfcg/htmlview](https://docs.google.com/spreadsheets/d/tFvgSsIlOALT_M1qGBuZfcg/htmlview)



[https://plus.google.com/app/basic/stream/z12ljb2wruros5o5y04civ0h3kaei1qwpbw?cbp=16dhqi3v6hadd&sview=58&cid=5&soc-app=115&soc-platform=1&spath=%2Fapp%2Fbasic%2Fa%2Fautoload%2Fcommunities%2F108524206628971601859&sparm=cbp%3D1n00wpmonwlex%26sview%3D58%26cid%3D5%26soc-app%3D115%26soc-platform%3D1%26cdct%3DChYQvdfQxY2zwwIYuL7xnICzwwIgACgCEhQIABC4vvGcgLPDAhig9NPvvqLDAhgC%26spath%3D%2Fapp%2Fbasic%2Fcommunities%2F108524206628971601859](https://plus.google.com/app/basic/stream/z12ljb2wruros5o5y04civ0h3kaei1qwpbw?cbp=16dhqi3v6hadd&sview=58&cid=5&soc-app=115&soc-platform=1&spath=%2Fapp%2Fbasic%2Fa%2Fautoload%2Fcommunities%2F108524206628971601859&sparm=cbp%3D1n00wpmonwlex%26sview%3D58%26cid%3D5%26soc-app%3D115%26soc-platform%3D1%26cdct%3DChYQvdfQxY2zwwIYuL7xnICzwwIgACgCEhQIABC4vvGcgLPDAhig9NPvvqLDAhgC%26spath%3D%2Fapp%2Fbasic%2Fcommunities%2F108524206628971601859)


---
*Imported from [Google+](https://plus.google.com/105087915920635664119/posts/EhF8hnWjdJJ) &mdash; content and formatting may not be reliable*
