---
layout: post
title: "So, I'm having a hell of a time getting the 8-pin molex connector to work"
date: August 28, 2015 19:44
category: "Discussion"
author: Erik Scott
---
So, I'm having a hell of a time getting the 8-pin molex connector to work. I've crimped the wires into the pins, but now I can't get the pins into the plastic connectors. It's like the don't fit or are for the wrong connector. I'm almost certain I got the right ones from digikey, the ones specified on the BOM. any thoughts or ideas? 





**Erik Scott**

---
---
**Frank “Helmi” Helmschrott** *August 28, 2015 19:46*

I can only tell you that i'm on a personal war with Molex connectors. I've purchased also the ones from the BOM but i was giving up quite quickly and swapped them out for Sub-D (15pin btw but could have been the 9 pin too).


---
**Igor Kolesnik** *August 28, 2015 19:46*

Can you post photos? They should be snug but nothing extream.﻿


---
**Erik Scott** *August 28, 2015 19:50*

I don't think pics would really explain anything. Crimp pins just straight-up don't fit in the connector. ﻿



I'm looking at some alternatives. I'm hoping there might be something I won't have to crimp at all, just solder the wires onto existing leads. 


---
**Oliver Seiler** *August 28, 2015 20:05*

I found them quite fiddly. Make sure that the part you crimp together isn't too large - that's a problem I had with the thicker wires I used for the heater. Make sure you have the orientation correct and also the pins are not bent.


---
**Eric Lien** *August 28, 2015 20:36*

To be honest I never crimped mine. I got a 8 pin molex extender cable from microcenter, cut it in the middle, and soldered both ends. The extender cable was like 3 bucks. But I tried to do the right thing and have people not solder on a cable that requires strain relief. Guess I maybe goofed? Or did some people make it work per the BOM?


---
**Erik Scott** *August 28, 2015 20:39*

Ah, okay. That was my fall-back plan. I'm going to look into that. 


---
**Brandon Cramer** *August 28, 2015 21:00*

Personally I gave up on the connector and wired it all the way down to the X5 mini. Kind of a pain when you need to swap something out or unhook but it works. 


---
**Eric Lien** *August 28, 2015 21:07*

Just an FYI, there are two versions of the carriage. One is for standard 8 pin molex, the other is for microFIT molex which is smaller and has a smaller recess in the body of the carriage.


---
**Ben Delarre** *August 28, 2015 21:08*

I ended up getting them to work after a couple of attempts. The molex pins have a very specific orientation in the plastic parts. If you get it wrong they don't line up correctly or don't hold in properly. I honestly hate the things and will be looking for an alternative sometime. 


---
**Eric Lien** *August 28, 2015 21:15*

Weird my standard 8 pin molex has held up great. Zero issues. Maybe I should ditch the micro version.


---
**Ben Delarre** *August 28, 2015 21:28*

Once you get them right they seem to work fine and are sturdy, it's just getting the orientation of the pins into the housing right is the trick. 


---
**Gus Montoya** *September 02, 2015 08:01*

I bought it, tried it manually and failed horribly. 



[http://www.ebay.com/itm/8-8-pin-12V-Power-Extension-Cable-Male-to-Female-EPS-P4-ATX-Motherboard-CPU-/141678329440?hash=item20fcafbe60](http://www.ebay.com/itm/8-8-pin-12V-Power-Extension-Cable-Male-to-Female-EPS-P4-ATX-Motherboard-CPU-/141678329440?hash=item20fcafbe60)


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/RzcotqcJw3p) &mdash; content and formatting may not be reliable*
