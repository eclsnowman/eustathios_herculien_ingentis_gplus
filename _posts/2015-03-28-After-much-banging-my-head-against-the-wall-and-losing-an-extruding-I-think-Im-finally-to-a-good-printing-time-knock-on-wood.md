---
layout: post
title: "After much banging my head against the wall and losing an extruding I think I'm finally to a good printing time (knock on wood)"
date: March 28, 2015 14:21
category: "Show and Tell"
author: Derek Schuetz
---
After much  banging my head against the wall and losing an extruding I think I'm finally to a good printing time (knock on wood). I have printed 2 complete dragons which I'm using to adjust bed adhesion (done complete) and retraction settings (almost there just have to find the sweet spot) this was print with a 1.2 mm nozzle at .2 layer height which amazes me the detail I can get with that large of a nozzle 

![images/57c9f9e9af482e22d3695639090bb80e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/57c9f9e9af482e22d3695639090bb80e.jpeg)



**Derek Schuetz**

---
---
**Eric Lien** *March 28, 2015 14:46*

The areas without retracts are looking pretty good. The areas with a lot of retracts look like more tuning is needed. What are your settings. I am looking at getting volcano so I was wonder what settings you are currently using.



I know **+Alex Lee**​ has been using his volcano for a while and may have some tips.


---
**Derek Schuetz** *March 28, 2015 17:01*

I did pid auto tune so that's correct I'm just increasing retraction by .5 mm each dragon until I see signs of over retraction. I am getting weird artifacts that are either from vibration from the blower fan, layer change, or over extrusion. But overall out of all my printers I have never had such nice layer alignment 


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/Z4R9ZEZa3gp) &mdash; content and formatting may not be reliable*
