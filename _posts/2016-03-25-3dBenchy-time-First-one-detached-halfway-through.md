---
layout: post
title: "3dBenchy time. First one detached halfway through"
date: March 25, 2016 17:58
category: "Show and Tell"
author: Rick Sollie
---
3dBenchy time. First one detached halfway through. Was printing ABS and left the ceiling fan running.  Now to enclose the build area.

![images/493af289f9a6bd31a45e6226f4c35973.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/493af289f9a6bd31a45e6226f4c35973.jpeg)



**Rick Sollie**

---
---
**Erik Scott** *March 25, 2016 18:17*

What's your secret?! I've never gotten a 3dBenchy to come out that well. 


---
**Rick Sollie** *March 25, 2016 18:25*

**+Erik Scott** To fine tune the extrusion steps, I ran 100mm through the hotend while the extruder(hot) was in the middle of the bed. This would be an average point for min/max on x and y printing via the bowden.  I saw other methods to set extrusion steps but this seemed to make more sense.


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/FdNGJFjawUF) &mdash; content and formatting may not be reliable*
