---
layout: post
title: "This looks interesting"
date: September 05, 2016 13:24
category: "Discussion"
author: Eirikur Sigbjörnsson
---
This looks interesting





[https://www.kickstarter.com/projects/steelmans/lokbuild-3d-print-build-surface?ref=hero_thanks](https://www.kickstarter.com/projects/steelmans/lokbuild-3d-print-build-surface?ref=hero_thanks)





**Eirikur Sigbjörnsson**

---
---
**Dale Dunn** *September 05, 2016 13:36*

How is this different from other materials that make the same claims?


---
**Eirikur Sigbjörnsson** *September 05, 2016 14:06*

for me the difference lies in easy install and removal of the surface. I use Printbite which is great and even better than this, but its glued down on the glass/aluminum plate and is thus permanent. Still there is no mention of materials like Nylon based ones.


---
**Ryan Carlyle** *September 05, 2016 14:35*

Looks like Buildtak. 


---
**Jeff DeMaagd** *September 05, 2016 22:53*

With no third party testing reviews apparent, and it's above its funding goal anyways, I'm fine with passing on it.


---
*Imported from [Google+](https://plus.google.com/118262882256504121671/posts/YNtNceXd9Q3) &mdash; content and formatting may not be reliable*
