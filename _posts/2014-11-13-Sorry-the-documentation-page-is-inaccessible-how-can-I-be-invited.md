---
layout: post
title: "Sorry, the documentation page is inaccessible, how can I be invited?"
date: November 13, 2014 19:49
category: "Discussion"
author: Ciro Caputo
---
Sorry, the documentation page is inaccessible, how can I be invited? 





**Ciro Caputo**

---
---
**David Heddle** *November 13, 2014 20:02*

Strange not working for me either? Paging **+Tim Rastall** he is the owner of the blog.


---
**James Rivera** *November 13, 2014 22:01*

Confirmed. The home page for blogspot immediately prompts for Google credentials. I'm guessing, but I suspect this was an overall settings change at blogspot--not something Tim did.  If I login to [blogspot.com](http://blogspot.com) and then go to the documentation page ([http://ingentistst.blogspot.com](http://ingentistst.blogspot.com)) I get this message: "This blog is open to invited readers only."


---
**Tim Rastall** *November 14, 2014 00:39*

Hi gents. Yeah. Mea culpa. I pulled it down a few weeks back to try to rationalize the instructions and more critically try to dovetail it into the eustanathios build as the guide isn't complete. The ingentis design has evolved enough to require a rewrite anyway and I don't have time for that right now. I'll put it back up with a disclaimer saying as much.


---
*Imported from [Google+](https://plus.google.com/105876441172064386136/posts/QuXs75HuKz3) &mdash; content and formatting may not be reliable*
