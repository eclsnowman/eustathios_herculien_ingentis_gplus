---
layout: post
title: "Hi, I would like to build a Eustathios"
date: March 28, 2016 12:37
category: "Discussion"
author: Peter Kikta
---
Hi, I would like to build a Eustathios.

I wanted to ask if somebody tried to do it with a direct drive extruder, what are your opinions about it?

The reason is the possibility to print flexible materials, I'm thinking that I try later to build it so that I could switch from bowden to direct if I need to, just wanted to know if the direct drive even works with it.

Thank you for any answers.





**Peter Kikta**

---
---
**Eric Lien** *March 28, 2016 13:11*

look at the work by **+Walter Hsiao** here: [http://thrinter.com/cartesian-flying-extruder/](http://thrinter.com/cartesian-flying-extruder/)



Also I have a direct drive mount for the HercuLien which can be swapped onto the Eustathios with just a change in the self-alignment bushings: [https://github.com/eclsnowman/HercuLien/blob/master/STL%20Files/HercuLien_Extruder_Base_Direct_Drive.STL](https://github.com/eclsnowman/HercuLien/blob/master/STL%20Files/HercuLien_Extruder_Base_Direct_Drive.STL)



[https://github.com/eclsnowman/HercuLien/blob/master/Photos/Direct_Drive_Extruder_Module%20(1).jpg](https://github.com/eclsnowman/HercuLien/blob/master/Photos/Direct_Drive_Extruder_Module%20(1).jpg)


---
**Daniel Fielding** *March 29, 2016 02:10*

Choose 3mm and you can do it


---
**Daniel Fielding** *March 29, 2016 02:11*

Check bondtech on youtube he prints a flexible wheel on an ultimaker 


---
*Imported from [Google+](https://plus.google.com/101209784005254598834/posts/DB7rbzdCEQG) &mdash; content and formatting may not be reliable*
