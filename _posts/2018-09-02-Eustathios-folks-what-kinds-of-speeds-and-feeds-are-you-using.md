---
layout: post
title: "Eustathios folks- what kinds of speeds and feeds are you using ?"
date: September 02, 2018 18:46
category: "Discussion"
author: Dennis P
---
Eustathios folks- what kinds of speeds and feeds are you using ?  These are my KISS settings for PETG and I feel while i get GREAT prints with them, I want to try speeding things up a bit. 



What are your suggestions- how fast are you printing at? 



![images/71a22afe71130df5af3ba5498fa539ee.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/71a22afe71130df5af3ba5498fa539ee.png)



**Dennis P**

---
---
**Zane Baird** *September 02, 2018 19:45*

**+Dennis P** What are your extruder settings? I've found these to be the most crucial. In my experience you want to aim for about 10mm retraction for every 1m of bowden length (these are very generous numbers and not to be taken as absolute truth...) at up to 60mm/s speed. With the Bondtechs I am able to hit about 3000-3500 mm/s^2 accel on retracts reliably and with good results. You should be easily able to hit ~30-40 mm/s print spreed on all features with no issues. 


---
**Dennis P** *September 02, 2018 20:06*

Zane -Interesting that you bring up retract. KISS does retracts in a different way, it has a PreloadViscoElasticity  constant to account for the springyness of the Bowden system and a Destring value. So at the end, the user does not really have a singular retract number like in S3D or Cura. 

How slow do you go for 1st later? 

Do you use a slower speed for top layers to improve finish?


---
**Zane Baird** *September 02, 2018 20:17*

**+Dennis P** I'm aware of and have used KISSlicer quite a bit  in the past. Here are my relevant suggestions. I would also add M204 E3000 to your start g-code as a starting point.

![images/cac9a1ebaee9628f75b7261b74f211ce.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cac9a1ebaee9628f75b7261b74f211ce.png)


---
**Dennis P** *September 02, 2018 21:53*

**+Zane Baird** thanks for posting that. It gives a broader sense of settings. I went through the tuning wizards starting with Temp all the way to destring and those are my results using Inland eSun silver PETG. Here are mine-

 

I am also realizing that I am probably hitting the limit of my hardware as I am on RAMPS/Mega and should not be too greedy. There are times when I think I have seen a hiccup or where I think the mechanics outrun the electronics. That is probably my biggest obstacle. I probably should pull the trigger on a 32bit board. 

![images/87347db1c1af46470e04c3d0301c5c63.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/87347db1c1af46470e04c3d0301c5c63.png)


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/Fd9Nrt98nRc) &mdash; content and formatting may not be reliable*
