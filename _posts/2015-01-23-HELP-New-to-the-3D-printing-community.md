---
layout: post
title: "HELP! New to the 3D printing community"
date: January 23, 2015 21:07
category: "Discussion"
author: Jeff Smeker
---
HELP!  New to the 3D printing community. Been doing a lot of reading and realizing that I could really get into this, have some fun, learn some stuff and hopefully contribute to the community.   Not 100% sure how to get started.  Currently I'm thinking that I should buy a smaller printer (was looking at a Prusa 8" i3v).  Then experiment and build another.  Does this make sense?  



Also, looking at getting Solidworks to teach myself 3D as I've been designing in 2D all these years.  But to spend $3700 seems crazy for this hobby.  Any tips in this regard?  Thanks for the help guys!





**Jeff Smeker**

---
---
**Daniel Salinas** *January 23, 2015 21:24*

I would love to know how to get solidworks for a hobbyist price.  Right now I'm learning 123D Design but I feel like I'm missing out.  But I won't pay 4 grand for a piece of software for me to play around.


---
**Daniel Salinas** *January 23, 2015 21:28*

As for where to get started I think that is a completely subjective thing.  I like the idea of starting with a kit or piecing together a kit.  I started out just recently too but I did so by buying a Robo3d which is a commercial reprap printer.  It was nice to know that I had a guaranteed working printer to get started with and learn from.  I'm using it now to print all the plastic parts for a Herculien printer.


---
**Jeff Smeker** *January 23, 2015 21:44*

I actually considered taking a Solidworks class at a local college, so I could get a student ID, so I can get a student price ($150).  Can't believe there are no other options though.



And I'm leaning toward eventually building a Herculien  as well. 


---
**Tim Rastall** *January 23, 2015 21:45*

Welcome Jeff.  Well,  given the intent of this community we're probably going to try to convince you to build a Eustathios or herculien :). My first bot was a mendelmax but if I'd start again from scratch I'd go straight to Eustathios I think.  There's not much difference in build difficulty and given your employer, I imagine you can get hold of 2020 extrusions pretty easily.  The folks on this community are very supportive and I suspect we could convince someone to print you some parts as long as you reciprocate one your machine is working. ﻿


---
**Tim Rastall** *January 23, 2015 22:03*

**+Jeff Smeker** Autodesk do free licences for Inventor pro 2014 if you're a student of any sort. I got my wife to sign up and she's a PhD in poetry. Inventor is a close competitor to Solid works, in fact I prefer it after using using both. 

[http://www.autodesk.com/education/free-software/all](http://www.autodesk.com/education/free-software/all)


---
**Eric Lien** *January 23, 2015 22:21*

Also designspark mechanical could get your feet wet with constraints and 3d modeling. It is the free "hobby" version of spaceclaim, which by the way runs on the autodesk inventor engine. It can output stl also for printing.


---
**Nathan Buxton** *January 23, 2015 22:29*

Autodesk Fusion 360 is another good choice. Check out the videos they have on YouTube (full tutorials). It is free for students (Everyone qualifies as a student) and it's even free for startup companies.


---
**James Rivera** *January 24, 2015 00:33*

Nobody mentioned Sketchup. It is great for quick and dirty small parts, and is also free for personal use. Sketchup Pro is sub-$500, I think. OpenSCAD is also free/OSS, but is a script driven CAD tool, so it may not appeal to everyone.


---
**James Rivera** *January 24, 2015 00:36*

If you have time to learn  free products (powerful, but with confusing user interfaces) then also look at Blender and FreeCAD. 


---
**Jim Wilson** *January 24, 2015 02:09*

Whatever model you choose, I suggest that you build your first printer, whether from a full kit or from parts you source yourself. You will treasure and benefit greatly from the knowledge gained from constructing and configuring it personally, I promise you.


---
**Eric Lien** *January 24, 2015 02:28*

**+Jeff Smeker**​ just looked at your profile. I worked in the same industry for about 10 years. Systems for Painting, Powdercoating, Curing Ovens, HVAC, Washers, Conveyors, Pumping and Bulk Transfer, Etc. Looks like you build some cool stuff at work. That robot rim coating line as the background of your profile page is sweet. 



I think you would be very happy with a x/y gantry style printer like the Ingentis/Eustathios/Hercules provides. The toughest part of the build is getting all the rods squared an paralleled. After that and some tuning they are very reliable machines with high speed capabilities due to low moving mass.



Glad to hear you are interested in joining the addiction that is 3D printing. Looking at your company and background I am sure you could bring a lot to the community.


---
**Jeff Smeker** *January 24, 2015 03:12*

Thanks for the info all.  I'm really liking the design of the HercuLien, and I would certainly be willing to pay someone to print up the parts for it, and/or return the favor for someone in the future.  Thanks **+Tim Rastall**  for the tip about Inventor for students. Currently downloading (3.9 GB),  hopefully I can get it running okay.  Hi **+Eric Lien** , yes it seems like we have a similar background, and at first glance I'm a fan of your printer design.  Really excited about getting into this hobby!


---
**Jeff Smeker** *January 24, 2015 23:06*

Well, got Inventor running, and after only a full day I already feel pretty comfortable with it.  After a little more reading, I think I want to just jump in and build a HercuLien.  Can anybody recommend my best bet for obtaining the printed components?  I'm more than happy to pay.



Thanks for the help!


---
**Nathan Buxton** *January 25, 2015 05:00*

Find someone near you who can print parts! (I accidentally deleted my last reply... I'm bad at G+). [3dhubs.com](http://3dhubs.com) or [makexyz.com](http://makexyz.com) can help.


---
*Imported from [Google+](https://plus.google.com/105647135467345701747/posts/HwDTGSzy1Wb) &mdash; content and formatting may not be reliable*
