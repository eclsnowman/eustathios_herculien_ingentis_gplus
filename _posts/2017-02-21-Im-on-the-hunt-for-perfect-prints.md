---
layout: post
title: "Im on the hunt for perfect prints"
date: February 21, 2017 21:34
category: "Discussion"
author: jerryflyguy
---
Im on the hunt for perfect prints. I've had issues since I've started using my Eustathios where I get minute position errors which translate into irregularities on the side walls and holes that aren't always perfectly circular.



To that end I decided to follow **+Eric Lien** 's video on dialing things in so they are smooth but loosening the bottom bearing blocks and using the carriage to set the spacing before tightening it all back up.



I've worked through the process a couple times and am still not getting a 'one finger' movable carriage. Not sure what else to try as far as getting things smooth. There seems to be enough stiction in the system that i suspect it's causing my positioning errors (we're talking 1/10s of a mm). 



Anything else that ppl have found caused these issues? I wondered if my carriage isn't printed square (and is binding when installed sqr)? Not sure how a person checks for that or if there is a fix for it? I didn't print my own parts, I had a local acquaintance print them on his Taz printer.  





**jerryflyguy**

---
---
**Markus Granberg** *February 21, 2017 22:06*

You can try my "anti bind" carrage. Its made for a titan direkt setup and igus bearing.. But it does not bind! 

[drive.google.com - MG carrage rev 1.STEP - Google Drive](https://drive.google.com/open?id=0B7kgdYk7GVumcFFOMV9SZlRoeGs)


---
**jerryflyguy** *February 21, 2017 22:08*

**+Markus Granberg** thanks, I'll download it when I get home & have a look


---
**jerryflyguy** *February 21, 2017 23:22*

**+Markus Granberg** so is the premise on your design that the cross bearing pivots to some degree? How is it constrained so that it is still rigid while giving that angular degree of freedom?


---
**Eric Lien** *February 21, 2017 23:32*

One finger movement is tricky. Let's just say I have had a lot of practice :)



But there are a lot of variables as you well know. Yes if the carriages were printed out of square that is a hard point to start from. You should be able to loosen the side pulleys on the shafts and see if it tries to go out of square when the alignment tools are not in place. Also if you are testing for easy motion make sure the primary belts (motor belts) are not installed. With them installed you won't likely get it to move with 1 finger.



This part of the process is a tricky one. It is not easy to explain in text. It's more of a feel when getting everything aligned. That's the hard part with these printers, so many degrees of freedom to get in balance.


---
**Eric Lien** *February 21, 2017 23:34*

But on a side note about misaligned layers. How solid is your hotend in the carriage? That is a big source of misalignment if that can move. Second is what are the stepper driver motor currents set to. Perhaps you have it too high causing overheating, or too low causing lack of torque.


---
**jerryflyguy** *February 22, 2017 03:34*

**+Eric Lien** I've done it a couple more times, no smoother. So I loosened off all the corner pulleys, tried sqr-ing the x&y, and it seems to 'pop' to one position or the other.. so I used a little printed 'bar' from your kit to lock one end of the cross rod to the rotating rod, then carefully measure and position the other end of the same pair of rods dimensionally (59.1mm) away so the rods are parallel. I thought maybe it was a 'touch' smoother afterwards but that's just me guessing? 



Motor belts are off through this whole process.

Could be the hot end is loose in some way? Not sure but it does seem repeatable? 



Motor amperage is 1.75amps on the large smw3d motors (2amp rated iirc)


---
**Eric Lien** *February 22, 2017 04:04*

Any chance you are seeing nozzle catches? You have some pictures close up of the issue on a print?


---
**Markus Granberg** *February 22, 2017 05:09*

**+jerryflyguy**​ when you tighten upp your pulleys you lock the angle between the rods. If the carriage wasn't there you still won't be able to change the angle between them right? The thing when fighting binding is to newer lock upp the same degree of freedom twice. 


---
**jerryflyguy** *February 22, 2017 14:20*

**+Markus Granberg** makes sense, how are the two parts tied together? Is there a pin in there? Very nice model btw!  


---
**jerryflyguy** *February 22, 2017 14:23*

**+Eric Lien** I don't think it's the nozzle catching? Could be but I don't think so? I'll find a part that shows it clearly. 



After messing around w/ all these various things I put the motor belts back on last night to give it another go, and the circular parts are drastically out of round now. Huge flats in the quadrant peaks of every round part. 



Kinda up's the frustration level.


---
**larry huang** *February 22, 2017 14:50*

If your printer's parts dimension are correct, then it won't be too much trouble solving the problem. How is the Straightness of the light rod? Are you able to check the 3D design file of the printer? Maybe check the dimension between each light rod and the parallel aluminum frame with a caliper?  Do you have any picture of the printed parts with magnification?


---
**ali AiL** *February 22, 2017 17:00*

Hlo


---
**jerryflyguy** *February 22, 2017 17:27*

**+larry huang** the printed parts are pretty accurate dimensionally. W/in 0.1mm (or less) usually 



The 8mm rods were very straight when installed. They are Musumi rods through-out.

I've used my calipers to get the 10mm rod and it's parallel 8mm rod parallel w/ each other too w/in 0.01mm on both axis.



I'll get some close up pics of the printed defects.. best I can w/ an iPhone at least. I don't have the proper photographic equipment to take macro shots.


---
**jerryflyguy** *February 22, 2017 21:30*

Here's a shot of the print issue that I was attempting to rectify. You'll notice at the upper end of the part, there is some surface disturbance/anomalies on the face of the two semi cylindrical features.



Also more z-banding than I'd like, which I think is related to the same issue?



[https://plus.google.com/photos/.](https://plus.google.com/photos/.)..


---
**jerryflyguy** *February 22, 2017 21:32*

Here are the rings I printed last night, after getting the rods all squared up beautifully... now I'm printing out of round (well, flat sided) parts![images/3331f541859a7b611c1eedc1f777266b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3331f541859a7b611c1eedc1f777266b.jpeg)


---
**jerryflyguy** *February 22, 2017 21:34*

This may be belt tension, but I can play a musical (low G) note on them. I think they are a touch looser than prior to my tweaking but I think they are plenty tight. If it's got a fault in its present config, I'd say it's the positional stiction over belt tension.![images/a63c02ea67349d6664082be73af589f2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a63c02ea67349d6664082be73af589f2.jpeg)


---
**Eric Lien** *February 22, 2017 22:53*

One thing I would check is the two side cross extrusions that the Z rods attached to at the top. Those are a stopping point for the lower side rods bearing blocks. If the print tolerances of the O.D. of that bearing block part is too large you will hit that frame with the bearing mounts and it can't go any lower. Try loosening and lowering those extrusions a little bit to see if the corner bearing blocks wants to go any lower but hit the extrusion stopping point.






---
**Eric Lien** *February 22, 2017 22:55*

Another thing to check is put shims ( a stack of Post-it notes should suffice) underneath one of the feet to make sure that the frame is sitting level. I found that if the table or frame are not perfectly flat that the twist imparted into the frame by it resting on only three of the feet can cause sticking on the Gantry above.


---
**Eric Lien** *February 22, 2017 22:56*

But looking at your latest pictures above you're definitely seeing either backlash due to lack of tension in the belts or sticking of the Gantry causing backlash when a direction changes on that axis.


---
**larry huang** *February 23, 2017 06:46*

I didn't encounter this kind of artifact before, if  there is pretty serious backlash happening on both axis, then when you lock the belt position with one hand, you will be able to move the carriage along that axis a bit? BTW, what's the speed setting of the printing? what's the acceleration and jerk setting you are using?


---
**jerryflyguy** *February 23, 2017 15:57*

**+larry huang** Larry I've not tried to lock the carriage via belts and move it to check lash. Will try that later today.



Those prints were all 2400mm/m to ensure it wasn't a speed issue. 'Junction deviation' is 0.5mm and accell is 2000mm/second/second


---
**jerryflyguy** *February 23, 2017 16:05*

**+Eric Lien** in regards to the cross bar extrusions, I actually did that when I had it all loose for the reason you mentioned. Didn't make a difference however.



I'm currently (trying) to print a new carriage and see if I can put that in.. maybe it'll make the difference. If that doesn't work then I think I'll try and make a new carriage similar to the one posted above, (just w/out the direct drive parts) and see if that makes a difference. 



I did tighten the belts a bunch more last night which took out about 1/3-1/2 of the lash (as viewed by comparing test parts).



One other issue I'm having (just starting to use ABS) is part warping and coming loose off the bed. To date I've used PLA but for printer parts, want to use something a little more temperature stable. I've got PEI on glass but for the life of me am having a dickens of a time getting it to not warp and get ripped off. My filament (from [spool3d.ca](http://spool3d.ca)) says 80-100C bed temps but nothing in that range makes it better? I've found if I do HUGE brims that 'sometimes ' it will stick.. just those first 10-15 layers are the kicker, after that it's fine?

(On a Eustathios, not [yet] enclosed)[spool3d.ca - 3D Printers, Filament and Supplies in Canada &#x7c; SPOOL3D.ca](http://spool3d.ca)


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/DPPF68my5zT) &mdash; content and formatting may not be reliable*
