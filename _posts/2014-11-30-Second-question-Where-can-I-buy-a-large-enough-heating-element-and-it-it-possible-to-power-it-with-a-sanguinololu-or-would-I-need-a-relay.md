---
layout: post
title: "Second question. Where can I buy a large enough heating element and it it possible to power it with a sanguinololu or would I need a relay?"
date: November 30, 2014 04:48
category: "Discussion"
author: Ethan Hall
---
Second question. Where can I buy a large enough heating element and it it possible to power it with a sanguinololu or would I need a relay?







**Ethan Hall**

---
---
**D Rob** *November 30, 2014 04:50*

Alirubber on [aliexpress.com](http://aliexpress.com). you'll need a ssr


---
**Eric Lien** *November 30, 2014 05:30*

I agree with rob. Go 110v on the bed for alirubber, use an DC to AC SSR to power the bed from the mains. Hook your sanguinololu bed outputs to the DC side. Hook the bed up to mains through the AC side of the SSR. Just don't get a cheap SSR, I got an omron SSR. 


---
**Jim Wilson** *November 30, 2014 06:21*

You'll regret going through the board for the heated bed, I did. It takes FOREVER to heat up even a smallish build plate with the added thermal mass of a metal or glass surface.


---
**Mike Miller** *November 30, 2014 13:26*

So, with 110v direct to the build plate, just how long would you estimate it takes to get to 100C?



I print at 90C as 12v 35amp gets there a whole lot faster than 100C...I'll turn the printer on, tell it to reach temp, then take the SD card to the kitchen to prep the model to print, by the time I'm back, it's usually pretty close to the desired temp.  (10-12 minutes)


---
**Eric Lien** *November 30, 2014 14:23*

I get to 110c in 3.5min with 110v, 1/4" thick heat spreader, and 800W heater. I actually had to turn down my heater max output in PID due to overshoot. 700W would have been perfect.


---
**Mike Miller** *November 30, 2014 14:38*

90C took about 7 minutes this morning. Are you using a glass build plate? Worried about thermal shock?


---
**Eric Lien** *November 30, 2014 14:46*

**+Mike Miller** no issues so far. And I am just using window glass ( 17" boro was too much$)


---
**Daniel F** *December 01, 2014 19:57*

I would also go for an AC powered 110/220V silicon heater with SSR, especially with a heat bed larger than 30x30cm. I use a 24V silicon heater with 350W (340x340mm) it takes forever to get hot. Today, from 8°C (in the basement) to 90°, it took 18 min 45s (in summer it takes 15min). To reach 100°C it takes around 23 min. You also need a big PSU, just for the heater. I even added rockwool below the bed to keep the heat where it belongs to.

With the difference to smaller PSU you can probably buy an SSR.


---
**Seth Mott** *December 02, 2014 19:25*

Who has the best prices on 8 and 10mm solid smooth rods?


---
**Eric Lien** *December 02, 2014 21:15*

**+Seth Mott** I used Misumi, but smw3d has some great pricing: [http://www.smw3d.com/quenched-and-tempered-o1-steel-shaft/](http://www.smw3d.com/quenched-and-tempered-o1-steel-shaft/)


---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/gifZjCpUsx8) &mdash; content and formatting may not be reliable*
