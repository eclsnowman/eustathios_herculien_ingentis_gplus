---
layout: post
title: "Hey Eric Lien - i'm finally finding the time to overhink the carriage a bit as i already promised some weeks ago"
date: September 25, 2015 18:58
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Hey **+Eric Lien** - i'm finally finding the time to overhink the carriage a bit as i already promised some weeks ago. I'm currently just looking around for ideas and playing around a bit in Fusion360.



No matter what i'm thinking about there's one thing that i feel is in the way for real flexibility there and this is the distance between the two inner/crossing rods. They don't allow to have linear ball bearings of standard size (LM8UU / LM8LUU). Their distance is 6mm edge to edge or 14mm center to center, right? Of course i know i could simply modify this but i thought it might be a good idea to modify this for future versions to allow more flexibility on the carriage.



There's not much that is missing for an LM8UU (15/16mm outer diameter, i've already seen both versions). Maybe raising the distance to 12mm, making it 20 center-to-center would be a good idea. This would give enough room to easily mount two LM8(L)UU above each other without any difficulties. It would even be enough for LM10(L)UU - but of course there would be other modifications needed too for 10mm crossing rods.



What do you think, Eric? Like i said it would be easy to make this changes downstream but i think having more flexibility in the project could be of use for everyone.



Keen to hear everyones feedback.





**Frank “Helmi” Helmschrott**

---
---
**Joe Spanier** *September 25, 2015 19:03*

I will say the LM8uu that are in my Open18 build have way too much slop to be useful in this setup. I can grab the carriage and rotate 1mm or so in either direction. So my end effector at its tip has a ton of play. I tried LM8UUs from two different suppliers and rods from 3 and had little change. 


---
**Frank “Helmi” Helmschrott** *September 25, 2015 19:17*

**+Joe Spanier** looks like you got really bad ones. I have the cheapest ones i can think of and i currently have them in (tho in a not so good temporary solution) and i don't have any issues with play.



But that's not the point - i don't want to decide for other people which solution is the better one - i think everyone has to use the solution that he/she thinks its best - i'm just aiming for more flexibility.


---
**Joe Spanier** *September 25, 2015 19:48*

Understood. They seemed like they were extraordinarily bad. Id never noticed LM8s be that bad before. 


---
**Eric Lien** *September 25, 2015 19:49*

One problem is further C-C means greater lever arm when the opposing rod moves the carriage down it's axis. I have thought about increasing spacing, but since I prefer the bushings I never had the need. ﻿


---
**Frank “Helmi” Helmschrott** *September 25, 2015 19:54*

sure, **+Eric Lien** but 6mm more would mean 3mm more lever arm on each side. Don't think this would mean much of a difference regarding this "problem", do you?



I see you prefer the bushings and i think they're probably good if your motors are powerful enough. Probably there's the the two setup situations where you have bigger/powerful motors with the bushings or motors with less power (in my case more resolution due to 400 steps/rev) and bearings. I did quite some printining since the bearings are in and am quite satisfied with it - but like i said... your milage may vary.


---
**Ishaan Gov** *September 25, 2015 23:12*

If you're also redesigning, remember to keep in mind center of gravity of the entire printhead--not sure if Fusion360 can do this, but I think other autodesk products can--just so that the center of gravity is optimally placed in between the two cross rods


---
**Frank “Helmi” Helmschrott** *September 26, 2015 06:12*

I think COG would be hard to automatically find for the software as it is definitely dependent from the infill density of the print and the weight and exact details of fans etc. I think it'll be enough to work it out myself like **+Eric Lien** probably did too.


---
**Jarred Baines** *September 26, 2015 11:41*

Cog would be STRONGLY based on the bearings and hotend assembly (and any fans) not so much on the printed body (I would imagine)


---
**Eric Lien** *September 26, 2015 12:41*

In my opinion having the hotend as close as possible to the intersection of the rods is the main goal. Lower lever arm from the intersection will yield the least deflection at the hot end tip... Which I believe is your main goal.


---
**Jarred Baines** *September 26, 2015 12:59*

Oh yeah, of course, I've been in this thought-loop before ;-)



And the one thing I could never decide on was if I were to completely re-do the x y, would I favour a large C-C or small, I just could never get my head around which was best.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/4s4NLxEkRBV) &mdash; content and formatting may not be reliable*
