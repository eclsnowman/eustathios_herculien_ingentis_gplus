---
layout: post
title: "Great interview of Walter Hsiao out on Thingiverse"
date: April 14, 2018 22:20
category: "Discussion"
author: Eric Lien
---
Great interview of **+Walter Hsiao** out on Thingiverse. It is worth a read about a great designer, community member, and fellow builder of the Eustathios printer. 



Walter made many of the great upgrades to the printer that the community now uses. 



Also just as a note about the article, giving me credit for the Eustathios design would be incorrect. I made an iteration of the great Eustathios design by **+Jason Smith**. Most of my work on it was remodeling it from Sketchup into Solidworks. Then I added further changes in Spider V2. But credit goes where it is due, to **+Jason Smith** for the Eusthathios, and before jason to **+Tim Rastall** for his Ingentis which inspired jason, and before tim to **+Brad Hill** with his t-slot Tantillus... The list goes on. I know it is more than they wanted to put in an article about Walter (not the history of one of his printers) but I never like taking credit for the work of others as I stand on their shoulders. 



[https://www.thingiverse.com/groups/thingiverse/forums/the-designer-corner/topic:29262](https://www.thingiverse.com/groups/thingiverse/forums/the-designer-corner/topic:29262)









**Eric Lien**

---
---
**Eric Lien** *April 14, 2018 22:24*

BTW congratulation on the article Walter. Your designs are truly an inspiration about how form and function need not be at odds. When I finish a design I try and think... how would walter make this look less blocky. I still have  along ways to go, but you inspire me to be better.


---
**ThantiK** *April 15, 2018 04:49*

Yeah, **+Tim Rastall** kind of disappeared from G+ altogether.  I should really put together a HercuLien myself, as I never quite finished my Ingentis due to my hackerspace crashing down around me and losing a lot of friends that I was building with in tandem.


---
**Eric Lien** *April 16, 2018 13:01*

**+ThantiK** unfortunately a lot of people disappeared from g+. I hear a lot of the traffic went to Facebook (I don't know I stopped using Facebook account years ago, then deleted it about 8months ago).



Kinda miss the G+ hayday. The amount of quality posts per day was really inspiring.


---
**ThantiK** *April 16, 2018 13:49*

**+Eric Lien** I'm working on that, hopefully.  Some guys local to me recently wanted to start a new 3D printing forum of sorts, and came to me asking what we'd need for it, etc.



Nils also suggested Tom's forum, but I gotta check that out too.  I don't know what we're gonna do with the G+ community.  The developers and the company behind it have obviously given up on it.


---
**Eric Lien** *April 16, 2018 17:49*

**+ThantiK** I hear ya. G+ was the perfect combination of interest based forum, asynchronous chat, direct user connectivity via hangouts, and when it was a priority for Google the platform was fast and getting features regularly. Plus push notifications direct to my phone is great.



It's a tall order to find a replacement. But I guess long term something needs to be found if the community is to be revitalized.



I guess one question is why are there less people. Is the community not as busy because of the platform loosing favor? Or is there less custom builders with the ubiquity of capable/affordable off the shelf options?


---
**ThantiK** *April 16, 2018 19:05*

**+Eric Lien** The thing is, we're also a help/support forum for those affordable off the shelf options.  There should be <i>more</i> posts, not less, because I'm sure these things are far from plug-and-play given their lower than average quality.



I've asked some of the Google employees on the G+ specific help forum, explaining my frustrations with the platform and all I keep getting is basically "our tools are perfectly fine, you just need more moderators" and hand waving saying that I'm not explaining my problem and that I'm not reporting my problems with enough detail.  Gina even chimed in and still the same thing.  "The tools are fine, everything is caught in the spam folder and things don't get false flagged"...which isn't true.



So even the people assigned with the task of helping the G+ platform are blind to the problems facing it.  That, along with things like Hangouts being separated, and then Allo and whatever Google's third messaging platform is...I just don't see any reason to keep it on Google's ecosystem.



I'm working with some people for another 3D print board, and I've asked them if I could be a moderator there, but haven't spoken with them in a bit.  The site is called "3DPubs" - as in like a bar atmosphere, and I'm friends with the guys who are developing the underlying infrastructure.



Other possibilities are moving everything over to the RepRap forums, Tom's Forums (though this isn't neutral-ground, so I'm not so sure about it)



I want to keep an active community, but posting is dwindling because of the lack of development, and the lack of feature-parity with other social platforms.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/Bc9HyQ4EsVT) &mdash; content and formatting may not be reliable*
