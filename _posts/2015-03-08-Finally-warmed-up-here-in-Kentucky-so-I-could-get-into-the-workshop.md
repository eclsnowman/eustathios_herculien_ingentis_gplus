---
layout: post
title: "Finally warmed up here in Kentucky, so I could get into the workshop"
date: March 08, 2015 22:26
category: "Show and Tell"
author: Bruce Lunde
---
Finally warmed up here in Kentucky, so I could get into the workshop. Test fit of the first components!

![images/a700ca5ef659c2a7dc0383ec44f1ed31.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a700ca5ef659c2a7dc0383ec44f1ed31.jpeg)



**Bruce Lunde**

---
---
**Dat Chu** *March 08, 2015 22:39*

Man, need to pick mine up from my dealer. ☺ Yours is looking great. 


---
**Eric Lien** *March 08, 2015 22:52*

Looking great. Hopefully the **+Daniel Salinas**​ assembly guide is coming in handy. He is going such an amazing job with it.


---
**Bruce Lunde** *March 08, 2015 23:42*

**+Eric Lien**​ You are correct, **+Daniel Salinas**​ instructions are up on the screen as I go about this build!


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/75p2dsnRPXS) &mdash; content and formatting may not be reliable*
