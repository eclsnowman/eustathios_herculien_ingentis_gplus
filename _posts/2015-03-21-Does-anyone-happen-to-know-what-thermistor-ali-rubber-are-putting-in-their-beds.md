---
layout: post
title: "Does anyone happen to know what thermistor ali rubber are putting in their beds?"
date: March 21, 2015 07:20
category: "Discussion"
author: Oliver Seiler
---
Does anyone happen to know what thermistor ali rubber are putting in their beds? They say '100k', but that seems to be a bit generic. 

In my Smoothieboard config I can choose between EPCOS100K, Honeywell100K, RRRF100K and HT100K.





**Oliver Seiler**

---
---
**Maxim Melcher** *March 21, 2015 09:15*

Typical thermistor from China is "3950 100k", in Marlin "beta 3950"﻿


---
**Eric Lien** *March 21, 2015 11:32*

I have a thermistor table I built in my HercuLien Marlin files on github. I repurposed #60 in the thermistortables.h by commenting out the old values and adding mine.


---
**Eric Lien** *March 21, 2015 11:41*

But on smoothieware you can use the Beta value only if you want.


---
**Oliver Seiler** *March 22, 2015 00:33*

Thanks guys.


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/efA1VPMvRmX) &mdash; content and formatting may not be reliable*
