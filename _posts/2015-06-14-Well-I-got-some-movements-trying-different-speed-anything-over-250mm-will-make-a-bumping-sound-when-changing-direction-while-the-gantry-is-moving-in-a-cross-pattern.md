---
layout: post
title: "Well I got some movements, trying different speed, anything over 250mm will make a bumping sound when changing direction while the gantry is moving in a cross pattern"
date: June 14, 2015 23:14
category: "Show and Tell"
author: Vic Catalasan
---
Well I got some movements, trying different speed, anything over 250mm will make a bumping sound when changing direction while the gantry is moving in a cross pattern




**Video content missing for image https://lh3.googleusercontent.com/-8Oz9Yh5JgJ0/VX4KIwyl8gI/AAAAAAAAN5U/kyXj9uKtyOs/s0/20150613_213223.mp4.gif**
![images/af6108b100a674fcc5606b47a6ce3b9e.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/af6108b100a674fcc5606b47a6ce3b9e.gif)

**Video content missing for image https://lh3.googleusercontent.com/-s1_UAiForL4/VX4KI3VhRzI/AAAAAAAAN5U/JgXWb3q_xHg/s0/20150613_213502.mp4.gif**
![images/3def1fd1b50b34897f7cf5a900451d71.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3def1fd1b50b34897f7cf5a900451d71.gif)

**Video content missing for image https://lh3.googleusercontent.com/-Hxg8Y175MxM/VX4KI3mTzeI/AAAAAAAAN5U/dvomw2MM01Q/s0/20150613_213552.mp4.gif**
![images/522be64572dbff8042e3858f68e76ba9.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/522be64572dbff8042e3858f68e76ba9.gif)

**Vic Catalasan**

---
---
**Eric Lien** *June 14, 2015 23:51*

Looks like its happening on deceleration. Maybe some jerk setting tweaks?﻿


---
**Bruce Lunde** *June 15, 2015 00:05*

looks like a good start....


---
**Eric Lien** *June 15, 2015 00:05*

Plus things look slower when they are that big, those are long traverses going on there :)


---
**Vic Catalasan** *June 15, 2015 17:10*

**+Eric Lien** I agree, 250mm is pretty fast, what is the typical print speed we are getting with the Herculien?



I have not played with the jerk setting, I copied that portion from Herculien Marlin setting.


---
**Eric Lien** *June 15, 2015 17:23*

**+Vic Catalasan** high speed printing is almost always part size dependant. Very large parts can be 150mm/s, small parts can be below 30mm/s. I leverage minimum layer times often to help. If the previous layer or feature is not sufficiently cooled you will get corner/feature curling/deformation. I print mostly in ABS so I cannot use part cooling to speed things up.



Sorry for the non-answer. But the part and geometry really is the determining factor. But I print between 80 and 100mm/s often. But I slow outer most perimeter for better surface finish.


---
**Vic Catalasan** *June 15, 2015 17:30*

**+Eric Lien** Maybe it was not a great question since there are so many factors to consider. I have yet to print anything and just received my first spool. Thanks for the help


---
**Eric Lien** *June 15, 2015 20:31*

**+Vic Catalasan** Once you get to the printing stage hit me up. I will do what I can to get you some good starting settings.


---
**Øystein Krog** *June 15, 2015 20:48*

If you are using Marlin and 1/16 microstepping, you could very well be running up against the max stepping frequency. There is a problem where if you exceed the max stepping frequency (250mm/s for my setup) there are really ugly movement artifacts. See my video: [https://plus.google.com/+%C3%98ysteinKrog/posts/FPpTKnrfE27](https://plus.google.com/+%C3%98ysteinKrog/posts/FPpTKnrfE27)

Try to reduce microstepping, just to reduce the necessary steps that marlin has to output.


---
**Vic Catalasan** *June 15, 2015 22:02*

I have set my board to 1/2 stepping, which gives the correct travel distance based on the Herculien Marlin settings. I was trying to find the max speed the machine would run without loosing steps. It will safely move around at 250mm but I am sure when loaded could be a bit lower


---
**Eric Lien** *June 15, 2015 22:07*

I have printed before at 250, its exciting... But it feels like something could break at any second... And 8bit board stutters. 150 is screaming fast in reality. Match the printer with a volcano and a bondtech extruder... And the parts complete fast enough to make your head spin. 


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/DVuHFagLGb4) &mdash; content and formatting may not be reliable*
