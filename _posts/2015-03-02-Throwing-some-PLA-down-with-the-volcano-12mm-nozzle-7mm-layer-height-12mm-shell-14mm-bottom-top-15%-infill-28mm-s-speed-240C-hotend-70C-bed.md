---
layout: post
title: "Throwing some PLA down with the volcano: 1.2mm nozzle, .7mm layer height, 1.2mm shell, 1.4mm bottom/top, 15% infill, 28mm/s speed, 240C hotend, 70C bed"
date: March 02, 2015 01:13
category: "Show and Tell"
author: Jason Smith (Birds Of Paradise FPV)
---
Throwing some PLA down with the volcano: 1.2mm nozzle, .7mm layer height, 1.2mm shell, 1.4mm bottom/top, 15% infill, 28mm/s speed, 240C hotend, 70C bed. Model dimensions: 82mm x 103mm x 71mm. Total time: 75 minutes. 

![images/ed3a67aabcd17797afc32a2623782a9a.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ed3a67aabcd17797afc32a2623782a9a.gif)



**Jason Smith (Birds Of Paradise FPV)**

---
---
**James Rivera** *March 02, 2015 01:28*

Clearly it's fast. But around 1:00 or so, it seems to be pausing a lot. 


---
**Jason Smith (Birds Of Paradise FPV)** *March 02, 2015 01:29*

Yeah, I disabled combing on the skin in Cura, so it ended up with a lot of retraction.


---
**Dave Hylands** *March 02, 2015 04:10*

Can somebody explain what combing is?


---
**Bruce Lunde** *March 02, 2015 05:17*

Man, that is nice to see. I purchased a volcano for my Herculein build. Can't wait to get it built!


---
**Chris Brent** *March 02, 2015 18:16*

Has anyone done dual extruders with a volcano in one side and a regular nozzle in the other? Sounds like the best of both worlds to me?


---
**James Rivera** *March 02, 2015 18:27*

**+Dave Hylands** Cura "Combing" appears to be the same as, "avoid crossing perimeters" in Slic3r, with the intent being to not scar the surface of the print. More info here: [http://umforum.ultimaker.com/index.php?/topic/2949-combing-aka-avoid-crossing-perimeters/](http://umforum.ultimaker.com/index.php?/topic/2949-combing-aka-avoid-crossing-perimeters/)


---
**James Rivera** *March 02, 2015 18:33*

**+Chris Brent** agreed. Slic3r has support for an "infill extruder" on the Print Settings tab that the Volcano seems ideally suited for.


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/7XGsPZsYejS) &mdash; content and formatting may not be reliable*
