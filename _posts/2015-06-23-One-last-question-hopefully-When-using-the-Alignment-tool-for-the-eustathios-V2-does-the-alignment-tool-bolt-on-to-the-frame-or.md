---
layout: post
title: "One last question (hopefully). When using the Alignment tool for the eustathios V2, does the alignment tool bolt on to the frame or?"
date: June 23, 2015 02:39
category: "Discussion"
author: Gus Montoya
---
One last question (hopefully). When using the Alignment tool for the eustathios V2, does the alignment tool bolt on to the frame or? Does anyone have picture's of their proper use?





**Gus Montoya**

---
---
**Eric Lien** *June 23, 2015 04:19*

They don't need to be bolted on. Here is how I used mine. Please note you may still have to tweak it to really dial it in once you have used them.  The precision of the printed alignment tools is only as good as the accuracy of the printed guides and the assembly. This should just give you a good starting point: [https://plus.google.com/+EricLiensMind/posts/Bso1ocs77iv](https://plus.google.com/+EricLiensMind/posts/Bso1ocs77iv)


---
**Gus Montoya** *June 23, 2015 04:28*

Thanks, yeah I looked at those pictures. But was not sure if you bolted down or not. Thanks :)


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/JcoJ498ycqW) &mdash; content and formatting may not be reliable*
