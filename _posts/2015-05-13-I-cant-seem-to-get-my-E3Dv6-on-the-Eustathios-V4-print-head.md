---
layout: post
title: "I can't seem to get my E3Dv6 on the Eustathios V4 print head"
date: May 13, 2015 23:16
category: "Discussion"
author: Erik Scott
---
I can't seem to get my E3Dv6 on the Eustathios V4 print head. This is as far as I could get it on when I attempted to test fit it. I'm afraid my printer might be printing a bit on the large side for some reason, as things always seem tighter than they should be. 



Any suggestions?



![images/c28cb1722f4f41bd4a774b8f3d3c53b5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c28cb1722f4f41bd4a774b8f3d3c53b5.jpeg)
![images/e204840126138a4a1973016e1a2193c6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e204840126138a4a1973016e1a2193c6.jpeg)
![images/f44185776a08f3fe278e5186d74e10c4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f44185776a08f3fe278e5186d74e10c4.jpeg)

**Erik Scott**

---
---
**Daniel F** *May 13, 2015 23:21*

In such situations I use needle files to make things fit.


---
**Gus Montoya** *May 13, 2015 23:31*

Your parts came out very stringy.


---
**Erik Scott** *May 13, 2015 23:34*

Part of that's the macro shot from the camera. You can't see nearly that much detail up close. I do agree the bottom side of the fan mount isn't the best. My support probably wasn't dense enough, and cura's infil isn't great to begin with. Eventually I'll get simplify3d. 


---
**Isaac Arciaga** *May 13, 2015 23:50*

Do you have a file? The fit should be as snug as possible. A few swipes with a file at the 4 corners will allow u to work the hotend in with a twisting left/right motion.


---
**Erik Scott** *May 14, 2015 00:07*

Yeah, I had to actually chisel some of the plastic off, but I got it in most if the way. Enough to call it good. It's certainly snug. 


---
**Oliver Seiler** *May 14, 2015 00:13*

I had the same problem. It worked in the end by cutting away a little bit at the corner of the carriage and heating the E3D with a hot air gun before putting it in place 


---
**Eric Lien** *May 14, 2015 00:23*

My second one came out perfect with .98 extrusion multiplier. But on my first I wiped the mating surfaces with a qtip with acetone, then press in (I use ABS parts). Makes it a perfect fit.


---
**Eric Lien** *May 14, 2015 00:24*

BTW nice surface finish. Great prints. That fan offset surface is a bugger to support.


---
**Erik Scott** *May 14, 2015 01:11*

Thanks! I still get more inconsistencies in the print than I'd like. I think there's something weird about my z-axis, as I get all these horizontal bands. I have a complete set of new parts for my re-build, and I'm thinking about moving to the external motor setup. 


---
**Mike Miller** *May 15, 2015 12:46*

I'm running into the identical situation, and the dremel is my friend. In my case, I scaled up 1.05% and the carriage Axis that prints parallel to XY came out perfect, the axis the prints vertically needed clearancing with a drill, and I'm still working on getting the extruder to fit snug (ran out of time this morning prior to going to work.) I'll play with Acetone this weekend. 



FWIW, and maybe it says more about my willingness to create stuff that's 'good enough', but if you zip-tied the extruder in place in the photos above, I'd bet it would print just fine. 


---
**Erik Scott** *May 15, 2015 13:13*

I will be zip tying it up. This is just a test fit. As for scaling it up, that's probably going to result in some issues when you go to assemble things. Generally, I prefer to play with the tolerances in CAD instead of scaling things. 


---
**Eric Lien** *May 15, 2015 14:43*

In ABS I print based on the the extrusion multiplier that a single 0.5mm walled cube determines (at planned print speeds) for that roll. Then I scale all models by 1.008% which is the standard scalar shrink factor for ABS from 240C down to room temp (Martin gave me this one... he did injection modling system design for years). After doing that parts now all fit like a glove first time.


---
**Chris Brent** *May 15, 2015 18:09*

Hey **+Eric Lien** can you expand on this a little more: "In ABS I print based on the the extrusion multiplier that a single 0.5mm walled cube determines (at planned print speeds) for that roll. "

If you print a 20mm cube with .5mm walls what do you measure to get the extrusion multiplier? 


---
**Eric Lien** *May 15, 2015 19:43*

**+Chris Brent** print a calibration cube in vase mode (1 perimeter). Now measure. Is that wall as thick as you told it to be (I use .5mm extrusion width as my default). If not adjust multiplier by the % difference, then reprint. Do this until you are on. You can double check by doing a 2 perimeter zero infill. Is that 1mm?


---
**Gus Montoya** *May 15, 2015 21:02*

These bits of information need to be archived some how. I've been using "print screen" to save them in a folder. I think many would find these useful.


---
**Erik Scott** *May 15, 2015 22:50*

**+Eric Lien** So you do a 0.5mm extrusion width? Is this equivalent to the nozzle diameter? In Cura, there's no extrusion width, just nozzle diameter. I use a 0.4mm nozzle (The standard that comes with E3Dv6). 


---
**Eric Lien** *May 15, 2015 23:45*

**+Erik Scott** I guess I haven't used cura in a while. I use s3d. But I think you can do it in slic3r. Maybe in Cura you can use wall thickness setting?


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/ifauXRWX9aj) &mdash; content and formatting may not be reliable*
