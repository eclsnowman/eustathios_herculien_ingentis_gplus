---
layout: post
title: "A couple of questions about leadscrews: * Misumi's 400-500mm long, 10-12mm diameter leadscrews have a max runout of 0.27mm, is this likely to cause problems?"
date: June 03, 2015 00:40
category: "Discussion"
author: Luke Strong
---
A couple of questions about leadscrews:

* Misumi's 400-500mm long, 10-12mm diameter leadscrews have a max runout of 0.27mm, is this likely to cause problems?

* I'm thinking of fixing one end (2 spaced bearings) of the leadscrew and leaving the other end floating, any disadvantage of this compared to supporting both ends like the Herculien? Would fixing one end and supporting the other over constrain it?



Got most of the design sorted, just still not sure about the z axis. Was originally looking at using belts with a worm drive but I'm considering just using leadscrews. Still concerned about the runout from leadscrews and the dreaded z wobble. I have a Prusa i3 which has noticeable z wobble and don't want the same problems in this build, that could well be from trying to pass threaded rod off as a leadscrew and that there is a slight bend in them. I might beef up the smooth rod to 12mm to be safe if I put a leadscrew in.



There are a few local companies that offer machining of leadscrews that may be able to straighten it if runout turns out to be a problem. AFAIK leadscrews can be straightened with some v-blocks, a dial indicator and an arbour press (which I don't have).



Thanks in advance.





**Luke Strong**

---
---
**Dale Dunn** *June 03, 2015 01:35*

Even if the lead screw is perfectly straight, it won't be perfectly parallel to your smooth rods. Make sure the screw is coupled only in Z and you won't have to worry about straightness or parallelism or how many ends are fixed. For a half meter long screw, I suspect you're going to need both ends constrained.



I'm surprised an i3 has noticeable wobble. You're sure it's not layer height banding?


---
**Eric Lien** *June 03, 2015 01:38*

I don't have Z wobble on my HercuLien that constrains both ends, but I spent a lot of time aligning the components. Eustathios has a floating top which works well also. The reason I constaigned the top of HercuLien was to allow for more rapid dropping of the bed on z change. But that's because HercuLien has a single bearing at the bottom. On Eustathios spider v2 I sandwiched two bearings so the same thing could be accomplished while leaving the top floating.


---
**Mutley3D** *June 03, 2015 02:30*

Z axis related issues such as banding/ribbing can be a black art to prevent or resolve. Some causes include poor linear bearings or low precision rods, insufficient rod thickness, bent screws, no lube or dirty rods/screws etc. Whilst using/finding the best components you can for your budget is good practice, even spending silly amounts can leave you with banding or other z artifacts if not assembled correctly or poor assembly design/configuration. My own view is isolate the Z axis from the print bed as much as you can. Z isolators are key to this whether the screw is constrained or not. Personally i go for constrained top and bottom with a good Z isolator. The trick with such a part, is to make sure the mating surfaces are smooth, and lubricated, so they are free to slide relative to each other when they need to. Pretension on the linear bearings can also help (ie mounted such that there is a fraction of a degree offset from vertical on the linear bearings, with the rods being perfectly vertical - unconventional but effective).


---
**Luke Strong** *June 03, 2015 05:54*

**+Dale Dunn**

Sorry got mixed up, the problem is with layer height banding not z wobble. The smooth rods on my i3 seem to do a good enough job to stop any wobble.



Haven't seen any extreme close ups of Eustathios prints but they don't appear to have any noticeable z banding on them. Would a z isolator help with banding? My concern is that anything allowing for horizontal movement of the nut could introduce backlash. Interesting z isolator here 
{% include youtubePlayer.html id=tmMrsJ1Jmy8 %}
[https://www.youtube.com/watch?v=tmMrsJ1Jmy8&feature=youtu.be](https://www.youtube.com/watch?v=tmMrsJ1Jmy8&feature=youtu.be)


---
**Dale Dunn** *June 03, 2015 13:13*

The Z isolator (nice design in the video) eliminates wobble. It won't help any with banding, since banding isn't about horizontal movement of the platform. Just isolate the screws (or don't use them) and make sure there is no slop in the guides.



For anyone reading, banding is usually caused by using a layer height that is not an integer multiple of whole steps on the Z stepper. This leads to a cyclic error in layer height (instead of a consistent one), which causes plastic to squish more on some layers than others. You can get rid of this just by making a small correction to the slicing setting. There's a nice calculator here: [http://prusaprinters.org/calculator/](http://prusaprinters.org/calculator/)

Scroll down to "Optimal layer height for your Z axis".


---
*Imported from [Google+](https://plus.google.com/106147943249970698774/posts/Qb4Vsa8UzqR) &mdash; content and formatting may not be reliable*
