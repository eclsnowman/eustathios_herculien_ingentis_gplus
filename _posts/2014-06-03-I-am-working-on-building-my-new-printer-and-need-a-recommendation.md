---
layout: post
title: "I am working on building my new printer and need a recommendation"
date: June 03, 2014 03:28
category: "Discussion"
author: Jason Barnett
---
I am working on building my new printer and need a recommendation. I have only worked with 3mm filament and direct drive extruder. My new printer will use 1.75 and bowden extruder. Any recommendations on extruder (cold end) designs I should check out? 

I am currently looking at the airtripper v3 and thing:159159 bowden extruder. Any suggestions would be greatly appreciated. 





**Jason Barnett**

---
---
**David Gray** *June 03, 2014 06:24*

This 1 is pretty solid.

[http://www.thingiverse.com/thing:60531](http://www.thingiverse.com/thing:60531)


---
**Tim Rastall** *June 03, 2014 08:16*

Airtripper worked for me once I'd modified it to allow the Bowden tube to pass through the airtripper and butt up against the hobbed pulley. 


---
**Ethan Hall** *June 03, 2014 13:45*

E3d v6


---
**Jason Barnett** *June 04, 2014 14:33*

Thanks everyone for your input. I am hoping to go with a direct drive to avoid the extra gears so I am going to try the airtripper first and see how it goes. If I run into issues with my stepper motor skipping, I'll try the one that **+David Gray** suggested.


---
*Imported from [Google+](https://plus.google.com/108834075676294261589/posts/2QCAaKE7MKq) &mdash; content and formatting may not be reliable*
