---
layout: post
title: "Hello folks, back in june i had problems printig circles (see: )"
date: October 28, 2015 20:17
category: "Discussion"
author: Florian Schütte
---
Hello folks,

back in june i had problems printig circles (see: [https://plus.google.com/111818668280736846325/posts/SyWzEq4Tm59](https://plus.google.com/111818668280736846325/posts/SyWzEq4Tm59)). Small circles (few millimeters) are more rectangular than round. I'm trying to solve this problem since then. But had no luck. Now i am very desperate. I don't know what else i can do, to solve the problem. I hope, someone of you has an idea, what else i may check. Here a list of things i checked:



1. Dimensions: Dimensions are ok. Printed 30 x 30mm cube. Measured ~30.05mm



2. Backlash. I fastened all pulleys (steppers and rods). I pushed them to the sides at the bearing blocks, so the rods aren't able to move forward and backward.



3. I played with the belt tension



4. Decreased acceleration



5. Decreased print speed



6. Changed steppers



7. Carriage rods are parallel to outer rods



8. Different slicers (Cura & Simplify3D)



9. New carriage + new bearings for carriage + complete new rods for x/y mechanics



My guesses, that i have not tried yet: 

- Stepper driver

- change all belt tensioners and bearing blocks incl. bearings



So has anyone an idea whats going on there or where to put further investigation?





**Florian Schütte**

---
---
**Florian Schütte** *October 28, 2015 20:18*

And the problems with infill are also not gone. Testprints done with ABS.


---
**Michaël Memeteau** *October 28, 2015 21:14*

One suggestion to clear anything that would be filament related would consist in substitute the hot-end by a simple pen. If the issue maintain you can probably focus on mechanical/electronics and do quicker test (no need to heat-up).

When moving the hot-end by hand, does it feel rigid? Any leeway noticeable?


---
**Michaël Memeteau** *October 28, 2015 21:37*

... Also trying to isolate any problem from the pulley, try to use some loctite or superglue (cyanoacrylate) between the shaft and the pulley. It's easily removable by applying heat, so reversible.


---
**Blake Dunham** *October 29, 2015 03:35*

Possibly try completely recalibrating all of your motors and steps/mm just to be sure. Also check your motor amperages to make sure they aren't skipping steps


---
**Florian Schütte** *November 09, 2015 17:45*

**+Blake Dunham** I alredy checked the things you mentioned twice.



**+Michaël Memeteau**  I secured the pulleys. But no luck. Also it seems not to be related to the filament. I don't test with a pencil, but now with two different PLA, two different ABS and HISP. All the same.



My last try now: i ordered new printed parts (Bearing Holders and XY-Tensioner Assembly). The parts i have now are printed on an friends crappy rigidbot. I'm afraid of they have some issues. I will take this as an opportunity to switch to the v2 version.

This will take some time. I have a lot other things to do at the moment. 



Until then, my prints will have squared holes - Especially small ones or rings with small walls - and not that good infill on top and bottom. BUT results are ok.


---
**Blake Dunham** *November 09, 2015 17:49*

Just a few more things you could check. Your hotend could be loose and moving around. If you are using a Bowden extruder you could be having some inconsistent extrusion problems. In some cases that can give the illusion that you have a mechanical problem. 


---
**Florian Schütte** *November 18, 2015 23:08*

**+Blake Dunham** Will this extrusion problem really end in squared holes? I sounds only plausible for the gaps between wall and infill.


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/YEPgJNLZhoQ) &mdash; content and formatting may not be reliable*
