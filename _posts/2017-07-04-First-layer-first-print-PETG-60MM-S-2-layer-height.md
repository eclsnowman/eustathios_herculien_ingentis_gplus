---
layout: post
title: "First layer, first print! PETG 60MM/S .2 layer height"
date: July 04, 2017 14:41
category: "Build Logs"
author: James Ochs
---
First layer, first print!  PETG 60MM/S .2 layer height.



This printer is rediculously quiet and the first layer is so much better than my old printer that I spent two years trying to get calibrated! 


**Video content missing for image https://lh3.googleusercontent.com/-xCm18vZZLpw/WVupJ4yfevI/AAAAAAAAyhM/hXF6UCTJjRUwIEkeOfCdJn143IfXNRlvACJoC/s0/VID_20170704_103716.mp4**
![images/69bfa4ee96310d6b087e9473a2b0530e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/69bfa4ee96310d6b087e9473a2b0530e.jpeg)



**James Ochs**

---
---
**Brad Vaughan** *July 04, 2017 15:00*

Great to see you up and running!



Same here... My Eustathios is the quietest printer I've had (once you  replace that E3D fan), and it produces some very high quality prints.


---
**James Ochs** *July 04, 2017 15:21*

Heh, you are right, that e3d fan is the noisiest component on the printer.  It's going to live in my office, so I think I can live with it since it is still quieter than my paper printer ;)


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/XDK5fHQ3wjW) &mdash; content and formatting may not be reliable*
