---
layout: post
title: "First Herculien print with some cheap PLA , trying to figure out the correct steps per mmm on the Z-axis"
date: September 23, 2015 07:10
category: "Show and Tell"
author: Jo Miller
---
First Herculien print with some cheap PLA ,  

trying to figure out the correct steps per mmm on the Z-axis.  so far x y +z  running on 32bit DRV 8825  but thinking of changing the chip on the z axis to a 16 bit one.



Setup of the AZSMZ-board (with its own Display ) was easy.



![images/a9e987c517596d10f0249db34e16892f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a9e987c517596d10f0249db34e16892f.jpeg)
![images/c507937fbee3abe524b2680f316c7cef.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c507937fbee3abe524b2680f316c7cef.jpeg)
![images/b23419e99d980c0585d500f09c4ad87a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b23419e99d980c0585d500f09c4ad87a.jpeg)
![images/98b4ad4483fa2a9fda5ec6caebe951ba.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/98b4ad4483fa2a9fda5ec6caebe951ba.jpeg)
![images/9c0e59db1fa8dad07cdc564b267b04d4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9c0e59db1fa8dad07cdc564b267b04d4.jpeg)
![images/86ab0e5d28d67852bea887b4be39530e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/86ab0e5d28d67852bea887b4be39530e.jpeg)

**Jo Miller**

---
---
**Eric Lien** *September 23, 2015 07:23*

I use this: [http://prusaprinters.org/calculator/#stepspermmlead](http://prusaprinters.org/calculator/#stepspermmlead)



1/32 isn't listed. So just double the value of 1/16 if you go that route.

If you used the BOM leadscrews the pitch is 2mm. For the gear ratio the first box is number of teeth on your stepper pulley, second box is teeth on your lead screw pulley.



Hope that helps.


---
**Eric Lien** *September 23, 2015 07:24*

BTW I tend to set microstepping to 1/16 or 1/8 on my Z. 


---
**Michaël Memeteau** *September 23, 2015 07:37*

I guess the apple used to calibrate the Z axis "à la Newton" :)

Why 2 thermistor on the silicon mat? Redundancy? Backup?


---
**Jeff DeMaagd** *September 23, 2015 11:46*

**+Michaël Memeteau**

The manufacturer will include it for free (or $1?), so why not have a spare? It can be a safety thing too, if you feed the second thermistor to a separate watchdog device to prevent thermal runaway.


---
**Jo Miller** *September 23, 2015 18:58*

yes, planned as rundundancy backup, or as a watchdog device, and yes the alirubber-guys didn´t charge me any extras for that   :-)



the apple is purely for gravity tests reasons. for leveling out the printbed i tend to use an egg     :-)


---
*Imported from [Google+](https://plus.google.com/103341289176473342280/posts/EbyVRGLm7an) &mdash; content and formatting may not be reliable*
