---
layout: post
title: "Questions about assembly on bearings and pulleys"
date: May 25, 2016 17:39
category: "Discussion"
author: Pieter Koorts
---
Questions about assembly on bearings and pulleys. The ball bearings and the holders, how do people hold the bearing in place? Do you glue it or just have the pulley hold it in place.



I see on spider v2 that the steppers use smaller pulleys than the 32 teeth ones on the rods. Does this effectively make a 1.8 degree stepper provide 0.9 steps in terms of gearing. Lets say 16 teeth on the stepper and 32 teeth on the rods.





**Pieter Koorts**

---
---
**Eric Lien** *May 25, 2016 18:08*

Which hearings? If you mean the ones on the gantry side rods those are held in by the pulley grub screws. The lead screw ones are hend by gravity on the upper bearing, and a collar on the lower (but the lead screw could be machined with a thread too and go to a nut which would be even better. The bronze bushings on the rod end carriages and central carriage are held in by ABS softening with acetone and pressed in (assuming you printed in ABS). Otherwise if you use PLA or PETG for those parts use a glue of some sort, but try not to make the fit too tight or you can split the part due to how it is printed.



Yes there is pulley/gear reduction on the gantry and z stages. I go with 20tooth on the motor, ad 32tooth on the rods. But others have played with different ratios too. I just used what the original Eustathios by **+Jason Smith** designed and it works great of me. I have found 16tooth pulleys to have less tooth engagement than I would like so stuck with the standard pulleys. But 32/16 reduction would give integer values on the reduction which might be nice on the Z-stage to get full step optimal layer heights to equal more standard layer sizes (.1mm, .2mm, etc.) Right now the full step optimal layer heights are easily calculated, but do not work out to be nice round numbers: [http://prusaprinters.org/calculator/](http://prusaprinters.org/calculator/)


---
**Pieter Koorts** *May 25, 2016 18:30*

Yeah. Sorry I meant the gantry ball bearings. I do have some 16tooth pulleys so might give it a try but will likely go standard if it does not work. Thanks Eric


---
**Daniel F** *May 26, 2016 22:06*

I just installed 16 tooth pulleys on all 3 axes and so far it works. Max speed is limmited to 125mm/s for XY in my setup but I never print at speeds above 100mm/s.


---
**Pieter Koorts** *May 26, 2016 22:32*

**+Daniel F** speed is not something I am aiming for but obviously super slow would be just as bad. I think 100mm/s is a pretty good starting point. If I find it is too slow I will probably just get larger pulleys.


---
**Eric Lien** *May 26, 2016 23:00*

I have tested as high as 250mm/s before with 20/32 pulley combo. So 16 tooth should be no issue at realistic print speeds.


---
**Daniel F** *May 27, 2016 06:52*

sorry I forgot to mention that these speed values are with 128 microstetps and an Arduino Due/Repetier board , max steps/s of this combination is about 100k. I will keep an eye on the belts to see if there is more wearing as the force per tooth is probably a bit bigger with smaller pulleys


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/CNrdEMExzFw) &mdash; content and formatting may not be reliable*
