---
layout: post
title: "Just echoing some thoughts: I'm just looking at buying the linear shafts (smooth rods) for the Eusthios spider v2, and it occurs to me that, unless I am mistaken, you could simply remove 3 of the top shafts and this printer"
date: August 10, 2015 05:15
category: "Discussion"
author: Ted Huntington
---
Just echoing some thoughts: I'm just looking at buying the linear shafts (smooth rods) for the Eusthios spider v2, and it occurs to me that, unless I am mistaken, you could simply remove 3 of the top shafts and this printer would function as an H-bot. So there must be some advantage to using 3 extra shafts- I'm guessing that it helps stability and accuracy. Another thing I am somewhat wondering is if everything could be done in 8m diameter shafts- it's not that big of a printer (around 500mm).





**Ted Huntington**

---
---
**Erik Scott** *August 10, 2015 05:23*

The Eustathios uses the ultimaker kinematics. The idea is to reduce the mass of the print head for fast travel. I also quite like the idea of having rods pull double duty as guides and as a way to transmit rotational motion. 



I suppose you could alter the design to create new XY kinematics, but I don't really see an advantage. You'd have to retain 2 rods for the X axis so that the carriage remains stable, and the X stepper would likely have to move on the Y axis, increasing the moving mass. What's nice about the current design is the X and Y axes are essentially eqnivilent, kinematically and dynamically. You don't have different inertias along each axis. 



Because of the size of the printer, I would NOT recommend using only 8mm rods as you may start to see deflections which would adversely affect the print quality. ﻿



EDIT: I see you mentioned that you could simply create a Cartesian H-bot by removing 3 of the rods (I presume 2 of the 10mm rods and one of the 8mm rods) without any further modifications. If this is the case I think you have a fundamental misunderstanding of how the printer works. Take a close look. Each of the 10 mm rods provides rotational motion to drive the belts that drive the perpendicular axis while simultaneously acting as the linear rail for the parallel axis. It's a clever design and I like it a lot. 


---
**Igor Kolesnik** *August 10, 2015 05:29*

Sure you can, but it will not be Eustathios Spider V2 it will become an h-bot. I started my build from 8mm rods, it works but they feel flimsy and having x/y assembly solid, is a nice part of good quality prints. Upgraded to 10mm.﻿ Hbot design has its problems as any other, this one is easier to calibrate and is not that sensitive to belt tension  difference. In ideal world I might have thought about building hbot but this IMHO is a more solid design. BTW you ask Eric why he switched from his hbot.


---
**Ted Huntington** *August 10, 2015 05:36*

**+Erik Scott**

Great points- I totally missed that you would need to have a stepper motor that travels with the Y axis- so it's not possible to just remove 3 shafts to have an H-bot design- the X-motor would need to be moved.



This is one reason why I ask about 8mm shafts- to see if people have found problems with 8mm shafts on a 500mm design.


---
**Ted Huntington** *August 10, 2015 05:38*

**+Igor Kolesnik**

thanks- I'm convinced- if you went through all that trouble to find out that answer!


---
**Erik Scott** *August 10, 2015 05:43*

The Eustathios is not a 500mm design. It has a build volume slightly less than 300x300x300mm and the frame has a width and length of 465mm. Even so, I think 8mm would be a bit thin for the 4 primary rods as they take a fair amount of load. I'm not the original designer, but I believe the existing 2 rods that support the carriage are 8mm to reduce the moving mass. You could go ahead and try it and tell us how it works out. The design can easily accommodate either. You'll just have to use different rotational bearings in the bearing holders and swap out the bushings in the riders. 


---
**Frank “Helmi” Helmschrott** *August 10, 2015 06:17*

just becaus it fits here. If you're looking for an open hardware H-Bot-style printer you might want to look at the quite popular Sparkcube. Unfortunately i couldn't find any english language information on a quick hand. It is a project that has grown up in Germany and therefore lots of information is in German. It is in many details different to the Eusthatios project though.


---
**Ted Huntington** *August 10, 2015 14:38*

Thanks for all the info- I am looking for a good open-source H-bot design, but I think I can see the value of the Eustathios design- because the X-motor does not need to ride on the Y-axis- that must make the carriage much lighter.


---
**Erik Scott** *August 10, 2015 14:48*

A robust H-bot design would be good if you're looking to go for a direct-drive setup so that you can print flexible filament. If that's not something that you're going to be doing, then the ultimaker kinematics are the way to go. 


---
**Ted Huntington** *August 10, 2015 15:28*

So flexible filament doesn't work with a bowden tube huh- that's too bad if true, and you can't do direct-drive with the ultimaker design I guess- not that I want to- I pretty much am happy with the bowden tube design.


---
**Erik Scott** *August 10, 2015 15:32*

Think about it this way, have you ever tried to push a rope through a tube? That's how it's been described here in this very community. And you COULD do direct extrusion on the ultimaker, but you'd have to make everything beefier. Replace the 8mm rods with 10, and the 10mm rods with 12. You'll also lose build space due to the size of the carriage. 


---
**Ted Huntington** *August 10, 2015 15:35*

forget that then! I currently have no direct-drive printer so I guess I will have to wait to use flexible filament ;(


---
**Frank “Helmi” Helmschrott** *August 10, 2015 18:05*

Just to clarify one thing: On an "H-Bot-Design" (which for me includes the H-Bot and the CoreXY-Design) the both X/Y motors also don't move. Related to that there's no difference from the Eusthatios/Ultimaker-design to H-Bots.



There is of course one design where the X-Motor rides along the Y axis. This is for example used in the new BigBox3D from E3D that is currently on Kickstarter.



The CoreXY ([http://corexy.com/](http://corexy.com/)) is what is used in the Sparkcube i linked above and which i am aiming to build as a direct drive printer next. Of course when building as a direct drive you have to move the Extruder motor which you don't have to move with a bowden - but that is also true for both, Eusthatios and H-Bots.


---
**Erik Scott** *August 10, 2015 18:15*

I didn't realize the H-bot design is different from the type of kinematics used on the BigBox3D. Sorry for any confusion. 



In any case, some designs are still more suited to direct extrusion than others, ones where the head is more rigidly mounted to the frame. A direct extrusion Eustathios wouldn't work very well, I don't think. 


---
**Frank “Helmi” Helmschrott** *August 10, 2015 18:18*

Well yes and no - the Herculien is used to do direct extrusion as far as i understood. You have to go slower of course but i think it may work on an eusthatios too. Though the CoreXY design is truly more made for this. It offers enormous stability in design due to the crossing belts. H-Bot itself isn't that stable.


---
**Erik Scott** *August 10, 2015 18:48*

For sure. No disagreement there. One of the important aspects of the Eustathios is its ability to print quickly. A heavy printhead requiring you to move slowly kinda defeats that point. 


---
**Ted Huntington** *August 11, 2015 03:18*

So I am thinking that an "H-bot" is mostly defined by the linear shafts which have the H shape. The CoreXY is the only H-bot design I know of where the X-axis motor does not move along with the Y axis. Hearing about Eric's trouble with the CoreXY design, and the complicated nature of all the belting leads me to other more simple designs.


---
**Ted Huntington** *August 11, 2015 03:23*

cool to hear about the bigbox- I had never heard of that- looks like a good project- open source thankfully


---
**Frank “Helmi” Helmschrott** *August 11, 2015 05:21*

**+Ted Huntington** The H-Bot is defined by the two motors beein fixed. The real H-Bot is just like CoreXY but there's only one belt. You can see it in action here in a demo 
{% include youtubePlayer.html id=cfUdKa20LZc %}
[https://www.youtube.com/watch?v=cfUdKa20LZc](https://www.youtube.com/watch?v=cfUdKa20LZc). The difference to CoreXY is that the belt doesn't cross at the end and there's only one long belt. This makes it a bit more unstable which is the reason why the CoreXY genereally is known as the better H-Bot.



So "H-Bot" is not a general word for the way the rods are aligned but more for the motion system where two motors share their work driving both axis not one for each motor.



Regarding complications the CoreXY isn't that problematic. I had a self-designed prototype running for nearly 2 years which ran very reliably and the Sparkcube is one of the most famous projects in the German RepRap area currently. You should look at their build videos - i think this isn't anyhow more complicated than an Eusthatios or Herculien. The only thing is that you have to have firmware that supports this motion system as like i said both motors share their work in driving the axis. But most Firmware that i know (Repetier, Marlin) do support H-Bots.


---
**Ted Huntington** *August 11, 2015 06:07*

Ah ok - thanks for clearing that up- so the H-bot gets its name from the H-shape of the belts I guess. Yeah I was wondering if popular firmware supports the dual motor/H-bot design- great to hear that Marline, and Repetier do at least. So I'm not sure what to call just a plain one belt moves Y, one belt moves X printer. I guess I am willing to keep a more open mind to the CoreX/H-bot design. I'm totally a beginner - I've only built 1 RepRap Mendel ;(


---
**Frank “Helmi” Helmschrott** *August 11, 2015 07:15*

Yes, the H-Shape of the belt might be the reason for the naming. I'm also not sure about the naming of the more simple, classical cartesian printer designs but i also think that naming doesn't matter too much. If you're looking for a next printer to build for yourself i think it is valid to look at all these projects. Each of them should work also for beginners if you're nosy enough to try things out. All of the bigger printer projects have a community anwhere around that is willing to help when problems arise so i'd just say go for it :)


---
**Ted Huntington** *August 11, 2015 19:14*

**+Frank Helmschrott**

thanks - yes I'm sure I will go for it - currently I think I am going to try the Eustathios spider 2 to start with - and I am sure I will have many questions for this group.


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/7B62aUAgtAk) &mdash; content and formatting may not be reliable*
