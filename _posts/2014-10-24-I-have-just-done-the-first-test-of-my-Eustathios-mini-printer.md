---
layout: post
title: "I have just done the first test of my Eustathios-mini printer"
date: October 24, 2014 13:20
category: "Show and Tell"
author: Miguel Sánchez
---
I have just done the first test of my Eustathios-mini printer. I have designed the parts using OpenSCAD. Z-axis is still undefined but I might use one of the existing designs.  Sources can be get from [http://www.thingiverse.com/thing:513955](http://www.thingiverse.com/thing:513955)



I wanted a smaller printer, so some of you will find this model too small. I am using 250mm smooth rods and 240mm and 320mm extrusions.





**Miguel Sánchez**

---
---
**Eric Lien** *October 24, 2014 19:54*

Like it. I love all the variations out there. The bed is small enough that a more standard ultimaker style cantelivered z stage might be a good option.


---
**Miguel Sánchez** *October 24, 2014 20:46*

Thanks **+Eric Lien**. I am planning on using a plain aluminum square with resistors epoxied in the bottom. It has worked for me with Prusas and deltas and it gives me a fine control on the power delivered. I think I will go as you say, a cantilevered bed, like Ultimaker or TantilluTs.


---
**Øystein Krog** *October 25, 2014 08:10*

This is very interesting, do you have a github?


---
**Miguel Sánchez** *October 25, 2014 13:23*

+Øystein Krog thingiverse link is provided. source code is available there. one of this days I have to start using github :-)


---
*Imported from [Google+](https://plus.google.com/113179837473309823193/posts/PYuEd9YWHpE) &mdash; content and formatting may not be reliable*
