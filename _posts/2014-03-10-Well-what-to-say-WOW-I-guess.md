---
layout: post
title: "Well, what to say. WOW I guess"
date: March 10, 2014 05:42
category: "Show and Tell"
author: Tim Rastall
---
Well,  what to say.  WOW I guess.  Looks like belts work then.  Lead screw driven Z too. 



<b>Originally shared by Jason Smith (Birds Of Paradise FPV)</b>





**Tim Rastall**

---
---
**Kodie Goodwin** *March 10, 2014 05:52*

Thanks. I suddenly hate my printer :)


---
**Eric Lien** *March 10, 2014 06:30*

I didn't see the leadscrew.


---
**D Rob** *March 10, 2014 06:31*

It's beautiful


---
**D Rob** *March 10, 2014 06:32*

It's not installed yet but on the bed rod bracket the hold for it is there and I assume the nut trap﻿


---
**Wayne Friedt** *March 10, 2014 06:50*

Nice. Looks like maybe GT2 belts.


---
**Brian Bland** *March 10, 2014 07:29*

Hopefully he shares the files.


---
**Tim Rastall** *March 10, 2014 07:46*

**+Brian Bland** I hope so too but if he doesn't,  it will be pretty trivial to reverse engineer the xy ends and Z axis mount. 


---
**Jarred Baines** *March 10, 2014 07:48*

Jeez! That's amazing!


---
**Brian Bland** *March 10, 2014 07:50*

I'm not very good with making 3d parts yet.  Still trying to figure out what software I like the best.  I keep falling back on Sketchup because I know how to use that the best, but it really sucks creating solid meshes.  I have student license of Autocad and Inventor,  but haven't got used to them yet.﻿


---
**Tim Rastall** *March 10, 2014 08:57*

**+Ashley Webster** I tend to doubt everything till I've seen it working :) 


---
**Jarred Baines** *March 10, 2014 09:26*

Lol - another pulley order **+D Rob**?


---
**Daniel Fielding** *March 10, 2014 12:19*

I want the files as I was planning on dropping my makerslide plans due to unavailability in Australia anyway. Plus it has a leadscrew driven z. All the work I was going to attempt is already done.


---
**Jarred Baines** *March 10, 2014 13:07*

Ditto! Whose bot is this?


---
**Riley Porter (ril3y)** *March 10, 2014 20:46*

This is **+Jason Smith** s bot. He designed it all in sketch up. We plan on sharing soon. He wants to clean a few things up first. Also the really cool thing is the misumi is all machined and cut at order. Tapped and all! Its being driven with our #tinyg CNC controller. Our current board does not do 3d printing however the v9 board will very soon. We used tinyg to get the speed + constant jerk (so we do not rock the machine right off the table) that is not able on rep* based controllers. I printed all the parts on my ultimaker 1 FYI. The z leadscrews are suppose to be in today. Oh and yes it is all gt2.


---
**Tim Rastall** *March 10, 2014 22:35*

**+Riley Porter** Thanks for the Info.  I'm really pleased to see the design evolving,  this was exactly what I wanted when I released the initial files for Ingentis :D. Where did you get the GT2 10mm bore pulleys from btw? 


---
**Riley Porter (ril3y)** *March 11, 2014 03:10*

**+Daniel fielding** and all.

[https://github.com/jasonsmit4/Eustathios](https://github.com/jasonsmit4/Eustathios)



Files.  It's just a SKP file for now.  Again this is all pretty raw.  **+Jason Smith** will add BOM tomorrow.


---
**Brian Bland** *March 11, 2014 03:24*

Thanks for sharing.  Will help out a lot.﻿  Now have to wait until I get off work in the morning to look at.


---
**Tim Rastall** *March 11, 2014 04:19*

I see its called Eustathios,  nice that the Greek naming convention continues :)


---
**Tim Rastall** *March 11, 2014 08:13*

Here's the model in a more accessible .obj format. It's all meshes atm :( - If I get a chance I'll pass it through Freecad and convert it to solids.

[https://drive.google.com/file/d/0B3uiE-urF0rKdFpHcE54VTA0OGM/edit?usp=sharing](https://drive.google.com/file/d/0B3uiE-urF0rKdFpHcE54VTA0OGM/edit?usp=sharing)


---
**Jarred Baines** *March 11, 2014 12:05*

God stuff happens fast in these communities! Thanks so much **+Riley Porter**, **+Jason Smith** and **+Tim Rastall**!


---
**Mike Miller** *March 11, 2014 12:09*

Holy Crap! I have visions of loops of filament flinging through the air....and after tuning, loops of filament flying through the air, to land precisely where they're intended. 


---
**Daniel Fielding** *March 11, 2014 12:48*

Love g+ and these communities.


---
**Jason Smith (Birds Of Paradise FPV)** *March 12, 2014 00:36*

Hi all.  Thanks for the interest and the positive comments!  I've updated the github page ([https://github.com/jasonsmit4/Eustathios](https://github.com/jasonsmit4/Eustathios)) with a link to the partial BOM.  Sorry for the delayed response, I was busy working on the Z Axis until late yesterday, which is now working very well.  I'll post another video of that soon.


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/1PR1AnCBfNh) &mdash; content and formatting may not be reliable*
