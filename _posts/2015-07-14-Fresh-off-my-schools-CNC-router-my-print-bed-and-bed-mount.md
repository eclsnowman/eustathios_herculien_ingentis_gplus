---
layout: post
title: "Fresh off my school's CNC router: my print bed and bed mount"
date: July 14, 2015 15:01
category: "Show and Tell"
author: Chirag Patel
---
Fresh off my school's CNC router: my print bed and bed mount. Now I wait for the rest of the parts to arrive in the mail.



![images/8c940042dcec8519ba46f8a289f12c08.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8c940042dcec8519ba46f8a289f12c08.jpeg)
![images/3ff786c2fbc1165386f5110a43d725d6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3ff786c2fbc1165386f5110a43d725d6.jpeg)
![images/59bd82eee3e399db9879df532d5f11fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/59bd82eee3e399db9879df532d5f11fa.jpeg)

**Chirag Patel**

---
---
**Dat Chu** *July 14, 2015 15:34*

Man. That bed mount looks sweet. 


---
**Chirag Patel** *July 14, 2015 15:40*

**+Dat Chu** Thanks! I probably could have taken out some more material from that inside cut to save weight but hey it still looks good.


---
**Gus Montoya** *July 14, 2015 23:35*

Nice cuts, what school do you go to? Just in case I need something else cut LOL  J/king


---
**Chirag Patel** *July 15, 2015 00:07*

**+Gus Montoya** Haha it's actually my High School FIRST Robotics team's CNC. Now that I'm an alum of the team, I still get free access to it! I just need to supply the material. 


---
*Imported from [Google+](https://plus.google.com/101451310698405542910/posts/H6UQcfFrNBH) &mdash; content and formatting may not be reliable*
