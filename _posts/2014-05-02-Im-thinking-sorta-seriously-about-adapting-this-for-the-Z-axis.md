---
layout: post
title: "I'm thinking sorta seriously about adapting this for the Z axis"
date: May 02, 2014 21:25
category: "Discussion"
author: Dale Dunn
---
I'm thinking sorta seriously about adapting this for the Z axis. It's probably cheaper than the Ø10mm bearing rods, and maybe easier to deal with (aluminum can be cut to length much more easily). With custom long carriages, it might even be more rigid.





**Dale Dunn**

---
---
**ThantiK** *May 03, 2014 03:27*

Interested in trying one of these out myself.  I tried one with ball bearings once, and it was awesome but I'm not sure how accurate they can be.


---
**Dale Dunn** *May 03, 2014 14:37*

Accuracy is a good question. That requires some thought.


---
**D Rob** *May 03, 2014 20:28*

If a v- shaped rail were added on each end where the slot meets the carriage and a pre-load spring system were added I think prints would be slightly less critical and the guide would keep a snug fit as it wears due to the springs also 4x 643's could be used in the side slots to stabilize it further (depends on size of extrusion slots). These also could use springs .


---
**Dale Dunn** *May 03, 2014 21:08*

Speaking of wearing in, I wonder if an ABS carriage would get dimpled after a period of inactivity. I sometimes go several weeks between prints, and I have a couple stressed parts on my printer that have relaxed from a preloaded condition. Maybe this isn't a good idea after all.


---
**D Rob** *May 03, 2014 23:41*

**+Dale Dunn**  what about nylon? 645 or bridge...


---
**Dale Dunn** *May 04, 2014 04:02*

Polycarbonate maybe, but I'm not set up to handle it. The new machine will have the capability. Nylon... I just don't know.


---
**D Rob** *May 04, 2014 04:15*

with nylon you could design it to exploit the flexibility and use itself for preload


---
**Dale Dunn** *May 04, 2014 04:17*

Heh. I've done that with ABS (just not for a bearing race). That's how I know it creeps.


---
**D Rob** *May 04, 2014 04:29*

hmmm. from all of the taulman destruction vids Ive seen. I'm reasonably sure nylon will be better than abs.


---
**Dale Dunn** *May 04, 2014 18:47*

Did any of the destruction vids take months? That's the time frame that's concerning me.



OK, I found some data for Nylon creep: [http://www.toray.jp/plastics/en/products/amilan/technical/tec_001.html](http://www.toray.jp/plastics/en/products/amilan/technical/tec_001.html) It looks like it stabilizes after 100 hours in the test, a few percent deformation. So, after a week of inactivity, there may be some notchiness in the bearing, depending on how much preload was applied. Very little preload should be necessary, so it may work out alright.


---
**Jarred Baines** *May 09, 2014 15:00*

Hips?

Prints like abs but more resilient in my (limited) experience...



Cheap too... great material... going to print a lot of my ingentis bits in hips...



Don't know bout long-term tho...


---
*Imported from [Google+](https://plus.google.com/107543004166274026989/posts/PZoULAyHR6o) &mdash; content and formatting may not be reliable*
