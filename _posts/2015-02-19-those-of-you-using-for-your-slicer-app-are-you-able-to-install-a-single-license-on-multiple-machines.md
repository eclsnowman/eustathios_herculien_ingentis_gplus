---
layout: post
title: "those of you using for your slicer/app, are you able to install a single license on multiple machines?"
date: February 19, 2015 02:21
category: "Discussion"
author: Seth Messer
---
those of you using  #simplify3d  for your slicer/app, are you able to install a single license on multiple machines? i checked their FAQ and also reached out to them (haven't heard back) to find out before I pull the trigger. thanks!





**Seth Messer**

---
---
**Daniel Fielding** *February 19, 2015 02:22*

Yes i have been using it on 2 machines inside my local network.


---
**Seth Messer** *February 19, 2015 02:23*

much thanks **+Daniel fielding** 


---
**Wayne Friedt** *February 19, 2015 02:26*

Ya you get two.


---
**Seth Messer** *February 19, 2015 02:32*

purchased. thanks again fellas.


---
**Eric Lien** *February 19, 2015 03:48*

Yes. And if you need to switch to a third there is a system to request moving the license.


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/JaR2hoN3EMP) &mdash; content and formatting may not be reliable*
