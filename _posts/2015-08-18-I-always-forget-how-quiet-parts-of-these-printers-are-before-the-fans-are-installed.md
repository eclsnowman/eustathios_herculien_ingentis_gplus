---
layout: post
title: "I always forget how quiet parts of these printers are before the fans are installed"
date: August 18, 2015 03:36
category: "Show and Tell"
author: Mike Miller
---
I always forget how quiet parts of these printers are before the fans are installed.  #Ingentilere  

![images/dfbad9776694f8e1daca1d4a798ff187.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dfbad9776694f8e1daca1d4a798ff187.gif)



**Mike Miller**

---
---
**ThantiK** *August 18, 2015 04:25*

Yeah, get the SilentStepStick from **+Watterott electronic**, you'll be even more amazed at how quiet they <i>can</i> get.


---
**Øystein Krog** *August 18, 2015 08:19*

TMC2100 + Igus bushings == bliss:P


---
**Mike Miller** *August 18, 2015 11:37*

No Igus in this one...Oil impregnated Bronze. 


---
**Øystein Krog** *August 18, 2015 11:39*

I used bronze w/graphite for a while and it was much better than ball bearings, but IGUS is even more quiet, especially at high speeds.

Your setup sounds very quiet though!

I don't recommend bronze w/graphite, the graphite means everything becomes a bit dirty:P


---
**Mike Miller** *August 18, 2015 12:09*

I've got a machineshop in the garage...which means I have everything from super-heavy Way oil for the lathe to really light machine oil...which I'll be using on the bushings...I'll be sticking with the tried and true Teflon superlube for the leadscrew. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/bR6fx3QnSrU) &mdash; content and formatting may not be reliable*
