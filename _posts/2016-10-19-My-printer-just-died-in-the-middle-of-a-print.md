---
layout: post
title: "My printer just died in the middle of a print"
date: October 19, 2016 04:57
category: "Show and Tell"
author: Gústav K Gústavsson
---
My printer just died in the middle of a print. But I found the problem ;-)



![images/fea6fd8dfef4c63125f366c7581ded2e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fea6fd8dfef4c63125f366c7581ded2e.jpeg)
![images/d76334a0f14b5b4352c51496378386b3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d76334a0f14b5b4352c51496378386b3.jpeg)
![images/fef1f97091152137763bab48835f97e1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fef1f97091152137763bab48835f97e1.jpeg)

**Gústav K Gústavsson**

---
---
**Daniel Kruger** *October 19, 2016 05:03*

Ohhh.... ouch


---
**Oliver Seiler** *October 19, 2016 05:09*

Sounds familiar! That light on the switch must be so tempting...


---
**ekaggrat singh kalsi** *October 19, 2016 05:43*

been there a few times till i decided to put everything in a locked switch box :))


---
**Nicolas Arias** *October 19, 2016 10:35*

Oh yeah,  been there


---
**Norr Norr** *March 07, 2017 17:21*

Ohhh....ouch


---
*Imported from [Google+](https://plus.google.com/+GústavKGústavsson/posts/Y6Eth2fCGY4) &mdash; content and formatting may not be reliable*
