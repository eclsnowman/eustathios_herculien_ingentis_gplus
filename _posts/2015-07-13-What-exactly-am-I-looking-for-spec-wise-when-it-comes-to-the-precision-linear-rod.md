---
layout: post
title: "What exactly am I looking for, spec-wise, when it comes to the precision linear rod?"
date: July 13, 2015 19:56
category: "Discussion"
author: Bryan Weaver
---
What exactly am I looking for, spec-wise, when it comes to the precision linear rod?  I've already ordered 8mm precision chrome rod from Folgertech.com, but they do not have 10mm chrome rod.  What they do have is "10mm smooth Drill Rod - O1 Grade".  How do either of these compare to the 52100 steel rod from Misumi that is specified in the BOM?  Will those suffice, or am I safer  just biting the bullet and spending a little more to get the rods from Misumi?  The 10mm rod from SMW3D appears to be O1 grade also, so I assume O1 steel rod is sufficient to use?  They have been sold out for a couple weeks, however..



There's just a lot of different options out there and it can be a little daunting..





**Bryan Weaver**

---
---
**Chris Brent** *July 13, 2015 20:06*

I think drill rod is unhardened tool steel. You'd have to temper is yourself to make it as hard as a precision rod. I'm also not sure that it's diameter and straightness is very precise as it's usually used to make other tools (like drills :) so it's turn down and ground from the original diameter. 


---
**Eric Lien** *July 13, 2015 21:54*

**+Bryan Weaver**​ look at the hardness and dim specs of the rods in the BOM from the Misumi website. If you can find comparable rods for cheaper with the same specs, then you should be OK. If they are lower quality you mileage may vary.



If I recall **+Bruce Lunde**​ ran into issues with discount rods.


---
**Bruce Lunde** *July 13, 2015 22:03*

**+Bryan Weaver**​ -as **+Eric Lien**​​ said, I went discount on hardened drill rod and wasted the money. I then got the Misumi rod and  was back in business.﻿


---
**Bryan Weaver** *July 13, 2015 22:06*

Good to know.  I was very close to ordering drill rod, then got to reading some negative reviews on amazon about tolerances not being up to snuff.  I guess I'll play it safe and go with Misumi.  


---
**Zane Baird** *July 13, 2015 23:22*

Another heads up, I originally ordered the rods for my Herculien from smw3d. They came and were 10.00-10.01mm OD (their specs listed were 10.00 +/-0.013 so they weren't lying). However, they would not fit in the 10mm ID bearings I ordered from [fasteddybearings.com](http://fasteddybearings.com). Even tried sanding the ends of the rods down with some fine-grained sand paper with no luck. They slid right on the Misumi rods and (other than one that I'm very doubtful of) the rods were straight as could be. The extra $ are worth it from my experience



Just waiting on a couple closed-loop belts and it should be ready for it's first moves... And another hitch, the machine shop I have access too won't let me use the CNC router with MDF. Have to spend some extra dollars for some 11 ply plywood after buying a 2'x4' MDF sheet. Good thing the MDF was cheap.


---
**Bryan Weaver** *July 14, 2015 00:03*

I just ordered from misumi, 8mm rods also. Not going to chance it.


---
**Øystein Krog** *July 14, 2015 09:24*

There are different fit tolerances for linear shafts. There are iso standards. You need to ensure that the linear shafts tolerance type is the same as the bushings. I think most of the time you want g6 shafts and G6 bushings, this means the shafts should be a max of X mm (e.g max 10mm), basically slightly undersize. h6 shafts would be slightly oversize.



[http://www.amesweb.info/FitTolerance/FitTolerance.aspx](http://www.amesweb.info/FitTolerance/FitTolerance.aspx)


---
**Øystein Krog** *July 14, 2015 09:48*

In addition to fit tolerance the hardness of rods matter a lot. When using ball bearings you need them to be super hard, Rc60 is good but 58 is ok too. For metal bushing I think hardness requirements are slightly lowered, and plastic bushings do not require much hardness at all.


---
**Øystein Krog** *July 14, 2015 09:57*

Misumi is very good as all these things are clearly visible in the web interface. When ordering from e.g aliexpress one must be a lot more careful, I recently ordered some 8mm shafts and received mixed g6 and h6, which meant that only some fit my bushings. With ball bearings both types "worked" but the h6 types were clearly much tighter.  When I spoke to the seller he would not understand the issue, from his perspective all his shafts worked well with ball bearings.

In the end I took it as a lesson, always make sure to specify the fit tolerance:) 


---
*Imported from [Google+](https://plus.google.com/111820797809026464429/posts/YhgSFG5wxne) &mdash; content and formatting may not be reliable*
