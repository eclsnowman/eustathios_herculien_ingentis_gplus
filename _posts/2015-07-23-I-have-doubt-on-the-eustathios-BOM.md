---
layout: post
title: "I have doubt on the eustathios BOM.."
date: July 23, 2015 14:32
category: "Discussion"
author: Godwin Bangera
---
I have doubt on the eustathios BOM.. there is this item from SMW3D or openbuilds called Precision Ball Bearing ... What are the specs of these bearings?

And find these bronze bushings 8 & 10 mm (a_7z41mpsb08m & a_7z41mpsb10m) is damn difficult here locally ... is there an alternative to this or some 3d printed or modded, some makeshift solution ???? i have ordered this from ultibots but its gonna take pretty long till i get it





**Godwin Bangera**

---
---
**Bryan Weaver** *July 23, 2015 14:53*

Just buy this Idler Pulley kit.  It includes the pulley, bearings and precision washer that you will need.



[http://www.smw3d.com/idler-pulley-kit/](http://www.smw3d.com/idler-pulley-kit/)



I had a tough time tracking down the bronze bushings also, finally ended up ordering them from Ultibots..


---
**Godwin Bangera** *July 23, 2015 15:01*

The Precision Ball Bearing = the idler pulley kit



and no replacement for the bronze bushing right ?


---
**Bryan Weaver** *July 23, 2015 15:13*

On the Eustathios Spider V2 BOM, under 10.12.11 Idler Wheel Assembly, there are 6 parts listed to build the assembly; V-slot pulley, precision ball bearing (x2), 5mm precision washer, Nylock nut, M5 x 35mm cap screw, and .125 inch aluminum spacer.  The idler pulley kit from SMW3D includes the pulley, 2x bearings, 5mm precision washer, and nylock nut.  It also comes with a cap screw and nylon spacer which you will not need.  You will have to get the 35mm M5 screw and aluminum spacer separately to build the pulley assembly.



I know a couple of guys here have tried the self lubricating bronze bushings from robotdigg instead of the specified sintered bushings, but I think they are running into alignment issues.  Maybe they can chime in here.


---
**Godwin Bangera** *July 23, 2015 15:16*

sorry my bad ... i got :)


---
**Godwin Bangera** *July 23, 2015 16:27*

was wondering if the Igus bearings will match those sinitered press bearings



igubal® pressfit bearing KGLM, mm

or 

igubal® pressfit bearing EGLM, mm



any suggestions ??


---
**Frank “Helmi” Helmschrott** *July 23, 2015 16:31*

No, they won't match. But feel free to modify the parts to make them fit if you plan to use them. I'm not sure how they will work out as they will probably have a bit more play then the bronze ones. Would be great having someone to test them. I'd be highly interested in building an Eusthatios with IGUS bushings and aluminum rods.


---
**Daniel F** *July 23, 2015 19:28*

You can also use the idler from robotdigg, it worked fine for me. The bearing has a smaller diameter, therefore I had to change the hole for the fixing screw from 5 to 4mm (and use a 4mm screw of coarse).﻿


---
**Eric Lien** *July 23, 2015 19:29*

I would avoid the IGUS or other composite bearings. They have a higher coeficient of static friction to overcome and hence can fall pray to the slip/stick issue on direction changes. I have had a fair amount of headaches with the composite bearings when I had a corexy printer. Broze on steel has a much lower friction barrier to overcome.



Here are some more suppliers for the self aligning bronze bushings:



[https://www.lulzbot.com/products/10mm-bronze-bushing-4-pack](https://www.lulzbot.com/products/10mm-bronze-bushing-4-pack)

[https://www.lulzbot.com/products/8mm-bronze-bushing-pack-4](https://www.lulzbot.com/products/8mm-bronze-bushing-pack-4)



[http://www.sdp-si.com/eStore/Catalog](http://www.sdp-si.com/eStore/Catalog)

[http://sdp-si.com/ss/pdf/80505055.pdf](http://sdp-si.com/ss/pdf/80505055.pdf)



[https://www.qbcbearings.com/BuyRFQ/PressB_Sintered_CSR_M.htm](https://www.qbcbearings.com/BuyRFQ/PressB_Sintered_CSR_M.htm)



Hope that helps. There are likely more.


---
*Imported from [Google+](https://plus.google.com/102290637403942484150/posts/BEm27rnLHbt) &mdash; content and formatting may not be reliable*
