---
layout: post
title: "Anyone figured out what options at Misumi to pick for the extrusion to be drilled appropriately for Tim's method for frame assembly?"
date: March 11, 2014 18:38
category: "Discussion"
author: Ben Delarre
---
Anyone figured out what options at Misumi to pick for the extrusion to be drilled appropriately for Tim's method for frame assembly?





**Ben Delarre**

---
---
**Eric Moy** *March 12, 2014 01:49*

In order to understand the options, you need to view the catalog pages.  I'm lucky enough to have an older hard copy laying around.  I just got the preconfigured box, using the corner brackets, which require no drilling, tapping, or self-tapping screws (which are a PITA), and is also decently stronger than blind connections.  IMHO, they're easier to install as well.


---
**Jim Squirrel** *March 12, 2014 02:12*

I just had them drill and tap the ends. but forgot to have those options for the zstage


---
**Brian Bland** *March 12, 2014 06:06*

Here is what you need.  These match the dimensions on the Ingentis Blog.  Should get you all the holes drilled and the ends tapped.



4 ea of : HFS5-2020-465-TPW-LCP-RCP-AH95 (vertical legs)

10 ea of : HFS5-2020-425-TPW-LCP-RCP



TPW is both ends tapped.

LCP is left end cross drilled.

RCP is right end cross drilled.

AH95 is a hole drilled 95 mm from one end.﻿



I drilled and tapped my own, as it saves a lot of money.﻿



The 465 length rails it adds ~$10 to each extrusion.

The 425 length rails it adds ~$8 to each extrusion.


---
**ThantiK** *March 13, 2014 17:52*

**+Tim Rastall**, may want to possibly add these to the BOM for people who don't have access to the machinery required to drill and tap their own.  I would have gladly skipped a few parts and spent the money if I could have had this all pre-done for me.


---
**Ben Delarre** *March 13, 2014 17:55*

**+Brian Bland** Thanks for that, very helpful. I honestly can't trust myself with a drill to get the holes perfect so I'll either get these pre-cut and drilled, or resort to the L bracket assembly method instead. Both are a little more pricey than doing it yourself, but there's a reason I want computer controlled machining and printing - my hand-eye coordination sucks! ;-)


---
**Jason Smith (Birds Of Paradise FPV)** *March 13, 2014 23:09*

You can also find the Eustathios version of these parts in the BOM:

[https://github.com/jasonsmit4/Eustathios](https://github.com/jasonsmit4/Eustathios)

[https://docs.google.com/spreadsheet/ccc?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing](https://docs.google.com/spreadsheet/ccc?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing)


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/2rTStgVpYPP) &mdash; content and formatting may not be reliable*
