---
layout: post
title: "At 200% for break in code motors at 1.15 current and not a single grinding sound...drop down to less then 40mm a sec death comes walking in"
date: May 25, 2015 18:12
category: "Show and Tell"
author: Derek Schuetz
---
At 200% for break in code motors at 1.15 current and not a single grinding sound...drop down to less then 40mm a sec death comes walking in

![images/9cf1b998f2c0cee8915afe3656b49a12.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9cf1b998f2c0cee8915afe3656b49a12.gif)



**Derek Schuetz**

---
---
**Derek Schuetz** *May 25, 2015 18:17*

Up to 250% still smooth


---
**Gus Montoya** *May 25, 2015 18:42*

i hope you find an answer soon....i would like to know what is the cause of it.


---
**Eric Lien** *May 25, 2015 19:18*

Could you get video of the low speed issues? Also if you twist on the carriage is there any play? Or on any of the rod ends? Are all the pulleys up tight against the bearings on the x/y rods? Also, How is your Z travel working? ﻿


---
**Derek Schuetz** *May 25, 2015 19:32*

Z travel was flawless up until one point where really quick small z axis changes did not move  but once I put it to 1/8 step that was solved. I'll get a video of low speed issues give me a sec


---
**Bruce Lunde** *May 25, 2015 20:06*

  Does it get up to speed roughly or smoothly?  I know that from the video it is good at fast speed, but how it is doing at initial acceleration from a stop getting to the fast speeds?


---
**Eric Lien** *May 25, 2015 20:06*

**+Derek Schuetz** I run my Z at 1/8 also.


---
**Derek Schuetz** *May 27, 2015 16:08*

Went back to 1/32 on my z turned out to be a Acura issue works flawlessly with slic3r which I can't figure out


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/MYfRyx7US7i) &mdash; content and formatting may not be reliable*
