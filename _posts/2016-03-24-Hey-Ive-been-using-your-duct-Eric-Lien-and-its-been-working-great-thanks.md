---
layout: post
title: "Hey, I've been using your duct Eric Lien and its been working great thanks!"
date: March 24, 2016 13:56
category: "Discussion"
author: Nicholas Brown
---
Hey, I've been using your duct **+Eric Lien** and its been working great thanks!  It's been really good at overhangs, however I was having trouble keeping the extruder up to temperature.  It seemed that too much air was hitting the heater block and cooling it down quite a lot.  My solution was to use about 5 strips of 1cm wide kapton tape stuck to the inside of the duct across the opening, then with a knife cutting a hole for the nozzle, then attached it to the print carriage.  This directs the air towards the nozzle and insulates the heater block from the direct airflow of the duct.  (I've tried to draw this on the attached pic as best as I can to try and help explain) This has improved the ability of the extruder to get up to temperature quickly, but it was quite fiddly, and it took a long time to do.  One solution could be to have the bottom of the cooling duct hinged so you could open it up, apply the kapton tape, then close it up again, I tried doing this but my 3d skill are quite poor (sorry).  With this hinge in place it might be possible to also add glass fibre insulation (or some other insulation) around the heater block as well to help minimize the 3 way battle between the heater block, the cooling duct and the heat sink on the extruder.



Are there any better solutions?  I was looking at some of **+Walter Hsiao** cooling ducts which also look good, anyone had any luck with them?

![images/c17752d23358726605c9725e14dd0d17.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c17752d23358726605c9725e14dd0d17.png)



**Nicholas Brown**

---
---
**Eric Lien** *March 24, 2016 14:41*

I've seen people have good luck wrapping the heater block itself in strips of wool padding and then wrapping that in Kapton. This lowers the affected surface area of the block so it doesn't cool the surface of the aluminum. I've also seen people do similar things with high temperature silicone wrap around the heater block with good results.


---
**Tomek Brzezinski** *March 24, 2016 15:26*

I like this Kapton solution especially if you were to add a layer of ceramic wool to seperate the heater block from the kapton (thus allowing a higher heater temp than the 260C limit of the kapton since the kapton will be mostly cooled on one side.  



I found wrapping the heater block with wool to be quite finicky for me, especially with a highly ducted fan assembly like the one I use on my solidoodle and this one for the herculien.   



The wrapping would just like to unravel itself and get in the way of wires. This seems sufficient but simpler. 


---
*Imported from [Google+](https://plus.google.com/106875376729457897724/posts/DuWDEYAftoM) &mdash; content and formatting may not be reliable*
