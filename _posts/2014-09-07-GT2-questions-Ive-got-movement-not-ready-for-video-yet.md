---
layout: post
title: "GT2 questions I've got movement (not ready for video yet)"
date: September 07, 2014 13:40
category: "Discussion"
author: Mike Miller
---
GT2 questions



I've got movement (not ready for video yet). Which is good. I also have some belt skipping, which is bad. Those of you with long belt runs from the top of the printer to the bottom, did you need an idler assembly, or reduction in acceleration, or jerk or something to prevent belt skipping?



Z and extruder aren't done yet, but watching the sled move, there's some points in a print where I swear the plastic would be 'scribbleing all over it self', meaning one axis is moving back and forth, covering it's tracks, and the other axis isn't moving. It's not skipping, it's not overheating (I think I've got that licked...or maybe not.)



Could it be a math issue where one axis' movement is so small, it doesn't end up with physical moviement? (Physical specs: 1.8 degree motors (200 steps per), 20 tooth drive gear, 40 tooth receiving gear, e-steps pretty close at 160, 1/16 microstepping)





**Mike Miller**

---
---
**Erik Scott** *September 07, 2014 14:09*

Use the prusa calculator to get your steps/mm. I don't use idlers on my belt runs; I maintain tension by adjusting the x and y motor mount positions. I've never had any issues with belt skips. Make sure your numbers are correct. 


---
**Mike Miller** *September 07, 2014 14:22*

I'm pretty sure the e-steps are nailed down. 10mm requested is 10mm in the real world. I'm hearing a noise on sudden movements and I can't tell if it's just normal movement or the belt slipping. 



I could see printing something like the PrintrSimple Y-axis that positively traps the belt around the motor pulley:

[http://www.powerprint.es/blog/wp-content/uploads/2014/03/slides-jr-belt.png](http://www.powerprint.es/blog/wp-content/uploads/2014/03/slides-jr-belt.png)


---
**Eric Lien** *September 07, 2014 14:40*

**+Mike Miller** I had this at first too. Make sure movement is effortless on the gantry (no bind at all, perfectly aligned rods, this bot is very sensitive to that). Then generate a break in gcode. Corner to corner, and around the box. Set the limits a good 20-40 mm in from the limits in case of miss steps and eventual crash. And let her break in. I ran mine for 4 hrs before things really began to glide and I could turn up the speed.﻿


---
**Mike Miller** *September 07, 2014 14:54*

Things are pretty well in alignment now. Surprisingly, I <i>did</i> have to dust the gantry rods this morning as it was...juddering? It's a behavior that's hard to explain...but it wasn't right. A quick wipe with a microfiber cloth and it was okay after that. 



I dunno if I'll see the same behavior **+Shauki Bagdadi** saw with the IGUS until I get the nozzle extriding plastic. It appears pretty good, but appearances can be deceiving. (and I wouldn't have this thread if it was perfect. )


---
**Eric Lien** *September 07, 2014 15:13*

**+Mike Miller** if IGUS is any thing like pbc linear frelon, then the slip/stick phenomenon might be an issue. My corexy was plagued by this. Any misalignment or torque cause little micro binding and dimensions would be off. Circles are the true test I found for this issue. Oblong circle are what to look for.


---
**Mike Miller** *September 07, 2014 15:28*

We'll, it's goot data for **+igus Inc.** if that turns out to be the case. I think the interference fit they're expecting is a big part of the problem. The answer being: Do we engineer a solution, or jump ship to Linear Bearings/bushings. I'm not ready to do that yet as there's still good research to be done. 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/bQqdpF5Pg2b) &mdash; content and formatting may not be reliable*
