---
layout: post
title: "Sleeve for electrical wiring designed and printed a custom sleve to join electrical tubes with different diameters"
date: May 24, 2016 11:28
category: "Show and Tell"
author: Daniel F
---
Sleeve for electrical wiring

designed and printed a custom sleve to join electrical tubes with different diameters. This was necessary to move an electrical tube in the house that was sticking out when we tore down a wall.



![images/4b3568e6733f2c7cee7f62fd3e564971.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4b3568e6733f2c7cee7f62fd3e564971.jpeg)
![images/ec4bab5f33419e6808a76d1c759dcea6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ec4bab5f33419e6808a76d1c759dcea6.jpeg)
![images/917703c733f49740101e0b581d498250.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/917703c733f49740101e0b581d498250.jpeg)
![images/5ec2a1379db61cfd29bd62c6ece9d8d3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5ec2a1379db61cfd29bd62c6ece9d8d3.jpeg)

**Daniel F**

---
---
**Sean B** *May 24, 2016 12:23*

Awesome simple, functional print.  I much prefer this to benchys to be completely honest.


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/7VqSSRuK9vD) &mdash; content and formatting may not be reliable*
