---
layout: post
title: "Kinda like christmas today :) What also arrived today was my Titan Aero from E3D Online"
date: April 08, 2017 16:26
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
Kinda like christmas today :) What also arrived today was my Titan Aero from E3D Online. The motors from OMC-Steppersonline already came a few days ago. The one I ordered ([http://www.omc-stepperonline.com/nema-17-bipolar-step-motor-29v-07a-18ncm255ozin-17hs100704s-p-260.html](http://www.omc-stepperonline.com/nema-17-bipolar-step-motor-29v-07a-18ncm255ozin-17hs100704s-p-260.html)) actually only weigs in at 145g and not 180g as advertised. The Titan Aero comes at around 255g which makes this a 300g direct extruder. 



Hope I can find the time to design a carriage and test this out.

![images/c2cfd859bdf58bf49a228585f31bb89c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c2cfd859bdf58bf49a228585f31bb89c.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 16:39*

I want to see this. 


---
**Eric Lien** *April 08, 2017 18:06*

I have this carriage for the BMG. The duct could be modified for the Titan.

![images/23ac33c35d914aeadcce2d95b0b52d20.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/23ac33c35d914aeadcce2d95b0b52d20.jpeg)


---
**Eric Lien** *April 08, 2017 18:06*

![images/5ae0dd51ca6fec54e6e96feebea84ddf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5ae0dd51ca6fec54e6e96feebea84ddf.jpeg)


---
**Eric Lien** *April 08, 2017 18:06*

![images/eb19d47eb5249eb7131153d829670bbb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eb19d47eb5249eb7131153d829670bbb.jpeg)


---
**Eric Lien** *April 08, 2017 18:07*

![images/e487d8d5ebde8ec32ce46892d4e95d03.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e487d8d5ebde8ec32ce46892d4e95d03.jpeg)


---
**Frank “Helmi” Helmschrott** *April 08, 2017 18:09*

oh, very nice. Have you tried that setup yet? Would be interested to know how much it weighs with the E3D v6 under there.



I was thinking about the same mounting idea as there's no other mounting option on the Titan Aero. Though I will go more before the rod to be able to go deeper as the hotend is so short. Is that a 50mm blower? That looks nice. that way. 




---
**Eric Lien** *April 08, 2017 18:21*

Yeah the Aero is problematic because of the short hotend. You would have to mount under the rods which kind of defeats the advantages of the Aero because you loose Z height and put the center of mass further from the bushings leading to higher deflection.



I haven't run it yet. I designed and printed it. But since I have other direct drive printers I chose to stay with Bowden for better print quality at high speeds.


---
**Eric Lien** *April 08, 2017 19:26*

Here is a weight comparison when I was comparing Titan vs BMG.

![images/39eadab74ca8df7f8623d508450c0d87.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/39eadab74ca8df7f8623d508450c0d87.png)


---
**Eric Lien** *April 08, 2017 19:26*

![images/0567f7f6f9467a3f3bfc4be046fea727.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0567f7f6f9467a3f3bfc4be046fea727.png)


---
**Eric Lien** *April 08, 2017 19:26*

![images/0fe24a02dae7bf8409b39e78b572f6a0.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0fe24a02dae7bf8409b39e78b572f6a0.png)


---
**Frank “Helmi” Helmschrott** *April 08, 2017 19:28*

ok, should probably come out at 300g at the end too with hotend and cables , I guess. Good comparision. Might still have to see if the aero works well on the Eusthatios. As you said it might not work well with the short hotend but definitely tempting to try.




---
**Eric Lien** *April 08, 2017 20:14*

Certainly worth a try. I am excited to see how it turns out. Also I will get the carriage up on the GitHub once I get back to my computer.


---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 20:55*

Thoughts about a sideways mounting configuration? Shown is Titan with pancake on my steel i3. ![images/8ae2803a9041f9a9cb97ad9e8187dba9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8ae2803a9041f9a9cb97ad9e8187dba9.jpeg)


---
**Eric Lien** *April 08, 2017 22:35*

With a Cross-rod Gantry there is no sideways :)


---
**Ray Kholodovsky (Cohesion3D)** *April 08, 2017 22:42*

Good point. So in that case I mean moving the assembly forward such that the pancake motor is in front of the rod instead of over it. Then you can move everything down. 


---
**Eric Lien** *April 08, 2017 23:07*

Yeah that works. The hard part is ideally you want the center of mass of the extruder or hotend between the bushings on Cross Rod for that axis. This means widening the carriage because you are limited at starting in the corner of the rod crossing since the cross rod can't go through the motor. 



This leaves two options for an extruder like the Aero with a short hotend. Mount it below the cross rods (you can maybe tuck it in a little under the upper cross rod). Or secondly in a corner between the rods crossing which results in a wide center carriage eating up XY travel.


---
**Dale Dunn** *April 09, 2017 01:05*

Can the Aero be altered to use a belt drive instead of gear reduction? That might allow you to move the motor to balance the assembly on the cross-rods, while maintaining the short drive gear-to-nozzle length of the Aero.


---
**Frank “Helmi” Helmschrott** *April 09, 2017 05:10*

**+Dale Dunn** this sounds crazy, but I guess not. 


---
**Rainer Weissenberger** *April 09, 2017 08:10*

**+Ray Kholodovsky** 

 would you share your lovely design for the titan on the i3?


---
**Ray Kholodovsky (Cohesion3D)** *April 09, 2017 13:37*

**+Rainer Weissenberger** not my design, google for Titan Prusa i3, you will find E3D wiki article and corresponding thingiverse link. I think Gyrobot designed that one. But it is a different fan shroud than the one in that thingiverse link. And as of now it cools the nozzle more than the print so not in use. 


---
**Jeff DeMaagd** *April 09, 2017 16:10*

**+Dale Dunn** I think you'd be better off designing the extruder from scratch.


---
**Frank “Helmi” Helmschrott** *April 14, 2017 07:55*

**+Eric Lien** did you find the time yet to upload your carriage anywhere? The source files? I think I'll give that a try as I'll probably be using the standard Titan. Using the aero would mean too much of a modification to the printer as the bed needed to go higher then. 


---
**Eric Lien** *April 14, 2017 12:22*

**+Frank Helmschrott** I haven't yet. I will work on getting it up on the GitHub tonight.


---
**Frank “Helmi” Helmschrott** *April 14, 2017 12:44*

Thanks **+Eric Lien** – but don't rush. I will only partly have time to worry about that stuff so no need to hurry.


---
**Eric Lien** *April 14, 2017 14:34*

Here is a google drive link until I can get more organized.



[drive.google.com - Eustathios BMG Carriage - Google Drive](https://drive.google.com/drive/folders/0B1rU7sHY9d8qRXRHTHRmWFF2clk?usp=sharing)


---
**Lopez Tom** *April 30, 2017 12:49*

Someone have made a fan duct for the E3D titan ?


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/D4CsgAMN8ZV) &mdash; content and formatting may not be reliable*
