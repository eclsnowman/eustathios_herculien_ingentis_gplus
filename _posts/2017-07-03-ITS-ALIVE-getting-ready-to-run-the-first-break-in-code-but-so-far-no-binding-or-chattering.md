---
layout: post
title: "IT'S ALIVE!!!! getting ready to run the first break in code, but so far no binding or chattering!"
date: July 03, 2017 16:14
category: "Build Logs"
author: James Ochs
---
IT'S ALIVE!!!!



getting ready to run the first break in code, but so far no binding or chattering!  I do have a rattly side panel though 😀


**Video content missing for image https://lh3.googleusercontent.com/-ZHNoL-2Ow8Y/WVptShAbpQI/AAAAAAAAybc/xI__n2m3IhUU4oiApOdtwH1im4sc8kCWwCJoC/s0/VID_20170703_120711.mp4**
![images/66b4aec8f89b5bf9a1797bbb4e6b5b28.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/66b4aec8f89b5bf9a1797bbb4e6b5b28.jpeg)



**James Ochs**

---
---
**Eric Lien** *July 03, 2017 22:13*

I am always so excited to see first moves on a new build.



Looking great. You can probably do some tuning to the homing speeds. 



Can't wait to see the first print.


---
**Brad Vaughan** *July 04, 2017 13:01*

Congrats!  So satisfying, isn't it?


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/Vr1tR833yHy) &mdash; content and formatting may not be reliable*
