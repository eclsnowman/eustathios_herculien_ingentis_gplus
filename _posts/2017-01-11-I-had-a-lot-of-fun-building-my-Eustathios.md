---
layout: post
title: "I had a lot of fun building my Eustathios!"
date: January 11, 2017 19:43
category: "Mods and User Customizations"
author: Torsten Endres
---
I had a lot of fun building my Eustathios! Thanks a lot **+Eric Lien** for publishing the files and creating that community here. To give some feedback, I like to show some pictures and a video of my modded printer. Most of my modifications were motivated to reduce cost or/and to reuse stuff. Just my final carriage is a modification worth mentioning:



My primary requirements for that printer was a good dual printing capability for PVA support. Initially, i tried out the chimera and cyclops nozzle, but the results were frustrating since you have to spend more time for oozing precautions than for real printing and even then my primary build material (most time PETG) gets brittle by PVA contamination. First of all, i consider a dual x carriage modification (like e.g.  BCN3D Sigma), but this is hard to realize for a crossing rods xy-design.  Finally, i found that dual rocking extruder for the Prusa i3:

[http://www.thingiverse.com/thing:673816](http://www.thingiverse.com/thing:673816)



This is a really smart design, which I had to try out....

I have not printed much yet, but nevertheless the results are already much better than with the classical approaches. If somebody wants to try that extruder too, i can see how to submit my carriage into the repository.




**Video content missing for image https://lh3.googleusercontent.com/-KQikRIHQhQw/WHaK57_oyTI/AAAAAAAAOA0/NDIUcGDw_MQPjVkdVx33NkyNLyD_GciuwCJoC/s0/2017-01-07%252B13.04.04.mp4**
![images/c9bacbd1d0de41f606dbec2857e73efa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c9bacbd1d0de41f606dbec2857e73efa.jpeg)
![images/35a2c4c5bc7061d2d5d81bd474cc187d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/35a2c4c5bc7061d2d5d81bd474cc187d.jpeg)
![images/a044a9916df8a9bfb94ffebb3e60c187.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a044a9916df8a9bfb94ffebb3e60c187.jpeg)
![images/5f1d59ae811befa30def1b316fea27a8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5f1d59ae811befa30def1b316fea27a8.jpeg)

**Torsten Endres**

---
---
**Eric Lien** *January 11, 2017 23:22*

Yes please collect up the files and pictures of your upgrades. If you can do a pull request to the github that would be great. If not give me a Link to the files in a zip, and I will get them added to the repository.


---
**Eric Lien** *January 11, 2017 23:23*

I absolutely love cool mods and upgrades to this platform of printers. So many talented people in our group pushing development forward. Great job **+Torsten Endres**​


---
**Jo Miller** *January 11, 2017 23:44*

looks great  !




---
**larry huang** *January 12, 2017 09:41*

This is one interesting design, I noticed the surface of grey section is a bit rough, I wonder what may be the cause? 


---
**Torsten Endres** *January 12, 2017 15:43*

The gray material is the PVA based water-soluble filament Scaffold from E3D. My print quality using PVA is mostly relative poor. However, it will be dissolved later, so i do not worry much about it.


---
*Imported from [Google+](https://plus.google.com/107299512071300136474/posts/M1A6XhjSi9r) &mdash; content and formatting may not be reliable*
