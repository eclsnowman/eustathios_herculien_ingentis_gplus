---
layout: post
title: "The Community Mods folders are up for the Github of HercuLien and Eustathios Spider V2"
date: November 19, 2015 15:57
category: "Discussion"
author: Eric Lien
---
The Community Mods folders are up for the Github of HercuLien and Eustathios Spider V2.  If you are familiar with Github and have some upgrades you would like to add please feel free to submit a pull request. 



The folders are setup such that the mods/upgrades are in folders based on the Maker who designed the upgrade. I want people to be clear that credit for these great upgrades is from them. Then in the designers folder (or sub folders if there are multiple designs) please include the STL files, native models, and any pictures or text descriptions.



I really appreciate everything this community does to make our platform better and better. Thanks to everyone for all the creativity, I hope these folders will help others not miss out on all the great additions our community comes up with.



[https://github.com/eclsnowman/HercuLien/tree/master/Community%20Mods%20and%20Upgrades](https://github.com/eclsnowman/HercuLien/tree/master/Community%20Mods%20and%20Upgrades)



[https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades) 





**Eric Lien**

---
---
**Bud Hammerton** *November 19, 2015 16:09*

**+Eric Lien**, I do all my modeling in Fusion 360, not having access to SolidWorks and not wanting to pay for a copy. How would you like the submissions formatted? I assume STL is not sufficient, and I would never presume to upload just the STL files. But Fusion 360 is limited to SAT, SMT, IGES, STEP and their own native backup file format.


---
**Ray Kholodovsky (Cohesion3D)** *November 19, 2015 16:43*

I'd vouch for STEP - it's a universal format that most CAD suites will open. So is IGES, but I think more people prefer STEP. Keep in mind you lose any parametric history when you deviate from the existing design program.  

Can you make a public link in Fusion360 so that if someone wanted to build on your design they could just click on it and pick up where you left off in the cloud? It might be nice to supply such a link in the description/ readme with the STEP files. 


---
**Bud Hammerton** *November 19, 2015 16:45*

**+Ray Kholodovsky**​​ Or just provide the backup file which is stored locally. All parametric data is retained in the backup file. ﻿


---
**Ray Kholodovsky (Cohesion3D)** *November 19, 2015 16:48*

I suppose, but wouldn't that take more work than someone just clicking a public link to the design and that's it, they're ready to view and edit? 


---
**Bud Hammerton** *November 19, 2015 16:51*

I am not familiar with collaboration options in A360 to know if you can prevent modifications to probagate to the original model. ﻿



And sharing still requires that the model move, actually copy, from one user to another.  One happens solely in the cloud the other on a local PC.﻿


---
**Eric Lien** *November 19, 2015 16:52*

My personal preference is always X_T for import into solidworks (tends to have the least errors and cleanest import). But STEP is also a great choice. I tend to hate IGES (I always call it "I Guess") because I get nothing but import geometry errors bringing them into solidworks.



Also if you can save backups of Fusion360 native formats which people can import that includes the design tree that would be great too. It's all about options.



For me I just think having some native editable formats are key so the people that come after us can continue to make great upgrades and play with our work. STLs can be editied... but it is a lot messier and not as clean.



For people who use Onshape of similar maybe include links to the online files, then also include some basic format like X_T or STEP just in case people do not have onshape accounts or perhaps it is not allowed in their country for some reason.



But please also include STL for people with no modeling software, who just want to print the upgrades.



Thanks again.


---
**Ray Kholodovsky (Cohesion3D)** *November 19, 2015 16:53*

Gotcha. I use Inventor locally so I'll have to play around with A360 to see what's possible. 


---
**Bud Hammerton** *November 19, 2015 17:03*

I know why X_T is not included in Fusion 360, it is a closed source format that requires a license. Owned by Seimens.


---
**Eric Lien** *November 19, 2015 17:07*

**+Bud Hammerton** AHH good to know. Step is great too. But I stand by the fact that IGES is crap.


---
**Joe Spanier** *November 19, 2015 17:42*

Yea I-guess is almost pointless. Step is the way to go


---
**Florian Schütte** *November 19, 2015 20:02*

So. Here it comes. The day i finally learn to use git :D And some other modelling SW than SketchUp (don't blame me for this please ;))




---
**Eric Lien** *November 19, 2015 20:30*

**+Florian Schütte** it was the first time for all of us at one point, so don't feel bad. :)


---
**Bud Hammerton** *November 19, 2015 21:38*

**+Florian Schütte**, SketchUp is a hard place to start, so I envy you. I started with 123D Design and outgrew its capabilities within the first few days of use. Went straight to FreeCAD (convoluted with hard to comprehend workflow). Then I tried a bunch of the online web-based stuff. Finally settled on Fusion 360 as it was more robust, works a lot like other Autodesk products and was free.


---
**Jo Miller** *November 21, 2015 10:58*

I´m using Maxon C4D  , but thats mainly for its additional sculpting options. 


---
**Eric Lien** *November 24, 2015 16:40*

**+Walter Hsiao** thanks for the pull requests. Files are up on the github now.



**+Ashley Webster** Perhaps Walter can help me on this. I am to be honest not the best at using githib. But my understanding is you want to add all the files to your fork of community mod folder, then make those changes (your files only) a commit. Then submit that commit as a pull request ([https://help.github.com/articles/using-pull-requests/](https://help.github.com/articles/using-pull-requests/))



**+Walter Hsiao** does this sound right with how you just submitted yours? I am not very used to collaboration on my Github. So I am actually a big noob.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/W64oMjvwGCZ) &mdash; content and formatting may not be reliable*
