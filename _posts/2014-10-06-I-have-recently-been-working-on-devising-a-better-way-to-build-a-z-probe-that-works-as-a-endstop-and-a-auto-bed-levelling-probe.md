---
layout: post
title: "I have recently been working on devising a better way to build a z probe that works as a endstop and a auto bed levelling probe"
date: October 06, 2014 15:20
category: "Show and Tell"
author: Rupin Chheda
---
I have recently been working on devising a better way to build a z probe that works as a endstop and a auto bed levelling probe.



I am happy to say that I got success today.



There are a lot of reasons why the servo probe did not work for me. The primary was that servos were noisy and affected callibration.



A solenoid actuator was put into use to lift and extend an M3 screw. On this screw was a interruptor that runs into its slot when moved up and down.



Although the solenoid will have two spots in which it stays stable, we can push it with the print bed to interrupt a beam of light coming from the Infrared photo interrupter module.

There was a lot of hacks in software +hardware that i had to perform to accomplish this.



One of them was that the power to the interruptor module is now driven from a Arduino pin, rather than the 5v supply. This is to avoid any large changes in Marlin code to ignore interruptions of the module when the probe moves up and down. The module is only powered when the interruption is supposed to happen.







Enjoy the video of the probe in action.




{% include youtubePlayer.html id=IcpadyjHqhs %}
[Solenoid Probe](https://www.youtube.com/watch?v=IcpadyjHqhs)





**Rupin Chheda**

---
---
**Rupin Chheda** *October 06, 2014 17:21*

There has to be an actuator of some sort. It can be a servo, a solenoid or something else. 


---
**Alex Benyuk** *October 06, 2014 17:36*

great work! I am going to use servo for the probe, hate them but I have no experience with solenoids


---
**James Rivera** *October 06, 2014 21:43*

Looks nice!  This reminds me a bit of **+nophead**'s auto-z probe, although his only checked one spot.



[http://hydraraptor.blogspot.com/2011/04/auto-z-probe.html](http://hydraraptor.blogspot.com/2011/04/auto-z-probe.html)


---
**ThantiK** *October 07, 2014 01:12*

Hey Rupin!  Grats on the Hackaday feature!  We talked there a bit :)


---
**Eric Lien** *October 08, 2014 00:54*

What about a small air solenoid (they can be found in very small and low weight configurations). Super low power pump to drive it. Small push lock line so no extra power lines to the head. If you wanted to get fancy use a pressure transducer on the air line to sense contact with the bed and then even the sensor could be remote (off the carriage)?﻿


---
**Mike Kelly (Mike Make)** *October 17, 2014 17:19*

Rupin, nice design. Is there a compelling reason to use opto-endstops vs a mechanically actuated endstop like on **+ThantiK**'s delta bot?


---
**Rupin Chheda** *October 21, 2014 05:58*

The reason i used a opto probe vs a mechanical switch is that the probe needs clearance across its entire range. The slot of the opto helps me with that constraint. The part that interrupts the beam should be able to move through the opto, below it (extended) and above it (retracted)


---
*Imported from [Google+](https://plus.google.com/+RupinChheda/posts/3m2FBNSe91u) &mdash; content and formatting may not be reliable*
