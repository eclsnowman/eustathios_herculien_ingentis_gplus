---
layout: post
title: "Ingentis Parts printing. Borrowed Prusa I2 with Prusa Nozzle V.1 tuned fairly well"
date: March 25, 2014 07:46
category: "Show and Tell"
author: Jim Squirrel
---
Ingentis Parts printing. Borrowed Prusa I2 with Prusa Nozzle V.1 tuned fairly well. 

![images/8a0f7ca9997bb01a87fedf9323254440.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8a0f7ca9997bb01a87fedf9323254440.gif)



**Jim Squirrel**

---
---
**Jim Squirrel** *March 25, 2014 07:49*

Also this was printed with cura 14.01 because i like the heater control button  before i press print.


---
**Brian Bland** *March 25, 2014 10:01*

Do you use Cura to control the printer?  I just use it for slicing and use OctoPrint for the printer.


---
**Jim Squirrel** *March 25, 2014 10:04*

Yes I had cura do the slicing and printer control via USB 


---
**D Rob** *March 25, 2014 19:29*

**+Jim Squirrel** the way I do it to although I am moving to a gadgets3d controller on my bots


---
**D Rob** *March 26, 2014 05:18*

**+Jim Squirrel** what version carriage is that and who designed it?


---
**Jim Squirrel** *March 27, 2014 05:54*

that was from **+Tim Rastall**'s design off youmagine


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/99Ybp6U5qMX) &mdash; content and formatting may not be reliable*
