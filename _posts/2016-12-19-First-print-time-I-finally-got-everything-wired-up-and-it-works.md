---
layout: post
title: "First print time! I finally got everything wired up, and it works!"
date: December 19, 2016 09:44
category: "Build Logs"
author: Neil Merchant
---
First print time!



I finally got everything wired up, and it works! Well, it worked for a little while. Smoothie called a halt partway through the print citing that the bed had failed to reach the set temperature  - a tad odd, seeing as it was already there and had been for awhile. I'm sure I just messed up a config option somewhere.



For what it did print it looks pretty good - I've got a little vibration that needs working out when the Y axis moves in the negative direction, but otherwise it's not bad at all for a first print.




**Video content missing for image https://lh3.googleusercontent.com/-GjdfqoN58kA/WFer_d1wX8I/AAAAAAAAJis/M7lN73QzbHc9Hoe0f-t3Jn6gbWEjEwtPQCJoC/s0/VID_20161215_232338.mp4**
![images/f693a401e0906811d6de56db991bb860.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f693a401e0906811d6de56db991bb860.jpeg)
![images/132246f9212972592bf90c89c24622c1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/132246f9212972592bf90c89c24622c1.jpeg)

**Neil Merchant**

---
---
**Eric Lien** *December 19, 2016 16:25*

Have you tuned your PID values for you bed (each printer will be slightly different depending on the heater you used). How stable is the bed temp currently?



I never had much luck with smoothieware auto PID tuning. I have always had to hand tune mine to get the results I want.


---
**Eric Lien** *December 19, 2016 16:29*

This graphic is about the best thing I have found to show how changing the P, I, and D terms will change the loop performance. It really helps visualize how to tune for stable temps: 

![images/44c123a3cc52a664cbdc9315caa19ebf.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/44c123a3cc52a664cbdc9315caa19ebf.gif)


---
**Eric Lien** *December 19, 2016 16:30*

The graphic is from here which has lots of good info: [en.wikipedia.org - PID controller - Wikipedia](https://en.wikipedia.org/wiki/PID_controller)




---
**Ted Huntington** *December 19, 2016 16:54*

looks good - sounds like possibly there is still some friction- this was a problem I found initially too- the carriage just needs more working out I think- possibly recheck alignments.


---
**Neil Merchant** *December 20, 2016 07:08*

**+Eric Lien**  I haven't tried to tune my PID for the bed, but I've actually been pretty happy with it - it holds within 0.3 degrees using the settings provided in the example firmware from the github.



 **+Ted Huntington** You're right - there is some friction when the Y axis moves in the negative direction. If it doesn't work itself out in the next hour or two of movement I'll likely go back and check my alignments again; I've gone around the machine four times now, but once more will never hurt.


---
*Imported from [Google+](https://plus.google.com/105839137113604539428/posts/esGYh2Ev1oF) &mdash; content and formatting may not be reliable*
