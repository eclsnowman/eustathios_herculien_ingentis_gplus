---
layout: post
title: "I don't know what happened, but I accidentally doubled up on stepper motors and 300x300mm silicone heater pads (24V) from Robotdigg"
date: July 15, 2015 14:35
category: "Discussion"
author: Bryan Weaver
---
I don't know what happened, but I accidentally doubled up on stepper motors and 300x300mm silicone heater pads (24V) from Robotdigg.  I just completed the order when I realized my mistake, so they are on the way.  Let me know if you are interested.





**Bryan Weaver**

---
---
**Gus Montoya** *July 16, 2015 05:43*

I am interested, can you message me please.


---
**Igor Kolesnik** *July 16, 2015 18:11*

Did you get the geared motor for extruder? I would be interested. Also heated pad would be nice. Please contact me﻿


---
**Bryan Weaver** *July 16, 2015 18:42*

Yes.  Geared 5:1 stepper and 3 x 60mm steppers.  I don't have the stuff in-hand yet, it's on it's way. **+Gus Montoya** has first shot at it, but  I'll let you know what's going on when the stuff arrives.


---
**Igor Kolesnik** *July 16, 2015 18:46*

**+Bryan Weaver** cool, thanks


---
*Imported from [Google+](https://plus.google.com/111820797809026464429/posts/cMerbkAarcy) &mdash; content and formatting may not be reliable*
