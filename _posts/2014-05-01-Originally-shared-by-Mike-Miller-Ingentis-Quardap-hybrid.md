---
layout: post
title: "Originally shared by Mike Miller Ingentis/Quardap hybrid"
date: May 01, 2014 07:32
category: "Deviations from Norm"
author: Tim Rastall
---
<b>Originally shared by Mike Miller</b>



Ingentis/Quardap hybrid. 



It'll have the Ingentis Z-axis, and use the top X and Y axes for gantries to actuate the extruder, a-la **+Shauki Bagdadi**'s design. 



Still a work in progress, but it'll be on display as this weekend's Denver Makerfaire, along with the sample corner piece in the photo and info on ultra-low-cost but not low-quality printing. 



It's right on target for a 12x12x18 (300x300x450cm) build platform...for a whole lot less than a ZX18.



(and thanks, **+MISUMI USA** ! #Misumifirst150)

![images/d534a257519d792c6281c4c9213f8325.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d534a257519d792c6281c4c9213f8325.jpeg)



**Tim Rastall**

---
---
**Mike Brown** *May 01, 2014 08:13*

Definitely shaping up to be a sexy beast.  Can't wait for finalized plans to build one myself.  My tantillus just isn't satisfying my lust for large prints.


---
**Mike Miller** *May 01, 2014 11:38*

Thanks **+Tim Rastall**, I meant to cross post but am easily distracted. #Squirrel!


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/Y3HGKhgDaa4) &mdash; content and formatting may not be reliable*
