---
layout: post
title: "I have a question that has been nagging me"
date: November 20, 2016 22:07
category: "Discussion"
author: Sean B
---
I have a question that has been nagging me.  I am looking for a filament drying solution that I saw recently..It can be printed and attaches to an individual roll, then you slide in a strip of plastic to contain the roll and pop in a bag or two of silica gel.  Has anyone seen this or can point me in the right direction?







**Sean B**

---
---
**Eric Lien** *November 21, 2016 00:59*

Yeah, desiccant will only help maintain the humidity by absorbing excess humidity in the enclosure or surrounding air. To get the filament to release humidity already in it, It's best to heat the filament then seal it in an enclosure with dry desiccant.


---
**Carlton Dodd** *November 21, 2016 01:18*

I bought a food dehydrator, added a cardboard spacer to fit two spools, and another tray on top for desiccant bags (they need to be dryer than what you're trying to keep dry).  Seems to work great, though I haven't printed anything recently.



![images/caf7600e4d535328421190647a62cfb4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/caf7600e4d535328421190647a62cfb4.jpeg)


---
**Sean B** *November 21, 2016 12:28*

Thanks everyone, I know small desiccant packets aren't going to dry my filament.  I have a large amount of silica gel for that, I was just looking for a way to maintain dried filament while on the machine.


---
**Dani Epstein** *November 22, 2016 10:15*

I also use a food dryer, but dry my dessicant in the microwave, which takes around 3 minutes for a small bowl. Never, ever touch hot dessicant out of a microwave, it will give you horrble nurns


---
**Sean B** *November 22, 2016 10:39*

**+Dani Epstein** I know burns hurt but I can't imagine what nurns feel like 😀


---
**Dani Epstein** *November 22, 2016 10:43*

Nurns are like burns, but with more "n"s. Funny how the spell checker changes almost everything my fat fingers mash out except for the important ones. This time round I had to force it to let me write "nurns". Weird.


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/MDLhrtqnPxx) &mdash; content and formatting may not be reliable*
