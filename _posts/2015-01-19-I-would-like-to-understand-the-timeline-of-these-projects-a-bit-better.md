---
layout: post
title: "I would like to understand the timeline of these projects a bit better"
date: January 19, 2015 23:28
category: "Discussion"
author: Dat Chu
---
I would like to understand the timeline of these projects a bit better. It looks like the Eustathios is based on the Ingentis. Is the Herculien based on these options and thus come after them?



I would like to understand better the design choices between these quad machines. Knowing who comes first would make it easier to see the progress.





**Dat Chu**

---
---
**Eric Lien** *January 20, 2015 00:36*

Tantilus -> Ingentis -> Eustathios -> HercuLien. I built a Eustathios, and liked it so much I gutted my CoreXY (except the Z-stage cause it worked great) and made a larger version of Eustathios since my corexy frame was 24"x24"x24".


---
**Eric Lien** *January 20, 2015 00:40*

Oh and ultimaker is in there, but I was never sure if it is before or after tantilus.


---
**Dat Chu** *January 20, 2015 01:00*

Cool. Thank you for the info. Is there a resource out there that have some numbers on how much more rigid these options are comparing to other printer types?



Also, if I play the devil's advocate, why should one go with Herculien instead of a rigidbot? They have very similar builds. The placements of the motors are different but given that I am a total noob at designing machines, I am trying to figure out the Herculien's improvements.


---
**Tim Rastall** *January 20, 2015 01:01*

The Sequence goes as follows:

Ultimaker (2011)

Tantillus (2012)

Ingentis (Late 2013)

Eustathios (March 2014)

Herculien (August 2014)



The choice lies between Eustathios or Herculien - This really boils down to your budget as the Herculien has a a larger BOM and uses a lot more aluminium but results in a (what I assume is) a more rigid bot. Whether there is any improvement in speed or print quality is debatable.


---
**Eric Lien** *January 20, 2015 01:17*

I would say fine detail quality goes to Eustathios due to smaller span and lower weight. Large build for large items and large ABS prints goes to HercuLien with the enclosure and thick heat spreader for really uniform heat.



I will be the first to admit I fight z ribbing from time to time, and not all my prints are as pretty as ones I post... But most of the time bad prints are due to experiments with settings that don't go well. Also I should really buy higher quality filament.﻿


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/BVFQmcUuT9J) &mdash; content and formatting may not be reliable*
