---
layout: post
title: "My printer is still a big battlefield (need to print a few thinks to complete it), but it started to print today"
date: October 23, 2016 16:13
category: "Discussion"
author: Peter Kikta
---
My printer is still a big battlefield (need to print a few thinks to complete it), but it started to print today. It is now printing it's first print :)

Thank you all, you are a great community.

![images/0b612f4a5ae4d291692c19c2bf0436a6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0b612f4a5ae4d291692c19c2bf0436a6.jpeg)



**Peter Kikta**

---
---
**Eric Lien** *October 23, 2016 16:52*

Man, lot's of new printers recently. This is so much fun to watch. Neat to see lots of people going with the **+Walter Hsiao**​ carriage. He does such great work.


---
*Imported from [Google+](https://plus.google.com/101209784005254598834/posts/RYWw1KYkbDD) &mdash; content and formatting may not be reliable*
