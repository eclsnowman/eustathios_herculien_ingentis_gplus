---
layout: post
title: "For the frame construction, is there any reason why not to use a regular Home Depot 90 degrees bracket instead of the expensive special T-Slot brackets?"
date: November 08, 2014 19:56
category: "Discussion"
author: Shachar Weis
---
For the frame construction, is there any reason why not to use a regular Home Depot 90 degrees bracket instead of the expensive special T-Slot brackets? I can buy a whole box of these for a few bucks.





**Shachar Weis**

---
---
**D Rob** *November 08, 2014 20:47*

if you use t slot, brackets aren't needed


---
**Kalani Hausman** *November 08, 2014 21:09*

Angle brackets can easily flex at the angle of inflection - not so much as to cause them to fail at holding up a shelf, but enough to distort the extruder's alignment in a 3D Printer.


---
**Shachar Weis** *November 08, 2014 21:29*

Gotcha


---
**Shachar Weis** *November 08, 2014 21:42*

**+D Rob** Can you explain? In all the diagram and pictures I've seen, everyone seems to use brackets.


---
**Eric Lien** *November 08, 2014 21:53*

**+Shachar Weis** cross drilled hole and a 5mm button head screw can do the trick. Look back through D Robs YouTube videos. He explained it in one of his assembly videos.


---
**D Rob** *November 08, 2014 22:00*

Tap the end holes in the extrusion. put a 1/4-20 or 5mm button head screw in it. Watch **+Thomas Sanladerer** outlaw tapping video. The head fits the t slot like a t nut. All you need are holes through the t slot for the allen wrench to secure. It's tighter than Dick's hatband. my vid does show an assembly method that make all cuts, but the vertical extrusion a whole lot less critical.


---
**D Rob** *November 08, 2014 22:05*

 here is the link to said video in case anyone else is curious. [https://plus.google.com/u/0/108729945898131117315/videos?pid=5989375241070804258&oid=108729945898131117315](https://plus.google.com/u/0/108729945898131117315/videos?pid=5989375241070804258&oid=108729945898131117315) 


---
**Wayne Friedt** *November 08, 2014 23:26*

I ended printing some 100mm x 100mm corner brackets that work way better than those little corner ones you can buy. This is one of the corners that incorporated a filament holder into. The other corners just have a 45 deg brace.   I can send if anybody would like. [https://www.facebook.com/my3dph/photos/pb.595124113871348.-2207520000.1415489025./774121785971579/?type=3&src=https%3A%2F%2Fscontent-a-sea.xx.fbcdn.net%2Fhphotos-xap1%2Fv%2Ft1.0-9%2F10455188_774121785971579_3448500890548846248_n.jpg%3Foh%3D7ba9ae6a6a0b5ddb0a07ccd71f289282%26oe%3D54D28993&size=960%2C720&fbid=774121785971579](https://www.facebook.com/my3dph/photos/pb.595124113871348.-2207520000.1415489025./774121785971579/?type=3&src=https%3A%2F%2Fscontent-a-sea.xx.fbcdn.net%2Fhphotos-xap1%2Fv%2Ft1.0-9%2F10455188_774121785971579_3448500890548846248_n.jpg%3Foh%3D7ba9ae6a6a0b5ddb0a07ccd71f289282%26oe%3D54D28993&size=960%2C720&fbid=774121785971579)


---
**Mike Thornbury** *November 09, 2014 02:20*

I work a bit with extrusion. Tapping the end is a mugs game :)



For joining extrusion I use self-tapping hex-head screws, like these: [http://www.aliexpress.com/item/Grade-8-8-HEX-socket-Head-self-tapping-Screws-M5-30mm-50pcs-for-MODEL-SPEAKERS/831267061.html](http://www.aliexpress.com/item/Grade-8-8-HEX-socket-Head-self-tapping-Screws-M5-30mm-50pcs-for-MODEL-SPEAKERS/831267061.html)



You just drill the end to ease the way - for M5 screws I drill a 4mm hole (always use lube for drilling and screwing, my Dad says! ;)



For mounting 10mm or 8mm or 6mm smooth shafts, I use these: [http://www.aliexpress.com/item/8-mm-bearing-kirksite-bearing-insert-bearing-with-housing-KP08-pillow-block-bearing/1121096212.html](http://www.aliexpress.com/item/8-mm-bearing-kirksite-bearing-insert-bearing-with-housing-KP08-pillow-block-bearing/1121096212.html)



There are all manner of different sizes and they screw straight into t-slot nuts. You can swap out the bearings if you want - so you can fit any sized shaft you need.



As to corner brackets, I use two types with 2020 - the small 2020 sized ones like this: [http://www.aliexpress.com/item/Corner-Brakets-Aluminum-color-powder-coated-used-for-profile-20-series/798082376.html](http://www.aliexpress.com/item/Corner-Brakets-Aluminum-color-powder-coated-used-for-profile-20-series/798082376.html)



Or for making sure things are square, I use the larger 3060 brackets - they fit in the 2020 channel just fine and work well as feet.: [http://www.aliexpress.com/item/3060-Aluminum-Profile-Corner-Fitting-Angle-30-60-Decorative-Brackets-Aluminum-Profile-Accessories-L-Connector/2032521626.html](http://www.aliexpress.com/item/3060-Aluminum-Profile-Corner-Fitting-Angle-30-60-Decorative-Brackets-Aluminum-Profile-Accessories-L-Connector/2032521626.html)



They do project past the extrusion, so you need to think about where you will use them, but they are strong as heck and only 60c each.



I tend to buy stock - 50 or so at a go. And buy quality spring-loaded t-nuts, not those cheap but annoying hammer-head t-nuts that fall out or twist when you are trying to slide them into place. I always have a large selection of drop-in t-nuts as well. Nothing worse than having to pull your creation apart because you forgot to drop in a t-nut.


---
**Shachar Weis** *November 09, 2014 03:40*

**+Zut Alors** Awesome links, thanks ! Looks like this could save some time and money.


---
**Mike Thornbury** *November 09, 2014 04:06*

**+Shachar Weis**  If you are going to buy from Aliexpress, make sure you do so on or after the 11/11. There is a massive sale going on - you should be able to get about 15% off most things.



And if you haven't dealt with Aliexpress before - be really careful about reading the small print - they aren't dishonest, but sometimes details are lost in translation. And always negotiate.


---
**Shachar Weis** *November 09, 2014 04:52*

How do you negotiate? Do you send messages to the vendors asking for a lower price ?


---
**Wayne Friedt** *November 09, 2014 05:07*

Best to install " Trade manager ". Can be downloaded from alibaba's site.

Can send mes also. each item for sale has a ms link to the seller.


---
*Imported from [Google+](https://plus.google.com/117479393665221551027/posts/bgLVxbVyJum) &mdash; content and formatting may not be reliable*
