---
layout: post
title: "Does anyone have a better setup guide for the Azteeg X5 mini and Viki2.0 LCD?"
date: August 31, 2015 23:49
category: "Discussion"
author: Bryan Weaver
---
Does anyone have a better setup guide for the Azteeg X5 mini and Viki2.0 LCD?  I'm trying to follow the quick setup guide on Panucatt's website, but it's not really working for me.



I got the firmware file from the Panucatt website and the config file from the Eustathios Spider V2 Github and loaded them on my microSD.  I insert the microSD in the X5 and connect the USB to my computer, like the Quick Setup Guide tells me to.  It says some LEDs are supposed to flash letting you know Smoothieware is running.  The LEDs blink, but the computer just says it cannot connect to the USB device and I don't know where to go from there.  The Guide says once Smoothieware is running you should be able to connect to Repetier, but that's not working for me either..  Don't really know where to go from here.





**Bryan Weaver**

---
---
**Eric Lien** *September 01, 2015 00:10*

You might need to install the driver file for your OS to recognize the board. Are you on windows, Mac, or Linux?



[http://smoothieware.org/windows-drivers](http://smoothieware.org/windows-drivers)


---
**Bryan Weaver** *September 01, 2015 02:46*

Running Windows.  Got the driver installed.  Got the board to connect to Repetier, but can't get anything to move.  I should be able to manually move stuff around in Repetier, right?


---
**Eric Lien** *September 01, 2015 03:17*

**+Bryan Weaver** yes. Did you rename the firmware before saving it to the SD card so it installs when you power up?


---
**Bryan Weaver** *September 01, 2015 12:47*

There's 2 links for the firmware file on Panucatt's website.  The first link (that you're supposed to rename 'firmware.bin') is not working.  The alternate link, to download the firmware from the Smoothieware Github, already has the file named firmware.bin when you download it.  That's the one i'm using.


---
**Jim Wilson** *September 01, 2015 13:56*

It's probably not the issue, but you MAY have to initiate homing before it'll let you move in any other direction, but that may not be the case with your board


---
**Bryan Weaver** *September 01, 2015 15:19*

Homing doesn't do anything either, just says "Command Waiting". 



Windows still gives me a "USB device not recognized" error when I initially plug it in, but when I go to the device manager everything seems to be working properly..


---
**Eric Lien** *September 01, 2015 17:54*

Try 1.0 driver vs 1.1


---
**Bryan Weaver** *September 01, 2015 18:11*

I'll try that when I get home this evening.



I'm not just missing some glaringly obvious step, am I? Firmware and config saved on microSD, insert microSD into X5,  connect board to computer via USB (with jumper set to USB power), then connect to Repetier Host. I should be able to home and move some things around at that point, right? (For the record, I've gone through all steps with and without external 24v power hooked up to the X5. Nowhere in the panucatt quick setup guide does it say to connect to any power other than USB..)


---
**Eric Lien** *September 01, 2015 19:56*

You cannot move the printer without the 24V to the board. USB power is just for control logic, not stepper power. Also I always run the jumper to power from the int, not usb. But that's just preference, 


---
**Bryan Weaver** *September 01, 2015 21:17*

I figured, just found it curious that the guide makes no mention of ever connecting external power.. 



Anyway, I'll try moving the jumper and the 1.0 driver later, see if I can get anything to happen.


---
**Bryan Weaver** *September 02, 2015 02:46*

I moved the jumper to INT and I no longer get a "USB device not recognized" error when I connect to the computer, but still nothing happening. I tried connecting the Viki2.0 lcd, should anything be displaying on there when the firmware loads? The LCD backlight  turns on, but no text or anything is displayed.


---
**Eric Lien** *September 02, 2015 05:05*

**+Bryan Weaver** can you provide a picture of how you have it wired?


---
**Bryan Weaver** *September 02, 2015 17:58*

Progress!!  No pictures yet., but I went home at lunch and messed with it, finally figured out I had the endstops wired up wrong.  I had the ground wired to NO on the endstops instead of NC.  After that everything booted up correctly and I was able to move stuff around from the Viki lcd.  I'll be able to mess with it some more when I get home this evening.  Hopefully get some video of the break in gcode in action.



One thing I did notice, when trying to home the axes, the carriage is moving away from the endstop in the Y direction (toward the front of the printer).  How do I go about reversing the direction?


---
**Eric Lien** *September 02, 2015 20:36*

You edit the config file to reverse the direction on Y, or you can flip two wires on the stepper.


---
**Eric Lien** *September 02, 2015 20:37*

Also home should be front left usually.


---
**Bryan Weaver** *September 02, 2015 21:42*

I could have sworn I saw a rendering that showed the endstops at the back-right of the printer (that's the way my i3 is set up so I didn't think anything of it.  Prints come out facing the back of the bed.)  Looking at the renderings again, I see the endstops clearly at the front-left... I don't know what I was looking at..



So I guess my Y stepper is moving in the right direction and X is reversed.  I just have my endstops in the wrong spot.



Jeez, building all the mechanical parts of the printer was a breeze, this configuration stuff is kicking my ass...


---
**Eric Lien** *September 02, 2015 23:27*

**+Bryan Weaver** you are on the home stretch.


---
**Eric Lien** *September 02, 2015 23:28*

And back right is fine, just all your jogging will be reversed from the screen in host software


---
**Bryan Weaver** *September 03, 2015 00:52*

Well I just finished moving the endstops to the front left right before reading this, ha.. I think it works better in the front left than back right though.  It's a pretty tight fit to get the endstops situated correctly back there and I'm not sure the extruder is able to reach to full extents of the bed. 


---
*Imported from [Google+](https://plus.google.com/111820797809026464429/posts/EbKbMvwy9vS) &mdash; content and formatting may not be reliable*
