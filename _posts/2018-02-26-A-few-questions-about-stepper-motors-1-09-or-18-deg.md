---
layout: post
title: "A few questions about stepper motors: (1) 0.9 or 1.8 deg?"
date: February 26, 2018 23:05
category: "Discussion"
author: Julian Dirks
---
A few questions about stepper motors:



(1) 0.9 or 1.8 deg?

(2) Robotdigg ones obviously work but is there anything to be gained by paying a bit more for something of supposedly better quality?



If it matters I'm more interested in print quality than high printing speeds.  





**Julian Dirks**

---
---
**Sébastien Plante** *February 26, 2018 23:36*

0.9 (400 steps per revolution) is better than 1.8 (200 steps), but 1.8 is most common. You must look at the others specs, like holding torque, speed, ...





It depends on your need, I've got some cheap Chinese from Aliexpress and they work fine, but better quality will have better control (more precise), less chance of skipping step, specs will be more "actually" what is the stepper is, might run cooler, etc... but does it worth it? I don't know...


---
**Oliver Seiler** *February 26, 2018 23:49*

I'm quite happy with these ones. I've geared them down 2:1 for more torque and they're still fast enough

[robotdigg.com - NEMA17 60mm 17hs6002 high torque stepper motor - RobotDigg](https://www.robotdigg.com/product/29/NEMA17-60mm-17hs6002-high-torque-stepper-motor)


---
**Daniel F** *February 27, 2018 00:12*

I use the same ones, reliable for over 3 years. The connectors are also handy if you need to take things apart.


---
**Eric Lien** *February 27, 2018 01:16*

I would say the 60mm robotdigg ones have served me well. But a quality 0.9deg stepper would also serve you well.


---
**Oliver Seiler** *February 27, 2018 01:19*

FYI I have used Robotdigg 0.9degree steppers before and they had too little torque and did not work.


---
**Julian Dirks** *February 27, 2018 01:56*

**+Oliver Seiler** Could you please explain more about gearing the motors down? (how/why etc).  Conscious I have gone for 10mm cross rods and will probably go for a direct drive so will be trying to move a fair bit of weight around.  Sounds like a good option just to start with 1.8 degree Robotdiggs.


---
**Oliver Seiler** *February 27, 2018 02:03*

**+Julian Dirks** I don't think it's really required, if I remember correctly I had a few carbon fibre infused PET prints with skipped steps. PET-CF tends to  build up filament on the nozzle a fair bit, so I wanted a little more grunt. I've simply replaced the pulleys on the motor/rods with 20/40 (or 16/32) teeth ones

![images/15a039c11f9540567e17010c1962fdd7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/15a039c11f9540567e17010c1962fdd7.jpeg)


---
**wes jackson** *February 27, 2018 04:56*

Here is a link to the best steppers ive used. They are 1.8, but since i only use TMC drivers, i have never noticed any diff.

[https://www.ebay.com/itm/5pcs-NEMA17-BIPOLAR-STEPPER-MOTOR-76-oz-in-for-3D-Printer-ship-from-chicago/181582676246?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649](https://www.ebay.com/itm/5pcs-NEMA17-BIPOLAR-STEPPER-MOTOR-76-oz-in-for-3D-Printer-ship-from-chicago/181582676246?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649)


---
**Sébastien Plante** *February 27, 2018 13:00*

**+wes jackson** wow, pretty price too! :o


---
**wes jackson** *February 27, 2018 17:59*

yus, its the only ones i get anymore. shipped out of chicago too, so usually fast.


---
**Walter Hsiao** *February 27, 2018 19:39*

Just fyi, the robotdigg 0.9 motors are also available in a 60mm version now, I'm not sure when they added them but the largest ones I could get when I built my printer were 48mm - [https://www.robotdigg.com/product/1250/0.9-step-angle-NEMA17-stepper-motors](https://www.robotdigg.com/product/1250/0.9-step-angle-NEMA17-stepper-motors).  I just upgraded the ones in my printer.



You can buy from Automation Technology directly if you want fewer motors or a mix of motors. - [https://www.automationtechnologiesinc.com/products-page/nema-17/nema17-stepper-motor-3/](https://www.automationtechnologiesinc.com/products-page/nema-17/nema17-stepper-motor-3/)

Looks like they also have a 72mm, 124oz-in motor for anyone who wants to run really fast - [https://www.automationtechnologiesinc.com/products-page/nema-17/kl17h272-20-4a/](https://www.automationtechnologiesinc.com/products-page/nema-17/kl17h272-20-4a/)




---
**Walter Hsiao** *February 27, 2018 19:44*

I like the E3D silicone socks (pro version) for preventing PETG buildup on the nozzles,  They eventually break down though. [https://www.filastruder.com/collections/e3d-spare-parts-and-accessories/products/silicone-socks-3-pack](https://www.filastruder.com/collections/e3d-spare-parts-and-accessories/products/silicone-socks-3-pack)


---
**Oliver Seiler** *February 27, 2018 19:49*

I could never get the silicone socks to works properly, they always got in the way at some point. Maybe time to try again,thanks for the reminder **+Walter Hsiao**


---
**Walter Hsiao** *February 27, 2018 20:04*

Yeah, they don't attach very securely, it may be worth using kapton or wire to secure them.  I also find they block airflow around the nozzle so I don't use them with PLA, but I usually put them on if I'm doing a medium or large PETG print.


---
**Dennis P** *February 27, 2018 21:59*

**+Sébastien Plante** I am local to the seller and have used those motors and some of their other products on other projects too.Though, I am having my own issues right now,  I am pretty sure its not the motors. 


---
**Dennis P** *February 27, 2018 22:01*

+Oli**+Oliver Seiler**  I put a couple of wraps of 4mm kapton tape around the sock a the top to keep the ears from falling away from the heater block so that the lips stay engaged. Seems to work pretty good and easily removable and replaceable.  

 


---
**Julian Dirks** *March 01, 2018 00:57*

Thanks for the advice.  I've ordered the 1.8 Deg steppers from Automation technologies in the US mainly because of freight.  Robotdigg was going to be US$38 freight for $30 worth of steppers.  To be fair they're heavy things but even freight from E3D in the UK (pretty much the polar opposite to NZ) was cheaper than RD.


---
*Imported from [Google+](https://plus.google.com/113795478307151372873/posts/Q5NohctGEbE) &mdash; content and formatting may not be reliable*
