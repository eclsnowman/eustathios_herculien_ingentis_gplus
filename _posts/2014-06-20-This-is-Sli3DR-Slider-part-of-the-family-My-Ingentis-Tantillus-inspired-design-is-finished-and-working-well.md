---
layout: post
title: "This is Sli3DR (Slider) - part of the family - My Ingentis/Tantillus inspired design is finished and working well"
date: June 20, 2014 11:58
category: "Show and Tell"
author: Richard Horne
---
This is Sli3DR (Slider) - part of the  #RepRapMy   #3DR  family - My Ingentis/Tantillus inspired design is finished and working well.



I will share the files, design and info about it on my Blog and **+YouMagine** as soon as I can.



I hope you like it, main aim was to keep as much as possible 3D printed, use spectra line for all axis (including Z) and use the recently discussed (was it was recent when I designed it) 'unnamed mechanism' for the X/Y motion.



It uses a modified Ingentis Z arm (thanks **+Tim Rastall** ) - all other parts are 'new' including an experimental full-supported Bowden Extruder.



![images/de4682c0d40aad033ed0c142ed75b0bf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/de4682c0d40aad033ed0c142ed75b0bf.jpeg)
![images/7435ba8c8d170e3d8e955042ab555833.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7435ba8c8d170e3d8e955042ab555833.jpeg)
![images/1eb01b6ffbb48706141181f573e29fdb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1eb01b6ffbb48706141181f573e29fdb.jpeg)
![images/350a501ee3d5d7b2b960010422829e73.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/350a501ee3d5d7b2b960010422829e73.jpeg)
![images/db93e5126d86a279fb68c3357ba2af9f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/db93e5126d86a279fb68c3357ba2af9f.jpeg)
![images/24fe499c87e2b99cf7af7c4c59a31324.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/24fe499c87e2b99cf7af7c4c59a31324.jpeg)
![images/3abe61678e30283d4de85ed0476ddce3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3abe61678e30283d4de85ed0476ddce3.jpeg)
![images/3abc4869ca961d70087b32f1eefde6d5.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3abc4869ca961d70087b32f1eefde6d5.gif)
![images/9d26944c4f2168c33dc60dd419a704aa.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9d26944c4f2168c33dc60dd419a704aa.gif)
![images/02077605f298ae9fa49f51a73931bc2e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/02077605f298ae9fa49f51a73931bc2e.jpeg)
![images/6f682434ad0caeab51af234cd22be563.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6f682434ad0caeab51af234cd22be563.jpeg)

**Richard Horne**

---
---
**Lynn Roth** *June 20, 2014 12:21*

I look forward to learning more about this.  Do you have a link for the  'unnamed mechanism' ?


---
**Richard Horne** *June 20, 2014 12:34*

**+Lynn Roth** Try this one - [https://plus.google.com/116889746506579771100/posts/VZPzLNnC69G](https://plus.google.com/116889746506579771100/posts/VZPzLNnC69G)

and check the comments on the post


---
**Anders Venaas** *June 20, 2014 12:40*

It looks realy nice. What is the printing size?


---
**Lynn Roth** *June 20, 2014 13:42*

**+Richard Horne** Thanks.  I remember seeing this back then, but had forgotten.


---
**Joe Spanier** *June 20, 2014 15:02*

Hey Rich have you done a guide on the proper way to wind spectra pulleys? I've tried a few times with little luck. 


---
**James Rivera** *June 20, 2014 16:23*

Nice work **+Richard Horne**. How do the layers look? I'm curious to know if you have any Z ribbing.


---
**Tim Rastall** *June 20, 2014 23:37*

Great to see you implementing the cablebot mechanics - I felt pretty sure it was a viable option :).


---
**Tim Rastall** *June 21, 2014 00:04*

**+James Rivera** Z ribbing is usually an artefact of rounding errors related to imperial thread lead screws. It can also occur if a lead screws rotation is eccentric, causing.the bed to move very slightly in the xy plane. Finally, with a heated bed, it's possible that microscopic changes in the bed surfaces thickness, as a result of thermal expansion can produce a form of z ribbing.

In all cases, it's unlikely that Rich's machine will suffer this as he has control over the steps/mm in the Z axis (because that's contingent on the size of the pulley that drives the spectra), there are no lead screws and (I suspect) he'll be running PID control on the heated bed (if there is one).


---
**James Rivera** *June 21, 2014 01:44*

**+Tim Rastall** I'm aware of those problems with z-screws, but I must admit the heat bed expansion is something I hadn't considered. The reason I asked was because I'm wondering if the thickness of the spectra line could be an issue if it overlapped itself enough to change the diameter of pulley, and thus the height. I guess it might be as negligible as the heat bed expansion. I was just curious to hear **+Richard Horne**'s assessment.



The other reason (perhaps another non-issue from me) is if the spectra line stretches under the weight of the platform. Oddly enough, now that I think of it, could they counteract each other? Hmmm...possibly not worth worrying about. Just rambling thoughts I had about the design...  ?:-/


---
**Tim Rastall** *June 21, 2014 02:06*

**+James Rivera** my experience of spectra is that there's not much stretch once it's under tension.  I hung heavy weights off the end of lengths I used on Ingentis to get some of the stretch out before putting it in the bot. The spectra shouldnt overlap but it will walk up and down the axis of the pulley as the pulley rotates.  This,  in theory, means that the distance moved for a given number of steps is very slightly different depending on where the spectra is leaving the pulley (because trigonometry),  allthough I wasn't ever able to measure this effect. EDIT g scratch that about varying distances,  the bride bearings at the base of the Z would negate it I think.


---
**Kalani Hausman** *June 23, 2014 18:38*

I was able to get a mock-up of the looped cables working with stacked 608 bearings and Spectra line, although I suspect there are lower-profile alternatives available. I think a single-strand metal cable like piano wire might also reduce friction during transit of the shuttle.


---
**Richard Horne** *June 27, 2014 10:08*

**+James Rivera** **+Tim Rastall** Having a spectra drive for Z is looking really good, it's not as easy as I wanted to get it all tied up and stretched, but after a short time printing and tightening it's giving really good results.



The gearing is also good enough to get some nice fine layers and also very quick Z lift moves. All things I want on my quest for the perfect low-cost Z axis drive.


---
**James Rivera** *June 28, 2014 03:11*

**+Richard Horne** what kind/brand/strength of spectra line did you use?


---
**Richard Horne** *June 30, 2014 13:14*

**+James Rivera** always real official Spectra Extreme Braid and between 60 and 120lb depending on size of printer, Tantillus and 3DR use 60lb (0.4mm) and Sli3DR uses 80lb (0.48mm)


---
*Imported from [Google+](https://plus.google.com/+RichardHorne_RichRap3D/posts/DLHiQjfVAke) &mdash; content and formatting may not be reliable*
