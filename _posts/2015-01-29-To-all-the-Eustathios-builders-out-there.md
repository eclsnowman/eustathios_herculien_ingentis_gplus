---
layout: post
title: "To all the Eustathios builders out there"
date: January 29, 2015 20:33
category: "Discussion"
author: Isaac Arciaga
---
To all the Eustathios builders out there. Any of you use alternative more affordable gt2 pulleys outside of the original BoM? In particular the high torque 32t pulleys that need to be purchased from Misumi. Does it have to be 32t?

Is there's an alternate BoM out in the wild that's can be sourced easier? I'm not trying to nickle and dime the build, but if I can save some money, I would like to know!





**Isaac Arciaga**

---
---
**Tim Rastall** *January 29, 2015 20:52*

[http://www.aliexpress.com/item/GT2-Pulley-32-Tooth-10mm-Bore/1776739650.html](http://www.aliexpress.com/item/GT2-Pulley-32-Tooth-10mm-Bore/1776739650.html)


---
**Tim Rastall** *January 29, 2015 20:56*

I suggest you drill out a pulley and install you own grub screw. Ive done this in the past by drilling a hole in the aluminium pulley with 2.5mm bit, then tapping it out with a 3mm hardened steel machine screw and loads of cutting fluid. You have to back it out a lot but I couldn't be bothered to go the the store and by a 3mm tap. You then jsut repalce the screw with a standard 3mm grub. 


---
**Tim Rastall** *January 29, 2015 20:59*

**+Oliver Schönrock** The cost of these pulleys is another reason I'm working on the Procerus.


---
**Isaac Arciaga** *January 29, 2015 21:08*

**+Oliver Schönrock** I thought about turning off the shoulders from the RobotDigg 32t pulleys but I do not have the machinery to do this. I would have to send it to someone I know out in Wisconsin (im in S. California) that would be willing to do it for me after first waiting a month or more for the pulleys to arrive from China (if they arrive). Personally I would prefer to dump the $112 from the 7 required pulleys on high end bearings/shafts/lead screws.


---
**James Rivera** *January 29, 2015 21:35*

Would replacing these "odd" pulleys with more standard ones do anything to the build other than reduce the build volume slightly? Is there some other drastic change that would be needed? I mean, looking at the Eustathios design, could a simple spacer shim be added to shaft mounts?


---
**James Rivera** *January 29, 2015 22:22*

If you have Sketchup (free at Sketchup.com) you can view the Eustathios design here:

[https://github.com/eclsnowman/Lien3D_Eustathios_Spider/tree/master/Other%203D%20Formats](https://github.com/eclsnowman/Lien3D_Eustathios_Spider/tree/master/Other%203D%20Formats)


---
**Isaac Arciaga** *January 29, 2015 22:57*

**+James Rivera** Thank you, I already have that covered and been using SpaceClaim to review **+Eric Lien** 's model (awesome btw).



I do have a question for you all regarding the Lead Screw configuration from Misumi. Since Misumi discontinued the "S56-Q8" configuration and the only way to obtain the required step length is by adding a 3mm Keyway. Would this configuration be ok MTSBRB12-410-S56-Q8-C3-J0 ? "J0" is the placement of the keyway from the end of the rod which I had set to 0.


---
**Isaac Arciaga** *January 29, 2015 23:15*

**+Oliver Schönrock** maybe some of the misumi pulleys can be replaced with pulleys with shoulders? [http://i.imgur.com/mWBh0ex.png](http://i.imgur.com/mWBh0ex.png)


---
**D Rob** *January 29, 2015 23:46*

**+Isaac Arciaga** a rod, drill, and hacksaw blade can easily remove the shoulder. Lock the rod in the drill. The pulley on the rod. Then as it turns use the saw like a cut off tool and done.


---
**Eric Lien** *January 29, 2015 23:51*

Put a hole in two of the bearing mounts, extend two of the shafts, and move the drive pulleys/motors to the outside like HercuLien. Then all robotdigg pullies could be used, as well as the cheaper and smaller closed loop belts from robotdigg.


---
**Isaac Arciaga** *January 30, 2015 07:51*

**+Eric Lien** Thanks! I'll give that a shot in your SpaceClaim model. I ordered the pulley's from RobotDigg. I can always fall back on the Misumi's if it doesn't work out.

﻿

Has anyone done a SyncroMesh version? I would actually spend the money on something like that vs the Misumi pulleys :)


---
**Paul Sieradzki** *January 30, 2015 21:03*

Adafruit has really affordable pulleys. Just note that theoretically GT2 is not the same as 2GT that Misumi carries, so best not to mix and match


---
**James Rivera** *January 30, 2015 21:25*

**+Paul Sieradzki** how are they different?


---
**Paul Sieradzki** *January 30, 2015 21:27*

I'm not actually sure! I was just told by Misumi that they don't recommend mixing and matching.


---
**Isaac Arciaga** *January 30, 2015 21:47*

**+Paul Sieradzki** Thanks for the tip on adafruit! I'll take a look there as well! They accept Bitcoin (I need to spend mine) :)


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/Uaf7VFV8gPx) &mdash; content and formatting may not be reliable*
