---
layout: post
title: "I received my X5 Mini with the screw type terminals"
date: September 17, 2015 17:21
category: "Discussion"
author: Brandon Cramer
---
I received my X5 Mini with the screw type terminals. Now all I get when I turn on the printer is a blue type of screen with no text on my Viki LCD. 



Got any ideas where I might be going wrong? The SD card has two files on it. The config.txt and FIRMEWARE.CUR. 



:(





**Brandon Cramer**

---
---
**Brandon Cramer** *September 17, 2015 17:26*

All of the LED's 1-4 and lit up including the LED by the INT/USB jumper. 


---
**Bryan Weaver** *September 17, 2015 17:39*

Mine did this when I first hooked everything up. The only problem I could find was that my endstop wires were switched and connected to the wrong terminals.


---
**Brandon Cramer** *September 17, 2015 17:50*

I removed the endstop wires entirely and still just have the blue screen. I also unplugged all the stepper motors cables from motors themselves, but still no go. 


---
**Bryan Weaver** *September 17, 2015 18:34*

Do they need to all be connected? I don't know, just taking a stab in the dark.


---
**Brandon Cramer** *September 17, 2015 20:57*

I guess I must have deleted the firmware file off the sd card at some point. It's working now that I added the firmware back onto the sd card. 


---
**rafael lozano** *September 17, 2015 20:58*

Mira primero si la tarjeta la lee la x5, y si el archivo Config está bien configurado para tu lcd


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/UFLsq9oqHYm) &mdash; content and formatting may not be reliable*
