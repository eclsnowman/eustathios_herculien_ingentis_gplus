---
layout: post
title: "Been busy trying to get the Herculien parts online and ready to place the order ..."
date: July 01, 2015 14:37
category: "Discussion"
author: Godwin Bangera
---
Been busy trying to get the Herculien parts online and ready to place the order ... but i came across this doubt on these items :

Timing Belt GT2 Profile - 2mm pitch - 6mm wide 1164mm long ... does this need to be closed (looped/endless) ??

I dont think Robodigg has this particular length or loop?

thanks in advance





**Godwin Bangera**

---
---
**Eric Lien** *July 01, 2015 14:46*

That is the closed belt for Z drive. I know they have it at adafruit also. It needs to be pretty close to that length to tension properly.



[https://www.adafruit.com/products/1184](https://www.adafruit.com/products/1184)



But I know robotdigg does have one very close.


---
**Godwin Bangera** *July 01, 2015 15:06*

thanks Eric, so will get the one from adafruit

thanks


---
**Godwin Bangera** *July 01, 2015 15:07*

If you do remember the robotdigg belt which can be used pls do let me know

thanks


---
**Eric Lien** *July 01, 2015 15:47*

Looks like the closest is 1140mm: [http://www.robotdigg.com/product/270](http://www.robotdigg.com/product/270)



This should work, the idlers will just be spaced further away... but that is fine.


---
**Godwin Bangera** *July 01, 2015 15:54*

thanks a lot Eric .. as always the guardian angel watchin over :)


---
*Imported from [Google+](https://plus.google.com/102290637403942484150/posts/GNySegY8B41) &mdash; content and formatting may not be reliable*
