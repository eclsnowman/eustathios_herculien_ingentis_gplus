---
layout: post
title: "Fix lcd location"
date: June 21, 2016 05:50
category: "Build Logs"
author: Botio Kuo
---
Fix lcd location 

![images/8fac4bdf99dcd808a694add17dc88f70.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8fac4bdf99dcd808a694add17dc88f70.jpeg)



**Botio Kuo**

---
---
**Derek Schuetz** *June 21, 2016 06:08*

Looking good


---
**Botio Kuo** *June 21, 2016 06:09*

**+Derek Schuetz** Thank you, but my night mare will be wire thing .... I'm pretty sure...


---
**Derek Schuetz** *June 21, 2016 06:11*

**+Botio Kuo** ya it is I just redid my k40 and it's a rats nest but everything works perfectly 


---
**Brandon Satterfield** *June 21, 2016 20:11*

Great looking build sir!


---
**Botio Kuo** *June 21, 2016 20:12*

**+Brandon Satterfield** Hey, I haven't got the invoice for the plate. Please sent the invoice to me so I can pay. haha


---
**Brandon Satterfield** *June 21, 2016 20:26*

Haha, totally blanked on that. Your the man for reminding me!!!



I'm sitting here rather upset with myself right now. I'm on about the eighteenth iteration of a DIY printer kit for the site. Every time I think I actually have something new I cruise the internet for something unrelated and realize subconsciously I got the idea somewhere else. I cut your bed, didn't even think about it, about a month ago designed this same bed. I was stealing it from Eric!!! Dang it, so hard to come up with an original idea on a 3D printer. Did the same thing on X using a linear slider, then saw a makergear printer at MRRF.. doh


---
**Botio Kuo** *June 22, 2016 15:36*

**+Brandon Satterfield** haha... I think you just need a vacation for getting out of your desk. You may find inspirations from something funny and stupid. The original idea is not so easy to get, but it could be easier when you had some inspirations. BTW, I paid already. Thank you for your help. :) 


---
**Brandon Satterfield** *June 22, 2016 20:50*

Awesome advice **+Botio Kuo** I also think it's just plain and simple getting older. Used to have a new idea daily, a tweak, something. Possible over exposure, may take a day off soon. Thanks captain. 


---
**Stefano Pagani (Stef_FPV)** *June 29, 2016 00:53*

**+Brandon Satterfield** How much are your beds?


---
**Brandon Satterfield** *June 29, 2016 02:54*

Hey **+Stefano Pagani** hit me up on the site through contact@smw3d.com. Will depend where you are for the shipping, not a standard product but happy to help guys out from the community. 


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/3HMSZGzDhB4) &mdash; content and formatting may not be reliable*
