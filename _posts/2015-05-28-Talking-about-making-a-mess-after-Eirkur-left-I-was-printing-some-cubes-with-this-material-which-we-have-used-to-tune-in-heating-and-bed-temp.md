---
layout: post
title: "Talking about making a mess, after Eirkur left I was printing some cubes with this material which we have used to tune in heating and bed temp"
date: May 28, 2015 19:09
category: "Show and Tell"
author: Gústav K Gústavsson
---
Talking about making a mess, after Eiríkur left I was printing some cubes with this material which we have used to tune in heating and bed temp. Is also printing at more speed. Lowered the bed to 90 degrees. Tried 220 degrees head temp, didn't go well (reel said 230-250) tried 230 and seemed to be Ok until infill it was not solid and was curling up, tuned to 235 and got a lot better. Left for an hour... And this was what the result was when I got back.

![images/f6781b074bbd18d16ef88d6cea611db4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f6781b074bbd18d16ef88d6cea611db4.jpeg)



**Gústav K Gústavsson**

---
---
**Ian Hoff** *May 28, 2015 19:59*

The skirt looks like there was another part that came loose. which could be caused by all kinds of things, like a little over extrusion.



in  #reprap  (irc) I commonly suggest triffid_hunters calibration guide for any tuning issues. Its both general and specific enough to be useful in most cases.



http://reprap.org/wiki/Triffid_Hunter%27s_Calibration_Guide


---
**Eric Lien** *May 28, 2015 21:50*

For PETG I use 255C hot end, 75C bed, on hairsprayed glass (heavy coat). Sticks like crazy to the bed, pops of like magic when cooled.


---
**Eirikur Sigbjörnsson** *May 28, 2015 23:15*

Well lowering the heat on the bed to 90°c (from 110°c) was probably a factor (I wonder who got that idea...). But I still think that if the bed had been wiped over with water soaked tissue just before print things would have gone better. Hairspray is something I still have to try out. Btw, I don't think I will be using the Ultimaker2 to print my stuff for the Herculinen unless I have to. The plan is to use a Cobblebolt Vanguard (if it arrives in July as promised) with dual extrusion and use water soluble material (or HIPS) as support


---
*Imported from [Google+](https://plus.google.com/+GústavKGústavsson/posts/VYg1rffnDdq) &mdash; content and formatting may not be reliable*
