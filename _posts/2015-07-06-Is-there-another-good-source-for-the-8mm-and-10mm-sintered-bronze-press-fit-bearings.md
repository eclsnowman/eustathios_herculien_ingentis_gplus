---
layout: post
title: "Is there another good source for the 8mm and 10mm sintered bronze press-fit bearings?"
date: July 06, 2015 16:02
category: "Discussion"
author: Bryan Weaver
---
Is there another good source for the 8mm and 10mm sintered bronze press-fit bearings?  Or maybe a good alternative?  I'm having a tough time finding anywhere with both 8mm and 10mm in stock.  Plus, SDP/SI's shipping and handling charge for 8 10mm bearings is $17.  I'd really like to avoid spending $80 on 12 bearings if possible...





**Bryan Weaver**

---
---
**Igor Kolesnik** *July 06, 2015 16:30*

Robotdigg has those. 8, 10, 12 in selfgrafite.


---
**Bryan Weaver** *July 06, 2015 17:14*

I assume the STL's will need to be modified a bit to accept those bearings?


---
**Mike Thornbury** *July 06, 2015 17:34*

Igus plastic bushings -less wear, slipperier and cheaper.



'Plastic' isn't really a good descriptive term... They are engineered poly, designed for that exact job.



I've been experimenting with their other bushes and bearings -very impressive. They will send you samples and they have a great 3D printing application guide. Good tech support, as well.



[http://www.igus.com/iglide](http://www.igus.com/iglide)


---
**Bryan Weaver** *July 06, 2015 18:50*

Which iglide bushings specifically would you suggest?  There's about a million different options..


---
**Mike Thornbury** *July 06, 2015 19:39*

What size are your existing ones? Find the replacement size for those. The web site has a comprehensive filter function. Failing that, post a question to their tech support -they are helpful and prompt.


---
**Bryan Weaver** *July 06, 2015 19:58*

Nothing existing, I've just started sourcing parts.  First 2 shipments arriving today!  :D



I was looking for something close to the press-fit bearings listed in the BOM so that I don't have to modify any STLs.  Maybe someone here already has some modified files to accept some alternate bearings...


---
**Mike Thornbury** *July 06, 2015 20:33*

Igus will likely have a compatible part.


---
**Eric Lien** *July 06, 2015 22:10*

The bronze bushings are self aligning (keep that in mind). 



Here are other sources:



[https://www.lulzbot.com/products/8mm-bronze-bushing-pack-4](https://www.lulzbot.com/products/8mm-bronze-bushing-pack-4)



[https://www.lulzbot.com/products/10mm-bronze-bushing-4-pack](https://www.lulzbot.com/products/10mm-bronze-bushing-4-pack)



[http://www.ultibots.com/10mm-bronze-bearing-4-pack-a-7z41mpsb10m/](http://www.ultibots.com/10mm-bronze-bearing-4-pack-a-7z41mpsb10m/)



[http://www.ultibots.com/8mm-bronze-bearing-4-pack-a-7z41mpsb08m/](http://www.ultibots.com/8mm-bronze-bearing-4-pack-a-7z41mpsb08m/)






---
**Eric Lien** *July 06, 2015 22:15*

Also you could use ones like **+Walter Hsiao**​ from robotdigg, but take care, bronze bushings are sensitive to misalignment, so stacking bushings end to end can cause binding at the seam. Using one longer bushing is preferred to two smaller ones.


---
*Imported from [Google+](https://plus.google.com/111820797809026464429/posts/U84twNzGpBK) &mdash; content and formatting may not be reliable*
