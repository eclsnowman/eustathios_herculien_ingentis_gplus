---
layout: post
title: "Help! I accidentally unscrewed my ball screw and I have no Idea how to put it back together!"
date: August 17, 2016 15:56
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Help! I accidentally unscrewed my ball screw and I have no Idea how to put it back together!

![images/f2f608c05a2cee80d086b7aad353ff21.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f2f608c05a2cee80d086b7aad353ff21.jpeg)



**Stefano Pagani (Stef_FPV)**

---
---
**Stefano Pagani (Stef_FPV)** *August 17, 2016 16:28*

Ok, Thanks!


---
**Thomas Sanladerer** *August 17, 2016 17:39*

Oh yeah, getting your balls back in place is an absolute pain. Better get a replacement and tick it off as a lesson learned. 


---
**Oliver Seiler** *August 17, 2016 18:54*

I've almost done the same, a few balls came out. I managed to get them all back in but the screw woudldn't run smoothly after that every now and then, so, yes, get a new one ;)


---
**Samer Najia** *August 17, 2016 20:11*

I was never able to put mine back together when I did this


---
**William Rilk** *August 18, 2016 00:50*

A q-tip with vaseline to pick up the balls.  The vaseline will hold them in the races so you can thread the screw back into the nut.  Tedious, for sure, but far from impossible.  I've had to repack a ballnut on my mill numerous times.  Good luck!


---
**Maxime Favre** *August 18, 2016 13:06*

This thing is usefull for thosewho wants to turn a ball screw:

[http://www.thingiverse.com/thing:809141](http://www.thingiverse.com/thing:809141)


---
**Pieter Koorts** *August 20, 2016 08:23*

+1 for the vaseline/grease method. get them to stick to the rod and pop the outer piece back on slowly.


---
**Stefano Pagani (Stef_FPV)** *August 20, 2016 17:51*

Ok, Thanks for all the replies! I will give it a try but I may have lost a ball... I heard something fall and can't find it. :(


---
**Stefano Pagani (Stef_FPV)** *August 21, 2016 23:23*

Any know where I can buy said ballnut? **+Peter van der Walt** you said I can find one for $8?


---
**Stefano Pagani (Stef_FPV)** *August 24, 2016 00:02*

HOLY CRAP IT DID IT!!111!


---
**Stefano Pagani (Stef_FPV)** *August 24, 2016 00:03*

Just spent a while finding out how it worked, then assembled. Youtube didn't​ help


---
**Stefano Pagani (Stef_FPV)** *August 24, 2016 00:03*

Ballnuts a majestical


---
**Stefano Pagani (Stef_FPV)** *August 24, 2016 00:16*

Oh God. Balls... Balls Everywhere​...


---
**Stefano Pagani (Stef_FPV)** *August 24, 2016 02:49*

**+Ashley Webster** It wasn't feeling right so I took it apart again, spilled them everywhere. I think I'm​ just gonna give up for now. Spent 4 hrs, reassembled about 6 times, it felt pretty sticky every time, I may have lost a ball.


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/44Re9EJCFfr) &mdash; content and formatting may not be reliable*
