---
layout: post
title: "Wow, what a difference adding 10deg C on the hot end makes on the effectiveness of Bowden retracts with PETG"
date: May 10, 2015 14:30
category: "Show and Tell"
author: Eric Lien
---
Wow, what a difference adding 10deg C on the hot end makes on the effectiveness of Bowden retracts with PETG. This Inland brand PETG is so much nicer to print with than the Red MadeSolid PET+

![images/0484225dff0fd3dd9a43dd9cbf7d839a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0484225dff0fd3dd9a43dd9cbf7d839a.jpeg)



**Eric Lien**

---
---
**Isaac Arciaga** *May 10, 2015 22:22*

**+Eric Lien** glad you finally got a chance to try PETG and PET+ out! PET+ is a little trickier on a bowden system because of how much it oozes. It is more rigid than PETG however using less material. Bumping up the temp with both materials fixes most issues. There is also a limitation on how fast you will be able to go. I usually print one part at a time for best results. Too much island hopping for me ends up with build up on the nozzle that results to the hotend hitting the parts so hard that it knocks my glass off to the side. [http://i.imgur.com/1RgpPea.jpg](http://i.imgur.com/1RgpPea.jpg)


---
**Eric Lien** *May 11, 2015 00:03*

**+Isaac Arciaga**​ what temps do you use for pet+



I was using 270c at 35mm/s on Red PET+(which is way slow for me). Inter-Layer adhesion was almost non existent, and I had strings and blobs everywhere.



The PETG above was at 110mm/s at 257C.﻿


---
**Derek Schuetz** *May 11, 2015 00:33*

I still need to try my roll out


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/QNugHpSXEwk) &mdash; content and formatting may not be reliable*
