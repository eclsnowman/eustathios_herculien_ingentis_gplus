---
layout: post
title: "Hi, All set to build the HercuLien, which will be my 5th machine ..."
date: June 04, 2015 11:53
category: "Discussion"
author: Godwin Bangera
---
Hi,

All set to build the HercuLien, which will be my 5th machine ...

I was wondering about the 2080 extrusions, that is it possible to use 2020 x 4 or 2040 x 2 instead ... will this cause a major major road block and is it doable ??

Any advice is greatly appreciated

thanks in advance





**Godwin Bangera**

---
---
**Eric Lien** *June 04, 2015 12:42*

Looks like **+Mikael Sjöberg**​ is using two 20x40, [https://plus.google.com/118217975155696261814/posts/UK8tWCeq4eE](https://plus.google.com/118217975155696261814/posts/UK8tWCeq4eE)


---
**Eric Lien** *June 04, 2015 12:44*

The hard part will be the holes that go through the seam for the drive pulleys and knobs. Likely would have to be milled out, or cut out like a dado.﻿


---
**Godwin Bangera** *June 04, 2015 14:25*

thanks Eric ... i guess i will make a plate connection across the width of the 2020/2040 ext to see that they are clamped together .. thin 20-40 mm wide aluminum 3 - 5mm thick strip's, bolted  down with t-nuts and then cut them down to size 569.5mm and then will put this, with these strips on my CNC machine and drill the holes ... that should solve this issue  i guess .. what do you think ?... (if you did not understand, i will make a sketch and send it) ...



then either leave the support strips or pull them out after the entire assembly ... we can take a call about this later 


---
**Eric Lien** *June 04, 2015 15:15*

**+Godwin Bangera** sounds like a good idea.


---
**Godwin Bangera** *June 04, 2015 15:55*

thanks


---
*Imported from [Google+](https://plus.google.com/102290637403942484150/posts/PQuv56ywL4X) &mdash; content and formatting may not be reliable*
