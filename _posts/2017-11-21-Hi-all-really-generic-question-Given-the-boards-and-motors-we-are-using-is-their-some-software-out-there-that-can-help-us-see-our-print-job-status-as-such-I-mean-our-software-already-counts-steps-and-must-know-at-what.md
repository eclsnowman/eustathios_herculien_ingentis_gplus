---
layout: post
title: "Hi all, really generic question. Given the boards and motors we are using, is their some software out there that can help us see our print job status as such, I mean our software already counts steps and must know at what"
date: November 21, 2017 02:24
category: "Discussion"
author: Gus Montoya
---
Hi all, really generic question. Given the boards and motors we are using, is their some software out there that can help us see our print job status as such, I mean our software already counts steps and must know at what layer height the part is...ugh I wish I never stopped coding....this would be such a nifty thing to have.But yes the question: is their software readily available to incorporate into our current components (regardless of mobo).:  [https://www.3d-printerstore.ch/images/product_images/popup_images/13592_5.jpg](https://www.3d-printerstore.ch/images/product_images/popup_images/13592_5.jpg)





**Gus Montoya**

---
---
**Eric Lien** *November 21, 2017 04:41*

I run all my printers from octoprint. With that there are several push notifications scripts, like Pushbullet:



[https://github.com/OctoPrint/OctoPrint-Pushbullet/blob/master/README.md](https://github.com/OctoPrint/OctoPrint-Pushbullet/blob/master/README.md)


---
**Eric Lien** *November 21, 2017 04:46*

And the Touch UI plugin for octoprint gives great info as well:

 [http://plugins.octoprint.org/plugins/touchui](http://plugins.octoprint.org/plugins/touchui)/






---
**Gus Montoya** *November 26, 2017 23:56*

Thank you! I purchased octoprint, does the job :)


---
**Eric Lien** *November 27, 2017 00:13*

purchased octoprint? You mean a PI3 and SD Card?


---
**Gus Montoya** *November 27, 2017 03:23*

Yes, that's what I meant haha


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/JEdTxNGeVNF) &mdash; content and formatting may not be reliable*
