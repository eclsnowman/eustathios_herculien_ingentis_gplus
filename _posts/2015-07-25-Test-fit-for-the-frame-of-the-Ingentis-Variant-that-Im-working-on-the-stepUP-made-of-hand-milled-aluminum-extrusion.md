---
layout: post
title: "Test fit for the frame of the Ingentis Variant that I'm working on, the stepUP, made of hand-milled aluminum extrusion;"
date: July 25, 2015 04:23
category: "Show and Tell"
author: Ishaan Gov
---
Test fit for the frame of the Ingentis Variant that I'm working on, the stepUP, made of hand-milled aluminum extrusion; still have to find one last extrusion for the top which I misplaced and get some more work done on the rest of the machine 



![images/752da78d7dd46b3fb45a7ff9129a9ce1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/752da78d7dd46b3fb45a7ff9129a9ce1.jpeg)
![images/285dc74b1b251fe588471432c3741730.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/285dc74b1b251fe588471432c3741730.jpeg)
![images/f59a95c80ac5f7c49ab7f4b26a131e13.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f59a95c80ac5f7c49ab7f4b26a131e13.jpeg)

**Ishaan Gov**

---
---
**Eric Lien** *July 25, 2015 04:33*

Very cool. Love the creativity. Cannot wait to see your progress.


---
*Imported from [Google+](https://plus.google.com/113675942849856761371/posts/8TqDtfPqZr4) &mdash; content and formatting may not be reliable*
