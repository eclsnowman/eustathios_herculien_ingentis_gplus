---
layout: post
title: "Just got the call for my glass order"
date: February 03, 2015 16:08
category: "Discussion"
author: Daniel Salinas
---
Just got the call for my glass order.  420mm x 430mm 3/16" Pyrex sheet is ready for pickup.  Damn that was expensive.  But AWESOME!





**Daniel Salinas**

---
---
**Eric Lien** *February 03, 2015 21:13*

How much was it. I used to swear by boro. But after using window glass for 6months I am wondering if the juice is worth the squeeze.


---
**Daniel Salinas** *February 04, 2015 00:46*

$127


---
**Eric Lien** *February 04, 2015 01:44*

Ouch and awesome all at the same time. If you get a second one try these guys: [http://www.voxelfactory.com/products/custom-cut-borosilicate-glass](http://www.voxelfactory.com/products/custom-cut-borosilicate-glass)


---
**Marc McDonald** *February 04, 2015 04:48*

**+Eric Lien** **+Daniel Salinas** VoxelFactory shipped mine quick and cut to size.


---
**Mike Thornbury** *February 04, 2015 06:45*

It's only for printing on... why spend $127 when you could have spent $10 on a mirror tile?



I would rather spend the money where it's useful - like on some BondTech extruders.


---
**Daniel Salinas** *February 04, 2015 07:05*

Yeah heat of the moment basically. If I need another piece I'll shop around. Basically I was just happy to find a local glass source that knew what boro glass was. 


---
**Daniel Salinas** *February 04, 2015 07:09*

Also I'll submit a PR to herculien repo with a link to voxel listed in the BOM 


---
**Daniel Salinas** *February 04, 2015 07:11*

For now I'm going with "only the best for my baby!" Excuse.  Even if it isn't truthful. 


---
**Eric Lien** *February 04, 2015 07:11*

**+Daniel Salinas** sounds good.


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/ZSMwhqD4ydN) &mdash; content and formatting may not be reliable*
