---
layout: post
title: "I have uploaded the model files for a direct drive carriage for the Eustathios"
date: April 15, 2017 13:12
category: "Deviations from Norm"
author: Eric Lien
---
I have uploaded the model files for a direct drive carriage for the Eustathios. Hope you guys can have some fun with it. 



Files are in the "Community Mods and Upgrades" section of the GitHub:

 [https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/Eric%20Lien/Eustathios%20BMG%20Carriage](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/Eric%20Lien/Eustathios%20BMG%20Carriage)

![images/06d28b49b2fb3cac45c601d981a30f1f.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/06d28b49b2fb3cac45c601d981a30f1f.png)



**Eric Lien**

---
---
**Frank “Helmi” Helmschrott** *April 15, 2017 13:23*

Thanks a lot, Eric. Very appreciated.


---
**Ben Delarre** *April 15, 2017 21:34*

Any thoughts on a design with a modular extruder mount? Would be nice to have a standard plate we can mount different extruder setups to. That would save a lot of disassembly to try different designs out. Or are these sort of systems more weight than they are worth?


---
**Eric Lien** *April 15, 2017 21:39*

**+Ben Delarre** this one should be compatible with a Bondtech BMG (my preference after having tested both extensively) and the E3D Titan. Making it more modular would increase the complexity IMHO, but I am totally open to see what people can come up with.


---
**Ben Delarre** *April 15, 2017 21:58*

Yeah I guess that's the tradeoff. I will probably order a BMG soon for my eustathios.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/EbU2oDAPwdH) &mdash; content and formatting may not be reliable*
