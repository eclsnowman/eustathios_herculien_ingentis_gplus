---
layout: post
title: "Test fitting the prototype On-Carriage-Direct-Drive modular extruder for ."
date: August 27, 2014 03:23
category: "Deviations from Norm"
author: Eric Lien
---
Test fitting the prototype On-Carriage-Direct-Drive modular extruder for #HercuLien. I know it's ugly and I have still not figured out my on-carriage wire management scheme. But it should be perfectly functional and open up #Herculien for additional filaments not ideal for Bowden applications.



It is completely modular so I pull off the dual Bowden tube retention head normally on the carriage via qty:2 x 3mm bolts. Bolt on the direct drive which uses two bolts and a pin-in-hole support lug. And attach the stepper jumper wire harness where it normally connects to the Bowden extruder. 



The only real pain is having to have two different marlin configs to handle the different steps/mm of the direct drive versus my normal 5.18/1 gear reduced Bowden drive. That is unless there is another easy way to implement that via the lcd panel. I run from octoprint usually so it is actually a pain to load firmware changes because it involves opening my control box behind the LCD and unhooking USB from the Pi to my desktop PC. ﻿



![images/8409825f91fbc3a4fac4bae7b1846b9f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8409825f91fbc3a4fac4bae7b1846b9f.jpeg)
![images/426a6b024c2cbf5216842a850544e090.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/426a6b024c2cbf5216842a850544e090.jpeg)
![images/e2c3f573e7915747b32b6dd2e3edadd5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e2c3f573e7915747b32b6dd2e3edadd5.jpeg)
![images/c93091fae9e0193ba6a59e09a0da7d6c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c93091fae9e0193ba6a59e09a0da7d6c.jpeg)
![images/2cf4405f110cf6be3ae9884d230fbe8e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2cf4405f110cf6be3ae9884d230fbe8e.jpeg)
![images/1d5c969ad0ec4e5697227f1c7ba2845f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1d5c969ad0ec4e5697227f1c7ba2845f.jpeg)
![images/e62c6ac7027cf1ebf6550a9c125bc709.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e62c6ac7027cf1ebf6550a9c125bc709.jpeg)

**Eric Lien**

---
---
**Brian Wilson** *August 27, 2014 03:35*

There are people who know this better than I do, but I am pretty sure you can load firmware via the terminal on the rPi to the arduino.  I think you save the hex files for each firmware configuration to the rPi and then use the avr commands to load the firmware.  To simplify it even more, you could set each command (which from what I recall is sort of long) to an alias on the terminal.


---
**Eric Lien** *August 27, 2014 03:47*

**+Brian Wilson** thanks. I will have to look into that.


---
**Wayne Friedt** *August 27, 2014 04:01*

It may be possible to do in S3d but adding the steps to the starting script. Can make that a different process.


---
**Eric Lien** *August 27, 2014 04:16*

**+Wayne Friedt** another good suggestion. Thanks.


---
**Martin Bondéus** *August 27, 2014 04:56*

Cant you use m92 exxxx, and then m500 


---
**James Rivera** *August 27, 2014 05:44*

I agree with what **+Martin Bondéus** said. Just use g-code to store the settings in EEPROM each time you switch the extruder.


---
**James Rivera** *August 27, 2014 05:47*

More info here: [http://reprap.org/wiki/G-code](http://reprap.org/wiki/G-code)


---
**Miguel Sánchez** *August 27, 2014 07:56*

As **+Martin Bondéus**  suggest you can change the steps/mm for the extruder on the fly via g-code. What I would do, however, is to add it to the beginning code of the  slicer program. This way you can change the printer settings on slic3r and getting the correct settings set for you before printing. No need to store them on EEPROM (M500) unless you want to use previously sliced code. 



OTOH, you may want to reduce max acceleration on X&Y when adding the extruder. You can do that with M201 and even you can change max feed rate with M203. Again I would do that as part of the "begin" custom code.


---
**Jean-Francois Couture** *August 27, 2014 12:11*

I think that M201 & M203 can be changed on the LCD panel. Remember that if you dont change the M92 Exxx (in the console window if there is one in octo print) before manually extruding, you will not get the exact amount exiting the hotend.. the above suggestions are for when the print starts.. not in manual control :-)


---
**Brandon Satterfield** *August 27, 2014 13:13*

Love that the mass is directly the extruder, no moment arm should let you really fly! 


---
**Jean-Francois Couture** *August 27, 2014 15:15*

**+Eric Lien** Whats the name / place your got that red ABS, really looks good.. I have to order more plastic and I want to add a spool like that


---
**Joe Spanier** *August 27, 2014 15:18*

You could built the M92 command into a fff process in simplify. This would by far be the easiest. This is how I deal with swapping to the flexystruder on the lulzbots at work. 


---
**Eric Lien** *August 27, 2014 16:33*

**+Jean-Francois Couture** pushplastic. I love their materials and the price.


---
**James Rivera** *August 27, 2014 17:49*

**+Eric Lien** Regarding settings, each time you swap/modify the extruder you are making a hardware change.



So from the perspective of which layer of abstraction to change, it seems better to make the corresponding change in firmware (i.e. - store the settings in EEPROM) at the same time.



The additional benefit is this means you can keep the g-code files the same and not have to edit them if/when you want to change the extruder head (which, I suspect, would not happen every day).



Basically, I think it would be a lot easier  to remember to save the new settings to EEPROM when you change the hardware than it would be to remember to edit each and every g-code file after doing so.  My $.02.


---
**Joe Spanier** *August 27, 2014 18:24*

**+Eric Lien** have you had moisture issues with Push? I've had about 7 rolls of PLA and ABS and all the ABS both premium and normal was super wet. Lots of bubbles. After baking though its gorgeous.


---
**Eric Lien** *August 27, 2014 18:29*

I keep mine in a sealed Rubbermaid tote with a 750G silica gel can. So for me moisture has never been an issue.


---
**Joe Spanier** *August 27, 2014 18:34*

this has been straight out of the bag. Open the box cut the bag print, wet filament.


---
**Eric Lien** *September 26, 2014 18:54*

Not yet. Waiting for some spare $ to buy ninjaflex to try it. For abs I see no need for direct drive extruder motor. Just extra weight.


---
**Eric Lien** *September 26, 2014 19:08*

Right now I run between 5 on abs and 4.5 on Pla.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/Y4V8sHcjmgu) &mdash; content and formatting may not be reliable*
