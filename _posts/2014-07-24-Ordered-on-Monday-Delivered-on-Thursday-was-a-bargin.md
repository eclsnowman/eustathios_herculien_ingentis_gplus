---
layout: post
title: "Ordered on Monday Delivered on Thursday! was a bargin!"
date: July 24, 2014 20:57
category: "Show and Tell"
author: Jim Squirrel
---
Ordered on Monday Delivered on Thursday!  [trimcraftaviationrc.com](http://trimcraftaviationrc.com) was a bargin! Thanks **+Brian Bland** for telling me about them. 

![images/9171ede6b6fb47063b6c2d3f6f3ea785.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9171ede6b6fb47063b6c2d3f6f3ea785.jpeg)



**Jim Squirrel**

---
---
**Chet Wyatt** *July 24, 2014 22:08*

Wow, you guys are ordering brass... good thing this isn't an addiction ;) Bling!


---
**Brian Bland** *July 24, 2014 23:02*

They are Stainless Steel.


---
**Jim Squirrel** *July 25, 2014 00:27*

Stupid google photo enhance made it look like brass


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/1qRwBjvdwpX) &mdash; content and formatting may not be reliable*
