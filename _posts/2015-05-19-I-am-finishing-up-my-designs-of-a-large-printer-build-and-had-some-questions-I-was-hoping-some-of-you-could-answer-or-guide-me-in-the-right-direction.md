---
layout: post
title: "I am finishing up my designs of a large printer build and had some questions, I was hoping some of you could answer or guide me in the right direction"
date: May 19, 2015 19:24
category: "Discussion"
author: Russell Hummerick
---
I am finishing up my designs of a large printer build and had some questions, I was hoping some of you could answer or guide me in the right direction.



The big question is on the controller. I currently have a PCduino that was planned for another project but was thinking of trying to have this be the controller as it can use Arduino Motor shields and be its own stand alone computer.



Has anyone used the PCduino for this or know someone that has?





**Russell Hummerick**

---
---
**Eric Lien** *May 20, 2015 17:29*

To be honest I have never seen one used. But I would be excited to see you use one. Variety is the spice of life.


---
**Russell Hummerick** *May 20, 2015 22:04*

I have been doing trying to do some research.  I am seeing people using a Arduino Mega as the controller. Is it possible to have a Arduino Uno be the controller? if the Uno can be a controller then I shouldn't have a problem getting the PCduino to control it as it has the same pins 


---
*Imported from [Google+](https://plus.google.com/100843029189377920683/posts/dYhvYoJb5xK) &mdash; content and formatting may not be reliable*
