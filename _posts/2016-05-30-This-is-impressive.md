---
layout: post
title: "This is impressive"
date: May 30, 2016 17:53
category: "Discussion"
author: Sean B
---
This is impressive.





**Sean B**

---
---
**Carlton Dodd** *May 30, 2016 18:01*

So, the idea is that the splice gets to the nozzle at exactly the right time to change color.  Um, sure.  We all have that kind of precision in our extruders and hot ends, right?


---
**Carlton Dodd** *May 31, 2016 02:43*

**+Nathan Walkner** For some of the models they showed?  Very precise.

It's a cool idea.  I just think it's still a young idea, and needs more refinement.


---
**Carlton Dodd** *May 31, 2016 03:04*

**+Nathan Walkner**

Absolutely.  But at $1000, it's going to be tough to get any traction.

Don't get me wrong; I'm all for this idea.  I just think they're hyping it a bit much. 

Personally, I'd like to see it open-sourced, and see it perfected that way.  Maybe even get some sort of feedback from the printer.  Then, these guys would have a much better product to sell.


---
**Jeff DeMaagd** *May 31, 2016 04:18*

Their Kickstarter promised they would open source the software, but I don't know where to look.


---
**Ryan Carlyle** *May 31, 2016 21:04*

It's got an encoder you install near the nozzle to fine-tune the filament lengths and color transitions. Bigger issue is actually the color change time of the melt zone in the extruder. Varies wildly by hot end -- could be a few inches or a few feet of extruded strand. They currently recommend a purge tower. It's in the hands of hobbyists and being rapidly fine-tuned now. Getting four-color prints from one nozzle is pretty sexy. 


---
**Ryan Carlyle** *May 31, 2016 21:05*

Write up and results by Jetguy: [https://groups.google.com/forum/?nomobile=true#!topic/3dprintertipstricksreviews/BESyPC3voRo](https://groups.google.com/forum/?nomobile=true#!topic/3dprintertipstricksreviews/BESyPC3voRo)


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/16kWFffXydo) &mdash; content and formatting may not be reliable*
