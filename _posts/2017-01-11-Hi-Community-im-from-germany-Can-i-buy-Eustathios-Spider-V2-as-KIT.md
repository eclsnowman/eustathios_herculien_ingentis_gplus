---
layout: post
title: "Hi Community, i'm from germany. Can i buy Eustathios Spider V2 as KIT?"
date: January 11, 2017 16:42
category: "Discussion"
author: Batbold Jamts
---
Hi Community,



i'm from germany. Can i buy Eustathios Spider V2 as KIT?

I am very impressed by the printer.



Thank you





**Batbold Jamts**

---
---
**Eric Lien** *January 11, 2017 23:26*

There are no full kits available (it would really cool if a company put one together). But all the source files are available, and the BOM if pretty detailed for sourcing the parts yourself.


---
**Eric Lien** *January 11, 2017 23:31*

There is an Extrusion kit available from **+Makeralot**​ ( [https://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207](https://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207)/ )

[makeralot.com - 20mm x 20mm Extruded Aluminum for Eustathios 3D Printer : MAKERALOT, Maker Tools and Materials](https://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207/%29.)


---
**Daniel F** *January 15, 2017 17:31*

For the frame there is a local supplier in  D called motedis, I used their 2020 type B profile for my Eustathios, you can order pices cut to lenght but you need to drill holes and cut threads yourself. Dold mechatronic also has some mechanical parts like 8 and 10mm colomns (Wellen).


---
**Batbold Jamts** *January 23, 2017 10:21*

**+Daniel F**

Hallo Daniel,



vielen Dank für die Info. Hast du evt. eine Liste, wo du die Teile in Deutschland gekauft hast.



Vielen Dank für die Antwort im Voraus.



Grüße Bat


---
*Imported from [Google+](https://plus.google.com/104254407031666307204/posts/PsGhn1L4cbF) &mdash; content and formatting may not be reliable*
