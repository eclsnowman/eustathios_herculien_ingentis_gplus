---
layout: post
title: "It begins to take form"
date: February 17, 2015 08:18
category: "Show and Tell"
author: Derek Schuetz
---
It begins to take form

![images/a29d40ac19c332094bfea6041ab74c40.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a29d40ac19c332094bfea6041ab74c40.jpeg)



**Derek Schuetz**

---
---
**Mikael Sjöberg** *February 17, 2015 08:33*

Nice work! My frame is in cargo now, Looking forward to follow your progress and to get started with mine!


---
**Eric Lien** *February 17, 2015 14:00*

Looking good.


---
**Daniel Salinas** *February 17, 2015 14:28*

looks awesome **+Derek Schuetz** your parts are packed along with the remaining spool of abs and will be on their way to you today.


---
**Seth Messer** *February 18, 2015 14:45*

love that so many of you guys are paying it forward like this! **+Derek Schuetz** that looks great so far.


---
**Daniel Salinas** *February 18, 2015 16:13*

I have a demo e-nable hand to print next but after that my robo3d will be freed up to print a parts set for the next person who needs it.


---
**Daniel Salinas** *February 18, 2015 16:14*

In the beginning I was going to build an ingentis so I've got some experience printing those parts as I had already printed about half a set in PLA just to figure it out so if anyone is looking to build one I can get those whipped up pretty quick.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/TEoTr56ndRL) &mdash; content and formatting may not be reliable*
