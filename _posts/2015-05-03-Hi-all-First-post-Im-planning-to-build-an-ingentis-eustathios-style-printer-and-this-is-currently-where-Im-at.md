---
layout: post
title: "Hi all, First post. I'm planning to build an ingentis/eustathios style printer and this is currently where I'm at"
date: May 03, 2015 03:50
category: "Deviations from Norm"
author: Luke Strong
---
Hi all,



First post. I'm planning to build an ingentis/eustathios style printer and this is currently where I'm at. Like the idea of using seperate rods for linear/rotational movement and using linear bearings. Would also like to look into using Mutley3D's flex3drive extruder on it. Planning to go with a belted Z with a worm drive.



Would welcome some constructive feedback. It is abit of a web of rods.



Cheers

Luke

![images/e8091acc9d7b531ccb867667af6fafbb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e8091acc9d7b531ccb867667af6fafbb.jpeg)



**Luke Strong**

---
---
**Jeff DeMaagd** *May 03, 2015 04:16*

Where are you getting those corner vertical post extrusions? I've never seen that kind before.


---
**Erik Scott** *May 03, 2015 05:05*

It's interesting you've decided to separate the linear and rotational rods. I think the rods pulling double-duty was one of the most ingenious things about the ultimaker-style kinematics. If you do go through with this, please keep us updated. One thing I foresee when I look at this is trouble getting all the rods properly aligned. You've got 8 of them now, and they're all held by individual pieces. If you were to make a combined holder for the rotating and stationary rods, that would help a lot.


---
**Luke Strong** *May 03, 2015 07:42*

**+Jeff DeMaagd** It's Misumi L shaped extrusion. [http://us.misumi-ec.com/vona2/detail/110302685320/?Inch=0](http://us.misumi-ec.com/vona2/detail/110302685320/?Inch=0)



**+Erik Scott** I got the idea from the "Homefac" ([https://www.youmagine.com/designs/homefac-3d-printer-reprap-style](https://www.youmagine.com/designs/homefac-3d-printer-reprap-style)).

My ideas behind it:

* Allows the use of linear bearings which also means that the xy end pieces can be more easily removed without having to take the outer shafts off.

* Can use 10mm for the linear shaft and 8mm with a 20T pulley for the rotational shaft, which should give more than enough resolution with a direct drive approach.

* I don't imagine that the rotational shaft will be very alignment sensitive because they're only used for the belts, but just my guess.

* Can use a machined and cheaply available SK10 shaft support so should only have to worry about vertical alignment provided the frame is square.


---
**Tim Rastall** *May 03, 2015 19:57*

I like the shaped extrusions as a means of providing enough space to mount the support and drive shafts in close proximity. Also,  you may want to have a look at my feed to see the same idea already built :) 


---
*Imported from [Google+](https://plus.google.com/106147943249970698774/posts/FcnbT5ChrK7) &mdash; content and formatting may not be reliable*
