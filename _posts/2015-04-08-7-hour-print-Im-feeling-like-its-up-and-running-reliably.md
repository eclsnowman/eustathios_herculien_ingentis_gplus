---
layout: post
title: "7 hour print I'm feeling like its up and running reliably"
date: April 08, 2015 16:02
category: "Show and Tell"
author: Derek Schuetz
---
7 hour print I'm feeling like its up and running reliably 

![images/80436dbed74d071366dfa94e7c2b569b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/80436dbed74d071366dfa94e7c2b569b.jpeg)



**Derek Schuetz**

---
---
**Carlton Dodd** *April 08, 2015 16:39*

I like that model. Props for the MtG swag in the background, too.


---
**Mike Miller** *April 08, 2015 16:50*

Don't Jinx it, man! I feel like my printer is good for pretty much unlimited printing, then find the filament varies enough to fail the print. 


---
**Derek Schuetz** *April 08, 2015 16:53*

So much mtg swag in my office...currently 1 of my 4 desks is full of cards


---
**Eric Lien** *April 08, 2015 17:00*

What is it?


---
**Derek Schuetz** *April 08, 2015 17:30*

**+Eric Lien** [http://www.thingiverse.com/thing:734380](http://www.thingiverse.com/thing:734380) gonna paint it


---
**Carlton Dodd** *April 08, 2015 22:59*

I didn't realize that was Zim!  I loved that show!  (esp. Gir)


---
**Gus Montoya** *April 14, 2015 16:52*

Oh wow, that looks like a nice print. You actually don't live too far from me. Only about 1.5 hr away (I'm in San Diego). Could I interest you in printing V2 Eustathios parts for me? It's all I am missing for my build. I can pay you if you so require it. :)


---
**Derek Schuetz** *April 14, 2015 21:44*

**+Gus Montoya** I can arrange printing you the parts if you want to supply material. My glass break just cracked though because of the force of my parts being stuck to it and then cooling


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/beKD8an5XkA) &mdash; content and formatting may not be reliable*
