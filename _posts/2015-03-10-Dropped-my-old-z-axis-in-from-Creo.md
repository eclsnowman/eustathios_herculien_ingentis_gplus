---
layout: post
title: "Dropped my old z axis in from Creo"
date: March 10, 2015 17:46
category: "Deviations from Norm"
author: Joe Spanier
---

{% include youtubePlayer.html id=MlalQhaU0C8 %}
[https://www.youtube.com/watch?v=MlalQhaU0C8&feature=youtu.be](https://www.youtube.com/watch?v=MlalQhaU0C8&feature=youtu.be)



Dropped my old z axis in from Creo. Really enjoying fusion. Was also able to pull some of Eric's excellent solidworks models in for things like pulleys and stuff I dont want to re-model. 





**Joe Spanier**

---
---
**Eric Lien** *March 10, 2015 21:52*

Very cool.


---
*Imported from [Google+](https://plus.google.com/+JoeSpanier/posts/f2RByWKdPKX) &mdash; content and formatting may not be reliable*
