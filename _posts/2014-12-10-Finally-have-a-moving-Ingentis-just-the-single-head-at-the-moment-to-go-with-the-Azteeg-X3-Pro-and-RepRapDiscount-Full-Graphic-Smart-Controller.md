---
layout: post
title: "Finally have a moving Ingentis, just the single head at the moment to go with the Azteeg X3 Pro and RepRapDiscount Full Graphic Smart Controller"
date: December 10, 2014 00:59
category: "Show and Tell"
author: David Heddle
---
Finally have a moving Ingentis, just the single head at the moment to go with the Azteeg X3 Pro and RepRapDiscount Full Graphic Smart Controller. 



What feedrate, acceleration and jerk are people using for the z axis?


**Video content missing for image https://lh5.googleusercontent.com/-mxWcEpSP23k/VIeU3DCM9BI/AAAAAAAAGrM/Yr0gyyiwMNU/s0/20141209_213215.mp4.gif**
![images/c3ca90e944cb380a2377927be876801f.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c3ca90e944cb380a2377927be876801f.gif)



**David Heddle**

---
---
**Eric Lien** *December 10, 2014 01:54*

Great motion. Looks smooth. If you want to see my marlin config on Eustathios look here: [https://github.com/eclsnowman/Lien3D_Eustathios_Spider/blob/master/Marlin%20Firmware/Marlin-Marlin_v1/Marlin/Configuration.h](https://github.com/eclsnowman/Lien3D_Eustathios_Spider/blob/master/Marlin%20Firmware/Marlin-Marlin_v1/Marlin/Configuration.h)


---
**Ethan Hall** *December 10, 2014 02:54*

Can't wait till I get mine built and functioning!


---
**Tim Rastall** *December 10, 2014 05:02*

Super awesome.  I get a real kick out of seeing these :D


---
**Eric Lien** *December 10, 2014 05:17*

**+Tim Rastall**​ it's like the holidays, you get time to see all the relatives... to Ingentis.﻿


---
*Imported from [Google+](https://plus.google.com/+DavidHeddle/posts/VdxuPqjnxfu) &mdash; content and formatting may not be reliable*
