---
layout: post
title: "Two heatbed heatzones in synch ? So Im building a sort of Herculien (z-axis) / Core XY printer (with linear Rails on the x axis ), basically for the need of a 20 x 80 cm printbed"
date: September 11, 2016 15:40
category: "Discussion"
author: Jo Miller
---
Two heatbed heatzones in synch ?



So I´m building a sort of Herculien (z-axis) /  Core XY  printer (with linear Rails on the x axis ),  basically  for the need of a  20 x 80 cm printbed. (special project)

 but before ordering the   silicon-heater in china. it occurs to me that on such a lenght I´d be better off with two thermistors  placed on 1/3 and 2/3 of  the overal lenght.

OR using two 20 x 40 silicon headbeds   however ... using two silicon headbeds.. do I still  use one DC-AC SSR to heat them both up ? in parralel ? in serial ?       I did recognize a third <s>free</s> thermistor input on the Ramps-board    however  does the Marlin or Repetier sofware allows to use two heatbed heatzones in synch ?  (instead of dual printheads )





Its unknown territory for me , I´m not a electricity/electronic specialist.





**Jo Miller**

---
---
**Arthur Wolf** *September 11, 2016 15:53*

Why would you need two thermistors ? It's not like the two heaters are going to be doing anything different, or like they'll be under different circumstances. This is the same as a 20x20 bed, just bigger. 

Smoothie supports this sort of dual bed setup but I really think it's not actually needed.


---
**Stefano Pagani (Stef_FPV)** *September 11, 2016 20:00*

If you  print small things most of the time I would consider using 2 heat beds just for efficiency (with separate SSR's), but I think just one with one thermistor would be totally fine.




---
**Jo Miller** *September 12, 2016 08:41*

Thanks Arthur & Stefano



well I was considering this due to lenghty size of this heatbed.  the size of a 20 x 80 might show even stronger cold patches  in the corners. I had the impression that a silicon-internal heat-wiring in general    shows some temperature-drop from the center towards the edges. and with this odd-shape..



I guess I´will go for a smoothieboard for this dual bed setup.


---
*Imported from [Google+](https://plus.google.com/103341289176473342280/posts/SYu7kk14JCH) &mdash; content and formatting may not be reliable*
