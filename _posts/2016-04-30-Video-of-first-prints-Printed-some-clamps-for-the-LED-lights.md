---
layout: post
title: "Video of first prints Printed some clamps for the LED lights"
date: April 30, 2016 23:16
category: "Show and Tell"
author: Daniel F
---
Video of first prints

Printed some clamps for the LED lights



![images/9b644acee9505bdb5ffda6897dd5965f.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9b644acee9505bdb5ffda6897dd5965f.gif)
![images/099ecd45d54dcfc4955c227a9ef8c797.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/099ecd45d54dcfc4955c227a9ef8c797.gif)
![images/dc8383e773e907b3b0fd3a6afa9d16ca.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dc8383e773e907b3b0fd3a6afa9d16ca.gif)

**Daniel F**

---


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/BdWtCrH3GaB) &mdash; content and formatting may not be reliable*
