---
layout: post
title: "Got it all wired up. Its moving really nicely"
date: March 21, 2015 18:17
category: "Show and Tell"
author: Daniel Salinas
---
Got it all wired up. Its moving really nicely. Need to get the extruders hooked up tonight. **+Eric Lien**​ wewt!!!


**Video content missing for image https://lh3.googleusercontent.com/-No77mZCf1GQ/VQ21m9Yta6I/AAAAAAAATV8/9yU13xUoouk/s0/VID_20150321_124934.mp4.gif**
![images/98c01c0756a55ac1325d4b2a2deaf87f.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/98c01c0756a55ac1325d4b2a2deaf87f.gif)



**Daniel Salinas**

---
---
**Derek Schuetz** *March 21, 2015 20:11*

So jealous 


---
**Daniel Salinas** *March 21, 2015 20:13*

I'm building the 2 Bondtech V2 extruders now.  :-)


---
**Seth Messer** *March 21, 2015 21:29*

i echo what **+Derek Schuetz** said. :) congrats **+Daniel Salinas** that bad boy is coming along quite nicely.


---
**Eric Lien** *March 21, 2015 22:56*

I feel like a proud papa! Excellent work and love the black and red color scheme.


---
**Gus Montoya** *March 21, 2015 23:11*

Awesome, may I step on a pinky toe and say yours looks better then Eric Liens LOL


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/6radLymE3zb) &mdash; content and formatting may not be reliable*
