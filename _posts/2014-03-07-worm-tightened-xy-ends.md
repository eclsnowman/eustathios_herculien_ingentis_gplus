---
layout: post
title: "worm tightened xy ends"
date: March 07, 2014 05:01
category: "Show and Tell"
author: D Rob
---
worm tightened xy ends



![images/5a0b65be023985353a3f4cb8f043fa33.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5a0b65be023985353a3f4cb8f043fa33.jpeg)
![images/25cf58559e1f3eaa7039435d787e2d50.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/25cf58559e1f3eaa7039435d787e2d50.jpeg)
![images/298214f36d68f87cae1aee7975f5d772.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/298214f36d68f87cae1aee7975f5d772.jpeg)
![images/f087dd46e5e5a9e341970034e7380335.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f087dd46e5e5a9e341970034e7380335.jpeg)
![images/eec681cf603c322b21328bd1e439f49e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eec681cf603c322b21328bd1e439f49e.jpeg)
![images/c6b0bca74f5144ddd104043cdff1f6a8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c6b0bca74f5144ddd104043cdff1f6a8.jpeg)
![images/f921865bd314c609b52c664cfb399680.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f921865bd314c609b52c664cfb399680.jpeg)
![images/2c3c36af880d8d09a64df85f76e8b976.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2c3c36af880d8d09a64df85f76e8b976.jpeg)
![images/c71f4765b7f31a21bb5008dcf90b2d61.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c71f4765b7f31a21bb5008dcf90b2d61.jpeg)

**D Rob**

---
---
**Dale Dunn** *March 07, 2014 13:35*

I've been thinking about using an extension spring to tension cables. As in: cable> extension spring> XY end. The trick would be knowing how much tension is required in the spring, I suppose.


---
**D Rob** *March 07, 2014 14:43*

yes too little would play hell with backlash during acceleration. and maybe defeat any purpose of being there. too long and the spring will also eat into the build envelope to outer footprint ratio


---
**Dale Dunn** *March 07, 2014 16:59*

It sounds like you thought about it too.


---
**D Rob** *March 07, 2014 18:23*

**+Dale Dunn** I dont sleep much. ADD and all.


---
**Thomas Sanladerer** *March 07, 2014 23:16*

Hah, I bought a set of those exact same pegs for the exact same application just a day ago. Seems like you beat me to it, **+D Rob**. 

**+Dale Dunn** adding a spring without a rigid motion limiter will always result in added elasticity in the linear drive. But again, having that hard limit in place kinda defeats the purpose of using a spring in the first place, so a rigid tensioner, like pegs, really is the best way to do it. ﻿


---
**D Rob** *March 08, 2014 02:33*

I made those about 4 months ago


---
**D Rob** *March 09, 2014 07:03*

**+Thomas Sanladerer** I have mad a few iterations of these and I can send you a coy of the current revision if you want. also the rest of my work


---
**Thomas Sanladerer** *March 09, 2014 13:37*

**+D Rob** thnaks, but i've settled for a CoreXY-based design for now, which means that i'll have to figure out a way to tension things on the main carriage.


---
**D Rob** *March 09, 2014 17:57*

Best of luck on your build


---
**Thomas Sanladerer** *March 09, 2014 20:03*

Thanks! Good luck with yours as well. 


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/Guk2Nimbk8o) &mdash; content and formatting may not be reliable*
