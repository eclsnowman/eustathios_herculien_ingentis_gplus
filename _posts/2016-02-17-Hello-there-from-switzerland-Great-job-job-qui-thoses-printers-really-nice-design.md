---
layout: post
title: "Hello there from switzerland, Great job job qui thoses printers, really nice design!"
date: February 17, 2016 19:38
category: "Discussion"
author: Maxime Favre
---
Hello there from switzerland,

Great job job qui thoses printers, really nice design!



I'm beginning to source the parts and I have questions regarding bronze bearings vs the bushing + sleeve designed by **+Walter Hsiao**​ :



Any advantage exept size ? Alignement ?

And about those flex sleeves, do you have any direction regarding dimensions ? The system looks time consuming, does it worth it ?



Thanks !﻿





**Maxime Favre**

---
---
**Walter Hsiao** *February 17, 2016 22:30*

I've tried them both out now, and tend to prefer the self aligning bushings over the graphite ones  With the self aligning bushings, you can adjust out any play between the bushing and rod and they're more compact.  Main advantage of the graphite bushings is they're a fraction of the cost and easy to reuse.  



I forgot the dimensions of the sleeves, but it took some trial and error to get a good fit. They're  printed in vase mode so adjusting the thickness can be done by changing the extrusion width (might need a larger than normal nozzle).



I'd probably use the graphite bushings again for parts that are the same width as the bushing, for anything larger, the self aligning ones work much better.


---
**Maxime Favre** *February 18, 2016 09:19*

Great! thanks ! I'll go for the aligning ones.


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/MoL9GDNHfKC) &mdash; content and formatting may not be reliable*
