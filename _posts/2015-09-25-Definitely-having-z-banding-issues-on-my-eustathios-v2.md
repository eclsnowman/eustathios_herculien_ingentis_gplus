---
layout: post
title: "Definitely having z-banding issues on my eustathios v2"
date: September 25, 2015 16:29
category: "Discussion"
author: Ben Delarre
---
Definitely having z-banding issues on my eustathios v2.



I spent some time last weekend dialling in the extruder settings which I thought were causing overextrusion and the banding seen, I've now dialled the extruder in just about perfectly (see the test shape in the later photos). But the z-banding remains, its too regular and matches the leadscrew pitch perfectly, so I don't think I can put it down to extrusion.



Anyone else having banding issues on the v2? I'm wondering if the pair of bearings at the bottom is over constraining the screw? I don't really want to have to resort to z isolators but I'm not sure how to go about fixing this. Any suggestions?



![images/22801391b64dad047d087e67a5fd7d1c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/22801391b64dad047d087e67a5fd7d1c.jpeg)
![images/fe8b98691b08c132605bf50742658ab2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fe8b98691b08c132605bf50742658ab2.jpeg)
![images/2e4cd7837516576014850165db17c6cf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2e4cd7837516576014850165db17c6cf.jpeg)
![images/a790480618be794b2e30d28b4df7cad0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a790480618be794b2e30d28b4df7cad0.jpeg)
![images/04af63db98ed3e7c03276c3dfdfe6c55.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/04af63db98ed3e7c03276c3dfdfe6c55.jpeg)

**Ben Delarre**

---
---
**Miguel Sánchez** *September 25, 2015 16:37*

some ideas [http://www.soliforum.com/topic/1165/firmware-z-wobble-compensation/](http://www.soliforum.com/topic/1165/firmware-z-wobble-compensation/)


---
**Ben Delarre** *September 25, 2015 16:39*

Interesting discussion. I'm on smoothieware though so I'm not sure I can apply that hack easily. Also I think I'd rather solve this mechanically if I can since I've seen other people with the v2 having great results so its probably just something I've setup badly :-)


---
**Ben Delarre** *September 25, 2015 16:41*

Just as an additional note, the photos of the calibration object make it look like there's no banding present, there is, but it is substantially reduced. Making me wonder if its an infill issue perhaps, or would the infill just help exaggerate any wobble in the z axis?


---
**Miguel Sánchez** *September 25, 2015 16:41*

my first thought is that one of your threaded rods could be bent slightly and it is pushing the bed. a second possible cause would be not xy motion but uneven z-height on each layer (can't tell for sure from pictures), The latter would be cause by a layer height which is not represented by a full step.


---
**Ben Delarre** *September 25, 2015 16:43*

Tested the rods on my table top recently (which admittedly is probably not perfectly flat, but is as flat as I can find right now). Both rods are perfectly straight as far as I can tell. I'm also using the 0.1920 layer height to get full steps, so we can rule that one out too. Thanks for the suggestions though!


---
**Miguel Sánchez** *September 25, 2015 16:43*

Lastly it could be the heated bed temperature swinging significantly


---
**Ben Delarre** *September 25, 2015 16:44*

No heated bed either, this is on an acrylic bed with a mirror tile on top for flatness. So no temperature fluctuations there, could be hot end fluctuations I suppose but the regularity of it and its periodicity matching the z-screw perfectly points to the mechanical I think.


---
**Miguel Sánchez** *September 25, 2015 16:46*

**+Ben Delarre** yep, if period matches rods' pitch I bet that is the source of the problem even if visually it is not noticeable (if you have a dial indicator it may help you looking for side motion on the bed due to the z-axis motion)


---
**Ben Delarre** *September 25, 2015 16:49*

Yeah I'm going to have to rig up something to get the dial indicator to run along the edge of the bed this weekend and check it to confirm. Just don't know what to do about it. The smooth rods are straight and the leadscrews are straight. The leadscrews are in the bearings at the bottom block, and unconstrained at the top. As I understand the mechanics of z wobble if you don't over constrain the leadscrew you shouldn't be transferring much horizontal force into the bed but this is clearly doing that.



Could it be that the bottom bearing blocks are not holding the z axis on one side straight causing this wobble, even though I have smooth travel up and down the whole axis? I honestly don't know where to start in fixing this.


---
**Miguel Sánchez** *September 25, 2015 16:51*

Imperfect motor shaft alignment could definitely cause the problem.


---
**Ben Delarre** *September 25, 2015 16:52*

The z-axis is driven by belts on the eustathios v2, so its not that, this is what confuses me about it. The design is pretty sound in terms of avoiding all the normal pitfalls.


---
**Miguel Sánchez** *September 25, 2015 16:53*

Sorry, I was assuming you had dual motors on Z.


---
**Jason Smith (Birds Of Paradise FPV)** *September 25, 2015 16:56*

Funny you should ask about this. Eustathios #00001 has had the same issue since day 1. I haven't spent much time investigating, but definitely interested in a solution. 


---
**Miguel Sánchez** *September 25, 2015 16:57*

Ball screws may be the solution :-)


---
**Ben Delarre** *September 25, 2015 16:58*

**+Jason Smith** is #00001 a v2 style with the dual bearings at the bottom? **+Miguel Sánchez** no worries, thanks for trying to debug this with me.


---
**Eric Lien** *September 25, 2015 17:15*

**+Ben Delarre** this is why I think I will change the BOM to use ballscrews instead of leadscrews. I fight it every once and a while, but nothing like that. 


---
**Ben Delarre** *September 25, 2015 17:16*

**+Eric Lien** ah ok, well if you get it too I don't feel so bad. Your level of patience and attention to detail is well beyond mine :-)


---
**Eric Lien** *September 25, 2015 17:16*

Ballscrews from golmart on aliexpress are cheaper too.


---
**Ben Delarre** *September 25, 2015 17:17*

**+Eric Lien** I vaguely remember somebody already having made a ballscrew eustathios mod, was it this one by walter? [http://www.thingiverse.com/thing:854325](http://www.thingiverse.com/thing:854325)



I might bite the bullet and give it a go.


---
**Bryan Weaver** *September 25, 2015 17:25*

That's it.  Walter's got the bed supports on Thingiverse too.  Ballscrews were about $60 shipped from Golmart on Aliexpress.


---
**Eric Lien** *September 25, 2015 18:10*

**+Ben Delarre**  I plan to make the upgrade down the road too. 


---
**Jeff DeMaagd** *September 26, 2015 03:09*

I'd suggest trying isolators before shelling out more money. I don't really understand the aversion to isolators. The cheaper ball screws you're looking at aren't likely to be straighter than the lead screws you're already using anyway.


---
**Jarred Baines** *September 26, 2015 11:33*

Running super cheap ballscrews here, got them with the ends machined, I made my machine almost exclusively from Aluminium machined parts so its super-rigid and constrained and I do see very slight banding. Ballscrews are probably better but they're not a definite fix...



Someone have a link to a "z isolator"? I'd like to investigate these!


---
**Gus Montoya** *September 26, 2015 12:27*

@Ben Delarre. : will you have time to talk online this coming Monday. I noticed that you are running a smoothie on your V2. Let me know if Monday is good and about what time to communicate.


---
**Jeff DeMaagd** *September 26, 2015 12:38*

A z screw isolator basically disconnects the z nut from the thing being raised so it can only apply force in z. A flat or crowned surface touches the z axis parts, and something like an arm or fingers to keep the z screw from turning. I don't have one for this community's kind of machine (not built yet) but I can link to one I made for a different machine: [http://www.thingiverse.com/thing:750610](http://www.thingiverse.com/thing:750610)


---
**Ben Delarre** *September 27, 2015 16:25*

**+Jeff DeMaagd**​ my main concern with z isolators is that the eustathios homes at the top and any z isolator solution I can think of always has some backlash in it,  which will be especially evident on the first layer after reversing direction from the z homing. How do you avoid backlash issues when using isolators? 


---
**Jeff DeMaagd** *September 27, 2015 17:52*

Just make sure the platform can move up and down freely. Either I don't get backlash or it's repeatable enough that I can't tell. The weight of the platform should be more than enough to hold it down.


---
**Jason Perkes** *September 27, 2015 20:21*

**+Ben Delarre**  Just a thought since everything else seems to be mentioned above. Are you using helical lead screw couplers with motors mounted at the bottom? If so, is the lead screw bottomed out to the top of the motor shaft? Helical couplers will act like a spring if there is a gap between lead screw and motor shaft, allowing vertical movement on the Z axis.


---
**Ben Delarre** *September 28, 2015 02:36*

**+Jason Perkes** thanks for the suggestion, but the eustathios is driven with a belt from the motors to two pulleys one for each z leadscrew.


---
**Jason Perkes** *September 28, 2015 02:46*

Same principle or question applies whether they be belt or motor driven. If there are no helical couplers then question is invalid. Possibly might trigger a thought though. Would be interested to know the fix in the end :)


---
**Ben Delarre** *September 28, 2015 03:03*

Yeah no helical couplers. Its belt from motor to z axis leadscrew with pulleys directly mounted onto the leadscrew. Just switched filaments to some petg and the banding is much reduced, still there but tiny amounts. Now I just have bobbing on retracts and layer changes... If it's not one thing it's something else! 


---
**Jason Perkes** *September 28, 2015 03:07*

Blobbing on retracts, reduce all retract related speeds and accels to real slow then dial back up slowly. E axis acceleration or retract acceleration is one most overlooked. Reduce to 100mms'2  and 10mms speed for a slow and steady reliable retract baseline then dial e accels back up. That's how I work it anyways...hth.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/eUhUWtKhYDa) &mdash; content and formatting may not be reliable*
