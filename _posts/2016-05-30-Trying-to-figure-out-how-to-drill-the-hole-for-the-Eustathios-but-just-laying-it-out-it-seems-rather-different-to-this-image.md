---
layout: post
title: "Trying to figure out how to drill the hole for the Eustathios but just laying it out it seems rather different to this image"
date: May 30, 2016 15:45
category: "Discussion"
author: Pieter Koorts
---
Trying to figure out how to drill the hole for the Eustathios but just laying it out it seems rather different to this image. If I use 80mm based on the BOM for spider v2 the gap highlighted green is almost non-existent. Is there a drill guide for the extrusion bolting for Eustathios or a list of drill measurements? Is the 80mm centre to centre of the cross extrusion (from top one to support one)?

![images/b3a89e68103c5cac5a41f6b149fa6cf5.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b3a89e68103c5cac5a41f6b149fa6cf5.png)



**Pieter Koorts**

---
---
**Maxime Favre** *May 30, 2016 16:06*

I ordered the misumi drilled parts and the holes are at 470mm from the bottom. So 562-470 = 92mm from top to the middle of the extrusion.


---
**Pieter Koorts** *May 30, 2016 16:21*

Ah ok, I was measure 80mm from the top assuming the Misumi measurement started top down. Makes much more sense now. Thanks


---
**Daniel F** *May 30, 2016 18:10*

I printed a drill guide: [https://plus.google.com/111479474271942341508/posts/7wMEKzx9aw6](https://plus.google.com/111479474271942341508/posts/7wMEKzx9aw6)

I added 3mm because I did not use end caps. With the drill guide, it's easy to have the holes alligned. Measurements are in the thread.


---
**Pieter Koorts** *May 30, 2016 19:30*

Wow, thats amazing. I am even using the same extrusions as you ( because Misumi Europe suck :P )



That will help loads.


---
*Imported from [Google+](https://plus.google.com/100077479073911242630/posts/1kLLSvrnYTH) &mdash; content and formatting may not be reliable*
