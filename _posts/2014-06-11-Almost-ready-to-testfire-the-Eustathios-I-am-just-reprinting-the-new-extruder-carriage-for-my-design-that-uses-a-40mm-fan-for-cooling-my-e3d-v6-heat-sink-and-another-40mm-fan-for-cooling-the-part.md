---
layout: post
title: "Almost ready to testfire the Eustathios. I am just reprinting the new extruder carriage for my design that uses a 40mm fan for cooling my e3d v6 heat sink, and another 40mm fan for cooling the part"
date: June 11, 2014 08:24
category: "Show and Tell"
author: Eric Lien
---
Almost ready to testfire the Eustathios. I am just reprinting the new extruder carriage for my design that uses a 40mm fan for cooling my e3d v6 heat sink, and another 40mm fan for cooling the part.



On a side note can anyone let me know in the marlin firmware how I set the azteeg to only turn on the heat sink fan after a certain temp and what output I would use? Also can I PWM control it and how do I set it?



Is there something similar for the cooling fan I will have blowing on the stepper drivers on the azteeg?



Last note, can I modify the marlin menu to add a option to turn on my lighting led strips? Can I have the menu allow me to change the brightness value like the m42 command I have to do manually now? I would like to be able to control it when printing from the SD card and no computer is connected.



Thanks.



![images/bea992e05731e247f3d19928b3abcc02.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bea992e05731e247f3d19928b3abcc02.jpeg)
![images/6ce8ab0b74994f03808d8c82f497bfa3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6ce8ab0b74994f03808d8c82f497bfa3.jpeg)
![images/3a32da9eb37bc0f9037104a1e000771a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3a32da9eb37bc0f9037104a1e000771a.jpeg)
![images/71c0cec0bebbfb9efdeadd0441cc64fb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/71c0cec0bebbfb9efdeadd0441cc64fb.jpeg)
![images/5c77500af6594a1ce131418966fccb68.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5c77500af6594a1ce131418966fccb68.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-OPeVSbJYPbU/U5gSO1v30vI/AAAAAAAAeqw/jpV3P7zwe3U/s0/VID_20140611_024502.mp4.gif**
![images/93b80b10f1073a56bc85200957f783eb.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/93b80b10f1073a56bc85200957f783eb.gif)

**Eric Lien**

---
---
**Daniel Fielding** *June 11, 2014 08:39*

Looking so good.﻿ really looking forward to a video of your first print.


---
**Wayne Friedt** *June 11, 2014 10:01*

That is really clean and tidy. Looks great and you are fast.


---
**Nicolas Arias** *June 11, 2014 10:34*

**+Eric Lien**, you should check marlin's config_adv.h (or something similar). There you can setup tje autofan thing. As for the leds, its something i wanna work out, it needs to be added to the code. Ping me if you need any help


---
**Nicolas Arias** *June 11, 2014 11:29*

configuration_adv.h, fan connected to the shield mosfet output (the blue ones), on the terminal thats closer to the 12v input and fuse, + is the one on the fuse side.



Around line 78:



#define EXTRUDER_0_AUTO_FAN_PIN   17

#define EXTRUDER_1_AUTO_FAN_PIN   -1

#define EXTRUDER_2_AUTO_FAN_PIN   -1

#define EXTRUDER_AUTO_FAN_TEMPERATURE 50

#define EXTRUDER_AUTO_FAN_SPEED   255  // == full speed


---
**Eric Lien** *June 11, 2014 11:42*

**+Nicolas Arias** thanks. I am not that good at the code side of things. But this makes sense. How do I know that the blue output is 17? I have always struggled with where this is documented for the azteeg. 


---
**Eric Lien** *June 11, 2014 11:45*

**+Wayne Friedt** thanks. You turn out some pretty cool bots. I just noticed your new video on the corexy. Nice.


---
**Nicolas Arias** *June 11, 2014 13:46*

**+Eric Lien**: [http://files.panucatt.com/datasheets/x3v1_1_wiring_diagram.pdf](http://files.panucatt.com/datasheets/x3v1_1_wiring_diagram.pdf)


---
**Eric Lien** *June 11, 2014 14:43*

**+Nicolas Arias** I am a dork. I see it clear as day now. D17. Thanks.﻿


---
**Nicolas Arias** *June 11, 2014 14:46*

if it doesnt spin, change the cables to d16 and it will work :D


---
**Eric Lien** *June 12, 2014 18:24*

**+Nicolas Arias** fired it up last night. After some adjustments on leveling and squaring the rods it runs smooth as Teflon. All the cooling fans working as advertised. Thanks.



For the led control I am still at a loss. My googlefu has failed me. Any help by the marlin experts would be appreciated.



First test prints will be tonight. Per **+Tim Rastall** I am trying to generate a motion only gcode to run around the perimeters in squares and arcs to break in the bushings for about an hour. I don't know what I am doing hand coding gcode but hope to figure it out tonight.


---
**Nicolas Arias** *June 12, 2014 20:18*

**+Eric Lien** glad it worked. As for Led control, give me some time i will do it, sooner or later


---
**Riley Porter (ril3y)** *June 13, 2014 01:24*

**+Eric Lien** that is awesome!  


---
**Eric Lien** *June 13, 2014 03:32*

**+Riley Porter** thanks man. Read up a bit  on manual gcode and am running a 1hr cycle of the perimeters clockwise and counter clockwise, corner to corner cross over, and G2 and G3 arcs to break in the bronze bushings. Man do arcs stutter at high speeds. I had to slow the arcs way down. I don't think there's binding. I think its lag in the processor. If I remember arcs kind of tax the ATMEGA 2560. Thinking I should have gone arm based.



When's tinyG gonna be able to run a printer? :)﻿


---
**Tim Rastall** *June 13, 2014 06:27*

**+Eric Lien** go look at the cramps sheild for beaglebone black. I shared a link a few days ago. 


---
**Riley Porter (ril3y)** *June 13, 2014 11:46*

**+Eric Lien** yes very soon. Working out a few bugs in it. We are printing with it now actually.


---
**Riley Porter (ril3y)** *June 14, 2014 01:37*

**+Eric Lien** btw check out this plotter jason and I built. 



[https://www.flickr.com/photos/rileyporter/13988054029/](https://www.flickr.com/photos/rileyporter/13988054029/)



Its the eustathios as an xy plotter.  50,000 mm/min driven by tinyg is was hauling!


---
**Eric Lien** *June 14, 2014 02:02*

**+Riley Porter** that things hauling. I think I need to do a little more tensioning. I skip above 32,000 so far. And arcs... Forget about it. The board chokes on arcs above 7000. But I have 0.9 deg steppers and 1/16 micro stepping. 1/32 was just too much.


---
**Riley Porter (ril3y)** *June 14, 2014 02:29*

Yah that was before **+Jason Smith**  fixed it!  However, those arcs are at 45k mm/min :)


---
**Eric Lien** *June 14, 2014 03:02*

**+Riley Porter** the video was before he fixed it? Crap I've got some tuning to do then.﻿


---
**Tim Rastall** *June 14, 2014 06:04*

**+Riley Porter** wowee.  Can that video be posted in the 3d printing G+ community please.  That is bananas fast. 


---
**Eric Lien** *June 14, 2014 06:12*

**+Riley Porter** hard to tell cause its moving so fast. Are those bushings or linear bearings?


---
**Tim Rastall** *June 14, 2014 06:12*

**+Eric Lien** I seem to recall that marlin just breaks g2 and g3 arcs up into lots of tiny vectors anyway,  so the stuttering is likely due to you maxing out the speed at which the 8bit architecture can handle it.  Plus a few other inefficiencies in the way it buffers and processes the code.


---
**Riley Porter (ril3y)** *June 14, 2014 18:05*

**+Tim Rastall** sure about the video.  However we are not 100% ready to say TinyG is open for 3d printing  There are people doing it now however.  There are a few issues we are still dealing with.  



And about the arcs.  Yes this is what it is doing.   Breaking into a bunch of little lines.   In TinyG we do not.  We interpret g2/3 as arcs :).  **+Eric Lien** there is no way that any reprap controller is going to move this fast.  From my knowledge that is.  **+Jason Smith** is printing at 230 mm/min on his printer.  On his rambo2 that is.  The coolest thing I am looking forward to is to not need retraction :) if you move so fast you do not need to retract !


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/Gzuz2VEpQSr) &mdash; content and formatting may not be reliable*
