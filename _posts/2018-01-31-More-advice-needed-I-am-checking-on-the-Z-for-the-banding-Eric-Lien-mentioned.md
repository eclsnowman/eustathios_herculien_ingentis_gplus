---
layout: post
title: "More advice needed! I am checking on the Z for the banding Eric Lien mentioned"
date: January 31, 2018 14:49
category: "Discussion"
author: Bruce Lunde
---
More advice needed! I am checking on the Z for the banding **+Eric Lien** mentioned. I think my belts are tight, but this print is terrible. Also reviewing a common printing problem page that **+Dennis P** sent me.  I think I also should have printed this on its side, but it printed from the back bottom up into the air. Lots to learn about the slicing.







![images/916f65f96a8802878e8d692802c00460.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/916f65f96a8802878e8d692802c00460.jpeg)
![images/f7f40a1c4f061f08a81b9aee7e0792c7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f7f40a1c4f061f08a81b9aee7e0792c7.jpeg)

**Bruce Lunde**

---
---
**Michaël Memeteau** *January 31, 2018 15:49*

Looks like over-extruding to me. Check your step/mm for the extruder.


---
**Eric Lien** *February 01, 2018 00:45*

Can I get a video of printing this part? Zoomed in. And long enough for several layers of printing to be visible. There is a lot wrong in this picture. So seeing what is happening will give me a good idea of where you should start.


---
**Eric Lien** *February 01, 2018 00:46*

Also yes, you printed this in the wrong orientation.


---
**Eric Lien** *February 01, 2018 00:52*

Also, is this still with the 1.2mm nozzle. If so I really recommend starting with a 0.4mm to 0.6mm to cut your teeth. Then upgrade to larger nozzles once you get comfortable. 



You jumped in head first with a HercuLien, printing in ABS, and a 1.2mm nozzle as your first printer. Which I totally admire your determination. But I think a Crawl, Walk, Run approach will work better. Get in some wins... Then expand.


---
**Bruce Lunde** *February 01, 2018 02:37*

**+Eric Lien** This is with the .6mm nozzle.  I did not get a video on this print. I will take a step back and load some PLA.  I will alsol ay this on it's side. I thought after my prior work with the cube I had everything but the z axis leveling good.


---
**Bruce Lunde** *February 01, 2018 04:34*

   **+Eric Lien** Here is the same model, flipped on it’s side, and I changed the cura settings to draft quality. Much improved, but still some work needed.![images/719045964cfb504334b4ce0c70369802.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/719045964cfb504334b4ce0c70369802.jpeg)


---
**Bruce Lunde** *February 01, 2018 04:34*

![images/01b438766df945d7929552049158c5d7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/01b438766df945d7929552049158c5d7.jpeg)


---
**Bruce Lunde** *February 01, 2018 04:35*

![images/f388d86f958617e802cd44b8cfaf76c5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f388d86f958617e802cd44b8cfaf76c5.jpeg)


---
**Bruce Lunde** *February 04, 2018 01:00*

**+Eric Lien** and **+Dennis P** Your advice is paying off, take a look at this print:



![images/56df2b9041b734084eb6b7ad5c443858.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/56df2b9041b734084eb6b7ad5c443858.jpeg)


---
**Eric Lien** *February 04, 2018 01:19*

Keep it up. Thats what you are looking for. Daily progression, little by little. Printing is more about learning all the ways not to do something... Until the only thing left is the way that works :)



Congratulations.


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/PTwG5MqGfLj) &mdash; content and formatting may not be reliable*
