---
layout: post
title: "Well guys, I've sent another e-mail requesting an eta on these bad boys"
date: March 30, 2014 15:08
category: "Show and Tell"
author: D Rob
---
[http://www.robotdigg.com/product/149/10mm-bore-32-teeth-gt2-pulley](http://www.robotdigg.com/product/149/10mm-bore-32-teeth-gt2-pulley)



Well guys, I've sent another e-mail requesting an eta on these bad boys. The last one I sent (about 2 weeks ago) said 1-2 weeks before ship date. I hope to hear from them early next week (1st or so). For everyone who missed out on the crowd fund, our emails worked and this is a standard size pulley at [robotgigg.com](http://robotgigg.com) now. They are the only known manufacturer of a 10 mm bore gt2 pulley with a relatively low tooth count (32) 20 they said was not possible and 8 mm is the largest bore for it ) i.e. Ultimaker style pulleys. With machines getting larger and the need for larger smooth rod because of I am glad we are able to have this go from custom to readily available on their site. So buy em use em or they may take them away if they don't seem popular. Then min orders will be 50 at a time.





**D Rob**

---
---
**Eric Lien** *March 30, 2014 16:42*

Now everyone can benefit from your push to get them made.


---
**ThantiK** *March 30, 2014 16:43*

Thanks for taking the initiative on this, I wouldn't have got them otherwise - looking forward to swapping out those spectra lines on my current build before I go any further with things.


---
**D Rob** *March 30, 2014 17:42*

No prob guys :) I have been looking for these since october/november last year. then I stumbled on robotdigg and th rest is obscure history


---
**Shachar Weis** *March 30, 2014 18:43*

**+Anthony Morris** why ? I thought you were pro-specrta. Why switch to GT2 ?


---
**ThantiK** *March 30, 2014 20:19*

I'm not pro spectra.  I just wanted to be able to understand how it was assembled.  Spectra has problems of changing tension as you travel. ﻿



Spectra is nice in that it's REALLY CHEAP,  but given the option, I'll go belts any day. 


---
**Jim Squirrel** *March 30, 2014 22:31*

Of course now that I've seen a few with belts I'll be ordering six more


---
**Brian Bland** *April 10, 2014 03:37*

Did you ever here from Robotdigg?  I see they have an actual picture on the link now.  I ordered something off aliexpress from them and from their website.  They sure can't keep tracking numbers straight.  I thought I was getting the Aliexpress order and it was the one from the website.


---
**D Rob** *April 10, 2014 05:01*

i did hear back from them and they said a week or so from now they will ship. The lady I was talking to was out on "short leave" so I didnt hear back from her. I then used another of their contact emails. sales@robotdigg.com and got the reply.


---
**D Rob** *April 10, 2014 05:02*

as soon as I get everything I will contact everyone and send it on.I am growing anxious / impatient myself


---
**Brian Bland** *April 10, 2014 05:10*

No problem.  Just curious.  The GT2 belt I ordered from them went through New Jersey 4 days ago supposedly if they gave me the correct tracking number.


---
**ThantiK** *April 10, 2014 12:14*

No worries here.  I'm used to waiting a month or two ordering cheap Chinese stuff. 


---
**D Rob** *April 10, 2014 14:56*

Just got the tracking number this morning. It's via UPS to me. 


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/fDKUr6aDAVt) &mdash; content and formatting may not be reliable*
