---
layout: post
title: "I'm searching for inspiration, guys. I wonder what carriages you guys are using except from the standard one"
date: January 06, 2017 19:32
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
I'm searching for inspiration, guys. I wonder what carriages you guys are using except from the standard one. I'm missing motivation and creativity lately and already spent some hours drawing and throwing away ideas. My last design was based on the one from +Walter Hsiao which basically was great but failed at cooling the E3D in the right way. Also the mounting of my special kind of Bed Level Sensor (the small IR sensor) wasn't that good. I liked the dual blower setup for part cooling and I thought it would be worth keeping it.



If you hadn't seen the design, you can find some stuff here [https://plus.google.com/+FrankHelmschrott/posts/ZCLFZ7t5qX9](https://plus.google.com/+FrankHelmschrott/posts/ZCLFZ7t5qX9)



Basically i need better ventilation (or better air throughput) on the back of the E3D and a better place to mount the IR sensor board. Also I will probably switch back to the graphite bushings or even to SDSPI if I can get some.



Anyways - would be happy to see your carriages if you did any special ones.





**Frank “Helmi” Helmschrott**

---
---
**Matt Miller** *January 06, 2017 21:27*

Here's my version of the space invader - might help



[myhub.autodesk360.com - Space Invaders Carriage v13](http://a360.co/2jcPZO5)




---
**Frank “Helmi” Helmschrott** *January 07, 2017 08:23*

Hey **+Matt Miller** thanks. Does the cooling work for you? Your cooling holes in the back aren't much bigger than mine and for me that didn't work at all. 


---
**Matt Miller** *January 07, 2017 13:55*

Well, I can't say it's perfect as I've never measured it.  And I've only put PLA through it.  But it's never clogged on me.


---
**Frank “Helmi” Helmschrott** *January 07, 2017 13:59*

If it runs with PLA and also on small spikes or smaller elements like the benchy window posts it's probably good. that's where I got problems.



You've got much more room behind the hotend which lowers the pressure on the axial fan which should make the biggest difference. I could maybe take some ideas to redo mine completely. Thanks anyways for posting. it's always good to see how others work.


---
**Matt Miller** *January 07, 2017 14:08*

Hey, just glad I could help in some fractional way.  And if you're having problems getting those sintered bearings let me know.  I can ship something from the US to you.


---
**Frank “Helmi” Helmschrott** *January 07, 2017 14:10*

That's a great offers - thanks so much. I'm currently evaluating some similar IGUS bearings. I'm kind of an IGUS fan and will see if they can work for this. If not I'll definitely come back to your offer.


---
**Matt Miller** *January 07, 2017 14:18*

Cool.  Just let me know - happy to help!


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/LQ3rZStoJB4) &mdash; content and formatting may not be reliable*
