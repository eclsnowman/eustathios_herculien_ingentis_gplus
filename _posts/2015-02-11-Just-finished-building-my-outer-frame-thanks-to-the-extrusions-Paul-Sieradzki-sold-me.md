---
layout: post
title: "Just finished building my outer frame thanks to the extrusions Paul Sieradzki sold me"
date: February 11, 2015 02:35
category: "Deviations from Norm"
author: Ryan Jennings
---
Just finished building my outer frame thanks to the extrusions **+Paul Sieradzki** sold me. 



I am going to build a scaled up herculien, aiming for a 600x600x700mm print area. Now that i have outer frame built I can start on the inner frame reinforcements and getting my motion components purchased!

![images/907cb648813aa75191d800bf63e9d46b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/907cb648813aa75191d800bf63e9d46b.jpeg)



**Ryan Jennings**

---
---
**Paul Sieradzki** *February 11, 2015 02:53*

This makes me very happy. Looking good, Ryan!


---
**Derek Schuetz** *February 11, 2015 02:59*

That is freaking huge! I can't imagine the time it'll take to complete a print


---
**Ryan Jennings** *February 11, 2015 03:01*

I am planning on using a 24v e3d volcano with either the 1mm or 1.2mm nozzle for the main extruder.  so hopefully print times will be able to stay somewhat reasonable.  If i decide to setup the 2nd extruder then i will use the .6mm so i can still do higher detail when needed.


---
**Isaac Arciaga** *February 11, 2015 03:27*

**+Ryan Jennings**​ get'er done! Is this going to be your show piece for MRRF?


---
**Eric Lien** *February 11, 2015 04:09*

Yah MRRF.


---
**Derek Schuetz** *February 11, 2015 04:31*

How do you plan to offset the difference in nozzle legth for the dual extruder


---
**Eric Lien** *February 11, 2015 04:40*

**+Derek Schuetz** that wouldn't be too difficult. A little bit of moding on the solidworks files could handle that.


---
**Mike Miller** *February 11, 2015 04:40*

Mrrf?


---
**Eric Lien** *February 11, 2015 04:42*

**+Mike Miller** maker reprap fest. Goshen Indiana. It's coming up.


---
**Derek Schuetz** *February 11, 2015 07:07*

I really need to get solid works...


---
**Ryan Jennings** *February 11, 2015 12:35*

Yea I hope to get this thing done to take to the mrrf this year. **+Isaac Arciaga**​



**+Ashley Webster**​ for motion on x and y I will use rails pretty much like the herculien. Z I am still debating on rod or v rail as my motion guide. But looking at 12mm leadscrews



**+Derek Schuetz**​ as for dual I would try to use a e3d volcano on bothhotend for same length. Just different nozzle in each. But Eric is right it would not be difficult to do the offset in the printed part instead


---
**Nathan Buxton** *February 12, 2015 04:31*

Oh I can't wait to see this in action.


---
*Imported from [Google+](https://plus.google.com/100961646236572561479/posts/fvZZ4ak6muX) &mdash; content and formatting may not be reliable*
