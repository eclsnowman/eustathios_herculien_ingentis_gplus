---
layout: post
title: "Hey everyone, been lurking for a bit and finally trying to decide on a printer to build"
date: June 02, 2015 20:04
category: "Discussion"
author: Michael Brown
---
Hey everyone, been lurking for a bit and finally trying to decide on a printer to build.



If I wanted to modify either the Eustathios or the Spider v2 for quad-extrusion (i.e. Kraken), which design would be better for that? Is it even worth considering the original Eustathios or just go for the v2?





**Michael Brown**

---
---
**Eric Lien** *June 02, 2015 22:41*

Both a great. Jason's original design has zero problems for me. I designed V2 to get a little more height, and integrate a few things that worked well on my HercuLien design. 



The carriage for kraken would work for either equally. The cross bar spacing is identical. 


---
**Jeff DeMaagd** *June 02, 2015 23:19*

Have you built or used any machine before? Because dual and multi extrusion is quite a beast to tame. I think it's best to understand single extrusion backwards and forwards before tackling multi. Apologies if you already have a lot of 3DP experience, it's just that doing multi well isn't for the faint of heart.


---
**Michael Brown** *June 02, 2015 23:33*

**+Jeff DeMaagd** I've had a Printrbot Simple Metal since October and lately I've been itching to move onto a larger printer with multi-extrusion. No offense taken!


---
*Imported from [Google+](https://plus.google.com/102763810673021407550/posts/AVs1XW74vFv) &mdash; content and formatting may not be reliable*
