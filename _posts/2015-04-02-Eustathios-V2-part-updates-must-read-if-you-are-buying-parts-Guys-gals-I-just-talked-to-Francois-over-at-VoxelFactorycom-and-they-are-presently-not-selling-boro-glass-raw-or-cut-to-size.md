---
layout: post
title: "Eustathios V2 part updates -- must read if you are buying parts Guys/gals, I just talked to Francois over at VoxelFactory.com and they are presently not selling boro glass (raw or cut to size)"
date: April 02, 2015 14:17
category: "Discussion"
author: Seth Messer
---
<b>Eustathios V2 part updates -- must read if you are buying parts</b>



Guys/gals, I just talked to Francois over at VoxelFactory.com and they are presently not selling boro glass (raw or cut to size). They have a note on that product page with an explanation: [http://www.voxelfactory.com/collections/reprap-parts-prusa-i3-others/products/custom-cut-borosilicate-glass](http://www.voxelfactory.com/collections/reprap-parts-prusa-i3-others/products/custom-cut-borosilicate-glass)



Would be awesome to find an alternative solution that services N. America.



Also, I'm still missing details/links for 2 specific parts (both of which are available either through OpenBuilds or SMW3D) and are for the Idler Wheel Assembly. If you can, <b>please help</b> fill in these gaps for the community..



Part numbers/links needed:

- "Precision Ball Bearing"

- "V-Slot Idler Pulley"



I'm not sure exactly which of those two part number/descriptions are needed.



Lastly, I've started a similar build guide to **+Daniel Salinas**' work for the HercuLien, but for the Eustathios V2. I haven't started building yet, but wanted to start a document.



[https://github.com/megalithic/Eustathios-Spider-V2/tree/master/Documentation/Build%20Guide](https://github.com/megalithic/Eustathios-Spider-V2/tree/master/Documentation/Build%20Guide)



Any help would be great.



Thanks!

Seth





**Seth Messer**

---
---
**Eric Lien** *April 02, 2015 17:02*

Those misc openbuilds parts are the components included in this assembly (which has all the parts but a longer bolt is required): [http://www.smw3d.com/idler-pulley-kit/](http://www.smw3d.com/idler-pulley-kit/)


---
**Eric Lien** *April 02, 2015 17:08*

For the glass you can just use window glass cut to size at the local hardware store. I have been using it on HercuLien for over 8 months with zero issue. Plus it is cheap enough to have several on hand. Just don't get tempered glass as the pretension inherent from tempering causes it to crack when hit with the thermal shock of initial heat up.


---
**Seth Messer** *April 02, 2015 17:30*

Thanks tons Eric. I actually ordered some custom cut glass earlier this morning after I saw VoxelFactory was no longer carrying. Figure between that, a thin sheet of PEI and the aluminum plate, i should be good.


---
**Brad Hopper** *April 02, 2015 17:57*

Yeah the boro glass was a bust for me. Worked fine until a print pulled a chip out of the middle. It lasted just ~2 months, just a few 10s of hours of printing. Since then IKEA/Michaels mirrors have been working and have not chipped.


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/ELaSYB9uoA1) &mdash; content and formatting may not be reliable*
