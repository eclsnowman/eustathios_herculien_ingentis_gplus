---
layout: post
title: "Bed weight & thickness. Wondering if I need to be concerned about weight and if that has an impact on z-hops etc?"
date: March 27, 2018 19:17
category: "Discussion"
author: Julian Dirks
---
Bed weight & thickness.  Wondering if I need to be concerned about weight and if that has an impact on z-hops etc?



6mm = 2.35 Kg vs 10mm = 3.9 Kg



Hardly any difference in price.  Contemplating 10mm for absolute robustness and in case I want to mill pockets for magnets etc etc in the future.  Alloy is "Formodal 30" (cast & mill finished etc).

.

Anyone got any advice?







**Julian Dirks**

---
---
**Eric Lien** *March 27, 2018 20:36*

I am torn. I am with you on the mores better mentality. I just can't confirm Zhop won't be affected.



You can be the first to find out, and give feedback.


---
**Ryan Carlyle** *March 27, 2018 22:54*

What’s the screw pitch/lead? Probably won’t matter for 2mm but could have an impact for 8-12mm. 


---
**Julian Dirks** *March 27, 2018 23:00*

Golmart ballscrews as per Walters mod. On their website it says "screw pitch (lead) = 4"?


---
**Ryan Carlyle** *March 27, 2018 23:30*

if that's 4mm, it might be borderline or it might work fine. (What you COULD do is calculate the reflected inertia due to bed mass versus rotor and screw moment of inertia... but who has time for that?) Gonna go with **+Eric Lien** and say you should try it and find out. 


---
**Walter Hsiao** *April 03, 2018 16:22*

One other issue to be aware of is if you add too much weight, the bed will fall when you de-power the z-axis motor.  I suspect it would only be an issue if you plan to do very heavy multi-kg print jobs.



My bed is approximately 410x360x6.5mm, probably about 2.5kg.  I can add about 3-4kg to the bed before it starts to collapse under it's own weight when the motors are de-powered (it can hold 4 to 5kg, but once it starts moving, it doesn't stop).  My rods and screws are covered in hairspray so that could be helping too.  Using a 16T Z axis pulley and a 0.9 degree stepper from Robotdigg.




---
**Ryan Carlyle** *April 03, 2018 16:27*

If you don’t want the bed to fall, use 2mm pitch screws :-)


---
**Eric Lien** *April 03, 2018 17:18*

Lulzbot was showing off a small circuit board at MRRF. It shorted the leads on the stepper when power is cut so the bed doesn't fall. They are testing it to go along with a belt driven Z axis they are working on for the next Taz printer. Seems like a novel solution and easy enough to implement. I think they were even doing it as a stand-alone board. So it could be used by others if they put it up for sale. 


---
**Ryan Carlyle** *April 03, 2018 17:31*

I think I would vastly prefer putting a worm screw between the motor and Z belt rather than resistive braking. Regular belt Z drives don't have very good resolution. (EG the full step resolution of Walter's Z stage is 0.08mm which is quite coarse compared to most printers.) Worm gears are pretty cheap these days. Although you do run the risk of having TOO MUCH resolution and the Z stage can't move very fast.


---
**Ryan Carlyle** *April 03, 2018 17:33*

Also strikes me as a huge, violent electrical failure point to have a widget deliberately short inductor leads...


---
**Walter Hsiao** *April 03, 2018 17:46*

I'm running 3200 steps/mm at 16x microstepping or 0.005mm/full step on my Eustathios.  (my Z is 0.9 stepper w/ 16T pulley -> belt -> 32T Pulley w/ 4mm pitch ballscrew if I remember correctly, it's been a while).


---
**Ryan Carlyle** *April 03, 2018 17:50*

Oh, ok, I misunderstood, thought you just had a Z belt hanging off a 16t pulley. There are more people doing stuff like that these days. Most ball screws will NOT self-lock because of low friction, and you're relying on motor detent torque, right. 



A 2mm pitch lead screw will self-lock and never back-drive under gravity under any circumstances. That's what I generally recommend for people lifting nozzles on a Z stage (where dropping the load can damage the bed). 


---
**Dennis P** *April 03, 2018 19:13*

why put a NC relay that would be energized to be open when the printer is on to effectively short the windings like **+Eric Lien** described?  it would be just like the shaft safety on a scooter motor.


---
**Julian Dirks** *April 03, 2018 19:51*

I went with 10mm plate.  To get the weight down I cut the bed to 360 x 360mm which took 400g off down to 3.5 Kg. I'm going to stick PEI stright onto the bed so no glass weight. 



Thanks for the comments about bed drop - something to think about. I read somewhere about people using counterweights as well.  Kind of messy vs an electronic solution though.




---
**Ryan Carlyle** *April 03, 2018 20:20*

**+Julian Dirks** Counterweights add moving mass and hassle. Much better to use CF springs if you want to balance bed weight. 


---
**Ryan Carlyle** *April 03, 2018 20:24*

**+Dennis P** What does "energized to be open when the printer is on" mean? When the controller is getting power?

1) Capacitors on the PSU or inductance in the coils may create a window where the motors are energized but the relay failsafes, violently shorting the coils

2) Likewise, the relay might not close fast enough to prevent loss of position if the motors lose power faster than the rest of the controller

3) Most controllers turn drivers on/off electronically via ENABLE line, eg at the end of the print or upon extended idle time... what do you do then?



If all you want to do is prevent major nozzle/bed collision damage and let motors safely power down at the end of the print, the timing maybe isn't so critical, but if you want to recover a print after power loss, the brake has to engage pretty much exactly as the motor coils lose power... it's kind of a timing challenge...


---
**Julian Dirks** *April 03, 2018 20:26*

**+Ryan Carlyle** What is a CF spring?


---
**Ryan Carlyle** *April 03, 2018 20:27*

Constant force. [mcmaster.com - McMaster-Carr](https://www.mcmaster.com/#constant-force-springs/)


---
**Dennis P** *April 03, 2018 21:14*

**+Ryan Carlyle** Preventing bed falling collision is more like it.  Timing- I don't think its that critical. Its not like the weight is going to overcome the friction on the mechanical system and the bed is going to freefall. Its more like and interlock switch function. 



I am imagining that is about as simple as your standard Bosch 5 pin relay where you short across the coils of the motor windings with pins 87a and 30 so when the power is off, the relay is in the NC condition. When the printer is on, the control pins 86 and 85, would be energized opening the NC to open and thus returning the motor to normal function. 



Electromagnetic brake- similar thing,  spring powered with coil excitation to open.  But i think the market for these devices is too small to make it practical. 



As far as the condition you describe with drivers being switched on/off by the controller, I don't see it conflicting. AS long as the printer is ON and the relay gets power, the short-circuit condition will not exist. 



Frankly, recovering a print after power loss to me is non-sense. Yeah it sucks of you loose a print but its not like we are running the 911 call center and maintain 24/7 uptime.It's great in theory but proper execution is $$$. If you have hardware and firmware that supports it natively, I would use it. I think its much more wise and cost effective to take the signal from a UPS through something like the RaspberryPi, tell the printer to stop printing and write the position log to a file. hmm. Now that has me thinking.... 




---
**Ryan Carlyle** *April 03, 2018 21:26*

**+Dennis P** If you have a Z-nozzle and don't want it or the gantry to crash into the print when the print is complete/e-stopped, the driver disable case matters. (Some deltas have issues with this.) Although I would argue a CF spring is a much better solution since it's mechanical and simple.



If you have a Z bed, you shouldn't care if the bed drops, it won't hurt anything. (unless you have multiple unsynchronized non-self-locking screws and falling puts the bed out of alignment, in which case, shame on you for designing it that way.) 



So I guess I'm not seeing a usage case where the (low probability) risk from a coil-shorting relay and added wiring complexity is worth the benefit you get from resistive braking the stepper. 



On power failure recovery, I generally agree with you, but it's something that a lot of people are wanting in nicer printers these days. You put a few kilos of $$$ composite filament into a print, it's worth some effort to recover (or integrate batteries or whatever).


---
**Julian Dirks** *April 05, 2018 10:22*

10mm plate arrived (great service from "little metals" in ChCh in case anyone from NZ is ever in the hunt for a supplier). Now to get it mounted...

![images/3c0b6934e84946430bfa6fec4ab3981e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3c0b6934e84946430bfa6fec4ab3981e.jpeg)


---
*Imported from [Google+](https://plus.google.com/113795478307151372873/posts/7jB7ZquhmZT) &mdash; content and formatting may not be reliable*
