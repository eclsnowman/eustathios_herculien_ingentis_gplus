---
layout: post
title: "Long Time no news , but here comes an update ."
date: May 30, 2015 19:27
category: "Show and Tell"
author: Mikael Sjöberg
---
Long Time no news , but here comes an update . 

After a bitter discovery that I had drilled the holes for the Z midframe in the wrong position I started over and have slowly been working with my printer. The threaded Z-axis have been lathed in the ends, though they seem to be not as straight as I want. all good ideas to straight them up is appreciated! Though tje The nuts runs smoothly in the axis . 

The Z-Gantry plate is under manufacturing now and I can't wait to test it ! 

Also I have received several 3D printed parts in a nice warm  Green  Color from My good friend Martin Bondeus. 

Next step is to order and cut the glas....



![images/f4fe4da4ae6f5adab0729097a254be88.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f4fe4da4ae6f5adab0729097a254be88.jpeg)
![images/02793fe17b2dbc6f0f3e1371150bf52d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/02793fe17b2dbc6f0f3e1371150bf52d.jpeg)
![images/cdfc286f67c51995ffcd41a9b9bdd3da.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cdfc286f67c51995ffcd41a9b9bdd3da.jpeg)
![images/0e142a55732b33c5dbfab3acb9f13afc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0e142a55732b33c5dbfab3acb9f13afc.jpeg)

**Mikael Sjöberg**

---
---
**Eric Lien** *May 30, 2015 20:53*

Looks great. One thing to note is the lower bearing mount for the lead screw does not go all the way against the vertical extrusion. It has to be spaced off a tiny bit. I did this to allow adjustment in/out to get them perfectly aligned. Most people miss it the first time, but it if you don't space it out the bed will bind on the way down.



I look forward to seeing your progress.


---
**Eric Lien** *May 30, 2015 20:54*

I also love the addition of magnets for the bed :)


---
*Imported from [Google+](https://plus.google.com/118217975155696261814/posts/UK8tWCeq4eE) &mdash; content and formatting may not be reliable*
