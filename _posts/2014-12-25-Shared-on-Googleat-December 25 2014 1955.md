---
layout: post
title: "Shared on December 25, 2014 19:55...\n"
date: December 25, 2014 19:55
category: "Deviations from Norm"
author: Tim Rastall
---


![images/8a5204bc247f9ec5c354638340c87d6d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8a5204bc247f9ec5c354638340c87d6d.jpeg)



**Tim Rastall**

---
---
**James Rivera** *December 25, 2014 21:24*

I get it now. This is the manifestation of the discussion on using one fewer moving rods and linear bearings instead of bushings. But this actually increases the BoM, doesn't it?


---
**James Rivera** *December 25, 2014 21:25*

Why use a rod going all the way across instead of just a small bolt to hold the idler pulley?


---
**Tim Rastall** *December 26, 2014 02:54*

**+James Rivera** because the rod prevents the 2 ends from getting misaligned. 

Yes a slight increase in bom but no fiddly self aligning bushings or need for super straight shafts.  Note the dual steppers per axis too.  I learned a bunch from designing the ingentis and this is a possible alternative to a number of constraints to the Ingentis design. 


---
**Jarred Baines** *December 30, 2014 02:12*

Nice ideas there **+Tim Rastall**​! I like where you're taking it :-)


---
**ThantiK** *January 04, 2015 02:03*

I'm moving <i>FAR</i> <i>FAR</i> away from linear ball bearings.  That noise.  If I ever hear it again, it will be too soon.


---
**Tim Rastall** *January 04, 2015 02:54*

**+ThantiK** yeah but I'll take noise over bronze wearing out and delrin sticking under dynamic loads.  There's a reason why most industrial linear motion is achieved with ball bearings on hardened shafts.


---
**ThantiK** *January 04, 2015 03:05*

I'm likely to get back on my Ingentis soon, finish it up, sell it, and do it all over again with Polycarbonite V wheels, and Openbuilds 2020.


---
**Jarred Baines** *January 04, 2015 07:54*

Does polycarb wear well?



I thought it was tough, but soft?


---
**ThantiK** *January 04, 2015 15:23*

**+Jarred Baines**, dunno.  Experimental. :D


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/HAd4Jzdfw8H) &mdash; content and formatting may not be reliable*
