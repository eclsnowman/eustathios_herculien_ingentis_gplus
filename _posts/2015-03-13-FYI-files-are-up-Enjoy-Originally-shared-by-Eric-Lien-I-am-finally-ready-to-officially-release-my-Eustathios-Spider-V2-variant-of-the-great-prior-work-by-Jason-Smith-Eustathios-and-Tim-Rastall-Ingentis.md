---
layout: post
title: "FYI, files are up. Enjoy. Originally shared by Eric Lien I am finally ready to officially release my Eustathios Spider V2 variant of the great prior work by Jason Smith (Eustathios) and Tim Rastall (Ingentis)"
date: March 13, 2015 16:08
category: "Deviations from Norm"
author: Eric Lien
---
FYI, files are up. Enjoy.



<b>Originally shared by Eric Lien</b>



I am finally ready to officially release my Eustathios Spider V2 variant of the great prior work by **+Jason Smith**​ (Eustathios) and **+Tim Rastall**​ (Ingentis). In V2 I used some design styles from my #HercuLien variant as well as an upgrade to the smoothieware based Azteeg X5 Mini controller, Viki2 LCD, and great high torque stepper motors from robotdigg. 



Included in this release is a detailed excel BOM, and the start to some prints and documentation. I also converted my solidworks design files into many formats to help people without access to solidworks use my designs to improve and customize. This includes step, x_t, designspark, rhino, edrawings, 3d-pdf, sketch up, etc.



Hope you enjoy.





**Eric Lien**

---
---
**Seth Messer** *March 13, 2015 16:21*

high-five.



thanks **+Eric Lien** !


---
**Ben Delarre** *March 13, 2015 16:39*

Great stuff Eric, I shall be placing my Misumi order tonight. RobotDigg arrives this evening, hopefully have this built in the next couple of weeks!



I'm still torn about putting the motors on the outside of the frame like that, do you think the shorter belts will help with accuracy (reduced resonance?)? I assume you can still enclose the frame fairly easily with some small cutouts in the acrylic.


---
**Eric Lien** *March 13, 2015 16:49*

**+Ben Delarre** thats my thought. I really like the shorter belts for crisp motion minimizing belt stretch harmonics at high speed.


---
**Joe Spanier** *March 13, 2015 16:49*

Do you ever see 3d photos on your phone and try to rotate them? Cuz I do. Good work man! Can't wait to check it out. 


---
**Ben Delarre** *March 13, 2015 16:51*

**+Eric Lien** Well, I think i'll print out an extra set of motor mounts and spend the extra $16 at Misumi to get the alternate belt sizes and give your layout a go then I can contrast and compare.



I can't wait!


---
**Kodie Goodwin** *March 13, 2015 17:27*

Its beautiful! 


---
**Isaac Arciaga** *March 13, 2015 17:46*

Wow! Looks good **+Eric Lien** ! Thank you!


---
**Ben Delarre** *March 13, 2015 18:01*

Did anyone figure out a mount for the new bondeus extruder beta? 


---
**Isaac Arciaga** *March 13, 2015 18:13*

**+Ben Delarre** the current mount should fit fine on this machine if your are using a geared nema 17.


---
**Ben Delarre** *March 13, 2015 18:14*

I got the full kit so I guess I do.  Should arrive on Monday so we will see.  So the plastic parts that come with it should mount directly to the extrusion? 


---
**Eric Lien** *March 13, 2015 18:30*

**+Ben Delarre**​ you will have to bolt through the ring clamp all the way to the extrusion with a longer 5mm. ﻿i think it is tapped, so you need to drill it out.


---
**Eric Lien** *March 13, 2015 18:32*

Or just print the V1 holder. That what I use... But I am also the guy who modeled it ;)


---
**Sébastien Plante** *March 13, 2015 18:39*

I tired to open your files in Etc 3D and it doesn't work? Just kidding! 



Nice work, I'll rebuild mine into this soon! 


---
**Ben Delarre** *March 13, 2015 19:27*

**+Eric Lien** V1 holder for the bondeus? I haven't seen that in the eustathios repo, do you have it somewhere else?


---
**Seth Messer** *March 13, 2015 19:54*

**+Eric Lien** not sure if you are expecting it or wanting it, but i submitted a PR for minor readme edits. 


---
**Eric Lien** *March 13, 2015 21:01*

**+Ben Delarre** [http://www.bondtech.se/STL/Extruder%20Mount%20FFF%201.0.stl](http://www.bondtech.se/STL/Extruder%20Mount%20FFF%201.0.stl)


---
**Ben Delarre** *March 13, 2015 21:02*

Aha, didn't see that, thanks!


---
**Eric Lien** *March 13, 2015 22:26*

**+Seth Messer** merged. But I need to get better at knowing how to review request. I am still pretty green at github.


---
**Seth Messer** *March 13, 2015 23:08*

**+Eric Lien** feel free to hit me up with any questions or roadblocks you have with it.


---
**Matt Kraemer** *March 13, 2015 23:16*

Hey **+Eric Lien** do you have a rough estimate of what the BOM costs? It sure looks nice, and I'm starting to get diminishing returns on the delta configuration. It's time for me to switch sides.


---
**Eric Lien** *March 13, 2015 23:24*

**+Matt Kraemer** I am working on getting hardware costs dialed in. Should be done soon. If anybody want to help I would appreciate it. It's a lot if work optimizing product sources for best cost and reliability.


---
**Matt Kraemer** *March 13, 2015 23:30*

Agreed. I've noticed that HFSB5-2020-425-TPW is a discontinued product... 


---
**Eric Lien** *March 14, 2015 00:02*

Not as of yesterday. Weird.


---
**Jeff Kes** *March 14, 2015 03:49*

That looks awesome Eric!


---
**Seth Messer** *March 14, 2015 03:50*

**+Eric Lien** If you're able to confirm some products i'd be happy to update the BOM with more specifics.



1.) for the SSR, which of these 220B's is it? [http://www.amazon.com/s/ref=sr_nr_p_72_0?fst=as%3Aoff&rh=n%3A16310091%2Cn%3A306506011%2Cn%3A306528011%2Cn%3A306578011%2Cn%3A6374820011%2Ck%3Aomron+solid+state+relay%2Cp_72%3A1248921011&keywords=omron+solid+state+relay&ie=UTF8&qid=1426304764&rnid=1248919011](http://www.amazon.com/s/ref=sr_nr_p_72_0?fst=as%3Aoff&rh=n%3A16310091%2Cn%3A306506011%2Cn%3A306528011%2Cn%3A306578011%2Cn%3A6374820011%2Ck%3Aomron+solid+state+relay%2Cp_72%3A1248921011&keywords=omron+solid+state+relay&ie=UTF8&qid=1426304764&rnid=1248919011)



2.) for the fused plug switch, is it this one? [http://www.amazon.com/gp/product/B00511QVVK/ref=ox_sc_act_title_1?ie=UTF8&psc=1&smid=A1PH6OFMJYJKNX](http://www.amazon.com/gp/product/B00511QVVK/ref=ox_sc_act_title_1?ie=UTF8&psc=1&smid=A1PH6OFMJYJKNX)



Finally, I've confirmed that HFSB5-2020-425-TPW is indeed still available on Misumi (just added the necessary quantity to my shopping cart). /cc **+Matt Kraemer** 


---
**Riley Porter (ril3y)** *March 14, 2015 13:20*

**+Eric Lien**​ hey nice man. In the bom you list acrylic or polycarbonate. If you are laser cutting do not use polycarb.its horrible. Produces some nasty yellow gas and does not cut well. Mills fine but just and FYI to all. Been there tried that. Failed.


---
**Eric Lien** *March 14, 2015 13:54*

**+Seth Messer**​ one thing that will help is a vlookup against my Herculien BOM, many parts are the same. I am just kinda tired right now. Lots of late nights, and now my 2 year old has the flu. There is a lot of money to be saved getting the bolts and such from somewhere other than McMaster (like [http://www.trimcraftaviationrc.com/](http://www.trimcraftaviationrc.com/)). ﻿


---
**Joe Spanier** *March 14, 2015 16:10*

Fastenal? I know if your company has an account with them your discount can be nuts.  Mine is like 90%


---
**Eric Lien** *March 14, 2015 16:15*

Yeah but some thing tells me the big yellow heavy equipment company has more pull than mine ;)


---
**Erik Scott** *March 15, 2015 03:35*

Awesome work Eric! Looks like I may be making a few changes to mine. 



A few quick questions: How are you attaching the electronics bracket, SSR, PSU, etc to the underside of the acrylic floor? Glue?



Also, how do you plan to run the wires? Are you going to run them all down the single hole by the Y stepper? What about the wires for the heated bed?



Where can we get an aluminum bed like that cut to shape? 



Sorry, not trying to be annoying. I think I'll be taking a lot of these changes and making a custom, pubic Eustathios variant on onshape. 


---
**Eric Lien** *March 15, 2015 04:58*

**+Erik Scott** I was going to drill holes to mount the PSU and SSR. I forgot to model them. I run wires inside slot channel covers on HercuLien, it works great... But yes I forgot the entry hole. Wires for the heated bed have a cutout already in the back corner.



Cutting the aluminum will take a CNC or a steady hand. I called in a favor for mine. Sorry, I can't help much on that.


---
**Erik Scott** *March 16, 2015 20:05*

I might be able to find somewhere to get the bed cut. Looks like I'm going to have to get more acrylic cut as well. 


---
**Eric Lien** *March 16, 2015 22:28*

For acrylic I used a $10 fine tooth 10" plastic blade and a table saw. A trick I found is to spin the blade and smear the side with a bit of snowboard wax, once I did that cutting was easy and no burning smell due to friction on the side of the blade :)


---
**Erik Scott** *March 17, 2015 01:45*

Thanks for the tip. If it comes to that, I'll keep it in mind. I'm hoping I'm going to be able to get back up to school one of these weekends to see some friends and I may be able to sneak in some laser cutting. 


---
**Gus Montoya** *March 17, 2015 06:35*

So I can order the frame based on the xcell document? I'm good at price hunting but horrible at parts verification. (not sure if that makes sense or not). I buy alot of wrong parts and send them back all the time. 


---
**Eric Lien** *March 17, 2015 10:51*

**+Gus Montoya** to the best of my ability yes... You are good to go.


---
**Sébastien Plante** *March 21, 2015 23:30*

What size is the biggest part to print? ﻿


---
**Eric Lien** *March 22, 2015 04:58*

338x358x320 with room to optimized.


---
**Sébastien Plante** *March 22, 2015 12:49*

Thanks **+Eric Lien**​, but I mean, if I want to print the parts myself, what build size I need to print the bigger one. I will check the STL later to find out.


---
**Eric Lien** *March 22, 2015 15:21*

Ohh. The largest part that mount the bed to the bearing. Any printer should be able to print the parts I believe. I could shorten the bed mounts for you if you run into issues.


---
**Sébastien Plante** *March 22, 2015 15:26*

okay thanks, I'll be making a Canadian BOM before


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/LCKA9z2FFeY) &mdash; content and formatting may not be reliable*
