---
layout: post
title: "It's finally moving! ProTip guys, don't run the Z motor without micro-stepping"
date: September 27, 2015 03:18
category: "Show and Tell"
author: Erik Scott
---
It's finally moving!



ProTip guys, don't run the Z motor without micro-stepping. It's loud as hell! 


**Video content missing for image https://lh3.googleusercontent.com/-j1xYIv4qGdQ/VgdftVdqdHI/AAAAAAAABfg/sFeVdkNWaCU/s0/VID_20150926_222606.mp4.gif**
![images/53901c5f4f18c25449cec924202e7786.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/53901c5f4f18c25449cec924202e7786.gif)



**Erik Scott**

---
---
**Eric Lien** *September 27, 2015 03:28*

It is great to see it finally moving. Congratulations. BTW love the cable chain.


---
**Erik Scott** *September 27, 2015 03:42*

Thanks! Yup, I got everything tuned as best I can. Haven't worked up the nerve to print yet. Maybe tomorrow (That's why i always take so long, I'm always nervous I'm going to screw something up). 



Calibrating the bondtech extruder went really well. Every other time I've attempted to calibrate an extruder, I'd always have slight variations of a few mm over 100 mm extruded. With the bondtech, things are pretty much spot-on. 



Any idea why the Z-motor would be so loud when not microstepping? 


---
**Eric Lien** *September 27, 2015 03:58*

**+Erik Scott** yeah, **+Martin Bondéus**​ did an amazing job on the BondTech extruder. It is consistent as an atomic clock.



For the noise without microstepping on Z, I am not sure. I have never run one without any microstepping.


---
**Frank “Helmi” Helmschrott** *September 27, 2015 06:12*

why would you want to run your z axis without micro stepping?


---
**Erik Scott** *September 27, 2015 15:41*

I know you're supposed to use layer heights in full-step increments, so I figured I'd just forego the micro stepping and run 100 steps/mm on z. Made it super loud! Set it to 1/16 and 1600 steps/mm, and it's as quiet as my old one. 


---
**Frank “Helmi” Helmschrott** *September 27, 2015 19:29*

**+Erik Scott** i only heard theories so far around that fact but have never seen a difference. I do run mine with 1/32 on a 400 steps/rev motor. I do use layer heights like 0.1, 0.15 and 0.2mm and have seen exactly no difference to calculated layer heights that give fancy numbers. not saying that the theories aren't right but for me that makes things more complicated without a noticeable effect.  


---
**Jason Perkes** *September 27, 2015 20:04*

fwiw i use 1/4 stepping on Z for layer height accuracy. i use 2mm pitch lead screws and print layer height usually 0.12mm (sometimes 0.2mm) to match a 0.04mm travel per full step.


---
**Gus Montoya** *September 27, 2015 23:19*

Nice build plate. Wows yours is moving now. I feel so far behind. I haven't touched my printer in months. Maybe I need to change out my motherboard. I still haven't found to make it work. Not even the test code.


---
**Erik Scott** *September 28, 2015 00:45*

Again, thanks for getting that cut for me. The heat bed is working well. 



I get some shudders and resonance when the X and Y axis move in their positive directions. Can't figure out why. I run the break-in code, and it seems fine moving at 150mm/s. It's when it's printing at ~60mm/s and slower that it happens. Can't seem to find where it's coming from. I'm running 400step/rev motors at 1/32 microstepping. 


---
**Eric Lien** *September 28, 2015 00:50*

**+Erik Scott** maybe try a very light oil on the shafts and run the break-in code a few more times. The bushings take a while to find their home. But once they do things really smooth out.


---
**Erik Scott** *September 28, 2015 02:42*

I've been pretty liberal with the motor oil. Do you have a specific oil you recommend?


---
**Eric Lien** *September 28, 2015 20:33*

**+Nathan Walkner** whatever you do do not use grease. These are oilite bronze bushings. Grease clogs the bronze so it never wicks oil out during use. Similarly you never heat bronze bushings or the oil pushes out making them chatter.


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/J378Smsn43q) &mdash; content and formatting may not be reliable*
