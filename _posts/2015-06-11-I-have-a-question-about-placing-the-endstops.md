---
layout: post
title: "I have a question about placing the endstops"
date: June 11, 2015 16:47
category: "Show and Tell"
author: E. Wadsager
---
I have a question about placing the endstops. 

I think about placing x and y endstop in the red cirkel (picture). but if i do that would my end stop be max instead of min?  or should i place them in the green cirkel. 



At the z axis endstop should i place that in the red cirkel to so my endstop will be in first layer hight? 

 is that a max endstop to then? 



![images/e0271e3ca51b120abbfbceb9c130eae9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e0271e3ca51b120abbfbceb9c130eae9.jpeg)
![images/8f76419f83b6bd6c25f297c61a560383.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8f76419f83b6bd6c25f297c61a560383.jpeg)
![images/5bd0fddd0f754088a1fd6c3244fe99c7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5bd0fddd0f754088a1fd6c3244fe99c7.jpeg)
![images/c3b1070932816cbea834896f3ae88e53.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c3b1070932816cbea834896f3ae88e53.jpeg)

**E. Wadsager**

---
---
**Miguel Sánchez** *June 11, 2015 16:57*

It seems you are using LM10UU bearings on side carriages, that seems not to be a good choice for rotating smooth rods. 



I have put my endstops (in a smaller but similar printer) on the frame so one side carriage will press the endstop lever when is close enough. 


---
**E. Wadsager** *June 11, 2015 17:06*

yes I know the bearings is not a good choice . This my first printer and when I started I thought it was . Is it hard to change now i have thought about printing some pla bushings in lm10uu size and replace the bearings.

is that a good choice ? 

or any better suggestions?



would my endstops be max endstops?


---
**Miguel Sánchez** *June 11, 2015 17:13*

You can decide on which side (max or min) you want to put and endstop as far as you configure it consistently in your firmware (that includes the right home direction for each axis). 



I am not sure how bad your linear bearings are for the job, but if you can get it to print you can print yourself new parts for sintered bronze bushings.


---
**Erik Scott** *June 11, 2015 17:15*

You're going to want to put your X and Y endstops in the minimum position, the front left of the printer (your blue circle). The Z-endstop should be at the top of the printer, so the bed stops just before it hits the nozzle. 


---
**E. Wadsager** *June 11, 2015 17:17*

erik scott if i do that is all the endsstops min?


---
**Erik Scott** *June 11, 2015 18:58*

Yes. They should all be min endstops. You can add max endstops too, but you need the min ones for sure. 


---
**E. Wadsager** *June 11, 2015 19:17*

is it not possible  only to use max endstops and place them at the red cirkel?


---
**Erik Scott** *June 11, 2015 19:18*

Not sure, but I wouldn't recommend it. 


---
**Miguel Sánchez** *June 11, 2015 19:19*

**+E. Wadsager** unless you use negative coordinates on both axis the max should happen on two corners instead of one


---
**E. Wadsager** *June 11, 2015 19:25*

i think im just gonna place them at blue cirkel then :)


---
**Eric Lien** *June 11, 2015 20:40*

You can easily have them at the red circle, but y+ will be coming towards you, and x+ will be heading left as you stand at the front of the printer. I ran a printer like this for a while.


---
**Ishaan Gov** *June 12, 2015 18:42*

Is it not possible just to have max endstops and only home to max in marlin? I think that's what the printrbot simple does for its Y-axis (the only printer that I have really worked with), and it should be just as easy for all three axes


---
**E. Wadsager** *June 16, 2015 15:14*

I placed them in the red circle and turned the motor connectors around so:

x +: going left.

y + : going towards you.

z +: going down.


---
**Ishaan Gov** *June 16, 2015 17:22*

**+E. Wadsager**, instead of using them as min endstops, set the Marlin homing direction to MAX on your X and Y axes, and plug the endstops into the MAX header on your controller board


---
*Imported from [Google+](https://plus.google.com/103157635215674778495/posts/eZsWZdh5GCj) &mdash; content and formatting may not be reliable*
