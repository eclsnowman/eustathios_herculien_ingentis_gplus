---
layout: post
title: "If you have the capability to cut wood panels you probably have what you need for plastic"
date: November 24, 2015 15:58
category: "Discussion"
author: David Woods
---
If you have the capability to cut wood panels you probably have what you need for plastic.  Home Depot has various plastic panels varying in thickness and opacity.  If the panels are simply to retain heat or make it look cool you could use suspended ceiling light fixture panels.  They are 2'x4' and translucent with a texture.  More expensively is Lucite clear plastic panels. 



Straight cuts are easy.  Use a straight edge and a utility knife.  Simply score one side then bend it away from the scoring.  it will snap right off.  If you have a jointer you can run the edge over that or use a hand plane or simply sand the edge.



Curves are a bit more challenging.  if you want to make rectangular  opening with curved corners use a hole saw to make the corners first then use the previous technique for the straight lines.  You may find it easier if you score both sides in this case since you cannot bend it as well for the first 3 sides of the opening.



Vinyl sheeting is very forgiving with these methods.





**David Woods**

---
---
**Bud Hammerton** *November 24, 2015 18:47*

Thanks for the response, I am still trying to figure out why some people, including myself, can't see the original post, likely something to do with Android G+ app or the beta G+ web interface. I have a table saw, router table, drill press, sanding machine, etc. Will attempt to do it myself.


---
**Chris Brent** *November 24, 2015 19:33*

So there was context to this post? I was wondering as I don't see anyone asking about panels in any other posts.... Maybe people are trying out the "new" Google+?

Edit: Weird I'm seeing **+Bud Hammerton** 's original question now.


---
**Eric Lien** *November 24, 2015 19:36*

You don't want to use thin panels. They resonate even at 1/8" thick. I found my panels at a local surplus house plastics pile. I paid $35 for all my lexan panels. But that was lucky. There is good pricing on Amazon if you search a bit. Or call local suppliers. There is almost guaranteed to be a local plastics house in the nearest big city. There are many manufacturers that use sheet plastic by the truckloads, so you might find one of them looking to get rid of drop cuts. ﻿


---
**Bud Hammerton** *November 24, 2015 19:41*

I am sure it's the new G+ web interface's fault. I went in and specifically told it to "share publicly" but I still can't see my original question.


---
**Bud Hammerton** *November 24, 2015 19:44*

Now it shows up as a separate post, at least for me. Not connected to this discussion. I have gone back to the legacy G+ web interface.


---
**Bud Hammerton** *November 24, 2015 19:48*

So **+Eric Lien**  would 0.250 in. thick panels be sufficient to minimize resonance? Thicker would be easier to cut on my table saw. Guess I need to take a trip to the HomeDepot, need a new paneling blade.


---
**Jason Smith (Birds Of Paradise FPV)** *November 24, 2015 19:53*

I use 3/16" plexiglass (acrylic) from Amazon on the Eustathios, and it works great.  I used a laser to cut mine, but you could cut purely rectangular panels with a table or circular saw just as easily.


---
**Bud Hammerton** *November 24, 2015 19:59*

I think I will call around locally to see if anyone has any frosted or smoke-tinted panels in 0.1875 - 0.250 inch thicknesses.


---
**Eric Lien** *November 24, 2015 20:05*

**+Bud Hammerton** I forget is this for Eustathios or HercuLien. For HercuLien to enclose it you need a specific width from the BOM so it fits tight in the extrusion track plastic inserts.


---
**Bud Hammerton** *November 24, 2015 20:07*

Eustathios, don't think I want to go to the trouble of fitting the panels in the slot. Thinking more along the Ultimaker bolt the panels to the outside kind of installation.


---
**Jason Smith (Birds Of Paradise FPV)** *November 24, 2015 20:13*

Here's an older pic of my side panels.  I've actually added a couple panels to partially enclose the top since this was taken, among some other upgrades.  [https://goo.gl/photos/26TNQpDQExeRNGTm6](https://goo.gl/photos/26TNQpDQExeRNGTm6)


---
**Bud Hammerton** *November 24, 2015 20:21*

**+Jason Smith** How do you get the finished prints out with no hinged door? :) The dovetail joints, while visually appealing are just more work than I want to do. I am likely just going to use my router and a roundover bit to  fillet the outer edges of the panels.


---
**Jason Smith (Birds Of Paradise FPV)** *November 24, 2015 20:34*

**+Bud Hammerton** , I usually just pull the prints out from the top. My bed surface is PEI, and most materials (abs, pla, petg) pop off fairly easily once the bed drops to 45C or so. If I were to do it again, I'd probably design hinged panels on the front with magnets to hold them closed. Note that I've changed the bed plate and other items since that pic was taken. 


---
**Eric Moy** *November 26, 2015 21:58*

So resonance is a tricky subject. Using thicker material will change your natural frequency, but thicker material costs considerably more. You can cut any chatter by mounting something energy absorbent between the plastic and metal extrusion, like rubber sheet, rtv, popsicle sticks. If the actual sheet is resonating, you can do the proverbial 'slapping some clay on it' by glueing a weight to party of the sheet. This will change the resonant frequency, but it'll take a lot of experimenting, unless you understand vibrations, but it'll be cheaper﻿. Good luck either way


---
*Imported from [Google+](https://plus.google.com/+DavidWoodsTEC/posts/B1BXRaSqtyp) &mdash; content and formatting may not be reliable*
