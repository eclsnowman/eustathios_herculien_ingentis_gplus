---
layout: post
title: "One step closer. I now need to source the aluminum angle L, the aluminum bed and the mdf pocket"
date: June 24, 2015 02:29
category: "Discussion"
author: Dat Chu
---
One step closer. I now need to source the aluminum angle L, the aluminum bed and the mdf pocket. I have been putting off on these since I have had the time to call around town for a metal shop that has them.



If anyone knows a better place to get these in Houston than ordering online, I would love a suggestion.

![images/027bbb6ed5904801f0a1ddbecf52dc3b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/027bbb6ed5904801f0a1ddbecf52dc3b.jpeg)



**Dat Chu**

---
---
**Vic Catalasan** *June 24, 2015 17:12*

Your local hardware store might have the aluminum angles. Looks like your making good progress. I was able to substitute a 1/8"x 1"x 3" angle and with a volcano head it worked out for me.  The added 1" on the vertical will make it more stable as the roller bearings are further apart.



I might change my Z configuration and use 3 ballscrews with linear shafts and not use the V-grove roller bearings


---
**Dat Chu** *June 24, 2015 18:31*

What is a volcano head? Google search doesn't show many anything useful :(


---
**Vic Catalasan** *June 24, 2015 18:32*

I meant volcano hot end from E3D


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/dooLa1rwHVm) &mdash; content and formatting may not be reliable*
