---
layout: post
title: "A friend just lent me his Rigitbot"
date: December 18, 2014 06:42
category: "Show and Tell"
author: Florian Schütte
---
A friend just lent me his Rigitbot. Now i print the last parts for Eutathios. One step closer :) Other missing parts are hotend and heated bed. But they are on their way. I'm so exited to go on with the build next week! Might be the best christmas time ever :D



![images/2741bb9283be640141eea219fea8c524.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2741bb9283be640141eea219fea8c524.jpeg)
![images/011ad1a254b9f9f463d21d4c5751f7ef.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/011ad1a254b9f9f463d21d4c5751f7ef.jpeg)

**Florian Schütte**

---
---
**Eric Lien** *December 18, 2014 07:15*

Can't wait to see your progress.


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/XdCruxuaaSi) &mdash; content and formatting may not be reliable*
