---
layout: post
title: "So frustrated... printed about half of my parts in petg and was printing the carriage..."
date: May 23, 2017 23:14
category: "Discussion"
author: James Ochs
---
So frustrated... printed about half of my parts in petg and was printing the carriage... about halfway through, Marlin crashed and restarted.  Restarted the print, and it did it again.  The second time, I forgot to turn the heat on before  homing and it pulled the bed out of whack.  Got that sorted, releveled the bed, and printed the carriage over usb.  Next thing I went to print (xy tensioners), bed was out of level again.  Re-leveled the bed and now 've been trying to resume printing for three days and the petg won't stick to anything but the nozzle regardless of z height.  AAARGH!



Sorry had to vent 👿





**James Ochs**

---
---
**Eric Lien** *May 24, 2017 00:14*

We have all been there. But don't get discouraged. Everything worth doing is hard work. But there is light at the end of the tunnel :)


---
**James Ochs** *May 24, 2017 00:54*

Hehe, apparently all I needed to do was whine about it 😀. Back printing! Not the most perfect prints but it will get the job done

![images/c76f64ca4a7881aa9808cb41375a22de.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c76f64ca4a7881aa9808cb41375a22de.jpeg)


---
**Brad Vaughan** *May 24, 2017 00:56*

Nice!  You shamed it into submission.  Haha.


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/1G5ytsTU7Gf) &mdash; content and formatting may not be reliable*
