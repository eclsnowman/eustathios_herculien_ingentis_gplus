---
layout: post
title: "First test piece for in carriage direct drive extruder module for the HercuLien"
date: August 26, 2014 05:57
category: "Show and Tell"
author: Eric Lien
---
First test piece for in carriage direct drive extruder module for the HercuLien.


**Video content missing for image https://lh4.googleusercontent.com/-8Mb0UBuREg4/U_whxxkI-3I/AAAAAAAAi_Y/pQ9T613HFEU/s0/VID_20140825_233637.mp4.gif**
![images/0d0e14b59fd7d5d2020446de69fd4db4.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0d0e14b59fd7d5d2020446de69fd4db4.gif)



**Eric Lien**

---
---
**Øystein Krog** *August 26, 2014 06:26*

Hahaha, nice!


---
**Ricardo de Sena** *August 26, 2014 09:11*

Great!!!


---
**Maxim Melcher** *August 26, 2014 10:10*

Fast furious! ;-)  Is it PLA or ABS? 


---
**Eric Lien** *August 26, 2014 10:39*

**+Maxim Melcher** ABS


---
**Eric Lien** *August 26, 2014 11:16*

Big thanks to **+Tim Rastall** & **+Jason Smith**. My #Eustathios ( #Ingentis variant ) is such a great printer to have. Really a speed demon. Again this is only 50mm/s speeds. But with the machines high acceleration values print completion times are really quick.



 It allows me to prototype model, print, review, tweak the model, then print the final at higher quality settings in a small time frame. Without it I could have never made my new #HercuLien large format printer.﻿


---
**Jean-Francois Couture** *August 27, 2014 00:05*

Like you said, that looks fast ! what are the temps used ? (head / bed)


---
**Eric Lien** *August 27, 2014 00:14*

**+Jean-Francois Couture** Hotend=239C, Bed=110C


---
**Jean-Francois Couture** *August 27, 2014 00:16*

I'm about to start a ABS print as we speak.. thats about what I have on my prusa2.. head= 230, bed= 110C


---
**Eric Lien** *September 26, 2014 18:55*

I will look tonight to make sure I still have it.


---
**Eric Lien** *September 26, 2014 19:11*

**+Shauki Bagdadi** this was printing the on carriage direct drive version. No tabs for mounting, and holes added to bolt it down to the carriage top. Plus a nub that acts as an alignment pin into an existing hole.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/C1sQ7gfaJeE) &mdash; content and formatting may not be reliable*
