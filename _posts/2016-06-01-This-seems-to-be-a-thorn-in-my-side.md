---
layout: post
title: "This seems to be a thorn in my side"
date: June 01, 2016 15:24
category: "Discussion"
author: Brandon Cramer
---
This seems to be a thorn in my side. This happens to me every time I apply the 3D-EEZ film. This time it's really bad. I'm posting it on here hoping more people will see it and have recommendations.  



I cleaned the glass off with soap and water and scrubbed the heck out of it to clean it off. I wiped it dry and let it set for a bit. I applied 3 coats of film alternating directions every time with about an hour between coats.  

![images/58f65711bd5dc78e638e88c82abc9d93.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/58f65711bd5dc78e638e88c82abc9d93.jpeg)



**Brandon Cramer**

---
---
**Mike Miller** *June 01, 2016 15:43*

Perhaps you need to clean with alcohol?


---
**Eric Lien** *June 01, 2016 15:49*

Is that during part removal?


---
**Brandon Cramer** *June 01, 2016 15:58*

Yes that is what happened when I removed the part. 


---
**Eric Lien** *June 01, 2016 16:01*

Try a little bit higher from the bed in the nozzle height (less squish), and a little bit colder on the bed temp. May just be bonding too well. Also as mentioned clean with acetone or alcohol first (or window cleaner), then apply to the glass.


---
**Gary Hangsleben** *June 01, 2016 16:40*

Yep, have to clean with alcohol.  For a big print I'll let the bed cool a bit to help the release.  Also have a flat metal spatula 1" wide to help start lift.  But even on a stubborn removal, never had the surface pull off like that.


---
**Brandon Cramer** *June 01, 2016 16:44*

I will clean my other piece of glass with alcohol. I don't have a heated bed. I shouldn't need one for PLA right? Also this print sat for at least 8 hours before I tried to gently peel it up knowing that I would have this problem.


---
**Mike Miller** *June 01, 2016 17:10*

PLA works great cold...works great with hairspray, or Glue stick, or 3m blue tape rubbed down with rubbing alcohol.


---
**Brandon Cramer** *June 01, 2016 17:51*

I cleaned the glass with alcohol and I can already tell by putting on the first coat that the film sticks a lot better. I will test it out tomorrow and see how it works. 






---
**Martin Bogomolni (MB)** *June 01, 2016 18:10*

If you have a heated bed, you must set the bed to ~55C when applying the EEZ.  Wait till each layer is translucent between coats, and no more than 3 coats.  If you do NOT have a heated bed, use only one coat.


---
**Brandon Cramer** *June 01, 2016 18:18*

Seriously just the one coat?


---
**Eric Lien** *June 01, 2016 20:49*

**+Martin Bogomolni** I had problems with applying it on a hot bed. I had the best results alcohol wiping the glass, then applying the first coat. Let it air dry between coats, or put a fan near by so it evaporates the water faster so you can apply the remaining coats quicker.


---
**Martin Bogomolni (MB)** *June 01, 2016 20:57*

Yes.  On non heated build plates, just one coat.  And you must apply it with the plate at 55C if you have a heated bed.    If you dont follow the instructions, the result is as you see.. bad adhesion and ripping.


---
**Martin Bogomolni (MB)** *June 01, 2016 20:58*

If you are curious what eez is.. it's just PVA glue.  Seriously.  Its just fancy Elmer's Glue.


---
**Øystein Krog** *June 02, 2016 21:22*

I never had this problem when applied straight onto an aluminum bed, maybe your glass is too smooth/clean?

My bed is an MK3 alu bed, I apply while cold then heat up to cure, multiple layers.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/f4w8E8cHYMB) &mdash; content and formatting may not be reliable*
