---
layout: post
title: "Another Eustathios Spider (V1) in the making"
date: March 27, 2015 19:44
category: "Show and Tell"
author: Isaac Arciaga
---
Another Eustathios Spider (V1) in the making. I finally got around to starting this build last night. I will most likely keep it as V1 since I already had the BoM for it and prefer to have my motors tucked away.



Couple tips that will speed up frame assembly:

If I had to do the frame again I would use post install t-nuts. It will save a bunch of time not having to keep looking at the cad model to verify t-nut placements for pre-loading.

The other is printing out alignment spacers to line up all of the horizontal extrusions (see photo). You'll need these lengths, 65mm, 82mm and 100mm (80mm for V2). Anyone can probably create them on TinkerCad.



![images/2d58dba32b70a0be34329c1cc51ef7e3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2d58dba32b70a0be34329c1cc51ef7e3.jpeg)
![images/06191a0c2d6e0b47747bd684d29d4153.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/06191a0c2d6e0b47747bd684d29d4153.jpeg)

**Isaac Arciaga**

---


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/NjoJv9ZxxqP) &mdash; content and formatting may not be reliable*
