---
layout: post
title: "After months of a slow build (due to limited funds) I finally got my Herculien build moving last night and had some unexpected issues"
date: July 31, 2015 13:58
category: "Discussion"
author: Zane Baird
---
After months of a slow build (due to limited funds) I finally got my Herculien build moving last night and had some unexpected issues. I had relatively smooth movement in both X and Y when I realized I had installed the carriage backwards (I'm using the Eustathious Spider carriage with the cooling duct - v4). When I reinstalled the carriage the correct direction the X axis of the carriage now has a lot of resistance to motion. I tried spinning the rod with a drill (for about 15 minutes) which helped very slightly, but it is not nearly as smooth as the Y axis motion. I have smooth motion everywhere in the gantry, other than along the X axis of the carriage. The cross-rod running the Y direction is visibly flexing when manually performing an X-axis move...



Does anyone have any thoughts on how I might remedy this? I will try some 3 in 1 oil on the X cross-rod tonight, but I'm wondering if I should just replace the bushings for the X-axis of the carriage.  Has anyone experienced 1 or 2 bushings in a set that did not perform as well as the rest?





**Zane Baird**

---
---
**Bryan Weaver** *July 31, 2015 15:04*

I'm curious about this too.  The bushings on my extruder carriage are as smooth as can be, as well as the bushings on 2 of my XY belt tensioners.  The other 2 belt tensioners, however, have quite a bit of resistance.  I just can't quite seem to break them in like all the others.  Curious is there are any tips or secrets I was missing out on.


---
**Brandon Cramer** *July 31, 2015 15:08*

Not sure if this is the issue, but is the spacing the same on the right versus the left? I had that issue which caused a lot of tension on sliding the carriage back and forth. 


---
**Brandon Cramer** *July 31, 2015 15:09*

**+Zane Baird** I had the same exact issue yesterday with the carriage being backwards. Doh!!!


---
**Zane Baird** *July 31, 2015 15:41*

**+Brandon Cramer** the spacing is the same and is fixed by the corner brackets on the Herculien. I noticed this problem when I had the carriage and cross rods disconnected from the guide rods so I'm confident it is not due to alignment. I will try some extra lubrication tonight, but may have to replace the bushings if that doesn't work. On that note, does anyone know of an easy way to remove the press-fit bushings without destroying the carriage/bushings? I haven't figured that out just yet....


---
**Bryan Weaver** *July 31, 2015 15:57*

I'm getting resistance before anything is even installed, just sliding the XY tensioner back and forth on 10mm precision rod.


---
**Zane Baird** *July 31, 2015 16:08*

**+Bryan Weaver** That is the same thing I noticed, but with the x axis of my carriage only. If oil doesn't fix it I may just take a pass on those bushings with some 1200 grit sand paper... 


---
**Chris Brent** *July 31, 2015 17:28*

I think the documentation and advice around here is pretty darn good considering this is a totally open source project and nobody works on it full time!


---
**Vic Catalasan** *July 31, 2015 17:30*

I would probably pull all the rods out again and run the rods through a drill to align the bearings again. Make sure you adjust the belts on both sides again. I would not assembly anything that just do not slide on their own weight. After assembling make sure you take some measurements between rods. I would recommend using a caliper. 


---
**Chris Brent** *July 31, 2015 17:36*

Insert the rod into each side of the carriage and see if it's visually unaligned. Sometimes you can wiggle them into a better starting alignment, if they're too far out they won't move. They also only have a limited range of movement to self align (3 or 5 degrees) so if they're press fit too far out of alignment you'll never get them to align. You can check for that by doing the same thing. Only put the rod in a short distance and the long end hanging out should show up any weird alignment issue.


---
**Eric Lien** *July 31, 2015 21:27*

One issue is in order to make the Eustathios carriage more compact one of the cross rods had to be moved out to the edge so the hotend can be move inside the carriage envelope. This means it has different frictional characteristics moving one direction than the other. This is not the case on the HercuLien carriage which has the rods crossing at the carriage center. This could be changed by someone on the Eustathios carriage... But not without making it larger.



In my opinion the HercuLien carriage is a better fit for the larger printer as it is more stable. But both will work fine on a properly aligned machine.


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/5hnZTWC8LRX) &mdash; content and formatting may not be reliable*
