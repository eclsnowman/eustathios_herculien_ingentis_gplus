---
layout: post
title: "I have begun recreating all the Eustathios files in Onshape"
date: March 21, 2015 05:14
category: "Discussion"
author: Erik Scott
---
I have begun recreating all the Eustathios files in Onshape. I have made the document public and you can find the link below. You can also probably just search the public documents for "Eustathios". Of course, this assumes you have got a Beta invite to Onshape. 



Right now, only I can edit it, but I would like to add collaborators. Of course, **+Eric Lien** , **+Jason Smith** , and **+Tim Rastall** will all be added to the list, if they want. If any one else would like to work on it, please let me know. 





**Erik Scott**

---
---
**Mike Brown** *March 21, 2015 05:41*

I requested an invite through the website and got one pretty quick.  Found Eustathios.  Nice work.


---
**Eric Lien** *March 21, 2015 11:35*

I look forward to seeing what group CAD can accomplish.


---
**Joe Spanier** *March 21, 2015 11:58*

How is it so far? My beta invite just went through but I haven't had time to even log in yet. I'm looking forward to touch optimized cad. 


---
**Erik Scott** *March 21, 2015 15:12*

Its got its good points and bad points. I'm fairly familiar with solidworks from school, and seeing as it's by some of the original creators, it's very similar and so fairly easy for me to pick up. 



I feel like top down design is a lot easier as you relate dimension of parts to each other in these part studios rather than in assemblies. 



They went with a very flat, minimal UI, which is nice to look at, but I find it difficult to find things in the feature tree at a glance. There are no icons or colors that make it easy to differentiate a sketch from an extrude, from a fillet, etc. There is also no indecation of how things are related. 



Also, if you don't have a great connection, things lag, and sometimes you'll get thrown out of the document, requiring you to repeat a sketch. This is especially annoying if it was a complex one. 



However, the fact that it's free and collaborative makes it worth giving a real shot. I don't want to write a whole review here, so I won't. ﻿


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/7t17BLSgAA9) &mdash; content and formatting may not be reliable*
