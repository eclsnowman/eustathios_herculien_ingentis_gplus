---
layout: post
title: "Hey guys, has anyone come up witha better cooling duct for the Euth V2 regular carriage?"
date: February 28, 2016 19:57
category: "Discussion"
author: Derek Schuetz
---
Hey guys,

has anyone come up witha better cooling duct for the Euth V2 regular carriage? I remember **+Eric Lien**  made one but it does not work with my Volcano Nozzle. the only issue i have with the traditional cooling duct is it cools everything around the nozzle which is not as effective when printing small perimeters, also its really big and a pain to have to undo to do nozzle maintenance



thanks in advance





**Derek Schuetz**

---
---
**Eric Lien** *February 29, 2016 11:58*

**+Derek Schuetz**​ see the duct from **+Frank Helmschrott**​ in this post:



[https://plus.google.com/+FrankHelmschrott/posts/Jssk4TeiZxG](https://plus.google.com/+FrankHelmschrott/posts/Jssk4TeiZxG)


---
**Derek Schuetz** *February 29, 2016 17:25*

Thank you **+Eric Lien** printing it now


---
**Eric Lien** *February 29, 2016 18:29*

Also: [http://www.thingiverse.com/thing:1066144](http://www.thingiverse.com/thing:1066144)


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/VYu4FJNVjB5) &mdash; content and formatting may not be reliable*
