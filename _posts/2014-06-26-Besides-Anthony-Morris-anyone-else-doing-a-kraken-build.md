---
layout: post
title: "Besides Anthony Morris , anyone else doing a kraken build?"
date: June 26, 2014 16:51
category: "Discussion"
author: Tony White
---
Besides **+Anthony Morris**, anyone else doing a kraken build? I got my lm8u bearings in from the BOM, but realize the previously posted kraken mount probably is sized for brass bushings no? Should I order them or modify the carriage design? I only have the two lm8uus.....



![images/979314b21da9d85794f258ddb5cebbe9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/979314b21da9d85794f258ddb5cebbe9.jpeg)
![images/d96a509acd773e5d8d47e07a267a4b2c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d96a509acd773e5d8d47e07a267a4b2c.jpeg)

**Tony White**

---
---
**Eric Lien** *June 26, 2014 17:13*

I would modify the carriage. Let us know how it works with two.


---
**Eric Lien** *June 26, 2014 17:14*

If one per axis seems insufficient I would go with 1 long bearing versus two smaller ones.


---
**Mike Miller** *June 26, 2014 17:28*

So...am I missing the bearings for the other direction? Is this part of a multi-part, uh, part?


---
**Tim Rastall** *June 26, 2014 20:22*

Like **+Eric Lien** says,  look for lm8luu the extra 'l'  is for long :) 


---
**Brian Bland** *June 26, 2014 21:04*

**+Mike Miller** Yes it is has multiple parts.  This was done to prevent splitting when pressing in the self aligning bushings.


---
**Tony White** *June 26, 2014 23:59*

The lm8uu parts are a loose fit, but you guys seem to be saying that it's worth redesigning the ID of the carriage bore for the shaft to accommodate the lm slider bearings, rather than using the brash bushings used on the x and y drive points?


---
**Eric Lien** *June 27, 2014 00:10*

The bronze is fine. But looking at the speeds Jason and Riley are hitting using bearings I am contemplating the upgrade myself.


---
**Tony White** *June 27, 2014 02:08*

bearings it is. I'l post the revised kraken carriage for lm8uluu (8u long bearings) next week.


---
**Eric Lien** *June 27, 2014 02:12*

**+Anthony White** that's the great thing about having two 3d printers. If you don't like it... You can just print something else to try. 


---
**Tony White** *June 27, 2014 02:14*

haha the Eugentis will be my third! Mendel 90 and a Rostock Max (sorely in need of a better effector carriage / linkages) are my workhorses. I plan on putting the Kraken through its paces to say the least


---
**Mike Miller** *June 27, 2014 18:02*

We need to come up with some kinda taxonomy...I'm using Ingentis parts, but at this point am NOT using rod rotation to move the effector. And looking at Latin conjugations is making my brain hurt. 


---
**James Rivera** *June 28, 2014 00:57*

veni vidi um...printi? :-)


---
*Imported from [Google+](https://plus.google.com/+AnthonyWhiteMechE/posts/GxxxLdDM3gZ) &mdash; content and formatting may not be reliable*
