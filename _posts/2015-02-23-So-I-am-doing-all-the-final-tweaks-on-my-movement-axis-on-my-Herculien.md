---
layout: post
title: "So I am doing all the final tweaks on my movement axis on my Herculien"
date: February 23, 2015 22:15
category: "Discussion"
author: Derek Schuetz
---
So I am doing all the final tweaks on my movement axis on my Herculien. I got my x and y to have no binding but my z axis is giving me issues. If I keep the z couplers loose and not completely bolted down it moves smoothly but if I tighten instant binding to the axis. I have tried everything and can't stop the binding. Am I suppose to leave the couplers loose?





**Derek Schuetz**

---
---
**Eric Lien** *February 23, 2015 23:01*

**+Derek Schuetz**​​ do you mean the lead nuts? Does it bind at the top or the bottom of travel? The assembly doesn't leave much room for miss alignment. That is why the bottom mounts are spaced away from the z vslot. Then they can be moved in/ out to align. The top leadscrew bearing mount is fixed you may need to shim or shave the top plastic mount for perfect alignment if them printed parts or extrusion cuts aren't perfectly in line.﻿


---
**Derek Schuetz** *February 23, 2015 23:04*

It bind everywhere if I tighten the lead screw nuts it makes no sense but if I leave them loose it moves just fine


---
**Eric Lien** *February 23, 2015 23:09*

Also get some of this for the lead screw: Super Lube Synthetic Grease with Syncolon Multi Purpose Lubricant 3 oz Super Lube [http://www.amazon.com/dp/B000XBH9HI/ref=cm_sw_r_udp_awd_Wl76ub0DR9PVE](http://www.amazon.com/dp/B000XBH9HI/ref=cm_sw_r_udp_awd_Wl76ub0DR9PVE)


---
**Derek Schuetz** *February 23, 2015 23:13*

**+Eric Lien** is it ok to leave the lead nuts loose it doesn't seem to add any play to the build plate


---
**Eric Lien** *February 23, 2015 23:13*

**+Derek Schuetz** if it binds everywhere things are not properly aligned and squared. It does take some time to break in and move smooth once lined up. The grease should help speed that up.


---
**Eric Lien** *February 23, 2015 23:18*

**+Derek Schuetz** did you use the correct architectural angle on the bed. That has squared inside corners. I wonder if the lead nut is twisting off axis because the bottom of the angle is sloped somewhat off perfectly horizontal. That would cause a bind like you are describing.



Leaving it loose will likely give Z artifacts since z height has some slop as the nut can move up or down.


---
**Derek Schuetz** *February 23, 2015 23:30*

Ok I'll check to make sure everything is square again I guess it could be a deformity to the angle brackets. It occurred to me why don't we use extrusion for the gauntry plate to reduce the amount of drilling and metal working pieces? 


---
**Eric Lien** *February 23, 2015 23:36*

**+Derek Schuetz** I have thought about it. If you have some ideas I would love to see them. This bed design is a remnant of my original corexy design and could probably use a refresh.


---
**Derek Schuetz** *February 23, 2015 23:40*

Ok I'll see what I can mock up would just like to know if possible before I make the order. Il try to throw it together tonight 


---
**Eric Lien** *February 24, 2015 00:04*

**+Derek Schuetz** thanks. I would love to get more things updated and improved. Any help from the community is greatly appreciated.


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/MLxq3Fhc8u2) &mdash; content and formatting may not be reliable*
