---
layout: post
title: "So the belts listed on the BOM for Herculien will not be shipping till march 22nd I found a slightly shorter 226mm belt on eBay but I'm not sure if it would be better to go slightly longer or slightly shorter"
date: February 19, 2015 02:27
category: "Discussion"
author: Derek Schuetz
---
So the belts listed on the BOM for Herculien will not be shipping till march 22nd I found a slightly shorter 226mm belt on eBay but I'm not sure if it would be better to go slightly longer or slightly shorter. Or **+Eric Lien** or others have an alternate source?





**Derek Schuetz**

---
---
**Dat Chu** *February 19, 2015 03:05*

I got all my belts from [robotdigg.com](http://robotdigg.com) I believe you can move the motors mount a bit to make up for the difference. 


---
**Eric Lien** *February 19, 2015 03:09*

Is my BOM from robotdigg or misumi? Its been a while and I am out.﻿


---
**Seth Messer** *February 19, 2015 03:11*

**+Eric Lien** [robotdigg.com](http://robotdigg.com)


---
**Eric Lien** *February 19, 2015 03:47*

I I would wait then. You need the steppers anyways.


---
**Derek Schuetz** *February 19, 2015 04:07*

I would love to wait but I already have everything except heated bed insert and belts :(. I guess if heated bed is delayed a while I can just work on my Kossel 


---
**Eric Lien** *February 19, 2015 04:44*

You can try shorter, but it might be too short, I will have to check. Also confirm the eBay seller is not China based too.


---
**Derek Schuetz** *February 19, 2015 04:56*

Bad experience with Chinese belts?


---
**Eric Lien** *February 19, 2015 05:23*

No, but same Asian new year issue as everyone over there.


---
**Eric Lien** *February 20, 2015 04:32*

Here is cheaper than SDPSI for the closed belts: [http://www.robotdigg.com/product/282/228mm-or-232mm-length,-6mm-width,-Closed-loop,-GT2-belt](http://www.robotdigg.com/product/282/228mm-or-232mm-length,-6mm-width,-Closed-loop,-GT2-belt) adding it to the BOM now.


---
**Derek Schuetz** *February 20, 2015 04:54*

i put in an order since they gave me 20 credit


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/dQYCRMPyKTu) &mdash; content and formatting may not be reliable*
