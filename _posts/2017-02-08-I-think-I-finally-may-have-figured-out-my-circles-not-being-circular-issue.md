---
layout: post
title: "I think I finally may have figured out my circles not being circular issue..."
date: February 08, 2017 15:29
category: "Discussion"
author: Sean B
---
I think I finally may have figured out my circles not being circular issue...  It appears one of the bushings is sloppy as all hell...  See the video below (sorry for the quality) and it looks like it is moving back and forth.  Has anyone seen this before?  The only thing I can think is that I wore it open when using a drill to get the carriage to slide smooth.  I am still surprised it's this loose though.  I guess the only fix is to buy a new bushing.  Any advice?﻿


**Video content missing for image https://lh3.googleusercontent.com/-UhEijj1jkic/WJs5RKfsemI/AAAAAAAAa9k/zxfzht90nWoFPVBERDBLONXHjBwvcDx7wCJoC/s0/VID_20170207_213817.mp4**
![images/dc971f6c143d1b7eea2e8b3da1d0f3f5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dc971f6c143d1b7eea2e8b3da1d0f3f5.jpeg)



**Sean B**

---
---
**Maxime Favre** *February 08, 2017 16:03*

Ouch if there's gap, yes you can order a new one. And yes it has a responsibility on your circles issue.


---
**Miguel Sánchez** *February 08, 2017 16:24*

you can use a shim of paper to temporarily fill in the gap and  make sure this is the cause of your bad circles (which I think it is). Most likely your smooth rod or bushing are out of specs for such a large tolerance.


---
**jerryflyguy** *February 08, 2017 18:00*

Wow! Do you have photo of what the parts look like when printed with a bushing that loose?


---
**Eric Lien** *February 08, 2017 23:21*

It looks like you are using the 10mm bushings on the 8mm shafts? Are you sure you have the right bushings?


---
**Eric Lien** *February 08, 2017 23:23*

There are two different bushings used on Eustathios. 10mm on the side rods, and 8mm on the cross rods to the carriage.


---
**Sean B** *February 09, 2017 03:36*

Yeah I am fairly certain it is the correct bushing, once I pop it out I'll measure it.  The video makes it looks worse than it is.  Without the illuminated carriage you can't see it.  I will order another one and see about replacing it.  Although with a newborn in the house it is more on the priority list.



Jerry, I posted some parts on here a few months ago, most parts are actually perfect, except for radii and holes.  I'll see if I can slide some paper in to take up the slack and do a test print.


---
**Sean B** *February 12, 2017 00:16*

Just to update everyone.  The gap is too thin to slide paper in.  I tried a few thicknesses but haven't had any luck.  I also noticed the 8mm bearing is still on backorder as many here have already noted.


---
**Eric Lien** *February 12, 2017 01:32*

**+Sean B** Looks to be availible here: [https://www.ultibots.com/8mm-bronze-bearing-4-pack-a-7z41mpsb08m/](https://www.ultibots.com/8mm-bronze-bearing-4-pack-a-7z41mpsb08m/)


---
**Eric Lien** *February 12, 2017 01:33*

and 10mm here: [https://www.ultibots.com/10mm-bronze-bearing-4-pack-a-7z41mpsb10m/](https://www.ultibots.com/10mm-bronze-bearing-4-pack-a-7z41mpsb10m/)


---
*Imported from [Google+](https://plus.google.com/118220576483582342031/posts/UUoWCytSxNk) &mdash; content and formatting may not be reliable*
