---
layout: post
title: "Tim Rastall This is what I was talking about having the ability to tension the belt in the slide itself.That block is free floating that gets pulled together with the 2 screw holes"
date: March 15, 2014 00:29
category: "Discussion"
author: Riley Porter (ril3y)
---
**+Tim Rastall** This is what I was talking about having the ability to tension the belt in the slide itself.That block is free floating that gets pulled together with the 2 screw holes.  BTW this is my little printer I have been playing with.



I am going to laser cut some parts I need tonight.  I am not too far from assembling!



![images/f1430e729ea9c90332c7ee30a54affe9.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f1430e729ea9c90332c7ee30a54affe9.png)
![images/1cb4d221331e958bbb88fa7df8eab95e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1cb4d221331e958bbb88fa7df8eab95e.png)
![images/d17d59b6aa3f772817e265cf0d98525d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d17d59b6aa3f772817e265cf0d98525d.png)

**Riley Porter (ril3y)**

---
---
**Tim Rastall** *March 15, 2014 01:53*

Nice.  I'd not spotted that the second block was free floating.  How are you feeling about the plastic holding up to the tension? Nice bot design btw. 


---
**Denys Dmytriyenko (denix)** *March 15, 2014 01:59*

What CAD are you using to design/draw that?


---
**Jason Smith (Birds Of Paradise FPV)** *March 15, 2014 02:04*

**+Denys Dmytriyenko**, **+Tim Rastall**.  This is the same design that the Eustathios uses. Both designs were done in sketchup. There is absolutely 0 play/slop with gt2 belts. 


---
**Riley Porter (ril3y)** *March 15, 2014 02:48*

**+Jason Smith** I posted this because I was unsure how to post a photo to **+Tim Rastall** 's post.  So I just posted a pic on a new "thread".  Basically I really like **+Tim Rastall** 's new design for the XY sliders.  I was saying it would be perfect we could fit in a tensioner. 


---
**Tim Rastall** *March 15, 2014 04:05*

**+Riley Porter** I will combine the two tonight :) 


---
**Riley Porter (ril3y)** *March 15, 2014 04:31*

That will be insane! I love it.


---
*Imported from [Google+](https://plus.google.com/+RileyPorter/posts/FpnR9y6b7aX) &mdash; content and formatting may not be reliable*
