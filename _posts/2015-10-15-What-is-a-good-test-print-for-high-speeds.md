---
layout: post
title: "What is a good test print for high speeds?"
date: October 15, 2015 00:44
category: "Discussion"
author: Zane Baird
---


What is a good test print for high speeds? I just installed a 0.4mm Volcano nozzle on my Herculien and I'm looking to push the limits.  @0.2mm layer heights I'm printing external perimeters at 50 mm/s for quality, but I've bumped all other print speeds up to 80-125 mm/s and my initial test prints are turning out fantastic...  I've officially given up my plan for a v6-volcano dual extruder after seeing the print quality I'm getting now.



Larger nozzles have been a fun for drafts (also, large layer heights are sexy for the right print), but the 0.4 nozzle is a great balance with 

quality and speed on the Volcano. With the longer melt zone I feel like I should be able to push the boundaries with speed while still keeping quality at a premium (so long as I keep a relatively low acceleration on external perimeters).



I would like to eventually get 200 mm/s print speeds on the infill (just for fun) and videos of test prints will be posted....



Also: a big shout out to Sublime (don't know how to link on Google+), **+Tim Rastall**, **+Jason Smith**  , and **+Eric Lien**  for providing all the background and design for these printers. Glad I stumbled across this and I'd be lost without your knowledge.





**Zane Baird**

---
---
**Jim Stone** *October 15, 2015 00:46*

Volcano doesn't get its speed from moves. Volcano gets it from thicker layer heights and sheer quantity of material 


---
**Jim Stone** *October 15, 2015 00:47*

And my standard v6 0.4 keeps jamming. So I kinda don't like e3d atm:P  3mm direct


---
**Eric Lien** *October 15, 2015 01:03*

**+Zane Baird** the best items for high speed printing are large objects. Something where the layer times are long enough that previous layers have enough time to cool prior to heating them again with the next layer. For example a high speed printing a single wall vase is difficult since you are back around do the layer again so quickly. Also Items without much retracts. The reason is even at a retract speed of 80mm/s you start running into issues getting a retract and re-prime completed by the time the nozzle gets to the next feature.



So large filled objects benefit a lot from the higher speeds. I agree, 50->80mm/s on perimeters is a nice balance of speed and quality, while infill as fast as you can push it works great. Also using infill every X layers does wonders. Perimeters can still be high resolution, while infill can be taller layers making for stronger infill and less print time.



Best of luck, can wait to see some high speed video posts.


---
**Zane Baird** *October 15, 2015 01:05*

**+Jim Stone**, The thicker layers are an effect of more thermal energy transferred to the filament due to the longer melt zone. This can be translated in two ways: 1) increased layer height, 2) increased print speed. I'm aware of movement limitations, but I'm able to move at >300 mm/s on my Herculien currently so the movement isn't an issue. Acceleration comes into play at those speeds because it's limited by the frequency at which velocity along an axis is updated (ticks per second in smoothieware , or acceleration if we weren't dealing with discrete units). However, the big barrier for most hotends is volumetric flow rate. Thermoplastics (especially PLA and ABS) don't transfer heat well, so the longer melt-zone serves to provide better heat transfer to the filament, thus providing an appropriate viscosity and temperature for interlayer bonding and extrusion, respectively.

A smaller nozzle has a surprising effect... The pressure required to push a liquid of constant viscosity through an orifice increases with R^4. Meaning that reducing nozzle diameter is an increase in back-pressure by the same amount. Thus smaller nozzle struggle less with ooze.



Better explanation of nozzle size decrease. Cutting the diameter in half (2 fold) means that to maintain the same flow rate, a back-pressure of 2^4=24, or 2400% the original pressure is needed. This is the reason people struggle with the 0.25mm nozzle clogging so frequently. This also neglects all friction from the filament in the melt zone, so the number is even larger in reality.


---
**Zane Baird** *October 15, 2015 01:15*

**+Eric Lien** Do you have any good examples of good tests (preferably something another member of the community could use afterwards)? I understand the limitations, just looking for the best trial of those.


---
**Eric Lien** *October 15, 2015 02:22*

Most of my large prints were for my old job, so they are proprietary customer IP. So unfortunately I don't have any I could share. My largest was almost my full usable area 310^3 if I recall. I set it as fast as I dared and let it run for days ;) but for long printing at high speeds I recommend some heat sinks on the steppers. I use old thick northbridge heatsinks I had from old work dell PCs.﻿


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/CCNWdZArcUg) &mdash; content and formatting may not be reliable*
