---
layout: post
title: "Are folks still building Eustathios printers? I printed a mostly-complete set of parts to torture-test my printer and would like to get them to someone who is going to build since I have less cycles at the moment"
date: July 16, 2016 18:40
category: "Discussion"
author: Brian Jacoby
---
Are folks still building Eustathios printers?  I printed a mostly-complete set of parts to torture-test my printer and would like to get them to someone who is going to build since I have less cycles at the moment.



Pretty low bar, maybe reimbursement for shipping (from US).





**Brian Jacoby**

---
---
**Samer Najia** *July 16, 2016 20:25*

Hmmm, do I need another printer?  Am I a 3D Printer hoarder?  Why yes, yes I am.


---
**Brian Jacoby** *August 04, 2016 17:34*

Ok, let me know when/if you're next at Nova Labs....


---
**Samer Najia** *August 04, 2016 17:35*

Ha...how about in a blue moon.  I can cover shipping and all that.  I thought someone picked these up but I can come out someday or meet you somewhere.


---
*Imported from [Google+](https://plus.google.com/102160258356530092878/posts/Y4o4fhjRx63) &mdash; content and formatting may not be reliable*
