---
layout: post
title: "Made a quick mount for a cable chain to carry the heated bed wires"
date: August 08, 2015 18:16
category: "Discussion"
author: Erik Scott
---
Made a quick mount for a cable chain to carry the heated bed wires. It would make the printer look nicer and cleaner, but I am a bit worried this may put some forces on the bed that may be undesirable and affect print quality. The chain will ride on the left-rear vertical extrusion. I couldn't make this work in the CAD assembly, unfortunately, but it will in real life. Thoughts?



![images/e9d7dec512070e58fe5166144980321d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e9d7dec512070e58fe5166144980321d.png)
![images/2d4210b7dd3497f12a81d97f8c151f8d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2d4210b7dd3497f12a81d97f8c151f8d.png)
![images/d40a3b731e252d26497aad15e5e58d3d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d40a3b731e252d26497aad15e5e58d3d.png)
![images/565dbbc14f95ba6ecaf7542d250abba5.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/565dbbc14f95ba6ecaf7542d250abba5.png)

**Erik Scott**

---
---
**Eric Lien** *August 08, 2015 18:48*

I might try to have it connect closer to the rods to avoid it pulling the bed out of tram.


---
**Erik Scott** *August 08, 2015 18:49*

That's what I'm worried about, but I'd have to add something to keep the chain straight.


---
**Brandon Cramer** *August 19, 2015 20:53*

**+Erik Scott** **+Eric Lien**  Any update on this? I would love to finish my Eustathios build and get my heated bed installed along with this or similar cable chain. 


---
**Erik Scott** *August 19, 2015 20:58*

Sorry, I will get the files to you when I get home. Say maybe 2 hours or so. Alternatively, you can  find my Eustathios onshape file and export the part yourself. 


---
**Brandon Cramer** *August 19, 2015 21:02*

Thanks **+Erik Scott**  What is your username or how do I find you on onshape? 


---
**Erik Scott** *August 19, 2015 21:10*

Not sure. It isn't well organized. I'm on my phone right now, so I can't give you a link. 


---
**Brandon Cramer** *August 19, 2015 21:13*

No worries. I can't print it today anyway. 


---
**Erik Scott** *August 19, 2015 23:02*

STL: [https://www.dropbox.com/s/tv3d5lpu20gqnbp/Eustathios%20-%20Cable%20Carrier%20Bed%20Attachment.stl?dl=0](https://www.dropbox.com/s/tv3d5lpu20gqnbp/Eustathios%20-%20Cable%20Carrier%20Bed%20Attachment.stl?dl=0)
STEP: [https://www.dropbox.com/s/bptsqo3d0n4fcn5/Cable%20Carrier%20Bed%20Attachment.step?dl=0](https://www.dropbox.com/s/bptsqo3d0n4fcn5/Cable%20Carrier%20Bed%20Attachment.step?dl=0)


---
**Brandon Cramer** *August 19, 2015 23:22*

**+Erik Scott** Do you have an STL file for just the bracket? Also what cable chain would I use with this?


---
**Erik Scott** *August 19, 2015 23:33*

Oops, forgot I had some reference objects in there. This should be a clean version: [https://www.dropbox.com/s/ff5gt801vdbrqlt/Eustathios%20-%20Cable%20Carrier%20Attachment.stl?dl=0](https://www.dropbox.com/s/ff5gt801vdbrqlt/Eustathios%20-%20Cable%20Carrier%20Attachment.stl?dl=0)


---
**Brandon Cramer** *August 20, 2015 00:00*

**+Erik Scott**  Sweet! I will print that at home tonight on my other 3D printer. :) 



Which cable chain is this supposed to work with? 


---
**Erik Scott** *August 20, 2015 00:02*

Oops, sorry, I forgot: [http://www.mcmaster.com/#catalog/121/866/=yki53d](http://www.mcmaster.com/#catalog/121/866/=yki53d) and [http://www.mcmaster.com/#catalog/121/866/=yki5af](http://www.mcmaster.com/#catalog/121/866/=yki5af)


---
**Brandon Cramer** *August 20, 2015 17:24*

**+Erik Scott** Do you know which part # I need? I'm not exactly sure which part from mcmaster mounts to this quick mount. 



I also wonder if there is a cable chain that I can print instead of ordering one. 


---
**Erik Scott** *August 20, 2015 22:51*

Dunno why those didn't work. Try this: [http://www.mcmaster.com/#4516t45/=ykzqwz](http://www.mcmaster.com/#4516t45/=ykzqwz)

and this: [http://www.mcmaster.com/#4556t32/=ykzr4e](http://www.mcmaster.com/#4556t32/=ykzr4e)



Don't know what to suggest in regards to printable ones. Sorry. 


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/R4zfPZKmU5T) &mdash; content and formatting may not be reliable*
