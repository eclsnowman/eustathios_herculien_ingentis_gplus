---
layout: post
title: "So the first print ran and failed, the off to Maker Faire Orlando for my first prints"
date: September 13, 2015 12:35
category: "Show and Tell"
author: Rick Sollie
---
So the first print ran and failed,  the off to Maker Faire Orlando for my first prints. Talk about cutting it close.



![images/b0277f1338597a9e48f0116e2f6b8381.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b0277f1338597a9e48f0116e2f6b8381.jpeg)
![images/cd0af9d51503b337d58ac21d4dcd2ff9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cd0af9d51503b337d58ac21d4dcd2ff9.jpeg)

**Rick Sollie**

---
---
**Frank “Helmi” Helmschrott** *September 13, 2015 12:47*

What build plate are you using there?


---
**Øystein Krog** *September 13, 2015 14:57*

Could be white PEI?


---
**Rick Sollie** *September 14, 2015 00:04*

**+Frank Helmschrott** after seeing the delta printer work on clear acrylic I dropped this onto the bed to test with some place.  It is 1/8 inch white cell cast acrylic lightly sanded with 320 grit sandpaper. Sticks amazingly well.


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/iBBZkdTSbyt) &mdash; content and formatting may not be reliable*
