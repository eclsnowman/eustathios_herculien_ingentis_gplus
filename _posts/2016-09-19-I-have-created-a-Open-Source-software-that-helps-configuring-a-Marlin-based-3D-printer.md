---
layout: post
title: "I have created a Open-Source software that helps configuring a Marlin based 3D printer"
date: September 19, 2016 05:03
category: "Show and Tell"
author: Johnny Lindén
---
I have created a Open-Source software that helps configuring a Marlin based 3D printer.



Marlin3dPrinterTool is a open-source software that helps you configure and test your 3D printer. The software is under development and some features ar not fully tested on all types of 3D printers.  The code is tested on: BigBox Pro Rigidbot Big RapideLite 200  The main features are: * Endstop testing * Bed limit and bed abjusters position * Z-probe configuration * Bed Level Tool * Scan building surface and create visual chart * Z-rod maintenance * Auto PID calibration * Extruder calibration * Firmware upgrade and migration



The progress of the development can be followed at [https://plus.google.com/u/0/communities/103616587200621103791](https://plus.google.com/u/0/communities/103616587200621103791)





**Johnny Lindén**

---
---
**Jim Stone** *September 19, 2016 06:38*

I had a peek and that looks so helpful! especially when updating marlin to not miss settings etc.



also that representation of the bed mesh UNF now i need to go change my pants. i have been WANTING that for ages. i like to see the plane of my bed!



Seriously interested.


---
**Antonio “3DZillait” Ranaldi** *September 19, 2016 13:31*

her ise non a mac version?




---
**Johnny Lindén** *September 19, 2016 14:32*

**+Jim Stone**  Just download the Installer and try. The bed level and surfacescan should work when you have configured the endstops and the bedlimits. 

The bedsurface needs a z-probe but that is obvious


---
**Johnny Lindén** *September 19, 2016 14:34*

To do a Mac version is beond my knowledge. The program is written in C# and DotNet. To run it on a Mac you nedd a VirtualMachine running windows 7-windows 10


---
*Imported from [Google+](https://plus.google.com/102707506187095306417/posts/FPJFopShL3F) &mdash; content and formatting may not be reliable*
