---
layout: post
title: "The obligatory first buildlog post. For anyone in NZ looking for a frame I got this from \"Mecha4makers\" in Wellington"
date: February 19, 2018 20:49
category: "Build Logs"
author: Julian Dirks
---
The obligatory first buildlog post.  For anyone in NZ looking for a frame I got this from "Mecha4makers" in Wellington.  Nice to deal with, good price and very nicely machined. I roughly put the frame together with no corner brackets or tweaking and the top was perfectly square and the bottom was out by 0.5mm so very happy.

![images/179d80bf7ca55890b87243b6eb890c1e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/179d80bf7ca55890b87243b6eb890c1e.jpeg)



**Julian Dirks**

---
---
**Eric Lien** *February 19, 2018 23:19*

Looking great. Was there a good contact name for getting the frame from Mecha4makers? I can add it as an option on the BOM.


---
**Julian Dirks** *February 20, 2018 00:08*

The guy I dealt with was Lindsay - it's his company. Contact details on his website. I'm keeping a list of where I bought what and can share that when I'm done if it will be useful?


---
**Eric Lien** *February 20, 2018 01:21*

**+Julian Dirks** that would be great. I also think it might be time for me to do a slight refresh on the models and BOM on the GitHub. I should have the base system reflect what people are actually building (include some of the community upgrades in the base system, and update recommendations for newer control boards, etc).


---
**Julian Dirks** *February 20, 2018 04:58*

That woudl be really useful.  I'm agonising over a few things at the moment.  Have gone with 10mm rods all round (misumi, just arrived) and a bondtech BMG/E3Dv6 - also here and now on my Wanhao : )   Was planning on direct drive but now I'm wondering if I should start by going bowden as you originally intended.  not even thinking about controller boards yet!



Would be great to see more photos of the print quality achieved with the different setups.



FYI, found what I think is an error in the BOM, item 10.2 extruded aluminium. The Misumi part number donates 355mm with a hole half way along vs the description which says 562mm (don't take the the wrong way - the BOM is great -  just trying to save someone the grief of getting the wrong length if they aren't ordering from Misumi!).








---
**Eric Lien** *February 20, 2018 07:13*

In the excel version, or the google docs version?


---
**Julian Dirks** *February 20, 2018 07:24*

XL from Github


---
**Eric Lien** *February 20, 2018 13:55*

**+Julian Dirks** Thanks, I will look into it right now. 


---
*Imported from [Google+](https://plus.google.com/113795478307151372873/posts/AB3otQTw8SK) &mdash; content and formatting may not be reliable*
