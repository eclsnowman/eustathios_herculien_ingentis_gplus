---
layout: post
title: "Testing 50x50x15mm blower fan as a replacement of my current axial fan system on ."
date: August 28, 2014 12:28
category: "Show and Tell"
author: Eric Lien
---
Testing 50x50x15mm blower fan as a replacement of my current axial fan system on #HercuLien. So far I like it much more. Way more airflow, and higher velocity. Time to build a better mount and design a small directed duct.

![images/ba61cf948af62297f7dd7ac0a7497077.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ba61cf948af62297f7dd7ac0a7497077.jpeg)



**Eric Lien**

---
---
**Wayne Friedt** *August 28, 2014 12:36*

I like a fan that blows all the way around the part.


---
**Eric Lien** *August 28, 2014 15:50*

**+Wayne Friedt** I agree. But when ducting an axial fan around too many curves the static pressure drop really kills the cfm. I am hoping the centrifugal fan handles this better.


---
**Hilbrand Zijlstra** *August 28, 2014 16:53*

Try to make one that cools the print and also the hot end :D


---
**Eric Lien** *August 28, 2014 17:17*

**+Hilbrand Zijlstra** the problem is when you switch to abs.


---
**Joe Spanier** *August 28, 2014 18:21*

And when you change your fan rates mid print it can effect your hot end. 


---
**Jeremie Francois** *August 28, 2014 19:19*

What about noise? I tried once some time ago and mine was awfully noisy :D


---
**Eric Lien** *August 29, 2014 00:47*

**+Jeremie Francois** mine are silent other than the sound of rushing air... Which is a welcome sound for high speed PLA printing.﻿


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/fWmxDAhW3nz) &mdash; content and formatting may not be reliable*
