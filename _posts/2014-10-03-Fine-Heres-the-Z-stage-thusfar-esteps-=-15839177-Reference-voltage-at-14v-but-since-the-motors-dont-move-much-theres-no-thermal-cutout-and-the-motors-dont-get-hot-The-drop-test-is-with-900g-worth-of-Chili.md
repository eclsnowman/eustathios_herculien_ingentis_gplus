---
layout: post
title: "Fine. Here's the Z-stage thusfar. esteps = 158.39177 Reference voltage at 1.4v (but since the motors don't move much, there's no thermal cutout and the motors don't get hot.) The drop test is with 900g worth of Chili"
date: October 03, 2014 01:25
category: "Show and Tell"
author: Mike Miller
---
Fine. Here's the Z-stage thusfar. esteps = 158.39177  Reference voltage at 1.4v (but since the motors don't move much, there's no thermal cutout and the motors don't get hot.) The drop test is with 900g worth of Chili. :)




**Video content missing for image https://lh6.googleusercontent.com/-pAqfNX9oKeM/VC36v_D9voI/AAAAAAAAH-Y/yafkEBmFH2U/s0/VID_20141002_174909.mp4.gif**
![images/0e5232e10bd36ecb9a7bd15aea9eca27.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0e5232e10bd36ecb9a7bd15aea9eca27.gif)
![images/1ed132fbae2b15f4d8c5b39dffd871ec.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1ed132fbae2b15f4d8c5b39dffd871ec.gif)

**Video content missing for image https://lh6.googleusercontent.com/-ALFdJ4O_lJI/VC36vzeVYQI/AAAAAAAAH-Y/4mulbkdW0yQ/s0/VID_20141002_174552.mp4.gif**
![images/4d610e78ae477e1f4e91527e42d4ef8f.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4d610e78ae477e1f4e91527e42d4ef8f.gif)

**Mike Miller**

---
---
**Eric Lien** *October 03, 2014 02:04*

Nice job.﻿ Glad you got it working. Now for some print tests :)﻿


---
**Mike Miller** *October 03, 2014 02:16*

Getting closer. I had to give up on the glorious 13"^2 glass build plates...the motors couldn't lift the build plate (without anything printed.)



I'm pretty sure I can make it work. ;) Got some Gekotek on the way. 



I'm <i>really</i> reluctant to take the next step. In order to finish the new printer, I've gotta gut part of the old one. 


---
**Eric Lien** *October 03, 2014 02:18*

**+Mike Miller** put a geared stepper on there and you could gain esteps and lift the glass no problem.


---
**Mike Miller** *October 03, 2014 02:25*

Yeah, probably. Don't have a spare $60 for that at the moment. I'd want to figure out something other than the GT2 belt, too...pretty sure that would be the weak point then. 



As it is, the aluminum rod was deflecting with just one pulley and motor, the second lends a hand in lifting, but also keeps the mechanism in alignment. I had two really nifty idlers mae with the **+igusInc** rod, but didn't need them with two opposing forces.


---
**Mike Miller** *October 03, 2014 10:33*

You could, in fact the ingentus does just that, but I wanted precision over cost when there was a question between the two and machines aluminum wins that contest. I also wanted the inherent noise absorption that you get from timing belts. You can get awesome ground worm screws, but things get less certain when you're buying cheap Chinese screws (and not German). 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/Vj5sxzQcDJW) &mdash; content and formatting may not be reliable*
