---
layout: post
title: "I'm getting this noise only when moving in Y at low speeds"
date: July 08, 2017 00:20
category: "Discussion"
author: James Ochs
---
I'm getting this noise only when moving in Y at low speeds.  It's kind of like a chatter/binding sound, but I cant figure out where its coming from, it vibrates the whole machine... any hints?  Should I add a little light oil to the rods?


**Video content missing for image https://lh3.googleusercontent.com/-oqXGaxbpfUw/WWAlQ8PsbWI/AAAAAAAAykM/TPegIFQgEks5504IlB2wrYC_EPObb74FACJoC/s0/VID_20170707_201456.mp4**
![images/89534ba2bd492e88e176e006687e2113.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/89534ba2bd492e88e176e006687e2113.jpeg)



**James Ochs**

---
---
**Eric Lien** *July 08, 2017 00:49*

You can try a little oil.



But something else you can try is put a stack of a few post-it notes, or a small chunk of cardboard under one of the corners of the frame base. Then move it to  each of the four corners one at a time. See if the noise gets less or more at each corner. You might have a little tweak in the tolerance stackup of the whole assembly. Putting the stack under the corner will put a slight tweak into the frame. And how the noise reacts will help you hunt down where to start looking.



It can also just be a harmonic. Those can be difficult to fix. You can test for that by changing speed, or adding mass and see if it goes away.


---
**ThantiK** *July 08, 2017 01:06*

When you hear this, also lightly put your hand on the assembly -- if you can feel it then it's binding slightly.


---
**James Ochs** *July 08, 2017 13:22*

Thanks for the tips... I think I found the culprit, one of the Y belts had too much tension on it, loosened it up a little and it seems to have stopped the problem


---
**Stefan Gerkin** *October 04, 2017 03:21*

Stupid question here... but what is encompassing your bushings? is it some sort of adapter to fit the bushing to the carriage?




---
**Eric Lien** *October 04, 2017 04:01*

**+Stefan Gerkin** they are a a self aligning bushing. So think about it as an oilite bushing in a "ball and socket" configuration. The central bronze bushing has a few degrees of misalignment allowance that way.


---
**Stefan Gerkin** *October 04, 2017 15:20*

Oh! ok, thanks **+Eric Lien**. I just have standard bushings that fit in the slots of my printed parts. Where would I be able to get those as a replacement?




---
**Eric Lien** *October 04, 2017 20:58*

It depends on the size you need. I usually got mine here:



[ultibots.com - 10mm Bronze Bearing - 4 Pack - A 7Z41MPSB10M](https://www.ultibots.com/10mm-bronze-bearing-4-pack-a-7z41mpsb10m)/



[https://www.ultibots.com/8mm-bronze-bearing-4-pack-a-7z41mpsb08m/](https://www.ultibots.com/8mm-bronze-bearing-4-pack-a-7z41mpsb08m/)




---
**Eric Lien** *October 04, 2017 20:59*

But they are sdpsi products at the end of the day. So should be available from them or a sdpsi distributor.


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/YeCiwPLveHU) &mdash; content and formatting may not be reliable*
