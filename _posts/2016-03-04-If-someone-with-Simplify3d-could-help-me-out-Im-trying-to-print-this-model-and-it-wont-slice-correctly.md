---
layout: post
title: "If someone with Simplify3d could help me out, I'm trying to print this model and it won't slice correctly"
date: March 04, 2016 05:40
category: "Discussion"
author: Erik Scott
---
If someone with Simplify3d could help me out, I'm trying to print this model and it won't slice correctly. I'd like to see if anyone else gets issues like me. The holes get filled in on the 3rd layer, and it seems to almost skip a layer around layer 22 or 23. It only happens if I set the first layer height to 100%. If it's 95%, it seems to be okay. Any ideas? Is it just a bug in Simplify3D? It's quite annoying. 



Link: [https://www.dropbox.com/s/xthibs3xm9ljb9p/CalBlock_High.stl?dl=0](https://www.dropbox.com/s/xthibs3xm9ljb9p/CalBlock_High.stl?dl=0)





**Erik Scott**

---
---
**Zane Baird** *March 04, 2016 06:01*

Some models have done this for me as well. It's not a real fix, but try scaling it by 100.01 and see how it slices.


---
**Oliver Seiler** *March 04, 2016 07:19*

It's a known bug in Simplify3D and moving the specific layer to a slightly different level (by scaling the model, using a different layer height or different first layer height) will usually fix it.


---
**Samer Najia** *March 04, 2016 13:25*

I can try it for you if you still want someone to.  It will be in ABS on my CTC.  Let me know.


---
**Erik Scott** *March 04, 2016 14:38*

No need to actually print it, just looking to slice it. But since it's a known issue and there are work arounds, don't bother. I can make it work, and the exact height of the object isn't important in this case. Thanks guys!


---
**Carlton Dodd** *March 04, 2016 18:07*

"Math is hard."  -Simplify3D, probably


---
**Oliver Seiler** *March 04, 2016 19:30*

It slices fine for me at 0.15 and 0.1 layer height, but I get filled holes at 0.2mm

[https://goo.gl/photos/dCsC2QbPEpqShJ1i7](https://goo.gl/photos/dCsC2QbPEpqShJ1i7)


---
**Erik Scott** *March 04, 2016 20:30*

Yup, that's what I get. Do you also get the missing layer?


---
**Oliver Seiler** *March 04, 2016 20:57*

Hmm, wasn't sure what you meant but I do get a layer with only the holes filled and everything else void, above and below layers are treated as if they were bottom/top layers (i.e. solid instead of infill).

Weird. Should be reported to Simplify3D.


---
**Oliver Seiler** *March 04, 2016 20:57*

This is the generated gcode

[https://drive.google.com/file/d/0B2br-OijkAgwdTZkX09TLURQUlk/view?usp=sharing](https://drive.google.com/file/d/0B2br-OijkAgwdTZkX09TLURQUlk/view?usp=sharing)


---
**Carlton Dodd** *March 04, 2016 20:57*

The missing layer is probably a math problem.  But, the filled holes is an STL problem.  I get this on a bunch of slicers, if I don't extend the hole <b>past</b> the edge of the object.


---
**Oliver Seiler** *March 04, 2016 21:02*

I've been getting the filled holes with a number of STLs, including very simply SCAD models (a block with a cylinder hole through). I'd be surprised if that had anything to do with the STL. The same models have always sliced fine with slic3r.


---
**Carlton Dodd** *March 04, 2016 21:06*

**+Oliver Seiler**

Fair enough.  I just know I've learned to extend a hole at least a fraction of a millimeter beyond the edge in OpenSCAD.  If I don't, I get this behavior in Slic3r and Simplify3D.


---
**Oliver Seiler** *March 04, 2016 21:37*

Fair point **+Carlton Dodd**​ but I can sometimes see filled holes in the middle of a hole sometimes where it doesn't intersect with another surface 


---
**Erik Scott** *March 04, 2016 21:47*

I thought so too at first, but it's not an STL issue. I've checked the STL in Blender, exported from OnShape in High, Medium, and Low qualities, imported the STEP into Fusion360 and did the same. Each time the STL geometry was different. Each time it was manifold, and each time Simplify3d showed the same bug. I'll see what I can do about reporting it.



As for the missing layer, I have the same behavior: a solid layer below and above. The layer above the missing one is shown as a bridging operation.


---
**Oliver Seiler** *April 01, 2016 08:44*

This issue just happened to me again when slicing the 3DBenchy. I only realised when the print came out with a single solid layer across the deck. I've filed a support request with Simplify3D - let's see what they come back with.


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/HWL34JoyHtS) &mdash; content and formatting may not be reliable*
