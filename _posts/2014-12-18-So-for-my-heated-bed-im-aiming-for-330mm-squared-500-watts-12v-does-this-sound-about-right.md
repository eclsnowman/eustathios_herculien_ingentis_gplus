---
layout: post
title: "So for my heated bed im aiming for 330mm squared 500 watts 12v does this sound about right?"
date: December 18, 2014 03:04
category: "Discussion"
author: Ethan Hall
---
So for my heated bed im aiming for 330mm squared 500 watts 12v does this sound about right?





**Ethan Hall**

---
---
**D Rob** *December 18, 2014 03:05*

You could go AC with SSR


---
**Ethan Hall** *December 18, 2014 03:07*

What are you're suggestions for an SSR and heated bed specs.


---
**Ethan Hall** *December 18, 2014 03:14*

Also the SSR would hook up directly to mains ac power and then be converted to DC or not? Before being sent to the bed


---
**D Rob** *December 18, 2014 04:39*

The board's output triggers the ssr on and off. The power stays ac and the heater needs to be AC check Ali rubber. **+Eric Lien**​ what specs are you using?


---
**Eric Lien** *December 18, 2014 05:25*

Omeron G3NA-220B SSR. Heated bed running at 120v AC (don't fear the AC, you don't fear your vacuum cleaner, that's 120v and you handle that on a cord)﻿


---
**Florian Schütte** *December 18, 2014 06:54*

**+Eric Lien**  I will ground my buildplate. I mean...the bed is a cheap one from china ;)


---
**Ethan Hall** *December 18, 2014 18:41*

Thanks so much!


---
*Imported from [Google+](https://plus.google.com/104138254730622830594/posts/6fjfVS9CD6s) &mdash; content and formatting may not be reliable*
