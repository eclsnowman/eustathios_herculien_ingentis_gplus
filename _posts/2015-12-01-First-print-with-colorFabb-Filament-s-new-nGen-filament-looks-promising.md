---
layout: post
title: "First print with colorFabb Filament 's new nGen filament, looks promising"
date: December 01, 2015 20:14
category: "Show and Tell"
author: Oliver Seiler
---
First print with **+colorFabb Filament**'s new nGen filament, looks promising 



![images/85f6735b7166cf5dda51060ea8a1b3a1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/85f6735b7166cf5dda51060ea8a1b3a1.jpeg)
![images/4ab3771bdbd0626ff9ac6053daec4efb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4ab3771bdbd0626ff9ac6053daec4efb.jpeg)
![images/8568d1b8dec01473b18da50528cd3aff.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8568d1b8dec01473b18da50528cd3aff.jpeg)

**Oliver Seiler**

---


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/faub9ooP1hu) &mdash; content and formatting may not be reliable*
