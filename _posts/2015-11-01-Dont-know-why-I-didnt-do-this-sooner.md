---
layout: post
title: "Don't know why I didn't do this sooner"
date: November 01, 2015 21:02
category: "Discussion"
author: Ben Delarre
---
Don't know why I didn't do this sooner. The squeaking from the filament holder during prints was driving me nuts. Added a few loops of Teflon tape (red bit) around the holder and it's totally quiet now! 

![images/bb92cab035c1d959ae81b71aef31ffe7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bb92cab035c1d959ae81b71aef31ffe7.jpeg)



**Ben Delarre**

---
---
**Eric Lien** *November 01, 2015 21:04*

Gotta love simple solutions. Nice job **+Ben Delarre**​.



BTW how it running for you otherwise. Haven't seen any updates in a while.


---
**Ben Delarre** *November 01, 2015 21:22*

Yeah not had much time for 3d printer stuff,  but now burning man is over with and the dust is clear I'm getting back into it. 



I'm still trying to tune everything and getting very variable results to be honest. Beginning to think that the 3mm acrylic bed I have is flexing on z movements. Thinking of braving cutting the 1/4" aluminum plate today using my jigsaw... Anyone tried that? 


---
**Eric Lien** *November 01, 2015 21:40*

**+Ben Delarre** to be honest I think the large leadscrews cause too much force. I think It leads to the z-wobble and ribbing. In retrospect I would go with the ball screw now. Either that or smaller OD leadscrews.


---
**Eric Lien** *November 01, 2015 21:41*

Odd thing is my ribbing will come and go. Which makes no sense if its mechanical.


---
**Ben Delarre** *November 01, 2015 21:44*

Yeah I've noticed that the ribbing comes and goes too, right now its not doing it, but with the same filament on a different print it does it again. Which is what lead me to think it was the build plate maybe vibrating or flexing on z moves.



I want to get the heated bed on anyway so I've got to brave the alu plate at some point. Was hoping I'd get round to finding time on a friends CNC to get it cut but it seems like it'll be a few more weeks now. I have two plates that would fit so I thought I might sacrifice one and see if I can cut it with the jigsaw :)


---
**Isaac Arciaga** *November 02, 2015 00:43*

**+Ben Delarre**​ contact **+Jeff DeMaagd**​ he might have ATP-5 Eustathios plates left. Its similar to Mic6 plating. Nice and flat. 


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/edCvsN6FxrY) &mdash; content and formatting may not be reliable*
