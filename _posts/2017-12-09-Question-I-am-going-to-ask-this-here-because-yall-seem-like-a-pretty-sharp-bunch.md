---
layout: post
title: "Question- I am going to ask this here because y'all seem like a pretty sharp bunch..."
date: December 09, 2017 02:01
category: "Discussion"
author: Dennis P
---
Question- I am going to ask this here because y'all seem like a pretty sharp bunch... is there a more direct way to generate GCode to print instead of taking a solid model, processing it and exporting to a mesh of triangles only to interpret and then reconstruct it back into a solid model through via a slicer?  Every step additional step introduces errors. STL destroys the curves. If it took a NURBS, STEP or similar file and kept the geometry intact, that would be different.  



I struggle with slicers- Cura is just painfull.  S3D has a nicer UI but too many interlinked variables that seem to throw other things off-- I feel like I am playing a game of pic-up-sticks all the time with it. Believe it or not, KISSlicer give me the most reliable prints using PETG of all things, but I wish it had more 'features' lke support tweaks and infill patterns. I have tried to 'clone' the gcode settings from KISS output to put back into S3D but no such luck. Slic3r- still struggling getting it figured out honestly, but I feel like why bother with another slier. ARGH!



I guess what I am realizing is that if you are used to using professional grade  software day in and day out, , its hard to grapple with the 3D printing options out there. 





**Dennis P**

---
---
**Jeff DeMaagd** *December 09, 2017 04:00*

I think you have a pretty good grasp at the current state of things.



I’d really love a direct slice from STEP, for example but I have neither the money, time or connections to make that happen. Even just a slicer that notices the input file was updated and swaps in the new version would smooth out my workflow by a few steps.


---
**Miguel Sánchez** *December 09, 2017 10:28*

Whether you take the slice geometry from a mesh or from another format,  still a lot of parameters will need to be provided to create that layer's g-code. Tools with fewer user-selectable parameters can be easier to start with but not so powerful in the long run. I reckon the same reasoning can be applied to many software tools (Marlin firmware configuration comes to my mind). 



The bottom line is that many of the tweaks available now are there because they are useful sometimes. Over time users figure out what works best for them.



Manufacturers, by creating a smaller microcosmos, can reduce the number of variables and only present a subset of crucial parameters to the user while using sane defaults for the rest. That is the case of Prusa Research with [https://prusacontrol.org/](https://prusacontrol.org/)  


---
**Michaël Memeteau** *December 13, 2017 09:55*

Maybe the closest to what you're looking for right now is Kiri Moto (an Onshape add-on). I'm not sure if internally the transition from solid geometry to STL is hidden or direct, but it's not apparent for the user.

The slicer itself lacks a lot of bells and whistles from other more evolved package, but it's also one of rare one to support laser and CAM as well on the same platform directly (that I know of... Maybe Fusion 360 does or will do something in this area too). Best of all: it's free!!!


---
**Miguel Sánchez** *December 13, 2017 11:58*

**+Michaël Memeteau** given Kiri::Moto can also accept STL files when used outside of Onshape environment I would lean to believe they are entirely mesh-based a their core, but I have no proof ;-)



What I have never tried though is Fusion 360 CAM (but I am about to once I have a moment, maybe with a cylinder to see whether it uses arcs or linear movements to machine it).


---
**Dennis P** *December 13, 2017 21:42*

Thanks folks. My frustration is the constant machinations in the process.  360 CAM is also interesting since I alrady work in Acad, but it looks like it might be $300/year just to play. 




---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/8NB8bX5MMdH) &mdash; content and formatting may not be reliable*
