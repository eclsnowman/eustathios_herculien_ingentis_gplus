---
layout: post
title: "One step closer. Got most of the wiring in place"
date: September 06, 2015 03:40
category: "Show and Tell"
author: Rick Sollie
---
One step closer. Got most of the wiring in place. Hopefully my raspberry pi holder will be done tomorrow and I can finish up.

![images/33d0e28dca4a1153e379e6d153a07d02.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/33d0e28dca4a1153e379e6d153a07d02.jpeg)



**Rick Sollie**

---
---
**Chris Purola (Chorca)** *September 06, 2015 06:42*

Looking awesome! We should figure out if we can stick them side by side at Maker Faire! :D


---
**Rick Sollie** *September 06, 2015 11:58*

**+Chris Purola** unfortunately mine will not fit on your build plate so we will have to go side by side ;)


---
**Roland Barenbrug** *September 07, 2015 19:19*

**+Rick Sollie**  did you opt for the 0.9 degree, 44Nm version? (Best guess when looking at the picture)


---
**Rick Sollie** *September 08, 2015 01:25*

**+Roland Barenbrug**

  Yes, I figure the higher the resolution the better, and if its any problem, I'll just move from 1/32 step to 1/16.


---
**Bud Hammerton** *September 10, 2015 02:29*

**+Rick Sollie** I am going the same route as you. Got my 0.9° steppers from Robotdigg. They appear to be the same ones you are using. Was planning on using them with 1/16 steps. Figured that despite 1.8° x 1/32 vs. 0.9° x 1/16 may appear to be the same on paper, I figured it would be easier for the steppers to hold the 1/16 microstep rather than the 1/32. But if it works out well I could always switch to 1/32 steps for even finer resolutions.



Update: So I just read the next set of posts and see that there could be issues with lower torque motors and skips, I will worry about it after it's built, and worst case, I can always swap out the motors later. 


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/LpEQM1mudyg) &mdash; content and formatting may not be reliable*
