---
layout: post
title: "Should I go with 2 E3D V6's or one chimera?"
date: July 11, 2016 01:13
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Should I go with 2 E3D V6's or one chimera? I plan to order both at the same time. I have seen some members with 2 V6's so I was wondering if there is an advantage of that setup. (I'm going with 2 Volcanos anyway.) Thanks!





**Stefano Pagani (Stef_FPV)**

---
---
**Jeff DeMaagd** *July 11, 2016 03:57*

I don't know if that's an easy question. I think Chimera is easiest to design around. Chimera is easy to adjust, each hot end is held by two set screws so you can correct nozzle height issues.



I have a Chimera but I built a water cooled block as I had doubts about the stock air cooled block in my heated build environment, I haven't really given the stock setup a chance.



Ease of pulling out the second extruder is roughly the same either way, depending on how you set up your carriage though, maybe a nod to Chimera again, it's just two set screws and the heater block and heat break drops out.



Chimera has a much smaller nozzle-to-nozzle spacing, which is a major why I chose it. The more the nozzles are spaced apart, the more it reduces your build area for dual parts.



That's the quick run-down. My comments are rough generalizations and can depend on your design choices.


---
**Joe Spanier** *July 11, 2016 12:35*

I really enjoy the mounting options with the Chimera #killgroovemount 


---
**Stefano Pagani (Stef_FPV)** *July 12, 2016 02:51*

**+Jeff DeMaagd** Thanks for the detailed answer! I was leaning to the chimera myself for those reasons exactly.


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/5j3atmGLrZQ) &mdash; content and formatting may not be reliable*
