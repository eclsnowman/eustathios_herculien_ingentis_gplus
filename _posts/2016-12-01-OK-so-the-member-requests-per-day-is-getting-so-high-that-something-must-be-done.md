---
layout: post
title: "OK, so the member requests per day is getting so high that something must be done"
date: December 01, 2016 14:49
category: "Discussion"
author: Eric Lien
---
OK, so the member requests per day is getting so high that something must be done. At 30+ a day and growing it is taking me far to long to review each one and confirm they are not just spammers, bots, etc. The majority of the requests are unfortunately not real people, so the ratio of time I spend reviewing profiles to the quantity of new members is lopsided.



I would like to make the community public, but have new posts held for moderator review/approval like they recently did in the 3dprinting G+ community. This will take care of the potential spam posts my current member review is aimed at avoiding. Since the total numbers of posts per day in our group (because it is so specialized) is vastly outweighed by the number of new member requests we get... I can actually optimize the moderators time changing to this format. I just hope the number of spam posts to reject a day is not greater than the spam member requests... oh man that would be a double edged sword :)



But I want to check with the group before I do something like this. Once done our member count will likely become artificially inflated with these spammers and bots. I know that likely not all 1017 current group members real, some fake accounts have no doubt gotten through the cracks. But with the change I assume our member count will become meaningless. And there will be a delay (hopefully short) between when you post and when it will be visible because a moderator needs to approve it. I will likely need some more moderators if the delay gets long. But at the end of the day I need to do something for my sanity :)



As an example, attached is a screenshot of the requests I received when I woke up this morning. 



Let me know below how you feel about this potential change. I do the work I do because I think the community we have built together is something special. So your voice and opinion are of greater importance to me than my feelings on the matter. 

![images/1b027c2fa8713d69838e88915e4f7c9c.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1b027c2fa8713d69838e88915e4f7c9c.png)



**Eric Lien**

---
---
**Stephen Baird** *December 01, 2016 14:52*

I think that's a reasonable solution. The spam posts should (hopefully) be more obvious than the spam accounts anyway, so it should simplify moderation for you too. 


---
**Maxime Favre** *December 01, 2016 15:26*

I second that. Can help a bit if needed.


---
**Pieter Swart** *December 01, 2016 15:37*

Go for it Eric.


---
**Eric Czeladka (corethan)** *December 01, 2016 15:41*

Hi. I'm highly interested In the community and Reading a lot of post. I don't post myself because i'm still In a self-documentation step. I think your solution deserve to be tested. Obviously you cant spend most of your free time playing the moderator. 


---
**Edward Beheler** *December 01, 2016 15:47*

I think the change would ultimately end up making the problem worse.  It would probably be worthwhile to draft trusted members to help with accepting or rejecting membership requests.


---
**Eric Lien** *December 01, 2016 15:58*

**+Edward Beheler** that is my fear. That I solve one problem, but unleash a worse one. But I am inclined to agree with **+Stephen Baird**​ that a spam post is almost instantly recognizable, where a spam/fake user is more difficult to distinguish. And users with no posts and no profile might be one of two things:



1.) An interested member who is shy or not active in posting yet because they just started.



2.) A spamer trojan horse laying in wait to activate the frenzy of spam posts from a benign looking account.



But I do appreciate the discussion from everyone very much... keep them coming. I am all ears.


---
**Carter Calhoun** *December 01, 2016 17:17*

Sounds good Eric, though, it is a shame to fill the member ranks with spam accounts....  Just curious if there is another option...I don't know enough about how Google groups works- but is there a way that, if someone asks to join the group they need to provide some further detail? For example, prospective members give an introductory message that explains thier interest in the group, details? That would catch spam also.



If not, then it seems that the only realistic option is the one that you have proposed.


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 17:29*

I think about 20% of the 3DP group is filled with such dormant accounts. Hold the posts for moderation like you said. 

Also, add more mods! 


---
**Samer Najia** *December 01, 2016 17:35*

I second Ray's seconding


---
**Matt Wils** *December 01, 2016 18:37*

I second Ray's post. 


---
**Eric Lien** *December 01, 2016 19:08*

I think I am going to make the change. Most comments seem to lean that way. Thanks everyone, Lets hope this is an improvement. I will also be adding some moderators. If anyone has a desire to be added let me know, Otherwise I will just run down the list of our most prolific posters and commenters and see who bites.


---
**Frank “Helmi” Helmschrott** *December 01, 2016 22:11*

go for it, Eric — and let me know if I can help anyhow. 


---
**Roger Kolasinski** *December 02, 2016 01:06*

Myself, while I do not initially plan to build either, the information applies, generally to all my printers. Plus it's fun to see people's builds!


---
**Eric Lien** *December 02, 2016 01:46*

**+Frank Helmschrott** good, you are a mod now. Happy Festivus Season :)



Context for those unaware of my lame joke: [https://en.m.wikipedia.org/wiki/Festivus](https://en.m.wikipedia.org/wiki/Festivus)




---
**Maxime Favre** *December 03, 2016 09:09*

Can help too


---
**Eric Lien** *December 03, 2016 13:37*

**+Maxime Favre** thank you very much. Done. Now remember with great power comes great responsibility :)


---
**Eric Lien** *December 03, 2016 13:41*

It would appear we either got 300 more people interested in building this style printer... Or the bot accounts have begun to work there magic inflating the member numbers :)



Oh well, there have been no spam posts yet. I guess it is working as expected. Thanks again for the input everyone!


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/DJzym9aMP2D) &mdash; content and formatting may not be reliable*
