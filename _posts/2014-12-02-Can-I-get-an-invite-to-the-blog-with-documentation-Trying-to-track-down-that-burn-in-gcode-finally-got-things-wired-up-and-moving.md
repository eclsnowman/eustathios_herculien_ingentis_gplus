---
layout: post
title: "Can I get an invite to the blog with documentation ?Trying to track down that burn-in gcode, finally got things wired up and moving!"
date: December 02, 2014 19:14
category: "Discussion"
author: Tony White
---
Can I get an invite to the blog with documentation ?Trying to track down that burn-in gcode, finally got things wired up and moving!





**Tony White**

---
---
**Seth Mott** *December 02, 2014 19:20*

Same here, would like an invite!


---
**David Heddle** *December 02, 2014 19:42*

There is some test gcode in the eustathios github and some linked in the comments of this post [https://plus.google.com/117269052061351663459/posts/cfBab2C6GZ3](https://plus.google.com/117269052061351663459/posts/cfBab2C6GZ3)

I haven't tried it yet but getting very close!


---
**Eric Lien** *December 03, 2014 00:20*

That was mine. If you want it tweaked let me know.﻿



Don't forget to video the maiden voyage ;)


---
**Ethan Hall** *December 03, 2014 02:13*

Can I also get an invite?


---
**Eric Lien** *December 03, 2014 12:54*

**+Ethan Hall** Invite? Just click this link: [https://docs.google.com/file/d/0B1rU7sHY9d8qcVYwUmhBYzBJOUk/preview](https://docs.google.com/file/d/0B1rU7sHY9d8qcVYwUmhBYzBJOUk/preview)


---
**Ethan Hall** *December 03, 2014 14:22*

[http://ingentistst.blogspot.co.nz/](http://ingentistst.blogspot.co.nz/) an invite to this blog.


---
**Eric Lien** *December 03, 2014 15:21*

**+Ethan Hall** OK I see. That would be a question for **+Tim Rastall**​


---
*Imported from [Google+](https://plus.google.com/+AnthonyWhiteMechE/posts/gqiiHB7rz8E) &mdash; content and formatting may not be reliable*
