---
layout: post
title: "E3D Titan vs Bondtech Has anyone used both?"
date: May 29, 2016 23:59
category: "Discussion"
author: jerryflyguy
---
E3D Titan vs Bondtech

Has anyone used both? Or seen a comparison between the two? I've no bias at this point, just looking for the unit which will give the absolute best performance as far as print quality. 



I'm planning on using an E3D V6 as the hot end.





**jerryflyguy**

---
---
**Eric Lien** *May 30, 2016 00:35*

I am likely biased. But I have used many extruders. And having the "All Wheel Drive 😀" of the Bondtech means in the multiple years of using them I have never, ever, had a feed related print failure. To me that's worth it at three times the price. 


---
**Eric Lien** *May 30, 2016 02:48*

**+Nathan Walkner** have you been using it as Bowden? I have a feeling that due to smaller retraction settings required direct drive is less demanding. 


---
**Ryan Carlyle** *May 30, 2016 03:55*

Ungeared direct drive extruders can be extremely reliably by virtue of stalling the motor before stripping the filament. The extruder can recover if the blockage is cleared (eg printing first layer way too close to the bed. You CAN do this with geared extruders and Bowden drives, but you need much better grip on the filament to get the motor to stall at a torque level that allows good results. 


---
**Pieter Swart** *May 30, 2016 10:37*

I wouldn't mind playing around with a Bondtech extruder (my country's weak currency is really putting a dent on my hobbies). 



I recently got a Titan and can give some feedback - I've had one failure where the drive gear chewed through the filament because the hot-end got the raw deal on a warped ABS part that I was printing. If I was using a Bondtech, it might have been a different story. If you only print with stable filaments like PLA, this shouldn't be an issue. I must add that the part that I was printing should be considered a worse case scenario.



The Titan allows you to use a small motor on an already light extruder design - if you intend to go with direct-drive on an Ultimaker type printer, it should be considered as a good candidate. 



As advertised, It does an excellent job at self-cleaning (an advantage if you print with filaments like ABS that warp a lot). They've also designed it in such a way that if you really want to clean the gear, you can do so without dis-assembly (you can simply use a brush - on my previous extruder I had to use a knife to clear any plastic stuck between the teeth, but since the Titan is doing self cleaning already, you simply need to brush off the gear - in fact, a little bit of compressed air does the job equally well).  



The other thing that I like is that it can be used with both 1.75mm and 3mm filament. You just need to swap out two parts. I started off on 3mm filament so I still have a bunch of old 3mm filament lying around. When I was looking at the Bondtech, it seemed that I would have to buy two units to print the different sizes. 



I haven't actually used any of my 3mm filaments on the Titan - might give some feedback on it when I do.



It's also really easy to load and unload filament (but I see they've made some modifications to the Bondtech extruder so that it can now load and unload without breaking a sweat)



One thing that I can complain about (but this is actually an issue with bowdens in general), is the push-fittings. There's play on both the extruder side and the hot-end side. I'm almost certain that in the past (with my previous extruder) I could wiggle it out a bit so that whether the extruder was extruding or retracting, the fitting wouldn't move. It's not a disaster though - I've added a shim to the extruder side and still need to add one to the hot-end side.


---
**jerryflyguy** *May 30, 2016 14:47*

**+Pieter Swart** thanks for the very detailed reply!  If I read between the lines it would seem that the Bondtech just won't fail at the worst of times while there are scenarios w/ the Titan that it 'could'. I do plan to print the whole cross section of filament products types. I'm leaning to a Bowden style unit but liked the idea that I could experiment w/ direct if I bought a Titan.. 



Still firmly planted on the fence.. At the end of the day, all that really matters is UM2+ (or better?) quality prints at 2 times an i3 price :) (champagne taste on a beer budget)


---
**Pieter Swart** *May 30, 2016 15:18*

**+jerryflyguy** That sounds about right. Well, whichever one you decide to go with, I'm sure it will work well. Both are professionally designed and it looks like a fair bit of thought went into them.


---
**jerryflyguy** *May 30, 2016 15:28*

**+Pieter Swart** I meant to ask you, can you share your thoughts and experiences between 3mm and 1.75? (I know this is a loaded question) what prompted you to switch etc?


---
**Pieter Swart** *May 30, 2016 17:38*

**+jerryflyguy** 3mm filament can be difficult to work with on a bowden setup. 



You need a realy good extruder and it needs to be geared since just a standard Nema 17 can not produce enough torque. 



Even the geared extruder that's used on the Bondtech  will skip steps if you try to use it in the typical bowden extruder that you can print from thingeverse (I've tried). 



To properly use 3mm, you need something like a Wade's geared extruder. Titan should work just fine and I assume that the Bondtech will work due to its mechanics combined with a geared Nema17 motor.



Even though I was using a Wade's on my printer, I still had issues now and again that caused failures. I also found that the last few meters on a lot of spools were almost unusable since the filament is wound up so tight that it simply wants to stay curled up like a spring - Bowden tubes doesn't like the resistance of the filament when it wants to go everywhere but straight. 



Between the two filaments, the 1.75mm is definitely easier to work with. You can get away with much smaller extruder setups and it also works better on DIY bowden setups if you do not want to spend the money. 



From what I hear,  it's a bit more difficult to print with 1.75mm flexible filament since it's even more flexible than 3mm. Titan and Bondtech as well as a few other designs have tried to work around this issue, but many of the extruders are simply unable to cope.



A wile back, I backed a kickstarter (or was it an indiegogo) campaign for the Craftbot which was using 1.75mm. I then decided that I should standardize on one filament.  



Since using the 1.75mm on my Ultimaker style printer, I can say that I've had less failures, but it takes just as much time to fine-tune and I haven't really seen an increase in print quality. 



As far as extruders go, I can definitely recommend a professionally produced extruder.


---
**jerryflyguy** *May 30, 2016 17:44*

**+Pieter Swart** excellent thanks!


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/R3HTwZH4SM9) &mdash; content and formatting may not be reliable*
