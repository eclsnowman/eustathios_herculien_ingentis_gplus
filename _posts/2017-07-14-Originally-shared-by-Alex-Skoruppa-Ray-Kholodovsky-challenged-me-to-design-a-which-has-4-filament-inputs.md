---
layout: post
title: "Originally shared by Alex Skoruppa Ray Kholodovsky challenged me to design a which has 4 filament inputs"
date: July 14, 2017 13:08
category: "Discussion"
author: Jeff Kes
---
<b>Originally shared by Alex Skoruppa</b>



**+Ray Kholodovsky** challenged me to design a #fila_switch_four which has 4 filament inputs. The last foto shows Ray's printed version. I have no machine with four extruders, so I am excited about Ray's feedback and if he can get it working!



You can download the Fusion 360 file and the STLs from my GitHub repo:

[https://github.com/grainiac/Carbonoid/tree/master/fila_switch](https://github.com/grainiac/Carbonoid/tree/master/fila_switch)



![images/88e2aa0a6af05bbbd2afc661c80c9232.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/88e2aa0a6af05bbbd2afc661c80c9232.jpeg)
![images/2afb4cde144095baf31eb4b912b59130.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2afb4cde144095baf31eb4b912b59130.jpeg)
![images/13aa05b249c9fa87dce5aa6a3c3f9ae4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/13aa05b249c9fa87dce5aa6a3c3f9ae4.jpeg)
![images/558bfcd23e20c4471d343b9757f7bd3f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/558bfcd23e20c4471d343b9757f7bd3f.jpeg)
![images/9f096b600bb833df8baf402fbbf72adc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9f096b600bb833df8baf402fbbf72adc.jpeg)

**Jeff Kes**

---


---
*Imported from [Google+](https://plus.google.com/+JeffKes/posts/FTUXN6DwpB8) &mdash; content and formatting may not be reliable*
