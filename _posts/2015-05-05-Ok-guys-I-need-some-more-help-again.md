---
layout: post
title: "Ok guys I need some more help again"
date: May 05, 2015 02:38
category: "Discussion"
author: Ben Delarre
---
Ok guys I need some more help again.



Got the machine back together and what I thought was moving correctly. Got my e3d extruding nicely and my bondtech extruder working perfectly.



So I decided to run the full burn-in gcode again now that I'd got everything settled in.



Works great for about half an hour, then all of a sudden something sticks. Last time it was the z axis sticking on an upwards move, previous time it was xy. So I quickly kill the machine when the steppers start making that godawful noise, power it all back up again after a reset and everything is moving fine.



Since this was the second time it happened I figured, oh the steppers must be overheating, so I checked their temps before killing the machine and they weren't that hot at all, nowhere near where I would expect them to be having thermal issues anyway. Warm, but not hot.



To recap, I've oiled everything, the XY shafts and Z smooth rod with sewing machine oil and the Z leadscrews with synthetic grease.



Each time this happens I've tried reseating the xy bearing block holders and jiggling my z clamps around a bit and I think I've got it all working, I'll get halfway through the burn-in and then it'll do this again.



I'm at a bit of a loss as to what to do now. Any suggestions? Is it time I just gave up on my plastic parts and asked one of you kind folks to print me some new ones?





**Ben Delarre**

---
---
**Gus Montoya** *May 05, 2015 03:01*

That sounds really peculiar. If something were "physically" wrong with your parts, it would show up from first atempt. Have you rechecked your gcode and settings? 


---
**Ben Delarre** *May 05, 2015 03:03*

Using the standard burn-in gcode from the repo. Settings all seem correct, movements are all the right distances so I'm pretty sure I'm good there, and I matched the current settings in smoothieware config to the motor datasheets.


---
**Eric Lien** *May 05, 2015 03:08*

Can you move the carriage by hand easily, out to every corner, by turning the stepper motor pulley with your finger tips and a light touch. Same for the z axis (all the way up top to bottom). If not you may have alignment issues. Also what are your acceleration values. And which steppers did you use?﻿


---
**Gus Montoya** *May 05, 2015 03:08*

Can you post a video of your issue? I'd like to learn this, just in case I get the same issue.


---
**Ben Delarre** *May 05, 2015 03:22*

**+Gus Montoya** just took a video of it after another round of bearing alignment, but its really hard to catch it at the moment it goes wrong, my phone won't record video for that long.



**+Eric Lien** I can't really get a grip on the motor pulley to do that. I assume you mean to take the set screws out of the motor pulley and then turn it? Can't get a grip on the pulley good enough to turn the whole gantry no. With the stepper motor pulley set screws removed I can move the gantry around with one finger, though there are some points where when I try and start a move it takes a bit more force to get it going. Any further suggestions on improving alignment?


---
**Gus Montoya** *May 05, 2015 03:25*

Could your linear rods be bent? Does the mischief occur at the same spot?


---
**Eric Lien** *May 05, 2015 03:33*

**+Ben Delarre**​ are the motors energized? You want them disabled to test the hand movements. I can move my carriage by turning the 20 tooth stepper pullies with my fingers easily.﻿ Set screed still engaged.


---
**Ben Delarre** *May 05, 2015 03:42*

**+Gus Montoya** I checked the rods against the table and they seem pretty straight, at least as straight as I can measure anyway.



**+Eric Lien** I'm using 32 tooth pulleys on the xy steppers, and 0.9 degree nema 17s from robotdigg. I disconnected the steppers from the board so they definitely are not energized.



I'm beginning to think the xy bearing blocks are not square. The frame definitely is, checked that with a machinists square, but I think the inaccuracy of my printed blocks has meant the smooth rods themselves are not square. Reason I'm thinking this is that I've noticed a very small x axis movement when rotating the y axis pulley. To be specific, I've disconnected the steppers entirely, taken the small belts off. I've then moved the x-axis all the way over so its triggered the limit switch and won't go any further and the y axis so its right at the front of the machine. Then by hand I've rotated the y-axis pulley only, as I travel from the front to the back the limit switch is released, and I can see the x-axis has moved over a small amount. Its not much, about 0.75mm over the length of travel, but that's probably enough right?


---
**Ben Delarre** *May 05, 2015 03:56*

Right, just dug out the widgets for squaring the 8mm and 10mm rods to each other. Locked one in at one end, and the other was a good 2mm out. Loosened the pulleys on that end, and then adjusted the rod to be square and fit in the widget. This has actually made the travel stiffer!



At this point I'm done.



Can some kind soul print me a complete set of Eustathios V2 parts? I will happily pay whatever is asked. Any color is fine by me as long as its ABS!


---
**Eric Lien** *May 05, 2015 04:37*

I would guess if you are that far out and binding your carriage is not square.﻿ That or the bearing blocks are not uniform.


---
**Daniel Fielding** *May 05, 2015 05:17*

You say you checked the temp of the steppers what about the stepper drivers. Maybe you are getting some random stepper driver behaviour from over heating?


---
**Ben Delarre** *May 05, 2015 05:19*

If the smoothieboard stepper drivers overheat from 20minutes of use I think it's time to just give up entirely! 

;-)



I will double check that though just in case. 


---
**Oliver Seiler** *May 05, 2015 07:51*

Can it be the steppers drivers or your PSU? It sounds a bit odd that it only happens after half an hour and can be rectified by a reset. The driver ICs on my smoothieboard spread most of their heat on the PCB side and not on top, so keep that in mind. I've pointed a little fan at the board and made sure air moves on both sides.


---
**Vic Catalasan** *May 06, 2015 23:03*

You could try running the burn-in gcode without one of the stepper belts and re-run again to try and trouble shoot where it is jamming. 


---
**Ben Delarre** *May 06, 2015 23:03*

Not a bad idea, I'll try that after a complete rebuild if the problems are still occuring.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/dEFaFiZnXuX) &mdash; content and formatting may not be reliable*
