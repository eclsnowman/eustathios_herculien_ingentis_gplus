---
layout: post
title: "I have one of the AliRubber silicone heated beds"
date: September 04, 2015 21:20
category: "Discussion"
author: Jeff DeMaagd
---
I have one of the AliRubber silicone heated beds. I know the thermistor beta spec is 3950 and its resistance is supposed to be 100k at 25C. I set up the appropriate value in smoothieware config file and I'm getting readings of 130C at room temperature. The second thermistor also reads the same. Does anyone here have any idea what to try next?



The thermistors do respond with added heat (hand touching sensor area).





**Jeff DeMaagd**

---
---
**Matt Miller** *September 04, 2015 21:25*

Maybe [http://www.ametherm.com/thermistor/ntc-thermistors-steinhart-and-hart-equation](http://www.ametherm.com/thermistor/ntc-thermistors-steinhart-and-hart-equation)


---
**Jeff DeMaagd** *September 04, 2015 21:57*

I think I figured it out. Ali Rubber said it was 100k  @ 25C. On a multimeter, it reads less than 3k in that temp range. R0 = 2850 gets me readings that line up with my thermocouple meter so far.


---
**Jeff DeMaagd** *September 04, 2015 22:22*

A beta of about 3500 gets me readings that are pretty close to my thermocouple when at temp, at least in the 60-70C range. I won't try higher just yet.


---
*Imported from [Google+](https://plus.google.com/+JeffDeMaagd/posts/b7qAxAxF1Mk) &mdash; content and formatting may not be reliable*
