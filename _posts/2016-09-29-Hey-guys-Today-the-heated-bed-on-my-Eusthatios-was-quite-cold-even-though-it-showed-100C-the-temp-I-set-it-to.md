---
layout: post
title: "Hey guys, Today the heated bed on my Eusthatios was quite cold even though it showed 100C (the temp I set it to)"
date: September 29, 2016 18:03
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
Hey guys,



Today the heated bed on my Eusthatios was quite cold even though it showed 100°C (the temp I set it to). First of all my thought was the thermistor could be broken but then I saw some strange behaviour: The thermistor showed a quite nice curve when heating up and cooling down only it was way too fast for my big 4mm aluminum plate. So i removed the bed to see if probably some cables would be broken.



Here's what I saw... (Video)



[http://d.pr/v/12M1a](http://d.pr/v/12M1a)



Anyone seen that before? And sorry for the bad video link, should have better uploaded it to youtube... next time :)





**Frank “Helmi” Helmschrott**

---
---
**Oliver Seiler** *September 29, 2016 18:26*

Not good :(

Seeing this makes me glad I've chosen a 24V heater (which can still overheat, but at least should the wires get exposed I won't get electrocuted)


---
**Pieter Koorts** *September 29, 2016 19:35*

This is what makes me scared of silicon heaters. You just can't inspect the quality of it and rely on the seller being honest (even the good ones). PCB heaters and kapton heaters can at least be inspected to see faults and quality prior to install. 



Maybe not big enough for the Eustathios but there is a mains version of PCB heaters. Still would scare the hell out of me being mains but at least it has some safety features and you can inspect for any damage.



[hackaday.io - 110 / 230 V~ PCB Heated Bed](https://hackaday.io/project/8671-110-230-v-pcb-heated-bed)


---
**Isaac Arciaga** *September 29, 2016 20:04*

**+Frank Helmschrott** which vendor was the heater from?


---
**Frank “Helmi” Helmschrott** *September 29, 2016 20:16*

Robotdigg. Will try to get some feedback from them tomorrow.


---
**Oliver Seiler** *September 29, 2016 20:26*

Arthur Wolf recently posted something about thermal run-away protection in Smoothieware - I wonder if that would pick up the bed thermistor heating up too quickly?


---
**Frank “Helmi” Helmschrott** *September 29, 2016 20:29*

I will definitely bring that as a suggestion to Roland for the Repetier Firmware. He already has quite some security measures in place but something like that should he in there too


---
**Eric Lien** *September 29, 2016 20:47*

I like alirubber heated beds from AliExpress. I have yet to have any issues with them. Very good quality.


---
**Ted Huntington** *September 29, 2016 20:49*

I have 3 silicon heaters from alirubber using 110V ac, and I find that after a while, some of them report a higher temp than measured in infrared- so I just end up setting the temp about 10-20 degrees C more than usual. But it is mysterious and a total hassle. After they have been heated for about an hour- they can suddenly tend to reach the correct temp at which time I need to lower it and go back to normal temp settings.


---
**Ted Huntington** *September 29, 2016 20:51*

I should add that it very well may be the thermistor- they go bad all the time- and it is easy to just place a cheap one from ebay just under the pad- I have done that on one pad and it is a fine substitute.


---
**Eric Lien** *September 29, 2016 23:47*

**+Ted Huntington** yeah, if I were to do it again I would just have them leave a hole in the center, making installation of replacement thermistors easy. Plus then you could drill a small hole in the 1/4" aluminum and get better contact between the thermistor and the aluminum by potting it in with high temp compound.


---
**Ted Huntington** *September 30, 2016 05:12*

**+Eric Lien** That would be great if alirubber could do that. Even so, using 110v is a lot faster than 12 or 24v and the MK PCBs- but maybe things have somehow changed since I was using those MK2A and MK2B PCB boards- or they were somehow malfunctioning- eventually they never could reach 110C- at least with 110v and silicon they can reach 110C quickly. 


---
**Frank “Helmi” Helmschrott** *September 30, 2016 05:22*

Thats exactly what I ordered at Ali Rubber now. 220V, 500W, 320x320mm, 5mm hole in center, 21$ each. 




---
**Bruce Lunde** *September 30, 2016 13:38*

I agree with all, the alirubber 110v 800w heats up so quickly.  I can start printing with my system within 10 minutes of start-up.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/5nmAXrHNRTz) &mdash; content and formatting may not be reliable*
