---
layout: post
title: "Just a little printer bling added to HercuLien"
date: June 13, 2015 20:27
category: "Show and Tell"
author: Eric Lien
---
Just a little printer bling added to HercuLien.



![images/ad199393f0b2c990d5d61123b0e9169b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ad199393f0b2c990d5d61123b0e9169b.jpeg)
![images/91d5d7c66916ad871f1e0cd7e0f16eb1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/91d5d7c66916ad871f1e0cd7e0f16eb1.jpeg)

**Eric Lien**

---
---
**Brandon Satterfield** *June 13, 2015 21:17*

Looks soooo good man! Are you back to two nozzles? 


---
**Eric Lien** *June 13, 2015 22:00*

**+Brandon Satterfield** don't tell anyone... but the black center is sharpie :)



I was going to do a filament change half way up, but fell asleep at my desk last night.


---
**Sébastien Plante** *June 13, 2015 23:10*

**+Eric Lien** haha, still nice result! 


---
**Eric Lien** *June 13, 2015 23:58*

**+Sébastien Plante** it took me too long to do too. Eyes aren't as good as they used to be, and hands not as steady as they were back in high school art class ;)


---
**Sébastien Plante** *June 14, 2015 00:02*

**+Eric Lien**​ that's why we program computer to do it for us :D﻿


---
**Joe Spanier** *June 14, 2015 01:46*

**+Eric Lien** ive never fallen asleep at my desk before. Lol nope. Not once. 


---
**Mike Thornbury** *June 14, 2015 08:04*

You want to sell those as keychain fobs, **+Eric Lien** :)


---
**Isaac Arciaga** *June 14, 2015 21:55*

Nice! Which font is that? 


---
**Eric Lien** *June 14, 2015 22:32*

**+Isaac Arciaga** I will look when I get back to my desk.


---
**Eric Lien** *June 15, 2015 02:13*

**+Isaac Arciaga** The font is "Magneto", but I used Solidworks text tool to make the font taller  than normal and tighten the letter spacing so they run together more. Then I exploded the font to lines, trimmed the letter cross over, and used the offset path tool to create an outline. 


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/dxt4R5J29x7) &mdash; content and formatting may not be reliable*
