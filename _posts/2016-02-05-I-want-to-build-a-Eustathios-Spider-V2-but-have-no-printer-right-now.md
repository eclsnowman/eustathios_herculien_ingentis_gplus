---
layout: post
title: "I want to build a Eustathios Spider V2, but have no printer right now"
date: February 05, 2016 18:16
category: "Discussion"
author: Chris Thompson
---
I want to build a Eustathios Spider V2, but have no printer right now. I'd been looking at a FolgerTech i3 2020 as a "repstrap".



Now I'm looking at the Fabrikator Mini, which, at $177 is dirt cheap.



So here's my question. I can't tell looking at github how big these individual parts are. How can I find out if the parts for the Spider v2 can all fit within the Fabrikator Mini's 80x80x80mm build envelope?





**Chris Thompson**

---
---
**Frank “Helmi” Helmschrott** *February 05, 2016 18:44*

Hey Chris,



the best way is always to find out yourself though I think that won't fit with the bigger parts.



I would advise to download Meshlab (free) to load, view and check the STLs. Meshlab has a ruler function that you can use to measure the parts.



Good luck!


---
**Chris Brent** *February 05, 2016 19:48*

Or fire up Silc3r and setup an 80x80x80mm bed :) I think the largest part is the "Eustathios Z Axis Bed Support - 2 - v1.0.stl" If I were you I'd go with the FolgerTech. There's a big community around it, and it's going to be a much more useful second printer that the Fabrikator.


---
**Gústav K Gústavsson** *February 05, 2016 19:50*

Open each file in Cura selecting the part and selecting scale gives you dimensions.


---
**Jim Stone** *February 06, 2016 05:24*

alot of folgertech things are acrylic...even the "black" ones. or atleast what i have seen


---
**Jim Stone** *February 06, 2016 16:31*

You would be correct lol


---
**Glenn West** *February 07, 2016 02:56*

A Wanhao i3 duplicator v2 is a nice bootstrap

It's my backup printer


---
**Chris Thompson** *February 07, 2016 05:22*

For the record, the very first STL I opened, 50mm_Threaded_Spool_Holder_Full_Round.STL, was 96mm tall.



The Fabrikator was a whim, really, I'm not sure I'd have ever pulled the trigger.



I'm currently stuck between the Wanhao i3 -- which has a controller you have to solder wires onto because the thermistors inputs aren't actually tied to ground and so they float 10-15 degrees -- and the Folgertech 2020 i3, which is from a company that even now still can't ship a package with all the parts. From what I gather you are guaranteed to have to call and complain and wait for them to ship.


---
**Chris Brent** *February 08, 2016 05:05*

The shipped all the parts to my Kossel. The heater block was tapped off center and they sent me a new one in a day. Once you have a kit in hand they are very responsive. You aren't paying a lot so there is a trade off.


---
*Imported from [Google+](https://plus.google.com/115929464558533781010/posts/au8vgAMiND8) &mdash; content and formatting may not be reliable*
