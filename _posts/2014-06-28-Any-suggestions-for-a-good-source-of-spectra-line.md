---
layout: post
title: "Any suggestions for a good source of spectra line?"
date: June 28, 2014 02:25
category: "Discussion"
author: James Rivera
---
Any suggestions for a good source of spectra line?  Also, typical specs for use in a reprap?





**James Rivera**

---
---
**Kalani Hausman** *June 28, 2014 02:30*

I got mine from an Ebay vendor getting rid of his fishing supplies, but you can find it almost anywhere like Amazon or your local sporting good store (my local Gander Mountain has it on sale every now and then).


---
**James Rivera** *June 28, 2014 02:33*

What strength rating did you get? (what lb test)


---
**Kalani Hausman** *June 28, 2014 02:42*

I have some 50 and 70 lb results, though the China-produced Power Pro claims 40 lb but tests closer to 25 lb. Very poor "equivalence" there.


---
**James Rivera** *June 28, 2014 03:00*

I'm looking for high grade stuff that won't stretch.


---
**Kalani Hausman** *June 28, 2014 03:08*

If you can take the time, hang lengths with a weight before stringing the printer to draw the braid closer together. You will still have some tightening to do after you use it for a while, but that gets closer to the final measure. I use a ladder and weights from my home gym, although I am sure there are more elegant solutions possible.


---
**James Rivera** *June 28, 2014 06:32*

Perhaps it would help if I stated my questions more explicitly:



1. "Spectra" is a brand. Is it the only one people here in this community have been using? (and if you have a specific reason, why?)



2. What is the minimum recommended material strength specification when using Spectra line in a RepRap Z-axis system? (and do you recommend more than the minimum?)



3. If there are other non-brand name generic versions of it, what are they made of and are they any good?



4. Finally, (perhaps most importantly) can anyone provide me with links to sources for it that they have used successfully in a RepRap for an extended period of time? (read: long enough to tell if the stuff you've used is reliable for this purpose.)



 #3dprinting  


---
**Mike Miller** *June 28, 2014 16:08*

I've got (IIRC) 100 pound "yuwei spectra braid"  in my delta. It's been in nearly continuous service since January with no issues. The only replacement occurred because I got the Butane soldering iron a little too close to it. 



The stuff really is great. And I plan on using it for the Z on my Mutt-Entis


---
**Mike Miller** *June 28, 2014 17:32*

I just got it off Ebay...it was about half the price as anywhere local ($12 or so) and is a lifetime supply. 


---
**James Rivera** *June 28, 2014 23:05*

Thanks **+Mike Miller**!  I did some searching (a lot, actually) and it certainly seems like eBay is the cheapest option--by far.  Here is a quick spreadsheet of a few 100lb test versions I'm considering.  The cheapest is only $4.18, with free shipping...from Singapore?



[https://docs.google.com/spreadsheets/d/1kQHG2X3Oit0IeU1qpNpIkc55OE0erC2oru05L4y-cnc/edit#gid=0](https://docs.google.com/spreadsheets/d/1kQHG2X3Oit0IeU1qpNpIkc55OE0erC2oru05L4y-cnc/edit#gid=0)


---
**D Rob** *June 29, 2014 00:35*

technically it is dyneema. spectra as you have said is a brand. the higher the lb test the better. off brand is what i use. dyneema is stronger than nylon and is claimed to have little/no stretch. lanyards for construction are made from dyneema (some anyway) they use a nylon tie off though because since there is no stretch to reduce velocity of a fall it creates a higher jerk that can cause it to snap if near the materials limit. the nylon section counteracts this by allowing controlled stretch. any way i digress. i use 129lb test and guitar tuners to attach it to the carriage. this makes tuning out stretch easier. do both sides at the same time to even the tension.pluck to see if they sound the same. move the axis to and fro then pluck. when the sound is constant most of the stretching is complete. if the tune lowers repeat until the sound when plucked after repeatedly moving the axis does not change. sit over night try again then when it stays constant loosen  slightly and ensure both sides play the same note. done.


---
**James Rivera** *June 29, 2014 00:45*

Thanks **+D Rob**, that makes sense. I was starting to wonder if I was looking at the wrong thing, because so far every item in my spreadsheet (see above) says, "Dyneema".


---
**Kalani Hausman** *June 29, 2014 09:24*

That's the foreign-manufactured equivalent, although sometimes you will receive items labelled Power Pro or with "Made in USA" stickers attached, meant to convince the buyers they are getting a non-China-originated alternative.


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/UWN3hpwr1CX) &mdash; content and formatting may not be reliable*
