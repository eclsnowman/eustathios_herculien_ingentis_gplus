---
layout: post
title: "Eustathios / Ingentis taking shape (kraken mounting is next, starting design of acrylic 4 walls and pyramidal top)"
date: June 21, 2014 01:34
category: "Show and Tell"
author: Tony White
---
Eustathios / Ingentis taking shape (kraken mounting is next, starting design of acrylic 4 walls and pyramidal top)

![images/f1ff1d487b293ba469df12a9b32283f7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f1ff1d487b293ba469df12a9b32283f7.jpeg)



**Tony White**

---
---
**Wayne Friedt** *June 21, 2014 01:42*

Very cool.


---
**Eric Lien** *June 21, 2014 04:12*

Love the yellow on black look.


---
**Erik Scott** *June 21, 2014 04:26*

the yellow and black combination looks very nice.


---
**Riley Porter (ril3y)** *June 21, 2014 04:39*

agree re: color.


---
**Tim Rastall** *June 21, 2014 05:48*

Ditto. You should edge the acrylic with hazard tape :)


---
**Jarred Baines** *June 21, 2014 10:07*

Oh nice!


---
**Eric Lien** *June 21, 2014 11:47*

Did you get all the correct number of t-nuts in the first time? I forgot a few since I was modeling and building at the same time. I ended up getting 100 pack of drop in t-nuts to save my sanity :)


---
**Eric Lien** *June 21, 2014 11:51*

Also for the feet I found screw in hard rubber furniture floor protectors work perfect. $2 for an 8 pack from Menards.﻿



Replace screw with 5mm pan head bolt.


---
*Imported from [Google+](https://plus.google.com/+AnthonyWhiteMechE/posts/88gasBQ95HS) &mdash; content and formatting may not be reliable*
