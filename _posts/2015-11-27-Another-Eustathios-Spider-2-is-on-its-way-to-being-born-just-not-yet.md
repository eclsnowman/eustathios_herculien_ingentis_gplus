---
layout: post
title: "Another Eustathios Spider 2 is on its way to being born- just not yet!"
date: November 27, 2015 07:11
category: "Show and Tell"
author: Ted Huntington
---
Another Eustathios Spider 2 is on its way to being born- just not yet! I'm having a lot of fun and learning a lot of new things from assembling this printer. I haven't made a print yet, I just today got the limit switches wired, and there is still a problem not shown in the video- I saved everybody's ears - the motor grinds to a halt in one particular part of the break in gcode- the part where X is near 0 and just Y is moving - where the carriage is moving in a square around the outer perimeter - I have no idea why yet. There must be less friction when the rods are also turning as opposed to moving straight on without any rotation. This is just with Marlin, Ramps, 12V. It's starting not to grind there- so perhaps it eventually gets "broken in"?- spoke too soon- it just grinded there agin! But it seems to be grinding less often at that particular area.


**Video content missing for image https://lh3.googleusercontent.com/-lhF4LHTQYMA/Vlf_51L6SuI/AAAAAAAAAV8/otqZtqRUvYg/s0/VID_20151126_225319.mp4.gif**
![images/83776f793be2e9a6fae7cd1106518dd8.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/83776f793be2e9a6fae7cd1106518dd8.gif)



**Ted Huntington**

---
---
**Ted Huntington** *November 27, 2015 07:32*

Just to update- the grinding of the motors where X is near 0 and only Y is moving seems to have totally disappeared now - so that must be somewhat common- that it just takes a little bit of running through for everything to move pretty smoothly.


---
**Miguel Sánchez** *November 27, 2015 07:51*

Your bowden tube seems a bit too long


---
**Ted Huntington** *November 27, 2015 07:54*

yeah I was just thinking that- I'm going to trim it down a lil' more.


---
**Ted Huntington** *November 27, 2015 07:56*

Even though the grinding is gone during the break in gcode, it still has trouble homing- like it can't reach x=0 and y=0 - just grinding gears. Maybe I can increase the homing speed- but it's worrisome- I hope I can solve it.


---
**Miguel Sánchez** *November 27, 2015 07:57*

maybe reduce acceleration on X and Y axis


---
**Eric Lien** *November 27, 2015 08:04*

I think you want to lower acceleration. I have been learning the hard way that I was overly ambitious with my settings. Drop X and Y acceleration a bit and see if that helps. Try 3000 for starters. If it keeps up you likely need more break-in, so alignment tweaks.



Great to see another printer running in the wild. Great job keep it up.﻿


---
**Ted Huntington** *November 27, 2015 19:41*

That was with a mac accel of 2000- so you can see how slow I go :) But I upped it to 3000 just to see what happens. Speeding up the homing solved my homing problem believe it or not. The Z has trouble and is grinding to a halt past ~80mm - the frame is badly rhombus and I'm going to try inside corner brackets at the bottom and see if that solves it.


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/fNSsAUFBaUC) &mdash; content and formatting may not be reliable*
