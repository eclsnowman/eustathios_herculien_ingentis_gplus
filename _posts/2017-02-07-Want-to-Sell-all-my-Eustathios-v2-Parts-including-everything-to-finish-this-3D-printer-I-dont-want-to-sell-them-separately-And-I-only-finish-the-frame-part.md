---
layout: post
title: "Want to Sell all my Eustathios v2 Parts (including everything to finish this 3D printer, I don't want to sell them separately) And I only finish the frame part...."
date: February 07, 2017 05:14
category: "Discussion"
author: Botio Kuo
---
Want to Sell all my Eustathios v2 Parts (including everything to finish this 3D printer, I don't want to sell them separately) And I only finish the frame part.... so you need to put other parts together ~~ If anyone is interested, pls let me know : supremeoape@gmail.com



Location : Los Angeles, CA





**Botio Kuo**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 07, 2017 05:19*

Where are you located? This may be an important factor for people to consider. 


---
**Botio Kuo** *February 07, 2017 05:53*

**+Ray Kholodovsky**  Hi Ray, yes, thank you . Location : Los Angeles, CA﻿


---
**Eric Lien** *February 07, 2017 13:37*

**+Botio Kuo**​ sorry to hear you have decided not to finish the build. I hope you can find a buyer for your parts.


---
**Botio Kuo** *February 07, 2017 17:06*

**+Eric Lien** Hi Eric, haha if no one want to buy them ... I may finish them ..


---
**Greg Nutt** *February 07, 2017 23:27*

I emailed you Botio.  Did you get my email?


---
**Botio Kuo** *February 08, 2017 07:23*

**+Greg Nutt** Hi, I did not get your email. Could you sent me again? thx


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/EqiEBLXaUNN) &mdash; content and formatting may not be reliable*
