---
layout: post
title: "So I got a chance to sit down with Jon Hirschtick (founder of Solidworks and OnShape) and his wife during their stopover in Iceland"
date: June 28, 2015 01:10
category: "Show and Tell"
author: Eirikur Sigbjörnsson
---
So I got a chance to sit down with Jon Hirschtick (founder of Solidworks and OnShape) and his wife during their stopover in Iceland. Discussing my Herculien variant (which is the biggest project (not the most complicate one though) in Onshape atm :p) , 3d printing in general and OnShape.   **+Eric Lien** He knows about you now :)

![images/25a20c35769cf5f69e260c53b3b48801.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/25a20c35769cf5f69e260c53b3b48801.jpeg)



**Eirikur Sigbjörnsson**

---
---
**Gústav K Gústavsson** *June 28, 2015 01:37*

 Well the Project is on the map now ;-). Eiríkur dont we have to share some general preliminary pictures, some preview?


---
**Eirikur Sigbjörnsson** *June 28, 2015 01:41*

Yep soon 😊


---
**Eric Lien** *June 28, 2015 14:09*

**+Eirikur Sigbjörnsson**​ excited to see what you have done since your last post. And very exciting you had time to chat with Jon Hirschtick. Onshape is an impressive platform. I need to spend some more time with it.﻿


---
*Imported from [Google+](https://plus.google.com/118262882256504121671/posts/VUG4tB7x7oN) &mdash; content and formatting may not be reliable*
