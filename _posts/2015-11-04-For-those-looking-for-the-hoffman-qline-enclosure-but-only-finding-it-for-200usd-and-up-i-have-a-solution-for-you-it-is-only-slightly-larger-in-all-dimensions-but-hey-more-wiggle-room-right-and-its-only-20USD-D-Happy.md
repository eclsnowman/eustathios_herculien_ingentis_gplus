---
layout: post
title: "For those looking for the \"hoffman qline\" enclosure but only finding it for 200usd and up i have a solution for you it is only slightly larger in all dimensions ( but hey more wiggle room right?) and its only 20USD :D Happy"
date: November 04, 2015 04:11
category: "Discussion"
author: Jim Stone
---
For those looking for the "hoffman qline" enclosure but only finding it for 200usd and up i have a solution for you



[http://www.amazon.com/BUD-Industries-PN-1335-DG-High-Impact-Indoor/dp/B005T7AMUW/ref=sr_1_3?s=lamps-light&ie=UTF8&qid=1446608615&sr=1-3&keywords=electronics+enclosure](http://www.amazon.com/BUD-Industries-PN-1335-DG-High-Impact-Indoor/dp/B005T7AMUW/ref=sr_1_3?s=lamps-light&ie=UTF8&qid=1446608615&sr=1-3&keywords=electronics+enclosure)



it is only slightly larger in all dimensions ( but hey more wiggle room right?)



and its only 20USD :D



Happy BOM hunting





**Jim Stone**

---
---
**Eric Lien** *November 04, 2015 04:13*

Great find **+Jim Stone**​


---
**Eric Lien** *November 04, 2015 04:15*

Looks like they even have prints here to model it: [http://www.budind.com/view/NEMA+Boxes/NEMA+4X](http://www.budind.com/view/NEMA+Boxes/NEMA+4X)


---
**Jeff DeMaagd** *November 04, 2015 11:58*

Hoffman has STEPs. Might be able to just print one if you have an existing machine. Example: [http://www.hoffmanonline.com/product_catalog/catalog_item_detail.aspx?cat_1=34&cat_2=159992&cat_3=2310&catID=79228&itemID=3173&searchFor=Q181310PCECC,186&itemIDs=3173,11914&catalog_item=11914](http://www.hoffmanonline.com/product_catalog/catalog_item_detail.aspx?cat_1=34&cat_2=159992&cat_3=2310&catID=79228&itemID=3173&searchFor=Q181310PCECC,186&itemIDs=3173,11914&catalog_item=11914)


---
**Bud Hammerton** *November 04, 2015 23:59*

Just as easy to model something that fits the electronics you are planning on using. That is what I have done with the Makerbase SBase + MKS-TFT touchscreen for the E.S.v2


---
**Jim Stone** *November 05, 2015 00:01*

When I don't have modeling skills and lots of others don't.  That statement is kind of null


---
**Eric Lien** *November 05, 2015 00:39*

**+Jim Stone** for the price... I would likely buy the enclosure. But that's just me. 



Again, great find.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/TURzqZw3PsK) &mdash; content and formatting may not be reliable*
