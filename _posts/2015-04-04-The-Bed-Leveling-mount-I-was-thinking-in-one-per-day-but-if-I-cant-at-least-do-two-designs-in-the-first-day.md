---
layout: post
title: "The Bed Leveling mount. I was thinking in one per day but if I can't at least do two designs in the first day..."
date: April 04, 2015 20:54
category: "Show and Tell"
author: Ricardo Rodrigues
---
The Bed Leveling mount. I was thinking in one per day but if I can't at least do two designs in the first day...

This time I only used sketch for the nut insert, the rest is done in basic shapes, perhaps it isn't the best way of doing it, but I'm still learning.

![images/789b93df57583f91505c7c2aab80572c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/789b93df57583f91505c7c2aab80572c.jpeg)



**Ricardo Rodrigues**

---
---
**Eric Lien** *April 04, 2015 23:48*

Keep them coming. Great work. Any chance you would be willing to push them to the github once complete?


---
**Ricardo Rodrigues** *April 05, 2015 07:23*

**+Eric Lien**​ of course that was the plan all along. But there are parts that will be very difficult to me. Let's see how it goes 


---
*Imported from [Google+](https://plus.google.com/+RicardoRodriguesPT/posts/R9pMKcGJfcu) &mdash; content and formatting may not be reliable*
