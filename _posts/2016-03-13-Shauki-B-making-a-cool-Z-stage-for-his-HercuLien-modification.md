---
layout: post
title: "Shauki B making a cool Z stage for his HercuLien modification"
date: March 13, 2016 16:14
category: "Discussion"
author: Eric Lien
---
**+Shauki B**​ making a cool Z stage for his #shaulieki HercuLien modification. Looking good my friend. Reminds me of the z stage of the F306 printer: [http://www.fusion3design.com/f306_large_print_volume_3d_printer/](http://www.fusion3design.com/f306_large_print_volume_3d_printer/)





**Eric Lien**

---
---
**BİRALOGEG Ali İçer** *March 13, 2016 19:39*

do you have update Herculien master ?


---
**Eric Lien** *March 13, 2016 23:37*

**+Ali içer** this is not a change to HercuLien, but **+Shauki B**​ 's take on the HercuLien with many of his own modifications.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/P9G9jef2Ue9) &mdash; content and formatting may not be reliable*
