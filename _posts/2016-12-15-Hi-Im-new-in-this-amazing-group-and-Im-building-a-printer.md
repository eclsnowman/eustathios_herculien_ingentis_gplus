---
layout: post
title: "Hi, I'm new in this amazing group and I'm building a printer"
date: December 15, 2016 13:33
category: "Build Logs"
author: Ionut Aurelian Ariton
---
Hi, 

I'm new in this amazing group and I'm building a printer. I think I name it Frankenstein, because it has the design of the Eustathios, but it's built on bigger extrusion profiles and a little bit bigger dimmensions, pieces from different printers. The goal is to build a cheap 3d printer and mostly DIY. Today it made it's first movements of the XY axis. Work goes on! 

[https://drive.google.com/open?id=0B6tPM_8eUH36dXlTUXNFZklPbXc](https://drive.google.com/open?id=0B6tPM_8eUH36dXlTUXNFZklPbXc)





**Ionut Aurelian Ariton**

---
---
**Eric Lien** *December 15, 2016 14:09*

Looking great. I am always happy when I see new builds and especially happy to see build modifications. It's seeing creativity like this that really makes you appreciate the value of open source hardware designs. 



I am excited to see more of your build progress as time goes on and to see your first print once you reach that milestone. Keep up the good work and keep posting!


---
**Eric Lien** *December 15, 2016 14:11*

Oh, and don't forget to add your build to the list we started ([https://plus.google.com/+EricLiensMind/posts/Dz2h22GHX4m](https://plus.google.com/+EricLiensMind/posts/Dz2h22GHX4m)). As I review the list and see how many people and countries these designs have reached it is truly inspiring. 


---
**Ionut Aurelian Ariton** *December 15, 2016 18:12*

Thanks for appreciations! I hope to see the first print soon. 

I updated the file! 


---
*Imported from [Google+](https://plus.google.com/+IonutAurelianAriton/posts/5e2VaKyZjVj) &mdash; content and formatting may not be reliable*
