---
layout: post
title: "For the Eustathios Spider v2, is there a trick to keeping the \"I\" frame, 3 extrusion pieces of the bed from getting loose?"
date: November 16, 2015 21:37
category: "Discussion"
author: Ted Huntington
---
For the Eustathios Spider v2, is there a trick to keeping the "I" frame, 3 extrusion pieces of the bed from getting loose? I find that I tighten the 2 M5 screws holding them together as tight as I can, but the extrusion forms like a lever and somehow makes it easy to loosen the connection to the middle bar. I suppose that when the Aluminum plate is connected with springs the force to loosen it would be less.





**Ted Huntington**

---
---
**Eric Lien** *November 16, 2015 21:40*

Maybe some loctite. I have never had this issue on the bed personally, so I am not sure what the root cause could be.


---
**Tomek Brzezinski** *November 16, 2015 21:46*

Loctite, is king. I would use a medium grade not a high grade, though, as it is very hard to heat-out a strong loctite on an aluminum piece. 



Do not be fooled into thinking split washers or any kind of basic washer would do shit for you. They just don't. 



If eric hasn't had the problem though maybe you have an issue somewhere? Are the threads too loose? Hopefully you can catch this problem soon 


---
**Ted Huntington** *November 16, 2015 21:55*

You read my mind- I thought a washer might help - oh well- forget that - yeah I don't think it's a serious enough problem to worry about - it seems to stay pretty firmly together as long as I don't move it too much. I thought there might be some stupid thing I was not aware of or something that I was doing wrong that would solve it. Thanks for the good suggestions.


---
**Tomek Brzezinski** *November 16, 2015 22:52*

I've been meaning to try to find out why "split washers" ever became popular, when reading about their holding force under vibration conditions everywhere says they don't hold up. Maybe there was a reason at some point but I haven't found it.  Next time they get loose though, you may as well throw on a bottle of off-brand loctite. You can pickup a small bottle on ebay for $3.  Unlike most specialty glue,s because oxygen infiltration prevents the glue from setting (it sets in anaerobic conditions), they last seemingly forever (i've never had a bottle go bad.)  I keep a small bottle of medium loctite and a bottle of "wicking grade" loctite which can be useful for bearing housings and for when you want to throw on some small amount of loctite after assembly. 



In terms of "old things that are still used despite superior technology" I've read that phillips heads are so shitty in part by design. In manual factory assembly without an impact wrench the bit tends to slip in a way that prevents overtorquing. This was useful before ubiquitous torque wrenches. ....Mildly irrelevant sidenote. 


---
**Jim Stone** *November 16, 2015 23:47*

Just standard blue loctite. gel is my preferred type. or if youre lazy you can also use the green penetrating type so you dont have to unscrew. green however is weaker.



dont use red though unless you never EVER EVER want it apart without you getting very very angry and destroying the screw. red loctite is "forever: loctite. or atleast until heated. and even then it is still an pita


---
**Ted Huntington** *November 17, 2015 00:14*

**+Tomek Brzezinski**

interesting about Phillips head versus other screw head designs- I opted for Phillips screws for most of this Eustathios Spider v2 instead of socket cap screws because in my experience using a drill bit to fasten/unfasten screws I find the socket cap head strips sooner than a Phillips head.


---
**Ted Huntington** *November 17, 2015 00:15*

**+Jim Stone**

ok thanks for the info- I had never really thought much about loctite until now- interesting story.


---
**Jim Stone** *November 17, 2015 00:26*

yeah man Loctite brand and other similar products are really nice. i prefer the gel type. some people prefer the liquid type. i would recommend the gel type in a twist tube i always end up getting the liquid everywhere.



and what i have used before as a poormans loctite in low strength and low heat applications is Watered down white school glue.


---
**Bud Hammerton** *November 19, 2015 21:33*

Linseed oil also works in a pinch, as it semi solidifies and keeps threads from loosening due to vibration. That's an old school wheel building trick.


---
**Ted Huntington** *November 19, 2015 22:22*

**+Bud Hammerton**

Wow linseed oil- I have never heard of using that to tighten a thread. Another undiscovered trick of science!


---
**Bud Hammerton** *November 20, 2015 16:24*

Like I said, it's an old school wheel building trick. Back from the day when you tied and soldered spokes together. No need to do that today with so many thread tightening compunds avaliable.


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/7CM5WkAjgNL) &mdash; content and formatting may not be reliable*
