---
layout: post
title: "Since i have converted to belts i had to give the Prusa mendel i2 a workout tonight"
date: September 17, 2014 04:48
category: "Show and Tell"
author: Jim Squirrel
---
Since i have converted to belts i had to give the Prusa mendel i2 a workout tonight. 3 1/2 hr  print replacing the **+Tim Rastall** bearing brackets with a modified **+Eric Lien**  bearing block.  Also I'm using the motor Mount that **+Brian Bland** sent me to have direct drive. It's slowly getting together



![images/72ee0ed9e5b42ca07d16856b180d93c5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/72ee0ed9e5b42ca07d16856b180d93c5.jpeg)
![images/410351fccadddb8f5d1616e4d9f60ee1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/410351fccadddb8f5d1616e4d9f60ee1.jpeg)
![images/70adebd67939dd3eb87d2f00a5e1a1e8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/70adebd67939dd3eb87d2f00a5e1a1e8.jpeg)
![images/431be393c1e836b8f2baa0fa566cb776.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/431be393c1e836b8f2baa0fa566cb776.jpeg)

**Jim Squirrel**

---
---
**Jim Squirrel** *September 17, 2014 09:07*

Well as a father of five boys, avid vegetable gardener (because i have so many mouths to feed),and participant in the e-nable group  ( [http://enablingthefuture.org/](http://enablingthefuture.org/) ) i'm happy with what progress i have accomplished. Even more ecstatic with what i have learn on my way to bigger better printers. Ironically as I am finishing this printer I will be receiving my MakerLibre Delta from the indiegogo campaign. So this is the year of printers in my house.


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/HiY6Jfxj8z3) &mdash; content and formatting may not be reliable*
