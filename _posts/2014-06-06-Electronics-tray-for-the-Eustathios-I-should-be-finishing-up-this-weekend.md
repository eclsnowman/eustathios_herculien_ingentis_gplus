---
layout: post
title: "Electronics tray for the Eustathios I should be finishing up this weekend"
date: June 06, 2014 22:10
category: "Show and Tell"
author: Eric Lien
---
Electronics tray for the Eustathios I should be finishing up this weekend. MAN steppers take a long time to get to the states via China Post. But that's OK. I got to spend more time modeling little tweaks like this.﻿



![images/4ab005b449804b80196db8c49c2a9cc3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4ab005b449804b80196db8c49c2a9cc3.jpeg)
![images/454dcacb5523f2fb73ad4f24a58047d3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/454dcacb5523f2fb73ad4f24a58047d3.jpeg)
![images/ed66443164ebcd87caeb217d711be863.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ed66443164ebcd87caeb217d711be863.jpeg)
![images/fe7d8f1e42e4e6b44b800edab18205eb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fe7d8f1e42e4e6b44b800edab18205eb.jpeg)
![images/b5e0b45934354e1c51fc8a2329101a49.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b5e0b45934354e1c51fc8a2329101a49.jpeg)
![images/9a2029d2a349dce402aa182ad98eee24.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9a2029d2a349dce402aa182ad98eee24.jpeg)

**Eric Lien**

---
---
**Wayne Friedt** *June 06, 2014 23:14*

Very nice.


---
**Tony White** *June 07, 2014 01:42*

Care to share? I'm using the exact same electronics on my build, and would love to save the time...


---
**Eric Lien** *June 07, 2014 01:46*

**+Anthony White** sure. What format. Its syncing right now to the github as a Solidworks 2014. But I could do whatever format you want.﻿



Solidworks 2014:

[https://github.com/eclsnowman/Lien3D_Eustathios_Spider/blob/master/Electrical/Electronics_Mount.SLDPRT](https://github.com/eclsnowman/Lien3D_Eustathios_Spider/blob/master/Electrical/Electronics_Mount.SLDPRT)



STL:

[https://drive.google.com/file/d/0B1rU7sHY9d8qQl9yMFZIZGJmU2M/edit?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qQl9yMFZIZGJmU2M/edit?usp=sharing)﻿


---
**Eric Lien** *June 07, 2014 03:19*

**+Eric LeFort** thanks.


---
**Tony White** *June 07, 2014 03:29*

Thanks - Having it SW native is definitely best. I'll share when I have mine setup!


---
**Eric Lien** *June 07, 2014 03:32*

**+Anthony White** great. If it's like some of the other posts I have seen you make I am sure your design files will be much cleaner than mine. I am self trained on Solidworks and basically learn as I go.


---
**Jarred Baines** *June 08, 2014 11:24*

Jesus - that's a HUGE / good print - what material? didn't lift / warp?


---
**Eric Lien** *June 08, 2014 13:25*

**+Jarred Baines** it is Abs from from pushplastics. Printed onto glass covered in kapton with some glue stick on it. I have a full enclosure on my printer which helps. Other than some stringing I am pretty happy.﻿ No lift, but I used a brim and added the lighteners. This limits the shrink that tends to pull long flat surfaces off the build plate... And it saved filament.


---
**Bijil Baji** *June 13, 2014 23:39*

**+Eric Lien**  What os is the raspbery Pi running OctoPrint or any other custom os??


---
**Eric Lien** *June 14, 2014 00:04*

Octoprint SD image (based on raspian) updated to devel branch. 


---
**Bijil Baji** *June 14, 2014 00:20*

I thought so. octoprint is a good way to add wifi and lan conncetivity to a 3dprinter or cnc..


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/6fy73Rjtpdf) &mdash; content and formatting may not be reliable*
