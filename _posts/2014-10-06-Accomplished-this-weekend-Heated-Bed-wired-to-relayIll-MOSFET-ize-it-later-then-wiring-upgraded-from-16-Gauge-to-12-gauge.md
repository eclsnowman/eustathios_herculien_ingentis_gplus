---
layout: post
title: "Accomplished this weekend: Heated Bed wired to relay(I'll MOSFET-ize it later), then wiring upgraded from 16 Gauge to 12 gauge"
date: October 06, 2014 11:53
category: "Discussion"
author: Mike Miller
---
Accomplished this weekend:



Heated Bed wired to relay(I'll MOSFET-ize it later), then wiring upgraded from 16 Gauge to 12 gauge. Verified long term up to 100C (lesson learned: Infrared non-contact thermometer doesn't work: no reflective surfaces! Measures 25C on the aluminum plate, 80C on the heater surface...plate sure felt hotter than 25C. :) )



Buck Step down converter installed to power raspberry Pi ([http://www.ebay.com/itm/141082066753?ru=http%3A%2F%2Fwww.ebay.com%2Fsch%2Fi.html%3F_from%3DR40%26_sacat%3D0%26_nkw%3D141082066753%26_rdc%3D1](http://www.ebay.com/itm/141082066753?ru=http%3A%2F%2Fwww.ebay.com%2Fsch%2Fi.html%3F_from%3DR40%26_sacat%3D0%26_nkw%3D141082066753%26_rdc%3D1)) 



Extruder motor assembly installed but not wired. 



Broken: Left side top pulley assembly. Oops. 



Reason I'm posting: Between Boy Scout campouts and Halloween (a BIG deal with my family: 
{% include youtubePlayer.html id=gD1av-rE7Ho %}
[Haunt's Belfry - Halloween 2009](https://www.youtube.com/watch?v=gD1av-rE7Ho) ), I'll most likely not get to work on the printer til November. :(





**Mike Miller**

---
---
**Øystein Krog** *October 06, 2014 17:10*

Thanks for the tip for the converter, I picked up a few as well just now, great to have for misc uses:P



I used this relay, it's been working great.

[http://www.ebay.com/itm/251525260933?_trksid=p2060778.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT](http://www.ebay.com/itm/251525260933?_trksid=p2060778.m2749.l2649&ssPageName=STRK%3AMEBIDX%3AIT)



I did ofcourse disable PID mode in Marlin and use bang-bang instead, but temperature stability is no problem at all.

What relay did you use?


---
**James Rivera** *October 06, 2014 21:39*

**+Mike Miller** Ok, this is a nice looking power widget with some beefy looking caps and all...but the haunted house!?!  WOW!  I'm betting your kids LOVE it! :-D


---
**Matt Kraemer** *October 06, 2014 22:45*

**+Shauki Bagdadi** I drop the temperature a few degrees per 5 layers at the top of narrow parts where the layers take less than 3 seconds.


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/jmYgpor2iwJ) &mdash; content and formatting may not be reliable*
