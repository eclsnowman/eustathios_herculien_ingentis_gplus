---
layout: post
title: "Just wanted to post about Re-ARM for RAMPS for those who weren't aware of it"
date: November 20, 2016 05:00
category: "Discussion"
author: Luis Pacheco
---
Just wanted to post about Re-ARM for RAMPS for those who weren't aware of it.



I know **+Eric Lien** must know about it, he's in the special thanks section!



Thought it was an interesting find since there may be a few people out there with RAMPS 1.4 boards laying around and may want to upgrade them to allow running of smoothieware.



I'm grabbing a few just because I have one or two RAMPS boards laying around and know a few people with them as well that may like the upgrade.



What do you guys think about it?





**Luis Pacheco**

---
---
**Ted Huntington** *November 20, 2016 06:47*

Great idea- and it looks like it is taking off! $20,000 pledged already on a campaign goal of $5,000- that seems like about 500 PCBs already. 


---
**karabas3** *November 29, 2016 08:13*

It need to be cheaper to win against MKS SBASE MINI


---
*Imported from [Google+](https://plus.google.com/110276424073473119972/posts/EwAgUkeyUDV) &mdash; content and formatting may not be reliable*
