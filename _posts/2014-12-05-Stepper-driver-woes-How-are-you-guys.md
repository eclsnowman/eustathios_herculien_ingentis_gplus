---
layout: post
title: "Stepper / driver woes :-( How are you guys?"
date: December 05, 2014 08:29
category: "Discussion"
author: Jarred Baines
---
Stepper / driver woes :-(



How are you guys? Again I've been away for a while, got some time to jump back onto things and I've been trying to troubleshoot a x/y axis problem I'm having - I have ruled out mechanical issues as it is almost the same problem at 10mm/s as it is at 100mm/s (and the mechanics are really beautiful).



The problem is I'm loosing steps, repeatable each time I print the same part, I have a hunch this is 'stepper high delay' related or similar firmware issue.



I've narrowed the missing steps down to this:



Parts that involve a lot of XY detail are FINE.

Parts that involve a lot of E retractions are the culprit.



What settings do you guys use for your machines (I'm specifically using repetier firmware v.92)?



Keeping in mind I've CNC machined most of my components from oh-so-heavy (but oh-so-shiny!) aluminium, I have reduced the acceleration to 1000 (have had it as low as 500 trying to sort this out with no positive results). I have jerk set at 5 and I have reasonable microstepping (100steps/mm).



I've used the A4988 and DRV8825 drivers, both had issues, and I've since grabbed a few wantai DQ420MA drivers which give me much smoother, more powerful operation but, freak out when e retracts...



The loss of steps is EXTREMELY small, I can do things like vases that have no retraction and have no error, but noticed it when I started printing some lettering and with 8+ retracts per layer it started skewing. It's an almost perfect skew, not random in any way, so I'm guessing it's loosing the same steps at the same point on each layer. If I rotate the model 90 or 180 degrees the skew follows it. If I change the start point of my loops from front left to rear right the skew changes direction with that. (might be a hint - printing the word "Techne" it prints from left to right so, 6 retracts where it moves right immediately after and only 1 retract where the move is in the opposite direction)



I see for 'gantry systems', we do need a stepper high delay (according to repetier firmware setup) and I may also need to increase 'stepper direction delay' and 'quadstepping delay' but I'm not sure where to start.



Anyone have any issues/tips/information about movement / steps / firmware?





**Jarred Baines**

---
---
**Whosa whatsis** *December 05, 2014 08:53*

Hmm, first question I would ask to rule out possibilities is what slicer are you using, and have you been able to reproduce the results with more than one of them?


---
**Jarred Baines** *December 05, 2014 08:56*

Ah, yes... I'm also 10+years CNC experience and so can read gcode like English, its a clean, straight wall in the gcode - have tried with slic3r and kisslicer.



Possible point of interest - motors are directly coupled to  one side of the driven shaft for x/y (like xmaker posted very early in this community)﻿


---
**Whosa whatsis** *December 05, 2014 09:26*

You get identical results WRT the shifting in Slic3r and kisslicer? Interesting, have you tried printing the same gcode, but with zero retraction?


---
**Miguel Sánchez** *December 05, 2014 10:37*

If you turn 90 degree and same artifact turns too: that seems unusual. Do you mean you suspect step signal pulse-width is the problem? ﻿could you repeat the test with allegro or texas drivers?


---
**Jarred Baines** *December 05, 2014 11:59*

I don't know a lot about drivers, it is my weak point, I know a lot about mechanics and am fairly good with software (firmware) but electronics... Nope...

I have some "reprapdiscount A4988's" and some "pololu drv8835's and neither of them really had enough grunt for my liking but I will possibly try the drv8825s again...



I do suspect the pulse signal width is causing an issue.



**+Whosa whatsis**​ I have tried printing with zero retraction - it was flawless (blobs aside) straight as...



Just worked out my speeds where the double stepping kicks in, my extruder would have hit double steps during retracts but there are no other points in the program which would double step... So I reduced the extruder retract speed to be sure that st NO POINT do I double step...



The skew disappeared, but still some small, random layer shifting remains... I'm going to keep this speed and try to tune the stepper high delay and direction delay to work, then introduce the double stepping...



I hope I'm right... This has been doing my head in all week...


---
**Jarred Baines** *December 05, 2014 12:41*

**+D Rob** , been reading some of your posts and realised you were the one organising the 32t robotdigg pulleys, what (was) your setup before the leadscrew conversion? I'm interested in your settings, driver used, microstepping if you remember and anything else you remember about the setup including accel, speeds etc. Just so I have some idea of what to expect.



**+Tim Rastall** - similar question, did you experience any signal errors between RUMBA and drivers or any problems with those motors from your ingentis parts list (as they are the ones I've also purchased) they are 42BYGHM809's, 0.9deg



Did you guys modify the stepper delays in firmware or just used the stock values?


---
**D Rob** *December 05, 2014 16:29*

1/16 micro stepping and just plugged in straight from the Prusa calc. I use as stock a version of Marlin as possible (tried and true) my pulleys were 36t on the Motor and 32t on the rod. 


---
**D Rob** *December 05, 2014 16:32*

Lower micro stepping in x an y and see if this fixes the problem. The Arduino may be struggling. Also try Marlin and run from Cura with the current setup then after dropping micro stepping. Lower your accel and jerk (the price you pay for aluminum)


---
**Eric Lien** *December 05, 2014 17:47*

**+D Rob** **+Jarred Baines**​ I ran into problems with the 0.9deg steppers and 1/32 microstep. I now run 1.8deg steppers with 1/16 microsteps with zero problems.


---
**Tim Rastall** *December 05, 2014 18:33*

**+Jarred Baines** with the rumba I have seen crosstalk between adjacent drivers on 2 separate boards but it happened irrespective of retraction as I recall. 


---
**karabas3** *December 06, 2014 16:45*

I can recommend to go to closed loop stepper drivers/motors. The guy on [reprap.org](http://reprap.org) sells custom made ones. I am personally short of money now but I believe it is a must have. See [http://forums.reprap.org/read.php?1,263680,page=1](http://forums.reprap.org/read.php?1,263680,page=1)


---
**Jarred Baines** *December 07, 2014 15:20*

I've found that I was mistaken, the problem isn't due to retraction, it CAN happen without retraction. I'll need to test further...



**+karabas3**  The guys at work are using closed loop "hybrid steppers" (possibly the same thing as you're describing) and they're awesome motors, powerful, quiet and of course, self-correcting. I'm a bit strapped for cash as it's all going into the house at the moment but one day I would love to upgrade simply for the security.



**+Tim Rastall** I ditched the rumba and started hooking up the ramps last night based on your comment, I suspected exactly that, crosstalk between the drivers or 'signals on the board' (excuse my lack of electronics knowledge). Basically because I felt I had ruled out much else... I've narrowed it down to "somewhere between the rumba and the motors"



Since I'm so good at hooking up electronics, I had some really spastic movements late last night before deciding to go to bed, I will continue forth with the RAMPS tomorrow if I have time. Cheers for the info ;-)



**+Whosa whatsis** **+D Rob** **+Eric Lien** Thanks for the info too guys - I was concerned about using the 32t pulleys directly driven (thought I may need to gear them down but prefered not to, minimizing backlash)



I have to say though, it takes a LOT to stop the carriage with drivers set to 1.5A (motors are rated at 1.7, I'd assume this is RMS?) the print skews slightly with this amperage. With 2.0A the print has a skew, but also jumps a few mm every few mm ADDITIONALLY to the skew. At a lower setting (1.2A) the print I was testing on turned out perfectly. The problem is at this low amperage, any prints that curl even slightly DO stop the carriage and obviously stuff everything up...



Anyways, for now any further speculation is welcome but just wanted to say thanks for responding so far and I'll report back when I can try some stuff out!


---
**Jarred Baines** *December 12, 2014 16:33*

Narrowed it down to my motors, possibly faulty or just incompatible... Have replaced everything else in multiple configurations with the same issues...



Looking for recommendations on  some good motors, preferably 1.8 deg per step


---
**Eric Lien** *December 12, 2014 18:50*

I love my robodigg motors.


---
**Jarred Baines** *December 12, 2014 20:37*

Any specific model?


---
**Eric Lien** *December 12, 2014 20:42*

On #Herculien I use these. [http://www.robotdigg.com/product/29/Nema17-60mm-1.5A-high-torque-stepper-motor](http://www.robotdigg.com/product/29/Nema17-60mm-1.5A-high-torque-stepper-motor)


---
**Eric Lien** *December 12, 2014 20:44*

They have a ton of torque for 1.5a steppers and work great for high speed x/y without loosing steps. For Z that will depend on if your bracket allows for the extended length of a 60mm long motor.


---
**Eric Lien** *December 12, 2014 20:46*

Otherwise I know these should work: [http://www.robotdigg.com/product/7/NEMA17-Stepper-Motor-40mm-Long,-1.2A](http://www.robotdigg.com/product/7/NEMA17-Stepper-Motor-40mm-Long,-1.2A)


---
**Jarred Baines** *December 13, 2014 02:08*

Excellent - thanks for the info mate ;-) much appreciated!



Are you running these off DRV8825's or something else?


---
**Eric Lien** *December 13, 2014 06:51*

Panucatt dvr8825 on my azteeg x3


---
*Imported from [Google+](https://plus.google.com/+JarredBaines/posts/h9LW1YJz29Z) &mdash; content and formatting may not be reliable*
