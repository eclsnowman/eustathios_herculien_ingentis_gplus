---
layout: post
title: "How to make a nearly smooth carriage...a smooth carriage:"
date: March 06, 2016 16:56
category: "Show and Tell"
author: Mike Miller
---
How to make a nearly smooth carriage...a smooth carriage:



[https://metaprintr.wordpress.com/2016/03/06/eureka-smooth-moving-bushing-based-carriages/](https://metaprintr.wordpress.com/2016/03/06/eureka-smooth-moving-bushing-based-carriages/)





**Mike Miller**

---
---
**Eric Lien** *March 06, 2016 17:04*

Have you found they had any extra play after alignment with the reamer? If not I say... Good job. This would allow use of less expensive non-self-aligning bushing for people making their builds on a higher budget.


---
**James Rivera** *March 06, 2016 17:15*

Nice! First Google hit for me...

Price:	$7.28 Free shipping for Prime members when buying this Add-on Item: [http://www.amazon.com/Cutting-Operated-Reamer-Reaming-Length/dp/B00CW8UXRI](http://www.amazon.com/Cutting-Operated-Reamer-Reaming-Length/dp/B00CW8UXRI)


---
**Mike Miller** *March 06, 2016 17:16*

Since you're implementing a carriage that's affected by gravity ( :) ) and you have belts around the perimeter of the carriage handling XY alignment and loading, the slight amount of runout this adds to the equation isn't significant, and can be adjusted out with belt tension. 



I say this knowing someone probably COULD hamfistedly make a carriage this way that would suck...this is just a cheap way of making 'not quite good enough' good enough. If there were ONE bushing in each direction, you couldn't rely on it, if you have two, petty much ANY distance apart, you'll be fine so long as the reamer can reach both in one go. 



It also allows you to friction fit something like IGUS bushings, then restore their working surface to 10mm, if your friction fit was overly aggressive. The reamer cuts bronze, IGUS, and aluminum easily. 


---
**Ted Huntington** *March 07, 2016 05:57*

Yeah using a reamer on the bushings is a great idea. I think the bushing might come loose and start spinning sometimes because of the force of the reamer. There are a lot of low cost bushings on ebay and aliexpress- so it could reduce the price of these printers - but the difference in price between self-aligning bushings and regular bushings is not too much- perhaps $20-$30 less for a printer like the Eustathios.


---
**Mike Miller** *March 08, 2016 00:06*

The main issue is that there are several parts of a build where there's an inexpensive or more expansive way to build...adding them all up, however, can add 30% to the total build (or more). 



FWIW, I now see the benefit of roller-over-extrusion designs ( **+Shauki B** **+Michaël Memeteau** )   ...you lose the protection of the chassis hiding the moving parts, but there's no through-hole alignment issues..


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/RdWbwVSw3Es) &mdash; content and formatting may not be reliable*
