---
layout: post
title: "What lubrication did everyone use on their linear rods?"
date: June 28, 2015 21:22
category: "Discussion"
author: Gus Montoya
---
What lubrication did everyone use on their linear rods?





**Gus Montoya**

---
---
**Bruce Lunde** *June 28, 2015 22:00*

I use sewing machine oil.


---
**Eric Lien** *June 28, 2015 23:00*

Very very light oil. The bronze bushings are pre-impregnated with oil. So you need little to no oil on them. For the lead screws/nut I use  superlube. Its available on Amazon: [http://www.amazon.com/gp/aw/d/B000XBH9HI/ref=mp_s_a_1_1?qid=1435532354&sr=8-1&pi=AC_SY200_QL40&keywords=Superlube&dpPl=1&dpID=31-xhQ8JfAL&ref=plSrch](http://www.amazon.com/gp/aw/d/B000XBH9HI/ref=mp_s_a_1_1?qid=1435532354&sr=8-1&pi=AC_SY200_QL40&keywords=Superlube&dpPl=1&dpID=31-xhQ8JfAL&ref=plSrch)



Just don't use superlube on the bronze bushings rods, or the linear ball bearings... Just the lead screw.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/fPMni6j31Pq) &mdash; content and formatting may not be reliable*
