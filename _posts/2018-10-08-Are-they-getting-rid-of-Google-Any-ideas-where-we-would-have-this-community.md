---
layout: post
title: "Are they getting rid of Google+? Any ideas where we would have this community?"
date: October 08, 2018 18:06
category: "Discussion"
author: Brandon Cramer
---
Are they getting rid of Google+? Any ideas where we would have this community? **+Eric Lien**



[https://www.cnn.com/2018/10/08/tech/google-plus-security/index.html](https://www.cnn.com/2018/10/08/tech/google-plus-security/index.html)









**Brandon Cramer**

---
---
**Eric Lien** *October 08, 2018 19:52*

I am torn. Not just for the loss of the community. But the backlog of knowledge in these posts is indispensable. Our little group in the Tantillus, Ingentis, Eustathios, and HercuLien builders has spurned innovation in 3d printing. I have watched ideas in our group, and the general 3dprinting g+ group turn makers into self sustainable businesses with lots of employees. 



It is truely a sad day. And I am totally open to ideas from anyone for migration of our group and our back catalog of ideas.


---
**Scott Hess** *October 08, 2018 20:20*

I feel like for the backlog of ideas part, you could probably do some sort of data takeout and recast it as static pages.  It's even possible that Google will leave it up as static pages, though I would not hold my breath on that.


---
**Eric Lien** *October 08, 2018 21:15*

**+Scott Hess** yeah, I hope data take out includes the full thread conversation. Because if it's only my replies and posts... and not other users contributions to a discussion... Then it doesn't help much. 


---
**Scott Hess** *October 08, 2018 22:22*

I just did a takeout on my personal Google+, and it gave me HTML files of posts and comments.  It included some stuff like videos and photos, but I'm not sure where the line is, there (like would it include Google Photos albums which were shared via Google+, or only the earlier Google+ photo-sharing stuff?).  Postings included comments. 

 It didn't give me any communities I'm a member of, though reading around it sounds like on other products (like Groups), you can use takeout to get things out.



I guess just go ahead and run a takeout and see what you get!  Though, knowing Google, I would expect you'll have some time to decide.


---
**John Driggers** *October 09, 2018 00:05*

I asked our Google guy, he said the party line is 10 months (Aug '19) and that better export tools will be coming...  




---
**Eric Lien** *October 09, 2018 01:18*

**+John Driggers** thanks for the feedback. I would just really hate to lose all the valuable information the group has collected over the years. I hope they come up with a good solution that will make redistributing the information somewhere feasible.


---
**Sébastien Plante** *October 10, 2018 11:21*

**+Eric Lien** [https://takeout.google.com](https://takeout.google.com) :)


---
**Scott Hess** *October 10, 2018 19:09*

[Google+ Mass Migration](https://plus.google.com/communities/112164273001338979772) might be interesting.


---
**Michael K Johnson** *January 25, 2019 01:21*

**+John Driggers** It's pretty much clear now that Google isn't going to provide "better export tools" and **+Sébastien Plante** takeout works only for your own personal posts, not for whole communities.



Before Google clamped down on exports and started accusing me of being a robot, I got a relatively complete dump of this community and have been working with **+Eric Lien** to get it imported at [eclsnowman.gitlab.io - HercuLien and Eustathios community archive](https://eclsnowman.gitlab.io/eustathios_herculien_ingentis_gplus/) — it's missing pieces, but I think a lot of the knowledge is preserved for posterity now.



**+Scott Hess** Consider doing a JSON takeout of your personal posts as well as HTML. The JSON is more useful if you ever want to export it to some other format. HTML is more useful to browse personally on your own computer. Both seem to be necessary to get as complete information as possible.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/Zq2Cncfb6xA) &mdash; content and formatting may not be reliable*
