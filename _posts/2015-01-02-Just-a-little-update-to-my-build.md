---
layout: post
title: "Just a little update to my build"
date: January 02, 2015 19:50
category: "Show and Tell"
author: Florian Schütte
---
Just a little update to my build. I managed to flatten the bed (hammer and beer, thanks **+Eric Lien** :D). The x/y mechanics is moving by steppers now but needs some calibration. next step will be to assemble z mechanics and wait for the hotends from china.



![images/419835285335b8906cc20a0c9dea09ee.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/419835285335b8906cc20a0c9dea09ee.jpeg)
![images/2c78f89fcb22095f34efc654a4505a7c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2c78f89fcb22095f34efc654a4505a7c.jpeg)

**Florian Schütte**

---
---
**Eric Lien** *January 02, 2015 21:06*

Nice looking machine **+Florian Schütte**​



I can't wait to see your first prints.


---
**James Rivera** *January 03, 2015 01:05*

The black anodized aluminum adds a nice look to it! Also, the slang term for the hammer and beer is, "percussive maintenance". ;)


---
**Mike Thornbury** *January 03, 2015 05:36*

Alcohol-assisted persuasion.


---
**Florian Schütte** *January 03, 2015 08:36*

**+James Rivera** thank you. The cladding at Bottom will also be black anodized aluminium. When it's finished and running, i may will print black parts for it.


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/YJXeT9UsEvh) &mdash; content and formatting may not be reliable*
