---
layout: post
title: "Z Max is down. Giving some thought to endstops"
date: September 04, 2014 12:47
category: "Deviations from Norm"
author: Mike Miller
---
Z Max is down. 



Giving some thought to endstops. My printer's got the igentis method for lifting the Z axis, so I'm thinking a microswitch on a spring loaded base to set Z-max. X and Y I can get by with moving the sled with motors off. 



I'm still ruminating on a counterweight system - the build plate is cresting 6 Lbs/ 2.7 kg and I'm reluctant to leave it purely at the whims of the controller and motor. 



So, set a microswitch so that there's some deflection on springs with the platform all the way down (to keep from smashing it on power failure, then lift bed off switch, reverse direction until endstop hit, and use that to suss out where Zero Level is at the top of travel. 



Thoughts? 





**Mike Miller**

---
---
**Eric Lien** *September 04, 2014 13:37*

Gear reduced stepper on the z and it will never fall unless the belt snaps.﻿



[http://www.robotdigg.com/product/90/Geared-Nema17-Stepper-Motor](http://www.robotdigg.com/product/90/Geared-Nema17-Stepper-Motor)


---
**Miguel Sánchez** *September 04, 2014 14:02*

Two thoughts: no XY microswiches makes all simpler but resuming a part in mid-print (maybe you run out of filament mid-print or detected some missed steps). Geared stepper on Z not only helps keeping the bed in place without power as **+Eric Lien** suggests but it will provide you better Z resolution too.


---
**Mike Miller** *September 04, 2014 14:27*

Z will definitely be one of those things that iterates to a satisfactory solution. I have a second motor I could wire it to provide some passive braking if it didn't introduce printing defects when the other motor is moving things the other way. 



I may also have to reprint the effector...using 10mm shafts means slightly larger 40 tooth pulleys, which means some slight interference issues I have to work around. 



It's all part of why I like this hobby. There's fresh ground to walk on that hasn't been engineered away by large companies with millions in R&D. 


---
**Miguel Sánchez** *September 04, 2014 14:50*

**+Shauki Bagdadi** of course, are we to ask for directions? No way! :-)


---
**James Rivera** *September 04, 2014 15:56*

I like the idea of a counterweight (or a flywheel), but I do not like using an opposing stepper because induction could cause problems with printing. Ideally, I think it would be best to just keep the moving mass as low as possible instead of trying to mitigate it. Is there any way to shave some of the mass of the platform? Carbon fiber or...3D printed structure? 


---
**Mike Miller** *September 04, 2014 16:04*

Possibly. I'm using Extrusion in an H shape, the aluminum plate sits above springs and bolts to set level. The cork is there to keep the aluminum from becoming a great big heat-sink...and the heater and glass contribute to the mass. There's competing criteria here...the build plate has to be thick enough to handle the mass, the sub-structure has to be heavy enough to support the heated bed, and there has to be separation points to easily swap  glass for fresh builds.


---
**Mike Miller** *September 04, 2014 16:05*



Oh, and the other motor wouldn't be connected to anything, it'd just have the wire pairs connected to make it hard to turn. 


---
**James Rivera** *September 04, 2014 18:38*

If you're using glass as the print bed, and an H-shape extrusion as the primary support structure, then it seems the aluminum is only needed to support the glass in such a way as to prevent it from breaking.  If you drilled lots of holes or otherwise cut out sections of the aluminum it might significantly reduce the mass while still supporting the glass bed. Drilling holes would probably be easiest, but large squares, or if you have a mill, maybe even honeycomb shaped?


---
**James Rivera** *September 04, 2014 18:42*

Also, that is what I thought you meant (the stepper motor), but the faster you move the z-axis the greater the resistance to motion because the inductance would be greater. I don't know offhand if regular print speeds would be affected (they might), but I bet it would be really annoying when you're trying to move the bed up after a tall print.  Hmmm...maybe a switch to disconnect the coils (thus "turning it off") in that case would be useful?


---
**Mike Miller** *September 04, 2014 18:52*

Because this was always intended to be a heated bed/enclosed chamber printer. 



Honestly, I got in on the gekkotek kick starter, so might not need it, we'll see. 



I've tried abs on blue tape with superglue, and with two sided tape and it always seemed like a crapshoot. 


---
**Mike Miller** *September 04, 2014 18:54*

**+James Rivera** I may look into aluminum sheet I can bend to support. The plate served two purposes: I already had it, and it would fully support the heater+glass


---
**James Rivera** *September 04, 2014 19:40*

**+Shauki Bagdadi** Glass is a cheap way to get the ever-important flat surface.  I've used ordinary window glass (3/32"<2.5mm) without any problems with my heated bed to print both PLA with glue stick and ABS with hairspray (ABS juice is probably better, but I've never tried it).



That being said, now I mostly use glass with blue painter's tape on a cold bed to print PLA (love it!), and only print ABS on my bot with a machined aluminum bed and Kapton tape on a heated bed.



(aside: this may change when put my older bot's parts into an extrusion frame, because I like the idea of a removable bed, and it is much cheaper to have multiple glass print beds than multiple aluminum print beds.)


---
**James Rivera** *September 04, 2014 19:49*

**+Mike Miller** you might even try just using some OTS aluminum angle bar with flat pieces and screws at the corners to keep it all the same level.  Quick, dirty, and cheap enough that even **+Shauki Bagdadi** might approve!


---
**Mike Miller** *September 05, 2014 11:20*

The Glass plate and H frame are the two heaviest parts of the platform, no doubt. If the Gekkotek plate is in any way naturally rigid, I may forgo the glass altogether. (Pity...that's $25 for two build plates I might not had had to spend.)


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/auv7LmLEok8) &mdash; content and formatting may not be reliable*
