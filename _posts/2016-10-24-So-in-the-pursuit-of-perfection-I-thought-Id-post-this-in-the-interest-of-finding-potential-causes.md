---
layout: post
title: "So in the pursuit of perfection, I thought I'd post this in the interest of finding potential causes"
date: October 24, 2016 03:50
category: "Discussion"
author: jerryflyguy
---
So in the pursuit of perfection, I thought I'd post this in the interest of finding potential causes. 



Appreciate the insight of others on what causes it.




{% include youtubePlayer.html id=se1pqgil0gE %}
[http://www.youtube.com/watch?v=se1pqgil0gE](http://www.youtube.com/watch?v=se1pqgil0gE)





**jerryflyguy**

---
---
**Eric Lien** *October 24, 2016 04:29*

That is likely the backlash at the direction change of the axis. You can check the belt tension and pulley grub screws first. 



Next would be how easy does the axis move by hand (loosen xy motor mounts and remove the continuous loop drive belt to eliminate the motors from the equation). If there is resistance to motion you are likely seeing the a slight unloading of the spring of the system as the rods reverse the flex from the drag as the axis reverses. This usually improves as the bushings break in.


---
**jerryflyguy** *October 24, 2016 18:47*

**+Eric Lien** thanks. I'll go over it again and check the screws. It's got a hundred hrs (or so) of printing on it so, logical that some might have loosened up. I've still got to change out some bearing holders so might do that all at the same time. 



Curious as to your thoughts on putting a larger pulley on the ends of the x/y rods? I'd keep the open loop belt pulleys the same as they are (32 tooth) but jump the outside drive pulleys to 50+ tooth, this would increase my resolution a bit, wonder if that'd smooth out some of the 'texture' I see on my parts?  Or is that going down the wrong path?


---
**Eric Lien** *October 24, 2016 21:49*

Increasing gear reduction will help resolution, but at the cost of speed of course. I doubt will be a big problem, maybe just in non-print travel moves which tend to be faster.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/cn7Q4cs47pV) &mdash; content and formatting may not be reliable*
