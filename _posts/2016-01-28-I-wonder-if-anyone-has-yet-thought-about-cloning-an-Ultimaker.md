---
layout: post
title: "I wonder if anyone has yet thought about cloning an Ultimaker?"
date: January 28, 2016 23:00
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
I wonder if anyone has yet thought about cloning an Ultimaker? 



I'm looking for a good desktop size printer that can sit next to my computer, look good and don't take so much room. I'd be fine to have a small print bed for that and PLA only. 



The Ultimaker 2 Go looks quite cool for that and as it's open source and not so different to an Eusthatios Spider I thought I cant be the first one thinking about that. I already checked plans and looks like I would have around 80% of the stuff lying around.... what do you think?



[https://github.com/Ultimaker/Ultimaker2Go](https://github.com/Ultimaker/Ultimaker2Go)





**Frank “Helmi” Helmschrott**

---
---
**Eric Lien** *January 28, 2016 23:04*

Should be doable. Do you want panels, or extrusion for the frame?


---
**Tomek Brzezinski** *January 28, 2016 23:19*

I mean,  I was under impression ultimaker and makerbots were always cloned from China within 8 months of their release.  Probably if you want a literal clone then working from an alien press base would be most affordable, assuming you get anything of remote quality.  Personally I love ultimaker but I don't think their gantry scales well to bigger sizes,  so I don't think they're worth cloning if you want a bigger build platform. 


---
**Tomek Brzezinski** *January 28, 2016 23:19*

That was supposed to say aliexpress,  not alien presss


---
**Frank “Helmi” Helmschrott** *January 28, 2016 23:22*

**+Eric Lien** definitely panels. I have some interesting samples here that should be big enough to do a Go. 



**+Tomek Brzezinski** I don't want to go bigger but as small as the go is. apart from that their gantry stability should be about the same as the Eusthatios is. 


---
**Tomek Brzezinski** *January 28, 2016 23:24*

If you want to stick to same size or smaller,  you'll be OK.  The main issue with their gantry for scaling is that rotational inertia becomes significant on the rotating rods.  Also bear in mind you shouldn't replace any bushings with linear bearings for the parts that rotate rods, if you feel tempted to do so while cloning it. Since linear bearings aren't designed to rotate like that. 


---
**Frank “Helmi” Helmschrott** *January 28, 2016 23:31*

sure, thanks for the advise. the problems and facts are quite about the same to the Eusthatios Spider I already built. Also I'm not going to clone as much as possible but use up the most parts I already have. Sure thing that bushings will have to stay bushings where they need to rotate. the hot end part will be a bit hard to keep as compact as they have it - at the end I don't want to loose too much space there. 



we'll see. I'll keep you posted if I do. have to finish another printer beforehand ;-) 


---
**Christian Gooch** *January 31, 2016 03:13*

Why not a tantillus?  I'm in a similar situation as I'd like something small and good looking to sit by my computer.


---
**Frank “Helmi” Helmschrott** *January 31, 2016 07:47*

I already thought about building a Tantillus in the past. I don't know the actual state but going metric was a bit hacky back then, i didn't like the exsternal gears and the fact that it uses fishing lines. Although I ever wanted to try these and already have a spool I'd prefer to have something more reliable for this desktop machine. 



Also i'm aiming to make it look professional and sleak - the Tantillus is way more hacky. But don't get me wrong: It's most probably a solid, good working printer.


---
**Igor Kolesnik** *February 02, 2016 18:16*

[http://www.thingiverse.com/thing:811271](http://www.thingiverse.com/thing:811271) maybe to hacky but DIY clone


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/bBgY3QytFTr) &mdash; content and formatting may not be reliable*
