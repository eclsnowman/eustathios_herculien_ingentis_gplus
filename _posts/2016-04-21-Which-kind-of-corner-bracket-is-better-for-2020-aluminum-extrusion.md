---
layout: post
title: "Which kind of corner bracket is better for 2020 aluminum extrusion?"
date: April 21, 2016 10:16
category: "Discussion"
author: Makeralot
---
Which kind of corner bracket is better for 2020 aluminum extrusion? 17x20 piece or 20x28 piece?



![images/0f1237a1086576678acf89b879467cfd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0f1237a1086576678acf89b879467cfd.jpeg)
![images/3a7ce1cc111a9314c8ade2f2383e95b3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3a7ce1cc111a9314c8ade2f2383e95b3.jpeg)
![images/d987a2c410f631b0789875681f4d0355.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d987a2c410f631b0789875681f4d0355.jpeg)
![images/a92c98e07522511e3fcbbab4ce4ee133.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a92c98e07522511e3fcbbab4ce4ee133.jpeg)

**Makeralot**

---
---
**Pieter Koorts** *April 21, 2016 10:33*

A wider grip should technically be more rigid but other factors still apply. Also will the build allow larger brackets without moving to much about to accommodate the size?


---
**Alex Paverman** *April 21, 2016 12:09*

Do you really think it matters which?! I'd use (in fact, I did!) the cheapest ones... Not so much forces to really matter the size.


---
**Philipp Tessenow** *April 21, 2016 14:28*

I'd go with the smaller ones (left)


---
**Ryan Carlyle** *April 21, 2016 14:47*

Check if the small one allows you to use socket-head cap screws or if you have to use button head screws. Being forced to use a specific screw type is a pain.



Bigger will be more rigid. 


---
**Eric Lien** *April 21, 2016 22:03*

I am with **+Ryan Carlyle**​, go with the bigger ones. More contact area means more strength.


---
**Eric Moy** *April 22, 2016 09:04*

There's trade offs. The wider stance gives better rigidity, but larger footprint, so you won't be able to fit things next to it. The extra rigidity is probably unneeded in a 3d printer of this size, but till clearance is definitely better on the larger ones


---
*Imported from [Google+](https://plus.google.com/+Makeralotcom/posts/XFzeNEUGtQM) &mdash; content and formatting may not be reliable*
