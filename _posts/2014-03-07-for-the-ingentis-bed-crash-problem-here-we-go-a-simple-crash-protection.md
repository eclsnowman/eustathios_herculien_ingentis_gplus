---
layout: post
title: "for the ingentis bed crash problem. here we go, a simple crash protection"
date: March 07, 2014 02:41
category: "Discussion"
author: D Rob
---
for the ingentis bed crash problem. here we go, a simple crash protection. makes active z leveling impossible as is but hey I'm working on a solution for this too. ;)

![images/e5eccb42b971137eca2efc570d60e300.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e5eccb42b971137eca2efc570d60e300.jpeg)



**D Rob**

---
---
**D Rob** *March 07, 2014 03:18*

oh yeah to find these look up broom hanger in google and click images.


---
**D Rob** *March 07, 2014 03:20*

**+Tim Rastall** whats your opinion on this? would have to be customized or hacked to fit but the idea seems solid for breaks to me


---
**Jarred Baines** *March 07, 2014 04:15*

Are you thinking this would ride on the gear, belt or smooth rod (ie attached to the carriage)?


---
**David Heddle** *March 07, 2014 04:26*

[http://www.thingiverse.com/thing:184890](http://www.thingiverse.com/thing:184890)


---
**D Rob** *March 07, 2014 04:37*

**+Jarred Baines** rod of course these are used to hang mops and brooms and stuff


---
**D Rob** *March 07, 2014 04:38*

**+David Heddle** exactly


---
**Jarred Baines** *March 07, 2014 05:11*

Ah! Good find! I reckon that'll do it ;-)


---
**Jarred Baines** *March 07, 2014 05:13*

I was originally thinking it was supposed to work 90 degrees around, didn't realize the bracket was to seat the broom handle, looks much better now that I see how it's used ;-)


---
**D Rob** *March 07, 2014 05:15*

I would sugest ptinting with a curve to gain surface area on the smooth rod


---
**Jarred Baines** *March 07, 2014 05:26*

Yes, and on the 'stopper' also I guess, or this may not be necessary, might create more friction when printing.


---
**D Rob** *March 07, 2014 05:30*

no damn it this wont work. the bed travels down as it prints this would engage the break. there would need to be a way to engage it. maybe it could be held open via electromagnet that releases on power loss?


---
**D Rob** *March 07, 2014 05:34*

but with a break we still need a gcode run at the end of prints to lower the bed else when the motors disengage the stage falls but still has power to the electromagnet or more likely solenoid. How do these fair in a heated chamber?


---
**Jarred Baines** *March 07, 2014 05:35*

Oh shit...



Either would the cam or ratchet... it's GOTTA move that direction to print... I always think Z is up but on your bots it moves downward...



>_<



How did we all miss that?


---
**D Rob** *March 07, 2014 05:40*

I dont know. My running machine is a mendel 90/I3 hrbrid lol so I think of z as a head movement by default


---
**Jarred Baines** *March 07, 2014 07:36*

Yeah mines a RepRapPro Mendel so same deal...



I think you could hook the break up to the "common" (not sure if its neg or pos) lead on the stepper? Stepper off = solenoid retracts and brake is applied? That way z is free to move and CAN ONLY move when driven by the stepper?



We have a similar setup on the fourth axis CNC at work, when indexing, if the 4 axis is not being moved it is locked with a brake.


---
**D Rob** *March 07, 2014 09:34*

Or solenoid via relay to PSU or even straight to PSU since it is for power failure. Honestly though I think we need firmware correction that would remember the x, y, z, coordinate last commanded and store to eeprom or something so interrupted prints could be finished. Z could be homed. Via max﻿


---
**Tom Øyvind Hogstad** *March 07, 2014 12:45*

Wonder how a simple rubberband on the shaft of the Z motor and something else would work. The tension of the band will stop the motor for moving when not powered, naturally it have to be loose enough to allow it to move when powered :-)


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/d7cMFutfhcJ) &mdash; content and formatting may not be reliable*
