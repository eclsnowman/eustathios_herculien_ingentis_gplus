---
layout: post
title: "I am looking to get the Eustathios Aluminum build plate"
date: October 18, 2015 19:56
category: "Discussion"
author: Bud Hammerton
---
I am looking to get the Eustathios Aluminum build plate. Does anyone have any ideas where I can get one made, or have they bought one recently?





**Bud Hammerton**

---
---
**Eric Lien** *October 18, 2015 20:04*

**+Jeff DeMaagd**​ you up for making more?  Otherwise you could make the bed like **+Walter Hsiao**​ built it. It simplifies it to just a square you could have cut at a local metal house easily, then just drill 4 holes.


---
**Daniel F** *October 18, 2015 20:23*

I did what **+Eric Lien** sugested, I just ordered a square cast and surface milled aluminium plate 310x310x6mm.  I used 3 countersunk screws to fix it. This way, the build plate can be fixed inside the printing area, no need to mill a custom shape.


---
**Bud Hammerton** *October 18, 2015 20:27*

Square would work, and be cheaper too. I might then be able to use a local shop. Just wasn't sure if the cutouts on the sides were necessary or just decorative.



This might allow me to jump up to a cast bed (MIC-6) to ensure flatness.


---
**Eric Lien** *October 18, 2015 20:50*

**+Bud Hammerton** the big thing is if you go inside the build area go thick enough to countersink the heads of the bolt to fit the glass over them without obstruction.


---
**Bud Hammerton** *October 18, 2015 20:57*

**+Eric Lien** If I go a full 400 x 360, I just have to drill a couple of extra holes (or a notch) for the ballscrew and 10 mm rail. If not I will likely go 1/4" and countersink for flat head cap screws. and it looks like the leveling screws move completely inside. Then I need to worry about the heater pad getting in the way.


---
**Bud Hammerton** *October 18, 2015 21:00*

Maybe I could eliminate the four plastic blocks and just drill the leveling screw holes directly into the bed frame? What do you think?


---
**Jeff DeMaagd** *October 18, 2015 21:25*

I have one water jet cut plate left that I'd been keeping for myself. I can always have more cut later though. I wasn't pushing them or even had it listed because I'm still in my busy season.


---
**Bud Hammerton** *October 19, 2015 01:01*

**+Jeff DeMaagd** I'd you're willing to pay with it send me an email. 


---
**Jeff DeMaagd** *October 19, 2015 05:04*

I'm not sure how to private message on G+, I don't want to exchange email addresses in the open.


---
**Bud Hammerton** *October 19, 2015 05:11*

**+Jeff DeMaagd** I sent you my email address, but if you can see the little quotes icon in the upper right of the Plus page, that starts a hangouts conversation (equivalent to old school private messaging) it looks like quotes in a cartoon text bubble.


---
**Bud Hammerton** *October 19, 2015 14:25*

**+Jeff DeMaagd** Thanks for the offer, decided to go a slightly different route with a square MIC-6 plate. I have all the machine tools I need here to cut and drill manually. I was looking for options to make it easier on myself.


---
**Bud Hammerton** *October 21, 2015 17:24*

Figured out what I am going to do to mount a rectangular plate instead of the profile in the model. Will cut one more extrusion @ 321 mm, then make the bed support square instead of I shaped. Then the leveling mounts can be moved around to the front and rear sides of the bed rather than on the same sides as the ball screw. With the mounts in that location there is no drilling other than the 4 holes for mounting/leveling, no interference with the glass build plate on top or the heater underneath.



So my build plate will be 15.75 x 14.25 x .25" (still don't understand why American machine shops insist on using imperial measurements rather than metric). This does add a part (the extra piece of extrusion), but simplifies construction and lowers the cost of cutting a build plate.



I hope I explained that adequately, and I hope it might help someone else in the same situation to find a less expensive solution to having a build plate CNCed or Waterjet cut.


---
**Sean B** *July 10, 2016 04:11*

**+Bud Hammerton** Would you be able to upload pictures of your bed setup?  I am going the same route.  I have the square bed (15.75x14.25x0.25") and extra extrusion at 321mm.  Thanks!


---
**Bud Hammerton** *July 10, 2016 13:12*

**+Sean B** I will get some pictures up this afternoon. Off to Sunday Services with my family this morning.


---
**Sean B** *July 10, 2016 13:19*

**+Bud Hammerton** Awesome!  Thanks.


---
**Bud Hammerton** *July 10, 2016 20:14*

**+Sean B** It is pretty simple. Here is what I did. First I doubled up on the 321mm length of extrusion. It is the middle of the H shape. I could then use one at each end instead of just a single one in the middle. I then took the two 365mm lengths of extrusion and tapped the holes in the end. I took the four Bed_Leveling_Mounts and moved tem to the ends, securing the outer most hole with a 5mm screw directly into the end of the newly tapped extrusions, the other end was attached to the shorter extrusions with a t-nut. No other modifications were made to the support structure. The end result was a square bed frame rather than the original H bed frame.



The plate is as describe above and was a simple square, no fancy cutouts or relief on any side. Much cheaper to have cut. I drilled the four holes for the leveling screws myself at assembly time.



Here are a few pictures, they are at awkward angles since there is little room for a DSLR in that space.

[https://drive.google.com/file/d/0ByUVV3BSj0O6Vzl5blJKZU5XWEU/view?usp=sharing](https://drive.google.com/file/d/0ByUVV3BSj0O6Vzl5blJKZU5XWEU/view?usp=sharing)

[https://drive.google.com/file/d/0ByUVV3BSj0O6R1VwUHdRa0VkREE/view?usp=sharing](https://drive.google.com/file/d/0ByUVV3BSj0O6R1VwUHdRa0VkREE/view?usp=sharing)

[https://drive.google.com/file/d/0ByUVV3BSj0O6b3lncjZOZmw4Q0k/view?usp=sharing](https://drive.google.com/file/d/0ByUVV3BSj0O6b3lncjZOZmw4Q0k/view?usp=sharing)


---
**Sean B** *July 10, 2016 20:18*

This is great, hopefully in the next week or two it will be together.  Thanks again.


---
*Imported from [Google+](https://plus.google.com/+BudHammerton/posts/AuBihfnt8Zp) &mdash; content and formatting may not be reliable*
