---
layout: post
title: "Eric Lien mentioned he likes videos of first prints so I thought I'd post a couple of mine"
date: October 11, 2016 16:43
category: "Discussion"
author: jerryflyguy
---
**+Eric Lien** mentioned he likes videos of first prints so I thought I'd post a couple of mine. Wiring is a rats nest currently but figured I'd do some runs on it in case I have to make changes (and I have had to make a couple).



Need to print belt guards and spool holder yet but for my first few prints I'm pretty happy. Lots of tuning needed yet on **+Simplify3D**  to get it all perfect.




{% include youtubePlayer.html id=-wtqryyPCmg %}
[https://youtu.be/-wtqryyPCmg](https://youtu.be/-wtqryyPCmg)




{% include youtubePlayer.html id=1sg5JV1psM0 %}
[https://youtu.be/1sg5JV1psM0](https://youtu.be/1sg5JV1psM0)



(Sorry for the music in the second one, was just on my PC in the background)



I've printed a couple Marvin's and a Benchy last night, will post some pics of those w/ their warts 😉)





**jerryflyguy**

---
---
**Eric Lien** *October 11, 2016 16:48*

Looking great. Keep them coming. And if you have close-up shots of the prints along with s3d factory files with questions on how to tune settings we can likely give some advice. 



Great see it printing :)


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/6nbNAEtsLrZ) &mdash; content and formatting may not be reliable*
