---
layout: post
title: "I have a a few questions (OK, 6) about the BOM, ballscrews and Linear shafting"
date: November 28, 2017 03:19
category: "Discussion"
author: Dennis P
---
I have a a few questions (OK, 6) about the BOM, ballscrews and Linear shafting. Thanks for making the model available, it really helps to drill down to see parts and assemblies. 



1. Has anyone tried direct driving the shafts to the motors? I don't mean with couplers, I mean removing the shaft of the motor and turning down the SFJ to 8mm to press the bearings and rotors on. 



2.  Is there a consensus on Carbon Fiber rods/tubes for linear shafting? I see some mention of them in the older discussions by **+Michael Thornbury** and **+Eric Lien**, but not much followup. 



The weight reduction makes the most sense. Both for traversing the carriage and for rotational inertia of the 10mm SFJ guide rods.  I think that the ends with the timing pulleys would need some special attention to fill them so that they don't crush under setscrews, filled epoxy or JBweld.....   



3. Ballscrews- Was there any followup on the 46mm vs 40mm shaft dimension discrepancy for bearings from Misumi?  I am on the fence about ballscrews or TR*/10/12's. 



4. What size trapezoidal leadscrews have people used for the Z axis? Going from 12mm ball to 8mm screw is a little concerning . Granted the vertical guide rod braces the screw from buckling, I would think that we would want some more mass in there.  What did you use for nuts-- standard or anti-backlash? Bronze or delrin? 



5. Timing the Z lift screws- how are people getting the timing of the screws. I gues question I am trying to ask is getting the screws and nuts in sync so that one does not lead the other as they turn. 



6. BOM: Is the BOM direct report from Solidworks? I can not for the life of me get document handler working right with the eDrawings viewer right now. 



-Are the duplicate entries for bearings? Some are listed at Fast Eddy and at Robotdigg. Same bearing, different sources and counts for the same gear..  



-Filament: About how much filament should one expect to use to print out a park kit? I want to get enough of a 'classy' color. 



-Titanium 8-32 nuts? Is this a goof? 



Thanks in advance



Dennis 

 







**Dennis P**

---
---
**Eric Lien** *November 28, 2017 04:02*

Here are my answers in order by the numbers above:



1.) No one has ever went direct drive to the shaft on the X/Y Motors (some have with a coupler, but none ever with a turned down shaft as far as I understand it). I have never had issues with my belt system so I think you would have diminishing returns... but am always open to be proved wrong. Another option is something like what **+Mike Miller** did with his #ingentilire. This removed the linking belt by making the drive belt on one side also be the main drive belt. 



2.) I worry about binding and racking with carbon fiber rods... the coefficient of friction of oilite bronze on steel is very low. But I would love to see it done. I agree the rotating mass and cross rod inertia would he helpful. Then again I can print very fast with minimal ringing artifacting with things as is.



3.) I say go with the golmart ball screws... or just TR8x8 leadscrew. 12mm is way overkill. The weight is in compression and gravity acts as a great backlash compensation already. if I were to do it again I would probably save the costs and do leadscrews with an acetal nut (not brass the acetal can go faster for z-hop.)



4.) See 3.



5.) You just wait to tighten the pulleys on the lead screw until you have the two sides leveled. Timing of that is no issue. And once timed the continuous belt keeps things timed. This part is super easy so don't worry about it. You will see once the parts are together.



6.) for the BOM yes it is a direct solidworks export (hence all the mcmaster parts which you should buy elsewhere for costs savings). Everything is organized in the BOM by the sub assemblies in the model. If you have issues with edrawings... you can try fusion360 it will import the solidworks or X_T models really well.. There is also a 3D PDF for viewing only (requires real acrobat reader not something like foxit).



7.) Yeah on the two sources of the 6900 bearings... you can go with Fast Eddies or Robotdigg. 



When I have printed parts before I used just over a spool of filament. It depends on the number of perimeters and infill percentage. I recommend at least 3-4 perimeters on load bearing parts. wall thickness trumps infill for strength any day of the week.



Titanium nuts... yeah thats wrong. I must have grabbed the wrong bolts from Mcmaster while building the 3D model. BTW don't buy hardware from Mcmaster or Fastenal unless your work gets a crazy high discount. Go with [https://www.trimcraftaviationrc.com](https://www.trimcraftaviationrc.com) .  Gino over there has taken care of people in the community and has sourced parts for people looking to buy all the hardware needed  for the build in one location. If you need something they don't list just drop them a line, they can probably get it in a few days. And the prices are crazy good for 100% stainless hardware. I buy hardware I don't even need just in case I do one day. I have a more metric bolts than my local hardware store now thanks to these guys :)



[trimcraftaviationrc.com - Trimcraft Aviation RC](https://www.trimcraftaviationrc.com/)


---
**Mike Miller** *November 28, 2017 04:10*

#ingentilire needs a redesigned z-stage. X and Y are still great. The problem with these designs is getting everything in the same plane with home shop tolerances. But when you get it, it’s a great low-mass design. 



The problem with carbon fiber is friction...leading to carbon fiber fuzz...choosing a bearing and bearing surface is not hard, but the paths are well trodden for a reason. 


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/8bTj9UK3FyT) &mdash; content and formatting may not be reliable*
