---
layout: post
title: "Design opinion needed! Great designs that I can check out in Solidworks shared by Eric Lien"
date: April 11, 2015 15:24
category: "Discussion"
author: Carl Hicks
---
Design opinion needed!

Great designs that I can check out in Solidworks shared by Eric Lien.



I intend on building a variant that is smaller. I will start with the bed then out from there. (prusa i3 size)



I currently use an Ultimaker 2 at work. One feature I like is the vertical rods that hold the bed are both on the back (on one side). The bed is smaller and with the size of the beds on the HercuLien and Eustathios this would clearly be impractical. Basically while printing I can get some material that curls up on thin section it can lift above the height of the nozzle. On the next layer as the nozzle passes over this area I can see the bed flex slightly until higher up the build when It sorts itself out. I see this as a good thing as If it was rigid I would imagine it would just break the print off the bed.



How do these printers cope with this? both look very rigid but how does it manage when the nozzle has to pass over filament that has curled up or some ooze that come out halfway in the print?



Is there any flex?   

![images/796c4f56f248a47c9f3d88fa87524da3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/796c4f56f248a47c9f3d88fa87524da3.jpeg)



**Carl Hicks**

---
---
**Jason Smith (Birds Of Paradise FPV)** *April 11, 2015 15:29*

I would say that a more rigid design allows for more precise adjustment, which should let you dial the bed height in once and then not worry about it. Also, I use a spring-mounted bed on the eustathios which allows the bed to depress slightly if absolutely necessary. 


---
**Eric Lien** *April 11, 2015 15:42*

Usually plastic curling up is a sign that you need to slow down or add more cooling. But yes both beds are on springs to allow give on a nozzle catch. This is one reason I love ABS, it is far less likely to curl up like PLA and if it does the nozzle just tends to plow through and keep printing whereas PLA tends to cause a catch.


---
**Carl Hicks** *April 11, 2015 15:42*

Yes they both look very nice and rigid. The springs on the Ultimaker 2 are pretty stiff and don't move at all unless something goes very wrong. It just has a nice bit of movement that allows it to flex then back to normal as soon as the nozzle passes the raised part of the print.


---
**Eric Lien** *April 11, 2015 15:43*

P.S. I love the side by side render.


---
**Carl Hicks** *April 11, 2015 15:55*

Yes all the parts are looking good from your modeling. I think I have just lost a few mates. (Solidworks mates) If you can see the floating rods on the HurcuLien.  


---
**Eric Lien** *April 11, 2015 16:03*

**+Carl Hicks**​​ yeah I have seen that before too. Sometimes setting lightweight to resolved in large assembly mode solves it.﻿ If you figure out the reason let me know. I model parts in subassemblies, and so sometimes things go wacky.﻿


---
**Carl Hicks** *April 11, 2015 16:08*

Yes Solidworks is not an exact science. Check my profile if you want to see some of my Modeling all with Solidworks. 


---
**Jeff DeMaagd** *April 11, 2015 16:34*

Alumimaker 2 is an Ultimaker 2 that's supposedly easier to buy the parts for.


---
**Eric Lien** *April 11, 2015 16:40*

**+Carl Hicks** wow you have some design chops. I am looking forward to your scaled down version and also any recommendations you can make to improve my models. I am still pretty green at modeling, and self taught so I likely learned some bad habits.


---
**Carl Hicks** *April 11, 2015 16:59*

I am also self taught. I had managed to pass the Certified Solidworks Professional test before I even met or spoken to another Solidworks user. 



I will be building a smaller printer for home before I work on a design. Probably the prusa i3 hephestos. That will help fill in some gaps in my knowledge.



Apart from the usual performance of the printer I need to think about the looks. It has to pass the wife test to be able to live at home. Also safe for children and fairly quiet.


---
*Imported from [Google+](https://plus.google.com/100407738905084935809/posts/BYVXw8kGeNt) &mdash; content and formatting may not be reliable*
