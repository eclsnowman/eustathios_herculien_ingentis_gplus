---
layout: post
title: "I'm having a hell of a time getting Simplify 3D to work right"
date: December 16, 2015 05:49
category: "Discussion"
author: Erik Scott
---
I'm having a hell of a time getting Simplify 3D to work right. I can print a Benchy boat quite well in Cura (with the exception of the bit with the smokestack, where it blobs up a bit when it slows down) but I just can't seem to get it right in Simplify 3D. With the settings shown, which I have tried to match to my Cura settings as closely as possible, I always get this blob when, I believe, it switches from the first to second layer. It's like the filiment is backed up, but again, this doesn't happen in Cura for, what I can tell, the same settings. 



Can someone who's familiar with simplify 3D have a quick look at my settings and perhaps point out a few things I could try? All and all, I'm REALLY disappointed with Simplify3D thus far. I wasn't expecting miracles, but I didn't expect things to get so much harder and worse. 



![images/47f8645b8f0134c5c58169ae12c5b534.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/47f8645b8f0134c5c58169ae12c5b534.jpeg)
![images/f4930161f3f210de9eca2a6f7731f663.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f4930161f3f210de9eca2a6f7731f663.jpeg)
![images/f28c591da92df1492d93ce700772c4ad.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f28c591da92df1492d93ce700772c4ad.jpeg)
![images/3101d042d62358cb447c1e3a8fe49282.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3101d042d62358cb447c1e3a8fe49282.jpeg)
![images/f8e87f6e05bfcf7571f107eac67ece0d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f8e87f6e05bfcf7571f107eac67ece0d.jpeg)
![images/cbf107d1dc34d5b1a705e7103be44eef.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cbf107d1dc34d5b1a705e7103be44eef.jpeg)
![images/12735b931c63e6aa60b01f77eb8f64da.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/12735b931c63e6aa60b01f77eb8f64da.jpeg)
![images/b3a128939f9783f1b61d1d74fa460fc3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b3a128939f9783f1b61d1d74fa460fc3.jpeg)
![images/99bae19a8444c698b39facf98618c0de.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/99bae19a8444c698b39facf98618c0de.jpeg)
![images/e445d652c057323d7521279850885f6c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e445d652c057323d7521279850885f6c.jpeg)
![images/11e00b957e9fe23db710acb52f9691c2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/11e00b957e9fe23db710acb52f9691c2.jpeg)
![images/ada343a7d980e9ae10bceb5665187fc7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ada343a7d980e9ae10bceb5665187fc7.jpeg)
![images/a19b516984de793869b3fb6294cbb803.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a19b516984de793869b3fb6294cbb803.jpeg)
![images/81bfd4cb856ba66fb7d1a08d2d6e39a4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/81bfd4cb856ba66fb7d1a08d2d6e39a4.jpeg)
![images/1fb39b92f075cb0d0d13a601a21765b8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1fb39b92f075cb0d0d13a601a21765b8.jpeg)

**Erik Scott**

---
---
**Oliver Seiler** *December 16, 2015 06:12*

I'm not an expert, but have been using Simplify3D for a while and am mostly happy with it.

Any reason you've got 3 skirt layers? 1 should be fine.

It looks like your second layer is shifted in relation to the first one - skipping steppers?


---
**Oliver Seiler** *December 16, 2015 06:19*

Here's my profile for printing PLA. I'm mainly printing other filament types, so it's not perfect, but works for me (and I've printed the benchy fine using this - so it should work well on the Eustathios). 

[https://drive.google.com/file/d/0B2br-OijkAgwOG8ybEwzUzB5V2c/view?usp=sharing](https://drive.google.com/file/d/0B2br-OijkAgwOG8ybEwzUzB5V2c/view?usp=sharing)


---
**Oliver Seiler** *December 16, 2015 06:20*

And here's the profile for Colorfabb XT, which I've been using more often than PLA recently.

[https://drive.google.com/file/d/0B2br-OijkAgwc1A0ZktxejlWcUE/view?usp=sharing](https://drive.google.com/file/d/0B2br-OijkAgwc1A0ZktxejlWcUE/view?usp=sharing)


---
**Erik Scott** *December 16, 2015 07:02*

Thanks **+Oliver Seiler**! I'll take a look at them tomorrow when I'm not so tired and some of my sanity has returned. 


---
**Eric Lien** *December 16, 2015 07:39*

PLA nozzle catches. I hate that. Any chance you need to tune down the extrusion a little? Also can you try it with turning off retraction on layer change and see what that does? Also try a little bit of coast to end under you retraction settings and you may be able to eliminate the blob causes your catches. Long Bowden tubes fight the hysteresis of the tension on the filament continuing to extrude. If you have gaps when using coast to end you can counteract it with extra restart distance (which you can use a positive or negative value if you see holes vs blobs).


---
**Erik Scott** *December 16, 2015 07:47*

**+Eric Lien** Far as I know, I'm extruding the right amount. I tied the coast at end setting a little while back, and it resulted in a destroyed pillar in the boat's cabin. 



A more general problem seems to me that I get much thicker extrusions when the printer slows down, so if one layer needs to print slower than the one before it, I get a telltale line in the print (see the second boat). Would this also be solved with a slight coast at the end?


---
**Miguel Sánchez** *December 16, 2015 09:07*

If extruding the right amount nozzle is a bit closer to the bed not all plastic can flow out of the nozzle easily and some pressure might build up,  to be released on layer raise. Does it make sense?


---
**Erik Scott** *December 16, 2015 18:45*

**+Nathan Walkner** I know why I get the layer shift. It's because of that blob, and the nozzle catches. Again, the reason I think it's Simplify3D is because this doesn't happen in Cura with the same settings. 



**+Miguel Sánchez** It's possible Cura and S3D have different ideas of how high off the bed the nozzle should be fr the first layer, or maybe S3D pushes more filament then Cura does on the first layer. I'll have a look at that. 


---
**Oliver Seiler** *December 16, 2015 18:51*

**+Erik Scott** you've configured 'first layer height' and width to 100%, so it should be the right layer height and filament amount.

You might try to disable retraction during layer shifts, because that will make it pause a little while before moving to the next layer and that might cause the blob. Definitively try coasting as that will release some of the pressure as well.


---
**Erik Scott** *December 16, 2015 21:03*

idk, that was a pretty big blob. I may upgrade my steppers to some larger ones. I'll lose 400step/rev, but I can always switch to 1/32 microstepping. 



**+Oliver Seiler** Your profile is working quite well. The print isn't done, but It looks much better so far. I think I was printing too hot, which contributed to those lines. I still got a bit of a blob in the first layer, but because it prints so fast, it don't think it had much time to develop. I may just have to lower the bed a tad. 



And man, you like to print fast. 


---
**Oliver Seiler** *December 16, 2015 21:16*

**+Erik Scott** great that it helps. As I said I'm not using this PLA profile much and I always go through the settings before prints, so I will reduce the print speed if I want better quality. Your movement speed was higher than mine ;)

I used to slow down the first layer speed to make sure it comes out fine, but often I get better results leaving it at a decent speed.


---
**Oliver Seiler** *December 16, 2015 21:18*

BTW has anyone tried using a direct drive on the Eustathios? It took me a very long time to get the bowden running more or less reliably. I love the simplicity of my old direct drive printer, although in general it's much worse that the Eustathios in many other aspects.


---
**Eric Lien** *December 16, 2015 22:14*

**+Oliver Seiler** I believe **+Walter Hsiao**​ has a carriage that can run in both direct drive and Bowden modes. He has it out on thingiverse.


---
**Erik Scott** *December 17, 2015 00:51*

Direct drive would be nice, but we'd need a more rigid gantry. 



Here's the new boat: [http://imgur.com/a/QyDe2](http://imgur.com/a/QyDe2)



I'm getting those lines around the columns and smokestack again, where the print head slows down to allow for cooling. 



I'm printing another boat now, this time with the extrusion multiplier at 0.97. I tried lowering the bed, but it wouldn't stick, so I think I can rule that out as a source of the blob in the first layer. 


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/AxNSmL6pAsT) &mdash; content and formatting may not be reliable*
