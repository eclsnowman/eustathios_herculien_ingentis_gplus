---
layout: post
title: "Does anyone have a lead on the closed loop belts needed for the Herculien?"
date: May 18, 2017 12:02
category: "Discussion"
author: James Ochs
---
Does anyone have a lead on the closed loop belts needed for the Herculien?  the only ones I can find are at sdp/si and are out of stock until late june :(





**James Ochs**

---
---
**Eric Lien** *May 18, 2017 12:12*

Robotdigg has them.


---
**Zane Baird** *May 18, 2017 12:17*

Which closed loop belts? For X and Y or for Z?


---
**Michaël Memeteau** *May 18, 2017 12:19*

Try [beltingonline.com - Beltingonline.com, Supplier of power transmission and conveying products](https://www.beltingonline.com)




---
**Eric Lien** *May 18, 2017 13:12*

Between Motors and Side Drive Rods: [http://www.robotdigg.com/product/282/228mm-or-232mm-length,-6mm-width,-Closed-loop,-GT2-belt#](http://www.robotdigg.com/product/282/228mm-or-232mm-length,-6mm-width,-Closed-loop,-GT2-belt#)



For Z Stage: [adafruit.com - Timing Belt GT2 Profile - 2mm pitch - 6mm wide 1164mm long](https://www.adafruit.com/product/1184#)




---
**Eric Lien** *May 18, 2017 13:13*

These are the links provided in the HercuLien BOM: [https://github.com/eclsnowman/HercuLien/blob/master/BOM/HercuLien_BOMV3.pdf](https://github.com/eclsnowman/HercuLien/blob/master/BOM/HercuLien_BOMV3.pdf)



If you click on the prices in the PDF most will take you to links for the products.




---
**James Ochs** *May 18, 2017 13:33*

doh, I was working off of the XLS and it doesn't have links for those items.  thanks for the leads ;)


---
**Eric Lien** *May 18, 2017 13:57*

The XLS does as well, that's what generated the PDF 😉


---
**James Ochs** *May 18, 2017 13:59*

ID10T Error..  Lookie, there's another tab ;)


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/aK3EzNHvz2B) &mdash; content and formatting may not be reliable*
