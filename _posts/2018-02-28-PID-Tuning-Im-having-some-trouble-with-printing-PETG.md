---
layout: post
title: "PID Tuning? I'm having some trouble with printing PETG"
date: February 28, 2018 18:05
category: "Discussion"
author: Brandon Cramer
---
PID Tuning?



I'm having some trouble with printing PETG. I will be printing and it will just stop extruding filament. It seems to be pushing but is grinding like it can't push filament, almost like it's not hot enough. I'm printing PETG at 260 degrees. 



Is .5 amps enough for my extruder stepper motor?



Do I need to do some PID tuning? What commands do I need to use on an Azteeg X5 Mini V3? 



I've tried M303 S230 C10 but nothing happens. 



Here is my current config. 



[https://www.dropbox.com/s/cqogvw01r7ppgs6/config.txt?dl=0](https://www.dropbox.com/s/cqogvw01r7ppgs6/config.txt?dl=0)











**Brandon Cramer**

---
---
**Brandon Cramer** *February 28, 2018 18:31*

I think I figured out the command:



M303 E0 S260




---
**Dennis P** *February 28, 2018 19:05*

you might want to add the number of cycles too. the Cx is the number of cycles above and below target  temp


---
**Zane Baird** *February 28, 2018 19:15*

Is your temp fluctuating during the print? If not, PID tuning will gain you nothing. I would try turning the motor current up a bit. I typically run the NEMA17 on my bondtech at around 0.8-1A. 



Do you have a cooling fan coming on right before the jam? Could be a drop in temp with the fan blowing on the heater block. I also try and limit cooling with PETg to as low as I can go and still achieve decent overhangs


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/1Vd8ETmz91G) &mdash; content and formatting may not be reliable*
