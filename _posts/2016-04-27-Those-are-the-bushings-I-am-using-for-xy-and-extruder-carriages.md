---
layout: post
title: "Those are the bushings I am using for x,y and extruder carriages"
date: April 27, 2016 12:10
category: "Build Logs"
author: Dimitrios Tzioutzias
---
Those are the bushings I am using for x,y and extruder carriages

![images/2084fb972031a8904fd542fbc8bbdc80.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2084fb972031a8904fd542fbc8bbdc80.jpeg)



**Dimitrios Tzioutzias**

---
---
**Eric Lien** *April 27, 2016 12:41*

Those will work, but are more sensitive to misalignment. So you might want to print Ninjaflex adapters like **+Walter Hsiao**​ to allow for misalignment tolerance. He did that on these: [http://www.thingiverse.com/thing:854301](http://www.thingiverse.com/thing:854301)


---
**Ted Huntington** *April 27, 2016 19:54*

Let us know how it works- because that is an interesting alternative to the self aligned bushings which can be tough find sources for.


---
**Øystein Krog** *April 28, 2016 05:28*

I found graphite-infused bushings to be quite dirty over time, they leave graphite on the rods and whenever you're working on the printer you end up with graphite on your fingers. There's also some concern about loose graphite and electronics..


---
*Imported from [Google+](https://plus.google.com/108355995361667474020/posts/WtA7qZcjFmQ) &mdash; content and formatting may not be reliable*
