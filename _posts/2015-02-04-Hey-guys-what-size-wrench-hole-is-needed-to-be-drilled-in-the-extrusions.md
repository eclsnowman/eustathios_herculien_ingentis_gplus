---
layout: post
title: "Hey guys, what size wrench hole is needed to be drilled in the extrusions?"
date: February 04, 2015 01:03
category: "Discussion"
author: Isaac Arciaga
---
Hey guys, what size wrench hole is needed to be drilled in the extrusions? I measured the hole in Misumi's cad model and its over 7mm! That seems a bit larger than the diameter of a m5 button head. I'm using OpenBuilds 2020 for this.





**Isaac Arciaga**

---
---
**Dat Chu** *February 04, 2015 01:13*

Whatever the m5 go through is my guess. 


---
**Isaac Arciaga** *February 04, 2015 01:29*

Yeah I know, it was silly. Was just making sure ;)


---
**Mutley3D** *February 04, 2015 01:32*

M5 == 5mm diameter thread, M4 == 4mm thread etc. This does not account for thread pitch but assume standard pitches unless otherwise designated


---
**Isaac Arciaga** *February 04, 2015 01:52*

Let me clarify. I'm talking about the holes that go straight through the extrusions to butt up the corners instead of having to use corner brackets. Anyhows, m5 it is.


---
**Mutley3D** *February 04, 2015 01:58*

The end hole size varies with different extrusion manufacturers, some are 5mm some 6mm, Im not sure on misumi. I use kjn extrusions.


---
**Dat Chu** *February 04, 2015 02:11*

When I used openbuilds extrusion, i drill using the next larger Imperial drill bit. Seems to work. 


---
**Eric Lien** *February 04, 2015 02:40*

The hole only needs to be large enough for the Allen wrench to pass through. The button head doesn't go all the way through the extrusion, only inside the track on the near side to the mating extrusion. The hole is so you can tighten it. I used a 5mm bit.


---
**Eric Lien** *February 04, 2015 02:44*

Like this: [http://imgur.com/xydHvbc](http://imgur.com/xydHvbc)


---
**James Rivera** *February 04, 2015 03:31*

Wow. A picture sure is worth 1000 words. I had been thinking the screw went through both extrusions, so I was a lot more worried about how precise the holes needed to be than is actually necessary. This seems easy enough. Now I might actually get around to doing it. :) Thanks!

EDIT: I feel like an idiot for not understanding this sooner.


---
**Eric Lien** *February 04, 2015 03:37*

**+James Rivera** I didn't understand until **+D Rob**​ made a video. Its around here somewhere.


---
**Isaac Arciaga** *February 04, 2015 03:48*

**+James Rivera** exactly why I decided to ask. I'm glad I did. Hopefully a future builder will find this helpful. Thank you **+Eric Lien** for the photo.


---
**Joe Spanier** *February 04, 2015 05:12*

So why did the build go this direction instead of the 3 way corner blocks?


---
**Mike Thornbury** *February 04, 2015 06:38*

Cheaper and (if you cut square) more accurate.


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/X3CJCwF2XZT) &mdash; content and formatting may not be reliable*
