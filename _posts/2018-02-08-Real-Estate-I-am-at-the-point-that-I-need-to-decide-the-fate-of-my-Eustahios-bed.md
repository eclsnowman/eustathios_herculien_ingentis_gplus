---
layout: post
title: "Real Estate! I am at the point that I need to decide the fate of my Eustahios bed...."
date: February 08, 2018 03:59
category: "Deviations from Norm"
author: Dennis P
---
Real Estate! I am at the point that I need to decide the fate of my Eustahios bed.... The heat spreader is oversize in the L-R direction of the photo. 

The heater that I got from **+Bruce Lunde** is laying on top unattached. (My frame members were offcuts that are 455 instead of 425mm so Iam 30mm bigger than stock... the platform rails are also long at 455 right now). So the platform is almost 18" long and 15 3/8+/- wide. The build area is almost as big as the footprint as the heater.  

I am tempted to leave it this size- I would have a margin all the way around the build area this way. 

Do I want to cut it down so it fits inside the frame at the top instead?

 

 



![images/2f9aa6f1c8219c55b8d796088e72dcc6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2f9aa6f1c8219c55b8d796088e72dcc6.jpeg)



**Dennis P**

---
---
**Bruce Lunde** *February 08, 2018 04:14*

Go big or go home right?


---
**Eric Lien** *February 08, 2018 04:21*

If it travels up far enough, and doesn't interfere anywhere... I say let-er-buck :)


---
**Eric Lien** *February 08, 2018 04:41*

BTW **+Dennis P** , you should show the folks your belt path adjustment on Z. 


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/31xBCnYYRbg) &mdash; content and formatting may not be reliable*
