---
layout: post
title: "That is an interesting nozzle. Might solve some problems with abrasive filaments Originally shared by Jonathan Hurst Ryan Carlyle thanks for all the input and the back ground knowledge you shared"
date: October 21, 2015 06:55
category: "Show and Tell"
author: Igor Kolesnik
---
That is an interesting nozzle. Might solve some problems with abrasive filaments



<b>Originally shared by Jonathan Hurst</b>



**+Ryan Carlyle** thanks for all the input and the back ground knowledge you shared.  Here is the section I am planning on using for the production version.



original post can be found here: [https://plus.google.com/110195651396255620474/posts/BhwW1om31ig](https://plus.google.com/110195651396255620474/posts/BhwW1om31ig)



![images/b1c8d482d2785108696521dc3609387a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b1c8d482d2785108696521dc3609387a.jpeg)
![images/ff53a4c0fab15aee544e4c5c054b775b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ff53a4c0fab15aee544e4c5c054b775b.jpeg)
![images/2d9097e2e58dd0b6b5d8e46fe081eed4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2d9097e2e58dd0b6b5d8e46fe081eed4.jpeg)

**Igor Kolesnik**

---
---
**Eric Lien** *October 21, 2015 13:47*

Looks like only the orifice is hard material. Isn't the back part of the nozzle just going to wear like any other nozzle. I understand the majority of the abrasion will happen at the impingement point of the orifice, but you would think other parts would wear too.



Very original idea though.


---
**Chris Brent** *October 21, 2015 15:38*

It's an industrial sapphire. Someone asked on the original thread about internal wear but no answer yet. $75 a nozzle too, unless he can get proper volume.


---
**Ryan Carlyle** *October 22, 2015 03:01*

If you look at the worn nozzle cross-sections E3D has posted, the erosion all happens at the nozzle tip (rubbing the print) and inside the orifice throat downstream of the taper (unexplained, but may be dry slip or die swell effects due to the abrupt flow velocity change.) No one has reported any abrasion upstream of the orifice as far as I'm aware. Which makes sense if you think about it. If you think in simple terms of average fluid velocities, the speed through the 2mm diameter melt pool is only 1/25th of the speed through the 0.4mm orifice. So at most you'd expect 4% as much wear. And when considering laminar vs turbulent flow and goofy polymer molecule alignment effects, the difference should be even bigger than that. 


---
**Igor Kolesnik** *October 22, 2015 03:08*

I'd assume that thick plastic with high friction on the wall will create a turbulent flow creating a kind of the wave bearing and will work like a lube. Plus abrasive effect should be much less noticeable on lower speeds. 


---
*Imported from [Google+](https://plus.google.com/+IgorKolesnik/posts/Q2Z1dcMteKG) &mdash; content and formatting may not be reliable*
