---
layout: post
title: "I have octopi running on my raspberry pi"
date: July 16, 2015 20:28
category: "Discussion"
author: Brandon Cramer
---
I have octopi running on my raspberry pi. Is there anything I need to do to the Azteeg X5 Mini before it will allow me to move the Z stepper motor? 



I disabled access control when I first booted up with octopi. I'm logged in as dummy right now. It wont allow me to add a user via the web interface. Not sure if this is my issue with moving the Z stepper motor but wanted to mention this just in case. 



What do you think about the wiring so far? Good or bad? Suggestions? 

![images/bb0e0ec73e8e5979081d8fdeb51298f4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bb0e0ec73e8e5979081d8fdeb51298f4.jpeg)



**Brandon Cramer**

---
---
**Daniel Fielding** *July 16, 2015 20:48*

Do you have your z end stop hooked up and have you clicked home? The controller needs to know where it is before it will move up or down.


---
**Brandon Cramer** *July 16, 2015 20:57*

I'm guessing I need to get this smoothieware loaded on the SD card before any of this will work right? I forgot that step. Any idea what the micro-stepping jumper settings should be?



I was afraid to hit home. I just wanted to see if it would move the Z motor. Do I need to have the z end stop hooked up before it will move? 


---
**Brandon Cramer** *July 16, 2015 21:02*

Ok. I guess I have my answer. It attempts to do something with the motor but the motor doesn't turn. 


---
**Jim Wilson** *July 16, 2015 21:23*

Often you HAVE to home before a controller will let you do any other movements.



Trust me, after many hotend crashes, its the right logic, lol.


---
**Brandon Cramer** *July 16, 2015 22:28*

Is the Viki 2.0 getting its config from the Azteeg X5 Mini? When I have it wired up, it just boots and shows an lcd screen with nothing on it. 



No luck with getting the stepper motors to turn. They try, but nothing happens??


---
**Brandon Cramer** *July 16, 2015 22:29*

Oh one more question... How do I wire up these end stops? Is this something I would have ordered when I ordered everything on the BOM? If so what pieces do I need? 


---
**Eric Lien** *July 16, 2015 22:59*

I went with the screw terminal azteeg, so yes you will need wiring with the proper ends to clip onto the board.


---
**Brandon Cramer** *July 16, 2015 23:13*

**+Eric Lien** Doh!! Wishing I had that version right about now. I will have to take a trip and see what I can find at the hardware store. Does the end stop just need the two wires? 


---
**Eric Lien** *July 16, 2015 23:37*

Yup. Any gauge will do since it is just a signal wire, it draws no real current.


---
**Brandon Cramer** *July 17, 2015 15:42*

I'm not having much luck with the Z motor. It acts like it wants to start the motor, but it doesn't turn it. I have a little jumper block shorting the end stop for x, y, and z. When I click home in Octopi, it acts like I don't have the end stop engaged with the jumper block. Where do I need the jumper block? I've tried GND and SIG, and also GND and V+. 



I'm using the Eustathios V1 Config file for my Azteeg x5 mini. Do I need to have any of these jumper blocks hooked up anywhere else on my Azteeg X5? 


---
**Brandon Cramer** *July 17, 2015 15:53*

Ok! I finally got the Z motor to turn. I had to set the micro-stepping jumper to 1/32. What is the normal setting for this?


---
**Eric Lien** *July 17, 2015 16:37*

You can run microstepping where ever you would like. For example I run lower microstepping on Z because the gear ratio is already so high and you should try to print at z heights that are multiples of full steps (not microsteps) anyway. Have you configured the smoothie firmware yet? That should be your first goal before trying to move things around so much. 


---
**Brandon Cramer** *July 17, 2015 16:41*

**+Eric Lien** So I should probably leave it at 1/32 than right? Configured smoothie firmware yet... Is that loading the config file on the sd card and booting it up? Do I need to update the firmware? If so how?



When I had the Viki LCD hooked up, it wasn't working. That is going to be the next thing I tackle I think. 


---
**Eric Lien** *July 17, 2015 16:49*

Smoothie consits of the firmware, and the config. You can update to the latest firmware, but your board did come with one loaded. What you need to do is configure the printer with travel speeds, steps/mm, confirm rotation/direction of steppers, configure thermistors...



Perhaps someone who build a V2 using an Azteeg X5 could share theirs. I have one I could share, but I am still on a hybrid Eustathios 1.5 :)



This should get you close: [https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Smoothieware/Eustathios%20V2%20Config%20File](https://github.com/eclsnowman/Eustathios-Spider-V2/tree/master/Smoothieware/Eustathios%20V2%20Config%20File)


---
**Brandon Cramer** *July 20, 2015 17:06*

This Azteeg X5 Mini board is a pain in my ass!! I wish it had the screw terminals instead of the pins. 



Does anyone know where I can get the following?



2 Meter stepper jumpers

2 Meter end stop jumpers



I don't know the technical name for the connector that plugs into the board. I could always make my own if I had the connectors and ordered some cable to go with it. 



Please help!


---
**Brandon Cramer** *July 20, 2015 18:08*

Alright, I'm done venting. [robotdigg.com](http://robotdigg.com) is going to make me the cables I need. :)


---
**Brandon Cramer** *July 20, 2015 21:07*

So I got all the endstops hooked up temporarily until I get the cables from [robotdigg.com](http://robotdigg.com). 



When I click home for Z, the print bed moves down instead of up to where home is and the endstop switch for Z. Is this a setting in the Azteeg config file?


---
**Eric Lien** *July 20, 2015 21:29*

Sorry Man, I will note this in the BOM. In my opinion screw terminal is the only way to go. 


---
**Brandon Cramer** *July 20, 2015 21:45*

**+Eric Lien** Oh well. It is what it is. At least I was able to get the cables made that I need. 



Any idea why the Z Home button is sending the print bed down instead of up?


---
**Eric Lien** *July 20, 2015 23:03*

Yes the motor needs to be reversed. It is done in the firmware, or by reversing the wiring.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/8R55raJmCpZ) &mdash; content and formatting may not be reliable*
