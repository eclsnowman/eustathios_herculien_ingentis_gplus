---
layout: post
title: "200mm/s no Z break in"
date: July 03, 2017 21:22
category: "Build Logs"
author: James Ochs
---
200mm/s no Z break in


**Video content missing for image https://lh3.googleusercontent.com/-61I26MM0Q5M/WVq1m_UfWQI/AAAAAAAAydg/1Ot61TZpjG8Oe4KIHvHJ_JYCy7vEFg4FQCJoC/s0/VID_20170703_172047.mp4**
![images/9c2e91fd9ff7ef74ac67c9a3cb087023.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9c2e91fd9ff7ef74ac67c9a3cb087023.jpeg)



**James Ochs**

---
---
**Eric Lien** *July 03, 2017 22:08*

Looking good. What is the clicking noise I hear when traveling away from the camera? Sounds like maybe the cabling catching, or a belt rubbing?


---
**James Ochs** *July 03, 2017 22:26*

Somehow I managed to miss the flat on my Y motor spindle and the toothed gear was slipping.  took me a while to find it because as soon as I would look for it it would stop doing it until I looked away 😬



After a while at 200mm/s it got loose enough that it was obvious... 



Reset the gear and went around and checked all the motors and grub screws on the drive train and it's all good now!



Should be getting to a print in a day or two...



Don't skip the break in step ;)


---
**James Ochs** *July 03, 2017 22:31*

Oh, and what are you using for homing?  Right now I'm at 40,40, and 10.  I had Z at 15, and it ran fine, just seemed scary fast!


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/YkUPU4fGjQA) &mdash; content and formatting may not be reliable*
