---
layout: post
title: "I'm just assembling the x/y-mechanics. Any tips and tricks to make it move smooth?"
date: December 25, 2014 17:53
category: "Discussion"
author: Florian Schütte
---
I'm just assembling the x/y-mechanics. Any tips and tricks to make it move smooth? There is too much tension. I nearly can't move in x/y direction.





**Florian Schütte**

---
---
**Florian Schütte** *December 25, 2014 17:54*

...No belts installed at the moment


---
**D Rob** *December 25, 2014 17:56*

It's harder to move without belts because nothing keeps the sides aligned. Hold both ends and move as close to same on each end as possible. Congrats on your build. Merry Christmas and good luck.


---
**Jason Smith (Birds Of Paradise FPV)** *December 25, 2014 17:58*

Installing the belts which will force the carriage rods to remain parallel with the outer rods will help it move much better. I loosened all the pulleys, and used some spacers to ensure the carriage rods were parallel with the outside frame before tightening the pulleys on the shafts.  Make sure to use some lubricant on the bushings. You will need several hours of break-in to get things running really smoothly though. 


---
**Eric Lien** *December 25, 2014 18:24*

**+Jason Smith** careful with lube. Only use a very very light oil if anything (sewing machine oil). Bushing are oil infused from the factory. The belts are the real key, and yes square the gantry before tightening the pulleys. I agree a break in is required. There are a few breaking gcodes floating around.


---
**Tim Rastall** *December 25, 2014 18:26*

Getting the bushings well aligned also helps.  Have a trick for that....


---
**Tim Rastall** *December 25, 2014 18:30*

[https://video-downloads.googleusercontent.com/AKxTqlz-DwMvoFiy8LsFubAkT7eCwDs-6FHzHbnM2Qqn1mqPM2VnpZ0ztl66Wnt5e3lWQ1HhmPZLlSrRD1F5EbdRMrbsuwh9506RYIO9FlAmbErr_bpjdWvoMM1R3aJ48TMiuTppdAu7UQocvqRvi3ST5-nGR-msiJ-FzdSPOp4XSytpr3q0Wazl9zu5ue4GET_d7txsLYB5JqxHdZwzuu9jCp5JTnMZRs3X8UiEOrXhPjjsBxyRU57r0IInS52p2ueOHEvyZF6L82GjndjJ7qVVy1TC7H8E6QvBcYc_ta4jYM5UA6MkYOrQauhloLP2H-dMg9KmwBk2b2-LwVJmnF5qAIhpcRfiNJonY92tOoveciodfe2ZJR1L9Qch1zsNCW8bor7S7010g-AuZwVjXZ01obcPjU3CzEamlu0JBs00rDZg7x7XcnRek-Tl7PyWg-u0tsMJuZN3nqNRN7nJ7tSSGyIpDqvyh-gII_iVcNOWnzMgzlhoPeu64zT8QaoVJhwinMsSx1Z50UWEXAkaxnHUpV8ojpSrYM8RV-BMhmQ2WYXtUdWuJQ_oC06H7zhyuUWh3htMsGK3Iq9gibBZUZbhxCD1ZqYtC0SS6Q3yBeNy60oWT4NEHMqqv_WSVQQrYvuEdeKoxHpkjXPjo2m4C97KSYeJTRT9vaGQNglu2UGq3ZApuMHwX38LrkUl89DfjI79TuK0ePigL-OIREUuN8mIs-ADAYglZfQZPAYG_YoMKoM3X4FvXNN7_VPko5liTjhgHV8FGBnPHnzsSmxyzj2PXsGegEJTDsQHG0o9tEP9t--5BwHyUWbFqMwmlbnjdki6mu-yODb9](https://plus.google.com/photos/116889746506579771100/albums/6096844228085536561?authkey=CPya4LyQ6-SZ5QE)


---
**Florian Schütte** *December 25, 2014 18:41*

Thank you, **+Tim Rastall** . I will try this now. But way to long video for this small trick ;)


---
**Tim Rastall** *December 25, 2014 18:45*

**+Florian Schütte** well,  I was vblogging progress as I went :p.  Feel free to make an abbreviated version. 


---
**James Rivera** *December 25, 2014 19:56*

I think using a stick or some other jig for measuring the distance could be useful for making sure the parts are equally spaced, which is important for parallelism and smooth movement. **+Eric Lien**​ did this?


---
**Eric Lien** *December 25, 2014 20:25*

**+James Rivera** yup. I printed a spacer that went between both side rods and the crossbar. Can't find my file though.


---
**James Rivera** *December 25, 2014 20:33*

Doh! Oh well. But the size of the jig is not as important as just having the spaces be the same, so even a simple dowel or block of wood could suffice once you've cut it to roughly the right length. Then use it to adjust your rod spacing.


---
**David Heddle** *December 25, 2014 20:40*

Heres one I have, [https://drive.google.com/a/heddle.net/file/d/0B0IDRYw_FT29LUh4OVBWZVo3Rlk/view?usp=sharing](https://drive.google.com/a/heddle.net/file/d/0B0IDRYw_FT29LUh4OVBWZVo3Rlk/view?usp=sharing)


---
**James Rivera** *December 25, 2014 20:59*

Not shared publicly.  :(


---
**David Heddle** *December 25, 2014 21:06*

Oops try again, thought I had done that.


---
**James Rivera** *December 25, 2014 21:10*

That's better--got it!


---
**Florian Schütte** *December 25, 2014 21:27*

**+Tim Rastall**​ : worked fine. I just reassembled everything and it moves reltively smooth. Now i will finetune with a spacer  like **+James Rivera**​ and **+Eric Lien**​ suggested. After that its time To test my selfmade endless belts.


---
**Alex Benyuk** *December 25, 2014 23:14*

nice one, cheers. I've created one for myself too [http://www.thingiverse.com/thing:610215](http://www.thingiverse.com/thing:610215) probably need to be a bit wider as yours


---
**Florian Schütte** *December 25, 2014 23:25*

Thanks **+David Heddle**  and **+Alex Benyuk** . Will make them tomorrow out of plywood


---
**SalahEddine Redjeb** *December 26, 2014 02:03*

interesting, good idea those spacers, ill be making two of wood


---
**SalahEddine Redjeb** *December 29, 2014 22:22*

So, i've got my belts installed and all today, while one of the rods moves relatively fine and smoothly (i have no idea how it should),  the other one is harder to the point i have to hold the structure with one hand and push the other to make it move, anybody have a comment on that?


---
**Eric Lien** *December 29, 2014 23:28*

**+SalahEddine Redjeb** are the motors hooked up?


---
**SalahEddine Redjeb** *December 30, 2014 08:40*

nope, they are not yet.


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/EcQqUg3e72J) &mdash; content and formatting may not be reliable*
