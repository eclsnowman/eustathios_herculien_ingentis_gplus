---
layout: post
title: "The build plate of Eustathios is relatively large, I'm thinking using a drag chain on XY plane to guide the electric cable for the extruder"
date: April 16, 2017 17:49
category: "Discussion"
author: larry huang
---
The build plate of Eustathios is relatively large, I'm thinking using a drag chain on XY plane to guide the electric cable for the extruder. I'm hoping doing that may help ease the tension on the extruder. If any one using or interested in a setup like that, please share some experience.



(Image from internet)

![images/c0afd17fb175248019b0d469f944710c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c0afd17fb175248019b0d469f944710c.jpeg)



**larry huang**

---


---
*Imported from [Google+](https://plus.google.com/104789216831460398100/posts/KcarufycrjQ) &mdash; content and formatting may not be reliable*
