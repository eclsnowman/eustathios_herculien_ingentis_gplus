---
layout: post
title: "Alright everyone has been asking about z solutions for the ingentis this was the solution I decided on several months ago"
date: March 10, 2014 02:39
category: "Show and Tell"
author: D Rob
---
Alright everyone has been asking about z solutions for the ingentis this was the solution I decided on several months ago. I printed it on my then not very well calibrated Mendel big12. Also the STK was 2x to big when I dl'd it from thing diverse and saw wouldn't recognize features. Do I made the hub and the holes 2x to big and in Cura printed ½ scale. I think Bette can be done and a reduction worm box someone posted is my next try enjoy

![images/c3ff8fe329e2684b6b9ddf6e8a8b680c.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c3ff8fe329e2684b6b9ddf6e8a8b680c.gif)



**D Rob**

---
---
**Wayne Friedt** *March 10, 2014 02:45*

Actually i plan to use 2 10mm Trapezoidal screws on mine/


---
**Ricardo de Sena** *March 10, 2014 03:22*

Good job.


---
**ThantiK** *March 10, 2014 03:28*

Largely thinking about doing the "same thing" except with a 5.18:1 planetary gear reduction - Should provide a nice non-printed solution, hold the bed on both sides to prevent cantilever forces, and be largely straightforward.


---
**ThantiK** *March 10, 2014 05:02*

And I just realized, nope.  Screw that idea.  Turns into 1036 steps/rotation - so 1/1036 turns into some really nasty repeating number   Would be near impossible to get nice round numbers for step/layer height.


---
**D Rob** *March 10, 2014 06:34*

What is a good radio to shoot for **+Anthony Morris**?


---
**ThantiK** *March 10, 2014 12:33*

Problem is, planetary gears naturally have a non-integer number.  As long as it's an integer it's fine.  If it was 5:1 (instead of 5.18:1) then it would be 1000 steps per rotation. and you'd have 0.001 step resolution.  Which would be perfect.


---
**Carlton Dodd** *March 10, 2014 12:41*

Just an aside here:


{% include youtubePlayer.html id=Bt9zSfinwFA %}
[http://youtu.be/Bt9zSfinwFA](http://youtu.be/Bt9zSfinwFA)


---
**Daniel Fielding** *March 10, 2014 13:17*

Video looked great on my phone filled up the whole screen :)


---
**D Rob** *March 11, 2014 06:01*

finally watched the link. ok firstly I'm sorry, secondly that was hilarious 


---
**Carlton Dodd** *March 11, 2014 13:34*

**+D Rob** LOL! No need to be sorry, just friendly ribbing. ;-)


---
**D Rob** *March 12, 2014 00:58*

**+Carlton Dodd** I know, but I really didn't even think about that﻿


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/Z9snHE6M9HH) &mdash; content and formatting may not be reliable*
