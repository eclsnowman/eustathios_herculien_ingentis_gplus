---
layout: post
title: "Walter Hsiao , Your work is always so good"
date: February 18, 2016 16:57
category: "Mods and User Customizations"
author: Eric Lien
---
**+Walter Hsiao**, Your work is always so good. Love the updated compact Eustathios carriage:



[http://www.thingiverse.com/thing:1322641](http://www.thingiverse.com/thing:1322641)





**Eric Lien**

---
---
**Walter Hsiao** *February 18, 2016 19:35*

Thanks Eric!  I still need to add it to github and send in a pull request.  I'll do that soon.


---
**Alex Lee** *February 18, 2016 19:39*

**+Walter Hsiao** is the man! I'm still working on your machine Walter, I haven't forgotten about you!


---
**Ben Delarre** *February 18, 2016 19:56*

Hmm I love the look of the flying extruder. Might have to give that a go.



I noticed you said you tried having an A-frame above the printer with the extruder mounted but still had bowden problems. I was thinking of trying a similar thing. How long was your bowden in this setup? 


---
**Walter Hsiao** *February 18, 2016 20:08*

Thanks Alex!  Can't wait to get my hands on one!


---
**Walter Hsiao** *February 18, 2016 20:19*

**+Ben Delarre** I think the tube was about a foot long.  Here's a couple of parts that may be useful if you have some extra extrusions for the A frame:



[http://www.thingiverse.com/thing:891686](http://www.thingiverse.com/thing:891686)

[http://www.thingiverse.com/thing:867153](http://www.thingiverse.com/thing:867153)

[http://www.thingiverse.com/thing:868402](http://www.thingiverse.com/thing:868402)



With the extruder mounted above, there were parts of the bed where the bend in the PTFE tube would swivel around 180°.  I didn't notice any issues from that, but I'm not sure how much testing of large prints I did back then.  I also didn't notice any obvious artifacts from the changing radius of the bend in the tube with that setup, though I never tested explicitly for it.


---
**Ben Delarre** *February 18, 2016 20:28*

Thanks **+Walter Hsiao** I was thinking of just using two pieces to make a right angle coming out the top, I have 3 metal right angle supports and two pieces of extrusion that would put the extruder about a foot above the gantry. I'll probably give this a shot this weekend as I think its probably a step in the right direction over the massive bowden tube I have now. Though I guess cable management for the hot end might be more tricky than with an a-frame?


---
**Jo Miller** *February 18, 2016 20:40*

I like the fans placed on top !, would this thingifile  also work on my single Hotend Herculien  ? (but keeping my 650 mm bowden-tupe)


---
**Walter Hsiao** *February 18, 2016 21:58*

**+Ben Delarre**  For the hotend wiring, I originally installed it with a cable chain, separate from the bowden tube, and left it like that even though the flying extruder arm would be a perfect place to run the hotend wiring (too lazy to redo the wiring).



**+Jo Miller** This carriage should fit on the Herculien (I think they use the same cross rod spacing and the 8 and 10mm self aligning bushings mount in the same holes).  The nozzle is significantly higher though, so I don't know if the bed will reach the nozzle on the Herculien without other changes. I don't think it reaches with the normal Eustathios Spider v2 build.


---
**Jo Miller** *February 19, 2016 20:34*

thanks Walter,  I will give it a try in solidworks first,


---
**Bud Hammerton** *February 20, 2016 03:14*

**+Walter Hsiao** I am so glad you also post STEP files, I was able to model a spacer that went under your duct for the E3D Volcano. It was a perfect fit.


---
**Eric Lien** *February 20, 2016 03:30*

**+Bud Hammerton** Please share :)


---
**Bud Hammerton** *February 20, 2016 14:42*

Took me a bit to figure out how to make the pull request, but it is there now. It is just the spacer and nothing else. included multiple 3D model formats as well as the STL.



Just as an FYI, I did not use this myself, I actually combined it into one piece with the duct section and printed it as a single piece.


---
**Eric Lien** *February 20, 2016 14:59*

**+Bud Hammerton** thanks, I love that people are using the github for archiving these mods and collaborations. I think it will help long term so all these great additions don't get lost in the G+ streams.


---
**Walter Hsiao** *February 20, 2016 18:53*

Thanks Bud!  Good to hear the step files were useful.  I'll have to try out the spacer, I was planning on making a volcano duct, but have never gotten around to it.


---
**Bud Hammerton** *February 20, 2016 18:55*

Spacer adds 11 mm depth per E3D's specs. You can always scale in the Z axis of needed. 


---
**Bud Hammerton** *February 24, 2016 18:56*

[http://www.thingiverse.com/make:198640](http://www.thingiverse.com/make:198640)

Made one. Had to order some magnets as I only had 10 mm round on hand.


---
**Walter Hsiao** *February 24, 2016 19:28*

Cool! Looks great, hope it works well for you.  Thanks for sharing the picture


---
**Maxime Favre** *February 29, 2016 06:42*

I'd like to give a try to the carriage but can't print ABS (no heatbed).

I managed to print the duct in tglase but the block is another story.

Assuming PLA won't hold the heat of the heatbed and the extruder. any recommandation ? XT ?


---
**Walter Hsiao** *February 29, 2016 10:37*

I don't have much experience printing without a heated bed, but some brands of PETG seems to warp more than others for me.  I haven't used XT, and haven't had problems with tglase warping, but I've seen it happen on a friends printer.  For the block, I've printed it in PETG from Atomic, 3D Supply Source, and Sainsmart, but always with a heated bed.



Plastics with fillers usually have lower shrink rates than the unfilled versions so maybe Colorfabb CF-XT or 3DXTech Glass Fiber PETG could work?  I don't know.  Another option is to just print it in PLA, it will probably work for weeks, maybe months, long enough to print a block on the new printer.  If you do that, you'll probably want to make sure the bushings aren't too tight since you'll need get them back out.


---
**Maxime Favre** *February 29, 2016 10:53*

I will do it that way, the block should hold a few prints, the time to upgrade the concerned parts. Thanks for your feedback


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/32wG2zR2NW4) &mdash; content and formatting may not be reliable*
