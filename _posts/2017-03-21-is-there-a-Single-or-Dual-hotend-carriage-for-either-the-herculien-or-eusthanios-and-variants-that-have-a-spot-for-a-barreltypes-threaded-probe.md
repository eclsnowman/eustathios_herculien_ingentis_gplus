---
layout: post
title: "is there a Single or Dual hotend carriage for either the herculien or eusthanios and variants that have a spot for a barreltypes/threaded probe?"
date: March 21, 2017 00:10
category: "Discussion"
author: Jim Stone
---
is there a Single or Dual hotend carriage for either the herculien or eusthanios and variants that have a spot for a barreltypes/threaded probe? specifically this or similar size 



i have looked and cant find one. 

**+Eric Lien** **+dr train**

tagged incase some are found so they can be archived.﻿

![images/d321f37e453842a796fd56d20100eab1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d321f37e453842a796fd56d20100eab1.jpeg)



**Jim Stone**

---
---
**Eric Lien** *March 21, 2017 01:22*

**+dr train**​ you have that new Carriage with the probe you would be willing to share?


---
**Eric Lien** *March 21, 2017 01:24*

Otherwise I know **+Markus Granberg**​ has his direct drive version.


---
**Jim Stone** *March 21, 2017 02:41*

long as they fit the bushings used in the herc BOM i aint picky :P




---
**Ted Huntington** *March 21, 2017 03:54*

you can always get blender and add one to the .stl, it's great to learn the basics of modeling and printing 3D objects.


---
**Jim Stone** *March 21, 2017 03:55*

I have been trying to learn solid works and others.



Not at that level quite yet


---
**Markus Granberg** *March 21, 2017 07:35*

[drive.google.com - MG carrage rev 1.STEP - Google Drive](https://drive.google.com/open?id=0B7kgdYk7GVumcFFOMV9SZlRoeGs)

Thats my carriage. Its for igus bearings tho..




---
**Jim Stone** *March 21, 2017 08:25*

they look pretty close to the 7z41mpsb10m [sdp-si.com - Sintered Bronze Pressbearings](https://www.sdp-si.com/D795/HTML/79501077.html) i use. is it just a different material these igus are made of?






---
**Markus Granberg** *March 21, 2017 08:31*

Yes the igus bearings is made of their plastic blend. Super light and easy to get hold of where I live.




---
**Jim Stone** *March 21, 2017 08:35*

ok. but the external dimensions should be the same no?



Do you have the STL you printed from? i cant navigate the STEP File effectively. :(




---
**Markus Granberg** *March 21, 2017 08:47*

[photos.google.com - Nytt foto av Markus Granberg](https://goo.gl/photos/smjabHNRQ1c47rrp6)

talking about the devil, my order just showed up..

Sure I save a stl file for you, but i dont think your bearings will fit without modification, but its close!


---
**Markus Granberg** *March 21, 2017 08:49*

[https://drive.google.com/file/d/0B7kgdYk7GVumZmQ5LTE0MlJSU1U/view?usp=sharing](https://drive.google.com/file/d/0B7kgdYk7GVumZmQ5LTE0MlJSU1U/view?usp=sharing)

[https://drive.google.com/file/d/0B7kgdYk7GVumOVVuclFNTjJMYTQ/view?usp=sharing](https://drive.google.com/file/d/0B7kgdYk7GVumOVVuclFNTjJMYTQ/view?usp=sharing)


---
**Markus Granberg** *March 21, 2017 08:49*

[drive.google.com - Carriage_V4_Molex_Microfit_3 rev4.STL - Google Drive](https://drive.google.com/open?id=0B7kgdYk7GVumZmQ5LTE0MlJSU1U)




---
**Jim Stone** *March 21, 2017 08:51*

oh wow. those are alot thinner than i thought they would be. hmm...guess its back to the drawing board :(



thanks for trying **+Markus Granberg** i do appreciate it




---
**Zane Baird** *March 21, 2017 12:47*

**+Jim Stone** I have a carriage that includes a location for a probe, but it is for different bearings. I recently modified the carriage to fit the bushings in the BOM, but also removed the probe point. I'll look at it and see if I can easily include the probe again. 


---
**Jim Stone** *March 21, 2017 21:14*

thank you **+Zane Baird** !!! :D




---
**Zane Baird** *March 22, 2017 01:45*

**+Jim Stone** I don't have time to make the modification necessary at the moment, but I have a working version that doesn't include a spot for a probe. I plan on making a commit to the github after MRRF that will include solidworks and .stl files for different derivations I've made




---
**Jim Stone** *March 22, 2017 01:48*

Ok. Maybe with normal solid works files I could figure something out. Or fail horribly haha. Either way. I can guarantee it will be chunky and ugly...and probably a cube with a hole that is 2 piece n tightens lololol


---
**Jim Stone** *April 02, 2017 09:45*

welp this sucks. the computer i design on psu died and took everything with it. and the laptop i run the printers and flash firmware with isnt powerful enough to 3d model so i cant do my bad designs even if i got my paws on something to muck up :(



**+Zane Baird** whats the status on your end?


---
**Markus Granberg** *April 03, 2017 15:35*

Try onshape it runs in your browser or phone/tablet. And it's free


---
**Zane Baird** *April 03, 2017 16:50*

**+Jim Stone** I haven't modded the version that uses BOM bushings for an inductive probe yet. I will try and get this done in the next night or to and push the changes to the github




---
**Jim Stone** *April 03, 2017 17:28*

**+Zane Baird** thank you so much. 


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/f1ysoijJm4F) &mdash; content and formatting may not be reliable*
