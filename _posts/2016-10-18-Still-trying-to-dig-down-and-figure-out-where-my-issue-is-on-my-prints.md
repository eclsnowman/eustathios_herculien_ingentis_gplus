---
layout: post
title: "Still trying to dig down and figure out where my issue is on my prints"
date: October 18, 2016 03:16
category: "Discussion"
author: jerryflyguy
---
Still trying to dig down and figure out where my issue is on my prints. I've now got a 120mm fan cooling the steppers(can put my fingers on the heat sinks indefinitely w/out any issue)



I tried printing a new Benchy w/ the same code as I've used in the past (which has given ok prints). Pictures show the results. So I wondered if the issue was a corrupted SD card. Reformatted the card and printed a second Benchy. 



In the pictures the far right Benchy was the 'good' one, middle was my first test today and the left one is after reformatting. The middle shows the worst defects but I'm. It convinced the reformat was what made the last one better?



Wondering if maybe I have fried my Y axis motor? And it has a segment of its steps which aren't any good but then it 'picks up' position as it comes out of that 'region' of the motors coils?



![images/9f2a1c487903296ef204887f550262c2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9f2a1c487903296ef204887f550262c2.jpeg)
![images/295f16ca4b5f5da8e37f5005e3af5683.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/295f16ca4b5f5da8e37f5005e3af5683.jpeg)
![images/be3a8fd0b0df6dc57b51fcce60f23014.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/be3a8fd0b0df6dc57b51fcce60f23014.jpeg)

**jerryflyguy**

---
---
**Maxime Favre** *October 18, 2016 20:31*

Maybe lower current on the steppers. You shouldn't have to cool them actively. I'm at 1.2A with BOM's motors, no problems. If already at a value like that, maybe belts tension is too high. I can move mines up and down about 1cm in the middle (the 4 long belts).

Check all your pulley screws too, even if they look tightened! Some have have play during quick change of direction.


---
**jerryflyguy** *October 18, 2016 20:49*

**+Maxime Favre** thanks, it's possible that belts are too tight.. I jacked them up as much as I could. I've noticed textures on my parts that seem to match the belt pitch. Wondered if too tight was a possibility.



My current is 1.3a right now, I've never noticed the actual motors getting hot, just the drivers.


---
**jerryflyguy** *October 19, 2016 03:13*

**+Maxime Favre** I checked my belts tonight, conscious of actual deflection in them and it'd be approx 1cm (or so) of course this is terribly subjective as far as how much force one is applying to get the deflection. 


---
**Maxime Favre** *October 19, 2016 05:06*

I know, I tension them by feeling so I tring to explain that in words... ;) I mean reasonable force, put a finger on it until you feel resistance. When I read "I jacked them up as much as I could", I think you should loose them. Belt didn't need a lot of tension to perform correctly, I learned that while building my eustathios.


---
**Mike Miller** *October 19, 2016 12:54*

A well adjusted system takes very little force to move the carriage. Any drag means the belts need to be tighter means the motors have to work harder means the drivers get hotter means you have to actively cool them. @ingentilire's motors are warm to the touch and there's no active cooling anywhere other than the hot-end. The belts aren't very tight, and the voltages are low. 



You might run some break in code the way things are, then loosen everything up, recheck alignment, and snug everything up. You may just have something a little bit out of true. 


---
**jerryflyguy** *October 19, 2016 16:04*

**+Mike Miller** thanks, I've actually got to disassemble most of the motion system to swap a couple printed parts (parts getting switched for cosmetic reasons) so will go through the whole re-rig and will do the break-in again at that point.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/a4dBF2RqLsF) &mdash; content and formatting may not be reliable*
