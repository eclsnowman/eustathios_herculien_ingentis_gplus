---
layout: post
title: "So... has anyone incorporated an inductive sensor into the carriage for this family of printers?"
date: April 22, 2017 15:23
category: "Discussion"
author: Mike Miller
---
So... has anyone incorporated an inductive sensor into the carriage for this family of printers? I have an aborted rebuild of my large printer and really need to tear it down and take care of those issues, and while doing so, incorporating mesh bed levelling (based on my Prusa MK2 experiences) will be a necessity. 









**Mike Miller**

---
---
**Benjamin Liedblad** *April 22, 2017 15:52*

I bought a sensor in anticipation of needing one, but didn't have a carriage design for it. I figured I would design one and print it as soon as the printer was functional.



So far, I haven't needed one - so my design effort didn't even start. 



With a little patience on the initial setup, I was able to level the bed such that with a 0.4mm nozzle I can get 0.1mm first layers on glass for parts spanning 180mm (PETG with hairspray). For ABS can get 0.2mm first layers with gluestick.



The machine isn't under heavy use, just hobby stuff evenings and weekends, but I have not leveled the bed in over a month.



While adding a sensor would be nice, I've read a few posts here stating that some of the bed-leveling sensors only give 0.2mm repeatability...



Unless someone can attest to better than that, I'll probably just stick with manual leveling - only had to do it once so far. 




---
**Mike Miller** *April 22, 2017 16:36*

They're good for overcoming issues in a printer design.  I have issues in my printer design. :D


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/QUgvxPJcQJd) &mdash; content and formatting may not be reliable*
