---
layout: post
title: "Also started to build an Eustathios, extrusions, motors, pulleys arrived"
date: April 15, 2015 09:57
category: "Show and Tell"
author: Daniel F
---
Also started to build an Eustathios, extrusions, motors, pulleys arrived. Printed the first parts.



![images/4d3cf07a8256e98e3b2707cdfad3f2ab.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4d3cf07a8256e98e3b2707cdfad3f2ab.jpeg)
![images/a36ec337f0e938e11938e843130f19ec.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a36ec337f0e938e11938e843130f19ec.jpeg)

**Daniel F**

---
---
**Eric Lien** *April 15, 2015 13:16*

I look forward to seeing your progress. Are you building V1 or V2?


---
**Daniel F** *April 15, 2015 20:44*

V2 with the option for V1 if enclosing the frame should be too difficult with the steppers on the side (bought some extra 32 teeth pulleys from robotdigg to cut the collar). No belts from sdp-si so far...

Z will be slightly different from V1/V2, I plan to use dia 10mm, 2mm pitch ACME threaded rod and bearings with 10mm inner diameter with no fancy machining on the ends, just cut to length (works well on my QR).


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/41XqzJaWPsR) &mdash; content and formatting may not be reliable*
