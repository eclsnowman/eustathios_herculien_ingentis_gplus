---
layout: post
title: "Eric Lien you sure did design a lovely part here"
date: January 24, 2015 02:13
category: "Show and Tell"
author: Jim Wilson
---
**+Eric Lien**​ you sure did design a lovely part here. 

![images/29040f62fc7e499894518b7560b57ed4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/29040f62fc7e499894518b7560b57ed4.jpeg)



**Jim Wilson**

---
---
**Dat Chu** *January 24, 2015 02:17*

Whoa, that is quite something. So is this replacing the dual extruder already in the Herculien?


---
**Eric Lien** *January 24, 2015 02:33*

**+Dat Chu**​ right now it is for the Eustathios. But if I changed the hole between the bushings to something over 10mm it would work fine for HercuLien too. I am torn. I like the flexibility dual heads provide. But to be honest I Never utilize it, so single extruder and lower weight would likely be a smarter move for me.


---
**Eric Lien** *January 24, 2015 02:39*

**+Jim Wilson**​ BTW great looking print quality. Nice job.


---
**Jason Smith (Birds Of Paradise FPV)** *January 24, 2015 03:56*

That is awesome. I'm sold. Starting the print tomorrow. 


---
**Eric Lien** *January 24, 2015 04:37*

**+Jason Smith** I was thinking about modifying it for cable strain relief zip tie spots. Give me a day of so to play around.


---
**Jim Wilson** *January 24, 2015 06:52*

Oh no! Hes redesigning it! If I only I had some sort of machine that could produce multiple iterations of a prototype in mere hours at practically no cost in materia-MUAHAHAHAHAHAHA!


---
**Eric Lien** *January 24, 2015 07:30*

I want to find a small strong clip to make the power modular. On my last version I had used an 8 pin computer connector, but it is bulky. Are there any electronics wiz type guys out there that could suggest a good locking connector with enough current for the heater element that might be smaller?﻿


---
**Jim Wilson** *January 24, 2015 07:37*

Was going to suggest the simple 4pin connector that regular motherboard PSUs use, but then you still need an additional pair for the fans, more if you want to have controllable cooling. 



However I'm far from an expert or even a simpleton, my wiring is all just using 6 of these neolithic relics clustered together and insulated with electrical tape: [http://i.imgur.com/11JnD3S.jpg](http://i.imgur.com/11JnD3S.jpg)


---
**Joe Spanier** *January 24, 2015 22:48*

**+Eric Lien**​ check out the connectors on the lulzbot. They are listed in the bom with the mouser link. 


---
**Eric Lien** *January 24, 2015 23:19*

**+Joe Spanier** will do. I also found cad models for the molex 8pin mini-3 connectors that lock. They seem to be a good fit too.


---
**Erik Scott** *January 31, 2015 04:40*

Guess I'm a little late to the party, but was this printed in PLA or ABS? At the moment, I can only do PLA, but I'm really liking this and I might be willing to give up my second hotend that I never use for this. So, would PLA be up to the task? I'm worried about that duct around the heatsink.


---
**Jim Wilson** *January 31, 2015 05:42*

This is in pla. Haven't tested this particular model yet, but my whole carriage and ducting on my i2 is pla, with a fan right on the heatsink of the e3d v6 it doesn't radiate any noticeable heat.


---
*Imported from [Google+](https://plus.google.com/101778058628996936791/posts/9xFUogMY88f) &mdash; content and formatting may not be reliable*
