---
layout: post
title: "I went ahead and forked the HercuLien repo to start working on docs"
date: January 17, 2015 23:36
category: "Discussion"
author: James Ochs
---
I went ahead and forked the HercuLien repo to start working on docs.  I've added a tab to the BOM workbook that lists each part number and the quantity of that item needed. I organized it by type of part and aggregated the vslot components to make it easier to figure out what to order.  Are you interested in pull requests for these updates?





**James Ochs**

---
---
**James Rivera** *January 17, 2015 23:57*

**+Eric Lien** 


---
**Eric Lien** *January 18, 2015 00:12*

Sure am. Now I need to learn how to do that :)


---
**James Ochs** *January 18, 2015 00:18*

Ok, I created the pull request... I learned everything I know about it from here:  [https://help.github.com/articles/using-pull-requests/](https://help.github.com/articles/using-pull-requests/)


---
**Seth Messer** *January 18, 2015 03:28*

Lemme know if you guys need git help. I use it everyday at work, would be happy to assist in any of the repo stuffs.


---
**Eric Lien** *January 18, 2015 03:51*

**+Seth Messer** thanks. I think I can get the pull stuff figured out. But once it's time to figure out how to do the markup language on the wiki part I know who to call :)


---
**James Ochs** *January 19, 2015 15:24*

 I noticed that the acrylic sides of the top cover appear to be about 12 inches tall.  In the BOM spreadsheet it looks like you've spec'd 24 x 24 for each of those panels. Will 12 x 24 panels work there or do they come out to over 12" tall?


---
**Eric Lien** *January 19, 2015 15:55*

**+James Ochs** I am at work so I cannot confirm, but the overall length of the top hat is 12", so two side panel should be possible from one 24" x24" because the extrusion takes up space on the ends and top/bottom.


---
**James Ochs** *January 19, 2015 15:57*

**+Eric Lien** Ok, I'll update the BOM sheet to reflect that.


---
**James Ochs** *January 19, 2015 16:04*

**+Eric Lien** Nevermind... I just looked on amazon and the 24x24 sheet is actually cheaper than the 12x24 :P  I'll just adjust quantities and add a note on my tab


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/6oSdrWvYuMA) &mdash; content and formatting may not be reliable*
