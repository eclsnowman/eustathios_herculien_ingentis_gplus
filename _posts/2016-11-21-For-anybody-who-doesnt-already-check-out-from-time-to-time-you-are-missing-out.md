---
layout: post
title: "For anybody who doesn't already check out from time to time you are missing out"
date: November 21, 2016 05:14
category: "Discussion"
author: Eric Lien
---
For anybody who doesn't already check out [thrinter.com](http://thrinter.com) from time to time you are missing out. It is an amazing blog by **+Walter Hsiao**. I just noticed he posted an overview of all of his great upgrades to the Eustathios. Check it out, and the rest of his posts. He has a great series on filaments, and also smoothieware tweaks that are certainly worth reading through.





**Eric Lien**

---
---
**Eric Lien** *November 21, 2016 06:18*

**+Mark Ellison** I think he used a Hatchbox transparent ABS. Printed at 100% infill.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/bDLiWiDVoqW) &mdash; content and formatting may not be reliable*
