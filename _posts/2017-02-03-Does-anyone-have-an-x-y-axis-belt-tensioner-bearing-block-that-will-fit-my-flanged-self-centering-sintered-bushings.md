---
layout: post
title: "Does anyone have an x/y axis belt tensioner bearing block that will fit my flanged self centering sintered bushings"
date: February 03, 2017 03:04
category: "Discussion"
author: Mike Smith
---
Does anyone have an x/y axis belt tensioner bearing block that will fit my flanged self centering sintered bushings. They are for 10mm rod and have an OD of 16mm.





**Mike Smith**

---
---
**Eric Lien** *February 05, 2017 16:55*

If you provide all the dimensions we can get one modified for you.


---
**Mike Smith** *February 05, 2017 18:41*

Thanks **+Eric Lien** I was just seeing if there was already something out there that was already made up. I starting designing my own using Fusion 360. Wanted to get use to the software anyway. I use Inventor at work and have used Solidworks for the past 20 years. This will also give me a chance to see what measurements I have to design to that will print the measurements that I need for press fit bearings and such.


---
*Imported from [Google+](https://plus.google.com/+MikeSmith/posts/EVtdgYDrEvD) &mdash; content and formatting may not be reliable*
