---
layout: post
title: "Here's the final-ish version of the clamped V4 carriage"
date: June 08, 2015 02:01
category: "Discussion"
author: Erik Scott
---
Here's the final-ish version of the clamped V4 carriage. Heatsink is super rigid now, and the screws pulled everything right together; no filing, sanding, or chiseling required. 



It is mostly backwards compatible with Eric Lien's cooling duct, but I couldn't quite match the angle when I re-created everything. So, I CADed up a new one and I have that going on the printer now. What you see in the photos is an old one I printed a few weeks ago. As you can see, it fits, but it's not perfect. 



Thoughts? Opinions? Requests?



![images/b8688d350e2e9fcae036557da240263c.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b8688d350e2e9fcae036557da240263c.png)
![images/5b0ce8da96e75564c02ef6c059afc1c5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5b0ce8da96e75564c02ef6c059afc1c5.jpeg)
![images/090d8f29baca0a4207be694d756b740d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/090d8f29baca0a4207be694d756b740d.png)
![images/eb60b48819d22217a603f5a8c4c5e63a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eb60b48819d22217a603f5a8c4c5e63a.jpeg)
![images/f523f06951bcd255b813cf2b0e521550.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f523f06951bcd255b813cf2b0e521550.png)
![images/593079297f15f1c0e32cec10feb1cd0e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/593079297f15f1c0e32cec10feb1cd0e.png)
![images/bdf216ebb7ffeb3d504cb2a7d6408ba1.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bdf216ebb7ffeb3d504cb2a7d6408ba1.png)
![images/991e41b7a8458dcd17fc8b9fffdb4aba.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/991e41b7a8458dcd17fc8b9fffdb4aba.png)
![images/7000b5828ffaf8c99e799a7fa8f42716.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7000b5828ffaf8c99e799a7fa8f42716.png)
![images/805e33fd276944535b5c721b243619d8.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/805e33fd276944535b5c721b243619d8.png)
![images/cb6a4ff8ee3260fca6721755643f6867.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cb6a4ff8ee3260fca6721755643f6867.jpeg)
![images/cb2e891b7e82d78ab58db38994bdfd5e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/cb2e891b7e82d78ab58db38994bdfd5e.jpeg)

**Erik Scott**

---
---
**Godwin Bangera** *June 08, 2015 12:32*

very nice


---
**Frank “Helmi” Helmschrott** *June 10, 2015 18:38*

very nice design. Will you release it? including sources? 


---
**Erik Scott** *June 10, 2015 18:50*

It's public on Onshape, so you can check it out there. 


---
**Frank “Helmi” Helmschrott** *June 10, 2015 18:54*

ahh great. do you have a link to that document there? I can't seem to find a way to discover it otherwise.


---
**Erik Scott** *June 11, 2015 02:32*

You can go to public documents and search Eustathios. You can also try this link: [https://cad.onshape.com/documents/a45f6c99e2154f3b94955175/w/443cf4d7046e41d1b21d5426](https://cad.onshape.com/documents/a45f6c99e2154f3b94955175/w/443cf4d7046e41d1b21d5426)


---
**Frank “Helmi” Helmschrott** *June 11, 2015 04:47*

Looks like this is for logged in users only while registration isn't free or open, right?


---
**Frank “Helmi” Helmschrott** *June 11, 2015 05:10*

never mind - they say you can only request an invite but they instantly send you one then. got it managed to get to this workspace.


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/M8M5RyTRj6b) &mdash; content and formatting may not be reliable*
