---
layout: post
title: "My Eustathios Spider V2 can't seem to get up to temperature unless I put my finger on the hotend fan and stop it from blowing air"
date: November 25, 2015 19:00
category: "Discussion"
author: Brandon Cramer
---
My Eustathios Spider V2 can't seem to get up to temperature unless I put my finger on the hotend fan and stop it from blowing air. 



I'm not sure what the issue is. Do I need to adjust that fan speed? I wouldn't think so, but I could be wrong. The other fan for cooling the PLA isn't spinning until after the first layer. 









**Brandon Cramer**

---
---
**Eric Lien** *November 25, 2015 19:15*

Has it always need like this? If not your heater cartridge may be failing... Or your thermistor is not making good contact and displays a lower than accurate temp.


---
**Eric Lien** *November 25, 2015 19:15*

Also have you ever PWM tuned your hotend?


---
**Brandon Cramer** *November 25, 2015 19:23*

It just started this about a week ago. It was having trouble getting up to 240, so I powered it off last Friday. 



Remind me how to PWM tune the hotend. If I'm asking, I think you know the answer. lol


---
**Eric Lien** *November 25, 2015 19:52*

You have a azteeg X5 correct?


---
**Brandon Cramer** *November 25, 2015 19:55*

Yes. X5 mini. 


---
**Brandon Cramer** *November 25, 2015 20:14*

Do I need to issue this M303 S240 C5 command and wait? If so, how long should I need to wait for it to cycle 5 times? Seems to be taking forever and not really doing much. 




---
**Brandon Cramer** *November 25, 2015 21:11*

I swapped out the hotend. Seems to have fixed the issue of getting up to temp. Not sure about the PWM yet. 




---
**Zane Baird** *November 25, 2015 21:23*

**+Eric Lien** when you tune PID in smoothieware, do you wait for it to be up to temp before issuing the PID autotune? I had great success performing it this way, rather than issuing the autotune command from ambient temps. The method smoothieware uses to autotune PID is meant to oscillate around a stable temp from my understanding...


---
**Eric Lien** *November 25, 2015 22:53*

I always tune from cold, because I thought that it tunes oscillating around a setpoint as well as tuning the approach and overshoot. But I may be mistaken.


---
**Zane Baird** *November 25, 2015 23:05*

**+Eric Lien** From my reading ([http://brettbeauregard.com/blog/2012/01/arduino-pid-autotune-library/](http://brettbeauregard.com/blog/2012/01/arduino-pid-autotune-library/)) it's suggested that PID autotuning be done at steady-state. Overshoot should still be accounted for by this method. In fact, PID control should really only turn down PWM duty cycle when it's close to temp anyways, so oscillation around a set value accounts for this if starting from steady-state. I could be entirely wrong here, but that's my understanding from reading up on it a bit. Then again, I'm no process control engineer...


---
**Bud Hammerton** *November 26, 2015 14:16*

**+Zane Baird** Ambient temp is a steady state. You really don't need to bring it up to temp before doing any auto PID tuning. That is one of the beauties of using automatic tuning. Besides even in the article you linked to the final determination is only the last three iterations with the results within 5% in his particular library. Preheating may lower the amount iterations but not the result. Same is likely true for other implementations of auto tuning PID (Smoothie/Marlin).


---
**Brandon Cramer** *March 24, 2016 17:33*

How can I need a new heating element after 17 weeks? I need to add a quick attachment if I have to replace the heating element every time I turn around. 



Which one should I purchase this time? Maybe there is a better one than the one I'm using today. 


---
**Zane Baird** *March 24, 2016 17:50*

The 40W noodle wire cartridge heater from RobotDigg heats better than any other cartridge I've used (no idea why). However, unless you are going to order a bunch of stuff it really isn't worth it.


---
**Brandon Cramer** *March 24, 2016 18:23*

I think I will order 4 of them. Any idea what thermistor I would need from RobotDigg?


---
**Zane Baird** *March 24, 2016 19:02*

Any of the ones robotdigg has should work, but you should be able to use the same thermistor you are using now. What control board are you using? I ask because in Marlin you may have to create your own temperature look-up table for a different thermistor and in smoothie you'd want to fit the temp table to a steinhart-hart equation and specify the coefficients. I would not recommend using beta values for the hotend temp measurement.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/L9fTxP6mTeT) &mdash; content and formatting may not be reliable*
