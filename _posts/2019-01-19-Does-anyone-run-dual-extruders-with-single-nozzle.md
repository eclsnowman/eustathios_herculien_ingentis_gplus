---
layout: post
title: "Does anyone run dual extruders with single nozzle?"
date: January 19, 2019 18:41
category: "Discussion"
author: Dennis P
---
Does anyone run dual extruders with single nozzle? Care to share your thoughts?  Can you share a pic of your setup? 

I am toying with using a Y-Splitter and can not decide on putting both extruders and rolls of filament on the same side for the sake of wiring or opposite sides to try and 'balance' out the tugging forces on the carriage. 

For now, my intended use is to use a contrasting color for embossed text on the top of some objects, not a lot of filament changes. 







**Dennis P**

---
---
**Yochi Shilo** *January 19, 2019 18:42*

אני                                    אוהבת       את     זה


---
**Stefano Pagani (Stef_FPV)** *January 19, 2019 20:05*

Why don’t you try something like the prusa mmu 


---
**Oliver Seiler** *January 19, 2019 20:41*

I've done a few prints with an E3D Cyclops (and many more on the Chimera).

My main extruder (Bondtech) sits on the right, and secondary (Titan) on the left of the frame, with a spool holder on either side, although I often use larget spools suspended above. That seems to generally work fine. The main issues I'm having with the Cyclops is dialing in the retraction on filament change well enough to prevent clogging, particularly when not using the same filaments (I tried PETG and TPU once, but no success at all, whereas PLA on both works ok).

I bought the Titan mainly to use as a direct extruder (before the BMG came out) and for this setup (the Cyclops requires quite some pressure) I'd rather recommend a Bondtech extruder, or direct drive setup.

For prints that only require few filament changes I often just stop the print and change the filament manually, instead of changing the setup (swap carriage) of my printer. Cleaning a clogged Cyclops isn't fun.


---
**Carter Calhoun** *January 21, 2019 23:23*

I've been using a Diamond Hotend for the past couple of years and really enjoy it. It is temperamental, but I've gotten some great prints from it, and tested successfully with TPU, soluble, and wood filaments.  It does have a tendency to jam, which will ruin prints, but most of the time the jams clear easy enough.  I assembled a structure that sits on top of the printer to hold the filament and motors, and feed the filament down from above.



![images/70eee23e8863b397c90b24e2f8047407.jpg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/70eee23e8863b397c90b24e2f8047407.jpg)


---
**Carter Calhoun** *January 21, 2019 23:25*

My toddler for visual reference ;)

![images/64253bab6fdd12d05cb3b01bfa997507.jpg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/64253bab6fdd12d05cb3b01bfa997507.jpg)


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/cWoL23tbyER) &mdash; content and formatting may not be reliable*
