---
layout: post
title: "Shared on March 07, 2014 05:00...\n"
date: March 07, 2014 05:00
category: "Show and Tell"
author: D Rob
---


![images/d935894916904a934cf063aaca4a316d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d935894916904a934cf063aaca4a316d.jpeg)



**D Rob**

---
---
**Jarred Baines** *March 07, 2014 05:23*

Place your bets folks!

:-P



Good luck - be good to see the results! What did you print it in? ABS?


---
**D Rob** *March 07, 2014 05:25*

yeah glow in the dark reprapper blue


---
**Eric Moy** *March 07, 2014 10:14*

I'm curious to see if it'll work without grease


---
**D Rob** *March 07, 2014 14:46*

its deffinitelt a part I should keep spares to. Since the photo the worm has been acetone smoothed. It made a big difference in the friction level


---
**Tony White** *March 07, 2014 18:15*

what package did you design the worm in? I think we'd all like to see a video of it rotating, even at this early stage


---
**D Rob** *March 07, 2014 18:20*

It has no housing I keep it open. I may add a rod that goes to the front from the tip of the worm and has a key like a crank pencil sharpener to manually move the z of just a knob like on the rod ends for the xy


---
**D Rob** *March 07, 2014 18:22*

It is my hypothesis that the bed weight will keep positive force on the worm through gravity and minimize any backlash from z move reversal


---
**Tony White** *March 07, 2014 18:51*

I meant what 3d design package...


---
**Liam Jackson** *March 07, 2014 21:03*

Possibly useful:

[http://www.thingiverse.com/thing:251004](http://www.thingiverse.com/thing:251004)


---
**D Rob** *March 07, 2014 22:58*

I took stl from thingiverse 2 times to big in solid works and extruded and bored the hub and hole 2 times to big and printed half size.


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/8n8EubsiPKj) &mdash; content and formatting may not be reliable*
