---
layout: post
title: "I wanted to put this out there for folks- I FINALLY finished remixing Walter Hsiao 's miniblock carriage- I added a shelf to locate the E3D V6 hotend, a top cap and fan duct that accepts David Crocker's IR sensor board, I am"
date: May 28, 2018 03:42
category: "Deviations from Norm"
author: Dennis P
---
I wanted to put this out there for folks- I FINALLY finished remixing **+Walter Hsiao** 's miniblock carriage- I added a shelf to locate the E3D V6 hotend, a top cap and fan duct that accepts David Crocker's IR sensor board, I am cleaning up the files and stl's and will put on github in the next few days. 



![images/10a53dc005e7ca50afdfabbac1e2db84.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/10a53dc005e7ca50afdfabbac1e2db84.png)



**Dennis P**

---
---
**Dennis P** *May 28, 2018 03:46*

I put in brass M3 heatserts for the main cooling fan mount, the **+Daniel F** fan support brackets, the top and bottom clamps, but removed the LED light sockets- i could not find the same light modules. 

I also modded the 5x5 magnet pockets by shifting them up 0.5mm so that they will register a little better. The ones in the block sit a hair inside, the ones in the fan duct sit a hair high. Just enough to register their location but not enough to prevent breakaway if there is an obstruction. 

![images/20046f84a1d4b1a49e6029ce86fb72a9.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/20046f84a1d4b1a49e6029ce86fb72a9.png)


---
**Dennis P** *May 28, 2018 03:47*

![images/4d67b311c412d11e458199f4d25abf73.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4d67b311c412d11e458199f4d25abf73.png)


---
**Dennis P** *May 28, 2018 03:48*

![images/2833b23fcc8800415af2fd22698df981.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2833b23fcc8800415af2fd22698df981.png)


---
**jerryflyguy** *May 29, 2018 00:54*

**+Dennis P** tangential to your post, how do you like the IR sensor? What surface are you measuring off? Been wanting to upgrade my carriage and add some kind of bed leveling to my printer(Eustathios 2). Do you get good first layers with this setup?


---
**Dennis P** *May 29, 2018 22:53*

**+jerryflyguy** So far i like it. Its small in size and lightweight. it seems to be accurate and very functional. I did spray paint the aluminum bed plate with flat black bbq grill paint, and my bed is supported on lt blue silicone heat transfer pads. I am not sure if the sensor is probing over those exactly yet. I want to map those out and see what is going on. I have also been trying to figure out the Unified Bed Leveling vs Bilinear. Both yield that my glass bed looks like a saddle or a Pringles potato chip. I want to try cleaning the bed REALLY well and move the pads around and then check it with a straight edge. 

. 

I am still trying to figure out how to configure and use it with RAMPS/ Marlin. I am printing PETG on hot glass with Aquanet. Marlin issue seems to be where you put in the probe Z offset- I had it hard coded in Configuration.h and had it set with M851 and my bed visualizer was all over the map.  Once I cleared M851, things seemed more like they should be,. I think that I will put in Z0.0 in the Configuration.h file and set manually with M851 gcode. Probe sensing height seems to be ~2.6mm. 

    

The only thing I do not like is that you can not see the hotend now because the probe is in the way- but i figure it can moved to the back or side later.  or just look from the other side for now. 


---
**Dennis P** *June 19, 2018 02:24*

I put up the final part files on the github- top clamp with locating ledge  for E3D indexes to the 2 holes on top of the carriage that were used for Bowden tube snorkel, the carriage with locating ledge for E3D, and the fan duct with ir sensor offset to the side so hotend is visible. 

I have found that the nozzle height is VERY sensitive to align the top of the hotend to the top of the carriage.  

  

![images/28ee4f3fa2f09f23333c05d1a6d7d89b.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/28ee4f3fa2f09f23333c05d1a6d7d89b.png)


---
**Stefano Pagani (Stef_FPV)** *June 20, 2018 17:22*

Do you have a .STEP of his?


---
**Stefano Pagani (Stef_FPV)** *June 20, 2018 17:23*

nvm found it!


---
**Dennis P** *June 20, 2018 20:17*

There should be a 3d dxf of this that you can import. If Eric didn't merge the git repository yet, I am happy to send to you directly. 

FWIW- You can use the top block without the modified carriage- the shelf on it works like a claw to grab the backside of the hotend and it registers the top face of the hotend to the top face of the carriage. 


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/4DnGFAUeewy) &mdash; content and formatting may not be reliable*
