---
layout: post
title: "I have a Herculien that I'm prepping to enclose and was wondering how you all route your wires/cables for a clean build"
date: March 07, 2016 13:14
category: "Discussion"
author: Luis Pacheco
---
I have a Herculien that I'm prepping to enclose and was wondering how you all route your wires/cables for a clean build. My old printer looks like Medusa's head. I keep seeing these super clean builds and I can't wrap my head around it. Any pictures would help greatly, thanks!





**Luis Pacheco**

---
---
**Tomek Brzezinski** *March 07, 2016 16:19*

One of my favorite cable routing methods is cable ties + foam backed adhesive cable tie mounts. It is infinitely better than non-routed cables, but itself is still not the nicest.    You can find 100pcs for <$6 on ebay.   



If you want to pay a little premium to look nicer, you can get foam or screw mounted cable <b>clips</b> and also use nylon sheathing for your cables, and shrink-tube the ends (the sheathing tends to fray on ends.)  That looks pretty classy. 


---
**Chris Brent** *March 07, 2016 16:47*

This stuff [http://www.amazon.com/gp/product/B00H2RCY0I?psc=1&redirect=true&ref_=oh_aui_detailpage_o07_s00](http://www.amazon.com/gp/product/B00H2RCY0I?psc=1&redirect=true&ref_=oh_aui_detailpage_o07_s00) helps a lot (they have smaller diameters too). As Tomek said you want to heat shrink cover the ends, and pull the braid tight too.


---
**Jim Stone** *March 07, 2016 17:12*

Slot Covers. if youre using all openbuilds extrusion then get openbuilds slot cover. otherwise get from misumi.



but its real nice. and if you get two colors you can "decorate" :P



the only wires i so far see on my build are on the inside when they travel up and out to the box.


---
**Luis Pacheco** *March 12, 2016 22:32*

I do have some slot covers that I will be using to run the wires along the machine, I just wasn't sure how people were running Herculien wires from inside the machine to the outside after putting on the plexiglass.


---
**Jim Stone** *March 12, 2016 22:34*

Oh...I run mine all to one common corner. Then up. As close and tight to the corner as I can. 



Same corner the hole in the lid for filament is


---
**Luis Pacheco** *March 14, 2016 12:19*

I'll have to give that a try, I'll modify the plexiglass on that side so that I can slide the cables out on a corner then up the extrusion hidden by slot covers. Thanks **+Jim Stone** !


---
**Jim Stone** *March 18, 2016 08:44*

i think you misunderstood me. i hadnt modifed any of the plexi. ill take pics tomorrow.


---
**Luis Pacheco** *March 20, 2016 03:11*

That would be great **+Jim Stone** 


---
*Imported from [Google+](https://plus.google.com/110276424073473119972/posts/dGekatScMTZ) &mdash; content and formatting may not be reliable*
