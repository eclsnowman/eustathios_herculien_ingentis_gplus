---
layout: post
title: "Rod Wear? How do your rods appear to be wearing in?"
date: April 15, 2018 19:04
category: "Discussion"
author: Dennis P
---
Rod Wear?



How do your rods appear to be wearing in?



I try to move the prints around so I can 'maximize' the use of my hairspray on glass and distribute wear on the rods. I am beginning to see how the middle sections of my guide rods are a little more polished. Its an ever so light patina difference in just the right light.  



I can not tell if its bronze wearing into the surface of the rods of the chrome on the rods wearing a little bit. 



To those who have long term use- what are your observations?







**Dennis P**

---
---
**Eric Lien** *April 15, 2018 19:29*

Years of printing at the center of the bed. Yes the rods surface has a different shine, but no dimension change on the rods measured with my micrometer. The carriage bushings have likely worn in. But since two bushings span across the carriage width the wear has not generated any "slop" in the carriage that I can tell by hand or visible in the prints.


---
**Ryan Carlyle** *April 15, 2018 19:46*

The rods should be vastly harder than the bushings, so negligible rod wear. More likely some burnishing. 


---
**Dennis P** *April 15, 2018 21:19*

**+Ryan Carlyle** burnishing is more like it. I did not think of that as a description but its probably more accurate. 


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/Nk9xHwE1aNq) &mdash; content and formatting may not be reliable*
