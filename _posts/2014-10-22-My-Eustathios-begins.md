---
layout: post
title: "My Eustathios begins!"
date: October 22, 2014 23:40
category: "Show and Tell"
author: Jim Wilson
---
My Eustathios begins!

![images/5f0605072eab23900682584cd132876c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5f0605072eab23900682584cd132876c.jpeg)



**Jim Wilson**

---
---
**Eric Lien** *October 23, 2014 00:02*

Would you mind taking pics during the build. I lost mine due to an sdcard corruption that none of the standard restore utilities could repair. I have been meaning to build an assembly writeup for the wiki. I can use solidworks exploded views... But some times pics just make things easier.


---
**Jim Wilson** *October 23, 2014 11:32*

Absolutely. Sadly I got nearly all the parts other than the hammerhead nuts, once they get in I'll take lots of overall and detail shots.


---
**Jim Squirrel** *October 23, 2014 15:25*

**+Eric Lien** file scavenger is an app i use to recover data. [http://www.quetek.com/prod02.htm](http://www.quetek.com/prod02.htm) it's slow but does get most jobs done

[http://www.quetek.com/prod02.htm](http://www.quetek.com/prod02.htm) if you want to buy it 


---
**Eric Lien** *October 23, 2014 15:31*

**+Jim Squirrel** I will give it a go... But to be honest I don't think it will work. Let's just say the corruption was the result of F=MA not not your standard file system corruption ;)



When an heavy object collides with an immoveable object bad things happen.


---
**Jim Wilson** *October 23, 2014 17:26*

Does a wiki already exist for ingentis/eustathios? I did a bit of googling but was not able to find it.﻿


---
**Eric Lien** *October 23, 2014 17:55*

**+Jim Wilson** not yet. The github by Jason Smith has a BOM, and notes, but a build guide would be icing on the cake.


---
**Jim Wilson** *October 23, 2014 18:20*

**+Eric Lien** I'll be happy to contribute, thank you!﻿ I can make any part of my build process available. I think the only thing I'm customizing from the design is the extruder, which seems to be an interchangeable part anyway.﻿


---
**Eric Lien** *October 23, 2014 18:29*

**+Jim Wilson** Sounds great. Thanks, the community will be indebted. 


---
**Jeremy Felix D.** *October 23, 2014 21:41*

Where'd u buy the extrusions, I've been shopping around but haven't settled.


---
**Jim Wilson** *October 30, 2014 20:24*

**+Jeremy Felix D.** I used aliexpress, they shipped from China


---
**Jim Wilson** *October 30, 2014 20:26*

**+Eric Lien** sorry for the delay. Think I have all the printed parts ready, I hadn't noticed your spider part variants and the superior carriage originally, so I'm printing most of those now.



The nuts that slot into the extrusion are the only frame hardware that hasn't arrived yet, when they do I'll grab a good camera and start the glamor shots.


---
**Eric Lien** *October 30, 2014 22:36*

**+Jim Wilson** Thank you for this. It will be a great community resource.


---
*Imported from [Google+](https://plus.google.com/101778058628996936791/posts/ecsZQKDYkLP) &mdash; content and formatting may not be reliable*
