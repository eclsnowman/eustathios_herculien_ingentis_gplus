---
layout: post
title: "I'm asking this question in this group because I know all the cool people hang out here ;-) No seriously, its cos I feel there are some and mostly awesome minds in this club"
date: November 07, 2015 06:31
category: "Deviations from Norm"
author: Jarred Baines
---
I'm asking this question in this group because I know all the cool people hang out here ;-)



No seriously, its cos I feel there are some and mostly awesome minds in this club.



I'm looking at making a Delta out of some excess t slot and an old Mendel 3d printer as a donor for most other parts.



Question is: what is the best (most rigid and strong) plastic for printing printer components? I want the corners to be stiff AND strong... Nylon was a thought but I'm not sure that it will be rigid enough? PET-G or ABS? PLA is very stiff, then there are more exotics and branded materials such as colorfab XT etc...



Opinions? Information will be relevant to any reprap 3d printers. A lot of builds use ABS for hi temp parts and PLA for low temp parts, makes sense but what would be ideal? Quality over price, don't mind paying for better materials.





**Jarred Baines**

---
---
**Whosa whatsis** *November 07, 2015 06:56*

Off the top of my head, maybe PC-ABS?


---
**Eirikur Sigbjörnsson** *November 07, 2015 11:12*

Best that I have tried is the XT-CF20 Carbon Fiber material (Colorfabb), but its not cheap and you would need a hardened steel nozzle  for it


---
**Hakan Evirgen** *November 07, 2015 11:18*

I have also used XT-CF20 and it is really stiff. And yes, you need steel nozzle or your brass nozzle will be worn out after about 300g.


---
**Eric Lien** *November 07, 2015 13:38*

Personal opinion. Buy the extruded aluminum vertex kit from Robotdigg. For the money you would be hard pressed to print anything with similar rigidity.


---
**Eric Lien** *November 07, 2015 13:41*

And for the other parts, I am using PETG on my Delta. Love it. But for added stiffness xt-cf20 would be difficult to top.


---
**Miguel Sánchez** *November 07, 2015 14:40*

Do you know there are metal corners for 20x20 too? [http://www.aliexpress.com/item/6-pcs-set-Metal-Corner-for-Kossel-of-2020-OpenBeam-for-Kossel-3D-printer/32523023191.html?spm=2114.01020208.3.30.yHjamT&ws_ab_test=searchweb201556_1_79_78_77_91_80,searchweb201644_5,searchweb201560_9](http://www.aliexpress.com/item/6-pcs-set-Metal-Corner-for-Kossel-of-2020-OpenBeam-for-Kossel-3D-printer/32523023191.html?spm=2114.01020208.3.30.yHjamT&ws_ab_test=searchweb201556_1_79_78_77_91_80,searchweb201644_5,searchweb201560_9)


---
**Mutley3D** *November 07, 2015 23:06*

Having recently used polycarbonate filament from polymaker I can safely say nothing stronger that I know of, it's almost glass-like. I have used cf-abs and pc seems far stronger but you need all metal hotend to print with it.


---
*Imported from [Google+](https://plus.google.com/+JarredBaines/posts/aqX2oxnDtMW) &mdash; content and formatting may not be reliable*
