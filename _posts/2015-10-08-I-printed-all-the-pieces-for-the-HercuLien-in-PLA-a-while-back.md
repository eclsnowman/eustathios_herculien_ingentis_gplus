---
layout: post
title: "I printed all the pieces for the HercuLien in PLA a while back"
date: October 08, 2015 20:49
category: "Discussion"
author: Brandon Cramer
---
I printed all the pieces for the HercuLien in PLA a while back. I don't see myself building the HercuLien anytime soon so I wanted to see if anyone is interested in purchasing the 3D Printed pieces. The only thing missing is the second Flange and Spool holder. Everything else is there. 



Let me know if you are interested. 

![images/fac842fec5664f1c6b4a10b51dce3a60.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fac842fec5664f1c6b4a10b51dce3a60.jpeg)



**Brandon Cramer**

---
---
**Paul Bryan** *October 09, 2015 16:04*

Are these for the standard 20 x 20 extrusions? How much do you want? 


---
**Brandon Cramer** *October 09, 2015 21:05*

Yes, I would say $50 plus shipping. If these sit on my desk much longer, I might start the build on the HercuLien. :) If you send me your zip code I can try to figure out the cost to ship them UPS ground with a tracking number.


---
**Paul Bryan** *October 11, 2015 02:23*

Thanks Brandon, sound reasonable. I am in Houston 77065 zip 


---
**Brandon Cramer** *October 11, 2015 03:45*

**+Paul Bryan** Looks like $25.00 should cover it. So $75.00 total. Let me know if you are still interested. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/GMLFsfw3JfU) &mdash; content and formatting may not be reliable*
