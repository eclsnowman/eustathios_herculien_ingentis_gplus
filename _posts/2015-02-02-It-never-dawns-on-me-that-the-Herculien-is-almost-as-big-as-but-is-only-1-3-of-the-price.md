---
layout: post
title: "It never dawns on me that the Herculien is almost as big as but is only 1/3 of the price"
date: February 02, 2015 20:00
category: "Discussion"
author: Dat Chu
---
It never dawns on me that the Herculien is almost as big as [http://www.re3d.org/gigabot/](http://www.re3d.org/gigabot/) but is only 1/3 of the price. :D



This seems to reinforce the notion that for a fast moving with plenty of interest industry like 3d printing, spending too much money is a bad idea.





**Dat Chu**

---
---
**Mike Miller** *February 02, 2015 20:44*

I'll bet   #Herculien   is better built, too. 


---
**James Rivera** *February 02, 2015 20:52*

The build volume of the Gigabot is something almost nobody will ever need or use. Plus, how long would a print that uses the full volume take? I doubt this thing is a rocket. Also, would such a long print actually succeed? I'm skeptical of that point in particular. Big print volume does not necessarily mean reliable enough to actually print something of that volume.  This Gigabot thing is for people with too much money who have bought into the hype.



Bang for the buck, HercuLien/Eustathios/Ingentis (and maybe Procerus) seem like the best bang for the DIY buck at the best print quality.



If you just want to buy an assembled one, then Ultimaker/Lulzbot/Printrbot seem like the best choices.



My $.02. YMMV.


---
**Dat Chu** *February 02, 2015 20:56*

it's honestly hard to beat the print volume at $1500. The community seems to be moving toward faster print and quieter print. But what are the options? SLA certain wins when the object has a big enough cross section area. How about 4 extruders simultaneously working?


---
**Mike Miller** *February 02, 2015 21:18*

It's always cheaper to build it yourself. A Company has to pay employees, rent, and utilities. After hours, my time is free. 


---
**James Rivera** *February 02, 2015 21:24*

**+Dat Chu** Long-term, I think a fast layer technology will win out over FFF/FDM. Similar to DLP+resin, like exposing a photo, frame by frame (with a layer being the frame). Doing a whole layer more or less simultaneously has obvious speed advantages. Current light-cured resins are mostly there. If there was a way to embed a color (e.g. have the wavelength of light determine the color or something like that) then you have a full-color printer at high speed and high resolution. But cost is always an issue, and right now, RepRap FFF/FDM are the best game in town.


---
**Eric Lien** *February 03, 2015 00:48*

Hercule frame is 24x24x24 (without the lid), build volume on it is around 338x358x320. I think I can get more by relocating some components.



But the gigabit is 24"^3 build volume. Now that's huge.


---
**Joe Kachmar** *February 03, 2015 00:56*

**+James Rivera** It's not whole-layer, but Yvo de Haas is working on an SLS-style printer that uses an inkjet printhead to lay down a bonding agent onto the powder for each print layer. The printhead sweeps over a section of the print at a time, instead of tracing it out like SLS lasers or FFF hot ends to.



[http://ytec3d.com/plan-b/](http://ytec3d.com/plan-b/)


---
**James Rivera** *February 03, 2015 01:59*

**+Joe Kachmar** that looks interesting, but it looks like it is still pre-alpha and it doesn't do multiple colors. Also, it's not clear how strong the parts it produces will be, which may severely limit its usefulness. Then there is the question of consumable costs. And with resolution being roughly comparable to a RepRap FFF/FDM style printer, why would anyone build it? Not trying to rain on the parade, but it just doesn't seem ready to be useful...yet.


---
**Joe Kachmar** *February 03, 2015 02:20*

**+James Rivera** Yeah, it's definitely rough around the edges compared to the significantly more mature FFF RepRap printers out there. HP's whitepaper on a very similar SLS-style printer shows what the technology is capable of though:



[http://h20195.www2.hp.com/v2/GetPDF.aspx/4AA5-5472ENW.pdf](http://h20195.www2.hp.com/v2/GetPDF.aspx/4AA5-5472ENW.pdf)



Obviously something like this is way outside the budget/scope of open source hobbyist designs, but then so was FDM back in the 80s. 


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/Mh7iFZ9Sy3B) &mdash; content and formatting may not be reliable*
