---
layout: post
title: "Top panel finally done! Ran this guy 22 hours a day for a month no breakdowns!"
date: November 07, 2018 14:21
category: "Show and Tell"
author: Stefano Pagani (Stef_FPV)
---
Top panel finally done! Ran this guy 22 hours a day for a month no breakdowns!



Also got a nice 5.5 LCD



![images/f913a5cfb463a8f88dc6695bfd73db74.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f913a5cfb463a8f88dc6695bfd73db74.jpeg)
![images/8aa8000cd16f2675f2ef7dbf960caa07.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8aa8000cd16f2675f2ef7dbf960caa07.jpeg)

**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *November 07, 2018 15:30*

Looking great. Glad to hear you got all the bugs worked out.



Very clean looking machine.


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/Re7TnNzuP2s) &mdash; content and formatting may not be reliable*
