---
layout: post
title: "Thanks for the add to the group"
date: June 30, 2016 20:50
category: "Discussion"
author: Carlos Porto
---
Thanks for the add to the group. I've been thinking of converting my eclip3d coreXY printer into either of the HercuLien or Eustathios.  Having seen some amazing prints from Eric Lien. I was wondering, why does this type of printer seem to produce better prints in general to a coreXY machine. What are the advantages or disadvantages. Sorry if this has been asked before, just trying to expand my knowledge further.



Thanks!





**Carlos Porto**

---
---
**Tomek Brzezinski** *June 30, 2016 21:43*

I'd like to just follow this conversation but don't see anyway to do that on mobile (besides posting.) 


---
**Ryan Carlyle** *June 30, 2016 22:32*

Shouldn't be any meaningful difference in print quality, assuming similar component quality and scaling. If anything, CoreXY has higher print resolution due to the way the belts work. What kind of CoreXY are you not getting awesome results out of?


---
**Carlos Porto** *June 30, 2016 22:53*

I've been getting pretty good print quality out of my Eclips3d. So no complaints there. But having seen prints on HercuLien by **+Eric Lien**. Got me thinking otherwise.


---
**Ben Delarre** *July 01, 2016 00:16*

It's all down to the operator if you ask me. I have a eustathios, and I can't get near the quality I have seen from **+Eric Lien**​ and others here. 


---
**Carlos Porto** *July 01, 2016 01:42*

Indeed I can agree with an operator knowing how to dial things in, but was hoping this wasn't a grass is greener on the other side situation.


---
**Eric Lien** *July 01, 2016 02:10*

Quality prints are always a function of good mechanical design on the printer,  but also (and even more importantly) knowing how to tune the printer. Some of the best prints I've ever seen came off my friends self-sourced prusa i3.



My first printer design was actually a coreXY and I did get very good prints off of it. My problem was I designed that printer from scratch having never owned a 3D printer before. I should have spent a little more time in the design phase, but some of the best lessons are learned through mistakes .



I looked at the eclipse3d printer there's a few things I'm not a big fan of ([http://i.imgur.com/Q2DdNbW.jpg](http://i.imgur.com/Q2DdNbW.jpg)). For example to belt paths and large cantilever of the idlers on the X axis are poor choices IMHO. After fighting my round rail based corexy, I feel corexy is a better candidate for ground linear guide rails on X/Y. Also keeping the pulleys and belts closer to the rails travel and if possible supporting all pulleys from both sides.



But maybe modifying your current printer would be a better place to start than completely gutting it. That being said if you do decide to modify it into a cross rod gantry printer I will be excited to see your progress. But please note it will take a lot of modifications if you try and use the existing frame and bed.



﻿


---
**Carlos Porto** *July 01, 2016 04:51*

**+Eric Lien**, thanks for the constructive advice. So it sounds like continuing with coreXY wouldn't be a bad idea and maybe at a later point build an Eustathios. With that being said, let me pick your brain a bit further.



It's funny you mentioned the rails. It's been one of my pet peeves and I've been thinking of upgrading all the rods to linear rails for some time. So that would probably be the first change. 



For the belt path, do you recommend something similar to a D-bot or FuseBox, a stacked coreXY configuration. I noticed, especially with the D-bot the belts are kept quite close to the rails and pullies.



I think one thing I will directly borrow from the Herculean is the z-stage since it seems to be pretty smart use of one motor.



Anymore feedback would be excellent.



Thanks again.


---
**Roland Barenbrug** *July 01, 2016 09:53*

can only confirm **+Eric Lien** statement. Building of the Eustathios is the easy part especially when you have a friend for printing the parts (which I didn't so had to spend some time on printing) and have the frames nicely cut (which I did). Tuning the machine to create great prints is a different league. Today my Prusa I3 prints still better than my Eustathios. The good news is that Eustathios is slowly approaching ;-)


---
**Carlos Porto** *July 01, 2016 15:17*

Thankfully I have my Printrbot Simple, it's slow, but a trusty work horse and really prints fantastic. The eclips3d was the first printer I've ever built. So it was a great learning experience. But still need to get more experience under my belt.




---
*Imported from [Google+](https://plus.google.com/112207937847873893334/posts/KjVkQ8Gwh6F) &mdash; content and formatting may not be reliable*
