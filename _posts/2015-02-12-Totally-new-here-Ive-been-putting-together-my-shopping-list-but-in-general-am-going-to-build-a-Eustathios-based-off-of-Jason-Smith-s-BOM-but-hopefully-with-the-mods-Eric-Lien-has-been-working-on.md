---
layout: post
title: "Totally new here, I've been putting together my shopping list, but in general am going to build a Eustathios based off of Jason Smith 's BOM , but hopefully with the mods Eric Lien has been working on"
date: February 12, 2015 20:11
category: "Discussion"
author: Seth Messer
---
Totally new here, I've been putting together my shopping list, but in general am going to build a Eustathios based off of **+Jason Smith**'s BOM , but hopefully with the mods **+Eric Lien** has been working on. My question is, is there a service, or do any of you offer to print parts for a charge for those of us that don't have access to a printer yet?



Thanks much!





**Seth Messer**

---
---
**Joe Kachmar** *February 12, 2015 20:16*

**+Eric Lien** put up a Print it  Forward spreadsheet in this post:



[https://plus.google.com/+EricLiensMind/posts/bg7NPYn4AA1](https://plus.google.com/+EricLiensMind/posts/bg7NPYn4AA1)



I'm actually in the process of getting a semi-working Kossel up and running, so I might be taking my name off of that list if I can get it printing dimensionally accurate parts.


---
**Seth Messer** *February 12, 2015 20:19*

Thanks so much **+Joe Kachmar**! Looks like someone added me already! Awesome community.


---
**Ryan Jennings** *February 13, 2015 00:27*

I am happy to print parts for people but i only have pla, so the main carriage is the biggest issue.  Even for the PIF thread i am happy to provide plenty of pla parts. 

 

I have a half dozen kg of 3mm pla for a old printer that im just trying to use up to switch over to 1.75


---
**Gus Montoya** *February 18, 2015 05:44*

Hi **+Seth Messer** Can you verify for me that Jason Smith's parts are the same as Eric Liens spider build? From the looks of the 2020 extrusions part numbers. Eric's Eutathios is bigger. Still trying to figure out the lead screw and linear rods to purchase. Also the bearings, bushings, and pulleys etc


---
**Seth Messer** *February 18, 2015 15:17*

**+Gus Montoya** no, i don't think all the parts are the same. **+Eric Lien** is working on a V2 BOM and updated STL files and all that awesome stuff, but at this time, no ETA for any of that. Hah, I believe he may have over-booked himself with 2 popular machine designs and maintaining/supporting both for the community. :) Too bad the build design creators don't take donations (though, they may, but I haven't seen patreon/paypal/bc links).



In other words, I'm purchasing the non-size specific stuff first, and will have all that ready to go so that whenever Eric's Eustathios V2 BOM/designs are completed, i'll jump on ordering the rest. Hopefully too, I'll be further up the list on the pay-it-forward printed parts list. I'd even pay for that (materials, time, shipping).



Good luck **+Gus Montoya**! Hope that answers your question. (in a rather long-winded way)


---
**Gus Montoya** *February 18, 2015 23:54*

I received word from Eric that he'll have things done in less then a week. I'll go ahead and do the same as you (order the non-size specific stuff first. So it's safe to order the bearings and pulley's from james smith list right?


---
**Seth Messer** *February 18, 2015 23:55*

hmm.. Gus, I wouldn't. I've completely held off on all things structural and moving. i've ordered electrical related things, including hotend (going with E3D v6 for now since his carriage assembly v4 didn't fit the volcano, which is what **+Jason Smith** was using)


---
**Gus Montoya** *February 19, 2015 00:07*

Ok yeah then I have all I can have at the moment. I'll have to wait and be patient.


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/JJLmFstzuZq) &mdash; content and formatting may not be reliable*
