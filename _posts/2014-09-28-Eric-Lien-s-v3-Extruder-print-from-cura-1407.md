---
layout: post
title: "Eric Lien 's v3 Extruder print from cura 14.07...."
date: September 28, 2014 09:00
category: "Show and Tell"
author: Jim Squirrel
---
**+Eric Lien**'s v3 Extruder print from cura 14.07.... support from bed... 50mm/sec .03 layer height 2mm shell thickness fill Densitiy 20% 3mm E3D v6 direct drive with seemecnc ezstruder (direct drive) so damn tuned that support gently pulls off like **+Simplify3D**  minus the price tag.... (btw **+Simplify3D**  I will do a free review against the free products I use if you provide me with a free license of your paid product. ) 




**Video content missing for image https://lh3.googleusercontent.com/-HXgiQ7aYPmQ/VCfKzj6RZcI/AAAAAAAAFsM/hk4-U2qlVZk/s0/20140928_030130.mp4.gif**
![images/71287021776c4504becc76a92201890f.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/71287021776c4504becc76a92201890f.gif)
![images/f368f68f4c00f0f54ac31e3a050373ba.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f368f68f4c00f0f54ac31e3a050373ba.jpeg)
![images/9a56b078d31717fff3a2023a915729af.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9a56b078d31717fff3a2023a915729af.jpeg)
![images/3a5c3b8748b3d4cfb5002f470a45a39f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3a5c3b8748b3d4cfb5002f470a45a39f.jpeg)
![images/741a0d707a682c35f9b471681bd6b9aa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/741a0d707a682c35f9b471681bd6b9aa.jpeg)
![images/1822ddaae189f65791dc1f31736258eb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1822ddaae189f65791dc1f31736258eb.jpeg)
![images/7f67af4a6a18e5557664040244adf81a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7f67af4a6a18e5557664040244adf81a.jpeg)
![images/17a5ee5cdfa65ec3f7ab6ffb5cc322a7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/17a5ee5cdfa65ec3f7ab6ffb5cc322a7.jpeg)
![images/98654118adeb38fa66a15433ad6e6f72.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/98654118adeb38fa66a15433ad6e6f72.jpeg)
![images/055b3a87443ffdb6927dd3a0f297c19f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/055b3a87443ffdb6927dd3a0f297c19f.jpeg)
![images/67e227d031ae2efa45a94f78511ff594.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/67e227d031ae2efa45a94f78511ff594.jpeg)
![images/033cadc47f8fa5cb5ecf977d90d9812f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/033cadc47f8fa5cb5ecf977d90d9812f.jpeg)
![images/a3e105ed746ed5f5100211f1fb633de8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a3e105ed746ed5f5100211f1fb633de8.jpeg)
![images/9ceff1bce759020f2d582f24b4433add.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9ceff1bce759020f2d582f24b4433add.jpeg)
![images/f8b8775c43baf0f2d764ce32d58be954.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f8b8775c43baf0f2d764ce32d58be954.jpeg)

**Jim Squirrel**

---
---
**Eric Lien** *September 28, 2014 11:20*

Looks great.


---
**Eric Lien** *September 28, 2014 14:16*

Just a word of advice. Soften the bushing holes with acetone before pushing in the bushings (presuming you printed it in abs). This will make the job easier, act as a glue to hold it in long term, and limit the chances of splitting the part along the top where the part is thin and layers run in a direction more likely to separate. Can you tell I am speaking from experience :)


---
**Hendrik Wiese** *September 28, 2014 21:18*

0.03mm layer height? I suppose you mean 0.3mm. Layers appear pretty thick for 0.03mm. Or do you use a different unit?


---
**Jim Squirrel** *September 28, 2014 23:23*

0.3 is what i meant


---
**Eric Lien** *September 29, 2014 00:01*

**+Jim Squirrel** Let me know how the v3 carriage works. I will be redesigning it soon to replace the tube axial part cooling fan with a centrifugal fan. But v3 is the version I printed when I made a Eustathios kit for **+cobraphil789** a few weeks ago. It should direct the air more directly at the nozzle than the version I am using now.


---
**Brandon Satterfield** *September 29, 2014 13:21*

**+Jim Squirrel** man you really do have it tuned in. As a S3D user I can tell you that you an epsilon within having the same quality. 

Nice ringing around the holes..:-) thought I was the only one that left that in my prints. 


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/VjeANLEioDE) &mdash; content and formatting may not be reliable*
