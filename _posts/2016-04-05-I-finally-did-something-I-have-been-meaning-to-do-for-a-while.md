---
layout: post
title: "I finally did something I have been meaning to do for a while"
date: April 05, 2016 14:32
category: "Discussion"
author: Eric Lien
---
I finally did something I have been meaning to do for a while. My old HercuStruder design allowed for mounting my second extruder onto the 20x20 upright extrusion since the face of the extruder sits flush with the extrusion. But the Bondtech housing does not allow for this since the clamp holds onto the planetary section of the stepper so the Bondtech housing sits forward of the face of the extrusion. I have my control box sitting out front on a hinge, it can swing out of the way to open the front door. This would swing it back into the second Bondtech. I have had it hacked to work for a while... but I finally got around to making a new mount. The model is odd looking but places both extruders exactly how I wanted them. It was as compact as I could get things while still allowing both QR hinges to swing independently. 



The only pain is that to make the print as strong as possible it should be printed laying down, so that requires a fair amount of support. This is no issue for Simplify3D. But with other slicers YMMV.



It is designed to use brass thermal set inserts in the back side to clamp it together. So depending on the size inserts you have the size of this pocket hole may need adjustment. Also please don't judge my modeling on this one. The solidworks design tree is atrocious, but I was in a hurry to finally get this done and so took a few shortcuts.



Solidworks:

[https://drive.google.com/file/d/0B1rU7sHY9d8qWkhzY09SaXczVDA/view?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qWkhzY09SaXczVDA/view?usp=sharing)



STL:

[https://drive.google.com/file/d/0B1rU7sHY9d8qUXlJdFVUbVpZODA/view?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qUXlJdFVUbVpZODA/view?usp=sharing)



![images/b679e5aea3f853a3c114e289867e7c54.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b679e5aea3f853a3c114e289867e7c54.jpeg)
![images/9d6ff63f9e16bf6baee9a4edfb0897a4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9d6ff63f9e16bf6baee9a4edfb0897a4.jpeg)
![images/4b629aaa1f225a3bf7ce5742dd551596.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4b629aaa1f225a3bf7ce5742dd551596.jpeg)
![images/523e9ea75ad7ee1c068aeb46983b5865.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/523e9ea75ad7ee1c068aeb46983b5865.png)
![images/f6788e9613b0ad8b22183548b67bf739.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f6788e9613b0ad8b22183548b67bf739.png)
![images/27fe43f12a975d3c8b864acfa5be6086.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/27fe43f12a975d3c8b864acfa5be6086.jpeg)

**Eric Lien**

---
---
**Joe Spanier** *April 05, 2016 14:41*

thats awesome looking.  Good job Eric


---
**Maxime Favre** *April 05, 2016 17:06*

Looking good!


---
**Steve Johnstone** *April 05, 2016 19:08*

Nice work Eric!



Is there a CAD model around for the bondtec extruder? I haven't checked their website but it would be great to be able to drop one into a design working on - external surfaces would be fine.


---
**Eric Lien** *April 05, 2016 23:44*

**+Steve Johnstone** I have a model, But since it's not my design it is not mine to share. That would be up to Martin. But I think a simplified surface model of the extruder motor and housing for design placement is a good idea. It could help people create proper space allowances for the BondTechs in their custom printer designs. 



**+Martin Bondéus**​ what do you think.


---
**Martin Bondéus** *April 06, 2016 06:46*

**+Steve Johnstone**  I can provide you with a step-file of the extruder that you can use for design purposes, drop me an e-mail at martin@bondtech.se and I will send you the file.



If anymore is interested just let me know.


---
**Steve Johnstone** *April 06, 2016 18:39*

Brilliant Martin thankyou! Eric I totaly agree, having a packaging model to use during the design would be really useful. 


---
**Fash Azad** *April 10, 2016 11:02*

Eric, fantastic job with the spider V2 design.  I've been following your work for a bit and you've inspired me to build one of your awesome printers.  I was wondering if you could tell me where I could purchase a kit of the printed parts. 


---
**Eric Lien** *April 10, 2016 13:35*

**+Fash Azad**​ I don't know of anyone selling kits per say. But some people have used 3dhubs. Some found a local hackerspace. Some made friends with someone in the group and asked a favor. I know I have personally printed many many kits but had to stop once the volume and frequency of requests became to often.



But I wish you the best in sourcing the printed parts. And congratulations on your decision to build the Eustatios Spider V2. There are many amazing people in the group here for building, tuning, and modification advice.﻿


---
**Botio Kuo** *May 25, 2016 20:53*

Could you tell me where is the single one version for Eustathios, pls ? thank you


---
**Eric Lien** *May 25, 2016 21:18*

**+Botio Kuo** [http://shop.bondtech.se/ec/extruders/bondtech-qr-175-universal.html](http://shop.bondtech.se/ec/extruders/bondtech-qr-175-universal.html)


---
**Eric Lien** *May 25, 2016 21:20*

**+Botio Kuo** [http://shop.bondtech.se/ec/mounts/herculien-mount.html](http://shop.bondtech.se/ec/mounts/herculien-mount.html)


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/FJFNNZWJ2o4) &mdash; content and formatting may not be reliable*
