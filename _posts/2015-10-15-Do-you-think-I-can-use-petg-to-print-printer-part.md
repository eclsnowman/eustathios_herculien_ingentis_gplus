---
layout: post
title: "Do you think I can use petg to print printer part?"
date: October 15, 2015 08:27
category: "Discussion"
author: Olivier KUNTZ
---
Do you think I can use petg to print printer part?





**Olivier KUNTZ**

---
---
**Frank “Helmi” Helmschrott** *October 15, 2015 08:30*

sure, mine exists fully of PETG - even the carriage and the fan duct with no problem.


---
**Olivier KUNTZ** *October 15, 2015 09:23*

Good news :)


---
**Eric Lien** *October 15, 2015 10:43*

The only issue I see is if you enclose the printer like HercuLien. If so ABS is still the best bet due to heat build up on long prints. On a long ABS print my chamber reaches thermal equilibrium at around 55C to 60C which may cause issues over a long time exposure with PETG parts in tension. But for a unenclosed Eustathios PETG is perfect.


---
**Olivier KUNTZ** *October 15, 2015 11:36*

I would like to make an enclosed printer. 


---
**Olivier KUNTZ** *October 15, 2015 11:38*

But i have some problem with abs. 


---
**Bud Hammerton** *October 15, 2015 12:54*

What about something like Taulman Alloy 910. Might be easier to print than ABS and much stronger also. Mine is all printed in PETG also, except for the carriage which is Nylon.



Only issue I see is that nylon would double the cost of the printed parts, since the filament is nearly double the price of PETG.


---
**Olivier KUNTZ** *October 15, 2015 13:50*

Perhaps I can use both : nylon for parts in tension en petg for the other parts.


---
**Jeff DeMaagd** *October 15, 2015 18:15*

Even with an enclosed printer might still be OK if you have an autofan if the temp gets too high for comfort. I know Marlin and Smoothieware can be set up to turn on a fan if a certain sensor exceeds a certain temperature.


---
**Eirikur Sigbjörnsson** *October 15, 2015 21:28*

After having tested the PETG and liked it, I am now leaning towards using the Carbon Fibre enhanced XT material from Colorfabb for my Herculien parts. However the regular XT material is much better than the PETG and at the moment tops my list after having tested a range of materials. My advise would be to buy some samples of it and maybe some other samples ( you can order those from either [http://globalfsd.com/](http://globalfsd.com/) or directly from colorfabb), and do some test prints


---
**Olivier KUNTZ** *October 16, 2015 09:06*

**+Jeff DeMaagd** Good news, I will use Marlin. I add this to the todo list :)


---
**Olivier KUNTZ** *October 16, 2015 09:07*

**+Eirikur Sigbjörnsson** It can bee a solution. May I need steel nozzle to print Carbon fibre?


---
**Olivier KUNTZ** *October 16, 2015 09:08*

**+Ashley Webster** I have nylon but I don't remember witch one... I will see.


---
**Eirikur Sigbjörnsson** *October 16, 2015 09:47*

**+Olivier KUNTZ** Yes you will need a steel nozzle. Preferable the new hardened steel nozzle from E3d. I think they still have an offer of a Carbon reel with hardened steel nozzle included.



[http://e3d-online.com/TCT-Special-Colorfabb-E3D-XT-CF20-and-Hardened-Steel-Nozzle-1.75mmx0.4mm](http://e3d-online.com/TCT-Special-Colorfabb-E3D-XT-CF20-and-Hardened-Steel-Nozzle-1.75mmx0.4mm)


---
**Olivier KUNTZ** *October 16, 2015 12:33*

**+Eirikur Sigbjörnsson** Interesting but a bit expensive.


---
**Eirikur Sigbjörnsson** *October 16, 2015 12:37*

Yep XT-CF20 is not a cheap filament, but then again special filaments are all expensive


---
*Imported from [Google+](https://plus.google.com/+OlivierKUNTZ/posts/dRgtC1ZDTu7) &mdash; content and formatting may not be reliable*
