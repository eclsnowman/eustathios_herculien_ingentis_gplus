---
layout: post
title: "One more update to my weekend maintenance was more a cosmetic issue"
date: January 24, 2016 11:43
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
One more update to my weekend maintenance was more a cosmetic issue. I had already cut a fitting front plate for my display when the printer was finished but quickyl exchanged electronics (and display) for a new one. The display then sit there for months fixed with some tape. Yesterday i finally managed to cut a new front plate and an acrylic display cover. I'm quite satisfied with the result - what do you think?



For those interested: I used 4mm Dibond/Firstbond for the covers. It's normally used for advertising signs as it can be printed on. It has a plastic core and a painted aluminum front and back cover. It's quite tough/stable, has a nice finish and it's good to work with (drill, mill, saw)

![images/6d19d683ec2180720fad115dd18007d2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6d19d683ec2180720fad115dd18007d2.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**Jake Shi** *January 24, 2016 11:58*

Looks great! I'm planning to fit some acrylic boards too. One question: will the boards increase run noise? I mean there are more stuff that are prone to vibrate now.


---
**Michaël Memeteau** *January 24, 2016 13:11*

Not related to your improvement (which looks just fine by the way), but curious to know if your bed is supported by 3 or 4 adjustable screws?


---
**Frank “Helmi” Helmschrott** *January 24, 2016 13:15*

**+Michaël Memeteau** 3 of course:-) 


---
**Frank “Helmi” Helmschrott** *January 24, 2016 13:17*

**+Shengjun Shi** no vibration. You need to fix it well of course. Should have screws wherever the boards contact the frame to not give them room to vibrate against the frame. 


---
**Eric Lien** *January 24, 2016 15:07*

**+Frank Helmschrott** I am torn. On HercuLien I actually get better repeatability on bed level with 4 point level than I did when I had 3point. I would see it because I printed a lot of very large parts for work at the time that used a good portion of the bed. I think it is because on really large beds the two cantilevered corners stick out so far.


---
**Michaël Memeteau** *January 25, 2016 07:49*

**+Eric Lien**​ What's your bed thickness? Maybe a central point would help avoid deflection?


---
**Frank “Helmi” Helmschrott** *January 25, 2016 08:52*

**+Eric Lien** What exactly do you mean by repeatability in this case. Are you having problems with the bed staying in position with 3 screws? I did have quite a lot trouble with 4 screws on stiff beds in the past and found 3 screws to be much more comfy to setup so i did that on the Eusthatios too though it was the first bed of that size for me. Works well so far allthough i have to revise the possibility of adding some glass as the aluminum doesn't stay perfectly flat on the long run.


---
**Jake Shi** *January 25, 2016 09:41*

How about grid auto-leveling? I can see it's helpful for large print bed like this.


---
**Eric Lien** *January 25, 2016 13:19*

**+Michaël Memeteau** 1/4" thick


---
**Eric Lien** *January 25, 2016 13:42*

**+Frank Helmschrott** yes, the consistency of the bed returning to the same position between prints was much worse for me with 3-point level than it is now with 4-point.


---
**Daniel F** *January 25, 2016 17:54*

Nice cover. Is this display a reprap12864 (the one you can get cheap from ali)? If so, how did you connect it to the RADDS (if I remember right, you mentioned that you used one of those). I use a RADDS but I don't like the 2 line display they sell with it.


---
**Frank “Helmi” Helmschrott** *January 25, 2016 17:58*

The display is actually the one made for RADDS from Sparklab. They sell it here [http://sparklab-shop.de/elektronik/29/sparklcd-grafik-display-fuer-radds-board](http://sparklab-shop.de/elektronik/29/sparklcd-grafik-display-fuer-radds-board)


---
**Daniel F** *January 25, 2016 21:02*

Thanks for the hint, I might order the sparklcd or try to connect a cheap 10$ clone reprap GLCD like described here: [https://github.com/luc-github/DUE-RADDS-GLCD/blob/master/README.md](https://github.com/luc-github/DUE-RADDS-GLCD/blob/master/README.md). Advantage of the spark display is that you can choose it from the online configurator and don't need to mess with pin definition files...


---
**Frank “Helmi” Helmschrott** *January 25, 2016 21:19*

and additionally it runs from the extension port and not from the rather sensitive/in stable LCD port. I also have the default display which definitely was problematic from the beginning (not only for me) in terms of stability. 


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/FcB9bwuhVTN) &mdash; content and formatting may not be reliable*
