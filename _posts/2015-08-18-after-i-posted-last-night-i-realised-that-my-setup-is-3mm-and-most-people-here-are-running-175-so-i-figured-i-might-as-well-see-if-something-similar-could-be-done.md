---
layout: post
title: "after i posted last night i realised that my setup is 3mm and most people here are running 1.75, so i figured i might as well see if something similar could be done"
date: August 18, 2015 20:18
category: "Show and Tell"
author: Jeff Heath
---
after i posted last night i realised that my setup is 3mm and most people here are running 1.75, so i figured i might as well see if something similar could be done.  it works... i center drilled a 4mm bolt, heated the end of the tube, and screwed it in.  its not perfect, as my bit angled a bit while drilling.  ideally i would probably machine an aluminum adapter and thread the tube onto that, but for proof of concept it worked pretty well.

![images/6180618c2f50b562de33fc420526d0b4.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6180618c2f50b562de33fc420526d0b4.jpeg)



**Jeff Heath**

---


---
*Imported from [Google+](https://plus.google.com/105415486539145588112/posts/XQ1FBbVAWpe) &mdash; content and formatting may not be reliable*
