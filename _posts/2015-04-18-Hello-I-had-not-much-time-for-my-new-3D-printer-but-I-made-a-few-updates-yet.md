---
layout: post
title: "Hello. I had not much time for my new 3D printer, but I made a few updates yet"
date: April 18, 2015 18:34
category: "Show and Tell"
author: Ivans Nabereznihs
---
Hello. I had not much time for my new 3D printer, but I made a few updates yet. Printing volume is 330x330x390 (I have 405mm lead screws).My printer have Azteeg x5 mini, E3D volcano. The XY printer's mechanics  are more stable and shake not so much now.  I'm experimenting with double Wade extruder and have a question Bond Tech extruder is good enough?


**Video content missing for image https://lh3.googleusercontent.com/-zTiKvmuL0uo/VTKf20jmd8I/AAAAAAAAAHk/VuCn2BUBar0/s0/min.mp4.gif**
![images/ded55af55350fafc1b23675a45ea8b13.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ded55af55350fafc1b23675a45ea8b13.gif)



**Ivans Nabereznihs**

---
---
**Mutley3D** *April 18, 2015 18:37*

Flex3drive would rock :-)


---
**Martin Bondéus** *April 19, 2015 05:36*

If you want problem free extrusion without slipping, grinding and with a huge force the answer is yes **+Ivans Nabereznihs**.


---
**hon po** *April 19, 2015 13:32*

Love the quiet movement. There's a high pitch after the pump turned on. Is it due to air exiting the hood?


---
**Ivans Nabereznihs** *April 19, 2015 15:05*

Yes, I got an air pump that is not so quiet. A new one is on its way to me. All stepper motors on my printer attached by rubber damper and they do not sound so high. Quietness and speed are important parameter for me.


---
**Mutley3D** *April 20, 2015 15:20*

**+Ivans Nabereznihs** If you want speed, increased control resolution with precise retraction control in a direct drive extruder head weighing in at 70 grammes, Flex3Drive might be worth a look into :) Youll certainly be able to stretch the limits of your machine with it. Nice build there too.


---
**Ivans Nabereznihs** *April 20, 2015 18:01*

Yeah, but maybe it is possible to combine BondTech and Flex3Drive  extruders and it becomes a monster extruder :-)


---
**Mutley3D** *April 20, 2015 18:15*

A Flex3BondusRex  or a BondusFex3Rex ?? ;p


---
*Imported from [Google+](https://plus.google.com/118257495094694997465/posts/DbpEYydrBYp) &mdash; content and formatting may not be reliable*
