---
layout: post
title: "My printer finished it's longest print so to celebrate i printed a .5 scale of the gear bearing"
date: September 17, 2014 08:41
category: "Show and Tell"
author: Jim Squirrel
---
My printer finished it's longest print so to celebrate i printed a .5 scale of the gear bearing.  

![images/a9b83dcda456481ed5b07f07ea94576a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a9b83dcda456481ed5b07f07ea94576a.jpeg)



**Jim Squirrel**

---
---
**Mike Miller** *October 15, 2014 11:28*

Did you have to do any carving to get it to turn? I printed an AWESOME one last night, and am pretty sure it's 97% non-interfering, but I can't break the gears loose where they're glued together at the base. 


---
**Jim Squirrel** *October 15, 2014 13:12*

Nope just grabbed center gear and twist. It wasn't as nice as the one **+Eric Lien** posted but it moved 




---
**Eric Lien** *October 15, 2014 15:27*

To be honest I cheated a little. I upped my retraction to eliminate any blobs. I also set the extrusion multiplier to .95 to minimize the chance of over extrusion bonding the surfaces. Last bit of advice is push on the center hub sideways to crack the surfaces loose before trying to spin. This deflects the surfaces and breaks things free.


---
**Mike Miller** *October 15, 2014 15:36*

I'm carving on it with an Xacto. I'm not ready to call it a failed print yet!


---
*Imported from [Google+](https://plus.google.com/102862083035944525354/posts/17eQKDgQXQV) &mdash; content and formatting may not be reliable*
