---
layout: post
title: "I finally got my first print in ABS on my Eustathios Spider V2"
date: December 05, 2015 22:47
category: "Show and Tell"
author: Ted Huntington
---
I finally got my first print in ABS on my Eustathios Spider V2. It took a little while to get all the linear rails perfectly aligned and the carriage moving with very little friction. I like this printer much more than the moving bed designs of the RepRaps and I think this design is a great starting point to branch from for all my future 3D printers.



I modeled 2 parts that others might find useful:

I could not get the Z-stop exact and so made an adjustable z-stop here: [https://www.youmagine.com/designs/eustathios-spider-v2-adjustable-z-stop](https://www.youmagine.com/designs/eustathios-spider-v2-adjustable-z-stop)  which works pretty well.

in addition, I wanted to experiment with more corner brackets but didn't have any more handy so modeled one here:

[https://www.youmagine.com/designs/2020-aluminum-extrusion-corner-bracket](https://www.youmagine.com/designs/2020-aluminum-extrusion-corner-bracket)





There are a lot of pluses to this Eustathios Spider V2 design:

pluses:

1) simple design with basic parts- nothing that requires precision printing

2) extruded aluminum is easy to work with

  a) belt tightness can easily be adjusted on Z belt

  b) if holes in aluminum plate are not perfectly aligned (read: you drilled the 4 holes in the aluminum plate with a hand drill), it is very easy to slide the 4 bed spring screw supports to perfectly align (at least one dimension) with the misaligned screw holes.

  c) very easy to attach limit switches/end stops

  d) sliding on/off extruder, and most other parts fastened to the extrusion is very fast and easy because it only requires loosening 2-3 screws and sliding the part off

e) the Eustathios can be turned completely upside down with no problem- the only possible obstruction is if a filament spool is attached and that can be easily removed.



Other build notes

+it's a good idea to determine how many nuts will need to be in each extrusion before attaching it to the frame. Because otherwise you need to keep removing extrusion to add nuts.

+the Z pulley could easily be 3D printed (probably in two interlocking parts)

+I found that even after tightening the 2 screws that hold the middle extrusion of the "I" or "H" shaped frame that holds the bed, the two legs of the "H" shape tended to loosen the middle extrusion. But if it is not moved much it seemed to hold. Some people recommended using Loctite.

+I used ballscrews with Walter's adapted parts

+hercustruder uses 2 4mm nuts,  found this was easy without needing a m4 die- only using the little m4-inside-threaded part used to thread the outside of the Bowden tube for the RepRapPro Mendel- note I don't think that a M4 nut is on BOM

+I found that the one of the 10mm smooth rods (linear rails) I bought from CNCAutomation: [http://www.aliexpress.com/snapshot/6839868337.html?orderId=68650359435803](http://www.aliexpress.com/snapshot/6839868337.html?orderId=68650359435803), although it measures at exactly 10mm diameter with a calliper, was just a fraction of a mm too thick and would not allow the 10mm bearing to slide down it. So I had to turn part of the end down on my mini lathe- which made the end pretty rough- I found that the sheave would wobble if both set screws were tightened- but turned mostly in a nice circle by only screwing in a single set screw.

+my two 458mm smooth rods (which I also bought from CNCAutomation) were 460mm and so I needed to trim off a few mm from one end with an abrasive saw.

+the 2 m8 rods were 483mm instead of 480mm and had to be cut

+I should say that to CNCAutomation's credit their prices are low, the ball screws are fine including machining- with the m10 rod they said they will send another if I want- it's probably better to just cut your own smooth rods because they tend to add a few mm - perhaps thinking that it's better to be a little over than under.

+I used some acetone to insert the bushings, but it took a minute to figure out that you need to really work in the acetone for about 30 seconds before the bushings will fit without too much force- and no vice is needed. My ABS carriage top rail cracked a little when I put in the bushings- so I probably needed to use the acetone a little more.

+I did run a smooth rod in the drill for the bushing pairs and found that the connector with the bushing pairs did, after a few minutes, seem to slide more easily up and down the rod.

+I made a really stupid mistake in not initially screwing in the upper-middle and bottom threaded holes- just the corner brakets- as a result the Eusthatios frame kept falling into a rhombus shape.

+many parts did not need the holes to be re-drilled through- apparently the holes were perhaps made a little larger for convenience where precision is not necessary- it made assembly a little faster- and convinced me of the value in modeling of making any holes 0.2-0.5mm larger where precision doesn't matter.

+I had to file down a few mm off the top of an X axis belt tensioner so that it could slide under and close the X limit switch- but possibly my rails were not entirely aligned in that early stage.

+fastening and tightening the belts is a little tough because of the tight fit- especially where the second upper extruded bar is.

+In each tensioner it helps to cut away excess plastic in the space that holds the belt with an exacto knife, and use a needlenose pliers tip to push the belt into the middle space. It's useful to explain the tensioner design, which is a brilliant design: the belt can be attached with a little slack, because the two side screws with attached nuts are then used to adjust the tension of (one side of) the belt after the belt has been fastened to the two tensioner parts. So there should always be a space between the two parts that hold each end of the belt <s>so that the belt can be easily tighened more in the future by just tightening those two horizontal screws.</s>

<s>+I decided to make my bearing holders solid filled because at 40% infill the belt tension broke two of them</s> but then I was experimenting and probably the tension was too tight then.

+the carriage cooling duct screw tabs are easily broken so don't fasten the m3 screws too tightly.

+I found that all screws could be exactly replaced by regular phillips pan head screws- the one exception is the 4 M5 screws used to hold the M8 smooth rods in the belt tensioners- a m5x18mm pan head probably will be ok (I didn't try) (a 20mm is just a little too long), a m5x20mm countersunk works perfectly- which is what I used.

+one possible improvement to the belt tensioners is to find a way to run the hole for the M8 carriage smooth rods all the way through instead of just part of the way. Some reasons being: 1) ease in inserting and removing smooth rods, and 2) in the event, as was the case for me, when a smooth rod is a few mm too long, the rod will not have to be cut shorter, and 3) the clamp holds the rod just as firmly as when the hole is not all the way through the belt tensioner. Currently I think the belt is in the way there- so it's probably not an easy adjustment- but perhaps extending it a little might be.

+one question is- do belt tensioner A and B make a lot of difference- can't just part A be used for all 4? it simply means that the tensioning part on the back switches sides with the tensioner end.



I starting putting together a basic sketch of assembly instructions and feel free to copy and change as you all feel necessary. It would be great to have photos for each step:





Assembly Instructions:

1) connect the frame

  note that all threaded holes need to be used- leave no threaded hole unfilled.

  a) connect 4 425mm extrusion pieces horizontally (forming the bottom square) to the 4 562mm vertical legs

  put in 2 nuts for the brackets on each leg before connecting, and then connect each using the bottom threaded holes with 2 M5x10mm (or M5x12mm). The screw head slides in the slot into the threaded hole and is tightened with a screw driver through the hole in the extrusion.

     1) add 3 nuts on the bottom of 2 opposite 425mm extrusions for the z axis, and 2 more for the Z motor before connecting them to the vertical extrusion pieces.

  b) connect the corner brackets vertically using M5x10mm screws

  c) screw on the two upper side 425mm extrusion pieces adding three nuts on the inside for the Z axis and Z limit switch, and another two on top of one for the Y limit switch. Use 4 M5x10mm (or x12mm) for the threaded holes

  d) connect 4 corner brackets vertically under the 2 425mm extrusion pieces using 8 M5x10mm screws. ***it is very important that these two upper middle extrusion pieces are at the same height. For example the measurement from top horizontal extrusion to upper middle extrusion should be around 112mm- whatever it is, it should be the same in each of the 4 corners- because the bearings holding the perimeter smooth rods are fastened directly above these extrusions and even 1 mm of difference in height will cause a lot of friction in the carriage bushings.

  e) screw on the top 4 425mm extrusion pieces using 8 m5x10mm (or 12mm) screws.

  f) attach 4 corner brackets horizontally connecting the top 4 425mm to the 562mm vertical extrusion pieces using 8 M3x10mm screws.

2) Connect the Z axis

3) Connect the XY axes

  a) add 2 M10 bushings into each belt tensioner, if printed in ABS rub acetone inside for about 30 seconds applying pressure until the bushing tightly slides in, you can use a strong thumb push - or a vice - but be careful with a vice because it may crack the part. If PLA possibly a hot air gun or blow dryer may work to soften the inside enough to fit the bushing

  b) turn a M10 smooth rod through each belt tensioner running the tensioner up and down the rod until it moves very freely perhaps a minute or two.

  c) add the four M8 bushings to the carriage using the same above technique

  d) Push 8 M10 bearings into the 8 bearing holders. Attach the 4 perimeter smooth rods to the frame using the 8 bearing holders (the 2 with holes are for the motor spindles): The motor rods have an outer sheave at the motor end for the motor belt, an inner sheave for the perimeter belt, the belt tensioner (which connects to the carriage rod), another sheave on the inside far end for the perimeter belt on the other side- again the bearing holders hold the bearings. It is important to make sure that the bearing holders are pressed all the way against the upper or lower extrusion, because if they are not aligned it will cause friction in the carriage.

  f) attach the belts: you need to cut the perimeter belts to the correct size. Cut a piece of GT2 belt that will be a little too long. Attach the tensioner end part (in the correct orientation) with 2 horizontal M3x35mm screws and just put on 2 M3 lock nuts. You can then attach one cut end of the belt to the tensioner tightening it with an M3x25mm (it should just be long enough for the lock nut) vertical screw and lock nut, run the belt through the two sheaves on the end and back to the tensioner. On one side the unbroken side of the belt runs above the tensioners opposite sides, on the adjacent side the unbroken side of the belt runs under the two tensioners on opposite sides. Align the two opposing tensioners to be positioned in exactly the same position- there is a tool ([http://www.thingiverse.com/thing:610215](http://www.thingiverse.com/thing:610215)) that can be used to make sure the spacing is correct. (I find a good method is to align the two opposing tensioner as best as possible, and then just before connecting the second end of the belt run the carriage back and forth to find the least resistance, then use the needle-nose pliers to insert the belt into the tensioner- there is usually enough friction to hold the belt in place while you insert the vertical M3 screw and tighten the belt clamp.) Connect the other end of the belt to the tensioner end and tighten with a vertical M3 screw and lock nut. You will most likely need to use an exacto knife to cut the crack in the tensioner the belt goes into. Make sure to use a needle nose pliers to push the belt to the farthest back in the tensioner (and aligned with the rest of the belt) otherwise when the belt is near the sheave there will be a lot of friction. Make sure there is no slack on either side of the belt, but you can tighten one side of the belt by screwing in the 2 horizontal screws in the tensioner. Tighten the belt if necessary by screwing in the 2 horizontal screws- but the tension should not be very tight- because the two opposing tensioners cannot be exactly aligned (stretching and sliding in the belt can also cause the rods to become a little unaligned) the M8 smooth rod of the carriage needs to be able to move a little, otherwise it will cause too much friction in the carriage bushings for the stepper motors to move.

  e) attach the carriage

4) connect the X and Y motors and add a small circular belt to each, adjust the belt tension by sliding the motor down the vertical leg.

4) Connect the extruder, and spool holder

5) Connect the electronics

  a) determine where your power supply will be mounted

  b) wire the limit switches



Problem: Too much friction- stepper motor can't move carriage

Possible solutions: When moving the carriage manually, it has to be very easy to move, or else the stepper motors will not be able to move it.

  a) Make sure that the two opposing tensioners are exactly aligned- there is a tool ([http://www.thingiverse.com/thing:610215](http://www.thingiverse.com/thing:610215)) that can be used to make sure the spacing is correct.

  b) disconnect the stepper motors from the electronics (because moving the motor spindle manually causes a voltage that could damage the electronics) and move the carriage manually in long strokes, in a big square, in a big circle, etc. Try pushing down and pulling up gently just to push the bushings into place. You may eventually feel that the carriage becomes very easy to move.

  c) make sure that all bearing holders are at the same height- I found that even 2mm off caused a significant amount of friction for the carriage bushings.

  d) make sure that the belt is aligned with each sheave.

  e) make sure belts are not too tight (and not too loose- but too tight is a problem I found - loosening the belts made motion a little easier)

  f) make sure the sheaves are turning symmetrically without any wobble, if you see a sheave with wobble, try tightening only 1 set screw and see if that solves the wobbling.

  g) make sure the linear rails are not too long and so are rubbing against the bearing holder back.


**Video content missing for image https://lh3.googleusercontent.com/-3zVH9_bBmwo/VmNi0haZMxI/AAAAAAAAAYo/JBDvj4sNCSw/s0/VID_20151205_115730.mp4.gif**
![images/46866be2762a05d0f6a3d2aca481eb2e.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/46866be2762a05d0f6a3d2aca481eb2e.gif)



**Ted Huntington**

---
---
**Ted Huntington** *December 05, 2015 22:51*

I want to thank everybody too for the amazing open-source design and for answering all my questions about the assembly.


---
**Eric Lien** *December 05, 2015 22:59*

**+Ted Huntington** holy post :) I think it will take a few reads to absorb it all. But great work. I love to see new printers running. It's like having another member added to the family.



Congratulations. Now let's see all the cool things you can make with the printer running :)


---
**Ted Huntington** *December 06, 2015 00:21*

I built up a lot of notes over the course of the build and I just concluded that I would dump them all out and share them once I finally finished! :) This is a great printer- I can't say enough about how amazing it is - any doubts I had have been erased- I couldn't believe a single belt could turn two lead screws- but yes it definitely can and does a great job at it- just a great design - so awesome to be open source- and I am looking forward to many more printings!


---
**Eric Lien** *December 06, 2015 00:31*

**+Ted Huntington** I like to give back to the community where I can. If it wasn't for the open source's of **+Tim Rastall**​, **+Jason Smith**​ and many others I could have never done what I have so far. That is the greatest strength of our movement IMHO. No prideful "mine, mine, mine" mentality. Just a bunch great individuals looking to raise all boats with their contribution to the rising tide.


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/c7t6cRUP2i8) &mdash; content and formatting may not be reliable*
