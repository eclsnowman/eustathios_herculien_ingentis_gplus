---
layout: post
title: "I am about to make an order for heated bed silicone from alirubber"
date: February 16, 2015 16:43
category: "Discussion"
author: Dat Chu
---
I am about to make an order for heated bed silicone from alirubber. The shipping cost is ~$35 to the US. Does anyone else (especially those in Houston, TX) wanting to do a group buy to save on the shipping cost? 



If you are not in Houston, the extra shipping cost via USPS to your place can be checked at [http://postcalc.usps.com/](http://postcalc.usps.com/)



Here is what I am ordering for myself. It costs $34



380x380

120v

1000w

Thermistor in center leads coming from back side off center 30mm. Please see drawing

1.5m long

Adhesive back



[https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Silicone%20Heater%20Thermistor%20and%20Cable%20Locations%20for%20Alirubber.jpg](https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Silicone%20Heater%20Thermistor%20and%20Cable%20Locations%20for%20Alirubber.jpg)





**Dat Chu**

---
---
**Matt Miller** *February 16, 2015 16:57*

What specs are you sending?


---
**Dat Chu** *February 16, 2015 16:58*

380x380

120v

800W

Thermistor in center leads coming from back side off center 30mm.



[https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Silicone%20Heater%20Thermistor%20and%20Cable%20Locations%20for%20Alirubber.jpg](https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Silicone%20Heater%20Thermistor%20and%20Cable%20Locations%20for%20Alirubber.jpg)


---
**Matt Miller** *February 16, 2015 17:02*

did you get a price yet?



Why not 1000W?


---
**Mike Miller** *February 16, 2015 17:04*

I don't need one, but I feel for ya...that lead time to shipping is a killer. 


---
**Dat Chu** *February 16, 2015 17:05*

Not sure why not. This is my first build it yourself printer so I try to stick to the recommended build. The price is 34 plus shipping plus PayPal fee of $2.5



**+Matt Miller** do you recommend a 1000W? I am not sure what I gain/lose.


---
**Seth Messer** *February 16, 2015 18:14*

i'd love to jump on this, but i'm doing a Eustathios (V2 preferably, so i'm just sort of waiting for final BOM as Eric gets time with everything else he's doing). Too bad you can't cut them down in size yourself (or maybe you can?)


---
**Matt Miller** *February 16, 2015 18:24*

You're gaining overhead.  It's more efficient to run a 1000W heated bed @ 100C than a 800W one at 100C - you'll pull fewer amps with the larger wattage bed to hit the same target temp.  As **+Eric Lien** noted, you'll have to turn down PIDMAX to something appropriate for tuning, but you'll probably have to do that anyway.



Also, to further clarify 1000W for a 14" square bed isn't really high-power.  My 10" square heat sheet pumps out 1000W, so that's 10W/in^2.  1000W over 196in^2 is appx 5W/in^2.  But for literally half the price ([http://www.mcmaster.com/#35765k514/=vxq9kn](http://www.mcmaster.com/#35765k514/=vxq9kn)) it's a good deal.  I'm in.  


---
**Eric Lien** *February 16, 2015 19:14*

**+Seth Messer** & **+Dat Chu**​ they don't have to be the same size. You could order any number of configurations at the same time.


---
**Dat Chu** *February 16, 2015 19:19*

Ok 1000w it is. Yeah any configuration would work. 


---
**Seth Messer** *February 16, 2015 19:22*

sign me up then! i guess i'd do a 300x300x270mm for the #Eustathios. you guys get all giddy when you make part purchases? heh.. one small step closer. **+Dat Chu** were you going to handle all the purchases and shipping from your house to the folks that want to join in for the original purchase? thanks for doing this btw.


---
**Matt Miller** *February 16, 2015 19:28*

I don't need the thermistors, the rest is perfect.  Are these adhesive-backed?  


---
**Dat Chu** *February 16, 2015 20:52*

I believe so right **+Eric Lien** ?


---
**Eric Lien** *February 16, 2015 21:08*

**+Dat Chu** so long as you tell them to make it adhesive backed, then they will be.


---
**Matt Miller** *February 16, 2015 21:14*

Damn!  What other options are there?  Can we specify the lead size (AWG)?


---
**Dat Chu** *February 16, 2015 21:19*

**+Seth Messer**​ sure but you will need to pay for the shipping cost from my place 77002 to yours, whatever is on the usps site. My time is free for the community. +Matt Miller, I guess so. Perhaps someone who has ordered from them before can chime in. ​


---
**Eric Lien** *February 16, 2015 21:36*

You can, as well as the length. I told them 1.5meters long, and there was no up charge. 


---
**Gus Montoya** *February 17, 2015 01:24*

I am building Lien3D Eustathios Spider. I'm not sure if I will require one. Anybody has thoughts? Would it be a good upgrade?


---
**Derek Schuetz** *February 17, 2015 01:53*

**+Matt Miller** **+Eric Lien** is that McMaster heating pad suffices enough to heat a herculien bed?


---
**Dat Chu** *February 17, 2015 03:25*

Ok, updated. The 1000W version will be the same cost. **+Matt Miller** and **+Seth Messer** can you guys PM me the sizing you want by the end of the week? Chinese New Year means they won't work this week at all even if we send in an order early.



Here is the format:

380x380

120v

1000w

Thermistor in center leads coming from back side off center 30mm. Please see drawing

1.5m long

Adhesive back



[https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Silicone%20Heater%20Thermistor%20and%20Cable%20Locations%20for%20Alirubber.jpg](https://github.com/eclsnowman/HercuLien/blob/master/Drawings/Silicone%20Heater%20Thermistor%20and%20Cable%20Locations%20for%20Alirubber.jpg)



Make sure you specify the AWG if you want something specific.


---
**Seth Messer** *February 17, 2015 04:27*

thanks **+Dat Chu** .. let me see if **+Eric Lien** will verify thermistor lead location, and voltage he went with for the Eustathios V2 that he's been working on. I'll have you definitive response for my order as soon as I get confirmation on matching specs for his V2 build. again, thanks for organizing this little group buy.


---
**Eric Lien** *February 17, 2015 13:03*

Seth. My current Eustathios bed is 320x320 , 400w, 24v. This requires a minimum of 500W 24v PSU. I would go 500w or 550w 120v AC through an SSR. Then you can use a smaller power supply for the board and steppers.


---
**Seth Messer** *February 17, 2015 13:29*

thanks **+Eric Lien** and add ed to my notes this time.


---
**Daniel Salinas** *February 17, 2015 21:35*

**+Dat Chu** FYI Alirubber is out of the office until Feb 26.  I placed my order on the 10th and was informed they have their spring festival holiday from Feb 11 - 25.  So I'm waiting :'(


---
**Dat Chu** *February 17, 2015 21:40*

:-) Thanks Dan. I am in no rush since I am celebrating Chinese New Year as well. I was surprised they responded to my price quote. I got **+Matt Miller** specs. Just waiting until the end of the week to see if anyone else wants to go in.


---
**Derek Schuetz** *February 17, 2015 22:57*

Ya last I heard was they did not receive my payment and then an ok picture


---
**Seth Messer** *February 18, 2015 00:05*

**+Dat Chu** how do you have your power supply setup will you be using to handle the 1000w version?


---
**Dat Chu** *February 18, 2015 00:07*

I use a ssr and plug directly to the wall since the rubber is specced for 120v. The psu is just any 24v psu from eBay. 


---
**Seth Messer** *February 18, 2015 00:09*

**+Dat Chu** man that was quick reply. :) thanks. ok put me down for 1000w, 320x320, with the 1.5m cable, i'm assuming, too, that we'll want the adhesive backed version? and thermistor position like the one **+Eric Lien** has in his HeruLien drawing/link.



i'll be picking up a similar 24v PSU and SSR setup as well.


---
**Eric Lien** *February 18, 2015 00:21*

**+Dat Chu** I would run it through a fuse first, not direct to the wall. I use this: [www.amazon.com/dp/B0050HH70E/ref=cm_sw_r_awd_KN94ub03KVENK](http://www.amazon.com/dp/B0050HH70E/ref=cm_sw_r_awd_KN94ub03KVENK)



Down stream of the fuse/switch I go to my power supply, then daisy chain on the terminals on the power supply over to the SSR and bed. I will try to make a wiring diagram later.


---
**Eric Lien** *February 18, 2015 00:36*

**+Seth Messer** I run a 400 watt on my Eustathios and it runs great. 1000 will be like putting in a finishing nail with a sledge hammer. I run my HercuLien on 800W and the bed is almost 17x17" and 1/4" thick. Even then I had to limit max output in firmware not to overshoot so bad that pid tuning would fail. I would say 600W absolute max for Eustathios and even that is overkill. Otherwise you chance warping the 1/8" heat spreader Eustathios uses by the thermal shock when that many Watts hit one side of thin plate.


---
**Seth Messer** *February 18, 2015 00:39*

**+Eric Lien** sounds good. how long does it take, at present, on your Eustathios to heat the bed to your target temperature usually?


---
**Seth Messer** *February 18, 2015 00:45*

**+Dat Chu** 

I'm changing my order is as follows:

320x320

550w

120v

1.5m long

Adhesive back

Thermistor in center leads coming from back side off center 30mm.


---
**Eric Lien** *February 18, 2015 00:50*

3.5min to reach equilibrium for PLA at 70, 6.5min to reach equilibrium for ABS at 110. Heating is all about balance. Too small and it has to be on 100% of the time to maintain heat. Too much and you will have trouble pid tuning because deltaT is so fast that maintaining an even temp becomes impossible. Thermal expansion with large temp swings will change your bed to nozzle distance. This shows itself as z ribbing.﻿


---
**Eric Lien** *February 18, 2015 00:54*

That is why I reset Z on my printer when I change from PLA to ABS. If I go from PLA to ABS without resetting Z there will be way to much squish and the nozzle drags around plastic. Conversely if I go from ABS to PLA the PLA will lift because it is just falling onto the bed.﻿ that's how much differences 40deg makes.



Allowing the system to reach equilibrium is equally critical. Otherwise things will still tend to have a crown. The center will be higher because the sides and top are still cooler than the bottom where the heater is located.


---
**Eric Lien** *February 18, 2015 00:59*

**+Seth Messer** like I said earlier 500w would be fine, it may even help abs temps, even 550W. But 1000 would be difficult (in my opinion) to get balanced.


---
**Seth Messer** *February 18, 2015 01:03*

Right on, i switched it to 550w then. Either way, the cost isn't awful, so if it ends up that 550 is too high, etc then i can always just re-order.


---
**Eric Lien** *February 18, 2015 01:12*

**+Seth Messer** if its too much just turn down max output in Marlin: #define MAX_BED_POWER 255 // limits duty cycle to bed; 255=full current



// This sets the max power delivered to the bed, and replaces the HEATER_BED_DUTY_CYCLE_DIVIDER option.



// all forms of bed control obey this (PID, bang-bang, bang-bang with hysteresis)



// setting this to anything other than 255 enables a form of PWM to the bed just like HEATER_BED_DUTY_CYCLE_DIVIDER did,



// so you shouldn't use it unless you are OK with PWM on your bed. (see the comment on enabling PIDTEMPBED)



I assume Smoothie has something similar. Once my azteeg x5 mini is operating I guess I will find out where that is (since I plan on testing it on Eustathios as well as HercuLien)


---
**Gus Montoya** *February 19, 2015 00:05*

**+Dat Chu**  Is it possible to wait a bit longer to place my order, so that **+Eric Lien**  can post his documentation for his Eutathios V2 build? I only have the main frame on order and waiting on everything else. 


---
**Eric Lien** *February 19, 2015 00:12*

**+Gus Montoya**​ I would just go 500W or 550W 120v 320x320 like **+Seth Messer**​​. That is plenty, but you can always dial it back via firmware if it is too much.﻿


---
**Dat Chu** *February 19, 2015 00:45*

Sure **+Gus Montoya**​, the seller won't get back until next week


---
**Gus Montoya** *February 19, 2015 04:44*

Thanks **+Eric Lien** , ok **+Dat Chu** you heard the man LOL Thanks to both of you for the hard work and contribution. I would have to pony up some serious cash for an assembled printer at half the capability. 


---
**Jeff DeMaagd** *February 25, 2015 02:05*

Has anyone heard from Ali Rubber since Chinese New Year? When do they open again? Or should I message again?


---
**Dat Chu** *February 25, 2015 02:17*

They are not open yet. Haven't heard from them. Expect to get something no later than next week.


---
**Dat Chu** *March 02, 2015 16:22*

Last call. Putting in order monday or Tuesday and I will send the final order to the seller on Wednesday. 


---
**Gus Montoya** *March 02, 2015 22:05*

**+Dat Chu** I'm on the list correct? Also a coworker wants in, what contact info can I give him?


---
**Dat Chu** *March 02, 2015 22:14*

Yes you are on the list. 


---
**Mark Runnels** *March 18, 2015 20:22*

Did you guys have any luck getting a group order in for Alirubber? I requested a quote last week from them and haven't heard back from them yet.


---
**Dat Chu** *March 18, 2015 20:50*

I have submitted the order. It is enroute (probably) :)


---
**Seth Messer** *March 18, 2015 21:09*

yay. thanks again **+Dat Chu** for heading this lil group guy up.


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/T1M5PQExGS1) &mdash; content and formatting may not be reliable*
