---
layout: post
title: "First calibration print had me adjust flow to 0.97, then printed this off"
date: November 02, 2015 21:35
category: "Deviations from Norm"
author: Matt Miller
---
First calibration print had me adjust flow to 0.97, then printed this off.  Really happy.  Minor stringing to be tuned out.  Layers show some contraction at the base.  Maybe too much heat on the bed.  210C at the nozzle and 60C on the glass.  



Not sure about the general layer inconsistencies.  Fist time using hatchbox filament...maybe diameter issues.



Still working on organizing the repo.  



![images/f00ed1bd99bf7cdbc60ce63385a48206.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f00ed1bd99bf7cdbc60ce63385a48206.jpeg)
![images/45a9eee68aaf004ff3a7ea9eb134c326.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/45a9eee68aaf004ff3a7ea9eb134c326.jpeg)
![images/e6bfc91ed0906595d5f419bfba5ca07b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e6bfc91ed0906595d5f419bfba5ca07b.jpeg)
![images/b96b94c7bd21a62cf3942cbf445488f1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b96b94c7bd21a62cf3942cbf445488f1.jpeg)
![images/36cd57e9db1a25584f98adcababfc896.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/36cd57e9db1a25584f98adcababfc896.jpeg)

**Matt Miller**

---
---
**Eric Lien** *November 02, 2015 23:47*

A quick test could be print a 20x20 cube in spiral vase mode, but stretch it out in Z to 100mm or more. Then do the same in a single wall zero infill print. Compare results. And look for Z ribbing.﻿


---
*Imported from [Google+](https://plus.google.com/+MattMiller_akhlut/posts/4QiAGMG3hBM) &mdash; content and formatting may not be reliable*
