---
layout: post
title: "SMW3D, ROBODIGG, and ENCO deliveries last evening will make for a nice Saturday in the workshop on my HercuLein!"
date: March 21, 2015 14:29
category: "Show and Tell"
author: Bruce Lunde
---
SMW3D, ROBODIGG, and ENCO deliveries last evening will make for a nice Saturday in the workshop on my HercuLein!

![images/51b1ef32129dd71bf25b232d68cd942d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/51b1ef32129dd71bf25b232d68cd942d.jpeg)



**Bruce Lunde**

---
---
**Seth Messer** *March 21, 2015 15:18*

**+Bruce Lunde** what all did you end up picking up from smw3d? all the misumi type stuff I presume?


---
**Eric Lien** *March 21, 2015 16:49*

Look forward to the progress. If you need any pictures let me know.


---
**Gus Montoya** *March 21, 2015 23:17*

Nice, I'm full of envy. I'm stuck at work in the ER with a bunch of crazy people that think their  normal. It scares me how little it takes for a person to "function" on a daily basis.


---
**Bruce Lunde** *March 22, 2015 00:11*

**+Seth Messer** I ordered a bunch from Mitsumi originally (as much as I could afford) now I an getting the rest from SWM3D. I have to admit I did not organize the build in an solid manner, it was more what $$$ I had each time I placed an order.  Now I am combing through the BOM to order the balance. **+Eric Lien** Are there two threaded rods that lift the heated bed? BOM says one, but the pics look like there are two, so it looks like I have to order another?


---
**Bruce Lunde** *March 22, 2015 00:14*

**+Gus Montoya** I hope to get this running over the next few weeks, then maybe I can print your parts! P.S. I work in a hospital too (doing IT) and my users are like crazy people when their machines do not work, lol!


---
**Eric Lien** *March 22, 2015 04:59*

**+Bruce Lunde** there are two if you look closely there is qty two of that sub assembly by the way I structured the BOM.


---
**Gus Montoya** *March 22, 2015 06:25*

Bruce , you know what hospital staff say about themselves. Hospital staff make the worst customer/patient in the hospital. Because thy think they know it all, including wait time.


---
**K Burk** *March 23, 2015 15:45*

looks like a start to something great!


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/BM4d7oYHMKf) &mdash; content and formatting may not be reliable*
