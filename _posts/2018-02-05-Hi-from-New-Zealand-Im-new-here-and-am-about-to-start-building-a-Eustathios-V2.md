---
layout: post
title: "Hi from New Zealand. I'm new here and am about to start building a Eustathios V2"
date: February 05, 2018 22:48
category: "Discussion"
author: Julian Dirks
---
Hi from New Zealand.  I'm new here and am about to start building a Eustathios V2. Have had a Wanhao I3 (plus mods) for a year or so loving that and decided to build one myself for the fun/experience but also to hopefully end up with a better printer.  Have agonised over what to build...corexy hype etc etc but it was the quality of support in this group the clinched it for me.



Got a lot of questions obviously but just thought I would introduce myself here and I'll ask some specific questions in other posts rather than write a novel.



Cheers





**Julian Dirks**

---
---
**Eric Lien** *February 08, 2018 04:25*

Sorry your post just made it out of the Google+ spam filter. 



I look forward to helping you through the build in any way I can. Its a fun journey. Not for the faint of heart... But a worthwhile one once you make it.


---
**Oliver Seiler** *February 08, 2018 04:54*

Hey Julian, I'm Wellington based and had an Eustathios for a while. I got most parts from Aliexpress, happy to dig out the specific shops/parts for you. Give me a shout if you're in the area and want to visit. 


---
**Julian Dirks** *February 08, 2018 09:49*

Thanks Eric- I’m going to need all the help I can get!



And hi Oliver. And thanks for the offer. Will be in touch I’m sure. I’m determined to buy good quality parts where it matters but really expensive here and he freight to get stuff to nz is killing me. Did you buy your t slot in nz? Freight from misumi was more than the t slot....




---
**Oliver Seiler** *February 08, 2018 10:25*

**+Julian Dirks** do you mean the 2020 extrusions? Got mine from AliExpress, cut, drilled and tapered the myself. You have to make sure to make good, clean, 90degree cuts, otherwise your frame is borked, but it's totally doable.

I think I got a couple of 8mm rods from Misumi in the end as one AliExpress vendor kept shipping slightly bent ones. I got my money back and have a few ok-ish spares now, but got tired of waiting and opening disputes... Everthing else I got from AliExpress turned out to be perfect, I did get geniune E3D hotends, BondTech extruder though.




---
**Oliver Seiler** *February 08, 2018 10:27*

Got the heated bed custom made from the local Ulrich aluminium shop.


---
**Dennis P** *February 08, 2018 23:33*

Julian- I am finishing up my build currently. I too have a Di3. It did fine for making parts. As yo uarein NZ, then Aliexpress might be your best choice. I found few other vendors that were a little bit better on price and shipping than RobotDigg- linckcnc, 3D Factory Store and 3D_Home. I make my extrusion cuts on a miter box and drilled and tapped them myself. I have some jigs in my Mods folder on gihhub you might find helpful. 


---
*Imported from [Google+](https://plus.google.com/113795478307151372873/posts/ihtfDtW4srd) &mdash; content and formatting may not be reliable*
