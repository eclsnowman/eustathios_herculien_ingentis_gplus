---
layout: post
title: "Has anyone else issues with opening the Zane Baird mod files for the Herculien?"
date: August 21, 2016 17:29
category: "Discussion"
author: Eirikur Sigbjörnsson
---
Has anyone else issues with opening the **+Zane Baird** mod files for the Herculien? I can open the stl files for the tensioner blocks, and the photos, but the rest of the stl files produce error in Netfabb (invalid stl file) and I also get error on the Solidworks files





**Eirikur Sigbjörnsson**

---
---
**Eric Lien** *August 21, 2016 17:58*

Yeah it's a known issue right now. Zane is going to re upload to the GitHub when he has some time.


---
**Zane Baird** *August 21, 2016 18:14*

I should be able to fix this tonight. I'll post an update when I have the issue resolved 


---
**Eirikur Sigbjörnsson** *August 21, 2016 18:52*

thanks :)


---
*Imported from [Google+](https://plus.google.com/118262882256504121671/posts/9m7AUSVwCHK) &mdash; content and formatting may not be reliable*
