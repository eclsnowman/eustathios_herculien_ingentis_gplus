---
layout: post
title: "Has anyone taken a look at this?"
date: October 13, 2015 16:18
category: "Discussion"
author: Rick Sollie
---
Has anyone taken a look at this? Looks very familiar;)

Just wondering if anyone has done speeds like they are talking?



Is there any advantage to having the z axis done this way? I only saw one motor so I doubt they have it setup for bed levelling via 4 lead screws.





[https://all3dp.com/readybox-promises-worlds-fastest-3d-printer/](https://all3dp.com/readybox-promises-worlds-fastest-3d-printer/)





**Rick Sollie**

---
---
**Dat Chu** *October 13, 2015 16:26*

This has been discussed in other communities. It's not worth the time or the money. Vaporware. 


---
**Eric Lien** *October 13, 2015 16:30*

Three screws is best, and two rods. Three points establish a plane, two points establish a line. Anything more is just unneeded constraints.



I have done 300mm/s on a huge prototype foundry pattern on infill. But for outside surfaces these speeds are unsatisfactory IMHO. Also I wonder what their proprietary high pressure extruder is, my guess is a knockoff BondTech like this is a knockoff Ultimaker. But I may be wrong, time will tell.


---
**ThantiK** *October 13, 2015 17:21*

**+Dat Chu** I'm sure it's not vaporware, they only needed enough to build something like 5-10 machines.


---
**Ishaan Gov** *October 13, 2015 18:46*

I'd still question exclusively using lead screws as Z linear motion guides, as you will get all the Z-wobble artifacts in your prints if your Z screws aren't perfectly straight


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/9kCF6PAhinm) &mdash; content and formatting may not be reliable*
