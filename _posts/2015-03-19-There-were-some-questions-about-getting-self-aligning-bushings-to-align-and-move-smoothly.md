---
layout: post
title: "There were some questions about getting self-aligning bushings to align and move smoothly"
date: March 19, 2015 14:28
category: "Discussion"
author: Jason Smith (Birds Of Paradise FPV)
---
There were some questions about getting self-aligning bushings to align and move smoothly. Dug up this classic post by **+Tim Rastall** explaining everything you need to know. 



<b>Originally shared by Tim Rastall</b>



Video 1: Getting Self aligning bronze bushings to run smoothly.




**Video content missing for image https://lh5.googleusercontent.com/-8maoQMLUo64/Uglhvucly1I/AAAAAAAAGJc/oQug6dk1btQ/s0/Big%25252520Tantillus%25252520%252525231%25252520-%25252520X-Y%25252520ends.mp4.gif**
![images/967b1399d7dce5d8b6daba54f240afff.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/967b1399d7dce5d8b6daba54f240afff.gif)

**Jason Smith (Birds Of Paradise FPV)**

---
---
**Dat Chu** *March 19, 2015 15:45*

Fantastic. Thank you. :)


---
**Isaac Arciaga** *March 19, 2015 18:00*

Thanks!


---
*Imported from [Google+](https://plus.google.com/103009815307828556107/posts/jE7WnHbdXkd) &mdash; content and formatting may not be reliable*
