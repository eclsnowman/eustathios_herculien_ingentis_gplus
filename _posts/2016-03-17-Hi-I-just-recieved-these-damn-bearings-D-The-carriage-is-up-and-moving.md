---
layout: post
title: "Hi, I just recieved these damn bearings :D The carriage is up and moving"
date: March 17, 2016 21:35
category: "Build Logs"
author: Maxime Favre
---
Hi,

I just recieved these damn bearings :D

The carriage is up and moving. Wiring is almost done.

I used the alignement tools with the printer upside down, they worked great. The carriage has a little resistance I don't know is it due to alignement, belt tension or when I tightened the M5 screws on the belt tensioner. I'll do some fine tuning later.



![images/832150e96a5026c2408d2fc8050b01e9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/832150e96a5026c2408d2fc8050b01e9.jpeg)
![images/fc65db3d01805c2965c792f7f853922a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fc65db3d01805c2965c792f7f853922a.jpeg)

**Maxime Favre**

---
---
**James Rivera** *March 17, 2016 22:49*

That carriage looks awesome. Nice wiring job, too!


---
**Eric Lien** *March 18, 2016 02:25*

Run the carriage into the corners by hand and check for increased resistance. If you get increased binding into corners you will want to tune the lower side rod positions to eliminate the resistance.



Also run the break-in gcode, then realign. The bushings have a little time to settle in. But once they do you will be very pleased. Once everything is right, you will know it.



Looking great. Can't wait to see your first prints.


---
**Maxime Favre** *March 18, 2016 11:40*

**+Eric Lien** Thanks. The corners looks fine.

How did you generate the break-in code ? Because my print area is smaller with this carriage. Btw I can use find & replace in the gcode.


---
**James Rivera** *March 19, 2016 01:25*

**+Maxime Favre** I had the same problem. I wrote a quick break-in g-code generator for myself. It is not ready for sharing and I don't have time to clean it up right now, but it already created some g-code I used to test my printer.  Right now, it only does a large square and a small square, both at 300mm/s. If that is too fast, the speed should be easy to change with search/replace. It is for a smaller bed (e.g. 200x200mm) and is actually only going to 180x180 max. Here is a google drive link to the g-code file.

[https://drive.google.com/file/d/0B2KLgRvoTcJdaGZSODNWQlRTOWs/view?usp=sharing](https://drive.google.com/file/d/0B2KLgRvoTcJdaGZSODNWQlRTOWs/view?usp=sharing)


---
**James Rivera** *March 19, 2016 21:55*

Here is the same thing with diagonals added, so both X and Y are working at the same time:

[https://drive.google.com/file/d/0B2KLgRvoTcJdRVV5RVdvb1lyWFU/view?usp=sharing](https://drive.google.com/file/d/0B2KLgRvoTcJdRVV5RVdvb1lyWFU/view?usp=sharing)


---
**Maxime Favre** *March 21, 2016 06:27*

**+James Rivera** great, thanks. I will try that ;)


---
**Maxime Favre** *March 22, 2016 21:36*

I have modified **+James Rivera**  Gcode for the mini space invader carriage, link in description: 

[http://www.thingiverse.com/make:205874](http://www.thingiverse.com/make:205874)



Break-in seems fine, it still need some angles tuning to have perfect squares.

PID auto-tuning is done. Still waiting on the correct Z ballscrews and 3m tape for the heatpad.


---
**Botio Kuo** *June 26, 2016 01:03*

**+Maxime Favre** Hi Max, about the LED power wire... where did you wire on the board ? I meant which location on the board you wire with LED light. Thanks


---
**Maxime Favre** *June 26, 2016 08:15*

Mines are always on so they are connected on the PS. The LEDs for the space invaders carriage are 12v so wire them in serial if your PS is 24v﻿.

If you want to control them, use a spare mosfet


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/ML4r3auL5fj) &mdash; content and formatting may not be reliable*
