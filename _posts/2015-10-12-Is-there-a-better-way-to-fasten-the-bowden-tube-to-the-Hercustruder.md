---
layout: post
title: "Is there a better way to fasten the bowden tube to the Hercustruder?"
date: October 12, 2015 13:11
category: "Discussion"
author: Seth Mott
---
Is there a better way to fasten the bowden tube to the Hercustruder?  I am using a 4mm nut, but it pops out of the bowden every so often.  Should I glue the nut to the bowden?





**Seth Mott**

---
---
**hon po** *October 12, 2015 13:49*

Never had this problem, but I usually print slow. How fast you print? May need to up the temp to compensate.



Someone mentioned before not drill out the bowden tube, but have a 2mm rod inside while threading the nut on top for a tighter fit.


---
**Seth Mott** *October 12, 2015 13:55*

Ah, I drilled out the inside, I will try using a 2mm drill bit inside while I thread on the nut. I am printing around 55mm/s.


---
**Eric Lien** *October 12, 2015 14:14*

I think there is a version of the top cover on youmagine someone made for a standard pushfit connection. If you can't find anything out there I will whip something for you up tonight.﻿


---
**Joe Spanier** *October 12, 2015 15:11*

I have that model on my pc. I'll link to it. Its in a drop box folder somewhere. I'll see if I can find the link. That's what I have been using on mine though. 


---
**Seth Mott** *October 12, 2015 15:26*

Thanks. I think the lining in my e3dv6 is worn causing me occasional jamming issues. It is over a year old now.


---
**Eric Lien** *October 12, 2015 15:33*

**+Seth Mott** what lining do you mean. The v6 is all metal, and I don't think there should be any where on the metal unless you are printing Carbon Fiber reinforced filaments.


---
**Seth Mott** *October 12, 2015 15:35*

Ah I was thinking of the pfte lining in Jheads for some reason. I must just need to take it apart and clean it really well again.


---
**Eric Lien** *October 12, 2015 15:55*

**+Seth Mott** Do a couple of Nylon Cold Pulls. It is way better than any other cleaning method I have tried.


---
**Hakan Evirgen** *October 12, 2015 17:30*

**+Seth Mott** if you have Original E3D V6 for Bowden then you have a push fit connecter integrated. It is a black plastic to be fitted on top and it locks the tubing in place. Also on the original version the tubing goes at least half way in.


---
**Ian Hoff** *October 12, 2015 19:11*

E3Dv6 is all metal with ptfe with the exception of the 3mm direct which has no ptfe liner... kind of a misnomer...



As for Jheads I wont use anything else to print PLA, there is nothing comparable IME. Last month THE manufacturer of actual jheads quit. Which made me very sad. I know some people hate on the jhead for its inability to do high temp plastic, but I want a hotend that prints something well, not prints everything mediocre.



I have been fighting a v6-lite for a few months now and finally got a part in to put the jhead back on... I was printing PLA correctly in 15 minutes. The difference is amazing.



The ptfe on the lite6 likes to creep back up out of position even with a retaining clip and made flat, or tapered and hot tightening, and I have half an idea whats up with a hotend and could not get the v6-lite to work as a bowden with PLA. No amount of strain relief seemed to matter I would have needed a set screw.



Anyone have any ideas what hotends are good for pla? All metal can cause problems with pla for various reasons, between PLA natural stickiness and metals ability to move heat it can soften pla too far from the melt chamber.


---
**Hakan Evirgen** *October 13, 2015 06:04*

**+Ian Hoff** I have second printer, which is bq Prusa i3 Hephestos. The hot end of this printer just works with everything and did not clog even when I tried on purpose for testing. AFAIK this is the only electro polished hot end on the market. This is what is used: [http://www.bq.com/gb/heatcore-unibody](http://www.bq.com/gb/heatcore-unibody)


---
**Seth Mott** *October 21, 2015 23:06*

**+Joe Spanier**  were you able to find the hercustruder model with push to fit?


---
**Joe Spanier** *October 22, 2015 13:50*

[https://drive.google.com/file/d/0BxM7qG4i9PtROHhvRERseTFRcGM/view?usp=sharing](https://drive.google.com/file/d/0BxM7qG4i9PtROHhvRERseTFRcGM/view?usp=sharing)



I couldnt find the original. It was a dropbox link similar to this. I use these daily with the black e3d bowden connectors though and it works great.


---
**Seth Mott** *October 22, 2015 13:52*

Awesome.  Thanks!


---
*Imported from [Google+](https://plus.google.com/108197211464469447407/posts/MPtrxBZzLwF) &mdash; content and formatting may not be reliable*
