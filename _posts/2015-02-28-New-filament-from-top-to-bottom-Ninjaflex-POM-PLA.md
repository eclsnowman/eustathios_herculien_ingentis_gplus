---
layout: post
title: "New filament from top to bottom Ninjaflex, POM, PLA. :)"
date: February 28, 2015 07:48
category: "Discussion"
author: Gus Montoya
---
New filament from top to bottom Ninjaflex, POM, PLA.  :)

![images/26857118e48612d80acf990dac6fdf64.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/26857118e48612d80acf990dac6fdf64.jpeg)



**Gus Montoya**

---
---
**Maxim Melcher** *February 28, 2015 08:54*

Source of POM filament?


---
**Gus Montoya** *February 28, 2015 08:56*

To be completely honest I don't recall. I bought it last year and forgot about it. Not until I went to my parents house today did I find the box without tags. I will try to find the source for you within the week but might be later. 


---
**Maxim Melcher** *February 28, 2015 08:58*

Thank You!


---
**James Rivera** *February 28, 2015 18:35*

**+Gus Montoya**  "...I bought it last year and forgot about it." This is pretty much me. With shipping from China generally taking so long, I tend to buy stuff in advance of really knowing what I'm going to do with it. I now have several microcontrollers, various sensors, filament, etc. that I'm not even sure what I'm going to do with it yet. But when free time shows up, I'm ready to go! :-)


---
**Gus Montoya** *March 01, 2015 07:52*

I was going to use it to make my VR goggle and some other things I needed. But I never got my stupid 2up printer to make a single print. It's been a year now. ... Damn thing is so poorly designed.   :(


---
**James Rivera** *March 01, 2015 17:32*

Ouch. Yeah, the 1Up & 2Up both looked too cheap to be reliable, IMHO. But, good parts cost more than bad ones. At least you got it. I spent money on a Makibox I never received. :-( 


---
**Gus Montoya** *March 01, 2015 21:51*

Depending the bank account you used, you can file a complaint and get your money back. I was lucky to get my full amount back. it took 3 months but it happened. 


---
**Gus Montoya** *March 01, 2015 21:52*

**+Maxim Melcher**  I purchased the POM from E3D at a cost of 28 pounds or their abouts. 


---
**Maxim Melcher** *March 01, 2015 22:35*

**+Gus Montoya** Thanks!


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/FqeaWxnZ7hu) &mdash; content and formatting may not be reliable*
