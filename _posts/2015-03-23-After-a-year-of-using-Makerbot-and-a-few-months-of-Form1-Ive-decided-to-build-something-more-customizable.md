---
layout: post
title: "After a year of using Makerbot and a few months of Form1, I've decided to build something more customizable"
date: March 23, 2015 09:15
category: "Deviations from Norm"
author: Ilya R
---
After a year of using Makerbot and a few months of Form1, I've decided to build something more customizable. After checking numerous designs, the Eustathios looks like a very good place to start, and I really like the kinematics on this build. However, I do like the option of direct extrusion, or just mounting some additional equipment (not too heavy, though) on the carriage, next to the bowden extruder.  Therefore, I'm a bit doubtful about the 8mm rods, and thinking I need something more rigid. And, perhaps, a switch to the 4040 extrusion.

So, any and all advice from a more experienced builder would be most welcome. 





**Ilya R**

---
---
**Jason Smith (Birds Of Paradise FPV)** *March 23, 2015 11:53*

My best guess is that you'll be fine with the existing design for any reasonable amount of weight you want to add to the carriage. There's a lot of overhead there. The first place in need of beefing up would likely be the 8mm rods, but again I think you'll be fine with the lengths/weights you're talking about. The extrusion is plenty strong/stiff. The only benefit a larger profile would give you is additional strength at the joints, but this isn't an issue if you use the tapped ends as well as corner brackets for connecting everything. Good luck. Let us know what you decide. 


---
**Ishaan Gov** *March 23, 2015 14:03*

A geared NEMA 14 motor for a direct drive extruder may be an option instead of the NEMA 17 that is commonly used


---
**Erik Scott** *March 23, 2015 14:11*

Welcome to the community! You may want to check out the herculien as that uses beefier rods. It's still based on the Eustathios, and has the same kinematics. 


---
**Eric Lien** *March 23, 2015 17:03*

**+Ilya R**​ take a look at this post: [https://plus.google.com/+EricLiensMind/posts/Y4V8sHcjmgu](https://plus.google.com/+EricLiensMind/posts/Y4V8sHcjmgu)


---
**Tim Rastall** *March 23, 2015 19:51*

I would go for twin 8mm on each axis if you want more carriage stability. A la Zortrax, which is direct drive. [http://www.3ders.org/images/zortrax5.jpg](http://www.3ders.org/images/zortrax5.jpg)


---
**James Rivera** *March 23, 2015 21:19*

If you want a larger, more rigid frame, why not just go with the HercuLien?


---
**Mutley3D** *March 23, 2015 23:14*

I hear there will soon be a Flex3Drive assembly that accomodates LM8 bearings for this family of machines :)


---
**Eric Lien** *March 24, 2015 00:14*

**+Mutley3D** great to hear it. It could be a great combo for opening the possibilities of flexible filament on the x/y gantry family of printers.


---
**Mutley3D** *March 24, 2015 01:42*

**+Eric Lien** Perhaps the name can be confusing, Flex3Drive, is a geared hi speed hi flow direct drive extruder mounted on your moving gantry or carriage, driven by a flexible shaft. The extruder motor is mounted on the frame. Flexible shaft transfers drive from motor to extruder. Fast light accurate extrusion producing great quality prints :)


---
**Eric Lien** *March 24, 2015 02:21*

**+Mutley3D** i understand. But isn't your drive capable of ninjaflex? Bowden isn't.


---
**Erik Scott** *March 24, 2015 02:27*

Ohhhh, that's really neat! As long as the drive gear is mounted right above the hotend, I don't see why it couldn't do flexible stuff. What kind of backlash is there in the flexible drive shaft? (ie. torsional rigidity)


---
**Mutley3D** *March 24, 2015 03:05*

**+Eric Lien** Yes to NinjaFlex, and the even more flexible FilaFlex. **+Erik Scott** A lot of investigation has been done into driveshafts and suppliers. Now using/supplying with shafts that have no evident angular displacement within the torque demands of the extruder mechanism.


---
*Imported from [Google+](https://plus.google.com/113946807625816852599/posts/ens3qaEFP12) &mdash; content and formatting may not be reliable*
