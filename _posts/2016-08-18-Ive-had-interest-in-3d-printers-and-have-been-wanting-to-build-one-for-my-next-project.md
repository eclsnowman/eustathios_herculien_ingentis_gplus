---
layout: post
title: "I've had interest in 3d printers and have been wanting to build one for my next project"
date: August 18, 2016 01:57
category: "Discussion"
author: William Rilk
---
I've had interest in 3d printers and have been wanting to build one for my next project.  Funny how a mention and a split second picture on one of Thomas Sanladerer's youtube videos hooked my interest in the HercuLien so much. I am very much still in the planning phase and intend to take my time as the build is sometimes the most enjoyable part to me.



I don't have access to a printer for parts, but I do have a mill I converted to CNC that I plan to make most of the parts with.  This shouldn't be much of an issue for the stationary components, but I'm concerned the machine would take a huge performance hit if it had to sling around an aluminum carriage and belt tensioners.  



My first thought was to use delrin, but to my thinking, sourcing it isn't realistic for a couple reasons.  Now I'm leaning back towards aluminum components, albeit with a more slender design.



Any thoughts or ideas?





**William Rilk**

---
---
**Eric Lien** *August 18, 2016 02:05*

Check out the modified carriage by **+Zane Baird**​​ on the github user mods section. He made some beautiful upgrades, one of which is his carriage mod which uses one Volcano hot end and one standard v6. Plus it has less bulk. Also, if you go with aluminum you can shave even more because of the higher strength at thinner wall thickness.



[https://github.com/eclsnowman/HercuLien/tree/master/Community%20Mods%20and%20Upgrades/Zane%20Baird](https://github.com/eclsnowman/HercuLien/tree/master/Community%20Mods%20and%20Upgrades/Zane%20Baird)﻿


---
**Zane Baird** *August 18, 2016 03:14*

Just an FYI, I haven't yet gotten around to fixing the broken .stl files on the github page just yet. You should be able to open the solidworks files no problem though.


---
**William Rilk** *August 19, 2016 04:16*

Thank you.  I hadn't looked in the mods folder yet.



I really like Zane's belt tensioner design.  The model looks like it will lend itself well to being pared down.



I had recently thought about a volcano/v6 combination and had wondered about dealing with the different hot end lengths, so I need to look closer at Zane's design.  As I said, I am a  noob to 3d printing and I'm curious how two different size nozzles/extrusion rates are handled in firmware (Tom remarked that it can be done quite easily).  All stuff I look forward to learning!



I intend to post pictures when I actually have physical parts completed.  Thanks again.




---
*Imported from [Google+](https://plus.google.com/100191047182984055447/posts/LymT5JoPJnc) &mdash; content and formatting may not be reliable*
