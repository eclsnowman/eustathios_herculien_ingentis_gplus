---
layout: post
title: "Anyone had any problems getting their SSRs to work with their 110v heated beds?"
date: February 12, 2016 20:53
category: "Discussion"
author: Ben Delarre
---
Anyone had any problems getting their SSRs to work with their 110v heated beds?



I've got a 500W 110v silicone heating pad, with the live wire routed through a Fotek SSR-25da and neutral directly connected to the other wire on the bed. My Smoothieboard is connected to the SSR through P1.23 (PWM capable pin), and configured to drive the pwm_frequency at 20. The SSR indicator light turns on when expected, but the bed does not heat.



I've confirmed the bed works by hooking it up directly to live and neutral, but through the SSR I get nothing. I've tried using bang bang to drive the SSR too and still nothing.



From what I understand of the way AC SSRs work its pretty hard to measure if they are 'open' or not since they need AC current to drive them. 



Anyone got any ideas? I've tried two different Fotek SSR-25das from two different suppliers but no luck yet.





**Ben Delarre**

---
---
**Frank “Helmi” Helmschrott** *February 12, 2016 21:03*

Is there any reason you are using PWM here? The SSR should only work with a constant input. You should also set your Firmware (Don't know anything about Smoothieware, sorry) to slowly switch the bed - i think it's called ping-pong in Repetier firmware where you can set delay times between switch cycles. You shouldn't switch your SSR more than needed.


---
**Ben Delarre** *February 12, 2016 21:04*

Yeah Smoothieboard has pwm_frequency limit setting which means it won't switch the SSR on/off more than that frequency, they recommend a setting of 20hz. I've tried lower but still no joy. I also tried the bang bang approach which does as you say just use a constant on/off value with a hysteresis factor, but no luck there either.


---
**Frank “Helmi” Helmschrott** *February 12, 2016 21:04*

ahh found it on the smoothieware website - it's called Bang Bang there [http://smoothieware.org/temperaturecontrol#toc12](http://smoothieware.org/temperaturecontrol#toc12)


---
**Eric Lien** *February 12, 2016 21:04*

While the light is on can you confirm you have 110V between the output of the SSR back to neutral? If not the SSR is not actually latching when the DC input triggers. Also beware the Fotek SSR. They are poorly potted and cannot maintain anywhere near the nameplate specs. 



This is the one I prefer: Omron G3NA-220B-DC5-24 Solid State Relay, Zero Cross Function, Yellow Indicator, Phototriac Coupler Isolation, 20 A Rated Load Current, 24 to 240 VAC Rated Load Voltage, 5 to 24 VDC Input Voltage [https://www.amazon.com/dp/B003B2Z0N6/ref=cm_sw_r_other_awd_bIKVwbY5QWVDW](https://www.amazon.com/dp/B003B2Z0N6/ref=cm_sw_r_other_awd_bIKVwbY5QWVDW)



Also just use the standard bed output into the SSR. Most SSR need 5v minimum inbound to trigger.


---
**Frank “Helmi” Helmschrott** *February 12, 2016 21:06*

**+Ben Delarre** 20 Hz is 20 times per second - having it set at once per 2 seconds would way more fit what SSRs are made for :) Definitely not PWM.


---
**Ben Delarre** *February 12, 2016 21:09*

Just ordered that Omron relay, will get it tomorrow and let you know how it goes.



I tested the AC value between the input of the SSR and neutral and it was definitely giving 110v, but I didn't try the output. Will test again tonight when I get in. I'll try switching to the small mosfet as well so I can get the higher voltage, though the Fotek is supposed to drive at 3v.



I'll also try backing off the PWM frequency, but I'm pretty sure that they can latch that fast, the datasheet says <10ms for on/off, that gies you 100hz frequency at worst.


---
**Frank “Helmi” Helmschrott** *February 12, 2016 21:14*

Well it may technically be possible but it just doesn't make any sense. The bang bang mode (or ping pong in other firmware) is made for these cases. Switching the SSR on/off multiple times a second would at least be inefficient I'd guess. I don't know if there are limitations in switch cycles on SSRs but I'm quite sure they're not made for this. 



Regarding the minimal 3V - this is not true for almost all SSRs i have seen so far. Each one that told me 3V needed at least 3.5-4V. Others print 5V on it and already switch at 4V - you might guess what this means :)


---
**Ben Delarre** *February 12, 2016 21:23*

Hmm, is the thermal mass of the bed large enough that you don't get massive overshoot though with full power without PWM? Its a 500W 110v heater, it heats up real fast. I guess as long as there's enough thermal mass in the aluminum bed then this isn't a huge problem.



But yeah, totally get what you mean about trigger levels, could well be not going fully on even though the light is indicating it is.


---
**Gústav K Gústavsson** *February 12, 2016 21:46*

The SSR should get is input from the bed output of the Smoothieboard NOT from a PWM output. Although I dont have the specification for smoothieboard but PWM outputs most definitely can't Supply the current to switch a high power SSR. And most SSR need at least 5 volt to switch. And I dont under stand why do you want to use PWM for a heater?


---
**Ben Delarre** *February 12, 2016 21:48*

Its what they instruct in their documentation, check it out: [http://smoothieware.org/3d-printer-guide#toc26](http://smoothieware.org/3d-printer-guide#toc26)


---
**Gústav K Gústavsson** *February 12, 2016 23:26*

Sorry just get.

This guide does not excist.



Maybe have to log in to get it? 


---
**Ben Delarre** *February 12, 2016 23:28*

Weird. Works for me o  multiple devices. No idea why you get that. 


---
**Ben Delarre** *February 13, 2016 21:03*

Ok turns out the 3.3v wasn't enough to drive the Fotek SSR to fully open, running through the small mosfet at 24v made it work perfectly. I have now switched over to the Omron SSR since the horror stories about the Foteks exploding had me nervous. Working good now, 500W heated bed is awesome :-)


---
**Eric Lien** *February 13, 2016 21:43*

**+Ben Delarre** great news and glad to hear you upgraded to the Omron. I sent my spares out to people who had fires almost start due to critical failure on those cheap SSR's.


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/TwkAhr6s1Qw) &mdash; content and formatting may not be reliable*
