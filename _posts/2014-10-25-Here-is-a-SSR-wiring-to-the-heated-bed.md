---
layout: post
title: "Here is a SSR wiring to the heated bed"
date: October 25, 2014 01:58
category: "Discussion"
author: BoonKuey Lee
---
Here is a SSR wiring to the heated bed.  Comments?



With this, can I still use bed PID?

![images/5843b45f7c8563fe7a5d27e07bc6efc7.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5843b45f7c8563fe7a5d27e07bc6efc7.png)



**BoonKuey Lee**

---
---
**Brian Bland** *October 25, 2014 02:19*

Not the way to wire it up.  I use PID.


---
**Jean-Francois Couture** *October 25, 2014 02:53*

the SSR is powered by the normal bed 12V ouput of your board.



it goes : ArduinoBoard --> SSR --> heatbed



you connect the bed in series with your power source..  (one wire to the bed from the power source, the other one goes to terminal #1 of the ssr, then terminal #2 of the ssr to the heatbed)



this is on high end side (24-480AC if your source is main's power) 



the low side (3-32DC) is wire to wire with the + & - of the arduino board. they are marked so you can't go wrong.



Don't forget to also jump the bed input with your power supply's 12V


---
**Jean-Francois Couture** *October 25, 2014 02:59*

Has your picture, if the bed is also 12V DC then you need a DC-DC SSR. (the one in the picture with thoses specs is for AC voltage)



then you simply split the power supply's power in two.. (one for the board and one for the bed)



This is the model you need.. look at the picture.. just don't use this one for main's power. [http://www.ebay.ca/itm/Solid-State-Relay-SSR-40DD-40A-5-60VDC-3-32VDC-/141023030534?pt=LH_DefaultDomain_2&hash=item20d5a0ad06](http://www.ebay.ca/itm/Solid-State-Relay-SSR-40DD-40A-5-60VDC-3-32VDC-/141023030534?pt=LH_DefaultDomain_2&hash=item20d5a0ad06)


---
**ThantiK** *October 25, 2014 03:02*

Just no. No no no. 


---
**BoonKuey Lee** *October 25, 2014 03:04*

Sorry. I am new with SSR.


---
**Jean-Francois Couture** *October 25, 2014 03:11*

that is why we are here.. Power to the community ! :-)

We prevent you from blowing up your investment ! LOL


---
**ThantiK** *October 25, 2014 03:14*

Sorry, I replied with the short comment so I could get you stopped before you did anything dangerous.  I needed the time to create a diagram: [http://i.imgur.com/TsoUdHF.png](http://i.imgur.com/TsoUdHF.png)


---
**BoonKuey Lee** *October 25, 2014 03:24*

**+ThantiK** **+Jean-Francois Couture** thank you. The image helps a lot.


---
**Eugene Lee** *October 25, 2014 04:13*

I tried something similar to this a few weeks ago after I found a schematic on a spanish? forum. Didn't understand a thing, thought I would just try it out. Turn bed on, BOOM! 0-200degC in 2 seconds. I had my finger on the switch just incase and hit it just in time, so there was no lasting damage to my alu bed. Lesson learned, be careful with AC, don't be stupid haha! 


---
**ThantiK** *October 25, 2014 04:24*

**+BoonKuey Lee**, make sure the bed is matched up with the input voltage of the outlet.  Also make sure to use the right kind of connectors so that no metal is ever exposed anywhere (your SSR should have a cover); you don't want this touching a metal frame, etc.  Take proper safety precautions.  Use terminal blocks/connectors like this: [http://www.adafruit.com/images/1200x900/677-00.jpg](http://www.adafruit.com/images/1200x900/677-00.jpg) - which have all of the screws/connections recessed to prevent shorting.  And <i>install strain relief</i> everywhere there is a moving wire.


---
**BoonKuey Lee** *October 25, 2014 06:05*

Looks like DC is safer. I can also go with DC To DC having a separate 24VDC supply.  Good option? 


---
**Øystein Krog** *October 25, 2014 08:01*

Yes, I use a 24VDC  supply that I tweaked to output 30V (of course just for the heatbed).


---
*Imported from [Google+](https://plus.google.com/105655952080130147525/posts/UZEoHBVMNnT) &mdash; content and formatting may not be reliable*
