---
layout: post
title: "Quick question guys, I've never encountered this before"
date: February 27, 2015 04:04
category: "Discussion"
author: Daniel Salinas
---
Quick question guys, I've never encountered this before.  I'm trying to print a eustathios set for **+Eric Bessette** and I've run into this issue with all the slicers I have for the XY Tensioner blocks.  It <b>seems</b> like the wall width is under my nozzle diameter.  I usually print with an extrusion width of .48mm and I use a .4mm nozzle on an E3D V6 hotend.  I can get that wall to slice to a full face if I change my extrusion width settings to .32.  Is this something you guys have run into?  Workarounds?

![images/ddea0654be11b6da62fdf945e77ababd.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ddea0654be11b6da62fdf945e77ababd.png)



**Daniel Salinas**

---
---
**Erik Scott** *February 27, 2015 04:11*

Yes, I had this issue when I first printed my parts. It'll be ugly, but it'll work. I did end up modifying the part for when I printed my replacement parts. I can get you the STLs tomorrow if you would like. 


---
**Daniel Salinas** *February 27, 2015 04:18*

**+Erik Scott** that would be awesome thanks!  I'm sure **+Eric Bessette** would appreciate it too since these are for him :)


---
**Erik Scott** *February 27, 2015 23:25*

I have put all my STLs here: [https://www.dropbox.com/sh/emgxes8ebwl39h5/AAC3iiUGlpzBaTEt7DXNPE0ga?dl=0](https://www.dropbox.com/sh/emgxes8ebwl39h5/AAC3iiUGlpzBaTEt7DXNPE0ga?dl=0)



I have 2 versions of both the A and B belt tensioners. You want the 2.0 version. Not only have I corrected the little derpy spot, but I also chamfered the insides where the pressfit bearings go, so you can print nicely without supports. 



Hope it works out for you. Feel free to use any of my other STLs if you like! (and that goes for anyone)


---
**Daniel Salinas** *February 28, 2015 00:40*

Great thanks


---
**Daniel Salinas** *February 28, 2015 00:41*

**+Eric Bessette**​ I'll pull these down tonight and reprint the xy tensioner sets for your parts but I will send you the other parts too for backup.


---
**Erik Scott** *February 28, 2015 00:44*

No problem! Just something to keep in mind, I shrunk the counterbored section a bit. You need to use a socket head M5 screw. I don't think a button-head will work as the head has a larger diameter. 


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/PwRUEubJXMF) &mdash; content and formatting may not be reliable*
