---
layout: post
title: "Now its getting interesting "
date: January 28, 2016 13:16
category: "Show and Tell"
author: Mikael Sjöberg
---
Now its getting interesting 😀

![images/e4a1f4ba9b57ebccf7967830167af373.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e4a1f4ba9b57ebccf7967830167af373.jpeg)



**Mikael Sjöberg**

---
---
**Eric Lien** *January 28, 2016 13:38*

I've been looking forward to an update, how close are you to having everything you need?


---
**Mikael Sjöberg** *January 28, 2016 14:40*

Well, very close. My Z axis was not straight enough so I sent them for straightening them up . Hopefully they are good enogh now, and they should be ready by now. 

Otherwise, I miss the 220 v connection ( the black one, i have the power supply) . I think Thats all ...


---
**Jim Stone** *January 28, 2016 19:54*

is that a package from voxel factory? ( not including the heat pad)




---
*Imported from [Google+](https://plus.google.com/118217975155696261814/posts/5g2oJCv3ecM) &mdash; content and formatting may not be reliable*
