---
layout: post
title: "So the smoothie board is not mounted to the plastic 3d printed part, it's mounted to the plastic sheet of the printer?"
date: July 05, 2015 20:43
category: "Discussion"
author: Gus Montoya
---
So the smoothie board is not mounted to the plastic 3d printed part, it's mounted to the plastic sheet of the printer? Same with power supply? Could someone share their electronic mount?

   Also whats the best way to clean off linear rods from oil? I used the wrong type now the bushings are hard to move. The oil used is too thick.





**Gus Montoya**

---
---
**Eric Lien** *July 05, 2015 21:24*

Acetone should work. What type of oil did you use. You don't even need oil actually. The oil sintered bronze bushings are self lubricating. If anything a very light sewing machine oil, or a 0W oil.


---
**Gus Montoya** *July 05, 2015 23:00*

**+Eric Lien** I prefer not to say. Thinking about it makes me feel beyond stupid. I'll grab some acetone and clean it up. 


---
**Mike Miller** *July 05, 2015 23:28*

We've all been there...I had a sample teflon bearing I was playing with...used teflon lube on it and it pretty much turned to glue. 


---
**Eric Lien** *July 05, 2015 23:51*

**+Gus Montoya** I understand. Yes some oils have binders and silicone in them to get them to stay tacky on gears etc. If that is the case, and it's on the surface of the bronze... I hate to say it but you might be screwed. The rods will cleanup up no problem. But the bronze is much more porous by design to let the oil migrate out during use/heat/wear. If the pours get clogged they will no longer self lubricate. 


---
**Mike Thornbury** *July 06, 2015 15:33*

Silicone spray is very good. Doesn't attract gunk.



TF2 is also very good, again a 'dry' lubricant. I use the bottle, not the spray, for all sorts of mating surfaces.


---
**Eric Lien** *July 06, 2015 18:44*

**+Mike Thornbury** I would avoid silicone spray on oilite bronze bushings. 


---
**Mike Thornbury** *July 06, 2015 19:40*

I meant to put graphite... Your post on silicone got in my head :)


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/RWZ73icA1k9) &mdash; content and formatting may not be reliable*
