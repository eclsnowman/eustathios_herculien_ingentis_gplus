---
layout: post
title: "Z Axis done, gantry will be up tonight!"
date: April 23, 2015 03:49
category: "Show and Tell"
author: Isaac Arciaga
---
Z Axis done, gantry will be up tonight! Thank you **+Alex Lee** and Roxy for cutting my aluminum bed plate!

![images/335a4b53b77c16e17beb37c34a512c4f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/335a4b53b77c16e17beb37c34a512c4f.jpeg)



**Isaac Arciaga**

---
---
**Gus Montoya** *April 23, 2015 05:16*

I agree, looking good.


---
**Eric Lien** *April 23, 2015 08:55*

I just noticed the embossed text on the carriage. Nice touch ;)


---
**Isaac Arciaga** *April 23, 2015 09:20*

Thanks guys! **+Eric Lien** I was wondering when someone would notice!


---
**Gus Montoya** *April 23, 2015 13:51*

Hmm I had to look hard but, yeah that's cool.


---
**Øystein Krog** *April 23, 2015 17:55*

The all black is very sexy :P


---
**E. Wadsager** *May 16, 2015 21:53*

nice car


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/4hRZ21LR4i6) &mdash; content and formatting may not be reliable*
