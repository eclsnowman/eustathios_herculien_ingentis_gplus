---
layout: post
title: "Printed one of the rod alignment tool that Eric Lien posted to realize I need to print another "
date: February 16, 2015 00:48
category: "Discussion"
author: Dat Chu
---
Printed one of the rod alignment tool that **+Eric Lien**​ posted to realize I need to print another 😯. 



Also I am getting these ringing effects on the vertical surface of my print. What is the cause of this? 



![images/8db7be74a5ce9f320d0e24d53282be4a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8db7be74a5ce9f320d0e24d53282be4a.jpeg)
![images/85b5d4bea6fa9e5b5b123c723b33b3db.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/85b5d4bea6fa9e5b5b123c723b33b3db.jpeg)

**Dat Chu**

---
---
**Brandon Satterfield** *February 16, 2015 00:54*

Slow your non-printed moves brother. Are you on the simple? 


---
**Dat Chu** *February 16, 2015 01:11*

Yes. I am on the simple metal. 


---
**Dat Chu** *February 16, 2015 01:11*

My speed is only 40. Hardly fast. 


---
**Eric Lien** *February 16, 2015 01:26*

Also, just an FYI these don't work for HercuLien. The corner brackets set the distance by themselves. But the downside is on HercuLien if you don't have accurate prints you could bind since there are no degrees of freedom. But mine worked right after assembly smooth as butter, so it is a better method IMHO than trying to align 8 rod ends independently.


---
**James Rivera** *February 16, 2015 02:26*

**+Dat Chu** what are your jerk settings at?


---
**Dat Chu** *February 16, 2015 03:10*

My firmware has the acceleration set to 1500. Retraction acceleration 3000. Is that the one? **+James Rivera**​. Thanks for the info **+Eric Lien**​. I assume there is a way to improve print measurement accuracy? 


---
**James Rivera** *February 16, 2015 03:23*

**+Dat Chu** No, look at the next line in the M501 dump, the M205 command line.  Here is the dump from my Printbot+ LC v2:



SENDING:M501

echo:Stored settings retrieved

echo:Steps per unit:

echo:  M92 X80.00 Y80.00 Z1600.00 E518.61

echo:Maximum feedrates (mm/s):

echo:  M203 X100.00 Y100.00 Z2.00 E14.00

echo:Maximum Acceleration (mm/s2):

echo:  M201 X2000 Y2000 Z30 E10000

echo:Acceleration: S=acceleration, T=retract acceleration

echo:  M204 S3000.00 T3000.00

echo:Advanced variables: S=Min feedrate (mm/s), T=Min travel feedrate (mm/s), B=minimum segment time (ms), <b>X=maximum XY jerk (mm/s),  Z=maximum Z jerk (mm/s),  E=maximum E jerk (mm/s)</b>

<b>echo:  M205 S0.00 T0.00 B20000 X20.00 Z0.40 E5.00</b>

echo:Home offset (mm):

echo:  M206 X0.00 Y0.00 Z0.00

echo:PID settings:

echo:   M301 P22.20 I1.08 D114.00

echo:Min position (mm):

echo:  M210 X0.00 Y0.00 Z0.00

echo:Max position (mm):

echo:  M211 X200.00 Y200.00 Z200.00


---
**Dat Chu** *February 16, 2015 03:28*

I see. When I use octopi I can't seem to get the values out since the app will keep on scrolling. I will update this post later. So what values should we put in m205?


---
**Dave Hylands** *February 16, 2015 05:32*

In octopi, if you turn off autoscroll then it's much easier to scroll back and see the output of M503.


---
**Eric Lien** *February 16, 2015 14:13*

If you want to print fast, but avoid exterior ringing, print inside infill and perimeters first at high speed, then just slow down the outside perimeters.


---
**Dat Chu** *February 16, 2015 15:12*

I think cura slices it with the inner perimeter then outer perimeter then the infill. What do you use to control these settings? Repetier. 


---
**Eric Lien** *February 16, 2015 15:51*

Simplify3d


---
**Dave Hylands** *February 16, 2015 16:08*

The shadows of the circular feature I call ringing. It's basically mechanical oscillations that occur when the head changes directions. You generally need to slow down the outer perimiters to get rid of these. If you can modify the design to have rounded edges, then this artifact will be minimized.



The horizontal lines are causes by somehing in the z-axis. The exact cause will depend on the design of your printer. If you have z-steppers on top with hanging couplers, then the spring in the couplers is the cause. You either need a thrust bearing or something at he bottom for the leadscrews to rest on so that they don't bounce up and down.



If you have the couplers at the bottom, make sure that the leadscrew and the stepper shaft are touching inside the coupler.


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/UXztfJ718ka) &mdash; content and formatting may not be reliable*
