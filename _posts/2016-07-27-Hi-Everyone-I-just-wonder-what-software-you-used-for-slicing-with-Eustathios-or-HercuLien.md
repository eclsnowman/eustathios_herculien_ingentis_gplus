---
layout: post
title: "Hi Everyone, I just wonder what software you used for slicing with Eustathios or HercuLien ?"
date: July 27, 2016 17:55
category: "Discussion"
author: Botio Kuo
---
Hi Everyone, I just wonder what software you used for slicing with Eustathios or HercuLien ?  Thank you





**Botio Kuo**

---
---
**Martin Bogomolni (MB)** *July 27, 2016 18:43*

I use Cura, and Simplify3D


---
**Eric Lien** *July 27, 2016 18:55*

Simplify3d


---
**Derek Schuetz** *July 27, 2016 19:49*

Simplify3d...Cura won't work for my machine 


---
**jerryflyguy** *July 27, 2016 20:57*

Is there a public Simplify3D setup file that we can use to accelerate our start to using the printer?


---
**Botio Kuo** *July 27, 2016 21:19*

**+jerryflyguy** do you mean FFF profile file ?


---
**Eric Lien** *July 27, 2016 21:27*

It would probably be a good idea to collect a few sample FFF profiles from people in the community to get a database of known good parameters for each filament type. The items we would need to know are:



1.) What filament

2.) Any machine Modifications (ball screws, Bondtech, etc.)



The other things to watch out for would be if people have different controllers or different tuning on the controller configs that would make one printer behave different than another.


---
**jerryflyguy** *July 27, 2016 21:49*

**+Botio Kuo** I guess so? I'm not that far along yet, still waiting for the last of my printed parts. I've not bot Simplify3D yet but it's my intention. All I know is there are parameter files that can be loaded for specific machines. 



**+Eric Lien** sounds like a good idea, no doubt there will be differences due to the custom nature of the builds. They would be a decent starting point, I'd think?



Any way to include this in the file site as a central holding point for the various setups?


---
**Eric Lien** *July 27, 2016 22:04*

**+jerryflyguy** the github would be best. But if people send me links I can get them added to the github.


---
**Pieter Swart** *July 28, 2016 19:12*

You can also try craftware. 


---
**Roland Barenbrug** *July 29, 2016 12:03*

Happily using Slic3r. Take one of the recent builds, the official latest release crashes all the time on my systems (Windows 7/10).




---
**Maxime Favre** *August 02, 2016 09:00*

Hi, simplify 3d here too. I have a FFF file with ballscrews, bondtech (fly extruder & bowden) if you want something to start.


---
**Botio Kuo** *August 02, 2016 14:26*

**+Maxime Favre** Hi Max, Could you sent this to me pls? thank you so much!! :) I just buy simplify3D with student discount XD


---
**Sean B** *September 05, 2016 23:50*

**+Maxime Favre** I am also running S3D and use ballscrews and bowden.  Could you share your FFF file?


---
**Maxime Favre** *September 06, 2016 07:06*

**+Sean B**​ Sure ! Note that you'll probably need to tweak some value accordingly to your printer.

[drive.google.com - Eustathios.fff - Google Drive](https://drive.google.com/file/d/0B3rOLUu_uwqNdVhsY0VWQUFDZjQ/view?usp=drivesdk)


---
**Sean B** *September 06, 2016 15:32*

**+Maxime Favre** thanks!!


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/frmDMn5fQYu) &mdash; content and formatting may not be reliable*
