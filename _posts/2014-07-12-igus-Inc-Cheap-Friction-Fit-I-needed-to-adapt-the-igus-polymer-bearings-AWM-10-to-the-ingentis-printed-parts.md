---
layout: post
title: "igus Inc. Cheap Friction Fit: I needed to adapt the igus polymer bearings (AWM-10) to the ingentis printed parts"
date: July 12, 2014 14:26
category: "Deviations from Norm"
author: Mike Miller
---
**+igus Inc.** Cheap Friction Fit:



I needed to adapt the igus polymer bearings (AWM-10) to the ingentis printed parts. I first attempted printing spacers, but the difference in thickness is nearly at the width of extruded filament...I then made non-circular (open) spacers, in the hopes that it would take up the slack and form to the gap between the printed part and the bearing. That caused issues with alignment between bearings in a part that uses one on each side. 



I needed something that was:

- cheap

- easy to duplicate (no machine turned parts for this part of the build)

- non-destructive (in case I determined it wasn't the ultimate solution. I didn't want to keep calling the folks at igus for replacement bearings...not due to cost, but due to the round trip time is would add to the project.)

- slightly deformable - There's a break-in period with igus parts which will create a 'set' and allow for good movement, however, better alignment is better alignment. When the bushings are installed correctly, there's very little run-in necessary to make things happy. 



So I'm experimenting with PTFE tape. It's REALLY cheap, and two dozen or so loops around an igus busing make for a nice, snug, interference fit in the Ingentis printed parts. 



Make sure it's thick enough to require a little pressure to seat, and make sure you twist the bushing the same way the tape is applied. 



unlike the printed shims, which needed a vice to install and caused alignment headaches, the PTFE tape is very easy on the parts, and the alignment in the assembled part is excellent. 



![images/265d50e6557cec3b44fcf09ff0ab4a8f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/265d50e6557cec3b44fcf09ff0ab4a8f.jpeg)
![images/4f0621d65bbbcea074c983426dfdfdb8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4f0621d65bbbcea074c983426dfdfdb8.jpeg)

**Mike Miller**

---
---
**Brandon Satterfield** *July 12, 2014 14:37*

Hey Mike did you print the part? 


---
**Mike Miller** *July 12, 2014 14:52*

I did not, **+Brian Bland** bland did. His printer is <i>exceptionally</i> well dialed in. 


---
**Mike Miller** *July 12, 2014 14:57*

**+Shauki Bagdadi** that was how I interpreted Point 2 on this page ([http://www.igus.com/wpck/10618/banderole_drylinW?C=US&L=en](http://www.igus.com/wpck/10618/banderole_drylinW?C=US&L=en)) 


---
**Joe Spanier** *July 12, 2014 15:11*

Is this working for you to get the press? I'm having issues dialing in the perfect diameter


---
**Mike Miller** *July 12, 2014 15:27*

It's a sunk-cost fallacy, **+Shauki Bagdadi**, I've got the ingentis parts already, they're dimensionally correct for <i>Ingentis</i>. I do not wish to throw that effort away. The PTFE tape is cheap, slightly deformable, and if you don't have enough for the press fit, it's no big deal to add more. Since there is no drag in a well calibrated system, the bushings won't walk out over time. And believe me, these are much higher quality prints than I could turn out (which is why I'm building a 2nd printer in the first place!) 



His parts are orange, and dimensionally correct, the blue ones are not:

[http://imgur.com/a/1HSE2](http://imgur.com/a/1HSE2)


---
**Mike Miller** *July 12, 2014 16:00*

Well that's what I'm doing! I started out building an ingentis, but didn't like some of the bits and bobs, pulled igus into the mix, got jealous of **+Eric Lien**'s printer and made some other changes...



I have NO desire to document a full print build, but have no problems documenting how to use igus parts on ingentis if people so desire. 


---
**James Rivera** *July 12, 2014 18:04*

I have been very interested in the Igus polymer bearing for one major reason: silence. My first Printrbot has recirculating metal bearings (LM8UU) that are <b>very</b> noisy! My 2nd one is less noisy (newer bearings), but I'm imagining the day where I can print overnight next to a bedroom without waking anybody up. I think polymer bearings are the solution.


---
**James Rivera** *July 12, 2014 18:06*

I find myself increasingly attracted to **+Eric Lien**'s Eustathios design, but I'm also trying to keep the cost down, and the idea of driving everything via braided fishing line is really starting to appeal to me. I think the equation is: Fishing line drive + polymer bearings = happiness. :-)


---
**James Rivera** *July 12, 2014 18:07*

Oy, I'm kind of thread-jacking here aren't I? Sorry. BTW, I meant say up front that I really like your idea of using the PTFE plumber's tape for the press fit.


---
**Mike Miller** *July 12, 2014 18:10*

I suspect REAL silence will require isolating the motors. Rapidly moving metal, screwed to metal = noise. I've heard plenty of printers on youtube that are noisy...but my delta (aside from fan noise) is pretty quiet. 


---
**Mike Miller** *July 12, 2014 19:30*

Good to know...I wonder why you can hear some..like the lulzbot YouTube videos...


---
**James Rivera** *July 12, 2014 19:42*

Really? Most of the videos of Lulzbots printing I've heard either have A) music, or B) quiet whirring of the steppers. I haven't really heard much else. It's one of the reasons I started wanting polymer bearings...


---
**Mike Miller** *July 12, 2014 23:06*

Of course, now that I've said it, I can't find an example...when I do, I'll report back. :) it's a musical whining from the motors, but it's impossible to tell on video just how loud it is. 


---
**karabas3** *July 13, 2014 09:09*

I built my M90 using widely available here short bushings selled as spare parts for grinders. It is quiet enough. But Y axis is always much louder in such printers.


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/4wMifWShpUr) &mdash; content and formatting may not be reliable*
