---
layout: post
title: "So I couldn't help myself and I tore apart a perfectly working Eustathios to install a modified carriage that has mounts for a 40mm fan to quiet it down a bit"
date: February 04, 2018 15:23
category: "Discussion"
author: Brad Vaughan
---
So I couldn't help myself and I tore apart a perfectly working Eustathios to install a modified carriage that has mounts for a 40mm fan to quiet it down a bit.  Of course, now I'm struggling to get it back to its former glory.  The carriage movement isn't quite as smooth, but not binding terribly.



In any event, circles are slightly out of round, and they're fairly consistently out of round with a flat spot on the left and a bulge in the top right.



Below is a picture, but it will be hard to notice the issues.  When printing figures or more organic shapes, this isn't noticeable, but when printing circles, it becomes more obvious.



Here is a quick rundown of where my head is at right now, but if anyone has any "ah hah" moments, I'd love the help.





Troubleshooting so far:

1. Verified cross rods are straight (i.e., not bent; roll flat on granite counter)

2. Verified (as best I can) the rods are at 90 degrees from each other when installed in the carriage

3. Verified top of frame is square (measuring diagonally in both directions)

4. I've loosened the lower rod mounts and run the carriage around a several times as **+Eric Lien** shows in his cross rod gantry alignment video

5. Run break-in code, albeit for only 20-30

6. Lowering acceleration to 6K, then down to 3K without any perceptible improvement

7. Tried speeds ranging from 100mm/s to 25mm/s

8. Increasing current of stepper drivers/motors



Observations:

1. The gantry binds a bit more on the right (X 225 and beyond) than the left; front to back provides the same resistance across the entire travel

2. "Flat" spot in circles seems to imply the print head is not moving as far as it needs to in the X axis (binding, missing steps, other?)



Next steps:

1. Recheck for square on all faces of the printer frame (not just the top)

2. Print on different areas of the print bed; 1 or 2 tests on the bottom left didn't show any difference, and that's where the carriage travel is the smoothest, but worth more testing

3. Test speeds, acceleration, and current more deliberately (i.e., build a matrix of the 3 and test different combinations, noting quality)

4. Update firmware and take advantage of the new[er] mm_max_arc_error config value vs. the old[er] mm_per_arc_segment 



![images/0a9a8d9f5ca602afa956e4ec00f9693f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0a9a8d9f5ca602afa956e4ec00f9693f.jpeg)



**Brad Vaughan**

---
---
**Eric Lien** *February 04, 2018 19:48*

As you have noticed... The main culprit is your X-axis movement. The flat spot you are seeing is most likely because the coefficient of static friction is higher than the coefficient of kinetic friction between bronze and steel. This will be exacerbated by and misalignment in the system causing a side load on the bronze bushings. Essentially while its moving friction is low. But as the x axis reaches zero velocity (like is does at the apex along the left side of your circle) a friction force spike is imparted into the carriages motion. This causes a lag in the movement at the apex until it breaks through the static friction to head in the other direction. All I can say is align, break-in, test, align, break-in, test... Rinse repeat. It is the worst part of the build on cross-rod gantry printers. These large printers with tight tolerances, and parts needing to align with so many degrees of freedom... Well as you know it's a pain.



But like I always tell people: keep at it. When it's right... You will know. I know it sucks to feel like you moved backwards after the carriage change. But give it time. You will get there.



Another thing to check is belt tension. You can see similar issues on circles if you have an elastic system (i.e. backlash in the belts at an axis direction change).


---
**Brad Vaughan** *February 04, 2018 20:01*

Good point on the coefficient of friction on the apex/axis direction changes.  I hadn't thought of that.  That makes much more sense that it exhibits the same behavior, regardless of how the part is oriented or where it so on the bed.



Belts seem good from a tension standpoint.  I'm not noticing any backlash.



I've been running the break-in code at 150mm/s for the past hour.  I'll probably let it go for another hour, then run through the alignment process and use that opportunity to recheck for square on the faces just to be sure I haven't torqued it in any direction after several iterations of loosening/tightening everything.  My OCD is kicking in not having the motor belt guards on anyway :)



I'll rinse and repeat a few times as necessary.  Thanks!


---
**Dennis P** *February 05, 2018 07:50*

Did you by change flip-flop the bearings or the rods? From other life experiences, even flipping them end for end, as they were part of a previously mated system could  give you fits. I would not change them  after are redoing the break-in.  I would in the future mark them as best i could with carbide scribe on the ends and keep track of the bearings. 




---
**Dennis P** *February 08, 2018 23:33*

Brad- any progress on this? Curious how it worked out for you. 


---
**Brad Vaughan** *February 08, 2018 23:53*

I did run the break-in code for a while, went back, loosened the lower rail mounts, and let it find where it wants to be, but I haven't had a chance to test the progress. It feels a little better,but still not where it needs to be. I won't get to work on it until this weekend.



BTW, good suggestion on marking the positions of each rail when you get it dialed in. I'm fact, I may swap them around and retry things if I can't get it where I want it.


---
**Brad Vaughan** *February 27, 2018 01:20*

Sorry for the necro-post, but I wanted to get back to you **+Dennis P**.  I think my issue may have been that I squeezed the outer rods in a bit when tightening down the screw and captured nut that holds the gantry rods in the belt tensioners.



Either way, I swapped the gantry rods, ran the break-in code for 1.5 hours, and it's much, much better.



Circles seem circular.  I printed a nickel test fit at 75mm/s and at 40mm/s.  Both came out well.  Obligatory #benchy looked fine (steam stack at the top is nice and round).



Back in business.  It seems like you're making good progress as well.  Best of luck!


---
**Eric Lien** *February 27, 2018 02:03*

**+Brad Vaughan** thanks for the follow-up. I was hoping you had gotten everything figured out.


---
**Dennis P** *February 27, 2018 02:24*

**+Brad Vaughan** i am trying to sort out my own alignment and break in issues. Would you  be willing to run the break-in cycle scripts I posted in the other thread? 




---
*Imported from [Google+](https://plus.google.com/109037736098991448813/posts/V61TmjrnvCW) &mdash; content and formatting may not be reliable*
