---
layout: post
title: "Hey guys thanks for the add. Im currently on the planning portion of building a Herculien"
date: February 02, 2015 16:42
category: "Discussion"
author: Derek Schuetz
---
Hey guys thanks for the add. Im currently on the planning portion of building a Herculien. This will be my 4th printer assembly (currently have a 3ft Kossel). I'm a little hesitant on printing the parts my self as my current printer dimensional accuracy is not that great and ABS warping is a problem since there's no enclosure. Would anyone be able to point me to a place to get the printed parts or perhaps give me a price and print them for me?

Thanks in advanced





**Derek Schuetz**

---
---
**Dat Chu** *February 02, 2015 17:09*

You can join the list of print it forward program. If you show a pix of your already purchased parts for the build, I think **+Eric Lien**​ can be persuaded to front you the abs parts if you would print the same set for someone else once your printer is  complete. 


---
**Derek Schuetz** *February 02, 2015 17:25*

Cool good to know thank you! One less thanking to worry about


---
**Eric Lien** *February 02, 2015 18:50*

I would love to. But a full set takes around 1 week of printing and prep. I use my printer for work mostly, on won't have an opportunity for a while.


---
**James Rivera** *February 02, 2015 19:44*

Either [https://www.3dhubs.com](https://www.3dhubs.com) or [http://www.makexyz.com](http://www.makexyz.com) might be an option, if one of them has a 3D printer near you. (Disclaimer: I have an account on 3DHubs but I have not actually used either one).


---
**Dat Chu** *February 02, 2015 19:56*

You can potentially get one set made by shapeways I suppose.


---
**Derek Schuetz** *February 02, 2015 19:59*

about how long are we talking **+Eric Lien** 


---
**Eric Lien** *February 02, 2015 20:28*

**+Derek Schuetz** I probably have a month of work lined up. Most work prints take multiple days because I run it slow to avoid issues. Nothing is worst than loosing a print to impatience that took multiple days and spools to create. I wish I could post pics of them, but it is customers prototype I.P., so no sense catching the rather of corporate lawyers for bragging rights.


---
**James Rivera** *February 02, 2015 20:53*

**+Dat Chu** Yikes! Shapeways would be expensive!


---
**Dat Chu** *February 02, 2015 20:57*

I wish I could print it for you Derek. I am still in the process of building mine. And I need to get a vapor thingibabob working.


---
**Derek Schuetz** *February 02, 2015 21:00*

Vapor thingibabbob?


---
**James Rivera** *February 02, 2015 21:26*

ABS Vapor smoothing.

[http://lmgtfy.com/?q=abs+vapor+smoothing](http://lmgtfy.com/?q=abs+vapor+smoothing)


---
**Daniel Salinas** *February 02, 2015 21:59*

**+Derek Schuetz**​ what is your timeline?


---
**Derek Schuetz** *February 02, 2015 22:27*

Honestly if I can know I can secure the printed parts I can purchase everything. I already have set up my PO with misumi and have my McMaster cart saved. Everything else is from several websites but I can get started on the frame assembly. And then Start squiring motors and electronics


---
**Daniel Salinas** *February 02, 2015 23:41*

Spoke with Derek offline, he's shipping me some filament and I'll be printing his parts for him.


---
**James Rivera** *February 02, 2015 23:49*

This community is awesome.


---
**Eric Lien** *February 03, 2015 00:04*

**+Daniel Salinas** you are going to have everything dialed in printing your set that by the time you start printing his you might get jealous ;) That's what happened on the set I made for "print it forward". I almost hated to part with them :)


---
**Daniel Salinas** *February 03, 2015 02:08*

**+Eric Lien** yeah about midway through my prints I got my steps per mm dialed in so sweetly I'm tempted to go back and reprint the first half of my parts :)  Since I'm gonna be taking this really slow and doing pics for a build manual I can use the first run parts for those but by the time I get ready to build the thing I bet I will have already reprinted them.


---
**Daniel Salinas** *February 03, 2015 02:16*

For me the big challenge is the tall parts.  Really just the corner brackets.  they're so big and my printer isn't enclosed so I kept getting delamination issues.  I think I've got that all sorted.  If not I'll just do like we talked about and put a box around the printer to print those.


---
**Derek Schuetz** *February 03, 2015 02:36*

My best solution for that was to coat the bed with a nice thick layer of abs slurry. That's how I printed all my Kossel parts


---
**Daniel Salinas** *February 03, 2015 02:39*

no the delamination was happening between upper layers.  I have no issues with bed adhesion since I moved to glue stick.  0 warping since then.  the last set of corner brackets I printed had no delamination so I think I've got it mostly sorted but I still vapor smoothed them a little bit just to be sure.


---
**Daniel Salinas** *February 03, 2015 02:41*

ABS slurry didn't work for me.  Parts pulled off of it mid print.  I tried 4 different "recipes" and none worked.  Hair spray didn't work for me but it works like a champ for **+Eric Lien**   I think it's really just a matter of environment.  I live in Texas so normally I don't think maintaining a good print temp is gonna be hard but we're in our 2-3 cool months right now which makes printing ABS and Nylon a little harder.


---
**Daniel Salinas** *February 03, 2015 02:43*

The genius that designed my home put the main air intake for the central air right outside the door of my office so in the summer it sucks all the cool air out of the room and in the winter it can get down into the 50's in the room.  I just put an oscillating space heater blowing near the printer and that keeps cold drafts off of it and the small corner of the room where the printer is a little more even.  Then things started working out great for me.


---
**Eric Lien** *February 03, 2015 03:29*

Once I went Suave Max Hold hairspray I never looked back. But yes delamination is all environment driven. That's why enclosed printers are nice :)﻿


---
**Daniel Salinas** *February 03, 2015 03:38*

And thats the reason I'm building a Herculien :-)


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/WBpMdNZKf2W) &mdash; content and formatting may not be reliable*
