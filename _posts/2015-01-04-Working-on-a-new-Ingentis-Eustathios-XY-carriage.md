---
layout: post
title: "Working on a new Ingentis/Eustathios XY carriage"
date: January 04, 2015 15:59
category: "Show and Tell"
author: ThantiK
---
Working on a new Ingentis/Eustathios XY carriage.  I don't know solidworks for crap, so I've been figuring it out as I'm going along.  This is how far I am right now.



The idea (for me) is that I don't need that belt tensioning mechanism, and in order to fasten the belts I will have a clamp on top.  Also, the way things are cut out right now, doesn't hinder the clamping functionality of the original block.  I also wanted some more natural curves, instead of just cube on top of cube on top of cube.  Fillets <i>everywhere</i> when I'm done. :D

![images/7d5143407451a7b401451da3e61865f9.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7d5143407451a7b401451da3e61865f9.png)



**ThantiK**

---
---
**ThantiK** *January 04, 2015 17:05*

[https://drive.google.com/file/d/0ByxvvRr2A-puTjI4UFZ5WkJUQ1k/view?usp=sharing](https://drive.google.com/file/d/0ByxvvRr2A-puTjI4UFZ5WkJUQ1k/view?usp=sharing) is what I have currently.  Will export in STEP/IGES later.  I still need to figure out the best way to fasten the top.  Either way, should have more belt clearance with this one, and it should look a little better.


---
**Joe Spanier** *January 04, 2015 17:36*

All the fillets


---
**Eric Lien** *January 04, 2015 19:50*

Looks nice. But how do you plan to tension the belt? I am all for new designs... But the rod ends work so well right now and tensioning is so easy with the current design.


---
**ThantiK** *January 04, 2015 19:57*

**+Eric Lien**​, lived without the need to tension belts constantly for a while now.  Once they're set, they're set, generally. 


---
**Eric Lien** *January 04, 2015 19:59*

**+ThantiK**​ I agree. I have not adjusted mine since I installed it. I just mean the initial tension.﻿


---
**Eric Lien** *January 04, 2015 20:51*

I finally pulled up the google docs model with edrawings on my phone. Looks great. I like your use of chamfers vs fillets. Gives a great look and improves first few layers printability.


---
**ThantiK** *January 06, 2015 21:32*

Ya know, I pirated SolidWorks, but just noticed that eDrawings was a sldprt viewer for Android at $2.  I always say that I'll pay for things that are reasonably priced, so just gave them mah money. :)


---
*Imported from [Google+](https://plus.google.com/+AnthonyMorris/posts/8CuqezVsaU5) &mdash; content and formatting may not be reliable*
