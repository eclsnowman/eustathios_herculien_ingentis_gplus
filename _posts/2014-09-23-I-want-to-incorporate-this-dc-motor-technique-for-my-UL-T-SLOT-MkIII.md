---
layout: post
title: "I want to incorporate this dc motor technique for my UL-T-SLOT Mk.III"
date: September 23, 2014 03:25
category: "Discussion"
author: D Rob
---
[http://www.3ders.org/articles/20140116-dc-motor-powered-rapy-3d-printer-made-in-korea.html](http://www.3ders.org/articles/20140116-dc-motor-powered-rapy-3d-printer-made-in-korea.html) I want to incorporate this dc motor technique for my UL-T-SLOT Mk.III. Can someone help me in the right direction. I will use a stepper motor for z still, but I can't code for crap, and firmware mods will definitely be needed. This change will help people be able to salvage motors from 2D printers to build 3D printers too. 





**D Rob**

---
---
**Liam Jackson** *September 23, 2014 09:57*

I guess we don't use this method because its difficult to get DC motors to be accurate without decent firmware and a fast feedback loop. Once you add in the cost of the feedback loop you don't save much cash vs just using steppers and increase the complexity!



I would like to see if BLDC motors could be used as cheap servos, but the low kV ones arent as cheap and driving them will probably cost more in hardware than a pololu.


---
**Michael Ball** *September 25, 2014 22:40*

I keep hearing this argument, but I've not found that to be true.  Proper encoders/gear ratio/end stops, and DC motor drive can be quite effective and high resolution. The added benefit, if using linear encoders, is the ability to snap back to location if the head is bumped.


---
**Chris Purola (Chorca)** *September 30, 2014 22:16*

The one thing that's somewhat prevented much movement there is that steppers work and are easy, and because of that we don't have any good open-source DC servo drives. I've been using GeckoDrive's G320X on my servo builds, and it works well, but you do have to size your motors properly, make sure your encoders work well, and tune things up right to make them work. Someone modded a makerbot ToM awhile back with a couple of them: 
{% include youtubePlayer.html id=wBnbQrs6JsQ %}
[Makerbot Thing-O-Matic with dc servo drive by Gecko g320x drivers](https://www.youtube.com/watch?v=wBnbQrs6JsQ)


---
**D Rob** *October 01, 2014 00:01*

I'm thinking maybe gut the ramps, by removing the steeper drivers I/o then add gecko interface ports. Make it 12v- 24v ready and have configurable profiles for linear and/or rotary encoders.


---
**Chris Purola (Chorca)** *October 01, 2014 14:47*

As far as "gecko interface" all you really need is just step/dir/gnd and you're good. I just made up a pin header to plug into where a stepper driver would normally go, and route via a wire to the gecko. You can optionally enable/disable the drive and have the drive report when it enters error state (if difference between commanded location and real location exceeds a certain amount) as well, would just require some more firmware work.

If you're using a GeckoDrive, the nice thing is that the RAMPS/Marlin doesn't care, it's transparent. You may have to use some different variables for jerk/speed/accel, but otherwise it acts like a stepper driver. All motor tuning is done on the Gecko.

The Y axis of my hueg mendel is driven by one: 
{% include youtubePlayer.html id=BsK9JfH_l6M %}
[MendelMax Mega Y axis](https://www.youtube.com/watch?v=BsK9JfH_l6M)


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/fth9YeS7mJK) &mdash; content and formatting may not be reliable*
