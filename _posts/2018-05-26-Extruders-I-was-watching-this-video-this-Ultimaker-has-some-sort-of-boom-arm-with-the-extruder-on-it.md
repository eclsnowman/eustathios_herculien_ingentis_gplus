---
layout: post
title: "Extruders- I was watching this video, this Ultimaker has some sort of boom arm with the extruder on it"
date: May 26, 2018 13:23
category: "Discussion"
author: Dennis P
---
Extruders- I was watching this video, this Ultimaker has some sort of boom arm with the extruder on it. Has anyone seen something like this? I have extra rods from mis-measuring my frame and some extra bushings.  I know Walter did an articulating arm with bearings but this seems a little bit simpler.  The notes say Bondtech, but I don't recognize it as such. 


{% include youtubePlayer.html id=lIJQ93Jyou4 %}
[https://www.youtube.com/watch?v=lIJQ93Jyou4](https://www.youtube.com/watch?v=lIJQ93Jyou4)  





**Dennis P**

---
---
**Scott Hess** *May 26, 2018 14:29*

Neat idea!  I guess the theory is the weight reduction on the existing linear bearings offsets the additional friction from the new bearing?  I almost want to reach in and put a quarter-circle support for a radial bearing at the end of the rod.


---
**Dennis P** *May 26, 2018 14:42*

i am trying to get my head around it- it looks promising. we can get square, hex or even splined shafting to drive the extruder. the extruder gear is what has me stymied right now. 




---
**Pete LaDuke** *May 28, 2018 12:04*

Still trying to understand what exactly this would do, but it appears to be a “Zero gravity extruder”.  [thingiverse.com - Zero Gravity Extruder Direct Drive by Gudo & Neotko by Neotko](https://www.thingiverse.com/thing:2348263)


---
**Dennis P** *May 28, 2018 12:15*

**+Pete LaDuke** That is it! I could not get thingiverse search to work. . It would shorten up the Bowden to an almost Direct drive length system. Walter has an articulated arm system doing similar. 

"Much still to write about how to print this, assemble and use it correctly"


---
**Pete LaDuke** *May 28, 2018 12:34*

**+Dennis P** I have seen Walters version and I agree that they appear similar, I’m just not sure they function the same.    After reading the full write up from Thingiverse this setup appears to be geared for printing very accurately with PLA, but not so much with ABS, or I’m assuming with other materials. 


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/2JJddiHTY9Z) &mdash; content and formatting may not be reliable*
