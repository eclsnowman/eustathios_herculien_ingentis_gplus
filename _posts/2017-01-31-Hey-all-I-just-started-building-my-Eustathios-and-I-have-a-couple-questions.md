---
layout: post
title: "Hey all. I just started building my Eustathios and I have a couple questions"
date: January 31, 2017 02:31
category: "Discussion"
author: Mike Smith
---
Hey all. I just started building my Eustathios and I have a couple questions. I have been using my own variation of the Prusa I3 for a while now and I have a touch probe for auto bed leveling. There are a few things I don't like about it and I have seen a lot of people using the proximity sensors and I think I want to use that on my new build instead of the touch probe. What is a good one to use and will they work with an aluminum bed with glass over it? also trying to source some good but inexpensive ball screws or lead screws and nuts. Also what is everyones take on ball screw verses lead screw pros and cons. really looking forward to having a printer where the bed only moves up and down. I'm thinking I might be able to get better prints with this design. Any help is appreciated. TIA 

![images/9ae949c7a23ef921cd0750f399ed4c90.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9ae949c7a23ef921cd0750f399ed4c90.jpeg)



**Mike Smith**

---
---
**Ted Huntington** *January 31, 2017 04:01*

some people put a thin plate of steel under the glass- I found that the inductive sensor I bought on ebay just would not work with an aluminum-only bed- the glass was too thick but there was no thinner glass I could find.


---
**larry huang** *January 31, 2017 17:24*

I think Ball screw have less play than lead screw, lower fiction when the load put on the axis is heavy. BTW,What is the purpose of a proximity sensor on Z axis ? 


---
**Mike Smith** *January 31, 2017 22:46*

**+larry huang** auto bed leveling. Some people have told that I don't need it.....that once the bed is leveled it's done but I print with abs, nylon, pc and the like with a heated bed and I have discovered that when the bed heats up the height between the bed and nozzle changes so I like to level the bed every once in a while.


---
**jerryflyguy** *February 01, 2017 01:37*

**+Eric Lien** has a link for the ballscrews, I used them and they are great (no comment on the lead screws as I've not used them). I didn't use a probe or sensor, just a simple limit switch. I level my bed about once a month. It just works, hot or cold. I only heat to about ~70C so maybe the higher temps cause warp issues? I used a simple laser cut alum plate. There is a better material to use (Mic6??) which is cast plate and is supposed to warp less. I'd think 3/8"(8mm) would be bullet proof as far as temp/warp issues?


---
**Mike Smith** *February 01, 2017 02:11*

**+jerryflyguy** you bring up a good point about the bed warp issue. I have auto bed leveling....or auto bed compensation whatever people want to call it....on my Prusa redesign but I think the reason I may have the fluctuation in the bed when it heats up is because my main bed is just a pcb heater with a piece of glass clamped to it. If I were to use something more rigid I most likely would not have those issues any more.


---
**larry huang** *February 01, 2017 07:19*

My first 3D printer is a Prusa i3, the heated bed or the Z axis gave me a  lot of trouble back then. I later made a coreXY printer with vertical moving platform, reinforced the 3mm aluminum bed with angle bar, then the z axis rarely cause any problem. when the bed is not leveled, I suppose printing on it just make the printed object with not leveled bottom, even with a proximity sensor?


---
**matumbo franco** *February 18, 2017 07:56*

i just have it at my home(casa)


---
*Imported from [Google+](https://plus.google.com/+MikeSmith/posts/GWVMJtakUVn) &mdash; content and formatting may not be reliable*
