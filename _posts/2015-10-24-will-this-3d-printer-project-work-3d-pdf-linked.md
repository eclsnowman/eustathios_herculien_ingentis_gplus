---
layout: post
title: "will this 3d printer project work. 3d pdf linked"
date: October 24, 2015 12:24
category: "Discussion"
author: E. Wadsager
---
will this 3d printer project work. 

3d pdf linked. 

[https://www.dropbox.com/s/nn6vahau0n7lwub/printer.PDF?dl=0](https://www.dropbox.com/s/nn6vahau0n7lwub/printer.PDF?dl=0)





**E. Wadsager**

---
---
**Eric Lien** *October 24, 2015 14:09*

**+Nathan Walkner** did you open it in actual acrobat an a desktop. Otherwise you cannot view a 3D PDF. I am on a phone now. I will look later today.


---
**Gústav K Gústavsson** *October 24, 2015 14:38*

Quick look, three 90 deg bends on bowden tube? Think that spells trouble.


---
**Bud Hammerton** *October 24, 2015 16:25*

**+Gústav K Gústavsson**​ The 90° bends are just for ease of drawing, the path is horizontal to vertical from spool to extruder then a simple 180° arc from extruder to hotend, basically no different than nearly all other bowden equipped printers.



**+E. Wadsager**​ Can't say much for the rest of the design, other than there is a lot of fabrication that needs to happen to build this, especially for the bed support. ﻿


---
**E. Wadsager** *October 24, 2015 18:05*

for the bed support i will use 

[http://www.byghjemme.dk/images/MILLARCO_HYLDEKNAEGT-UPROFIL-ELG-p.jpg](http://www.byghjemme.dk/images/MILLARCO_HYLDEKNAEGT-UPROFIL-ELG-p.jpg) 

they only need to be cut in lenght. 



they 2x 90 deg. bends on the tube is of couse only on the drawing. 



btw i use Selfgraphite bearings  for x and y axis. 


---
*Imported from [Google+](https://plus.google.com/103157635215674778495/posts/JC2aCzPn7Go) &mdash; content and formatting may not be reliable*
