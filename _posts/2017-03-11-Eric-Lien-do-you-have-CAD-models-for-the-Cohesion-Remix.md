---
layout: post
title: "Eric Lien do you have CAD models for the Cohesion Remix?"
date: March 11, 2017 03:31
category: "Discussion"
author: jerryflyguy
---
**+Eric Lien** do you have CAD models for the Cohesion Remix? Mounting brackets etc?





**jerryflyguy**

---
---
**Ray Kholodovsky (Cohesion3D)** *March 11, 2017 03:35*

Oh boy do we!



Should be there. As I told you there's also case models. If they are not posted yet I will get them to you. 



[cohesion3d.freshdesk.com - Support : Cohesion3D](https://cohesion3d.freshdesk.com/support/home)


---
**jerryflyguy** *March 11, 2017 03:52*

**+Ray Kholodovsky** thanks Ray, was hoping **+Eric Lien** had something specific for the Eustathios, if I had to go total enclosed I can but was hoping there might be something that isn't enclosed already out there


---
**Eric Lien** *March 11, 2017 12:58*

**+jerryflyguy** sorry man. 


---
**jerryflyguy** *March 12, 2017 02:59*

**+Eric Lien** no worries, I'll make one 😉


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/bQg44owbFAZ) &mdash; content and formatting may not be reliable*
