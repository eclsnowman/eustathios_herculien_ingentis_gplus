---
layout: post
title: "I wonder if Herculien can use these as well"
date: February 20, 2015 15:15
category: "Discussion"
author: Dat Chu
---
I wonder if Herculien can use these as well. Nvm, the bearing plate in the Herculien looks beefier. 



<b>Originally shared by ThantiK</b>



Found these, which basically look like the 3D printed versions that are being used for the Ingentis, except - hey, METAL!  They also have 10mm, 12mm diameters and an added benefit is <i>set screws!</i>



Probably gonna order 8 of these and replace my printed brackets with them.





**Dat Chu**

---
---
**Isaac Arciaga** *February 20, 2015 15:39*

I ran into the same one looking for alternatives for the Eustathios. Amazon (Prime) has them if you don't want to wait for China lead times. [http://amzn.com/B00H8QGIO6](http://amzn.com/B00H8QGIO6)


---
**Seth Messer** *February 20, 2015 16:39*

**+Isaac Arciaga** i love that they are marked as Prime shipping. :) so, for those of us that literally don't have any other parts for their Eustathios yet and would need parts printed anyway, would it be worth going ahead and getting this and accepting the lead time? i'd think being on the pay-it-forward list and waiting on someone's printer to open up + the print time would be just as long. Unless of course someone wants to be paid for time/materials/shipping to get bumped to the front of the list. <b>hint, hint</b> :)


---
**Tim Rastall** *February 20, 2015 17:14*

And half the price from China for the exact same thing. Just longer shipping time:

(4PCS)  KP08  Zinc Alloy  Pillow Block Bearing 8MM

[http://www.aliexpress.com/item/4PCS-KP08-Zinc-Alloy-Pillow-Block-Bearing-8MM/1383733704.html](http://www.aliexpress.com/item/4PCS-KP08-Zinc-Alloy-Pillow-Block-Bearing-8MM/1383733704.html)




---
**Tim Rastall** *February 20, 2015 17:15*

Oh.  And you'd want kp000, not kp08.﻿ (4PCS)  KP000  Zinc Alloy  Pillow Block Bearing 10MM

[http://www.aliexpress.com/item/4PCS-KP000-Zinc-Alloy-Pillow-Block-Bearing-10MM/1383668281.html](http://www.aliexpress.com/item/4PCS-KP000-Zinc-Alloy-Pillow-Block-Bearing-10MM/1383668281.html)


---
**Seth Messer** *February 20, 2015 19:44*

**+Tim Rastall** for the eustathios or for the herculien?


---
**Tim Rastall** *February 20, 2015 21:15*

**+Seth Messer** both use 10mm outer shafts AFAIK. 


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/fWGsTX4HNs3) &mdash; content and formatting may not be reliable*
