---
layout: post
title: "Already running out of tnut and m5 8mm"
date: June 01, 2015 13:28
category: "Discussion"
author: Dat Chu
---
Already running out of tnut and m5 8mm. Reinforcement coming soon from McMaster and Carr. In the mean time, I shall use openbuilds components. 

![images/6925cfeba60d93a245db9281c92dcb99.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6925cfeba60d93a245db9281c92dcb99.jpeg)



**Dat Chu**

---
---
**Brandon Satterfield** *June 01, 2015 15:02*

Looking good!


---
**Dat Chu** *June 01, 2015 16:32*

Thanks to your help **+Brandon Satterfield**​. Wouldn't have gotten to this stage without your help with the cutting. 


---
**Dat Chu** *June 03, 2015 16:42*

Half in end mill bit is coming today. Time to get these big holes done. 


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/LXbXETTjdbA) &mdash; content and formatting may not be reliable*
