---
layout: post
title: "Finally added part cooling fans I didn't want to glue the fans to carriage so I designed a little fixing clip that holds the fans"
date: January 11, 2018 21:56
category: "Mods and User Customizations"
author: Daniel F
---
Finally added part cooling fans

I didn't want to glue the fans to carriage so I designed a little fixing clip that holds the fans. It can be printed mirrored.



![images/54db0478617b1283315d4c96fc971332.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/54db0478617b1283315d4c96fc971332.jpeg)
![images/588eb1ad5a0f50fed1c2b2f0cf3bc954.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/588eb1ad5a0f50fed1c2b2f0cf3bc954.jpeg)
![images/e2b704937de33c0d9312e26c99d1b2bd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e2b704937de33c0d9312e26c99d1b2bd.jpeg)

**Daniel F**

---
---
**Maxime Favre** *January 12, 2018 07:20*

Nice addition !


---
**Dennis P** *January 12, 2018 20:28*

Good Thinking! If you can put the parts in the Community Mods area, I would like to use them on my carriage.  I just finished adding tapered bores to teh .stp model for M3 Heatserts and clearance screw holes for the hotend clamps and thickened the edge around the fan hole on the face. 


---
**Daniel F** *January 13, 2018 21:42*

**+Dennis P** added the part file to the Eustathios-Spider-V2/Community Mods and Upgrades


---
**Ryan Fiske** *January 13, 2018 22:54*

Digging it! Thanks!


---
**Dennis P** *January 14, 2018 05:00*

**+Daniel F** on Github right? I am looking here- [https://github.com/eclsnowman/Eustathios-Spider-V2](https://github.com/eclsnowman/Eustathios-Spider-V2)

I wonder if it needs moderation.




---
**Dennis P** *January 14, 2018 05:53*

Here is my carriage model from the step file **+Walter Hsiao** kindly included in his mods, with 

Fans from grabcad 

[https://grabcad.com/library/radial-cooling-5015-fan-50mm-dc12v-1](https://grabcad.com/library/radial-cooling-5015-fan-50mm-dc12v-1)

E3D from grabcad

E3D Hotend: [https://grabcad.com/library/e3d-v6-1-75-hotend-1](https://grabcad.com/library/e3d-v6-1-75-hotend-1)

and other part files from McMaster

bearings: [https://www.mcmaster.com/#2032N24](https://www.mcmaster.com/#2032N24)

M3 Heatserts: [https://www.mcmaster.com/#94180A333](https://www.mcmaster.com/#94180A333)



![images/559bc2e26c47bad42eb93b58253bd038.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/559bc2e26c47bad42eb93b58253bd038.png)


---
**aess ssm** *January 26, 2018 18:17*

روُعَهِہ‏‏




---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/YPiZWFvZsgj) &mdash; content and formatting may not be reliable*
