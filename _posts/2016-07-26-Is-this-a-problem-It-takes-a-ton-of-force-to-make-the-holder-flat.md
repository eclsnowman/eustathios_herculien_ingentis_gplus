---
layout: post
title: "Is this a problem? It takes a ton of force to make the holder flat"
date: July 26, 2016 01:22
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
Is this a problem? It takes a ton of force to make the holder flat. Printed on a MakerGear M2 with PLA, 3 shells, 80 percent infill.

![images/f65f5fb5ab75f5ed812dab63793799b1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f65f5fb5ab75f5ed812dab63793799b1.jpeg)



**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *July 26, 2016 01:31*

My guess is your ID came in undersized. The part should not open up that far... So the bearing to ID tolerances are off. You could reprint adjusting the settings to get a better fit, or ream the ID to fit better.


---
**Stefano Pagani (Stef_FPV)** *July 26, 2016 01:54*

**+Eric Lien** Ok, I can't reprint the part, and this means that all of my parts are off... Could I heat gun the bearing/part or use Ethyl Acetate? (Or something like that forgot what it's called) to melt the PLA?


---
**Eric Lien** *July 26, 2016 02:04*

Try heat first, but one problem you might run into is that now the bearing isn't held at the proper distance from the Extrusion so you will get slight amount of binding at top and bottom and bowing of the vertical rods in the middle.


---
**Stefano Pagani (Stef_FPV)** *July 26, 2016 02:07*

Ok, I'll try that tomorrow, I could also put the bearing in and then heat the part so the plastic stretches at the stress points.


---
**Stefano Pagani (Stef_FPV)** *July 26, 2016 02:20*

**+Eric Lien** Thanks for the help, if I do decide to ream, what size should I ream to?


---
**Eric Moy** *July 27, 2016 20:19*

Measure the OD of the bearing and ream to just under that


---
**Stefano Pagani (Stef_FPV)** *July 28, 2016 12:04*

Ok Thanks, I cant tune the printers (there my schools) so ill just edit the model and make the hole a bit bigger


---
**Christian Gooch** *August 04, 2016 16:51*

Don't be afraid to talk to your teacher, or whoever manages the printers. I can't see them having any issues letting you adjust settings if the printer is off.  They probably don't have the time and no one else has noticed.  In the long run it will probably be easier than modifying all the models 


---
**Dave M** *August 11, 2016 20:48*

Won't the reamer ream out to exactly the OD of your bearing?  But the bigger problem is going to be getting the reamer setup concentric to the hole you want to ream out.  To save time on a reprint, iterate more quickly by taking a smaller section of your actual part to get the ID correct.


---
**Eric Moy** *August 22, 2016 09:54*

The ranger should find the center of the existing hole. Either use a hand drill with the part in a vice, or hill the part free while the reamer is in a drill press


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/QjiZFed248Q) &mdash; content and formatting may not be reliable*
