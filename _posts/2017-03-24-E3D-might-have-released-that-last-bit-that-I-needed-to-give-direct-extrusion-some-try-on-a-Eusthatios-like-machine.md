---
layout: post
title: "E3D might have released that last bit that I needed to give direct extrusion some try on a Eusthatios like machine"
date: March 24, 2017 18:43
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
E3D might have released that last bit that I needed to give direct extrusion some try on a Eusthatios like machine. :)




{% include youtubePlayer.html id=xfpvm9z28Qk %}
[https://www.youtube.com/watch?v=xfpvm9z28Qk&mc_cid=53436fc10b&mc_eid=7a5d7e4177](https://www.youtube.com/watch?v=xfpvm9z28Qk&mc_cid=53436fc10b&mc_eid=7a5d7e4177)



Probably not yet for me as I'm still celebrating my final success in cooling and tuning in some stuff but definitely looks appealing to me.





**Frank “Helmi” Helmschrott**

---
---
**Maxime Favre** *March 24, 2017 18:54*

I was thinking about a bondtech BMG with this small factor hotend ;)


---
**Frank “Helmi” Helmschrott** *March 24, 2017 18:54*

And how did you think you'd do cooling on that?




---
**Jeff DeMaagd** *March 24, 2017 21:37*

I'd be interested in trying a mashup of BMG and the Aero heat sink.


---
**jerryflyguy** *March 25, 2017 01:46*

I think your going to have a heap of trouble cooling this with anything but a OEM setup. As soon as I see it listed in N.A. I'll order a conversion kit. I think this is the key to making direct viable. I've got to find a much smaller stepper to drive it, the one I'm using right now is waay over kill.


---
**Pete LaDuke** *March 26, 2017 03:31*

While, I'm in the mist of building my Eustathious printer, this is a curiosity. I'm headed to MRRF tomorrow, and I hear that e3d is there so I'll have to ask some more questions.  Looks like a step in the right direction.




---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/PAqvKG5hFrF) &mdash; content and formatting may not be reliable*
