---
layout: post
title: "Got these today! Back to printing non-stop after I get it installed"
date: April 07, 2016 18:46
category: "Discussion"
author: Brandon Cramer
---
Got these today! Back to printing non-stop after I get it installed. :)

![images/7785eb0884daa4bb9010c2faed0f04cc.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7785eb0884daa4bb9010c2faed0f04cc.jpeg)



**Brandon Cramer**

---
---
**Eric Lien** *April 11, 2016 12:35*

**+Zane Baird**​ said they heat up way faster. Do you have the same experience **+Brandon Cramer**​?


---
**Brandon Cramer** *April 11, 2016 15:33*

**+Eric Lien** It seems like it heats up much faster. About 90 seconds to get up to 220 degrees Celsius. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/RxrnmsLneGH) &mdash; content and formatting may not be reliable*
