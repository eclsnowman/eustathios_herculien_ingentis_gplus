---
layout: post
title: "Well, I have to admit that I am horrible at cad"
date: May 26, 2015 09:57
category: "Discussion"
author: Gus Montoya
---
Well, I have to admit that I am horrible at cad. I am trying to varify the measurements on this cad drawing. But I can't seem to do it without screwing up the model in solidworks. Can any one please tell me what the distance measurents are for the following 5 distances?

![images/4ba9e0af81ca0cbb46ce37f20551b1f7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4ba9e0af81ca0cbb46ce37f20551b1f7.jpeg)



**Gus Montoya**

---
---
**Gus Montoya** *May 26, 2015 09:58*

I also forgot to ask for the measurement of the middle beam of the build plate support. I need to drill out holes to screw my beam in. 


---
**Daniel F** *May 26, 2015 10:50*

I would use the Z shaft as a reference, not the mounts. The shaft needs to be placed in the middle of the extrusion. The Z-motor position will be defined automatically, just slide it to tension the belt. For the middle beam I'm not sure as my implementation is different here but I would expect it to be in the "middle" of the two other bed beams. Otherwise you could use corner brackets, the you can move it if necessary.


---
**Derek Schuetz** *May 27, 2015 13:32*

I would just wing it. The electronics don't need to be exact they just need to be held in place


---
**Ben Malcheski** *May 27, 2015 21:44*

You can measure using the measuring tool under the evaluate tab. You can also select edges, faces, a pair of parallel faces, etc and it will give you some dimensional info in the lower right of the SW window.


---
**Gus Montoya** *May 28, 2015 00:29*

I'm still looking into it. I started taking a solidworks course. Hopefully I can get proficient at it rather quickly.


---
**Ben Malcheski** *May 28, 2015 13:02*

Also getting familiar with the pop up menu when you select the face of a part or a part/feature in the feature tree is invaluable for being fast. Additionally, expanding the Show Display Pane by clicking the ">>" at the upper right of the Feature Tree, Property Manager, Configuration Manager, Display Manager area will give you some nice hide/show/transparency, edge dispaly, and color options. The Section View is also great to check interference or see sketch planes that may be obstructed otherwise.


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/jYyEVhJGo7P) &mdash; content and formatting may not be reliable*
