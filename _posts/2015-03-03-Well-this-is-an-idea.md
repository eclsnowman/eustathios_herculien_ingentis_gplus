---
layout: post
title: "Well this is an idea"
date: March 03, 2015 08:27
category: "Discussion"
author: Gus Montoya
---
Well this is an idea. 



[https://www.kickstarter.com/projects/1661525705/z-unlimited-add-on-for-ultimaker](https://www.kickstarter.com/projects/1661525705/z-unlimited-add-on-for-ultimaker)





**Gus Montoya**

---
---
**Daniel Salinas** *March 03, 2015 15:15*

I think it's really cool but highly specialized.  Not really worth kickstarting IMO.


---
**Gus Montoya** *March 03, 2015 19:07*

I agree it's not kickstartable,but it's a good idea none the less.


---
**Mike Miller** *March 04, 2015 02:08*

That was surprisingly awesome. 


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/LmQrjPFwanH) &mdash; content and formatting may not be reliable*
