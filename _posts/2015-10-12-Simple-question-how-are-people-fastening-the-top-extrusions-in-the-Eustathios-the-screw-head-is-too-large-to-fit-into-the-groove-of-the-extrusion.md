---
layout: post
title: "Simple question: how are people fastening the top extrusions in the Eustathios- the screw head is too large to fit into the groove of the extrusion"
date: October 12, 2015 00:07
category: "Discussion"
author: Ted Huntington
---
Simple question: how are people fastening the top extrusions in the Eustathios- the screw head is too large to fit into the groove of the extrusion. Do we need to drill the hole wider, grind the screw head smaller, or just use long screws and tighten them on the outside of the extrusion?





**Ted Huntington**

---
---
**Jason Smith (Birds Of Paradise FPV)** *October 12, 2015 00:09*

Slide the screw heads in from the ends of the extrusion. Use the holes to access the screw heads in order to tighten them. 


---
**Ted Huntington** *October 12, 2015 00:16*

doh! damn, that's ingenius. ok


---
**Ted Huntington** *October 12, 2015 00:17*

thanks


---
*Imported from [Google+](https://plus.google.com/101412962363141430834/posts/5KGvAqpAZcL) &mdash; content and formatting may not be reliable*
