---
layout: post
title: "Using Simplify3D iam able to design parts in a way I just would not have before"
date: April 24, 2014 23:04
category: "Discussion"
author: Wayne Friedt
---
Using Simplify3D iam able to design parts in a way I just would not have before. The supports just work, impressive! 



![images/d17ddda9f303f17ac267666d7ef9388d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d17ddda9f303f17ac267666d7ef9388d.jpeg)
![images/0fef5c0b2b82506fcb070bc86dcc81f3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0fef5c0b2b82506fcb070bc86dcc81f3.jpeg)
![images/6c007401fb83bfb0dd10ab861e9c31f7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6c007401fb83bfb0dd10ab861e9c31f7.jpeg)
![images/32d62c6128f401368fd3218626978838.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/32d62c6128f401368fd3218626978838.jpeg)
![images/f995a0a9887fd763b16cecd76ef8c9e7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f995a0a9887fd763b16cecd76ef8c9e7.jpeg)

**Wayne Friedt**

---
---
**Eric Lien** *April 24, 2014 23:16*

The support structure works so much better than cura and slic3r its rediculous.﻿


---
**Wayne Friedt** *April 24, 2014 23:22*

There is no comparison. I have you to thank really. **+Eric Lien**  I would of never thought about buying that if i hadn't seem your post before. One thing i am researching now is my part was scaled down some and doesn't fit. I may have done that and not know it as i am still learning the program. 


---
**Denys Dmytriyenko (denix)** *April 24, 2014 23:28*

Repetier doesn't slice, it uses either Slic3r or Skeinforge...


---
**Wayne Friedt** *April 25, 2014 00:26*

**+Eric Lien** Have you had an issue with scale. I have it set to 1.0000 now but my part comes out small. 


---
**Eric Lien** *April 25, 2014 00:49*

**+Wayne Friedt** nope I have scaled several items all with good results. The lion was at x2.5. I did some calibration items at x0.5.


---
**Riley Porter (ril3y)** *April 25, 2014 00:56*

**+Eric Lien** What "post" is **+Wayne Friedt**  referring to?  I have had pretty good success with cura's supports.  I would love to see a more indepth posting if its available.


---
**David Gray** *April 25, 2014 01:16*

**+Riley Porter** I believe this is the post. [https://plus.google.com/109092260040411784841/posts/EtLkbChjfh6](https://plus.google.com/109092260040411784841/posts/EtLkbChjfh6)


---
**D Rob** *April 25, 2014 01:17*

that support looks outstanding


---
**Wayne Friedt** *April 25, 2014 03:48*

**+D Rob** Thanks and i comes OFF!


---
**David Gray** *April 25, 2014 04:11*

I bought this software after watching this vid. pretty good overview of the software. 
{% include youtubePlayer.html id=D968RL1Z6l4 %}
[Simplify3D Demo and Review (Software for 3D Printing)](https://www.youtube.com/watch?v=D968RL1Z6l4&feature=youtu.be)


---
**Wayne Friedt** *April 25, 2014 04:26*

I now can design parts more freely i guess you call it. Not so constrained to have to CAD the part differently than i had visioned because it was impossible to print it in that configuration. As my example. I had that CADed differently before, but this is how i wanted to CAD it and now that is possible. If i can just figure out why my part is scaled smaller than actual size ill be in.. 


---
**D Rob** *April 25, 2014 04:29*

omg! how long have I been begging for support selection like in the video. It was my idea... pay me lol. I am probably getting this


---
**D Rob** *April 25, 2014 04:34*

back to basics. try a fresh install. both the simplify and try reflashing your firmware


---
**D Rob** *April 25, 2014 04:35*

paying careful attention to all fields of course


---
**D Rob** *April 25, 2014 04:47*

is it just a slicer and host? do they have firmware too? what firmwares are supported?


---
**Wayne Friedt** *April 25, 2014 04:54*

Not sure but just check here. [http://www.simplify3d.com/](http://www.simplify3d.com/)


---
**Tim Rastall** *April 25, 2014 05:27*

The UP! Printers have been doing supports like this for ages.  It's why I've been so frustrated with Cura and Slic3r,  the developers just dont seem to see how critical snap-away supports are.


---
**Eric Lien** *April 25, 2014 05:57*

**+D Rob** works with marlin and repetier.﻿ that I have tested.


---
**Wayne Friedt** *April 25, 2014 09:53*

Problem solved of the parts scaled down. From switching between several different kinds of printers my X and Y steps were off by 8. So that accounts for everything being to small. I thought that when you plugged into a printer the EEPROM setting were being read from the EEPROM and not what is stored in Repeiter. So i am not sure how it got changed but anyway that the scoop.


---
**Andreas Thorn** *April 25, 2014 12:56*

I'm very happy with Simplify3D too. So far all support I have printed snaps right off.


---
**Chet Wyatt** *April 29, 2014 22:28*

That's funny, I was about to ask them for a demo and saw the same video and just went ahead and bought it. It has some serious issues like all the rest, but overall I'm real happy. The supports, simulator,  the interface, and being able to apply different settings to multiple objects at different layers on the same print is kick ass.


---
*Imported from [Google+](https://plus.google.com/+WayneFriedt/posts/Lzjzg3xsMXj) &mdash; content and formatting may not be reliable*
