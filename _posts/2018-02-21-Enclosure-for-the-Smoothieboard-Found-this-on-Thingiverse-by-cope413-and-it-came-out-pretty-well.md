---
layout: post
title: "Enclosure for the Smoothieboard. Found this on Thingiverse by cope413, and it came out pretty well"
date: February 21, 2018 02:11
category: "Show and Tell"
author: Bruce Lunde
---
Enclosure for the Smoothieboard.  Found this on Thingiverse by cope413, and it came out pretty well. Still learning, still tuning, but this felt good. Cura 3.2.1,  200 C, 60 on bed, PLA.  Octoprint/OctoPi. Needs a touch of clean-up, still learning on printing support.

![images/437ed6b6a3dcd7c408aa6c79727ed9fa.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/437ed6b6a3dcd7c408aa6c79727ed9fa.jpeg)



**Bruce Lunde**

---
---
**wes jackson** *February 21, 2018 02:49*

Got the link?


---
**Bruce Lunde** *February 21, 2018 03:13*

**+wes jackson**  [thingiverse.com - Smoothieboard Case by cope413](https://www.thingiverse.com/thing:281561)




---
**Dennis P** *February 21, 2018 05:57*

are there 'bubbles' in the top layers over the cells of the infill? 

you might want to increase the infill density or increase the top layers


---
**Bruce Lunde** *February 22, 2018 01:39*

**+Dennis P** yes you are seeing bubbles,  I do think I need to increase the top layers like you mention.




---
**Dennis P** *February 22, 2018 03:00*

you might get more effect if you increase your infill percentage




---
**Bruce Lunde** *February 22, 2018 13:24*

**+Dennis P** I'll give that a try too.


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/Ji7hh3bHb5z) &mdash; content and formatting may not be reliable*
