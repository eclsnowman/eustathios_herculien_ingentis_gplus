---
layout: post
title: "I'm running into a problem. When running the gcode for break in I get a weird shuddering"
date: October 02, 2015 01:50
category: "Discussion"
author: Rick Sollie
---
I'm running into a problem. When running the gcode for break in I get a weird shuddering.  This happens mainly after moving along the x axis and switching to a diagonal movement.     Any ideas on where to start troubleshooting?





**Rick Sollie**

---
---
**Zane Baird** *October 02, 2015 02:03*

Triple check your alignment. If you search past posts in this community there as been quite a bit of discussion. Once you get it you'll know. Over time it will get smoother, but you need good alignment to start.


---
**Erik Scott** *October 02, 2015 02:49*

I'm having the same issue, though it's not only on the diagonals, only when the head is in a certain position (right rear corner) and only at slower speeds (~60mm/s). I can't figure it out for the life of me. 


---
**Jo Miller** *October 02, 2015 06:15*

i did some additional hairspraying on my glassplate without removing the plate from the Alu-bed.      Don´t do that ! , it swirls on the rods and makes them do that weird shuttering


---
**Erik Scott** *October 02, 2015 13:19*

Haven't used hairspray yet, so it's not that, at least in my case. 


---
**Eric Lien** *October 02, 2015 14:11*

There is an issue (I think) with the current carriage design. The issue is the cross over point of the two rods not being centered between the bushings. This causes the force applied for motion on the cross rods to be unequal depending on the direction of travel. Ideally the rods should cross at the center between the bushings. But since there needs to be a location for the hot end to pass through and mount the rod paths and bushing needed to be moved out from the center. One solution is to center the rods and move the hot end to one of the corners... But this makes the carriage bigger.



But I can confirm the carriage works just fine with everything in alignment, square and true, after break-in has completed. 



Another potential issue is the quality of the printed parts. No printer makes perfect parts, no matter how well tuned. So there is a compound of errors from each part in the gantry assembly that must be balanced using the available degrees of freedom during assembly. It takes a while, but when its right you just know. If things are correct, you should be able to disconnect the pulley to the motors, and it should move freely to every corner with one finger pushing. If there is any chatter during this test... You have more tuning to do. 



This portion of the build is the most difficult to get right. But it is easily the most critical, and the most beneficial if done correctly.



My advice is if you are running into issue go back to first principals, and make no assumptions.



1.) Is it square (check with a good square, and measure corner to corner across the whole frame).



2.) Is it parallel (use blocks of a known dimension and chech the cross rod relations to the side rods).



3.) Is it level ( set the upper side rods at a known equal distance to the top of frame, then allow the other corner bearing mounts to be aligned by the carriage by moving it to every corner and let the carriage determine the spacing. Do every corner this way then recheck.)



Best of luck guys.﻿


---
**Rick Sollie** *October 02, 2015 19:49*

Thanks everyone for the advice and insights. This started after 'fixing'an issue where my hot end was not planar to the bed. It was equidistant from the bed in three corners and not the fourth.  Turns out the bearings were high in that corner. I've checked and I'm out of square.


---
**Rick Sollie** *October 02, 2015 19:55*

The most of the extrusion was from another project so it was cut with a chop saw and some cross supports are slightly longer(+<s>.5mm here +</s>.75 there) enough to make it almost impossible to true the frame, even if I space the apart using the mounting brackets. Probably going to order some new extrusion for the parts that are shy.


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/WUjAPtcXeMn) &mdash; content and formatting may not be reliable*
