---
layout: post
title: "Am I just half asleep or their isn't a wiring diagram for the smoothieboard?"
date: June 22, 2015 09:10
category: "Discussion"
author: Gus Montoya
---
Am I just half asleep or their isn't a wiring diagram for the smoothieboard?  Any one have pic's?





**Gus Montoya**

---
---
**Mike Thornbury** *June 22, 2015 09:32*

It's pretty clearly marked on the board, but here: [http://i.imgur.com/keOPkoC.png](http://i.imgur.com/keOPkoC.png)


---
**Gus Montoya** *June 22, 2015 19:57*

I like to double make sure, I've already spent some good $ on replacement parts because I didn't double make sure. I should have been done with my build ages ago. **+Derek Schuetz** finished his build at 1/3 the time it's taking me. And I started WAYYYYYY before him.


---
**Mike Thornbury** *June 22, 2015 20:24*

I'm about to hack mine to accept a wifi transceiver... Tomorrows tech challenge.



Good luck with your build -measure twice, cut once :)


---
**Bruce Lunde** *June 23, 2015 01:02*

**+Gus Montoya**  Have you followed this link, I had the system on-line and moving within 15 minutes of attaching the servos, attaching usb cable,  and putting pronterface on my PC via this web page:  [http://smoothieware.org/3d-printer-guide](http://smoothieware.org/3d-printer-guide)   



The wiring is clearly explained, and the web interface makes it easy to work with. You do need to enable the ethernet, and use usb to get to it to find the IP address, then it is simple to access: [http://ip_of_smoothie/](http://ip_of_smoothie/)


---
**Gus Montoya** *June 23, 2015 02:18*

I havn't yet. I am wiring things up now while I try to find someone to save me with 2 alignment tools for the gantry. 


---
*Imported from [Google+](https://plus.google.com/+GusMontoyadaisosasen02/posts/Wo2t6eLvvWN) &mdash; content and formatting may not be reliable*
