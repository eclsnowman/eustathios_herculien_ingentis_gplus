---
layout: post
title: "Can't get Autodesk Fusion 360 to open Eric Lien 's Eustathios Spider V2 skp file"
date: March 18, 2015 00:25
category: "Discussion"
author: Oliver Seiler
---
Can't get Autodesk Fusion 360 to open **+Eric Lien** 's Eustathios Spider V2 skp file. Uploading through the app stalls every time and when I add it on the web page I can't open in in the app. Any ideas? Someone else with Fusion happy to share it with me?





**Oliver Seiler**

---
---
**Oliver Seiler** *March 18, 2015 00:26*

Oh, and I've cleared all the caches, re-installed the app, stroke a black cat in the moonlight.... nothing helps :(


---
**James Rivera** *March 18, 2015 00:29*

Wrong file! SKP is for Sketchup.


---
**James Rivera** *March 18, 2015 00:31*

Maybe the STEP files will work with Autodesk Fusion?


---
**Eric Lien** *March 18, 2015 01:17*

I just tried, and the x_t imported beautifully. Try that. Otherwise I will get a .f3d archive up soon.


---
**Oliver Seiler** *March 18, 2015 01:30*

**+James Rivera** Hmm, I managed to get the original Eustathios files loaded and am pretty sure I used the skp.


---
**Oliver Seiler** *March 18, 2015 01:42*

Thanks **+Eric Lien** x_t did the job!


---
**Eric Lien** *March 18, 2015 01:55*

Plus X_T is the closest format to native solidworks you can get. It's always my go to for export/import.


---
**Joe Spanier** *March 18, 2015 03:42*

Just drop the native Solidworks in. It will work beautifully. Sometimes the assembly constraints get bunged up but they are fairly easy to fix. Let me know if you have any issues. 


---
**Jeff DeMaagd** *March 18, 2015 04:21*

Maybe you really loaded a .stp before? Sketchup files aren't solid models.﻿


---
**Joe Spanier** *March 18, 2015 06:08*

They just added sketchup file support literally this weekend. So I doubt that's what you used before. Probably stp


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/6iQjGdz9K7B) &mdash; content and formatting may not be reliable*
