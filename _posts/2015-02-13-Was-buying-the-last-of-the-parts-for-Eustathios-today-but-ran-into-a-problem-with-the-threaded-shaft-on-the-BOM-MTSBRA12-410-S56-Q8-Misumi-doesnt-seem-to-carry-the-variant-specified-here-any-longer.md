---
layout: post
title: "Was buying the last of the parts for Eustathios today, but ran into a problem with the threaded shaft on the BOM: \"MTSBRA12-410-S56-Q8\", Misumi doesn't seem to carry the variant specified here any longer"
date: February 13, 2015 18:13
category: "Discussion"
author: Jim Wilson
---
Was buying the last of the parts for Eustathios today, but ran into a problem with the threaded shaft on the BOM: "MTSBRA12-410-S56-Q8", Misumi doesn't seem to carry the variant specified here any longer. The "S56" and "Q8" are what seem to be giving the search tool trouble.



[https://docs.google.com/spreadsheet/ccc?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing#gid=0](https://docs.google.com/spreadsheet/ccc?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing#gid=0)



Or is there something basic about this process I just don't understand when it comes to ordering machine components?





**Jim Wilson**

---
---
**Dat Chu** *February 13, 2015 18:16*

What I got from Misumi for the threaded shafts are 



MTSBRW12-480-F10-V8-S26-Q8 SCREW THREAD 2 48.69 97.38



I certainly hope I didn't misordered :( Especially since I did double check with Eric online at the time.


---
**Dat Chu** *February 13, 2015 18:20*

I was building a Herculien so it might be different. So when I search for MTSBRA12-410 on Misumi, I can then choose Q8 and S40. Looks like they no longer allow the stepped part to be that long.


---
**Joe Kachmar** *February 13, 2015 18:35*

Misumi no longer sells that exact part, however if you add a small keyway it can be ordered at the appropriate length. I believe MTSBRB12-410-S56-Q8-C3-J0 is a lead screw with a small (3mm) keyway at the base.



That's what I have in my list of parts-to-be-ordered anyway, so someone please correct me if I'm wrong.


---
**Eric Lien** *February 13, 2015 18:46*

**+Joe Kachmar** you got it. I am changing the z stage slightly on the Eustathios V2 varriant I am working on to help get around this.


---
**Jim Wilson** *February 13, 2015 19:17*

Thank you sirs!


---
**Seth Messer** *February 13, 2015 19:27*

again.. love this community. :)


---
**Seth Messer** *February 13, 2015 19:28*

**+Eric Lien** can't wait to see what you have in store for V2.


---
**Isaac Arciaga** *February 13, 2015 21:41*

**+Eric Lien** awesome! Are the changes in V2 primarily to accommodate for an easier accessible BoM? My BoM for the Eust Spider is complete and have already printed half the parts with the CF ABS! If there are performance or structural upgrades I'll wait before I print the remaining parts.


---
**Gus Montoya** *February 13, 2015 21:43*

**+Eric Lien**  So I should order that same part also? I need help on the part numbers for the linear shafts, bearings and bushings also. 


---
**Seth Messer** *February 13, 2015 21:50*

**+Isaac Arciaga** you have that BOM handy? at least for the deviations from Jason's original Eustathios BOM? Thanks!


---
**Isaac Arciaga** *February 13, 2015 22:04*

**+Seth Messer** It's the same BoM (what you have) used in Eric's Eustathios Spider variant. I used Eric's SolidWorks model to reference the part #'s. It's not the V2 he is currently working on to help with BoM accessibility.


---
**Gus Montoya** *February 13, 2015 22:45*

**+Isaac Arciaga**  I'm not good at solidworks, actually I don't know anything aside from opening the program and viewing files. Can you share your parts list for my build?


---
**Isaac Arciaga** *February 13, 2015 22:51*

**+Gus Montoya** It is the same BoM Jim linked on his original post. [https://docs.google.com/spreadsheet/ccc?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing#gid=0](https://docs.google.com/spreadsheet/ccc?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing#gid=0)


---
**Gus Montoya** *February 14, 2015 01:47*

**+Isaac Arciaga**  Thank you Issac, I'll order the parts.


---
*Imported from [Google+](https://plus.google.com/101778058628996936791/posts/jNPoShuCTVK) &mdash; content and formatting may not be reliable*
