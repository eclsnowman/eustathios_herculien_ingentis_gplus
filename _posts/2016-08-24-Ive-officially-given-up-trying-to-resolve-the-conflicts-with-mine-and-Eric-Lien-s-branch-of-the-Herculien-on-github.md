---
layout: post
title: "I've officially given up trying to resolve the conflicts with mine and Eric Lien 's branch of the Herculien on github..."
date: August 24, 2016 02:47
category: "Mods and User Customizations"
author: Zane Baird
---
I've officially given up trying to resolve the conflicts with mine and **+Eric Lien**'s branch of the Herculien on github... Instead, I'm hosting my mods in a separate repository that can be found here: [https://github.com/drtrain/Herculien-Mods](https://github.com/drtrain/Herculien-Mods)



These files should open for everyone with no conflicts and contain my most up-to-date mads as well as the originals (labeled as deprecated). Eventually I'll try and get around to merging my branch with Eric's but for now, this will do.





**Zane Baird**

---
---
**Bruce Lunde** *October 01, 2016 15:45*

Glad to see this, needing enclosure for my smoothieboard, thanks for publishing!




---
**Zane Baird** *October 01, 2016 17:57*

**+Bruce Lunde** Fair warning, Its a really tight fit in the enclosure. A little difficult to get in, but it works just fine. I used a leftover piece of acrylic to make the cover for the enclosure.


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/EYPJ33pc83c) &mdash; content and formatting may not be reliable*
