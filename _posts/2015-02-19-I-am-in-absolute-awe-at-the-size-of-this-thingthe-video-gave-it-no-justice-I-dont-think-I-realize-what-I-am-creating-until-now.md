---
layout: post
title: "I am in absolute awe at the size of this thing...the video gave it no justice I don't think I realize what I am creating until now"
date: February 19, 2015 09:23
category: "Show and Tell"
author: Derek Schuetz
---
I am in absolute awe at the size of this thing...the video gave it no justice I don't think I realize what I am creating until now

![images/1726cbae017541dbd1db792876c4682a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1726cbae017541dbd1db792876c4682a.jpeg)



**Derek Schuetz**

---
---
**Eric Lien** *February 19, 2015 12:49*

Looking great.


---
**Eric Lien** *February 19, 2015 13:05*

Do you feel the lid is too tall, that is the size of mine. I have a very gentle arch on the bowden... But others have mentioned they think its a bit too much.


---
**Eric Lien** *February 19, 2015 13:10*

Also I have a lid stop on my current build but as you can see from my pics my lid was made with 1x1 angle and glued in panels. So it doesn't work with the new hinge placement of the 20x20. If you come up with a good solution let me know, otherwise I should really get working on something new for the latest design.


---
**Derek Schuetz** *February 19, 2015 15:54*

I can see the lid being ok tall but I'm not sure by how much till I get the Bowden system installed. I'm not how much space there is when the carriage is closest to the extruders.


---
**Rick Sollie** *February 20, 2015 01:15*

How big a print area for this monster?


---
**Eric Lien** *February 20, 2015 02:52*

**+Rick Sollie** as shown 338x358y340z mm (13.3x14.1y13.6z"). There is some room to squeeze more out of it, but I haven't had the need yet.


---
**Eric Lien** *February 20, 2015 02:56*

**+Derek Schuetz** here is a pic when it's near the front right corner on mine: [http://imgur.com/whHdRwP](http://imgur.com/whHdRwP)


---
**Derek Schuetz** *February 20, 2015 03:00*

it doesnt seem like theres much space without strain on the filament


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/LyvBvjXLapH) &mdash; content and formatting may not be reliable*
