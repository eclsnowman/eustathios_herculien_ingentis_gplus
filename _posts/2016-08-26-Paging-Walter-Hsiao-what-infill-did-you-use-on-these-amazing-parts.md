---
layout: post
title: "Paging Walter Hsiao ...what infill did you use on these amazing parts?"
date: August 26, 2016 11:38
category: "Discussion"
author: Mike Miller
---
Paging **+Walter Hsiao**...what infill did you use on these amazing parts? 

![images/0e83e44bae258d82dbb44e455d509511.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0e83e44bae258d82dbb44e455d509511.jpeg)



**Mike Miller**

---
---
**Xiaojun Liu** *August 26, 2016 12:16*

I saw he noted in thingiverse. 100% infill I think.


---
**Stefano Pagani (Stef_FPV)** *August 26, 2016 14:55*

Yup, he uses 100




---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/7wB4AuG25fq) &mdash; content and formatting may not be reliable*
