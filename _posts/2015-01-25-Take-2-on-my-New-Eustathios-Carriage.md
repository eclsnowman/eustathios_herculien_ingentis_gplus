---
layout: post
title: "Take 2 on my New Eustathios Carriage"
date: January 25, 2015 06:24
category: "Show and Tell"
author: Eric Lien
---
Take 2 on my New Eustathios Carriage. I added a shark fin tab so I could mount a connector so electrical for the carriage could be modular/removable. I did this on my last version also and it worked out well (see photos). I have 3 versions of the STL. One designed for a Molex Microfit 3.0 8pin connector, one for a PCI-E 2x4 connector (larger but easy to get an extendor cable from your local PC store), and a version just for a ziptie for those that don't want it modular but want strain relief on the wires.



Files are here for now: [https://drive.google.com/folderview?id=0B1rU7sHY9d8qRWhOd2pydGxaUWc&usp=sharing](https://drive.google.com/folderview?id=0B1rU7sHY9d8qRWhOd2pydGxaUWc&usp=sharing)



I am working on redoing the entire solidworks assembly for Eustathios and cleaning things up to make the BOM easier. Once I do that I will push it out to the gitgub with the rest of the changes.



I opened up the clearance holes to 11.2mm so this carriage can work for HercuLien or Eustathios (just use different misalignment bushings per the needed shaft size).



The Molex Microfit connectors are PN# 430200801 & 430250800, and can be obtained here:



[http://www.digikey.com/product-search/en?x=0&y=0&lang=en&site=us&keywords=430250800](http://www.digikey.com/product-search/en?x=0&y=0&lang=en&site=us&keywords=430250800)



[http://www.digikey.com/product-search/en?vendor=0&keywords=430200801](http://www.digikey.com/product-search/en?vendor=0&keywords=430200801)



I still need to figure out which crimp pins I need for the microfit connectors since the parts above are the housings only. I get a little confused on the electrical side of things. I am much better at mechanical than electrical.



Hope you enjoy



![images/527cb8f5c562a259f8a3b59d6e33e63e.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/527cb8f5c562a259f8a3b59d6e33e63e.png)
![images/63857c9df8b8da5723395204662cb48d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/63857c9df8b8da5723395204662cb48d.png)
![images/32175888ca2288af49e2f3b458046e7b.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/32175888ca2288af49e2f3b458046e7b.png)
![images/e18b50ed3947ea8391d3eb516fbe6f32.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e18b50ed3947ea8391d3eb516fbe6f32.jpeg)
![images/35fc2576651500ed46a33f03f59c9b07.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/35fc2576651500ed46a33f03f59c9b07.jpeg)
![images/024ec6dc34db0ef3cdbea04c1dc72f3a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/024ec6dc34db0ef3cdbea04c1dc72f3a.jpeg)
![images/d355a21f8d3f9dc5474b1696bc67b6db.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d355a21f8d3f9dc5474b1696bc67b6db.jpeg)
![images/106c1b4d9724b0623dac7f4736553592.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/106c1b4d9724b0623dac7f4736553592.jpeg)

**Eric Lien**

---
---
**Eric Lien** *January 25, 2015 06:26*

**+Jason Smith** I think this should be good to try now.


---
**Wayne Friedt** *January 25, 2015 06:30*

Towards the bottom of you link they give the connector terminals related to each connector and are listed by the wire size used.


---
**Eric Lien** *January 25, 2015 06:37*

**+Wayne Friedt** see I knew the smart guys could point me in the right direction :)


---
**Brian Bland** *January 25, 2015 06:42*

Just as I was getting ready to print a modified version for linear bearings you posted the new version.  Have to modify the new version now.


---
**Eric Lien** *January 25, 2015 06:48*

**+Brian Bland** I might be interested in that linear bearing version. Are you willing to share?


---
**Wayne Friedt** *January 25, 2015 06:50*

**+Eric Lien** My 6 year old niece pointed that out to me or i would not have found it on my own.


---
**Eric Lien** *January 25, 2015 14:33*

**+Oliver Schönrock**​ I don't think I have a scale that measure this small of a weight accurately. But I will check it at work next week.﻿


---
**Eric Lien** *January 25, 2015 15:59*

**+Oliver Schönrock** yes I used support. I slice with simplify3d so support is a non-issue. The fan is offset to fit inside the duct ring. If you slice in Cura select support only with bed contact ( not part to part). You could also try craftware which like simplify3d let's you make custom supports. Lastly I could make a dummy filler piece with a 1 layer gap. Problem with the last one is getting your print settings dialed in so the dummy piece is removable.


---
**Jean-Francois Couture** *January 25, 2015 19:29*

**+Eric Lien**  i'm using the Micro mate-m-lok ([http://www.digikey.com/product-detail/en/794616-8/A30272-ND/684995](http://www.digikey.com/product-detail/en/794616-8/A30272-ND/684995)) but looks like its about the same as you with the 3mm pitch. I use a 12 pins on my Prusa i3 setup (since its a direct drive, the motor is also on the connector. I find that a 3mm pitch is the limit for the 18 awg wires for the hotend though.


---
**Eric Lien** *January 25, 2015 20:59*

Just a reminder to people that the normal bed supports don't reach the shortened carriage (nozzle won't touch the glass). The STL for the modified supports is in the folder also.


---
**Eric Lien** *January 25, 2015 21:48*

**+Oliver Schönrock** also the bed supports would hit the upper z-rod mounts.


---
**Daniel Salinas** *January 25, 2015 23:50*

Awesome!


---
**Marcos Duque Cesar** *January 28, 2015 12:13*

sexy


---
**Rick Sollie** *March 08, 2015 04:18*

This looks great and I plan on using it, but I had purchased the LM8UU Linear Bearing based off the original eustathios design.  What are you using for bearings?


---
**Eric Lien** *March 08, 2015 04:36*

**+Rick Sollie** 8mm sintered bronze misalignment bushings.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/PYXqEkU8niR) &mdash; content and formatting may not be reliable*
