---
layout: post
title: "Build plate heaters- Has anyone tried using 4 ~160mm x 160mm 12V square heaters instead of the one large one under the build plate"
date: January 19, 2018 22:43
category: "Discussion"
author: Dennis P
---
Build plate heaters- Has anyone tried using  4 ~160mm x 160mm 12V square heaters instead of the one large one under the build plate. 4*$6 instead of large 320mmx320mm one for $120?

  





**Dennis P**

---
---
**wes jackson** *January 19, 2018 22:55*

if you have enough inputs it should work fine. Just make sure you have a heat spreader. I just get mine from aliexpress, my delta is 450mm for 50$


---
**Dale Dunn** *January 19, 2018 22:59*

I haven't tried it, but I've been thinking it might be necessary for very large platforms. The same for magnetic build surfaces too, such as the BuildTak flex plate. I'm not going back to a fixed bed for 200 x 200, I surely wouldn't want one for a large format machine. The seams could require some planning though. Which brings me back to the heaters. They may not heat identically, so you could have a hot or cold quadrant on your platform.


---
**wes jackson** *January 19, 2018 23:00*

24V 300W 300*300MM Silicone Heater Bed For 3D Printer With 100k thermistor and 3M Tape

 [aliexpress.com - 24V 300W 300*300MM Silicone Heater Bed For 3D Printer With 100k thermistor and 3M Tape](http://s.aliexpress.com/7feMzEfa?fromSns=Copy) to Clipboard 

(from AliExpress Android)


---
**Eric Lien** *January 19, 2018 23:38*

Bed heater shouldn't be anywhere near $120. Talk to Nicole Liu at Alirubber (email = nicole "at" alirubber.com.cn)(email address formatted to avoid bot spam to Nicole.).



The part that drives the cost up a little is the shipping costs. If possible you might be able to find someone in the group to order along with to"group buy". 


---
**Daniel F** *January 20, 2018 00:17*

Would also recommend Ali Rubber, I think mine is 230V/350W, 290x290mm, works great with Omron SSR from ebay. Heats up really fast (5 min to 90°C), much faster than 24V and you don't need a large PSU. Only drawback is you can't go much above 100° with mine because the adhesion foil gets loose at around 110°. 90° is enough for most filaments though.


---
**Dennis P** *January 20, 2018 01:03*

OK, I was looking at the wrong things... yes, the right size/link **+wes jackson** posted is better. ... 

if anyone is looking to get one, I will probably order in the next few days.. I am in Chicago and would be happy to reship group buy once recieved. 


---
**Dennis P** *January 20, 2018 01:11*

**+Dale Dunn** agreed, concept is one thing, execution is another. i did consider variation in temps. i really like the way that the PCB trace heater is laminated to the the aluminum build plate on my i3.  i was looking at some vendors and DIY stuff too to see if i could do similar. 


---
**Eric Lien** *January 20, 2018 01:23*

With my 120v Alirubber heater spec'd in the BOM there should be no problem hitting 120C. And the 3m 468mp tape included on the heater pad from Alirubber should have no issues holding at those temps.


---
**Bruce Lunde** *January 22, 2018 00:59*

**+Dennis P** I ordered 2 off the 120volt units but only built the one printer. I'll sell you the spare one if you want to drive it with AC.


---
**Dennis P** *January 23, 2018 03:37*

**+Bruce Lunde**​ I will gladly BUY it off you and pay for shipping. Message me on Hangouts please. 


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/MEDyWRYGKF8) &mdash; content and formatting may not be reliable*
