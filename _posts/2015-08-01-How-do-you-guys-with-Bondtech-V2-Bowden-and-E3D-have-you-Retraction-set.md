---
layout: post
title: "How do you guys with Bondtech V2, Bowden and E3D have you Retraction set?"
date: August 01, 2015 19:44
category: "Discussion"
author: Frank “Helmi” Helmschrott
---
How do you guys with Bondtech V2, Bowden and E3D have you Retraction set? Basically retraction works but i see a late extrusion after retracts from time to time and i'm completely stuck where this comes from. Maybe the bondetch force isn't set right with the screws yet.



I normally do retract about 5-6mm with 60-80mm/s and have 1-2mm as minimum movement before retraction and 0.1-0.2mm minimum extrusion before retract set.





**Frank “Helmi” Helmschrott**

---
---
**Isaac Arciaga** *August 01, 2015 22:18*

Hi **+Frank Helmschrott** which slicer are you using? If you are using S3D for example, you can try setting additional nozzle priming after a retraction. It's called "Extra Restart Distance". In Slic3r it's called "Extra Length on Restart"  in the Printer Settings tab.


---
**Frank “Helmi” Helmschrott** *August 02, 2015 07:13*

**+Isaac Arciaga** thanks, i know that. In my 3d printing career so far this wasn't needed as long as the extruder was setup well so to me it's more a feature to cover problems instead of fixing them. I still hope to get this solved otherwise.


---
**Isaac Arciaga** *August 02, 2015 07:53*

**+Frank Helmschrott** understood, I've never had to use it myself until recently while testing a Craftbot. I was suffering with the same issue. 80mm/s retracts. I was going to try slowing the retract down a bit to see if it makes a difference just to rule out the unlikely chance of cavitation.


---
**Joe Spanier** *August 02, 2015 18:01*

60mm/sec 4mm using S3D


---
**Frank “Helmi” Helmschrott** *August 03, 2015 08:08*

Well it looks like i found two problems one of which could be fixed easily. This was Extruder Acceleration that was set to 5000mm/s² - i don't know why. This has most probably caused my retraction problem. Until now retraction looked good after lowering acceleration, i'm still tuning. How did you guys set accel with the bondtech extruder?



What's left is a problem with my E3D. This seems to clog rather quick on "problematic" filaments. I saw this with wood fill (Colorfabb) before and also with the CF20 (Colorfabb). Yesterday i had it again with Bendlay (which is a bit softer). Maybe i'm just going to fast with them. Have to check this out.


---
**Sal42na 68** *August 03, 2015 11:13*

I usually run hot end hotter if I'm going faster


---
**Joe Spanier** *August 03, 2015 11:53*

How fast are you going. Flexible filaments almost always need to go slow


---
**Frank “Helmi” Helmschrott** *August 03, 2015 12:09*

**+Joe Spanier** no, not flexible like in Ninja Flex of course. I know that they need slow speed. WoodFill and CF20 aren't flexible at all, BendLay is just a little flexible but pushes well for quite a while until it clogs and then the power of the **+Bondtech AB** Extruder squeezes the filament and the mess is perfect :)



**+Sal Fortuna** do you have any speed/temperature comparison for me preferably on some of the problematic filaments like woodfill and cf20?


---
**Sal42na 68** *August 03, 2015 12:46*

Haven't used any fancy stuff yet, just pla


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/NTB5tJZdXfR) &mdash; content and formatting may not be reliable*
