---
layout: post
title: "I have problem. when im printing its like the layer is getting to high, that so my nozzle hits the last layer when travel"
date: July 02, 2015 09:39
category: "Discussion"
author: E. Wadsager
---
I have problem. when im printing its like the layer is getting to high, that so my nozzle hits the last layer when travel.  Does any one know what is causing this? 



![images/d89985b3fde67c55e5452c9bf2ae0f15.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d89985b3fde67c55e5452c9bf2ae0f15.jpeg)
![images/84f31dccbf3fd1399b0ed8edcf87f386.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/84f31dccbf3fd1399b0ed8edcf87f386.jpeg)

**E. Wadsager**

---
---
**Sal42na 68** *July 02, 2015 10:12*

That happened on my robo3d printer. My fix was to reduce the number of steps per mm on the extruder. 


---
**E. Wadsager** *July 02, 2015 10:23*

i made it run 50mm of filament through and measured it it close to 50mm.. 

should i still try change the settings? and how much?

i use a mk8 extruder and the steps pr. mm is 97.0332.


---
**Sal42na 68** *July 02, 2015 10:32*

I did the 100mm test and wasn't even close. Try 50 steps per mm and see how it looks


---
**Gústav K Gústavsson** *July 02, 2015 17:43*

I would try to run through about 200 mm of filament and measure it accuratly and tweak steps Pr. mm. 50 mm is to small to fine tune. Also check filament diameter is set correctly in your slicer.


---
**Jim Wilson** *July 02, 2015 18:40*

Excellent guide for calibrating steps: [http://reprap.org/wiki/Triffid_Hunter%27s_Calibration_Guide](http://reprap.org/wiki/Triffid_Hunter%27s_Calibration_Guide)


---
**E. Wadsager** *July 03, 2015 21:48*

i did run 100mm through and measured 99,8mm. i tried raising the temperature and the that helped a bit. 


---
**Sal42na 68** *July 03, 2015 21:59*

Try running 100mm through but set it so only 75mm runs through. Then try that print again


---
**Владислав Зимин** *July 04, 2015 00:15*

Use this article, friend:

[http://prusaprinters.org/calculator/](http://prusaprinters.org/calculator/)


---
*Imported from [Google+](https://plus.google.com/103157635215674778495/posts/HVjnb8Xc9Ky) &mdash; content and formatting may not be reliable*
