---
layout: post
title: "Looking at the Spider V2 in Solidworks, I don't see the information for the MDF plate that holds the heater"
date: April 21, 2015 00:26
category: "Discussion"
author: Rick Sollie
---
Looking at the Spider V2 in Solidworks, I don't see the information for the MDF plate that holds the heater.  Was this only on the first version? if so  what is the rubber heating mat sitting on?





**Rick Sollie**

---
---
**Eric Lien** *April 21, 2015 05:31*

Underside of the aluminum heat spreader. You need to order the heater with adhesive to stick it to the aluminum.﻿


---
**Eric Lien** *April 21, 2015 05:33*

There is no MDF insulator like HercuLien, but there could be.


---
**Rick Sollie** *April 21, 2015 14:54*

**+Eric Lien** Thanks. Been looking at all your builds and got confused. I forgot to include info they needed for the order , so I was able to update the requirements on the heated pad.


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/P7cK96pnBPN) &mdash; content and formatting may not be reliable*
