---
layout: post
title: "coming along. A couple of tips: 1) when you order the silicon heater, specify you want the wires offset from the centerline"
date: June 11, 2017 01:59
category: "Build Logs"
author: James Ochs
---
coming along.  A couple of tips:



1) when you order the silicon heater, specify you want the wires offset from the centerline.  mine are dead center where they exit the heated bed and I had to tie them back so they don't rub on the ball screw.



2) Also tell them to make the wires a few inches longer.  I needed about 6 more inches to put the terminal block where I wanted it.  It was supposed to be closer to the center, rather than in the corner.



3) I should have added another 10-15 mm to the power box,  I didn't quite have enough to hide wiring.  I'll add it to the model before I upload it, but it's not particularly easy to re-install at this point, so mines staying that way.



4) I extended the power supply "window" from my original design so that I can actually get a hand in there to work on it.  I should have that write up posted soon.﻿



![images/e448a3432beb0d542766c029184fbe44.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e448a3432beb0d542766c029184fbe44.jpeg)
![images/6dd4beb173d12a602ebb7d394b3f572b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6dd4beb173d12a602ebb7d394b3f572b.jpeg)
![images/e42640e195fb6ae20827d2efa035feb3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e42640e195fb6ae20827d2efa035feb3.jpeg)
![images/dc7bc09e36add98837bd27154f7364cb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dc7bc09e36add98837bd27154f7364cb.jpeg)
![images/6cca36b345feabf24df2040d744bbeca.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6cca36b345feabf24df2040d744bbeca.jpeg)
![images/22272d3c281317e7fb78c7ee9630c4ac.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/22272d3c281317e7fb78c7ee9630c4ac.jpeg)
![images/60fbc273a70dd1458589381baed80df3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/60fbc273a70dd1458589381baed80df3.jpeg)

**James Ochs**

---
---
**Khald Khdam** *June 26, 2017 22:34*

- 


---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/83MpqSUPKnF) &mdash; content and formatting may not be reliable*
