---
layout: post
title: "Hey Everyone, I had some open printing time recently on my HercuLien and I wanted to put it to some good use"
date: January 13, 2015 06:49
category: "Discussion"
author: Eric Lien
---
Hey Everyone, I had some open printing time recently on my HercuLien and I wanted to put it to some good use. There have been some discussions recently about improving Eustathios/Ingentis documentation (and HercuLien could use some TLC in this area also).



This prompted me to think back my first printer which was a corexy I designed from scratch before ever owning a printer. At that time I had a friend who just built a Prusa i3, and without him I could never have built my first design. 3D printers are great, but most of the designs require a printer in the first place. Kind of a Catch-22 issue. So I printed a full HercuLien plastics kit.



I will be giving away the kit to someone on two conditions:



1.) That they actually intend to build the printer (if the person who gets the kit isn't serious about building that would deprive another maker of the opportunity)



2.) That they pledge to PAY IT FORWARD and print an Ingentis / Eustathios / or HercuLien plastics kit for another user once their printer is operational and they have a comfort level to make parts for someone else.



My hope is two fold. One that this can be a vehicle to promote these type of printers since I personally feel any of our variants are among the best printer designs available, and two that it will motivate and enable a new 3d printer enthusiast who has the desire to learn, but no method by which to begin. Also the more of our printers are out there, the more documentation and eventual design diversity we will have... looking at you IGentUS :) 



So here is the deal, if you are interested drop a comment below saying something like "I Want A HercuLien". I will take the names from these comments in 1 week, use an online random selection tool to pick the name, and then ship the winner the parts. I would ask if the winner lives somewhere where shipping costs are just plain ridiculous that they help me out, but if they cannot I will ship it anyway. If you are not interested, but know someone who is let them know about the give away.



Printed Part Details: All major components are ABS, printed on my HercuLien at a speed of 100mm/s or above (just for fun), and I vapor smoothed them because I feel it helps with strength by fusing the surface more uniformly. There are some parts in PLA, I did that for parts that are not exposed to appreciable heat, or are enhanced by the ridgidity of PLA over ABS (like the lower bearing holders).

![images/d3f504054bfdeb090fb64cbc63b88695.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d3f504054bfdeb090fb64cbc63b88695.jpeg)



**Eric Lien**

---
---
**Eric Lien** *January 13, 2015 07:00*

BTW for anyone who is new and doesn't know what the heck HercuLien is, you can see details on my G+ posts, and all my design files live out on github: [https://github.com/eclsnowman/HercuLien](https://github.com/eclsnowman/HercuLien)



Also feel free to share this outside our Ingentis/Eustathios community if you think it might be of interest to others and to promote all the great work our community here is doing.


---
**Eric Lien** *January 13, 2015 07:31*

**+Oliver Schönrock** that would be great for multiplication purposes... I just don't want to scare people off by mandating too much just to get the parts. But if they want to print more than one set the community would be very greatful for their generosity. 


---
**Wayne Friedt** *January 13, 2015 07:35*

Very generous offer Eric.


---
**Tim Rastall** *January 13, 2015 08:34*

To celebrate this, I have edited the community name :).


---
**Mike Miller** *January 13, 2015 10:16*

IGentUS was never meant to have a BOM and build instructions, I wanted to tweak what was out there as I saw fit without much thought that there'd be a Serial No 2.



But the boy wants to build a printer with me, so who's to say I don't take better notes this time. 


---
**Whosa whatsis** *January 13, 2015 10:34*

This is the way Reprap was always intended to work. You get a set of plastics free (or at the cost of plastic), on the condition that you print at least two more full sets and give them away under the same conditions. It never worked in the early days for a couple of reasons:



First, the designs, software, and vitamin quality (extruder components in particular) were frankly horrible by today's standards, so most of those machines (especially the ones built by people who didn't care enough to pay a lot for plastics) often never printed well enough to make a full usable set of parts. A machine would either be abandoned or would take many months (often more than a year) to get to usable condition, and then something would break. The low speeds and overly-complicated sets of parts made a full set take more printing time than the machines' mean time to failure.



Second, printers that actually could print well enough to reproduce were rare enough that those sets of parts were a rare commodity. When I got into 3d printing, a full set of <i>just plastics</i> for a Sells Mendel popped up on ebay a couple of times a month, and sold for more than most complete printers today. Those who had working machines got dollar signs in their eyes, and weren't about to give away something so valuable.



Third, the community wasn't strong enough to make it work. There were so many people who wanted a set and so few able to make them, and no way for those who had them to fairly decide who to give them to other than "highest bidder". The few pay-it-forward printers that did get built went to local friends, creating small pockets of printer owners and not spreading to new geographical areas.



We might be at the point where these issues can be overcome. One thing I would suggest would be to stipulate that the first pay-it-forward set of parts should be shipped to someone in a different state/country, not given to someone local. Subsequent kits can be distributed locally if desired.


---
**Chris English** *January 13, 2015 11:05*

I like the title change. It's like **+Eric Lien**​ ​just became partner of the law firm 


---
**Mutley3D** *January 13, 2015 12:27*

I want one. I want one. :) But it's ok i have capacity and machines to print my own so I shant deprive someone who is more deserving. I intend to build one of these very soon. Maybe even with some tweaks that I might see as improvements during the build. I wont over promise on any documentation though ;p. I have everything downstairs for it but just need to clear a backlog of others things. I will certainly be making a kit donation once its up and running to make a return on the design effort put in by others :)


---
**Brandon Satterfield** *January 13, 2015 13:45*

To **+Eric Lien** a gentleman and true Reprapper 🍻! 



I'm feeling motivated. When you find your deserving candidate, let me know, I'll contribute as well.


---
**Rick Sollie** *January 13, 2015 15:08*

Eric, This is a generous offer. You Rock!


---
**Eric Lien** *January 13, 2015 15:22*

**+Mutley3D** I would love to see a HercuLien running flexdrive. Keep us posted.


---
**Eric Lien** *January 13, 2015 15:25*

**+Tim Rastall** thanks. I would have never asked for it. But I truely appreciate the addition of HercuLien to the title. I am proud that my design is good enough to be listed with your and Jason's great work.


---
**Bruce Lunde** *January 13, 2015 15:33*

I Want A HercuLien  

Even if I don't get selected, this is the size and type I would like to build.  I already have the E3D v6 on order.  I don't have access to a 3d printer in my small town, so was thinking about trying to mill the plastic parts with my CNC'd HF mini-mill.


---
**Mike Thornbury** *January 13, 2015 15:50*

I would love to build one. 



I don't have a printer, but do have extrusion, steppers, tinyg, a Bondtech extruder, 10kg of ABS and PLA, 250 various sprockets and belts, lots of bearings and smooth shafts, a 400w 24V psu, a heated bed and spreader, just missing some screw shafts and nuts, but I'm sure that can be arranged.



It may seem strange to have all that and not have a printer, but here in the third world you need to have every single component needed before you start, as you can't buy anything locally, everything needs to be shipped in.



A case in point is my latest build -a large cnc router. I have been stalled for about 4 weeks waiting on my last and final component -a 6mm to 8mm flexible coupling... Everything is sitting there ready to go, but I can't move the z-axis. Doh!


---
**Mike Miller** *January 13, 2015 15:55*

You do realize that, when you're waiting on robotdigg or Ali express, that Denver Colorado, USA might as well be a third world country. ;)


---
**Richard Meyers** *January 13, 2015 21:20*

I want a Hurculien!!!!



I would really love to win these parts. I'm  new to this community and Google plus.  I stumbled on to your profile last night and since then I have gone through all of your posts and I am dumbfounded by the quality you are able to attain from this printer.  The only 3d printers I have ever touched were makerbots (at school) and I have never seen a print that looks so good. Makerbots has nothing on your printer. I really hope I win the drawing:) fingers crossed. 




---
**Mike Thornbury** *January 13, 2015 22:52*

**+Mike Miller** but you have alternative (and local, and cheap) sources of supply.



My cnc router cost more than $1200 in shipping charges alone. I don't know if you can understand what it's like living where you just can't buy anything appropriate for mechanical or electronic needs, including wire, screws, washers, etc.



You literally cannot buy machine screws, light gauge wire, LEDs, resistors, capacitors, transistors, plugs, sockets, etc. I can't think of a single component I have been able to source locally -and it's not just the cost, it's the time. Third world postal systems mean that it takes six weeks or more for deliveries to arrive -if they ever do. I am still waiting for items ordered Nov. 12th.



You have a huge range of buying option, but you choose to bypass US businesses in favour of buying cheaper.



I know your comment was meant tongue in cheek, but the reality of having a tech hobby in a country that hasn't any compatible businesses isn't much fun.


---
**Eric Lien** *January 14, 2015 00:53*

**+Mike Miller**​ I just noticed my comment might have been misunderstood. I was saying I like the variation IGentUS provides, not that documentation is lacking. Sorry my wording might have lead to alternate interpretations.﻿


---
**Eric Lien** *January 14, 2015 00:59*

**+Brandon Satterfield** that would be great and generous of you. I really like the products [www.smw3d.com](http://www.smw3d.com) provides. Nice selection of parts and prices. I am just bummed I bought most of my parts before you opened shop. I could have saved $ and time waiting for slow boat shipping from China.


---
**Eric Lien** *January 14, 2015 01:04*

**+Mike Thornbury**​ where are you located? 


---
**Mike Thornbury** *January 14, 2015 01:05*

Borneo. Brunei.


---
**Mike Miller** *January 14, 2015 01:55*

Nah, I get it **+Mike Thornbury**, I've lived in the styx before, but even that didn't compare. I still had a hardware store and a Radio Shack.

 


---
**Jeremie Francois** *January 14, 2015 09:19*

Just a plain, excellent, reprap idea :) Like Whosa says, may be try to have the recipient send new parts to someone far from him so as to help disseminate the thing!


---
**Gus Montoya** *January 14, 2015 17:42*

Oh nice, my printer is broken again :)  1 please :)


---
**Seth Mott** *January 14, 2015 18:54*

I Want A HercuLien!  I have a Makibox and I am unable to print accurately dimensioned parts due to it's X/Y backlash.  I started printing Eustathios parts but having to scale each one perfectly was just too tedious!


---
**Seth Messer** *January 16, 2015 02:54*

I Want A HercuLien. :)



I literally have a printer cart i built that has been bare since I built, enjoyed, and sold my delta. Now I want a HeruLien! :)



I'd pay for full shipping too, These parts help out so much, the shipping cost is worth it to the winner to pay. IMO of course.



Anyway, thanks for doing this Eric.


---
**Bruce Lunde** *January 16, 2015 13:48*

**+Alex Lee** I would like to take you up on that! I am just finishing my CNC Router build, so I am going to start ordering the materials for this one, I just downloaded the BOM to get started!  Happy to pay the material and shipping costs!


---
**Bruce Lunde** *January 16, 2015 17:51*

**+Alex Lee** I have no preference on a color, my workspace has them all, what ever selection you have is perfectly fine with me!  Constant build updates will be no problem; and a document with pictures showing the process from start to finish will be created and placed in the public domain!


---
**Dat Chu** *January 16, 2015 18:09*

I would be interested in joining in this <s>pyramid</s> pay it forward scheme of awesomeness please. :)


---
**Seth Mott** *January 16, 2015 19:10*

I will get in line behind whoever is next to pay it forward. Color does jot matter and I'd be happy to pay shipping costs.


---
**James Ochs** *January 16, 2015 22:40*

What seth said ;)  I really like the design of this printer, and the idea behind this thread, so when I get mine built (regardless of whether or not I get in on the giveaway) I'll commit to paying forward at least 3 sets.


---
**James Ochs** *January 17, 2015 16:19*

btw, you mentioned that the herculien build docs could use some TLC... I don't see them in the github repo and can't seem to find them anywhere else, am I missing something or is the TLC just that they need to be written?


---
**Eric Lien** *January 17, 2015 16:30*

**+James Ochs**​​​ there is the 3d models, some prints, and the BOM. That's about it. To be honest that is all I have done. I haven't made an assembly manual, mostly because when I made it it was untested, and since then it has pushed over 25kg of plastic (it is basically always printing long jobs since I make stuff for work).



The BOM was a large undertaking. It has every nut, bolt, multiple suppliers, etc. But I lost momentum documenting 6 months ago when work tasked me with our ERP process conversion.



If someone is a solidworks wizz and could help with exploded views or prints that would be an awsome start for a manual. To be honest I am really a newb at solidworks. So any help is appreciated.



I plan on taking pictures of all the parts before shipping to help the documentation also.﻿


---
**James Ochs** *January 17, 2015 16:38*

Ahh, I'll stop searching for it then ;)  It looks like it won't be too difficult to figure out from the pictures.  Once I get the parts together, I'll try to document the steps and anywhere I have to take things apart to get it to go together. I'm also a solidworks newb, so I don't think I'd be much help there.


---
**Frank “Helmi” Helmschrott** *January 18, 2015 00:04*

As not everyone owns the rather expensive solid works it would probably be a good idea to go with **+Autodesk Fusion 360** which is free for makers. it should import the existing solid works files and also supports some export formats. 


---
**Eric Lien** *January 18, 2015 00:16*

**+Frank Helmschrott** there are already many formats under the other formats folder. But if anyone makes it in any additional formats I will gladley add it.


---
**Eric Lien** *January 20, 2015 04:55*

And the winner is **+Dat Chu** , I made a new post with some notes ([https://plus.google.com/109092260040411784841/posts/YtJaA4EZi7K](https://plus.google.com/109092260040411784841/posts/YtJaA4EZi7K)). Thanks to everyone for the interest, the kinds words, and for being such a great community. 



Now the question is... who is gonna get in line for DAT on the next set :)



Dat hit me up on a hangout when you get time to exchange shipping info.


---
**Dat Chu** *January 20, 2015 05:02*

If you can create a spreadsheet containing the names of the people who showed interest and share it with the group, the second someone is ready to do the print, he/she can simple randomize and pick a person in that list. That way the queue keeps moving. :)



Also, Eric, message sent.


---
**Eric Lien** *January 20, 2015 05:08*

**+Dat Chu** I will make a google doc tomorrow with the names. Then people can edit it to add or remove themselves based on their desire to build, and if life/$ change remove themselves.


---
**Gus Montoya** *February 12, 2015 16:56*

Am I on that list? I've invested as much time as I can rectifying my 2up, but to no avail. The main cross frame broke. So it's now dead :(    I'm looking for a solution.


---
**Eric Lien** *February 12, 2015 18:25*

**+Gus Montoya** it should be open to edit for adding your name if I did it right.


---
**Gus Montoya** *February 13, 2015 07:09*

Am I missing something? I don't see a link or anything of the sort. This is making me feel REAL stupid :(


---
**Gus Montoya** *February 13, 2015 08:09*

Ok nm I found it, my name is on there. DUH!!! even with enough sleep I get senioritis. 


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/Xhc7wnKyGFq) &mdash; content and formatting may not be reliable*
