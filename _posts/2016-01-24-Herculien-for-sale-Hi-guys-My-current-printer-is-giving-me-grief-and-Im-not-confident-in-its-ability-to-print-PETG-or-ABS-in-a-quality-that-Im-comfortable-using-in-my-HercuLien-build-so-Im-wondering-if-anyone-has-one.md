---
layout: post
title: "Herculien for sale? Hi guys. My current printer is giving me grief and I'm not confident in it's ability to print PETG or ABS in a quality that I'm comfortable using in my HercuLien build so I'm wondering if anyone has one"
date: January 24, 2016 17:21
category: "Discussion"
author: Greg Johnson
---
Herculien for sale?



Hi guys. My current printer is giving me grief and I'm not confident in it's ability to print PETG or ABS in a quality that I'm comfortable using in my HercuLien build so I'm wondering if anyone has one they would be willing to part with, or if anyone offers a service of printing the pieces.



If not, it won't hurt my feelings too bad because it'll force me to get that rostock I've been eyeing, but I'd love to save a grand :)





**Greg Johnson**

---
---
**Daniel F** *January 24, 2016 17:53*

There was this print it forward initiative **+Eric Lien** started quite a while ago. If that's still going on that could be an option but you would need some time to wait for soneone to print the parts for you.


---
**Greg Johnson** *January 24, 2016 18:06*

That sounds awesome. Not sure how it's organized, but I'll start digging.




---
**Zachary Tonsmeire** *January 24, 2016 18:11*

I would be more than happy to print the parts ABS or PETG  and mail them to you for a fee (cheaper than 3Dhubs or a commercial printing company). Shoot me an email zt0827@gmail.com and we can get the ball rolling 


---
**Greg Johnson** *January 24, 2016 18:18*

That sounds... Amazing. At the very least I must pay for the filament and shipping.


---
**Greg Johnson** *January 24, 2016 18:18*

Just saw your email address. Shooting you an email now.


---
**Eric Lien** *January 24, 2016 18:45*

**+Daniel F** unfortunately it has not gone as planned. It was a good idea, but required a certain number of people building them and "print in forward" to hit critical mass.


---
**Greg Johnson** *January 24, 2016 18:59*

Well, it worked in this instance and I'll be sure to 'print it forward' myself.


---
**Gunnar Meyers** *January 27, 2016 04:12*

I have a Herculien that is about 95% finished with a smoothie board, Vicki 2Lcd, and dual E3d lite hotends. I have all the nesicary parts and pieces excluding a bed heater. I used high quality hardware and components. I just lost steam when it came to finishing/working on the electronics.  Let me know if you are interested. I can also give you left over hardware and pieces.  


---
**Greg Johnson** *January 27, 2016 05:55*

**+Gunnar Meyers** I'm very interested. How can I best get in touch?


---
**Gunnar Meyers** *January 27, 2016 06:25*

If you have a hangouts account we can chat there. 


---
*Imported from [Google+](https://plus.google.com/117128629201142457691/posts/LUEXQgZn2dh) &mdash; content and formatting may not be reliable*
