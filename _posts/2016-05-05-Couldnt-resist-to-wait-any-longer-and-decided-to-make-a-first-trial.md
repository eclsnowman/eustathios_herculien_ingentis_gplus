---
layout: post
title: "Couldn't resist to wait any longer and decided to make a first trial"
date: May 05, 2016 17:46
category: "Show and Tell"
author: Roland Barenbrug
---
Couldn't resist to wait any longer and decided to make a first trial. X and Y rods are still loose and still waiting for a heatbed. As a temporarily solution just printed on a loose sheet of paper. Calibration was limited to real basics (X,Y,Z,E steps). Sounds promising for the next steps.

Cable management has been inspired very much by the work shown by **+Maxime Favre**. Thnx.




**Video content missing for image https://lh3.googleusercontent.com/-tkjOQmyWYy4/VyuG9_9Sl7I/AAAAAAAAC5s/IK_u7nLwPY0YP6QlprkI8X1tm29LgZ7xw/s0/VIDEO0057.mp4.gif**
![images/67a717499ec2ae137672a5e4bf04dcbc.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/67a717499ec2ae137672a5e4bf04dcbc.gif)
![images/7233d88904fe0b27f273d1e3710ae481.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7233d88904fe0b27f273d1e3710ae481.jpeg)
![images/7c0c601d6f5df91af5991c76acbf7580.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7c0c601d6f5df91af5991c76acbf7580.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-NVoLYRNa90I/VyuG99db8NI/AAAAAAAAC5s/Fxl42iZjQu8ayHI1BCmUIlk8YYotOYb6Q/s0/VIDEO0060.mp4.gif**
![images/8f2093a3c638ab983dc23705331cf0ff.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8f2093a3c638ab983dc23705331cf0ff.gif)

**Roland Barenbrug**

---
---
**Eric Lien** *May 05, 2016 20:12*

Looks great keep us updated


---
**Roland Barenbrug** *May 07, 2016 22:08*

Great learning today: both belts on the same axis should have equal or close to equal tension to avoid twisting of the gantry. This was the main adjustment I had to make after vertical and horizontal alignment.


---
**Maxime Favre** *May 25, 2016 20:39*

Looking good, always glad to see clean wiring :)


---
*Imported from [Google+](https://plus.google.com/118296832015849309457/posts/BW2PWRvXZFR) &mdash; content and formatting may not be reliable*
