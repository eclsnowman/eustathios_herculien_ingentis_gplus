---
layout: post
title: "Hey there, I will soon (probably in a few months to get everything in and assembled) be joining your ranks"
date: July 03, 2016 05:46
category: "Discussion"
author: Justin Popa
---
Hey there,



I will soon (probably in a few months to get everything in and assembled) be joining your ranks. My prusa finally gave up the ghost in a magnificent gout of flame and I decided it's time to start from scratch again. That said, I have two questions:



1) Is there anyone here in the US that would mind printing me a set of parts for a HercuLien?



2) Does anyone have any recommendations on cutting the z-gantry? I'm in an apartment so my power tool assortment is limited to a drill and a janky jig saw.





**Justin Popa**

---
---
**Eric Lien** *July 03, 2016 13:21*

The Z can be made with with simple hand tools. I used a hacksaw :) . It only takes cutting an aluminum extrusion and an aluminum angle. A straight edge guide like a strip of steel flat bar goes a long way.



For cutting the 1/4" aluminum bed perhaps **+Brandon Satterfield**​ could cut you one (for a fee of course).


---
**Ted Huntington** *July 03, 2016 13:59*

I chose to just have the local metal company (IMS) cut the aluminum to size, then I used a round hand file to make rounded areas for the Z screw and rail- it took a while but works. Seeing Eric's comment, yeah possibly a hacksaw could be used to cut triangles to clear the area.


---
**Ted Huntington** *July 03, 2016 14:04*

l would try to fix your Prusa too- because why throw away a fine printer that needs a new electronics? RAMPS+LCD is like $30 now on ebay and aliexpress. I still use my old RepPrap Mendel for small prints- and it lost a Melzi and numerous plastic parts over the years, but is still pumping out the prints!


---
**Eric Lien** *July 03, 2016 15:17*

Just print out the dxf at 1:1 scale and carefully tape it on the aluminum angle. Then center punch the holes, and score the cut lines with a razor. Each side can be made in less that 10 minutes.



[https://github.com/eclsnowman/HercuLien/raw/master/Documentation/Drawings/Z_Gantry_Plate.DXF](https://github.com/eclsnowman/HercuLien/raw/master/Documentation/Drawings/Z_Gantry_Plate.DXF)


---
**Ted Huntington** *July 03, 2016 17:16*

That's a great idea- I guess you need to steer the hacksaw around the curves- I'm sure that's easily done with a small hack saw. Wish I had thought of that!


---
**Eric Lien** *July 03, 2016 18:21*

**+Ted Huntington** what curves? He mentioned a HercuLien... so that is a square bed, 20x80 v-slot Z guides, and aluminum angle bed brackets. No real rounded parts.


---
**Justin Popa** *July 03, 2016 18:52*

**+Ted Huntington** Yeah, I'm going to fix the prusa at some point. However it wasn't the electronics. I had a thermal runaway. Lost the hot end, extruder, some stuff above it, and a lot of the wiring.


---
**Ted Huntington** *July 03, 2016 18:56*

**+Eric Lien** oops my bad- still that's a good idea for the Eustathios too!


---
**Ted Huntington** *July 03, 2016 19:01*

**+Justin Popa**

sounds nasty! Most firmwares have thermal runaway protection- but I'm sure it can still happen with a faulty thermistor, or wires that melt together.


---
*Imported from [Google+](https://plus.google.com/104100197965770237152/posts/MeWirjEkVGC) &mdash; content and formatting may not be reliable*
