---
layout: post
title: "Hey all, Another weekend, another day of tweaking"
date: June 28, 2015 22:34
category: "Discussion"
author: Ben Delarre
---
Hey all,



Another weekend, another day of tweaking. Managed to get print quality much better than last time, then my bowden tube started popping out of my Bondtech extruder.



Anyone experience this? It seems I can't complete a print now without this happening. Its happened 3 or 4 times now. This started after I switched to some cheap filament that definitely had a clogging issue, the nozzle clogged and then the bowden adapter popped out of its housing.



Since then it has happened 3 or 4 times with this blue filament which I've been using from the start and is generally good (protoparadigm).



I had wondered if the bowden path was pad, I know tight turns are to be avoided. I've included a couple of shots of the hot end at its various positions, near, middle and far. This popped out this time while printing around the center of the bed.



Is there a better way of hooking up the bowden? I've been considering adding some extra extrusion to mount the extruder above the printer and just have the bowden coming downwards to the hot end. Anyone tried that?



![images/0bf6492fd8e3021e0fb0f705d0fe2cb5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0bf6492fd8e3021e0fb0f705d0fe2cb5.jpeg)
![images/fc76d0b9415599a18e20140f5373ea91.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fc76d0b9415599a18e20140f5373ea91.jpeg)
![images/f6f770fcd1ec0f301ade5e0ca9354ff9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f6f770fcd1ec0f301ade5e0ca9354ff9.jpeg)
![images/17a1869c22f59c01b992af8503775bce.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/17a1869c22f59c01b992af8503775bce.jpeg)

**Ben Delarre**

---
---
**Jim Wilson** *June 28, 2015 22:36*

Measure the filament diameter with calipers along multiple points along the spool, make sure it stays a constant 1.75 or whatever diameter it is supposed to be and doesn't vary drastically. Can possibly be a bad roll, I've had a few that went from 1.55mm to 1.95mm and jammed within the Bowden tube.﻿


---
**Ben Delarre** *June 28, 2015 22:37*

Hmm, I'll double check but this roll has been pretty good thus far and I've been keeping it in a dehumidified bucket. Calipers are currently out of action (battery dead), but I should have a replacement later today.


---
**Eric Lien** *June 28, 2015 22:55*

I think Martin presses that fitting in. To get you through for now I would recommend a little JB weld on that brass piece, then push it back into the housing and let it cure. Then it shouldn't pop out again. The plastic shark bite portion can/should be removed from the brass piece while you glue it in.



I would check your temps. With E3D hotends I run much hotter than with J-head style hot ends. If it is taking enough force to extrude that it is piping out fittings... Chances are you have a clog, or are too cold. Also if you have nylon filament around do a few cold pulls to clear out the hot end. To cold pull heat up the hot end to at least 235C, extrude some nylon out the hot end by hand, drop you temp to 110C while applying pressure on the nylon to keep the hotend full, once at 110C pull the nylon filament back out by hand. The nylon grabs the junk/burnt filament and pulls it out backwards from the hotend.


---
**Ben Delarre** *June 28, 2015 23:02*

OK so gluing it in is ok? I'll give that a go, I think I have some epoxy around.



I tried a cold pull with some Nylon 618 today to clear out after the clog from the crappy filament. But I can try it again. Generally its not that hard to extrude, I can push the filament through by hand easily enough. Running at 210 for PLA, so perhaps I should turn it up a bit.


---
**Joe Spanier** *June 28, 2015 23:24*

**+Martin Bondéus**​ and I were talking about this a couple weeks ago. He said he's started taking a punch and putting 4 points in the aluminum around the brass to give it some more tension. I haven't done it to mine yet there last time I pressed it in its started. 


---
**Ben Delarre** *June 28, 2015 23:28*

**+Joe Spanier** that sounds interesting. How would you specifically do this? On mine the brass goes about 1mm down inside the aluminum. Would you punch the top edge of the alu down into the hole to seal in the brass? 


---
**Gústav K Gústavsson** *June 29, 2015 00:52*

Maybe it is the angle of the camera but there seems to be nearly 90 deg bend on the Bowden Tube shortly after the Bondtech ? Also it seems that when the head is travelling there are sometimes tight bend on the Bowden tube where it enters the head? Possible the Tube is to long and the "webbed sleeve", (don't know the English name) around the wiring is forcing the Bowden to tight angles? Try to take the Bowden temporary out of the sleeve and see if it forms as tight angles as it travels. Just a thought.


---
**Ben Delarre** *June 29, 2015 00:58*

**+Gústav K Gústavsson** I had wondered if the bowden is too long, but then when it is at full travel in the last photo I can't really make it any shorter as it won't reach well, but I can try and mess around with it a little. I've ordered some more PTFE to see if I can find a better layout. Anyone got photos of their Eustathios bowden tube / extruder setups?


---
**Vic Catalasan** *June 29, 2015 08:31*

How much of a retraction are you applying? I have noticed too high temp and 4mm retractions caused clogging in my heat break. Also I have read somewhere if the nozzle is too low away from the heater block can cause clogging as well. I have yet to try cooking oil to prep the hot end but if it keeps happening I will try this myself


---
**Mike Kelly (Mike Make)** *June 29, 2015 16:16*

I agree with **+Gústav K Gústavsson** the bowden tube has a lot of sharp angles that will cause an increase in friction traveling to the nozzle. I'd suggest removing it from the wire loom and allowing for a nice gradual arc. 



I believe longer bowden tube with gradual arcs is better than shorter and more severe arcs. 


---
**Ben Delarre** *June 29, 2015 18:40*

That sharp corner above the extruder was only created after it popped out. Generally it didn't have anything like that angle on it.  The tube length currently is about 700mm. I will test it all out again tonight now that the epoxy has cured. Hopefully it is now good. 


---
**Eric Lien** *June 29, 2015 18:42*

**+Ben Delarre** My tube is around 900 to 950mm


---
**Ben Delarre** *June 29, 2015 18:47*

Oh...interesting, where did you get such a long tube? I ordered from Filastruder but they only had 750mm.


---
**Eric Lien** *June 29, 2015 19:08*

$12 on robotdigg: [http://www.robotdigg.com/product/133/PTFE+Tube+2*4mm+10+Meters](http://www.robotdigg.com/product/133/PTFE+Tube+2*4mm+10+Meters)


---
**Joe Spanier** *June 29, 2015 19:08*

[mcmastercarr.com](http://mcmastercarr.com)


---
**Ben Delarre** *June 29, 2015 19:10*

Damn, I just made a mcmastercarr order yesterday, and I've got another 750mm of ptfe coming from filastruder too. Oh well. I'll retest it all tongiht and try and setup a better path and see if its working, if not I'll order some more.


---
**Eric Lien** *June 29, 2015 19:12*

Also put a length of tube on the inlet of the bondtech extruder. It helps guide filament more smoothly into the extruder. BTW, why is the bondtech extruder mounted upside down? On mine I use the other side as the outlet for Bowden.


---
**Ben Delarre** *June 29, 2015 19:13*

Yeah, when my new tube arrives I'll add that. I'm confused, how did you manage to have the other side be the outlet? Surely the groove mount part is where the exit is designed to be?


---
**Eric Lien** *June 29, 2015 19:45*

**+Ben Delarre** the design can run in either direction. I have the bottom as the inlet and the top as the outlet when the text reads right side up. Both side have bowden tube fittings. All you do is reverse the motor direction.


---
**Ben Delarre** *June 29, 2015 19:47*

Oh...interesting, maybe I'll do that then :-)


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/2u1gE1FjYQP) &mdash; content and formatting may not be reliable*
