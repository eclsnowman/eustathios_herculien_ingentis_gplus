---
layout: post
title: "Once I set the microstepping to 1/32 instead of 1/2 (oops) I was able to run the break in g code"
date: July 27, 2015 16:42
category: "Discussion"
author: Brandon Cramer
---
Once I set the microstepping to 1/32 instead of 1/2 (oops) I was able to run the break in g code. I'm having trouble though.



Got any suggestions? 

![images/3882fcb547716c547a2991a53918cf2f.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3882fcb547716c547a2991a53918cf2f.gif)



**Brandon Cramer**

---
---
**Frank “Helmi” Helmschrott** *July 27, 2015 16:52*

looks like your alignment isn't as good as it should be. Alternatively try putting a bit more power into the steppers.


---
**Gus Montoya** *July 27, 2015 17:13*

Ouch.


---
**Владислав Зимин** *July 27, 2015 17:36*

[http://i.ytimg.com/vi/5EzooGRqzHI/hqdefault.jpg](http://i.ytimg.com/vi/5EzooGRqzHI/hqdefault.jpg)


---
**Vic Catalasan** *July 27, 2015 19:38*

Your binding in the front portion of the machine. Move the carriage to front and loosen and tighten the bearing block screws, there should be no tension moving the carriage at all corners. You might want to disengage the stepper belts to feel any friction.


---
**Brandon Cramer** *July 27, 2015 21:02*

I tore it all apart. It definitely is sliding around really easy. I have this vibrating issue now. 



[https://www.dropbox.com/s/g25kq0j2yp0yff2/2015-07-27%2013.58.32.mov?dl=0](https://www.dropbox.com/s/g25kq0j2yp0yff2/2015-07-27%2013.58.32.mov?dl=0)


---
**Brandon Cramer** *July 27, 2015 22:12*

I got it working like a top! The belt on both directions was off about a tooth or so from left to right. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/JHbVJ7gmYGP) &mdash; content and formatting may not be reliable*
