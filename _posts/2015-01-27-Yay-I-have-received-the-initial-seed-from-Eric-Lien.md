---
layout: post
title: "Yay, I have received the initial seed from Eric Lien ."
date: January 27, 2015 04:19
category: "Discussion"
author: Dat Chu
---
Yay, I have received the initial seed from **+Eric Lien** . Putting together an order as I write this.



I am having trouble ordering black anodized 2080 from Misumi though. Since Misumi can cut to length for me precisely, I am thinking of them as the first supplier. I wonder if Open Build will add cut to size service in the future.





**Dat Chu**

---
---
**Dat Chu** *January 27, 2015 05:06*

If someone has some info on custom cut to size black anodized aluminum extrusion, please let me know. 


---
**Eric Lien** *January 27, 2015 16:44*

**+Brandon Satterfield**​ you want to add cut to length with a charge to your capabilities list?


---
**Dat Chu** *January 27, 2015 16:53*

I am talking to Brandon as well. Since he is local to me, I am gonna get all the extrusions from him. Looks like he has the tools to help me cut stuff to size too. Bumblebee Herculien in progress. 


---
**Brandon Satterfield** *January 27, 2015 16:56*

Yeah Dat and I are working on this now actually. I need to sit down and look at the lengths and see how precise the cuts need to be. This will dictate how much time I have to spend on a kit.


---
**Eric Lien** *January 27, 2015 20:10*

**+Brandon Satterfield**​ making sure they are the same length is more critical than that they are the exact length. Except the two that make the front and back of the bed platform since those set alignment of the v-wheels with the vslots and also lead nut to the screw itself. In retrospect I really should allow more degrees of alignment freedom in that assembly.﻿


---
**Brandon Satterfield** *January 27, 2015 20:14*

**+Oliver Schönrock** wish I could brother. :-).



**+Eric Lien** that is what I need to look at, thanks for the info. It is the same thing on the OX kit. As long as all lengths that mate are the same a 1/32 here and there doesn't make any difference. 


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/9oyGMg1h6df) &mdash; content and formatting may not be reliable*
