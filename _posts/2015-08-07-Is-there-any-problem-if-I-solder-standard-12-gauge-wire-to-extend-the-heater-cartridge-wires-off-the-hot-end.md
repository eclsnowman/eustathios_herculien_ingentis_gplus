---
layout: post
title: "Is there any problem if I solder standard 12 gauge wire to extend the heater cartridge wires off the hot end?"
date: August 07, 2015 01:33
category: "Discussion"
author: Gunnar Meyers
---
Is there any problem if I solder standard 12 gauge wire to extend the heater cartridge wires off the hot end?





**Gunnar Meyers**

---
---
**Igor Kolesnik** *August 07, 2015 01:37*

Nothing is wrong with that. 12 gauge might be a bit of overkill. Cartridge is 40W so it is less than 4A in any case


---
**Eric Lien** *August 07, 2015 02:15*

Should not be a problem. Just ensure you have a good joint, and avoid solder joints at areas where flexing will occur.


---
**Gunnar Meyers** *August 07, 2015 02:24*

Great. I am printing out a smoothie board mount right now and will hopefully complete wiring tomorrow. 


---
**Chris Brent** *August 07, 2015 17:19*

[http://www.instructables.com/id/Master-a-perfect-inline-wire-splice-everytime/](http://www.instructables.com/id/Master-a-perfect-inline-wire-splice-everytime/) Learn the lineman splice and solder the wires like that. Much nicer finish.


---
**Jeff DeMaagd** *August 09, 2015 09:57*

20ga should be plenty.


---
*Imported from [Google+](https://plus.google.com/+GunnarMeyers/posts/bzAUTcP1D9V) &mdash; content and formatting may not be reliable*
