---
layout: post
title: "For the HercuLien, do you guys wire the two fans on the hotend always on, [ just to the power supply], or do you connect them to the control board so you can turn them off and on?"
date: March 04, 2016 00:59
category: "Discussion"
author: Bruce Lunde
---
For the HercuLien, do you guys wire the two fans on the hotend always on,

[ just to the power supply], or do you connect them to the control board so you can turn them off and on? And if connected to the control board, are they together or on separate control pins? I can do either with the Smoothieboard, just wondering what the prevailing opinion is.





**Bruce Lunde**

---
---
**Eric Lien** *March 04, 2016 01:30*

I wire mine so they turn on and off at 50C


---
**Derek Schuetz** *March 04, 2016 01:32*

I have mine directly to PSU but burn out fans quicker. How do you set up that wiring **+Eric Lien**


---
**Zane Baird** *March 04, 2016 01:48*

I use the same strategy as **+Eric Lien** on my Herculien. The relevant lines from my config file are below.



# automatically toggle a switch at a specified temperature. Different ones of these may be defined to monitor different temperatures and switch different swithxes

# useful to turn on a fan or water pump to cool the hotend

temperatureswitch.hotend.enable	              true             #

temperatureswitch.hotend.designator           T                # first character of the temperature control designator to use as the temperature sensor to monitor

temperatureswitch.hotend.switch               fan2             # select which switch to use, matches the name of the defined switch

temperatureswitch.hotend.threshold_temp      50.0             # temperature to turn on (if rising) or off the switch

temperatureswitch.hotend.heatup_poll         4               # poll heatup at 15 sec intervals

temperatureswitch.hotend.cooldown_poll       60               # poll cooldown at 60 sec intervals





#configuration for switch controlling fan on E3D hotend

switch.fan2.input_on_command                 M42              # gcode to turn on

switch.fan2.input_off_command                M43              # gcode to turn off

switch.fan2.enable							 true

switch.fan2.output_pin                       2.6              # pin that controls the fan on E3D

switch.fan2.output_type                      digital            # on/off control (no pwm)


---
**Bruce Lunde** *March 04, 2016 01:56*

**+Zane Baird** What controller are you using?


---
**Jim Stone** *March 04, 2016 01:57*

i wire mine to 24v as i cant get my azteeg board to turn them on at the ports described in the instructions


---
**Zane Baird** *March 04, 2016 02:15*

**+Bruce Lunde** I'm using a smoothieboard on my herculien. I don't think it's accurate after my rebuild, but here is a picture of my wiring ([https://drive.google.com/file/d/0B_-kEphyaFs4cGFjWlpNRFZOWUk/view?usp=sharing](https://drive.google.com/file/d/0B_-kEphyaFs4cGFjWlpNRFZOWUk/view?usp=sharing)) with a description ([https://drive.google.com/file/d/0B_-kEphyaFs4SzQtWnJVeDdYS1E/view?usp=sharing](https://drive.google.com/file/d/0B_-kEphyaFs4SzQtWnJVeDdYS1E/view?usp=sharing)).



I definitely have it slightly different now, but it only means you have to change the pin numbers.


---
**Bruce Lunde** *March 04, 2016 02:22*

Thanks for sharing that **+Zane Baird** 


---
**Zane Baird** *March 04, 2016 02:36*

**+Bruce Lunde** No problem. If you have problems or questions just let me know. I can also share my config file for that setup if you need it, but I suggest you work that out for your particular setup so you know how changes will affect it.


---
**Jeff DeMaagd** *March 04, 2016 04:05*

Thermostat fan control is so easy that I don't see why not use it unless you're out of FETs.﻿ Yes, ideally the machine should be printing whenever it's on, but no need to make a racket when it's not.


---
**Mike Miller** *March 04, 2016 04:45*

I'm of the other opinion. Ingentilire is on 24x7, ready...waiting....but only turns the e3d fans and board cooling fan when th extruder crests 50c, like **+Eric Lien**...


---
**Eric Lien** *March 04, 2016 05:03*

I have just gone through so many hot end fans. It's a difficult job those heat sink fans serve. Hot location, and lots of acceleration forces acting on the bearing (or in most cases bushings). If I can save some life on the fan by turning it off when not needed... Count me in :)﻿


---
**Zane Baird** *March 04, 2016 05:06*

For awhile I used a 4020 blower to cool my hotends... That was only because I was waiting on 24V 30mm fans. I will get on board with the next controller that give me a variable voltage selection on a switching PS for fan outputs only. That way I can source the fans a little easier


---
**Eric Lien** *March 04, 2016 05:07*

**+Derek Schuetz**​ I wire mine as shown (open in acrobat to see my sticky notes in the PDF). Then there are a few items in Marlin to config... But I am on my phone in a hotel. So I will have to look later. Just need to make sure pins.h is setup with those outputs as the heatsink fan output. And then define the turn on temp in config.h I think.﻿



[https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Azteeg_x3_v2_wiring_(with_notes).pdf?raw=true](https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Azteeg_x3_v2_wiring_(with_notes).pdf?raw=true)


---
**Vic Catalasan** *March 05, 2016 08:34*

I followed Eric's settings at a specified temp  to turn on the hot end fan automatically. Works great on my Azteeg X3. I rarely use the bed fan since I mainly print ABS but can turn that on when creating a print file from Cura. The hardest part of all this is setting is the firmware and finding what pin to use for a specific controller board.   


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/ZYp9ae9Azhj) &mdash; content and formatting may not be reliable*
