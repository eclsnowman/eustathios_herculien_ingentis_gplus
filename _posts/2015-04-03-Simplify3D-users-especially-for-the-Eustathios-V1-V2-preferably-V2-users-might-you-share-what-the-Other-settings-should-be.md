---
layout: post
title: "Simplify3D users, especially for the Eustathios V1/V2 (preferably V2) users, might you share what the 'Other' settings should be?"
date: April 03, 2015 01:25
category: "Discussion"
author: Seth Messer
---
Simplify3D users, especially for the Eustathios V1/V2 (preferably V2) users, might you share what the 'Other' settings should be? I'm trying to prepare for everything as best I can before the build begins. Would love to just get used to the software too. Also, for those of you with a Smoothieboard, would you also be willing to share your firmware/configurations for the Eustathios V1/V2 (preferably V2).



Thank y'all so much.

- Seth





**Seth Messer**

---
---
**Eric Lien** *April 03, 2015 01:32*

I will post my Eustathios v1 smoothie files on github. They are for an azteeg x5 mini, so you would need to edit the default config for Smoothie x5c similar.


---
**Seth Messer** *April 03, 2015 01:32*

Much thanks **+Eric Lien**


---
**Eric Lien** *April 03, 2015 02:26*

Uploading now.


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/AuP9LL5unYS) &mdash; content and formatting may not be reliable*
