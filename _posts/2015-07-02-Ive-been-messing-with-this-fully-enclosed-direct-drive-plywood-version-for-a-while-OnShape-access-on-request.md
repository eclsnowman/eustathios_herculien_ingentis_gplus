---
layout: post
title: "I've been messing with this fully enclosed, direct drive, plywood version for a while (OnShape access on request)"
date: July 02, 2015 01:17
category: "Discussion"
author: Vim Au
---
I've been messing with this fully enclosed, direct drive, plywood version for a while (OnShape access on request).

While I'm fairly happy so far, I'm stuck choosing the Z lift mechanism. 

The belt lift is simple and cheap but seems to be out of fashion at the moment.  



I'd really appreciate your thoughts on Z lift options!



Thanks :)

![images/b58c86c022b16753f0f383d8ad961f5f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b58c86c022b16753f0f383d8ad961f5f.jpeg)



**Vim Au**

---
---
**Eric Lien** *July 02, 2015 01:54*

Belt lift has the advantage of being cheap an little chance of Z-ribbing like lead screws. The downside is it can stretch and tends to fall with gravity when steppers aren't energized. But I think a geared stepper could solve the falling issue and an adjustable idler could handle tension issues.


---
**Joe Spanier** *July 02, 2015 02:19*

I'm seeing that with my tr8-8 lead screw's too though. 


---
**Vim Au** *July 02, 2015 03:28*

Eric - a geared stepper raises the price close to a the leadscrew option, though keeps the simplicity.  

I feel there has to be some sort of simple rate limiter (fluid/air) solution. A big lump of soft foam should do well enough also ;)



Belt stretch is something I hadn't considered!  Do you think two 6mm belts will be sufficient for a 400x200 heatbed?



Joe - you're seeing banding? or drop?



Thanks for the replies.


---
**Eric Lien** *July 02, 2015 03:43*

**+Vim Au** a geared stepper is only marginally more than a standard stepper ([http://www.robotdigg.com/product/103/Nema17+40mm+Stepper+Gearmotor](http://www.robotdigg.com/product/103/Nema17+40mm+Stepper+Gearmotor))



Lead screws are much more.


---
**Joe Spanier** *July 02, 2015 03:53*

Seeing drop. 


---
**Jeff DeMaagd** *July 02, 2015 04:13*

I wouldn't do more than a single start lead screw. Dual and four start lead screws have the risk of back driving (falling) when the z motor turns off. They're harder to find though.


---
**Mike Miller** *July 02, 2015 11:20*

I've got the rubber band lift Z and will be swapping it out for lead screws...it's too fiddly. The bed is near the limit of what the system can lift without slipping, and the belt lifting the bed offset from the centerline of the bed means there's some hysteresis that alters the alignment of the bed....I'm always re-adjusting the bed height for the initial Z layer. 



I can go a week or so with the printer just being dead-nuts reliable, but if there's a problem, it's nearly always related to Z movement. 


---
**Jason Perkes** *July 05, 2015 01:17*

Ive been thinking about a belt driven Z lift recently. Vertically aligned belts to lift the Z axis, one belt each side of build platform. Idler pulleys at the top, with driving pulleys at the bottom. The driving pulleys will be fitted to each end of a single shaft lying horizontal at the bottom of the printer. The shaft would be driven via a worm and wheel gear. Should be hi-res and accurate, but with the benefit of no drop when motors are off.


---
**Vim Au** *July 05, 2015 16:39*

Thank you all for your thoughts!



Jason - that sounds very similar to the solution as (sort of) shown above.  I couldn't find a printed worm drive that was suitable and proven so was going to use direct drive, at least to begin with.  It's a simple and cheap solution but given comments above by Mike Miller I'm inclined to try for something with less room for 'fiddly'.


---
**Vim Au** *June 23, 2016 03:43*

for the record. (has it been this long!!)

this went nowhere, as local CNC services wanted to charge $500 to cut this...



Cheap and reliable 2020 is available from China at the time of writing, so I've migrated the design to that.



:)




---
*Imported from [Google+](https://plus.google.com/109859547173137570469/posts/18AxrQYLm2s) &mdash; content and formatting may not be reliable*
