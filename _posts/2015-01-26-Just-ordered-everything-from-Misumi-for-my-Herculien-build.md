---
layout: post
title: "Just ordered everything from Misumi for my Herculien build...."
date: January 26, 2015 21:01
category: "Discussion"
author: Daniel Salinas
---
Just ordered everything from Misumi for my Herculien build.... ooooof.  :D





**Daniel Salinas**

---
---
**James Rivera** *January 26, 2015 21:25*

Ouch. ALL from Misumi? What did that cost (before shipping)?


---
**Daniel Salinas** *January 26, 2015 22:12*

Just all the extrusions and metal parts from the BOM. This was quite a few things. 23 line items with many of those that had more than one for thebitem count. I haven't gotten the shipping cost yet but the whole order was $487 and change. 


---
**James Rivera** *January 26, 2015 22:15*

Ok, thx. That's less than I expected. What part number did you end up with for the lead screws?  I'm not sure if I have it right. This is what I ended up with (I have not ordered anything yet): MTSRB12-410-S56-Q8-C5-J5


---
**Brandon Satterfield** *January 26, 2015 22:43*

Congrats! A great start to an amazing printer!!


---
**Daniel Salinas** *January 26, 2015 22:49*

the part I ordered for the lead screws is MTSBRW12-480-F10-V8-S26-Q8


---
**Daniel Salinas** *January 26, 2015 22:49*

thats what I got from the BOM **+Eric Lien**  please tell me I ordered right.  :)


---
**Daniel Salinas** *January 26, 2015 22:50*

at $50 a throw, thats a big mistake to make.  Hopefully I didn't mess that one up.


---
**Eric Lien** *January 26, 2015 23:07*

**+Daniel Salinas** what I had in the Bill of Materials was what I ordered so it should work :) 



I think James is talking about the Eustathios lead screw.


---
**Daniel Salinas** *January 26, 2015 23:12*

Ok here's an update.  It looks like ground shipping to me in Texas is $100. So the total Misumi part of the BOM was $587 for me.


---
**James Rivera** *January 27, 2015 01:04*

Ugh. Yes. I was looking at the Eustathios BOM. Sorry.


---
**Marc McDonald** *January 27, 2015 01:05*

**+Daniel Salinas** **+Eric Lien** **+James Rivera** I ordered the lead screw listed in Eric's bom and it functions correctly in the HercuLien I have functioning.


---
**Eric Lien** *January 27, 2015 01:17*

**+Marc McDonald** thank you for the confirmation.


---
**James Rivera** *January 27, 2015 03:12*

I'm starting to lean toward the Ingentis design. Nothing wrong with Eustathios or HercuLien--they're actually freaking great. But they're kind of overkill for me, and the Ingentis design is quite clean and simple. And since it uses belts on Z (I might use fishline) I don't need lead screws. This also means I don't need to use Misumi's, "let's abbreviate every option and make 'em hunt/guess" approach. And constantly changing their part number/naming scheme doesn't help. Did I mention I hate their configurator? :(


---
**Daniel Salinas** *January 27, 2015 03:22*

If you click the links in the herculien BOM it takes you right to each part. My big thing was having an enclosed printer since my office is really drafty. Is it overkill? Maybe. But I don't really need a 3d printer so if I'm gonna do it, I'm gonna do it :-)


---
**Eric Lien** *January 27, 2015 03:49*

I over build stuff. Blessing and a curse.


---
**Daniel Salinas** *January 27, 2015 03:51*

I love the design and am looking forward to everything getting delivered. I still need to source the closed belt and one or two other things


---
**Marc McDonald** *January 27, 2015 04:25*

**+Daniel Salinas** I sourced the X & Y small 113 tooth (226mm long) closed loop GT2 Belt from Misumi GBN2262GT-60 when I ordered my extrusions and RobotDigg has a 114 tooth version which would most probably work as SDP-SI is out of stock.


---
**Daniel Salinas** *January 27, 2015 05:15*

Thanks **+Marc McDonald**​ I'll check on that. I'm gonna wait until everything I ordered gets in so I can do a physical inventory. Still need to order the lexan and hotends


---
**James Rivera** *January 27, 2015 05:50*

**+Daniel Salinas** **+Eric Lien** I see the HercuLien BOM XLS file now. It does indeed have the links. The hard part for me has been trying to figure out how to get the holes pre-drilled and tapped in the right locations. If this was easier, I would've pulled the trigger on it (probably for a Eustathios) a long time ago (shakes fist at Misumi). <b>Question:</b> do the links to the specific parts in the HercuLien BOM take you to the part with the pre-drilled and tapped holes already configured? Their page makes me feel like an idiot because I can't tell if it does (or if it is the right thing). I've been tempted to try and build the frame without blind joints simply because I can't seem to figure this out (read: maybe I am an idiot). I mean, I'm tempted to build the HercuLien over the Eustathios if only for the BOM links to the correct customized parts.


---
**Eric Lien** *January 27, 2015 06:06*

**+James Rivera** here are the drill guides I used. [https://drive.google.com/folder/d/0B1rU7sHY9d8qT0J1RVBSeV9LdU0/edit](https://drive.google.com/folder/d/0B1rU7sHY9d8qT0J1RVBSeV9LdU0/edit)



The only hard part is the holes for the 10mm rods to go through the 20x80. It is half on half off a rib. It would be best to cut those holes with an end mill style bit than a standard bit to avoid bit walk.



But the rest are easy with the printable guides: [https://plus.google.com/app/basic/stream/z13bx1kzpxn1ibmdv23qs3cw1mnkhr5dz?cbp=1uwgm8rnm1h13&sview=28&cid=5&soc-app=115&soc-platform=1&spath=%2Fapp%2Fbasic%2Fs&sparm=sc%3Dpo%26sq%3Ddrill%2Bguide](https://plus.google.com/app/basic/stream/z13bx1kzpxn1ibmdv23qs3cw1mnkhr5dz?cbp=1uwgm8rnm1h13&sview=28&cid=5&soc-app=115&soc-platform=1&spath=%2Fapp%2Fbasic%2Fs&sparm=sc%3Dpo%26sq%3Ddrill%2Bguide)






---
**James Rivera** *January 27, 2015 06:18*

**+Eric Lien** Thanks! But...does this mean Misumi doesn't sell parts with the holes drilled and tapped, or is this just DIY to save a few bucks?


---
**Eric Lien** *January 27, 2015 07:24*

I just saved some$ on HercuLien. The wife caught on to me. The BOM has them cut to length, but not drilled.


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/5RM5YvVZfoL) &mdash; content and formatting may not be reliable*
