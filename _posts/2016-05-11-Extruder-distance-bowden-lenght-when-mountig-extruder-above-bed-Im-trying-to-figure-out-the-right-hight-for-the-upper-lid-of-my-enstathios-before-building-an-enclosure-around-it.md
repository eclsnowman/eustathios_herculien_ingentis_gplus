---
layout: post
title: "Extruder distance/bowden lenght when mountig extruder above bed I'm trying to figure out the right hight for the upper lid of my enstathios before building an enclosure around it"
date: May 11, 2016 08:55
category: "Discussion"
author: Daniel F
---
Extruder distance/bowden lenght when mountig extruder above bed



I'm trying to figure out the right hight for the upper lid of my enstathios before building an enclosure around it. Original lenght of the upprer extrusions is 25cm, in the video I lowered the top to 19cm (top level to original frame top). I also tried to lower it to 15cm but this was defnitely too low and the force on the hotend was too high. The bowden tube got twisted and was turning around when the carriage moved from corner to corner. While 25cm works well, it adds quite some hight to the printer which might limit ease of transport (car trunk). Is there any rule of thumb or did anyone test this setup as well? Bowden tube is 6mm (3mm filament), which is probably a disadvantage for this setup (but will hopefully allow to print flexible filament). The lenght of the tube is around 30cm, XY buid area is 29x29cm

![images/d7071edcaae7c1769a812d7e246c8ea1.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d7071edcaae7c1769a812d7e246c8ea1.gif)



**Daniel F**

---
---
**Michaël Memeteau** *May 11, 2016 09:27*

Check the work **+Walter Hsiao** did with his flying extruder:

* [http://thrinter.com/cartesian-flying-extruder/](http://thrinter.com/cartesian-flying-extruder/)

An inspiration for all of us...




---
**Daniel F** *May 11, 2016 10:34*

**+Michaël Memeteau** thanks for the link, I've seen that but there is this arm that holds the weight of the extruder which conflicts with the enclosure. The "elbow" would hit the walls.


---
**Michaël Memeteau** *May 11, 2016 10:40*

I'm working myself on a pyramidal structure that would mount on top of the printer (removable for transport) and would allow for a shorter bowden. I'll share it when it's ready, but it may not be compatible with your bondtech (I'll use a saintflint).


---
**Mike Miller** *May 13, 2016 12:46*

**+Michaël Memeteau** Interesting, that would take that pacakging issue with the filament entering and exiting on the same side and make it not such a big deal. 



It might also make stress relief of the wiring more manageable. 


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/3czE4PrCUBy) &mdash; content and formatting may not be reliable*
