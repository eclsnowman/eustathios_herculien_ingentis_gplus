---
layout: post
title: "I could not wait any longer for the bed heater to arrive so my son and I wanted to see how it would print ...."
date: June 19, 2015 17:32
category: "Show and Tell"
author: Vic Catalasan
---
I could not wait any longer for the bed heater to arrive so my son and I wanted to see how it would print .... after reassembly the Z-gantry and build plate (lot easier with help) we tried a test print of a square with a cylinder.  We warmed up the build plate with a heat gun and after screwing up all the settings in Slic3r settings in Pronterface we finally see a recognizable object. I wonder if there is a noob setting for all the variables we have? 



This was printed using black ABS maybe PLA would be easier to start with? I am using the largest Volcano 1.2mm nozzle which I have incorrectly set to .5 (default). The first few prints I mistakenly set the extruder multiplier to 2 and 4, all the extruder did was burn rubber on the fillament thus causing a lot of missfeed, and finally going back to default of 1 (default) worked out and  after reading about this setting a 4 = 400 percent is much too high!  DUH!



![images/b5cbb6c2752fa30377942317b37dfc12.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b5cbb6c2752fa30377942317b37dfc12.jpeg)
![images/11a17763479bbee9e02bd46a7c56cd7f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/11a17763479bbee9e02bd46a7c56cd7f.jpeg)

**Vic Catalasan**

---
---
**Eric Lien** *June 19, 2015 19:10*

Yes ABS on a cold bed doesn't work to well. But great to see you making progress :)



I love seeing more printers come to life. Great work.


---
**Mike Miller** *June 20, 2015 14:51*

You COULD buy a spool of PLA...it'll stick pretty well to that Blue tape if you rub it down with Alcohol. 


---
**Jeff DeMaagd** *June 20, 2015 18:16*

Blue tape is mostly known to work with PLA anyway. For ABS, you want kapton if you're using a tape.



Small pieces might work with ABS on an unheated platform, but you would need a couple tricks combined. You would need to experiment.


---
**Vic Catalasan** *June 20, 2015 19:23*

Thanks for the advice, I got some PLA and it was a little easier to print without heating the bed. I am trying .60 nozzle. Is there a way to auto start the hot end fan when it is heated up


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/5985eUMnrJa) &mdash; content and formatting may not be reliable*
