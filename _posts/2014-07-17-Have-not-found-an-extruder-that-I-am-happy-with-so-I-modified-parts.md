---
layout: post
title: "Have not found an extruder that I am happy with, so I modified parts"
date: July 17, 2014 03:03
category: "Show and Tell"
author: Brian Bland
---
Have not found an extruder that I am happy with, so I modified parts.  I am getting closer just have to figure out the best way to mount it to the frame.  I also like the direct drive steppers better than the belt drive.



![images/8a746f4fe2682eed7d30dc722b4e3920.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8a746f4fe2682eed7d30dc722b4e3920.jpeg)
![images/ba21542de46f4eb03811abbfaff3ce06.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ba21542de46f4eb03811abbfaff3ce06.jpeg)
![images/fac614c1106e894a2d4cd6211ce5040f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fac614c1106e894a2d4cd6211ce5040f.jpeg)

**Brian Bland**

---
---
**James Rivera** *July 17, 2014 03:16*

Is there a reason to not use the classic Wade's geared style? It seems to be a pretty time-tested and proven design, and will work with Bowden setups.


---
**Jim Squirrel** *July 17, 2014 04:12*

where did you find the couplers isn't 10mm to 5mm?


---
**Brian Bland** *July 17, 2014 10:04*

**+Jim Squirrel** Found them on Ebay. 5mm to 10mm


---
**Brandon Satterfield** *July 17, 2014 13:13*

Looks great keep us posted! 


---
*Imported from [Google+](https://plus.google.com/+BrianBland/posts/7G9T9HnqvQT) &mdash; content and formatting may not be reliable*
