---
layout: post
title: "anyone have a good method of aligning the lead screws for the herculien?"
date: February 11, 2016 19:08
category: "Discussion"
author: Jim Stone
---
anyone have a good method of aligning the lead screws for the herculien?



no matter what i do i dont seem to be able to get it right. its always binding somewhere and if i get the bindingsorted in one spot. it binds in another UGH!



So damn frustrating.



i have tried using calipers to check distances and doing it that way.doesnt work.



ive tried just doing it by feel manually. doesnt work.





**Jim Stone**

---
---
**Eric Lien** *February 11, 2016 19:18*

You don't have the lower bearing support all the way against the vertical extrusion do you? They need to be spaced off slightly for proper alignment.


---
**Jim Stone** *February 11, 2016 19:22*

no. thats what ive been messing with to try and get it to adjust it and get it going well.



but its always getting tight somewhere.



and i want to say only on one rod. but im not 100% on that.


---
**Zane Baird** *February 11, 2016 19:32*

Have you measured the top and bottom of your vertical extrusions to make sure the spacing is equal? By this, I mean if you are looking at the front of the printer and you measure the from the narrow face of the v slot to the front of your frame it should equal 235mm.



I'll send you a video later tonight regarding the assembly of the vertical axis supports


---
**Jim Stone** *February 11, 2016 19:35*

Top. Bottom and middle where I leave the z (measuring under the nuts)



Currently have an equivalent to what you guys would call harbor freight atm as a digital caliper



Measuring off the vertical z vslot to the threads at an angle so I don't go in I lie on top. 


---
**Jim Stone** *February 11, 2016 19:39*

so frustrating being this close to being mechanically working and just being hit with a problem that doesnt seem to have a solution D:


---
**Eric Lien** *February 11, 2016 20:28*

The delrin nuts do fit tight on the leadscrew. I recommend this to help along with the break-in code with Z movements: [http://www.amazon.com/gp/aw/d/B000XBH9HI/ref=mp_s_a_1_1?qid=1455222483&sr=8-1&pi=SY200_QL40&keywords=Super+Lube](http://www.amazon.com/gp/aw/d/B000XBH9HI/ref=mp_s_a_1_1?qid=1455222483&sr=8-1&pi=SY200_QL40&keywords=Super+Lube)


---
**Jim Stone** *February 12, 2016 02:20*

i think the full clean of the screws...a greasing and a retry at alignment helped. that and a reduction in feedrate.


---
**Ted Huntington** *February 12, 2016 07:51*

Yeah feedrate is important- I set mine to homing=15mm/s but only 3mm/s feed rate. I used ballscrews and didn't have much problem with Z binding at all- I did have a little due to the two nuts being at different heights- it's important that the screw nuts are at the same position on both screws- I measured with a caliper the protruding screw height above and below the two ends. I just used a tape measure to make sure that the two Z screws were in the exact middle of the X and Y (horizontally).


---
**Mike Miller** *February 12, 2016 13:41*

**+Jim Stone** there is a not insignificant break-in period. I'm both of my printers I've been amazed at the difference between what I thoguht was 'broken in' and what they felt like after a steady week of operation. 


---
**Jim Stone** *February 12, 2016 18:27*

would you guys say your motors run warm-hot?



mine are currently at correct vref.



other printer runs the motors stone cold. they never get temp even the extruder.


---
**Mike Miller** *February 12, 2016 18:29*

Mine run hot on one, not so much on the other. They have different drivers.


---
**Jim Stone** *February 12, 2016 18:32*

yeah 8825 on the herc and classic a4988 ( or whatever the number is) on the other.



do yours also make a sonic hum? similar to the soundgrenades and soundgrenade app





does it even at idle.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/eRY8J4QXKpT) &mdash; content and formatting may not be reliable*
