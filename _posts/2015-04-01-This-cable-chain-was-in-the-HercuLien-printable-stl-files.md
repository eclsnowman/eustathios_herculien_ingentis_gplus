---
layout: post
title: "This cable chain was in the HercuLien printable stl files"
date: April 01, 2015 17:54
category: "Discussion"
author: Brandon Cramer
---
This cable chain was in the HercuLien printable stl files. Does it actually get used somewhere? If it does, I have no idea how many links need printed, but I was having fun printing them. 

![images/b45d726d51c0e8e1f5b229350fad6ee7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b45d726d51c0e8e1f5b229350fad6ee7.jpeg)



**Brandon Cramer**

---
---
**Derek Schuetz** *April 01, 2015 18:08*

It's for the heated bed cables it's not necessary but I believe you need 22-26 links


---
**Daniel Salinas** *April 01, 2015 18:09*

It is for the heated bed power and thermistor leads. I printed one of each end and 20 links. Mines a little long so you could probably get by with 18


---
**Brandon Cramer** *April 01, 2015 19:00*

Thanks


---
**Eric Lien** *April 01, 2015 19:27*

Here is how I used mine: [https://plus.google.com/+EricLiensMind/posts/L3FvGyuuPBF](https://plus.google.com/+EricLiensMind/posts/L3FvGyuuPBF)



Also you alternate the direction the links are connected every other one.


---
**Brandon Cramer** *April 01, 2015 21:58*

**+Eric Lien**  Oh yeah you can alternate them!! Thanks. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/fzAsvB2wSqq) &mdash; content and formatting may not be reliable*
