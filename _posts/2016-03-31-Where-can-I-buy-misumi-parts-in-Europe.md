---
layout: post
title: "Where can I buy misumi parts in Europe?"
date: March 31, 2016 14:53
category: "Discussion"
author: Jaimie Fryer
---
Where can I buy misumi parts in Europe? Can I get them shipped to Iceland?





**Jaimie Fryer**

---
---
**Eric Lien** *March 31, 2016 21:08*

**+Makeralot**​ is working up an extrusion kit on their site, it might be a good alternative and definitely a cost savings. Also get the ball screws from GolMart on AliExpress. At that point you shouldn't have to get anything from the misumi.



[http://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207/](http://www.makeralot.com/20mm-x-20mm-extruded-aluminum-for-eustathios-3d-printer-p207/)



[http://m.aliexpress.com/item/32592258963.html](http://m.aliexpress.com/item/32592258963.html)


---
**Gústav K Gústavsson** *March 31, 2016 21:57*

I tried to order from Misumi some months ago and made some inquiries which they answered. But when trying to order they didn't ship to Iceland. Complained and made other arrangements to get the parts (much cheaper but not as detailed specs). Now they are sending me offers from another branch news@misumi-europe.email  and [http://www.misumi-europe.com/](http://www.misumi-europe.com/) which seems to be in Germany but haven't tried to order from them and 


---
**Jo Miller** *March 31, 2016 23:09*

yeah, i experienced the same,  I finally ended up ordering it through my local befriended volvo dealer (!). 


---
**Jaimie Fryer** *March 31, 2016 23:22*

Thanks again **+Eric Lien**​, I'll take your advice.



Think I can just print the parts I'll struggle to get? I can't find this 3 way anywhere else. [http://us.misumi-ec.com/vona2/detail/110300439630/](http://us.misumi-ec.com/vona2/detail/110300439630/)

Will it make a huge difference?


---
**Eric Lien** *March 31, 2016 23:31*

**+Jaimie Fryer** don't use those 3way corners if you can avoid it. Just bolt the extrusion directly together using button head bolts.


---
**Makeralot** *April 01, 2016 07:02*

**+Jaimie Fryer**  I am happy to provide you with aluminum extrusions and as well as other accessories, if you are interested please you message me.


---
**Roland Barenbrug** *April 01, 2016 07:43*

Or use  [http://www.motedis.com/](http://www.motedis.com/) in Germany. They provide 2020 extrusions for very affordable prices. Build my Eustathious with their components.


---
**Eirikur Sigbjörnsson** *April 01, 2016 18:14*

**+Jaimie Fryer**  I ordered all the alu profiles from Openbuilds for my (much delayed) Herculien build, and they ship to Iceland. Fasteners were pain to get together since McMaster didn't want to ship to Iceland. I had to source them from all over. Leadscrews I had made in China (shipment cost was much higher than the actual price for the screws). The rods I got (with some difficulties) from Igus through their representatives in Holland.  I can't recommend Igus after that adventure...


---
**Jaimie Fryer** *April 02, 2016 16:32*

OK so I've sat down today and looked at the BOM.



I can get the frame parts from openbuilds.

I can get the printed parts from my local Fablab.



I'm a little confused by the McMasterCarr section in the totals.

**+Eric Lien** Did you buy a pack of 100 for the bolts that you only needed less than 10 of? If I take 1 of every pack its ~$216.



Are the totals in the BOM for the driven version or the other version? What's the difference?


---
**Eric Lien** *April 02, 2016 16:46*

**+Jaimie Fryer** McMaster parts are just easy because of their models. I would source them elsewhere. My preferred vendor on bolts is [http://www.trimcraftaviationrc.com/](http://www.trimcraftaviationrc.com/)



And yes the McMaster bolts are base on bag qty. So if I needed 90 bolts, but they came in a bag of 100 from McMaster the qty would be 1. 


---
**Eirikur Sigbjörnsson** *April 02, 2016 19:58*

**+Jaimie Fryer** You can get some of the screws from Openbuilds ofc. I ordered most of the other fasteners from Trimcraft. the Aluminum plates you can get locally. I got mine from [http://www.stalnaust.is/](http://www.stalnaust.is/) I would recommend them because they cut with waterjet so the plates are not bent. Then you can order the motors, etc from [robotdigg.com](http://robotdigg.com), but they only ship fedex and the shipment cost is high.


---
**Jaimie Fryer** *April 03, 2016 20:36*

OK I've spent the best part of 2 days wholly looking into the BOM and I've concluded that perhaps I'm trying to save the cheapest part of my printer, the frame.



I want to thank you all for your contributions to my planning process. I think this design is kick-ass, but beyond my means.



I believe that as much as I admire the UM style gantry the complexity of the BOM in combination with Icelandic import taxes will simply prove too expensive and too much of a logistical headache for me. 



If you'd like you can have my Fusion360 files if there are people here looking for a smaller Eustathios Spider Design. (~240*240*200 build volume) All you have to do is ask :)



I think my future lays with the P3Steel, it has a great review on 3dhubs and I can source it from 2 European vendors.


---
*Imported from [Google+](https://plus.google.com/+JaimieFryer/posts/1cNx4yrqVyz) &mdash; content and formatting may not be reliable*
