---
layout: post
title: "I started off wanting to build the Eustathios Spider V2"
date: March 27, 2015 04:24
category: "Discussion"
author: Brandon Cramer
---
I started off wanting to build the Eustathios Spider V2. These are the pieces I have printed in Orange. If anyone is interested, they are almost free + shipping. I decided to go for the gold and build the HercuLien. 

![images/2ff09af1122623e9fc1054886f0dea44.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2ff09af1122623e9fc1054886f0dea44.jpeg)



**Brandon Cramer**

---
---
**Brandon Cramer** *March 27, 2015 05:32*

I still need to print the following for a complete set:



XY Axis Belt Tensioner A

XY Axis Belt Tensioner End

XY Axis Belt Tensioner A

XY Axis Belt Tensioner End

XY Axis Belt Tensioner B

XY Axis Belt Tensioner End

XY Axis Belt Tensioner B

XY Axis Belt Tensioner End

Y Endstop Mount

Z_Axis_Bed_Support

Z_Axis_Shaft_Mount_B

Z_Axis_Leadscrew_Support_V2

Z_Axis_Leadscrew_Support_Clamp_V2

Z_Axis_Leadscrew_Support_V2 (With Tensioner Config)

Z_Axis_Leadscrew_Support_Clamp_V2

Z_Stepper_Mount_V2

Viki2_Mount

X_Belt_Guard

Y_Belt_Guard



In the picture I have these items printed:



Bearing_Holder

Passthrough_Bearing_Holder

Bearing_Holder

Bearing_Holder

Passthrough_Bearing_Holder

Bearing_Holder

X Endstop Mount

Carriage_V4_Molex_Microfit_3

Carriage_V4_Part_Cooling_Duct

Bed_Leveling_Mount

Z_Axis_Shaft_Mount_A

External_Motor

Electronic_Package Mount

Outlet_Switch_Mount

HercuLien_Extruder_Base_V2

HercuStruder_Bearing_Pivot

HercuStruder_Dogbone_Connector

HercuStruder_Pin

Bowden_Tube_Nut_Trap

Eustathios_Threaded_Flange

50mm_Threaded_Spool_Holder_Full_Round



Where would I need to ship these? I would also need to order more Orange PLA filament to finish printing these items, but I can do it if we can agree on a fair price. The filament is BuMat PLA Orange. $30.00 per roll. 


---
**Eric Lien** *March 27, 2015 14:10*

BTW very nice looking prints. What is your current printer. Must be well tuned.


---
**Brandon Cramer** *March 27, 2015 15:01*

Thanks **+Eric Lien**  I'm using a Printrbot Simple Metal. I'm currently using the Simple Slow Config File from [http://www.printrbot.com/simple](http://www.printrbot.com/simple). It's slow but does a good job. :)


---
**Eric Bessette** *April 01, 2015 19:59*

I'm interested in the bottom z-axis mounts if you're sending out pieces.  With the back order problem at SDP/SI for the Eustathios V1 mounts, I would like to upgrade to the V2 solution.


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/QucYsHUggm1) &mdash; content and formatting may not be reliable*
