---
layout: post
title: "Just realised I don't have the 8mm bore 32tooth GT2 pulleys for my Z-axis"
date: April 07, 2015 04:37
category: "Discussion"
author: Ben Delarre
---
Just realised I don't have the 8mm bore 32tooth GT2 pulleys for my Z-axis. Is there any downside to taking 36tooth or 30tooth instead? I'm running 0.9 degree steppers with the misumi leadscrews.





**Ben Delarre**

---
---
**Eric Lien** *April 07, 2015 05:31*

I use 36 on HercuLien without issues.


---
**Eric Lien** *April 07, 2015 05:33*

But if you are building Eustathios Spider V2 you need the shoulder. I recessed the pulley slightly into a pocket... Or you could add shims.﻿


---
**Ben Delarre** *April 07, 2015 05:52*

Yeah I noticed that. Thanks **+Eric Lien**​


---
*Imported from [Google+](https://plus.google.com/114825475221343681660/posts/XV73zPckWhV) &mdash; content and formatting may not be reliable*
