---
layout: post
title: "A while ago it was mentioned that pillow blocks could be used instead of printed parts for holding the bearings"
date: May 07, 2015 09:51
category: "Discussion"
author: Miguel Sánchez
---
A while ago it was mentioned that pillow blocks could be used instead of printed parts for holding the bearings. I gave it a shot and this picture shows the result of some KP008 parts from [http://www.aliexpress.com/item/4pcs-8mm-KP08-kirksite-bearing-insert-bearing-shaft-support-Spherical-roller-zinc-alloy-mounted-bearings-pillow/1739080976.html](http://www.aliexpress.com/item/4pcs-8mm-KP08-kirksite-bearing-insert-bearing-shaft-support-Spherical-roller-zinc-alloy-mounted-bearings-pillow/1739080976.html)



As you can see the pressed bearing angle is random and quite far from 90 degrees. Does anyone know what chances do I have of fixing it? 



So now, besides bent rods please be aware you can get this problem too :-)

![images/bc13aaa3ec410f067e73cce32ca47184.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bc13aaa3ec410f067e73cce32ca47184.jpeg)



**Miguel Sánchez**

---
---
**Richard Earl** *May 07, 2015 10:19*

You should be able to adjust the bearing angle within the pillow block, they are quite stiff when new, I would suggest using some scrap shaft as a lever and fixing the block to something like a vice or scrap timber. I had this same issue when I got pillow blocks for my telescope shaft, after taking them back and explaining that they weren't square it was explained to me that they are adjustable to allow mounting the block at odd angles but allowing the bearings to be aligned.


---
**Miguel Sánchez** *May 07, 2015 10:25*

Thanks a lot **+Richard Earl** I was afraid I could break them. I have seem some pillow blocks are sold as self-aligning but these were not, so I was assuming the bearings were pressed with bad alignment and I was unsure if using a lever will fix the problem or would brake the bearing.


---
**Richard Earl** *May 07, 2015 10:35*

Hi **+Miguel Sánchez**  if they are not self aligning then you should find a lip on one side which the bearing is pressed against, compare the straight one with the others and if you can see a gap between the bearing and the lip, all you need to do is seat the bearing flush with the lip, the best approach would be to place the block on a solid surface and use a metal drift (rod) on the raised part of the bearings outside case and tap it into alignment with a hammer, do not try to hit the centre part of the bearing as that could damage the bearings inside.


---
**Richard Earl** *May 07, 2015 10:39*

**+Miguel Sánchez** I assumed they were self aligning from the picture, do the blocks look the same from both sides? and does the bearings outer case seem to be convex within the block? if so I would think they are self aligning.


---
**Miguel Sánchez** *May 07, 2015 12:03*

**+Richard Earl** Not sure. I posted a link to the seller's page [http://www.aliexpress.com/item/4pcs-8mm-KP08-kirksite-bearing-insert-bearing-shaft-support-Spherical-roller-zinc-alloy-mounted-bearings-pillow/1739080976.html](http://www.aliexpress.com/item/4pcs-8mm-KP08-kirksite-bearing-insert-bearing-shaft-support-Spherical-roller-zinc-alloy-mounted-bearings-pillow/1739080976.html). I failed to see any mention to the fact these are self-aligning. And while they use the word spherical in the title, it may well be a translation problem too. 


---
**Richard Earl** *May 07, 2015 12:51*

**+Miguel Sánchez**​ looking at the sellers page, the picture is of a self aligning pillowblock, the two slots that are visible in the picture on either side of the bearing are how the bearing is inserted into the block, the bearing is inserted into the slots while parallel with the block base, then rotated 90º, if you can see the slots then you have self aligning pillowblocks, sorry I should have thought to check the suppliers page. 




---
**Miguel Sánchez** *May 07, 2015 14:13*

Thanks again **+Richard Earl** . I will use some persuasion then to the bearings straight.


---
**Jeff DeMaagd** *May 07, 2015 18:23*

Yeah, the bearing outer race is a spherical surface. With care, it can be straightened. I would try tapping on the outer race with the largest socket that fits.


---
**Tim Rastall** *May 07, 2015 19:44*

I've got some of these and yes, they can be adjusted.  You can get them fairly true by using a spare piece of shaft as described above.


---
**Miguel Sánchez** *May 07, 2015 19:47*

Thanks **+Tim Rastall** and **+Jeff DeMaagd** 


---
*Imported from [Google+](https://plus.google.com/113179837473309823193/posts/jU6JxSS9hgu) &mdash; content and formatting may not be reliable*
