---
layout: post
title: "Noozle clogging. New method to clean. Variation of Atomic/cold pull"
date: October 19, 2015 16:15
category: "Discussion"
author: Gústav K Gústavsson
---
Noozle clogging. New method to clean. Variation of Atomic/cold pull.



Note: Here is a picture of the pull ends I got with my method AFTER I DID NORMAL ATOMIC/COLD PULL FIRST and had a clean filament and thought the problem was fixed but was not satisfied.



Well, most of you probable know the "Atomic" (cold pull variant) way to clean clogged heads. It has saved me many times but not allways. And I think I found a way to improve the result.



Some time ago I had a clogged head (On Ultimaker 2) after printing with some exotic materials. Followed the atomic procedure until the filament was coming out clean, started to print and the head clogged again. Repeated the procedure and got some dirt out until filement clean and started print again. But was not satisfied with the flow, seemed uneven but still feeding. Had studied the atomic method and had some ideas which might improve it so I decided to try some ideas. Will detail here step by step what I did for those who are not familiar with this method. Basically the atomic method with some deviations which seems to  improve the result dramatically. This is for Ultimaker 2 with bowden tube but you should be able to adapt it to your printer.



1: Remove bowden tube from head assembly.

2: Cut some lenght of filament from a spool (30-40cm). FOR BEST RESULT USE NYLON FILAMENT!!! If you don't have that available you have to experiment with the temperature settings and in my experience I had not as good results. And even Nylon temperatures will vary between manufacturers *note 1

3: Heat Noozle to 260 deg Celsius (MAX temp on Ultimaker 2 or max temp your filament can stand without instant burning).

4: Feed Nylon filament into the head and push down until (some) nylon starts to come out of the noozle, quickly lower the temperature to 90 deg Celsius. KEEP PUSHING the filament in because at 260 deg Celsius you are practically cooking the nylon and you don't keep feeding you will burn it and make the clogging worse. Also don't make the nylon make a blob on the noozle, preferable is a thin solid thread.

At about 182 deg Celsius it will get harder to push the filament in and you can then let up on the pressure on it until it stops flowing at about 179-180 degree Celsius.

5: Let the print head reach 90 degress Celsius and let it idle for 5 minutes or so. (Or more, longer times don't hurt. Grab some coffe or read your mail.)

6: Cut excess filament thread feed from noozle, about 1-2 mm from noozle.

7: Prepare to increase the noozle temp, pull tight on the filament and slowly increase the temp. At about 140 deg celsius the filament should loose its grip on the noozle and get loose. On the Ultimaker 2 I actually pull so that the spring holding the PFTE insulator and Noozle/heater assembly in place compresses a little and the assembly lifts from it's seat and snaps into place when the filament looses hold.

8: Examine filement end. Cut the dirty part off. (2-3 cm)

           Is some dirt on/in the filament? Then repeat the process from step 3.

           Is the excess filement that had exited the noozle on the filament end? If not repeat from step 3.

           Look through the noozle from the top. Can you see through and the noozle opening round? If not repeat from step 3.

Usually after 2 passes from step 3 everything is nice and clean and I can start printing again. If the flow is seriously hindered on the first pass or completely blocked you may have to do it 3-4 times to make sure.



*note: I have found out that using Nylon gives best results. The temp "window" from solid to completely fluid seemst to be wery small. Mostly rigid at 178 degrees C and like water at 185 degree C and floats into every available space trapping particles. Very strong immediatly after cooling and will not break about 1 cm into the noozle when you pull as other materials often seem to do, leaving worse clogging behind :-( And the nylon glides smootlly and doesnt stick when solid.



Here is a picture of the pull ends I got with my method AFTER I DID NORMAL ATOMIC PULL FIRST and had a clean filament and thought the problem was fixed but was not satisfied. Suspect this is accumalated dirt because it is much easier to clean the head after this cleaning and usually it looks like filament no 2 on the first try.



Hope somebody finds this usefull :-)﻿

![images/e2efd14e855dcd24f0b7147b5d46094c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e2efd14e855dcd24f0b7147b5d46094c.jpeg)



**Gústav K Gústavsson**

---
---
**Joe Spanier** *October 19, 2015 16:33*

What is the atomizer method. I've always found cold pulls to be best which is what you described. 


---
**Gústav K Gústavsson** *October 19, 2015 17:00*

Sorry should have been ATOMIC PULL. Which is similar to cold pull.

[http://support.3dverkstan.se/article/10-the](http://support.3dverkstan.se/article/10-the)



Have heard of bent rods and broken head mounts after cold pulls. I first did the Atomic/cold pull BEFORE I did it my way and already had a clean filament coming out. My method which is a small variant of the cold pull seems to me to be a lot more effective (see pictures) and places a lot less stress on printer parts.


---
**Gústav K Gústavsson** *October 19, 2015 17:03*

Edited to correct the methods name.﻿ Edited the start of the original post to clarify in the beginning what's this long post is all about.


---
**Whosa whatsis** *October 19, 2015 18:51*

Pretty much exactly what I do (described here: [http://bukobot.com/nozzle-cleaning](http://bukobot.com/nozzle-cleaning)). At the time I wrote that, I wasn't using bowden machines, but I do usually remove the tube to do them when I do have one. Of course, the Bukito also has a knurled knob for turning the drive gear manually, so it's pretty easy to control the filament form that end as well.



What kind of nylon do you use? I've found that the classic Nylon 618 works best, but that looks more like 645 or maybe Bridge. It's definitely important to keep is moving as you cool, especially if your nylon has absorbed a lot of moisture from the air, as the moisture boiling out creates bubbles that weaken the filament and may cause it to break rather than pulling out whole.


---
**Gústav K Gústavsson** *October 19, 2015 20:31*

Actually this is Bridge as I had some laying around. Have also used other nylon filaments (618 I think) with far better results than ABS or PLA which breaks more easily and make me start from scratch.

Had a look at your description and seems my methods is pretty much the same as yours as you said. Only think that I maybe stress more cooling down and leave it for 5 minutes or more, have actually shut the printer down in the cool down stage and slowly heated it up some hours later with good result. Have found out that if I cool down to 90 and then immediately heat to 140 and then pull it has happened when I have a bad clog that the filament is completely stuck and I have actually lifted the printer up from the table when I try to yank it out. Printer dangling on the filament which sometimes breaks so the printer falls on the table. Not desirable. (Ultimaker 2). But if I let it cool for 5 minutes or more and keep tension on the filament it seems to break loose at 136-140 degrees every time and pull out carbonized plastic if present. Without excessive force. Maybe the nylon contracts more if it is left to cool longer and break the bonds between the deposit and nozzle? Anyway the picture is after I had used the cold pull method and the filament was coming out clean but I was not satisfied, my experience said that I had to push harder or heat the nylon filament more to get normal flow. Just a feeling. So I tried again and basically did the same except let it cool down longer in the cool down process. Imagine my satisfaction and surprise when the filament came out as the leftmost filament in the picture. Have cleared some clogs after those in the picture (and even clogged the nozzle intentionally don't tell my employer:-) and the method has worked every time, comes out clean on second pass, max 3 to make sure.


---
*Imported from [Google+](https://plus.google.com/+GústavKGústavsson/posts/aAVQNjfN1Fw) &mdash; content and formatting may not be reliable*
