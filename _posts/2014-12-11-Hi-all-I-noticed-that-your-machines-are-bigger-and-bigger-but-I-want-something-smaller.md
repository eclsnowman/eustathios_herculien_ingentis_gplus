---
layout: post
title: "Hi all! I noticed that your machines are bigger and bigger :) but I want something smaller!"
date: December 11, 2014 08:57
category: "Discussion"
author: Jarek Szczepański
---
Hi all! I noticed that your machines are bigger and bigger :) but I want something smaller! e.g. 300x300x300mm external dimensions. Is it safe to scale your projects down? what I should be aware? I prefer belt version.





**Jarek Szczepański**

---
---
**Florian Schütte** *December 11, 2014 09:05*

The Eustathios has ~300*300*270 build volumen. I think that matches very well.


---
**Jarek Szczepański** *December 11, 2014 09:11*

if build volume is 300x300x270 it means that overall size is something 400-500mm on each side - pretty much to big for me.

For 300x300x300 OD build volume will be around 200mm3


---
**Miguel Sánchez** *December 11, 2014 09:18*

That is more or less my goal too: [http://www.thingiverse.com/thing:513955](http://www.thingiverse.com/thing:513955)



I am using 240mm extrusions for the square bases, depending on how you join them you have 280x280 or 240x240mm footprint. I expect to have a build surface of 150x150mm.


---
**Jarek Szczepański** *December 11, 2014 09:22*

**+Miguel Sánchez** awesome! thanks!


---
**Miguel Sánchez** *December 11, 2014 09:23*

**+Jarek Szczepański** Please note you'll need to shuffle things a bit to get X & Y motors inside the cube (I did not think it through and I had to put one of them on the top).


---
**Jeremie Francois** *December 11, 2014 23:24*

Registering to this thread. Same goal :) I like the small factor of these printers, but I would add that some of the steppers are outside of the frame which defeat one of my goal, as I want a movable, compact and sturdy printer at the same time ;) 90% of all my prints for the last years are well within 100x60x50mm...


---
*Imported from [Google+](https://plus.google.com/+JarekSzczepanski/posts/99U85RfXm6C) &mdash; content and formatting may not be reliable*
