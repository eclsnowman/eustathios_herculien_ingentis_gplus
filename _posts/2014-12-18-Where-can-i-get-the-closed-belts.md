---
layout: post
title: "Where can i get the closed belts?"
date: December 18, 2014 13:34
category: "Discussion"
author: Florian Schütte
---
Where can i get the closed belts? Not Sdp-si (i'm from germany) and not misumi (won't ship to private person). These are my last parts but i can't find them anywhere. Only at misumi and sdp-si :(





**Florian Schütte**

---
---
**Carlton Dodd** *December 18, 2014 13:39*

Misumi USA will ship to individuals (not sure about EU). I just put "hobbyist" for my business name. 

Give them a call. If they're anything like the USA division, they are very nice and helpful. 


---
**Florian Schütte** *December 18, 2014 13:44*

I called Misumi Germany...they where more like "fu.. off". My online registration was also denied. How long does int. shipping take at misumi us? Holidays are near....


---
**Daniel F** *December 18, 2014 14:20*

have you checked [robotdigg.com](http://robotdigg.com)? If you search for "Closed-loop GT2 Belt" you get a list with different lenghts.


---
**David Heddle** *December 18, 2014 14:25*

[robotdigg.com](http://robotdigg.com) have them, What size do you need? I have a few I could send you from the UK probably be quicker than from China.


---
**Florian Schütte** *December 18, 2014 14:57*

**+David Heddle** I need all three closed belts.

I'd really appreciate that.


---
**Florian Schütte** *December 18, 2014 14:59*

I can't find the right belts at robotdigg (502,497,475 teeth)


---
**David Heddle** *December 18, 2014 15:31*

Sorry I only have shorter ones.

Robotdigg do custom sizes [http://www.robotdigg.com/product/14/CUSTOMIZED-Timing-Pulley-and-Timing-Belt](http://www.robotdigg.com/product/14/CUSTOMIZED-Timing-Pulley-and-Timing-Belt) 


---
**Maxim Melcher** *December 18, 2014 15:54*

[http://www.maedler.de/](http://www.maedler.de/)


---
**Eric Lien** *December 18, 2014 16:41*

I think the distance is longer than the carriage travel. You might be able to use open belts with the old zip tie trick?


---
**James Rivera** *December 18, 2014 18:46*

Hey  #misumi  -- your German office's customer service seems to be lacking--you might want to look into it.


---
**Florian Schütte** *December 18, 2014 18:54*

**+James Rivera**​ Thank you. I never thought of this option. Thanks **+Eric Lien**​ i will try this is a Option.


---
**Florian Schütte** *December 18, 2014 18:59*

**+Maxim Melcher**​ I can't find belts with 2mm pitch. Their products are starting at T2,5


---
**Maxim Melcher** *December 18, 2014 21:02*

**+Florian Schütte**

[http://www.keilriemenexpress.de/zahnflachriemen-zahnriemen-shop/zahnriemen-zoellig/zahnriemen-mxl.html](http://www.keilriemenexpress.de/zahnflachriemen-zahnriemen-shop/zahnriemen-zoellig/zahnriemen-mxl.html)



[https://www.maedler.de/product/1643/1616/981/zahnriemen-mxl-025](https://www.maedler.de/product/1643/1616/981/zahnriemen-mxl-025)



T2 gibt es nicht. Wo hast Du die Zahnscheiben her?


---
**Mike Thornbury** *December 18, 2014 23:14*

Aliexpress - you can get any size you want.


---
**Jo Miller** *January 18, 2015 13:00*

@ Florian

Hi Florian,  being located in Lübeck, I made the same experience with Mitsumi/europe...ggrrr 

anyway  ,  what size of   Lead Screw for the Z axis did you go for    TR10x2  or a TR12x2  ?  (or a plain "Gewindestange ?)



(I´m building the Herculien)  and where to order it here in Europe ? 


---
**Mike Thornbury** *January 18, 2015 13:04*

**+Florian Schütte** This company has belts in hundreds of sizes: [http://www.aliexpress.com/store/702327](http://www.aliexpress.com/store/702327)



Cheap and cheap shipping to Europe. 


---
**James Rivera** *January 18, 2015 22:56*

**+MISUMI Europa GmbH**  


---
*Imported from [Google+](https://plus.google.com/111818668280736846325/posts/4L6MANWznj3) &mdash; content and formatting may not be reliable*
