---
layout: post
title: "I give you the nightmare that is the black Euth's first print using slic3r for the first time in over 3 years"
date: May 27, 2015 13:34
category: "Show and Tell"
author: Derek Schuetz
---
I give you the nightmare that is the black Euth's first print using slic3r for the first time in over 3 years. The original dimensions are 50.80. 

![images/d057d91fc03d62f898a1219e9819b461.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d057d91fc03d62f898a1219e9819b461.jpeg)



**Derek Schuetz**

---
---
**Eric Lien** *May 27, 2015 14:45*

Very nice. And surface finish looks great, just need a little more light to see more detail. 


---
**Eric Lien** *May 27, 2015 14:46*

I just noticed the corner ringing. What are the speeds, acceleration, temps, material, etc.


---
**Derek Schuetz** *May 27, 2015 15:06*

80 infil and perimeters 60 internal perimeters. 10 sec minimum layer time 220 black abs 


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/R5Ezuffitv1) &mdash; content and formatting may not be reliable*
