---
layout: post
title: "I was updating the image for the G+ Group, and decided to have some fun"
date: March 01, 2016 17:18
category: "Discussion"
author: Eric Lien
---
I was updating the image for the G+ Group, and decided to have some fun. Many people have problems visualizing the size of these machines. So I thought... why not put them in a model along side printers many people know the size of. So without further adieu here is Herculien and Eustathios Spider V2 compared to the size of an Ultimaker2 and a Prusa I3 (steel variant).



I figure this might help people not be caught off guard when building something like the HercuLien. Most people who have built them tell me after starting: "Crap, I am going to need a bigger desk" :)



﻿



![images/f04ffe963d26f3550de81a9dd4ca047f.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f04ffe963d26f3550de81a9dd4ca047f.png)
![images/4f8748381e4c43cc44f3877c4a7a7906.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4f8748381e4c43cc44f3877c4a7a7906.png)
![images/ea2fb8945e4a0338d8833367e5dc1ced.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ea2fb8945e4a0338d8833367e5dc1ced.png)
![images/e5c16843af48f0a2375be06394bc81a8.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e5c16843af48f0a2375be06394bc81a8.png)

**Eric Lien**

---
---
**Jim Stone** *March 01, 2016 17:27*

hey the crap bigger desk is what i said XD


---
**Zane Baird** *March 01, 2016 17:32*

Great addition! Really puts it all in perspective.


---
**Eric Lien** *March 01, 2016 17:33*

**+Peter van der Walt**  Yeah, it is like I took a printer and smashed it with the OVERKILL HAMMER for about half an hour. I like it, but in all reality it is more printer than almost anyone needs. At the time I was printing prototype foundry pattern tools (to determine correlation of model geometry to real world shrink factors). It worked great for that because we could adjust the models to perfectly account for non-scaler shrink of ductile iron prior to committing to expensive production tooling.



But now it seams a bit large to print Yoda heads and Pokemon for my son :)


---
**Eric Lien** *March 01, 2016 17:34*

**+Jim Stone** You are indeed... just not the first :)


---
**Ariel Yahni (UniKpty)** *March 01, 2016 17:40*

Is there a word for that feeling you get when you want to just build build build 


---
**Ray Kholodovsky (Cohesion3D)** *March 01, 2016 17:57*

Oh, overkill hammer. **+Ariel Yahni**


---
**Samer Najia** *March 01, 2016 18:40*

In my case it would be...i need a bigger garage!


---
**Ariel Yahni (UniKpty)** *March 01, 2016 19:40*

**+Ray Kholodovsky**​ overkill precision hammer


---
**Ted Huntington** *March 01, 2016 20:49*

very nice- I didn't realize that the Eust is so much bigger than the Ult and Prusa. For people looking for a neat and cheap solution to housing two Eustathios sized printers I just bought this cart for $60 from Harbor Freight: [http://www.harborfreight.com/24-in-x-36-in-two-shelf-steel-service-cart-62587.html](http://www.harborfreight.com/24-in-x-36-in-two-shelf-steel-service-cart-62587.html). Plastic is probably better but the metal one is about 1/2 of the price of the plastic one [http://www.harborfreight.com/24-inch-x-36-inch-industrial-polypropylene-service-cart-92862.html](http://www.harborfreight.com/24-inch-x-36-inch-industrial-polypropylene-service-cart-92862.html) and has a ton of space on and in it. A 3D printer on wheels is easier to relocate if necessary too. Just updating: I measured and the bottom shelf is about 1" too short for a Eustathios :(


---
**Sébastien Plante** *March 01, 2016 23:38*

Oh, levitating printer! :D


---
**Eric Lien** *March 02, 2016 01:54*

**+Sébastien Plante** yeah, I moved them up in the model because the shadow made it harder to see the printers at the bottom. But it does look kind of odd.


---
**Daniel Seiler** *March 02, 2016 14:40*

Thanks, you saved me. Nice thing about Herculien is dual extrusion - has anyone modded Eustathios to have dual extrusion?


---
**Eric Lien** *March 02, 2016 15:56*

**+Daniel Seiler** just print the HercuLien carriage and replace the controller with a dual extrusion capable one. The carriages are interchangeable, just get the right cross rod bushings :)


---
**Liam Jackson** *March 02, 2016 21:40*

Great idea! That's a really cool image! 


---
**Daniel Seiler** *March 03, 2016 00:34*

**+Eric Lien** Oh - nice! Thank you!


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/4SwZfMsFDvD) &mdash; content and formatting may not be reliable*
