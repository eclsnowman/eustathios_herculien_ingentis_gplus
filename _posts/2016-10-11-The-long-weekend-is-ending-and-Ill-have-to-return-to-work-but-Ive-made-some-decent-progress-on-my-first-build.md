---
layout: post
title: "The long weekend is ending and I'll have to return to work, but I've made some decent progress on my first build"
date: October 11, 2016 09:21
category: "Build Logs"
author: Neil Merchant
---
The long weekend is ending and I'll have to return to work, but I've made some decent progress on my first build.  All  the belts are in place for the movement systems and seem to work reasonably well; my next step is to find a place locally that can cut a plate for the bed, then I can see about wiring this thing up properly. I'm very pleased with the design so far - I'm excited to get this thing working.

![images/3b60d11dd5bda8a6aef7f92cca81f29f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3b60d11dd5bda8a6aef7f92cca81f29f.jpeg)



**Neil Merchant**

---
---
**Eric Lien** *October 11, 2016 13:47*

So great to see another printer coming to life. Please share some first moves and first prints videos when you get there. That is always my favorite part. 


---
**jerryflyguy** *October 11, 2016 16:33*

It looks like you came into my basement and took a pic of mine! Looks great!


---
*Imported from [Google+](https://plus.google.com/105839137113604539428/posts/P1Xhr2QN3xd) &mdash; content and formatting may not be reliable*
