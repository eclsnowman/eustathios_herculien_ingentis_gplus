---
layout: post
title: "For those who use the Spyraflo self aligning bushings, did you press them into the receiving bore or just to the knurl> Do the faces sit flush to the surface or stick out?"
date: September 08, 2018 16:44
category: "Discussion"
author: Dennis P
---
For those who use the Spyraflo self aligning bushings, did you press them into the receiving bore or just to the knurl> 



Do the faces sit flush to the surface or stick out? 



I am trying to assemble a remodeled carriage and I think that the bore needs a slight counterbore to properly accept the bearing. 





**Dennis P**

---
---
**Eric Lien** *September 08, 2018 17:39*

The outside flange sits proud of the surface in the original design.


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/d3PSER47RGC) &mdash; content and formatting may not be reliable*
