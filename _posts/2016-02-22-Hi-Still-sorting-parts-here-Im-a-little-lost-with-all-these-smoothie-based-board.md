---
layout: post
title: "Hi, Still sorting parts here. I'm a little lost with all these smoothie-based board"
date: February 22, 2016 10:25
category: "Discussion"
author: Maxime Favre
---
Hi,

Still sorting parts here.

I'm a little lost with all these smoothie-based board.

I prefer to have an LCD but it seems that the smoothie isn't so much friend with the viki2 and adapter boards will not be available soon (or never).

In the other end Azteeg X5 looks good, Do you recommand them over the smoothie ?

Is there an other "ultimate board" that I am missing ? Rumba ? Others?





**Maxime Favre**

---
---
**Eirikur Sigbjörnsson** *February 22, 2016 10:49*

Well there is Ultratronics which is new and runs Marlin (not smoothie based)



[https://reprapworld.com/?products_details&products_id/1177/cPath/1591_1750](https://reprapworld.com/?products_details&products_id/1177/cPath/1591_1750)



then there is this adapter for using GLCD on the Smoothie



[http://robotseed.com/index.php?id_product=23&controller=product&id_lang=2](http://robotseed.com/index.php?id_product=23&controller=product&id_lang=2)


---
**Hakan Evirgen** *February 22, 2016 12:08*

**+Maxime Favre** I use Smoothieboard together with Viki2 without any problems. If you disable onboard LEDs on the Smoothieboard then you can use all features of Viki2.



For that you need to solder the headers on the board and connect the Viki2 cables to it as you like. Then you just put in the config file the pins, where you connected what.



if you need, I can give you a sample config file and some photos how I connected it.


---
**Maxime Favre** *February 22, 2016 12:34*

**+Hakan Evirgen** Ha ! Sample config and some wiring pics would be welcome ! (and maybe added on github ?)


---
**Eric Lien** *February 22, 2016 13:15*

I don't have one but: [https://groups.google.com/forum/m/#!topic/smoothieware-support/yz59HdNxGC4](https://groups.google.com/forum/m/#!topic/smoothieware-support/yz59HdNxGC4)


---
**Maxime Favre** *February 22, 2016 13:26*

Yeah I just found this link too, thanks.

I think I'll go for the smoothieboard. Sadly there's no smoothie-based board compatible with TMC2100 drivers ;(


---
**Eric Lien** *February 22, 2016 13:36*

If you only plan for a single extruder the X5 mini V2 is a great option.


---
**Maxime Favre** *February 22, 2016 14:06*

Dispite the extruder limitation, price and ethernet, is there any advantage of one vs the other ? Stepper performance ?

Sorry for all these question, it's hard to find any comparison.


---
**Zane Baird** *February 22, 2016 14:36*

The X5 is much smaller and allows you to select microstepping all the way from full-steps to 1/128 microstepping. Plus the pin arrangement is a bit more aesthetically pleasing and the viki2 connects to it with a simple wire harness


---
**Maxime Favre** *February 22, 2016 15:04*

I think you convinced me :D Thank you ;)

Everything is ordered except misumi, I still need to source some screws locally


---
**Jim Stone** *February 22, 2016 19:22*

screws from the place referenced in the BOM is great.


---
**Maxime Favre** *February 22, 2016 19:30*

**+Jim Stone** Yes BOM is really great to work with. I ended to order screws by McMaster. Prices+Shipping are just insane compared to Switzerland's prices ô0

Now printing...


---
**Hakan Evirgen** *February 23, 2016 16:53*

**+Maxime Favre**  at the moment I am busy to search and compile the Viki2 config+photos. Remind me in a week or two and I will do so.


---
**Jo Miller** *February 25, 2016 07:30*

have a look at [http://www.thingiverse.com/thing:745001](http://www.thingiverse.com/thing:745001)



i use it on my herculien and it works fine with smoothieware,  & the display you can order with it is just 25$


---
*Imported from [Google+](https://plus.google.com/+MaximeFavre/posts/GTcpqmPN6X1) &mdash; content and formatting may not be reliable*
