---
layout: post
title: "Working on trying to dial in carbon fiber abs"
date: February 28, 2016 20:00
category: "Show and Tell"
author: Eric Lien
---
Working on trying to dial in carbon fiber abs. Going well so far but I needed to add some extra restart distance to avoid gaps after retract. Anybody have any pointers for me?



Current settings:

70mm/S 85% outer perimeter

240c hot end

70c bed (for now, will go up but it sticks really well to PEI even at 70C)

5.5mm retract @ 60mm/s, 0.5mm extra restart distance



Wish me luck :)








**Video content missing for image https://lh3.googleusercontent.com/-00r0VnVmglU/VtNR35tuDxI/AAAAAAACFgM/4g28LU0VFxI/s0/VID_20160228_133410.mp4.gif**
![images/5b474ac22d4e978fcdc182ae8c621a2a.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5b474ac22d4e978fcdc182ae8c621a2a.gif)



**Eric Lien**

---
---
**Matt Miller** *February 28, 2016 20:16*

Hardened e3d nozzle?


---
**Eric Lien** *February 28, 2016 20:25*

**+Matt Miller**  I will be honest, I bought some cheap stainless ones of aliexpress a while back when I got the deal on this filament (super discounted). I wanted to try it out first to see if it is something I would like. And I do, so time to get some proper Official E3D hardened ones now :)


---
**Eric Lien** *February 28, 2016 20:37*

I am also looking at this stuff with interest: [http://www.3dxtech.com/glass-fiber-reinforced-amphora-petg-filament/](http://www.3dxtech.com/glass-fiber-reinforced-amphora-petg-filament/)



I love PETG (my favorite by far). But want a little stiffer parts. Looks like a winning combo and better colors than just cf-petg has to offer.


---
**Chris Brent** *February 28, 2016 21:43*

Edit: Just re-read and noticed the comment about color. I guess that's why the 3dxtech link is glass fibre as the carbon will always have black specs.

That one is glass fiber, which is an interesting idea too. This one looks like carbon [http://www.3dxtech.com/carbon-fiber-reinforced-petg-3d-printing-filament/](http://www.3dxtech.com/carbon-fiber-reinforced-petg-3d-printing-filament/) Have you used their filamnet before? I've never heard of them. Of course there's also ColorFab [http://colorfabb.com/xt-cf20](http://colorfabb.com/xt-cf20) which looks to be the same polymer. I thought Amphora was some "new" polymer? Is it just PETG or PET(some other modifier)? 


---
**Eric Lien** *February 28, 2016 22:26*

I have used their ABS before (via push plastic). Great stuff.


---
**Eric Lien** *February 28, 2016 22:27*

Well I just found out the hard way they miss labeled it, and I'm printing carbon fiber PLA not carbon fiber abs. I was wondering why I was getting funny extrusion. Dropped the temp down to 205 and turned on the fan and getting much better results, unfortunately I have no use for PLA :-(


---
**Jim Stone** *February 28, 2016 23:00*

was about to say. i havent heard of ABS carbon fiber infused filament yet :P


---
**Eric Lien** *February 29, 2016 00:06*

**+Jim Stone** oh it's available.


---
**Zane Baird** *February 29, 2016 00:29*

**+Jim Stone** there is lots of it actually. I like 3dxtech filament but I've really only printed a couple kg of the red stuff (carbon nano tubes) and a sample of their CF filled ABS. CF filled filament are definitely more problematic through 0.4 mm noodles though **+Eric Lien**. They don't flow so well through small nozzles. It can be worked around, but consistency is tough


---
**Glenn West** *February 29, 2016 00:29*

They say it will ablade the head unless I have the right type in as little as a quarter role 


---
**Zane Baird** *February 29, 2016 00:32*

**+Glenn West** it's the truth. Turns a 0.4mm brass nozzle into a 0.6mm in about 100g or less (speaking from experience)


---
**Jeff DeMaagd** *February 29, 2016 04:05*

I'm doing a crash course in CF-PETG this week. I am prepared with hardened nozzles and drive wheels, but the part is big and time is short., with no experience with PETG on this machine, hot end or extruder configuration.


---
**Glenn West** *February 29, 2016 04:06*

Wow I worry about the copper pla i have.

Guess I better change to the new nozzle before printing 


---
**Chris Brent** *February 29, 2016 04:07*

**+Eric Lien** So what's wrong with Carbon PLA? Glass transition still too low?


---
**Eric Lien** *February 29, 2016 05:48*

**+Chris Brent** I wanted to print a new carriage for my enclosed printer. PLA doesn't cut the mustard when the çhamber is 50-55C. Plus I just tendnot to like PLA. It's a preference really, back from my e3d v5 days when I used to fight jams alot.


---
**Bud Hammerton** *February 29, 2016 13:37*

**+Eric Lien** I was given some test filament by William at [INTSERVO.COM](http://INTSERVO.COM). It is something new they are testing, carbon fiber filled nylon filament. So at least very soon there will be carbon fiber in nearly all common filament types. Same proviso applies for all nylon, it will warp without proper environmental controls, similar but not as severe as ABS. You might ask him about it.



Prints very light parts even at 100% infill and it is much stiffer than straight nylon filament. Does need heat, had to print at 260°C.



Plus the video was interesting seeing how your two heatsink fans spin at different rates.


---
**Eric Lien** *February 29, 2016 13:55*

**+Bud Hammerton** that's the LED PWM effect combined with the second fan slowly failing :)


---
**Hakan Evirgen** *March 01, 2016 23:56*

So how are your prints? Can we see some end results?


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/E5n9DP7goB7) &mdash; content and formatting may not be reliable*
