---
layout: post
title: "More questions for my build: I broke my thermistor while trying to mount it, and have to order a replacement, has anyone used these: Since I was using the m3 screw to mount it, this would work nicely"
date: February 15, 2016 03:12
category: "Discussion"
author: Bruce Lunde
---
More questions for my build:



I broke my thermistor while trying to mount it, and have to order a replacement, has anyone used these:

[http://www.ebay.com/itm/3pcs-M3-screw-on-NTC-3950-Thermistor-100K-modular-1M-wire-3d-printer-hot-end-/331721610140?var=540836069669](http://www.ebay.com/itm/3pcs-M3-screw-on-NTC-3950-Thermistor-100K-modular-1M-wire-3d-printer-hot-end-/331721610140?var=540836069669)



Since I was using the m3 screw to mount it, this would  work nicely.



Second question:  Where did you all mount the limit switches?  I scoured the  pictures, but I don't see the locations. 



These are the last two items I need to wrap up to complete the build and start the break in ops.





**Bruce Lunde**

---
---
**Ray Kholodovsky (Cohesion3D)** *February 15, 2016 03:21*

I remember seeing these. I got a similar one but with a short cable and a connector from China when my e3dv6 thermistor burned out. Easy swap, works great now. 


---
**Ray Kholodovsky (Cohesion3D)** *February 15, 2016 03:22*

For domestic that's an amazing price. 


---
**Bruce Lunde** *February 15, 2016 03:45*

I'll give them a try, cannot hurt!


---
**Eric Lien** *February 15, 2016 03:50*

These are some old pictures, but hopefully they're helpful. I'm not around my printer right now: 



[http://imgur.com/rjMs082](http://imgur.com/rjMs082)

[http://imgur.com/Ywg5GVT](http://imgur.com/Ywg5GVT)




---
**Bruce Lunde** *February 15, 2016 03:58*

Thanks Eric!


---
**Jim Stone** *February 15, 2016 04:34*

right side extrusion front corner.



and



front extrusion leftside corner.


---
**Glenn West** *February 15, 2016 11:45*

I'd love a high quality of this 

I have tried a few and they tend to be a bit easy to break. I'd broke 4 or 5 of them. 

Especially if u print near 250


---
**Sébastien Plante** *February 15, 2016 17:17*

I'm using those on my E3Dv6, but from another vendor. I love them ! Easy to install/swap ! 



 (RP One Lab : [http://m.ebay.ca/itm/321341736569?_mwBanner=1&varId=510241930326](http://m.ebay.ca/itm/321341736569?_mwBanner=1&varId=510241930326) ).﻿


---
**Sébastien Plante** *February 15, 2016 17:19*

**+Glenn West** Might want to check my vendor then, I ordered 3 long time ago, still running on the first (did about 2 roll of PLA since).


---
**Bruce Lunde** *February 15, 2016 17:24*

I will send feedback, I have ordered a 3 pack. While waiting, I will get the limit switches installed.  Thanks all for the feedback!




---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/gTMtP56JSKH) &mdash; content and formatting may not be reliable*
