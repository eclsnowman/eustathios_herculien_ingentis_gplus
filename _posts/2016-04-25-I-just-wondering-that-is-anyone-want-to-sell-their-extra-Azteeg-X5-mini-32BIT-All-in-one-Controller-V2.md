---
layout: post
title: "I just wondering that is anyone want to sell their extra \"Azteeg X5 mini 32BIT All-in-one Controller V2\" ?"
date: April 25, 2016 06:58
category: "Discussion"
author: Botio Kuo
---
I just wondering that is anyone want to sell their extra "Azteeg X5 mini 32BIT All-in-one Controller V2" ?



I really want to buy one for my printer. 



Thanks





**Botio Kuo**

---
---
**Blake Dunham** *April 25, 2016 16:23*

I have an original azteeg x5 mini with headers and not the terminal blocks if you're interested 


---
**Ryan Carlyle** *April 25, 2016 18:56*

Note that the v1 used 8825s while the v2 uses 6128s. The 6128s work with all motor/voltage combos whereas 8825s are sensitive to component selection to avoid rippled in prints. 



I'm sure Panucatt is working on getting the v2 back in stock, but I'm hearing that 6128 chips are getting hard to find. 


---
**Botio Kuo** *May 07, 2016 03:16*

**+Ryan Carlyle** hi, so do you know when they will have v2 ready for sell ?


---
**Blake Dunham** *May 07, 2016 03:34*

The V2's seem to have been subjected to bad components so as far as we know, panucatt is still trying to find a new manufacturer 


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/YH2qsLckx1d) &mdash; content and formatting may not be reliable*
