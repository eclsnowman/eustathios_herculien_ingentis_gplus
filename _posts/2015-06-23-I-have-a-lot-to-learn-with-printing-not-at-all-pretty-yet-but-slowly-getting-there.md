---
layout: post
title: "I have a lot to learn with printing, not at all pretty yet but slowly getting there"
date: June 23, 2015 22:03
category: "Show and Tell"
author: Vic Catalasan
---
I have a lot to learn with printing, not at all pretty yet but slowly getting there. Yup, definitely need to slow printing down, I believe I have some imperfections on some parts, it could be the bearing mounts for the rods a bit loose, bearing just fall off when assembling the gantry and I can rattle it a bit, will shoe goop the bearings in so there is no play.  On this video of the Iphone amp stand , I can see I have quite a bit of miss extrusions and a fair bit of spider webbing. I will try .03 layer instead of .5 and maybe some extrude retract setting. 


**Video content missing for image https://lh3.googleusercontent.com/-7mxzPZG1vNg/VYnVgZgfpLI/AAAAAAAAN94/cbFAHDC4n44/s0/20150621_182728.mp4.gif**
![images/8a0458bd781b3f5ceec3a1398d928742.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8a0458bd781b3f5ceec3a1398d928742.gif)



**Vic Catalasan**

---
---
**Jim Wilson** *June 24, 2015 00:48*

What is your hotend nozzle diameter? .5 is a VERY large layer height, you should be able to do .2 easily with most hot ends.


---
**Vic Catalasan** *June 24, 2015 01:26*

I am using the Volcano's .6 nozzle and with that print was set at .5 layer  and I have recently tried .3 with much better result. 


---
**Vic Catalasan** *June 24, 2015 17:19*

I am pretty happy with printing in PLA and I cant say so much for ABS till I get it enclosed with a heated bed. I can only strain myself a short while holding the heat gun with ABS and I am pretty much guessing the temperature


---
*Imported from [Google+](https://plus.google.com/+VicCatalasan/posts/e7nHtJ7ieMp) &mdash; content and formatting may not be reliable*
