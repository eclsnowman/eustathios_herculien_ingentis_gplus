---
layout: post
title: "Still a lot of work to do, but it's up and running"
date: May 19, 2015 20:27
category: "Show and Tell"
author: Walter Hsiao
---
Still a lot of work to do, but it's up and running.  I accidentally mounted the extruder carriage backwards, and cracked a few of the bearing holders, so I have to rebuild the top.  Other than that, the printer assembly was much easier than I expected.  The ball screws seem to be working fine, and the graphite bushings seem to be working too, though I don't have anything to compare them too.  I did have some problems with binding when placing two bushings in a row, but printing a single 60mm NinjaFlex sleeve to hold both together seems to have fixed it.



The silicone heater arrived today, so that will be going in next, and I'm considering converting to direct drive bondtech at some point.



![images/56ad014bca0a1b1ec07088616b0bd3ee.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/56ad014bca0a1b1ec07088616b0bd3ee.jpeg)
![images/eabd8883c4d143830d5446b100be8260.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eabd8883c4d143830d5446b100be8260.jpeg)
![images/8d38a1366010177188e1e3c405d1a295.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8d38a1366010177188e1e3c405d1a295.jpeg)
![images/64dffb679d07311aa9ea9d386a2f101d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/64dffb679d07311aa9ea9d386a2f101d.jpeg)
![images/062c4f4973fdb8a095e0546c76f4c1f0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/062c4f4973fdb8a095e0546c76f4c1f0.jpeg)
![images/584c9bd6629fddcf16f54843a07255b8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/584c9bd6629fddcf16f54843a07255b8.jpeg)
![images/a3f96e15dbeb98b8fa29d7fbe7da991e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a3f96e15dbeb98b8fa29d7fbe7da991e.jpeg)

**Walter Hsiao**

---
---
**Chris Brent** *May 19, 2015 20:40*

Nice looking build. 


---
**Walter Hsiao** *May 19, 2015 20:46*

**+Alex Lee** I'm sure no one will notice if you add one more printer to your collection :)



Thanks Alex!  I'll let you know if I need anything.  I just went with a rectangular plate for the heatspreader to keep it simple.  I also made it 16" long to avoid having to cut glass plates (though I'm kind of regretting that last bit after trying out some nicer glass cutting tools last weekend at the maker faire).


---
**Isaac Arciaga** *May 19, 2015 21:01*

**+Walter Hsiao** the translucent black abs looks great with the clear extrusions. Creative idea using NinjaFlex as a sleeve for the bushings!


---
**Gus Montoya** *May 19, 2015 21:33*

**+Alex Lee**  Uhm I think you need an intervention. hahaha


---
**Vic Catalasan** *May 19, 2015 22:33*

Nice build and looks pro!


---
**Eric Lien** *May 19, 2015 22:48*

Love how you flipped the split in the bed mount supports. That makes more sense allowing the bolting of the piece to the bed frame to hold the gap closed. I will likely have to incorporate that one.



Super clean build. Love all your design choices.﻿


---
**Walter Hsiao** *May 20, 2015 01:22*

**+Eric LeFort** I like the color too, but I would recommend against using it for at least some of the printer parts (probably applies to clear ABS in general).  It seems too brittle for the bearing mounts and the rod holders, I'm going to have to reprint them since some of them have started to crack.


---
**Walter Hsiao** *May 20, 2015 09:05*

The problem with the parts cracking may be specific to the hatchbox transparent ABS I'm using, I didn't mean to imply that your PLA parts are likely to have the same problem. I've printed parts before that were under tension with blue hatchbox abs and it ended up cracking months later (if I had remembered that when I started printing the parts, I probably would have chosen a different filament).  The same part in semi-transparent PLA lost its springiness over time, but did not crack.  Still, an spare set of parts is good to have (though a spare printer is even better :)


---
**Frank “Helmi” Helmschrott** *May 27, 2015 10:07*

This is an Amazing build, Walter. Are you going to release the parts you modified? I was also thinking about using ball screws and the self lubricating graphite bushings.


---
**Walter Hsiao** *May 27, 2015 19:51*

Thanks! yeah, any new parts or parts with significant modifications will be shared.  I usually upload things to thingiverse (under walter or tchotchke), so they'll probably show up there.


---
**Frank “Helmi” Helmschrott** *June 10, 2015 16:35*

**+Walter Hsiao** i'm already printing some of your parts and am trying to find out all the changes you made to keep up with everything else. I like that you switched to a square aluminum plate - that makes it much easier to source. Did you already write down something about your changes? What sizes does your aluminum sheet have? Thanks in advance.


---
**Walter Hsiao** *June 10, 2015 21:38*

I haven't written anything down, but that's a good idea.  I'll document my changes, good and bad, to give people an idea if it's something they want to do.



I made my bed out of a 1/4 inch sheet of 360mm x 16in aluminum.  I only went with 16" because 12x16" is a common size for precut glass in the states, If i built another printer from scratch, I'd probably make it 360x360.  I went with 1/4" since I thought it was more likely to be flat - it wasn't (I got my aluminum cut at a local scrap yard).  It was easy to sand it flat enough for glass though.  I was also hoping the thicker aluminum would be less likely to change height with temperature (don't know if it made any difference there).



If you're building a heavy bed with ball screws be aware of the weight, if you make it too heavy, it'll move on it's own.  In my case I can probably add about 4-5kg to the bed before it'll drop on its own when powered off.


---
**Frank “Helmi” Helmschrott** *June 11, 2015 04:55*

Thanks, **+Walter Hsiao** - i thought of going with 360x360 but i think 4-5mm is way enough 1/4" makes it 6,35mm which i think would be really much in terms of heating up etc.



The flatness of the aluminum is indeed something that is a bit hard to reach. I've approached some german sellers on ebay to talk with them upfront. There are many different flavours of aluminum out there, maybe they can give me some information on that. Mostly they're only surprised why you would need it that flat :)



Is there any decision (apart from the 10mm cross bars) that you didn't make again? For all that i have seen so far everything looks quite great.


---
**Walter Hsiao** *June 11, 2015 09:08*

Other than what was already discussed previously (cross bars, bed size, ball screw milling), I can't think of any significant things I wish I had done differently, but I'm still working on it.  I like having max limit switches and I probably should have added those earlier.  A lot of the t-nuts from RobotDigg needed to be deburred to fit, I should have ordered extras, would have saved a lot of filing.  And next time I'll probably pay Misumi to do the tapping for me.


---
**Frank “Helmi” Helmschrott** *June 23, 2015 18:50*

**+Walter Hsiao** did you already release the modified carriage you used? I guess you only modified the holes for the Robotdigg-Bushings, or did you do anything else?


---
**Walter Hsiao** *June 23, 2015 19:41*

Yeah, the only change was to make the outer 15.9mm holes go all the way through.  I didn't post it since it was a minor change, but I can if it would be useful.


---
**Frank “Helmi” Helmschrott** *June 23, 2015 21:27*

oh right, you did use the 10mm rods - i forgot. I'm going with 8mm so i'll use the 12mm OD bushings from Robotdigg. Guess i'll make it 13,5mm - that should fit well with a small sleeve around it.



The sleeves alredy worked well on the outer rods. I printed them from FPE Shore 45D as Filaflex (similar to Ninjaflex) didn't work on my Ordbot with Bowden.


---
**Frank “Helmi” Helmschrott** *June 27, 2015 14:14*

**+Walter Hsiao** how did you get your robotdigg graphite bushings aligned correctly on the carriage? I did install mine today including the soft sleeves (though my soft filament isn't exactly as soft as NinjaFlex cause i can't print it on my bowden now) but movement is way too stiff. I'm a bit stuck as how to improve that. 


---
**Walter Hsiao** *July 01, 2015 01:21*

My setup is a bit different with the 10mm rods, but here's what I tried.  I first tried putting sleeves on each bushing, but couldn't get that to work.  I then tried putting the two bushings in a single 60mm sleeve and that worked without having to anything more than a rough alignment.  I also trimmed some holes in the sleeves where the rods cross (my bushings are 14mm in diameter so there isn't any clearance for NinjaFlex where they cross), shouldn't be an issue with the RobotDigg bushings though.



My current extruder is larger, and I initially had problems with binding now that the bushings are separated.  I ended up using shorter 10mm NinjaFlex rings on the outside of the bushings so it isn't as constrained.  Using the shorter NinjaFlex rings makes it more like the Ultibots self-aligning bushing setup.



I did try printing a PolyFlex bushing sleeve before I used NinjaFlex, but it seemed too stiff.  My sleeves are about 1mm thick though, a 2mm thick sleeve could be made softer by printing with less infill / shells.  If you use a short bowden tube, the bondtech extruder, and print really slowly (5-10mm/sec), it will probably work well enough to print out some bushing sleeves in NinjaFlex.


---
**Frank “Helmi” Helmschrott** *July 01, 2015 06:39*

Thanks **+Walter Hsiao** this is highly appreciated. Independent from your answer i investigated a bit more yesterday when continuing the build with my friend. I think there are multiple problems. One for sure is that the quality of my print isn't really good. The holes aren't as good/round as they should be - especially the one that is printed horizontally. That is mostly due to the missing cooling fan on the ordbot. I'm in the process of designing an extruder for the printrbot gearheads. This will be a direct drive and hopefully Filaflex (like ninjaflex) should print well there. Also i have ordered some adjustable reamers to be able to ream the wholes to a constant diameter and make sure they're round. The shorter flex rings are a good idea - maybe i'll give that a try.


---
*Imported from [Google+](https://plus.google.com/+WalterHsiao/posts/QC1zvar5Zfg) &mdash; content and formatting may not be reliable*
