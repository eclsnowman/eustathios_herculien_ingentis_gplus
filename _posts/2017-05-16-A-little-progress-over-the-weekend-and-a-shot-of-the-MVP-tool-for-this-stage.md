---
layout: post
title: "A little progress over the weekend... and a shot of the MVP tool for this stage"
date: May 16, 2017 01:33
category: "Build Logs"
author: James Ochs
---
A  little progress over the weekend... and a shot of the MVP tool for this stage.  Not a bad investment for $70 at harbor freight....



![images/2a302adf3460f145d66948a539e41df7.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/2a302adf3460f145d66948a539e41df7.jpeg)
![images/92a968022fc559d193054d169b85cd72.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/92a968022fc559d193054d169b85cd72.jpeg)
![images/aa52f617c66f545223c0d8f071ae003e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/aa52f617c66f545223c0d8f071ae003e.jpeg)

**James Ochs**

---
---
**Eric Lien** *May 16, 2017 01:47*

Keep us updated on your progress. Not as many HercuLien builds out there as there are Eustathios builds. Always fun to see who chooses the "go big or go home" option :)


---
**Mike Miller** *May 24, 2017 12:21*

Drillpress for $70. That just blows my mind.




---
**James Ochs** *May 24, 2017 14:37*

I've had pretty good luck with the harbor freight power tools so far.  I'm not a heavy user though, mostly just need things like that once in a while.


---
**Mike Miller** *May 24, 2017 21:07*

Oh, certainly. I buy HF and if I wear it out, replace it with a 'real one'...haven't had to do that much. 




---
*Imported from [Google+](https://plus.google.com/105174837986897451687/posts/PkAni9KPoDQ) &mdash; content and formatting may not be reliable*
