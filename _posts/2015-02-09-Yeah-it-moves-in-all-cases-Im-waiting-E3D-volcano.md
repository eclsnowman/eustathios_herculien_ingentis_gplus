---
layout: post
title: "Yeah, it moves in all cases. I'm waiting E3D volcano :)"
date: February 09, 2015 22:06
category: "Show and Tell"
author: Ivans Nabereznihs
---
Yeah, it moves in all cases. I'm waiting E3D volcano :)


**Video content missing for image https://lh5.googleusercontent.com/-GAOG5-XIxss/VNkuw8-syQI/AAAAAAAAAGg/GeSff4dLJvE/s0/20150209_224825.mp4.gif**
![images/d5a21842b36f0d450da808c85c521c56.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d5a21842b36f0d450da808c85c521c56.gif)



**Ivans Nabereznihs**

---
---
**Jim Wilson** *February 10, 2015 15:01*

**+Eric Lien**​​ made an excellent upgraded carriage since that one you may want to have a look at: [https://drive.google.com/folderview?id=0B1rU7sHY9d8qRWhOd2pydGxaUWc&usp=sharing](https://drive.google.com/folderview?id=0B1rU7sHY9d8qRWhOd2pydGxaUWc&usp=sharing)﻿ prints cleaner and is from the FUTURE!


---
**Ivans Nabereznihs** *February 10, 2015 17:01*

It's test carriage. I project a carriage that will use similar duct from Eric Lien but I intend to use an air pump for fish tank instead turbine fan. I think not hot air from the outside of the printer will cool better.


---
*Imported from [Google+](https://plus.google.com/118257495094694997465/posts/1JxXSLzfbE6) &mdash; content and formatting may not be reliable*
