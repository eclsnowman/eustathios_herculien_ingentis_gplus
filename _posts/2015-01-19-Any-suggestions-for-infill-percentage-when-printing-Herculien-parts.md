---
layout: post
title: "Any suggestions for infill percentage when printing Herculien parts?"
date: January 19, 2015 21:15
category: "Discussion"
author: Daniel Salinas
---
Any suggestions for infill percentage when printing Herculien parts?  So far I've successfully printed the 4 corner bearing brackets and I used 100% infill but I wasn't sure if that is overkill for those parts or any other parts.  My printing profile is 4 walls, 4 layers top and 4 layers bottom then solid infill for the brackets.  Normally I print bigger parts with 20-30% but for the hotend carriage I was thinking it would need to be more dense.





**Daniel Salinas**

---
---
**Wayne Friedt** *January 19, 2015 22:46*

Not many parts need 100%. I would say 30% with 3 shells, 3 top and 3 bottom layers will be very strong.


---
**Eric Lien** *January 20, 2015 00:18*

If you have good adhesion three perimeters and 25% infill will be more than enough. I run a larger than standard extrusion width at 0.52mm on my 0.4mm nozzle on perimeters, and 200% width on infill.


---
**Eric Lien** *January 20, 2015 00:22*

Number of top and bottom layers will depend upon what layer height you are printing to avoid any step over rate issues. Also I try and keep top and bottom thicknesses similar to what the wall thickness based on the number of perimeters﻿


---
*Imported from [Google+](https://plus.google.com/106001140952121359286/posts/8ck4D3Wq5Gw) &mdash; content and formatting may not be reliable*
