---
layout: post
title: "Might not be the best place to ask this but, I was looking at CoreXY"
date: May 05, 2015 03:31
category: "Discussion"
author: Sébastien Plante
---
Might not be the best place to ask this but, I was looking at CoreXY. Is the Eustathios better? And why? 





**Sébastien Plante**

---
---
**Dale Dunn** *May 05, 2015 04:18*

CoreXY has a higher moving mass, so acceleration will be less. Also, CoreXYrelies on the bearings to keep the traveling beam aligned, so high acceleration at the edge of the print area may reveal dynamic alignment problems. The design can work well, you just need to design accordingly.



Eustathios and all related printers have very low moving mass and alignment of the moving parts is fully controlled by the belts or cords.



If you want to develop an automatic tool change system to switch extruders, the CoreXY and H-bot, etc. have the advantage.


---
**Eric Lien** *May 05, 2015 04:19*

I had a corexy for about 1 year. My circles were never perfectly round with corexy due to racking and backlash. I think using proper ground linear guides instead of round rods could have made it better. Corexy runs better if the system is very ridged.



I like this "ultimaker" style mechanics much better. But I am probably biased.


---
**Daniel F** *May 05, 2015 08:45*

I built a CoreXY about a year ago and I'm building an Eustathios at the moment. There are pros and cons but I would go for a design with a "crossed gantry" and isolated X and Y as Eustathios and others like QuadRap.

Although the principle of coreXY is captive, there are some drawbacks. The moving bar needs to be strong so it does not tilt. Best is to use just one axis for pulleys on the moving bar and place 2 pulleys on top of each other. X and Y are not independent which makes troubleshooting more complicated, you need to move both motors at the same time to move in a straight line. This also means the steppers should be the same kind and the current should be the same for both of them (not so easy to adjust with stepsticks). Belt tensioners helped to adjust the bar to get orthogonal, I can adjust the angle with the tension.

The belts also get quit long, mine are 1,8m (320x320mm build area) each but this is no problem, even with high speeds an acceleration.

The software also need to support CoreXY but that shouldn't be too much of a concern as there are several open source projects available that support it, repetier works fine in my case.

Here a link to a video: [https://plus.google.com/photos?pid=6049263941556416866&oid=111479474271942341508](https://plus.google.com/photos?pid=6049263941556416866&oid=111479474271942341508)


---
**Sébastien Plante** *May 05, 2015 19:03*

Okay thanks ! I think I'll go both. I have almost all the stuff to do a CoreXY, but lacking many parts for the Eustathios. I think I'll build the CoreXY to print the Eusthatios later! :)



As for adjusting the stepper, I'll be using the Smootieboard, so tuning is software based, should be easier than turning a little pot on a dirver! :)


---
**Daniel F** *May 05, 2015 19:08*

Exactly what I did--the journey is the destination. You'll definitely learn a lot by building your own printer, no matter which one.


---
*Imported from [Google+](https://plus.google.com/+excessnet/posts/Q9SqfRx9VNp) &mdash; content and formatting may not be reliable*
