---
layout: post
title: "Live Streaming Gus Montoya 's replacement V2 bearing holders"
date: May 27, 2015 23:24
category: "Show and Tell"
author: Isaac Arciaga
---
Live Streaming **+Gus Montoya** 's replacement V2 bearing holders. Testing ABS filament from [atomicfilament.com](http://atomicfilament.com) printing on my FuseMatic from Maker's Tool Works.

**+Mike Payson** 



[https://streamup.com/isaax](https://streamup.com/isaax)





**Isaac Arciaga**

---


---
*Imported from [Google+](https://plus.google.com/116829535781456592425/posts/AFwFGyr1D8a) &mdash; content and formatting may not be reliable*
