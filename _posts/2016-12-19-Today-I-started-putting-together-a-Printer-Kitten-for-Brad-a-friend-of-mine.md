---
layout: post
title: "Today I started putting together a Printer Kitten for Brad, a friend of mine"
date: December 19, 2016 05:40
category: "Build Logs"
author: Patrick Woolfenden
---
Today I started putting together a Printer Kitten for Brad, a friend of mine. I made some pretty good progress tonight and hope to have it finished up by Tuesday. 



I designed the Kitten about a year ago. If anyone is interested, this is the link for the github. [https://github.com/woolfepr/Printer-Kitten](https://github.com/woolfepr/Printer-Kitten) The kitten shares a lot in common with the HercuLien and Eustathios, just in a mini size. It has a 100 X 100mm print area with a 200 X 200mm "frame" footprint. It too uses the Tantillus/ultimaker derived XY motion system.



![images/233c4e49b75cb84b77db5b26038b2583.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/233c4e49b75cb84b77db5b26038b2583.jpeg)
![images/ab7ae99f3ec0c6278ea10849d994fdc1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/ab7ae99f3ec0c6278ea10849d994fdc1.jpeg)

**Patrick Woolfenden**

---
---
**Stephen Baird** *December 19, 2016 06:00*

It's so tiny! I love it. 


---
**Eric Lien** *December 19, 2016 07:03*

**+Stephen Baird** you should see it print. It does exceptional!


---
**Brad Hill** *December 21, 2016 07:19*

I saw the first thumbnail and was like that looks like kitten parts, then I looked closer and hey those are my parts :D A really smartly designed little printer, one thing I loved about Sublimes design and all the derivatives; some real unique design choices.


---
**Mateusz Nawrot** *December 21, 2016 13:59*

Same like me :D


---
**Frank “Helmi” Helmschrott** *January 06, 2017 20:07*

Oh that one looks adorable. I'm always looking for a small desktop 3d printer. Already thought about cloning the UM2Go but that would be quite a bit bigger then. This one looks great.


---
*Imported from [Google+](https://plus.google.com/105660857457447815256/posts/66J9EyQCgLC) &mdash; content and formatting may not be reliable*
