---
layout: post
title: "Getting an Azteeg X5 GT wired in to my printer"
date: May 24, 2017 06:27
category: "Discussion"
author: jerryflyguy
---
Getting an Azteeg X5 GT wired in to my printer. Everything is done but the fans. 



When connecting to the fan pins, should I be connecting to the Gnd and +VS pins or the +12V pins? The wiring diagram shows it either way, just not sure which is the correct way?



Also if I've got 24vdc power supply into the board, will it produce 12vdc or 24 on these pins? (My fans are 24v)





**jerryflyguy**

---
---
**Jeff DeMaagd** *May 24, 2017 11:09*

Use Vs. to get your main supply voltage (24V in your case). The 12V orientation is for 12V fans.


---
**Jeff DeMaagd** *May 24, 2017 11:17*

The board has a 12V converter on it for 12V accessories. This comes in handy for 24V users because some variations of fans, lights and other devices aren't easily found in 24V.


---
**Brad Vaughan** *May 24, 2017 12:04*

For a visual representation, the fan on the left will run at 12V.  The fan on the right will run at 24V (if you're running a 24V PSU as you are).





![images/0a68683a58b970801d26edd85ff2230d.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0a68683a58b970801d26edd85ff2230d.png)


---
**jerryflyguy** *May 24, 2017 17:12*

Thanks guys! So does the +VS pin control the fan speed via gcode? Is the +12v pin just 'always on' or variable also?


---
**Brad Vaughan** *May 24, 2017 17:44*

These pins are controllable via G-Code.  See the sample config values below.



The first block will allow you to control the first fan (the set of pins on the left in the diagram above) via the M106 and M107 G-Codes (the standard fan on/off codes).



The second block allows you to control the second fan (the set of pins on the right in the diagram above) via M42 and M43.  You can use those for a second fan, for LEDs, etc.



Either of these sets of pins (fan 1 or fan 2) can be used with either 12V or 24V (your source voltage), depending on which pins you use (top or bottom).



The last 4 blocks are just for fun.  They provide custom menu options on your LCD (if you have one) so you can test turning the fans (or other accessories connected to the pins in question) on and off.



I hope this helps.  Let me know if it's unclear.





switch.fan1.enable                        false

switch.fan1.input_on_command              M106

switch.fan1.input_off_command             M107

switch.fan1.output_pin                    0.26



switch.fan2.enable                        false

switch.fan2.input_on_command              M42

switch.fan2.input_off_command             M43

switch.fan2.output_pin                    1.22



custom_menu.fan1_on.enable                true

[custom_menu.fan1_on.name](http://custom_menu.fan1_on.name)                  Fan_1_On

custom_menu.fan1_on.command               M106



custom_menu.fan1_off.enable               true

[custom_menu.fan1_off.name](http://custom_menu.fan1_off.name)                 Fan_1_Off

custom_menu.fan1_off.command              M107



custom_menu.fan2_on.enable                true

[custom_menu.fan2_on.name](http://custom_menu.fan2_on.name)                  Fan_2_On

custom_menu.fan2_on.command               M42



custom_menu.fan2_off.enable               true

[custom_menu.fan2_off.name](http://custom_menu.fan2_off.name)                 Fan_2_Off

custom_menu.fan2_off.command              M43




---
**jerryflyguy** *May 25, 2017 05:38*

**+Brad Vaughan** someday I'll understand this stuff to the degree that I could create stuff instead of tiptoeing around, tweaking the few lines I need to, to get the basic functionality out of the box.



To that end, when using the sample BigFoot config, I see all the stepper 'enable' pins are labeled 'nc'. I assume I should leave these unchanged as they are (using BSD2660 Bigfoot drives).


---
**jerryflyguy** *May 25, 2017 05:41*

Also under the config file

"Motor_driver_control.delta.enable " should be set to false for a Eustathios? (As it's not a delta?) 


---
**jerryflyguy** *May 25, 2017 05:46*

Same goes for 

"Motor_driver_control.epsilon.enable" (should be false?) I don't have a daughter board to configure (just making it a standard Cartesian printer w/ a single extruder--- for now anyway)


---
**Roy Cortes** *May 25, 2017 07:20*

Yes leave the "nc" in there as the enable lines are used as Chip Select for the SPI drivers. 



The Motor drivers are labeled on Smoothie as Alpha(X), Beta (Y), Gamma(Z), Delta(E1), and Epsilon (E2).  The Delta in there does not pertain to the type of 3D printer configuration you have. Keep Motor driver delta enabled (True) if you have 1 hotend . Epsilon is for the 2nd hotend, so False in your case. 


---
**jerryflyguy** *May 26, 2017 02:12*

**+Roy Cortes** thanks **+Roy Cortes** just about to fire this bad boy up!


---
**jerryflyguy** *May 26, 2017 05:08*

**+Roy Cortes** first fire up and a couple anomalies. All to do with the panel.



1- the cursor movement from rotating the dial is backwards to the X5Mini, not sure how to reverse that.

2- the cursor movement is very 'clunky'. When you rotate the knob, the cursor may move, it may not move.. it may move much more than you'd think given the knob movement.

3-there are numerous delays while navigating the menus. Sometimes it's instant in changing pages, sometimes it's a 3-4s delay before displaying the change. Sometimes the panel will display partially corrupted characters and then subsequently correct the corruption 3-4s later? 



Suggestions?




---
**jerryflyguy** *May 26, 2017 05:30*

**+Roy Cortes** comparing the X5 Mini to the sample GT config file I see :



-encoder A and B pins are reversed (would this be my reversed rotation issue?)

-encoder resolution in the mini was 4, the GT sample had it at 1 (I changed it to '4', maybe that's caused an issue?)



First test was with the SD in the board slot, tried it in the Panel slot on attempt 2 but only got a backlit panel, w/ no characters.


---
**Roy Cortes** *May 26, 2017 05:48*

Reversing the encoder pins will flip the direction of the cursor.



Try a resolution of 1 or 2 and see what works best for you. 1 will make it move for every click.



Not sure whats causing the delays, haven't seen that much of a delay on the LCD. 


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/W9ifx8wvACk) &mdash; content and formatting may not be reliable*
