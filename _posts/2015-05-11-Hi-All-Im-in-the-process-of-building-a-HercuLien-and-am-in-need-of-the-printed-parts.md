---
layout: post
title: "Hi All! I'm in the process of building a HercuLien and am in need of the printed parts"
date: May 11, 2015 14:07
category: "Discussion"
author: Luis Pacheco
---
Hi All!

I'm in the process of building a HercuLien and am in need of the printed parts. Would anyone here be willing to print me two complete sets of the printed parts? I'm located in Tampa, FL and am prepared to pay for parts and shipping.



Thanks!





**Luis Pacheco**

---
---
**Daniel Salinas** *May 11, 2015 18:58*

Are you building 2 of them???


---
**Luis Pacheco** *May 11, 2015 19:26*

Structurally I have the parts for 2. Some electronics I have only purchased for one and will be purchasing the second set at a later date.


---
**Daniel Salinas** *May 11, 2015 20:08*

That's a lot of printing :)  In my experience its about 1.5 to 1.75 spools of abs to do one set so you're looking at 3 to 4 spools of plastic. 


---
**Luis Pacheco** *May 12, 2015 00:35*

Did not know it was that much! I'm willing to pay it regardless but if it's easier for someone to do one set I can always build the first and print the parts for the second from that one.


---
**Ben Malcheski** *May 12, 2015 16:49*

I could print them on my Stratasys machine. It'll cost quite a bit more than a few typical rolls of filament though.﻿



You'd probably only want to get one set to save. Let me know if you're interested and I'll quote it out for you.


---
**Luis Pacheco** *May 13, 2015 20:37*

If you could give me an overall cost I'll definitely consider it.


---
**Ben Malcheski** *May 19, 2015 17:36*

I ran the numbers. Unfortunately because the cost of the material for my machine is so high, you are going to think I am crazy. I would have to get about $450 for printing the essentials of one set at 100% infill. That doesn't include spool holders or things you can get by without. Some of the parts could be printed with the sparse infill setting, which I estimate at about 20%, to save some but you would have to tell me which ones you would be ok with that on. Some of the parts do have some really thick sections that I'm not sure if so much material is necessary on. We might be able to get the total down to around $350.


---
**Luis Pacheco** *May 20, 2015 12:08*

Didn't realize it would be so much, but sounds about right for being out of a stratasys. I'll have to pass at that cost and find another route. Do you know if PLA would work for short term? I could print them myself but I can only print them in PLA and would rather have them in ABS.


---
**Ben Malcheski** *May 20, 2015 15:54*

**+Luis Pacheco** Understandable. You should be able to temporarily use some PLA parts. You will just have to be careful that the enclosure doesn't get too warm inside. I would recommend any plastics touching the bed or hotend be made out of something with a higher Tg.


---
**Ben Malcheski** *May 29, 2015 04:26*

[https://plus.google.com/106419408152490638700/posts/1GFGRieS1Mf](https://plus.google.com/106419408152490638700/posts/1GFGRieS1Mf)


---
*Imported from [Google+](https://plus.google.com/110276424073473119972/posts/jFyWcFbqnUq) &mdash; content and formatting may not be reliable*
