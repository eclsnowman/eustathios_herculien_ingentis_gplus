---
layout: post
title: "Finally upgraded my extruder to Eric Lien 's hurcustruder"
date: February 10, 2015 04:16
category: "Show and Tell"
author: Erik Scott
---
Finally upgraded my extruder to **+Eric Lien**'s hurcustruder. The difference is night and day! I can now start a print and leave. No fiddling with the bed to make sure the plastic doesn't jam. Planetary stepper just powers through. I also quite like the filament spool mount. Only complaint is that it's much harder to change the filament.



I took the time to re-enforce my hotend (It could move slightly along the Y axis, resulting in some ribbing) and actually mount the printer board to the printer itself. I'll eventually do the same with the PSU.



I printed a 30mm test cube, 0.2mm layer height, 40mm/s inner perimeter, 20mm/s outer perimeter, 50m/s infill. It's alright; nothing really spectacular though. I would like to get those faces super smooth though, even at 0.2mm. Any suggestions on how to improve this?



![images/d296e5593eb84a84424c5286d60d6cf6.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d296e5593eb84a84424c5286d60d6cf6.jpeg)
![images/db302082a92d1bf0fd10e1362440c358.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/db302082a92d1bf0fd10e1362440c358.jpeg)
![images/3ad06a5bc4813176d6788173f93b55df.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3ad06a5bc4813176d6788173f93b55df.jpeg)
![images/802fef64533b347c4751363e7f79105a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/802fef64533b347c4751363e7f79105a.jpeg)
![images/4cd04f8e0ddb4a9fc6838cb223652c7d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4cd04f8e0ddb4a9fc6838cb223652c7d.jpeg)
![images/3866c9788675680884d8dfc3f869b04c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3866c9788675680884d8dfc3f869b04c.jpeg)
![images/fb67bb6ea3d4c75b4496afcb4e11bb30.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fb67bb6ea3d4c75b4496afcb4e11bb30.jpeg)
![images/70fdb5f7bc51da0b641f2a3910f6be8d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/70fdb5f7bc51da0b641f2a3910f6be8d.jpeg)
![images/d952a3218aa7f3170e0995407df87e68.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d952a3218aa7f3170e0995407df87e68.jpeg)

**Erik Scott**

---
---
**Eric Lien** *February 10, 2015 06:28*

Looking great. Love the Bowden pushlock adapter.



For the filament change it does take longer, I agree. But so long as feed is consistent I guess I take the trade off.



Clean prints. Nice job.


---
**James Rivera** *February 10, 2015 07:19*

**+Erik Scott** I have a "bird nose" on my classic Greg's Wade's pusher. It allows me to set the tension and then quick release whenever I need to inspect the gear teeth or change filament. Then when I put it back in place, then tension is exactly what it was before. If I'm going PLA to PLA, or ABS to ABS, I don't need to mess with it at all. Something similar here would probably solve this problem.


---
**hon po** *February 10, 2015 08:23*

**+James Rivera** I agree the quick release lever is a must have for this type of extruder. It allow quick disengage of clamp without touching the spring tension. So always back to the same compression after filament change.



**+Erik Scott** Have you calibrated your E steps? There is tiny ripple in Y face. I wonder if there is any guide in tuning acceleration and jerk to eliminate this.


---
**Jeremie Francois** *February 10, 2015 09:51*

Weird, I failed to find "hurcustruder" via google... Where do you get it?

I need to discard my own one (that fails with a speedy 24V delta) and I was currently looking for an alternative...


---
**James Rivera** *February 10, 2015 10:04*

It is from the HercuLien. Check the github reference at the community root page. It should be there.


---
**Eric Lien** *February 10, 2015 12:40*

**+Jeremie Francois** it is actually hercustruder.


---
**Eric Lien** *February 10, 2015 12:41*

[https://www.youmagine.com/designs/hercustruder](https://www.youmagine.com/designs/hercustruder)


---
**Erik Scott** *February 10, 2015 14:25*

**+Eric Lien** Doh! Should have gotten the spelling correct. It was late when I posted so that's going to be my excuse. Thanks for the positive comments though!



**+James Rivera**​ do you have any pics of your extruder? I'm looking for ideas on how to possibly add a lever to more easily change filament. 



**+hon po**​ I have indeed calibrated my E steps, and I seem to be extruding the right amount. Off the top my my head, it's about 480 steps/mm. **+Eric Lien**​, does that sound about right for a 5.18:1 geared stepper? 


---
**Eric Lien** *February 10, 2015 16:10*

Sounds about right, but it depends a lot on the hobb gear diameter. Just calibrate as you usually would and you should be good.


---
**James Rivera** *February 11, 2015 19:33*

**+Erik Scott** this* is where I got it from and it has better pictures than the ones I took anyway. :)

[http://www.thingiverse.com/thing:11731](http://www.thingiverse.com/thing:11731)



*apologies for the Takerbot link, but this is where I originally got it from.


---
**Jeremie Francois** *February 11, 2015 19:40*

I am printing it with XT (colorfabb), but it had a lot of trouble with the really steep concentric overhang of the body... This material is great, expect for bridges where it fails a lot (I never was able to drag more than 1-2 mm of it in the air). Still should be fairly OK as I expected.


---
**Erik Scott** *February 11, 2015 19:58*

**+Eric Lien**​ good, I'm in the right ballpark then. 



**+James Rivera**​ thanks for that. Seems pretty clever. I may give it a shot. 



**+Jeremie Francois**​ I didn't have any trouble printing it with PLA. I do have a fan + duct directly cooling the extruded plastic, however, which helps a ton with overhangs like that. 


---
**Jeremie Francois** *February 11, 2015 20:29*

**+Erik Scott** yeah no doubt PLA would be much easier. But I wanted a rock solid stuff, and temperature resistant (which is why I opted for XT). WIll most probably re-print some parts in CFPLA. if it works fine and does not heat too much (working with 24V at high speed...)


---
**Eric Lien** *February 11, 2015 21:37*

All mine are abs. I can print the assembly of all components at 80mm/s easy, which is why I love abs... It just seems to run better at high speeds than PLA.


---
**Jeremie Francois** *February 11, 2015 22:01*

I highly suspected that -- but ABS is the only material I never print. At 200mm/s (nozzle=0.6mm) my own extruder completely fails, but I highly suspects the MK8 chinese bolt is crappy...

Not sure 1.75mm gives enough grip btw.

Ref: [https://github.com/MoonCactus/fisherstruder/blob/master/fisherstruder.jpg](https://github.com/MoonCactus/fisherstruder/blob/master/fisherstruder.jpg)


---
**Eric Lien** *February 11, 2015 22:52*

**+Jeremie Francois** very cool extruder design. I like how compact it is.


---
**Jeremie Francois** *February 11, 2015 23:26*

**+Eric Lien** glad you like it! Indeed the idea was to be very compact, easy to print (2 parts), and compatible with a reductor -- an interesting challenge I was given by a friend. The shape quickly came out (could not have 3 screws b/c of the place it stands in our deltas - has to be flat). I thought about making it even hollower and faster to print as it is really tough,  but it would better benefit from a quick lever like yours (no easy solution here). I would improve it if the grip suited me, but it sucks. I would better go and print myself another good old rollerstruder instead. Hey, what would be your recommendation for a very reliable 1.75mm bolt?


---
**Eric Lien** *February 12, 2015 00:54*

**+Jeremie Francois** bolt?


---
**Jeremie Francois** *February 12, 2015 12:38*

**+Eric Lien** lol, who on earth is still making his own bolts? ;)

Not sure it would be easy for 1.75mm filaments but it is worth the try. ... will check this week end and tell (I am sure I can do at least as bad as what I have here)


---
**Joe Spanier** *February 12, 2015 12:40*

Do  you need to release the latch for a filament change on this one? On my air tripper I just pull the filament out, and poke it back up through. A little direct pressure and the filament just pops in and goes into the bowden. The extruder motor needs to be free wheeling obviously. 


---
**Joe Spanier** *February 12, 2015 12:42*

**+Jeremie Francois** Do you mean hobbed bolt? Or hobbed drive roll in this case?



The ones from **+UltiBots LLC**  are fantasic, with great customer service. 


---
**Jeremie Francois** *February 12, 2015 13:25*

**+Joe Spanier** a hobbed bolt. Wrote in length about them a long time ago, and I made mine for 3mm, but it was certainly easier than for 1.75mm

[http://www.tridimake.com/2013/03/which-hobbed-bolt-for-filament-feeder.html](http://www.tridimake.com/2013/03/which-hobbed-bolt-for-filament-feeder.html)

Thanks for the ref anyway, but I suspect these are all "grinding" bolts :s


---
**Joe Spanier** *February 12, 2015 16:52*

They are all about the same. The ultibots one is the only one I've ever seen that specifically says its for 1.75 and the hobb is narrowed with a higher pitch. But it works about the same as any I've used. 


---
**James Rivera** *February 12, 2015 23:31*

**+Jeremie Francois** nice blog post on the subject of hobbed bolts.  Another post on gears that I thought was really good is this one from AirTripper:

[http://airtripper.com/1676/3d-printer-extruder-filament-drive-gear-review-benchmark/](http://airtripper.com/1676/3d-printer-extruder-filament-drive-gear-review-benchmark/)


---
**Jeremie Francois** *February 14, 2015 19:23*

Given that it worked very well for 3mm, I eventually made one for 1.75 mm this evening: [https://plus.google.com/117153332077257116587/posts/SnhurkMXPvw](https://plus.google.com/117153332077257116587/posts/SnhurkMXPvw)

Thanks **+James Rivera** for the link, I should check I mention this post from Airtripper in mine.


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/5zGY62AB96A) &mdash; content and formatting may not be reliable*
