---
layout: post
title: "Originally shared by D Rob Look what the USPS fairy brought me"
date: November 17, 2014 18:38
category: "Show and Tell"
author: D Rob
---
<b>Originally shared by D Rob</b>



Look what the USPS fairy brought me. Thanks +Roy Cortes (Panucatt).



Everyone stay tuned. I'll be giving a review on this bad boy soon!

![images/aa595dbaa732d33cfa5671d58850e37a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/aa595dbaa732d33cfa5671d58850e37a.jpeg)



**D Rob**

---
---
**James Rivera** *November 17, 2014 20:55*

Whoa. Does that have support for 8 stepper drivers?


---
**D Rob** *November 17, 2014 21:18*

It does indeed


---
**D Rob** *November 17, 2014 21:20*

12-24v, 8-axis, micro sd on board, blade fuses and chocked full of awesomeness


---
**James Rivera** *November 17, 2014 21:34*

I see, looks like this is it:



[http://www.panucatt.com/ProductDetails.asp?ProductCode=AX3pro](http://www.panucatt.com/ProductDetails.asp?ProductCode=AX3pro)


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/XG5dN5uM3MV) &mdash; content and formatting may not be reliable*
