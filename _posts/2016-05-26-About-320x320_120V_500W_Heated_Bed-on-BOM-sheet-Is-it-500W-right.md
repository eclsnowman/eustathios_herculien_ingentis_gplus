---
layout: post
title: "About 320x320_120V_500W_Heated_Bed on BOM sheet. Is it 500W right?"
date: May 26, 2016 19:13
category: "Build Logs"
author: Botio Kuo
---
About 320x320_120V_500W_Heated_Bed on BOM sheet. Is it 500W right? the Power supply Meanwell_SP-200-24 can handle it ? @@"





**Botio Kuo**

---
---
**Sébastien Plante** *May 26, 2016 19:24*

No, it's 120V, you plug it directly to the wall :)


---
**Botio Kuo** *May 26, 2016 19:26*

Hi, and do you know how to connect heated bed with aluminum sheet ? Tape or ? Thank you :)


---
**Derek Schuetz** *May 26, 2016 19:32*

I used high heat rubber gasket for automotice to glue my bed. And as Sebastien said the PSU does not power the bed it's all done through 120v and a relay


---
**Tomek Brzezinski** *May 26, 2016 19:57*

Also called "RTV silicone gasket" usually


---
**Eric Lien** *May 26, 2016 21:24*

**+Botio Kuo** when you order the heated bed you should ask for it to come with double-sided tape on one side


---
**Eric Lien** *May 26, 2016 21:24*

Yep it's the solid state relay that's in the bill of materials. it's an Omron


---
**Botio Kuo** *May 26, 2016 21:27*

Hi, it's my first time do wiring by myself with power supply Meanwell_SP-200-24 ... what kind of cable wire I should use for 24V- 8.4A ? 12 AWG Silicone Wire ? 16AWG ? or 10AWG ? ( I'm kind of stupid for this ... sorry...)


---
**Botio Kuo** *May 26, 2016 21:28*

**+Nathan Walkner** OH!! I bought a SSR on BOM sheet. But I still try to figure how to connect them with 24V power supply ... Thanks


---
**Botio Kuo** *May 26, 2016 21:50*

**+Nathan Walkner** OMG, thank you so much. I'm watching it now. :)


---
**Eric Lien** *May 26, 2016 22:55*

**+Botio Kuo** you can also look at this. It is for a different controller, but you use the bed outputs similarly into input+ / input- on the SSR: [https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Azteeg_x3_v2_wiring_(with_notes).pdf?raw=true](https://github.com/eclsnowman/HercuLien/blob/master/Documentation/Azteeg_x3_v2_wiring_(with_notes).pdf?raw=true)


---
**Eric Lien** *May 26, 2016 22:59*

I also recommend shielded wire if you can get it. Then run all the shield's to ground at one end of the printer. This will shield noise in and out of the wires. I like the shielded wire that has both foil and a braided shield wire. You can get  Belden or Carol 4 conductor shielded wire cheap in eBay if you keep your eyes open. I get end cuts of it from work... so I have never had to purchase any.


---
**jerryflyguy** *May 26, 2016 23:30*

Anyone know where a person can buy one in the US/Canada relatively cost effectively or is AliExpress the only place?


---
**jerryflyguy** *May 27, 2016 00:13*

**+Nathan Walkner** sorry, I was meaning the silicone heat beds.


---
**Eric Lien** *May 27, 2016 00:14*

I like the ones from: [https://alirubber.en.alibaba.com/](https://alirubber.en.alibaba.com/)



Ask for Daisy



They will custom build it with whatever wire lengths, size, wattage, voltage, thermistor qty, adhesive backed, etc that you request. And it's cheap and fast. Not much more you could ask for.﻿


---
**Botio Kuo** *May 27, 2016 03:33*

**+Eric Lien** Hi Eric, I found this one on ebay [http://www.ebay.com/itm/14-2-awg-SHIELDED-STRANDED-SECURITY-CABLE-SOLD-BY-THE-FOOT-1-FT-ALARM-AUDIO-WIRE-/181500530039?hash=item2a42464d77:g:H0oAAOxyiOxRzy24](http://www.ebay.com/itm/14-2-awg-SHIELDED-STRANDED-SECURITY-CABLE-SOLD-BY-THE-FOOT-1-FT-ALARM-AUDIO-WIRE-/181500530039?hash=item2a42464d77:g:H0oAAOxyiOxRzy24) , is it what you are talking about ? @@" thx


---
**Eric Lien** *May 27, 2016 04:08*

Like this for steppers and limit switches: [http://www.tektel.com/PLC4234/Security-Wire-22Awg---4-Stranded-Shielded-500Ft-White.html?&Affiliate=googletrack](http://www.tektel.com/PLC4234/Security-Wire-22Awg---4-Stranded-Shielded-500Ft-White.html?&Affiliate=googletrack)



Or



[http://www.tektel.com/PLC4504/Security-Burglar-Alarm-Cable-18Awg---4C-Stranded-Shielded-500Ft-White.html](http://www.tektel.com/PLC4504/Security-Burglar-Alarm-Cable-18Awg---4C-Stranded-Shielded-500Ft-White.html)



Then again if you order long leads from Robotdigg for the steppers you can avoid splicing and just run it full length. You wouldn't get the shielded, but would save some headache.




---
**Botio Kuo** *May 27, 2016 04:23*

Hi Eric, but wire for power supply to motherboard should be 12Awg or 16Awg right ?


---
**Eric Lien** *May 27, 2016 05:10*

I used 10 or 12 I think. Then again the board will draw very little. The bed runs from the SSR. So the biggest load is not even through the controller.


---
**Sébastien Plante** *May 27, 2016 13:20*

**+Nathan Walkner** Question was : Is the power supply can handle it.  But yeah, if he want to control it, he will need an SSR, but the Power Supply won't provide it power since it's 120V.



**+Botio Kuo** You can use 16awg if you are under 10 amps, else I'll go 12 or more.


---
**jerryflyguy** *June 11, 2016 05:07*

**+Eric Lien** just wanted to add my recommendation to using this Alirubber on AliExpress. Ordered it and 8 days later it's in my hands. Was expecting it to take 3x as long. Haven't used it yet but great experience this far along. Total cost w/ shipping and import duties etc is $75 CAD, not too bad.


---
**Eric Lien** *June 11, 2016 06:10*

**+jerryflyguy** yeah, they have been great to everyone I have asked so far. Not bad from order, to custom fabricated heater, around the world to your door in 8days :) 



Pretty amazing world we live in!


---
*Imported from [Google+](https://plus.google.com/117769613099225133203/posts/Le9EdsqfuXn) &mdash; content and formatting may not be reliable*
