---
layout: post
title: "\"Open18\" is coming together at MRRF. I think he's posted renderings here last week."
date: March 22, 2015 15:13
category: "Show and Tell"
author: Jeff DeMaagd
---
"Open18" is coming together at MRRF. I think he's posted renderings here last week.﻿



![images/097911bd36f4e5c3b3ff172da2e00045.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/097911bd36f4e5c3b3ff172da2e00045.jpeg)
![images/eab768fd21d16f503f1f0b4ae3963957.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eab768fd21d16f503f1f0b4ae3963957.jpeg)
![images/3a54c149978562d4ce669ed0f8f0c1ab.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3a54c149978562d4ce669ed0f8f0c1ab.jpeg)

**Jeff DeMaagd**

---
---
**Isaac Arciaga** *March 22, 2015 17:46*

Is that aluminum plate?


---
**Joe Spanier** *March 22, 2015 18:21*

The body is MDF. Bed is aluminum plate with glass. The machine worked! Well in the since that everything moved and both extruders shot plastic out at an ill-concieved rate. 


---
**Javier Prieto** *March 22, 2015 22:36*

**+Griffin Paquette** and  I are going to design one exactly equal to this one. It is really beautiful, congrats!


---
**Joe Spanier** *March 22, 2015 22:58*

Awesome! Give me a couple weeks to work out the kinks. I need a better belt tensioning method, and I'm dropping the LM8UU bearings on the cross carriage, they just have too much play. 



Also I need to add in end stops. I am using MachineKit so I can get by without then but I am still going to put them in. 


---
**Javier Prieto** *March 22, 2015 23:29*

**+Joe Spanier** Have you tried using 6mm rods in the cross? They will weight less, will have less friction and the increment of vertical deformation isnt a big issue compared to 8 mm rods.

-Advantage of using closed belts in x and y motors? I have thought using directly open belts in x and y, with the motor moving directly the lateral carriage (as in Skeleton [http://www.thingiverse.com/thing:560353](http://www.thingiverse.com/thing:560353) ). It will need just a couple of centimeters in the upper part of the system.

-For the Z axis, are you using 5 or 8 mm threaded rod? I think 8 mm would be amazing, as it does not tend to flex or be deformed (I have seen some 5 mm threaded rod deformed from the shop :/ ).

-Does the bed flex? I am afraid of that in these systems. Could be reduced using some high flex resistant bed support. What do you think?

-What is the MDF thickness?

-Have you seen Ditto's ( [http://www.tinkerine.com/](http://www.tinkerine.com/) ) ?

-How are the printings? And speeds? Noises? Wood is definately the key reducing noises and vibrations :D

Hope you dont get upset with all these questions, just want to know other points of view :)


---
**Joe Spanier** *March 23, 2015 01:08*

So first thing. We need to level set here. This machine is massive. Its about 22" wide with a 14x14x18 printable area. 



-the cross rails are 485mm long. 6mm rods get a bit spindly at that distance. There is very little weight on the head as it is so mass dropping isn't much of a concern. I just need bearings that don't suck. Not sure if its this batch of lm8s that was bad or what. But I'm going to the self aligning brass bushings either way. Really liking them. 



-in a closed box like this that motor config is difficult. It works great in something like **+Shauki B**​'s quadrap but over that ~500mm span you need something driving on the other side. Where its a constrained belt like I'm using or a motor like Shauki uses. 

-The Z axis is tr8-8 trapezoidal rod. Its great. Very fast but almost too efficient. With the weight of the bed the bed drops when the power goes off. I'll probably need to move to a single start rod. 



- the bed is 1/8" aluminum plate with a 500w 110v AC silicone mat adhered to the bottom. Its fully supported by a CNC routed MDF plate. No flex



- .75"

-yes. They fit on my bed :)

-eh, things need to be tuned. I'm using machinekit with a E3D Cyclops and that is barely understood as to how to make that work together. 

Speed was fast. I only had it moving for about 2 hours before I had to pack it up and drive 5 hours. 



Its very very quiet thanks to the bushings, machinekit and 24v


---
**Javier Prieto** *March 23, 2015 09:44*

Really thank you! Your machine is definately bigger than the one in our minds, but you have told us some interesting things :) Will try my lm6uu printed bushings and if they dont work properly will move to brass ones (I think Griffin will begin with them :P ). I had though using the IGUS spindle, as it has some nice reviews, but begin with the 8 mm threaded rod (as I have it spare on my house).

Thank you again and looking forwards to see it moviiiiiiiing!


---
*Imported from [Google+](https://plus.google.com/+JeffDeMaagd/posts/5SAKvuNFiUs) &mdash; content and formatting may not be reliable*
