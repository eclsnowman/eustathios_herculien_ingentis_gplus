---
layout: post
title: "Finally installed Walter Hsiao 's Mini Space Invaders Carriage with 40mm Fan"
date: May 29, 2017 22:42
category: "Build Logs"
author: Daniel F
---
Finally installed **+Walter Hsiao**'s Mini Space Invaders Carriage with 40mm Fan. The bushings from my original carriage got loose an fell off during a print so I installed the carriage that has been half finished for quite some time. It's printed in ASA-X. This carriage is higher than the original, therefore I had to modify the Z-axis support to allow the bed to reach the nozzle (one screw head touched the Z-shaft mount). I might redesign the Z-end stop switch mount as it had to be twisted to move up the switch high enough. Part cooling fans are ordered. Does anyone know how they are fixed? Glue?



![images/a556d8c4d38f541c05ae39138eb30aca.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a556d8c4d38f541c05ae39138eb30aca.jpeg)
![images/4ab3f7e023a530daa0e8248bf3804a2e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4ab3f7e023a530daa0e8248bf3804a2e.jpeg)
![images/5e06a2d82fb410d6a0f749b36601ba84.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5e06a2d82fb410d6a0f749b36601ba84.jpeg)
![images/56b9255fb442754d81cbcc872e5d4d03.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/56b9255fb442754d81cbcc872e5d4d03.jpeg)
![images/3ed0319880497b0c928c68501911180d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3ed0319880497b0c928c68501911180d.jpeg)

**Daniel F**

---
---
**Benjamin Liedblad** *May 29, 2017 23:40*

My fans just pressed in. They are snug, but not tight. 


---
**Maxime Favre** *May 30, 2017 04:28*

I added 2 dots of glue on each for security.


---
**Sean B** *June 01, 2017 20:26*

I used a small amount of hot glue


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/VRUK7nwkHGE) &mdash; content and formatting may not be reliable*
