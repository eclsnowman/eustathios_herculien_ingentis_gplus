---
layout: post
title: "That point where Eric Lien 's efficiency causes your printer to fly apart because Max Z is still a good Centimeter away from the nozzle"
date: February 16, 2015 00:50
category: "Discussion"
author: Mike Miller
---
That point where **+Eric Lien**'s efficiency causes your printer to fly apart because Max Z is still a good Centimeter away from the nozzle. ﻿

![images/e2f06732dc97df5e715be54f9f95bd87.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e2f06732dc97df5e715be54f9f95bd87.jpeg)



**Mike Miller**

---
---
**Eric Lien** *February 16, 2015 01:29*

Yeh, I had to design new zbed supports moving the bed up. You can use longer bed bolts and spacers as a stop-gap measure.


---
**Mike Miller** *February 16, 2015 01:42*

I've got rubber belts lifting the bed, good thing I've got extra belt, but I've got to COMPLETELY re-establish tram and plumb and calibration. I did have the carriage run a two hour run in before tearing into it. I think it'll be pretty great as a result. 


---
**Eric Lien** *February 16, 2015 07:52*

**+Ashley Webster** the assembly for the new V4 Eustathios carriage I designed is shorter. So his bed couldn't reach the nozzle once installed.


---
**Mike Miller** *February 16, 2015 19:49*

No, what cause the printer to fall apart was that max-lift on the printer bed was limited by my choice of brackets, and everything was stacked from top to bottom...there's no way to lift the bed supports 10-20 cm without disassembling EVERYTHING. 



Still..it's a good excuse to make some structural changes. 


---
**Mike Miller** *February 16, 2015 20:10*

Ease of assembly is usually pretty far down the list when making 1 of 1. :) 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/KBxjcrj26Tk) &mdash; content and formatting may not be reliable*
