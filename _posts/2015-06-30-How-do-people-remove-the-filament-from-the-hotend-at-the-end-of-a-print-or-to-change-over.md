---
layout: post
title: "How do people remove the filament from the hotend at the end of a print or to change over?"
date: June 30, 2015 10:08
category: "Discussion"
author: Oliver Seiler
---
How do people remove the filament from the hotend at the end of a print or to change over? Just a fast retract while hot? 

I had some issues with filament deposits in the heatbreak. Retract during print is set to 6mm for PETG, do I suspect it's from changing filament. 





**Oliver Seiler**

---
---
**Eric Lien** *June 30, 2015 11:12*

I drop temp to 110, then cold pull by hand by removing the Bowden tube from the hot end. Cold pulls tend to bring get the majority of the old filament out. If I am changing material types I tend to do a nylon cold pull in between the old and new filament. Nylon cold pulls are great for cleaning out the hotend. For loading and unloading filament I made custom buttons with the the CustomControl plugin in octoprint (one for load, one for unload). With the Bondtech Extruder I could probably use it for cold pulls, but I would need to disable the cold extrude safety check.



Unload:

G91

G1 E-950 F1500

G90



Load:

G91

G1 E950 F1500

G90﻿


---
**Oliver Seiler** *June 30, 2015 19:51*

How is that for wear and tear on the bowden and couplings?


---
**Eric Lien** *July 01, 2015 00:03*

Mine is fine after more than 1 year, if that is an indicator.


---
**Oliver Seiler** *July 01, 2015 10:04*

**+Eric Lien** Good enough for me ;)


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/WLLBUCMbz5v) &mdash; content and formatting may not be reliable*
