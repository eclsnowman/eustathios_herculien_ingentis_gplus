---
layout: post
title: "Integrate lighting into my Herculien question. I want to add some LED lighting potentially some white led strips up top"
date: February 14, 2015 06:27
category: "Discussion"
author: Derek Schuetz
---
Integrate lighting into my Herculien question. I want to add some LED lighting potentially some white led strips up top. But I know little about electrical. I am running. A 24v PSU and all led strips are 12v is this an issue if I'm running them through the Aztega v3?





**Derek Schuetz**

---
---
**Eric Lien** *February 14, 2015 06:40*

I got 24v led strips from aliexpress. They are so bright I run them at S60 output. S254 was blinding. The whole roll cost me $10. If you were closer I would ship you some.﻿


---
**Derek Schuetz** *February 14, 2015 08:27*

For the extrusion drill guides am I right to assume the one with the 4 small holes is to mark where the smooth rides come through the frame?


---
**Eric Lien** *February 14, 2015 14:06*

**+Derek Schuetz** yes. They create the pilot holes. You can tell which hole to use by holding the corresponding corner bracket up.



Also use a hole saw or and endmill bit to drill the large hole. A regular bit will walk since it is half on, half off a rib in the extrusion. You will need to use a drill press.


---
**Derek Schuetz** *February 14, 2015 16:58*

Ok I have a drill press. What size hole should I do 3/4" seems too big and that's the smallest Holesaw I can find


---
**Eric Lien** *February 14, 2015 17:20*

I used a 1/2" regular bit. That's why I am recommending you avoid it. I fought it walking like crazy. The shaft is 10mm, so I wanted at least 1mm clearance on each side minimum.


---
**Derek Schuetz** *February 14, 2015 17:27*

Did you have a drill press or did you do it with a hand drill?


---
**Eric Lien** *February 14, 2015 17:49*

With a drill press, with a x/y adjustable vice and mount. But the slope on the tip of a standard spiral bit will put a lot of side force once you hit the vertical rib. Again, you CAN do it with a standard bit, I did... But it was such a royal pain. So  I will recommend against it as strongly as I can. I took it super slow (like 2 hours for 4 holes slow) and I still had bit walk.


---
**Derek Schuetz** *February 14, 2015 19:06*

Ok I'll take your word and try to find a 1/2 Holesaw 


---
**Jarred Baines** *February 15, 2015 00:28*

Slotdrill / centre cutting endmill? Will still need to take it slow but it won't be influenced by the tip like a standard drill bit would...


---
*Imported from [Google+](https://plus.google.com/117180320763045071687/posts/DvY8qRZnrWt) &mdash; content and formatting may not be reliable*
