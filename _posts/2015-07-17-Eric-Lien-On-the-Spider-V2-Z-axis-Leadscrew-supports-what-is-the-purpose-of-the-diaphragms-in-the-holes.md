---
layout: post
title: "Eric Lien , On the Spider V2 Z-axis Leadscrew supports, what is the purpose of the diaphragms in the holes?"
date: July 17, 2015 19:47
category: "Discussion"
author: Bryan Weaver
---
**+Eric Lien** , On the Spider V2 Z-axis Leadscrew supports, what is the purpose of the diaphragms in the holes?  Just curious before I print them, wondering if i'll need support material or anything.





**Bryan Weaver**

---
---
**Frank “Helmi” Helmschrott** *July 17, 2015 19:49*

Actually they are there so you don't need support. Those can be bridged while the wholes without diaphragms couldn't be bridged. also the ones on the edges are there to help you with warping issues. they can also be removed afterwards.



You need to remove them after printing.


---
**Bryan Weaver** *July 17, 2015 19:51*

Awesome, thanks.


---
**Eric Lien** *July 17, 2015 20:25*

I think I had to fix that model. If I recall I extruded the diaphragm in the wrong direction at first so ot was hovering over the hole. I have since fixed it in solidworks and on the STL's out on Github. So make sure you have newest version. 



The disks at the corners are to hold downs to print the part in ABS . Make sure the tabs are down in your software.



I started using these disks long ago and it works great to avoid having to use BRIM all the way around. The corners are always what lift (strong differential of cooling with 2 sides exposed). So the disks add mass and surface contact outside the corner and keep the heat even so they don't lift.


---
**Bryan Weaver** *July 17, 2015 20:35*

I'm getting the files fro the Github and they say V2, So I assume i've got the right ones.



I was wondering about the disks when I printed the extruder carriage.  They worked like a charm though, very clever idea.


---
**Erik Scott** *July 18, 2015 21:33*

I just took a look at those STLs, and I was curious, Eric, why you chose to print in that orientation? I figured if you printed it the other way you wouldn't need the diaphragms in the 3 M5 holes/counterbores and it would be one less thing to worry about. 



I do like the lillipad disk idea though. it would be nice to have that integrated into the slicer. 


---
**Eric Lien** *July 18, 2015 22:00*

I try and print all parts in a constained assembly (like the Z rods and screws) in the same orientation. That way any printer nuances are the same across all printed parts tied to that constraint. Also I try and take into account stresses and put the layers perpendicular to any loads.


---
**Erik Scott** *July 18, 2015 22:04*

The loads would still be perpendicular as the part would be flipped 180 degrees. It's not like you couldn't flip that symmetric rod-clamp piece. I like to do as little "post processing" as possible. Additionally, if you did flip it, the nut traps would also be printed in a more favorable orientation. 


---
*Imported from [Google+](https://plus.google.com/111820797809026464429/posts/JTDoPXQHhBy) &mdash; content and formatting may not be reliable*
