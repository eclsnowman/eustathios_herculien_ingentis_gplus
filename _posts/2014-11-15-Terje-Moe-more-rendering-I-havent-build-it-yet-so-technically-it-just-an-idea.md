---
layout: post
title: "Terje Moe, more rendering, I haven't build it yet, so technically, it just an idea"
date: November 15, 2014 17:14
category: "Deviations from Norm"
author: hon po
---
Terje Moe, more rendering, I haven't build it yet, so technically, it just an idea.



![images/d5f1b96f38e48de8b02fe97ebe468685.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d5f1b96f38e48de8b02fe97ebe468685.jpeg)
![images/358d75f15653f1ca0af52c1347ec77d5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/358d75f15653f1ca0af52c1347ec77d5.jpeg)
![images/33f8b812e6d8afc355ff0933ca06958b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/33f8b812e6d8afc355ff0933ca06958b.jpeg)
![images/c5447449cabf035e90bab94ad7026013.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c5447449cabf035e90bab94ad7026013.jpeg)
![images/31fd056be23626b1621f8ae4a779c0e3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/31fd056be23626b1621f8ae4a779c0e3.jpeg)
![images/e41e707a511141cdd45f59818f3d30ec.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e41e707a511141cdd45f59818f3d30ec.jpeg)

**hon po**

---
---
**Eric Lien** *November 15, 2014 18:20*

Veri cool design. Plus geared steppers do not roll backwards when powered off. So platform will not fall unless spectra line breaks.


---
**Terje Moe** *November 15, 2014 22:41*

Thank you, I get it!



I've been thinking in same line as you, but for a bigger printer. Have you seen this tread, a little down on the page is a schematic drawing of a Spectra line system with 3:1 and 7:1 setup, check it out! 



[http://forums.reprap.org/read.php?2,377858,page=2](http://forums.reprap.org/read.php?2,377858,page=2)


---
**Mike Thornbury** *November 16, 2014 19:01*

I too have been experimenting with multiple spools and pulleys, this and the thread you linked too are very illuminating.



I love the idea of a guitar machine head for tensioning. Proven, reliable, plentiful and affordable. Aliexpress here I come!


---
**hon po** *November 17, 2014 03:03*

**+Eric Lien** I could turn the shaft of the geared stepper that I have with hand. So a heavy bed would still fall. May put foam at bottom for the worst case.



After moving the screw tie point to correct point, the two drive point on the bed has been too far from the Z rods. Plan to move spectra line to middle & offset Z rods to diagonal position.



**+Terje Moe** Thanks for the good post, so much to learn. The traction drive is even more interesting.



**+Mike Thornbury** guitar tuning pegs are great, easy for re-tightening. A printed tensioning wheel need 2 hand, one to tension line, second to tight the screw. But it also prevent novice user from loosening the tensioned line casually.


---
*Imported from [Google+](https://plus.google.com/111735302194782648680/posts/99eK6YtHroZ) &mdash; content and formatting may not be reliable*
