---
layout: post
title: "I you all were going to build your first 3D printer with accuracy the most important factor, what would you build?"
date: May 02, 2016 03:34
category: "Discussion"
author: Robert Clayton
---
I you all were going to build your first 3D printer with accuracy the most important factor, what would you build? I also notice the increasing number of references to delta printers not sure if that is due to a fad or improvement.﻿





**Robert Clayton**

---
---
**Ray Kholodovsky (Cohesion3D)** *May 02, 2016 03:44*

I would build a machine similar to the 3040 CNC with ballscrews, which can be positioned down to extremely small values repeatably. However, printing isn't cutting. The flow dynamics of molten plastic are your least precise element in play here, and that's why there are so many machines driven by timing belts. 



And that's when CNCs started using timing belts because the popularity of printers made the cheap, vicious circle. 


---
**Jeff DeMaagd** *May 02, 2016 04:01*

I'd suggest making the frame rigid and try to keep the moving mass as low as possible. I think intersecting gantry is the easiest way to do this, CoreXY is pretty popular these days and it does seem to have interesting benefits but it's hard to explain and seems more complicated.


---
**Tomek Brzezinski** *May 02, 2016 04:09*

If accuracy was of true utmost need, I would make a dlp SLA printer in the likes of a b9creator.but utmost accuracy is not my main goal instead very-high accuracy and repeatability would be mine... So overkill quality straight extrusions should go a long way. Fill in the rest with parts from this usergroup


---
**Tomek Brzezinski** *May 02, 2016 04:11*

Ray I don't think cnc market is yet driven in any meaningful way by the 3d printer market. What makes you say that cncs use timing belts? Most don't, as far as I've seen.  I think your main point about the precision suffering due to flow dynamics was spot on though. And that's why the small errors of belts isn't too important.  And if you want precision you'll want a multi extruder, for certain. 


---
**Ray Kholodovsky (Cohesion3D)** *May 02, 2016 04:14*

Tomek, I agree with you about the SLA, that was outside the scope of what I was thinking about when answering this question.

Regardless of whether the 3D Printing market caused it or not, GT2 timing belts and pulleys have gotten extremely cheap out of China and machines like the Shapeoko use it.  The OX uses GT3.  Ask any old timer machinists out there and they will scoff at the idea of belts and tell you that a true CNC should be screw driven.  Ball screw for ultimate precision.


---
**Tomek Brzezinski** *May 02, 2016 04:46*

Hmm, I would not consider the shapeoko really a CNC metal machine, but I see what you mean. I would consider shapeoko more a CNC wood router with limited metal abilities. Maybe just bias in thinking of more industry machines. 



Overall screws are not the dominant cost in CNC machines so I believe they don't direct much the choices made. I.e shapeoko more exception than rule.  


---
**Ray Kholodovsky (Cohesion3D)** *May 02, 2016 04:49*

I don't think we specified metal.  Shapeoko cuts aluminum too, as does my GT2 belt and Nema 17 driven OX. 


---
**Tomek Brzezinski** *May 02, 2016 04:50*

To make a truly rigid and precise fdm printer is I believe a more than $1k project so I would indeed look to SLA as an achievable route for similar money at precision printing (at smaller sizes). But for pumping out lots of quite high and fast fdm prints, Id stick to the SLA route. Mainly, just words to suggest evaluating what amount your goal of precision is for precision needs vs wants. I think I've seen a lot of rigid fdm printers with ballscrews and other high accuracy additions that really just slow down the whole machine.   So you have to ask what your goals really are (not explicitly questioning you just anyone thinking along the lines of precision being the ultimate guide of their fdm printer design)


---
**Blake Dunham** *May 02, 2016 09:18*

My upmost concern is always Z axis rigidity. So many times I see home made prints with terrible Z wobble and layer shifting. Having a very sturdy and rigid Z axis can solve many of the problems most people have with their printers. If you cheap out on everything else, buy a quality ball screw or lead screw and either mount everything to a rail and some nice polished rods and linear bearings. To expand upon this, having a rock solid hotend mount and bed mount will make a pretty significant difference as well. 


---
**J Drake** *May 18, 2016 16:52*

I would like cheap methods of closed loop feedback to enter the conversation.


---
**Tomek Brzezinski** *May 18, 2016 17:32*

**+Blake Dunham** Great example I think of maybe overthinking; money != rigidity. A proper sliding/sleeve bearing can offer better tolerance than most linear ball bearings, which allow for some angular tolerance.  In anycase, I agree the Z needs to be well supported, and that also means a good length of support.   I'm a big fan of beefy linear supports and wimpy isolated threaded rod for z-axis.


---
**Tomek Brzezinski** *May 18, 2016 17:47*

**+Jeffrey Drake** I'd love to see someone characterize % of failure due to small recovered failure and % of failure due to catastrophic failure. 90% of my failures seem to be a snag or total multiple-mm failure, which can sometimes be detected with modern stepper motor drivers (by a drop a change in the back EMF signature).



I used to be a huge fan of closed loop control, and it can actually be implemented fairly cheaply these days with hall sensors or magnetic rotational encoders, or even optical sensors (i'd say availability of cheap hall sensors makes them more appealing though.) But I now think it makes sense almost exclusively if you're changing motor control. Open loop steppers are quite reliable unless you have some other underlying mechanical issue. On the other hand, DC motors they seem hard to get the same $/value. Like right now I can buy killer Nema17 motors for <$20. The $30 pololu DC gearmotors motors of similar power, suck. The only time I've found good motors (<b>beefy</b> high quality brushes, solid gears, etc) has been with high end motors (maxxon / $300) at which point you've redesigned a DC servomotor.   So DC motors or even brushless should be available with gearboxes at our pricepoint, but it doesn't seem like there's a lot of availability. More ready availability would incentivize developing some good closed loop stuff. 



<b>*note*</b> for a premium build closed-loop still makes sense even for steppers, but I'm just saying it's not the low hanging fruit for most stepper motor builds on 3D printers or lasercutters (which are low-force devices.)


---
**Jeff DeMaagd** *May 18, 2016 18:02*

I've not had a noticeable step skip in some time, maybe years. There is the O Drive project on Hackaday projects that uses BLDC motors that looks exciting though.


---
**Tomek Brzezinski** *May 18, 2016 18:42*

BLDC open servomotor would be great, but I haven't found affordable quality gearboxes that could deal with a meaningful torque impulse.  Only option seems to be pulled industrial equipment but it'd be so much nicer if it was "off the shelf." 



It does work for the CNC power needs though because the power needs are high and the load is less consistent.


---
**Jeff DeMaagd** *May 18, 2016 20:16*

Here's a link and he has a couple videos  [https://hackaday.io/project/11583-odrive-high-performance-motor-control](https://hackaday.io/project/11583-odrive-high-performance-motor-control)


---
**Jeff DeMaagd** *May 18, 2016 20:19*

I understand there's three common winding types, at least the BLDC system that I use has three winding options. I suspect that the R/C hobby uses the fastest type, rewinding for torque could be done.


---
**Tomek Brzezinski** *May 18, 2016 21:02*

Rewinding for torque gives you either 1.4 or 1.7 time (i forget number & theory) the torque and same reduction in speed/kv. You could get something closer to good if you rewound the motor with a different number of copper wraps. But in any case, you intrinsically want a gearbox for a BLDC motor because ultimately it's characteristics are more suitable and more readily controllable at higher speeds. At least, if you want good torque out of it.  Even best case scenario the RC motors are not suitable for direct-drive in our applications.


---
**Jeff DeMaagd** *May 19, 2016 14:07*

How do the industrial machines with BLDCs do it then? Or is it more a matter of how the device was designed in the first place?


---
**Tomek Brzezinski** *May 19, 2016 15:01*

They have wye-wound bldcs (as you already suggested), they are dimension-ed for preferentially pancake-style dimensions (big stator=  high torque), and <b>key</b> they have quality gearboxes on the output. Always they have gearboxes on the output. A BLDC is similar in ways to like a several-step per revolution stepper motor, which is another way to look (to an extent) at the reason why it can't be direct-drive- you'd have way too big of a step angle. 



There's also an extent to which a commercial BLDC servomotor can have really good optimization so with good algorithms do better at low speed than you would ever get with a generic motor and controller. 



This is out my league though, and I don't know a lot about how BLDC servomotors actually get it done, just some of the reasons why it would be hard to use an RCmotor (even a rewound one) for a BLDC servomotor.


---
*Imported from [Google+](https://plus.google.com/103318213942990361542/posts/gAc2H6i9jh6) &mdash; content and formatting may not be reliable*
