---
layout: post
title: "Ive got my herculien running, all rods smooth and fast, yippie !!"
date: September 15, 2015 18:42
category: "Discussion"
author: Jo Miller
---
I´ve got my herculien running, all rods smooth and fast, yippie !!



now its time for the smoothie-software on my Atztek-Mini clone.



Problem: I can´t find any information about what type of thermistor is delivered with the Alli-Rubber heatpads as mentioned in the BOM. 



should I trust the smoothieware PID autotune  on good luck ? or how can you figure-out more info about an unknown thermistor ?





**Jo Miller**

---
---
**Zane Baird** *September 15, 2015 18:50*

In smoothieware you can use the beta value to determine temperature from thermistor readings. The 25/50C beta value for the Alirubber heat pads is 3950. This means that using the beta method, your temperatures will be accurate up to about 60 deg C or so. Above that, the temperature readings will be approximately 5 deg higher. As this is for the heated bed, this error is generally acceptable.


---
**Erik Scott** *September 15, 2015 18:51*

Mine came with 100k thermistors, I believe. That might be a decent assumption. It should be in the invoice you received, assuming you ordered directly from them.


---
**Eric Lien** *September 15, 2015 19:10*

I just tweaked the beta number using a fluke with a K-type thermocouple to be accurate for the top of the aluminum heat spreader taped down (with thermal compound) at 70C to 120C. But they have cheap multimeters out there with thermocouples for like $20 that should get you really close.﻿


---
**Jo Miller** *September 15, 2015 20:46*

thanks to you all,  I´´ll first try the 3950 beta methodb, could´nt find any details on my invoice-


---
*Imported from [Google+](https://plus.google.com/103341289176473342280/posts/EWxydw9teqm) &mdash; content and formatting may not be reliable*
