---
layout: post
title: "I hope it's ok I keep posting this stuff"
date: October 19, 2016 02:56
category: "Discussion"
author: jerryflyguy
---
I hope it's ok I keep posting this stuff. I've not found a conclusive answer to my problems w/ the Eustathios V2.



I tried printing a different (simple) part. Dropped the steeper current back, drop the print speed to 50mm/s. In these pictures I dropped the layer height to 0.1mm.



I had issues with variations of the layer accuracy in the first part. Some regions of the part printed good through the various layers while other regions were obviously not putting the part profiles one on top of the next. I also had one of the center support pads/towers have issues also. The initial layer of the support pad was in the correct location while subsequent layers where printed in a much different location.



Then using the same code I tried printing again and got results that in some ways were similar yet different in others. The net result was the same in that areas of the print turned out well while other areas just couldn't seem to print the layers on top of the correct spot in a repeatable manner.  It did print the support pads in the correct locations the second time.



I'll include the pictures of the first print here.



![images/a53d83a8db1315ee592e8f69d8a30f3c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a53d83a8db1315ee592e8f69d8a30f3c.jpeg)
![images/20e0b42a89c6807bbcb025bfe8f3663a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/20e0b42a89c6807bbcb025bfe8f3663a.jpeg)
![images/f8cf8e288b36a5edcb63c4e31e817e50.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f8cf8e288b36a5edcb63c4e31e817e50.jpeg)
![images/98a975a5af0209e99b21968834cc6ccf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/98a975a5af0209e99b21968834cc6ccf.jpeg)

**jerryflyguy**

---
---
**jerryflyguy** *October 19, 2016 02:59*

Pic of the first layer of the second attempt ![images/a884b677bd3538591ad3c838e7e17cf5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a884b677bd3538591ad3c838e7e17cf5.jpeg)


---
**jerryflyguy** *October 19, 2016 02:59*

Also second attempt ![images/597e64e8ac9a6f9bfa7a753f30faa757.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/597e64e8ac9a6f9bfa7a753f30faa757.jpeg)


---
**jerryflyguy** *October 19, 2016 03:02*

Last pic of the second print, same code as the first print. I can't figure out if this is a stepper issue, or a driver or if it's something all together different like simplfy3d or the controller itself? 



Completely at a stand still here, don't want to just throw money at it (can't afford to) esp if I can figure out what component is causing the issue![images/321501482b778bb5b53262f564c9f2bb.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/321501482b778bb5b53262f564c9f2bb.jpeg)


---
**jerryflyguy** *October 19, 2016 03:05*

For kicks and giggles I tried a third print (shown) but rotated the part in simplify3d and went back to 0.20mm layers. Similar result![images/edece67720322c1f920b1eab35192d6e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/edece67720322c1f920b1eab35192d6e.jpeg)


---
**Mike Miller** *October 19, 2016 03:11*

Try over extruding, then dial back till it looks right printing a simple part (cube) then mess with retraction...like 50mm/s and 1.2 mm. Walk before you make hypoid gears. 


---
**jerryflyguy** *October 19, 2016 03:16*

**+Mike Miller** hi Mike, my issue here isn't the quality of extrusion, it's a randomness of positional accuracy.. if you look at the printed parts you'll see places where the layers line up one on top of the other and then other places where they don't, and it's pretty repeatable also. 


---
**Bruce Lunde** *October 19, 2016 03:26*

It still looks like you are under-extruding? I have been tuning my HercuLien as well and was doing so initially. You might also check your belts, I found I had to tighten mine up twice before my layers lined up good. I am now working on getting rid of blobs on corners, so it seems to take some work to get it just so?


---
**jerryflyguy** *October 19, 2016 03:57*

**+Bruce Lunde** hi Bruce, I think my first layer is under extruding for sure. I'm going to tweak the first layer width to see if I can get it to fill in some. I decided I'd try switching stepper drivers around. Swapped the Y w/ the Z driver. I also found one set screw (of the two, second screw was tight)on a pulley which was a tiny bit loose. Fired it back and it's working. Not convinced the screw was the issue, would tend to think it was something w/ the driver (connection or something) that was the issue. Will keep banging away at it. I'm not near done tweaking the settings but until I can get reliable and repeatable motion, no point in messing w/ simplify3d. 


---
**jerryflyguy** *October 19, 2016 03:58*

Fourth print, which so far is looking ok![images/f9029b271360959b0540c2f80da45ef0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f9029b271360959b0540c2f80da45ef0.jpeg)


---
**Eric Moy** *October 19, 2016 09:19*

It looks like one of your steppers is skipping steps. Can you easily love the axes by hand with no resistance? Also, why did you disk back the stepper current? Generally stepper current is dialed back until it skips, then bumped back up a bit for good measure


---
**Bruce Lunde** *October 19, 2016 15:49*

**+jerryflyguy** That more recent print looks good!


---
**jerryflyguy** *October 19, 2016 16:00*

**+Eric Moy** it was my thought that I was skipping steps also, I upped the current but that made it worse? So pulled the current back to what **+Eric Lien** was running. There was a concern that I was over currenting the drives and hitting a thermal limiter.



Since I've switched the stepper drives (switched Y and Z) it ran perfectly on the last print.



I did find a solitary set screw that was a bit loose at the same time (1 of two screws in a main belt pulley, the other screw was tight). It is possible that the higher current was causing the shaft to slip and when I reduced the current it 'grabbed' enough to work.. sorta. 



My next test will be to switch the stepper drives back the way they were and see if the problem comes back. If it does, I'd assume I wrecked a drive, if it doesn't then it was the screw or a bad connection?


---
**jerryflyguy** *October 19, 2016 16:14*

**+Bruce Lunde** yes, that print turned out very well. Got to figure out how to make the top surface fill in better, the side walls are very smooth on the part. (I'll post a picture of the finished part in a couple hrs, I'm currently at work)


---
**jerryflyguy** *October 19, 2016 19:24*

**+Bruce Lunde** the finished part![images/dd9dc908c4303c1fd1954397a682364e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dd9dc908c4303c1fd1954397a682364e.jpeg)


---
**Eric Lien** *October 20, 2016 03:03*

**+jerryflyguy**​ looking much better. I think the set screw was your root cause. You weren't exhibiting standard layer shift... But more of a sloppy skew in the print geometry. An area to watch for is circles, watch for an out of round tendency in circles. If you see that I lean towards mechanical issues.



If instead you see consistent layer shift but the geometry still matches the layer below then I would look for nozzle catches or stepper/driver thermal throttling.


---
**jerryflyguy** *October 20, 2016 03:13*

**+Eric Lien** thanks. Makes sense, I'm going to switch the z & Y axis drives back to the way it was, just to confirm the issue doesn't show back up.


---
**Eric Lien** *October 20, 2016 03:33*

**+jerryflyguy** keep us posted. Love all the posting you have done recently. There is a lot to learn in this hobby, and with the nuances of building your own printer from scratch. My design files are by no means paint-by-numbers Ikea plans, so the knowledge in our community and it's members is our most valuable resource. I am glad you keep asking questions. Also, I am glad the community has been so responsive recently about helping answer them since I have been so swamped at work. I hope to be done with some of these large work projects soon and get back to designing, posting, and answering more community questions.



The questions you ask help you, but the answers and discussions help many who were afraid to ask. Keep up the great work everyone.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/Bg9NXz64EvD) &mdash; content and formatting may not be reliable*
