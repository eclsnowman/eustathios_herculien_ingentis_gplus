---
layout: post
title: "It's happening. Herculien extrusion being cut. Yay.  Brandon Satterfield"
date: February 14, 2015 18:24
category: "Discussion"
author: Dat Chu
---
It's happening. Herculien extrusion being cut. Yay. ☺ **+Brandon Satterfield**​



#thisisgonnabeawesome

![images/f1b12421c3555e6e6725383f8c214a13.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/f1b12421c3555e6e6725383f8c214a13.jpeg)



**Dat Chu**

---
---
**Eric Lien** *February 14, 2015 19:04*

All I can say is that **+Brandon Satterfield**​ is chalk full of win.



Excited to watch the progress. Take lots of pics ;)



I sure wish I had.


---
**Seth Messer** *February 14, 2015 22:31*

Ooooo. Fun. What kind of blade did you get for the miter saw? 


---
**Mike Thornbury** *February 15, 2015 15:28*

**+Seth Messer** it's just small extrusion, you can use the blade it came with and get great results.



The most important thing is to make sure your saw is square and straight.



Take it slow, clamp your pieces, be ready to do a lot of clean-up :)



If you cut a lot, a small-kerf 100-tooth+ carbide tipped blade will go through like butter, but it's not needed for the occasional cut.﻿



Always wear eye protection, aluminium fairly flies out and goes everywhere.


---
**Seth Messer** *February 15, 2015 15:50*

Thanks Mike!  I happen to have a decent 80 tooth Irwin blade on my saw. I may pick up another one to keep for non-wood cutting duties. ﻿


---
**Mike Thornbury** *February 16, 2015 07:35*

That will be perfect. Unless you screw up, the alloy won't hurt your Irwin at all.



One thing I will say, hold the saw down until the blade stops spinning - If I could be bothered I would dig out the piece of 8040 that I only held by one side and then let the blade come back up while it was still spinning and take a photo. Luckily it missed me, but the amount of energy imparted to the extrusion was enough to twist it out of square - no mean feat for 8040. The blade stopped rather fast and the whole saw jumped about half a foot in the air. :)


---
*Imported from [Google+](https://plus.google.com/+DatChu/posts/FmbSmWCMrE8) &mdash; content and formatting may not be reliable*
