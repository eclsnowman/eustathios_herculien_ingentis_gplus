---
layout: post
title: "Isaac asked for Eust photos..."
date: March 21, 2015 02:15
category: "Show and Tell"
author: Jeff DeMaagd
---
Isaac asked for Eust photos...



![images/4a6b7724fdb039c92ef7c0edfdc2a4e3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/4a6b7724fdb039c92ef7c0edfdc2a4e3.jpeg)
![images/77622655034efce3a0403b29a4b0919f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/77622655034efce3a0403b29a4b0919f.jpeg)

**Jeff DeMaagd**

---
---
**Eric Lien** *March 21, 2015 03:19*

Its a fun show. Lots of great people. Just had some dinner with a gaggle of printer guys, and now having some drinks. If your not here you are missing out ;)


---
**Derek Schuetz** *March 21, 2015 05:08*

way to rub it in **+Eric Lien** 


---
**Isaac Arciaga** *March 21, 2015 05:44*

**+Jeff DeMaagd** you rock. Wish I could be there with you all!


---
*Imported from [Google+](https://plus.google.com/+JeffDeMaagd/posts/H2KgDFQRyRc) &mdash; content and formatting may not be reliable*
