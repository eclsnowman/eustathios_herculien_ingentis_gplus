---
layout: post
title: "Corner brackets and Z shafts have arrived today"
date: March 24, 2015 09:14
category: "Show and Tell"
author: Oliver Seiler
---
Corner brackets and Z shafts have arrived today.

How important is precise alignment of the Z shafts? I've measured and placed them in the middle and the linear bearings seem to be running fine. Is there a more sophisticated way of aligning them?

![images/fe5e175b5ff6b35b0cceba166fc16fc8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fe5e175b5ff6b35b0cceba166fc16fc8.jpeg)



**Oliver Seiler**

---
---
**Eric Lien** *March 24, 2015 10:54*

Alignment is pretty important. But smooth travel will let you know if things are wrong. One thing, I think the white bed supports you have will need to be replaced with the newer version. I had to move the lead screw spacing when I designed the lower mounts.


---
**Erik Scott** *March 24, 2015 12:25*

Yes, that's something I noticed when doing my CAD work recently. Your bed supports and z screw/rod style are incompatible. 



There isn't really a super precise way of positioning the z shafts, at least that I know of, besides careful measurement and placement. It worked fine for me. 


---
**Eric Lien** *March 24, 2015 14:10*

**+Erik Scott** they are compatible with themselves, just not compatible between V1 and V2. The reason is clearance for the leadscrew pulley. On V2 it is above instead of below the lower mount. I had to move the screws in towards the center-line for clearance.


---
**Erik Scott** *March 24, 2015 14:11*

That's what I meant. ﻿when I said 'your' I was referring to Oliver's printed parts in the picture, not the components in your cad files. Sorry for any confusion. ﻿


---
**Eric Lien** *March 24, 2015 14:41*

I figured that's what you meant. I just wanted to clarify for others.


---
**Gus Montoya** *March 24, 2015 15:30*

**+Oliver Seiler**  Wow nice!


---
**Oliver Seiler** *March 24, 2015 17:20*

Oops. Thanks for the heads up ;)


---
*Imported from [Google+](https://plus.google.com/+OliverSeiler/posts/2tP3JtmpsUR) &mdash; content and formatting may not be reliable*
