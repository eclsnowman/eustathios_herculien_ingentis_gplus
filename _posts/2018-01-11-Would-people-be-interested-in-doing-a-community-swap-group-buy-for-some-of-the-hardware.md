---
layout: post
title: "Would people be interested in doing a community swap /group buy for some of the hardware?"
date: January 11, 2018 17:10
category: "Discussion"
author: Dennis P
---
Would people be interested in doing a community swap /group buy  for some of the hardware?  Kind of like friends of Carl for sourdough or PFranc for Garmin connectors where we share. Does something like this already exist?



The Hercstruder needs 2- M3x50mm cap head screws and 2 springs. I have tried a couple of GOOD local industrial hardware stores and no luck for either. So McM it is... 25 screws for $3.50, 12 springs for $7.50. I would be happy to share 4 screws and 2 springs for $2.50 send in a first class envelope.  



Its not a money making proposition, just a way to help each other out a bit and manage the costs better. 





  





**Dennis P**

---
---
**Eric Lien** *January 11, 2018 17:31*

How about I send you my old HercuStruders printed parts and hardware. I only use Bondtech now. I really swear by the Bondtech QR in a long bowden setup like Eustathios and HercuLien. Coming in a close second is the Bondtech BMG, and it has the benifit of faster retraction with it's 3:1 reduction, and doesn't need the planetary gearbox motor which can be pricey.



Then there are the offerings from E3D like the Titan which I have tested extensively and works well, but is only single sided drive. And finally if budget is your driving force, I know a few people who said some of the Titan Clones on Aliexpress can be ok.



I would do most of these before my HercuStruder. It works well, but was designed and not updated from many years ago. There have been great advances and price reductions since then in the extruder arena.



To your second point, I agree a part swap arena would be a good thing. I know I have spares of things. The only difficult part is shipping costs. But perhaps I can drag my spares out the MRRF, and if anyone joins me we can do some horse trading :)


---
**Dennis P** *January 12, 2018 20:22*

Eric- Thanks for the very gracious offer. Unfortunately, the extruder is the first thing I made though :( I may eventually run dual extruders so I wanted to give the Bowden setup a try.  I went ahead and got a 25 packs of the M3x40's, and will use them for some of the shorter screws needed buy using  dremel with cutoff wheel to cut them down.  Menards/Hardware store has springs in the Hillam drawers- there is one that is 0.187 diam x 1.5" in 0.020 wire. I figure I will try cutting two of those in half and using both pieces per screw.   



I think that the swap area/garage sale area might be useful for all.  


---
**Bruce Lunde** *January 15, 2018 05:09*

Eric - which bondtech unit do you use? 


---
**Eric Lien** *January 15, 2018 08:06*

i use  QR on the Eustathios and HercuLien.  But the BMG would work great too.


---
**Bruce Lunde** *January 15, 2018 19:56*

Eric and All,   Has anyone placed these (QR/BMG) directly  on the carriage.  I have fought both the filament feed and my hercstruder and never have gotten a really good part out of my printer, so I am going to try the Bondtech solution.


---
**Eric Lien** *January 15, 2018 21:25*

Walter has the flying extruder, and a few people have made direct drive versions if you look back through posts. I have a direct Drive version for the BMG:



[https://github.com/satbe/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/Eric%20Lien/Eustathios%20BMG%20Carriage](https://github.com/satbe/Eustathios-Spider-V2/tree/master/Community%20Mods%20and%20Upgrades/Eric%20Lien/Eustathios%20BMG%20Carriage)





![images/69ff393a7a57eed5fd9675d65ce6a101.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/69ff393a7a57eed5fd9675d65ce6a101.png)


---
**Eric Lien** *January 15, 2018 21:27*

That being said **+Bruce Lunde**​, I am sad to hear you haven't been having luck. I know the Hercustruder is no Bondtech... But should certainly be able to make prints. Can you give any feedback in another post about issues you have been having. We have a great group here. Should be able to get things figured out.


---
**Bruce Lunde** *January 16, 2018 01:36*

**+Eric Lien** I have made a post tonight as suggested, i have not given up, I know a bit more time and some parts will get me there!


---
**Dennis P** *January 16, 2018 02:23*

**+Bruce Lunde** that and a winning lottery ticket will get us all the hardware we need! I will probably migrate over to the solutions that Eric and other recommended eventually, but I want to get this thing operational first and try to learn the Bowden setup- seems to be a right of passage for all. 


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/ZKfW6bMAiPX) &mdash; content and formatting may not be reliable*
