---
layout: post
title: "hello guys, thanks for accepting me. I am new to this community"
date: April 20, 2014 11:39
category: "Discussion"
author: Bijil Baji
---
hello guys,

 thanks for accepting me. I am new to this community. I have a prsa i2 i am having troublr the extrusion stops after 30-40 minutes of contionious printing. any idea to solve this problem??





**Bijil Baji**

---
---
**D Rob** *April 20, 2014 13:27*

Try different tension settings on the filament bearing. Too tight and it strips. Too loose and if strips.


---
**Bijil Baji** *April 20, 2014 14:39*

ok i will ty that if possible could you provide me with ideal settting for slic3r i use my filament diameter is 1.7 PLA j hotend nossile size .4mm


---
**D Rob** *April 20, 2014 14:56*

I use cura. made by ultimaker. I reccomend it to anyone.especially beginners. super easy to use.

[http://software.ultimaker.com/](http://software.ultimaker.com/)


---
**Bijil Baji** *April 20, 2014 17:18*

Thanks **+D Rob** I will definetly try it. actually i was asking about ideal setiing for infill density stuff like that. i am thinking of making replacement parts for my prusa i2 so they have to be strong and durable.


---
**D Rob** *April 20, 2014 17:33*

Infill depends on the desired strength of the part. Also consider wall thickness. I recommend cura as it is the easiest (imo) slicer to get up and running. If you are printing parts for a printer upgrade. I suggest leaving the i2 design. Actually the mendel design altogether is a very space inefficient design. Any design with the bed as x or y is. the extra mass limits speeds, acceleration, and adds unnecessary mass to be moved on what should be a rapid axis. x and y move constantly and z sits there whole layers. I started with a mendel hybrid design (mendel90 and i3) I am now using it to build a better design. however if you want to stay with i2 I will help where I can. Have you calibrated your machine in x y and z? what firmware do you use? which host? Which electronics?


---
**Bijil Baji** *April 20, 2014 21:39*

Thanks **+D Rob** definitely i want to upgrade the design. I have calibrated the printer i was working fine for some time the problems started. I use the repetitor host frimware it was really easy to configure to my needs i ued RAMPS and wanthai 4.2Kg torqe motors but i have kysan 5Kg torqe stock. i am some what beginer on the 3d printing field. I have an online CNC store and Project designing center and i have the resources but currently lack of budget forced me to go for the i2. i am always  open for ideas and updates.  i have a arduino based community you might be intrested

[https://plus.google.com/u/0/communities/118202087054340224465](https://plus.google.com/u/0/communities/118202087054340224465)


---
**D Rob** *April 20, 2014 22:14*

A simple upgrade would be a t slot framed machine. **+MISUMI USA** has tight tolerances (.5mm) on their cuts this cuts down threaded rod frames helps wire routing and makes disassembly/reassembly super quick and easy. Check out the [youmagine.com](http://youmagine.com) ingentis post by **+Tim Rastall** awesome machine with an elegant design and super prints


---
**Bijil Baji** *April 22, 2014 12:32*

**+D Rob** i tied using Cura but i am facing some problem. even if i move the position it is only printing at the 0,0 position and i cant find any manual controls the


---
**D Rob** *April 23, 2014 00:20*

did you set home as 0,0? this is for delta machines try changing this in file>machine settings


---
**D Rob** *April 23, 2014 00:21*

are you using marlin?


---
**Bijil Baji** *April 23, 2014 00:35*

**+D Rob** you were right the machine center was set to 0,0 and what about the manual control??


---
**Bijil Baji** *April 23, 2014 20:17*

**+D Rob** still having trouble with extruder stops after 10 mins of printing. Its really frustrating wasted a lot of filaments. Please help.... i am thinking of upgrading even for that ineed a printer.


---
**D Rob** *April 24, 2014 00:11*

what extruder design? what hot end, hobbed bolt? diy hobbed bolt? post pics up close in the community of the whole feed chain from spool to extruder to hotend. back out 6 inches of filament and show pics of the 6in that backed out. Is the filament grinding, slipping, or being squished before the hot end?


---
**Bijil Baji** *April 24, 2014 01:06*

**+D Rob** Wades extruder design. J hot end. its Normal hobbed bolt. the filment slips after extrusion stops


---
**D Rob** *April 24, 2014 01:53*

printing pla?

real jhead?


---
**Bijil Baji** *April 24, 2014 02:27*

**+D Rob** Printing PLA


---
**D Rob** *April 24, 2014 02:46*

join the 3d printing community post there in a fresh post. a bunch of us will help you


---
**Bijil Baji** *April 24, 2014 02:58*

ok sure will do **+D Rob** thanks for all your help


---
**Brian Bland** *April 24, 2014 06:09*

Do you have a fan blowing across the PEEK on the Jhead?  Fan is required with PLA.


---
**Brian Bland** *April 24, 2014 06:12*

I see the fan in your pics.  Is it set to run the whole time your printing?  What temps are you extruding at?


---
**Bijil Baji** *April 24, 2014 06:17*

**+Brian Bland** yes its running continusly i print at 195-200 on PLA havnt tried ABS


---
**D Rob** *April 24, 2014 16:28*

to keep it simple I'll move our other conversation here 


---
*Imported from [Google+](https://plus.google.com/+BijilBaji/posts/hVKE5UE7TRw) &mdash; content and formatting may not be reliable*
