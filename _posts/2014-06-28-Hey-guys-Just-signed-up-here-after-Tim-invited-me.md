---
layout: post
title: "Hey guys! Just signed up here after Tim invited me"
date: June 28, 2014 01:10
category: "Discussion"
author: Eric I.
---
Hey guys!



Just signed up here after Tim invited me. I'm considering getting my first 3D printer am considering all my options. I've never designed my own machine from scratch, but I was able to buy and assemble my current computer as well as assemble my own furniture, so I'm pretty good at fitting things together.

 I was debating between a Da Vinci and a Printrbot Simple, but after looking up the designs here I think the Eustathios looks very impressive (
{% include youtubePlayer.html id=aucE49ZBXx0 %}
[3D Printer - Eustathios (Ingentis Clone) TinyG XY Axis Move Test](https://www.youtube.com/watch?v=aucE49ZBXx0)), and may be a good mix between everything that I wanted out of a printer.

So what would I need to do to build one? Can the parts be ordered online (transportations an issue for me right now)? Is there any guide that would give me an idea where all the parts go? And about how much do you think it would cost me overall?



Thanks guys, I look forward to talking with you all ;)





**Eric I.**

---
---
**Eric Lien** *June 28, 2014 02:30*

Mostly complete BOM here. [https://docs.google.com/spreadsheet/lv?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing](https://docs.google.com/spreadsheet/lv?key=0Am629YCI5h_wdHkxa1gyajBrak5LbDVwejFldXFORUE&usp=sharing)


---
**Eric Lien** *June 28, 2014 02:31*

My solidworks rendition of it is here: [https://github.com/eclsnowman/Lien3D_Eustathios_Spider?files=1](https://github.com/eclsnowman/Lien3D_Eustathios_Spider?files=1)


---
**Eric Lien** *June 28, 2014 02:32*

I plan on doing a full set of prints and build details but I have actually taken on some projects recently that are taking up a great deal of my time.


---
**Eric I.** *June 28, 2014 06:02*

Thanks! Is there any way to open these without Solidworks?


---
**Eric I.** *June 28, 2014 06:03*

Whoa, and to be clear, according to the BOM this would wind up costing about $850 in parts alone right?


---
**Tim Rastall** *June 28, 2014 08:43*

What **+Ashley Webster** said.  Tbh,  if you want a cheapish bot with good build instructions,  I'd go for a mendel90 or failing that a prusa i3. The Ingentis for factor provides a number of critical advantages from a speed and quality perspective but the downside is a higher cost for materials.  Eric's BOM is a little pricey though,  the azteeg board,  leadscrews etc do push the price up. With a quality ramps board and my original belt driven Z axis,  you could shave $100 of easily. 


---
**Tim Rastall** *June 28, 2014 08:50*

**+Eric Ironshell**,  here is the build guide for the mendel90.  [https://github.com/nophead/Mendel90/blob/master/dibond/manual/Mendel90_Dibond.pdf](https://github.com/nophead/Mendel90/blob/master/dibond/manual/Mendel90_Dibond.pdf)

The mendel 90 kit can be bought direct from **+nophead** for 499 pounds plus tax and shipping.  His email is nop.head@gmail.com


---
**Eric Lien** *June 28, 2014 11:43*

**+Tim Rastall** I have not heard of a person yet who dislikes their mendel90. And there seems to be lots of people with mendel90 work horses out there printing 24/7. Not sure Why I forgot to mention it.


---
**Eric I.** *June 28, 2014 22:34*

Also, as a side note, how are you guys getting your hands on Solidworks for home use?

I've looked into it before, but I thought it could only be used by full blown companies or, in a very diminished capacity, a student version.


---
**D Rob** *June 28, 2014 23:16*

**+Eric Lien** I have a Prusa i3/Mendel 90 hybrid that I built. Love it but with the heavy bed and tiny build to footprint ratio it is really inefficient.


---
**Eric I.** *June 29, 2014 01:04*

yeah, while I'm still thinking about things, I am starting to lean more and more towards the Da Vinci. Joining this community has shown me that the Printrbot Simple kit is only scratching the surface of the builder's fun that can be had with 3D printers. The Eustathios looks like a great printer with a great building experience, but frankly I don't think I have the time, experience, or the money to complete that right now, as the Da Vinci was already pushing my budget.



So I'm thinking that the Da Vinci is probably my best bet, as it should have me up and running within about a week. I'm finding plenty of hacks to jail-break it and train it to be very high-level raptier rep rap, so that should be an option if the closed-box system gets on my nerves. It will also give me the opportunity to test out 3D printing and see how much I even like it before spending dozens of man-hours assembling one.



And then, if in a year or two I'm still in love with 3D printing and I have the time, money, and tools, maybe I'll be able to come back and build a 2nd printer and go all the way this time. I can't wait to see the designs you guys will have cooked up by then ;)



Anyone got any thoughts on my plan before I place my order?


---
**Eric Lien** *July 02, 2014 00:34*

**+Eric LeFort** glad to hear you are interested in the Ingentis & Eustathios family. One thing to remember is the Ingentis design is Tim's and the mods/redesign into Eustathios is Jason's. I am just learning solidworks so remodeled the entire sketchup assembly from Jason's github into solidworks. I am not the inventor... Just an apprentice drafter.



But I am very active on G+ so people give me undeserved credit. 


---
**Eric Lien** *July 02, 2014 00:46*

**+Eric LeFort** thanks. I do feel I made a pretty nice model... But I don't want to anger the printing gods with false claims of genius. If I do they shall rain down plagues of extruder jams and electronics gremlins upon me. ;)


---
**Eric I.** *July 02, 2014 19:39*

Thanks for the clarification Eric (both of you :P), but what version of Solidworks are you guys using? It looks like some amazing software and I'd love to own my own copy too, but was never sure how...


---
**Eric Lien** *July 02, 2014 20:59*

I am using solidworks 2014. But I can export out into lots of formats. On my Github there is .Step, Sketchup, Creo. Spaceclaim, and 3DPDF.


---
**Eric I.** *July 02, 2014 21:27*

Oh, well that would help, but more specifically I'm wondering how I could get my hands on my own copy of solidworks to use with my projects. Anyone know?


---
**Eric Lien** *July 02, 2014 21:38*

**+Eric Ironshell** there are two ways. Pay for it.. Or pirate it. Let your concience be your guide.﻿


---
**Eric I.** *July 03, 2014 01:46*

(Deleted cause comment may have been insulting when I didn't mean for it to be, sorry for any offense, I'll try to be more careful :C)


---
**D Rob** *July 03, 2014 14:47*

**+Eric Ironshell** not cool :/


---
**Eric I.** *July 03, 2014 18:04*

oh...I'm sorry, I meant it as a joke and it sounded funny in my head, but now that I reread it it does sound like an insult. I never intended it that way. I removed it just in case, as even if taken as a joke it's not that funny anyway :/



I hope I didn't insult you Eric, and I'm sorry if I did. And I really appreciate you helping me and the community so much :)



Any hard feelings?


---
**Eric Lien** *July 03, 2014 18:24*

I am fine. It takes a lot to hurt my feelings. Let me know if you need any help on the models. I could also convert it to designspark mechanical which is free and essentially spaceclaim with features removed. Spaceclaim uses the autodesk inventor backend engine and had some nice features. ﻿


---
**Eric I.** *July 03, 2014 19:05*

Ok good, glad to hear it ;)

Also, this is the first time I've heard of Design spark mechanical, but it looks very impressive! The only thing I noticed it's lacking from Solidworks is the ability to model the physics of your design and make sure the gears mesh correctly, etc. Anyone know of a freeware product that offers the physics side of things?



Also, and I've mentioned this elsewhere on the several threads I've been running but I just realized I made have not made this very clear, but I've already ordered a Da Vinci as my first 3D printer and will probably be tinkering with that for at least a few months before considering making a Eustathios. So you can go ahead and export it for DesignSpark mechanical for me if that's easy to do, but I'd mostly just enjoy looking at the design for now anyway.



I'm mostly just looking for info on design cads right now, as those seem useful overall.


---
*Imported from [Google+](https://plus.google.com/106160893078256019861/posts/9deeYWJAgHy) &mdash; content and formatting may not be reliable*
