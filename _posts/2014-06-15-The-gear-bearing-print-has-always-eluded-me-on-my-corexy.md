---
layout: post
title: "The gear bearing print has always eluded me on my corexy"
date: June 15, 2014 19:04
category: "Show and Tell"
author: Eric Lien
---
The gear bearing print has always eluded me on my corexy. Printed first try on the Eustathios. I have said it before but I will say it again: thanks **+Tim Rastall** and **+Jason Smith**. This printer is amazing.



Plus I printed a side mount interchangeable diameter spool holder. Works slick. I will get the new flange mount added up to my other files on repables.﻿ The flange mounts to the side of the printer connected to the two horizontal aluminum extrusions.



![images/6196c808515ff59e333a240603fb5461.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6196c808515ff59e333a240603fb5461.jpeg)

**Video content missing for image https://lh3.googleusercontent.com/-OD0JA4oc6_k/U53uSoqLpfI/AAAAAAAAe4Y/to80FBaPJL4/s0/VID_20140615_114003.mp4.gif**
![images/c8bd05aed56304885785fac76d4f1d60.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c8bd05aed56304885785fac76d4f1d60.gif)

**Video content missing for image https://lh3.googleusercontent.com/-Y8YQsK1_zVA/U53uSjf3rcI/AAAAAAAAe3w/eDA4MTxXJro/s0/VID_20140615_103337.mp4.gif**
![images/6535af235c6a07328646b935c61cbb90.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6535af235c6a07328646b935c61cbb90.gif)

**Eric Lien**

---
---
**Eric Lien** *June 15, 2014 19:47*

**+Eric LeFort** PLA.



On a side note has anyone noticed the "processing" on videos on google plus takes a lot longer than it used to? What used to take minutes takes much longer now.


---
**Ethan Hall** *June 15, 2014 22:36*

How much total did it cost you to build your printer? I want to build an ingentis but don't know if it is possible because of costs. 


---
**Eric Lien** *June 15, 2014 23:06*

I haven't added it up yet. I am kind of afraid to. Probably $900. But misumi first $150 took some of the pain away :)



I will be dialing in the BOM over the next month for documentation of the printer. **+Jason Smith** was nice enough to make me a contributor on his github. I will be creating fully dimensioned prints and models (Solidworks, step, & stl). I have never done this before so it should be fun.



There are areas where money could be saved. 8mm fine all-thread vs leadscrews (my corexy ran this way for months with zero z banding). No heated bed. Chinese ramps and no LCD. Raw extrusions versus Misumi precut. Etc. But I will say the precut from Misumi are awesome because it does take the precision tools needed down a notch.﻿


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/XuWpLtL78TV) &mdash; content and formatting may not be reliable*
