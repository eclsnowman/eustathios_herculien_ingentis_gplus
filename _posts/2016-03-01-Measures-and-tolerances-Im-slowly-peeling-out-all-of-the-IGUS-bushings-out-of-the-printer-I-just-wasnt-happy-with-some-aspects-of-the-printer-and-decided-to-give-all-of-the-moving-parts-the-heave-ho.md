---
layout: post
title: "Measures and tolerances I'm slowly peeling out all of the IGUS bushings out of the printer, I just wasn't happy with some aspects of the printer and decided to give all of the moving parts the heave-ho"
date: March 01, 2016 23:21
category: "Discussion"
author: Mike Miller
---
Measures and tolerances



I'm slowly peeling out all of the IGUS bushings out of the printer, I just wasn't happy with some aspects of the printer and decided to give all of the moving parts the heave-ho. 



Ordered new 10mm Hardened shaft...it measures 9.98mm



Trying to reuse the 10mm bearings at the corners...they measure 9.93mm



You have any idea how hard it is to pound an interference fit bearing the 10mm it needs to go while the shaft is in a printed support piece? 



I may have to freeze and heat parts, hoping to keep the hot parts cold enough to not melt plastic. 



Any suggestions? 







**Mike Miller**

---
---
**Carlton Dodd** *March 02, 2016 00:33*

Fine sandpaper on the ends of the shafts?


---
**Eric Lien** *March 02, 2016 00:53*

Also the ID is probably closer to 10mm than that. Measuring ID holes is better suited for a bore gauge than calipers. Calipers jaw offset causes holes to appear more undersized than they are.


---
**Mike Miller** *March 02, 2016 00:59*

Well it sure doesn't fit on like 10mm hole and a 10mm shaft. :)


---
**Eric Lien** *March 02, 2016 01:30*

I think someone else, maybe **+Bruce Lunde**​ had that issue too. But then he ordered the fasteddy bearings and all was good.


---
**Bruce Lunde** *March 02, 2016 04:21*

**+Mike Miller**  **+Eric Lien** is correct, I went with alternative vendors for the rods and bearings to save a few dollars but they did not fit; then replaced with the suggestions from the BOM and everything worked perfectly.  


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/NnL1T5j9kBA) &mdash; content and formatting may not be reliable*
