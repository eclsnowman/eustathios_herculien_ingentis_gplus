---
layout: post
title: "Eric Lien Do you have a config file (Marlin for an X3) for the Eustathios?"
date: September 10, 2016 21:35
category: "Discussion"
author: Stefano Pagani (Stef_FPV)
---
**+Eric Lien** Do you have a config file (Marlin for an X3) for the Eustathios? I can only find the smoothie one on the github





**Stefano Pagani (Stef_FPV)**

---
---
**Eric Lien** *September 10, 2016 22:20*

I have the HercuLien one that should work. You just need to disable the second extruder and change the build volume: [github.com - HercuLien](https://github.com/eclsnowman/HercuLien/tree/master/Marlin%20Firmware)



But note it is a really outdated version. My suggestion would be to start with a fresh Marlin download, and edit it. It really only takes a few minutes once you learn the details. Plus it is important to teach yourself the settings and what they do. Look at the YouTube channel of **+Thomas Sanladerer**​​, he recently released an updated video about it. But he also has several versions in the past going over PID tuning, extruder calibration, etc.﻿


---
**Stefano Pagani (Stef_FPV)** *September 11, 2016 03:49*

Ok, ill just edit it, I will be sure to check the videos! Thanks! P.S. I have 2 extruders, that's why I went with the X3. (Wish I waited for the GT though)


---
**Eric Lien** *September 11, 2016 04:30*

The X3 will serve you well, Marlin still runs the majority of printers out there. And, you can always upgrade later. A printer is never finished, just varying states of operational :)


---
*Imported from [Google+](https://plus.google.com/101784843665098940301/posts/78or2Eqimub) &mdash; content and formatting may not be reliable*
