---
layout: post
title: "It's been a long time coming, but I have some updates for those using the Herculien"
date: June 19, 2016 21:53
category: "Show and Tell"
author: Zane Baird
---
It's been a long time coming, but I have some updates for those using the Herculien.



I completely redesigned the belt-tensioner assemblies to enable a much easier way to tension belts. It can now be done with a singe screw turn using a hex wrench. Additionally, the size of these belt tensioners allows for clearance to remove the cross-rods from the gantry without loosening belts or disassembly of any XY guide rod/pulley systems. This means that the carriage can be easily exchanged for ease of modification. As a result I had to redesign the endstop mounts to accommodate the changes as well. The new belt-tensioners require only 2 screws for assembly and also provide another 1 cm distance between bushings to alleviate misalignment issues (with no loss in build area.



All of these changes have been added to the Herculien GitHub along with some of my other modifications (including my custom V6/volcano dual carriage). I have also included an alternative break-in gcode file that incorporates arc movements at the center and corners of the bed (see video for a demonstration). I'm eager to hear your thoughts on these mods and look forward to seeing remixes that incorporate similar design elements!



Link to github mod page: [https://github.com/eclsnowman/HercuLien/tree/master/Community%20Mods%20and%20Upgrades/Zane%20Baird](https://github.com/eclsnowman/HercuLien/tree/master/Community%20Mods%20and%20Upgrades/Zane%20Baird)



![images/e9672eff52919c49cb4378b4b410f654.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e9672eff52919c49cb4378b4b410f654.jpeg)
![images/0c5b0b2f448964af558e55ee0e4b0fab.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0c5b0b2f448964af558e55ee0e4b0fab.jpeg)
![images/7785dc6a423b52ea184a2f4223a87136.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7785dc6a423b52ea184a2f4223a87136.jpeg)
![images/b6cc530c47dad92e203a55099d1a3e54.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b6cc530c47dad92e203a55099d1a3e54.jpeg)
![images/9373d4e48294793a45f45ec3795a2279.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9373d4e48294793a45f45ec3795a2279.jpeg)
![images/eb078dbf64cd04a7cdca18b81db1caaf.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eb078dbf64cd04a7cdca18b81db1caaf.jpeg)
![images/98c2ef61fdc576b960df8b09bcc63646.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/98c2ef61fdc576b960df8b09bcc63646.jpeg)
![images/96a7d7eb3947d720a4e8bc0a42b36113.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/96a7d7eb3947d720a4e8bc0a42b36113.jpeg)
![images/077bb380eb87dac4cfda3582af4f9fdc.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/077bb380eb87dac4cfda3582af4f9fdc.gif)
![images/9f219627fa8a7322d03daf4152b742cd.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9f219627fa8a7322d03daf4152b742cd.gif)

**Zane Baird**

---
---
**Eric Lien** *June 19, 2016 22:46*

Just like **+Walter Hsiao**​ did for the Eustathios, you took the printer and converted my blocky utilitarian designs... and made them something more functional... and elegant:)



Great job as always **+Zane Baird**​


---
**James Rivera** *June 19, 2016 22:49*

Nice work! I really like the belt tensioners!


---
**Fred Ling** *July 12, 2016 01:42*

Looks awesome!  I am having problem downloading the STL files though(XY_EZ_belt_tensioner_A.STL and XY_EZ_belt_tensioner_B.STL).  Can you double check?  I tried from two computers.  Thanks,


---
**Zane Baird** *July 12, 2016 13:58*

**+Fred Ling** I see what you mean. Not sure what happened here. I'm currently at work and don't have access to my google drive, but I will message you later with a link to the files that you can download. Sorry about the trouble with that.


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/BXQcfsyYkX6) &mdash; content and formatting may not be reliable*
