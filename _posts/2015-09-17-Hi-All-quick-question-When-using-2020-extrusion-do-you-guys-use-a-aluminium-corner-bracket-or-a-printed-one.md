---
layout: post
title: "Hi All, quick question. When using 2020 extrusion, do you guys use a aluminium corner bracket or a printed one ?"
date: September 17, 2015 13:04
category: "Discussion"
author: Jean-Francois Couture
---
Hi All, quick question.

When using 2020 extrusion, do you guys use a aluminium corner bracket

or a printed one ? 



Or simply the screw on one end and the head in the groove of the other is plenty strong ?



I'd like to know if the printed one is plenty strong combined with the screw in the groove.



Thanks 





**Jean-Francois Couture**

---
---
**Daniel F** *September 17, 2015 13:08*

Both, screws and aluminium corner brackets. Screw needs less space and is chaper. Printed corners are not rigid enough.


---
**Igor Kolesnik** *September 17, 2015 13:10*

BOM says aluminum, I do not see a reason why you can't use printed. Not using brackets will work too but overkill is always better then underkill


---
**Igor Kolesnik** *September 17, 2015 13:13*

PLA in my humble experience should OK for that but go with a nice thick model. 


---
**Daniel F** *September 17, 2015 13:15*

Leverage is too big for plastic brackets, frame will deform when you accelerate the  gantry.


---
**Eirikur Sigbjörnsson** *September 17, 2015 13:51*

Then there is the question about the hidden corner brackets from OpenBuilds. I have few of those and they seem very sturdy



[http://openbuildspartstore.com/inside-hidden-corner-bracket/](http://openbuildspartstore.com/inside-hidden-corner-bracket/)


---
**Daniel F** *September 17, 2015 14:14*

One more, the video explains how it works: [http://www.motedis.ch/shop/Nutprofil-Zubehoer/Zubehoer-30-I-Typ-Nut-6/Automatik-Verbinder-I-Typ-Nut-6::999991311.html](http://www.motedis.ch/shop/Nutprofil-Zubehoer/Zubehoer-30-I-Typ-Nut-6/Automatik-Verbinder-I-Typ-Nut-6::999991311.html)


---
**Erik Scott** *September 17, 2015 14:29*

On my first printer, I skipped the brackets and just did the end screws. On my current build, I went all the way and did both. I haven't run the new one yet (hopefully this weekend!), so I don't have much to compare, but my first printer seems sturdy enough. 



You could do printed brackets, and I would recommend PLA I'd you do, but I would not use them alone; do the end screws as well. 


---
**Eric Lien** *September 17, 2015 14:35*

I use both. End screw are great to pull corners together tight. Corner bracket then add more contact area for gusseting. Tighten the end screws first, then the corner brackets. Only downside to end bolt method is extrusion needs to be cut perfectly to length and square. No room for adjusting like just the corner bracket method.


---
**Jean-Francois Couture** *September 17, 2015 15:03*

Thanks guys.. i've placed a order on robodigg and added there aluminum corner brackets.. as this printer will have closed sides, i can't use PLA .. I was thinking of ABS.


---
**Erik Scott** *September 17, 2015 19:31*

If you use aluminum extrusions from 8020, you can just screw into it without tapping. It will take some force and some oil. But it can be done. The 8020 extrusions have 4 channels along the hole to allow material to exit as you screw it in. 


---
**Jean-Francois Couture** *September 17, 2015 19:48*

Whould the bigbox store's 3/4"x3/4" 90 deg brackets used for shelfing be something workable with ?



something like this: [http://store.workshopsupply.com/catalogue/images/4311-A1-0115.jpg](http://store.workshopsupply.com/catalogue/images/4311-A1-0115.jpg)


---
**Jean-Francois Couture** *September 17, 2015 19:48*

**+Erik Scott** I'll be using 2020 all the way for this project. 


---
**Erik Scott** *September 17, 2015 20:02*

**+Jean-Francois Couture**​ when I said 8020, I meant the brand 8020. They sell extrusions in a number of sizes. You can buy 20mmx20mm extrusions from them, as I did for my first printer. 


---
*Imported from [Google+](https://plus.google.com/105576148076542448710/posts/cfKnhBw2URX) &mdash; content and formatting may not be reliable*
