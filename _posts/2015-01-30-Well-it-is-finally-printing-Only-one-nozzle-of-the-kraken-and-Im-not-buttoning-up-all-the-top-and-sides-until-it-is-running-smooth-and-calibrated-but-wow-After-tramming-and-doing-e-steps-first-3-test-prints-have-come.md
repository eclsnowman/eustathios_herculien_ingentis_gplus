---
layout: post
title: "Well it is finally printing! Only one nozzle of the kraken, and I'm not buttoning up all the top and sides until it is running smooth and calibrated, but wow - After tramming and doing e-steps, first 3 test prints have come"
date: January 30, 2015 02:55
category: "Show and Tell"
author: Tony White
---
Well it is finally printing! Only one nozzle of the kraken, and I'm not buttoning up all the top and sides until it is running smooth and calibrated, but wow - 

After tramming and doing e-steps, first 3 test prints have come out amazingly. I've never had prints look this good from the get-go - great machine! I'll post files on pyramid top soon



![images/1e21a9e8ed27182f9a82f8a21114e388.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1e21a9e8ed27182f9a82f8a21114e388.jpeg)
![images/182715d1fd6e91a6589ed85a31ab4248.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/182715d1fd6e91a6589ed85a31ab4248.jpeg)
![images/5f04493f4a417de8340ea0faad90acdd.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5f04493f4a417de8340ea0faad90acdd.jpeg)
![images/b15f2e4c3f2fc8fdb775d3ee7181763b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b15f2e4c3f2fc8fdb775d3ee7181763b.jpeg)

**Tony White**

---
---
**Daniel Salinas** *January 30, 2015 03:08*

Congrats Anthony!  Looks awesome.  Videos?


---
**Eric Lien** *January 30, 2015 03:50*

Looks like my office ;)



I know what you mean about the prints. Eustathios and the quality it had was what convinced me to turn my corexy into HercuLien. My first print on Eustathios was better than anything I had produced on my corexy up till then. And with tuning it got even better.


---
**Mike Miller** *January 30, 2015 04:40*

Preach it! Going from my Delta to this was a helluva drug. 


---
**Isaac Arciaga** *January 30, 2015 23:40*

**+Anthony White** what thickness is the acrylic platform you have down in the bottom?


---
**Tony White** *January 31, 2015 06:37*

I used 1/4" for everything - top, sides, and bottom.


---
**Isaac Arciaga** *February 01, 2015 03:55*

**+Anthony White** Thanks. Do you happen to know where I can source the aluminum heat spreader and have it cut locally? Which thickness did you go with and which heater did you use? I noticed you're in LA. I'm in the OC area.


---
**Tony White** *February 01, 2015 16:37*

I ordered the heat spreader cut from a local shop, wasn't cheap. Maybe someone in the cnc google+ groups can help you there. I used 24Vheater from Aliexpress


---
*Imported from [Google+](https://plus.google.com/+AnthonyWhiteMechE/posts/35wfkSnq1zE) &mdash; content and formatting may not be reliable*
