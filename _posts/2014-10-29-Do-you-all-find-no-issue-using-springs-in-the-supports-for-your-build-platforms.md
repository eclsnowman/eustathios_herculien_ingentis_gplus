---
layout: post
title: "Do you all find no issue using springs in the supports for your build platforms?"
date: October 29, 2014 14:28
category: "Discussion"
author: Jim Wilson
---
Do you all find no issue using springs in the supports for your build platforms?



I had a decent quality and reliability improvement after removing them from my Prusa I2, but I think the issue came from the springs bucking around at higher speeds since they were moving laterally with the Y carriage. Perhaps this doesn't affect things at all when the build platform only moves slowly and rarely (compared to X and Y) in Z?





**Jim Wilson**

---
---
**Whosa whatsis** *October 29, 2014 16:54*

The springs should be on screws that are secured to the platform so that they stay straight (with a nut tight against the bottom of the platform). When people forget that step, the platform wobbles.


---
**Jim Wilson** *October 29, 2014 17:10*

I think that's what my original problem was, it was bolt-washer-platform-washer-spring-nut instead of bolt-washer-platform-washer-nut-washer-spring-nut


---
**Eric Lien** *October 29, 2014 19:36*

It should be no issue anyway on a vertical z bed like ingentis/eustathios unless the nozzle is dragging.


---
**Whosa whatsis** *October 29, 2014 21:55*

**+Eric Lien** Or unless the machine is shaking or otherwise moving around, but your point is correct that this <i>should</i> be less of an issue on that type of machine than, one that moves the platform on the X and/or Y axis, such as the various Mendel variants.


---
*Imported from [Google+](https://plus.google.com/101778058628996936791/posts/iV2NSLQvsXQ) &mdash; content and formatting may not be reliable*
