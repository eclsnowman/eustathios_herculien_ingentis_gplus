---
layout: post
title: "Fan duct, Eric Lien , or Duckface?"
date: November 30, 2014 13:24
category: "Discussion"
author: Mike Miller
---
Fan duct, **+Eric Lien**, or Duckface? 

![images/0cae86dfd055267c37b9d159f7430b44.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/0cae86dfd055267c37b9d159f7430b44.jpeg)



**Mike Miller**

---
---
**Eric Lien** *November 30, 2014 14:24*

Ha.


---
**Eric Lien** *November 30, 2014 14:32*

I prefer the centrifugal fan version now. More flow, less duckface.


---
**Mike Miller** *November 30, 2014 14:39*

That sounds like a marketing slogan: "More Flow, Less Duckface: Herculien!"


---
**Eric Lien** *November 30, 2014 16:53*

**+Shauki Bagdadi** I couldn't have done it without your suggestions on avoiding splines.


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/XzDkwGzXWSy) &mdash; content and formatting may not be reliable*
