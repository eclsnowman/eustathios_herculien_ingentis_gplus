---
layout: post
title: "\"machined\" some pulleys from robotdigg so I can use the Eustathios V1 setup with the steppers in the base"
date: April 26, 2015 22:56
category: "Show and Tell"
author: Daniel F
---
"machined" some pulleys from robotdigg so I can use the Eustathios V1 setup with the steppers in the base. Put the pulleys on an 10mm aluminium shaft, fixed it with the grub screws in the collar and put it in the vice. Just used a metal saw to cut off the collar. drilled a hole and hand tapped an M3 thread. Next step is to design new stepper holders that work with 976mm belts from robotdigg.



![images/53059b5fe813b3bd78064b64e31f10ba.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/53059b5fe813b3bd78064b64e31f10ba.jpeg)
![images/3e29788f73270938996be3e6469a2143.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/3e29788f73270938996be3e6469a2143.jpeg)

**Daniel F**

---
---
**Eric Lien** *April 26, 2015 23:17*

Nice mod. :)


---
*Imported from [Google+](https://plus.google.com/111479474271942341508/posts/i8sNQfTRqmk) &mdash; content and formatting may not be reliable*
