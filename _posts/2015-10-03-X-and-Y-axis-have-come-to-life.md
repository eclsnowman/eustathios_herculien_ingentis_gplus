---
layout: post
title: "X and Y axis have come to life"
date: October 03, 2015 16:17
category: "Show and Tell"
author: Roland Barenbrug
---
X and Y axis have come to life. Speed and acceleration kept low to avoid stalling stepper motors. So far so good. Waiting for some fundamental parts (e.g. ball screw z-axis, extruder stepper, hot end) before I can continue. 1/32 micro steps seems to small (insufficient torgue), 1/16 doing fine so far in combination with 0.9 degree stepper motors. If my calculations are correct, the setting is 160 steps per mm (0.9 degree, 16 micro steps, 20 teeth / 32 teeth pullies).


**Video content missing for image https://lh3.googleusercontent.com/-Arouk8ekZZc/Vg_-s8RJpdI/AAAAAAAAALE/1N1ffEyft44/s0/VIDEO0042.mp4.gif**
![images/c888e86a905dc7920a506aa61e459175.gif](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c888e86a905dc7920a506aa61e459175.gif)



**Roland Barenbrug**

---


---
*Imported from [Google+](https://plus.google.com/118296832015849309457/posts/EG9XGX3PeZN) &mdash; content and formatting may not be reliable*
