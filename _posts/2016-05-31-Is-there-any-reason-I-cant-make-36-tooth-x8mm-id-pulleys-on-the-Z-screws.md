---
layout: post
title: "Is there any reason I can't make 36 tooth x8mm id pulleys on the Z screws?"
date: May 31, 2016 05:05
category: "Discussion"
author: jerryflyguy
---
Is there any reason I can't make 36 tooth x8mm id pulleys on the Z screws? (BOM calls for 30 tooth) I'm going to use 1204 ballscrews instead of leadscrews. 





**jerryflyguy**

---
---
**Mike Miller** *May 31, 2016 12:16*

Not at all...the hard part comes when setting the e-steps for that axis. I used something like this: [http://littlemachineshop.com/products/product_view.php?ProductID=1593&category=](http://littlemachineshop.com/products/product_view.php?ProductID=1593&category=)



(There's some hokey math conversions...you want to end up with e-steps in metric, but the indicator uses inches....unless, you know, you get a metric one to begin with.)


---
**jerryflyguy** *May 31, 2016 14:13*

**+Mike Miller** yeah, I assume it's easy to adjust the steps/mm or steps/inch. I did this exercise when I built my mill (Mach3) but ya never know, there could a reason not to do it! 


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/QrT13A8L1v3) &mdash; content and formatting may not be reliable*
