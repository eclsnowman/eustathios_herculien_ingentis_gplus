---
layout: post
title: "Up until this point I have been using a 1kW 120VAC silicone heating pad for my Herculien with 2 thermistors as specified in the BOM"
date: January 17, 2017 04:05
category: "Discussion"
author: Zane Baird
---
Up until this point I have been using a 1kW 120VAC silicone heating pad for my Herculien with 2 thermistors as specified in the BOM. I recently decided to replace the aluminum heat spreader with an actual flat plate and noticed some burns on my heater. Since I didn't want to continue with a potential fire hazard, I decided drew up some new heater specification with some input from **+Eric Lien**. 



This heater uses the same cutout idea that I believe **+Frank Helmschrott** used in his eustatious heater (cutout for thermistor). I drilled a small divot into my aluminum heat spreader and taped a thermistor in with some thermal compound for good heat conduction. 



I’m getting much more realistic heating curves now that mimic what I would expect from such a larger thermal mass. Didn’t bother try PID autotune  on this one but took **+Eric Lien**'s advice and tuned manually keeping this graph in mind: ([https://en.wikipedia.org/wiki/PID_controller#/media/File:PID_Compensation_Animated.gif](https://en.wikipedia.org/wiki/PID_controller#/media/File:PID_Compensation_Animated.gif))



It took a lot of time and patience (an entire evening) to arrive on the correct PID settings for smoothieware, but my final values were P: 120, I:1.2, D14. With these values I’m getting heating curves as shown in the attached image. As a note, my bed PID temperature_control.module_name.pwm_frequency) was set to 50 Hz which is slightly below the max switching frequency of the SSR I am using.





![images/eb346919019750dbfd538dff1cbdff76.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/eb346919019750dbfd538dff1cbdff76.jpeg)
![images/8784cd35ecac70755f610bf929e36e46.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8784cd35ecac70755f610bf929e36e46.png)
![images/a2a033ac2c3c623477ca02b8631eb7f4.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a2a033ac2c3c623477ca02b8631eb7f4.png)

**Zane Baird**

---
---
**Frank “Helmi” Helmschrott** *January 17, 2017 06:08*

Good to hear that it works for you too. I'm also much more satisfied with this solution.



Regarding PID I did switch it of for the heated bed. As the thermal mass is that big it's not really needed to have this exact kind of temp regulation. I'm running with a simple bang bang control (Repetier, don't know how this works on Smoothieware) which works great and doesn't really need any big tuning.



"It just works" :) 


---
**Zane Baird** *January 17, 2017 14:49*

**+Frank Helmschrott** What is your hysteresis setting for the bang bang control? I was going for a little variation as possible which is why I ultimately went with PID over bang-bang. Fluctuations of even 0.5 degrees were showing up as Z-banding


---
**Frank “Helmi” Helmschrott** *January 17, 2017 14:59*

To be honest: I don't know. I haven't heard of such Z-Banding problems due to 0,5°C Variations on the heated bed and I wonder how this could happen except from when the heated bed temperature is too high. But if you're successful with PID it's probably to keep it.


---
**Zane Baird** *January 17, 2017 15:07*

I was surprised myself. I only observe it at ABS temps and the thickness/size of the plate on the herculien probably accentuates the issue.


---
**Eric Lien** *January 19, 2017 02:07*

I was very surprised back in the day to see the difference between PID and Bang-bang, see my post from a long while back: 



[https://plus.google.com/+EricLiensMind/posts/L7DX4weDS2Z](https://plus.google.com/+EricLiensMind/posts/L7DX4weDS2Z)




---
**Jeff DeMaagd** *January 19, 2017 03:08*

I could see the difference in thermostat controlled ambient air so, PID air heating it became.



If you don't get a problem, then more power to you. I've got a pretty extreme setup and somehow autotune worked. The most hand-holding I gave the bed autotune was preheat it to near autotune temp before starting it.


---
**Frank “Helmi” Helmschrott** *January 19, 2017 06:24*

**+Eric Lien** wow that's interesting. I wouldn't thinkt that this makes quite a difference. I have seen Z-Banding on my printer before I needed to replace the whole carriage and will check when rebuilding it if it may have to with that. Although I haven't only seen that with ABS but also with PLA and I would guess my temp differences on the bed are quite low. But definitely good to know and keep an eye on.


---
*Imported from [Google+](https://plus.google.com/115824832953735584348/posts/8up6sFMedmi) &mdash; content and formatting may not be reliable*
