---
layout: post
title: "Random question: I think I may have stripped some grub screws in my GT2 20 tooth 8mm bore pulleys, and I don't have them in front of me to check"
date: March 17, 2016 20:49
category: "Discussion"
author: James Rivera
---
Random question: I think I may have stripped some grub screws in my GT2 20 tooth 8mm bore pulleys, and I don't have them in front of me to check. Does anybody know what size grub screws they typically are so I can order some more? I'm posting here because I'm betting you guys are likely to know. :)





**James Rivera**

---
---
**Samer Najia** *March 17, 2016 20:57*

I seem to recall M1.5 or M2.5


---
**Tomek Brzezinski** *March 17, 2016 21:01*

It actually does depend, they are not standardized. There are definitively two different sizes between my several GT2 pulleys, purchased at different times and from different places.  



probably metric though. 



Did you strip the hex hole or the threads? If you stripped the threads you probably stripped the aluminum threads and then you need to drill out a tiny amount and tap with a larger set screw. 



Don't forget if have a flat then you can use a regular screw. If you don't have a flat, it might not be wise to be transferring torque with that pulley (wow I just realized my solidoodle does this), but if you're not transferring torque than a regular screw will be fine again. 


---
**James Rivera** *March 17, 2016 21:10*

**+Tomek Brzezinski** It feels like I have stripped (or will strip soon) the interior of the hex hole. But, <b>you totally made the best suggestion</b>: <i>just use regular screws!</i>  I would rather use regular metric screws anyway, because If I strip the hex hole again, I can still use pliers to pull it out and put a new one in. I think I have a few small M2 or M2.5 screws, so maybe I don't need to order anything at all.  Thanks!

Aside: Sometimes, when shown a simple solution to a problem, I feel I has the dumb. :-/


---
**Thomas Sanladerer** *March 17, 2016 21:29*

They are typically M3 grubs, which use a 1.5mm hex key.

Some newer pulleys also use M4 with a 2mm hex key.


---
**Eric Lien** *March 17, 2016 21:29*

**+James Rivera** if I get ones that have small grub screws I often drill and tap them to a larger size anyway to give them a little more bite so I don't deal with this in the future


---
**James Rivera** *March 17, 2016 21:44*

**+Thomas Sanladerer** I know I have several M3x7mm hex cap screws lying around.  I'll give those a try. Thanks!


---
**James Rivera** *March 17, 2016 21:45*

**+Eric Lien** I will keep that idea in mind, for sure!


---
**Mutley3D** *March 17, 2016 22:22*

the only fastener I have seen in GT2 pulleys are M3 grub screws aka set screws. Personally I wouldnt use ordinary screws but if they work then great.



IF you think you are going to strip the internal hex key socket, you could use a very small torx bit in one of those multibit screwdrivers,, just fractionally larger than the hex socket, tap it in gently and carefully, and then with great care  undo it, and throw the set screw in the bin once you get it out. You may sacrifice the bit in doing this. Also offer 2 live chickens to a god of your choice.



A decent quality hex key set i worth the investment, as are all quality tools, and will help you avoid this situation again in the future.



Oh the days of a tool chest full of Snap-On, Hazet, Facom and Stahlwillie :)


---
**James Rivera** *March 17, 2016 22:43*

LOL @ "Also offer 2 live chickens to a god of your choice." :)


---
**Brandon Satterfield** *March 18, 2016 16:16*

Yes sir, should be M3 grub. Take it easy on them screws brother your simply too strong..:-)


---
**James Rivera** *March 19, 2016 17:17*

**+Brandon Satterfield** I'd like to, but I've been troubleshooting a bearing stiction problem that has been causing missed steps, so I wanted to torque them down pretty tight. Side note: they are definitely M3x3mm grub screws, and the larger normal hex cap screws are not an option because the cap interferes with the other pulley. Oh well. I just ordered this assortment of grub screws from amazon (they don't seem to have any on TrimCraftAviationRC.com):

[http://www.amazon.com/gp/product/B014OMFU1I](http://www.amazon.com/gp/product/B014OMFU1I)


---
**James Rivera** *March 19, 2016 17:36*

There are also some larger ones (M3/4/5/6/8), so if it is still a problem then I may drill the pulley holes out and tap them to a larger thread. Fool me once, shame on you; fool me twice, shame on me. :)


---
**Chris Brent** *March 21, 2016 03:47*

Maybe grind a flat on the stepper shaft if you're going to the trouble of making the grub screw bigger?


---
**James Rivera** *March 21, 2016 04:29*

**+Chris Brent** that's not a bad idea. Not sure I'm at that point yet, though.


---
**Makeralot** *March 24, 2016 08:12*

M3 grubs


---
*Imported from [Google+](https://plus.google.com/+JamesRivera1/posts/NouGGK9Ex1L) &mdash; content and formatting may not be reliable*
