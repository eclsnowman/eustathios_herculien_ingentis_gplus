---
layout: post
title: "So looking to source parts and I'm trying to determine if the parts from RobotDigg are the same as Misumi ( )"
date: December 06, 2014 04:29
category: "Discussion"
author: Rick Sollie
---
So looking to source parts and I'm trying to determine if  the parts from RobotDigg [http://www.robotdigg.com/product/222/GT2-Pulley-32-Teeth-8mm-Bore](http://www.robotdigg.com/product/222/GT2-Pulley-32-Teeth-8mm-Bore)  are the same as Misumi ([http://us.misumi-ec.com/vona2/result/?Keyword=GPA32GT2060-A-P8](http://us.misumi-ec.com/vona2/result/?Keyword=GPA32GT2060-A-P8)).





**Rick Sollie**

---
---
**James Rivera** *December 06, 2014 06:49*

Same: Teeth, bore, pitch, shaft diameter, belt width, GT2. Those should be interchangeable.  The only difference I can see is the RB version appears to only have 1 set screw (judging from the picture), while the Misumi version appears to have 2. But this should have little impact if you have a flat on your motor shaft. But if it doesn't have a flat side, I highly recommend making one, or else definitely get the 2 set screw version.


---
**Florian Schütte** *December 06, 2014 08:59*

The big difference is, that the one from misumi has no collar ("A"-Version, See schematic drawings at misumi). But this does  only really matter when we talk about 32T 10mm  pulleys. For the 32T 8mm ones the robotdigg pulleys are ok i think. I plan To use them.


---
**James Rivera** *December 06, 2014 19:45*

I didn't see that. The collar might change the spacing enough to cause problems in the design. I've heard many of the Ingentis community folks speak highly of the RobotDigg parts, so that may be a good choice.  Good luck!


---
**Jarred Baines** *December 07, 2014 15:37*

I'm using robotdigg ones, although my design is customised from the stock ingentis / eustathios a bit... They do come with 2 set screw holes (at least mine did) and the collar as already mentioned.


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/iRTnvSjn5EJ) &mdash; content and formatting may not be reliable*
