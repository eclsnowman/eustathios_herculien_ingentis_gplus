---
layout: post
title: "Dialing in on to find the optimal printing settings"
date: September 06, 2014 16:50
category: "Show and Tell"
author: Eric Lien
---
Dialing in on #HercuLien to find the optimal printing settings. Batman was printed in ABS at 60mm/s, Octopus was in PLA at  70mm/s but extrusion was off slightly, and the green lifting bracket foundry pattern was for work and printed in PLA at 40mm/s. The bracket pattern mounts to a molding plate and becomes the mold positive for greensand ductile iron casting. The feed riser was printed integral to the part. The pattern is about 11" across, and after some priming, sanding, and high build pattern paint the impression was glass smooth.



I will share the cast & machined part pictures once it is complete.



![images/984e0c9f8ce5f739b7f7f813d6ad2343.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/984e0c9f8ce5f739b7f7f813d6ad2343.jpeg)
![images/e3cea993eca3701c73e8cda02f711fd1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e3cea993eca3701c73e8cda02f711fd1.jpeg)
![images/9e39eea88ab40a9fd371872071bbcc5b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/9e39eea88ab40a9fd371872071bbcc5b.jpeg)
![images/87642237a58de4101fade331d5e7aa65.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/87642237a58de4101fade331d5e7aa65.jpeg)
![images/78e7616415a3080ce05373b28a7d621d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/78e7616415a3080ce05373b28a7d621d.jpeg)
![images/1afbe0563b534bd987c79a1b66443c7f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1afbe0563b534bd987c79a1b66443c7f.jpeg)
![images/30ac63af159acb9c66052d10e855d438.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/30ac63af159acb9c66052d10e855d438.jpeg)
![images/c73a47ed6bf50e6a6a9c62112ad1f379.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c73a47ed6bf50e6a6a9c62112ad1f379.jpeg)
![images/65bcea7da6ab9036cd1d63b123587976.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/65bcea7da6ab9036cd1d63b123587976.jpeg)

**Eric Lien**

---
---
**D Rob** *September 06, 2014 19:48*

Awesome. I'm finally back home. (Stranded 2 weeks 100 miles from home when I blew the heads on my new truck) Still don't have a truck though. I think I'll go ahead and finish my "top secret" build.


---
**Eric Lien** *September 06, 2014 19:50*

**+D Rob** cannot wait to see it.


---
**Jean-Francois Couture** *September 07, 2014 03:03*

**+Eric Lien** what are you using to make the ABS stick to build platform?  i'm using slurry but its not giving good results.. i'm testing with kapton tape @ H:240C and B:95C . I had it at 110C but the slurry was laterally cooking on the glass.. Hope to make a mense with ABS.. looks like your having very good success (batman is look great.. and also #hercuLien).. got any pointers ? 


---
**Eric Lien** *September 07, 2014 03:09*

**+Jean-Francois Couture** recently switched back to kapton on glass, then sprayed with extreme hold strength hair spray (from the dollar store). Then the build plate at 110C.



I had been brushing on 50/50 water mixed with Elmer's craft bond strength PVA glue. It's odd. Sometimes I could barely get it off. Other times it popped off mid print. 



Last recommendation is use a very thick first layer. I go .3mm and set a wide extrusion width.



I﻿


---
**Jean-Francois Couture** *September 07, 2014 03:24*

**+Eric Lien** So, you spray hair spray on top of the Kapton ?



I wonder if woodglue PVA would do.. beeing a weekend woodworker, thats something I have alot in the shop ! LOL



Donc you have trouble with hair spray at 110C ?

 One last thing, do you use a brim ? 



I've got one on right now (5 times around) and still.. a side just poped off


---
**Eric Lien** *September 07, 2014 04:21*

**+Jean-Francois Couture** hairspray with the plate cold & off the machine (it with damage your rods & bearings with over spray).



Wood glue PVA would work fine. Brush it on at 50c. Let's it dry. Then crank it up.



I use brim on most abs. Otherwise I build corner disks into the models for tack points. Corners are your enemy in abs. High surface area to mass. They are the first to cool and start the contraction... Hence corner lift.


---
**Dan Kirkpatrick** *October 07, 2014 12:30*

I've tried everything, but recently started using "blustick", from Blu Tack/Bostik.  Blustick on kapton has too much adhesion--had to chisel parts off the build plate.  Blustick on glass is perfect--great hold while the bed is heated to 110, yet cracks off without external assistance when the build plate cools to ~30.  Afterwords, the glue residue cleans up easily, as it is water soluble.  Great stuff, and it works with nylon and PLA, too.  It's PVP, not PVA glue.


---
**Dan Kirkpatrick** *October 07, 2014 12:36*

Eric, any chance you can share how you fine tune your extrusion temps, print speeds, etc?  I can't get ABS overhangs to look nearly so neat unless I use a layer cooling fan, and with ABS that causes layer adhesion problems.  I also get some layer curling on overhangs as it is printing, causing roughness to the overhang once upper layers are printed.  Makes me think I've got temps wrong, but advice I see tells me conflicting things--too hot/too cold, too slow, too fast, etc etc.


---
**Eric Lien** *October 07, 2014 13:45*

Speeds and temps are the key. I ran this print at 239c, and sliced with simplify3d. It does a good job of slowing down on small features and overhangs. This one was printed at 60mm/s but I set the under speed allowance to 20%. So the small features can run as slow as 12mm/s which allows for cooling between layers on small features. Also the e3d has a more flattened nozzle bottom profile which helps on overhangs.


---
**Jean-Francois Couture** *October 07, 2014 13:48*

**+Eric Lien** I'm hearing alot about simplify3D is it any good ? does the cost justify (no pawn intended) over other slicers like Cura and Slic3r ?


---
**Eric Lien** *October 07, 2014 14:49*

**+Jean-Francois Couture** I have been able to print files that both Cura and slicer failed me at. I will admit I like the look of Cura's paths when watching the movements. But I like the surface finish and support structure of simplify3d far more.



Is it worth it... That is more a personal question. You can accomplish many of simplify3d features with hacks (multiple sliced files hacked together for print settings by zone). Create support material into the models. Etc. But for me, yes it is worth the hastle savings and results 


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/bPptLvqrctR) &mdash; content and formatting may not be reliable*
