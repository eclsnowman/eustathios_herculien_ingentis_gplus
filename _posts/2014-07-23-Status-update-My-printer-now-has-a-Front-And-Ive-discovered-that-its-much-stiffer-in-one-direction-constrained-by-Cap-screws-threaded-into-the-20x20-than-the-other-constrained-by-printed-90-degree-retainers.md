---
layout: post
title: "Status update: My printer now has a 'Front' And I've discovered that it's much stiffer in one direction (constrained by Cap screws threaded into the 20x20) than the other (constrained by printed 90 degree retainers)"
date: July 23, 2014 12:38
category: "Discussion"
author: Mike Miller
---
Status update: My printer now has a 'Front'



And I've discovered that it's much stiffer in one direction (constrained by Cap screws threaded into the 20x20) than the other (constrained by printed 90 degree retainers). So when I get the dimensions sorted, I'll be drilling and tapping those cross pieces.



There are some interesting take-aways to using the **+igus Inc.**  parts in this design. The shorter bushings are slightly larger than the diameter of the rod they slide on. The bushing ID is 10.12mm, and the rods are 9.97mm. Whether this is for thermal expansion or to adjust for slight mis-alignment, I don't know. But when things are dialed in, they're great. 



Where this dimensional change is apparent is in places where only a single conventional bushing might be adequate, these bushings really need a little more to work with. In the photo of the gantry, the axis that has two bushings for support, there is no undesirable movement. In the other direction, there is an unacceptable amount of play that would translate to the print head. I'll be printing a replacement that will utilize four bushings instead of the three here. 



**+igusInc** Also sells drop-in replacement linear bushings that have an Aluminum shell to make the bushing the same dimensions as a conventional linear bearing. There is <i>no</i> play in this part and movement is exceptional. I ordered a part with an ID of 10mm and an OD of 19mm....the ingentis parts are expecting an OD of 20mm, so I'll need to 'warp to fit'. 



![images/b3e28354a38bf8a91f8eeada5226e42e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b3e28354a38bf8a91f8eeada5226e42e.jpeg)
![images/df6414209bd992aece2f7ca8879f5a38.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/df6414209bd992aece2f7ca8879f5a38.jpeg)
![images/1fd766e5942e69a0a63616b6daa28e1f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1fd766e5942e69a0a63616b6daa28e1f.jpeg)

**Mike Miller**

---
---
**Mike Miller** *July 23, 2014 13:28*

The carriage in that axis is a point load, spreading it out across two bearings should handle the problem nicely. #knockswood


---
**igus Inc.** *July 23, 2014 13:33*

Very nice!!


---
**Joe Spanier** *July 23, 2014 14:47*

that "oversize" is because the bearings are meant to be pressed in. Once pressed to the appropriate diameter they will be the correct ID.


---
**Eric Lien** *July 23, 2014 15:13*

One thing I want to change on the Eustathios is the lack of Z rod position adjustment in X. Currently it is controlled by the tolerance of the main frame to the z rod mount, the rod mount to the rod, the rod to the bearing, the bearing to the bed bearing mount, the bearing mount to the bed extrusions... And all that again on the other side back to the other z rod. That a LOT of stack tolerance for our printers accuracy. If it's off you will get binding in Z. 



I haven't drawn anything up yet. But I have some ideas.﻿


---
**Mike Miller** *July 23, 2014 15:37*

Yeah, right now the printer LOOKS square, but I'm constantly pulling corners apart to add components and the like. I'm going to have to re-wrap the teflon tape around the bushings and go over the whole printer and loosen everything up and make it all square. I also need to sort out a solution for the fishing line pulley system in Z...which is funny, that always seems to be something folks leave til after XY is working. :P


---
**Mike Miller** *July 23, 2014 21:21*

**+Joe Spanier** I may end up making aluminum sleeves then...that might also remove the play in the one axis photographed above. 


---
**Joe Spanier** *July 23, 2014 21:28*

Most of them you can get drill bits at the size you need. I've had good luck printing parts slightly oversize and reaming them with a drill bit. 


---
**Eric Lien** *July 23, 2014 21:58*

**+Mike Miller** why not just reprint the carriage with the right sized holes?


---
**Mike Miller** *July 23, 2014 22:04*

I'm not sure the part would take the pressure necessary to press-fit deform the bushing to final dimensions


---
**Joe Spanier** *July 23, 2014 22:16*

They don't take much force at all. If your parts have appropriate layer fusion and the holes are sized you won't have any issues. 


---
**Eric Lien** *July 23, 2014 22:16*

**+Mike Miller** is the bushing designed that pressure makes it the final dimension? Curious. I just usually soften the hole with acetone, push in the bushing (bronze in my case), then let it set up. Nice thing with that method is you can quickly insert a rod, and spin in a drill. That ensures proper alignment bushing to bushing concentric down the rod.


---
**Øystein Krog** *July 24, 2014 09:19*

Looks really nice, are these Misumi extrusions and smooth rods?

I could stare at the finish on those smooth rods for a while:P


---
**Mike Miller** *July 24, 2014 11:06*

**+MISUMI USA** extrusions from their First150 deal, the sliding parts, including rods, are **+igusInc**


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/GNVr2TPcQLb) &mdash; content and formatting may not be reliable*
