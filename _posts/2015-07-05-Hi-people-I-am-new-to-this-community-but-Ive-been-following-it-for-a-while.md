---
layout: post
title: "Hi people. I am new to this community but I've been following it for a while"
date: July 05, 2015 19:53
category: "Show and Tell"
author: Igor Kolesnik
---
Hi people. I am new to this community but I've been following it for a while. I am getting to the first print stage so I decided to share what I've built so far.

![images/fc5386d313e910df521aa445e5523f5d.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/fc5386d313e910df521aa445e5523f5d.jpeg)



**Igor Kolesnik**

---
---
**Eric Lien** *July 05, 2015 21:22*

Looking good.


---
**Chris Purola (Chorca)** *July 06, 2015 05:26*

Looking awesome! Nice looking t-slot and I like the way you used angle aluminum to utilize those linear bearings for the cantilevered Z!


---
**Igor Kolesnik** *July 06, 2015 05:36*

My hands are itching to work on the project but damn job... Will be able to do something only on Thursday. After waiting for almost 5 month for parts to be delivered.﻿ Thanks for nice words, I really appreciate any suggestions and remarks on what I can be doing wrong﻿


---
*Imported from [Google+](https://plus.google.com/+IgorKolesnik/posts/381jeMJjjJr) &mdash; content and formatting may not be reliable*
