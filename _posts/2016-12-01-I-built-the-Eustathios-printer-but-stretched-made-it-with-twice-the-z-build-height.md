---
layout: post
title: "I built the Eustathios printer, but stretched made it with twice the z build height"
date: December 01, 2016 19:12
category: "Discussion"
author: Rick Sollie
---
I built the Eustathios printer, but stretched made it with twice the z build height.   I'm now getting z wobble and was thinking of converting to using v slot like the HercuLien.  Has anyone done this before, any advice on converting?





**Rick Sollie**

---
---
**Eric Lien** *December 01, 2016 19:34*

I really like V-slot / V-Wheel combo as the guides on Z. Another option would be ground linear guides instead of shafts or v-slot (see the image below as an example on a project I have been working on). The price of these ground guides and blocks is a lot lower now a days, and they are more compact. That being said the v-slot Z-stage on HercuLien has always been unbelievably repeatable for me. So you can't go wrong.





![images/bfef810022c4fef904236e8f273d530b.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bfef810022c4fef904236e8f273d530b.png)


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 20:12*

**+Eric Lien** that looks familiar :)


---
**Rick Sollie** *December 01, 2016 20:50*

**+Eric Lien** I have two of those in the garage but they are horizontal only.  Do you get any deflection in your valor?

 


---
**Ray Kholodovsky (Cohesion3D)** *December 01, 2016 20:51*

For those unfamiliar with apple's autodecorrect, valor = vslot :)


---
**Rick Sollie** *December 01, 2016 21:15*

**+Ray Kholodovsky** thanks 


---
**Eric Lien** *December 01, 2016 21:28*

**+Rick Sollie** HercuLien has 20x80 as the z-guides. The 80 is in the direction deflection forces would be. If you deflect that... You are doing it wrong :)



But seriously, the wheels being cantilevered on 5mm bolts would be your source of deflection. But if it helps I haven't releveled my HercuLien is a year+ 


---
*Imported from [Google+](https://plus.google.com/117184878828437001711/posts/DPyJ1tF8ZFU) &mdash; content and formatting may not be reliable*
