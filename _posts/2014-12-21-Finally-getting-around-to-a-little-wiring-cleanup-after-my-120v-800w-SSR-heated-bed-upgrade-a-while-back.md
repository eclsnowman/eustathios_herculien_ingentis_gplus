---
layout: post
title: "Finally getting around to a little wiring cleanup after my 120v/800w/SSR heated bed upgrade a while back"
date: December 21, 2014 03:41
category: "Show and Tell"
author: Eric Lien
---
Finally getting around to a little wiring cleanup after my 120v/800w/SSR heated bed upgrade a while back. The printed cable chain worked surprisingly well (a little dab of superlube at the link joints make the motion smooth as butter).



Here is the one I used, but I modeled a custom bottom link to bolt it to the slot using standard m5 slot nuts: [https://www.youmagine.com/designs/cable-guard](https://www.youmagine.com/designs/cable-guard)

![images/421448b365518626e5b74719fe927fee.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/421448b365518626e5b74719fe927fee.jpeg)



**Eric Lien**

---
---
**Jean-Francois Couture** *December 21, 2014 16:29*

I like the chain... Nice work Eric :)


---
**Eric Lien** *December 21, 2014 16:50*

**+Jean-Francois Couture**  thanks. I think I might install cable tray around the base to clean things up further. Cable clutter drives me crazy.


---
**Joe Spanier** *December 21, 2014 19:02*

How long did these take to print? I've been looking at some options for my laser build. Would love to print it and save 100$


---
**Eric Lien** *December 21, 2014 21:13*

**+Joe Spanier** It prints quick. I filled up my Eustathios build plate with them nested very tightly. Takes about 3 hours when I print at high speed.


---
**Joe Spanier** *December 22, 2014 02:40*

Nice. Looks like I found my chain


---
**Jo Miller** *January 18, 2015 13:09*

Hi Eric, 

You designed a 12/2  leadscrew for the Herculien,  but I have a problem finding  it here in Europe, so   would a 10/2 Leadscrew work too ?  (I know a 12/2 is harder to bend.  ..but apart from that ?)


---
**Eric Lien** *January 18, 2015 13:18*

**+Jo Miller** I am sure it would. In fact I used a 8mm fine pitch threaded rod for months before getting the proper lead screws. They made nice prints also, but I had to run lower z speeds due to friction.


---
**Joe Spanier** *January 18, 2015 15:57*

What size SSR are you running for that pad?


---
**Jo Miller** *January 18, 2015 17:08*

**+Eric Lien**  thanks, I´ll go for the 10/2 mm then 


---
**Eric Lien** *January 18, 2015 17:37*

**+Joe Spanier** the model number is in the BOM, but it is a 20a omron (watch out for knock offs). Since it is a 800w 120v heater the amps draw is only a fraction of the 20a capacity.


---
**Marc McDonald** *January 20, 2015 06:59*

**+Eric Lien** Do you have a link to the cable chain with the modified bottom link as it looks good and will clean up the power connection to my #HercuLien heat bed? I looked through the  #herculien  files and could not locate it.


---
**Eric Lien** *January 20, 2015 12:38*

**+Marc McDonald**  I don't have it modeled in solidworks yet. I used freecad to modify an existing design. Here are the files until I get them modeled in solidworks: [https://drive.google.com/folderview?id=0B1rU7sHY9d8qaWhtOUQzRm1vTVk&usp=sharing](https://drive.google.com/folderview?id=0B1rU7sHY9d8qaWhtOUQzRm1vTVk&usp=sharing)


---
**Eric Lien** *January 20, 2015 13:04*

**+Marc McDonald**​​ fyi you have to remove the orange outer silicone installation through the cable chain because it won't fit the one Large cable. But it removes easily.


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/L3FvGyuuPBF) &mdash; content and formatting may not be reliable*
