---
layout: post
title: "Hello, I printed this 6 way PVC connector"
date: June 30, 2016 15:41
category: "Discussion"
author: Brandon Cramer
---
Hello,



I printed this 6 way PVC connector. 4 of the 6 holes work. The top and bottom don't but they are very close. I've had problems with other parts as well. I'm thinking I need to adjust the Z calibration? 



What do I need to adjust? I'm using an Azteeg X5 Mini on my Eustathios Spider V2. 

![images/e0163590f23a9cea6a3da09bbb2baf63.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e0163590f23a9cea6a3da09bbb2baf63.jpeg)



**Brandon Cramer**

---
---
**Martin Bogomolni (MB)** *June 30, 2016 15:54*

[https://www.deltarap.org/printing-undersized-holes/](https://www.deltarap.org/printing-undersized-holes/)


---
**Tomek Brzezinski** *June 30, 2016 16:08*

I believe you are describing vertical tubes that are not circular. First thing to look at is your xy tension and make sure it's balancef


---
**Brandon Cramer** *June 30, 2016 20:15*

I have tightened up the belts. They were a little loose. We will see what happens. 


---
*Imported from [Google+](https://plus.google.com/116995257251567694736/posts/WnMEn1LK9nC) &mdash; content and formatting may not be reliable*
