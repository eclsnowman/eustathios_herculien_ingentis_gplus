---
layout: post
title: "Has anyone ever changed the Z stage 'H' frame to have a second crossbar the same dimensions as the center bar but placed at one end of the 'H'"
date: May 27, 2016 04:25
category: "Discussion"
author: jerryflyguy
---
Has anyone ever changed the Z stage 'H' frame to have a second crossbar the same dimensions as the center bar but placed at one end of the 'H'. Then you could use 3 point leveling instead of 4. I'd put it to the back so two screws are at the front.



Thoughts?





**jerryflyguy**

---
---
**Eric Lien** *May 27, 2016 04:42*

3point works questionable on large printers IMHO. I tried it on both printers over time. Always went back to 4point. Stays rock stable. I never have to relevel. I go months without touching the bed in any way.


---
**jerryflyguy** *May 27, 2016 04:48*

**+Eric Lien** good to know! Thanks Eric ;)


---
**Michaël Memeteau** *May 27, 2016 06:42*

Agree with **+Eric Lien**, I actually tried to have best of both worlds. Check the Black Panther on my posts (QuadRap design). I have a X as the support for the Z-stage and 3 leadscrews. The 2 first in front and the third directly supporting the bed.

The fact is for such a large bed (~420), you need to accommodate for slight deviations. Theory & reality start to diverge... ;)

 


---
**jerryflyguy** *May 27, 2016 12:54*

**+Michaël Memeteau** it makes sense, my thought had been that if the aluminum plate was thin enough and the bed heat-warped then 4 points would help. If the bed was thicker (say 1/4" or more) it may not warp. If it did, I wonder if the joints of the  'H' wouldn't flex/move enough to make the 4 point adjustment  ineffective? This brought me full circle from thinking a 'thicker bed =better' back to a thin bed (1/8") is probably the best way to go.


---
**Eric Lien** *May 27, 2016 23:51*

**+jerryflyguy** 1/4" is fine. I just use the aluminum as a heat spreader, then glass for flatness, then PEI for bed adhesion. But there are other combinations that work too. These are just my preference.


---
**Michaël Memeteau** *May 28, 2016 05:56*

+Eric LieI Which form of PEI are you using?


---
**Eric Lien** *May 28, 2016 13:02*

**+Michaël Memeteau** [https://www.zoro.com/ultem-sheet-stck-24-in-w-24-in-l-0040-in-t-1nrh8/i/G2846611/?q=G2846611](https://www.zoro.com/ultem-sheet-stck-24-in-w-24-in-l-0040-in-t-1nrh8/i/G2846611/?q=G2846611)


---
**Eric Lien** *May 28, 2016 13:43*

And 3m 468mp adhesive to bond the PEI to the glass. Then clip the glass to the bed with ABS printed clips. I like the printed clips because if you use the metal clips and the bed crowns during heating,  regular glass can crack because there is so little give. So the glass bows with the aluminum bed and cracks. This is more of a problem with larger beds (12"+) than smaller 8"x8" beds 


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/E66kdn8A2rR) &mdash; content and formatting may not be reliable*
