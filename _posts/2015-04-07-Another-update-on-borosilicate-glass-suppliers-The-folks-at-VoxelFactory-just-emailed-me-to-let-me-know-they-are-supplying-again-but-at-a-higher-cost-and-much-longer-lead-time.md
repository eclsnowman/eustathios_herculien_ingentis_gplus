---
layout: post
title: "Another update on borosilicate glass suppliers. The folks at VoxelFactory just emailed me to let me know they are supplying again, but at a higher cost and much longer lead time"
date: April 07, 2015 21:09
category: "Discussion"
author: Seth Messer
---
Another update on borosilicate glass suppliers.



The folks at VoxelFactory just emailed me to let me know they are supplying again, but at a higher cost and much longer lead time. Just wanted to pass it along.





**Seth Messer**

---
---
**Dat Chu** *April 07, 2015 21:19*

Thanks Seth for spearheading these.


---
**Seth Messer** *April 07, 2015 23:58*

my pleasure **+Dat Chu** 


---
**Gus Montoya** *April 14, 2015 16:48*

Are you receiving plenty of incoming parts? It must be christmas everyday for you now haha


---
**Seth Messer** *April 14, 2015 19:42*

definitely :) now it's just a pile of parts on my assembly table in the garage lol.


---
*Imported from [Google+](https://plus.google.com/+SethMesser/posts/JZxsTXFMx26) &mdash; content and formatting may not be reliable*
