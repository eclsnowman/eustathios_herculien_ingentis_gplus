---
layout: post
title: "Second Benchy print. Increased the retraction to 1.5mm but didn't have much effect"
date: October 12, 2016 01:30
category: "Show and Tell"
author: jerryflyguy
---
Second Benchy print. Increased the retraction to 1.5mm but didn't have much effect. I'll try a much higher retract next.



This was 0.10mm layers.



I'm getting more of the weird layer stuff just on the one side, incidentally it's the same side and direction of the part as the other one I noted in the video I posted, I think it's a similar type of issue?



![images/7c9f41106acb390a4bbea4ebc37134f1.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7c9f41106acb390a4bbea4ebc37134f1.jpeg)
![images/8223a0b55ba99617cb2cbd6211137d57.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8223a0b55ba99617cb2cbd6211137d57.jpeg)
![images/37b11366a3381600d6a8cedbe17c9a77.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/37b11366a3381600d6a8cedbe17c9a77.jpeg)

**jerryflyguy**

---
---
**Eric Lien** *October 12, 2016 01:41*

Try a little hotter on the hotend. I run PLA at 225 on my E3D V6. It might help your retracts to be more effective.


---
**Eric Lien** *October 12, 2016 01:48*

Also have you calibrated your extruder for length? Also calibrate your extrusion multiplier via the perimeter only cube method: compare theoretical extrusion width to the actual extrusion printer output (two perimeter at 0.5mm extrusion width should be 1mm thick wall... Adjust extrusion multiplier until it comes in to dimension averaging the four sides). I mention it because it looks like you are under extruding a bit.


---
**jerryflyguy** *October 12, 2016 03:17*

**+Eric Lien** best place to upload the fff?


---
**Eric Lien** *October 12, 2016 04:15*

**+jerryflyguy** do you have Dropbox or a Google drive?


---
**jerryflyguy** *October 12, 2016 04:18*

Nope but I'll give Dropbox a go


---
**jerryflyguy** *October 12, 2016 17:52*

**+Eric Lien** tried this two perimeter test, getting about 1.15mm walls w/ 0.50width and .95 multiplier. Best way to adjust this is to keep dropping the multiplier? My filament is measuring 1.73 (as best as I can measure).



Any links to a good description/how to of doing this test?

(Will post my FFF tonight)


---
**Eric Lien** *October 12, 2016 18:15*

**+jerryflyguy** This one is pretty good. But I like two perimeters versus 1:




{% include youtubePlayer.html id=cnjE5udkNEA %}
[https://www.youtube.com/watch?v=cnjE5udkNEA](https://www.youtube.com/watch?v=cnjE5udkNEA)


---
**jerryflyguy** *October 12, 2016 20:35*

**+Eric Lien** 



[https://www.dropbox.com/s/yceg9rr700o65oe/Eustathios%20Spider%20V2%20%28JJ%29.fff?dl=0](https://www.dropbox.com/s/yceg9rr700o65oe/Eustathios%20Spider%20V2%20%28JJ%29.fff?dl=0)



Should be the link to my FFF file? [new to this stuff :)  ]

[https://www.dropbox.com/s/yceg9rr700o65oe/Eustathios%20Spider%20V2%20%28JJ%29.fff?dl=0](https://www.dropbox.com/s/yceg9rr700o65oe/Eustathios%20Spider%20V2%20%28JJ%29.fff?dl=0)


---
**jerryflyguy** *October 19, 2016 20:36*

**+Eric Lien** how does the wall perimeter overlap factor impact this test? I just noticed mine is set at ~15%



Wondering if a single perimeter print isn't a better evaluation of print width as it applies to extrusion multiplier?


---
**Eric Lien** *October 20, 2016 02:55*

I tend to run more like 18-20% depending on the characteristics of the filament. Some plastics tend to have more or less die swell after exiting the Nozzle.


---
*Imported from [Google+](https://plus.google.com/102717204236948729189/posts/64GWkQEWNd1) &mdash; content and formatting may not be reliable*
