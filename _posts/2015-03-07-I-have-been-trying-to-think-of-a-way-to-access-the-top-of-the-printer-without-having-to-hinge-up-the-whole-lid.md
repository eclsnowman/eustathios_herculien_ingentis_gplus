---
layout: post
title: "I have been trying to think of a way to access the top of the printer without having to hinge up the whole lid"
date: March 07, 2015 07:25
category: "Show and Tell"
author: Eric Lien
---
I have been trying to think of a way to access the top of the printer without having to hinge up the whole lid. I think I found a solution. 



Only problem is I can no longer tell if I am designing printers, or transformers :)



![images/67c8cc9be975f629050d5b2552d35ed1.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/67c8cc9be975f629050d5b2552d35ed1.png)
![images/d32dd92c958e067af890984fcffaf9bc.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d32dd92c958e067af890984fcffaf9bc.png)
![images/b16aeb24c223a36ae6eb817788feadfd.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b16aeb24c223a36ae6eb817788feadfd.png)
![images/76805b1219ff074b6ca1d61ef7559125.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/76805b1219ff074b6ca1d61ef7559125.png)

**Eric Lien**

---
---
**Владислав Зимин** *March 07, 2015 11:53*

epic :)


---
**Dale Dunn** *March 07, 2015 13:49*

Nice. Do they make a hinge that will allow the top door to lay on top when open? Can the controller (or just the control and display) by mounted on the front door so it doesn't have to swing out of the way separately? Oh, but then you wouldn't be able to control things with the door open. Maybe the controls could be mounted on the frame between the doors, and the rest of the control box mounted around the side.



Is there any of that you hadn't already thought of, or am I just second guessing someone who's actually getting things done?


---
**Eric Lien** *March 07, 2015 14:50*

**+Dale Dunn**​​ I have thought of it. I was aiming to keep wires short. I should just flip the door hinge to the other side and mount things to the door, eliminate the dual hing setup. But I mounted the controller before I had panels and doors, so it would be a complete rewire... 



But you are right, I should change the model. Their is some nice things about having them separate, like the controller is still accessible when the door is open 270deg, and I can point the controller towards me with the door shut (I have a large L shaped desk and the printer is on the left of the L).﻿


---
**Derek Schuetz** *March 08, 2015 02:34*

How would the rattle be with this new design witch the extra door it may rattle more


---
**Eric Lien** *March 08, 2015 03:35*

**+Derek Schuetz** I plan on adding magnetic holds, and seals.


---
**Daniel Salinas** *March 08, 2015 06:27*

**+Eric Lien**​ I will work on a mod for my magnetic door stay to fit on a 20x20. Should be an easy change. 


---
**Mark “MARKSE” Emery** *March 09, 2015 21:53*

You just need a bit of Cube Radiation Eric. [https://m.youtube.com/watch?v=cmNf47Ms5tQ](https://m.youtube.com/watch?v=cmNf47Ms5tQ)


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/hxCVS8Vc8x2) &mdash; content and formatting may not be reliable*
