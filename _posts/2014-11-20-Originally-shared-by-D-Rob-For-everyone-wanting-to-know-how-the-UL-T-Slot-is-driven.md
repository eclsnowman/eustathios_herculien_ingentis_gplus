---
layout: post
title: "Originally shared by D Rob For everyone wanting to know how the UL-T-Slot is driven"
date: November 20, 2014 04:28
category: "Show and Tell"
author: D Rob
---
<b>Originally shared by D Rob</b>



For everyone wanting to know how the UL-T-Slot is driven.



![images/068e4e2396758b1d87f5dd703ed7fb4a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/068e4e2396758b1d87f5dd703ed7fb4a.jpeg)
![images/92dad7e450967a8a7689b0c210f8ccc3.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/92dad7e450967a8a7689b0c210f8ccc3.jpeg)

**D Rob**

---
---
**James Rivera** *November 20, 2014 05:22*

Are those Nema 14s on the X & Y?


---
**Eric Lien** *November 20, 2014 06:53*

Check the max travel to be safe. This goes to the corners (280mm max x/y) it also does some arc travels.



[https://drive.google.com/file/d/0B1rU7sHY9d8qcVYwUmhBYzBJOUk/view?usp=sharing](https://drive.google.com/file/d/0B1rU7sHY9d8qcVYwUmhBYzBJOUk/view?usp=sharing)


---
**D Rob** *November 20, 2014 06:55*

Print area will be in the vicinity of 300mmx300mm


---
**D Rob** *November 20, 2014 06:56*

**+James Rivera**​ they are nema 17.78oz/in the machine is just that big ;)﻿


---
**Jesper Lindeberg** *November 20, 2014 07:19*

Why are they mount on the outside of the frame? Looks like there is space on the inside. Nice machine anyway :-)


---
**D Rob** *November 20, 2014 07:22*

When i enclose it i want my motors cool.


---
**Jesper Lindeberg** *November 20, 2014 07:23*

^^ Makes sense :-) Nice job


---
**Mihail (MVSmaker)** *November 20, 2014 08:56*

it is a low-speed solution i think... I try this, and i have jamming on high acceleration values.


---
**D Rob** *November 20, 2014 08:57*

This is a 50mm pitch nothing slow about it


---
**Liam Jackson** *November 20, 2014 12:32*

Can this go as fast as belts + rods? How does cost compare? 


---
**Jesper Lindeberg** *November 20, 2014 13:09*

**+Liam Jackson** Say you have GT2 belt with 20 tooth pulleys, you get 80 steps/mm and with an arduino you can go to about 300mm/s before it stutters. With 50mm pitch on the screw it will be 64 steps/mm so speed should be as fast and maybe a bit faster.


---
**Liam Jackson** *November 20, 2014 13:11*

**+Jesper Lindeberg** interesting! I'm assuming xy resolution and backlash aren't a problem then? 


---
**Jesper Lindeberg** *November 20, 2014 13:16*

I can't say anything about backlash, depends on the nut's **+D Rob** is using and if they have antibacklash. XY res is a bit less, but stil ok. I have 66,666steps/mm on my Mendel90 and it's fine, can see any difference compared to my prusa with 100steps/mm.


---
**D Rob** *November 20, 2014 14:44*

These are anti backlash nuts that let you adjust a screw to remove backlash and are spring loaded. Check my posts.﻿


---
**Øystein Krog** *November 20, 2014 18:11*

Awesome **+D Rob** , can't wait to see movement!


---
*Imported from [Google+](https://plus.google.com/108729945898131117315/posts/Sgj7Fi3mrF7) &mdash; content and formatting may not be reliable*
