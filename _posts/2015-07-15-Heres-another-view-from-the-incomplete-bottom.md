---
layout: post
title: "Here's another view from the (incomplete) bottom"
date: July 15, 2015 12:49
category: "Show and Tell"
author: Frank “Helmi” Helmschrott
---
Here's another view from the (incomplete) bottom. 

![images/6871e62ead89a54cf08c7dd83987dce2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6871e62ead89a54cf08c7dd83987dce2.jpeg)



**Frank “Helmi” Helmschrott**

---
---
**Mike Miller** *July 15, 2015 14:53*

Cool...but...if you have a fan blowing (or sucking) you should probably engineer the airflow a little better, it's nowhere near the drivers, and there's no additional inlet/outlet..leaving the bottom open would certainly handle that, but there'd be zero chance you could ensure cooling air was being drawn across the heat sinks for the drivers. 



Secondarily, the motor <s>might</s> need cooling, but I've found if you're driving the motor hard enough that it's losing steps due to heat, there's an awful lot of unnecessary drag somewhere. These printers shouldn't need cooling on the steppers if everything is well aligned and lubricated. 


---
**Frank “Helmi” Helmschrott** *July 15, 2015 14:58*

Hey Mike, thanks for the heads up. From my experience with the DRV8825 a slight breeze of air should be enough keep them in their operational temperature. The current 50mm fan is blowing in their direction. If that's not enough (time will tell) there's still room for improvement.



I have never cooled any motors on any printers - i would be suprised if it's needed here. Especially on the z-motors that shouldn't be the case. I have motor coolers for passive cooling - they should still fit under there and should be a first step if we're facing problems.


---
**Mike Miller** *July 15, 2015 15:48*

Bah...google plus is formatting my posts in an unintended way.. Don't surround words with -'s...you shouldn't need motor cooling if the drivers are adjusted correctly...but you already know that... ;)


---
**Eric Lien** *July 15, 2015 19:00*

Looking good. 


---
**Eric Lien** *July 15, 2015 19:02*

Make a printed duct on that PSU exhaust fan to elbow it out the back and you will get pretty good air changes under the skirt.


---
*Imported from [Google+](https://plus.google.com/+FrankHelmschrott/posts/EHMG1pvtWEt) &mdash; content and formatting may not be reliable*
