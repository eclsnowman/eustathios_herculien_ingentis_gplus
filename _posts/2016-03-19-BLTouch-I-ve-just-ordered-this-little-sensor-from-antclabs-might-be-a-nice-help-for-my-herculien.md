---
layout: post
title: "BLTouch I ve just ordered this little sensor from antclabs , might be a nice help for my herculien,"
date: March 19, 2016 00:41
category: "Mods and User Customizations"
author: Jo Miller
---
BLTouch



I ve just ordered this little sensor from antclabs ,  might be a nice help  for my herculien,     
{% include youtubePlayer.html id=9JwNg1mT6u0 %}
[https://www.youtube.com/watch?v=9JwNg1mT6u0](https://www.youtube.com/watch?v=9JwNg1mT6u0)





**Jo Miller**

---
---
**Gústav K Gústavsson** *March 19, 2016 01:51*

Well we are geting there, next build HercuLien +++++ 

At the moment we are Building Cobblebot... Sort of... Realized it would never be a printer to our standards so we modified it. But we needed it for our Heculien build. And testing all the new things emerging. The Cobblebot will have a dual z motors, smoothieboard and BLtouch. Well we see how it works just programmed the smoothieboard to operate the BLtouch servo input just to realize it is used for the fan control. Well other pins available so program other output. Will it work?


---
**Martin Bogomolni (MB)** *March 19, 2016 01:59*

What a lovely little sensor ... this is an excellent idea for bed leveling.  How accurate?




---
**Gústav K Gústavsson** *March 19, 2016 02:01*

Not there yet. Will show if/when it works.


---
**Samer Najia** *March 19, 2016 14:43*

So a question: if you have a unleveled bed does that mean, with bed leveling, that your part prints with a slight lean or does the printer compensate to make sure the the part is perfectly true?  Imagine if I print so that the part ends up slightly off vertical, but now I have a surface that is actually flat but should have itself been at the same angle  (i.e. perpendicular to bed).  I know the variation is very small but for accurately fitting parts does that not pose a problem?


---
**Matthew Kelch** *March 19, 2016 20:58*

This looks great!  In my opinion, this is a much better solution compared to a inductive sensor since most people are using PEI on glass these days. Definitely follow up with us on how it works for you!


---
**Sean B** *March 27, 2016 22:44*

I ordered one of these about 2 months ago, it shipped directly from South Korea, cost was $35.  Took a bit to get dialed in through Marlin config.h but I love it.  I am in the process of attempting to build the Spider V2 and currently have it on my Folgertech i3 2020.  It is single handedly the best upgrade to my printer.  The first layer is perfect every time.  You can see the z lead screws adjusting for any height differences in the build surface.


---
**Samer Najia** *March 27, 2016 23:35*

**+Sean B**​, I also have a FG2020 i3, I'd be interested in details on how you implemented yours.  I am getting ready for assembly and I have been gathering and printing parts to convert mine to an all leadscrew implementation so I'd love to learn more.


---
**Samer Najia** *March 27, 2016 23:37*

Also if someone could answer my question above about how the print comes out (leaning, straight, irrelevant?), I would appreciate it.


---
**Sean B** *March 28, 2016 00:13*

**+Samer Najia**​​ regarding a leaning part that is a really good question.  I've never checked.  It wold be very slight I think, unless you are making a tall piece with a terribly unlevel bed.  I may do some test in the future to see if this is an issue, but so far so good.  I can help you out with setting up you FG2020.  I used the following mount, it works nicely, the pin in the up position is about 3mm above the nozzle.  BLTOUCH RAISED MOUNT found on #Thingiverse [http://www.thingiverse.com/thing:1353324](http://www.thingiverse.com/thing:1353324) the only issue I have is that the bl touch is so close to the bed that I come close to my bed clips on one side during the first few layers.﻿


---
**Samer Najia** *March 28, 2016 00:20*

Thanks **+Sean B**​.  I will probably start on the FG next.  If you could share the .H file and some pictures of your setup and connections/wiring, I would appreciate it.


---
**Sean B** *March 28, 2016 02:13*

**+Samer Najia** Here is my config.h file [http://pastebin.com/xFqgxKp8](http://pastebin.com/xFqgxKp8) my printer is running right now but I have the BLtouch plugged into the top of the ramps 1.4 board with a jumper added to have 5V.  Check their bltouch google+ page for a picture.


---
*Imported from [Google+](https://plus.google.com/103341289176473342280/posts/ergzCvtCkYW) &mdash; content and formatting may not be reliable*
