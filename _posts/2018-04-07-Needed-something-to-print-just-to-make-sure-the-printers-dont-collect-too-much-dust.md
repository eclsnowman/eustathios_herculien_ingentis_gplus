---
layout: post
title: "Needed something to print, just to make sure the printers don't collect too much dust"
date: April 07, 2018 23:19
category: "Show and Tell"
author: Eric Lien
---
Needed something to print, just to make sure the printers don't collect too much dust. Trying the Makers Muse torture egg. I like printing perimeters outside in for better surface finish and tolerance (inside out can push last outside perimeters out if the inside perimeters are at all overstuffed). Looks like i need to dial in a hait more. The knife edge created by the window cut-outs in the egg leaves no room for error if the start of extrude begins on the edge. I wish you could tell simplify3d not to start at an edge. If it is a simple geometry I can define a seam start location. But for something with radial symmetry there is no good solution. That being said, not bad for a printer with a 850mm Bowden Tube ;) (but there is always room for improvement).



Anyway, I don't post as often as I used to. So I figured I would just shoot this out there.



Anyone out there printing anything cool? If so post it out. Always good to see our printers in action.



And a quick shout out to everyone who made it to MRRF. It was a blast. And glad to be able to talk to everyone who made it.



![images/d0b025218fa095fa8712a43bb09bf3b9.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/d0b025218fa095fa8712a43bb09bf3b9.jpeg)
![images/c4b722c817df6864154f8c0ab300bb1c.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/c4b722c817df6864154f8c0ab300bb1c.jpeg)

**Eric Lien**

---
---
**Bruce Lunde** *April 08, 2018 00:43*

Looks great from this angle!


---
**Bruce Lunde** *April 08, 2018 00:47*

I am continuing to enjoy good prints, here are the front and back structural plates for my Inmoov Robot project. I have to do very little prep to get the pieces to interlock! ![images/dda0d3be206b5a216860c3f92526a91a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/dda0d3be206b5a216860c3f92526a91a.jpeg)


---
**Bruce Lunde** *April 08, 2018 00:47*

![images/346b0cc38dc81f74f1ace21363fd39ae.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/346b0cc38dc81f74f1ace21363fd39ae.jpeg)


---
**Bruce Lunde** *April 08, 2018 00:48*

![images/e7c4331867b6135de109b42a3ade49b8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e7c4331867b6135de109b42a3ade49b8.jpeg)


---
**Eric Lien** *April 08, 2018 00:56*

**+Bruce Lunde** you are getting better all the time. Your prints are starting to look great. Glad to see you starting to hit your stride.



Also hope the car trouble all got figured out when you got back from MRRF. It was  a lot of fun getting to chat in person after all these years. 


---
**Bruce Lunde** *April 08, 2018 01:07*

**+Eric Lien** Thanks, enjoyed meeting up and talking, also the “little” adjustment you made in my cura settings has a lot to do with my success!


---
**Eric Lien** *April 08, 2018 01:31*

**+Bruce Lunde** glad to hear I helped.



MRRF is the perfect place to bring your printer troubleshooting. Someone there has certainly run into it before.


---
**Eric Lien** *April 08, 2018 01:32*

Print finished![images/6ceda401f7b4a6858e588c857f0f530a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6ceda401f7b4a6858e588c857f0f530a.jpeg)


---
**Eric Lien** *April 08, 2018 01:32*

![images/b09e7bce765bfd516cf11ff9bddbc8d5.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b09e7bce765bfd516cf11ff9bddbc8d5.jpeg)


---
**Jason Smith (Birds Of Paradise FPV)** *April 08, 2018 02:16*

Looks awesome. What material, settings?


---
**Eric Lien** *April 08, 2018 03:06*

**+Jason Smith** 



[https://atomicfilament.com/collections/opaque-pla-filaments-1/products/neon-yellow-uv-reactive-pla-filament](https://atomicfilament.com/collections/opaque-pla-filaments-1/products/neon-yellow-uv-reactive-pla-filament)



40mm/s, 210C hotend, 65C bed, 2 perimeters, 0% infill, perimeter order outside then inside, 0.3mm zhop. 


---
**Eric Lien** *April 08, 2018 18:54*

Looks like the filament is out of stock at Atomic, but it looks SMW3D has the neon yellow and neon green in stock: 



[https://www.smw3d.com/filament](https://www.smw3d.com/filament)

[smw3d.com - Filament](https://www.smw3d.com/filament)


---
**Jason Smith (Birds Of Paradise FPV)** *April 08, 2018 22:02*

![images/bd5f4298af421c145492c8f4727ba577.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/bd5f4298af421c145492c8f4727ba577.jpeg)


---
**Eric Lien** *April 08, 2018 22:10*

**+Jason Smith** nice to see someone using the direct drive carriage. How is it working for you? Looks like you need a few shorter bolts for the Bondtech :)


---
**Jason Smith (Birds Of Paradise FPV)** *April 08, 2018 22:32*

Direct drive carriage has been working great.  I’ve been printing TPU left and right with no issues. Totally forgot I had those washers in there. They’ve been there so long that I don’t even notice.   Its good to have the extra weight. I wouldn’t want the printer working too well now would I  :)

I’m about to convert back to a regular v6 nozzle with all the upgrades rather than the volcano. A buddy gifted me one of those fancy ruby nozzles for the v6, and I think the v6 can achieve slightly higher quality anyway. 


---
**Eric Lien** *April 10, 2018 03:03*

**+Jason Smith** how did it turn out.


---
**Jason Smith (Birds Of Paradise FPV)** *April 10, 2018 03:19*

**+Eric Lien** _[https://plus.google.com/photos/..._](https://plus.google.com/photos/..._)

I managed to get one of the layers separated. Might have to do some work for the other one. I also may print another version using the “exclusive” layer calculation method in Cura. Or I may print at 2x size rather than 1x. Not bad for first attempt though. 


---
**Eric Lien** *April 10, 2018 04:26*

Hmm, link to your photo doesn't seem to work for me.


---
**Jason Smith (Birds Of Paradise FPV)** *April 10, 2018 04:27*

**+Eric Lien** ![images/247b3473b8f94db30a8223da5871c411.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/247b3473b8f94db30a8223da5871c411.jpeg)


---
**Eric Lien** *April 10, 2018 04:36*

**+Jason Smith**  Yeah, I did 1.5X on mine. It made the features more reasonable.



BTW, I didn't like his sharp edged in the model. SO I remodeled from scratch and added fillets. If you want to check it out the file is here in a few formats.



[https://drive.google.com/folderview?id=1QszFvylHqf68g5WDbcr5qtQho-sTrRFa](https://drive.google.com/folderview?id=1QszFvylHqf68g5WDbcr5qtQho-sTrRFa)


---
**Eric Lien** *April 10, 2018 04:37*

![images/a0557721baaabdcadcd475b43d41aa61.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a0557721baaabdcadcd475b43d41aa61.png)


---
**Eric Lien** *April 10, 2018 04:37*

![images/8896ed2c955c738ad0c6098a83f61a80.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8896ed2c955c738ad0c6098a83f61a80.png)


---
**Eric Lien** *April 10, 2018 04:38*

Not exact 1:1, I took some liberties while modeling.




---
**Dennis P** *April 10, 2018 05:39*

**+Eric Lien**google drive link no work :( i would be game to print one for funzies... i need a torture test. <EDIT> no multicolors look possible with single extruder :(






---
**Eric Lien** *April 10, 2018 05:55*

**+Dennis P** hmm the link on my end works. I will paste the link again below:



[https://drive.google.com/folderview?id=1QszFvylHqf68g5WDbcr5qtQho-sTrRFa](https://drive.google.com/folderview?id=1QszFvylHqf68g5WDbcr5qtQho-sTrRFa)


---
**Dennis P** *April 11, 2018 16:36*

I think I am finally getting dialed in- Modeled and made a spindle nose protector for the lathe. 1-1/2x8 internal threads,~2" OD knurled body, 1/4" spanner hole. It fits snug, good enough for light work.  

![images/5a6709a78f1d124c066ab198a9766bf3.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/5a6709a78f1d124c066ab198a9766bf3.png)


---
**Eric Lien** *April 11, 2018 17:03*

**+Dennis P** Looking good. Your ability in openscad is impressive. I have never been able to learn it. I am far more point and click.


---
**Dennis P** *April 11, 2018 17:35*

**+Eric Lien** What you don't see is all the wood chips from all the hacking at it that I have done!  I am by no means proficient, but OpenSCAD feels kind of natural coming from ACAD/lisp.  Threads and knurls seem to be taxing on the rendering on my workstation. The final print was after making 2, 1/2" tall threaded check rings to get the tolerances and dimensions spot on. I ended up adding 0.2mm clearance to get the register diameter and the  threads to compensate for dimensions. I could do better I think with the slicer and calibration. . 


---
**Eric Lien** *April 11, 2018 17:57*

.15 to .2 is a pretty standard fit for printed parts. That's my default tolerance for a good robust fit with enough wiggle room to not be a pain. Now when you get to stacking tolerances... that's another matter. But a .2mm fit leaves room for some die swell, filament diameter variation, perimeter start scars, etc. 


---
**Natasa Iljinov** *April 14, 2018 22:14*

Jeeeees


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/bjnCuQBqEe9) &mdash; content and formatting may not be reliable*
