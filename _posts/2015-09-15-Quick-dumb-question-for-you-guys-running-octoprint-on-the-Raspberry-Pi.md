---
layout: post
title: "Quick dumb question for you guys running octoprint on the Raspberry Pi"
date: September 15, 2015 04:12
category: "Discussion"
author: Erik Scott
---
Quick dumb question for you guys running octoprint on the Raspberry Pi. How do you go about connecting the Pi to the printer board (Azteeg X5 mini)? Is there a way to keep all the connections internal? I'm working on the wiring right now, and I need to figure this out. ﻿





**Erik Scott**

---
---
**Eric Lien** *September 15, 2015 04:40*

Yeah I wasn't too happy with my solution. I wanted outside access in case people ran without the Pi from a PC, But allow a small jumper cable from the PI to the board if you did. Guess I should have designed it with panel mount patch cables instead of holes and move things in further to keep things clean.


---
**Mike Miller** *September 15, 2015 12:24*

You can also get an inexpensive buck-transformer (like this: [http://www.miniinthebox.com/ultra-small-lm2596-power-supply-module-dc-dc-buck-3a-adjustable-buck-module-regulator-ultra-lm2596s-24v-switch-12v-5v-3v_p1023925.html](http://www.miniinthebox.com/ultra-small-lm2596-power-supply-module-dc-dc-buck-3a-adjustable-buck-module-regulator-ultra-lm2596s-24v-switch-12v-5v-3v_p1023925.html) though no warrantee is express or implied) to power a Pi: Be aware though that your printer will reset approx 45 seconds after powerup when Octoprint connects to your controller. (unless that's just a RAMPS thing...then nevermind.)


---
**Mike Miller** *September 15, 2015 12:26*

ETA: I actually used one like this: [http://www.miniinthebox.com/dc-buck-converter-step-down-12v-to-5v-5a-15w-power-supply-dc-step-down-module_p494373.html](http://www.miniinthebox.com/dc-buck-converter-step-down-12v-to-5v-5a-15w-power-supply-dc-step-down-module_p494373.html)


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/AZDKkDgW4oU) &mdash; content and formatting may not be reliable*
