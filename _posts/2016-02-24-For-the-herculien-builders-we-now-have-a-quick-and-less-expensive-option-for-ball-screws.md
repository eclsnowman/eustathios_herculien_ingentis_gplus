---
layout: post
title: "For the herculien builders, we now have a quick (and less expensive) option for ball screws"
date: February 24, 2016 20:01
category: "Discussion"
author: Jim Stone
---
For the herculien builders, we now have a quick (and less expensive) option for ball screws [http://www.aliexpress.com/store/product/RM1204-Ball-Screw-L480mm-Ballscrew-With-SFU1204-Single-Ballnut-For-CNC-Processing-length-can-be-customized/920371_32612035532.html](http://www.aliexpress.com/store/product/RM1204-Ball-Screw-L480mm-Ballscrew-With-SFU1204-Single-Ballnut-For-CNC-Processing-length-can-be-customized/920371_32612035532.html)





**Jim Stone**

---
---
**Eric Lien** *February 24, 2016 20:11*

Thanks Jim


---
**Tomek Brzezinski** *February 24, 2016 20:15*

Age old Q: Do we have anything to gain from ballscrews? They have increased backlash, technically, and mostly are useful for fast-moving parts. We usually use them for Z-axis not anything typically fast moving. 



Mostly I just don't like the cost and difficulty sourcing quality ballscrews. 



Anyone really happy with their switch to ballscrews? What is it that you like?


---
**Eric Lien** *February 26, 2016 00:41*

Well I guess if the prints on the Eustathios by **+Walter Hsiao**​ are any example... The golmart ballscrews are pretty good. His prints look amazing.


---
**Tomek Brzezinski** *February 26, 2016 01:11*

Is that eusthathios using ballscrews for XY? I'm more willing to think that XY can benefit from ball screws vs leadscrews, but just that it's a heavy handed solution (because of the literal inertial 'weight')



I think some of the kickass printers using ballscrews for Z-axis alone look really good simply because the people who use ballscrews are careful and willing to pay enough for a better machine overall.   There isn't an explicit major benefit of ball screws over preloaded appropriately sourced leadscrews. I mean, I'm open to any suggestions why I might be wrong.  And there are cases where people print in a way that moves the Z axis alot, or just use z-lift, maybe in those cases ballscrew could be better.  It does introduce some problems though (more backlash.) 


---
**Walter Hsiao** *February 26, 2016 03:09*

I chose ballscrews (Z only) partially because I was curious about them, but mostly because they looked like an easy replacement that would cut the costs of my build by $100 (I saved about $90 on the ballscrews vs Misumi leadscrews and $10 on shims at the time).  I might have been able to find a pair of custom machined or compatible 12mm lead screws for less than $60, I didn’t look.



I’ve been happy with them.  I’m using the smaller, 0.9 degree steppers instead of the ones in the BOM and an extra large 1/4” aluminum build plate so I expect the ball screws help with z-axis speed (it takes about 10 seconds to travel 300mm with my current speed/acceleration settings).  Not useful during printing, but it’s useful when dropping the bed to apply hair spray (I do terrible things like apply hair spray to the bed while it’s on the printer and knock prints off the bed with a hammer, also on the printer).  With the heavy bed, I assumed that backlash wouldn’t be an issue whether I went with lead screws or ball screws.  



An unexpected benefit is I’ve found the ball screws to be much lower maintenance compared to the lead screws, just shove a bunch of grease into the grease port and your done, no squeaking or other issues so far despite all the hair spray. Personally I don’t think it would have made a difference in print quality if I had used lead screws, but I don’t have any basis for that opinion.


---
*Imported from [Google+](https://plus.google.com/110273126198750367391/posts/dJytBWs8MjY) &mdash; content and formatting may not be reliable*
