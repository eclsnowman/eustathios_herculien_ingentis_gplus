---
layout: post
title: "I just received 4 of these in the mail...."
date: October 04, 2015 20:44
category: "Discussion"
author: Eric Lien
---
I just received 4 of these in the mail.... Can't wait to compare them to the SD8825 I have been using for years now. The low noise operation modes and up to 1/128 microstepping are really cool upgrades from most of the steppers used out there right now.





**Eric Lien**

---
---
**Eirikur Sigbjörnsson** *October 04, 2015 20:55*

Keep us updated since this is something I need to make a decision on soon :)


---
**Eric Lien** *October 04, 2015 20:58*

**+Eirikur Sigbjörnsson** will do. I think they will be a killer combo with the rumored new controller Roy is working on. I think 1/128 microstepping would really benefit from a 32bit controller over the standard 8bit controllers out there.


---
**Eirikur Sigbjörnsson** *October 04, 2015 21:36*

**+Eric Lien**  My plan is to use the Smoothie 5XC on the big boy so these controllers could be a perfect fit for me.


---
**Jean-Francois Couture** *October 05, 2015 13:41*

**+Eric Lien** Could those be drop in replacements for other controllers out there ?


---
**Eric Lien** *October 05, 2015 14:15*

As I understand it, yes these are drop in replacements for any of these standard socket style driver boards: [http://files.panucatt.com/datasheets/sd6128_user_guide.pdf](http://files.panucatt.com/datasheets/sd6128_user_guide.pdf)


---
*Imported from [Google+](https://plus.google.com/+EricLiensMind/posts/bvb9M9spxaW) &mdash; content and formatting may not be reliable*
