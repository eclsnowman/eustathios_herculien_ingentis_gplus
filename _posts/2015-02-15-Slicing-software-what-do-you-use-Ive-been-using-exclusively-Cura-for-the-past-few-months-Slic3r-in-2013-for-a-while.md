---
layout: post
title: "Slicing software: what do you use? I've been using exclusively Cura for the past few months, Slic3r in 2013 for a while"
date: February 15, 2015 18:11
category: "Discussion"
author: Nathan Buxton
---
Slicing software: what do you use?



I've been using exclusively Cura for the past few months, Slic3r in 2013 for a while. I have craftware on my computer, but never actually sliced and printed with it. I bought Simplify3D recently, and it seems pretty good. I also have KISSlicer, but never used it either.



What do you guys and gals swear by?



I'm finding S3D to be worth the money (though the Canadian dollar could have been better!! $200 after conversion!) I really like being able to print different objects at different layer heights.





**Nathan Buxton**

---
---
**Ubaldo Sanchez** *February 15, 2015 18:24*

I have always wanted to use simplify 3d, but I cannot justify the price tag. I used to swear by cura though because it was very fast and the tool path seemed logical. But one day I decided to just configure kisslicer and give it a go. Best decision I've  made in my  printing hobby! 



It lacks some features, but damn it makes beautiful prints at decent speeds! Give it a go, you probably won't regret it.


---
**Nathan Buxton** *February 15, 2015 18:28*

I might try Kiss, but after dropping $200 it will be hard to justify not using my new software! :P To be honest, I was on the fence about s3d for a long time. It wasn't until I had a chance (and a need) to use it, and some of it's features that I was convinced. If you know someone who has it, see if you can slice some things for yourself and print them. I'm sure Kiss can slice and print as well, but S3d has some super duper life saving features. I wonder if Kiss has them, too? :P


---
**Ubaldo Sanchez** *February 15, 2015 18:36*

Haha don't blame you. What other features of s3d do you find useful? I haven't looked into it much considering that all the prints I do are for the fun of it and do not warrant fancy/expensive software. 




---
**Eric Lien** *February 15, 2015 18:44*

I am using simplify3d always now. But once you get good settings in any software it's hard to want to start over. I have always wanted to try kiss slicer... But man that interface is ugly. Maybe a project for next weekend.


---
**Nathan Buxton** *February 15, 2015 18:45*

Well, Cura can do this too in a sense, and I haven't tried it yet but I know I will soon, and that is changing the layer height at different layers. So the first xx layers can be .25mm, then you can specify a region that needs .2mm, or .1mm layers.



As I said that, that isn't entirely unique to s3d. But I think being able to specify different build paramters, includign layer height, for different models on a build plate is great.


---
**Ubaldo Sanchez** *February 15, 2015 18:49*

That's pretty cool. Yeah, cura doesn't seem to have it down like s3d in that. I can definitely see that feature being useful though. 



And hell yeah the kiss interface us ugly! I couldn't get over it at first. 




---
**Len Trigg** *February 15, 2015 19:24*

Recently I have been using slic3rs modifier mesh feature to specify regions (not just layers or whole pieces) of the print to use different infill density and other properties, which is pretty damn cool. Is this something that other slicing tools can do?﻿


---
**James Rivera** *February 15, 2015 19:26*

I also tried KissSlicer and I abandoned it because the user interface almost looked intentionally confusing. But that is what happens when you design UI around the object model or features instead of the user scenario/work flow. Anyway, while I still like Slic3r for being able to tweak every little setting, I find myself using Cura nearly all the time. I have heard some good things about support material being implemented very well in Simplify 3d, but I try to just avoid needing support material so that is not of much value to me. Regarding different layer height settings at specific heights, you can do this manually by editing the Gcode before printing. But editing layer heights for different objects is much more difficult. If S3d does that, it could be very useful.


---
**Tim Rastall** *February 15, 2015 19:33*

Kiss gives you the most consistent results and had good snap away supports (once tuned) but yes,  the UI is a mess.  Worth persisting with if you dont want to fork out for s3d. I found cura to be sporadically good but not reliable enough unless you had a similar set up to an ultimaker.


---
*Imported from [Google+](https://plus.google.com/+NathanBuxton3D/posts/gX72FP6qDsV) &mdash; content and formatting may not be reliable*
