---
layout: post
title: "I am one of those with a half built printer that is begging to be finished"
date: November 21, 2017 02:08
category: "Build Logs"
author: Pete LaDuke
---
I am one of those with a half built printer that is begging to be finished.  Attempted to "Assemble" my Bed assembly with the Z-axis lead screw over the weekend, and found an issue I'm curious to hear from others about.  



Summary is that my lead screws only have a 40mm step distance.  Not 46mm which is desired so that you have room to assemble the collet underneath.



From the BOM.  The Misumi part number MTSBRB12-425-S46-Q8-C3-J0 Appears to be an impossible setup.  It is a 12mm Threaded lead screw with a 2mm pitch.  425mm long.  With one end turned down to Dia "Q" (8mm) With a "Step Distance" S dim of (46mm)



Misumi is saying that they can't make it.  As they so eloquently put it... " when specifying Q <= 9, then S <= Q x 5."  



Currently my lead screws are 1) from Misumi, and 2) only have a 40mm step.  I'm curious to hear from others who have experienced this and any ideas on what the possible work around is?



Thanks,



Pete





**Pete LaDuke**

---
---
**Gus Montoya** *November 21, 2017 02:20*

Which printer are you working on? Just curious as I never read of this issue before. Following just in case I run into the same thing. 


---
**Zane Baird** *November 21, 2017 02:29*

**+Pete LaDuke** I would suggest doing what I did and going with TR8x8 ACME screws. They will fit in the same pulleys with no modification and will be MUCH cheaper than the Misumi part listed in the BOM. Additionally as they are 4-start screws (8mm lead) you can move the bed up and down pretty fast. 


---
**Gus Montoya** *November 21, 2017 02:36*

**+Zane Baird** Are they same pitch? thanks for the advice, a penny saved is a penny earned hehe


---
**Pete LaDuke** *November 21, 2017 02:38*

**+Gus Montoya** Ah good point, Eustathios 


---
**Pete LaDuke** *November 21, 2017 02:42*

**+Zane Baird** I am new enough to this “build your own” concept that I don’t fully appreciate the implications of your suggestion.   I don’t mind switching design direction but I do have all the other parts ready to go for the 12mm lead screw with bearings, holder, washer and collet.    Have a source in mind?    I will definitely look into it.


---
**Pete LaDuke** *November 21, 2017 02:44*

**+Gus Montoya** I haven’t seen it before either but I just put in the trouble ticket with Misumi this weekend and that was my response today.   Doesn’t sound right, but I am just attempting to work though it.


---
**Zane Baird** *November 21, 2017 03:01*

**+Pete LaDuke** If you are in the US, SMW3d is a great supplier. **+Gus Montoya** With a leadscrew you get the steps per mm by calculating the "lead" rather than the pitch. The threads have a 2mm pitch, but as there are 4 starts you get the "Lead" by multipling the two: 2x4 = 8mm vertical movement per rotation. With the gearing on the z-axis of the herculien and 1/16 microstepping that equates to 720 steps/mm on the Z-axis


---
**Pete LaDuke** *November 21, 2017 03:36*

**+Zane Baird** thanks for the clarification Zane.  Helpful.


---
**Gus Montoya** *November 21, 2017 03:45*

**+Pete LaDuke**  If you purchased the lead screw from the links provided on the BOM, you shouldn't have any trouble. Unless Misumi sent you a wrong set. But please do keep us posted in your resolution.


---
**Eric Lien** *November 21, 2017 04:52*

Another option is the ballscrew version for golmart others have utilized. But as Zane Mentioned, an inexpensive TR8x8 leadscrew will work as well. 



The leadcrew issues from Misumi drive me nuts. I have fought the fact that they change the part numbers regularly. I have no clue why they do it. To be honest, I should just pull their leadscrews from thr BOM due to the headaches its caused me.


---
**Pete LaDuke** *November 21, 2017 23:52*

**+Eric Lien** I can totally understand your frustration/reasoning behind pulling the Misumi part number (If you do). It appears that the part number formula no longer allows you order it.   My parts have a 40mm step, I’m going to try to get them machined down.   It they get damaged I’ll buy new ones like **+Zane Baird** 


---
**Jason Smith (Birds Of Paradise FPV)** *November 24, 2017 17:53*

My guess is that Misumi has stopped offering the 46mm step versions because they have had issues machining them straight when the step is that long.  I have the very first pair of lead screws with those dimensions, and I've had noticeable z-wobble (aligned with each revolution of the lead screw) from the beginning.


---
**Pete LaDuke** *November 24, 2017 18:01*

**+Jason Smith** that is a great insight.   Thanks for letting me and the community know.


---
*Imported from [Google+](https://plus.google.com/100658478011121421875/posts/YnZrRrVdTFX) &mdash; content and formatting may not be reliable*
