---
layout: post
title: "Motor Flex- Do the motors flex on your printer when under load?"
date: March 24, 2018 00:06
category: "Discussion"
author: Dennis P
---
Motor Flex- 



Do the motors flex on your printer when under load?  



This is my Y motor under break-in gcode squares. I was 125% of 150 mm/s or about 185mm/s travel speeds. 



Granted I upped the motors to Automation Direct's 124 ounce-in motors after 2 disappointing RobotDIgg  shipping fails... and these things are beasts. 



I am only running them at 0,5Vref, or 1V and the noise is not bad. 



But the flex has me concerned. The mounts were printed in PETG at ~30% infill. 



Anyone else have vibrations like these? 



I may try realigning the rods after a break-in cycle. 

<a href="https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8f1f8ed4e7fbc8faff0b2cd27f373700.mp4">![images/1789a589849d14e4c3085229ed45c06e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/1789a589849d14e4c3085229ed45c06e.jpeg)</a>



**Dennis P**

---
---
**Ryan Carlyle** *March 24, 2018 00:42*

Looks like an eccentric pulley bore or bent motor shaft. Put a sharpie dot on the pulley or something and see if the wobble syncs up with the pulley rotation. 


---
**Bruce Lunde** *March 24, 2018 00:45*

They do a little....


---
**Ryan Carlyle** *March 24, 2018 02:02*

Try different pulleys? Eg swap within the printer and see if the problem moves. 


---
**Dennis P** *March 24, 2018 03:16*

that is a good idea. i will try that in the morning. 


---
**Dennis P** *March 26, 2018 19:08*

turned out that the pulley bores are not on center. 


---
*Imported from [Google+](https://plus.google.com/114764801971637832887/posts/ermEeqUDEkW) &mdash; content and formatting may not be reliable*
