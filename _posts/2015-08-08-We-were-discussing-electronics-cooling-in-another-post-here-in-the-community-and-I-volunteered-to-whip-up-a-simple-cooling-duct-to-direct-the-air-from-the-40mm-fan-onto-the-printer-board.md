---
layout: post
title: "We were discussing electronics cooling in another post here in the community and I volunteered to whip up a simple cooling duct to direct the air from the 40mm fan onto the printer board"
date: August 08, 2015 02:05
category: "Discussion"
author: Erik Scott
---
We were discussing electronics cooling in another post here in the community and I volunteered to whip up a simple cooling duct to direct the air from the 40mm fan onto the printer board. 



Now, if I were really dedicated, I'd go ahead an analyze this in OpenFOAM or something to make sure the air distribution is okay, but I'm not. I just kinda eyeballed it. It might be more beneficial to make it even a bit sorter as I feel the outboard side of the board will be getting much less air. That said, the important thing is that there's air moving across it, so there's no doubt this will be an improvement. 



There are 2 channels for the air to flow through to better distribute the air. It also makes the duct a bit stronger, not that there's any load on it or anything. 



As you can see from some of the photos, I got a fair amount of overhang derp on the first print. The second time, I added support for anything over 45deg. Worked very well and wasn't too difficult to remove. I would recommend doing this if you plan to print it yourself. I use Cura at the moment, and I left all support settings alone except the density, which I upped to 25%. 



Speaking of printing it yourself, here's the STL: [https://www.dropbox.com/s/t30x4v8ob4ic24x/Eustathios%20-%20Air%20Duct%20Electronics.stl?dl=0](https://www.dropbox.com/s/t30x4v8ob4ic24x/Eustathios%20-%20Air%20Duct%20Electronics.stl?dl=0)

STEP file: [https://www.dropbox.com/s/e096qalzkkv6jbw/Eustathios%20-%20Electronics_AirDuct.step?dl=0](https://www.dropbox.com/s/e096qalzkkv6jbw/Eustathios%20-%20Electronics_AirDuct.step?dl=0)



The models, like the rest of the printer, are CC-BY-SA 4.0



As always in this community, we all benefit from each other's work. If you do end up printing it and you think something could be better, please let me know or go ahead and make the changes yourself and share it. 



![images/22354cf2b4070c500ce2292349a33b4c.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/22354cf2b4070c500ce2292349a33b4c.png)
![images/339b70baeb57fafc351e66e89eb942e6.png](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/339b70baeb57fafc351e66e89eb942e6.png)
![images/05fc858a26ef855d5487f33ad0b40aae.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/05fc858a26ef855d5487f33ad0b40aae.jpeg)
![images/6bb38769a9564d317b4252ccddba4372.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6bb38769a9564d317b4252ccddba4372.jpeg)
![images/8f2a3addd638591c9f7aaccfd8078358.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/8f2a3addd638591c9f7aaccfd8078358.jpeg)
![images/96abe9d6743320fd20a01ee36ce8110f.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/96abe9d6743320fd20a01ee36ce8110f.jpeg)
![images/84b79679b0543d722257f231092e0480.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/84b79679b0543d722257f231092e0480.jpeg)

**Erik Scott**

---
---
**Eric Lien** *August 08, 2015 02:10*

Looks great man. I will get this added to the github.


---
**Alex Lee** *August 08, 2015 15:40*

**+Eric Lien** We should create a true repository for all things CNC. Like a glorified Wiki, but easier to add and edit. Kinda like the Openbuilds "Builds" page, but with more options.


---
**Eric Lien** *August 08, 2015 17:15*

**+Alex Lee** you code the back end and maybe we can make the thingiverse of CNC.


---
**Alex Lee** *August 08, 2015 17:17*

**+Eric Lien** then we take over the world? MUHAHAHA!!!


---
**Erik Scott** *August 08, 2015 18:41*

I agree there needs to be a more organized repository of all the various versions and improvements made to these printers. 


---
*Imported from [Google+](https://plus.google.com/+ErikScott128/posts/4aBhuRBdATj) &mdash; content and formatting may not be reliable*
