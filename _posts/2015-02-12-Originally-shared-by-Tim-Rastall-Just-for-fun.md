---
layout: post
title: "Originally shared by Tim Rastall Just for fun"
date: February 12, 2015 07:31
category: "Show and Tell"
author: Tim Rastall
---
<b>Originally shared by Tim Rastall</b>



Just for fun.  I thought I'd see if I could build an extruder from bits of aluminum I had lying around.  Turns out I could.  The printed gear has been sitting on a shelf for a year waiting for this. 



![images/55bd24380374308276b702d1503d2c22.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/55bd24380374308276b702d1503d2c22.jpeg)
![images/e2409f7eebbb4380cf9865addb0890f2.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/e2409f7eebbb4380cf9865addb0890f2.jpeg)
![images/7fa106ef66b3fe5afb569a6f3e7b8891.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/7fa106ef66b3fe5afb569a6f3e7b8891.jpeg)

**Tim Rastall**

---
---
**Bruce Lunde** *February 12, 2015 13:54*

How well does it work for you, or was it just an experiment?


---
**Brandon Satterfield** *February 12, 2015 16:14*

That is "making", great job Tim!


---
**Tim Rastall** *February 12, 2015 20:11*

**+Bruce Lunde** haven't tried it in anger yet.  Going on the new prototype that has yet to have its Z stage installed. 


---
*Imported from [Google+](https://plus.google.com/+TimRastall/posts/Dh7jrKoWe8C) &mdash; content and formatting may not be reliable*
