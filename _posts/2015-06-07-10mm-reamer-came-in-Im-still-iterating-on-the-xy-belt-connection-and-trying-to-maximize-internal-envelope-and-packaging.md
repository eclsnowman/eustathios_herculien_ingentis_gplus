---
layout: post
title: "10mm reamer came in, I'm still iterating on the xy belt connection, and trying to maximize internal envelope and packaging"
date: June 07, 2015 02:58
category: "Deviations from Norm"
author: Mike Miller
---
10mm reamer came in, I'm still iterating on the xy belt connection, and trying to maximize internal envelope and packaging. 



![images/885673d14152e5f40ab36fa6e47c5556.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/885673d14152e5f40ab36fa6e47c5556.jpeg)
![images/6c261dcfc9b2fa39bb653d22d4274b7a.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/6c261dcfc9b2fa39bb653d22d4274b7a.jpeg)
![images/a487be2759e6ce9b3fd53fad3debf38e.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/a487be2759e6ce9b3fd53fad3debf38e.jpeg)

**Mike Miller**

---


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/8Yuv3kygLw9) &mdash; content and formatting may not be reliable*
