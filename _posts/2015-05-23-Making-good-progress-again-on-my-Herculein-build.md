---
layout: post
title: "Making good progress again on my Herculein build"
date: May 23, 2015 00:46
category: "Show and Tell"
author: Bruce Lunde
---
Making good progress again on my Herculein build.



![images/b6f71c7453ed91540ab7d64498c573c0.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b6f71c7453ed91540ab7d64498c573c0.jpeg)
![images/80a1f3dfd2e7fe93eb7a8cf9347fda73.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/80a1f3dfd2e7fe93eb7a8cf9347fda73.jpeg)

**Bruce Lunde**

---
---
**Dat Chu** *May 23, 2015 00:57*

Very nice. I take it you are following the guide posted on github?


---
**Bruce Lunde** *May 23, 2015 01:43*

**+Dat Chu**​ I am following **+Daniel Salinas**​ instructions, with **+Eric Lien**​ pictures on the adjacent screen.


---
**Dat Chu** *May 23, 2015 01:48*

Great. I am following that guide as well. Albeit all this tapping is taking much time. 


---
**Eric Lien** *May 23, 2015 02:24*

Glad to see things coming together.


---
**Gus Montoya** *May 23, 2015 08:16*

Nice progress is always good.


---
*Imported from [Google+](https://plus.google.com/+BruceLunde/posts/1nDCLVLtBwu) &mdash; content and formatting may not be reliable*
