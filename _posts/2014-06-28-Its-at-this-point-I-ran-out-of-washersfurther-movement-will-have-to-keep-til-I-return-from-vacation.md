---
layout: post
title: "It's at this point I ran out of washers....further movement will have to keep til I return from vacation"
date: June 28, 2014 21:42
category: "Show and Tell"
author: Mike Miller
---
It's at this point I ran out of washers....further movement will have to keep til I return from vacation. (Like my High-Zoot Carbon Fiber mock-up bearings?)



![images/b40dd1ac271ae3d3ba1d69c388982e2b.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/b40dd1ac271ae3d3ba1d69c388982e2b.jpeg)
![images/533da392980bbfb7234ac54258b734d8.jpeg](https://gitlab.com/eclsnowman/eustathios_herculien_ingentis_gplus/raw/master/images/533da392980bbfb7234ac54258b734d8.jpeg)

**Mike Miller**

---
---
**Mike Miller** *June 28, 2014 22:01*

Two years ago was the year of the Bow and Arrow around here...Pixar's Brave, The Hunger Games, etc. The kids got a bow for Xmas....used twice. 


---
**Eric Lien** *June 28, 2014 23:31*

Looks like it was used for target practice :)


---
**James Rivera** *June 28, 2014 23:50*

Arrowgentius?  ;-)


---
**ThantiK** *June 29, 2014 07:00*

Eeeek, you've gotta stiffen up that frame.


---
**Mike Miller** *June 29, 2014 10:34*

Easy there kiddo, I ran out of washers...before I added all of the cross braces. There are still 4 more extrusions to be added...


---
**Miguel Sánchez** *September 02, 2014 16:30*

What type of bushing to you use on them?


---
**Mike Miller** *September 02, 2014 16:42*

Currently igus plastic bearings. How they perform will determine if I continue to use them, or swap them out for something else. 


---
**Miguel Sánchez** *September 02, 2014 16:44*

But you're keeping your carbon-fiber rods, right?


---
**Mike Miller** *September 02, 2014 17:20*

Look closely, they're Arrows I temporarily stole from my Son's Bow and arrow set. ;)



I'm using 10mm ceramic coated aluminum rods currently. 


---
**Miguel Sánchez** *September 02, 2014 17:48*

I was asking as I was considering the idea of using carbon fiber tubes for my build (maybe just for the cross) but I have failed to see carbon fiber rods mentioned on the group (maybe it's a bad idea). 


---
**Mike Miller** *September 02, 2014 17:54*

They're light, but not particularly stiff in arrow diameters...they're also prone to wear. Folks use em all the time for the arms of deltas, but they wouldn't make a good bearing surface. 


---
**Miguel Sánchez** *September 02, 2014 17:57*

Thanks a lot for the insight **+Mike Miller** . You are right, I've used them for my deltas and because I have some leftovers I thought I might use them for a mini Eustathios I am building.


---
**Mike Miller** *September 02, 2014 18:03*

Give it a shot! I seems like, with every printer I build, I come closer and closer to being able to make an additional printer for free.


---
**Eric Lien** *September 03, 2014 01:14*

**+Mike Miller** if it wasn't for those pesky electronics & vitamins. :)


---
**Mike Miller** *September 03, 2014 01:17*

**+Eric Lien** I'm a gonna have three printers and one e3d I swap between 'em. 


---
**Eric Lien** *September 03, 2014 01:57*

**+Mike Miller** v5 or v6? I am about at my wits end with v5 and pla. Problem is I have qty:2 v5 and only qty:1 v6.


---
**Eric Lien** *September 03, 2014 01:58*

They work fine for abs.


---
**Mike Miller** *September 03, 2014 02:13*

V5...my problems go away when I turn off retraction. ;) 


---
*Imported from [Google+](https://plus.google.com/+MikeMiller0/posts/cKmptfRYAuf) &mdash; content and formatting may not be reliable*
